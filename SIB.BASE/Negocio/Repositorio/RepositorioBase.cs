﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Repositorio;
using System.Data.Linq;
using System.Reflection;

namespace SIB.BASE.Negocio.Repositorio
{
    class RepositorioBase : RepositorioSIB
    {

        //////////////////////////////////////////////////////////////////////////////////
        // DEFINICION DE METODOS PARA EL MANEJO DE USUARIO                             ///
        //////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Nombre: traer_a_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para seleccionar usuario 
        /// Modificacion:
        /// </summary>
        /// <param name="lousuario"></param>
        /// <returns></returns>

        public a_usuario traer_a_usuario(a_usuario lousuario)
        {
            Table<a_usuario> loRegistroASeleccionar = Transaction.GetTable<a_usuario>();
            a_usuario loRegistro = loRegistroASeleccionar.Single(p => p.codigo_usuario == lousuario.codigo_usuario);
            return loRegistro;
        }

        /// <summary>
        /// Nombre: traer_a_usuario1
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para seleccionar usuario 
        /// Modificacion:
        /// </summary>
        /// <param name="lousuario"></param>
        /// <returns></returns>

        public a_usuario traer_a_usuario1(a_usuario lousuario)
        {
            Table<a_usuario> loRegistroASeleccionar = Transaction.GetTable<a_usuario>();
            a_usuario loRegistro = loRegistroASeleccionar.Single(p => p.login == lousuario.login);
            return loRegistro;
        }

        /// <summary>
        /// Nombre: modificar_a_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para modificar usuario 
        /// Modificacion:
        /// </summary>
        /// <param name="lousuario"></param>
        /// 

        public void modificar_a_usuario(a_usuario lousuario)
        {
            Table<a_usuario> loRegistoAModificar = Transaction.GetTable<a_usuario>();
            a_usuario loRegistro = loRegistoAModificar.Single(p => p.codigo_usuario == lousuario.codigo_usuario);

            loRegistro.login = lousuario.login;
            loRegistro.clave = lousuario.clave;
            loRegistro.codigo_grupo_usuario = lousuario.codigo_grupo_usuario;
            loRegistro.nombre = lousuario.nombre;
            loRegistro.fecha_expiracion = lousuario.fecha_expiracion;
            loRegistro.cod_compania = lousuario.cod_compania;
            loRegistro.ind_compania = lousuario.ind_compania;
            loRegistro.codigo_comisionista = lousuario.codigo_comisionista;
            loRegistro.comisionista_principal = lousuario.comisionista_principal;
            loRegistro.codigo_regional = lousuario.codigo_regional;
            loRegistro.cod_compania = lousuario.cod_compania;
            loRegistro.estado = lousuario.estado;
            loRegistro.login_usuario = lousuario.login_usuario;
            loRegistro.fecha_hora_actual = lousuario.fecha_hora_actual;
            loRegistro.codigo_operador = lousuario.codigo_operador;
        }

        /// <summary>
        /// Nombre: guardar_a_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para guardar usuario Subyacente
        /// Modificacion:
        /// </summary>
        /// <param name="lousuario"></param>
        /// 

        public void guardar_a_usuario(a_usuario lousuario)
        {
            Transaction.GetTable<a_usuario>().InsertOnSubmit(lousuario);
        }

        /// <summary>
        /// Nombre: borrar_a_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para borrar usuario Subyacente
        /// Modificacion:
        /// </summary>
        /// <param name="lousuario"></param>
        /// 

        public void borrar_a_usuario(a_usuario lousuario)
        {
            Table<a_usuario> loRegistro = Transaction.GetTable<a_usuario>();

            var loRegistroABorrar = from p in loRegistro
                                    where p.codigo_usuario == lousuario.codigo_usuario
                                    select p;

            loRegistro.DeleteAllOnSubmit(loRegistroABorrar);
        }


        //////////////////////////////////////////////////////////////////////////////////
        // DEFINICION DE METODOS PARA EL MANEJO DE MENU                                ///
        //////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Nombre: traer_a_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para seleccionar menu
        /// Modificacion:
        /// </summary>
        /// <param name="lomenu"></param>
        /// <returns></returns>

        public a_menu traer_a_menu(a_menu lomenu)
        {
            Table<a_menu> loRegistroASeleccionar = Transaction.GetTable<a_menu>();
            a_menu loRegistro = loRegistroASeleccionar.Single(p => p.codigo_menu == lomenu.codigo_menu);
            return loRegistro;
        }

        /// <summary>
        /// Nombre: modificar_a_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para modificar menu 
        /// Modificacion:
        /// </summary>
        /// <param name="lomenu"></param>
        /// 

        public void modificar_a_menu(a_menu lomenu)
        {
            Table<a_menu> loRegistoAModificar = Transaction.GetTable<a_menu>();
            a_menu loRegistro = loRegistoAModificar.Single(p => p.codigo_menu == lomenu.codigo_menu);

            loRegistro.menu = lomenu.menu;
            loRegistro.ruta = lomenu.ruta;
            loRegistro.frame = lomenu.frame;
            loRegistro.tabla = lomenu.tabla;
            loRegistro.visualizar = lomenu.visualizar;
            loRegistro.estado = lomenu.estado;
            loRegistro.login_usuario = lomenu.login_usuario;
            loRegistro.fecha_hora_actual = lomenu.fecha_hora_actual;
        }

        /// <summary>
        /// Nombre: guardar_a_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para guardar menu
        /// Modificacion:
        /// </summary>
        /// <param name="lomenu"></param>
        /// 

        public void guardar_a_menu(a_menu lomenu)
        {
            Transaction.GetTable<a_menu>().InsertOnSubmit(lomenu);
        }

        /// <summary>
        /// Nombre: borrar_a_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para borrar menu
        /// Modificacion:
        /// </summary>
        /// <param name="lousuario"></param>
        /// 

        public void borrar_a_menu(a_menu lomenu)
        {
            Table<a_menu> loRegistro = Transaction.GetTable<a_menu>();

            var loRegistroABorrar = from p in loRegistro
                                    where p.codigo_menu == lomenu.codigo_menu
                                    select p;

            loRegistro.DeleteAllOnSubmit(loRegistroABorrar);
        }


        //////////////////////////////////////////////////////////////////////////////////
        // DEFINICION DE METODOS PARA EL MANEJO DE GRUPO MENU                          ///
        //////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Nombre: traer_a_grupo_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para seleccionar GrupoMenu 
        /// Modificacion:
        /// </summary>
        /// <param name="loGrupoMenu"></param>
        /// <returns></returns>

        public a_grupo_menu traer_a_grupo_menu(a_grupo_menu loGrupoMenu)
        {
            Table<a_grupo_menu> loRegistroASeleccionar = Transaction.GetTable<a_grupo_menu>();
            a_grupo_menu loRegistro = loRegistroASeleccionar.Single(p => p.codigo_grupo_menu == loGrupoMenu.codigo_grupo_menu);
            return loRegistro;
        }

        /// <summary>
        /// Nombre: modificar_a_grupo_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para modificar GrupoMenu 
        /// Modificacion:
        /// </summary>
        /// <param name="loGrupoMenu"></param>
        /// 

        public void modificar_a_grupo_menu(a_grupo_menu loGrupoMenu)
        {
            Table<a_grupo_menu> loRegistoAModificar = Transaction.GetTable<a_grupo_menu>();
            a_grupo_menu loRegistro = loRegistoAModificar.Single(p => p.codigo_grupo_menu == loGrupoMenu.codigo_grupo_menu);
            loRegistro.id_padre = loGrupoMenu.id_padre;
            loRegistro.codigo_grupo_usuario = loGrupoMenu.codigo_grupo_usuario;
            loRegistro.codigo_menu = loGrupoMenu.codigo_menu;
            loRegistro.orden = loGrupoMenu.orden;
            loRegistro.consultar = loGrupoMenu.consultar;
            loRegistro.crear = loGrupoMenu.crear;
            loRegistro.modificar = loGrupoMenu.modificar;
            loRegistro.eliminar = loGrupoMenu.eliminar;
            loRegistro.login_usuario = loGrupoMenu.login_usuario;
            loRegistro.fecha_hora_actual = loGrupoMenu.fecha_hora_actual;
        }

        /// <summary>
        /// Nombre: guardar_a_grupo_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para guardar GrupoMenu 
        /// Modificacion:
        /// </summary>
        /// <param name="loGrupoMenu"></param>
        /// 

        public void guardar_a_grupo_menu(a_grupo_menu loGrupoMenu)
        {
            Transaction.GetTable<a_grupo_menu>().InsertOnSubmit(loGrupoMenu);
        }

        /// <summary>
        /// Nombre: borrar_a_grupo_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para borrar GrupoMenu IVA
        /// Modificacion:
        /// </summary>
        /// <param name="loGrupoMenu"></param>
        /// 

        public void borrar_a_grupo_menu(a_grupo_menu loGrupoMenu)
        {
            Table<a_grupo_menu> loRegistro = Transaction.GetTable<a_grupo_menu>();

            var loRegistroABorrar = from p in loRegistro
                                    where p.codigo_grupo_menu == loGrupoMenu.codigo_grupo_menu
                                    select p;

            loRegistro.DeleteAllOnSubmit(loRegistroABorrar);
        }


        //////////////////////////////////////////////////////////////////////////////////
        // DEFINICION DE METODOS PARA EL MANEJO DE GRUPO usuario                          ///
        //////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Nombre: traer_a_grupo_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para seleccionar Grupousuario 
        /// Modificacion:
        /// </summary>
        /// <param name="loGrupousuario"></param>
        /// <returns></returns>

        public a_grupo_usuario traer_a_grupo_usuario(a_grupo_usuario loGrupousuario)
        {
            Table<a_grupo_usuario> loRegistroASeleccionar = Transaction.GetTable<a_grupo_usuario>();
            a_grupo_usuario loRegistro = loRegistroASeleccionar.Single(p => p.codigo_grupo_usuario == loGrupousuario.codigo_grupo_usuario);
            return loRegistro;
        }

        ///// <summary>
        ///// Nombre: modificar_a_grupo_usuario
        ///// Fecha: Agosto 15 de 2008
        ///// Creador: Olga Lucia Ibanez
        ///// Definicion de metodo para modificar Grupousuario 
        ///// Modificacion:
        ///// </summary>
        ///// <param name="loGrupousuario"></param>
        ///// 

        public void modificar_a_grupo_usuario(a_grupo_usuario loGrupousuario)
        {
            Table<a_grupo_usuario> loRegistoAModificar = Transaction.GetTable<a_grupo_usuario>();
            a_grupo_usuario loRegistro = loRegistoAModificar.Single(p => p.codigo_grupo_usuario == loGrupousuario.codigo_grupo_usuario);
            loRegistro.descripcion = loGrupousuario.descripcion;
            loRegistro.estado = loGrupousuario.estado;
            loRegistro.tipo_perfil = loGrupousuario.tipo_perfil;
            loRegistro.administrador_operador = loGrupousuario.administrador_operador;
            loRegistro.subastador = loGrupousuario.subastador;
            loRegistro.login_usuario = loGrupousuario.login_usuario;
            loRegistro.fecha_hora_actual = loGrupousuario.fecha_hora_actual;
        }

        ///// <summary>
        ///// Nombre: guardar_a_grupo_usuario
        ///// Fecha: Agosto 15 de 2008
        ///// Creador: Olga Lucia Ibanez
        ///// Definicion de metodo para guardar Grupousuario
        ///// Modificacion:
        ///// </summary>
        ///// <param name="loGrupousuario"></param>
        ///// 

        public void guardar_a_grupo_usuario(a_grupo_usuario loGrupousuario)
        {
            Transaction.GetTable<a_grupo_usuario>().InsertOnSubmit(loGrupousuario);
        }

        ///// <summary>
        ///// Nombre: borrar_a_grupo_usuario
        ///// Fecha: Agosto 15 de 2008
        ///// Creador: Olga Lucia Ibanez
        ///// Definicion de metodo para borrar Grupousuario 
        ///// Modificacion:
        ///// </summary>
        ///// <param name="loGrupousuario"></param>
        ///// 

        public void borrar_a_grupo_usuario(a_grupo_usuario loGrupousuario)
        {
            Table<a_grupo_usuario> loRegistro = Transaction.GetTable<a_grupo_usuario>();

            var loRegistroABorrar = from p in loRegistro
                                    where p.codigo_grupo_usuario == loGrupousuario.codigo_grupo_usuario
                                    select p;

            loRegistro.DeleteAllOnSubmit(loRegistroABorrar);
        }

        /////////////////////////////////////////////////////////////////////////
        // DEFINICION DE METODOS PARA EL MANEJO DE LA TABLA DE BLOQUEOS///
        /////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Nombre: guardar_a_bloqueo_registro
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para guardar tabla de bloqueos
        /// Modificacion:
        /// </summary>
        /// <param name="loBloqueoRegistro"></param>
        /// 

        public void guardar_a_bloqueo_registro(a_bloqueo_registro loBloqueoRegistro)
        {
            Transaction.GetTable<a_bloqueo_registro>().InsertOnSubmit(loBloqueoRegistro);
        }

        /// <summary>
        /// Nombre: borrar_a_bloqueo_registro
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para borrar almacenes de deposito
        /// Modificacion:
        /// </summary>
        /// <param name="loBloqueoRegistro"></param>
        /// 

        public void borrar_a_bloqueo_registro(a_bloqueo_registro loBloqueoRegistro)
        {
            Table<a_bloqueo_registro> loRegistro = Transaction.GetTable<a_bloqueo_registro>();

            var loRegistroABorrar = from p in loRegistro
                                    where p.nombre_tabla == loBloqueoRegistro.nombre_tabla &
                                          p.llave_registro == loBloqueoRegistro.llave_registro
                                    select p;

            loRegistro.DeleteAllOnSubmit(loRegistroABorrar);
        }

        /// <summary>
        /// Nombre: borrar_a_bloqueo_registro_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para borrar almacenes de deposito
        /// Modificacion:
        /// </summary>
        /// <param name="loBloqueoRegistro"></param>
        /// 

        public void borrar_a_bloqueo_registro_usuario(a_bloqueo_registro loBloqueoRegistro)
        {
            Table<a_bloqueo_registro> loRegistro = Transaction.GetTable<a_bloqueo_registro>();

            var loRegistroABorrar = from p in loRegistro
                                    where p.usuario_bloqueo == loBloqueoRegistro.usuario_bloqueo
                                    select p;

            loRegistro.DeleteAllOnSubmit(loRegistroABorrar);
        }


        //////////////////////////////////////////////////////////////////////////////////
        // DEFINICION DE METODOS PARA EL MANEJO DE FESTIVO                             ///
        //////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Nombre: traer_m_festivo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para seleccionar Festivo
        /// Modificacion:
        /// </summary>
        /// <param name="loFestivo"></param>
        /// <returns></returns>

        public m_festivo traer_m_festivo(m_festivo loFestivo)
        {
            Table<m_festivo> loRegistroASeleccionar = Transaction.GetTable<m_festivo>();
            m_festivo loRegistro = loRegistroASeleccionar.Single(p => p.fecha == loFestivo.fecha);
            return loRegistro;
        }

        /// <summary>
        /// Nombre: modificar_m_festivo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para modificar Festivo
        /// Modificacion:
        /// </summary>
        /// <param name="loFestivo"></param>
        /// 

        public void modificar_m_festivo(m_festivo loFestivo)
        {
            Table<m_festivo> loRegistoAModificar = Transaction.GetTable<m_festivo>();
            m_festivo loRegistro = loRegistoAModificar.Single(p => p.fecha == loFestivo.fecha);
            loRegistro.ano = loFestivo.ano;
            loRegistro.mes = loFestivo.mes;
            loRegistro.dia = loFestivo.dia;
            loRegistro.ind_dia_habil = loFestivo.ind_dia_habil;
            loRegistro.estado = loFestivo.estado;
            loRegistro.login_usuario = loFestivo.login_usuario;
            loRegistro.fecha_hora_actual = loFestivo.fecha_hora_actual;

        }

        /// <summary>
        /// Nombre: guardar_m_festivo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para guardar Festivo
        /// Modificacion:
        /// </summary>
        /// <param name="loFestivo"></param>
        /// 

        public void guardar_m_festivo(m_festivo loFestivo)
        {
            Transaction.GetTable<m_festivo>().InsertOnSubmit(loFestivo);
        }

        /// <summary>
        /// Nombre: borrar_m_festivo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Definicion de metodo para borrar Festivo
        /// Modificacion:
        /// </summary>
        /// <param name="loFestivo"></param>
        /// 

        public void borrar_m_festivo(m_festivo loFestivo)
        {
            Table<m_festivo> loRegistro = Transaction.GetTable<m_festivo>();

            var loRegistroABorrar = from p in loRegistro
                                    where p.fecha == loFestivo.fecha
                                    select p;

            loRegistro.DeleteAllOnSubmit(loRegistroABorrar);
        }



    }
}

