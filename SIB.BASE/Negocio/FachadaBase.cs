﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using SIB.Global.Dominio;
using SIB.Global.Negocio;
using SIB.Global.Presentacion;
using SIB.BASE.Negocio.Manejador;
using SIB.Global.Negocio.Manejador;
using PCD_Infraestructura.Business;

namespace SIB.BASE.Negocio
{
    public class FachadaBase : FachadaSIB, INegocioBase
    {
        /// <summary>
        /// 
        /// </summary>
        private static FachadaBase fachada;

        /// <summary>
        /// 
        /// </summary>
        public static FachadaBase Instance
        {
            get
            {
                if (fachada == null)
                {
                    fachada = new FachadaBase();
                }

                return fachada;
            }
        }

        //////////////////////////////////////////////////////////////////
        // DEFINICION DE METODOS GENERALES USADOS EN EL MODULO BASE     //
        //////////////////////////////////////////////////////////////////


        /// <summary>
        /// Nombre: ValidarExistencia
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo general usado para validacion de existencia de registros en la Base de Datos
        /// Modificacion:
        /// </summary>
        /// <param name="lsTabla"></param>
        /// <param name="lsCondicion"></param>
        /// <param name="infoSession"></param>
        /// <returns></returns>
        /// 
        public bool ValidarExistencia(string lsTabla, string lsCondicion, InfoSessionVO infoSession)
        {
            clValidaciones lValidacion = new clValidaciones();
            return lValidacion.ValidarExistencia(lsTabla, lsCondicion, infoSession);
        }

        /// <summary>
        /// Nombre: ValidarDigitoVerificacion
        /// Fecha: Agosto 17 de 2008
        /// Descripcion: Resuelve el digito de verificacion de un nit ingresado
        /// Recibe como parametro el nit en tipo de dato entero largo y devuelve un entero
        /// Modificacion:
        /// </summary>
        /// <param name="lsTabla"></param>
        /// <param name="lsCondicion"></param>
        /// <param name="infoSession"></param>
        /// <returns></returns>
        /// 

        public int ValidarDigitoVerificacion(long llnit_tercero)
        {
            clValidaciones lValidacion = new clValidaciones();
            return lValidacion.ValidarDigitoVerificacion(llnit_tercero);
        }
        /// <summary>
        /// Nombre: ValidaCont
        /// Fecha: Agosto 17 de 2008
        /// Descripcion: valida la complejidad de la contraseña
        /// Recibe como parametro la contraseña
        /// Modificacion:
        /// </summary>
        /// <param name="lsTabla"></param>
        /// <param name="lsCondicion"></param>
        /// <param name="infoSession"></param>
        /// <returns></returns>
        /// 

        public string ValidaCont(string lsContraseña, string lsContraseñaEncrip, string lsUsuario, string lsIndicador, InfoSessionVO lsInfo)
        {
            clValidaciones lValidacion = new clValidaciones();
            return lValidacion.ValidaCont(lsContraseña, lsContraseñaEncrip, lsUsuario, lsIndicador, lsInfo);
        }
        /// <summary>
        /// Nombre: LlenarGrilla
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que Implementa la Interface LlenarGrilla
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <param name="lProcedimiento"></param>
        /// <returns></returns>
        public DataView LlenarGrilla(SqlConnection lConexion, String lsProcedimiento)
        {
            clProcesosSql lProcesos = new clProcesosSql(lsProcedimiento);
            return lProcesos.LlenarGrilla(lConexion);
        }

        /// <summary>
        /// Nombre: LlenarGrillaConParametros
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que Implementa la Interface LlenarGrillaConParametros
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <returns></returns>
        public DataView LlenarGrillaConParametros(SqlConnection lConexion, string lsProcedimiento, string[] lsNombrearametros, SqlDbType[] lsTipoParametros, string[] lsValorParametros)
        {
            clProcesosSql lProcesos = new clProcesosSql(lsProcedimiento, lsNombrearametros, lsTipoParametros, lsValorParametros);
            return lProcesos.LlenarGrillaConParametros(lConexion);
        }

        /// <summary>
        /// Nombre: LlenarGrillaParametros
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que Implementa la Interface LlenarGrillaParametros
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <returns></returns>
        public DataView LlenarGrillaParametros(SqlConnection lConexion, String lsProcedimiento, String lsCondicion)
        {
            clProcesosSql lProcesos = new clProcesosSql(lsProcedimiento);
            return lProcesos.LlenarGrillaParametros(lConexion, lsCondicion);
        }

        /// <summary>
        /// Nombre: LlenarControl
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que Implementa la Interface LlenarControl
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <param name="lProcedimiento"></param>
        /// <returns></returns>
        public SqlDataReader LlenarControl(SqlConnection lConexion, String lsProcedimiento, string lsTabla, string lsCondicion)
        {
            clProcesosSql lProcesos = new clProcesosSql(lsProcedimiento, lsTabla, lsCondicion);
            return lProcesos.LlenarControl(lConexion);
        }

        /// <summary>
        /// Nombre: llenarHtmlTabla
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que Implementa la Interface llenarHtmlTabla
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <param name="lProcedimiento"></param>
        /// <returns></returns>
        public String llenarHtmlTabla(SqlConnection lConexion, string lsProcedimiento, string lsCondicion, string[] lsColumnas, string lsTabla)
        {
            clProcesosSql lProcesos = new clProcesosSql(lsProcedimiento, lsTabla, lsCondicion);
            return lProcesos.llenarHtmlTabla(lConexion, lsColumnas);
        }

        /// <summary>
        /// Nombre: llenarHtmlTabla
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que Implementa la Interface llenarHtmlTabla, para exportar con procedimientos de parametros variables
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <param name="lsProcedimiento"></param>
        /// <param name="lsNombrearametros"></param>
        /// <param name="lsValorParametros"></param>
        /// <param name="lsColumnas"></param>
        /// <returns></returns>
        public String llenarHtmlTabla(SqlConnection lConexion, string lsProcedimiento, string[] lsNombrearametros, string[] lsValorParametros, string[] lsColumnas, string lsIndica)
        {
            clProcesosSql lProcesos = new clProcesosSql(lsProcedimiento, lsNombrearametros, lsValorParametros);
            return lProcesos.llenarHtmlTabla(lConexion, lsColumnas, lsIndica);
        }


        /// <summary>
        /// Nombre: EjecutarProcedimientoYObtenerDatos
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que Implementa la Interface EjecutarProcedimientoYObtenerDatos, para ejecutar un procedimiento de
        /// actualizacion de inf en la base de datos y devuleve un conjunto de registros
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <param name="lsProcedimiento"></param>
        /// <param name="lsNombreParametros"></param>
        /// <param name="lTipoparametros"></param>
        /// <param name="lValorParametros"></param>
        /// <returns></returns>
        public SqlDataReader EjecutarProcedimientoYObtenerDatos(SqlConnection lConexion, String lsProcedimiento, string[] lsNombreParametros, SqlDbType[] lTipoparametros, Object[] lValorParametros)
        {
            clProcesosSql lProcesos = new clProcesosSql(lsProcedimiento, lsNombreParametros, lTipoparametros, lValorParametros);
            return lProcesos.EjecutarProcedimientoYObtenerDatos(lConexion);
        }



        /// <summary>
        /// Nombre: CifrarCadena
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que encriptar la Contraseña
        /// Modificacion:
        /// </summary>
        /// <param name="lsCadena"></param>
        /// <returns></returns>
        public String CifrarCadena(string lsCadena)
        {
            encriptar lProcesos = new encriptar();
            return lProcesos.CifrarCadena(lsCadena);
        }

        /// <summary>
        /// Nombre: EjecutarProcedimiento
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que Implementa la Interface EjecutarProcedimiento, para ejecutar un procedimiento almacenado
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion">Conexion</param>
        /// <param name="lsProcedimiento">Nombre del Procedimiento</param>
        /// <param name="lsNombrearametros">Arreglo con los Nombres de los Parametros</param>
        /// <param name="lsTipoParametros">Arreglo con los Tipos de los Parametros</param>
        /// <param name="lsValorParametros">Arreglo con los Valores de los Parametros</param>
        /// <returns></returns>
        public bool EjecutarProcedimiento(SqlConnection lConexion, string lsProcedimiento, string[] lsNombrearametros, SqlDbType[] lsTipoParametros, string[] lsValorParametros)
        {
            clProcesosSql lProcesos = new clProcesosSql(lsProcedimiento, lsNombrearametros, lsTipoParametros, lsValorParametros);
            return lProcesos.EjecutarProcedimiento(lConexion);
        }

        /// <summary>
        /// Nombre: EjecutarProcedimientoYObtenerDatos
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que Implementa la Interface EjecutarProcedimientoYObtenerDatos, para ejecutar un procedimiento de
        /// actualizacion de inf en la base de datos y devuleve un conjunto de registros
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <param name="lsProcedimiento"></param>
        /// <param name="lsNombreParametros"></param>
        /// <param name="lTipoparametros"></param>
        /// <param name="lValorParametros"></param>
        /// <returns></returns>
        public SqlDataReader EjecutarProcedimientoYObtenerDatos(SqlConnection lConexion, String lsProcedimiento, string[] lsNombreParametros, SqlDbType[] lTipoparametros, Object[] lValorParametros, InfoSessionVO infoSession)
        {
            clProcesosSql lProcesos = new clProcesosSql(lsProcedimiento, lsNombreParametros, lTipoparametros, lValorParametros);
            return lProcesos.EjecutarProcedimientoYObtenerDatos(lConexion, infoSession);
        }
        /// <summary>
        /// Nombre: EjecutarProcedimiento
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que Implementa la Interface EjecutarProcedimiento, para ejecutar un procedimiento de
        /// actualizacion de inf en la base de datos y devuleve el estado de la actualizacion.
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <param name="lsProcedimiento"></param>
        /// <param name="lsNombreParametros"></param>
        /// <param name="lTipoparametros"></param>
        /// <param name="lValorParametros"></param>
        /// <returns></returns>
        public bool EjecutarProcedimiento(SqlConnection lConexion, String lsProcedimiento, string[] lsNombreParametros, SqlDbType[] lTipoparametros, Object[] lValorParametros, InfoSessionVO infoSession)
        {
            clProcesosSql lProcesos = new clProcesosSql(lsProcedimiento, lsNombreParametros, lTipoparametros, lValorParametros);
            return lProcesos.EjecutarProcedimiento(lConexion, infoSession);
        }


        /// <summary>
        /// Nombre: EjecutarProcedimientoYObtenerDatosConTransaccion
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que Implementa la Interface EjecutarProcedimientoYObtenerDatosConTransaccion, para ejecutar un procedimiento de
        ///              actualizacion de inf en la base de datos controlando transaccion y devuleve un conjunto de registros
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <param name="lsProcedimiento"></param>
        /// <param name="lsNombreParametros"></param>
        /// <param name="lTipoparametros"></param>
        /// <param name="lValorParametros"></param>
        /// <param name="oTransaccion"></param>
        /// <returns></returns>
        public SqlDataReader EjecutarProcedimientoYObtenerDatosConTransaccion(SqlConnection lConexion, String lsProcedimiento, string[] lsNombreParametros, SqlDbType[] lTipoparametros, Object[] lValorParametros, SqlTransaction oTransaccion)
        {
            clProcesosSql lProcesos = new clProcesosSql(lsProcedimiento, lsNombreParametros, lTipoparametros, lValorParametros);
            return lProcesos.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion, oTransaccion);
        }


        /// <summary>
        /// Nombre: EjecutarProcedimientoRetornando
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que Implementa la Interface EjecutarProcedimientoYObtenerDatos, para ejecutar un procedimiento de
        /// actualizacion de inf en la base de datos y devuleve un conjunto de registros
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <param name="lsProcedimiento"></param>
        /// <param name="lsNombreParametros"></param>
        /// <param name="lTipoparametros"></param>
        /// <param name="lValorParametros"></param>
        /// <returns></returns>
        public SqlDataReader EjecutarProcedimientoRetornando(SqlConnection lConexion, String lsProcedimiento)
        {
            clProcesosSql lProcesos = new clProcesosSql(lsProcedimiento);
            return lProcesos.EjecutarProcedimientoRetornando(lConexion);
        }

        /// <summary>
        /// Nombre: EjecutarProcedimientoConTransaccion
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que Implementa la Interface EjecutarProcedimientoConTransaccion, para ejecutar un procedimiento de
        /// actualizacion de inf en la base de datos controlado transacciones y devuleve el estado de la actualizacion.
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <param name="lsProcedimiento"></param>
        /// <param name="lsNombreParametros"></param>
        /// <param name="lTipoparametros"></param>
        /// <param name="lValorParametros"></param>
        /// <param name="oTransaccion"></param>
        /// <returns></returns>
        public bool EjecutarProcedimientoConTransaccion(SqlConnection lConexion, String lsProcedimiento, string[] lsNombreParametros, SqlDbType[] lTipoparametros, Object[] lValorParametros, SqlTransaction oTransaccion)
        {
            clProcesosSql lProcesos = new clProcesosSql(lsProcedimiento, lsNombreParametros, lTipoparametros, lValorParametros);
            return lProcesos.EjecutarProcedimientoConTransaccion(lConexion, oTransaccion);
        }

        /// <summary>
        /// Nombre: EjecutarProcedimientoYObtenerDatosConTransaccion
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que Implementa la Interface EjecutarProcedimientoYObtenerDatosConTransaccion, para ejecutar un procedimiento de
        ///              actualizacion de inf en la base de datos controlando transaccion y devuleve un conjunto de registros
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <param name="lsProcedimiento"></param>
        /// <param name="lsNombreParametros"></param>
        /// <param name="lTipoparametros"></param>
        /// <param name="lValorParametros"></param>
        /// <param name="oTransaccion"></param>
        /// <returns></returns>
        public SqlDataReader EjecutarProcedimientoYObtenerDatosConTransaccion(SqlConnection lConexion, String lsProcedimiento, string[] lsNombreParametros, SqlDbType[] lTipoparametros, Object[] lValorParametros, SqlTransaction oTransaccion, InfoSessionVO oInfo)
        {
            clProcesosSql lProcesos = new clProcesosSql(lsProcedimiento, lsNombreParametros, lTipoparametros, lValorParametros);
            return lProcesos.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion, oTransaccion, oInfo);
        }
        /// <summary>
        /// Nombre: EjecutarProcedimientoConTransaccion
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que Implementa la Interface EjecutarProcedimientoConTransaccion, para ejecutar un procedimiento de
        /// actualizacion de inf en la base de datos controlado transacciones y devuleve el estado de la actualizacion.
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <param name="lsProcedimiento"></param>
        /// <param name="lsNombreParametros"></param>
        /// <param name="lTipoparametros"></param>
        /// <param name="lValorParametros"></param>
        /// <param name="oTransaccion"></param>
        /// <returns></returns>
        public bool EjecutarProcedimientoConTransaccion(SqlConnection lConexion, String lsProcedimiento, string[] lsNombreParametros, SqlDbType[] lTipoparametros, Object[] lValorParametros, SqlTransaction oTransaccion, InfoSessionVO oInfo)
        {
            clProcesosSql lProcesos = new clProcesosSql(lsProcedimiento, lsNombreParametros, lTipoparametros, lValorParametros);
            return lProcesos.EjecutarProcedimientoConTransaccion(lConexion, oTransaccion, oInfo);
        }


        //////////////////////////////////////////////////////////////////////////////////
        // DEFINICION DE METODOS PARA MANEJO DE USUARIO                                //
        //////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Nombre: traer_a_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir la seleccion de usuario 
        /// Modificacion:
        /// </summary>
        /// <param name="infoSession"></param>
        /// <param name="lousuario"></param>
        /// <returns></returns>
        /// 
        public a_usuario traer_a_usuario(InfoSessionVO infoSession, a_usuario lousuario)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            return loManejador.traer_a_usuario(lousuario);
        }

        /// <summary>
        /// Nombre: traer_a_usuario1
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir la seleccion de usuario 
        /// Modificacion:
        /// </summary>
        /// <param name="infoSession"></param>
        /// <param name="lousuario"></param>
        /// <returns></returns>
        /// 
        public a_usuario traer_a_usuario1(InfoSessionVO infoSession, a_usuario lousuario)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            return loManejador.traer_a_usuario1(lousuario);
        }

        /// <summary>
        /// Nombre: modificar_a_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir la modificacion de usuario Subyacente
        /// Modificacion:
        /// </summary>
        /// <param name="infoSession"></param>
        /// <param name="lousuario"></param>

        public void modificar_a_usuario(InfoSessionVO infoSession, a_usuario lousuario)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            loManejador.modificar_a_usuario(lousuario);
        }

        /// <summary>
        /// Nombre: guardar_a_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir el grabado de usuario Subyacente
        /// Modificacion:
        /// </summary>
        /// <param name="infoSession"></param>
        /// <param name="loAlmacenDeposito"></param>

        public void guardar_a_usuario(InfoSessionVO infoSession, a_usuario lousuario)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            loManejador.guardar_a_usuario(lousuario);
        }

        /// <summary>
        /// Nombre: borrar_a_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir el borrado de usuario Subyacente
        /// Modificacion:
        /// </summary>
        /// <param name="liSubyacente"></param>
        /// <param name="lipais"></param>
        /// <param name="liDpto"></param>
        /// <param name="liCiudad"></param>
        /// <param name="ldFecha"></param>
        /// 
        public void borrar_a_usuario(InfoSessionVO infoSession, int liCodUsuario)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            loManejador.borrar_a_usuario(liCodUsuario);
        }



        //////////////////////////////////////////////////////////////////////////////////
        // DEFINICION DE METODOS PARA MANEJO DE MENU                                    //
        //////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Nombre: traer_a_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir la seleccion de menu
        /// Modificacion:
        /// </summary>
        /// <param name="infoSession"></param>
        /// <param name="lomenu"></param>
        /// <returns></returns>
        /// 
        public a_menu traer_a_menu(InfoSessionVO infoSession, a_menu lomenu)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            return loManejador.traer_a_menu(lomenu);
        }

        /// <summary>
        /// Nombre: modificar_a_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir la modificacion de menu
        /// Modificacion:
        /// </summary>
        /// <param name="infoSession"></param>
        /// <param name="lomenu"></param>

        public void modificar_a_menu(InfoSessionVO infoSession, a_menu lomenu)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            loManejador.modificar_a_menu(lomenu);
        }

        /// <summary>
        /// Nombre: guardar_a_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir el grabado de menu
        /// Modificacion:
        /// </summary>
        /// <param name="infoSession"></param>
        /// <param name="lomenu"></param>

        public void guardar_a_menu(InfoSessionVO infoSession, a_menu lomenu)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            loManejador.guardar_a_menu(lomenu);
        }

        /// <summary>
        /// Nombre: borrar_a_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir el borrado de usuario Subyacente
        /// Modificacion:
        /// </summary>
        /// <param name="liCodMenu"></param>
        public void borrar_a_menu(InfoSessionVO infoSession, int liCodMenu)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            loManejador.borrar_a_menu(liCodMenu);
        }



        //////////////////////////////////////////////////////////////////////////////////
        // DEFINICION DE METODOS PARA MANEJO DE GRUPO MENU                              //
        //////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Nombre: traer_a_grupo_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir la seleccion de GrupoMenus 
        /// Modificacion:
        /// </summary>
        /// <param name="infoSession"></param>
        /// <param name="loGrupoMenu"></param>
        /// <returns></returns>
        /// 
        public a_grupo_menu traer_a_grupo_menu(InfoSessionVO infoSession, a_grupo_menu loGrupoMenu)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            return loManejador.traer_a_grupo_menu(loGrupoMenu);
        }

        /// <summary>
        /// Nombre: modificar_a_grupo_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir la modificacion GrupoMenus iva
        /// Modificacion:
        /// </summary>
        /// <param name="infoSession"></param>
        /// <param name="loGrupoMenu"></param>

        public void modificar_a_grupo_menu(InfoSessionVO infoSession, a_grupo_menu loGrupoMenu)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            loManejador.modificar_a_grupo_menu(loGrupoMenu);
        }

        /// <summary>
        /// Nombre: guardar_a_grupo_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir el grabado de GrupoMenus IVA
        /// Modificacion:
        /// </summary>
        /// <param name="infoSession"></param>
        /// <param name="loAlmacenDeposito"></param>

        public void guardar_a_grupo_menu(InfoSessionVO infoSession, a_grupo_menu loGrupoMenu)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            loManejador.guardar_a_grupo_menu(loGrupoMenu);
        }

        /// <summary>
        /// Nombre: borrar_a_grupo_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir el borrado de GrupoMenus
        /// Modificacion:
        /// </summary>
        /// <param name="liGrupoMenu"></param>
        /// <param name="ldFecha"></param>

        public void borrar_a_grupo_menu(InfoSessionVO infoSession, int liGrupoMenu)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            loManejador.borrar_a_grupo_menu(liGrupoMenu);
        }

        //////////////////////////////////////////////////////////////////////////////////
        // DEFINICION DE METODOS PARA MANEJO DE GRUPO usuario                              //
        //////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Nombre: traer_a_grupo_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir la seleccion de Grupousuarios 
        /// Modificacion:
        /// </summary>
        /// <param name="infoSession"></param>
        /// <param name="loGrupousuario"></param>
        /// <returns></returns>
        /// 
        public a_grupo_usuario traer_a_grupo_usuario(InfoSessionVO infoSession, a_grupo_usuario loGrupousuario)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            return loManejador.traer_a_grupo_usuario(loGrupousuario);
        }

        /// <summary>
        /// Nombre: modificar_a_grupo_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir la modificacion Grupousuarios 
        /// Modificacion:
        /// </summary>
        /// <param name="infoSession"></param>
        /// <param name="loGrupousuario"></param>

        public void modificar_a_grupo_usuario(InfoSessionVO infoSession, a_grupo_usuario loGrupousuario)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            loManejador.modificar_a_grupo_usuario(loGrupousuario);
        }

        ///// <summary>
        ///// Nombre: guardar_a_grupo_usuario
        ///// Fecha: Agosto 15 de 2008
        ///// Creador: Olga Lucia Ibanez
        ///// Metodo definir el grabado de Grupousuarios
        ///// Modificacion:
        ///// </summary>
        ///// <param name="infoSession"></param>
        ///// <param name="loAlmacenDeposito"></param>

        public void guardar_a_grupo_usuario(InfoSessionVO infoSession, a_grupo_usuario loGrupousuario)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            loManejador.guardar_a_grupo_usuario(loGrupousuario);
        }

        ///// <summary>
        ///// Nombre: borrar_a_grupo_usuario
        ///// Fecha: Agosto 15 de 2008
        ///// Creador: Olga Lucia Ibanez
        ///// Metodo definir el borrado de Grupousuarios
        ///// Modificacion:
        ///// </summary>
        ///// <param name="liGrupousuario"></param>
        public void borrar_a_grupo_usuario(InfoSessionVO infoSession, int liGrupousuario)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            loManejador.borrar_a_grupo_usuario(liGrupousuario);
        }

        //////////////////////////////////////////////////////////////////
        // DEFINICION DE METODOS PARA MANEJO BLOQUEOS DE REGISTOS       //
        //////////////////////////////////////////////////////////////////

        /// <summary>
        /// Nombre: guardar_a_bloqueo_registro
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir el grabado de bloqueos de registro
        /// Modificacion:
        /// </summary>
        /// <param name="infoSession"></param>
        /// <param name="loBloqueoRegistro"></param>

        public void guardar_a_bloqueo_registro(InfoSessionVO infoSession, a_bloqueo_registro loBloqueoRegistro)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            loManejador.guardar_a_bloqueo_registro(loBloqueoRegistro);
        }

        /// <summary>
        /// Nombre: borrar_a_bloqueo_registro_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir el borrado de bloqueos de registro
        /// Modificacion:
        /// </summary>
        /// <param name="infoSession"></param>
        /// <param name="lsNombretabla"></param>
        /// <param name="lsLlaveRegistro"></param>
        public void borrar_a_bloqueo_registro_usuario(InfoSessionVO infoSession, string lsUsuario)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            loManejador.borrar_a_bloqueo_registro_usuario(lsUsuario);
        }


        /// <summary>
        /// Nombre: borrar_a_bloqueo_registro
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir el borrado de bloqueos de registro
        /// Modificacion:
        /// </summary>
        /// <param name="infoSession"></param>
        /// <param name="lsNombretabla"></param>
        /// <param name="lsLlaveRegistro"></param>
        public void borrar_a_bloqueo_registro(InfoSessionVO infoSession, string lsNombreTabla, string lsLlaveTabla)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            loManejador.borrar_a_bloqueo_registro(lsNombreTabla, lsLlaveTabla);
        }


        //////////////////////////////////////////////////////////////////////////////////
        // DEFINICION DE METODOS PARA MANEJO DE FESTIVO                                 //
        //////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Nombre: traer_m_festivo1
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir la seleccion de Festivo
        /// Modificacion:
        /// </summary>
        /// <param name="infoSession"></param>
        /// <param name="loFestivo"></param>
        /// <returns></returns>
        /// 
        public m_festivo traer_m_festivo1(InfoSessionVO infoSession, m_festivo loFestivo)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            return loManejador.traer_m_festivo1(loFestivo);
        }



        /// <summary>
        /// Nombre: traer_m_festivo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir la seleccion de Festivo
        /// Modificacion:
        /// </summary>
        /// <param name="infoSession"></param>
        /// <param name="loFestivo"></param>
        /// <returns></returns>
        /// 
        public m_festivo traer_m_festivo(InfoSessionVO infoSession, m_festivo loFestivo)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            return loManejador.traer_m_festivo(loFestivo);
        }

        /// <summary>
        /// Nombre: modificar_m_festivo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir la modificacion de Festivo
        /// Modificacion:
        /// </summary>
        /// <param name="infoSession"></param>
        /// <param name="loFestivo"></param>

        public void modificar_m_festivo(InfoSessionVO infoSession, m_festivo loFestivo)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            loManejador.modificar_m_festivo(loFestivo);
        }

        /// <summary>
        /// Nombre: guardar_m_festivo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir el grabado de Festivo
        /// Modificacion:
        /// </summary>
        /// <param name="infoSession"></param>
        /// <param name="loAlmacenDeposito"></param>

        public void guardar_m_festivo(InfoSessionVO infoSession, m_festivo loFestivo)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            loManejador.guardar_m_festivo(loFestivo);
        }

        /// <summary>
        /// Nombre: borrar_m_festivo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo definir el borrado de Festivo
        /// Modificacion:
        /// </summary>
        /// <param name="liAno"></param>
        /// <param name="liMes"></param>
        /// <param name="liDia"></param>
        /// 
        public void borrar_m_festivo(InfoSessionVO infoSession, DateTime ldFecha)
        {
            ManejadorBase loManejador = (ManejadorBase)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorBase), infoSession);
            loManejador.borrar_m_festivo(ldFecha);
        }

        /// <summary>
        /// Nombre: put_archivo
        /// Fecha: Enero 7 de 2009
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que implementa la interface put_archivo, para pasar por FTP, archivo de carga de OPE, del Servidor Web
        ///              al Servidor de Base de datos.
        /// Modificacion:
        /// </summary>
        /// <param name="lsNombre"></param>
        /// <param name="lsUrl"></param>
        /// <param name="lsUsuario"></param>
        /// <param name="lsClave"></param>
        /// <returns></returns>
        public bool put_archivo(string lsNombre, string lsUrl, string lsUsuario, string lsClave)
        {
            clFtpArchivo lFtpArchivo = new clFtpArchivo(lsNombre, lsUrl, lsUsuario, lsClave);
            return lFtpArchivo.put_archivo();
        }


    }

}
