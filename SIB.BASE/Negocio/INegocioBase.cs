﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

namespace SIB.BASE.Negocio
{
    public interface INegocioBase
    {
        Boolean autenticar(InfoSessionVO info);

        Boolean actualizarPassword(InfoSessionVO info, string nuevoPassword);

        void accesoSistema(InfoSessionVO info);

        void registrarProceso(InfoSessionVO info, string lsProceso, string lsRegistro);

        List<Hashtable> consultarMenu(InfoSessionVO info, int liPadre);

        List<Hashtable> consultarMenuGrupo(InfoSessionVO info, int liPadre, int liGrupoUsuario);

        Hashtable consultarPermisoMenu(InfoSessionVO info, String lsTabla);

        //IQueryable<T> consultarTabla<T>(InfoSessionVO infoSession, IEnumerable<T> collection, List<String> llFiltro );
        DataTable consultarTabla<T>(InfoSessionVO infoSession, IEnumerable<T> collection, List<String> llFiltro, string lsCondicion);

        string DefinirFiltros<T>(InfoSessionVO infoSession, IEnumerable<T> collection, List<String> llFiltro, string lsCondicion);

        IQueryable<String> ayudaMaestro(InfoSessionVO infoSession, string lsCadenaBusqueda, string lsTabla, string lsCodigo, string lsDescripcion, string lsTipo);

        IQueryable<String> ayudaMaestroSib(InfoSessionVO infoSession, string lsCadenaBusqueda, string lsTabla, string lsCodigo, string lsDescripcion, string lsTipo);

        IQueryable<String> ayudaMaestroCondicion(InfoSessionVO infoSession, string lsCadenaBusqueda, string lsTabla, string lsCodigo, string lsDescripcion, string lsTipo, string lsCondicion);

        bool ValidarExistencia(string lsTabla, string lsCondicion, InfoSessionVO infoSession);

        int ValidarDigitoVerificacion(long llnit_tercero);

        DataView LlenarGrilla(SqlConnection lConexion, string lProdimiento);

        SqlDataReader LlenarControl(SqlConnection lConexion, string lsProdimiento, string lsTabla, string lsCondicion);

        DataView LlenarGrillaConParametros(SqlConnection lConexion, string lsProdimiento, string[] lsNombrearametros, SqlDbType[] lsTipoParametros, string[] lsValorParametros);

        DataView LlenarGrillaParametros(SqlConnection lConexion, string lsProdimiento, string lsCondicion);

        String llenarHtmlTabla(SqlConnection lConexion, string lsProdimiento, string lsCondicion, string[] lsColumnas, string lsTabla);

        String llenarHtmlTabla(SqlConnection lConexion, string lsProdimiento, string[] lsNombrearametros, string[] lsValorParametros, string[] lsColumnas, string lsIndica);

        String CifrarCadena(string lsContrasena);

        bool EjecutarProcedimiento(SqlConnection lConexion, string lsProcedimiento, string[] lsNombrearametros, SqlDbType[] lsTipoParametros, string[] lsValorParametros);

        SqlDataReader EjecutarProcedimientoYObtenerDatos(SqlConnection lConexion, string lsProdimiento, string[] lsNombreParametros, SqlDbType[] lTipoparametros, Object[] lValorParametros);

        SqlDataReader EjecutarProcedimientoYObtenerDatos(SqlConnection lConexion, string lsProdimiento, string[] lsNombreParametros, SqlDbType[] lTipoparametros, Object[] lValorParametros, InfoSessionVO infoSession);

        bool EjecutarProcedimiento(SqlConnection lConexion, string lsProdimiento, string[] lsNombreParametros, SqlDbType[] lTipoparametros, Object[] lValorParametros, InfoSessionVO infoSession);

        string ValidaCont(string lsContraseña, string lsContraseñaEncrip, string lsUsuario, string lsIndicador, InfoSessionVO lsInfo);

        SqlDataReader EjecutarProcedimientoYObtenerDatosConTransaccion(SqlConnection lConexion, string lsProdimiento, string[] lsNombreParametros, SqlDbType[] lTipoparametros, Object[] lValorParametros, SqlTransaction oTransaccion);

        SqlDataReader EjecutarProcedimientoYObtenerDatosConTransaccion(SqlConnection lConexion, string lsProdimiento, string[] lsNombreParametros, SqlDbType[] lTipoparametros, Object[] lValorParametros, SqlTransaction oTransaccion, InfoSessionVO oInfo);

        SqlDataReader EjecutarProcedimientoRetornando(SqlConnection lConexion, string lsProdimiento);

        bool EjecutarProcedimientoConTransaccion(SqlConnection lConexion, String lsProcedimiento, string[] lsNombreParametros, SqlDbType[] lTipoparametros, Object[] lValorParametros, SqlTransaction oTransaccion);

        bool EjecutarProcedimientoConTransaccion(SqlConnection lConexion, String lsProcedimiento, string[] lsNombreParametros, SqlDbType[] lTipoparametros, Object[] lValorParametros, SqlTransaction oTransaccion, InfoSessionVO oInfo);



        ///////////////////////////////////////////////////////////////////////
        ///// DEFINICION DE  USUARIOS                                       ///
        ///////////////////////////////////////////////////////////////////////

        a_usuario traer_a_usuario(InfoSessionVO infoSession, a_usuario loUsuario);

        a_usuario traer_a_usuario1(InfoSessionVO infoSession, a_usuario loUsuario);

        void modificar_a_usuario(InfoSessionVO infoSession, a_usuario loUsuario);

        void guardar_a_usuario(InfoSessionVO infoSession, a_usuario loUsuaurio);

        void borrar_a_usuario(InfoSessionVO infoSession, int liCodUsuario);

        ///////////////////////////////////////////////////////////////////////
        ///// DEFINICION DE MENU                                             //
        ///////////////////////////////////////////////////////////////////////

        a_menu traer_a_menu(InfoSessionVO infoSession, a_menu loMenu);

        void modificar_a_menu(InfoSessionVO infoSession, a_menu loMenu);

        void guardar_a_menu(InfoSessionVO infoSession, a_menu loMenu);

        void borrar_a_menu(InfoSessionVO infoSession, int liCodMenu);

        ///////////////////////////////////////////////////////////////////////
        ///// DEFINICION DE  GRUPO mENU                                     ///
        ///////////////////////////////////////////////////////////////////////

        a_grupo_menu traer_a_grupo_menu(InfoSessionVO infoSession, a_grupo_menu loGrupoMenu);

        void modificar_a_grupo_menu(InfoSessionVO infoSession, a_grupo_menu loGrupoMenu);

        void guardar_a_grupo_menu(InfoSessionVO infoSession, a_grupo_menu loGrupoMenu);

        void borrar_a_grupo_menu(InfoSessionVO infoSession, int liGrupoMenu);

        ///////////////////////////////////////////////////////////////////////
        ///// DEFINICION DE  GRUPO USUARIO                                     ///
        ///////////////////////////////////////////////////////////////////////

        a_grupo_usuario traer_a_grupo_usuario(InfoSessionVO infoSession, a_grupo_usuario loGrupousuario);

        void modificar_a_grupo_usuario(InfoSessionVO infoSession, a_grupo_usuario loGrupousuario);

        void guardar_a_grupo_usuario(InfoSessionVO infoSession, a_grupo_usuario loGrupousuario);

        void borrar_a_grupo_usuario(InfoSessionVO infoSession, int liCodigo);

        ///////////////////////////////////////////////////////////////////////
        ///// DEFINICION DE METODOS PARA BLOQUEOS DE REGISTRO               ///
        ///////////////////////////////////////////////////////////////////////

        void guardar_a_bloqueo_registro(InfoSessionVO infoSession, a_bloqueo_registro loBloqueoRegistro);

        void borrar_a_bloqueo_registro(InfoSessionVO infoSession, string lsNombretabla, string lsNombreRegistro);

        void borrar_a_bloqueo_registro_usuario(InfoSessionVO infoSession, string lsUsuario);

        ///////////////////////////////////////////////////////////////////////
        ///// DEFINICION DE FESTIVOS                                        ///
        ///////////////////////////////////////////////////////////////////////

        m_festivo traer_m_festivo(InfoSessionVO infoSession, m_festivo loFestivo);

        m_festivo traer_m_festivo1(InfoSessionVO infoSession, m_festivo loFestivo);

        void modificar_m_festivo(InfoSessionVO infoSession, m_festivo loFestivo);

        void guardar_m_festivo(InfoSessionVO infoSession, m_festivo loFestivo);

        void borrar_m_festivo(InfoSessionVO infoSession, DateTime ldFecha);

        ///////////////////////////////////////////////////////////////////////
        ///// FTP                                                           ///
        ///////////////////////////////////////////////////////////////////////

        bool put_archivo(string lsNombre, string lsUrl, string lsUsuario, string lsClave);

    }
}
