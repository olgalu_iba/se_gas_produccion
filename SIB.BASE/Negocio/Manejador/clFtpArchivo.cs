﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading;
using System.IO;
using System.Web;

namespace SIB.BASE.Negocio.Manejador
{
    class clFtpArchivo
    {
        private string gsNonbre; // Nombre Completa Archivo en el Servidor Origen 
        private string gsUrl; // Url Completa del Archivo en el servidor Destino
        private string gsUser; // Usuario del FTP en el servidor destino
        private string gsClave; // Clave del FTP en el Servidor destino

        public clFtpArchivo()
        {

        }

        public clFtpArchivo(string lsNombre, string lsUrl, string lsUsuario, string lsClave)
        {
            gsNonbre = lsNombre;
            gsUrl = lsUrl;
            gsUser = lsUsuario;
            gsClave = lsClave;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool put_archivo()
        {
            FtpWebRequest ftpRequest;
            FtpWebResponse ftpResponse;

            try
            {
                //Seleccion del Archivo a Cargar
                string lsPat = gsNonbre;
                ftpRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri(gsUrl));
                ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;
                ftpRequest.Proxy = null;
                ftpRequest.UseBinary = true;
                ftpRequest.Credentials = new NetworkCredential(gsUser, gsClave);

                FileInfo loInformacion = new FileInfo(lsPat);
                byte[] fileContents = new byte[loInformacion.Length];

                //will destroy the object immediately after being used
                using (FileStream loStream = loInformacion.OpenRead())
                {
                    loStream.Read(fileContents, 0, Convert.ToInt32(loInformacion.Length));
                }
                using (Stream loEscritor = ftpRequest.GetRequestStream())
                {
                    loEscritor.Write(fileContents, 0, fileContents.Length);
                }
                //Gets the FtpWebResponse of the uploading operation
                ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                //Response.Write(ftpResponse.StatusDescription); //Display response
            }
            catch (WebException webex)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool get_archivo()
        {
            FtpWebRequest ftpRequest;
            FtpWebResponse ftpResponse;
            int contentLen;

            try
            {
                //Seleccion del Archivo a Cargar
                string lsPat = gsNonbre;
                ftpRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri(gsUrl));
                ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                ftpRequest.Proxy = null;
                ftpRequest.UseBinary = true;
                ftpRequest.Credentials = new NetworkCredential(gsUser, gsClave);

                FileInfo loInformacion = new FileInfo(gsUrl);
                byte[] fileContents = new byte[loInformacion.Length];

                //will destroy the object immediately after being used
                //using (FileStream loStream = loInformacion.OpenRead())
                //{
                //    loStream.Read(fileContents, 0, Convert.ToInt32(loInformacion.Length));
                //}
                //using (Stream loEscritor = ftpRequest.GetRequestStream())
                //{
                //    loEscritor.Write(fileContents, 0, fileContents.Length);
                //}

                // Configuro el buffer a 2 KBytes
                int buffLength = 2048;
                byte[] buff = new byte[buffLength];

                using (FileStream fs = new FileStream(lsPat, FileMode.Create,
                                        FileAccess.Write, FileShare.None))
                using (Stream strm = ftpRequest.GetResponse().GetResponseStream())
                {
                    // Leer del buffer 2kb cada vez
                    contentLen = strm.Read(buff, 0, buffLength);

                    // mientras haya datos en el buffer...
                    while (contentLen != 0)
                    {
                        // escribir en el stream del fichero
                        //el contenido del stream de conexión
                        fs.Write(buff, 0, contentLen);
                        contentLen = strm.Read(buff, 0, buffLength);
                    }
                }
                //Gets the FtpWebResponse of the uploading operation
                ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                //Response.Write(ftpResponse.StatusDescription); //Display response
            }
            catch (WebException webex)
            {
                return false;
            }
            return true;
        }


    }
}
