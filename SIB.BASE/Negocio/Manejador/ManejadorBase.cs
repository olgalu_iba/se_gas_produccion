﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.BASE.Negocio.Repositorio;
using PCD_Infraestructura.Transaction;
using System.Diagnostics;

namespace SIB.BASE.Negocio.Manejador
{
    class ManejadorBase : ManejadorSIB
    {

        /////////////////////////////////////////////////////////////
        //METODOS PARA EL MANEJO DE    USUARIO                    ///
        /////////////////////////////////////////////////////////////

        /// <summary>
        /// Nombre: traer_a_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para el proceso de Listar usuario 
        /// /// Recibe como parametro  usuario 
        /// Modificacion:
        /// </summary>
        /// <param name="lousuario"></param>
        /// <returns></returns>
        public a_usuario traer_a_usuario(a_usuario lousuario)
        {
            consultarPermiso("a_usuario", tiposPemisos.SELECT);
            try
            {
                RepositorioBase loRepositorio = (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);
                return loRepositorio.traer_a_usuario(lousuario);
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pueden listar los usuario , consulte sus permisos con el administrador del sistema");
            }
        }

        /// <summary>
        /// Nombre: traer_a_usuario1
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para el proceso de Listar usuario 
        /// /// Recibe como parametro  usuario 
        /// Modificacion:
        /// </summary>
        /// <param name="lousuario"></param>
        /// <returns></returns>
        public a_usuario traer_a_usuario1(a_usuario lousuario)
        {
            try
            {
                RepositorioBase loRepositorio = (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);
                return loRepositorio.traer_a_usuario1(lousuario);
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pueden listar los usuario , consulte sus permisos con el administrador del sistema");
            }
        }

        /// <summary>
        /// Nombre: modificar_a_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para realizar el proceso de modificacion del registro de usuario 
        /// Base de datos.
        /// /// Recibe como parametro la clase de usuario 
        /// Modificacion:
        /// </summary>
        /// <param name="lousuario"></param>
        public void modificar_a_usuario(a_usuario lousuario)
        {
            consultarPermiso("a_usuario", tiposPemisos.UPDATE);
            try
            {
                BeginTransaction();
                RepositorioBase loRepositorio = (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);
                lousuario.fecha_hora_actual = DateTime.Now;
                lousuario.login_usuario = UsuarioAuditoria;
                loRepositorio.modificar_a_usuario(lousuario);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pudo modificar el usuario , descripción del error: " + ex.Message);
            }
        }

        /// <summary>
        /// Nombre: guardar_a_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para guardar la informacion de usuario 
        /// Recibe como parametro el codigo de usuario 
        /// Modificacion:
        /// </summary>
        /// <param name="lousuario"></param>
        public void guardar_a_usuario(a_usuario lousuario)
        {
            consultarPermiso("a_usuario", tiposPemisos.INSERT);
            string mensaje = "";
            //try
            //{
            BeginTransaction();
            try
            {
                RepositorioBase loRepositorio = (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);
                lousuario.fecha_hora_actual = DateTime.Now;
                lousuario.login_usuario = UsuarioAuditoria;
                loRepositorio.guardar_a_usuario(lousuario);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                mensaje = ex.Message;
                //RollbackTransaction();
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pudo Crear el usuario , descripción del error: " + ex.Message);
            }
        }

        /// <summary>
        /// Nombre: borrar_a_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para Borrar el registro de usuario la Base de datos.
        /// Recibe Como parametro una cadena del usuario 
        /// Modificacion:
        /// </summary>
        /// <param name="liIdentificador"></param>
        /// <param name="liDato"></param>

        public void borrar_a_usuario(int liCodUsuario)
        {
            consultarPermiso("a_usuario", tiposPemisos.DELETE);
            try
            {
                BeginTransaction();
                a_usuario obj = new a_usuario();
                obj.codigo_usuario = liCodUsuario;

                RepositorioBase loRepositorio =
                     (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);

                loRepositorio.borrar_a_usuario(obj);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pudo Eliminar el  usuario , descripción del error: " + ex.Message);
            }

        }



        /////////////////////////////////////////////////////////////
        //METODOS PARA EL MANEJO DE    MENU                       ///
        /////////////////////////////////////////////////////////////

        /// <summary>
        /// Nombre: traer_a_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para el proceso de Listar menu 
        /// /// Recibe como parametro menu
        /// Modificacion:
        /// </summary>
        /// <param name="lomenu"></param>
        /// <returns></returns>
        public a_menu traer_a_menu(a_menu lomenu)
        {
            consultarPermiso("a_menu", tiposPemisos.SELECT);
            try
            {
                RepositorioBase loRepositorio = (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);
                return loRepositorio.traer_a_menu(lomenu);
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pueden listar los menu , consulte sus permisos con el administrador del sistema");
            }
        }

        /// <summary>
        /// Nombre: modificar_a_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para realizar el proceso de modificacion del registro de menu
        /// Base de datos.
        /// /// Recibe como parametro la clase de menu 
        /// Modificacion:
        /// </summary>
        /// <param name="lomenu"></param>
        public void modificar_a_menu(a_menu lomenu)
        {
            consultarPermiso("a_menu", tiposPemisos.UPDATE);
            try
            {
                BeginTransaction();
                RepositorioBase loRepositorio = (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);
                lomenu.fecha_hora_actual = DateTime.Now;
                lomenu.login_usuario = UsuarioAuditoria;
                loRepositorio.modificar_a_menu(lomenu);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pudo modificar el menu , descripción del error: " + ex.Message);
            }
        }

        /// <summary>
        /// Nombre: guardar_a_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para guardar la informacion de menu
        /// Recibe como parametro el codigo de menu 
        /// Modificacion:
        /// </summary>
        /// <param name="lomenu"></param>
        public void guardar_a_menu(a_menu lomenu)
        {
            consultarPermiso("a_menu", tiposPemisos.INSERT);
            string mensaje = "";
            //try
            //{
            BeginTransaction();
            try
            {
                RepositorioBase loRepositorio = (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);
                lomenu.fecha_hora_actual = DateTime.Now;
                lomenu.login_usuario = UsuarioAuditoria;
                loRepositorio.guardar_a_menu(lomenu);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                mensaje = ex.Message;
                //RollbackTransaction();
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pudo Crear el menu , descripción del error: " + ex.Message);
            }
        }

        /// <summary>
        /// Nombre: borrar_a_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para Borrar el registro de usuario la Base de datos.
        /// Recibe Como parametro una cadena del menu 
        /// Modificacion:
        /// </summary>
        /// <param name="liCodMenu"></param>
        public void borrar_a_menu(int liCodMenu)
        {
            consultarPermiso("a_menu", tiposPemisos.DELETE);
            try
            {
                BeginTransaction();
                a_menu obj = new a_menu();
                obj.codigo_menu = liCodMenu;

                RepositorioBase loRepositorio =
                     (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);

                loRepositorio.borrar_a_menu(obj);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pudo Eliminar el  menu , descripción del error: " + ex.Message);
            }

        }



        /////////////////////////////////////////////////////////////
        //METODOS PARA EL MANEJO DE  GRUPO MENU                   ///
        /////////////////////////////////////////////////////////////

        /// <summary>
        /// Nombre: traer_a_grupo_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para el proceso de Listar GrupoMenu
        /// /// Recibe como parametro  GrupoMenu 
        /// Modificacion:
        /// </summary>
        /// <param name="loGrupoMenu"></param>
        /// <returns></returns>
        public a_grupo_menu traer_a_grupo_menu(a_grupo_menu loGrupoMenu)
        {
            consultarPermiso("a_grupo_menu", tiposPemisos.SELECT);
            try
            {
                RepositorioBase loRepositorio = (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);
                return loRepositorio.traer_a_grupo_menu(loGrupoMenu);
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pueden listar los Grupo de Menus , consulte sus permisos con el administrador del sistema");
            }
        }

        /// <summary>
        /// Nombre: modificar_a_grupo_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para realizar el proceso de modificacion del registro de GrupoMenu 
        /// Base de datos.
        /// /// Recibe como parametro la clase de GrupoMenu 
        /// Modificacion:
        /// </summary>
        /// <param name="loGrupoMenu"></param>
        public void modificar_a_grupo_menu(a_grupo_menu loGrupoMenu)
        {
            consultarPermiso("a_grupo_menu", tiposPemisos.UPDATE);
            try
            {
                BeginTransaction();
                RepositorioBase loRepositorio = (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);
                loGrupoMenu.fecha_hora_actual = DateTime.Now;
                loGrupoMenu.login_usuario = UsuarioAuditoria;
                loRepositorio.modificar_a_grupo_menu(loGrupoMenu);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pudo modificar los Grupos de Menus, descripción del error: " + ex.Message);
            }
        }

        /// <summary>
        /// Nombre: guardar_a_grupo_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para guardar la informacion de GrupoMenu 
        /// Recibe como parametro el codigo de GrupoMenu 
        /// Modificacion:
        /// </summary>
        /// <param name="loGrupoMenu"></param>
        public void guardar_a_grupo_menu(a_grupo_menu loGrupoMenu)
        {
            //consultarPermiso("a_grupo_menu", tiposPemisos.INSERT);
            string mensaje = "";
            //try
            //{
            BeginTransaction();
            try
            {
                RepositorioBase loRepositorio = (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);
                loGrupoMenu.fecha_hora_actual = DateTime.Now;
                loGrupoMenu.login_usuario = UsuarioAuditoria;
                loRepositorio.guardar_a_grupo_menu(loGrupoMenu);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                mensaje = ex.Message;
                //RollbackTransaction();
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pudo Crear el Grupo Menu, descripción del error: " + ex.Message);
            }
        }

        /// <summary>
        /// Nombre: borrar_a_grupo_menu
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para Borrar el registro de GrupoMenu la Base de datos.
        /// Recibe Como parametro una cadena del GrupoMenu 
        /// Modificacion:
        /// </summary>
        /// <param name="liGrupoMenu"></param>

        public void borrar_a_grupo_menu(int liGrupoMenu)
        {
            //consultarPermiso("a_grupo_menu", tiposPemisos.DELETE);
            try
            {
                BeginTransaction();
                a_grupo_menu obj = new a_grupo_menu();
                obj.codigo_grupo_menu = liGrupoMenu;

                RepositorioBase loRepositorio =
                     (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);

                loRepositorio.borrar_a_grupo_menu(obj);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pudo Eliminar el Grupo Menu, descripción del error: " + ex.Message);
            }

        }


        /////////////////////////////////////////////////////////////
        //METODOS PARA EL MANEJO DE  GRUPO USUARIO                  ///
        /////////////////////////////////////////////////////////////

        /// <summary>
        /// Nombre: traer_a_grupo_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para el proceso de Listar los Grupos de Usuario
        /// /// Recibe como parametro  Subyacente 
        /// Modificacion:
        /// </summary>
        /// <param name="loSubyacente"></param>
        /// <returns></returns>
        public a_grupo_usuario traer_a_grupo_usuario(a_grupo_usuario loGrupoUsuario)
        {
            consultarPermiso("a_grupo_usuario", tiposPemisos.SELECT);
            try
            {
                RepositorioBase loRepositorio = (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);
                return loRepositorio.traer_a_grupo_usuario(loGrupoUsuario);
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pueden listar los subyacentes, consulte sus permisos con el administrador del sistema");
            }
        }

        /// <summary>
        /// Nombre: modificar_m_grupo_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para realizar el proceso de modificacion del registro de grupos de usuario en la 
        /// Base de datos.
        /// /// Recibe como parametro el codigo de empaque.
        /// Modificacion:
        /// </summary>
        /// <param name="loEmpaque"></param>
        public void modificar_a_grupo_usuario(a_grupo_usuario loGrupoUsuario)
        {
            consultarPermiso("a_grupo_usuario", tiposPemisos.UPDATE);
            try
            {
                BeginTransaction();
                RepositorioBase loRepositorio = (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);
                loGrupoUsuario.fecha_hora_actual = DateTime.Now;
                loGrupoUsuario.login_usuario = UsuarioAuditoria;
                loRepositorio.modificar_a_grupo_usuario(loGrupoUsuario);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pudo modificar el Grupo de Usuario, descripción del error: " + ex.Message);
            }
        }

        /// <summary>
        /// Nombre: guardar_m_empaque
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para guardar la informacion de los empaques
        /// Recibe como parametro el codigo de empaque
        /// Modificacion:
        /// </summary>
        /// <param name="loEmpaque"></param>
        public void guardar_a_grupo_usuario(a_grupo_usuario loGrupoUsuario)
        {
            consultarPermiso("a_grupo_usuario", tiposPemisos.INSERT);
            string mensaje = "";
            //try
            //{
            BeginTransaction();
            try
            {
                RepositorioBase loRepositorio = (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);
                loGrupoUsuario.fecha_hora_actual = DateTime.Now;
                loGrupoUsuario.login_usuario = UsuarioAuditoria;
                loRepositorio.guardar_a_grupo_usuario(loGrupoUsuario);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                mensaje = ex.Message;
                //RollbackTransaction();
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pudo Crear el Grupo Usuario, descripción del error: " + ex.Message);
            }
        }

        /// <summary>
        /// Nombre: borrar_a_grupo_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para Borrar el registro de Grupo Usuario de la Base de datos.
        /// Recibe Como parametro una cadena con el Codigo Grupo Usuario.
        /// Modificacion:
        /// </summary>
        /// <param name="lsCodigoEmpaque"></param>
        public void borrar_a_grupo_usuario(int lsCodigoGrupoUsuario)
        {
            consultarPermiso("a_grupo_usuario", tiposPemisos.DELETE);
            try
            {
                BeginTransaction();
                a_grupo_usuario obj = new a_grupo_usuario();
                obj.codigo_grupo_usuario = lsCodigoGrupoUsuario;

                RepositorioBase loRepositorio =
                     (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);

                loRepositorio.borrar_a_grupo_usuario(obj);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pudo Eliminar el Grupo Usuario, descripción del error: " + ex.Message);
            }

        }


        //////////////////////////////////////////////////////
        //METODOS PARA EL MANEJO DE BLOQUEOS DE REGISTRO///
        //////////////////////////////////////////////////////
        /// <summary>
        /// Nombre: guardar_a_bloqueo_registro
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para guardar la informacion de los bloqueos de registro
        /// Modificacion:
        /// </summary>
        /// <param name="loBloqueoRegistro"></param>
        public void guardar_a_bloqueo_registro(a_bloqueo_registro loBloqueoRegistro)
        {
            string mensaje = "";
            //try
            //{
            BeginTransaction();
            try
            {
                RepositorioBase loRepositorio = (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);
                loBloqueoRegistro.fecha_hora_bloqueo = DateTime.Now;
                loBloqueoRegistro.usuario_bloqueo = UsuarioAuditoria;
                loRepositorio.guardar_a_bloqueo_registro(loBloqueoRegistro);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                mensaje = ex.Message;
                //RollbackTransaction();
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pudo Crear el bloqueo, descripción del error: " + ex.Message);
            }
        }

        /// <summary>
        /// Nombre: borrar_a_bloqueo_registro
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para Borrar el registro de bloqueos de rgistro en la base de datos
        /// Modificacion:
        /// </summary>
        /// <param name="lsNombreTabla"></param>
        /// <param name="lsLlaveRegistro"></param>
        /// 

        public void borrar_a_bloqueo_registro(string lsNombreTabla, string lsLlaveRegistro)
        {
            try
            {
                BeginTransaction();
                a_bloqueo_registro obj = new a_bloqueo_registro();
                obj.nombre_tabla = lsNombreTabla;
                obj.llave_registro = lsLlaveRegistro;

                RepositorioBase loRepositorio =
                     (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);

                loRepositorio.borrar_a_bloqueo_registro(obj);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pudo Bloquear el registro, descripción del error: " + ex.Message);
            }

        }

        /// <summary>
        /// Nombre: borrar_a_bloqueo_registro_usuario
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para Borrar el registro de bloqueos de rgistro en la base de datos
        /// Modificacion:
        /// </summary>
        /// <param name="lsNombreTabla"></param>
        /// <param name="lsLlaveRegistro"></param>
        /// 

        public void borrar_a_bloqueo_registro_usuario(string lsUsuario)
        {
            try
            {
                BeginTransaction();
                a_bloqueo_registro obj = new a_bloqueo_registro();
                obj.usuario_bloqueo = lsUsuario;

                RepositorioBase loRepositorio =
                     (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);

                loRepositorio.borrar_a_bloqueo_registro_usuario(obj);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pudo Bloquear el registro, descripción del error: " + ex.Message);
            }

        }


        /////////////////////////////////////////////////////////////
        //METODOS PARA EL MANEJO DE FESTIVOS                      ///
        /////////////////////////////////////////////////////////////

        /// <summary>
        /// Nombre: traer_m_festivo1
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para el proceso de Listar Festivo
        /// /// Recibe como parametro  Festivo
        /// Modificacion:
        /// </summary>
        /// <param name="loFestivo"></param>
        /// <returns></returns>
        public m_festivo traer_m_festivo1(m_festivo loFestivo)
        {
            //consultarPermiso("m_festivo", tiposPemisos.SELECT);
            try
            {
                RepositorioBase loRepositorio = (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);
                return loRepositorio.traer_m_festivo(loFestivo);
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pueden listar los Festivo, consulte sus permisos con el administrador del sistema");
            }
        }


        /// <summary>
        /// Nombre: traer_m_festivo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para el proceso de Listar Festivo
        /// /// Recibe como parametro  Festivo
        /// Modificacion:
        /// </summary>
        /// <param name="loFestivo"></param>
        /// <returns></returns>
        public m_festivo traer_m_festivo(m_festivo loFestivo)
        {
            consultarPermiso("m_festivo", tiposPemisos.SELECT);
            try
            {
                RepositorioBase loRepositorio = (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);
                return loRepositorio.traer_m_festivo(loFestivo);
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pueden listar los Festivo, consulte sus permisos con el administrador del sistema");
            }
        }

        /// <summary>
        /// Nombre: modificar_m_festivo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para realizar el proceso de modificacion del registro de Festivo
        /// Base de datos.
        /// /// Recibe como parametro la clase de Festivo
        /// Modificacion:
        /// </summary>
        /// <param name="loFestivo"></param>
        public void modificar_m_festivo(m_festivo loFestivo)
        {
            consultarPermiso("m_festivo", tiposPemisos.UPDATE);
            try
            {
                BeginTransaction();
                RepositorioBase loRepositorio = (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);
                loFestivo.fecha_hora_actual = DateTime.Now;
                loFestivo.login_usuario = UsuarioAuditoria;
                loRepositorio.modificar_m_festivo(loFestivo);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pudo modificar el Festivo, descripción del error: " + ex.Message);
            }
        }

        /// <summary>
        /// Nombre: guardar_m_festivo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para guardar la informacion de Festivo
        /// Recibe como parametro el codigo de Festivo
        /// Modificacion:
        /// </summary>
        /// <param name="loFestivo"></param>
        public void guardar_m_festivo(m_festivo loFestivo)
        {
            consultarPermiso("m_festivo", tiposPemisos.INSERT);
            string mensaje = "";
            //try
            //{
            BeginTransaction();
            try
            {
                RepositorioBase loRepositorio = (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);
                loFestivo.fecha_hora_actual = DateTime.Now;
                loFestivo.login_usuario = UsuarioAuditoria;
                loRepositorio.guardar_m_festivo(loFestivo);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                mensaje = ex.Message;
                //RollbackTransaction();
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pudo Crear el Festivo, descripción del error: " + ex.Message);
            }
        }

        /// <summary>
        /// Nombre: borrar_m_festivo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Metodo para Borrar el registro de Festivo la Base de datos.
        /// Recibe Como parametro una cadena con  la llave del festivo
        /// Modificacion:
        /// </summary>
        /// <param name="liAno"></param>
        /// <param name="liDia"></param>
        /// <param name="liMes"></param>

        public void borrar_m_festivo(DateTime ldFecha)
        {
            consultarPermiso("m_festivo", tiposPemisos.DELETE);
            try
            {
                BeginTransaction();
                m_festivo obj = new m_festivo();
                obj.fecha = ldFecha;

                RepositorioBase loRepositorio =
                     (RepositorioBase)RepositoryFactory.CreateRepository(typeof(RepositorioBase), this);

                loRepositorio.borrar_m_festivo(obj);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name, ex);
                throw new Exception("No se pudo Eliminar el  Festivo, descripción del error: " + ex.Message);
            }

        }



    }

}
