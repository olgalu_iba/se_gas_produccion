using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Linq;
using System.IO;

namespace PCD_Infraestructura.Transaction
{
    public class TransactionFactory
    {
        public static DataContext GetTransaction(string ConnectionString)
        {
            return new DataContext(ConnectionString);
        }
    }
}
