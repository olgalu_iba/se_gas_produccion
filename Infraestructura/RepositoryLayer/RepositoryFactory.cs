using System;
using System.Collections.Generic;
using System.Text;
using PCD_Infraestructura.Business;

namespace PCD_Infraestructura.Transaction
{
    public class RepositoryFactory
    {
        public static AbstractRepository CreateRepository(Type TheType, AbstractHandler handler)
        {
            AbstractRepository repository = (AbstractRepository)Activator.CreateInstance(TheType);
            repository.Transaction = handler.Transaction;

            return repository;
        }
    }
}
