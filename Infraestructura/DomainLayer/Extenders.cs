﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Reflection;

namespace PCD_Infraestructura.DomainLayer
{
    /// <summary>
    /// Clase para extender la funcionalidad de Colecciones
    /// Programador: Javier Hernández Fecha: 08-Abr-2008
    /// </summary>
    public static class Extenders
    {
        /// <summary>
        /// Método que Convierte a DataTable independientemente del tipo de Dato
        /// </summary>
        /// <typeparam name="T">Tipo de Dato</typeparam>
        /// <param name="collection">Una Coleccion cualquiera</param>
        /// <returns>Un DataTable</returns>

        public static DataTable ToDataTable<T>(this IEnumerable<T> collection)
        {
            DataTable dt = new DataTable();
            Type t = typeof(T);
            PropertyInfo[] pia = t.GetProperties();
            //Crear Columnas en el DataTable
            foreach (PropertyInfo pi in pia)
            {
                if (!pi.PropertyType.Name.Equals("EntitySet`1") && !pi.PropertyType.Name.Substring(0, 2).Equals("m_"))
                {
                    if (!(pi.PropertyType.FullName.Contains(typeof(System.Nullable).FullName)))
                    {
                        dt.Columns.Add(pi.Name, pi.PropertyType);
                    }
                    else
                    {
                        dt.Columns.Add(pi.Name, (Nullable.GetUnderlyingType(pi.PropertyType)));
                    }
                }
            }
            //Llenar la tabla
            foreach (T item in collection)
            {
                DataRow dr = dt.NewRow();
                dr.BeginEdit();
                foreach (PropertyInfo pi in pia)
                {
                    if (!pi.PropertyType.Name.Equals("EntitySet`1") && !pi.PropertyType.Name.Substring(0, 2).Equals("m_"))
                    {
                        //if (!(pi.PropertyType.FullName.Contains(typeof(System.Nullable).FullName)))
                        //{
                        //    dr[pi.Name] = pi.GetValue(item, null);
                        //}
                        if (pi.GetValue(item, null) != "")
                            dr[pi.Name] = pi.GetValue(item, null);
                    }
                }
                dr.EndEdit();
                dt.Rows.Add(dr);
            }
            return dt;
        }


    }


}

