﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PCD_Infraestructura.Business
{
    [Serializable()]
    public class AbstractInfoSession
    {
        public string Servidor;
        public string BaseDatos;
        public string ServidorPdf;
        public string BaseDatosPdf;
        public string ServidorInf;
        public string BaseDatosInf;
        public string Usuario;
        public string Password;
        public string Programa;
        public int codigo_grupo_usuario;
        public string nombre;
        public string cod_comisionista;
        public char comisionista_principal;
        public int codigo_regional;
        public string cod_comisionistaC;
        public string cod_comisionistaV;
        public int codigo_usuario;
        public string mensaje_error;

        public AbstractInfoSession(string serv, string bd, string usu, string cla, string servPdf, string bdPdf, string servInf, string bdInf)
        {
            this.Servidor = serv;
            this.BaseDatos = bd;
            this.Usuario = usu;
            this.Password = cla;
            this.ServidorPdf = servPdf;
            this.BaseDatosPdf = bdPdf;
            this.ServidorInf = servInf;
            this.BaseDatosInf = bdInf;
        }

    }
}
