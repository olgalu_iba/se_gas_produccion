using System;
using System.Collections.Generic;
using System.Text;
using PCD_Infraestructura.Transaction;

namespace PCD_Infraestructura.Business
{
    public class BusinessHandlerFactory
    {
        public static AbstractHandler CreateBusinessHandler(Type TheType, AbstractInfoSession infoSession)
        {
            AbstractHandler handler = (AbstractHandler) Activator.CreateInstance(TheType);

            handler.CreateConnectionString(infoSession.Servidor, infoSession.BaseDatos, infoSession.Usuario, infoSession.Password);

            handler.ProgramaAuditoria = infoSession.Programa;

            handler.SetTransaction(TransactionFactory.GetTransaction(handler.ConnectionString));

            return handler;
        }
    }
}
