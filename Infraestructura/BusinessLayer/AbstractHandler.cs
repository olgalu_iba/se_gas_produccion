using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Linq;

namespace PCD_Infraestructura.Business
{
    public class AbstractHandler
    {
        public string ProgramaAuditoria;
        public string UsuarioAuditoria;

        private DataContext trx;
        public string ConnectionString { get; set; }

        public string CreateConnectionString(String Servidor, String BaseDatos, String Usuario, String Clave)
        {
            UsuarioAuditoria = Usuario;
            ConnectionString = @"Persist Security Info=False;User id=" + Usuario + ";password=" + Clave + ";Initial Catalog=" + BaseDatos + ";Data Source=" + Servidor;
            return ConnectionString;
        }

        public void SetTransaction(DataContext PTransaction)
        {
            this.trx = PTransaction;
        }

        public DataContext Transaction
        {
            get { return trx;  }
        }

        protected void BeginTransaction()
        {
            trx.Connection.Open();
            //�?
        }

        protected void CommitTransaction()
        {
            trx.SubmitChanges();           
        }

        protected void RollbackTransaction()
        {
            trx.Transaction.Rollback();
        }

        protected void EndTransaction()
        {
            //�?
        }
    }
}
