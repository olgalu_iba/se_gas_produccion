﻿/*
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 * Copyright (C) 2019 Bolsa Mercantil de Colombia, Todos los Derechos Reservados
 * Archivo:               PrecioConsumidorVO.cs
 * Tipo:                  Clase principal
 * Autor:                 Oscar Piñeros
 * Fecha creación:        2019 Noviembre 07
 * Fecha modificación:    
 * Propósito:             Vo para mostrar la información de la contratación propia de mercado secundario  
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 */

using System;

namespace SIB.Global.Presentacion
{
    [Serializable]
    // ReSharper disable once InconsistentNaming
    public class PrecioConsumidorVO
    {
        /// <summary>
        /// Código del registro 
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Nombre del punto de entrega
        /// </summary>
        public string Punto { get; set; }

        /// <summary>
        /// Fecha de contratación
        /// </summary>
        public string Fecha { get; set; }

        /// <summary>
        /// Cantidad en MBTUD
        /// </summary>
        public double Cantidad { get; set; }

    }
}
