﻿/*
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 * Copyright (C) 2019 Bolsa Mercantil de Colombia, Todos los Derechos Reservados
 * Archivo:               PrecioConsumidorVO.cs
 * Tipo:                  Clase principal
 * Autor:                 Oscar Piñeros
 * Fecha creación:        2019 Noviembre 07
 * Fecha modificación:    
 * Propósito:             Vo para la selección de botones en el formulario de gestión de contratos  
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 */

using System;

namespace SIB.Global.Presentacion
{
    [Serializable]
    // ReSharper disable once InconsistentNaming
    public class ContractButtonVO
    {
        /// <summary>
        /// Código de la funcionalidad seleccionada
        /// </summary>
        public int Codigo { get; set; }

        /// <summary>
        /// Botones de la funcionalidad   
        /// </summary>
        public EnumBotones[] Botones { get; set; }
    }
}
