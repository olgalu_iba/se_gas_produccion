﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PCD_Infraestructura.Business;

namespace SIB.Global.Presentacion
{
    [Serializable] //20210318 ha
    public class InfoSessionVO : AbstractInfoSession
    {
        public InfoSessionVO(string serv, string bd, string usu, string cla, string servPdf, string bdPdf, string servInf, string bdInf)
            : base(serv, bd, usu, cla, servPdf, bdPdf, servInf, bdInf)
        {

        }
    }
}
