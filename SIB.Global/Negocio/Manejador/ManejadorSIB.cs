﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data;
using System.Reflection;
using SIB.Global.Negocio.Repositorio;
using SIB.Global.Presentacion;
using SIB.Global.Dominio;
using PCD_Infraestructura.Business;
using PCD_Infraestructura.Transaction;
using PCD_Infraestructura.DomainLayer;
using System.Diagnostics;

namespace SIB.Global.Negocio.Manejador
{
    public class ManejadorSIB : AbstractHandler
    {
        public string LogErrores;
        public string LogEventos;
        public enum tiposPemisos
        {
            SELECT = 1,
            UPDATE = 2,
            INSERT = 8,
            DELETE = 10,
            EXECUTE = 20
        }

        public enum descripcionTiposPemisos
        {
            Consulta = 1,
            Actualización = 2,
            Inserción = 8,
            Borrado = 10,
            Ejecución = 20,
        }

        public ManejadorSIB()
        {
            LogErrores = "SIBEventos";
            LogEventos = "SIBEventos";
            //ConnectionString = @"Persist Security Info=False;User id=" + Usuario + ";password=" + Clave + ";Initial Catalog=" + BaseDatos + ";Data Source=" + Servidor;
        }

        public Boolean validarConexion()
        {
            Boolean dtt = true;
            try
            {
                RepositorioSIB repositorio = (RepositorioSIB)RepositoryFactory.CreateRepository(typeof(RepositorioSIB), this);
                repositorio.validarConexion();
            }
            catch
            {
                dtt = false;
            }
            return dtt;
        }

        public Boolean actualizarPassword(string usuario, string nuevoPassword, string antiguoPassword)
        {
            Boolean dtt = true;
            try
            {
                RepositorioSIB repositorio = (RepositorioSIB)RepositoryFactory.CreateRepository(typeof(RepositorioSIB), this);
                repositorio.actualizarPassword(usuario, nuevoPassword, antiguoPassword);
            }
            catch
            {
                dtt = false;
            }
            return dtt;
        }

        public void accesoSistema(string lsUsuario)
        {
            registrarEvento("Acceso al sistema", lsUsuario);
        }

        public void registrarProceso(string lsUsuario, string lsProceso, string lsRegistro)
        {
            registrarEvento(lsProceso, lsRegistro);
        }

        public void registrarError(string programa, string clase, Exception ex)
        {
            EventLog Log = new EventLog();
            Log.Source = this.LogErrores;
            string error = ex.Message + "::Traza - " + ex.StackTrace;
            Log.WriteEntry("Programa - " + programa + "::Clase - " + clase + "::Usuario - " + UsuarioAuditoria + "::Mensaje - " + error, EventLogEntryType.Error);
        }

        public void registrarEvento(string mensaje, string registro)
        {
            try
            {
                //EventLog Log = new EventLog();
                //Log.Source = this.LogEventos;
                //Log.WriteEntry("Usuario - " + UsuarioAuditoria + "::Mensaje - " + mensaje + "::Registro - " + registro, EventLogEntryType.Information);
                BeginTransaction();
                RepositorioSIB repositorio =
                     (RepositorioSIB)RepositoryFactory.CreateRepository(typeof(RepositorioSIB), this);
                a_auditoria_proceso loAuditoriaProceso = new a_auditoria_proceso();
                loAuditoriaProceso.usuario = UsuarioAuditoria;
                loAuditoriaProceso.proceso = mensaje;
                loAuditoriaProceso.registro = registro;
                loAuditoriaProceso.fecha_hora = DateTime.Now;
                repositorio.registrarEvento(loAuditoriaProceso);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name + " -> registrarEvento", ex);
                throw new Exception("No se puede registrar la información de auditoria de procesos, consulte al administrador del sistema");
            }
        }

        public void consultarPermiso(String lsTabla, tiposPemisos leTipo)
        {
            descripcionTiposPemisos lsTipoPermiso = (descripcionTiposPemisos)leTipo;
            RepositorioSIB repositorio =
                (RepositorioSIB)RepositoryFactory.CreateRepository(typeof(RepositorioSIB), this);
            if (!repositorio.consultarPermisoBD(lsTabla, ((int)leTipo).ToString()))
            {
                throw new Exception("El usuario " + UsuarioAuditoria + " no tiene permisos para la tabla " + lsTabla + " de " + lsTipoPermiso.ToString() + " en la base de datos, consulte el administrador del sistema");
            }
            if (!repositorio.consultarPermisoSistema(lsTabla, leTipo.ToString(), UsuarioAuditoria))
            {
                throw new Exception("El usuario " + UsuarioAuditoria + " no tiene permisos para la tabla " + lsTabla + " de " + lsTipoPermiso.ToString() + " en el sistema, consulte el administrador del sistema");
            }
        }

        public List<Hashtable> consultarMenu(int liPadre)
        {
            RepositorioSIB repositorio =
                (RepositorioSIB)RepositoryFactory.CreateRepository(typeof(RepositorioSIB), this);
            return repositorio.consultarMenu(UsuarioAuditoria, liPadre);
        }

        public List<Hashtable> consultarMenuGrupo(int liPadre, int liGrupoUsuario)
        {
            RepositorioSIB repositorio =
                (RepositorioSIB)RepositoryFactory.CreateRepository(typeof(RepositorioSIB), this);
            return repositorio.consultarMenuGrupo(liPadre, liGrupoUsuario);
        }
        public Hashtable consultarPermisoMenu(String lsTabla)
        {
            RepositorioSIB repositorio =
                (RepositorioSIB)RepositoryFactory.CreateRepository(typeof(RepositorioSIB), this);
            var permisosBD = repositorio.consultarPermisoMenu(lsTabla, UsuarioAuditoria);
            Hashtable permisos = new Hashtable();
            permisos.Add("SELECT", (permisosBD.consultar == 1));
            permisos.Add("UPDATE", (permisosBD.modificar == 1));
            permisos.Add("INSERT", (permisosBD.crear == 1));
            permisos.Add("DELETE", (permisosBD.eliminar == 1));
            return permisos;
        }

        public DataTable consultarTabla<T>(IEnumerable<T> collection, List<String> llFiltros, string lsCondicion)
        {
            //consultarPermiso(typeof(T).Name, tiposPemisos.SELECT);
            try
            {
                RepositorioSIB repositorio =
                     (RepositorioSIB)RepositoryFactory.CreateRepository(typeof(RepositorioSIB), this);
                //return repositorio.consultarTabla<T>(collection, llFiltros);
                return repositorio.consultarTabla<T>(collection, llFiltros, lsCondicion).ToDataTable<T>();
            }
            catch (Exception ex)
            {
                var t = typeof(T);
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name + " -> consultarTabla: " + t.Name, ex);
                throw new Exception("No se pudo consultar la tabla " + t.Name + ", consulte el administrador del sistema");
            }
        }

        public String DefinirFiltros<T>(IEnumerable<T> collection, List<String> llFiltros, string lsCondicion)
        {
            RepositorioSIB repositorio =
                 (RepositorioSIB)RepositoryFactory.CreateRepository(typeof(RepositorioSIB), this);
            return repositorio.DefinirFiltros<T>(collection, llFiltros, lsCondicion).ToString();
        }

        public IQueryable<String> ayudaMaestro(string lsCadenaBusqueda, string lsTabla, string lsCodigo, string lsDescripcion, string lsTipo)
        {
            try
            {
                RepositorioSIB repositorio = (RepositorioSIB)RepositoryFactory.CreateRepository(typeof(RepositorioSIB), this);
                return repositorio.ayudaMaestro(lsCadenaBusqueda, lsTabla, lsCodigo, lsDescripcion, lsTipo);
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name + " -> listadoAyuda: " + lsTabla, ex);
                throw new Exception("No se pudo consultar la tabla " + lsTabla + ", consulte el administrador del sistema");
            }
        }

        public IQueryable<String> ayudaMaestroSib(string lsCadenaBusqueda, string lsTabla, string lsCodigo, string lsDescripcion, string lsTipo)
        {
            try
            {
                RepositorioSIB repositorio = (RepositorioSIB)RepositoryFactory.CreateRepository(typeof(RepositorioSIB), this);
                return repositorio.ayudaMaestroSib(lsCadenaBusqueda, lsTabla, lsCodigo, lsDescripcion, lsTipo);
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name + " -> listadoAyuda: " + lsTabla, ex);
                throw new Exception("No se pudo consultar la tabla " + lsTabla + ", consulte el administrador del sistema");
            }
        }


        public IQueryable<String> ayudaMaestroCondicion(string lsCadenaBusqueda, string lsTabla, string lsCodigo, string lsDescripcion, string lsTipo, string lsCondicion)
        {
            try
            {
                RepositorioSIB repositorio = (RepositorioSIB)RepositoryFactory.CreateRepository(typeof(RepositorioSIB), this);
                return repositorio.ayudaMaestroCondicion(lsCadenaBusqueda, lsTabla, lsCodigo, lsDescripcion, lsTipo, lsCondicion);
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name + " -> listadoAyuda: " + lsTabla, ex);
                throw new Exception("No se pudo consultar la tabla " + lsTabla + ", consulte el administrador del sistema");
            }
        }

        public IQueryable<String> ayudaProcedimiento(string lsProcedimiento, string lsCadenaBusqueda)
        {
            try
            {
                RepositorioSIB repositorio = (RepositorioSIB)RepositoryFactory.CreateRepository(typeof(RepositorioSIB), this);
                return repositorio.ayudaProcedimiento(lsProcedimiento, lsCadenaBusqueda);
            }
            catch (Exception ex)
            {
                registrarError(ProgramaAuditoria, ((Object)this).GetType().Name + " -> listadoAyuda: " + lsProcedimiento, ex);
                throw new Exception("No se pudo Realizar la Consulta a la Base de Datos, consulte el administrador del sistema");
            }
        }
    }
}
