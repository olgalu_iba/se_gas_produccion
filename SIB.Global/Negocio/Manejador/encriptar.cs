﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace SIB.Global.Negocio.Manejador
{
    public class encriptar
    {

        ///El contructor por defecto, tomará como parámetro el tipo de algoritmo deseado para realizar el cifrado.
        /// <summary>
        /// Constructor por defecto.
        /// </summary>
        /// <param name="alg">Establece el algoritmo de Encripción a utilizar.</param>
        public encriptar()
        {
        }

        public String CifrarCadena(string lsCadena)
        {
            TripleDESCryptoServiceProvider t = new TripleDESCryptoServiceProvider();
            t.Key = Encoding.UTF8.GetBytes("24351876password");
            t.IV = Encoding.UTF8.GetBytes("24681357");
            //t.GenerateKey();
            String sk = Convert.ToBase64String(t.Key);
            //t.GenerateIV();
            String sIV = Convert.ToBase64String(t.IV);
            ICryptoTransform ct;
            MemoryStream m;
            CryptoStream c;
            byte[] b;
            ct = t.CreateEncryptor(t.Key, t.IV);
            b = Encoding.UTF8.GetBytes(lsCadena);
            m = new MemoryStream();
            c = new CryptoStream(m, ct, CryptoStreamMode.Write);
            c.Write(b, 0, b.Length);
            c.FlushFinalBlock();
            c.Close();
            return Convert.ToBase64String(m.ToArray());
        }
        /// <summary>
        /// Nombre: RandomString
        /// Fecha:  Enero 18 de 2016
        /// Descripcion: Genera cadena con caracteres aleatorios segun cantidad ingresada
        /// Modificacion:
        /// </summary>
        /// <param name="liCodigoRuta"></param>
        /// <returns></returns>
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }


    }
}
