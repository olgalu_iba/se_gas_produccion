﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIB.Global.Presentacion;
using System.Net.Mail;
using SIB.Global.Negocio.Repositorio;
using SIB.Global.Dominio;
using PCD_Infraestructura.Business;
using PCD_Infraestructura.Transaction;
using PCD_Infraestructura.DomainLayer;
using System.Diagnostics;
using System.Configuration; //20190321 ajsute correo   

namespace SIB.Global.Negocio.Manejador
{
    /// <summary>
    /// Nombre: clEmail
    /// Fecha: Enero 12 de 2009
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Clase que contiene los metodos para el envio de mail
    /// Modificacion:
    /// </summary>

    public class clEmail
    {
        public string gsDestinatario;
        public string gsAsunto;
        public string gsContenido;
        public string gsAdjunto;
        public string LogErrores;
        // 20190321 ajsute correo
        public string gFrom;
        public string gPuerto;
        public string gTsl;
        // 20190321 fin ajsute correo

        public clEmail(string lsDestinatario, string lsAsunto, string lsContenido, string lsAdjunto)
        {
            gsDestinatario = lsDestinatario;
            gsAsunto = lsAsunto;
            gsContenido = lsContenido;
            gsAdjunto = lsAdjunto;
            LogErrores = "SIBEventos";
            // 20190321 ajsute correo
            gFrom = ConfigurationSettings.AppSettings["FromSmtp"].ToString();
            gPuerto = ConfigurationSettings.AppSettings["PuertoSmpt"].ToString();
            gTsl = ConfigurationSettings.AppSettings["TslSmpt"].ToString();
            // 20190321 fin ajsute correo
        }

        /// <summary>
        /// Nombre: enviarMail
        /// Fecha: Enero 12 de 2009
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite el envo de mail.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        public string enviarMail(string lsUsuario, string lsClave, string lsServidor, string lsUser, string lsPrograma)
        {
            string[] lsDestino;
            string lsMailGestor = System.Web.HttpContext.Current.Session["MailGestor"].ToString();

            try
            {
                if (gsAsunto.Trim().Length > 0)
                {
                    // Creo la Variable de Correo
                    System.Net.Mail.MailMessage loCorreo = new System.Net.Mail.MailMessage();
                    // Fijo las Variables del Mensaje
                    loCorreo.From = new System.Net.Mail.MailAddress(gFrom); // 20190321 ajuste correo
                    lsDestino = gsDestinatario.Split(';');
                    foreach (string lsMail in lsDestino)
                    {
                        if (lsMail.Trim().Length > 0)
                            loCorreo.To.Add(lsMail);
                    }
                    loCorreo.CC.Add(lsMailGestor);
                    //loCorreo.To.Add(gsDestinatario);
                    loCorreo.Subject = gsAsunto;
                    loCorreo.Body = gsContenido;
                    loCorreo.IsBodyHtml = true;
                    loCorreo.Priority = System.Net.Mail.MailPriority.Normal;
                    // Defino el Servidor de Envio del Mail
                    System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                    smtp.Host = lsServidor;
                    // 20190321 ajsute correo
                    smtp.Port = Convert.ToInt32(gPuerto);
                    if (gTsl == "S")
                        smtp.EnableSsl = true;
                    // Defino las Credenciales del Correo
                    smtp.Credentials = new System.Net.NetworkCredential(lsUsuario, lsClave);

                    //20210410 ajsute error envio correo
                    string lsError = "";
                    int liConta = 1;
                    while (liConta <= 10)
                    {
                        //20210410 fin ajsute error envio correo
                        try
                        {

                            smtp.Send(loCorreo);
                            return "";
                        }
                        catch (Exception ex)
                        {
                            liConta++;//20210410 ajsute error envio correo
                            lsError = "Error al enviar el Mail - " + ex.Message + " ::Traza - " + ex.StackTrace;//20210410 ajsute error envio correo
                            //20210410 ajsute error envio correo
                            //EventLog Log = new EventLog();
                            //Log.Source = this.LogErrores;
                            //string error = "Error al enviar el Mail - " + ex.Message + " ::Traza - " + ex.StackTrace;
                            //Log.WriteEntry("Programa - " + lsPrograma + " ::Usuario - " + lsUser + " ::Mensaje - " + error, EventLogEntryType.Error);
                            //return "Se presento un problema con el envio de mail.! " + ex.Message.ToString();
                            //20210410 fin ajsute error envio correo
                        }
                    }
                    //20210410 ajsute error envio correo
                    EventLog Log = new EventLog();
                    Log.Source = this.LogErrores;
                    Log.WriteEntry("Programa - " + lsPrograma + " ::Usuario - " + lsUser + " ::Mensaje - " + lsError , EventLogEntryType.Error);
                    return lsError;
                    //20210410 fin ajsute error envio correo
                }    //20210410 ajsute error envio correo
                return "";
            }
            catch (Exception ex)
            {
                EventLog Log = new EventLog();
                Log.Source = this.LogErrores;
                string error = "Error al enviar el Mail - " + ex.Message + "::Traza - " + ex.StackTrace;
                Log.WriteEntry("Programa - " + lsPrograma + "::Usuario - " + lsUser + "::Mensaje - " + error, EventLogEntryType.Error);
                return "Se presento un problema con el envio de mail.! " + ex.Message.ToString();
            }
        }
        /// <summary>
        /// Nombre: enviarMailRec
        /// Fecha: Julio 7 de 2016
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite el envo de mail de la recuperacion de contraseña
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        public string enviarMailRec(string lsUsuario, string lsClave, string lsServidor, string lsUser, string lsPrograma)
        {
            string[] lsDestino;
            try
            {
                if (gsAsunto.Trim().Length > 0)
                {
                    // Creo la Variable de Correo
                    System.Net.Mail.MailMessage loCorreo = new System.Net.Mail.MailMessage();
                    // Fijo las Variables del Mensaje
                    loCorreo.From = new System.Net.Mail.MailAddress(gFrom); // 20190321 ajuste correo
                    lsDestino = gsDestinatario.Split(';');
                    foreach (string lsMail in lsDestino)
                    {
                        if (lsMail.Trim().Length > 0)
                            loCorreo.To.Add(lsMail);
                    }
                    loCorreo.Subject = gsAsunto;
                    loCorreo.Body = gsContenido;
                    loCorreo.IsBodyHtml = true;
                    loCorreo.Priority = System.Net.Mail.MailPriority.Normal;
                    // Defino el Servidor de Envio del Mail
                    System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                    smtp.Host = lsServidor;
                    // 20190321 ajsute correo
                    smtp.Port = Convert.ToInt32(gPuerto);
                    if (gTsl == "S")
                        smtp.EnableSsl = true;
                    // Habilito SSL para probar envios desde gmail
                    //smtp.EnableSsl = true;
                    // Defino las Credenciales del Correo
                    smtp.Credentials = new System.Net.NetworkCredential(lsUsuario, lsClave);

                    //20210410 ajsute error envio correo
                    string lsError = "";
                    int liConta = 1;
                    while (liConta <= 10)
                    {
                        //20210410 fin ajsute error envio correo
                        try
                        {

                            smtp.Send(loCorreo);
                            return "";
                        }
                        catch (Exception ex)
                        {
                            liConta++;//20210410 ajsute error envio correo
                            lsError = "Error al enviar el Mail - " + ex.Message + " ::Traza - " + ex.StackTrace;//20210410 ajsute error envio correo
                            //20210410 ajsute error envio correo
                            //EventLog Log = new EventLog();
                            //Log.Source = this.LogErrores;
                            //string error = "Error al enviar el Mail - " + ex.Message + " ::Traza - " + ex.StackTrace;
                            //Log.WriteEntry("Programa - " + lsPrograma + " ::Usuario - " + lsUser + " ::Mensaje - " + error, EventLogEntryType.Error);
                            //return "Se presento un problema con el envio de mail.! " + ex.Message.ToString();
                            //20210410 fin ajsute error envio correo
                        }
                    }//20210410 ajsute error envio correo
                     //20210410 ajsute error envio correo
                    EventLog Log = new EventLog();
                    Log.Source = this.LogErrores;
                    Log.WriteEntry("Programa - " + lsPrograma + " ::Usuario - " + lsUser + " ::Mensaje - " + lsError, EventLogEntryType.Error);
                    return "Se presento un problema con el envio de mail.! " +lsError ;
                    //20210410 fin ajsute error envio correo
                }
                return "";
            }
            catch (Exception ex)
            {
                EventLog Log = new EventLog();
                Log.Source = this.LogErrores;
                string error = "Error al enviar el Mail - " + ex.Message + "::Traza - " + ex.StackTrace;
                Log.WriteEntry("Programa - " + lsPrograma + "::Usuario - " + lsUser + "::Mensaje - " + error, EventLogEntryType.Error);
                return "Se presento un problema con el envio de mail.! " + ex.Message.ToString();
            }
        }
    }
}
