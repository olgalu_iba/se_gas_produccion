﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using SIB.Global.Presentacion;

namespace SIB.Global.Negocio.Manejador
{
    /// <summary>
    /// Nombre: clConexion
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Clase que permite la conexion con la base de datos.
    /// Modificacion:
    /// </summary>
    public class clConexion
    {
        public SqlConnection gObjConexion;
        public String gServidor;
        public String gBaseDatos;
        public String gServidorPdf;
        public String gBaseDatosPdf;
        public String gServidorInf;
        public String gBaseDatosInf;
        public String gUsuario;
        public String gPassword;

        public clConexion(InfoSessionVO oInfo)
        {
            gServidor = oInfo.Servidor;
            gBaseDatos = oInfo.BaseDatos;
            gServidorPdf = oInfo.ServidorPdf;
            gBaseDatosPdf = oInfo.BaseDatosPdf;
            gServidorInf = oInfo.ServidorInf;
            gBaseDatosInf = oInfo.BaseDatosInf;
            gUsuario = oInfo.Usuario;
            gPassword = oInfo.Password;
        }
        /// <summary>
        /// Nombre: Abrir
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite abrir la conexion.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        public bool Abrir()
        {
            String lStrConexion = "Data Source=" + gServidor.ToString() + "; Initial Catalog=" + gBaseDatos.ToString() + ";User ID = " + gUsuario.ToString() + "; Password=" + gPassword.ToString();
            try
            {
                gObjConexion = new SqlConnection(lStrConexion);
                gObjConexion.Open();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Nombre: Cerrar
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite cerrar la conexion.
        /// Modificacion:
        /// </summary>
        public bool Cerrar()
        {
            try
            {
                gObjConexion.Close();
                gObjConexion.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Nombre: AbrirInf
        /// Fecha: Septiembre 8 de 2010
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite abrir la conexion de los Informes.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        public bool AbrirInf()
        {
            String lStrConexion = "Data Source=" + gServidorPdf.ToString() + "; Initial Catalog=" + gBaseDatosPdf.ToString() + ";User ID = " + gUsuario.ToString() + "; Password=" + gPassword.ToString();
            try
            {
                gObjConexion = new SqlConnection(lStrConexion);
                gObjConexion.Open();
                SqlConnection.ClearAllPools();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Nombre: CerrarInf
        /// Fecha: Septiembre 8 de 2010
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite cerrar la conexion de los Informes.
        /// Modificacion:
        /// </summary>
        public bool CerrarInf()
        {
            try
            {
                gObjConexion.Close();
                gObjConexion.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Nombre: AbrirInf
        /// Fecha: Septiembre 8 de 2010
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite abrir la conexion de los Informes.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        public bool AbrirInforme()
        {
            String lStrConexion = "Data Source=" + gServidorInf.ToString() + "; Initial Catalog=" + gBaseDatosInf.ToString() + ";User ID = " + gUsuario.ToString() + "; Password=" + gPassword.ToString();
            try
            {
                gObjConexion = new SqlConnection(lStrConexion);
                gObjConexion.Open();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Nombre: CerrarInf
        /// Fecha: Septiembre 8 de 2010
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite cerrar la conexion de los Informes.
        /// Modificacion:
        /// </summary>
        public bool CerrarInforme()
        {
            try
            {
                gObjConexion.Close();
                gObjConexion.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Nombre: AbrirRep
        /// Fecha: Octubre 18 de 2012
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite abrir la conexion a la repliza para el cambio de clave
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        public bool AbrirRep(string lsServidor, string lsBaseDatos, string lsUsuario, string lsClave)
        {
            String lStrConexion = "Data Source=" + lsServidor.ToString() + "; Initial Catalog=" + lsBaseDatos.ToString() + ";User ID = " + lsUsuario.ToString() + "; Password=" + lsClave.ToString();
            try
            {
                gObjConexion = new SqlConnection(lStrConexion);
                gObjConexion.Open();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Nombre: CerrarRep
        /// Fecha: Octubre 18 de 2012
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite cerrar la conexion a la replica.
        /// Modificacion:
        /// </summary>
        public bool CerrarRep()
        {
            try
            {
                gObjConexion.Close();
                gObjConexion.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }




    }
}
