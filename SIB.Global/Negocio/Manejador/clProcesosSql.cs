﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIB.Global.Presentacion;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace SIB.Global.Negocio.Manejador
{
    /// <summary>
    /// Nombre: clProcesosSql
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Clase que contiene los metodos realizar todos los procesos que accesan la informacion de la Base de Datos,
    ///              como son: Llenenar controles DDL y DTG, ejecutar Procedimientos Almacenados.
    /// Modificacion:
    /// </summary>
    public class clProcesosSql
    {
        private SqlDataReader gLector;
        private SqlCommand gComando = new SqlCommand();
        private DataSet gObjetoSet;
        private SqlDataAdapter gAdaptador;
        public String gsProcedimiento;
        public String[] gsNombreParametros;
        public SqlDbType[] gTipoParametros;
        public Object[] gValorParametros;
        private String gsCondicion;
        private String gsTabla;


        public clProcesosSql()
        {

        }

        public clProcesosSql(String lsProcedimiento, string[] lsNombreParametros, SqlDbType[] lTipoparametros, Object[] lValorParametros)
        {
            gsProcedimiento = lsProcedimiento;
            gsNombreParametros = lsNombreParametros;
            gTipoParametros = lTipoparametros;
            gValorParametros = lValorParametros;
            gComando.CommandTimeout = 3600;
        }

        public clProcesosSql(String lsProcedimiento, string[] lsNombreParametros, Object[] lValorParametros)
        {
            gsProcedimiento = lsProcedimiento;
            gsNombreParametros = lsNombreParametros;
            gValorParametros = lValorParametros;
            gComando.CommandTimeout = 3600;
        }

        public clProcesosSql(String lsProcedimiento)
        {
            gsProcedimiento = lsProcedimiento;
            gComando.CommandTimeout = 3600;
        }

        public clProcesosSql(String lsProcedimiento, string lsTabla, string lsCondicion)
        {
            gsProcedimiento = lsProcedimiento;
            gsTabla = lsTabla;
            gsCondicion = lsCondicion;
            gComando.CommandTimeout = 3600;
        }

        /// <summary>
        /// Nombre: EjecutarProcedimiento
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite la ejecucion de un procedimiento almacenado y retorna Falso o Verdadero si encontro o No datos.
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <returns></returns>
        public bool EjecutarProcedimiento(SqlConnection lConexion)
        {
            try
            {
                Int32 liNumeroParametros = 0;
                Int32 liRegistros = 0;
                gComando.Connection = lConexion;
                gComando.CommandType = CommandType.StoredProcedure;
                gComando.CommandText = gsProcedimiento;
                if (gsNombreParametros != null)
                {
                    for (liNumeroParametros = 0; liNumeroParametros <= gsNombreParametros.Length - 1; liNumeroParametros++)
                    {
                        gComando.Parameters.Add(gsNombreParametros[liNumeroParametros], gTipoParametros[liNumeroParametros]).Value = gValorParametros[liNumeroParametros];
                    }
                }
                liRegistros = gComando.ExecuteNonQuery();
                if (liRegistros > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Nombre: EjecutarProcedimiento
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite la ejecucion de un procedimiento almacenado y Retorna si actualizó o No Registros
        ///              y actualiza la variable de session con el mensaje de error.
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <returns></returns>
        public bool EjecutarProcedimiento(SqlConnection lConexion, InfoSessionVO oInfo)
        {
            try
            {
                Int32 liNumeroParametros = 0;
                Int32 liRegistros = 0;
                gComando.Connection = lConexion;
                gComando.CommandType = CommandType.StoredProcedure;
                gComando.CommandText = gsProcedimiento;
                if (gsNombreParametros != null)
                {
                    for (liNumeroParametros = 0; liNumeroParametros <= gsNombreParametros.Length - 1; liNumeroParametros++)
                    {
                        gComando.Parameters.Add(gsNombreParametros[liNumeroParametros], gTipoParametros[liNumeroParametros]).Value = gValorParametros[liNumeroParametros];
                    }
                }
                oInfo.mensaje_error = "";
                liRegistros = gComando.ExecuteNonQuery();
                if (liRegistros > 0)
                    return true;
                else
                {
                    oInfo.mensaje_error = "NO SE ACTUALIZARON REGISTROS.";
                    return false;
                }
            }
            catch (Exception ex)
            {
                oInfo.mensaje_error = ex.Message.ToString();
                return false;
            }
        }
        /// <summary>
        /// Nombre: EjecutarProcedimientoConTransaccion
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite la ejecucion de un procedimiento almacenado controlando transacciones, 
        ///              y retorna Falso o Verdadero si encontro o No datos.
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <param name="oTransaccion"></param>
        /// <returns></returns>
        public bool EjecutarProcedimientoConTransaccion(SqlConnection lConexion, SqlTransaction oTransaccion)
        {
            try
            {
                Int32 liNumeroParametros = 0;
                Int32 liRegistros = 0;
                gComando.Connection = lConexion;
                gComando.Transaction = oTransaccion;
                gComando.CommandType = CommandType.StoredProcedure;
                gComando.CommandText = gsProcedimiento;
                if (gsNombreParametros != null)
                {
                    for (liNumeroParametros = 0; liNumeroParametros <= gsNombreParametros.Length - 1; liNumeroParametros++)
                    {
                        gComando.Parameters.Add(gsNombreParametros[liNumeroParametros], gTipoParametros[liNumeroParametros]).Value = gValorParametros[liNumeroParametros];
                    }
                }
                liRegistros = gComando.ExecuteNonQuery();
                if (liRegistros > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Nombre: EjecutarProcedimientoConTransaccion
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite la ejecucion de un procedimiento almacenado controlando transacciones, 
        ///              y actualiza la variable de session con el mensaje de error..
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <param name="oTransaccion"></param>
        /// <returns></returns>
        public bool EjecutarProcedimientoConTransaccion(SqlConnection lConexion, SqlTransaction oTransaccion, InfoSessionVO oInfo)
        {
            try
            {
                Int32 liNumeroParametros = 0;
                Int32 liRegistros = 0;
                gComando.Connection = lConexion;
                gComando.Transaction = oTransaccion;
                gComando.CommandType = CommandType.StoredProcedure;
                gComando.CommandText = gsProcedimiento;
                if (gsNombreParametros != null)
                {
                    for (liNumeroParametros = 0; liNumeroParametros <= gsNombreParametros.Length - 1; liNumeroParametros++)
                    {
                        gComando.Parameters.Add(gsNombreParametros[liNumeroParametros], gTipoParametros[liNumeroParametros]).Value = gValorParametros[liNumeroParametros];
                    }
                }
                oInfo.mensaje_error = "";
                liRegistros = gComando.ExecuteNonQuery();
                if (liRegistros > 0)
                    return true;
                else
                {
                    oInfo.mensaje_error = "NO SE ACTUALIZARON REGISTROS";
                    return false;
                }
            }
            catch (Exception ex)
            {
                oInfo.mensaje_error = ex.Message.ToString();
                return false;
            }
        }
        /// <summary>
        /// Nombre: EjecutarProcedimientoYObtenerDatos
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite la ejecucion de un procedimiento almacenado y retorna un SQLDATAREADER con la informacion
        ///              que devuelve el Procedimiento almacenado.
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <returns></returns>
        public SqlDataReader EjecutarProcedimientoYObtenerDatos(SqlConnection lConexion)
        {
            try
            {
                Int32 liNumeroParametros = 0;
                gComando.Connection = lConexion;
                gComando.CommandType = CommandType.StoredProcedure;
                gComando.CommandText = gsProcedimiento;
                if (gsNombreParametros != null)
                {
                    for (liNumeroParametros = 0; liNumeroParametros <= gsNombreParametros.Length - 1; liNumeroParametros++)
                    {
                        gComando.Parameters.Add(gsNombreParametros[liNumeroParametros], gTipoParametros[liNumeroParametros]).Value = gValorParametros[liNumeroParametros];
                    }
                }
                gLector = gComando.ExecuteReader();
                return gLector;
            }
            catch (Exception ex)
            {
                gLector = null;
                return gLector;
            }
        }
        /// <summary>
        /// Nombre: EjecutarProcedimientoYObtenerDatos
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite la ejecucion de un procedimiento almacenado y retorna un SQLDATAREADER con la informacion
        ///              que devuelve el Procedimiento almacenado y actualiza la variable de session con el mensaje de error.
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <returns></returns>
        public SqlDataReader EjecutarProcedimientoYObtenerDatos(SqlConnection lConexion, InfoSessionVO oInfo)
        {
            try
            {
                Int32 liNumeroParametros = 0;
                gComando.Connection = lConexion;
                gComando.CommandType = CommandType.StoredProcedure;
                gComando.CommandText = gsProcedimiento;
                if (gsNombreParametros != null)
                {
                    for (liNumeroParametros = 0; liNumeroParametros <= gsNombreParametros.Length - 1; liNumeroParametros++)
                    {
                        gComando.Parameters.Add(gsNombreParametros[liNumeroParametros], gTipoParametros[liNumeroParametros]).Value = gValorParametros[liNumeroParametros];
                    }
                }
                oInfo.mensaje_error = "";
                gLector = gComando.ExecuteReader();
                return gLector;
            }
            catch (Exception ex)
            {
                oInfo.mensaje_error = ex.Message.ToString();
                gLector = null;
                return gLector;
            }
        }
        /// <summary>
        /// <summary>
        /// Nombre: EjecutarProcedimientoYObtenerDatosConTransaccion
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite la ejecucion de un procedimiento almacenado controlando transaccion y retorna un 
        ///              SQLDATAREADER con la informacion que devuelve el Procedimiento almacenado.
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <returns></returns>
        /// </summary>
        /// <param name="lConexion"></param>
        /// <param name="oTransaccion"></param>
        /// <returns></returns>
        public SqlDataReader EjecutarProcedimientoYObtenerDatosConTransaccion(SqlConnection lConexion, SqlTransaction oTransaccion)
        {
            try
            {
                Int32 liNumeroParametros = 0;
                gComando.Connection = lConexion;
                gComando.Transaction = oTransaccion;
                gComando.CommandType = CommandType.StoredProcedure;
                gComando.CommandText = gsProcedimiento;
                for (liNumeroParametros = 0; liNumeroParametros <= gsNombreParametros.Length - 1; liNumeroParametros++)
                {
                    gComando.Parameters.Add(gsNombreParametros[liNumeroParametros], gTipoParametros[liNumeroParametros]).Value = gValorParametros[liNumeroParametros];
                }
                gLector = gComando.ExecuteReader();
                return gLector;
            }
            catch (Exception ex)
            {
                return gLector;
            }
        }
        /// <summary>
        /// Nombre: EjecutarProcedimientoYObtenerDatosConTransaccion
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite la ejecucion de un procedimiento almacenado controlando transaccion y retorna un 
        ///              SQLDATAREADER con la informacion que devuelve el Procedimiento almacenado.
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <returns></returns>
        /// </summary>
        /// <param name="lConexion"></param>
        /// <param name="oTransaccion"></param>
        /// <returns></returns>
        public SqlDataReader EjecutarProcedimientoYObtenerDatosConTransaccion(SqlConnection lConexion, SqlTransaction oTransaccion, InfoSessionVO oInfo)
        {
            try
            {
                Int32 liNumeroParametros = 0;
                gComando.Connection = lConexion;
                gComando.Transaction = oTransaccion;
                gComando.CommandType = CommandType.StoredProcedure;
                gComando.CommandText = gsProcedimiento;
                for (liNumeroParametros = 0; liNumeroParametros <= gsNombreParametros.Length - 1; liNumeroParametros++)
                {
                    gComando.Parameters.Add(gsNombreParametros[liNumeroParametros], gTipoParametros[liNumeroParametros]).Value = gValorParametros[liNumeroParametros];
                }
                oInfo.mensaje_error = "";
                gLector = gComando.ExecuteReader();
                return gLector;
            }
            catch (Exception ex)
            {
                oInfo.mensaje_error = ex.Message.ToString();
                return gLector;
            }
        }
        /// <summary>
        /// Nombre: EjecutarProcedimientoRetornando
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite la ejecucion de un procedimiento almacenado y retorna un SQLDATAREADER con la informacion
        ///              que devuelve el Procedimiento almacenado.
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <returns></returns>
        public SqlDataReader EjecutarProcedimientoRetornando(SqlConnection lConexion)
        {
            try
            {
                gComando.Connection = lConexion;
                gComando.CommandType = CommandType.StoredProcedure;
                gComando.CommandText = gsProcedimiento;
                gLector = gComando.ExecuteReader();
                return gLector;
            }
            catch (Exception ex)
            {
                return gLector;
            }
        }

        /// <summary>
        /// Nombre: LlenarGrilla
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite ejecutar el procedimiento almacenado y llenar el control DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <returns></returns>
        public DataView LlenarGrilla(SqlConnection lConexion)
        {
            gComando.Connection = lConexion;
            gComando.CommandType = CommandType.StoredProcedure;
            gComando.CommandText = gsProcedimiento;
            gAdaptador = new SqlDataAdapter(gComando);
            gObjetoSet = new DataSet();
            gAdaptador.Fill(gObjetoSet);
            return gObjetoSet.Tables[0].DefaultView;
        }
        /// <summary>
        /// Nombre: LlenarGrillaConParametros
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite ejecutar el procedimiento almacenado y llenar el control DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <returns></returns>
        public DataView LlenarGrillaConParametros(SqlConnection lConexion)
        {
            try
            {
                Int32 liNumeroParametros = 0;
                Int32 liRegistros = 0;
                gComando.Connection = lConexion;
                gComando.CommandType = CommandType.StoredProcedure;
                gComando.CommandText = gsProcedimiento;
                for (liNumeroParametros = 0; liNumeroParametros <= gsNombreParametros.Length - 1; liNumeroParametros++)
                {
                    gComando.Parameters.Add(gsNombreParametros[liNumeroParametros], gTipoParametros[liNumeroParametros]).Value = gValorParametros[liNumeroParametros];
                }
                gAdaptador = new SqlDataAdapter(gComando);
                gObjetoSet = new DataSet();
                gAdaptador.Fill(gObjetoSet);
                return gObjetoSet.Tables[0].DefaultView;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Nombre: LlenarControl
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite ejecutar el procedimiento almacenado y Retornar un SqlDataReader
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <returns></returns>
        public SqlDataReader LlenarControl(SqlConnection lConexion)
        {
            gComando.Connection = lConexion;
            gComando.CommandType = CommandType.StoredProcedure;
            gComando.CommandText = gsProcedimiento;
            gComando.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = gsTabla;
            gComando.Parameters.Add("@P_filtro", SqlDbType.VarChar).Value = gsCondicion;
            gLector = gComando.ExecuteReader();
            return gLector;
        }
        
        /// <summary>
        /// Nombre: LlenarGrillaParametros
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite ejecutar el procedimiento almacenado y llenar el control DataGrid, recibiendo
        ///              los parametros para la ejecucion del procedimiento almacenado
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <returns></returns>
        public DataView LlenarGrillaParametros(SqlConnection lConexion, String lsCondicion)
        {
            gComando.Connection = lConexion;
            gComando.CommandType = CommandType.StoredProcedure;
            gComando.CommandText = gsProcedimiento;
            gComando.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = "";
            gComando.Parameters.Add("@P_filtro", SqlDbType.VarChar).Value = lsCondicion;
            gAdaptador = new SqlDataAdapter(gComando);
            gObjetoSet = new DataSet();
            gAdaptador.Fill(gObjetoSet);
            return gObjetoSet.Tables[0].DefaultView;
        }

        /// <summary>
        /// Nombre: llenarHtmlTabla
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Tabla Htm para la Exportacion a Excel de los Maestros e Informes
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <returns></returns>
        public String llenarHtmlTabla(SqlConnection lConexion, string[] lsColumnas)
        {
            string lsCadena = "";
            gComando.Connection = lConexion;
            gComando.CommandType = CommandType.StoredProcedure;
            gComando.CommandText = gsProcedimiento;
            gComando.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = gsTabla;
            gComando.Parameters.Add("@P_filtro", SqlDbType.VarChar).Value = gsCondicion;
            if (lsColumnas.Length > 0)
            {
                gLector = gComando.ExecuteReader();
                if (gLector.HasRows)
                {
                    while (gLector.Read())
                    {
                        lsCadena += "<tr>";
                        foreach (string lstd in lsColumnas)
                        {
                            lsCadena += "<td>";
                            lsCadena += gLector[lstd].ToString();
                            lsCadena += "</td>";
                        }
                        lsCadena += "</tr>";
                    }
                }
            }
            else
                return "";
            return lsCadena;
        }

        /// <summary>
        /// Nombre: llenarHtmlTabla
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Tabla Htm para la Exportacion a Excel de los Maestros e Informes,
        ///              Recibiendo un Indicador Especial Indicando que es com parametros variables
        /// Modificacion:
        /// </summary>
        /// <param name="lConexion"></param>
        /// <param name="lsColumnas"></param>
        /// <param name="lsIndica"></param>
        /// <returns></returns>
        public String llenarHtmlTabla(SqlConnection lConexion, string[] lsColumnas, string lsIndica)
        {
            string lsCadena = "";
            Int32 liNumeroParametros = 0;
            gComando.Connection = lConexion;
            gComando.CommandType = CommandType.StoredProcedure;
            gComando.CommandText = gsProcedimiento;
            for (liNumeroParametros = 0; liNumeroParametros <= gsNombreParametros.Length - 1; liNumeroParametros++)
            {
                gComando.Parameters.Add(gsNombreParametros[liNumeroParametros], SqlDbType.VarChar).Value = gValorParametros[liNumeroParametros];
            }
            if (lsColumnas.Length > 0)
            {
                gLector = gComando.ExecuteReader();
                if (gLector.HasRows)
                {
                    while (gLector.Read())
                    {
                        lsCadena += "<tr>";
                        foreach (string lstd in lsColumnas)
                        {
                            lsCadena += "<td>";
                            lsCadena += gLector[lstd].ToString();
                            lsCadena += "</td>";
                        }
                        lsCadena += "</tr>";
                    }
                }
            }
            else
                return "";
            return lsCadena;
        }


    }
}
