﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIB.Global.Presentacion;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace SIB.Global.Negocio.Manejador
{
    /// <summary>
    /// Nombre: clValidaciones
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Clase que contiene los metodos para realizar las difernetes validaciones del SIB.
    /// Modificacion:
    /// </summary>
    public class clValidaciones
    {
        private SqlCommand gComando = new SqlCommand();
        /// <summary>
        /// Nombre: ValidarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo que permite realizar la validacion de la existencia de un registro en la
        ///              Base de datos, permite recibir como parametros: Tabla, Condicion y Informacion de Session.
        /// Modificacion:
        /// </summary>
        /// <param name="lsTabla"></param>
        /// <param name="lsCondicion"></param>
        /// <param name="lsInfo"></param>
        /// <returns></returns>
        public bool ValidarExistencia(string lsTabla, string lsCondicion, InfoSessionVO lsInfo)
        {
            bool lEncontro = false;
            SqlDataReader lLectorConsulta;
            clConexion lConexion = new clConexion(lsInfo);

            lConexion.Abrir();
            gComando.Connection = lConexion.gObjConexion;
            gComando.CommandText = "pa_ValidarExistencia";
            gComando.CommandType = CommandType.StoredProcedure;
            gComando.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = lsTabla;
            gComando.Parameters.Add("@P_filtro", SqlDbType.VarChar).Value = lsCondicion;
            lLectorConsulta = gComando.ExecuteReader();
            if (lLectorConsulta.HasRows)
            {
                lEncontro = true;
            }
            lLectorConsulta.Close();
            lLectorConsulta.Dispose();
            lConexion.Cerrar();
            return lEncontro;
        }
        /// <summary>
        /// Nombre: ValidarDigitoVerificacion
        /// Fecha: Agosto 17 de 2008
        /// Descripcion: Resuelve el digito de verificacion de un nit ingresado
        /// Recibe como parametro el nit en tipo de dato entero largo y devuelve un entero
        /// Modificacion:
        /// </summary>
        /// <param name="llnit_tercero"></param>
        /// <returns></returns>
        public int ValidarDigitoVerificacion(long llnit_tercero)
        {
            string lsdato = ""; // cadena de nit rellena con ceros 
            string lsnit = ""; //nit resuelto como string
            Int16 licont;
            Int32 lisuma = 0;
            Int32 lires;

            lsnit = llnit_tercero.ToString();

            int[] lopa = { 71, 67, 59, 53, 47, 43, 41, 37, 29, 23, 19, 17, 13, 7, 3 };
            lsdato = lsnit.PadLeft(15, '0');
            for (licont = 0; licont <= 14; licont++)
            {
                lisuma = lisuma + Convert.ToInt32(lsdato.Substring(licont, 1)) * lopa[licont];
            }
            lisuma = lisuma - (11 * (lisuma / 11));
            if (lisuma == 0 || lisuma == 1)
            {
                lires = lisuma;
            }
            else
            {
                lires = 11 - lisuma;
            }

            return lires;

        }
        /// <summary>
        /// Nombre: CrearFecha
        /// Fecha: Octubre 29 de 2008
        /// Descripcion: Obtiene los rangos de fecha minimo y maximo, partiendo de una fecha dada y un rangop de dias
        /// Modificacion:
        /// </summary>
        /// <param name="lsFecha"></param>
        /// <param name="liDesde"></param>
        /// <param name="liHasta"></param>
        /// <param name="lsDiaHabil"></param>
        /// <param name="lsIndica"></param>
        /// <param name="lsInfo"></param>
        /// <returns></returns>
        public string[] CrearFecha(string lsFecha, Int32 liDesde, Int32 liHasta, string lsDiaHabil, string lsIndica, InfoSessionVO lsInfo)
        {
            DateTime oFecha;
            string[] lsRangoFechas = { lsFecha, lsFecha };
            Int32 liDias = 0;
            SqlDataReader lLectorConsulta;
            clConexion lConexion = new clConexion(lsInfo);

            if (lsDiaHabil == "S")
            {
                if (lsIndica == "D")
                {
                    liDias = liDesde;
                    if (liDias != 0)
                    {
                        lConexion.Abrir();
                        gComando.Connection = lConexion.gObjConexion;
                        gComando.CommandText = "pa_traerFecha";
                        gComando.CommandType = CommandType.StoredProcedure;
                        gComando.Parameters.Add("@P_dias", SqlDbType.Int).Value = liDias;
                        gComando.Parameters.Add("@P_fecha", SqlDbType.VarChar).Value = lsFecha;
                        gComando.Parameters.Add("@P_indicador_dia_habil", SqlDbType.Char).Value = lsDiaHabil;
                        gComando.Parameters.Add("@P_indica", SqlDbType.Char).Value = lsIndica;
                        lLectorConsulta = gComando.ExecuteReader();
                        if (lLectorConsulta.HasRows)
                        {
                            while (lLectorConsulta.Read())
                                lsRangoFechas[0] = lLectorConsulta.GetValue(0).ToString().Replace("-", "/");
                        }
                        lLectorConsulta.Close();
                        lLectorConsulta.Dispose();
                        lConexion.Cerrar();
                    }
                }
                else
                {
                    liDias = liHasta;
                    if (liDias != 0)
                    {
                        lConexion.Abrir();
                        gComando.Connection = lConexion.gObjConexion;
                        gComando.CommandText = "pa_traerFecha";
                        gComando.CommandType = CommandType.StoredProcedure;
                        gComando.Parameters.Add("@P_dias", SqlDbType.Int).Value = liDias;
                        gComando.Parameters.Add("@P_fecha", SqlDbType.VarChar).Value = lsFecha;
                        gComando.Parameters.Add("@P_indicador_dia_habil", SqlDbType.Char).Value = lsDiaHabil;
                        gComando.Parameters.Add("@P_indica", SqlDbType.Char).Value = lsIndica;
                        lLectorConsulta = gComando.ExecuteReader();
                        if (lLectorConsulta.HasRows)
                        {
                            while (lLectorConsulta.Read())
                                lsRangoFechas[1] = lLectorConsulta.GetValue(0).ToString().Replace("-", "/");
                        }
                        lLectorConsulta.Close();
                        lLectorConsulta.Dispose();
                        lConexion.Cerrar();
                        gComando.Parameters.Clear();
                    }
                    liDias = liDesde;
                    if (liDias < 0)
                        liDias = liDias * (-1);
                    if (liDias != 0)
                    {
                        lConexion.Abrir();
                        gComando.Connection = lConexion.gObjConexion;
                        gComando.CommandText = "pa_traerFecha";
                        gComando.CommandType = CommandType.StoredProcedure;
                        gComando.Parameters.Add("@P_dias", SqlDbType.Int).Value = liDias;
                        gComando.Parameters.Add("@P_fecha", SqlDbType.VarChar).Value = lsFecha;
                        gComando.Parameters.Add("@P_indicador_dia_habil", SqlDbType.Char).Value = lsDiaHabil;
                        gComando.Parameters.Add("@P_indica", SqlDbType.Char).Value = "D";
                        lLectorConsulta = gComando.ExecuteReader();
                        if (lLectorConsulta.HasRows)
                        {
                            while (lLectorConsulta.Read())
                                lsRangoFechas[0] = lLectorConsulta.GetValue(0).ToString().Replace("-", "/");
                        }
                        lLectorConsulta.Close();
                        lLectorConsulta.Dispose();
                        lConexion.Cerrar();
                    }
                }
            }
            else
            {
                if (lsIndica == "D")
                {
                    oFecha = Convert.ToDateTime(lsFecha).AddDays(Convert.ToDouble(-liDesde));
                    lsRangoFechas[0] = oFecha.Year.ToString() + "/" + oFecha.Month.ToString() + "/" + oFecha.Day.ToString();
                }
                else
                {
                    oFecha = Convert.ToDateTime(lsFecha).AddDays(Convert.ToDouble(liDesde));
                    lsRangoFechas[0] = oFecha.Year.ToString() + "/" + oFecha.Month.ToString() + "/" + oFecha.Day.ToString();
                    oFecha = Convert.ToDateTime(lsFecha).AddDays(Convert.ToDouble(liHasta));
                    lsRangoFechas[1] = oFecha.Year.ToString() + "/" + oFecha.Month.ToString() + "/" + oFecha.Day.ToString();
                }
            }
            if (lsIndica == "A")
            {
                if (liDesde == 0)
                    lsRangoFechas[0] = lsFecha;
                if (liHasta == 0)
                    lsRangoFechas[1] = lsFecha;
            }
            return lsRangoFechas;
        }
        /// <summary>
        /// Nombre: CrearFecha
        /// Fecha: Octubre 29 de 2008
        /// Descripcion: Obtiene los rangos de fecha minimo y maximo, partiendo de una fecha dada y un rangop de dias
        /// Modificacion:
        /// </summary>
        /// <param name="lsInfo"></param>
        /// <param name="ldConsecutivoInterno"></param>
        /// <returns></returns>
        public string ValidarModificacion(InfoSessionVO lsInfo, decimal ldConsecutivoInterno)
        {
            string lsMensaje = "";

            SqlDataReader lLectorConsulta;
            clConexion lConexion = new clConexion(lsInfo);

            lConexion.Abrir();
            gComando.Connection = lConexion.gObjConexion;
            gComando.CommandText = "pa_GetParametrosModificacion";
            gComando.CommandType = CommandType.StoredProcedure;
            gComando.Parameters.Add("@P_consecutivo_interno", SqlDbType.Decimal).Value = ldConsecutivoInterno;
            lLectorConsulta = gComando.ExecuteReader();
            if (lLectorConsulta.HasRows)
            {
                lLectorConsulta.Read();
                if (Convert.ToInt32(lLectorConsulta["NoMaxModifi"].ToString()) <= Convert.ToInt32(lLectorConsulta["NoModifiOpe"].ToString()))
                    lsMensaje += "Ya se exedió el Número máximo de modificaciones. <br>";
                if (Convert.ToInt32(lLectorConsulta["NoMaxDiasModif"].ToString()) < Convert.ToInt32(lLectorConsulta["NoDiasOpera"].ToString()))
                    lsMensaje += "Ya exedió el máximo de días para modificación. <br>";
            }
            lLectorConsulta.Close();
            lLectorConsulta.Dispose();
            lConexion.Cerrar();
            return lsMensaje;
        }
        /// <summary>
        /// Nombre: ValidacionFecha
        /// Fecha: Octubre 29 de 2008
        /// Descripcion: Realiza la validacion de la fecha, en formato dd/mm/yyyy.
        /// Modificacion:
        /// </summary>
        /// <param name="lsFecha"></param>
        /// <returns></returns>
        public bool ValidacionFecha(string lsFecha)
        {
            DateTime ldFecha;
            try
            {
                ldFecha = Convert.ToDateTime(lsFecha);

            }
            catch (Exception ex)
            {
                return false;
            }
            try
            {
                if (Convert.ToInt32(lsFecha.Substring(0, 4)) < 2000)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Nombre: ValidaCont
        /// Fecha: Octubre 29 de 2008
        /// Descripcion: Realiza la validacion de la contraseña
        /// Modificacion:
        /// </summary>
        /// <param name="lsFecha"></param>
        /// <returns></returns>
        public string ValidaCont(string lsContraseña, string lsContraseñaEncrip, string lsUsuario, string lsIndicador, InfoSessionVO lsInfo)
        {
            string lsError = "";
            try
            {
                int liLonMin = Convert.ToInt16(ConfigurationSettings.AppSettings["LongMinCont"].ToString());
                int liLonMax = Convert.ToInt16(ConfigurationSettings.AppSettings["LongMaxCont"].ToString());
                int liMinNum = Convert.ToInt16(ConfigurationSettings.AppSettings["MinNumCont"].ToString());
                int liMinMay = Convert.ToInt16(ConfigurationSettings.AppSettings["MinMayCont"].ToString());
                int liMinMin = Convert.ToInt16(ConfigurationSettings.AppSettings["MinMinCont"].ToString());
                int liCantNum = 0;
                int liCantMay = 0;
                int liCantMin = 0;
                Regex oExpNum = new Regex("^[0-9]");
                Regex oExpMay = new Regex("^[A-Z]");
                Regex oExpMin = new Regex("^[a-z]");

                if (lsContraseña.Length < liLonMin)
                    lsError += "La contraseña debe tener mínimo " + liLonMin.ToString() + " caracteres <br>";
                if (lsContraseña.Length > liLonMax)
                    lsError += "La contraseña debe tener máximo " + liLonMax.ToString() + " caracteres <br>";
                for (int i = 0; i < lsContraseña.Length; i++)
                {
                    if (oExpNum.IsMatch(lsContraseña[i].ToString()))
                        liCantNum++;
                    if (oExpMay.IsMatch(lsContraseña[i].ToString()))
                        liCantMay++;
                    if (oExpMin.IsMatch(lsContraseña[i].ToString()))
                        liCantMin++;
                }
                if (liCantNum < liMinNum)
                    lsError += "La contraseña debe tener mínimo " + liMinNum.ToString() + " digitos numéricos <br>";
                if (liCantMay < liMinMay)
                    lsError += "La contraseña debe tener mínimo " + liMinMay.ToString() + " letras mayusculas <br>";
                if (liCantMin < liMinMin)
                    lsError += "La contraseña debe tener mínimo " + liMinMin.ToString() + " letras minusculas <br>";
            }
            catch (Exception ex)
            {
                lsError += "No se pudo consultar los parametros de control de contraseña<br>";
            }

            try
            {
                SqlDataReader lLectorConsulta;
                clConexion lConexion = new clConexion(lsInfo);

                string[] oNombre;
                lConexion.Abrir();
                gComando.Connection = lConexion.gObjConexion;
                gComando.CommandText = "pa_ValidarExistencia";
                gComando.CommandType = CommandType.StoredProcedure;
                gComando.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = "a_usuario";
                gComando.Parameters.Add("@P_filtro", SqlDbType.VarChar).Value = " login = '" + lsUsuario + "'";
                lLectorConsulta = gComando.ExecuteReader();
                if (lLectorConsulta.HasRows)
                {
                    lLectorConsulta.Read();
                    oNombre = lLectorConsulta["nombre"].ToString().Split(' ');
                    for (int i = 0; i < oNombre.Length; i++)
                    {
                        if (oNombre[i].Trim() != "" && System.Text.RegularExpressions.Regex.IsMatch(lsContraseña, oNombre[i], System.Text.RegularExpressions.RegexOptions.IgnoreCase))
                            lsError += "No puede usar el nombre/apellido " + oNombre[i] + " como parte de la contraseña <br>";
                    }
                    if (lLectorConsulta["estado_contrasena"].ToString() == "I" && lsIndicador == "I")
                        lsError += "La contraseña esta bloqueada por tiempo de caducidad. Debe Cambiarla <br>";
                    //20220211 contraseña
                    if (lLectorConsulta["estado_contr_fall"].ToString() == "I" )
                        lsError += "La contraseña esta bloqueada por intentos fallidos.";
                }
                lLectorConsulta.Close();
                lLectorConsulta.Dispose();
                string[] oMeses = { "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };
                for (int i = 0; i < oMeses.Length; i++)
                {
                    if (System.Text.RegularExpressions.Regex.IsMatch(lsContraseña, oMeses[i], System.Text.RegularExpressions.RegexOptions.IgnoreCase))
                    {
                        lsError += "No puede usar un mes como parte de la contraseña <br>";
                        break;
                    }
                }
                if (lsError == "" && lsIndicador == "C")
                {
                    gComando.Parameters.Clear();
                    gComando.CommandText = "pa_ValidarExistencia";
                    gComando.CommandType = CommandType.StoredProcedure;
                    gComando.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = "m_contrasena_h";
                    gComando.Parameters.Add("@P_filtro", SqlDbType.VarChar).Value = " login_usuario = '" + lsUsuario + "' and contrasena ='" + lsContraseñaEncrip + "'";
                    lLectorConsulta = gComando.ExecuteReader();
                    if (lLectorConsulta.HasRows)
                    {
                        lsError += "La contraseña ya se ha usado anteriormente<br>";
                    }
                    lLectorConsulta.Close();
                    lLectorConsulta.Dispose();
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lsError += "Usuario o Clave invalido<br>";
            }

            return lsError;
        }

    }
}
