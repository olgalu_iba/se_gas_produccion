﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using PCD_Infraestructura.Business;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace SIB.Global.Negocio
{
    public abstract class FachadaSIB : AbstractFacade
    {

        public Boolean autenticar(InfoSessionVO info)
        {
            ManejadorSIB manejador = (ManejadorSIB)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorSIB), (AbstractInfoSession)info);
            return manejador.validarConexion();
        }

        public Boolean actualizarPassword(InfoSessionVO info, string nuevoPassword)
        {
            ManejadorSIB manejador = (ManejadorSIB)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorSIB), info);
            return manejador.actualizarPassword(info.Usuario, nuevoPassword, info.Password);
        }

        public void accesoSistema(InfoSessionVO info)
        {
            ManejadorSIB manejador = (ManejadorSIB)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorSIB), info);
            manejador.accesoSistema(info.Usuario);
        }

        public void registrarProceso(InfoSessionVO info, string lsProceso, string lsRegistro)
        {
            ManejadorSIB manejador = (ManejadorSIB)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorSIB), info);
            manejador.registrarProceso(info.Usuario, lsProceso, lsRegistro);
        }


        public List<Hashtable> consultarMenu(InfoSessionVO info, int liPadre)
        {
            ManejadorSIB manejador = (ManejadorSIB)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorSIB), info);
            return manejador.consultarMenu(liPadre);
        }

        public List<Hashtable> consultarMenuGrupo(InfoSessionVO info, int liPadre, int liGrupoUsuario)
        {
            ManejadorSIB manejador = (ManejadorSIB)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorSIB), info);
            return manejador.consultarMenuGrupo(liPadre, liGrupoUsuario);
        }
        public Hashtable consultarPermisoMenu(InfoSessionVO info, String lsTabla)
        {
            ManejadorSIB manejador = (ManejadorSIB)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorSIB), info);
            return manejador.consultarPermisoMenu(lsTabla);
        }

        public DataTable consultarTabla<T>(InfoSessionVO infoSession, IEnumerable<T> collection, List<String> llFiltros, string lsCondicion)
        {
            ManejadorSIB manejador = (ManejadorSIB)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorSIB), infoSession);
            return manejador.consultarTabla<T>(collection, llFiltros, lsCondicion);
        }

        public String DefinirFiltros<T>(InfoSessionVO infoSession, IEnumerable<T> collection, List<String> llFiltros, string lsCondicion)
        {
            ManejadorSIB manejador = (ManejadorSIB)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorSIB), infoSession);
            return manejador.DefinirFiltros<T>(collection, llFiltros, lsCondicion);
        }


        public IQueryable<String> ayudaMaestro(InfoSessionVO infoSession, string lsCadenaBusqueda, string lsTabla, string lsCodigo, string lsDescripcion, string lsTipo)
        {
            ManejadorSIB manejador = (ManejadorSIB)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorSIB), infoSession);
            return manejador.ayudaMaestro(lsCadenaBusqueda, lsTabla, lsCodigo, lsDescripcion, lsTipo);
        }

        public IQueryable<String> ayudaMaestroSib(InfoSessionVO infoSession, string lsCadenaBusqueda, string lsTabla, string lsCodigo, string lsDescripcion, string lsTipo)
        {
            ManejadorSIB manejador = (ManejadorSIB)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorSIB), infoSession);
            return manejador.ayudaMaestroSib(lsCadenaBusqueda, lsTabla, lsCodigo, lsDescripcion, lsTipo);
        }

        public IQueryable<String> ayudaMaestroCondicion(InfoSessionVO infoSession, string lsCadenaBusqueda, string lsTabla, string lsCodigo, string lsDescripcion, string lsTipo, string lsCondicion)
        {
            ManejadorSIB manejador = (ManejadorSIB)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorSIB), infoSession);
            return manejador.ayudaMaestroCondicion(lsCadenaBusqueda, lsTabla, lsCodigo, lsDescripcion, lsTipo, lsCondicion);
        }

        public IQueryable<String> ayudaProcedimiento(InfoSessionVO infoSession, string lsProcedimiento, string lsCadenaBusqueda)
        {
            ManejadorSIB manejador = (ManejadorSIB)BusinessHandlerFactory.CreateBusinessHandler(typeof(ManejadorSIB), infoSession);
            return manejador.ayudaProcedimiento(lsProcedimiento, lsCadenaBusqueda);
        }

    }
}
