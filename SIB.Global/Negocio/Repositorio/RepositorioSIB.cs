﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using SIB.Global.Presentacion;
using SIB.Global.Dominio;
using PCD_Infraestructura.Transaction;

namespace SIB.Global.Negocio.Repositorio
{
    public class RepositorioSIB : AbstractRepository
    {
        public enum tiposPemisos
        {
            SELECT = 1,
            UPDATE = 2,
            INSERT = 8,
            DELETE = 10,
            EXECUTE = 20
        }

        public void validarConexion()
        {
            Transaction.ExecuteCommand("Select 1");
        }

        public void actualizarPassword(string usuario, string nuevoPassword, string antiguoPassword)
        {
            Transaction.ExecuteCommand("SP_UptClave '" + usuario + "','" + antiguoPassword + "','" + nuevoPassword + "'");
        }

        public void registrarEvento(a_auditoria_proceso loAuditoriaProceso)
        {
            Transaction.GetTable<a_auditoria_proceso>().InsertOnSubmit(loAuditoriaProceso);
        }

        public Boolean consultarPermisoBD(String lsTabla, String lsTipo)
        {
            string[] lsTablaSis;
            lsTablaSis = lsTabla.Split('-');
            //Int32 permiso = Transaction.ExecuteQuery<Int32>("IF PERMISSIONS(OBJECT_ID(N'" + lsTablaSis[0] + "'))&" + lsTipoPermiso + " SELECT 1 AS permiso ELSE SELECT 0 AS permiso").Single<Int32>();
            Int32 permiso = Transaction.ExecuteQuery<Int32>("pa_GetPermisosBaseDatos '" + lsTablaSis[0] + "'," + lsTipo + "").Single<Int32>();
            if (permiso == 1)
                return true;
            else
                return false;
        }

        public Boolean consultarPermisoSistema(String lsTabla, String lsTipo, String lsUsuario)
        {
            var consultaPermisos = from gm in Transaction.GetTable<a_grupo_menu>()
                                   from u in Transaction.GetTable<a_usuario>()
                                   from m in Transaction.GetTable<a_menu>()
                                   where
                                   gm.codigo_grupo_usuario == u.codigo_grupo_usuario &
                                   gm.codigo_menu == m.codigo_menu &
                                   u.login == lsUsuario &
                                   m.tabla == lsTabla
                                   select gm;

            var registro = consultaPermisos.Single<a_grupo_menu>();

            if (tiposPemisos.SELECT.ToString() == lsTipo)
                return registro.consultar == 1;
            else if (tiposPemisos.UPDATE.ToString() == lsTipo)
                return registro.modificar == 1;
            else if (tiposPemisos.INSERT.ToString() == lsTipo)
                return registro.crear == 1;
            else if (tiposPemisos.DELETE.ToString() == lsTipo)
                return registro.eliminar == 1;
            else
                return registro.consultar == 1;
        }

        public a_grupo_menu consultarPermisoMenu(String lsTabla, String lsUsuario)
        {
            a_grupo_menu permisoMenu = new a_grupo_menu();
            var permisos = from gm in Transaction.GetTable<a_grupo_menu>()
                           from u in Transaction.GetTable<a_usuario>()
                           from m in Transaction.GetTable<a_menu>()
                           where
                           gm.codigo_grupo_usuario == u.codigo_grupo_usuario &
                           gm.codigo_menu == m.codigo_menu &
                           u.login == lsUsuario &
                           m.tabla == lsTabla
                           select gm;
            foreach (a_grupo_menu grupMenu in permisos)
            {
                permisoMenu = grupMenu;
            }
            return permisoMenu;
        }

        public List<Hashtable> consultarMenu(String lsUsuario, int liPadre)
        {
            var menus = from gm in Transaction.GetTable<a_grupo_menu>()
                        from u in Transaction.GetTable<a_usuario>()
                        from m in Transaction.GetTable<a_menu>()
                        where
                             gm.codigo_grupo_usuario == u.codigo_grupo_usuario &
                             gm.codigo_menu == m.codigo_menu &
                             gm.id_padre == liPadre &
                             u.login == lsUsuario &
                             m.visualizar == 'S' &
                             m.estado == 'A'

                        orderby gm.orden
                        select new
                        {
                            codigo_grupo_menu = gm.codigo_grupo_menu.ToString(),
                            id_padre = gm.id_padre.ToString(),
                            menu = m.menu,
                            ruta = m.ruta,
                            frame = m.frame,
                            codigo_menu = gm.codigo_menu
                        };

            List<Hashtable> lista = new List<Hashtable>();
            foreach (var dat in menus)
            {
                Hashtable menu = new Hashtable();
                menu["id_padre"] = dat.id_padre;
                menu["codigo_grupo_menu"] = dat.codigo_grupo_menu;
                menu["menu"] = dat.menu;
                menu["ruta"] = dat.ruta;
                menu["frame"] = dat.frame;
                menu["codigo_menu"] = dat.codigo_menu;
                lista.Add(menu);
            }
            return lista;
        }

        public List<Hashtable> consultarMenuGrupo(int liPadre, int liGrupoUsuario)
        {
            var menus = from gm in Transaction.GetTable<a_grupo_menu>()
                        from m in Transaction.GetTable<a_menu>()
                        where
                             gm.codigo_grupo_usuario == liGrupoUsuario &
                             gm.codigo_menu == m.codigo_menu &
                             gm.id_padre == liPadre &
                             m.visualizar == 'S' &
                             m.estado == 'A'

                        orderby gm.orden
                        select new
                        {
                            codigo_grupo_menu = gm.codigo_grupo_menu.ToString(),
                            id_padre = gm.id_padre.ToString(),
                            menu = m.menu,
                            ruta = m.ruta,
                            frame = m.frame,
                            codigo_menu = gm.codigo_menu
                        };

            List<Hashtable> lista = new List<Hashtable>();
            foreach (var dat in menus)
            {
                Hashtable menu = new Hashtable();
                menu["id_padre"] = dat.id_padre;
                menu["codigo_grupo_menu"] = dat.codigo_grupo_menu;
                menu["menu"] = dat.menu;
                menu["ruta"] = dat.ruta;
                menu["frame"] = dat.frame;
                menu["codigo_menu"] = dat.codigo_menu;
                lista.Add(menu);
            }
            return lista;
        }
     

        public IQueryable<T> consultarTabla<T>(IEnumerable<T> collection, List<String> llFiltros, string lsCondicion)
        {
            string filtro = "";
            if (lsCondicion != "")
                filtro = lsCondicion;
            else
                filtro = " 1=1 ";
            Type t = typeof(T);
            PropertyInfo[] pia = t.GetProperties();

            foreach (T item in collection)
            {
                String[] laFiltros = llFiltros.ToArray();
                int i = 0;
                foreach (PropertyInfo pi in pia)
                {
                    if (!pi.PropertyType.Name.Equals("EntitySet`1") && !pi.PropertyType.Name.Substring(0, 2).Equals("m_"))
                    {
                        string lsFiltro = " = ";
                        try
                        {
                            if (!laFiltros[i].Equals(""))
                                lsFiltro = laFiltros[i];
                        }
                        catch { /*Si no se puede asignar que un =*/ }

                        bool fechaNula = false;
                        try
                        {
                            DateTime fecha = ((System.DateTime)pi.GetValue(item, null));
                            if (fecha.Year == 1) fechaNula = true;
                        }
                        catch { fechaNula = false; }

                        String lsComodin = "''";
                        if (pi.PropertyType == typeof(Decimal) || pi.PropertyType == typeof(Int16) || pi.PropertyType == typeof(Int32) || pi.PropertyType == typeof(Int64))
                        {
                            lsComodin = "";
                        }
                        if (pi.GetValue(item, null) != null && !pi.GetValue(item, null).Equals("") && !pi.GetValue(item, null).Equals('\0') && !pi.GetValue(item, null).Equals(0) && pi.GetValue(item, null).ToString() != "0" && !fechaNula)
                        {
                            String lsValor = "";

                            if (pi.PropertyType == typeof(DateTime))
                            {
                                if (lsFiltro.ToLower().Equals("like"))
                                    lsValor = lsFiltro + " " + lsComodin + "%" + Convert.ToDateTime(pi.GetValue(item, null).ToString()).ToShortDateString() + "%" + lsComodin + " ";
                                else
                                    lsValor = lsFiltro + " " + lsComodin + Convert.ToDateTime(pi.GetValue(item, null).ToString()).ToShortDateString() + lsComodin + " ";
                            }
                            else
                            {
                                if (lsFiltro.ToLower().Equals("like"))
                                    lsValor = lsFiltro + " " + lsComodin + "%" + pi.GetValue(item, null) + "%" + lsComodin + " ";
                                else
                                    lsValor = lsFiltro + " " + lsComodin + pi.GetValue(item, null) + lsComodin + " ";
                            }
                            filtro += " and " + pi.Name + " " + lsValor;
                        }
                        i++;
                    }
                }
            }
            var registrosEncontrados = Transaction.ExecuteQuery<T>("pa_consultar '" + t.Name + "', '*', '" + filtro + "'");
            return registrosEncontrados.AsQueryable<T>();
        }

        public string DefinirFiltros<T>(IEnumerable<T> collection, List<String> llFiltros, string lsCondicion)
        {
            string filtro = "";
            if (lsCondicion != "")
                filtro = lsCondicion;
            else
                filtro = " 1=1 ";
            Type t = typeof(T);
            PropertyInfo[] pia = t.GetProperties();

            foreach (T item in collection)
            {
                String[] laFiltros = llFiltros.ToArray();
                int i = 0;
                foreach (PropertyInfo pi in pia)
                {
                    if (!pi.PropertyType.Name.Equals("EntitySet`1") && !pi.PropertyType.Name.Substring(0, 2).Equals("m_"))
                    {
                        string lsFiltro = " = ";
                        try
                        {
                            if (!laFiltros[i].Equals(""))
                                lsFiltro = laFiltros[i];
                        }
                        catch { /*Si no se puede asignar que un =*/ }

                        bool fechaNula = false;
                        try
                        {
                            DateTime fecha = ((System.DateTime)pi.GetValue(item, null));
                            if (fecha.Year == 1) fechaNula = true;
                        }
                        catch { fechaNula = false; }

                        String lsComodin = "'";
                        if (pi.PropertyType == typeof(Decimal) || pi.PropertyType == typeof(Int16) || pi.PropertyType == typeof(Int32) || pi.PropertyType == typeof(Int64))
                        {
                            lsComodin = "";
                        }
                        if (pi.GetValue(item, null) != null && !pi.GetValue(item, null).Equals("") && !pi.GetValue(item, null).Equals('\0') && !pi.GetValue(item, null).Equals(0) && pi.GetValue(item, null).ToString() != "0" && !fechaNula)
                        {
                            String lsValor = "";

                            if (pi.PropertyType == typeof(DateTime))
                            {
                                if (lsFiltro.ToLower().Equals("like"))
                                    lsValor = lsFiltro + " " + lsComodin + "%" + Convert.ToDateTime(pi.GetValue(item, null).ToString()).ToShortDateString() + "%" + lsComodin + " ";
                                else
                                    lsValor = lsFiltro + " " + lsComodin + Convert.ToDateTime(pi.GetValue(item, null).ToString()).ToShortDateString() + lsComodin + " ";
                            }
                            else
                            {
                                if (lsFiltro.ToLower().Equals("like"))
                                    lsValor = lsFiltro + " " + lsComodin + "%" + pi.GetValue(item, null) + "%" + lsComodin + " ";
                                else
                                    lsValor = lsFiltro + " " + lsComodin + pi.GetValue(item, null) + lsComodin + " ";
                            }
                            filtro += " and " + pi.Name + " " + lsValor;
                        }
                        i++;
                    }
                }
            }
            return filtro;
        }

        public IQueryable<String> ayudaMaestro(string lsCadenaBusqueda, string lsTabla, string lsCodigo, string lsDescripcion, string lsTipo)
        {
            String lsColumnas = "convert(varchar," + lsCodigo + ") + '' - '' + " + lsDescripcion;
            if (lsTipo == "C") { lsColumnas = lsCodigo; }
            if (lsTipo == "D") { lsColumnas = lsDescripcion; }
            String lsFiltro = lsCodigo + " like ''%" + lsCadenaBusqueda + "%'' or " + lsDescripcion + " like ''%" + lsCadenaBusqueda + "%'' ";

            return Transaction.ExecuteQuery<String>("pa_consultar '" + lsTabla + "','" + lsColumnas + "','" + lsFiltro + "'").AsQueryable<String>();
        }

        public IQueryable<String> ayudaMaestroSib(string lsCadenaBusqueda, string lsTabla, string lsCodigo, string lsDescripcion, string lsTipo)
        {
            String lsColumnas = "convert(varchar," + lsCodigo + ") + '' - '' + " + lsDescripcion;
            if (lsTipo == "C") { lsColumnas = lsCodigo; }
            if (lsTipo == "D") { lsColumnas = lsDescripcion; }
            String lsFiltro = lsCodigo + " like ''%" + lsCadenaBusqueda + "%'' or " + lsDescripcion + " like ''%" + lsCadenaBusqueda + "%'' ";

            return Transaction.ExecuteQuery<String>("pa_consultar_sib '" + lsTabla + "','" + lsColumnas + "','" + lsFiltro + "'").AsQueryable<String>();
        }

        public IQueryable<String> ayudaMaestroCondicion(string lsCadenaBusqueda, string lsTabla, string lsCodigo, string lsDescripcion, string lsTipo, string lsCondicion)
        {
            String lsColumnas = "convert(varchar," + lsCodigo + ") + '' - '' + " + lsDescripcion;
            if (lsTipo == "C") { lsColumnas = lsCodigo; }
            if (lsTipo == "D") { lsColumnas = lsDescripcion; }
            String lsFiltro = "(" + lsCodigo + " like ''%" + lsCadenaBusqueda + "%'' or " + lsDescripcion + " like ''%" + lsCadenaBusqueda + "%'' )" + lsCondicion;

            return Transaction.ExecuteQuery<String>("pa_consultar '" + lsTabla + "','" + lsColumnas + "','" + lsFiltro + "'").AsQueryable<String>();
        }

        public IQueryable<String> ayudaProcedimiento(string lsProcedimiento, string lsCadenaBusqueda)
        {
            String lsFiltro = " LIKE ''%" + lsCadenaBusqueda + "%''";

            return Transaction.ExecuteQuery<String>(lsProcedimiento + " '" + lsFiltro + "'").AsQueryable<String>();
        }


    }
}
