﻿/// <summary>
/// Enumerador para los botones existentes en el user control que gestiona los botones del aplicativo
/// </summary>
public enum EnumBotones
{
    Crear,
    Actualizar,
    Buscar,
    Preliminar,
    Aprobar,
    Desaprobar,
    Rechazar,
    Reservar,
    SolicitudEliminacion,
    Cargue,
    Excel,
    Pdf,
    Facturar,
    ModificarFacturar,
    Detalle,
    Ninguno,
    Auditoria, //ajuste front-end
    Modificar, //20201124
    Salir //20201124
}
