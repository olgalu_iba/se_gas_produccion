﻿/// <summary>
/// Enumerador para los tipos de toastr que se pueden utilizar  
/// </summary>
public enum EnumTypeToastr
{
    Info,
    Success,
    Warning,
    Error
}