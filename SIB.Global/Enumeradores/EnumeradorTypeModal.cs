﻿/// <summary>
/// Enumerador para las acciones que pueden realizar los modals 
/// </summary>
public enum EnumTypeModal
{
    Abrir,
    Cerrar
}