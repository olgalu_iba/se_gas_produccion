﻿/// <summary>
/// Enumerador para los botones existentes en el user control que gestiona los botones exclusivos de las subastas 
/// </summary>
public enum EnumBotonesSubasta
{
    PublicarPostura,
    DecInf,
    DecPre,
    DecMan,
    CargaVenta,
    Oferta,
    EnviarMensaje,
    MisMensajes,
    Contrato,
    Suspender,
    Reactivar,
    Rueda,
    Punto,
    Ninguno
}
