﻿/*
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 * Copyright (C) 2019 Bolsa Mercantil de Colombia, Todos los Derechos Reservados
 * Archivo:               MasterPage.cs
 * Tipo:                  Clase principal
 * Autor:                 Oscar Enrique Piñeros
 * Fecha creación:        2019 Julio 05
 * Fecha modificación:    2019 Septiebre 07
 * Propósito:             Clase que permite administrar todas las operaciones que realiza la plantilla  
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.UI;
using Segas.Web.Elements;
using SIB.Global.Presentacion;

public partial class PlantillaPrincipal : MasterPage
{
    #region Propiedades

    private InfoSessionVO goInfo;

    private List<Hashtable> MenuElementos { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public string Titulo
    {
        get { return hTitulo.InnerText; }
        set { hTitulo.InnerText = value; }
    }

    /// <summary>
    /// 
    /// </summary>
    public string DescripcionPagina
    {
        get { return aDescripcion.InnerText; }
        set { aDescripcion.InnerText = value; }
    }

    #endregion Propiedades

    /// <summary>
    ///El evento de carga de la página se activa antes del evento de carga para cualquier control
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null)
            {
                Response.Redirect("~/login.aspx");
                return;
            }
            if (IsPostBack) return;
            //Se carga el menu
            CargarMenu();
        }
        catch (Exception ex)
        {
            Toastr.Error(this, ex.Message);
        }
    }

    /// <summary>
    /// Carga el menú del usuario en sesión  
    /// </summary>
    protected void CargarMenu()
    {
        lblInitialProfile.Text = goInfo.nombre[0].ToString().ToUpper();
        lblInitial.Text = goInfo.nombre[0].ToString().ToUpper();
        lblUsuario.Text = goInfo.Usuario;

        var builder = new StringBuilder();
        builder.Append($"{goInfo.nombre.ToUpper()} <br/>");
        builder.Append($"{Session["NomGrupoUsuarioAdc"].ToString().ToUpper()} <br/>");//20210728 viaucializacion
        if (Session["cod_comisionista"].ToString() != "0")
            builder.Append($"{Session["NomOperador"].ToString().ToUpper()} <br/>");
        builder.Append("Último ingreso:<br/>"); //20221212 texto 
        builder.Append($"{Session["UltimoIngreso"].ToString().ToUpper()}");
        lblUsuario1.Text = builder.ToString();
        //20210728 viaucializacion
        var builder1 = new StringBuilder();
        builder1.Append($"<b>{goInfo.nombre.ToUpper()}</b> <br/>");
        builder1.Append($"{Session["NomGrupoUsuarioAdc"].ToString().ToUpper()} <br/>");
        if (Session["cod_comisionista"].ToString() != "0")
            builder1.Append($"{Session["NomOperador"].ToString().ToUpper()} <br/>");
        builder1.Append("Último ingreso:<br/>"); //20221212 texto 
        builder1.Append($"{Session["UltimoIngreso"].ToString().ToUpper()}");
        lblUsuario2.Text = builder1.ToString();
        //20210728 fin viaucializacion
        if (IsPostBack) return;

        MenuElementos = DelegadaBase.Servicios.consultarMenu(goInfo, 0);

        var sb = new StringBuilder();

        foreach (Hashtable opcion in MenuElementos)
        {
            List<Hashtable> listaSubmenus = DelegadaBase.Servicios.consultarMenu(goInfo, Convert.ToInt32(opcion["codigo_menu"]));
            sb.Append("<li class=\"kt-menu__item  kt-menu__item--submenu kt-menu__item--rel\" data-ktmenu-submenu-toggle=\"click\" aria-haspopup=\"true\"><a href = \"javascript:;\" class=\"kt-menu__link kt-menu__toggle\"><span class=\"kt-menu__link-text\">" + opcion["menu"] + "</span><i class=\"kt-menu__ver-arrow la la-angle-right\"></i></a>");
            sb.Append("<div class=\"kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left\">");
            sb.Append("<ul class=\"kt-menu__subnav\">");
            foreach (Hashtable opcion1 in listaSubmenus)
            {
                if (opcion1["codigo_menu"].ToString().Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idMosRegCont", CultureInfo.CurrentCulture)?.ToString()) ||
                    opcion1["codigo_menu"].ToString().Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idMosRepoEjecdeCont", CultureInfo.CurrentCulture)?.ToString()) ||
                    opcion1["codigo_menu"].ToString().Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idMosPTDVFCIDVF", CultureInfo.CurrentCulture)?.ToString()) ||
                    opcion1["codigo_menu"].ToString().Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idMosCap", CultureInfo.CurrentCulture)?.ToString()))
                {
                    var uri = $"{Page.ResolveUrl(opcion1["ruta"].ToString())}?idInf={Convert.ToInt32(opcion1["codigo_menu"])}";
                    sb.Append("<li class=\"kt-menu__item\"aria-haspopup=\"true\"><a href =\"" + uri +
                              "\" class=\"kt-menu__link\"><i class=\"kt-menu__link-bullet kt-menu__link-bullet--dot\"><span></span></i><span class=\"kt-menu__link-text\">" +
                              opcion1["menu"] + "</span></a></li>");
                }
                else
                {
                    //20200727 ajsute GAP
                    if (opcion1["codigo_menu"].ToString().Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idParametros", CultureInfo.CurrentCulture)?.ToString()))
                    {
                        var uri = string.Empty;
                        uri = Page.ResolveUrl($"~/WebForms/Parametros/frm_parametros.aspx?idParametros={Convert.ToInt32(opcion1["codigo_menu"])}");
                        sb.Append("<li class=\"kt-menu__item\"aria-haspopup=\"true\"><a href =\"" + uri +
                              "\" class=\"kt-menu__link\"><i class=\"kt-menu__link-bullet kt-menu__link-bullet--dot\"><span></span></i><span class=\"kt-menu__link-text\">" +
                              opcion1["menu"] + "</span></a></li>");
                    }
                    else
                    {
                        List<Hashtable> listaSubmenus1 = DelegadaBase.Servicios.consultarMenu(goInfo, Convert.ToInt32(opcion1["codigo_menu"]));
                        if (listaSubmenus1.Count == 0)
                        {
                            sb.Append("<li class=\"kt-menu__item\"aria-haspopup=\"true\"><a href =\"" + Page.ResolveUrl(opcion1["ruta"].ToString()) +
                                      "\" class=\"kt-menu__link\"><i class=\"kt-menu__link-bullet kt-menu__link-bullet--dot\"><span></span></i><span class=\"kt-menu__link-text\">" +
                                      opcion1["menu"] + "</span></a></li>");
                        }
                        else
                        {
                            sb.Append("<li class=\"kt-menu__item  kt-menu__item--submenu\" data-ktmenu-submenu-toggle=\"hover\" aria-haspopup=\"true\"><a href = \"" +
                                Page.ResolveUrl(opcion1["ruta"].ToString()) + "\" class=\"kt-menu__link kt-menu__toggle\"><i class=\"kt-menu__link-bullet kt-menu__link-bullet--dot\"><span></span></i><span class=\"kt-menu__link-text\">" +
                                opcion1["menu"] + "</span><i class=\"kt-menu__hor-arrow la la-angle-right\"></i><i class=\"kt-menu__ver-arrow la la-angle-right\"></i></a>");
                            sb.Append("<div class=\"kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--right\" style=\"top: 10px; position: fixed;\">");
                            sb.Append("<ul class=\"kt-menu__subnav\">");
                            foreach (Hashtable opcion2 in listaSubmenus1)
                            {
                                var id = opcion1["codigo_menu"].ToString(); //20200727 ajsute front-end
                                var documntos = HttpContext.GetGlobalResourceObject("App_Configuration", "idDocumentos", CultureInfo.CurrentCulture)?.ToString();
                                var consultas = HttpContext.GetGlobalResourceObject("App_Configuration", "idConsultas", CultureInfo.CurrentCulture)?.ToString();
                                if (id.Equals(documntos) || id.Equals(consultas))
                                {
                                    var uri = string.Empty;
                                    if (id.Equals(consultas))
                                        uri = Page.ResolveUrl($"~/WebForms/Informes/frm_informe.aspx?idInf={Convert.ToInt32(opcion2["codigo_menu"])}");
                                    if (id.Equals(documntos))
                                        uri = Page.ResolveUrl($"~/WebForms/Documentos/frm_documento.aspx?idDoc={Convert.ToInt32(opcion2["codigo_menu"])}");
                                    sb.Append($"<li class=\"kt-menu__item\" aria-haspopup=\"true\"><a href=\"{uri}\" class=\"kt-menu__link \"><i class=\"kt-menu__link-icon flaticon2-document\"></i><span class=\"kt-menu__link-text\"> {opcion2["menu"]}</span></a></li>");
                                }
                                else
                                {
                                    sb.Append($"<li class=\"kt-menu__item\" aria-haspopup=\"true\"><a href=\"{Page.ResolveUrl(opcion2["ruta"].ToString())} \" class=\"kt-menu__link \"><i class=\"kt-menu__link-icon flaticon2-paper\"></i><span class=\"kt-menu__link-text\">{opcion2["menu"]}</span></a></li>");
                                }
                            }

                            sb.Append("</ul>");
                            sb.Append("</div>");
                        }
                    }
                }
            }

            sb.Append("</ul>");
            sb.Append("</div>");
            sb.Append("</li>");
        }

        ltlMenu.Text = sb.ToString();
    }

    /// <summary>
    /// Redirige al perfil del usuario 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RedirigirPerfil(object sender, EventArgs e)
    {
        Response.Redirect("~/WebForms/Administracion/frm_perfilUsuario.aspx");
    }

    /// <summary>
    /// Cierra la sesión 
    /// </summary>
    protected void btnCerrarSesion_Click(object sender, EventArgs e)
    {
        Session.RemoveAll();
        Session.Abandon();
        Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
        Response.AppendHeader("Cache-Control", "no-store");
        Response.Redirect(Page.ResolveUrl("~"));
    }

}
