﻿/*
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 * Copyright (C) 2019 Bolsa Mercantil de Colombia, Todos los Derechos Reservados
 * Archivo:               WucSubastaExtender.ascx.cs
 * Tipo:                  Clase Web User Control
 * Autor:                 Oscar Piñeros
 * Fecha creación:        2019 Agosto 25
 * Fecha modificación:    
 * Propósito:             Clase que permite administrar los botones de una subasta
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 */

using System;
using System.Web.UI;

namespace UsersControls.Subastas
{
    // ReSharper disable once InconsistentNaming
    public partial class WucSubastaExtender : UserControl
    {
        #region Propiedades

        /// <summary>
        /// Contiene los botones enviados por la funcionalidad
        /// </summary>
        private EnumBotonesSubasta[] Botones
        {
            get { return (EnumBotonesSubasta[])ViewState["BotonesSubasta"]; }
            set { ViewState["BotonesSubasta"] = value; }
        }

        #endregion Propiedades

        #region Eventos

        /// <summary>
        /// EventHandler para el botón de publicar posturas
        /// </summary>
        public event EventHandler PublicarPosturaOnclick;

        /// <summary>
        /// EventHandler para el botón de Plano Dec Inf
        /// </summary>
        public event EventHandler DecInfOnclick;

        /// <summary>
        /// EventHandler para el botón de Plano Dec Pre
        /// </summary>
        public event EventHandler DecPreOnclick;

        /// <summary>
        /// EventHandler para el botón de Ing Dec Inf
        /// </summary>
        public event EventHandler DecManOnclick;

        /// <summary>
        /// EventHandler para el botón de cargar posturas de venta
        /// </summary>
        public event EventHandler CargaVentaOnclick;

        /// <summary>
        /// EventHandler para el botón de enviar mensaje
        /// </summary>
        public event EventHandler EnviarMensajeOnclick;

        /// <summary>
        /// EventHandler para el botón de mis mensajes
        /// </summary>
        public event EventHandler MisMensajesOnclick;

        /// <summary>
        /// EventHandler para el botón suspender
        /// </summary>
        public event EventHandler SuspenderOnclick;

        /// <summary>
        /// EventHandler para el botón reactivar
        /// </summary>
        public event EventHandler ReactivarOnclick;

        /// <summary>
        /// EventHandler para el botón oferta
        /// </summary>
        public event EventHandler OfertaOnclick;

        /// <summary>
        /// EventHandler para el botón contrato
        /// </summary>
        public event EventHandler ContratoOnclick;

        #endregion Eventos

        /// <summary>
        /// Evento donde se cargarán los valores para su página 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) EstablecerPermisosSistema();
        }

        /// <summary>
        /// Inicializa el control
        /// </summary>
        /// <param name="botones"></param>
        public void Inicializar(EnumBotonesSubasta[] botones = null)
        {
            Botones = botones;
        }

        /// <summary>
        /// Evento que controla el botón de posturas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PublicarPostura(object sender, EventArgs e)
        {
            PublicarPosturaOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento que controla el botón suspender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SuspenderPagina(object sender, EventArgs e)
        {
            SuspenderOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento que controla el botón Plano Dec Inf
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DecInfSub(object sender, EventArgs e)
        {
            DecInfOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento que controla el botón Ing Dec Inf
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DecManSub(object sender, EventArgs e)
        {
            DecManOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento que controla el botón Plano Dec Pre
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DecPreSub(object sender, EventArgs e)
        {
            DecPreOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento que controla el botón de cargar posturas de venta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CargaVentaPagina(object sender, EventArgs e)
        {
            CargaVentaOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento que controla el botón de reactivar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ReactivarPagina(object sender, EventArgs e)
        {
            ReactivarOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento que controla el botón de oferta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OfertaSub(object sender, EventArgs e)
        {
            OfertaOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento que controla el botón de mis mensajes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void MisMensajesSub(object sender, EventArgs e)
        {
            MisMensajesOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento que controla el botón de enviar mis mensajes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnviarMensajeSub(object sender, EventArgs e)
        {
            EnviarMensajeOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento que controla el botón de contratos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ContratoSub(object sender, EventArgs e)
        {
            ContratoOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Establece los permisos de los botones
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            try
            {
                lbtCarga.Visible = false;
                lbtDecInf.Visible = false;
                lbtDecPre.Visible = false;
                lbtDecMan.Visible = false;
                lbtSuspender.Visible = false;
                lbtReactivar.Visible = false;
                lbtPublicarPostura.Visible = false;
                lbtOferta.Visible = false;
                lblMisMensajes.Visible = false;
                lblEnviarMensaje.Visible = false;
                lbtContrato.Visible = false;

                //Botones seleccionados en la funcionalidad
                if (Botones != null && Botones.Length > 0)
                    EnableButtons(Botones);
            }
            catch (Exception)
            {
                throw new Exception("Debe seleccionar la pantalla o los botones a mostrar");
            }
        }

        /// <summary>
        /// Cambia los botones visibles
        /// </summary>
        /// <param name="botones"></param>
        public void SwitchOnButton(params EnumBotonesSubasta[] botones)
        {
            Botones = botones;
            EstablecerPermisosSistema();
        }

        /// <summary>
        /// Activa los botones
        /// </summary>
        /// <param name="botones"></param>
        public void EnableButtons(params EnumBotonesSubasta[] botones)
        {
            foreach (EnumBotonesSubasta t in botones)
            {
                switch (t)
                {
                    case EnumBotonesSubasta.Oferta:
                        lbtOferta.Visible = true;
                        break;
                    case EnumBotonesSubasta.MisMensajes:
                        lblMisMensajes.Visible = true;
                        break;
                    case EnumBotonesSubasta.EnviarMensaje:
                        lblEnviarMensaje.Visible = true;
                        break;
                    case EnumBotonesSubasta.Contrato:
                        lbtContrato.Visible = true;
                        break;
                    case EnumBotonesSubasta.CargaVenta:
                        lbtCarga.Visible = true;
                        break;
                    case EnumBotonesSubasta.DecInf:
                        lbtDecInf.Visible = true;
                        break;
                    case EnumBotonesSubasta.DecPre:
                        lbtDecPre.Visible = true;
                        break;
                    case EnumBotonesSubasta.DecMan:
                        lbtDecMan.Visible = true;
                        break;
                    case EnumBotonesSubasta.Suspender:
                        lbtSuspender.Visible = true;
                        break;
                    case EnumBotonesSubasta.Reactivar:
                        lbtReactivar.Visible = true;
                        break;
                    case EnumBotonesSubasta.PublicarPostura:
                        lbtPublicarPostura.Visible = true;
                        break;
                    case EnumBotonesSubasta.Rueda:
                        break;
                    case EnumBotonesSubasta.Ninguno:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

    }
}