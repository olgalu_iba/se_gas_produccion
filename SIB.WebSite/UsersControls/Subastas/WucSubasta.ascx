﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucSubasta.ascx.cs" Inherits="UsersControls.Subastas.WucSubasta" %>

<div class="kt-portlet__head-toolbar">
    <div class="btn-group show" role="group">
        <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:LinkButton ID="lbtPublicarPostura" CssClass="btn btn-success btn-pill btn-sm" Visible="False" OnClick="PublicarPostura" runat="server">
                    <i class="flaticon2-add-1"></i>
                    Publicar postura
                </asp:LinkButton>
                <asp:DropDownList placeholder="Rueda" ID="ddlRueda" CssClass="selectpicker" OnSelectedIndexChanged="ddlRueda_SelectedIndexChanged" AutoPostBack="True" Visible="false" runat="server" />
                <asp:DropDownList placeholder="Punto" ID="ddlPunto" CssClass="selectpicker" data-live-search="true" OnSelectedIndexChanged="ddlPunto_SelectedIndexChanged" AutoPostBack="True" Visible="false" runat="server">
                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                </asp:DropDownList>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="ddlRueda" />
                <asp:PostBackTrigger ControlID="ddlPunto" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</div>
