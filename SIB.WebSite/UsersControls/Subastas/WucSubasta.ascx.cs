﻿/*
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 * Copyright (C) 2019 Bolsa Mercantil de Colombia, Todos los Derechos Reservados
 * Archivo:               WucSubasta.ascx.cs
 * Tipo:                  Clase Web User Control
 * Autor:                 Oscar Piñeros
 * Fecha creación:        2019 Agosto 25
 * Fecha modificación:    
 * Propósito:             Clase que permite administrar los botones de una subasta
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 */

using System;
using System.Collections;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UsersControls.Subastas
{
    // ReSharper disable once InconsistentNaming
    public partial class WucSubasta : UserControl
    {
        #region Propiedades

        /// <summary>
        /// Contiene los botones enviados por la funcionalidad
        /// </summary>
        private EnumBotonesSubasta[] Botones
        {
            get { return (EnumBotonesSubasta[])ViewState["BotonesSubasta"]; }
            set { ViewState["BotonesSubasta"] = value; }
        }

        /// <summary>
        /// Propiedad que contiene la lista de ruedas abiertas de la subasta
        /// </summary>
        public IEnumerable ListaRueda
        {
            get { return ddlRueda.Items; }
            set
            {
                ddlRueda.Items.Clear();
                var lItem = new ListItem { Value = @"0", Text = @"Seleccione Rueda" };
                ddlRueda.Items.Add(lItem);
                ddlRueda.Items.AddRange(value.Cast<ListItem>().ToArray());
            }
        }

        /// <summary>
        /// Propiedad que permite cambiar el estado de visibilidad de la lista de ruedas 
        /// </summary>
        /// <returns></returns>
        public bool EditarRueda
        {
            get { return ddlRueda.Enabled; }
            set { ddlRueda.Enabled = value; }
        }

        /// <summary>
        /// Propiedad para obtener o asignar el valor de la rueda
        /// </summary>
        public string ValorRueda
        {
            get { return ddlRueda.SelectedValue; }
            set { ddlRueda.SelectedValue = value; }
        }

        /// <summary>
        /// Propiedad que contiene la lista de putos o tramos de la subasta
        /// </summary>
        public IEnumerable ListaPunto
        {
            get { return ddlPunto.Items; }
            set
            {
                ddlPunto.Items.Clear();
                ddlPunto.Items.AddRange(value.Cast<ListItem>().ToArray());
            }
        }

        /// <summary>
        /// Propiedad que permite cambiar el estado de visibilidad de la lista de puntos o tramos
        /// </summary>
        /// <returns></returns>
        public bool EditarPunto
        {
            get { return ddlPunto.Enabled; }
            set { ddlPunto.Enabled = value; }
        }

        /// <summary>
        /// Propiedad para obtener o asignar el valor de la lista de puntos o tramos
        /// </summary>
        public string ValorPunto
        {
            get { return ddlPunto.SelectedValue; }
            set { ddlPunto.SelectedValue = value; }
        }

        /// <summary>
        /// Propiedad que obtiene o establece el index de la lista de puntos o tramos
        /// </summary>
        public int IndexPunto
        {
            get { return ddlPunto.SelectedIndex; }
            set { ddlPunto.SelectedIndex = value; }
        }

        #endregion Propiedades

        #region Eventos

        /// <summary>
        /// EventHandler para el botón de publicar 
        /// </summary>
        public event EventHandler PublicarPosturaOnclick;

        /// <summary>
        /// EventHandler para la selección de una rueda
        /// </summary>
        public event EventHandler RuedaOnSelectedIndexChanged;

        /// <summary>
        /// EventHandler para la selección de un punto o tramo
        /// </summary>
        public event EventHandler PuntoOnSelectedIndexChanged;

        #endregion Eventos

        /// <summary>
        /// Evento donde se cargarán los valores para su página 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) EstablecerPermisosSistema();
        }

        /// <summary>
        /// Inicializa el control
        /// </summary>
        /// <param name="reactivarId"></param>
        /// <param name="botones"></param>
        /// <param name="suspenderId"></param>
        public void Inicializar(string suspenderId = null, string reactivarId = null, EnumBotonesSubasta[] botones = null)
        {
            Botones = botones;
        }

        /// <summary>
        /// Evento que controla el botón de posturas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PublicarPostura(object sender, EventArgs e)
        {
            PublicarPosturaOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento que controla el seleccionar en la lista de ruedas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlRueda_SelectedIndexChanged(object sender, EventArgs e)
        {
            RuedaOnSelectedIndexChanged?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento que controla el seleccionar en la lista de puntos o tramos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPunto_SelectedIndexChanged(object sender, EventArgs e)
        {
            PuntoOnSelectedIndexChanged?.Invoke(sender, e);
        }

        /// <summary>
        /// Establece los permisos de los botones
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            try
            {
                lbtPublicarPostura.Visible = false;
                ddlRueda.Visible = false;
                ddlPunto.Visible = false;

                //Botones seleccionados en la funcionalidad
                if (Botones != null && Botones.Length > 0)
                    EnableButtons(Botones);
            }
            catch (Exception)
            {
                throw new Exception("Debe seleccionar la pantalla o los botones a mostrar");
            }
        }

        /// <summary>
        /// Cambia los botones visibles
        /// </summary>
        /// <param name="botones"></param>
        public void SwitchOnButton(params EnumBotonesSubasta[] botones)
        {
            Botones = botones;
            EstablecerPermisosSistema();
        }

        /// <summary>
        /// Activa los botones
        /// </summary>
        /// <param name="botones"></param>
        private void EnableButtons(params EnumBotonesSubasta[] botones)
        {
            foreach (EnumBotonesSubasta t in botones)
            {
                switch (t)
                {
                    case EnumBotonesSubasta.PublicarPostura:
                        lbtPublicarPostura.Visible = true;
                        break;
                    case EnumBotonesSubasta.Rueda:
                        ddlRueda.Visible = true;
                        break;
                    case EnumBotonesSubasta.Punto:
                        ddlPunto.Visible = true;
                        break;
                    case EnumBotonesSubasta.Ninguno:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

    }
}