﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucSubastaExtender.ascx.cs" Inherits="UsersControls.Subastas.WucSubastaExtender" %>

<div class="kt-portlet__head-toolbar">
    <div class="btn-group show" role="group">
        <asp:UpdatePanel UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <asp:LinkButton ID="lbtPublicarPostura" CssClass="btn btn-success btn-pill btn-sm" Visible="False" OnClick="PublicarPostura" runat="server">
                    <i class="flaticon2-add-1"></i>
                    Publicar postura
                </asp:LinkButton>
                <span class="d-inline-block" data-toggle="tooltip" title="Ing Dec Inf">
                    <asp:LinkButton ID="lbtDecMan" CssClass="btn btn-outline-brand btn-square" Visible="False" OnClick="DecManSub" runat="server">
                    <i class="flaticon2-sheet"></i>
                    </asp:LinkButton>
                </span>
                <span data-toggle="kt-tooltip" title="Plano Dec Inf" data-placement="top">
                    <asp:LinkButton ID="lbtDecInf" CssClass="btn btn-outline-brand btn-square" Visible="False" OnClick="DecInfSub" runat="server">
                    <i class="flaticon-file-1"></i>
                    </asp:LinkButton>
                </span>
                <span data-toggle="kt-tooltip" title="Plano Dec Pre" data-placement="top">
                    <asp:LinkButton ID="lbtDecPre" CssClass="btn btn-outline-brand btn-square" Visible="False" OnClick="DecPreSub" runat="server">
                    <i class="flaticon-file-1"></i>
                    </asp:LinkButton>
                </span>
                <span data-toggle="kt-tooltip" title="Ofertas" data-placement="top">
                    <asp:LinkButton ID="lbtOferta" CssClass="btn btn-outline-brand btn-square" Visible="False" OnClick="OfertaSub" runat="server">
                    <i class="flaticon2-line-chart"></i>
                    </asp:LinkButton>
                </span>
                <span data-toggle="kt-tooltip" title="Enviar Mensaje" data-placement="top">
                    <asp:LinkButton ID="lblEnviarMensaje" CssClass="btn btn-outline-brand btn-square" Visible="False" OnClick="EnviarMensajeSub" runat="server">
                    <i class="flaticon2-paper-plane"></i>
                    </asp:LinkButton>
                </span>
                <span data-toggle="kt-tooltip" title="Mis Mensajes" data-placement="top">
                    <asp:LinkButton ID="lblMisMensajes" CssClass="btn btn-outline-brand btn-square" Visible="False" OnClick="MisMensajesSub" runat="server">
                    <i class="flaticon-comment"></i>
                    </asp:LinkButton>
                </span>
                <span data-toggle="kt-tooltip" title="Contratos" data-placement="top">
                    <asp:LinkButton ID="lbtContrato" CssClass="btn btn-outline-brand btn-square" Visible="False" OnClick="ContratoSub" runat="server">
                    <i class="flaticon2-writing"></i>
                    </asp:LinkButton>
                </span>
                <span data-toggle="kt-tooltip" title="Cargar posturas de venta" data-placement="top">
                    <asp:LinkButton ID="lbtCarga" CssClass="btn btn-outline-brand btn-square" Visible="False" OnClick="CargaVentaPagina" runat="server">
                        <i class="flaticon-file-1"></i>
                    </asp:LinkButton>
                </span>
                <span data-toggle="kt-tooltip" title="Suspender" data-placement="top">
                    <asp:LinkButton ID="lbtSuspender" CssClass="btn btn-outline-brand btn-square" Visible="False" OnClick="SuspenderPagina" runat="server">
                    <i class="flaticon-stopwatch"></i>
                    </asp:LinkButton>
                </span>
                <span data-toggle="kt-tooltip" title="Reactivar" data-placement="top">
                    <asp:LinkButton ID="lbtReactivar" CssClass="btn btn-outline-brand btn-square" Visible="False" OnClick="ReactivarPagina" runat="server">
                    <i class="flaticon-time-1"></i>
                    </asp:LinkButton>
                </span>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
