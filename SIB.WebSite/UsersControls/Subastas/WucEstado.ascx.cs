﻿/*
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 * Copyright (C) 2019 Bolsa Mercantil de Colombia, Todos los Derechos Reservados
 * Archivo:               WucEstado.ascx.cs
 * Tipo:                  Clase Web User Control
 * Autor:                 Oscar Piñeros
 * Fecha creación:        2019 Agosto 25
 * Fecha modificación:    
 * Propósito:             Clase que permite administrar los estados de una subasta
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 */

using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace UsersControls.Subastas
{
    public partial class WucEstado : UserControl
    {
        #region Propiedades

        /// <summary>
        /// Propiedad que controla el tab 1
        /// </summary>
        public LinkButton Pestania1
        {
            get { return lkbPestania1; }
        }

        /// <summary>
        /// Propiedad que controla el tab 2
        /// </summary>
        public LinkButton Pestania2
        {
            get { return lkbPestania2; }
        }

        /// <summary>
        /// Propiedad que controla el tab 3
        /// </summary>
        public LinkButton Pestania3
        {
            get { return lkbPestania3; }
        }

        /// <summary>
        /// Propiedad que controla el tab 4
        /// </summary>
        public LinkButton Pestania4
        {
            get { return lkbPestania4; }
        }

        /// <summary>
        /// Propiedad que controla el tab 5
        /// </summary>
        public LinkButton Pestania5
        {
            get { return lkbPestania5; }
        }

        /// <summary>
        /// Propiedad que controla el tab 6
        /// </summary>
        public LinkButton Pestania6
        {
            get { return lkbPestania6; }
        }

        /// <summary>
        /// Propiedad que controla el tab 7
        /// </summary>
        public LinkButton Pestania7
        {
            get { return lkbPestania7; }
        }


        /// <summary>
        /// Propiedad que controla el tab 8
        /// </summary>
        public LinkButton Pestania8
        {
            get { return lkbPestania8; }
        }

        /// <summary>
        /// Propiedad que controla la hora 1
        /// </summary>
        public Label Hora1
        {
            get { return lblHora1; }
        }

        /// <summary>
        /// Propiedad que controla la hora 2
        /// </summary>
        public Label Hora2
        {
            get { return lblHora2; }
        }

        /// <summary>
        /// Propiedad que controla la hora 3
        /// </summary>
        public Label Hora3
        {
            get { return lblHora3; }
        }

        /// <summary>
        /// Propiedad que controla la hora 4
        /// </summary>
        public Label Hora4
        {
            get { return lblHora4; }
        }

        /// <summary>
        /// Propiedad que controla la hora 5
        /// </summary>
        public Label Hora5
        {
            get { return lblHora5; }
        }

        /// <summary>
        /// Propiedad que controla la hora 6
        /// </summary>
        public Label Hora6
        {
            get { return lblHora6; }
        }

        /// <summary>
        /// Propiedad que controla la hora 7
        /// </summary>
        public Label Hora7
        {
            get { return lblHora7; }
        }

        /// <summary>
        /// Propiedad que controla la hora 8
        /// </summary>
        public Label Hora8
        {
            get { return lblHora8; }
        }

        /// <summary>
        /// Descripción para el tab número 1
        /// </summary>
        public HtmlGenericControl Descripcion1
        {
            get { return p_4_1; }
        }

        /// <summary>
        /// Descripción para el tab número 2
        /// </summary>
        public HtmlGenericControl Descripcion2
        {
            get { return p_4_2; }
        }

        /// <summary>
        /// Descripción para el tab número 3
        /// </summary>
        public HtmlGenericControl Descripcion3
        {
            get { return p_4_3; }
        }

        /// <summary>
        /// Descripción para el tab número 4
        /// </summary>
        public HtmlGenericControl Descripcion4
        {
            get { return p_4_4; }
        }

        /// <summary>
        /// Descripción para el tab número 5
        /// </summary>
        public HtmlGenericControl Descripcion5
        {
            get { return p_4_5; }
        }

        /// <summary>
        /// Descripción para el tab número 6
        /// </summary>
        public HtmlGenericControl Descripcion6
        {
            get { return p_4_6; }
        }

        /// <summary>
        /// Descripción para el tab número 7
        /// </summary>
        public HtmlGenericControl Descripcion7
        {
            get { return p_4_7; }
        }

        /// <summary>
        /// Descripción para el tab número 8
        /// </summary>
        public HtmlGenericControl Descripcion8
        {
            get { return p_4_8; }
        }

        /// <summary>
        /// 
        /// </summary>
        public HiddenField Time
        {
            get { return HdfTime; }
        }

        /// <summary>
        /// 
        /// </summary>
        public HiddenField TimeServer
        {
            get { return HdfTimeTemp; }
        }

        #endregion Propiedades

        /// <summary>
        /// Evento donde se cargarán los valores para su página 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}