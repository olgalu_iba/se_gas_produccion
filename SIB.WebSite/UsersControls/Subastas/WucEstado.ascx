﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucEstado.ascx.cs" Inherits="UsersControls.Subastas.WucEstado" %>

<asp:HiddenField ID="HdfTime" ClientIDMode="Static" runat="server" />
<asp:HiddenField ID="HdfTimeTemp" ClientIDMode="Static" runat="server" />

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-2x nav-tabs-line-success" role="tablist" runat="server">
            <li class="nav-item">
                <asp:Label ID="lblHora1" Visible="false" ClientIDMode="Static" runat="server"></asp:Label>
                <asp:LinkButton ID="lkbPestania1" Visible="false" class="nav-link" data-toggle="tab" href="#kt_tabs_4_1" role="tab" aria-selected="false" runat="server" />
            </li>

            <li class="nav-item">
                <asp:Label ID="lblHora2" Visible="false" runat="server"></asp:Label>
                <asp:LinkButton ID="lkbPestania2" Visible="false" CssClass="nav-link" data-toggle="tab" href="#kt_tabs_4_2" role="tab" aria-selected="false" runat="server" />
            </li>

            <li class="nav-item">
                <asp:Label ID="lblHora3" Visible="false" runat="server"></asp:Label>
                <asp:LinkButton ID="lkbPestania3" Visible="false" CssClass="nav-link" data-toggle="tab" href="#kt_tabs_4_3" role="tab" aria-selected="false" runat="server" />
            </li>

            <li class="nav-item">
                <asp:Label ID="lblHora4" Visible="false" runat="server"></asp:Label>
                <asp:LinkButton ID="lkbPestania4" Visible="false" CssClass="nav-link" data-toggle="tab" href="#kt_tabs_4_4" role="tab" aria-selected="false" runat="server" />
            </li>

            <li class="nav-item">
                <asp:Label ID="lblHora5" Visible="false" runat="server"></asp:Label>
                <asp:LinkButton ID="lkbPestania5" Visible="false" CssClass="nav-link" data-toggle="tab" href="#kt_tabs_4_5" role="tab" aria-selected="false" runat="server" />
            </li>
            <li class="nav-item">
                <asp:Label ID="lblHora6" Visible="false" runat="server"></asp:Label>
                <asp:LinkButton ID="lkbPestania6" Visible="false" CssClass="nav-link" data-toggle="tab" href="#kt_tabs_4_6" role="tab" aria-selected="false" runat="server" />
            </li>
            <li class="nav-item">
                <asp:Label ID="lblHora7" Visible="false" runat="server"></asp:Label>
                <asp:LinkButton ID="lkbPestania7" Visible="false" CssClass="nav-link" data-toggle="tab" href="#kt_tabs_4_7" role="tab" aria-selected="false" runat="server" />
            </li>
            <li class="nav-item">
                <asp:Label ID="lblHora8" Visible="false" runat="server"></asp:Label>
                <asp:LinkButton ID="lkbPestania8" Visible="false" CssClass="nav-link" data-toggle="tab" href="#kt_tabs_4_7" role="tab" aria-selected="false" runat="server" />
            </li>

        </ul>
        <%--Descripcion--%>
        <div class="tab-content">
            <div class="tab-pane" id="kt_tabs_4_1" role="tabpanel">
                <p id="p_4_1" runat="server"></p>
            </div>
            <div class="tab-pane" id="kt_tabs_4_2" role="tabpanel">
                <p id="p_4_2" runat="server"></p>
            </div>
            <div class="tab-pane" id="kt_tabs_4_3" role="tabpanel">
                <p id="p_4_3" runat="server"></p>
            </div>
            <div class="tab-pane" id="kt_tabs_4_4" role="tabpanel">
                <p id="p_4_4" runat="server"></p>
            </div>
            <div class="tab-pane" id="kt_tabs_4_5" role="tabpanel">
                <p id="p_4_5" runat="server"></p>
            </div>
            <div class="tab-pane" id="kt_tabs_4_6" role="tabpanel">
                <p id="p_4_6" runat="server"></p>
            </div>
            <div class="tab-pane" id="kt_tabs_4_7" role="tabpanel">
                <p id="p_4_7" runat="server"></p>
            </div>
            <div class="tab-pane" id="kt_tabs_4_8" role="tabpanel">
                <p id="p_4_8" runat="server"></p>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
