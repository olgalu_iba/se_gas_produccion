﻿/*
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 * Copyright (C) 2019 Bolsa Mercantil de Colombia, Todos los Derechos Reservados
 * Archivo:               WucCRUD.ascx.cs
 * Tipo:                  Clase principal
 * Autor:                 Oscar Piñeros
 * Fecha creación:        2019 Julio 10
 * Fecha modificación:    
 * Propósito:             Clase que permite administrar los botones de la aplicación 
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 */

using System;
using System.Collections;
using System.Web.UI;
using SIB.Global.Presentacion;

namespace UsersControls
{
    // ReSharper disable once InconsistentNaming
    public partial class WucCRUD : UserControl
    {
        #region Propiedades

        /// <summary>
        /// Contiene la información de sesión
        /// </summary>
        private InfoSessionVO goInfo;

        /// <summary>
        /// Ruta que se evalúa para saber si el usuario tiene o no permisos según esta
        /// </summary>
        private string Ruta
        {
            get { return ViewState["Ruta"] != null ? ViewState["Ruta"].ToString() : null; }
            set { ViewState["Ruta"] = value; }
        }

        /// <summary>
        /// Contiene los botones enviados por la funcionalidad
        /// </summary>
        private EnumBotones[] Botones
        {
            get { return (EnumBotones[])ViewState["Botones"]; }
            set { ViewState["Botones"] = value; }
        }

        #endregion Propiedades

        #region EventHandler

        /// <summary>
        /// Delegado para la respuesta del botón crear
        /// </summary>
        public event EventHandler CrearOnclick;

        /// <summary>
        /// Delegado para la respuesta del botón actualizar
        /// </summary>
        public event EventHandler ActualizarOnclick;

        /// <summary>
        /// Delegado para la respuesta del botón buscar
        /// </summary>
        public event EventHandler FiltrarOnclick;

        /// <summary>
        /// Delegado para la respuesta del botón preliminar
        /// </summary>
        public event EventHandler PreliminarOnclick;

        /// <summary>
        /// Delegado para la respuesta del botón preliminar
        /// </summary>
        /// 20200727 ajsute front-end
        public event EventHandler AuditoriaOnclick;
        /// <summary>
        /// Delegado para la respuesta del botón autorizar
        /// </summary>
        public event EventHandler AutorizarOnclick;

        /// <summary>
        /// Delegado para la respuesta del botón desautorizar
        /// </summary>
        public event EventHandler DesAutorizarOnclick;

        /// <summary>
        /// Delegado para la respuesta del botón aprobar
        /// </summary>
        public event EventHandler AprobarOnclick;

        /// <summary>
        /// Delegado para la respuesta del botón desaprobar
        /// </summary>
        public event EventHandler DesaprobarOnclick;

        /// <summary>
        /// Delegado para la respuesta del botón rechazar
        /// </summary>
        public event EventHandler RechazarOnclick;

        /// <summary>
        /// Delegado para la respuesta del botón reservar
        /// </summary>
        public event EventHandler ReservarOnclick;

        /// <summary>
        /// Delegado para la respuesta del botón solicitud de eliminacion
        /// </summary>
        public event EventHandler SolicitudEliminacionOnclick;

        /// <summary>
        /// Delegado para la respuesta del botón de cargue
        /// </summary>
        public event EventHandler CargueOnclick;

        /// <summary>
        /// Delegado para la respuesta del botón exportar a excel
        /// </summary>
        public event EventHandler ExportarExcelOnclick;

        /// <summary>
        /// Delegado para la respuesta del botón exportar a pdf
        /// </summary>
        public event EventHandler ExportarPdfOnclick;

        /// <summary>
        /// Delegado para la respuesta del botón facturar
        /// </summary>
        public event EventHandler FacturarOnclick;

        /// <summary>
        /// Delegado para la respuesta del botón modificar facturar
        /// </summary>
        public event EventHandler ModificarFacturarOnclick;

        /// <summary>
        /// Delegado para la respuesta del botón detale
        /// </summary>
        public event EventHandler DetalleOnclick;

        /// <summary>
        /// Delegado para la respuesta del botón detale
        /// </summary>
        /// 20201124
        public event EventHandler ModificarOnclick;
        /// <summary>
        /// Delegado para la respuesta del botón detale
        /// </summary>
        /// 20201124
        public event EventHandler SalirOnclick;

        #endregion EventHandler

        /// <summary>
        /// Evento donde se cargarán los valores para su página 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (!IsPostBack) EstablecerPermisosSistema();
        }

        /// <summary>
        /// Inicializa el control
        /// </summary>
        /// <param name="ruta"></param>
        /// <param name="botones"></param>
        public void Inicializar(string ruta = null, EnumBotones[] botones = null)
        {
            Ruta = ruta;
            Botones = botones;
        }

        /// <summary>
        /// Evento para el botón crear
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrearPagina(object sender, EventArgs e)
        {
            CrearOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento para el botón actualizar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ActualizarPagina(object sender, EventArgs e)
        {
            ActualizarOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento para el botón rechazar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RechazarPagina(object sender, EventArgs e)
        {
            RechazarOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento para el botón reservar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Reservar_Click(object sender, EventArgs e)
        {
            ReservarOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento para el botón buscar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FiltrarPagina(object sender, EventArgs e)
        {
            FiltrarOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento para el botón preliminar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Preliminar(object sender, EventArgs e)
        {
            PreliminarOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento para el botón preliminar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20200727 ajsute fron-end
        protected void Auditoria(object sender, EventArgs e)
        {
            AuditoriaOnclick?.Invoke(sender, e);
        }
        /// <summary>
        /// Evento para el botón autorizar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Autorizar(object sender, EventArgs e)
        {
            AutorizarOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento para el botón desautorizar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DesAutorizar(object sender, EventArgs e)
        {
            DesAutorizarOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento para el botón aprobar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Aprobar(object sender, EventArgs e)
        {
            AprobarOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento para el botón desaprobar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Desaprobar(object sender, EventArgs e)
        {
            DesaprobarOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento para el botón solicitud de eliminacion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SolicitudEliminacion(object sender, EventArgs e)
        {
            SolicitudEliminacionOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento para el botón de cargue
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cargue(object sender, EventArgs e)
        {
            CargueOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento para el botón exportar a excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ExportarExcel(object sender, EventArgs e)
        {
            ExportarExcelOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento para el botón exportar a pdf
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ExportarPdf(object sender, EventArgs e)
        {
            ExportarPdfOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento para el botón factura
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Factura(object sender, EventArgs e)
        {
            FacturarOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento para el botón modificar factura
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ModificarFactura(object sender, EventArgs e)
        {
            ModificarFacturarOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Evento para el botón detallar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20201124
        protected void Modificar(object sender, EventArgs e)
        {
            ModificarOnclick?.Invoke(sender, e);
        }
        /// <summary>
        /// Evento para el botón detallar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20201124
        protected void Salir(object sender, EventArgs e)
        {
            SalirOnclick?.Invoke(sender, e);
        }
        /// <summary>
        /// Evento para el botón detallar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Detallar(object sender, EventArgs e)
        {
            DetalleOnclick?.Invoke(sender, e);
        }

        /// <summary>
        /// Establece los permisos de los botones
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            try
            {
                lbtAgregar.Visible = false;
                lbtActualizar.Visible = false;
                lbtConsultar.Visible = false;
                lbtPreliminar.Visible = false;
                lbtAuditoria.Visible = false; //20200727 ajuste front-end
                lbtAprobar.Visible = false;
                lbtDesaprobar.Visible = false;
                lbtRechazar.Visible = false;
                lkbReservar.Visible = false;
                lbtSolEli.Visible = false;
                lbtCargue.Visible = false;
                lbtExcel.Visible = false;
                lbtPdf.Visible = false;
                lbtFacturar.Visible = false;
                lbtModificarFacturar.Visible = false;
                lbtDetalle.Visible = false;
                lbtModificar.Visible = false; //20201124
                lbtSalir.Visible = false; //20201124
                //Botones seleccionados en la funcionalidad
                if (Botones != null && Botones.Length > 0)
                    EnableButtons(Botones);

                //Botones por permisos 
                if (string.IsNullOrEmpty(Ruta)) return;
                Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, Ruta);
                lbtAgregar.Visible = (bool)permisos["INSERT"];
                lbtConsultar.Visible = (bool)permisos["SELECT"];
                lbtExcel.Visible = (bool)permisos["SELECT"];
                //20220505 desisitimiento
                switch (Ruta)
                {
                    case "t_capacidad_inyectada":
                    case "t_energia_cons_nal":
                    case "t_energia_exportar":
                    case "t_energia_parqueo":
                    case "t_energia_recibida":
                    case "t_energia_a_suminsitrar":
                    case "t_energia_tomada":
                    case "t_energia_trasferida":
                    case "t_energia_trasportar":
                    case "t_entrega_usuario_final":
                    case "t_energia_gasoducto":
                    case "t_energia_gnc":
                    case "t_energia_estacion":
                    case "t_energia_asignacion":
                        lbtPdf.Visible = false;
                        break;
                    default:
                        lbtPdf.Visible = (bool)permisos["SELECT"];
                        break;
                }
            }
            catch (Exception)
            {
                throw new Exception("Debe seleccionar la pantalla o los botones a mostrar");
            }
        }
        /// <summary>
        /// Cambia los botones visibles
        /// </summary>
        /// <param name="botones"></param>
        public void SwitchOnButton(params EnumBotones[] botones)
        {
            Botones = botones;
            EstablecerPermisosSistema();
        }

        /// <summary>
        /// Activa los botones 
        /// </summary>
        /// <param name="botones"></param>
        private void EnableButtons(params EnumBotones[] botones)
        {
            foreach (EnumBotones t in botones)
            {
                switch (t)
                {
                    case EnumBotones.Crear:
                        lbtAgregar.Visible = true;
                        break;
                    case EnumBotones.Actualizar:
                        lbtActualizar.Visible = true;
                        break;
                    case EnumBotones.Buscar:
                        lbtConsultar.Visible = true;
                        break;
                    case EnumBotones.Preliminar:
                        lbtPreliminar.Visible = true;
                        break;
                        //20200727 ajsute front-end
                    case EnumBotones.Auditoria:
                        lbtAuditoria.Visible = true;
                        break;
                    case EnumBotones.Aprobar:
                        lbtAprobar.Visible = true;
                        break;
                    case EnumBotones.Desaprobar:
                        lbtDesaprobar.Visible = true;
                        break;
                    case EnumBotones.Rechazar:
                        lbtRechazar.Visible = true;
                        break;
                    case EnumBotones.Reservar:
                        lkbReservar.Visible = true;
                        break;
                    case EnumBotones.SolicitudEliminacion:
                        lbtSolEli.Visible = true;
                        break;
                    case EnumBotones.Cargue:
                        lbtCargue.Visible = true;
                        break;
                    case EnumBotones.Excel:
                        lbtExcel.Visible = true;
                        break;
                    case EnumBotones.Pdf:
                        lbtPdf.Visible = true;
                        break;
                    case EnumBotones.Facturar:
                        lbtFacturar.Visible = true;
                        break;
                    case EnumBotones.ModificarFacturar:
                        lbtModificarFacturar.Visible = true;
                        break;
                    case EnumBotones.Detalle:
                        lbtDetalle.Visible = true;
                        break;
                        //20201124
                    case EnumBotones.Modificar: 
                        lbtModificar.Visible = true;
                        break;
                    //20201124
                    case EnumBotones.Salir:
                        lbtSalir.Visible = true;
                        break;
                    case EnumBotones.Ninguno:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

    }
}