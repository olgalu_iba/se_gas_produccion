﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucCRUD.ascx.cs" Inherits="UsersControls.WucCRUD" %>

<div class="kt-portlet__head-toolbar">
    <div class="btn-group show" role="group">
        <asp:UpdatePanel ID="UdpCrud" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:LinkButton ID="lbtAgregar" CssClass="btn btn-danger kt-subheader__btn-options" Visible="False" OnClick="CrearPagina" runat="server">
                    <i class="flaticon2-add-1"></i>
                    Agregar
                </asp:LinkButton>
                <asp:LinkButton ID="lbtActualizar" CssClass="btn btn-danger kt-subheader__btn-options" Visible="False" OnClick="ActualizarPagina" runat="server">
                    <i class="flaticon2-add-1"></i>
                    Actualizar
                </asp:LinkButton>
                <asp:LinkButton ID="lbtConsultar" CssClass="btn btn-outline-brand btn-square" ValidationGroup="consulta" Visible="False" OnClick="FiltrarPagina" runat="server">
                    <i class="flaticon-search"></i>
                    Consultar
                </asp:LinkButton>
                <asp:LinkButton ID="lbtPreliminar" CssClass="btn btn-outline-brand btn-square" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" Visible="False" OnClick="Preliminar" runat="server">
                    <i class=""></i>
                    Información preliminar de asignación
                </asp:LinkButton>
                <%--20200727 ajute front-end--%>
                <asp:LinkButton ID="lbtAuditoria" CssClass="btn btn-outline-brand btn-square" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" Visible="False" OnClick="Auditoria" runat="server">
                    <i class=""></i>
                    Crear Certificado
                </asp:LinkButton>
                <asp:LinkButton ID="lbtAutorizar" CssClass="btn btn-outline-brand btn-square" Visible="False" OnClick="Autorizar" runat="server">
                    <i class=""></i>
                    Autorizar
                </asp:LinkButton>
                <asp:LinkButton ID="lbtDesAutorizar" CssClass="btn btn-outline-brand btn-square" Visible="False" OnClick="DesAutorizar" runat="server">
                    <i class=""></i>
                    Des-autorizar
                </asp:LinkButton>
                <asp:LinkButton ID="lbtAprobar" CssClass="btn btn-outline-brand btn-square" Visible="False" OnClick="Aprobar" OnClientClick="this.disabled = true;" runat="server">
                    <i class="flaticon2-checkmark"></i>
                    Aprobar
                </asp:LinkButton>
                <ajaxToolkit:ConfirmButtonExtender TargetControlID="lbtAprobar" ConfirmText="Esta Seguro(a) de Aprobar los Registros Seleccionados?" runat="server" />
                <asp:LinkButton ID="lbtDesaprobar" CssClass="btn btn-outline-brand btn-square" Visible="False" OnClick="Desaprobar" runat="server">
                    <i class="flaticon2-cancel"></i>
                    Desaprobar
                </asp:LinkButton>
                <ajaxToolkit:ConfirmButtonExtender TargetControlID="lbtDesaprobar" ConfirmText="Esta Seguro(a) de Desaprobar los Registros Seleccionados?" runat="server" />
                <asp:LinkButton ID="lbtRechazar" CssClass="btn btn-outline-brand btn-square" Visible="False" OnClick="RechazarPagina" runat="server">
                    <i class="flaticon2-cancel"></i>
                    Rechazar
                </asp:LinkButton>
                <ajaxToolkit:ConfirmButtonExtender TargetControlID="lbtRechazar" ConfirmText="Esta Seguro(a) de Rechazar los Registros Seleccionados?" runat="server" />
                <asp:LinkButton ID="lkbReservar" CssClass="btn btn-outline-brand btn-square" Visible="False" OnClick="Reservar_Click" runat="server">
                    <i class="flaticon2-sheet"></i>
                    Reservar
                </asp:LinkButton>
                <asp:LinkButton ID="lbtSolEli" CssClass="btn btn-outline-brand btn-square" Visible="false" OnClick="SolicitudEliminacion" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" runat="server">
                    <i class="flaticon-support"></i>
                    Solicitud Eliminación
                </asp:LinkButton>
                <asp:LinkButton ID="lbtCargue" CssClass="btn btn-outline-brand btn-square" Visible="False" OnClick="Cargue" OnClientClick="MostrarCrono(); this.disabled = true;" UseSubmitBehavior="false" runat="server">
                    <i class="flaticon-upload"></i>
                    Cargar Archivo 
                </asp:LinkButton>
                <asp:LinkButton ID="lbtExcel" CssClass="btn btn-outline-brand btn-square" OnClick="ExportarExcel" Visible="False" runat="server">
                    <i class="fa fa-file-excel"></i>
                    Exportar a Excel
                </asp:LinkButton>
                <asp:LinkButton ID="lbtPdf" CssClass="btn btn-outline-brand btn-square" OnClick="ExportarPdf" Visible="False" runat="server">
                    <i class="fa fa-file-pdf"></i>
                    Exportar Pdf
                </asp:LinkButton>
                <asp:LinkButton ID="lbtFacturar" CssClass="btn btn-outline-brand btn-square" OnClick="Factura" Visible="False" runat="server">
                    <i class="flaticon2-file-2"></i>
                    Interfaz Facturación
                </asp:LinkButton>
                <asp:LinkButton ID="lbtModificarFacturar" CssClass="btn btn-outline-brand btn-square" OnClick="ModificarFactura" Visible="False" runat="server">
                    <i class="flaticon2-file-2"></i>
                    Interfaz Modificación
                </asp:LinkButton>
                <asp:LinkButton ID="lbtDetalle" CssClass="btn btn-outline-brand btn-square" OnClick="Detallar" Visible="False" runat="server">
                    <i class="flaticon2-file-2"></i>
                   Detalle
                </asp:LinkButton>
                <%--20201124--%>
                <asp:LinkButton ID="lbtModificar" CssClass="btn btn-outline-brand btn-square" OnClick="Modificar" Visible="False" runat="server">
                    <i class="flaticon2-file-2"></i>
                   Modificar
                </asp:LinkButton>
                <%--20201124--%>
                <asp:LinkButton ID="lbtSalir" CssClass="btn btn-outline-brand btn-square" OnClick="Salir" Visible="False" runat="server">
                    <i class="flaticon2-file-2"></i>
                   Salir
                </asp:LinkButton>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtConsultar" />
                <asp:PostBackTrigger ControlID="lbtSolEli" />
                <asp:PostBackTrigger ControlID="lbtCargue" />
                <asp:PostBackTrigger ControlID="lbtExcel" />
                <asp:PostBackTrigger ControlID="lbtPdf" />
                <asp:PostBackTrigger ControlID="lbtFacturar" />
                <asp:PostBackTrigger ControlID="lbtModificarFacturar" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</div>
