﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace UsersControls.Contratos.Reporte_Ejecucion_Contratos
{
    public partial class WucConsultaEjecuContrato : UserControl
    {
        #region EventHandler

        /// <summary>
        /// Delegado para el manejo de los modals
        /// </summary>
        public delegate void EventHandlerModal(string id, string insideId, EnumTypeModal typeModal);

        /// <summary>
        /// EventHandler para la selección de los los modals
        /// </summary>
        public event EventHandlerModal ModalEvent;

        /// <summary>
        /// Delegado para el manejo de los toastr
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        /// EventHandler para la selección de los toastr 
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        /// Delegado para la selección de los botones   
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get { return ViewState["NameController"] != null && !string.IsNullOrEmpty(ViewState["NameController"].ToString()) ? ViewState["NameController"].ToString() : string.Empty; }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        #endregion Propiedades

        private InfoSessionVO goInfo = null;
        private static string lsTitulo = "Información de Ejecución de contratos";
        private clConexion lConexion = null;
        private SqlDataReader lLector;
        private DataSet lds = new DataSet();
        private SqlDataAdapter lsqldata = new SqlDataAdapter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lConexion = new clConexion(goInfo);
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            //Se agrega el comportamiento del botón
            ButtonsEvent?.Invoke(new[] { EnumBotones.Buscar }, null, NameController);

            // Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, DdlOperador, "m_operador", " estado = 'A' order by razon_social", 0, 4);
            lConexion.Cerrar();

            //buttons.Inicializar(botones: botones);
            if (Session["tipoPerfil"].ToString() != "N") return;
            DdlOperador.SelectedValue = goInfo.cod_comisionista;
            DdlOperador.Enabled = false;

            //Se establece que el controlador ya se inicializo    
            Inicializado = true;
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnConsultar_Click(object sender, EventArgs e)
        {
            var lblMensaje = new StringBuilder();
            DateTime ldFechaI = DateTime.Now;
            DateTime ldFechaF = DateTime.Now;
            TimeSpan DifFecha;
            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo Fecha Inicial. <br>");
                }
            }
            if (TxtFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    if (TxtFechaIni.Text.Trim().Length > 0)
                    {
                        ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
                        if (ldFechaI > ldFechaF)
                            lblMensaje.Append("La Fecha Final NO puede ser Menor que la Fecha de Inicial. <br>");
                    }
                    else
                        lblMensaje.Append("Debe digitar la fecha inicial antes que la final. <br>");
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo Fecha Final. <br>");
                }
            }
            if (TxtFechaIni.Text == "" && TxtOperacion.Text == "" && TxtContratoDef.Text == "")
                lblMensaje.Append("Debe digitar la fecha, la operación o el contrato para la consulta. <br>");

            if (lblMensaje.ToString() == "")
            {
                try
                {
                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    var lComando = new SqlCommand
                    {
                        Connection = lConexion.gObjConexion,
                        CommandTimeout = 3600,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "pa_GetContEjecucionRep"
                    };
                    lComando.Parameters.Add("@P_fecha_ini", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                    lComando.Parameters.Add("@P_fecha_fin", SqlDbType.VarChar).Value = TxtFechaFin.Text == "" ? TxtFechaIni.Text.Trim() : TxtFechaFin.Text.Trim();
                    lComando.Parameters.Add("@P_numero_contato", SqlDbType.Int).Value = TxtOperacion.Text == "" ? "0" : TxtOperacion.Text;
                    lComando.Parameters.Add("@P_contrato_definitivo", SqlDbType.VarChar).Value = TxtContratoDef.Text;
                    lComando.Parameters.Add("@P_punta", SqlDbType.VarChar).Value = DdlPunta.SelectedValue;
                    lComando.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = DdlOperador.SelectedValue;
                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgMaestro.DataSource = lds;
                    dtgMaestro.DataBind();
                    dtgMaestro.Visible = true;
                    ButtonsEvent?.Invoke(new[] { EnumBotones.Buscar, EnumBotones.Excel }, null, NameController);
                    lConexion.Cerrar();
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("No se Pudo Generar el Informe.! " + ex.Message.ToString());
                }
            }
            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnExcel_Click(object sender, EventArgs e)
        {
            string lsNombreArchivo = goInfo.Usuario.ToString() + "InfEjecucion" + DateTime.Now + ".xls";
            string lstitulo_informe = "";
            string lsTituloParametros = "";
            try
            {
                lstitulo_informe = "Información de Ejecución de contratos";
                lsTituloParametros = "";
                if (TxtFechaIni.Text != "")
                    lsTituloParametros += " - Fecha Inicial: " + TxtFechaIni.Text.Trim();
                if (TxtFechaFin.Text != "")
                    lsTituloParametros += " - Fecha Final: " + TxtFechaFin.Text.Trim();
                if (TxtOperacion.Text != "")
                    lsTituloParametros += " - Operación: " + TxtOperacion.Text.Trim();
                if (TxtContratoDef.Text != "")
                    lsTituloParametros += " - Contrato Definitivo: " + TxtContratoDef.Text.Trim();
                if (DdlPunta.SelectedValue != "")
                    lsTituloParametros += "  - Punta: " + DdlPunta.SelectedItem.ToString();
                if (DdlOperador.SelectedValue != "0")
                    lsTituloParametros += "  - Operador: " + DdlOperador.SelectedItem.ToString();
                decimal ldCapacidad = 0;
                StringBuilder lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                dtgMaestro.EnableViewState = false;
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                lform.Controls.Add(dtgMaestro);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = System.Text.Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());

                Response.End();
                lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("No se Pudo Generar el Excel.!" + ex.Message.ToString(), EnumTypeToastr.Error);
            }
        }
    }
}