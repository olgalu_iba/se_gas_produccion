﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucEjecucionContrato.ascx.cs" Inherits="UsersControls.Contratos.Reporte_Ejecucion_Contratos.WucEjecucionContrato" %>

<%--Contenido--%>
<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Fecha Registro Ejecución Inicial" AssociatedControlID="TxtFechaIni" runat="server" />
            <asp:TextBox ID="TxtFechaIni" runat="server" CssClass="form-control datepicker" Width="100%" ClientIDMode="Static" MaxLength="10" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Fecha Registro Ejecución Final" AssociatedControlID="TxtFechaFin" runat="server" />
            <asp:TextBox ID="TxtFechaFin" runat="server" CssClass="form-control datepicker" Width="100%" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Número Operación Inicial" AssociatedControlID="TxtNoContrato" runat="server" />
            <asp:TextBox ID="TxtNoContrato" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Número Operación Final" AssociatedControlID="TxtNoContratoFin" runat="server" />
            <asp:TextBox ID="TxtNoContratoFin" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Número Contrato" AssociatedControlID="TxtContratoDef" runat="server" />
            <asp:TextBox ID="TxtContratoDef" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
            <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Tipo Subasta" AssociatedControlID="ddlSubasta" runat="server" />
            <asp:DropDownList ID="ddlSubasta" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Tipo Mercado" AssociatedControlID="ddlTpoMercado" runat="server" />
            <asp:DropDownList ID="ddlTpoMercado" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Producto" AssociatedControlID="ddlProducto" runat="server" />
            <asp:DropDownList ID="ddlProducto" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Modalidad Contractual" AssociatedControlID="ddlModalidad" runat="server" />
            <asp:DropDownList ID="ddlModalidad" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
</div>
<%--Grilla--%>
<div class="table table-responsive">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                Width="100%" CssClass="table-bordered" OnItemCommand="dtgConsulta_EditCommand" AllowPaging="true"
                PageSize="10" OnPageIndexChanged="dtgConsulta_PageIndexChanged">
                <Columns>
                    <%--20190524 rq029-19--%>
                    <%--0--%>
                    <asp:BoundColumn DataField="fecha_gas" HeaderText="Fecha Gas" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <%--1--%>
                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--2--%>
                    <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--3--%>
                    <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left"
                        DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <%--4--%>
                    <asp:BoundColumn DataField="operador_venta" HeaderText="Operador Venta" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Width="100px"></asp:BoundColumn>
                    <%--5--%>
                    <asp:BoundColumn DataField="nombre_venta" HeaderText="vendedor" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Width="100px"></asp:BoundColumn>
                    <%--6--%>
                    <asp:BoundColumn DataField="operador_compra" HeaderText="Operador Compra" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Width="100px"></asp:BoundColumn>
                    <%--7--%>
                    <asp:BoundColumn DataField="nombre_compra" HeaderText="comprador" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Width="100px"></asp:BoundColumn>
                    <%--8--%>
                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                        DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                    <%--9--%>
                    <asp:BoundColumn DataField="precio" HeaderText="Precio Contrato" ItemStyle-HorizontalAlign="Right"
                        DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                    <%--10--%>
                    <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial"
                        ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <%--11--%>
                    <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Inicial"
                        ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <%--12--%>
                    <asp:BoundColumn DataField="codigo_punto" HeaderText="Código Punto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--13--%>
                    <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--14--%>
                    <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--15--%>
                    <asp:BoundColumn DataField="cantidad_autorizada" HeaderText="Cantidad autorizada" ItemStyle-HorizontalAlign="Right"
                        DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                    <%--16--%>
                    <asp:BoundColumn DataField="Valor_facturado" HeaderText="Valor facturado" ItemStyle-HorizontalAlign="Right"
                        DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                    <%--17--%>
                    <asp:BoundColumn DataField="codigo_contrato_eje" Visible="false"></asp:BoundColumn>
                    <%--18--%>
                    <asp:BoundColumn DataField="punta" Visible="false"></asp:BoundColumn>
                    <%--19--%>
                    <asp:BoundColumn DataField="codigo_verif_contrato" Visible="false"></asp:BoundColumn>
                    <%--20--%>
                    <asp:BoundColumn DataField="estado" Visible="false"></asp:BoundColumn>
                    <%--21--%>
                    <asp:BoundColumn DataField="tipo_demanda" Visible="false"></asp:BoundColumn>
                    <%--22--%>
                    <asp:BoundColumn DataField="sector_consumo" Visible="false"></asp:BoundColumn>
                    <%--23--%>
                    <asp:BoundColumn DataField="fecha_gas" Visible="false"></asp:BoundColumn>
                    <%--24--%>
                    <asp:BoundColumn DataField="hora_max_ejec" Visible="false"></asp:BoundColumn>
                    <%--25--%>
                    <asp:BoundColumn DataField="desc_subasta" HeaderText="Tipo Subasta"></asp:BoundColumn>
                    <%--26--%>
                    <asp:BoundColumn DataField="desc_mercado" HeaderText="Tipo Mercado"></asp:BoundColumn>
                    <%--27--%>
                    <asp:BoundColumn DataField="desc_producto" HeaderText="Producto"></asp:BoundColumn>
                    <%--28--%>
                    <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual"></asp:BoundColumn>
                    <%--29--%>
                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                        <ItemTemplate>
                            <div class="dropdown dropdown-inline">
                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="flaticon-more-1"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                    <!--begin::Nav-->
                                    <ul class="kt-nav">
                                        <li class="kt-nav__item">
                                            <asp:LinkButton ID="lkbIngresar" CssClass="kt-nav__link" CommandName="Ingresar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-crisp-icons"></i>
                                                            <span class="kt-nav__link-text">Ingresar</span>
                                            </asp:LinkButton>
                                        </li>
                                        <li class="kt-nav__item">
                                            <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Modificar</span>
                                            </asp:LinkButton>
                                        </li>
                                        <li class="kt-nav__item">
                                            <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon-delete"></i>
                                                            <span class="kt-nav__link-text">Eliminar</span>
                                            </asp:LinkButton>
                                        </li>
                                    </ul>
                                    <!--end::Nav-->
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <%--32--%>
                    <asp:BoundColumn DataField="valida_hora" Visible="false"></asp:BoundColumn>
                </Columns>
                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
            </asp:DataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<%--Excel--%>
<asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False" Visible="false"
    Width="100%" CssClass="table-bordered">
    <Columns>
        <asp:BoundColumn DataField="fecha_gas" HeaderText="Fecha Gas" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
        <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left"
            DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
        <asp:BoundColumn DataField="operador_venta" HeaderText="Operador Venta" ItemStyle-HorizontalAlign="Left"
            ItemStyle-Width="100px"></asp:BoundColumn>
        <asp:BoundColumn DataField="nombre_venta" HeaderText="vendedor" ItemStyle-HorizontalAlign="Left"
            ItemStyle-Width="100px"></asp:BoundColumn>
        <asp:BoundColumn DataField="operador_compra" HeaderText="Operador Compra" ItemStyle-HorizontalAlign="Left"
            ItemStyle-Width="100px"></asp:BoundColumn>
        <asp:BoundColumn DataField="nombre_compra" HeaderText="comprador" ItemStyle-HorizontalAlign="Left"
            ItemStyle-Width="100px"></asp:BoundColumn>
        <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
            DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
        <asp:BoundColumn DataField="precio" HeaderText="Precio Contrato" ItemStyle-HorizontalAlign="Right"
            DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
        <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial"
            ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
        <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Inicial"
            ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
        <asp:BoundColumn DataField="codigo_punto" HeaderText="Código Punto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <asp:BoundColumn DataField="cantidad_autorizada" HeaderText="Cantidad autorizada" ItemStyle-HorizontalAlign="Right"
            DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
        <asp:BoundColumn DataField="Valor_facturado" HeaderText="Valor facturado" ItemStyle-HorizontalAlign="Right"
            DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
        <asp:BoundColumn DataField="desc_subasta" HeaderText="Tipo Subasta"></asp:BoundColumn>
        <asp:BoundColumn DataField="desc_mercado" HeaderText="Tipo Mercado"></asp:BoundColumn>
        <asp:BoundColumn DataField="desc_producto" HeaderText="Producto"></asp:BoundColumn>
        <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual"></asp:BoundColumn>
    </Columns>
    <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
</asp:DataGrid>

<%--Modals--%>
<%--Registro de Contratos--%>
<div class="modal fade" id="modContratosReg" tabindex="-1" role="dialog" aria-labelledby="modContratosRegLabel" aria-hidden="true" clientidmode="Static" style="overflow-y: auto" runat="server">
    <div class="modal-dialog" id="modContratosRegInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="modContratosRegLabel">Registro</h5>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <%--Infirmacion Basica--%>
                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5>Información Básica</h5>
                                    </div>
                                    <div class="panel-body">
                                        <%--Contenido--%>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Operación</label>
                                                    <asp:Label ID="lblOperacion" runat="server" CssClass="form-control" />-<asp:Label ID="lblPunta"
                                                        runat="server" CssClass="form-control"></asp:Label>
                                                    <asp:HiddenField ID="hdfPunta" runat="server" />
                                                    <asp:HiddenField ID="hdfCodigoEjec" runat="server" />
                                                    <asp:HiddenField ID="hdfCantidad" runat="server" />
                                                    <asp:HiddenField ID="hdfCodDatUsu" runat="server" />
                                                    <asp:HiddenField ID="hdfCodigoVerif" runat="server" />
                                                    <asp:HiddenField ID="hdfUltDia" runat="server" />
                                                    <asp:HiddenField ID="hdfHora" runat="server" />
                                                    <%--20190507 rq023-19--%>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Número de contrato</label>
                                                    <asp:Label ID="lblContratoDef" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Subasta</label>
                                                    <asp:Label ID="lblSubasta" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Tipo Mercado</label>
                                                    <asp:Label ID="lblMercado" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Producto</label>
                                                    <asp:Label ID="lblProducto" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Modalidad Contractual</label>
                                                    <asp:Label ID="lblModalidad" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Cantidad</label>
                                                    <asp:Label ID="lblCantidad" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Precio del Contrato</label>
                                                    <asp:Label ID="lblPrecio" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Fecha Gas</label>
                                                    <asp:Label ID="lblFechaGas" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Punto de entrega</label>
                                                    <asp:DropDownList ID="ddlPunto" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Cantidad Autorizada</label>
                                                    <asp:TextBox ID="TxtCantidad" runat="server" type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" ValidationGroup="detalle" CssClass="form-control" OnTextChanged="TxtCantidad_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Valor Facturado</label>
                                                    <%--20201214 ajueste campo--%>
                                                    <asp:TextBox ID="TxtValor" runat="server" type="text"  ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FTBETxtValor" runat="server" TargetControlID="TxtValor"
                                                        FilterType="Custom, Numbers" ValidChars="."></ajaxToolkit:FilteredTextBoxExtender>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <%--Demanda--%>
                            <div id="tblDemanda" visible="false" runat="server">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5>Usuarios Finales</h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Tipo Demanda</label>
                                                    <asp:DropDownList ID="ddlDemanda" runat="server" OnSelectedIndexChanged="ddlDemanda_SelectedIndexChanged"
                                                        AutoPostBack="true" CssClass="form-control selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Sector de Consumo</label>
                                                    <asp:DropDownList ID="ddlSector" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Punto de Salida</label>
                                                    <asp:DropDownList ID="ddlSalida" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Usuario Final</label>
                                                    <asp:TextBox ID="TxtUsuarioFinal" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <ajaxToolkit:AutoCompleteExtender runat="server" BehaviorID="AutoCompleteExUsuF" ID="AutoCompleteExtender1"
                                                        TargetControlID="TxtUsuarioFinal" ServicePath="~/WebService/AutoComplete.asmx"
                                                        ServiceMethod="GetCompletionListUsuarioFinal" MinimumPrefixLength="3" CompletionInterval="1000"
                                                        EnableCaching="true" CompletionSetCount="20" CompletionListCssClass="autocomplete_completionListElement"
                                                        CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                        DelimiterCharacters=";,:">
                                                        <Animations>
                        <OnShow>
                            <Sequence>
                                <OpacityAction Opacity="0" />
                                <HideAction Visible="true" />
                                                                <ScriptAction Script="
                                    // Cache the size and setup the initial size
                                    var behavior = $find('AutoCompleteExUsuF');
                                    if (!behavior._height) {
                                        var target = behavior.get_completionList();
                                        behavior._height = target.offsetHeight - 2;
                                        target.style.height = '0px';
                                    }" />
                                
                                <Parallel Duration=".4">
                                    <FadeIn />
                                    <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteExUsuF')._height" />
                                </Parallel>
                            </Sequence>
                        </OnShow>
                        <OnHide>
                            <Parallel Duration=".4">
                                <FadeOut />
                                <Length PropertyKey="height" StartValueScript="$find('AutoCompleteExUsuF')._height" EndValue="0" />
                            </Parallel>
                        </OnHide>
                                                        </Animations>
                                                    </ajaxToolkit:AutoCompleteExtender>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Mercado Relevante</label>
                                                    <asp:DropDownList ID="ddlMercado" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Cantidad Contratada</label>
                                                    <asp:TextBox ID="TxtCantidadUsr" type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <asp:Button ID="btnCrearUsu" runat="server" CssClass="btn btn-primary btn-block" Text="Agregar Usuario" OnClick="btnCrearUsu_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" Visible="false" />
                                        <asp:Button ID="btnActualUsu" runat="server" CssClass="btn btn-primary btn-block" Text="Actualiza Usuario" OnClick="btnActualUsu_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" Visible="false" />

                                        <hr>

                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <b>TOTAL CANTIDAD:</b><asp:Label ID="lblTotlCantidad" runat="server" ForeColor="Red"
                                                        Font-Bold="true"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table table-responsive">
                                            <asp:DataGrid ID="dtgUsuarios" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                                Width="100%" CssClass="table-bordered" OnItemCommand="dtgUsuarios_EditCommand">
                                                <Columns>
                                                    <%--0--%>
                                                    <asp:BoundColumn DataField="codigo_contrato_eje_usr" Visible="false"></asp:BoundColumn>
                                                    <%--20190524 rq029-19--%>
                                                    <%--1--%>
                                                    <asp:BoundColumn DataField="tipo_demanda" HeaderText="Código Tipo Demanda"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--2--%>
                                                    <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--3--%>
                                                    <asp:BoundColumn DataField="sector_consumo" HeaderText="Código Sector Consumo"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--4--%>
                                                    <asp:BoundColumn DataField="desc_sector" HeaderText="Sector consumo"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--5--%>
                                                    <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Código Punto Salida"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--6--%>
                                                    <asp:BoundColumn DataField="desc_salida" HeaderText="Punto de Salida"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--20190524 fin  rq029-19--%>
                                                    <%--7--%>
                                                    <asp:BoundColumn DataField="nit_usuario_no_regulado" HeaderText="Identificacion Usuario Final"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--8--%>
                                                    <asp:BoundColumn DataField="nombre_usuario" HeaderText="Nombre Usuario"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--9--%>
                                                    <asp:BoundColumn DataField="codigo_mercado_relevante" HeaderText="Código Mercado Relevante" ItemStyle-HorizontalAlign="Left"
                                                        ItemStyle-Width="80px"></asp:BoundColumn>
                                                    <%--10--%>
                                                    <asp:BoundColumn DataField="desc_mercado" HeaderText="Mercado Relevante" ItemStyle-HorizontalAlign="Left"
                                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                                    <%--20190524 rq029-19--%>
                                                    <%--11--%>
                                                    <asp:BoundColumn DataField="cantidad_contratada" HeaderText="Cantidad Contratada" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                                    <%--12--%>
                                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                                        <ItemTemplate>
                                                            <div class="dropdown dropdown-inline">
                                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="flaticon-more-1"></i>
                                                                </button>
                                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                                    <!--begin::Nav-->
                                                                    <ul class="kt-nav">
                                                                        <li class="kt-nav__item">
                                                                            <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Modificar</span>
                                                                            </asp:LinkButton>
                                                                        </li>
                                                                        <li class="kt-nav__item">
                                                                            <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon-delete"></i>
                                                            <span class="kt-nav__link-text">Eliminar</span>
                                                                            </asp:LinkButton>
                                                                        </li>
                                                                    </ul>
                                                                    <!--end::Nav-->
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <%--Detalle--%>
                            <div id="tblDetalle" visible="false" runat="server">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5>Puntos Ingresados</h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="table table-responsive">
                                            <asp:DataGrid ID="dtgDetalle" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                                Width="100%" CssClass="table-bordered">
                                                <Columns>
                                                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Operación"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fecha_gas" HeaderText="Fecha Gas" DataFormatString="{0:yyyy/MM/dd}"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="codigo_Punto" HeaderText="Código Punto"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrtega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--20190524 rq029-19--%>
                                                    <%--<asp:BoundColumn DataField="tipo_demanda" HeaderText="Código Tipo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="sector_consumo" HeaderText="Código Sector Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_sector" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>--%>
                                                    <%--20190524  FIn rq029-19--%>
                                                    <asp:BoundColumn DataField="cantidad_autorizada" HeaderText="Cantidad Autorizada" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="valor_facturado" HeaderText="Valor Facturado" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0.00}"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                </Columns>
                                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnCancelar" runat="server" class="btn btn-secondary" Text="Cancelar" OnClick="btnRegresar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnCrear" runat="server" CssClass="btn btn-primary" Text="Crear" OnClick="btnCrear_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnActualizar" runat="server" CssClass="btn btn-primary" Text="Actualizar" OnClick="btnActualizar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
