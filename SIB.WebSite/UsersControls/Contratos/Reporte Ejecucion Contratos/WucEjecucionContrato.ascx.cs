﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace UsersControls.Contratos.Reporte_Ejecucion_Contratos
{
    public partial class WucEjecucionContrato : UserControl
    {
        #region EventHandler

        /// <summary>
        /// Delegado para el manejo de los modals
        /// </summary>
        public delegate void EventHandlerModal(string id, string insideId, EnumTypeModal typeModal);

        /// <summary>
        /// EventHandler para la selección de los los modals
        /// </summary>
        public event EventHandlerModal ModalEvent;

        /// <summary>
        /// Delegado para el manejo de los toastr
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        /// EventHandler para la selección de los toastr 
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        /// Delegado para la selección de los botones
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get { return ViewState["NameController"] != null && !string.IsNullOrEmpty(ViewState["NameController"].ToString()) ? ViewState["NameController"].ToString() : string.Empty; }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        #endregion Propiedades

        private InfoSessionVO goInfo = null;
        private static string lsTitulo = "Registro de Ejecución de Contratos";
        private clConexion lConexion = null;
        private clConexion lConexion1 = null;
        private SqlDataReader lLector;
        private DataSet lds = new DataSet();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            goInfo.Programa = lsTitulo;
            lConexion = new clConexion(goInfo);
            lConexion1 = new clConexion(goInfo);
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Excel };
            ButtonsEvent?.Invoke(botones, null, NameController);
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles1(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador !=0 order by codigo_operador", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlPunto, "m_pozo", " estado = 'A'  order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMercado, "m_mercado_relevante", " estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlDemanda, "m_tipo_demanda_atender", " estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo sec ", " sec.estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlSubasta, "m_tipos_subasta tpo", " tpo.estado ='A' and exists (select 1 from m_ejecucion_modalidad eje where tpo.codigo_tipo_subasta = eje.codigo_tipo_subasta and eje.estado ='A') order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlTpoMercado, "m_tipo_mercado tpo", "  exists (select 1 from m_ejecucion_modalidad eje where tpo.codigo_tipo_mercado = eje.tipo_mercado and eje.estado ='A') order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlProducto, "m_producto prd", " prd.estado ='A' and exists (select 1 from m_ejecucion_modalidad eje where prd.sigla = eje.destino_rueda and eje.estado ='A') order by descripcion", 6, 1);
            LlenarControles(lConexion.gObjConexion, ddlModalidad, "m_modalidad_contractual mod", " mod.estado ='A' and exists (select 1 from m_ejecucion_modalidad eje where mod.codigo_modalidad= eje.codigo_modalidad and eje.estado ='A') order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlSalida, "m_punto_salida_snt pto", " pto.estado ='A' order by descripcion", 0, 2); //20190524 rq029-19

            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
            }
            //Se establece que el controlador ya se inicializo    
            Inicializado = true;
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"] + "-" + lLector["razon_social"];
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: dtgConsulta_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgConsulta.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            var lblMensaje = new StringBuilder();
            DateTime ldFecha;
            int liValor = 0;
            string[] lsNombreParametros = { "@P_numero_contrato_ini", "@P_numero_contrato_fin", "@P_contrato_definitivo", "@P_codigo_operador", "@P_tipo_subasta", "@P_tipo_mercado", "@P_destino_rueda", "@P_codigo_modalidad", "@P_fecha_gas_ini", "@P_fecha_gas_fin" }; //20190524 rq029-19
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };//20190524 rq029-19
            string[] lValorParametros = { "0", "0", "", "0", "0", "0", "0", "0", "", "" };//20190524 rq029-19

            //20190524 rq029-19
            DateTime ldFechaI = DateTime.Now;
            DateTime ldFechaF = DateTime.Now;

            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo Fecha Inicial. <br>");
                }
            }
            else
                lblMensaje.Append("Debe digitar la Fecha Inicial. <br>");
            if (TxtFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    if (TxtFechaIni.Text.Trim().Length > 0)
                    {
                        ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
                        if (ldFechaI > ldFechaF)
                            lblMensaje.Append("La Fecha Final NO puede ser Menor que la Fecha de Inicial. <br>");
                    }
                    else
                        lblMensaje.Append("Debe digitar la fecha inicial antes que la final. <br>");
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo Fecha Final. <br>");
                }
            }
            //20190524 fin rq029-19
            if (TxtNoContrato.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoContrato.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo operación inicial. <br>");
                }

            }
            if (TxtNoContratoFin.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoContratoFin.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo operación final. <br>");
                }

            }
            if (TxtNoContrato.Text.Trim().Length == 0 && TxtNoContratoFin.Text.Trim().Length > 0)
            {
                lblMensaje.Append("Debe digitar el No. de contrato inicial antes que el final. <br>");
            }
            if (TxtNoContrato.Text.Trim().Length > 0 && TxtNoContratoFin.Text.Trim().Length > 0)
            {
                try
                {
                    if (Convert.ToInt32(TxtNoContratoFin.Text) < Convert.ToInt32(TxtNoContrato.Text))
                        lblMensaje.Append("El No. de contrato inicial debe ser menor o igual que el final. <br>");
                }
                catch (Exception ex)
                {

                }
            }
            if (lblMensaje.ToString() == "")
            {
                try
                {
                    if (TxtNoContrato.Text.Trim().Length > 0)
                    {
                        lValorParametros[0] = TxtNoContrato.Text.Trim();
                        lValorParametros[1] = TxtNoContrato.Text.Trim();
                    }
                    if (TxtNoContratoFin.Text.Trim().Length > 0)
                        lValorParametros[1] = TxtNoContratoFin.Text.Trim();
                    if (TxtContratoDef.Text.Trim().Length > 0)
                        lValorParametros[2] = TxtContratoDef.Text.Trim();
                    if (ddlOperador.SelectedValue != "0")
                        lValorParametros[3] = ddlOperador.SelectedValue;
                    lValorParametros[4] = ddlSubasta.SelectedValue;
                    lValorParametros[5] = ddlTpoMercado.SelectedValue;
                    lValorParametros[6] = ddlProducto.SelectedValue;
                    lValorParametros[7] = ddlModalidad.SelectedValue;
                    //20190524 rq029-19
                    lValorParametros[8] = TxtFechaIni.Text.Trim();
                    if (TxtFechaFin.Text != "")
                        lValorParametros[9] = TxtFechaFin.Text.Trim();
                    else
                        lValorParametros[9] = TxtFechaIni.Text.Trim();
                    //20190524 fin rq029-19
                    lConexion.Abrir();
                    dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContEjecucion", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgConsulta.DataBind();
                    dtgExcel.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContEjecucion", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgExcel.DataBind();
                    lConexion.Cerrar();
                    if (dtgConsulta.Items.Count > 0)
                    {
                        //imbExcel.Visible = true;
                    }
                    else
                    {
                        //imbExcel.Visible = false;
                        ToastrEvent?.Invoke("No se encontraron Registros.", EnumTypeToastr.Info);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("No se Pudo Generar el Informe.! " + ex.Message);
                }
            }

            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page")) return;

            var lblMensaje = new StringBuilder();
            try
            {
                hdfUltDia.Value = e.Item.Cells[30].Text;
            }
            catch
            {
            }
            hdfHora.Value = e.Item.Cells[24].Text;
            if (e.CommandName.Equals("Ingresar"))
            {
                string lsError = "N";
                if (hdfUltDia.Value == "S" && Convert.ToDateTime(hdfHora.Value) < Convert.ToDateTime(DateTime.Now.ToString("HH:mm"))) //20190524 rq029-19
                    lblMensaje.Append("Está fuera del horario para registro de ejecución de contrato");
                if (lblMensaje.ToString() == "")
                {
                    try
                    {
                        limpiarDatosUsu();
                        limpiarDatos();
                        ddlPunto.Enabled = true;
                        lblFechaGas.Text = e.Item.Cells[0].Text; //20190524 rq029-19
                                                                 //20190524 rq029-19 cambios de indices
                        lblContratoDef.Text = e.Item.Cells[2].Text;
                        lblOperacion.Text = e.Item.Cells[1].Text;
                        //hdfCantidad.Value = e.Item.Cells[8].Text; //20190524 rq029-19
                        lblCantidad.Text = e.Item.Cells[8].Text;
                        lblPrecio.Text = e.Item.Cells[9].Text;
                        hdfPunta.Value = e.Item.Cells[18].Text;
                        hdfCodigoVerif.Value = e.Item.Cells[19].Text;
                        lblSubasta.Text = e.Item.Cells[25].Text;
                        lblMercado.Text = e.Item.Cells[26].Text;
                        lblProducto.Text = e.Item.Cells[27].Text;
                        lblModalidad.Text = e.Item.Cells[28].Text;
                        hdfCantidad.Value = "0";
                        hdfCodigoEjec.Value = "0";
                        btnActualizar.Visible = false;
                        btnCrear.Visible = true;
                        CargarDatosDet();
                        lblTotlCantidad.Text = "0";//20190524 rq029-19

                        if (hdfPunta.Value == "C")
                        {

                            //                        hdfPunta.Value = "C";  //20190524 rq029-19
                            lblPunta.Text = "COMPRADOR";
                            //trCompra.Visible = true; //20190524 rq029-19
                            if (lsError == "N")
                                btnCrearUsu.Visible = true;

                            string[] lsNombreParametrosC = { "@P_cadena" };
                            SqlDbType[] lTipoparametrosC = { SqlDbType.VarChar };
                            string[] lValorParametrosC = { " Delete from t_contrato_ejecucion_usr where codigo_contrato_eje= 0 And login_usuario = '" + goInfo.Usuario + "' " };
                            DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_EjecutarCadena", lsNombreParametrosC, lTipoparametrosC, lValorParametrosC);
                            tblDemanda.Visible = true;
                            CargarDatosUsu();
                        }
                        if (hdfPunta.Value == "V")
                        {
                            //hdfPunta.Value = "V"; //20190524 rq029-19
                            lblPunta.Text = "VENDEDOR";
                            //trCompra.Visible = false; //20190524 rq029-19
                            btnCrearUsu.Visible = false;
                            btnActualUsu.Visible = false;
                            ddlDemanda.SelectedValue = "0";
                            ddlSector.SelectedValue = "0";
                            tblDemanda.Visible = false;
                        }
                        //Abre el modal de registro
                        ModalEvent?.Invoke(modContratosReg.ID, modContratosRegInside.ID, EnumTypeModal.Abrir);
                        tblDetalle.Visible = true;
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Append("Problemas en la Recuperación de la Información. " + ex.Message);
                    }
                }
            }
            if (e.CommandName.Equals("Modificar"))
            {
                string lsError = "N";
                try
                {
                    //valida que se pueda crear o modificar el registro
                    if (e.Item.Cells[17].Text == "&nbsp;") //20190524 rq029-19
                        lblMensaje.Append("No se ha ingresado la información para modificar<br>");
                    if (e.Item.Cells[20].Text == "A") //20190524 rq029-19
                        lblMensaje.Append("La información ya se ingresó por ambas puntas corectamente<br>");
                    if (hdfUltDia.Value == "S" && Convert.ToDateTime(hdfHora.Value) < Convert.ToDateTime(DateTime.Now.ToString("HH:mm"))) //20190524 rq029-19
                        lblMensaje.Append("Está fuera del horario para registro de ejecución de contrato");
                    if (lblMensaje.ToString() == "")
                    {
                        limpiarDatosUsu();
                        ddlPunto.Enabled = false;
                        lblFechaGas.Text = e.Item.Cells[0].Text; //20190524 rq029-19
                                                                 //20190524 rq029-19 canbio de inidices
                        lblOperacion.Text = e.Item.Cells[1].Text;
                        lblContratoDef.Text = e.Item.Cells[2].Text;
                        //hdfCantidad.Value = e.Item.Cells[8].Text; //20190524 rq019-19
                        lblCantidad.Text = e.Item.Cells[8].Text;
                        lblPrecio.Text = e.Item.Cells[9].Text;
                        ddlPunto.SelectedValue = e.Item.Cells[12].Text;
                        TxtCantidad.Text = Convert.ToDecimal(e.Item.Cells[15].Text).ToString();
                        hdfCantidad.Value = TxtCantidad.Text;
                        TxtValor.Text = e.Item.Cells[16].Text;
                        hdfCodigoEjec.Value = e.Item.Cells[17].Text;
                        hdfPunta.Value = e.Item.Cells[18].Text;
                        hdfCodigoVerif.Value = e.Item.Cells[19].Text;
                        ddlDemanda.SelectedValue = e.Item.Cells[21].Text;
                        //hdfActBorrUsr.Value = "N"; //20190507 rq023-19
                        //ddlDemanda_SelectedIndexChanged(null, null); //20190507 rq023-19
                        //hdfActBorrUsr.Value = "S"; //20190507 rq023-19
                        ddlSector.SelectedValue = e.Item.Cells[22].Text;
                        CargarDatosDet();
                        if (Session["tipoPerfil"].ToString() != "N")
                        {
                            lsError = "S";
                            CargarDatosUsu();
                        }
                        btnActualizar.Visible = true;
                        btnCrear.Visible = false;

                        if (hdfPunta.Value == "C")
                        {
                            lblPunta.Text = "COMPRADOR";
                            //trCompra.Visible = true; //20190524 rq029-19
                            if (lsError == "N")
                                btnCrearUsu.Visible = true;
                            CargarDatosUsu();
                            string[] lsNombreParametrosC = { "@P_cadena" };
                            SqlDbType[] lTipoparametrosC = { SqlDbType.VarChar };
                            string[] lValorParametrosC = { " Delete from t_contrato_ejecucion_usr where codigo_contrato_eje= 0 And login_usuario = '" + goInfo.Usuario + "' " };
                            DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_EjecutarCadena", lsNombreParametrosC, lTipoparametrosC, lValorParametrosC);
                            btnCrearUsu.Visible = true;
                            btnActualUsu.Visible = false;
                            tblDemanda.Visible = true;
                        }
                        if (hdfPunta.Value == "V")
                        {
                            lblPunta.Text = "VENDEDOR";
                            //trCompra.Visible = false; //20190524 rq029-19
                            btnCrearUsu.Visible = false;
                            btnActualUsu.Visible = false;
                            ddlDemanda.SelectedValue = "0";
                            ddlSector.SelectedValue = "0";
                            tblDemanda.Visible = false;
                        }
                        //Abre el modal de registro
                        ModalEvent?.Invoke(modContratosReg.ID, modContratosRegInside.ID, EnumTypeModal.Abrir);
                        tblDetalle.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Problemas en la Recuperación de la Información. " + ex.Message);
                }
            }
            if (e.CommandName.Equals("Eliminar"))
            {
                hdfCodigoEjec.Value = e.Item.Cells[17].Text; //20190524 rq029-19
                if (hdfCodigoEjec.Value == "&nbsp;")
                    lblMensaje.Append("No se ha ingresado la información para modificar<br>");
                if (e.Item.Cells[20].Text == "A") //20190524 rq029-19
                    lblMensaje.Append("La información ya se ingresó por ambas puntas corectamente<br>");
                if (hdfUltDia.Value == "S" && Convert.ToDateTime(hdfHora.Value) < Convert.ToDateTime(DateTime.Now.ToString("HH:mm"))) //20190524 rq029-19
                    lblMensaje.Append("Está fuera del horario para registro de ejecución de contrato");
                if (lblMensaje.ToString() == "")
                {
                    string[] lsNombreParametros = { "@P_codigo_contrato_eje", };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int };
                    string[] lValorParametros = { hdfCodDatUsu.Value, "3" };

                    try
                    {
                        lValorParametros[0] = hdfCodigoEjec.Value;
                        lConexion.Abrir();
                        if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_DelContEjec", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                        {
                            lblMensaje.Append("Se presentó un Problema en la Eliminación de la Información.!" + goInfo.mensaje_error);
                            lConexion.Cerrar();
                        }
                        else
                        {
                            lConexion.Cerrar();
                            ToastrEvent?.Invoke("Registro Eliminado Correctamente.!", EnumTypeToastr.Success);

                            CargarDatos();
                        }
                    }
                    catch (Exception ex)
                    {
                        ToastrEvent?.Invoke("Problemas al Eliminar el Registro.!" + ex.Message, EnumTypeToastr.Error);
                    }
                }
            }

            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_operador)
        {
            string lsCondicion = "nombre_tabla='t_contrato_ejecucion' and llave_registro='codigo_contato_eje=" + hdfCodigoEjec.Value + "'";
            string lsCondicion1 = "codigo_contrato_eje=" + hdfCodigoEjec.Value;
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_contrato_ejecucion";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
            }
            return true;
        }

        /// <summary>
        /// Realiza las Validaciones de los Tipos de Datos y Campos Obligatorios de la Pantalla
        /// </summary>
        /// <returns></returns>
        protected string validaciones()
        {
            string lsError = "";

            try
            {
                if (ddlPunto.SelectedValue == "0")
                    lsError += "Debe Seleccionar el punto de entrega. <br>";
                if (TxtCantidad.Text == "")
                    lsError += "Debe Ingresar la cantidad autorizada. <br>";
                if (TxtValor.Text == "")
                    lsError += "Debe Ingresar el valor facturado. <br>";
                else
                {
                    string[] lsValor;
                    decimal ldValor;
                    try
                    {
                        ldValor = Convert.ToDecimal(TxtValor.Text.Trim());
                        if (ldValor < 0)
                            lsError += "Valor Inválido en el valor facturado. <br>";
                        else
                        {
                            lsValor = TxtValor.Text.Trim().Split('.');
                            if (lsValor.Length > 1)
                            {
                                if (lsValor[1].Trim().Length > 2)
                                    lsError += "Valor Inválido en el valor facturado. <br>";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        lsError += "Valor Inválido en el valor facturado. <br>";
                    }
                }
                //20190524 rq029-19
                //if (hdfPunta.Value == "C")
                //{
                //    if (ddlDemanda.SelectedValue == "0")
                //        lsError += "Debe seleccionar el tipo de demanda. <br>");
                //    if (ddlSector.SelectedValue == "0")
                //        lsError += "Debe seleccionar el sector de consumo. <br>");
                //    if (dtgUsuarios.Items.Count == 0)
                //        lsError += "No ha ingresado el detalle de usuarios finales. <br>");
                //}
                //else
                //{
                //    ddlDemanda.SelectedValue = "0";
                //    ddlSector.SelectedValue = "0";
                //}
                //20190524 rq029-19
                if (hdfPunta.Value == "C")
                {
                    if (lblTotlCantidad.Text != hdfCantidad.Value)
                        lsError += "La cantidad del detalle de usuarios finales no es igual a la cantidad total de la ejecución del contrato. <br>";
                }
                return lsError;
            }
            catch (Exception ex)
            {
                return "Problemas en la Validación de la Información. " + ex.Message;
            }
        }

        /// <summary>
        /// Nombre: dtgUsuarios_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgUsuarios_EditCommand(object source, DataGridCommandEventArgs e)
        {
            var lblMensaje = new StringBuilder();

            if (e.CommandName.Equals("Modificar"))
            {
                try
                {
                    hdfCodDatUsu.Value = dtgUsuarios.Items[e.Item.ItemIndex].Cells[0].Text;
                    ddlDemanda.SelectedValue = dtgUsuarios.Items[e.Item.ItemIndex].Cells[1].Text; //20190524 rq029-19
                    ddlDemanda_SelectedIndexChanged(null, null);//20190524 rq029-19 
                    ddlSector.SelectedValue = dtgUsuarios.Items[e.Item.ItemIndex].Cells[3].Text; //20190524 rq029-19
                    ddlSalida.SelectedValue = dtgUsuarios.Items[e.Item.ItemIndex].Cells[5].Text; //20190524 rq029-19
                    if (dtgUsuarios.Items[e.Item.ItemIndex].Cells[7].Text != "&nbsp;") //20190524 rq029-19
                        TxtUsuarioFinal.Text = dtgUsuarios.Items[e.Item.ItemIndex].Cells[7].Text + "-" + dtgUsuarios.Items[e.Item.ItemIndex].Cells[8].Text; //20190524 rq029-19
                    else
                        TxtUsuarioFinal.Text = "";
                    ddlMercado.SelectedValue = dtgUsuarios.Items[e.Item.ItemIndex].Cells[9].Text; //20190524 rq029-19
                    TxtCantidadUsr.Text = Convert.ToDecimal(dtgUsuarios.Items[e.Item.ItemIndex].Cells[11].Text).ToString(); //20190524 rq029-19
                    btnCrearUsu.Visible = false;
                    btnActualUsu.Visible = true;
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Problemas al Recuperar el Registro.! " + ex.Message, EnumTypeToastr.Error);
                    return;
                }
            }
            if (e.CommandName.Equals("Eliminar"))
            {
                string[] lsNombreParametros = { "@P_codigo_contrato_eje_usr", "@P_accion" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { hdfCodDatUsu.Value, "3" };
                //20190524 rq029-19
                if (hdfUltDia.Value == "S" && Convert.ToDateTime(hdfHora.Value) < Convert.ToDateTime(DateTime.Now.ToString("HH:mm")))
                    lblMensaje.Append("Está fuera del horario para registro de ejecución de contrato");
                if (lblMensaje.ToString() == "")
                {
                    try
                    {
                        hdfCodDatUsu.Value = dtgUsuarios.Items[e.Item.ItemIndex].Cells[0].Text;
                        lValorParametros[0] = hdfCodDatUsu.Value;
                        lConexion.Abrir();
                        if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContEjecUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                        {
                            lblMensaje.Append("Se presentó un Problema en la Eliminación de la Información del Usuario.!" + goInfo.mensaje_error);
                            lConexion.Cerrar();
                        }
                        else
                        {
                            lConexion.Cerrar();
                            ToastrEvent?.Invoke("Registro Eliminado Correctamente.!", EnumTypeToastr.Success);
                            btnActualUsu.Visible = false;//20190524 rq029-19
                            btnCrearUsu.Visible = true;//20190524 rq029-19
                            limpiarDatosUsu();//20190524 rq029-19
                            CargarDatosUsu();
                        }
                    }
                    catch (Exception ex)
                    {
                        ToastrEvent?.Invoke("Problemas al Eliminar el Registro.! " + ex.Message, EnumTypeToastr.Error);
                        return;
                    }
                }
            }

            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
        }

        /// <summary>
        /// Nombre: CargarDatosUsu
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid de Usuarios.
        /// Modificacion:
        /// </summary>
        private void CargarDatosUsu()
        {
            string[] lsNombreParametros = { "@P_codigo_contrato_eje", "@P_login" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
            string[] lValorParametros = { "-1", "" };
            lblTotlCantidad.Text = "0";
            try
            {
                if (hdfCodigoEjec.Value.Trim().Length > 0)
                    lValorParametros[0] = hdfCodigoEjec.Value;
                lValorParametros[1] = goInfo.Usuario;
                lConexion.Abrir();
                dtgUsuarios.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContEjecUsr", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgUsuarios.DataBind();
                lConexion.Cerrar();
                //20190524 rq029-19
                if (dtgUsuarios.Items.Count > 0)
                {
                    foreach (DataGridItem Grilla in dtgUsuarios.Items)
                    {
                        lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(Grilla.Cells[11].Text)).ToString(CultureInfo.InvariantCulture);
                    }
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("No se Pudo consultar la información de usuarios finales.! " + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: CargarDatosUsu
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid de Usuarios.
        /// Modificacion:
        /// </summary>
        private void CargarDatosDet()
        {
            string[] lsNombreParametros = { "@P_codigo_verif", "@P_fecha_gas", "@P_punta" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { hdfCodigoVerif.Value, lblFechaGas.Text, hdfPunta.Value };
            try
            {
                lConexion.Abrir();
                dtgDetalle.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContEjecucion1", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgDetalle.DataBind();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCrearUsu_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_contrato_eje_usr", "@P_codigo_contrato_eje","@P_tipo_demanda","@P_sector_consumo", "@P_codigo_punto_salida", "@P_nit_usuario_no_regulado", "@P_codigo_mercado_relevante","@P_cantidad_contratada","@P_accion" //20190524 rq029-19
            };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int , SqlDbType.Int //20190524 rq029-19
            };
            string[] lValorParametros = { "0", "0", "0","0","0","", "0","0", "1" //20190524 rq029-19
            };

            var lblMensaje = new StringBuilder();
            string[] lsUsuario;
            lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
            try
            {
                //20190524 rq029-19
                if (ddlDemanda.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el tipo de demanda<br>");
                if (ddlSector.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el sector de consumo<br>");
                if (ddlSalida.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el punto de salida<br>");
                //20190524 fin rq029-19
                lConexion.Abrir();
                if (TxtUsuarioFinal.Enabled)
                {
                    if (TxtUsuarioFinal.Text.Trim() == "")
                        lblMensaje.Append("Debe Ingresar el Usuario no regulado<br>");
                    else
                    {

                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                        if (!lLector.HasRows)
                            lblMensaje.Append("El Usuario Ingresado NO existe en la Base de Datos.! <br>");
                        lLector.Close();
                    }
                }
                if (ddlMercado.Enabled)
                {
                    if (ddlMercado.SelectedValue == "0")
                        lblMensaje.Append("Debe Seleccionar el mercado relevante. <br> ");
                }
                //20190524 rq029-19
                if (TxtCantidadUsr.Text == "")
                    lblMensaje.Append("Debe ingresar la cantidad contratada. <br> ");
                else
                {
                    int liValor = Convert.ToInt32(TxtCantidadUsr.Text.Trim());
                    if (liValor <= 0)
                        lblMensaje.Append("La cantidad contratada debe ser mayor que cero. <br> ");
                    //else
                    //    if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(hdfCantidad.Value))
                    //    lblMensaje.Append("La cantidad Acumulada de Usuarios No puede ser Mayor que " + hdfCantidad.Value + ". <br> ";
                }
                //20190524 rq029-19
                if (hdfUltDia.Value == "S" && Convert.ToDateTime(hdfHora.Value) < Convert.ToDateTime(DateTime.Now.ToString("HH:mm")))
                    lblMensaje.Append("Está fuera del horario para registro de ejecución de contrato");

                if (lblMensaje.ToString() == "")
                {
                    if (hdfCodigoEjec.Value != "")
                        lValorParametros[1] = hdfCodigoEjec.Value;
                    lValorParametros[2] = ddlDemanda.SelectedValue; //20190524 rq029-19
                    lValorParametros[3] = ddlSector.SelectedValue; //20190524 rq029-19
                    lValorParametros[4] = ddlSalida.SelectedValue; //20190524 rq029-19
                    lValorParametros[5] = lsUsuario[0].Trim();
                    lValorParametros[6] = ddlMercado.SelectedValue;
                    lValorParametros[7] = TxtCantidadUsr.Text; //20190524 rq029-19

                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContEjecUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                    {
                        lblMensaje.Append("Se presentó un Problema en la Creación de la Información del Usuario.!" + goInfo.mensaje_error);
                        lConexion.Cerrar();
                    }
                    else
                    {
                        ToastrEvent?.Invoke("Información de Usuarios Finales Ingresada Correctamente.!", EnumTypeToastr.Success);
                        limpiarDatosUsu(); //20190524 rq029-19
                        CargarDatosUsu();
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnActualUsu_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_contrato_eje_usr", "@P_codigo_contrato_eje","@P_tipo_demanda","@P_sector_consumo", "@P_codigo_punto_salida", "@P_nit_usuario_no_regulado", "@P_codigo_mercado_relevante","@P_cantidad_contratada","@P_accion" //20190524 rq029-19
            };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int //20190524 rq029-19
            };
            string[] lValorParametros = { "0", "0","0","0","0", "", "0","0", "2" //20190524 rq029-19
            };
            var lblMensaje = new StringBuilder();
            string[] lsUsuario;
            lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
            try
            {
                //20190524 rq029-19
                if (ddlDemanda.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el tipo de demanda<br>");
                if (ddlSector.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el sector de consumo<br>");
                if (ddlSalida.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el punto de salida<br>");
                //20190524 fin rq029-19
                lConexion.Abrir();
                if (TxtUsuarioFinal.Enabled)
                {
                    if (TxtUsuarioFinal.Text.Trim() == "")
                        lblMensaje.Append("Debe Ingresar el Usuario no regulado<br>");
                    else
                    {

                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                        if (!lLector.HasRows)
                            lblMensaje.Append("El Usuario Ingresado NO existe en la Base de Datos.! <br>");
                        lLector.Close();
                    }
                }
                if (ddlMercado.Enabled)
                {
                    if (ddlMercado.SelectedValue == "0")
                        lblMensaje.Append("Debe Seleccionar el mercado relevante. <br> ");
                }
                //20190524 rq029-19
                if (TxtCantidadUsr.Text == "")
                    lblMensaje.Append("Debe ingresar la cantidad contratada. <br> ");
                else
                {
                    int liValor = Convert.ToInt32(TxtCantidadUsr.Text.Trim());
                    if (liValor <= 0)
                        lblMensaje.Append("La cantidad contratada debe ser mayor que cero. <br> ");
                    //else
                    //    if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(hdfCantidad.Value))
                    //    lblMensaje.Append("La cantidad Acumulada de Usuarios No puede ser Mayor que " + hdfCantidad.Value + ". <br> ";
                }
                //20190524 rq029-19
                if (hdfUltDia.Value == "S" && Convert.ToDateTime(hdfHora.Value) < Convert.ToDateTime(DateTime.Now.ToString("HH:mm")))
                    lblMensaje.Append("Está fuera del horario para registro de ejecución de contrato");
                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[0] = hdfCodDatUsu.Value;
                    if (hdfCodigoEjec.Value != "")
                        lValorParametros[1] = hdfCodigoEjec.Value;
                    lValorParametros[2] = ddlDemanda.SelectedValue; //20190524 rq029-19
                    lValorParametros[3] = ddlSector.SelectedValue; //20190524 rq029-19
                    lValorParametros[4] = ddlSalida.SelectedValue; //20190524 rq029-19
                    lValorParametros[5] = lsUsuario[0].Trim();
                    lValorParametros[6] = ddlMercado.SelectedValue;
                    lValorParametros[7] = TxtCantidadUsr.Text; //20190524 rq029-19
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContEjecUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                    {
                        lblMensaje.Append("Se presentó un Problema en la Actualización de la Información del Usuario.! " + goInfo.mensaje_error);
                        lConexion.Cerrar();
                    }
                    else
                    {
                        ToastrEvent?.Invoke("Información de Usuarios Finales Actualizada Correctamente.!", EnumTypeToastr.Success);
                        limpiarDatosUsu();//20190524 rq029-19
                        btnCrearUsu.Visible = true;
                        btnActualUsu.Visible = false;
                        CargarDatosUsu();
                        btnActualUsu.Visible = false;
                        btnCrearUsu.Visible = true;
                    }
                }
                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCrear_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_contrato_eje", "@P_codigo_operador", "@P_punta", "@P_numero_contrato", "@P_codigo_verif", "@P_contrato_definitivo", "@P_codigo_punto", "@P_cantidad_autorizada", "@P_valor_facturado", "@P_fecha_gas" }; //20190524 rq029-19
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar }; //20190524 rq029-19
            string[] lValorParametros = { "0", "0", "", "0", "0", "", "0", "0", "0", "" }; //20190524 rq029-19
            var lblMensaje = new StringBuilder();
            try
            {
                lblMensaje.Append(validaciones());
                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[0] = "0";
                    lValorParametros[1] = ddlOperador.SelectedValue;
                    lValorParametros[2] = hdfPunta.Value;
                    lValorParametros[3] = lblOperacion.Text;
                    lValorParametros[4] = hdfCodigoVerif.Value;
                    lValorParametros[5] = lblContratoDef.Text;
                    lValorParametros[6] = ddlPunto.SelectedValue;
                    lValorParametros[7] = TxtCantidad.Text;
                    lValorParametros[8] = TxtValor.Text;
                    //lValorParametros[9] = ddlDemanda.SelectedValue; //20190524 rq029-19
                    //lValorParametros[10] = ddlSector.SelectedValue; //20190524 rq029-19
                    lValorParametros[9] = lblFechaGas.Text; //20190524 rq029-19
                    lConexion.Abrir();
                    goInfo.mensaje_error = "";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetContEjec", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        lblMensaje.Append("Se presentó un Problema en la Creación del Registro del Contrato.! " + goInfo.mensaje_error);
                        lConexion.Cerrar();
                    }
                    else
                    {
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                if (lLector["ind_error"].ToString() == "S")
                                    lblMensaje.Append(lLector["error"] + "<br>");
                                else
                                {
                                    hdfCodigoEjec.Value = lLector["codigo"].ToString();
                                    ToastrEvent?.Invoke("Registro Ingresado Correctamente.!", EnumTypeToastr.Success);
                                    //Cierra el modal de Registro
                                    ModalEvent?.Invoke(modContratosReg.ID, modContratosRegInside.ID, EnumTypeModal.Cerrar);
                                    hdfCodigoEjec.Value = "0";
                                    if (hdfPunta.Value == "C")
                                        CargarDatosUsu();
                                    limpiarDatos();
                                    limpiarDatosUsu();
                                    CargarDatosDet();
                                }
                            }
                        }
                        else
                        {
                            ToastrEvent?.Invoke("Registro Ingresado Correctamente.!", EnumTypeToastr.Success);
                            //Cierra el modal de Registro
                            ModalEvent?.Invoke(modContratosReg.ID, modContratosRegInside.ID, EnumTypeModal.Cerrar);
                            limpiarDatos();
                        }
                        lConexion.Cerrar();
                        btnConsultar_Click(null, null);
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_contrato_eje", "@P_codigo_operador", "@P_punta", "@P_numero_contrato", "@P_codigo_verif", "@P_contrato_definitivo", "@P_codigo_punto", "@P_cantidad_autorizada", "@P_valor_facturado", "@P_fecha_gas" }; //20190524 rq029-19
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar }; //20190524 rq029-19
            string[] lValorParametros = { "0", "0", "", "0", "0", "", "0", "0", "0", "" }; //20190524 rq029-19
            try
            {
                var lblMensaje = new StringBuilder();
                lblMensaje.Append(validaciones());
                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[0] = hdfCodigoEjec.Value;
                    lValorParametros[1] = ddlOperador.SelectedValue;
                    lValorParametros[2] = hdfPunta.Value;
                    lValorParametros[3] = lblOperacion.Text;
                    lValorParametros[4] = hdfCodigoVerif.Value;
                    lValorParametros[5] = lblContratoDef.Text;
                    lValorParametros[6] = ddlPunto.SelectedValue;
                    lValorParametros[7] = TxtCantidad.Text;
                    lValorParametros[8] = TxtValor.Text;
                    //lValorParametros[9] = ddlDemanda.SelectedValue; //20190524 rq029-19
                    //lValorParametros[10] = ddlSector.SelectedValue;  //20190524 rq029-19
                    lValorParametros[9] = lblFechaGas.Text; //20190524 rq029-19
                    lConexion.Abrir();
                    goInfo.mensaje_error = "";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetContEjec", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        lblMensaje.Append("Se presentó un Problema en la Modificación del Registro.! " + goInfo.mensaje_error);
                        lConexion.Cerrar();
                    }
                    else
                    {
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                                lblMensaje.Append(lLector["error"] + "<br>");
                            lConexion.Cerrar();
                        }
                        else
                        {
                            ToastrEvent?.Invoke("Registro Modificado Correctamente.!", EnumTypeToastr.Success);
                            limpiarDatosUsu();
                            //Cierra el modal de Registro
                            ModalEvent?.Invoke(modContratosReg.ID, modContratosRegInside.ID, EnumTypeModal.Cerrar);
                            tblDetalle.Visible = false;
                            tblDemanda.Visible = false;
                            lConexion.Cerrar();
                            CargarDatos();
                        }
                    }
                }
                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            //Cierra el modal de Registro
            ModalEvent?.Invoke(modContratosReg.ID, modContratosRegInside.ID, EnumTypeModal.Cerrar);
            tblDetalle.Visible = false;
            TxtCantidad.Text = "";
            TxtValor.Text = "";
            ddlPunto.SelectedValue = "0";
            ddlSector.SelectedValue = "0";
            ddlDemanda.SelectedValue = "0";
            CargarDatos();
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lsTable, string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia(lsTable, lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: limpiarDatosUsu
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected void limpiarDatosUsu()
        {
            ddlDemanda.SelectedValue = "0"; //20190524 rq029-19
            ddlSector.SelectedValue = "0";//20190524 rq029-19
            ddlSalida.SelectedValue = "0";//20190524 rq029-19
            ddlMercado.SelectedValue = "0";
            TxtUsuarioFinal.Text = "";
            TxtCantidadUsr.Text = "";//20190524 rq029-19
        }

        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected void limpiarDatos()
        {
            ddlPunto.SelectedValue = "0";
            TxtCantidad.Text = "";
            TxtValor.Text = "";
            //ddlDemanda.SelectedValue = "0"; //20190524 rq029-19
            //ddlSector.SelectedValue = "0"; //20190524 rq029-19
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlDemanda_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlDemanda.SelectedValue == "1")
            {
                TxtUsuarioFinal.Text = "";
                TxtUsuarioFinal.Enabled = false;
                ddlMercado.Enabled = true;
            }
            else
            {
                TxtUsuarioFinal.Enabled = true;
                ddlMercado.SelectedValue = "0";
                ddlMercado.Enabled = false;
            }
            //20190507 rq023-19
            //if (hdfActBorrUsr.Value == "S") //20190507 rq023-19
            //{ //20190507 rq023-19
            //    string[] lsNombreParametros = { "@P_codigo_contrato_eje", "@P_accion" };
            //    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            //    string[] lValorParametros = { hdfCodigoEjec.Value, "4" };

            //    try
            //    {
            //        lConexion.Abrir();
            //        DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContEjecUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
            //        CargarDatosUsu();
            //    }
            //    catch (Exception ex)
            //    {
            //    }
            //}//20190507 rq023-19 
        }

        //20190524 rq029-19
        protected void TxtCantidad_TextChanged(object sender, EventArgs e)
        {
            hdfCantidad.Value = TxtCantidad.Text;
            if (hdfCantidad.Value == "")
                hdfCantidad.Value = "0";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnConsultar_Click(object sender, EventArgs e)
        {
            dtgConsulta.CurrentPageIndex = 0;
            CargarDatos();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void lkbExcel_Click(object sender, EventArgs e)
        {
            string lsNombreArchivo = goInfo.Usuario + "InfEjecucion" + DateTime.Now + ".xls";
            string lstitulo_informe = "";
            string lsTituloParametros = "";
            try
            {
                lstitulo_informe = "Consulta Ingreso Ejecución de contratos";
                lsTituloParametros = "";

                if (TxtNoContrato.Text.Trim().Length > 0)
                    lsTituloParametros = "No. Operación Inicial: " + TxtNoContrato.Text.Trim();
                if (TxtNoContratoFin.Text.Trim().Length > 0)
                    lsTituloParametros += " - No. Operación Final: " + TxtNoContratoFin.Text.Trim();
                if (TxtContratoDef.Text.Trim().Length > 0)
                    lsTituloParametros += " - Contrato: " + TxtContratoDef.Text.Trim();
                if (ddlOperador.SelectedValue != "0")
                    lsTituloParametros += " - Operador: " + ddlOperador.SelectedItem;
                if (ddlSubasta.SelectedValue != "0")
                    lsTituloParametros += " - Subasta: " + ddlSubasta.SelectedItem;
                if (ddlMercado.SelectedValue != "0")
                    lsTituloParametros += " - Tipo Mercado: " + ddlMercado.SelectedItem;
                if (ddlProducto.SelectedValue != "0")
                    lsTituloParametros += " - Producto: " + ddlProducto.SelectedItem;
                if (ddlModalidad.SelectedValue != "0")
                    lsTituloParametros += " - Modalidad Contractual: " + ddlModalidad.SelectedItem;
                decimal ldCapacidad = 0;
                StringBuilder lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                dtgExcel.Visible = true;
                dtgExcel.EnableViewState = false;
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                lform.Controls.Add(dtgExcel);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());

                Response.End();
                lds.Dispose();
                lConexion.CerrarInforme();

                dtgExcel.Visible = true;
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("No se Pudo Generar el Excel.!" + ex.Message, EnumTypeToastr.Error);
            }
        }
    }
}