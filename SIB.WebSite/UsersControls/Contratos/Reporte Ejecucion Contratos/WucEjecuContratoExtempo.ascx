﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucEjecuContratoExtempo.ascx.cs" Inherits="UsersControls.Contratos.Reporte_Ejecucion_Contratos.WucEjecuContratoExtempo" %>

<%--Contenido--%>
<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Número Contrato" AssociatedControlID="TxtContratoDef" runat="server" />
            <asp:TextBox ID="TxtContratoDef" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Número Operación" AssociatedControlID="TxtNoContrato" runat="server" />
            <asp:TextBox ID="TxtNoContrato" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
            <asp:CompareValidator ID="CvTxtNoContrato" runat="server" ControlToValidate="TxtNoContrato"
                Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Número Operación debe Ser numérico">*</asp:CompareValidator>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Código Ejecución" AssociatedControlID="TxtCodEjec" runat="server" />
            <asp:TextBox ID="TxtCodEjec" type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Id Registro" AssociatedControlID="TxtCodigoVerif" runat="server" />
            <asp:TextBox ID="TxtCodigoVerif" type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Fecha Registro Ejecución Inicial" AssociatedControlID="TxtFechaIni" runat="server" />
            <asp:TextBox ID="TxtFechaIni" runat="server" Width="100%" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Fecha Registro Ejecución Final" AssociatedControlID="TxtFechaFin" runat="server" />
            <asp:TextBox ID="TxtFechaFin" runat="server" Width="100%" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
            <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Estado" AssociatedControlID="ddlEstado" runat="server" />
            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
</div>
<div class="table table-responsive">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:DataGrid ID="dtgConsulta" Visible="False" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                Width="100%" CssClass="table-bordered"
                OnItemCommand="dtgConsulta_EditCommand" AllowPaging="true" PageSize="10" OnPageIndexChanged="dtgConsulta_PageIndexChanged">
                <Columns>
                    <%--0--%>
                    <asp:BoundColumn DataField="fecha" HeaderText="Fecha Gas" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <%--1--%>
                    <asp:BoundColumn DataField="desc_subasta" HeaderText="Subasta"></asp:BoundColumn>
                    <%--2--%>
                    <asp:BoundColumn DataField="desc_mercado" HeaderText="Mercado"></asp:BoundColumn>
                    <%--3--%>
                    <asp:BoundColumn DataField="desc_producto" HeaderText="Producto"></asp:BoundColumn>
                    <%--4--%>
                    <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual"></asp:BoundColumn>
                    <%--5--%>
                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--6--%>
                    <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato Definitivo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--7--%>
                    <asp:BoundColumn DataField="codigo_punto" HeaderText="Código Punto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--8--%>
                    <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--9--%>
                    <asp:BoundColumn DataField="operador_compra" HeaderText="Código Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--10--%>
                    <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Comprador" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                    <%--11--%>
                    <asp:BoundColumn DataField="operador_venta" HeaderText="Código Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--12--%>
                    <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--13--%>
                    <asp:BoundColumn DataField="cantidad_compra" HeaderText="Cantidad Compra" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                    <%--14--%>
                    <asp:BoundColumn DataField="cantidad_venta" HeaderText="Cantidad Venta" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                    <%--15--%>
                    <asp:BoundColumn DataField="Valor_compra" HeaderText="Valor Compra" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                    <%--16--%>
                    <asp:BoundColumn DataField="Valor_venta" HeaderText="Valor Venta" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                    <%--17--%>
                    <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--18--%>
                    <asp:BoundColumn DataField="estado" Visible="false"></asp:BoundColumn>
                    <%--19--%>
                    <asp:BoundColumn DataField="ind_modifica" Visible="false"></asp:BoundColumn>
                    <%--20--%>
                    <asp:BoundColumn DataField="cantidad" Visible="false"></asp:BoundColumn>
                    <%--21--%>
                    <asp:BoundColumn DataField="precio" Visible="false"></asp:BoundColumn>
                    <%--22--%>
                    <asp:BoundColumn DataField="estado" Visible="false"></asp:BoundColumn>
                    <%--23--%>
                    <asp:BoundColumn DataField="punta" Visible="false"></asp:BoundColumn>
                    <%--24--%>
                    <asp:BoundColumn DataField="codigo_verif_contrato" Visible="false"></asp:BoundColumn>
                    <%--25--%>
                    <asp:BoundColumn DataField="codigo_contrato_eje" Visible="false"></asp:BoundColumn>
                    <%--26--%>
                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                        <ItemTemplate>
                            <div class="dropdown dropdown-inline">
                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="flaticon-more-1"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                    <!--begin::Nav-->
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <asp:LinkButton ID="lkbAgregar" CssClass="kt-nav__link" CommandName="Agregar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-crisp-icons"></i>
                                                            <span class="kt-nav__link-text">Agregar</span>
                                                    </asp:LinkButton>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Modificar</span>
                                                    </asp:LinkButton>
                                                </li>
                                            </ul>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <!--end::Nav-->
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
            </asp:DataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--20190524 rq029-19--%>
</div>
<%--Excel--%>
<asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False" Visible="false" Width="100%" CssClass="table-bordered">
    <Columns>
        <asp:BoundColumn DataField="fecha" HeaderText="Fecha Gas" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
        <asp:BoundColumn DataField="desc_subasta" HeaderText="Subasta"></asp:BoundColumn>
        <asp:BoundColumn DataField="desc_mercado" HeaderText="Mercado"></asp:BoundColumn>
        <asp:BoundColumn DataField="desc_producto" HeaderText="Producto"></asp:BoundColumn>
        <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual"></asp:BoundColumn>
        <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato Definitivo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <asp:BoundColumn DataField="codigo_punto" HeaderText="Código Punto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <asp:BoundColumn DataField="operador_compra" HeaderText="Código Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Comprador" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
        <asp:BoundColumn DataField="operador_venta" HeaderText="Código Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <asp:BoundColumn DataField="cantidad_compra" HeaderText="Cantidad Compra" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
        <asp:BoundColumn DataField="cantidad_venta" HeaderText="Cantidad Venta" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
        <asp:BoundColumn DataField="Valor_compra" HeaderText="Valor Compra" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
        <asp:BoundColumn DataField="Valor_venta" HeaderText="Valor Venta" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
        <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
    </Columns>
    <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
</asp:DataGrid>

<%--Modals--%>
<%--Registro Contrato--%>
<div class="modal fade" id="modContratosReg" tabindex="-1" role="dialog" aria-labelledby="modContratosRegLabel" aria-hidden="true" clientidmode="Static" style="overflow-y: auto" runat="server">
    <div class="modal-dialog" id="modContratosRegInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="modContratosRegLabel">Registro de Contrato</h5>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <%--Infirmacion Basica--%>
                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5>Información Básica</h5>
                                    </div>
                                    <div class="panel-body">
                                        <%--Contenido--%>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Operación</label>
                                                    <asp:Label ID="lblOperacion" runat="server" CssClass="form-control"></asp:Label>-<asp:Label ID="lblPunta"
                                                        runat="server" CssClass="form-control"></asp:Label>
                                                    <asp:HiddenField ID="hdfPunta" runat="server" />
                                                    <asp:HiddenField ID="hdfCodigoEjec" runat="server" />
                                                    <asp:HiddenField ID="hdfCantidad" runat="server" />
                                                    <asp:HiddenField ID="hdfCodDatUsu" runat="server" />
                                                    <asp:HiddenField ID="hdfCodigoVerif" runat="server" />
                                                    <%--20190524 rq029-19--%>
                                                    <asp:HiddenField ID="hdfUltDia" runat="server" />
                                                    <asp:HiddenField ID="hdfHora" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Número de contrato</label>
                                                    <asp:Label ID="lblContratoDef" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Subasta</label>
                                                    <asp:Label ID="lblSubasta" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Tipo Mercado</label>
                                                    <asp:Label ID="lblMercado" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Producto</label>
                                                    <asp:Label ID="lblProducto" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Modalidad Contractual</label>
                                                    <asp:Label ID="lblModalidad" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Cantidad</label>
                                                    <asp:Label ID="lblCantidad" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Precio del Contrato</label>
                                                    <asp:Label ID="lblPrecio" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Fecha Gas</label>
                                                    <asp:Label ID="lblFechaGas" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Punto de entrega</label>
                                                    <asp:DropDownList ID="ddlPunto" runat="server" CssClass="form-control selectpicker" data-live-search="true"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Cantidad Autorizada</label>
                                                    <asp:TextBox ID="TxtCantidad" runat="server" ValidationGroup="detalle" CssClass="form-control" OnTextChanged="TxtCantidad_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                    <asp:CompareValidator ID="CvTxtCantidad" runat="server" ControlToValidate="TxtCantidad"
                                                        Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Cantidad Autorizada debe Ser numérico">El Campo Cantidad Autorizada debe Ser numérico</asp:CompareValidator>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Valor Facturado</label>
                                                    <asp:TextBox ID="TxtValor" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                                                    <%--20201214--%>
                                                     <ajaxToolkit:FilteredTextBoxExtender ID="FTBETxtValor" runat="server" TargetControlID="TxtValor"
                                                        FilterType="Custom, Numbers" ValidChars="."></ajaxToolkit:FilteredTextBoxExtender>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <%--Demanda--%>
                            <div id="tblDemanda" visible="false" runat="server">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5>Usuarios Finales</h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <%--20190524 rq029-19--%>
                                                    <label>Tipo Demanda</label>
                                                    <asp:DropDownList ID="ddlDemanda" runat="server" OnSelectedIndexChanged="ddlDemanda_SelectedIndexChanged"
                                                        AutoPostBack="true" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Sector de Consumo</label>
                                                    <asp:DropDownList ID="ddlSector" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Punto de Salida</label>
                                                    <asp:DropDownList ID="ddlSalida" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <%--20190524  fin rq029-19--%>
                                                    <label>Usuario Final</label>
                                                    <asp:TextBox ID="TxtUsuarioFinal" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <ajaxToolkit:AutoCompleteExtender runat="server" BehaviorID="AutoCompleteExUsuF" ID="AutoCompleteExtender1"
                                                        TargetControlID="TxtUsuarioFinal" ServicePath="~/WebService/AutoComplete.asmx"
                                                        ServiceMethod="GetCompletionListUsuarioFinal" MinimumPrefixLength="3" CompletionInterval="1000"
                                                        EnableCaching="true" CompletionSetCount="20" CompletionListCssClass="autocomplete_completionListElement"
                                                        CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                        DelimiterCharacters=";,:">
                                                        <Animations>
                        <OnShow>
                            <Sequence>
                                <OpacityAction Opacity="0" />
                                <HideAction Visible="true" />
                                                                <ScriptAction Script="
                                    // Cache the size and setup the initial size
                                    var behavior = $find('AutoCompleteExUsuF');
                                    if (!behavior._height) {
                                        var target = behavior.get_completionList();
                                        behavior._height = target.offsetHeight - 2;
                                        target.style.height = '0px';
                                    }" />
                                
                                <Parallel Duration=".4">
                                    <FadeIn />
                                    <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteExUsuF')._height" />
                                </Parallel>
                            </Sequence>
                        </OnShow>
                        <OnHide>
                            <Parallel Duration=".4">
                                <FadeOut />
                                <Length PropertyKey="height" StartValueScript="$find('AutoCompleteExUsuF')._height" EndValue="0" />
                            </Parallel>
                        </OnHide>
                                                        </Animations>
                                                    </ajaxToolkit:AutoCompleteExtender>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Mercado Relevante</label>
                                                    <asp:DropDownList ID="ddlMercado" runat="server" CssClass="form-control selectpicker" data-live-search="true"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <%--20190524 rq029-19--%>
                                                    <label>Cantidad Contratada</label>
                                                    <asp:TextBox ID="TxtCantidadUsr" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                                                    <asp:CompareValidator ID="CvTxtCantidadUsr" runat="server" ControlToValidate="TxtCantidadUsr"
                                                        Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Cantidad Contratada debe Ser numérico">El Campo Cantidad Contratada debe Ser numérico</asp:CompareValidator>
                                                </div>
                                            </div>
                                        </div>

                                        <asp:Button ID="btnCrearUsu" runat="server" CssClass="btn btn-primary btn-block" Text="Agregar Usuario" OnClick="btnCrearUsu_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" Visible="false" />
                                        <asp:Button ID="btnActualUsu" runat="server" CssClass="btn btn-primary btn-lg btn-block" Text="Actualiza Usuario" OnClick="btnActualUsu_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" Visible="false" />

                                        <hr>

                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <b>TOTAL CANTIDAD:</b><asp:Label ID="lblTotlCantidad" runat="server" ForeColor="Red"
                                                        Font-Bold="true"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table table-responsive">
                                            <asp:DataGrid ID="dtgUsuarios" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                                Width="100%" CssClass="table-bordered" OnItemCommand="dtgUsuarios_EditCommand">
                                                <Columns>
                                                    <%--0--%>
                                                    <asp:BoundColumn DataField="codigo_contrato_eje_usr" Visible="false"></asp:BoundColumn>
                                                    <%--20190524 rq029-19--%>
                                                    <%--1--%>
                                                    <asp:BoundColumn DataField="tipo_demanda" HeaderText="Código Tipo Demanda"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--2--%>
                                                    <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--3--%>
                                                    <asp:BoundColumn DataField="sector_consumo" HeaderText="Código Sector Consumo"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--4--%>
                                                    <asp:BoundColumn DataField="desc_sector" HeaderText="Sector consumo"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>

                                                    <%--5--%>
                                                    <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Código Punto Salida"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--6--%>
                                                    <asp:BoundColumn DataField="desc_salida" HeaderText="Punto de Salida"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--20190524 fin  rq029-19--%>
                                                    <%--7--%>
                                                    <asp:BoundColumn DataField="nit_usuario_no_regulado" HeaderText="Identificacion Usuario Final"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--8--%>
                                                    <asp:BoundColumn DataField="nombre_usuario" HeaderText="Nombre Usuario"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--9--%>
                                                    <asp:BoundColumn DataField="codigo_mercado_relevante" HeaderText="Código Mercado Relevante" ItemStyle-HorizontalAlign="Left"
                                                        ItemStyle-Width="80px"></asp:BoundColumn>
                                                    <%--10--%>
                                                    <asp:BoundColumn DataField="desc_mercado" HeaderText="Mercado Relevante" ItemStyle-HorizontalAlign="Left"
                                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                                    <%--20190524 rq029-19--%>
                                                    <%--11--%>
                                                    <asp:BoundColumn DataField="cantidad_contratada" HeaderText="Cantidad Contratada" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                                    <%--12--%>
                                                    <%--20210107--%>
                                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                                        <ItemTemplate>
                                                            <div class="dropdown dropdown-inline">
                                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="flaticon-more-1"></i>
                                                                </button>
                                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                                    <!--begin::Nav-->
                                                                    <ul class="kt-nav">
                                                                        <li class="kt-nav__item">
                                                                            <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Modificar</span>
                                                                            </asp:LinkButton>
                                                                        </li>
                                                                        <li class="kt-nav__item">
                                                                            <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon-delete"></i>
                                                            <span class="kt-nav__link-text">Eliminar</span>
                                                                            </asp:LinkButton>
                                                                        </li>
                                                                    </ul>
                                                                    <!--end::Nav-->
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <%--Detalle--%>
                            <div id="tblDetalle" visible="false" runat="server">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5>Puntos Ingresados</h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="table table-responsive">
                                            <asp:DataGrid ID="dtgDetalle" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                                Width="100%" CssClass="table-bordered">
                                                <Columns>
                                                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Operación"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fecha_gas" HeaderText="Fecha Gas" DataFormatString="{0:yyyy/MM/dd}"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="codigo_Punto" HeaderText="Código Punto"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrtega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--20190524 rq029-19--%>
                                                    <%--<asp:BoundColumn DataField="tipo_demanda" HeaderText="Código Tipo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="sector_consumo" HeaderText="Código Sector Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_sector" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>--%>
                                                    <%--20190524  FIn rq029-19--%>
                                                    <asp:BoundColumn DataField="cantidad_autorizada" HeaderText="Cantidad Autorizada" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="valor_facturado" HeaderText="Valor Facturado" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0.00}"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                </Columns>
                                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnCancelar" runat="server" class="btn btn-secondary" Text="Cancelar" OnClick="btnRegresar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnCrear" runat="server" CssClass="btn btn-primary" Text="Crear" OnClick="btnCrear_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnActualizar" runat="server" CssClass="btn btn-primary" Text="Actualizar" OnClick="btnActualizar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
