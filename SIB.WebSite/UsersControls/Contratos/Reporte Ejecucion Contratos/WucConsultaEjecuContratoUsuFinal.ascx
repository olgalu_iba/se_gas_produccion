﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucConsultaEjecuContratoUsuFinal.ascx.cs" Inherits="UsersControls.Contratos.Reporte_Ejecucion_Contratos.WucConsultaEjecuContratoUsuFinal" %>

<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <label for="Name">Fecha Inicial</label>
            <asp:TextBox ID="TxtFechaIni" runat="server" placeholder="yyyy/mm/dd" Width="100%" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <label for="Name">Fecha Final</label>
            <asp:TextBox ID="TxtFechaFin" runat="server" placeholder="yyyy/mm/dd" Width="100%" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div id="tdMes01" class="form-group" runat="server">
            <asp:Label Text="Número Operación" AssociatedControlID="TxtOperacion" runat="server" />
            <asp:TextBox ID="TxtOperacion" type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" ValidationGroup="detalle" MaxLength="10" Width="100%" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Contrato Definitivo" AssociatedControlID="TxtContratoDef" runat="server" />
            <asp:TextBox ID="TxtContratoDef" runat="server" ValidationGroup="detalle" MaxLength="30" Width="100%" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Operador" AssociatedControlID="DdlOperador" runat="server" />
            <asp:DropDownList ID="DdlOperador" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
</div>
<div class="table table-responsive">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:DataGrid ID="dtgMaestro" Visible="False" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table-bordered" AllowPaging="false"
                PagerStyle-HorizontalAlign="Center">
                <Columns>
                    <asp:BoundColumn DataField="fecha_gas" HeaderText="Fecha gas" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="desc_subasta" HeaderText="Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="desc_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                    <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato Definitivo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--20190524 rq029-19--%>
                    <asp:BoundColumn DataField="codigo_contrato_eje" HeaderText="Código Ejecución Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="codigo_punto" HeaderText="Código Punto" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                    <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="tipo_demanda" HeaderText="Código tipo demanda" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                    <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="sector_consumo" HeaderText="Código Sector Consumo" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                    <asp:BoundColumn DataField="desc_sector" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--20190524 rq029-19--%>
                    <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Código Punto Salida" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                    <asp:BoundColumn DataField="desc_salida" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--20190524 rq029-19--%>
                    <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Comprador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                    <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="operador_venta" HeaderText="Código Vendedor" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                    <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="cantidad_autorizada" HeaderText="Cantidad Compra" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="Cantidad_v" HeaderText="Cantidad Venta" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="valor_facturado" HeaderText="valor Compra" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0.00}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="valor_v" HeaderText="valor venta" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0.00}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="ind_modif" HeaderText="Modificado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="nit_usuario_no_regulado" HeaderText="Nit Usuario Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="nombre_usuario_final" HeaderText="Nombre Usuario Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="codigo_mercado_relevante" HeaderText="Código Mercado Relevante" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                    <asp:BoundColumn DataField="desc_mercado_relev" HeaderText="Mercado Relevante" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--20190524 rq029-19--%>
                    <asp:BoundColumn DataField="cantidad_contratada" HeaderText="Cantidad contratada" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                    <%--20190524 rq029-19--%>
                </Columns>
                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
            </asp:DataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

