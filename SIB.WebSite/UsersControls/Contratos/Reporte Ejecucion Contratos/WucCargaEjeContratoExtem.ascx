﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucCargaEjeContratoExtem.ascx.cs" Inherits="UsersControls.Contratos.Reporte_Ejecucion_Contratos.WucCargaEjeContratoExtem" %>

<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Archivo Ejecución" AssociatedControlID="FuArchivo" runat="server" />
            <asp:FileUpload ID="FuArchivo" CssClass="form-control" Width="100%" runat="server" EnableTheming="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Archivo Usuarios Finales (Solo Compradores)" AssociatedControlID="FuArchivoUsuarios" runat="server" />
            <asp:FileUpload ID="FuArchivoUsuarios" CssClass="form-control" Width="100%" runat="server" EnableTheming="true" />
        </div>
    </div>
</div>
