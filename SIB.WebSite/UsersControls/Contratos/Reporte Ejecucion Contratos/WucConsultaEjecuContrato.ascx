﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucConsultaEjecuContrato.ascx.cs" Inherits="UsersControls.Contratos.Reporte_Ejecucion_Contratos.WucConsultaEjecuContrato" %>

<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Fecha Inicial de Vigencia" AssociatedControlID="TxtFechaIni" runat="server" />
            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control datepicker" ClientIDMode="Static" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Fecha Final de Vigencia" AssociatedControlID="TxtFechaFin" runat="server" />
            <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control datepicker" ClientIDMode="Static" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div id="tdMes01" class="form-group" runat="server">
            <asp:Label Text="Número Operación" AssociatedControlID="TxtOperacion" runat="server" />
            <asp:TextBox ID="TxtOperacion" type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" ValidationGroup="detalle" MaxLength="10" Width="100%" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Contrato Definitivo" AssociatedControlID="TxtContratoDef" runat="server" />
            <asp:TextBox ID="TxtContratoDef" runat="server" ValidationGroup="detalle" MaxLength="30" Width="100%" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Punta" AssociatedControlID="DdlPunta" runat="server" />
            <asp:DropDownList ID="DdlPunta" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                <asp:ListItem Value="">Seleccione</asp:ListItem>
                <asp:ListItem Value="C">Comprador</asp:ListItem>
                <asp:ListItem Value="V">Vendedor</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Operador" AssociatedControlID="DdlOperador" runat="server" />
            <asp:DropDownList ID="DdlOperador" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
</div>
<div class="table table-responsive">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:DataGrid ID="dtgMaestro" Visible="False" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table-bordered" AllowPaging="false"
                PagerStyle-HorizontalAlign="Center">
                <Columns>
                    <asp:BoundColumn DataField="fecha_gas" HeaderText="Fecha gas" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                    <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato Definitivo" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                    <asp:BoundColumn DataField="codigo_punto" HeaderText="Código Punto" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                    <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--20190524 rq029-19--%>
                    <%--<asp:BoundColumn DataField="tipo_demanda" HeaderText="Código tipo demanda" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="sector_consumo" HeaderText="Código Sector Consumo" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_sector" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>--%>
                    <asp:BoundColumn DataField="operador_compra" HeaderText="Código Comprador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                    <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="operador_venta" HeaderText="Código Vendedor" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                    <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="cantidad_compra" HeaderText="Cantidad Compra" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="Cantidad_venta" HeaderText="Cantidad Venta" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="valor_compra" HeaderText="valor Compra" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0.00}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="valor_venta" HeaderText="valor Venta" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0.00}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="codigo_compra" HeaderText="Código Ejecución Contrato Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="codigo_venta" HeaderText="Código Ejecución Contrato Venta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="ind_modif" HeaderText="Modificado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="usuario_modif" HeaderText="Usuario Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                </Columns>
                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
            </asp:DataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
