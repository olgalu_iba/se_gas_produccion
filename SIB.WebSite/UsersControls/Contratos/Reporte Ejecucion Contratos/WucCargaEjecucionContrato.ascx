﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucCargaEjecucionContrato.ascx.cs" Inherits="UsersControls.Contratos.Reporte_Ejecucion_Contratos.WucCargaEjecucionContrato" %>

<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label ID="Label2" runat="server">  Archivo Ejecución:</asp:Label>
            <asp:FileUpload ID="FuArchivo" runat="server" EnableTheming="true" CssClass="form-control" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label ID="Label3" runat="server">Archivo Usuarios Finales (Solo Compradores):</asp:Label>
            <asp:FileUpload ID="FuArchivoUsuarios" runat="server" EnableTheming="true" CssClass="form-control" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4" style="display: none">
        <div class="form-group">
            <asp:Button ID="BtnCargar" runat="server" Text="Cargue Archivos" OnClick="BtnCargar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" /><br />
            <asp:HiddenField ID="hndID" runat="server" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label ID="ltCargaArchivo" runat="server" Width="100%" ForeColor="red"></asp:Label>
            <asp:HiddenField ID="hdfNomArchivo" runat="server" />
        </div>
    </div>
</div>
