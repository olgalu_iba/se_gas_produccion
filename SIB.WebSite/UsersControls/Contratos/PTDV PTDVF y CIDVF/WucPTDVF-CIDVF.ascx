﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucPTDVF-CIDVF.ascx.cs" Inherits="UsersControls.Contratos.PTDV_PTDVF_y_CIDVF.WucPTDVF_CIDVF" %>

<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Archivo" AssociatedControlID="FuArchivo" runat="server" />
            <asp:FileUpload ID="FuArchivo" CssClass="form-control" runat="server" Width="100%" EnableTheming="true" />
        </div>
    </div>
</div>
