﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucRepoInfoPTDVF-CIDVF.ascx.cs" Inherits="UsersControls.Contratos.PTDV_PTDVF_y_CIDVF.WucRepoInfoPTDVF_CIDVF" %>

<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Año Inicial" AssociatedControlID="TxtAnoIni" runat="server" />
            <asp:TextBox ID="TxtAnoIni" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Mes Inicial" AssociatedControlID="ddlMesIni" runat="server" />
            <asp:DropDownList ID="ddlMesIni" runat="server" Width="100%" CssClass="form-control" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Año Final" AssociatedControlID="TxtAnoFin" runat="server" />
            <asp:TextBox ID="TxtAnoFin" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Mes Final" AssociatedControlID="ddlMesFinal" runat="server" />
            <asp:DropDownList ID="ddlMesFinal" runat="server" Width="100%" CssClass="form-control" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
            <asp:DropDownList ID="ddlOperador" runat="server" Width="100%" CssClass="form-control" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Punto del SNT" AssociatedControlID="ddlPuntoSnt" runat="server" />
            <asp:DropDownList ID="ddlPuntoSnt" runat="server" Width="100%" CssClass="form-control" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Año Carga" AssociatedControlID="TxtAnoCarga" runat="server" />
            <asp:TextBox ID="TxtAnoCarga" runat="server" Width="100%" CssClass="form-control" ValidationGroup="detalle" />
        </div>
    </div>
</div>
<div class="table table-responsive">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:DataGrid ID="dtgMaestro" Visible="False" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                <Columns>
                    <asp:BoundColumn DataField="operador_del_campo" HeaderText="Operador Campo" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px"></asp:BoundColumn>
                    <asp:BoundColumn DataField="campo_punto_snt" HeaderText="Campo o Punto SNT" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px"></asp:BoundColumn>
                    <asp:BoundColumn DataField="socio_productor" HeaderText="Socio Productor incluye Estado" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px"></asp:BoundColumn>
                    <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                    <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                    <asp:BoundColumn DataField="ptdvf" HeaderText="PTDVF (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="cidvf" HeaderText="CIDVF (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                </Columns>
                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
            </asp:DataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
