﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace UsersControls.Contratos.PTDV_PTDVF_y_CIDVF
{
    public partial class WucRepoInfoPTDVF_CIDVF : UserControl
    {
        #region EventHandler

        /// <summary>
        /// Delegado para el manejo de los toastr
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        /// EventHandler para la selección de los toastr 
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        /// Delegado para el manejo del log de cargue de archivos
        /// </summary>
        public delegate void EventHandlerLogCargaArchivo(StringBuilder message);

        /// <summary>
        /// EventHandler para el log de cargue de archivos
        /// </summary>
        public event EventHandlerLogCargaArchivo LogCargaArchivoEvent;

        /// <summary>
        /// Delegado para la selección de los botones  
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get { return ViewState["NameController"] != null && !string.IsNullOrEmpty(ViewState["NameController"].ToString()) ? ViewState["NameController"].ToString() : string.Empty; }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        #endregion Propiedades

        private InfoSessionVO goInfo = null;
        private static string lsTitulo = "Reporte Información PTDVF";
        private clConexion lConexion;
        private SqlDataReader lLector;
        private DataSet lds = new DataSet();
        private SqlDataAdapter lsqldata = new SqlDataAdapter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lConexion = new clConexion(goInfo);
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            //Botones
            ButtonsEvent?.Invoke(new[] { EnumBotones.Buscar }, null, NameController);

            // Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' And codigo_operador != 0 order by codigo_operador", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlPuntoSnt, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMesIni, "m_mes", " 1=1 order by mes", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMesFinal, "m_mes", " 1=1 order by mes", 0, 1);
            lConexion.Cerrar();

            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
            }
            //Se establece que el controlador ya se inicializo    
            Inicializado = true;
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                }
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnConsultar_Click(object sender, EventArgs e)
        {
            var Error = false;
            int liValor;
            var lsCadenaArchivo = new StringBuilder();

            if (TxtAnoIni.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtAnoIni.Text);
                    if (liValor <= 0)
                    {
                        lsCadenaArchivo.Append("Valor Inválido en el Campo Año Final. <br>");
                        Error = true;
                    }
                }
                catch (Exception ex)
                {
                    lsCadenaArchivo.Append("Formato Inválido en el Campo Año Inicial. <br>");
                    Error = true;
                }
            }
            else
            {
                lsCadenaArchivo.Append("Debe Ingresar el Año Inicial. <br>");
                Error = true;
            }
            if (TxtAnoFin.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtAnoFin.Text);
                    if (liValor <= 0)
                    {
                        lsCadenaArchivo.Append("Valor Inválido en el Campo Año Final. <br>");
                        Error = true;
                    }
                }
                catch (Exception ex)
                {
                    lsCadenaArchivo.Append("Formato Inválido en el Campo Año Final. <br>");
                    Error = true;
                }
            }
            else
            {
                lsCadenaArchivo.Append("Debe Ingresar el Año Final. <br>");
                Error = true;
            }
            if (ddlMesIni.SelectedValue == "0")
            {
                lsCadenaArchivo.Append("Debe Ingresar el Mes Inicial. <br>");
                Error = true;
            }
            if (ddlMesFinal.SelectedValue == "0")
            {
                lsCadenaArchivo.Append("Debe Ingresar el Mes Final. <br>");
                Error = true;
            }
            if (TxtAnoCarga.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtAnoCarga.Text);
                    if (liValor <= 0)
                    {
                        lsCadenaArchivo.Append("Valor Inválido en el Campo Año Carga. <br>");
                        Error = true;
                    }
                }
                catch (Exception ex)
                {
                    lsCadenaArchivo.Append("Valor Inválido en el Campo Año Carga. <br>");
                    Error = true;

                }
            }
            else
            {
                lsCadenaArchivo.Append("Debe Ingresar el Año Carga. <br>");
                Error = true;
            }
            if (!Error)
            {
                try
                {
                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    SqlCommand lComando = new SqlCommand();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandTimeout = 3600;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_GetInformePTDVF";
                    lComando.Parameters.Add("@P_ano_ini", SqlDbType.Int).Value = TxtAnoIni.Text.Trim();
                    lComando.Parameters.Add("@P_mes_ini", SqlDbType.Int).Value = ddlMesIni.SelectedValue;
                    lComando.Parameters.Add("@P_ano_fin", SqlDbType.Int).Value = TxtAnoFin.Text.Trim();
                    lComando.Parameters.Add("@P_mes_fin", SqlDbType.Int).Value = ddlMesFinal.SelectedValue;
                    lComando.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = ddlOperador.SelectedValue;
                    lComando.Parameters.Add("@P_codigo_punto_snt", SqlDbType.Int).Value = ddlPuntoSnt.SelectedValue;
                    lComando.Parameters.Add("@P_ano_carga", SqlDbType.Int).Value = TxtAnoCarga.Text.Trim();
                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgMaestro.DataSource = lds;
                    dtgMaestro.DataBind();
                    dtgMaestro.Visible = true;
                    ButtonsEvent?.Invoke(new[] { EnumBotones.Buscar, EnumBotones.Excel }, null, NameController);
                    lConexion.Cerrar();
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("No se Pude Generar el Informe.! " + ex.Message, EnumTypeToastr.Error);
                }
            }
            else
            {
                ToastrEvent?.Invoke(lsCadenaArchivo.ToString(), EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnExcel_Click(object sender, EventArgs e)
        {
            string lsNombreArchivo = goInfo.Usuario + "InfInformacionPTDVF" + DateTime.Now + ".xls";
            var lstitulo_informe = "Informe Información PTDVF";
            var lsTituloParametros = "";
            try
            {
                lsTituloParametros = " Año Inicial: " + TxtAnoIni.Text + " - Mes Inicial: " + ddlMesIni.SelectedItem.ToString();
                lsTituloParametros += " - Año Final: " + TxtAnoFin.Text + " - Mes Final: " + ddlMesFinal.SelectedItem.ToString();

                if (ddlOperador.SelectedValue != "0")
                    lsTituloParametros += "  - Operador: " + ddlOperador.SelectedItem.ToString();
                if (ddlPuntoSnt.SelectedValue != "0")
                    lsTituloParametros += "  - Campo o Punto SNT: " + ddlPuntoSnt.SelectedItem.ToString();

                decimal ldCapacidad = 0;
                StringBuilder lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                dtgMaestro.EnableViewState = false;
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                lform.Controls.Add(dtgMaestro);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = System.Text.Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());

                Response.End();
                lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("No se Pude Generar el Excel.!" + ex.Message, EnumTypeToastr.Error);
            }
        }
    }
}