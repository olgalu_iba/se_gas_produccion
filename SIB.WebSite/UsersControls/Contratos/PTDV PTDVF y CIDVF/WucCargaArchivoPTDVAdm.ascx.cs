﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace UsersControls.Contratos.PTDV_PTDVF_y_CIDVF
{
    // ReSharper disable once IdentifierTypo
    // ReSharper disable once InconsistentNaming
    public partial class WucCargaArchivoPTDVAdm : UserControl
    {
        #region EventHandler

        /// <summary>
        /// Delegado para el manejo de los toastr
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        /// EventHandler para la selección de los toastr 
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        /// Delegado para el manejo del log de cargue de archivos
        /// </summary>
        public delegate void EventHandlerLogCargaArchivo(StringBuilder message);

        /// <summary>
        /// EventHandler para el log de cargue de archivos
        /// </summary>
        public event EventHandlerLogCargaArchivo LogCargaArchivoEvent;

        /// <summary>
        /// Delegado para la selección de los botones  
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        private InfoSessionVO goInfo;
        private clConexion lConexion;
        private string strRutaCarga;
        private string strRutaFTP;

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get { return ViewState["NameController"] != null && !string.IsNullOrEmpty(ViewState["NameController"].ToString()) ? ViewState["NameController"].ToString() : string.Empty; }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        #endregion Propiedades

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("~/index.aspx");
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
            strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            ButtonsEvent?.Invoke(new[] { EnumBotones.Cargue }, null, NameController);
            //Se establece que el controlador ya se inicializo    
            Inicializado = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void BtnCargar_Click(object sender, EventArgs e)
        {
            string lsRutaArchivo;
            string lsNombre;
            string[] lsErrores = { "", "" };
            var lsCadenaArchivo = new StringBuilder();
            SqlDataReader lLector;
            var lComando = new SqlCommand();
            var liNumeroParametros = 0;
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_ruta_ftp" };
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.VarChar };
            object[] lValorParametrosO = { "", strRutaFTP };
            lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;

            try
            {
                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);

                // Realiza las Validaciones de los Archivos
                if (FuArchivo.FileName != "")
                    lsErrores = ValidarArchivo(lsRutaArchivo);
                else
                {
                    ToastrEvent?.Invoke(HttpContext.GetGlobalResourceObject("AppResources", "ArchivoRequerido", CultureInfo.CurrentCulture)?.ToString(), EnumTypeToastr.Error);
                    return;
                }
                if (lsErrores[0] == "")
                {
                    if (DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]))
                    {
                        lValorParametrosO[0] = lsNombre;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = "pa_ValidaPlanoInfProdPtdvCidvAdm";
                        lComando.CommandTimeout = 3600;
                        if (lsNombreParametrosO != null)
                        {
                            for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                            {
                                lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                            }
                        }
                        lLector = lComando.ExecuteReader();
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                            }
                        }
                        else
                        {
                            ToastrEvent?.Invoke(HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga", CultureInfo.CurrentCulture)?.ToString(), EnumTypeToastr.Success);
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        ToastrEvent?.Invoke(HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString(), EnumTypeToastr.Error);
                    }
                }
                else
                {
                    lsCadenaArchivo.Append(lsErrores[0]);
                    DelegadaBase.Servicios.registrarProceso(goInfo, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCarga", CultureInfo.CurrentCulture)?.ToString(), "Usuario : " + goInfo.nombre);
                }
                //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                LogCargaArchivoEvent?.Invoke(lsCadenaArchivo);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        private string[] ValidarArchivo(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            var liTotalRegistros = 0;
            var liValor = 0;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };
            string[] lsDecimal;

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 19)) 
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 19 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; 
                    }
                    else
                    {
                        //validar codigo
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(0).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del registro {" + oArregloLinea.GetValue(0) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        //validar accion
                        if (oArregloLinea.GetValue(1).ToString() != "C" && oArregloLinea.GetValue(1).ToString() != "M" && oArregloLinea.GetValue(1).ToString() != "E")
                            lsCadenaErrores = lsCadenaErrores + "La acción {" + oArregloLinea.GetValue(1) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        /// Validar Operador
                        try
                        {
                            Convert.ToInt64(oArregloLinea.GetValue(2).ToString()); 
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del operador {" + oArregloLinea.GetValue(2) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida punto SNT
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(3).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El punto del SNT {" + oArregloLinea.GetValue(3) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida socio productor
                        if (oArregloLinea.GetValue(4).ToString().Length > 15)
                            lsCadenaErrores = lsCadenaErrores + "El nit del Socio Productor {" + oArregloLinea.GetValue(4) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        /// Valida socio productor
                        if (oArregloLinea.GetValue(5).ToString().Length > 1)
                            lsCadenaErrores = lsCadenaErrores + "La participación del estado {" + oArregloLinea.GetValue(5) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        /// Valida Ano
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(6).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El Año {" + oArregloLinea.GetValue(6) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida mes
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(7).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El Mes {" + oArregloLinea.GetValue(7) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida PTDV
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(8).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El PTDV {" + oArregloLinea.GetValue(8) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida PC suminsitro interno
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(9).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La producción comprometida de suministro de consumo interno {" + oArregloLinea.GetValue(9) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida PC exportaciones
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(10).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La producción comprometida de exportaciones {" + oArregloLinea.GetValue(10) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida PC refineria barrancabermeja
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(11).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La producción comprometida de Barrancabermeja {" + oArregloLinea.GetValue(11) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida PC refineria cartagena
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(12).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La producción comprometida de Cartagena {" + oArregloLinea.GetValue(12) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Producción portencial del campo
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(13).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La producción potencial del campo {" + oArregloLinea.GetValue(13) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida gas en oepración
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(14).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El gas en operación {" + oArregloLinea.GetValue(14) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida CIDV
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(15).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El CIDV {" + oArregloLinea.GetValue(15) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida poder calorifico
                        try
                        {
                            Convert.ToDecimal(oArregloLinea.GetValue(16).ToString());
                            lsDecimal = oArregloLinea.GetValue(16).ToString().Split('.');
                            if (lsDecimal.Length > 1 && lsDecimal[1].Length > 3)
                                lsCadenaErrores = lsCadenaErrores + "El Poder calorífico {" + oArregloLinea.GetValue(16) + "} debe tener máximo 3 decimales, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";

                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El poder calorífico {" + oArregloLinea.GetValue(16) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// 20220118 ajuste ptdv
                        if (oArregloLinea.GetValue(17).ToString().Length > 0)
                        {
                            try
                            {
                                Convert.ToDateTime(oArregloLinea.GetValue(17).ToString());
                                if (oArregloLinea.GetValue(17).ToString().Length != 10)
                                    lsCadenaErrores = lsCadenaErrores + "La fecha de resolución {" + oArregloLinea.GetValue(17) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                else
                                {
                                    string[] lsFecha = oArregloLinea.GetValue(17).ToString().Split('/');
                                    if (lsFecha[0].Length !=4)
                                        lsCadenaErrores = lsCadenaErrores + "La fecha de resolución {" + oArregloLinea.GetValue(17) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                }

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La fecha de resolución {" + oArregloLinea.GetValue(17) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        else
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar la fecha de resolución en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                    }
                    //validar operador registrado
                    if (oArregloLinea.GetValue(18).ToString()!= "S" && oArregloLinea.GetValue(18).ToString() != "N")
                    {
                        lsCadenaErrores = lsCadenaErrores + "El indicador de operador registrado debe ser S o N en la línea " + liNumeroLinea + " del Archivo Plano<br>";
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }
    }
}