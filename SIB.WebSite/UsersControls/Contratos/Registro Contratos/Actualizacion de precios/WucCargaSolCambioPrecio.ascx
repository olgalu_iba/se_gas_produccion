﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucCargaSolCambioPrecio.ascx.cs" Inherits="UsersControls.Contratos.Registro_Contratos.Actualizacion_de_precios.WucCargaSolCambioPrecio" %>

<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label ID="Label2" AssociatedControlID="FuArchivo" runat="server">Archivo</asp:Label>
            <asp:FileUpload ID="FuArchivo" class="form-control" runat="server" EnableTheming="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label ID="Label1" AssociatedControlID="display" runat="server">Tiempo transcurrido</asp:Label>
            <input type="text" name="display" id="display" size="8" value="00:00:0" class="form-control" readonly="true" clientidmode="Static" runat="server" />
        </div>
    </div>
</div>
