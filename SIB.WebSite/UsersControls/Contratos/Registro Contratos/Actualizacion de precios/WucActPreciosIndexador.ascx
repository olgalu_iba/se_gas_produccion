﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucActPreciosIndexador.ascx.cs" Inherits="UsersControls.Contratos.Registro_Contratos.Actualizacion_de_precios.WucActPreciosIndexador" %>

<%--Contenido--%>
<div id="tblMensaje" runat="server">
    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <asp:Label ID="lblNoContrato" Text="Número Operación" AssociatedControlID="TxtNoContrato" runat="server" />
                <asp:TextBox ID="TxtNoContrato" runat="server" class="form-control" ValidationGroup="detalle" />
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <asp:Label ID="lblNoIdRegistro" Text="Número Id Registro" AssociatedControlID="TxtNoIdRegistro" runat="server" />
                <asp:TextBox ID="TxtNoIdRegistro" runat="server" class="form-control" ValidationGroup="detalle" />
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <asp:Label ID="lblContratoDef" Text="Número Contrato" AssociatedControlID="TxtContratoDef" runat="server" />
                <asp:TextBox ID="TxtContratoDef" runat="server" class="form-control" ValidationGroup="detalle" />
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <asp:Label ID="lblAprobadoCompra" Text="Aprobado Comprador" AssociatedControlID="ddlAprobadoCompra" runat="server" />
                <asp:DropDownList ID="ddlAprobadoCompra" runat="server" CssClass="form-control selectpicker">
                    <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                    <asp:ListItem Value="N" Text="NO"></asp:ListItem>
                    <asp:ListItem Value="S" Text="SI"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <asp:Label ID="lblOperador" Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
                <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control selectpicker selectpicker" data-live-search="true" />
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <asp:Label ID="lblContraparte" Text="Operador Contraparte" AssociatedControlID="dldContraparte" runat="server" />
                <asp:DropDownList ID="dldContraparte" runat="server" CssClass="form-control selectpicker selectpicker" data-live-search="true" />
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <asp:Label ID="lblProducto" Text="Producto" AssociatedControlID="ddlProducto" runat="server" />
                <asp:DropDownList ID="ddlProducto" runat="server" CssClass="form-control selectpicker">
                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                    <%--20180302 rq004-18--%>
                    <asp:ListItem Value="T">Capacidad de transporte</asp:ListItem>
                    <%--20180302 rq004-18--%>
                    <asp:ListItem Value="G">Suministro de gas</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <div class="alert alert-info" role="alert">
                    Aprobación / Rechazo 
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <asp:Label ID="lblObservacion" Text="Observacion" AssociatedControlID="TxtObseAprob" runat="server" />
                <asp:TextBox ID="TxtObseAprob" runat="server" ValidationGroup="detalle" TextMode="MultiLine" CssClass="form-control" />
            </div>
        </div>
    </div>
    <%--Grilla--%>
    <div class="table table-responsive">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" OnItemCommand="dtgConsulta_EditCommand">
                    <Columns>
                        <%--0--%>
                        <asp:BoundColumn DataField="numero_operacion" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--20180302 rq004-18--%>
                        <%--1--%>
                        <asp:BoundColumn DataField="numero_id_registro" HeaderText="No. Registro"></asp:BoundColumn>
                        <%--2--%>
                        <asp:BoundColumn DataField="tipo_subasta" HeaderText="Tipo Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--3--%>
                        <asp:BoundColumn DataField="mercado" HeaderText="Mercado" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="80px"></asp:BoundColumn>
                        <%--4--%>
                        <asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                        <%--5--%>
                        <asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                        <%--6--%><%--2018302 rq004-18--%>
                        <asp:BoundColumn DataField="contrato_definitivo" HeaderText="contrato" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                        <%--7--%><%--2018302 rq004-18--%>
                        <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                        <%--8--%><%--2018302 rq004-18--%>
                        <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                        <%--9--%>
                        <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--10--%>
                        <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                        <%--11--%>
                        <asp:BoundColumn DataField="precio_actual" HeaderText="Precio Actual" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                        <%--12--%>
                        <asp:BoundColumn DataField="codigo_solicitud" HeaderText="No. Solicitud Cambio Precio" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                        <%--13--%>
                        <asp:BoundColumn DataField="aprobado" HeaderText="Aprobado" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <%--14--%>
                        <asp:BoundColumn DataField="aprobado" HeaderText="Aprobado" ItemStyle-HorizontalAlign="Center" Visible="false"></asp:BoundColumn>
                        <%--15--%>
                        <asp:BoundColumn DataField="observacion_bmc" HeaderText="Observación BMC" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--20180302 rq004-18--%>
                        <%--16--%>
                        <asp:BoundColumn DataField="operador_compra" Visible="false"></asp:BoundColumn>
                        <%--17--%>
                        <asp:BoundColumn DataField="operador_venta" Visible="false"></asp:BoundColumn>
                        <%--18--%>
                        <asp:BoundColumn DataField="codigo_tipo_subasta" Visible="false"></asp:BoundColumn>
                        <%--19--%>
                        <asp:BoundColumn DataField="destino_rueda" Visible="false"></asp:BoundColumn>
                        <%--20--%>
                        <asp:BoundColumn DataField="sigla_estado" Visible="false"></asp:BoundColumn>
                        <%--21--%>
                        <asp:BoundColumn DataField="tipo_mercado" Visible="false"></asp:BoundColumn>
                        <%--22--%>
                        <asp:EditCommandColumn HeaderText="Solicitar" EditText="Solicitud Cambio" Visible="False" />
                        <%--23--%>
                        <asp:EditCommandColumn HeaderText="Aprobar" EditText="Aprobación" Visible="False" />
                        <%--20180302 rq004-18--%>
                        <%--24--%>
                        <asp:BoundColumn DataField="nuevo_precio" HeaderText="Nuevo Precio"></asp:BoundColumn>
                        <%--25--%>
                        <asp:BoundColumn DataField="ruta_archivo" Visible="false"></asp:BoundColumn>
                        <%--26--%>
                        <asp:BoundColumn DataField="puede_solicitar" Visible="false"></asp:BoundColumn>
                        <%--27--%>
                        <asp:BoundColumn DataField="puerde_aprobar" Visible="false"></asp:BoundColumn>
                        <%--28--%>
                        <asp:TemplateColumn HeaderText="Sele.">
                            <ItemTemplate>
                                <label class="kt-checkbox">
                                    <asp:CheckBox ID="chkRecibir" runat="server" />
                                    <span></span>
                                </label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%--29--%> <%--20180302 rq004-18--%>
                        <asp:BoundColumn DataField="fecha_cambio" HeaderText="Fecha Cambio Precio" Visible="false"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                            <ItemTemplate>
                                <div class="dropdown dropdown-inline">
                                    <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="flaticon-more-1"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                        <!--begin::Nav-->
                                        <ul class="kt-nav">
                                            <li class="kt-nav__item">
                                                <asp:LinkButton ID="lkbSolicitar" CssClass="kt-nav__link" CommandName="Solicitar" runat="server">
                                                <i class="kt-nav__link-icon flaticon2-expand"></i>
                                                <span class="kt-nav__link-text">Solicitar</span>
                                                </asp:LinkButton>
                                            </li>
                                            <li class="kt-nav__item">
                                                <asp:LinkButton ID="lkbAprobar" CssClass="kt-nav__link" CommandName="Aprobar" runat="server">
                                                <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                <span class="kt-nav__link-text">Aprobar</span>
                                                </asp:LinkButton>
                                            </li>
                                        </ul>
                                        <!--end::Nav-->
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                </asp:DataGrid>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--Excel--%>
    <asp:DataGrid ID="dtgConsultaExcel" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
        Width="100%" CssClass="table-bordered" Visible="false">
        <Columns>
            <%--0--%>
            <asp:BoundColumn DataField="numero_operacion" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
            <%--20180302 rq004-18--%>
            <%--1--%>
            <asp:BoundColumn DataField="numero_id_registro" HeaderText="No. Registro"></asp:BoundColumn>
            <%--2--%>
            <asp:BoundColumn DataField="tipo_subasta" HeaderText="Tipo Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
            <%--3--%>
            <asp:BoundColumn DataField="mercado" HeaderText="Mercado" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="80px"></asp:BoundColumn>
            <%--4--%>
            <asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
            <%--5--%>
            <asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
            <%--6--%><%--2018302 rq004-18--%>
            <asp:BoundColumn DataField="contrato_definitivo" HeaderText="contrato" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
            <%--7--%><%--2018302 rq004-18--%>
            <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
            <%--8--%><%--2018302 rq004-18--%>
            <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
            <%--9--%>
            <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
            <%--10--%>
            <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
            <%--11--%>
            <asp:BoundColumn DataField="precio_actual" HeaderText="Precio Actual" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
            <%--12--%>
            <asp:BoundColumn DataField="codigo_solicitud" HeaderText="No. Solicitud Cambio Precio" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
            <%--13--%>
            <asp:BoundColumn DataField="aprobado" HeaderText="Aprobado" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
            <%--14--%>
            <asp:BoundColumn DataField="aprobado" HeaderText="Aprobado" ItemStyle-HorizontalAlign="Center" Visible="false"></asp:BoundColumn>
            <%--15--%>
            <asp:BoundColumn DataField="observacion_bmc" HeaderText="Observación BMC" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
            <%--20180302 rq004-18--%>
            <%--16--%>
            <asp:BoundColumn DataField="operador_compra" Visible="false"></asp:BoundColumn>
            <%--17--%>
            <asp:BoundColumn DataField="operador_venta" Visible="false"></asp:BoundColumn>
            <%--18--%>
            <asp:BoundColumn DataField="codigo_tipo_subasta" Visible="false"></asp:BoundColumn>
            <%--19--%>
            <asp:BoundColumn DataField="destino_rueda" Visible="false"></asp:BoundColumn>
            <%--20--%>
            <asp:BoundColumn DataField="sigla_estado" Visible="false"></asp:BoundColumn>
            <%--21--%>
            <asp:BoundColumn DataField="tipo_mercado" Visible="false"></asp:BoundColumn>
            <%--22--%>
            <asp:EditCommandColumn HeaderText="Solicitar" EditText="Solicitud Cambio" Visible="False" />
            <%--23--%>
            <asp:EditCommandColumn HeaderText="Aprobar" EditText="Aprobación" Visible="False" />
            <%--20180302 rq004-18--%>
            <%--24--%>
            <asp:BoundColumn DataField="nuevo_precio" HeaderText="Nuevo Precio"></asp:BoundColumn>
            <%--25--%>
            <asp:BoundColumn DataField="ruta_archivo" Visible="false"></asp:BoundColumn>
            <%--26--%>
            <asp:BoundColumn DataField="puede_solicitar" Visible="false"></asp:BoundColumn>
            <%--27--%>
            <asp:BoundColumn DataField="puerde_aprobar" Visible="false"></asp:BoundColumn>
            <%--28--%>
            <asp:TemplateColumn HeaderText="Sele.">
                <ItemTemplate>
                    <label class="kt-checkbox">
                        <asp:CheckBox ID="chkRecibir" runat="server" />
                        <span></span>
                    </label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <%--29--%> <%--20180302 rq004-18--%>
            <asp:BoundColumn DataField="fecha_cambio" HeaderText="Fecha Cambio Precio" Visible="false"></asp:BoundColumn>
        </Columns>
        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
    </asp:DataGrid>
</div>

<%--Modals--%>
<%--Modal Solicitud--%>
<div class="modal fade" id="mdlSol" tabindex="-1" role="dialog" aria-labelledby="mdlSolLabel" aria-hidden="true" clientidmode="Static" runat="server">
    <div class="modal-dialog" id="mdlSolInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel runat="server">
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnActualizar" />
                </Triggers>
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="mdlSolLabel" runat="server">Solicitud de Cambio</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label ID="Label1" Text="Numero Operación" AssociatedControlID="lblOperacion" runat="server" />
                                    <asp:Label ID="lblOperacion" runat="server" ForeColor="Red" Font-Bold="true" CssClass="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label ID="Label2" Text="Subasta" AssociatedControlID="lblSubasta" runat="server" />
                                    <asp:Label ID="lblSubasta" runat="server" ForeColor="Red" Font-Bold="true" CssClass="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label ID="Label3" Text="Precio" AssociatedControlID="lblPrecio" runat="server" />
                                    <asp:Label ID="lblPrecio" runat="server" ForeColor="Red" Font-Bold="true" CssClass="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label ID="Label4" Text="Nuevo Precio" AssociatedControlID="TxtPrecio" runat="server" />
                                    <asp:TextBox ID="TxtPrecio" runat="server" ValidationGroup="detalle" CssClass="form-control" MaxLength="30"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label ID="Label5" Text="Copia del Contrato" AssociatedControlID="FuArchivo" runat="server" />
                                    <%--   <ajaxToolkit:AsyncFileUpload ID="FuArchivo" CssClass="form-control" runat="server" />--%>
                                    <asp:FileUpload ID="FuArchivo" runat="server" EnableTheming="true" CssClass="form-control" />
                                    <asp:HyperLink ID="HplArchivo" runat="server">Ver Archivo</asp:HyperLink>
                                </div>
                            </div>
                            <div id="TrBmc" class="col-sm-12 col-md-6 col-lg-6" runat="server">
                                <div class="form-group">
                                    <asp:Label ID="Label6" Text="Observación" AssociatedControlID="TxtObservacion" runat="server" />
                                    <asp:TextBox ID="TxtObservacion" runat="server" ValidationGroup="detalle" CssClass="form-control" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <asp:Button ID="btnActualizar" runat="server" CssClass="btn btn-success" Text="Actualizar" OnClick="btnActualizar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnAprobar" runat="server" CssClass="btn btn-success" Text="Aprobar" OnClick="Button1_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="brnRechazar" runat="server" CssClass="btn btn-success" Text="Rechazar" OnClick="brnRechazar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                    </div>
                    <asp:HiddenField ID="hdfIdVerif" runat="server" />
                    <asp:HiddenField ID="hdfNoSolicitud" runat="server" />
                    <asp:HiddenField ID="hdfNoContrato" runat="server" />
                    <asp:HiddenField ID="hdfOPeraC" runat="server" />
                    <asp:HiddenField ID="hdfOPeraV" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
