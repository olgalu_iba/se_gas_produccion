﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace UsersControls.Contratos.Registro_Contratos.Actualizacion_de_precios
{
    public partial class WucActPreciosIndexador : UserControl
    {
        #region EventHandler

        /// <summary>
        /// Delegado para el manejo de los modals
        /// </summary>
        public delegate void EventHandlerModal(string id, string insideId, EnumTypeModal typeModal, Modal.Size size = Modal.Size.Large);

        /// <summary>
        /// EventHandler para la selección de los los modals
        /// </summary>
        public event EventHandlerModal ModalEvent;

        /// <summary>
        /// Delegado para el manejo de los toastr
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        /// EventHandler para la selección de los toastr 
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        /// Delegado para la selección de los botones   
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get { return ViewState["NameController"] != null && !string.IsNullOrEmpty(ViewState["NameController"].ToString()) ? ViewState["NameController"].ToString() : string.Empty; }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        #endregion Propiedades

        private InfoSessionVO goInfo = null;
        private static string lsTitulo = "Actualización Precios Indexador"; //20180228 rq004-18
        private clConexion lConexion = null;
        private SqlDataReader lLector;
        private DataSet lds = new DataSet();
        private string sRutaArc = ConfigurationManager.AppSettings["rutaCarga"];
        private string[] lsCarperta;
        private string Carpeta;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lConexion = new clConexion(goInfo);
            lsCarperta = sRutaArc.Split('\\');
            Carpeta = lsCarperta[lsCarperta.Length - 2];

            if (IsPostBack) return;
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "pa_ValidarExistencia", "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20180228 rq004-18

            if (Session["tipoPerfil"].ToString() != "B")
            {
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
                LlenarControles(lConexion.gObjConexion, dldContraparte, "pa_GetOperadorCont", "", ddlOperador.SelectedValue, 0, 1);//20180228 rq004-18
            }
            else
                LlenarControles(lConexion.gObjConexion, dldContraparte, "pa_ValidarExistencia", "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20180228 rq004-18
            lConexion.Cerrar();//20180228 rq004-18

            //Se cargan los botones  
            ButtonsEvent?.Invoke(new[] { EnumBotones.Buscar, EnumBotones.Excel, EnumBotones.Aprobar, EnumBotones.Rechazar }, null, NameController);
            //Se establece que el controlador ya se inicializo     
            Inicializado = true;
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsProc, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)//20180228 rq004-18
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, lsProc, lsTabla, lsCondicion); //20180228 rq004-18

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"] + "-" + lLector["razon_social"];
                }
                lDdl.Items.Add(lItem1);

            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            var lblMensaje = new StringBuilder();
            int liValor = 0;
            string[] lsNombreParametros = { "@P_numero_contrato", "@P_numero_registro", "@P_codigo_operador", "@P_aprobado", "@P_destino_rueda", "@P_contrato_definitivo", "@P_codigo_cont" };  //20180302 rq004-18
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int }; //20180302 rq004-18
            string[] lValorParametros = { "0", "0", ddlOperador.SelectedValue, ddlAprobadoCompra.SelectedValue, ddlProducto.SelectedValue, TxtContratoDef.Text, dldContraparte.SelectedValue }; //20180302 rq004-18

            if (TxtNoContrato.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoContrato.Text);
                }
                catch (Exception)
                {
                    lblMensaje.Append("Formato Inválido en el Campo No. Operación. <br>");//20180302 rq004-18
                }
            }
            if (TxtNoIdRegistro.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoIdRegistro.Text);
                }
                catch (Exception)
                {
                    lblMensaje.Append("Formato Inválido en el Campo No. Id Registro. <br>");
                }
            }

            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                return;
            }
            try
            {
                if (TxtNoContrato.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtNoContrato.Text.Trim();
                if (TxtNoIdRegistro.Text.Trim().Length > 0)
                    lValorParametros[1] = TxtNoIdRegistro.Text.Trim();

                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRegSolicCambioPrecio", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                dtgConsultaExcel.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRegSolicCambioPrecio", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsultaExcel.DataBind();

                lConexion.Cerrar();
                if (dtgConsulta.Items.Count > 0)
                {
                    //20180302 rq004-18
                    //foreach (DataGridItem Grilla in this.dtgConsulta.Items)
                    //{
                    //    if (Grilla.Cells[26].Text == "S") //20180302 rq004-18
                    //        dtgConsulta.Columns[22].Visible = true;//20180302 rq004-18
                    //    else
                    //        dtgConsulta.Columns[22].Visible = false;//20180302 rq004-18
                    //    if (Grilla.Cells[27].Text == "S")//20180302 rq004-18
                    //        dtgConsulta.Columns[23].Visible = true;//20180302 rq004-18
                    //    else
                    //        dtgConsulta.Columns[23].Visible = false;//20180302 rq004-18
                    //}
                    ////20180302 fin rq004-18
                }
                else
                {
                    //Se cargan los botones  
                    EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Aprobar, EnumBotones.Rechazar };
                    ButtonsEvent?.Invoke(botones, null, NameController);
                    ToastrEvent?.Invoke("No se encontraron Registros.", EnumTypeToastr.Info);
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("No se Pudo Generar el Informe.! " + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
        {
            hdfNoSolicitud.Value = "0";

            if (e.CommandName.Equals("Solicitar"))
            {
                if (e.Item.Cells[26].Text == "S")//20180302 rq004-18
                {//20180302 rq004-18
                    try
                    {
                        hdfNoSolicitud.Value = e.Item.Cells[12].Text; //20180302 rq004-18
                        hdfIdVerif.Value = e.Item.Cells[1].Text;
                        lblOperacion.Text = e.Item.Cells[0].Text;
                        lblSubasta.Text = e.Item.Cells[2].Text + " - Mercado: " + e.Item.Cells[3].Text;
                        lblPrecio.Text = e.Item.Cells[11].Text; //20180302 rq004-18
                        TxtPrecio.Text = e.Item.Cells[24].Text; //20180302 rq004-18
                        hdfNoContrato.Value = e.Item.Cells[0].Text;
                        HplArchivo.NavigateUrl = "../" + Carpeta + "/" + e.Item.Cells[25].Text.Replace(@"\", "/"); ; //20180302 rq004-18
                        HplArchivo.Target = "";

                        //Se abre el modal de la solicitud
                        ModalEvent?.Invoke(mdlSol.ID, mdlSolInside.ID, EnumTypeModal.Abrir);
                        btnAprobar.Visible = false;
                        brnRechazar.Visible = false;
                        TrBmc.Visible = false;
                    }
                    catch (Exception ex)
                    {
                        ToastrEvent?.Invoke("Problemas en la Recuperación de la Información. " + ex.Message, EnumTypeToastr.Error);
                    }
                }
                else //20180302 rq004-18 
                    ToastrEvent?.Invoke("No puede hacer la solicitud de cambio de precio", EnumTypeToastr.Error);
            }
            if (e.CommandName.Equals("Aprobar"))
            {
                if (e.Item.Cells[27].Text == "S") //20180302 rq004-18
                { //20180302 rq004-18
                    if (e.Item.Cells[20].Text == "I") //20180302 rq004-18
                    {
                        try
                        {
                            hdfNoSolicitud.Value = e.Item.Cells[12].Text; //20180302 rq004-18
                            hdfIdVerif.Value = e.Item.Cells[1].Text;
                            lblOperacion.Text = e.Item.Cells[0].Text;
                            lblSubasta.Text = e.Item.Cells[2].Text + " - Mercado: " + e.Item.Cells[3].Text;
                            lblPrecio.Text = e.Item.Cells[11].Text; //20180302 rq004-18
                            TxtPrecio.Text = e.Item.Cells[24].Text; //20180302 rq004-18
                            hdfNoContrato.Value = e.Item.Cells[0].Text;
                            HplArchivo.NavigateUrl = "../" + Carpeta + "/" + e.Item.Cells[25].Text.Replace(@"\", "/"); ; //20180302 rq004-18
                            HplArchivo.Target = "";
                            hdfOPeraC.Value = e.Item.Cells[16].Text; //20180302 rq004-18
                            hdfOPeraV.Value = e.Item.Cells[17].Text; //20180302 rq004-18

                            //Se abre el modal de la solicitud
                            ModalEvent?.Invoke(mdlSol.ID, mdlSolInside.ID, EnumTypeModal.Abrir);
                            btnAprobar.Visible = true;
                            brnRechazar.Visible = true;
                            TxtPrecio.Enabled = false;
                            TrBmc.Visible = true;
                            btnActualizar.Visible = false;

                        }
                        catch (Exception ex)
                        {
                            ToastrEvent?.Invoke("Problemas en la Recuperación de la Información. " + ex.Message, EnumTypeToastr.Error);
                        }
                    }
                    else
                        ToastrEvent?.Invoke("El contrato no está pendiente de aprobación", EnumTypeToastr.Info);
                }//20180302 rq004-18
                else//20180302 rq004-18
                    ToastrEvent?.Invoke("No puede hacer la aprobación", EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnActualizar_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_codigo_verif_contrato", "@P_numero_contrato", "@P_precio", "@P_ruta_archivo", "@P_observacion_bmc", "@P_accion", "@P_precio_act" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Decimal };
            string[] lValorParametros = { hdfNoSolicitud.Value, hdfIdVerif.Value, hdfNoContrato.Value, "0", "", "", "1", lblPrecio.Text.Trim() }; // Registro de la Solicitud
            var lblMensaje = new StringBuilder();
            decimal ldValor = 0;
            string[] lsPrecio;
            string lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "-" + DateTime.Now.ToShortDateString().Substring(3, 2) + "-" + DateTime.Now.ToShortDateString().Substring(0, 2);
            string sRuta = sRutaArc + lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\";

            try
            {
                if (TxtPrecio.Text.Trim().Length <= 0)
                    lblMensaje.Append("Debe Ingresar el nuevo precio. <br>"); //20180228 rq004-18
                else
                {
                    try
                    {
                        ldValor = Convert.ToDecimal(TxtPrecio.Text.Trim());
                        if (ldValor <= 0) //20180228 rq004-18
                            lblMensaje.Append("Valor Inválido en Nuevo Precio. <br>"); //20180228 rq004-18
                        else
                        {
                            lsPrecio = TxtPrecio.Text.Trim().Split('.');
                            if (lsPrecio.Length > 1)
                            {
                                if (lsPrecio[1].Trim().Length > 2)
                                    lblMensaje.Append("Valor Inválido en Nuevo Precio. <br>");
                            }
                        }
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append("Valor Inválido en Nuevo Precio. <br>"); //20180228 rq004-18
                    }
                }

                if (FuArchivo.FileName != "")
                {
                    int liReg = 0;
                    try
                    {
                        Directory.CreateDirectory(sRuta);
                        lsPrecio = FuArchivo.FileName.Split('.');
                        liReg = lsPrecio.Length;
                        if (lsPrecio[liReg - 1].ToUpper() != "PDF")
                            lblMensaje.Append("El Archivo Cargado debe ser PDF. <br>");
                        else
                        {
                            FuArchivo.SaveAs(sRuta + FuArchivo.FileName);
                        }
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Append("Problemas en la Carga del Archivo. " + ex.Message);
                        lConexion.Cerrar();
                    }
                }
                //else
                //    lblMensaje.Text += "Debe Seleccionar el Archivo PDF. <br>";
                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }
                lValorParametros[3] = TxtPrecio.Text;
                if (FuArchivo.FileName != "")
                    lValorParametros[4] = lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\" + FuArchivo.FileName;
                SqlTransaction oTransaccion;
                lConexion.Abrir();
                oTransaccion = lConexion.gObjConexion.BeginTransaction(System.Data.IsolationLevel.Serializable);
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetCambioPrecio", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    ToastrEvent?.Invoke("Se presentó un Problema en la Creación del Registro del Contrato.! " + goInfo.mensaje_error, EnumTypeToastr.Error);
                    oTransaccion.Rollback();
                    lConexion.Cerrar();
                }
                else
                {
                    if (!lLector.HasRows) //20180302 rq004-18
                                          // Obtgengo el errro devuelto del procedimiento
                                          //if (lLector["Error"].ToString() == "OK")
                    {
                        lLector.Close();
                        lLector.Dispose();
                        oTransaccion.Commit();
                        lConexion.Cerrar();
                        ToastrEvent?.Invoke("Registro Ingresado Correctamente.!", EnumTypeToastr.Success);
                        //Se cierra el modal de la solicitud
                        ModalEvent?.Invoke(mdlSol.ID, mdlSolInside.ID, EnumTypeModal.Cerrar);
                        TxtPrecio.Text = "";
                        TxtObservacion.Text = "";
                        CargarDatos();
                    }
                    else
                    {
                        while (lLector.Read()) //20180302 rq004-18
                            lblMensaje.Append(lLector["Error"] + "<br>"); //20180302 rq004-18
                        ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                        lLector.Close();
                        lLector.Dispose();
                        oTransaccion.Rollback();
                        lConexion.Cerrar();
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + lblMensaje.Text + "');", true); //20180302 rq004-18
                    }
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Metodo que realiza la aprobacion por parte de la BMC del cambio de precio
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_codigo_verif_contrato", "@P_numero_contrato", "@P_precio", "@P_ruta_archivo", "@P_observacion_bmc", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { hdfNoSolicitud.Value, hdfIdVerif.Value, hdfNoContrato.Value, "0", "", TxtObservacion.Text.Trim(), "2" }; // Aprobacion de la Solicitud
            var lblMensaje = new StringBuilder();

            try
            {
                if (TxtObservacion.Text.Trim().Length <= 0)
                {
                    ToastrEvent?.Invoke("Debe Ingresar la Observación de la Aprobación.!", EnumTypeToastr.Error);
                    return;
                }
                lValorParametros[3] = TxtPrecio.Text.Trim();
                lValorParametros[4] = "";
                lConexion.Abrir();
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetCambioPrecio", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    ToastrEvent?.Invoke("Se presentó un Problema en la Aprobación del Registro.! " + goInfo.mensaje_error, EnumTypeToastr.Error);
                    lConexion.Cerrar();
                }
                else
                {
                    if (!lLector.HasRows) //20180302rq004-18
                                          // Obtgengo el errro devuelto del procedimiento
                                          //if (lLector["Error"].ToString() == "OK")
                    {
                        lLector.Close();
                        lLector.Dispose();
                        //// Envio Alerta a los Operadores
                        string lsAsunto = "";
                        string lsMensaje = "";
                        string lsMensajeC = "";
                        string lsMailC = "";
                        string lsNomOperadorC = "";
                        string lsMailV = "";
                        string lsNomOperadorV = "";

                        /// Obtengo el mail del Operador Compra
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraC.Value + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lsMailC = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" + lLector["e_mail2"]; // 20180228 rq032-17
                            if (lLector["tipo_persona"].ToString() == "J")
                                lsNomOperadorC = lLector["razon_social"].ToString();
                            else
                                lsNomOperadorC = lLector["nombres"] + " " + lLector["apellidos"];
                        }
                        lLector.Close();
                        lLector.Dispose();
                        /// Obtengo el mail del Operador Venta
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraV.Value + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lsMailV = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" + lLector["e_mail2"]; // 20180228 rq032-17
                            if (lLector["tipo_persona"].ToString() == "J")
                                lsNomOperadorV = lLector["razon_social"].ToString();
                            else
                                lsNomOperadorV = lLector["nombres"] + " " + lLector["apellidos"];
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        ///// Envio del Mail de la Solicitud de la Correcion
                        lsAsunto = "Notificación Aprobación Solicitud Cambio Precio Indexador";
                        lsMensaje = "Nos permitimos informarle que " + lsNomOperadorC + " acaba de aprobar la Solicitud del cambio de precio de la operación No. " + lblOperacion.Text + ". El Precio Actualizado es " + TxtPrecio.Text + " US$/MBTU <br><br>";
                        lsMensaje += "<br><br>Observaciones: " + TxtObservacion.Text + " <br><br>";
                        lsMensaje += "Cordialmente, <br><br><br>";
                        lsMensaje += "Administrador SEGAS <br>";
                        lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                        clEmail mailC = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                        lblMensaje.Append(mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas"));
                        ////
                        lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                        clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                        lblMensaje.Append(mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas"));
                        lConexion.Cerrar();
                        ToastrEvent?.Invoke("Registro Aprobado Correctamente.!", EnumTypeToastr.Success);
                        //Se cierra el modal de la solicitud
                        ModalEvent?.Invoke(mdlSol.ID, mdlSolInside.ID, EnumTypeModal.Cerrar);
                        TxtPrecio.Text = "";
                        TxtObservacion.Text = "";
                        CargarDatos();

                        if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                            ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    }
                    else
                    {
                        while (lLector.Read())//20180302 rq004-18
                            lblMensaje.Append(lLector["Error"] + "<br>"); //20180302 rq004-18
                        ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + lblMensaje.Text + "');", true); //20180302 rq004-18
                    }
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Metodo que realiza el rechazo por parte de la BMC del cambio de precio
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void brnRechazar_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_codigo_verif_contrato", "@P_numero_contrato", "@P_precio", "@P_ruta_archivo", "@P_observacion_bmc", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { hdfNoSolicitud.Value, hdfIdVerif.Value, hdfNoContrato.Value, "0", "", TxtObservacion.Text.Trim(), "3" }; // Rechazo de la Solicitud
            var lblMensaje = new StringBuilder();

            try
            {
                if (TxtObservacion.Text.Trim().Length <= 0)
                {
                    ToastrEvent?.Invoke("Debe Ingresar la Observación del Rechazo.!", EnumTypeToastr.Error);
                    return;
                }

                lValorParametros[3] = "0";
                lValorParametros[4] = "";
                SqlTransaction oTransaccion;
                lConexion.Abrir();
                oTransaccion = lConexion.gObjConexion.BeginTransaction(System.Data.IsolationLevel.Serializable);
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetCambioPrecio", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    ToastrEvent?.Invoke("Se presentó un Problema en el Rechazo del Registro.! " + goInfo.mensaje_error, EnumTypeToastr.Error);
                    oTransaccion.Rollback();
                    lConexion.Cerrar();
                }
                else
                {
                    if (!lLector.HasRows) //20180302 rq004-18
                                          // Obtgengo el errro devuelto del procedimiento
                                          //if (lLector["Error"].ToString() == "OK")
                    {
                        lLector.Close();
                        lLector.Dispose();
                        oTransaccion.Commit();
                        //// Envio Alerta a los Operadores
                        string lsAsunto = "";
                        string lsMensaje = "";
                        string lsMensajeC = "";
                        string lsMailC = "";
                        string lsNomOperadorC = "";
                        string lsMailV = "";
                        string lsNomOperadorV = "";

                        /// Obtengo el mail del Operador Compra
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraC.Value + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lsMailC = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" + lLector["e_mail2"]; // 20180228 rq032-17
                            if (lLector["tipo_persona"].ToString() == "J")
                                lsNomOperadorC = lLector["razon_social"].ToString();
                            else
                                lsNomOperadorC = lLector["nombres"] + " " + lLector["apellidos"];
                        }
                        lLector.Close();
                        lLector.Dispose();
                        /// Obtengo el mail del Operador Venta
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraV.Value + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lsMailV = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" + lLector["e_mail2"]; // 20180228 rq032-17
                            if (lLector["tipo_persona"].ToString() == "J")
                                lsNomOperadorV = lLector["razon_social"].ToString();
                            else
                                lsNomOperadorV = lLector["nombres"] + " " + lLector["apellidos"];
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        ///// Envio del Mail de la Solicitud de la Correcion
                        lsAsunto = "Notificación Rechazo Solicitud Cambio Precio Indexador";
                        lsMensaje = "Nos permitimos  informarle que " + lsNomOperadorC + " acaba de rechazar la Solicitud del cambio de precio de la operación No. " + lblOperacion.Text + ". El Precio Actualizado es " + TxtPrecio.Text + " US$/MBTU <br><br>";
                        lsMensaje += "<br><br>Observaciones: " + TxtObservacion.Text + " <br><br>";
                        lsMensaje += "Cordialmente, <br><br><br>";
                        lsMensaje += "Administrador SEGAS <br>";
                        lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                        clEmail mailC = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                        lblMensaje.Append(mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas"));
                        ////
                        lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                        clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                        lblMensaje.Append(mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas"));
                        lConexion.Cerrar();
                        ToastrEvent?.Invoke("Registro Rechazado Correctamente.!", EnumTypeToastr.Success);
                        //Se cierra el modal de la solicitud
                        ModalEvent?.Invoke(mdlSol.ID, mdlSolInside.ID, EnumTypeModal.Cerrar);
                        TxtPrecio.Text = "";
                        TxtObservacion.Text = "";
                        CargarDatos();

                        if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                            ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    }
                    else
                    {
                        while (lLector.Read()) //20180302 rq004-18
                            lblMensaje.Append(lLector["Error"] + "<br>"); //20180302 rq004-18
                        if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                            ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                        lLector.Close();
                        lLector.Dispose();
                        oTransaccion.Rollback();
                        lConexion.Cerrar();
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + lblMensaje.Text + "');", true); //20180302 rq004-18
                    }
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnAprobar_Click(object sender, EventArgs e)
        {
            var lblMensaje = new StringBuilder();
            string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_codigo_verif_contrato", "@P_numero_contrato", "@P_precio", "@P_ruta_archivo", "@P_observacion_bmc", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0", "0", "", TxtObseAprob.Text.Trim(), "2" }; // Aprobacion de la Solicitud
            string lsNosolicitud = "";
            string lsIdVerif = "";
            string lsNoContrato = "";
            SortedList oListaOperador = new SortedList();
            try
            {
                if (TxtObseAprob.Text.Trim().Length <= 0)
                    lblMensaje.Append("Debe Ingresar la Observación de la Aprobación.<br>");
                foreach (DataGridItem Grilla in this.dtgConsulta.Items)
                {
                    CheckBox Checkbox = null;
                    Checkbox = (CheckBox)Grilla.Cells[28].Controls[1];  //20180302 rq004-18
                    if (Checkbox.Checked == true)
                    {
                        lsNosolicitud = Grilla.Cells[12].Text;  //20180302 rq004-18
                        lsIdVerif = Grilla.Cells[1].Text;
                        lsNoContrato = Grilla.Cells[0].Text;

                        if (Grilla.Cells[16].Text != goInfo.cod_comisionista)  //20180302 rq004-18
                            lblMensaje.Append("Solo la Punta Compradora Puede hacer la Aprobación de la Solicitud para el No. de Contrato " + lsNoContrato + "<br>"); //20180302 rq004-18
                        if (Grilla.Cells[20].Text != "I") //20180302 rq004-18
                            lblMensaje.Append("La solicitud no está pendiente de aprobación para el contrato No. " + lsNoContrato + "<br>");
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }

                lConexion.Abrir();
                foreach (DataGridItem Grilla in this.dtgConsulta.Items)
                {
                    CheckBox Checkbox = null;
                    Checkbox = (CheckBox)Grilla.Cells[28].Controls[1]; //20180302 rq004-18
                    if (Checkbox.Checked == true)
                    {
                        lsNosolicitud = Grilla.Cells[12].Text; //20180302 rq004-18
                        lsIdVerif = Grilla.Cells[1].Text;
                        lsNoContrato = Grilla.Cells[0].Text;

                        lValorParametros[0] = lsNosolicitud;
                        lValorParametros[1] = lsIdVerif;
                        lValorParametros[2] = lsNoContrato;
                        lValorParametros[3] = Grilla.Cells[24].Text; //20180302 rq004-18
                        oListaOperador.Add(lsNoContrato + "-" + Grilla.Cells[17].Text + "-" + Grilla.Cells[24].Text, ""); //20180302 rq004-18

                        goInfo.mensaje_error = "";
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetCambioPrecio", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                        if (goInfo.mensaje_error != "")
                        {
                            lLector.Close();
                            lLector.Dispose();
                            lblMensaje.Append("Se presentó un Problema en la Aprobación del Registro.! " + goInfo.mensaje_error); //20180228 rq004-18
                            lConexion.Cerrar();
                        }
                        else
                        {
                            if (!lLector.HasRows) //20180302 rq004-18
                                                  // Obtgengo el errro devuelto del procedimiento
                                                  //lLector.Read();
                                                  //if (lLector["Error"].ToString() == "OK")
                            {
                                lLector.Close();
                                lLector.Dispose();
                            }
                            else
                            {
                                while (lLector.Read()) //20180302 rq004-18
                                    lblMensaje.Append(lLector["Error"] + "<br>"); //20180302 rq004-18
                                lLector.Close();
                                lLector.Dispose();
                                lConexion.Cerrar();
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + lblMensaje.Text + "');", true); //20180302 rq004-18
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }
                lLector.Close();
                lLector.Dispose();
                //// Envio Alerta a los Operadores
                string lsAsunto = "";
                string lsMensaje = "";
                string lsMensajeC = "";
                string lsMailC = "";
                string lsNomOperadorC = "";
                string lsMailV = "";
                string lsNomOperadorV = "";
                string lsCodOperaV = "";
                /// Obtengo el mail del Operador Compra
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia",
                    "m_operador", " codigo_operador = " + goInfo.cod_comisionista + " ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsMailC = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" +
                              lLector["e_mail2"]; // 20180228 rq032-17
                    if (lLector["tipo_persona"].ToString() == "J")
                        lsNomOperadorC = lLector["razon_social"].ToString();
                    else
                        lsNomOperadorC = lLector["nombres"] + " " + lLector["apellidos"];
                }

                lLector.Close();
                lLector.Dispose();
                string lsOp1 = "";
                string[] lsLlave;
                string lsPrecio = "";
                /// Envio de los Mail de los Operadores Contrarios
                for (int i = 0; i < oListaOperador.Count; i++)
                {
                    lsLlave = oListaOperador.GetKey(i).ToString().Split('-');
                    lsOp1 = lsLlave[0].Trim();
                    lsCodOperaV = lsLlave[1].Trim();
                    lsPrecio = lsLlave[2].Trim();

                    /// Obtengo el mail del Operador Venta
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia",
                        "m_operador", " codigo_operador = " + lsCodOperaV + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsMailV = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" +
                                  lLector["e_mail2"]; // 20180228 rq032-17
                        if (lLector["tipo_persona"].ToString() == "J")
                            lsNomOperadorV = lLector["razon_social"].ToString();
                        else
                            lsNomOperadorV = lLector["nombres"] + " " + lLector["apellidos"];
                    }

                    lLector.Close();
                    lLector.Dispose();

                    ///// Envio del Mail de la Solicitud de la Correcion
                    lsAsunto = "Notificación Aprobación Solicitud Cambio Precio Indexador";
                    lsMensaje = "Nos permitimos informarle que " + lsNomOperadorC +
                                " acaba de aprobar la Solicitud del cambio de precio de la operación No. " + lsOp1 +
                                ". El Precio Actualizado es " + lsPrecio + " US$/MBTU <br><br>";
                    lsMensaje += "<br><br>Observaciones: " + TxtObseAprob.Text + " <br><br>";
                    lsMensaje += "Cordialmente, <br><br><br>";
                    lsMensaje += "Administrador SEGAS <br>";
                    lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                    clEmail mailC = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                    lblMensaje.Append(mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas"));
                    ////
                    lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                    clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                    lblMensaje.Append(mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas"));

                    if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                        ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                }

                lConexion.Cerrar();
                ToastrEvent?.Invoke("Registro Aprobado Correctamente.!", EnumTypeToastr.Success);
                //Se cierra el modal de la solicitud
                ModalEvent?.Invoke(mdlSol.ID, mdlSolInside.ID, EnumTypeModal.Cerrar);
                TxtPrecio.Text = "";
                TxtObservacion.Text = "";
                CargarDatos();
            }
            catch (Exception)
            {
                ToastrEvent?.Invoke("Se presentó un Error en la Aprobación de las Solicitudes.", EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnRechazoMas_Click(object sender, EventArgs e)
        {
            var lblMensaje = new StringBuilder();
            string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_codigo_verif_contrato", "@P_numero_contrato", "@P_precio", "@P_ruta_archivo", "@P_observacion_bmc", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0", "0", "", TxtObseAprob.Text.Trim(), "3" }; // Aprobacion de la Solicitud
            string lsNosolicitud = "";
            string lsIdVerif = "";
            string lsNoContrato = "";
            SortedList oListaOperador = new SortedList();
            try
            {
                if (TxtObseAprob.Text.Trim().Length <= 0)
                    lblMensaje.Append("Debe Ingresar la Observación del Rechazo.<br>");
                foreach (DataGridItem Grilla in this.dtgConsulta.Items)
                {
                    CheckBox Checkbox = null;
                    Checkbox = (CheckBox)Grilla.Cells[28].Controls[1]; //20180302 rq004-18
                    if (Checkbox.Checked == true)
                    {
                        lsNosolicitud = Grilla.Cells[12].Text; //20180302 rq004-18
                        lsIdVerif = Grilla.Cells[1].Text;
                        lsNoContrato = Grilla.Cells[0].Text;

                        if (Grilla.Cells[16].Text != goInfo.cod_comisionista) //20180302 rq004-18
                            lblMensaje.Append("Solo la Punta Compradora Puede hacer el Rechazo de la Solicitud para el No. de Contrato " + lsNoContrato + "<br>");
                        if (Grilla.Cells[20].Text != "I") //20180302 rq004-18
                            lblMensaje.Append("La solicitud no está pendiente de aprobación para el contrato No. " + lsNoContrato + "<br>");
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }

                SqlTransaction oTransaccion;
                lConexion.Abrir();
                oTransaccion = lConexion.gObjConexion.BeginTransaction(System.Data.IsolationLevel.Serializable);
                foreach (DataGridItem Grilla in this.dtgConsulta.Items)
                {
                    CheckBox Checkbox = null;
                    Checkbox = (CheckBox)Grilla.Cells[28].Controls[1]; //20180302 rq004-18
                    if (Checkbox.Checked == true)
                    {
                        lsNosolicitud = Grilla.Cells[12].Text; //20180302 rq004-18
                        lsIdVerif = Grilla.Cells[1].Text;
                        lsNoContrato = Grilla.Cells[0].Text;

                        lValorParametros[0] = lsNosolicitud;
                        lValorParametros[1] = lsIdVerif;
                        lValorParametros[2] = lsNoContrato;
                        oListaOperador.Add(lsNoContrato + "-" + Grilla.Cells[17].Text + "-" + Grilla.Cells[24].Text,
                            ""); //20180302 rq004-18

                        goInfo.mensaje_error = "";
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(
                            lConexion.gObjConexion, "pa_SetCambioPrecio", lsNombreParametros, lTipoparametros,
                            lValorParametros, oTransaccion, goInfo);
                        if (goInfo.mensaje_error != "")
                        {
                            lLector.Close();
                            lLector.Dispose();
                            lblMensaje.Append("Se presentó un Problema en el Rechazo del Registro.! " + goInfo.mensaje_error); //20180228 rq004-18
                            oTransaccion.Rollback();
                            lConexion.Cerrar();
                        }
                        else
                        {
                            if (!lLector.HasRows) //20180302 rq004-18
                                                  // Obtgengo el errro devuelto del procedimiento
                                                  //    lLector.Read();
                                                  //if (lLector["Error"].ToString() == "OK")
                            {
                                lLector.Close();
                                lLector.Dispose();
                            }
                            else
                            {
                                while (lLector.Read()) //20180302 rq004-18
                                    lblMensaje.Append(lLector["Error"]); //20180302 rq004-18
                                lLector.Close();
                                lLector.Dispose();
                                oTransaccion.Rollback();
                                lConexion.Cerrar();
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + lblMensaje.Text + "');", true); //20180302 rq004-18

                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }
                lLector.Close();
                lLector.Dispose();
                oTransaccion.Commit();
                //// Envio Alerta a los Operadores
                string lsAsunto = "";
                string lsMensaje = "";
                string lsMensajeC = "";
                string lsMailC = "";
                string lsNomOperadorC = "";
                string lsMailV = "";
                string lsNomOperadorV = "";
                string lsCodOperaV = "";
                /// Obtengo el mail del Operador Compra
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia",
                    "m_operador", " codigo_operador = " + goInfo.cod_comisionista + " ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsMailC = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" +
                              lLector["e_mail2"]; // 20180228 rq032-17
                    if (lLector["tipo_persona"].ToString() == "J")
                        lsNomOperadorC = lLector["razon_social"].ToString();
                    else
                        lsNomOperadorC = lLector["nombres"] + " " + lLector["apellidos"];
                }

                lLector.Close();
                lLector.Dispose();
                string lsOp1 = "";
                string[] lsLlave;
                string lsPrecio = "";
                /// Envio de los Mail de los Operadores Contrarios
                for (int i = 0; i < oListaOperador.Count; i++)
                {
                    lsLlave = oListaOperador.GetKey(i).ToString().Split('-');
                    lsOp1 = lsLlave[0].Trim();
                    lsCodOperaV = lsLlave[1].Trim();
                    lsPrecio = lsLlave[2].Trim();

                    /// Obtengo el mail del Operador Venta
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia",
                        "m_operador", " codigo_operador = " + lsCodOperaV + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsMailV = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" +
                                  lLector["e_mail2"]; // 20180228 rq032-17
                        if (lLector["tipo_persona"].ToString() == "J")
                            lsNomOperadorV = lLector["razon_social"].ToString();
                        else
                            lsNomOperadorV = lLector["nombres"] + " " + lLector["apellidos"];
                    }

                    lLector.Close();
                    lLector.Dispose();

                    ///// Envio del Mail de la Solicitud de la Correcion
                    lsAsunto = "Notificación Rechazo Solicitud Cambio Precio Indexador";
                    lsMensaje = "Nos permitimos informarle que " + lsNomOperadorC +
                                " acaba de rechazar la Solicitud del cambio de precio de la operación No. " +
                                lsOp1 + ". El Precio Actualizado es " + lsPrecio + " US$/MBTU <br><br>";
                    lsMensaje += "<br><br>Observaciones: " + TxtObseAprob.Text + " <br><br>";
                    lsMensaje += "Cordialmente, <br><br><br>";
                    lsMensaje += "Administrador SEGAS <br>";
                    lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                    clEmail mailC = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                    lblMensaje.Append(mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas"));
                    ////
                    lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                    clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                    lblMensaje.Append(mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas"));
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);

                lConexion.Cerrar();
                ToastrEvent?.Invoke("Registro Rechazado Correctamente.!", EnumTypeToastr.Success);
                //Se cierra el modal de la solicitud
                ModalEvent?.Invoke(mdlSol.ID, mdlSolInside.ID, EnumTypeModal.Cerrar);
                TxtPrecio.Text = "";
                TxtObservacion.Text = "";
                CargarDatos();
            }
            catch (Exception)
            {
                ToastrEvent?.Invoke("Se presentó un Error en la Aprobación de las Solicitudes.", EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnConsultar_Click(object sender, EventArgs e)
        {
            CargarDatos();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void lkbExcel_Click(object sender, EventArgs e)
        {
            try
            {
                var lsNombreArchivo = Session["login"] + "InfExcelReg" + DateTime.Now + ".xls";
                var lsb = new StringBuilder();
                var lsw = new StringWriter(lsb);
                var lhtw = new HtmlTextWriter(lsw);
                var lpagina = new Page();
                var lform = new HtmlForm();
                lpagina.EnableEventValidation = false;
                lpagina.Controls.Add(lform);
                dtgConsultaExcel.EnableViewState = false;

                dtgConsultaExcel.Visible = true;
                dtgConsultaExcel.Columns[22].Visible = false; //20180302 rq004-18
                dtgConsultaExcel.Columns[23].Visible = false; //20180302 rq004-18
                dtgConsultaExcel.Columns[28].Visible = false; //20180302 rq004-18
                dtgConsultaExcel.Columns[29].Visible = true; //20180302 rq004-18
                lform.Controls.Add(dtgConsultaExcel);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = System.Text.Encoding.Default;

                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + "Consulta Solicitud Cambio de Precio" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write(lsb.ToString());
                dtgConsultaExcel.Visible = false;
                Response.End();
                Response.Flush();
                dtgConsultaExcel.Columns[28].Visible = true; //20180302 rq004-18
                dtgConsultaExcel.Columns[29].Visible = false; //20180302 rq004-18
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Problemas al Consultar los Registros. ", EnumTypeToastr.Error);
            }
        }
    }
}