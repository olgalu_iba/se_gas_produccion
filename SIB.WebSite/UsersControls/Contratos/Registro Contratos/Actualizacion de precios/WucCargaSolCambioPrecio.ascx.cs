﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace UsersControls.Contratos.Registro_Contratos.Actualizacion_de_precios
{
    public partial class WucCargaSolCambioPrecio : UserControl
    {
        #region EventHandler

        /// <summary>
        /// 
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        /// 
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        /// 
        /// </summary>
        public delegate void EventHandlerLogCargaArchivo(StringBuilder message);

        /// <summary>
        /// 
        /// </summary>
        public event EventHandlerLogCargaArchivo LogCargaArchivoEvent;

        /// <summary>
        /// delegado para la selección de los botones  
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// 
        /// </summary>
        public string NameController
        {
            get { return ViewState["NameController"] != null && !string.IsNullOrEmpty(ViewState["NameController"].ToString()) ? ViewState["NameController"].ToString() : string.Empty; }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        #endregion Propiedades

        private InfoSessionVO goInfo = null;
        private clConexion lConexion = null;
        private string strRutaCarga;
        private string strRutaFTP;
        private SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
            strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            EnumBotones[] botones = { EnumBotones.Cargue };
            ButtonsEvent?.Invoke(botones, null, NameController);
            //Se establece que el controlador ya se inicializo    
            Inicializado = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void BtnCargar_Click(object sender, EventArgs e)
        {
            string lsRutaArchivo = "";
            string lsNombre = "";
            string[] lsErrores = { "", "" };
            var lsCadenaArchivo = new StringBuilder();
            bool oTransOK = true;
            bool oCargaOK = true;
            SqlDataReader lLector;
            SqlCommand lComando = new SqlCommand();
            Int32 liNumeroParametros = 0;
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_ruta_ftp", "@P_codigo_operador" };
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
            Object[] lValorParametrosO = { "", strRutaFTP, goInfo.cod_comisionista };
            lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;

            /// Realiza las Validaciones de los Archivos
            try
            {
                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);

                if (FuArchivo.FileName != "")
                    lsErrores = ValidarArchivo(lsRutaArchivo);
                else
                {
                    ToastrEvent?.Invoke("Debe seleccionar el archivo", EnumTypeToastr.Error);
                    return;
                }

                if (lsErrores[0] == "")
                {
                    if (DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]))
                    {
                        lValorParametrosO[0] = lsNombre;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = "pa_ValidaPlanoActPre";
                        lComando.CommandTimeout = 3600;
                        if (lsNombreParametrosO != null)
                        {
                            for (liNumeroParametros = 0;
                                liNumeroParametros <= lsNombreParametrosO.Length - 1;
                                liNumeroParametros++)
                            {
                                lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros],
                                    lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                            }
                        }

                        lLector = lComando.ExecuteReader();
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                            }
                        }
                        else
                        {
                            ToastrEvent?.Invoke("Archivo cargado satisfactoriamente.!", EnumTypeToastr.Success);
                        }

                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        ToastrEvent?.Invoke("Se presento un Problema en el {FTP} del Archivo al Servidor.!", EnumTypeToastr.Error);
                    }
                }
                else
                {
                    lsCadenaArchivo.Append(lsErrores[0]);
                    DelegadaBase.Servicios.registrarProceso(goInfo, "Finalizó la carga con errores", "Usuario : " + goInfo.nombre);
                }

                if (!string.IsNullOrEmpty(lsCadenaArchivo.ToString()) &&
                    lsCadenaArchivo.ToString().Contains("Se crearon correctamente"))
                {
                    string[] delim = { Environment.NewLine, "\n" }; // "\n" added in case you manually appended a newline
                    var lines = lsCadenaArchivo.ToString().Split(delim, StringSplitOptions.None);
                    lsCadenaArchivo.Clear();
                    foreach (var line in lines)
                    {
                        if (!line.Contains("Se crearon correctamente"))
                            lsCadenaArchivo.Append(line);
                        else
                            ToastrEvent?.Invoke(line, EnumTypeToastr.Success);
                    }
                }

                if (!string.IsNullOrEmpty(lsCadenaArchivo.ToString()))
                    //Se descarga el log para el usuario
                    LogCargaArchivoEvent?.Invoke(lsCadenaArchivo);

                ScriptManager.RegisterStartupScript(this, GetType(), "StartupAlert", "DetenerCrono();", true);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Se presento un Problema Al cargar los datos del archivo.! <br>" + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        protected string[] ValidarArchivo(string lsRutaArchivo)
        {
            Int32 liNumeroLinea = 0;
            decimal ldValor = 0;
            int liTotalRegistros = 0;
            Int64 liValor = 0;
            DateTime ldFecha;
            string lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };
            string[] lsDecimal;

            StreamReader lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 3))
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 3 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                    }
                    else
                    {
                        /// Valida contrato
                        try
                        {
                            if (oArregloLinea.GetValue(0).ToString().Length > 30)
                                lsCadenaErrores = lsCadenaErrores + "El Número de contrato {" + oArregloLinea.GetValue(0) + "} excede la longitud de 30 caracteres, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";//20180302 q004-18
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El Número de contrato {" + oArregloLinea.GetValue(0) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; //20180302 q004-18
                        }
                        /// Valida id registro
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(1).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El número de registro {" + oArregloLinea.GetValue(1) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; //20180302 q004-18
                        }
                        /// Valida precio
                        try
                        {
                            Convert.ToDecimal(oArregloLinea.GetValue(2).ToString());
                            lsDecimal = oArregloLinea.GetValue(2).ToString().Split('.');
                            if (lsDecimal.Length > 1 && lsDecimal[1].Length > 2)
                                lsCadenaErrores = lsCadenaErrores + "EL Precio {" + oArregloLinea.GetValue(2) + "} debe tener máximo 2 decímales, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El precio {" + oArregloLinea.GetValue(2) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; //20180302 q004-18
                        }
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }
    }
}