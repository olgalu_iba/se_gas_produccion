﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucCargaContratoPdf.ascx.cs" Inherits="UsersControls.Contratos.Registro_Contratos.Registro_Contratos.WucCargaContratoPdf" %>
<script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/dev/modal.js")%>"></script>

<%--Contenido--%>
<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Fecha Negociación Inicial" AssociatedControlID="TxtFechaIni" runat="server" />
            <asp:TextBox ID="TxtFechaIni" runat="server" CssClass="form-control datepicker" Width="100%" ClientIDMode="Static" MaxLength="10" />
            
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Fecha Negociación Final" AssociatedControlID="TxtFechaFin" runat="server" />
            <asp:TextBox ID="TxtFechaFin" runat="server" CssClass="form-control datepicker" Width="100%" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Operación" AssociatedControlID="TxtNoOper" runat="server" />
            <asp:TextBox ID="TxtNoOper" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Contrato" AssociatedControlID="TxtContratoDef" runat="server" />
            <asp:TextBox ID="TxtContratoDef" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Vendedor" AssociatedControlID="ddlVendedor" runat="server" />
            <asp:DropDownList ID="ddlVendedor" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Comprador" AssociatedControlID="ddlComprador" runat="server" />
            <asp:DropDownList ID="ddlComprador" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Tipo Mercado" AssociatedControlID="ddlMercado" runat="server" />
            <asp:DropDownList ID="ddlMercado" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                <asp:ListItem Value="">Seleccione</asp:ListItem>
                <asp:ListItem Value="P">Primario</asp:ListItem>
                <asp:ListItem Value="S">Secundario</asp:ListItem>
                <asp:ListItem Value="O">OTMM</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
</div>

<%--Grilla--%>
<div class="table table-responsive">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                Width="100%" CssClass="table-bordered" OnItemCommand="dtgConsulta_EditCommand" AllowPaging="true"
                PageSize="10" OnPageIndexChanged="dtgConsulta_PageIndexChanged">
                <Columns>
                    <%--0--%>
                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--1--%>
                    <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--2--%>
                    <asp:BoundColumn DataField="contrato_definitivo" HeaderText="No. Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--3--%>
                    <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--4--%>
                    <asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--5--%>
                    <asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--6--%>
                    <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicio" ItemStyle-HorizontalAlign="Left"
                        DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <%--7--%>
                    <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Fin" ItemStyle-HorizontalAlign="Left"
                        DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <%--8--%>
                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                        DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                    <%--9--%>
                    <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                        DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                    <%--10--%>
                    <asp:BoundColumn DataField="archivos" Visible="false"></asp:BoundColumn>
                    <%--11--%>
                    <asp:BoundColumn DataField="operador_venta" Visible="false"></asp:BoundColumn>
                    <%--12--%>
                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                        <ItemTemplate>
                            <div class="dropdown dropdown-inline">
                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="flaticon-more-1"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                    <!--begin::Nav-->
                                    <ul class="kt-nav">
                                        <li class="kt-nav__item">
                                            <asp:LinkButton ID="lkbCargar" CssClass="kt-nav__link" CommandName="Cargar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-crisp-icons"></i>
                                                            <span class="kt-nav__link-text">Cargar PDF</span>
                                            </asp:LinkButton>
                                        </li>
                                    </ul>
                                    <!--end::Nav-->
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
            </asp:DataGrid>
            <%--Excel--%>
            <asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False" Visible="false"
                Width="100%" CssClass="table-bordered">
                <Columns>
                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="contrato_definitivo" HeaderText="No. Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicio" ItemStyle-HorizontalAlign="Left"
                        DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Fin" ItemStyle-HorizontalAlign="Left"
                        DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                        DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                        DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="archivos" HeaderText="Pdf Contrato"></asp:BoundColumn>
                </Columns>
            </asp:DataGrid>

        </ContentTemplate>
    </asp:UpdatePanel>
</div>



<%--Modals--%>
<%--pantlla cara de plano--%>
<div class="modal fade" id="modContratosPdf" tabindex="-1" role="dialog" aria-labelledby="modContratosPdfLabel" aria-hidden="true" clientidmode="Static" style="overflow-y: auto" runat="server">
    <div class="modal-dialog" id="modContratosPdfInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel runat="server">
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnCargar" />
                </Triggers>
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="modContratosPdfLabel">Carga PDF Contratos</h5>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <%--Infirmacion Basica--%>
                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <%--<div class="panel-heading">
                                        <h5>Información Básica</h5>
                                    </div>--%>
                                    <div class="panel-body">
                                        <%--Contenido--%>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Operación</label>
                                                    <asp:Label ID="lblOperacion" runat="server" CssClass="form-control" />
                                                    <asp:HiddenField ID="hndVendedor" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Fecha Negociación</label>
                                                    <asp:Label ID="lblFechaNeg" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Id registro</label>
                                                    <asp:Label ID="lblIdRegistro" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Número Contrato</label>
                                                    <asp:Label ID="lblContrato" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Comprador</label>
                                                    <asp:Label ID="lblComprador" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Vendedor</label>
                                                    <asp:Label ID="lblVendedor" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Cantidad</label>
                                                    <asp:Label ID="lblCantidad" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Precio</label>
                                                    <asp:Label ID="lblPrecio" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Fecha Inicial</label>
                                                    <asp:Label ID="lblFechaIni" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Fecha Final</label>
                                                    <asp:Label ID="lblFechaFin" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Seleccione el Archivo:</label>
                                                    <asp:FileUpload ID="FuArchivo" CssClass="form-control" runat="server" EnableTheming="true" />
                                                </div>
                                            </div>
                                        </div>

                                        <asp:Button ID="btnCargar" runat="server" CssClass="btn btn-secondary " Text="Cargar" OnClick="BtnCargar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                                        <asp:Button ID="btnRegresar" runat="server" CssClass="btn btn-secondary" Text="Regresar" OnClick="btnRegresar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />

                                    </div>
                                </div>
                            </div>

                            <%--Detalle archiuvos--%>
                            <div id="tblArchivos" visible="true" runat="server">
                                <br />
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5>Archivos ya cargados</h5>
                                    </div>
                                    <div class="panel-body">

                                        <div class="table table-responsive">
                                            <asp:DataGrid ID="dtgPdf" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                                Width="100%" CssClass="table-bordered" OnItemCommand="dtgPdf_EditCommand">
                                                <Columns>
                                                    <asp:BoundColumn DataField="codigo_contrato_pdf" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="nombre_archivo" HeaderText="Archivo Sistema" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px"></asp:BoundColumn>
                                                    <%--20200430--%>
                                                    <asp:BoundColumn DataField="nombre_archivo_org" HeaderText="Nombre Archivo Original" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px"></asp:BoundColumn>
                                                    <%--20200430--%>
                                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                                        <ItemTemplate>
                                                            <div class="dropdown dropdown-inline">
                                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="flaticon-more-1"></i>
                                                                </button>
                                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                                    <!--begin::Nav-->
                                                                    <ul class="kt-nav">
                                                                        <li class="kt-nav__item">
                                                                            <asp:LinkButton ID="lkbVer" CssClass="kt-nav__link" CommandName="Ver" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Ver</span>
                                                                            </asp:LinkButton>
                                                                        </li>
                                                                        <li class="kt-nav__item">
                                                                            <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon-delete"></i>
                                                            <span class="kt-nav__link-text">Eliminar</span>
                                                                            </asp:LinkButton>
                                                                        </li>
                                                                    </ul>
                                                                    <!--end::Nav-->
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <%--Detalle--%>
                            <div id="tblDetalle" visible="false" runat="server">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5>Puntos Ingresados</h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="table table-responsive">
                                            <asp:DataGrid ID="dtgDetalle" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                                Width="100%" CssClass="table-bordered">
                                                <Columns>
                                                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Operación"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fecha_gas" HeaderText="Fecha Gas" DataFormatString="{0:yyyy/MM/dd}"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="codigo_Punto" HeaderText="Código Punto"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrtega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--20190524 rq029-19--%>
                                                    <%--<asp:BoundColumn DataField="tipo_demanda" HeaderText="Código Tipo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="sector_consumo" HeaderText="Código Sector Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_sector" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>--%>
                                                    <%--20190524  FIn rq029-19--%>
                                                    <asp:BoundColumn DataField="cantidad_autorizada" HeaderText="Cantidad Autorizada" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="valor_facturado" HeaderText="Valor Facturado" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0.00}"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                </Columns>
                                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
