﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucDesistimientoContrato.ascx.cs" Inherits="UsersControls.Contratos.Registro_Contratos.Registro_Contratos.WucDesistimientoContrato" %>
<script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/dev/modal.js")%>"></script>

<%--Contenido--%>
<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Operación" AssociatedControlID="TxtNoOper" runat="server" />
            <asp:TextBox ID="TxtNoOper" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Fecha Negociación Inicial" AssociatedControlID="TxtFechaIni" runat="server" />
            <asp:TextBox ID="TxtFechaIni" runat="server" CssClass="form-control datepicker" Width="100%" ClientIDMode="Static" MaxLength="10" />

        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Fecha Negociación Final" AssociatedControlID="TxtFechaFin" runat="server" />
            <asp:TextBox ID="TxtFechaFin" runat="server" CssClass="form-control datepicker" Width="100%" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
        </div>
    </div>
</div>

<%--Grilla--%>
<div class="table table-responsive">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                Width="100%" CssClass="table-bordered" OnItemCommand="dtgConsulta_EditCommand" AllowPaging="true"
                PageSize="10" OnPageIndexChanged="dtgConsulta_PageIndexChanged">
                <Columns>
                    <%--0--%>
                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--1--%>
                    <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <%--2--%>
                    <asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--3--%>
                    <asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--4--%>
                    <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicio" ItemStyle-HorizontalAlign="Left"
                        DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <%--5--%>
                    <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Fin" ItemStyle-HorizontalAlign="Left"
                        DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <%--6--%>
                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                        DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                    <%--7--%>
                    <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                        DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                    <%--8--%>
                    <asp:BoundColumn DataField="accion" Visible="false"></asp:BoundColumn>
                    <%--9--%>
                    <asp:BoundColumn DataField="ruta_archivo" Visible="false"></asp:BoundColumn>
                    <%--10--%>
                    <asp:BoundColumn DataField="Estado_Cont" HeaderText ="Estado Contrato"></asp:BoundColumn>
                    <%--11--%>
                    <asp:BoundColumn DataField="desc_estado" HeaderText ="Estado Desistimiento"></asp:BoundColumn>
                    <%--12--%>
                    <asp:BoundColumn DataField="codigo_obs_sol" visible="false" ></asp:BoundColumn>
                    <%--13--%>
                    <asp:BoundColumn DataField="observacion_sol" visible ="false" ></asp:BoundColumn>
                    
                    <%--<asp:BoundColumn DataField="plazo_vencido" visible ="false" ></asp:BoundColumn>--%>
                    <%--15--%>
                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                        <ItemTemplate>
                            <div class="dropdown dropdown-inline">
                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="flaticon-more-1"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                    <!--begin::Nav-->
                                    <ul class="kt-nav">
                                        <li class="kt-nav__item">
                                            <asp:LinkButton ID="lkbDesistir" CssClass="kt-nav__link" CommandName="Desistir" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-crisp-icons"></i>
                                                            <span class="kt-nav__link-text">Desistir de contrato</span>
                                            </asp:LinkButton>
                                        </li>
                                        <li class="kt-nav__item">
                                            <asp:LinkButton ID="lkbVer" CssClass="kt-nav__link" CommandName="Ver" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Ver</span>
                                            </asp:LinkButton>
                                        </li>
                                    </ul>
                                    <!--end::Nav-->
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
            </asp:DataGrid>
            <%--Excel--%>
            <%--<asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False" Visible="false"
                Width="100%" CssClass="table-bordered">
                <Columns>
                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicio" ItemStyle-HorizontalAlign="Left"
                        DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Fin" ItemStyle-HorizontalAlign="Left"
                        DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                        DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                        DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="Estado_Cont" HeaderText ="Estado Contrato"></asp:BoundColumn>
                    <asp:BoundColumn DataField="desc_estado" HeaderText ="Estado Desistimiento"></asp:BoundColumn>
                </Columns>
            </asp:DataGrid>--%>

        </ContentTemplate>
    </asp:UpdatePanel>
</div>



<%--Modals--%>
<%--pantlla cara de plano--%>
<div class="modal fade" id="modContratosPdf" tabindex="-1" role="dialog" aria-labelledby="modContratosPdfLabel" aria-hidden="true" clientidmode="Static" style="overflow-y: auto" runat="server">
    <div class="modal-dialog" id="modContratosPdfInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel runat="server">
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnCrear" />
                </Triggers>
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="modContratosPdfLabel">Desistimientos</h5>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <%--Infirmacion Basica--%>
                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <%--<div class="panel-heading">
                                        <h5>Información Básica</h5>
                                    </div>--%>
                                    <div class="panel-body">
                                        <%--Contenido--%>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Operación</label>
                                                    <asp:Label ID="lblOperacion" runat="server" CssClass="form-control" />
                                                    <asp:HiddenField ID="hndVendedor" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Fecha Negociación</label>
                                                    <asp:Label ID="lblFechaNeg" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Comprador</label>
                                                    <asp:Label ID="lblComprador" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Vendedor</label>
                                                    <asp:Label ID="lblVendedor" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Cantidad</label>
                                                    <asp:Label ID="lblCantidad" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Precio</label>
                                                    <asp:Label ID="lblPrecio" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4"  >
                                                <div class="form-group">
                                                    <asp:Label Text="Observación Solicitud Desistimiento" AssociatedControlID="ddlObservacionSol" runat="server" />
                                                    <asp:DropDownList ID="ddlObservacionSol" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                    
                                                    <asp:HiddenField id="hndArchivo" runat="server"/>
                                                </div>
                                            </div>
                                            <%--<div class="col-sm-12 col-md-6 col-lg-4" id="divObsApr" runat="server" >
                                                <div class="form-group">
                                                    <asp:Label Text="Observación Aprobación Desistimiento" AssociatedControlID="ddlObservacionApro" runat="server" />
                                                    <asp:DropDownList ID="ddlObservacionApro" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                </div>
                                            </div>--%>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Seleccione el Archivo:</label>
                                                    <asp:FileUpload ID="FuArchivo" CssClass="form-control" runat="server" EnableTheming="true" />
                                                    <asp:Button ID="BtnVer" runat="server" CssClass="btn btn-secondary " Text="Ver" OnClick="BtnVer_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                                                </div>
                                            </div>
                                        </div>

                                        <asp:Button ID="BtnCrear" runat="server" CssClass="btn btn-secondary " Text="Crear" OnClick="BtnCrear_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                                        <asp:Button ID="BtnModificar" runat="server" CssClass="btn btn-secondary " Text="Modificar" OnClick="BtnModificar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                                        <asp:Button ID="BtnAprobar" runat="server" CssClass="btn btn-secondary " Text="Aprobar" OnClick="BtnAprobar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                                        <asp:Button ID="BtnRechazar" runat="server" CssClass="btn btn-secondary " Text="Rechazar" OnClick="BtnRechazar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                                        <asp:Button ID="btnRegresar" runat="server" CssClass="btn btn-secondary" Text="Regresar" OnClick="btnRegresar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
