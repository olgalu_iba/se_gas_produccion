﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace UsersControls.Contratos.Registro_Contratos.Registro_Contratos
{
    public partial class WucMinutaC1 : UserControl
    {
        #region EventHandler

        /// <summary>
        /// Delegado para el manejo de los modals
        /// </summary>
        public delegate void EventHandlerModal(string id, string insideId, EnumTypeModal typeModal, Modal.Size size = Modal.Size.Large);

        /// <summary>
        /// EventHandler para la selección de los los modals
        /// </summary>
        public event EventHandlerModal ModalEvent;

        /// <summary>
        /// Delegado para el manejo de los toastr
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        /// EventHandler para la selección de los toastr 
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        /// Delegado para la selección de los botones  s  
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// 
        /// </summary>
        public string HdfOperacion
        {
            get { return ViewState["HdfOperacion"] != null && !string.IsNullOrEmpty(ViewState["HdfOperacion"].ToString()) ? ViewState["HdfOperacion"].ToString() : "0"; }
            set { ViewState["HdfOperacion"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string NoDiasSecun
        {
            get { return ViewState["NoDiasSecun"] != null && !string.IsNullOrEmpty(ViewState["NoDiasSecun"].ToString()) ? ViewState["NoDiasSecun"].ToString() : "0"; }
            set { ViewState["NoDiasSecun"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Contrato
        {
            get { return ViewState["Contrato"] != null && !string.IsNullOrEmpty(ViewState["Contrato"].ToString()) ? ViewState["Contrato"].ToString() : null; }
            set { ViewState["Contrato"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Indica
        {
            get { return ViewState["Indica"] != null && !string.IsNullOrEmpty(ViewState["Indica"].ToString()) ? ViewState["Indica"].ToString() : null; }
            set { ViewState["Indica"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Consec
        {
            get { return ViewState["Consec"] != null && !string.IsNullOrEmpty(ViewState["Consec"].ToString()) ? ViewState["Consec"].ToString() : "0"; }
            set { ViewState["Consec"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Actual
        {
            get { return ViewState["Actual"] != null && !string.IsNullOrEmpty(ViewState["Actual"].ToString()) ? ViewState["Actual"].ToString() : "0"; }
            set { ViewState["Actual"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string IndModif
        {
            get { return ViewState["IndModif"] != null && !string.IsNullOrEmpty(ViewState["IndModif"].ToString()) ? ViewState["IndModif"].ToString() : "0"; }
            set { ViewState["IndModif"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfDestinoRueda
        {
            get { return ViewState["HdfDestinoRueda"] != null && !string.IsNullOrEmpty(ViewState["HdfDestinoRueda"].ToString()) ? ViewState["HdfDestinoRueda"].ToString() : string.Empty; }
            set { ViewState["HdfDestinoRueda"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfTipoMerc
        {
            get { return ViewState["HdfTipoMerc"] != null && !string.IsNullOrEmpty(ViewState["HdfTipoMerc"].ToString()) ? ViewState["HdfTipoMerc"].ToString() : string.Empty; }
            set { ViewState["HdfTipoMerc"] = value; }
        }

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get { return ViewState["NameController"] != null && !string.IsNullOrEmpty(ViewState["NameController"].ToString()) ? ViewState["NameController"].ToString() : string.Empty; }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        #endregion Propiedades

        private InfoSessionVO goInfo = null;
        private static string lsTitulo = "Minuta contratos bimestrales C1 y c2";
        private clConexion lConexion = null;
        private clConexion lConexion1 = null;
        private SqlDataReader lLector;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                goInfo = (InfoSessionVO)Session["infoSession"];
                goInfo.Programa = lsTitulo;
                lConexion = new clConexion(goInfo);
                lConexion1 = new clConexion(goInfo);
                if (IsPostBack) return;
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Excel };
            ButtonsEvent?.Invoke(botones, null, NameController);


            //Se establece que el controlador ya se inicializo    
            Inicializado = true;
        }


        /// <summary>
        /// Nombre: dtgConsulta_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgConsulta.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            //this.dtgConsulta.CurrentPageIndex = 0;//20171130 rq026-17  //ajuste 20190130
            DateTime ldFecha;
            var liValor = 0;
            var valido = true;

            string[] lsNombreParametros = { "@P_fecha_ini", "@P_fecha_fin", "@P_operacion_ini", "@P_operacion_fin", "@P_codigo_operador" };
            SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { "", "", "0", "0" , goInfo.cod_comisionista };

            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception)
                {
                    ToastrEvent?.Invoke("Formato Inválido en el Campo Fecha Inicial de Negociación.", EnumTypeToastr.Error);
                    valido = false;
                }
            }

            if (TxtFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaFin.Text);
                }
                catch (Exception)
                {
                    ToastrEvent?.Invoke("Formato Inválido en el Campo Fecha Final de Negociación.", EnumTypeToastr.Error);
                    valido = false;
                }
            }

            if (TxtFechaIni.Text.Trim().Length == 0 && TxtFechaFin.Text.Trim().Length > 0)
            {
                ToastrEvent?.Invoke("Debe digitar la Fecha Inicial de Negociación.", EnumTypeToastr.Warning);
                valido = false;
            }

            //20161207 rq102 conformacion de rutas
            if (TxtFechaIni.Text.Trim().Length > 0 && TxtFechaFin.Text.Trim().Length > 0)
                try
                {
                    if (Convert.ToDateTime(TxtFechaFin.Text) < Convert.ToDateTime(TxtFechaIni.Text))
                    {
                        ToastrEvent?.Invoke("La Fecha inicial de Negociación debe ser menor o igual que la fecha final.", EnumTypeToastr.Warning);
                        valido = false;
                    }
                }
                catch (Exception)
                { }
            if (TxtNoContrato.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoContrato.Text);
                }
                catch (Exception)
                {
                    ToastrEvent?.Invoke("Formato Inválido en el Campo No Operación.", EnumTypeToastr.Error);
                    valido = false;
                }

            }

            if (TxtNoContratoFin.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoContratoFin.Text);
                }
                catch (Exception)
                {
                    ToastrEvent?.Invoke("Formato Inválido en el Campo No Operación final.", EnumTypeToastr.Error);
                    valido = false;
                }

            }

            if (TxtNoContrato.Text.Trim().Length == 0 && TxtNoContratoFin.Text.Trim().Length > 0)
            {
                ToastrEvent?.Invoke("Debe digitar el No de operación inicial antes que el final.", EnumTypeToastr.Warning);
                valido = false;
            }

            if (TxtNoContrato.Text.Trim().Length > 0 && TxtNoContratoFin.Text.Trim().Length > 0)
            {
                try
                {
                    if (Convert.ToInt32(TxtNoContratoFin.Text) < Convert.ToInt32(TxtNoContrato.Text))
                    {
                        ToastrEvent?.Invoke("El No Operación inicial debe ser menor o igual que el final.", EnumTypeToastr.Warning);
                        valido = false;
                    }
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
                }
            }
            if (valido)
            {
                try
                {
                    if (TxtNoContrato.Text.Trim().Length > 0)
                    {
                        lValorParametros[2] = TxtNoContrato.Text.Trim();
                        if (TxtNoContratoFin.Text.Trim().Length > 0)
                            lValorParametros[3] = TxtNoContratoFin.Text.Trim();
                        else
                            lValorParametros[3] = TxtNoContrato.Text.Trim();
                    }
                    if (TxtFechaIni.Text.Trim().Length > 0)
                    {
                        lValorParametros[0] = TxtFechaIni.Text.Trim();
                        if (TxtFechaFin.Text.Trim().Length > 0)
                            lValorParametros[1] = TxtFechaFin.Text.Trim();
                        else
                            lValorParametros[1] = TxtFechaIni.Text.Trim();
                    }

                    lConexion.Abrir();
                    dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContratosMinuta", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgConsulta.DataBind();
                    lConexion.Cerrar();
                    if (dtgConsulta.Items.Count > 0)
                    {
                        EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Excel };
                        ButtonsEvent?.Invoke(botones, null, NameController);
                        ldFecha = DateTime.Now.Date;

                        //foreach (DataGridItem Grilla in dtgConsulta.Items)
                        //{
                        //    var lkbSeleccionar = (LinkButton)Grilla.FindControl("lkbSeleccionar");
                        //    lkbSeleccionar.Visible = true;
                        //}


                    }
                    else
                    {
                        EnumBotones[] botones = { EnumBotones.Buscar };
                        ButtonsEvent?.Invoke(botones, null, NameController);
                        ToastrEvent?.Invoke("No se encontraron Registros.", EnumTypeToastr.Warning);
                    }


                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke($"No se Pudo Generar el Informe.! {ex}", EnumTypeToastr.Error);
                }
            }
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
        {
            var lsFecha = DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day;
            var lsMensaje = "";

            if (e.CommandName.Equals("Seleccionar"))
            {
                try
                {
                    if (e.Item.Cells[12].Text=="N")
                        lsMensaje = "Ya se venció el plazo para registrar la minuta";
                    if (e.Item.Cells[13].Text == "R")
                        lsMensaje = "La minuta ya está registrada";
                    if (lsMensaje.ToString() == "")
                    {
                        if (e.Item.Cells[14].Text == "C")
                        { 
                            trPago.Visible = false;
                            txtCuenta.Text = "";
                        }
                        else
                            trPago.Visible = true;

                        HdfOperacion = e.Item.Cells[0].Text;
                        string[] lsNombreParametros = { "@P_numero_operacion", "@P_codigo_operador" };
                        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                        string[] lValorParametros = { "0", "0" };
                        lValorParametros[0] = HdfOperacion;
                        lValorParametros[1] = goInfo.cod_comisionista;
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetDatosMinuta", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                        if (goInfo.mensaje_error == "")
                        {
                            if (lLector.HasRows)
                            {
                                lLector.Read();
                                lblVendedor.Text = lLector["vendedor"].ToString();
                                lblComprador.Text = lLector["comprador"].ToString();
                                lblPunto.Text = lLector["desc_punto"].ToString();
                                lblCantidad.Text = lLector["cantidad"].ToString();
                                lblPrecio.Text = lLector["precio"].ToString();
                                if (lLector["tipo_garantia_1"].ToString() == "S")
                                    chkGarantia1.Checked = true;
                                else
                                    chkGarantia1.Checked = false;
                                if (lLector["tipo_garantia_2"].ToString() == "S")
                                    chkGarantia2.Checked = true;
                                else
                                    chkGarantia2.Checked = false;
                                if (lLector["tipo_garantia_3"].ToString() == "S")
                                    chkGarantia3.Checked = true;
                                else
                                    chkGarantia3.Checked = false;
                                if (lLector["tipo_garantia_4"].ToString() == "S")
                                    chkGarantia4.Checked = true;
                                else
                                    chkGarantia4.Checked = false;
                                txtValorGrtia.Text = lLector["valor_garantia"].ToString();
                                txtMoneda.Text = lLector["moneda_garantia"].ToString();
                                lblPlazo.Text = lLector["periodo"].ToString();
                                txtFechaIniE.Text = lLector["fecha_inicial"].ToString();
                                txtFechaIniE.Text = lLector["fecha_final"].ToString();
                                txtFechaSus.Text = lLector["fecha_suscripcion"].ToString();
                                txtCuenta.Text = lLector["cuenta_pago"].ToString();
                                txtAtencion.Text = lLector["atencion_a"].ToString();
                                txtDireccion.Text = lLector["direccion"].ToString();
                                txtCorreo.Text = lLector["correo"].ToString();
                                TxtConfCorreo.Text = lLector["correo"].ToString();
                                TxtTelefono.Text = lLector["telefono"].ToString();
                                TxtFax.Text = lLector["fax"].ToString();
                                txtCorreoNom.Text = lLector["correo_nominacion"].ToString();
                                if (lLector["conflicto_1"].ToString() == "S")
                                    chkConflicto1.Checked = true;
                                else
                                    chkConflicto1.Checked = false;
                                if (lLector["conflicto_2"].ToString() == "S")
                                    chkConflicto2.Checked = true;
                                else
                                    chkConflicto2.Checked = false;
                                if (lLector["moneda_1"].ToString() == "S")
                                    chkMoneda1.Checked = true;
                                else
                                    chkMoneda1.Checked = false;
                                if (lLector["moneda_2"].ToString() == "S")
                                    chkMoneda2.Checked = true;
                                else
                                    chkMoneda2.Checked = false;

                                string[] lsNombreParametros1 = { "@P_numero_operacion" };
                                SqlDbType[] lTipoparametros1 = { SqlDbType.Int };
                                string[] lValorParametros1 = { HdfOperacion };
                                try
                                {
                                    lConexion1.Abrir();
                                    dtgUsuarios.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion1.gObjConexion, "pa_GetRegistroContratosUsuMin", lsNombreParametros1, lTipoparametros1, lValorParametros1);
                                    dtgUsuarios.DataBind();
                                    lConexion1.Cerrar();
                                }
                                catch (Exception ex)
                                {
                                    ToastrEvent?.Invoke($"Problemas en la Recuperación de la Información de usuaros finales. {ex.Message}", EnumTypeToastr.Error);
                                }
                                ModalEvent?.Invoke(registroContratoMod.ID, registroContratoModInside.ID, EnumTypeModal.Abrir);
                            }
                        }
                        lConexion.Cerrar();
                    }
                    else
                        ToastrEvent?.Invoke(lsMensaje.ToString(), EnumTypeToastr.Error);
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke($"Problemas en la Recuperación de la Información. {ex.Message}", EnumTypeToastr.Error);
                }

            }

        }


        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo)
        {
            var lsCondicion = "nombre_tabla='t_minuta_contrato' and llave_registro='numero_operacion=" + lscodigo + "'";
            var lsCondicion1 = "numero_operacion=" + lscodigo;
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                var lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_contrato_verificacion";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_contrato_verificacion", lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// Realiza las Validaciones de los Tipos de Datos y Campos Obligatorios de la Pantalla
        /// </summary>
        /// <returns></returns>
        protected string validaciones()
        {
            var lsError = "";
            if (!chkGarantia1.Checked && !chkGarantia2.Checked && !chkGarantia3.Checked && !chkGarantia4.Checked)
                lsError += "Debe seleccionar al menos una garantía";
            if  (txtValorGrtia.Text=="")
                lsError += "Debe digitar el valor de la garantía";
            if (txtMoneda.Text == "")
                lsError += "Debe digitar la moneda de la garantía";
            if (lblPlazo.Text == "")
                lsError += "Debe digitar el plazo de la garantía";
            if (txtCuenta.Text == "")
                lsError += "Debe digitar la cuenta para pagos";
            if (txtAtencion.Text == "")
                lsError += "Debe digitar al campo atención A";
            if (txtDireccion.Text == "")
                lsError += "Debe digitar la dirección";
            if (txtCorreo.Text == "")
                lsError += "Debe digitar el correo";
            if (TxtConfCorreo.Text== "")
                lsError += "Debe digitar la confirmación del correo";
            if (txtCorreo.Text != TxtConfCorreo.Text)
                lsError += "El correo y la confirmación son diferentes";
            if (TxtTelefono.Text == "")
                lsError += "Debe digitar el teléfono";
            if (TxtFax.Text == "")
                lsError += "Debe digitar el fax";
            if (txtCorreoNom.Text == "")
                lsError += "Debe digitar el correo de nominación";
            if (!chkConflicto1.Checked && !chkConflicto2.Checked )
                lsError += "Debe seleccionar un medio de resolución de conflicto";
            if (chkConflicto1.Checked && chkConflicto2.Checked)
                lsError += "Solo debe seleccionar un medio de resolución de conflicto";
            if (!chkMoneda1.Checked && !chkMoneda2.Checked)
                lsError += "Debe seleccionar una moneda de pago";
            if (chkMoneda1.Checked && chkMoneda2.Checked)
                lsError += "Solo debe seleccionar una moneda de pago";
            return lsError;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCrear_Click(object sender, EventArgs e)
        {
            var valido = true;

            string[] lsNombreParametros = { "@P_numero_operacion", "@P_garantia_1", "@P_garantia_2", "@P_garantia_3", "@P_garantia_4",
                "@P_valor_garantia","@P_moneda",  "@P_cuenta", "@P_atencion_a","@P_direccion", "@P_correo", "@P_telefono", "@P_fax","@P_correo_nominacion",
                "@P_conflicto_1", "@P_conflicto_2", "@P_moneda_1", "@P_moneda_2", "@P_codigo_operador"};
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.VarChar,
                SqlDbType.Decimal,SqlDbType.VarChar, SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,
                SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int};
            string[] lValorParametros = { "0", "N", "N", "N", "N", "0","",  "", "", "","", "", "", "", "N", "N", "N", "N", goInfo.cod_comisionista  };

            try
            {
                var val = validaciones();
                if (!string.IsNullOrEmpty(val))
                {
                    ToastrEvent?.Invoke(val, EnumTypeToastr.Warning);
                    valido = false;
                }
                if (valido)
                {
                    lValorParametros[0] = HdfOperacion;
                    if (chkGarantia1.Checked)
                        lValorParametros[1] = "S";
                    if (chkGarantia2.Checked)
                        lValorParametros[2] = "S";
                    if (chkGarantia3.Checked)
                        lValorParametros[3] = "S";
                    if (chkGarantia4.Checked)
                        lValorParametros[4] = "S";
                    lValorParametros[5] = txtValorGrtia.Text;
                    lValorParametros[6] = txtMoneda.Text;
                    lValorParametros[7] = txtCuenta.Text;
                    lValorParametros[8] = txtAtencion.Text;
                    lValorParametros[9] = txtDireccion.Text;
                    lValorParametros[10] = txtCorreo.Text;
                    lValorParametros[11] = TxtTelefono.Text;
                    lValorParametros[12] = TxtFax.Text;
                    lValorParametros[13] = txtCorreoNom.Text;
                    if (chkConflicto1.Checked)
                        lValorParametros[14] = "S";
                    if (chkConflicto2.Checked)
                        lValorParametros[15] = "S";
                    if (chkMoneda1.Checked)
                        lValorParametros[16] = "S";
                    if (chkMoneda2.Checked)
                        lValorParametros[17] = "S";


//                    SqlTransaction oTransaccion;
                    lConexion.Abrir();
  //                  oTransaccion = lConexion.gObjConexion.BeginTransaction(IsolationLevel.Serializable);
                    goInfo.mensaje_error = "";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetMinutaC1Bim", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        ToastrEvent?.Invoke("Se presentó un Problema en la Creación del Registro del Contrato.! ", EnumTypeToastr.Error);
                        //oTransaccion.Rollback();
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        if (lLector.HasRows)

                        {
                            lLector.Read();
                            ToastrEvent?.Invoke("La minuta quedó en estado " + lLector["descripcion_estado"].ToString(), EnumTypeToastr.Success);
                        }
                        else
                            ToastrEvent?.Invoke("No se creo el registro correctamente", EnumTypeToastr.Error);
                        lLector.Close();
                        lLector.Dispose();
                        //oTransaccion.Commit();


                        ModalEvent?.Invoke(registroContratoMod.ID, registroContratoModInside.ID, EnumTypeModal.Cerrar);
                        CargarDatos();
                    }
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            var valido = true;

            string[] lsNombreParametros = { "@P_numero_operacion", "@P_garantia_1", "@P_garantia_2", "@P_garantia_3", "@P_garantia_4",
                "@P_valor_garantia", "@P_moneda", "@P_cuenta", "@P_atencion","@P_direccion",  "@P_correo", "@P_telefono", "@P_fax","@P_correo_nominacion",
                "@P_conflicto_1", "@P_conflicto_2", "@P_moneda_1", "@P_moneda_2", "@P_codigo_operador"};
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.VarChar,
                SqlDbType.Decimal, SqlDbType.Int, SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,
                SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int};
            string[] lValorParametros = { "0", "N", "N", "N", "N", "0", "0", "", "", "", "", "","", "", "N", "N", "N", "N", goInfo.cod_comisionista };


            try
            {
                var val = validaciones();
                if (!string.IsNullOrEmpty(val))
                {
                    ToastrEvent?.Invoke(val, EnumTypeToastr.Warning);
                    valido = false;
                }
                if (valido)
                {
                    lValorParametros[0] = HdfOperacion;
                    if (chkGarantia1.Checked)
                        lValorParametros[1] = "S";
                    if (chkGarantia2.Checked)
                        lValorParametros[2] = "S";
                    if (chkGarantia3.Checked)
                        lValorParametros[3] = "S";
                    if (chkGarantia4.Checked)
                        lValorParametros[4] = "S";
                    lValorParametros[5] = txtValorGrtia.Text;
                    lValorParametros[6] = txtMoneda.Text;
                    lValorParametros[7] = txtCuenta.Text;
                    lValorParametros[8] = txtAtencion.Text;
                    lValorParametros[9] = txtDireccion.Text;
                    lValorParametros[10] = txtCorreo.Text;
                    lValorParametros[11] = TxtTelefono.Text;
                    lValorParametros[12] = TxtFax.Text;
                    lValorParametros[13] = txtCorreoNom.Text;
                    if (chkConflicto1.Checked)
                        lValorParametros[14] = "S";
                    if (chkConflicto2.Checked)
                        lValorParametros[15] = "S";
                    if (chkMoneda1.Checked)
                        lValorParametros[16] = "S";
                    if (chkMoneda2.Checked)
                        lValorParametros[17] = "S";


                    //SqlTransaction oTransaccion;
                    lConexion.Abrir();
                    //oTransaccion = lConexion.gObjConexion.BeginTransaction(IsolationLevel.Serializable);
                    goInfo.mensaje_error = "";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetMinutaC1Bim", lsNombreParametros, lTipoparametros, lValorParametros,  goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        ToastrEvent?.Invoke("Se presentó un Problema en la Creación del Registro del Contrato.! ", EnumTypeToastr.Error);
                        //oTransaccion.Rollback();
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        if (lLector.HasRows)

                        {
                            lLector.Read();
                            ToastrEvent?.Invoke("La minuta quedó en estado " + lLector["descripcion_estado"].ToString(), EnumTypeToastr.Success);
                        }
                        else
                            ToastrEvent?.Invoke("No se creo el registro correctamente", EnumTypeToastr.Error);
                        lLector.Close();
                        lLector.Dispose();
                        lLector.Close();
                        lLector.Dispose();
                        //oTransaccion.Commit();

                        ModalEvent?.Invoke(registroContratoMod.ID, registroContratoModInside.ID, EnumTypeModal.Cerrar);
                        CargarDatos();
                    }
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }


        public void btnConsultar_Click(object sender, EventArgs e)
        {
            dtgConsulta.CurrentPageIndex = 0;//20190130 ajuste
            CargarDatos();
        }


        protected void btnSalir_Click(object sender, EventArgs e)
        {
            CargarDatos();
        }
    }
}