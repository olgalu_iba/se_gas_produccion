﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucInfTransaccionalGestor.ascx.cs" Inherits="UsersControls.Contratos.Registro_Contratos.Registro_Contratos.WucInfTransaccionalGestor" %>

<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <asp:Label ID="Label2" AssociatedControlID="FuArchivo" runat="server">Archivo Contratos</asp:Label>
            <asp:FileUpload ID="FuArchivo" runat="server" CssClass="form-control" EnableTheming="true" />
            <%--20210224--%>
            <asp:HiddenField ID="hndArchivo" runat="server" />
            <asp:HiddenField ID="hndArchivoUsu" runat="server" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <asp:Label ID="Label3" runat="server" AssociatedControlID="FuArchivoUsuarios">Archivo Usuarios Finales por Contrato (Solo Compradores):</asp:Label>
            <asp:FileUpload ID="FuArchivoUsuarios" CssClass="form-control" runat="server" EnableTheming="true" />

        </div>
    </div>
</div>


<%--20210224 pasa validaciones--%>
<div class="modal fade" id="mdlConfirma" tabindex="-1" role="dialog" aria-labelledby="mdlConfirmaLabel" aria-hidden="true" clientidmode="Static" runat="server">
    <div class="modal-dialog" id="mdlConfirmaInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                <ContentTemplate>
                    <div class="modal-header" style="background-color: #3E5F8A;" >
                        <h5 class="modal-title" id="mdlConfirmaLabel" runat="server"  style ="color: white;">Confirmación</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff; opacity: 1;">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="background-color: #D3D3D3">
                        <asp:Label ID="lblMensajeCOnf" runat="server">Se presentan las siguientes validaciones:</asp:Label>
                    </div>
                    <div class="modal-body" style="background-color: #D3D3D3">
                        <asp:Label ID="lblValidacion" runat="server"></asp:Label>
                    </div>
                    <div class="modal-body" style="background-color: #D3D3D3">
                        <asp:Label ID="lblConforma" runat="server">Desea continuar de todas formas?</asp:Label>
                    </div>
                    <div class="modal-footer" style="background-color: #3E5F8A">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <asp:Button ID="btnAceptarConf" CssClass="btn btn-primary" Text="Aceptar" OnClick="BtnAceptarConf_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
