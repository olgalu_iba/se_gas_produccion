﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace UsersControls.Contratos.Registro_Contratos.Registro_Contratos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class WucCargaArchivoNegBila : UserControl
    {
        #region EventHandler

        /// <summary>
        /// Delegado para el manejo de los modals
        /// </summary>
        public delegate void EventHandlerModal(string id, string insideId, EnumTypeModal typeModal, Modal.Size size = Modal.Size.Large);

        /// <summary>
        ///  EventHandler para la selección de los los modals
        /// </summary>
        public event EventHandlerModal ModalEvent;

        /// <summary>
        ///  Delegado para el manejo de los toastr
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        /// EventHandler para la selección de los toastr 
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        /// Delegado para el manejo del log de cargue de archivos
        /// </summary>
        public delegate void EventHandlerLogCargaArchivo(StringBuilder message);

        /// <summary>
        /// EventHandler para el log de cargue de archivos
        /// </summary>
        public event EventHandlerLogCargaArchivo LogCargaArchivoEvent;

        /// <summary>
        /// Delegado para la selección de los botones  
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get { return ViewState["NameController"] != null && !string.IsNullOrEmpty(ViewState["NameController"].ToString()) ? ViewState["NameController"].ToString() : string.Empty; }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        #endregion Propiedades

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            EnumBotones[] botones = { EnumBotones.Cargue };
            ButtonsEvent?.Invoke(botones, null, NameController);
            //Se establece que el controlador ya se inicializo    
            Inicializado = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void BtnCargar_Click(object sender, EventArgs e)
        {
            var goInfo = (InfoSessionVO)Session["infoSession"];
            var lConexion = new clConexion(goInfo);
            string strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
            string strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];

            var lsRutaArchivo = "";
            var lsNombre = "";
            string[] lsErrores = { "", "" };
            var lsCadenaArchivo = new StringBuilder();
            var oCargaOK = true;
            var lsProcedimiento = ""; // Campo nuevo contingencia 20151016
            SqlDataReader lLector;
            var lComando = new SqlCommand();
            int liNumeroParametros;
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_codigo_operador", "@P_ruta_ftp", "@P_codigo_usuario", "@P_valida" }; //20210224
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar };//20210224
            Object[] lValorParametrosO = { "", goInfo.cod_comisionista, strRutaFTP, goInfo.codigo_usuario, "S" };//20210224
            lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;
            hndArchivo.Value = lsNombre; //20210224
            try
            {
                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);

                /// Realiza las Validaciones de los Archivos
                if (FuArchivo.FileName != "")
                    lsErrores = ValidarArchivo(lsRutaArchivo);
                else
                {
                    ToastrEvent?.Invoke(HttpContext.GetGlobalResourceObject("AppResources", "ArchivoRequerido", CultureInfo.CurrentCulture)?.ToString(), EnumTypeToastr.Error);
                    return;
                }

                if (lsErrores[0] == "")
                {
                    if (DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]))
                    {
                        lsProcedimiento = ddlTipoCargue.SelectedValue == "N" ? "pa_ValidaPlanoNegoDirecta" : "pa_ValidaPlanoNegoDirectaCont";
                        lValorParametrosO[0] = lsNombre;
                        lValorParametrosO[1] = goInfo.cod_comisionista;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = lsProcedimiento;
                        lComando.CommandTimeout = 3600;
                        if (lsNombreParametrosO != null)
                        {
                            for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                            {
                                lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                            }
                        }
                        string lsErrror = "N"; //20201124
                        string lsConfirma = "N"; //20201124
                        lblValidacion.Text = "";//20201124
                        lLector = lComando.ExecuteReader();
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {

                                //20201124
                                if (lLector["ind_error"].ToString() == "S")
                                {
                                    lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                                    lsErrror = "S";
                                }
                                else
                                {
                                    lblValidacion.Text += lLector["mensaje"] + "<br>";
                                    lsConfirma = "S";
                                }
                            }
                            //Se él modal de confirmación
                            //20210224
                            if (lsErrror == "N" && lsConfirma == "S")
                                ModalEvent?.Invoke(mdlConfirma.ID, mdlConfirmaInside.ID, EnumTypeModal.Abrir, Modal.Size.Normal);
                        }
                        else
                        {
                            //cambio de mesanjes 20150616
                            AbrirConfirmacion();
                        }

                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        ToastrEvent?.Invoke(HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString(), EnumTypeToastr.Error);
                    }
                }
                else
                {
                    lsCadenaArchivo.Append(lsErrores[0]);
                    DelegadaBase.Servicios.registrarProceso(goInfo, "Finalizó la carga con errores", "Usuario : " + goInfo.nombre);
                }
                //Se descarga el log para el usuario
                LogCargaArchivoEvent?.Invoke(lsCadenaArchivo);

                ScriptManager.RegisterStartupScript(this, GetType(), "StartupAlert", "DetenerCrono();", true);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20210224
        protected void BtnAceptarConf_Click(object sender, EventArgs e)
        {
            ModalEvent?.Invoke(mdlConfirma.ID, mdlConfirmaInside.ID, EnumTypeModal.Cerrar);

            var goInfo = (InfoSessionVO)Session["infoSession"];
            var lConexion = new clConexion(goInfo);
            string strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];

            var lsRutaArchivo = "";
            var lsNombre = "";
            string[] lsErrores = { "", "" };
            var lsCadenaArchivo = new StringBuilder();
            var lsProcedimiento = ""; // Campo nuevo contingencia 20151016
            SqlDataReader lLector;
            var lComando = new SqlCommand();
            int liNumeroParametros;
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_codigo_operador", "@P_ruta_ftp", "@P_codigo_usuario", "@P_valida" };
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar };
            Object[] lValorParametrosO = { "", goInfo.cod_comisionista, strRutaFTP, goInfo.codigo_usuario, "N" };
            try
            {

                lsProcedimiento = ddlTipoCargue.SelectedValue == "N" ? "pa_ValidaPlanoNegoDirecta" : "pa_ValidaPlanoNegoDirectaCont";
                lValorParametrosO[0] = hndArchivo.Value;
                lValorParametrosO[1] = goInfo.cod_comisionista;
                lConexion.Abrir();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = lsProcedimiento;
                lComando.CommandTimeout = 3600;
                if (lsNombreParametrosO != null)
                {
                    for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                    {
                        lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                    }
                }
                lLector = lComando.ExecuteReader();
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                    {
                        lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                    }
                }
                else
                {
                    //cambio de mesanjes 20150616
                    AbrirConfirmacion();
                }

                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();

                //Se descarga el log para el usuario
                LogCargaArchivoEvent?.Invoke(lsCadenaArchivo);

                ScriptManager.RegisterStartupScript(this, GetType(), "StartupAlert", "DetenerCrono();", true);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            decimal ldValor = 0;
            var liTotalRegistros = 0;
            Int64 liValor = 0;
            DateTime ldFecha;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };
            string[] lsFecha;  //20171130 rq026-17

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 30)) //20170814 rq036  //20171130 rq026-17 //20201124 //20210708
                    {
                        lsCadenaErrores = lsCadenaErrores + "El número de campos no corresponde con la estructura del plano { 30},  en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //20170814 rq036  //20171130 rq026-17 //2020819 ajuste OM //20201124
                    }
                    else
                    {
                        /// Validar Punta
                        if (oArregloLinea.GetValue(0).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar la punta {" + oArregloLinea.GetValue(0).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";  //2020819 ajuste OM 
                        else
                        {
                            if (oArregloLinea.GetValue(0).ToString().Trim() != "C" && oArregloLinea.GetValue(0).ToString().Trim() != "V")
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en la punta {" + oArregloLinea.GetValue(0).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM 
                        }
                        /// Destino Rueda
                        if (oArregloLinea.GetValue(1).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar el producto {" + oArregloLinea.GetValue(1).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //20200729 ajsute auditoria //2020819 ajuste OM
                        else
                        {
                            if (oArregloLinea.GetValue(1).ToString().Trim() != "T" && oArregloLinea.GetValue(1).ToString().Trim() != "G")
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en el producto {" + oArregloLinea.GetValue(1).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //20200729 ajsute auditoria //2020819 ajuste OM
                        }
                        /// Tipo Mercado
                        if (oArregloLinea.GetValue(2).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar el tipo de mercado {" + oArregloLinea.GetValue(2).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        else
                        {
                            if (oArregloLinea.GetValue(2).ToString().Trim() != "P" && oArregloLinea.GetValue(2).ToString().Trim() != "S")
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en el tipo de mercado {" + oArregloLinea.GetValue(2).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        }
                        /// Valida Operador Punta Contraria
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(3).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del operador contraparte {" + oArregloLinea.GetValue(3).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        }
                        /// Valida Punto de entrega
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(4).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del punto de entrega {" + oArregloLinea.GetValue(4).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// Valida Tipo de Contrato
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(5).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código de la modalidad contractual {" + oArregloLinea.GetValue(5).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //20200701 ajuste auditoria
                        }
                        /// Valida Fecha de Negociacion
                        try
                        {
                            ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(6).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La fecha de negociación {" + oArregloLinea.GetValue(6).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        }
                        /// Valida HOra de negociación
                        try
                        {
                            ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(7).ToString());
                            if (oArregloLinea.GetValue(7).ToString().Length != 5)
                                lsCadenaErrores = lsCadenaErrores + "La hora de negociación {" + oArregloLinea.GetValue(7).ToString() + "} es inválida, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La hora de negociación {" + oArregloLinea.GetValue(7).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        }
                        /// Valida Periodo de Entrega
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(8).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del periodo de entrega {" + oArregloLinea.GetValue(8).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        }
                        /// Valida Años
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(9).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El campo número de años {" + oArregloLinea.GetValue(9).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                        }
                        /// Valida HOra inicial
                        /// 20210308 ajuse OM
                        if (oArregloLinea.GetValue(10).ToString() != "0")
                        {
                            try
                            {

                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(10).ToString());
                                if (oArregloLinea.GetValue(10).ToString().Length != 5)
                                    lsCadenaErrores = lsCadenaErrores + "La hora inicial {" + oArregloLinea.GetValue(10).ToString() + "} es inválida, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La hora inicial {" + oArregloLinea.GetValue(10).ToString() + "} es inválida, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            }
                        }
                        /// Valida HOra final
                        /// 20210308 ajuse OM
                        if (oArregloLinea.GetValue(11).ToString() != "0")
                        {
                            try
                            {

                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(11).ToString());
                                if (oArregloLinea.GetValue(11).ToString().Length != 5)
                                    lsCadenaErrores = lsCadenaErrores + "La hora final {" + oArregloLinea.GetValue(11).ToString() + "} es inválida, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La hora final {" + oArregloLinea.GetValue(11).ToString() + "} es inválida, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            }
                        }

                        /// Valida Fecha Inicial
                        try
                        {
                            ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(12).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La fecha inicial {" + oArregloLinea.GetValue(12).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        }
                        /// Valida Fecha final 
                        if (oArregloLinea.GetValue(13).ToString().Trim() != "")
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(13).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La fecha final {" + oArregloLinea.GetValue(13).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            }
                        /// Valida Precio
                        try
                        {
                            ldValor = Convert.ToDecimal(oArregloLinea.GetValue(14).ToString());
                            string sOferta = ldValor.ToString();
                            int iPos = sOferta.IndexOf(".");
                            if (iPos > 0)
                                if (sOferta.Length - iPos > 3)
                                    lsCadenaErrores = lsCadenaErrores + "El precio {" + oArregloLinea.GetValue(14).ToString() + "} solo puede tener 2 decimales, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM

                            if (Convert.ToDouble(sOferta) <= 0)
                                lsCadenaErrores = lsCadenaErrores + "El precio {" + oArregloLinea.GetValue(14).ToString() + "} debe ser mayor que 0, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM

                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El precio {" + oArregloLinea.GetValue(14).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// Valida Cantidad
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(15).ToString());
                            if (liValor <= 0)
                                lsCadenaErrores = lsCadenaErrores + "La cantidad {" + oArregloLinea.GetValue(15).ToString() + "} debe ser mayor que 0, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La cantidad {" + oArregloLinea.GetValue(15).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        //20170814 rq036
                        if (oArregloLinea.GetValue(16).ToString().Length > 8)
                            lsCadenaErrores = lsCadenaErrores + "El código del Centro Poblado {" + oArregloLinea.GetValue(16).ToString() + "} dbe tener máximo 8 carácteres , en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        else
                            try
                            {
                                liValor = Convert.ToInt32(oArregloLinea.GetValue(16).ToString());
                                if (liValor < 0)
                                    lsCadenaErrores = lsCadenaErrores + "El código del Centro Poblado {" + oArregloLinea.GetValue(16).ToString() + "} no es válido , en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El código del Centro Poblado {" + oArregloLinea.GetValue(16).ToString() + "} no es válido , en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                            }
                        //20171130 rq026-17 valida fecha de suscripcion
                        if (oArregloLinea.GetValue(17).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar la fecha de suscripción {" + oArregloLinea.GetValue(17).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(17).ToString());
                                lsFecha = oArregloLinea.GetValue(17).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "Formato inválido en la fecha de suscripción {" + oArregloLinea.GetValue(17).ToString() + "},debe ser {YYYY/MM/DD}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La fecha de suscripción {" + oArregloLinea.GetValue(17).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            }
                        }
                        ///  20171130 rq026-17 Validar Sentido del Flujo
                        if (oArregloLinea.GetValue(18).ToString().Trim().Length > 0)
                        {
                            if (oArregloLinea.GetValue(18).ToString().Trim() != "NORMAL" && oArregloLinea.GetValue(18).ToString().Trim() != "CONTRAFLUJO") //20210915 ajsute
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en el sentido del flujo, valores válidos {NORMAL o CONTRAFLUJO} {" + oArregloLinea.GetValue(18).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM //20210915
                        }

                        /// 20171130 rq026-17 Validar Presion Punto Terminacion
                        if (oArregloLinea.GetValue(19).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar  la presión del punto de terminación (Valor por defecto 0) {" + oArregloLinea.GetValue(19).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        else
                        {
                            string[] lsPresion;
                            try
                            {
                                if (oArregloLinea.GetValue(19).ToString().Trim().Length > 500)
                                    lsCadenaErrores = lsCadenaErrores + "Longitud del campo presión del punto de terminación {" + oArregloLinea.GetValue(19).ToString() + "} supera el máximo permitido (500 caracteres), en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                                else
                                {
                                    lsPresion = oArregloLinea.GetValue(19).ToString().Trim().Split('-');
                                    foreach (string Presion in lsPresion)
                                    {
                                        try
                                        {
                                            ldValor = Convert.ToDecimal(Presion.Trim());
                                        }
                                        catch (Exception)
                                        {
                                            lsCadenaErrores = lsCadenaErrores + "Valor inválido en la presión del punto de terminación {" + Presion.Trim() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en la presión del punto de terminación {" + oArregloLinea.GetValue(19).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            }
                        }
                        /// 20171130 rq026-17 Valida Fuente o campo MP 20160603
                        if (oArregloLinea.GetValue(20).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar el código de la fuente {" + oArregloLinea.GetValue(20).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt16(oArregloLinea.GetValue(20).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en el código de la fuente {" + oArregloLinea.GetValue(20).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            }
                        }
                        /// 20171130 rq026-17 Validar No. Contrato
                        if (oArregloLinea.GetValue(21).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar el número de contrato {" + oArregloLinea.GetValue(21).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                        /// 20171130 rq026-17 valida tipo contrato
                        if (oArregloLinea.GetValue(22).ToString().Trim() != "N" && oArregloLinea.GetValue(22).ToString().Trim() != "S")
                            lsCadenaErrores = lsCadenaErrores + "Valor inválido en el indicador de contrato variable  {" + oArregloLinea.GetValue(22).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        /// 20171130 rq026-17 valida conectado al snt
                        if (oArregloLinea.GetValue(23).ToString().Trim() != "N" && oArregloLinea.GetValue(23).ToString().Trim() != "S")
                            lsCadenaErrores = lsCadenaErrores + "Valor inválido en el indicador de conexión al SNT {" + oArregloLinea.GetValue(23).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        /// 20171130 rq026-17 valida entrega en boca de pozo
                        if (oArregloLinea.GetValue(24).ToString().Trim() != "N" && oArregloLinea.GetValue(24).ToString().Trim() != "S")
                            lsCadenaErrores = lsCadenaErrores + "Valor inválido en la entrega en boca de pozo {" + oArregloLinea.GetValue(24).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        /// 20201124
                        /// Calsificacion de transporte
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(26).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La clasificación de transporte {" + oArregloLinea.GetValue(26).ToString() + "} es inválida, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                        }
                        /// 202010707
                        /// tipo tasa
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(27).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La tasa de cambio {" + oArregloLinea.GetValue(27).ToString() + "} es inválida, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; 
                        }
                        /// 202010707
                        /// observaciones
                        if (oArregloLinea.GetValue(28).ToString().Length ==0)
                            lsCadenaErrores = lsCadenaErrores + "Las observaciones de la tasa de cambio {" + oArregloLinea.GetValue(28).ToString() + "} son inválidas, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        /// 202010707
                        /// tipo moneda
                        if (oArregloLinea.GetValue(29).ToString() !="USD" && oArregloLinea.GetValue(29).ToString() != "COP")
                            lsCadenaErrores = lsCadenaErrores + "El tipo de moneda {" + oArregloLinea.GetValue(29).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        protected void AbrirConfirmacion()
        {
            var goInfo = (InfoSessionVO)Session["infoSession"];
            var lConexion = new clConexion(goInfo);
            SqlDataReader lLector;
            lblMensaje.Text = "";
            SqlCommand lComando = new SqlCommand();
            lConexion = new clConexion(goInfo);
            try
            {
                string[] lsNombreParametros = { "@P_codigo_usuario" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int };
                Object[] lValorParametros = { goInfo.codigo_usuario };
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetPlanoNegoDirHoraNeg", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Text += lLector["mensaje"].ToString();
                    btnAceptar.Enabled = true;
                }
                else
                {
                    lblMensaje.Text = "No hay registros para cargar";
                    btnAceptar.Enabled = false;
                }
                lConexion.Cerrar();

                if (!string.IsNullOrEmpty(lblMensaje.Text))
                {
                    ToastrEvent?.Invoke(lblMensaje.Text, EnumTypeToastr.Info);
                }
                //Se abre el modal de la confirmación 
                ModalEvent?.Invoke(mdlFechaOpBil.ID, mdlFechaOpBilInside.ID, EnumTypeModal.Abrir);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                SqlDataReader lLector;
                SqlCommand lComando = new SqlCommand();
                var goInfo = (InfoSessionVO)Session["infoSession"];
                var lConexion = new clConexion(goInfo);
                string[] lsNombreParametros = { "@P_codigo_usuario", "@P_codigo_operador" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
                Object[] lValorParametros = { goInfo.codigo_usuario, goInfo.cod_comisionista };
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetPlanoNegoDirecta", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    string lsError = "";
                    while (lLector.Read())
                    {
                        lsError += lLector["mensaje"] + "\\n";  //rq026-17  20171130
                    }
                    //Se notifica al usuario el valor de la transacción 
                    if (lsError.Contains("Se generó"))
                    {
                        ToastrEvent?.Invoke(lsError, EnumTypeToastr.Success);
                        //Se cierra el modal de la confirmación 
                        ModalEvent?.Invoke(mdlFechaOpBil.ID, mdlFechaOpBilInside.ID, EnumTypeModal.Cerrar);
                    }
                    else
                    {
                        ToastrEvent?.Invoke(lsError, EnumTypeToastr.Error);
                    }
                }
                else
                {
                    ToastrEvent?.Invoke("Operaciones disponibles en el módulo Registro de Contratos, para ingreso de información transaccional.!", EnumTypeToastr.Warning);
                    //Se cierra el modal de la confirmación 
                    ModalEvent?.Invoke(mdlFechaOpBil.ID, mdlFechaOpBilInside.ID, EnumTypeModal.Cerrar);
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Problemas en la Carga del Plano.!", EnumTypeToastr.Error);
            }
        }
    }
}