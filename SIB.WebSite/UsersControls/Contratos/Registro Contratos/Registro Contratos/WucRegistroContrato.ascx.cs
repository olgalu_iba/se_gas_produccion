﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;
using System.Drawing;

namespace UsersControls.Contratos.Registro_Contratos.Registro_Contratos
{
    public partial class WucRegistroContrato : UserControl
    {
        #region EventHandler

        /// <summary>
        /// Delegado para el manejo de los modals
        /// </summary>
        public delegate void EventHandlerModal(string id, string insideId, EnumTypeModal typeModal, Modal.Size size = Modal.Size.Large);

        /// <summary>
        /// EventHandler para la selección de los los modals
        /// </summary>
        public event EventHandlerModal ModalEvent;

        /// <summary>
        /// Delegado para el manejo de los toastr
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        /// EventHandler para la selección de los toastr 
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        /// Delegado para la selección de los botones  s  
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// 
        /// </summary>
        public string NoHorasPrim
        {
            get { return ViewState["NoHorasPrim"] != null && !string.IsNullOrEmpty(ViewState["NoHorasPrim"].ToString()) ? ViewState["NoHorasPrim"].ToString() : "0"; }
            set { ViewState["NoHorasPrim"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string NoDiasSecun
        {
            get { return ViewState["NoDiasSecun"] != null && !string.IsNullOrEmpty(ViewState["NoDiasSecun"].ToString()) ? ViewState["NoDiasSecun"].ToString() : "0"; }
            set { ViewState["NoDiasSecun"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Contrato
        {
            get { return ViewState["Contrato"] != null && !string.IsNullOrEmpty(ViewState["Contrato"].ToString()) ? ViewState["Contrato"].ToString() : null; }
            set { ViewState["Contrato"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Indica
        {
            get { return ViewState["Indica"] != null && !string.IsNullOrEmpty(ViewState["Indica"].ToString()) ? ViewState["Indica"].ToString() : null; }
            set { ViewState["Indica"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Consec
        {
            get { return ViewState["Consec"] != null && !string.IsNullOrEmpty(ViewState["Consec"].ToString()) ? ViewState["Consec"].ToString() : "0"; }
            set { ViewState["Consec"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Actual
        {
            get { return ViewState["Actual"] != null && !string.IsNullOrEmpty(ViewState["Actual"].ToString()) ? ViewState["Actual"].ToString() : "0"; }
            set { ViewState["Actual"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string IndModif
        {
            get { return ViewState["IndModif"] != null && !string.IsNullOrEmpty(ViewState["IndModif"].ToString()) ? ViewState["IndModif"].ToString() : "0"; }
            set { ViewState["IndModif"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfDestinoRueda
        {
            get { return ViewState["HdfDestinoRueda"] != null && !string.IsNullOrEmpty(ViewState["HdfDestinoRueda"].ToString()) ? ViewState["HdfDestinoRueda"].ToString() : string.Empty; }
            set { ViewState["HdfDestinoRueda"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfTipoMerc
        {
            get { return ViewState["HdfTipoMerc"] != null && !string.IsNullOrEmpty(ViewState["HdfTipoMerc"].ToString()) ? ViewState["HdfTipoMerc"].ToString() : string.Empty; }
            set { ViewState["HdfTipoMerc"] = value; }
        }

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get { return ViewState["NameController"] != null && !string.IsNullOrEmpty(ViewState["NameController"].ToString()) ? ViewState["NameController"].ToString() : string.Empty; }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }
        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        /// 20210224
        public string HdfValida
        {
            get { return ViewState["HdfValida"] != null && !string.IsNullOrEmpty(ViewState["HdfValida"].ToString()) ? ViewState["HdfValida"].ToString() : string.Empty; }
            set { ViewState["HdfValida"] = value; }
        }

        #endregion Propiedades

        private InfoSessionVO goInfo = null;
        private static string lsTitulo = "Registro de Contratos";
        private clConexion lConexion = null;
        private clConexion lConexion1 = null;
        private SqlDataReader lLector;
        private string sRutaArc = ConfigurationManager.AppSettings["rutaCarga"];

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                goInfo = (InfoSessionVO)Session["infoSession"];
                goInfo.Programa = lsTitulo;
                lConexion = new clConexion(goInfo);
                lConexion1 = new clConexion(goInfo);
                if (IsPostBack) return;
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Excel };
            ButtonsEvent?.Invoke(botones, null, NameController);
            // Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlProducto, "m_producto", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlComprador, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4);  //20180126 rq107-16
            LlenarControles(lConexion.gObjConexion, ddlVendedor, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20180126 rq107-16
            LlenarControles(lConexion.gObjConexion, ddlEstado, "m_estado_gas", " tipo_estado = 'V' order by descripcion_estado", 2, 3); //20180126 rq107-16
            LlenarControles(lConexion.gObjConexion, ddlMercadoRel, "m_mercado_relevante", " estado ='A' order by descripcion", 0, 1); //Mercado relevante sector 20160125
            LlenarControles(lConexion.gObjConexion, ddlFuente, "m_pozo", " ind_campo_pto='C' and estado ='A' order by descripcion", 0, 1); //Campo o fuente MP 20160601
            LlenarControles1(lConexion.gObjConexion, ddlCentro, "m_divipola", " codigo_centro <> '0' order by nombre_centro", 5, 6); //20170814 rq036-17
            LlenarControles(lConexion.gObjConexion, ddlPeriodo, "m_periodos_entrega", " estado ='A' order by descripcion", 0, 1); //20171130 rq026-17
            LlenarControles2(lConexion.gObjConexion, ddlCausa, "", "", 0, 1); //20171130 rq026-17
            LlenarControles(lConexion.gObjConexion, DdlClasifica, "m_clasifica_transporte", " 1=1  order by codigo_clasificacion", 0, 1);//20201124 sub tansporte
            LlenarControles(lConexion.gObjConexion, ddlTrm, "m_tipo_trm", " estado ='A'  order by descripcion", 0, 1);//20210707 trm moneda
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
            if (lLector.HasRows)
            {
                lLector.Read();
                hdfNoHorasPrim.Value = lLector["no_horas_correcion_primario"].ToString();
                hdfNoDiasSecun.Value = lLector["no_dias_cal_correc_secundario"].ToString();

            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();

            //Se establece que el controlador ya se inicializo    
            Inicializado = true;
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            var lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                var lItem1 = new ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"] + "-" + lLector["razon_social"];
                }
                lDdl.Items.Add(lItem1);

            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles1
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// 20170814 rq036-17
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            var lItem = new ListItem { Value = "0", Text = "Seleccione" };
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                var lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["nombre_centro"] + "-" + lLector["nombre_ciudad"] + "-" + lLector["nombre_departamento"];
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles2(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
            //dtgComisionista.DataBind();

            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetCausaModAdc", null, null, null);
            var lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);
            while (lLector.Read())
            {
                var lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["descripcion"].ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: dtgConsulta_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgConsulta.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            //this.dtgConsulta.CurrentPageIndex = 0;//20171130 rq026-17  //ajuste 20190130
            DateTime ldFecha;
            var liValor = 0;
            var valido = true;

            //20210815 ajuste cambi de nombre y parametros
            string[] lsNombreParametros = { "@PP_tipo_subasta", "@PP_numero_contrato", "@PP_fecha_contrato", "@PP_mercado", "@PP_codigo_producto",
                "@PP_operador_compra", "@PP_operador_venta","@PP_tipo_perfil","@PP_codigo_operador", "@PP_estado" ,
                "@PP_fecha_contrato_fin", "@PP_numero_contrato_fin" ,//20161207 rq102 conformacion de rutas
                "@PP_codigo_causa", "@PP_codigo_verif"}; //20171130 rq026-17
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int,
                SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar,
                SqlDbType.VarChar, SqlDbType.Int, //20161207 rq102 conformacion de rutas
                SqlDbType.VarChar, SqlDbType.Int};//20171130 rq026-17
            string[] lValorParametros = { "0", "0", "", "", "0", "0", "0", Session["tipoPerfil"].ToString(), goInfo.cod_comisionista, "0", "", "0", "0", "0" };//20161207 rq102 conformacion de rutas //20171130 rq026-17

            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception)
                {
                    ToastrEvent?.Invoke("Formato Inválido en el Campo Fecha Inicial de Negociación.", EnumTypeToastr.Error);//20161207 rq102 conformacion de rutas
                    valido = false;
                }
            }
            //20161207 rq102 conformacion de rutas
            if (TxtFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaFin.Text);
                }
                catch (Exception)
                {
                    ToastrEvent?.Invoke("Formato Inválido en el Campo Fecha Final de Negociación.", EnumTypeToastr.Error);
                    valido = false;
                }
            }
            //20161207 rq102 conformacion de rutas
            if (TxtFechaIni.Text.Trim().Length == 0 && TxtFechaFin.Text.Trim().Length > 0)
            {
                ToastrEvent?.Invoke("Debe digitar la Fecha Inicial de Negociación.", EnumTypeToastr.Warning);
                valido = false;
            }

            //20161207 rq102 conformacion de rutas
            if (TxtFechaIni.Text.Trim().Length > 0 && TxtFechaFin.Text.Trim().Length > 0)
                try
                {
                    if (Convert.ToDateTime(TxtFechaFin.Text) < Convert.ToDateTime(TxtFechaIni.Text))
                    {
                        ToastrEvent?.Invoke("La Fecha inicial de Negociación debe ser menor o igual que la fecha final.", EnumTypeToastr.Warning);
                        valido = false;
                    }
                }
                catch (Exception)
                { }
            if (TxtNoContrato.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoContrato.Text);
                }
                catch (Exception)
                {
                    ToastrEvent?.Invoke("Formato Inválido en el Campo No Operación.", EnumTypeToastr.Error);  // 20161207 rq102 conformación de rutas
                    valido = false;
                }

            }
            // 20161207 rq102 conformación de rutas
            if (TxtNoContratoFin.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoContratoFin.Text);
                }
                catch (Exception)
                {
                    ToastrEvent?.Invoke("Formato Inválido en el Campo No Operación final.", EnumTypeToastr.Error);
                    valido = false;
                }

            }
            // 20161207 rq102 conformación de rutas
            if (TxtNoContrato.Text.Trim().Length == 0 && TxtNoContratoFin.Text.Trim().Length > 0)
            {
                ToastrEvent?.Invoke("Debe digitar el No de operación inicial antes que el final.", EnumTypeToastr.Warning);
                valido = false;
            }
            // 20161207 rq102 conformación de rutas
            if (TxtNoContrato.Text.Trim().Length > 0 && TxtNoContratoFin.Text.Trim().Length > 0)
            {
                try
                {
                    if (Convert.ToInt32(TxtNoContratoFin.Text) < Convert.ToInt32(TxtNoContrato.Text))
                    {
                        ToastrEvent?.Invoke("El No Operación inicial debe ser menor o igual que el final.", EnumTypeToastr.Warning);
                        valido = false;
                    }
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
                }
            }
            if (valido)
            {
                try
                {
                    if (TxtNoContrato.Text.Trim().Length > 0)
                        lValorParametros[1] = TxtNoContrato.Text.Trim();
                    if (ddlSubasta.SelectedValue != "0")
                        lValorParametros[0] = ddlSubasta.SelectedValue;
                    if (TxtFechaIni.Text.Trim().Length > 0)
                        lValorParametros[2] = TxtFechaIni.Text.Trim();
                    if (ddlMercado.SelectedValue != "")
                        lValorParametros[3] = ddlMercado.SelectedValue;
                    if (ddlProducto.SelectedValue != "0")
                        lValorParametros[4] = ddlProducto.SelectedValue;
                    if (ddlComprador.SelectedValue != "0")
                        lValorParametros[5] = ddlComprador.SelectedValue;
                    if (ddlVendedor.SelectedValue != "0")
                        lValorParametros[6] = ddlVendedor.SelectedValue;
                    if (ddlEstado.SelectedValue != "0")
                        lValorParametros[9] = ddlEstado.SelectedValue;
                    //20161207 rq102 conformacion de rutas
                    if (TxtFechaFin.Text.Trim().Length > 0)
                        lValorParametros[10] = TxtFechaFin.Text.Trim();
                    else
                        lValorParametros[10] = lValorParametros[2];
                    //20161207 rq102 conformacion de rutas
                    if (TxtNoContratoFin.Text.Trim().Length > 0)
                        lValorParametros[11] = TxtNoContratoFin.Text.Trim();
                    else
                        lValorParametros[11] = lValorParametros[1];
                    lValorParametros[12] = ddlCausa.SelectedValue; //20171130 rq026-17
                                                                   //20171130 rq026-17
                    if (TxtNoId.Text.Trim().Length > 0)
                        lValorParametros[13] = TxtNoId.Text.Trim();
                    lConexion.AbrirInforme(); //20211110 conexción replica
                    dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetVerificaContratos", lsNombreParametros, lTipoparametros, lValorParametros); //20210915
                    dtgConsulta.DataBind();
                    lConexion.CerrarInforme(); //20211110 conexción replica
                    if (dtgConsulta.Items.Count > 0)
                    {
                        EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Excel };
                        ButtonsEvent?.Invoke(botones, null, NameController);
                        ldFecha = DateTime.Now.Date;
                        foreach (DataGridItem Grilla in dtgConsulta.Items)
                        {
                            if (Session["tipoPerfil"].ToString() == "B")
                            {
                                var lkbVerificacion = (LinkButton)Grilla.FindControl("lkbVerificacion");
                                lkbVerificacion.Visible = Grilla.Cells[14].Text == "En verificacion";
                            }
                            else
                            {
                                var lkbSeleccionar = (LinkButton)Grilla.FindControl("lkbSeleccionar");
                                lkbSeleccionar.Visible = Grilla.Cells[14].Text != "En verificacion";
                            }
                            if (Grilla.Cells[14].Text != "Contrato Registrado")  //20170814 rq036-17
                                Grilla.Cells[23].Text = "0";  //20170814 rq036-17
                        }
                    }
                    else
                    {
                        EnumBotones[] botones = { EnumBotones.Buscar };
                        ButtonsEvent?.Invoke(botones, null, NameController);
                        ToastrEvent?.Invoke("No se encontraron Registros.", EnumTypeToastr.Warning);
                    }
                    //Cambio por la selección de acciones
                    foreach (DataGridItem Grilla in dtgConsulta.Items)
                    {
                        var lkbSeleccionar = (LinkButton)Grilla.FindControl("lkbSeleccionar");
                        var lkbVerificacion = (LinkButton)Grilla.FindControl("lkbVerificacion");

                        lkbSeleccionar.Visible = true;
                        lkbVerificacion.Visible = Session["tipoPerfil"].ToString() == "B";
                    }

                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke($"No se Pudo Generar el Informe.! {ex}", EnumTypeToastr.Error);
                }
            }
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
        {
            var lsFecha = DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day;
            //string lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "/" + DateTime.Now.ToShortDateString().Substring(3, 2) + "/" + DateTime.Now.ToShortDateString().Substring(0, 2
            //string lsFecha = "2015/12/01";
            var liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
            var lsPuedeEntrar = "S";
            var lsFechaMaxCor = "";
            var lsFechaMaxModif = "";
            var lsIndVerif = "N"; // Campo nuevo algoritmo y verificacion 20150529
            var lsIndModif = "N"; // Campo nuevo algoritmo y verificacion 20150529
                                  // Campos nuevos control cambios modificacion Informacion transaccional 20150812
            var lsFechaIni = "";
            var lsFechaFin = "";
            var lsCodModa = "0";
            var lsCodPunto = "0";
            var lsCodRuta = "0";
            var lsCantidad = "0";
            var lsPrescio = "0";
            var lsCodTipoSubasta = "0";
            var lsConecSnt = "S"; //20171130 rq026-17
            var lsBocaPoz = "N"; //20171130 rq026-17
            var lsCentroPob = "0"; //20171130 rq026-17
            var lsPeriodo = "0"; //20171130 rq026-17
            var lsIndVar = "N"; //20171130 rq026-17
            var lsFecSus = ""; //20171130 rq026-17
            var lsContDef = ""; //20171130 rq026-17
            var lsFuente = ""; //20171130 rq026-17
            string lsError = "";//20220418
            string lsPuede = "N";//20220418
            if (e.CommandName.Equals("Seleccionar"))
            {
                try
                {
                    //20220418
                    if (e.Item.Cells[25].Text == "E" || e.Item.Cells[31].Text == "N" || e.Item.Cells[62].Text == "S")
                        lsPuede = "S";
                    if (lsPuede == "N")
                        lsError = "Ya modificó el contrato. Debe esperar que la contraparte apruebe";
                    if (lsError == "")//20220418
                    {
                        hdfCambioDatos.Value = e.Item.Cells[62].Text; //20220418
                        HdfValida = "S"; //20210224
                        TxtNumContrato.Enabled = true;
                        TxtFechaSus.Enabled = true;
                        ddlModalidad.Enabled = true;
                        ddlPuntoEntr.Enabled = true;
                        TxtCantidad.Enabled = true;
                        TxtPrecio.Enabled = true;
                        TxtFechaInicial.Enabled = true;
                        TxtHoraInicial.Enabled = true;
                        TxtFechaFinal.Enabled = true;
                        TxtHoraFinal.Enabled = true;
                        ddlSentidoFlujo.Enabled = true;
                        TxtPresion.Enabled = true;
                        ddlFuente.Enabled = true; //20220418 correccion contrato
                        ddlConecSnt.Enabled = true; //20171114 rq036-17
                        ddlEntBocPozo.Enabled = true; //20171114 rq036-17
                        ddlCentro.Enabled = true; //20171114 rq036-17
                        ddlPeriodo.Enabled = true; //20171130 rq026-17
                        ddlIndVar.Enabled = true; //20171130 rq026-17

                        //20220418 correccion contrato
                        TxtNumContrato.ForeColor = Color.Black;
                        TxtFechaSus.ForeColor = Color.Black;
                        ddlModalidad.ForeColor = Color.Black;
                        ddlPuntoEntr.ForeColor = Color.Black;
                        TxtCantidad.ForeColor = Color.Black;
                        TxtPrecio.ForeColor = Color.Black;
                        TxtFechaInicial.ForeColor = Color.Black;
                        TxtHoraInicial.ForeColor = Color.Black;
                        TxtFechaFinal.ForeColor = Color.Black;
                        TxtHoraFinal.ForeColor = Color.Black;
                        ddlSentidoFlujo.ForeColor = Color.Black;
                        TxtPresion.ForeColor = Color.Black;
                        ddlFuente.ForeColor = Color.Black;
                        ddlConecSnt.ForeColor = Color.Black;
                        ddlEntBocPozo.ForeColor = Color.Black;
                        ddlCentro.ForeColor = Color.Black;
                        ddlPeriodo.ForeColor = Color.Black;
                        ddlIndVar.ForeColor = Color.Black;
                        DdlClasifica.ForeColor = Color.Black;
                        ddlTrm.ForeColor = Color.Black;
                        ddlMoneda.ForeColor = Color.Black;
                        //20220418 fin coreccion contrato

                        /////////////////////////////////////////////////////////////////////////
                        /// Inhabilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///
                        /////////////////////////////////////////////////////////////////////////
                        TdIndica.Visible = false;
                        //TdIndica1.Visible = false;
                        /////////////////////////////////////////////////////////////////////////
                        hdfCodVerifCont.Value = "0";
                        lblTotlCantidad.Text = "0";
                        lblOperacion.Text = e.Item.Cells[0].Text;
                        lblSubasta.Text = e.Item.Cells[2].Text + " - Mercado: " + e.Item.Cells[3].Text;
                        lblProducto.Text = e.Item.Cells[4].Text;
                        hdfIdVerif.Value = e.Item.Cells[15].Text;  //20170814 rq036-17
                        hdfCodTipoSub.Value = e.Item.Cells[18].Text; //20170814 rq036-17
                        HdfDestinoRueda = e.Item.Cells[24].Text;  //20170814 rq036-17
                        hdfEstadoAct.Value = e.Item.Cells[25].Text; //20170814 rq036-17
                        HdfTipoMerc = e.Item.Cells[26].Text; //20170814 rq036-17
                        TxtHoraInicial.Text = "00:00";
                        TxtHoraFinal.Text = "00:00";
                        lsIndVerif = e.Item.Cells[31].Text.Trim().Replace("&nbsp;", "N"); // Campo nuevo algoritmo y verificacion 20150529 //20160603 fuente o campo  //20170814 rq036-17
                        hdfOPeraC.Value = e.Item.Cells[16].Text; // Campo nuevo algoritmo y verificacion 20150529 //20170814 rq036-17
                        hdfOPeraV.Value = e.Item.Cells[17].Text; // Campo nuevo algoritmo y verificacion 20150529 //20170814 rq036-17
                        lsFechaMaxCor = e.Item.Cells[33].Text; // Campo nuevo algoritmo y verificacion 20150529 //20160603 fuente o campo //20170814 rq036-17
                        lsIndModif = e.Item.Cells[32].Text; // Campo nuevo Modificacion Contratos 20150617 //20160603 fuente o campo //20170814 rq036-17
                        lsFechaMaxModif = e.Item.Cells[34].Text; // Campo nuevo Modificacion Contratos 20150617 //20160603 fuente o campo //20170814 rq036-17

                        // Campos nuevos requerimiento modificacoin inf treansaccoinal 20150812
                        lsFechaIni = e.Item.Cells[36].Text; //20160603 fuente o campo  //20170814 rq036-17
                        lsFechaFin = e.Item.Cells[37].Text; //20160603 fuente o campo  //20170814 rq036-17
                        lsCodModa = e.Item.Cells[38].Text; //20160603 fuente o campo  //20170814 rq036-17
                        lsCodPunto = e.Item.Cells[39].Text; //20160603 fuente o campo  //20170814 rq036-17
                        lsCodRuta = e.Item.Cells[42].Text; //20160603 fuente o campo  //20170814 rq036-17
                        lsCantidad = e.Item.Cells[40].Text; //20160603 fuente o campo  //20170814 rq036-17
                        hdfCantCnt.Value = e.Item.Cells[40].Text; //20160603 fuente o campo  //20170814 rq036-17
                        lsPrescio = e.Item.Cells[41].Text; //20160603 fuente o campo  //20170814 rq036-17
                        lsCodTipoSubasta = e.Item.Cells[18].Text; //20170814 rq036-17

                        lsConecSnt = e.Item.Cells[44].Text; //20171130 rq026-17
                        lsBocaPoz = e.Item.Cells[45].Text; //20171130 rq026-17
                        lsCentroPob = e.Item.Cells[46].Text; //20171130 rq026-17
                        lsPeriodo = e.Item.Cells[47].Text; //20171130 rq026-17
                        lsIndVar = e.Item.Cells[48].Text; //20171130 rq026-17
                                                          //20171130 rq026-17
                        if (e.Item.Cells[49].Text == "&nbsp;")
                            lsFecSus = "";
                        else
                            lsFecSus = e.Item.Cells[49].Text;
                        //20171130 rq026-17
                        if (e.Item.Cells[50].Text == "&nbsp;")
                            lsContDef = "";
                        else
                            lsContDef = e.Item.Cells[50].Text;
                        lsFuente = e.Item.Cells[27].Text; //20171130 rq026-17

                        //20201124
                        try
                        {
                            ddlSentidoFlujo.SelectedValue = e.Item.Cells[52].Text;
                        }
                        catch (Exception ex)
                        {
                        }
                        //20201124
                        try
                        {
                            TxtPresion.Text = e.Item.Cells[53].Text;
                        }
                        catch (Exception ex)
                        {
                        }
                        //20201124
                        try
                        {
                            DdlClasifica.SelectedValue = e.Item.Cells[54].Text;
                        }
                        catch (Exception ex)
                        {
                        }
                        //20210305 broker
                        TxtIdTraMp.Text = e.Item.Cells[56].Text;
                        if (goInfo.cod_comisionista == e.Item.Cells[16].Text) //20170814 rq036-17
                        {
                            hdfPunta.Value = "C";
                            lblPunta.Text = "COMPRADOR";
                            lnlNomOpera.Text = "Nombre del Comprador";
                            lblTipoDocOpera.Text = "Tipo Documento Comprador";
                            lblNoDocOPera.Text = "No. Documento Comprador";
                            lblNomOperador.Text = e.Item.Cells[5].Text;
                            lblTipoDocOperador.Text = e.Item.Cells[19].Text;  //20170814 rq036-17
                            lblNoDocumentoOperaqdor.Text = e.Item.Cells[21].Text;  //20170814 rq036-17

                            divDemanda.Visible = true;

                            /////////////////////////////////////////////////////////////////////////
                            /// Habilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///
                            /////////////////////////////////////////////////////////////////////////
                            if (HdfDestinoRueda == "G" && HdfTipoMerc == "P")
                            {
                                TdIndica.Visible = true;
                                //TdIndica1.Visible = true;
                            }
                            /////////////////////////////////////////////////////////////////////////
                        }
                        else
                        {
                            hdfPunta.Value = "V";
                            lblPunta.Text = "VENDEDOR";
                            lblTipoDocOpera.Text = "Tipo Documento Vendedor";
                            lnlNomOpera.Text = "Nombre del Vendedor";
                            lblNoDocOPera.Text = "No. Documento Vendedor";
                            lblNomOperador.Text = e.Item.Cells[6].Text;
                            lblTipoDocOperador.Text = e.Item.Cells[20].Text;  //20170814 rq036-17
                            lblNoDocumentoOperaqdor.Text = e.Item.Cells[22].Text;  //20170814 rq036-17
                        }
                        //if (hdfEstadoAct.Value == "R" || hdfEstadoAct.Value == "D" || hdfEstadoAct.Value == "B") //20220831
                        if (hdfEstadoAct.Value == "R" || hdfEstadoAct.Value == "B")//20220831
                        {
                            lsPuedeEntrar = "N";
                            divDemanda.Visible = false;
                            ToastrEvent?.Invoke("No se puede ingresar o actualizar la información, ya que el contrato está en un estado NO válido para modificación!.", EnumTypeToastr.Warning);
                        }
                        else
                        {
                            if (lsIndVerif == "N") // Campo nuevo algoritmo y verificacion 20150529
                            {
                                if (lsIndModif == "S")
                                {
                                    if (DateTime.Now > Convert.ToDateTime(lsFechaMaxModif))
                                    {
                                        lsPuedeEntrar = "N";
                                        ToastrEvent?.Invoke($"No se puede Ingresar a Modificar la Información YA que se venció el plazo para hacerlo. {lsFechaMaxModif}!", EnumTypeToastr.Warning); //20161207 rq102 conformacion de rutas
                                        divDemanda.Visible = false;
                                    }
                                }
                                else
                                {
                                    //if (Convert.ToDateTime(lsFecha) > Convert.ToDateTime(e.Item.Cells[13].Text))  //20170814 rq036-17 //20220616
                                    if (DateTime.Now > Convert.ToDateTime(e.Item.Cells[13].Text))  //20170814 rq036-17 //20220616
                                    {
                                        lsPuedeEntrar = "N";
                                        ToastrEvent?.Invoke($"No se puede Ingresar o Actualizar la Información YA que se venció el plazo para hacerlo. {e.Item.Cells[13].Text}!", EnumTypeToastr.Warning); //20161207 rq102 conformacion de rutas //20170814 rq036-17
                                        divDemanda.Visible = false;
                                    }
                                    //if (lsFecha == e.Item.Cells[13].Text)  //20170814 rq036-17
                                    //{
                                    //    lConexion.Abrir();
                                    //    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_verificacion", " codigo_tipo_subasta = " + hdfCodTipoSub.Value + " And tipo_mercado = '" + e.Item.Cells[25].Text.Trim() + "' And '" + e.Item.Cells[28].Text + "' >= hora_inicial_negociacion And '" + e.Item.Cells[28].Text + "' <= hora_final_negociacion ");  //20170814 rq036-17
                                    //    if (lLector.HasRows)
                                    //    {
                                    //        lLector.Read();
                                    //        lsHoraFin = lLector["hora_maxima_registro"].ToString();
                                    //        liValFin = ((Convert.ToDateTime(lsHoraFin).Hour * 60) + Convert.ToDateTime(lsHoraFin).Minute);
                                    //    }
                                    //    lLector.Close();
                                    //    lLector.Dispose();
                                    //    lConexion.Cerrar();

                                    //    if (liHoraAct > liValFin)
                                    //    {
                                    //        lsPuedeEntrar = "N";
                                    //        tblDemanda.Visible = false;
                                    //        lblMensaje.Text = "No se puede Ingresar o Actualizar la Información YA que se vencio el plazo para hacerlo. {" + e.Item.Cells[12].Text + "-" + lsHoraFin + "}!"; //20161207 rq102 conformacion de rutas //20170814 rq036-17
                                    //    }
                                    //}
                                }
                            }
                            else
                            {
                                if (DateTime.Now <= Convert.ToDateTime(lsFechaMaxCor))
                                {
                                    lsPuedeEntrar = "S";
                                }
                                else
                                {
                                    lsPuedeEntrar = "N";
                                    ToastrEvent?.Invoke($"No se puede Ingresar o Corregir la Información YA que se venció el plazo para hacerlo. {lsFechaMaxCor}!", EnumTypeToastr.Warning); //20161207 rq102 conformacion de rutas
                                }
                            }
                        }
                        if (lsPuedeEntrar == "S")
                        {
                            if (!manejo_bloqueo("V", hdfIdVerif.Value))
                            {
                                /// Validaciones Cuando el Que entra en la pantalla es negociador
                                //if (hdfEstadoAct.Value != "R" && hdfEstadoAct.Value != "S")
                                //{
                                if (HdfDestinoRueda == "G")
                                {
                                    lblPuntoE.Text = "Punto de entrega de la energía al comprador";
                                    lblCantidad.Text = "Cantidad de energía contratada (MBTUD)";
                                    lblPrecio.Text = "Precio a la fecha de suscripción del contrato (USD/MBTU)";
                                    lblFechaInc.Text = "Fecha Hora de inicio de la obligación de entrega";
                                    lblFechaFin.Text = "Fecha Hora de terminación de la obligación de entrega";
                                    TrTrans.Visible = false;
                                }
                                else
                                {

                                    lblPuntoE.Text = "Tramo o grupos de gasoductos";
                                    lblCantidad.Text = "Capacidad Contratada  (KPCD)";
                                    lblPrecio.Text = "Tarifa a la fecha de suscripción del contrato (Moneda vigente/KPC)"; //20220828
                                    lblFechaInc.Text = "Fecha Hora de inicio de la prestación del servicio";
                                    lblFechaFin.Text = "Fecha Hora de terminación prestación del servicio";
                                    if (HdfTipoMerc == "P")
                                        TrTrans.Visible = true;
                                    else
                                        TrTrans.Visible = false;

                                }
                                ddlModalidad.Items.Clear();
                                ddlPuntoEntr.Items.Clear();
                                dlTipoDemanda.Items.Clear();
                                ddlMotivoModifi.Items.Clear();
                                //ddlCesionario.Items.Clear(); //20210224

                                lConexion.Abrir();

                                if (hdfPunta.Value == "C")
                                {
                                    string[] lsNombreParametrosC = { "@P_cadena" };
                                    SqlDbType[] lTipoparametrosC = { SqlDbType.VarChar };
                                    string[] lValorParametrosC = { " Delete from t_contrato_datos_usuarios where codigo_cont_datos = 0 And login_usuario = '" + goInfo.Usuario + "' " };
                                    /// Se obtiene los Datos de la Punta Compradora
                                    DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_EjecutarCadena", lsNombreParametrosC, lTipoparametrosC, lValorParametrosC);
                                }
                                /// LLeno los Controles
                                LlenarControles(lConexion.gObjConexion, ddlModalidad, "m_modalidad_contractual", " estado = 'A'  order by descripcion", 0, 1);
                                if (HdfDestinoRueda == "G")
                                    LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_pozo", " estado = 'A'  order by descripcion", 0, 1);
                                else
                                {
                                    if (hdfCodTipoSub.Value == "3")  //Subasta UVLP 20161005
                                        LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_ruta_snt", " estado in ('A','E') order by descripcion", 0, 4);
                                    else
                                        LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_ruta_snt", " estado = 'A' order by descripcion", 0, 4);
                                }
                                LlenarControles(lConexion.gObjConexion, dlTipoDemanda, "m_tipo_demanda_atender", " estado = 'A' order by descripcion", 0, 1);
                                LlenarControles(lConexion.gObjConexion, ddlMotivoModifi, "m_motivos_modificacion", " estado = 'A' order by descripcion", 0, 1);
                                //LlenarControles(lConexion.gObjConexion, ddlCesionario, "m_operador", " estado = 'A' And codigo_operador !=  " + goInfo.cod_comisionista + " order by razon_social", 0, 4); //20210224
                                //20190607 rq036-19
                                if (lsCodTipoSubasta == "5")
                                {
                                    ddlPeriodo.Items.Clear();
                                    //lConexion.Abrir();
                                    LlenarControles(lConexion.gObjConexion, ddlPeriodo, "m_periodos_entrega per, m_caracteristica_sub scar", " per.estado = 'A'  and scar.destino_rueda = '" + HdfDestinoRueda + "'  and scar.tipo_mercado= '" + HdfTipoMerc + "' and scar.codigo_tipo_subasta = 5 and scar.estado ='A' and scar.tipo_caracteristica ='E' and scar.codigo_caracteristica = per.codigo_periodo  order by per.descripcion", 0, 1);
                                    //lConexion.Cerrar();
                                }
                                else
                                {
                                    ddlPeriodo.Items.Clear();
                                    LlenarControles(lConexion.gObjConexion, ddlPeriodo, "m_periodos_entrega per", " per.estado = 'A'  order by per.descripcion", 0, 1);
                                }
                                //20190607  fin rq036-19
                                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_contrato_datos_verif", " codigo_verif_contrato = " + hdfIdVerif.Value + " And punta_contrato = '" + hdfPunta.Value + "' ");
                                if (lLector.HasRows)
                                {
                                    lLector.Read();
                                    /// LLamo metodo que bloquea los campos de la pantalla cuando las partes entran a corregir la informacion 
                                    /// Requerimiento ALgoritmo y verificacion 20150529
                                    if (lsIndVerif == "S")
                                        inactivarCampos();
                                    /////////////////////
                                    // Inactiva campos que vienen del contrato 20150812
                                    /////////////////////
                                    if (lsCodTipoSubasta != "5")
                                    {
                                        ddlModalidad.Enabled = false;
                                        ddlPuntoEntr.Enabled = false;
                                        //TxtCantidad.Enabled = false;
                                        TxtPrecio.Enabled = false;
                                        TxtFechaInicial.Enabled = false;
                                        TxtFechaFinal.Enabled = false;
                                        ddlPeriodo.Enabled = false; //20171130 rq026
                                        TxtHoraInicial.Visible = false;//20171130 rq026
                                        TxtHoraFinal.Visible = false;//20171130 rq026
                                    }
                                    //Cambio de restrcciio de campos 20160125
                                    if (lsCodTipoSubasta == "5")
                                    {
                                        TxtFechaInicial.Enabled = false;
                                        TxtFechaFinal.Enabled = false;
                                    }
                                    hdfAccion.Value = "M";
                                    TrModifi.Visible = true;
                                    if (Session["tipoPerfil"].ToString() == "N")
                                    {
                                        mdlRegistroContratoLabel.InnerHtml = "Modificación";
                                        btnCrear.Visible = false;
                                        btnActualizar.Visible = true; //20220418 correccion contato
                                                                      //20220418 correccion contato
                                        if (hdfEstadoAct.Value == "E" || e.Item.Cells[31].Text == "N" || e.Item.Cells[63].Text == "N")
                                        {
                                            btnRechazar.Visible = false;
                                            if (hdfEstadoAct.Value != "E" && e.Item.Cells[31].Text == "S")
                                                btnActualizar.Text = "Aprobar";
                                            else
                                                btnActualizar.Text = "Actualizar";
                                        }
                                        else
                                        {
                                            btnRechazar.Visible = true;
                                            btnActualizar.Text = "Aprobar";
                                        }
                                    }
                                    else
                                    {
                                        mdlRegistroContratoLabel.InnerHtml = "Registro";
                                        btnCrear.Visible = false;
                                        btnActualizar.Visible = false;
                                    }
                                    /// Recuperacion Datos
                                    DateTimeFormatInfo fmt = (new CultureInfo("hr-HR")).DateTimeFormat;
                                    DateTime ldFecha;
                                    var lsFechaIniTmp = "";
                                    var lsFechaFinTmp = "";
                                    hdfCodVerifCont.Value = lLector["codigo_cont_datos"].ToString();
                                    TxtNumContrato.Text = lLector["contrato_definitivo"].ToString();
                                    //TxtFechaSus.Text = lLector["fecha_suscripcion_cont"].ToString().Substring(6, 4) + "/" + lLector["fecha_suscripcion_cont"].ToString().Substring(3, 2) + "/" + lLector["fecha_suscripcion_cont"].ToString().Substring(0, 2); //20220418
                                    TxtFechaSus.Text = Convert.ToDateTime(lLector["fecha_suscripcion_cont"].ToString()).ToString("yyyy/MM/dd"); //20220418
                                    try
                                    {
                                        ddlModalidad.SelectedValue = lLector["codigo_modalidad"].ToString();
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    //20190718 ajuste
                                    try
                                    {
                                        ddlPuntoEntr.SelectedValue = lLector["codigo_punto_entrega"].ToString();
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    // campo o fuente MP 20160601
                                    try
                                    {
                                        ddlFuente.SelectedValue = lLector["codigo_fuente"].ToString();
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    // 2020925 c1 bimestral
                                    //try
                                    //{
                                    //    ddlMinuta.SelectedValue = lLector["maneja_minuta"].ToString();
                                    //}
                                    //catch (Exception)
                                    //{
                                    //}
                                    TxtCantidad.Text = lLector["cantidad"].ToString();
                                    TxtPrecio.Text = lLector["precio"].ToString();
                                    ldFecha = Convert.ToDateTime(lLector["fecha_inicial"].ToString());
                                    lsFechaIniTmp = ldFecha.ToString("t", fmt);
                                    ldFecha = Convert.ToDateTime(lLector["fecha_final"].ToString());
                                    lsFechaFinTmp = ldFecha.ToString("t", fmt);

                                    //TxtFechaInicial.Text = lLector["fecha_inicial"].ToString().Substring(6, 4) + "/" + lLector["fecha_inicial"].ToString().Substring(3, 2) + "/" + lLector["fecha_inicial"].ToString().Substring(0, 2); //20220418
                                    TxtFechaInicial.Text = Convert.ToDateTime(lLector["fecha_inicial"].ToString()).ToString("yyyy/MM/dd"); //20220418
                                                                                                                                           //TxtFechaFinal.Text = lLector["fecha_final"].ToString().Substring(6, 4) + "/" + lLector["fecha_final"].ToString().Substring(3, 2) + "/" + lLector["fecha_final"].ToString().Substring(0, 2); //20220418
                                    TxtFechaFinal.Text = Convert.ToDateTime(lLector["fecha_final"].ToString()).ToString("yyyy/MM/dd"); //20220418
                                    TxtHoraInicial.Text = lsFechaIniTmp;
                                    TxtHoraFinal.Text = lsFechaFinTmp;
                                    hdfRutaArchivo.Value = lLector["ruta_contrato"].ToString();
                                    if (hdfPunta.Value == "C")
                                    {
                                        //try
                                        //{
                                        //    dlTipoDemanda.SelectedValue = lLector["codigo_tipo_demanda"].ToString();
                                        //}
                                        //catch (Exception ex)
                                        //{

                                        //}
                                        //hdfTipoDemanda.Value = dlTipoDemanda.SelectedValue;
                                        divDemanda.Visible = true;
                                        btnActualUsu.Visible = false;
                                        btnCrearUsu.Visible = true;
                                        CargarDatosUsu();
                                        //if (dlTipoDemanda.SelectedValue == "1")
                                        //    TxtUsuarioFinal.Enabled = false;
                                        //if (hdfTipoDemanda.Value == "1")
                                        //{
                                        //    TxtUsuarioFinal.Enabled = false;
                                        //    lblMercadoRelev.Text = "Mercado Relevante Usuario Regulado";
                                        //    if (HdfDestinoRueda == "G")
                                        //        lblCantContra.Text = "Cantidad a entregar";
                                        //    else
                                        //        lblCantContra.Text = "Capacidad Contratada";
                                        //}
                                        //else
                                        //{
                                        //    if (HdfDestinoRueda == "G")
                                        //    {
                                        //        lblCantContra.Text = "Cantidad Contratada por usuario (MBTUD)";
                                        //        lblMercadoRelev.Text = "Mercado Relevante Usuario No Regulado";
                                        //    }
                                        //    else
                                        //    {
                                        //        lblMercadoRelev.Text = "Mercado Relevante Transporte Usuario No Regulado";
                                        //        lblCantContra.Text = "Capacidad Contratada por usuario (KPCD)";
                                        //    }
                                        //}

                                    }
                                    //20201124
                                    //if (HdfDestinoRueda == "T" && HdfTipoMerc == "P")
                                    //{
                                    //    try
                                    //    {
                                    //        ddlSentidoFlujo.SelectedValue = lLector["sentido_flujo"].ToString().Replace("&nbsp;", "");
                                    //    }
                                    //    catch (Exception)
                                    //    {

                                    //    }
                                    //    TxtPresion.Text = lLector["presion_punto_fin"].ToString();
                                    //}
                                }
                                else
                                {
                                    lLector.Close();
                                    lLector.Dispose();
                                    /// Lleno los campos que vienen del contrato y deshabilito los campos 20150812
                                    // Se bloquean los campos para las subastas UVCP, SSCI y BIMESTRAL
                                    if (lsCodTipoSubasta != "5")
                                    {
                                        try
                                        {
                                            ddlModalidad.SelectedValue = lsCodModa;
                                            if (HdfDestinoRueda == "G")
                                                ddlPuntoEntr.SelectedValue = lsCodPunto;
                                            else
                                                ddlPuntoEntr.SelectedValue = lsCodRuta;
                                            TxtCantidad.Text = lsCantidad;
                                            TxtPrecio.Text = lsPrescio;
                                            TxtFechaInicial.Text = lsFechaIni;
                                            if (lsCodTipoSubasta == "2")
                                                TxtFechaFinal.Text = lsFechaIni;
                                            else
                                                TxtFechaFinal.Text = lsFechaFin;
                                            ddlModalidad.Enabled = false;
                                            ddlPuntoEntr.Enabled = false;
                                            //TxtCantidad.Enabled = false;
                                            TxtPrecio.Enabled = false;
                                            TxtFechaInicial.Enabled = false;
                                            TxtFechaFinal.Enabled = false;
                                        }
                                        catch (Exception)
                                        {

                                        }
                                    }
                                    if (lsCodTipoSubasta == "5") //Crea el else para traer las fechas 20151228                                                            
                                    {
                                        ddlModalidad.SelectedValue = lsCodModa;
                                        if (HdfDestinoRueda == "G")
                                            ddlPuntoEntr.SelectedValue = lsCodPunto;
                                        else
                                            ddlPuntoEntr.SelectedValue = lsCodRuta;
                                        TxtCantidad.Text = lsCantidad;
                                        TxtPrecio.Text = lsPrescio;
                                        TxtFechaInicial.Text = lsFechaIni;
                                        TxtFechaFinal.Text = lsFechaFin;
                                        TxtFechaInicial.Enabled = false;
                                        TxtFechaFinal.Enabled = false;
                                    }
                                    ddlConecSnt.SelectedValue = lsConecSnt; //20171130 rq026-17
                                    ddlEntBocPozo.SelectedValue = lsBocaPoz; //20171130 rq026-17
                                                                             //20171130 rq026-17
                                    try
                                    {
                                        ddlCentro.SelectedValue = lsCentroPob;
                                    }
                                    catch (Exception)
                                    { }
                                    //20190607 rq036-19
                                    try
                                    {
                                        ddlPeriodo.SelectedValue = lsPeriodo; //20171130 rq026-17
                                    }
                                    catch (Exception)
                                    { }
                                    ddlIndVar.SelectedValue = lsIndVar; //20171130 rq026-17
                                    TxtFechaSus.Text = lsFecSus; //20171130 rq026-17
                                    TxtNumContrato.Text = lsContDef; //20171130 rq026-17
                                    ddlFuente.SelectedValue = lsFuente; //20171130 rq026-17
                                    ddlPeriodo_SelectedIndexChanged(null, null);

                                    if (hdfPunta.Value == "C")
                                    {
                                        divDemanda.Visible = true;
                                        btnActualUsu.Visible = false;
                                        btnCrearUsu.Visible = true;
                                        CargarDatosUsu();
                                        //if (dlTipoDemanda.SelectedValue == "1")
                                        //    TxtUsuarioFinal.Enabled = false;
                                        //if (hdfTipoDemanda.Value == "1")
                                        //{
                                        //    TxtUsuarioFinal.Enabled = false;
                                        //    lblMercadoRelev.Text = "Mercado Relevante Usuario Regulado";
                                        //    if (HdfDestinoRueda == "G")
                                        //        lblCantContra.Text = "Cantidad a entregar";
                                        //    else
                                        //        lblCantContra.Text = "Capacidad Contratada";
                                        //}
                                        //else
                                        //{
                                        //    if (HdfDestinoRueda == "G")
                                        //    {
                                        //        lblCantContra.Text = "Cantidad Contratada por usuario (MBTUD)";
                                        //        lblMercadoRelev.Text = "Mercado Relevante Usuario No Regulado";
                                        //    }
                                        //    else
                                        //    {
                                        //        lblMercadoRelev.Text = "Mercado Relevante Transporte Usuario No Regulado";
                                        //        lblCantContra.Text = "Capacidad Contratada por usuario (KPCD)";
                                        //    }
                                        //}
                                    }
                                    TrModifi.Visible = false;
                                    hdfAccion.Value = "C";
                                    if (Session["tipoPerfil"].ToString() == "N")
                                    {
                                        btnCrear.Visible = true;
                                        btnActualizar.Visible = false;
                                    }
                                    else
                                    {
                                        btnCrear.Visible = false;
                                        btnActualizar.Visible = false;
                                    }
                                }
                                lLector.Close();
                                lLector.Dispose();
                                lConexion.Cerrar();
                                //dlTipoDemanda_SelectedIndexChanged(null, null);

                                //Abre el modal de Modificación
                                ModalEvent?.Invoke(registroContratoMod.ID, registroContratoModInside.ID, EnumTypeModal.Abrir);
                                //}
                                //else
                                //    lblMensaje.Text = "El Registro No Puede ser Modificado, Ya que esta en Un estado NO Válido para Modificacion.!";
                            }
                            else
                                ToastrEvent?.Invoke($"No se puede Modificar el Registro YA que se encuentra Bloqueado por el Administrador.!", EnumTypeToastr.Warning);
                        }
                        //20170814 rq036-17
                        if (HdfDestinoRueda == "G" && HdfTipoMerc == "S" && e.Item.Cells[43].Text == "S") //20170814 rq036-17
                        {
                            trSum.Visible = true;
                            ddlConecSnt.SelectedValue = e.Item.Cells[44].Text; //20170814 rq036-17
                            ddlEntBocPozo.SelectedValue = e.Item.Cells[45].Text; //20170814 rq036-17
                            ddlConecSnt_SelectedIndexChanged(null, null);
                            //lblCentro.Visible = false;
                            //ddlCentro.Visible = false;
                            ddlCentro.SelectedValue = e.Item.Cells[46].Text; //20170814 rq036-17
                        }
                        else
                        {
                            trSum.Visible = false;
                            ddlConecSnt.SelectedValue = "N";
                            ddlEntBocPozo.SelectedValue = "N";
                            ddlCentro.SelectedValue = "0";
                            lblPuntoE.Visible = true;
                            ddlPuntoEntr.Visible = true;
                        }
                        //20190607 rq036-19
                        try
                        {
                            ddlPeriodo.SelectedValue = e.Item.Cells[47].Text; //20171130 rq026-17
                        }
                        catch (Exception)
                        { }
                        ddlIndVar.SelectedValue = e.Item.Cells[48].Text; //20171130 rq026-17
                        ddlPeriodo_SelectedIndexChanged(null, null);//20171130 rq026-17
                                                                    //20210707 trm moneda
                        try
                        {
                            ddlTrm.SelectedValue = e.Item.Cells[57].Text;
                        }
                        catch (Exception ex)
                        {
                        }
                        if (e.Item.Cells[59].Text != "&nbsp;")
                            TxtObsTrm.Text = e.Item.Cells[59].Text;
                        else
                            TxtObsTrm.Text = "";
                        try
                        {
                            ddlMoneda.SelectedValue = e.Item.Cells[60].Text;
                        }
                        catch (Exception ex)
                        {
                        }
                        //20210707 fin trm moneda
                    }
                    else //20220418
                        ToastrEvent?.Invoke(lsError, EnumTypeToastr.Error);
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke($"Problemas en la Recuperación de la Información. {ex.Message}", EnumTypeToastr.Error); //20180126 rq107-16
                }
                //Fuente o campo MP 20160601
                if (HdfDestinoRueda == "G" && HdfTipoMerc == "P")
                {
                    TrFuente.Visible = true;
                }
                else
                {
                    TrFuente.Visible = false;
                    ddlFuente.SelectedValue = "0";
                }
                trMsTra.Visible = false;//20210305 broker
                //20171130 rq026-17
                if (hdfCodTipoSub.Value != "5")
                {
                    ddlPeriodo.Enabled = false;
                    TxtHoraInicial.Enabled = false;
                    TxtHoraFinal.Enabled = false;

                }
                else//20210305 broker
                {
                    if (HdfDestinoRueda == "T" && HdfTipoMerc == "S" && hdfPunta.Value == "V")
                    {
                        trMsTra.Visible = true;
                    }
                }
                //20210305 broker
                if (!trMsTra.Visible)
                {
                    TxtIdTraMp.Text = "";
                }

                //2020925 subasta bimestral c1
                //if (hdfCodTipoSub.Value == "10")
                //    TrMinuta.Visible = true;
                //else
                //{
                //    TrMinuta.Visible = false;
                //    ddlMinuta.SelectedValue = "N";
                //}
                //20210707 trm moneda
                if (hdfPunta.Value == "C")
                    TxtObsTrm.Enabled = false;
                else
                    TxtObsTrm.Enabled = true;
                //20220418 correccion contrato
                if (hdfCambioDatos.Value == "S")
                {
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_contrato_datos_verif", " codigo_verif_contrato = " + hdfIdVerif.Value + " And punta_contrato <> '" + hdfPunta.Value + "' ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        ddlModalidad.SelectedValue = lLector["codigo_modalidad"].ToString();
                        TxtCantidad.Text = lLector["cantidad"].ToString();
                        TxtPrecio.Text = lLector["precio"].ToString();
                        if (HdfDestinoRueda == "G" && HdfTipoMerc == "S" && e.Item.Cells[43].Text == "S")
                        {
                            ddlConecSnt.SelectedValue = lLector["conectado_snt"].ToString();
                            ddlConecSnt_SelectedIndexChanged(null, null);
                            ddlEntBocPozo.SelectedValue = lLector["ent_boca_pozo"].ToString();
                            ddlEntBocPozo_SelectedIndexChanged(null, null);
                            ddlCentro.SelectedValue = lLector["codigo_centro_pob"].ToString();
                        }
                        else
                        {
                            ddlConecSnt.SelectedValue = "S";
                            ddlEntBocPozo.SelectedValue = "N";
                            ddlCentro.SelectedValue = "0";
                        }
                        ddlPuntoEntr.SelectedValue = lLector["codigo_punto_entrega"].ToString();
                        ddlPeriodo.SelectedValue = lLector["codigo_periodo"].ToString();
                        TxtFechaIni.Text = Convert.ToDateTime(lLector["fecha_inicial"].ToString()).ToString("yyyy/MM/dd");
                        TxtFechaFin.Text = Convert.ToDateTime(lLector["fecha_final"].ToString()).ToString("yyyy/MM/dd");
                        ddlIndVar.SelectedValue = lLector["ind_contrato_var"].ToString();
                        TxtFechaSus.Text = Convert.ToDateTime(lLector["fecha_suscripcion_cont"].ToString()).ToString("yyyy/MM/dd");
                        TxtNumContrato.Text = lLector["contrato_definitivo"].ToString();
                        ddlSentidoFlujo.SelectedValue = lLector["sentido_flujo"].ToString();
                        TxtPresion.Text = lLector["presion_punto_fin"].ToString();
                        ddlMoneda.SelectedValue = lLector["tipo_moneda"].ToString();
                        ddlTrm.SelectedValue = lLector["codigo_trm"].ToString();
                    }
                    lLector.Close();
                    lConexion.Cerrar();
                }
            }
            /// Realiza la visualizacion de la Pantalla de Verificacion
            if (e.CommandName.Equals("Verificar"))
            {
                string[] lsNombreParametros = { "@P_codigo_cont_datos", "@P_codigo_verif_contrato", "@P_punta_contrato" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
                Object[] lValorParametros = { "0", "0", "" };
                var lsVerifica = "1"; // 1=Correcta 0=Incorrecta

                try
                {
                    lblOperacion.Text = e.Item.Cells[0].Text;
                    lblSubasta.Text = e.Item.Cells[2].Text + " - Mercado: " + e.Item.Cells[3].Text;
                    lblProducto.Text = e.Item.Cells[4].Text;
                    hdfIdVerif.Value = e.Item.Cells[15].Text;  //20170814 rq036-17
                    hdfCodTipoSub.Value = e.Item.Cells[18].Text;  //20170814 rq036-17
                    HdfDestinoRueda = e.Item.Cells[24].Text;  //20170814 rq036-17
                    hdfEstadoAct.Value = e.Item.Cells[25].Text;  //20170814 rq036-17
                    HdfTipoMerc = e.Item.Cells[26].Text;  //20170814 rq036-17
                    hdfOPeraC.Value = e.Item.Cells[16].Text;  //20170814 rq036-17
                    hdfOPeraV.Value = e.Item.Cells[17].Text;  //20170814 rq036-17

                    //// Obtengo la Informacion del Comprador
                    lConexion.Abrir();
                    lValorParametros[1] = hdfIdVerif.Value;
                    lValorParametros[2] = "C";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetRegistroContratosDatos", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error == "")
                    {
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lblDat1C.Text = lLector["contrato_definitivo"].ToString().Trim().ToUpper();
                            lblDat2C.Text = lLector["fecha_suscripcion_cont"].ToString().Trim();
                            lblDat3C.Text = lLector["codigo_modalidad"] + " - " + lLector["desc_modalidad"].ToString().Trim();
                            lblDat4C.Text = lLector["codigo_punto_entrega"] + " - " + lLector["desc_punto_ent"].ToString().Trim();
                            lblDat5C.Text = lLector["cantidad"].ToString().Trim();
                            lblDat6C.Text = lLector["precio"].ToString().Trim();
                            lblDat7C.Text = lLector["fecha_inicial"].ToString().Trim();
                            lblDat8C.Text = lLector["fecha_final"].ToString().Trim();
                            lblDat9C.Text = lLector["sentido_flujo"].ToString().Trim();
                            lblDat10C.Text = lLector["presion_punto_fin"].ToString().Trim();
                            if (lLector["ruta_contrato"].ToString().Trim().Length > 0)
                                LinkC.HRef = "../archivos/" + lLector["ruta_contrato"].ToString().Trim();
                            else
                                LinkC.Visible = false;
                            lblDat4Op.Text = lLector["entrega_cont"].ToString();
                            lblDat3Op.Text = lLector["modalidad_cont"].ToString();
                            lblDat5Op.Text = lLector["cantidad_cont"].ToString().Trim();
                            lblDat6Op.Text = lLector["precio_cont"].ToString().Trim();
                            lblDat7Op.Text = lLector["fecha_inicial_cont"].ToString().Trim();
                            lblDat8Op.Text = lLector["fecha_final_cont"].ToString().Trim();
                        }
                    }
                    else
                    {
                        ToastrEvent?.Invoke($"Problemas al Obtener los Datos de la Compra.! {goInfo.mensaje_error}", EnumTypeToastr.Error);
                    }
                    lLector.Close();
                    lLector.Dispose();
                    //// Obtengo los Datos de la Venta
                    lValorParametros[2] = "V";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetRegistroContratosDatos", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error == "")
                    {
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lblDat1V.Text = lLector["contrato_definitivo"].ToString().Trim().ToUpper();
                            lblDat2V.Text = lLector["fecha_suscripcion_cont"].ToString().Trim();
                            lblDat3V.Text = lLector["codigo_modalidad"].ToString().Trim() + " - " + lLector["desc_modalidad"].ToString().Trim();
                            lblDat4V.Text = lLector["codigo_punto_entrega"].ToString().Trim() + " - " + lLector["desc_punto_ent"].ToString().Trim();
                            lblDat5V.Text = lLector["cantidad"].ToString().Trim();
                            lblDat6V.Text = lLector["precio"].ToString().Trim();
                            lblDat7V.Text = lLector["fecha_inicial"].ToString().Trim();
                            lblDat8V.Text = lLector["fecha_final"].ToString().Trim();
                            lblDat9V.Text = lLector["sentido_flujo"].ToString().Trim();
                            lblDat10V.Text = lLector["presion_punto_fin"].ToString().Trim();
                            if (lLector["ruta_contrato"].ToString().Trim().Length > 0)
                                LinkV.HRef = "../archivos/" + lLector["ruta_contrato"].ToString().Trim();
                            else
                                LinkV.Visible = false;
                        }
                    }
                    else
                    {
                        ToastrEvent?.Invoke($"Problemas al Obtener los Datos de la Compra.! {goInfo.mensaje_error}", EnumTypeToastr.Error);
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lblDat1Ver.Text = "Correcta";
                    lblDat2Ver.Text = "Correcta";
                    lblDat3Ver.Text = "Correcta";
                    lblDat4Ver.Text = "Correcta";
                    lblDat5Ver.Text = "Correcta";
                    lblDat6Ver.Text = "Correcta";
                    lblDat7Ver.Text = "Correcta";
                    lblDat8Ver.Text = "Correcta";
                    lblDat9Ver.Text = "Correcta";
                    lblDat10Ver.Text = "Correcta";
                    /// Realizo la Verificacion entre los Datos de la Compra y la Venta
                    if (HdfDestinoRueda == "G")
                    {
                        TrTra1.Visible = false;
                        TrTra2.Visible = false;
                        lblTit1.Text = "Número del contrato";
                        lblTit2.Text = "Fecha de suscripción del contrato";
                        lblTit3.Text = "Modalidad de Contrato";
                        lblTit4.Text = "Punto de entrega de la energía al comprador";
                        lblTit5.Text = "Cantidad de energía contratada (MBTUD)";
                        lblTit6.Text = "Precio a la fecha de suscripción del contrato (USD/MBTU)";
                        lblTit7.Text = "Fecha de inicio de la obligación de entrega";
                        lblTit8.Text = "Fecha de terminación de la obligación de entrega";
                        lblTit11.Text = "Copia Contrato";
                        if (lblDat1V.Text != lblDat1C.Text)
                        {
                            lsVerifica = "0";
                            lblDat1Ver.Text = "Incorrecta";
                        }
                        if (lblDat2V.Text != lblDat2C.Text)
                        {
                            lsVerifica = "0";
                            lblDat2Ver.Text = "Incorrecta";
                        }
                        if (lblDat3V.Text != lblDat3C.Text)
                        {
                            lsVerifica = "0";
                            lblDat3Ver.Text = "Incorrecta";
                        }
                        if (lblDat4V.Text != lblDat4C.Text)
                        {
                            lsVerifica = "0";
                            lblDat4Ver.Text = "Incorrecta";
                        }
                        if (lblDat5V.Text != lblDat5C.Text)
                        {
                            lsVerifica = "0";
                            lblDat5Ver.Text = "Incorrecta";
                        }
                        if (lblDat6V.Text != lblDat6C.Text)
                        {
                            lsVerifica = "0";
                            lblDat6Ver.Text = "Incorrecta";
                        }
                        if (lblDat7V.Text != lblDat7C.Text)
                        {
                            lsVerifica = "0";
                            lblDat7Ver.Text = "Incorrecta";
                        }
                        if (lblDat8V.Text != lblDat8C.Text)
                        {
                            lsVerifica = "0";
                            lblDat8Ver.Text = "Incorrecta";
                        }
                    }
                    else
                    {
                        lblTit1.Text = "Número del contrato";
                        lblTit2.Text = "Fecha de suscripción del contrato";
                        lblTit3.Text = "Modalidad de Contrato";
                        lblTit4.Text = "Tramo o grupos de gasoductos";
                        lblTit5.Text = "Capacidad Contratada  (KPCD)";
                        lblTit6.Text = "Tarifa a la fecha de suscripción del contrato (Moneda vigente/KPC)"; //20220828
                        lblTit7.Text = "Fecha de inicio de la prestación del servicio";
                        lblTit8.Text = "Fecha de terminación prestación del servicio";
                        lblTit9.Text = "Sentido Contratado para el flujo de Gas Natural";
                        lblTit10.Text = "Presión Punto Final";
                        lblTit11.Text = "Copia Contrato";
                        if (lblDat1V.Text != lblDat1C.Text)
                        {
                            lsVerifica = "0";
                            lblDat1Ver.Text = "Incorrecta";
                        }
                        if (lblDat2V.Text != lblDat2C.Text)
                        {
                            lsVerifica = "0";
                            lblDat2Ver.Text = "Incorrecta";
                        }
                        if (lblDat3V.Text != lblDat3C.Text)
                        {
                            lsVerifica = "0";
                            lblDat3Ver.Text = "Incorrecta";
                        }
                        if (lblDat4V.Text != lblDat4C.Text)
                        {
                            lsVerifica = "0";
                            lblDat4Ver.Text = "Incorrecta";
                        }
                        if (lblDat5V.Text != lblDat5C.Text)
                        {
                            lsVerifica = "0";
                            lblDat5Ver.Text = "Incorrecta";
                        }
                        if (lblDat6V.Text != lblDat6C.Text)
                        {
                            lsVerifica = "0";
                            lblDat6Ver.Text = "Incorrecta";
                        }
                        if (lblDat7V.Text != lblDat7C.Text)
                        {
                            lsVerifica = "0";
                            lblDat7Ver.Text = "Incorrecta";
                        }
                        if (lblDat8V.Text != lblDat8C.Text)
                        {
                            lsVerifica = "0";
                            lblDat8Ver.Text = "Incorrecta";
                        }
                        if (HdfTipoMerc == "P")
                        {
                            TrTra1.Visible = true;
                            TrTra2.Visible = true;
                            if (lblDat9V.Text != lblDat9C.Text)
                            {
                                lsVerifica = "0";
                                lblDat9Ver.Text = "Incorrecta";
                            }
                            if (lblDat10V.Text != lblDat10C.Text)
                            {
                                lsVerifica = "0";
                                lblDat10Ver.Text = "Incorrecta";
                            }
                        }
                        else
                        {
                            TrTra1.Visible = false;
                            TrTra2.Visible = false;
                        }
                    }
                    if (lsVerifica == "0")
                    {
                        btnRegCont.Visible = false;
                    }
                    else
                        btnRegCont.Visible = true;
                    //Abre el modal de Verificación
                    ModalEvent?.Invoke(registroContratoVer.ID, registroContratoVerInside.ID, EnumTypeModal.Abrir);
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke($"Problemas en la Recuperación de la Información. {ex.Message}", EnumTypeToastr.Error);
                }
            }
        }

        /// <summary>
        /// Metodo que inactiva los campos de la pantalla cuando se esta corrigiendo el registro despues
        /// de la verificacion.
        /// Requerimiento ALgoritmo y verificacion 20150529
        /// </summary>
        protected void inactivarCampos()
        {
            if (hdfEstadoAct.Value == "E") //20220418 correcion contrato
            {
                string[] lsNombreParametros = { "@P_codigo_cont_datos", "@P_codigo_verif_contrato", "@P_punta_contrato" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
                Object[] lValorParametros = { "0", "0", "" };
                SqlDataReader oLector;

                var lsContratoC = "";
                var lsfechaSusC = "";
                var lsCodModaC = "";
                var lsCodPtoC = "";
                var lsCantC = "";
                var lsPrecC = "";
                var lsFecIniC = "";
                var lsFecFinC = "";
                var lsSentiC = "";
                var lsPresionC = "";
                var lsConectadoSntC = "";  //20171114 rq036-17
                var lsEntBocaPozoC = ""; //20171114 rq036-17
                var lsFuenteC = ""; //20220418 corerccion contrato
                var lsCentroC = ""; //20171114 rq036-17
                var lsPeriodoC = "";//20171130 rq026-17
                var lsIndVarC = "";//20171130 rq026-17
                var lsClasTraC = "";//20201224
                var lsTrmC = "";//20210707
                var lsObsTrmC = "";//20210707
                var lsMonedaC = "";//20210707

                var lsContratoV = "";
                var lsfechaSusV = "";
                var lsCodModaV = "";
                var lsCodPtoV = "";
                var lsCantV = "";
                var lsPrecV = "";
                var lsFecIniV = "";
                var lsFecFinV = "";
                var lsSentiV = "";
                var lsPresionV = "";
                var lsConectadoSntV = "";  //20171114 rq036-17
                var lsEntBocaPozoV = ""; //20171114 rq036-17
                var lsFuenteV = ""; //20220418 corerccion contrato
                var lsCentroV = ""; //20171114 rq036-17
                var lsPeriodoV = ""; //20171130 rq026-17
                var lsIndVarV = "";//20171130 rq026-17
                var lsClasTraV = "";//20201224
                var lsTrmV = "";//20210707
                var lsObsTrmV = "";//20210707
                var lsMonedaV = "";//20210707

                try
                {
                    lConexion1.Abrir();
                    // Obtengo los Datos de la Punta Compradora
                    lValorParametros[1] = hdfIdVerif.Value;
                    lValorParametros[2] = "C";
                    oLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion1.gObjConexion, "pa_GetRegistroContratosDatos", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error == "")
                    {
                        if (oLector.HasRows)
                        {
                            oLector.Read();
                            lsContratoC = oLector["contrato_definitivo"].ToString().Trim().ToUpper();
                            lsfechaSusC = oLector["fecha_suscripcion_cont"].ToString().Trim();
                            lsCodModaC = oLector["codigo_modalidad"] + " - " + oLector["desc_modalidad"].ToString().Trim();
                            lsCodPtoC = oLector["codigo_punto_entrega"] + " - " + oLector["desc_punto_ent"].ToString().Trim();
                            lsCantC = oLector["cantidad"].ToString().Trim();
                            lsPrecC = oLector["precio"].ToString().Trim();
                            lsFecIniC = oLector["fecha_inicial"].ToString().Trim();
                            lsFecFinC = oLector["fecha_final"].ToString().Trim();
                            lsSentiC = oLector["sentido_flujo"].ToString().Trim();
                            lsPresionC = oLector["presion_punto_fin"].ToString().Trim();
                            lsConectadoSntC = oLector["conectado_snt"].ToString().Trim();  //20171114 rq036-17
                            lsFuenteC = oLector["codigo_fuente"].ToString().Trim(); //2020418 corerccon contratos
                            lsEntBocaPozoC = oLector["ent_boca_pozo"].ToString().Trim(); //20171114 rq036-17
                            lsCentroC = oLector["codigo_centro_pob"].ToString().Trim(); //20171114 rq036-17
                            lsPeriodoC = oLector["codigo_periodo"].ToString().Trim(); //20171130 rq026-17
                            lsIndVarC = oLector["ind_contrato_var"].ToString().Trim(); //20171130 rq026-17
                            lsClasTraC = oLector["clasificacion_transp"].ToString().Trim(); //20201224
                            lsTrmC = oLector["codigo_trm"].ToString().Trim(); //20210707
                            lsObsTrmC = oLector["observacion_trm"].ToString().Trim(); //20210707
                            lsMonedaC = oLector["tipo_moneda"].ToString().Trim(); //20210707
                        }
                    }
                    oLector.Close();
                    oLector.Dispose();
                    // Obtengo los Datos de la Punta Vendedora
                    lValorParametros[1] = hdfIdVerif.Value;
                    lValorParametros[2] = "V";
                    oLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion1.gObjConexion, "pa_GetRegistroContratosDatos", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error == "")
                    {
                        if (oLector.HasRows)
                        {
                            oLector.Read();
                            lsContratoV = oLector["contrato_definitivo"].ToString().Trim().ToUpper();
                            lsfechaSusV = oLector["fecha_suscripcion_cont"].ToString().Trim();
                            lsCodModaV = oLector["codigo_modalidad"] + " - " + oLector["desc_modalidad"].ToString().Trim();
                            lsCodPtoV = oLector["codigo_punto_entrega"] + " - " + oLector["desc_punto_ent"].ToString().Trim();
                            lsCantV = oLector["cantidad"].ToString().Trim();
                            lsPrecV = oLector["precio"].ToString().Trim();
                            lsFecIniV = oLector["fecha_inicial"].ToString().Trim();
                            lsFecFinV = oLector["fecha_final"].ToString().Trim();
                            lsSentiV = oLector["sentido_flujo"].ToString().Trim();
                            lsPresionV = oLector["presion_punto_fin"].ToString().Trim();
                            lsConectadoSntV = oLector["conectado_snt"].ToString().Trim();  //20171114 rq036-17
                            lsEntBocaPozoV = oLector["ent_boca_pozo"].ToString().Trim(); //20171114 rq036-17
                            lsFuenteV = oLector["codigo_fuente"].ToString().Trim(); //2020418 correccon contrato
                            lsCentroV = oLector["codigo_centro_pob"].ToString().Trim(); //20171114 rq036-17
                            lsPeriodoV = oLector["codigo_periodo"].ToString().Trim(); //20171130 rq026-17
                            lsIndVarV = oLector["ind_contrato_var"].ToString().Trim(); //20171130 rq026-17
                            lsClasTraV = oLector["clasificacion_transp"].ToString().Trim(); //20201224
                            lsTrmV = oLector["codigo_trm"].ToString().Trim(); //20210707
                            lsObsTrmV = oLector["observacion_trm"].ToString().Trim(); //20210707
                            lsMonedaV = oLector["tipo_moneda"].ToString().Trim(); //20210707
                        }
                    }
                    oLector.Close();
                    oLector.Dispose();
                    // Compraro los datos para inhabilitar las cajas de texto
                    if (lsContratoV == lsContratoC)
                        TxtNumContrato.Enabled = false;
                    if (lsfechaSusV == lsfechaSusC)
                        TxtFechaSus.Enabled = false;
                    if (lsCodModaV == lsCodModaC)
                        ddlModalidad.Enabled = false;
                    if (lsCodPtoV == lsCodPtoC)
                        ddlPuntoEntr.Enabled = false;
                    if (lsCantV == lsCantC)
                        TxtCantidad.Enabled = false;
                    if (lsPrecV == lsPrecC)
                        TxtPrecio.Enabled = false;
                    if (lsFecIniV == lsFecIniC)
                    {
                        TxtFechaInicial.Enabled = false;
                        TxtHoraInicial.Enabled = false;
                    }
                    if (lsFecFinV == lsFecFinC)
                    {
                        TxtFechaFinal.Enabled = false;
                        TxtHoraFinal.Enabled = false;
                    }
                    if (lsSentiV == lsSentiC)
                        ddlSentidoFlujo.Enabled = false;
                    if (lsPresionV == lsPresionC)
                        TxtPresion.Enabled = false;
                    //20220418 correccion contrato 
                    if (lsFuenteV == lsFuenteC)
                        ddlFuente.Enabled = false;
                    //20171114 rq036-17
                    if (lsConectadoSntV == lsConectadoSntC)
                        ddlConecSnt.Enabled = false;
                    //20171114 rq036-17
                    if (lsEntBocaPozoV == lsEntBocaPozoC)
                        ddlEntBocPozo.Enabled = false;
                    //20171114 rq036-17
                    if (lsCentroV == lsCentroC)
                        ddlCentro.Enabled = false;
                    //20171130 rq026-17
                    if (lsPeriodoV == lsPeriodoC)
                        ddlPeriodo.Enabled = false;
                    //20171130 rq026-17
                    if (lsIndVarV == lsIndVarC)
                        ddlIndVar.Enabled = false;
                    //20201224
                    if (lsClasTraV == lsClasTraC)
                        DdlClasifica.Enabled = false;
                    //20210707
                    if (lsTrmV == lsTrmC)
                        ddlTrm.Enabled = false;
                    //20210707
                    if (lsMonedaV == lsMonedaC)
                        ddlMoneda.Enabled = false;
                    lConexion1.Cerrar();
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke($"Se presentó un Error en el bloqueo de los Campos de la Corrección. {ex.Message}", EnumTypeToastr.Error);
                }
            }//20220418
            else//20220418
            {
                string[] lsNombreParametros = { "@P_tabla", "@P_filtro" };
                SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar };
                Object[] lValorParametros = { "t_contrato_datos_mod", "codigo_verif_contrato=" + hdfIdVerif.Value };
                SqlDataReader oLector;

                try
                {
                    lConexion1.Abrir();
                    oLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion1.gObjConexion, "pa_ValidarExistencia", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error == "")
                    {
                        if (oLector.HasRows)
                        {
                            oLector.Read();
                            TxtNumContrato.Enabled = false;
                            if (oLector["contrato_definitivo"].ToString() == "S")
                                TxtNumContrato.ForeColor = Color.Red;
                            TxtFechaSus.Enabled = false;
                            if (oLector["fecha_suscripcion_cont"].ToString() == "S")
                                TxtFechaSus.ForeColor = Color.Red;
                            ddlModalidad.Enabled = false;
                            if (oLector["codigo_modalidad"].ToString() == "S")
                                ddlModalidad.ForeColor = Color.Red;
                            ddlPuntoEntr.Enabled = false;
                            if (oLector["codigo_punto_entrega"].ToString() == "S")
                                ddlPuntoEntr.ForeColor = Color.Red;
                            TxtCantidad.Enabled = false;
                            if (oLector["cantidad"].ToString() == "S")
                                TxtCantidad.ForeColor = Color.Red;
                            TxtPrecio.Enabled = false;
                            if (oLector["precio"].ToString() == "S")
                                TxtPrecio.ForeColor = Color.Red;
                            TxtFechaInicial.Enabled = false;
                            TxtHoraInicial.Enabled = false;
                            if (oLector["fecha_inicial"].ToString() == "S")
                            {
                                TxtFechaInicial.ForeColor = Color.Red;
                                TxtHoraInicial.ForeColor = Color.Red;
                            }
                            TxtFechaFinal.Enabled = false;
                            TxtHoraFinal.Enabled = false;
                            if (oLector["fecha_final"].ToString() == "S")
                            {
                                TxtFechaFinal.ForeColor = Color.Red;
                                TxtHoraFinal.ForeColor = Color.Red;
                            }
                            ddlSentidoFlujo.Enabled = false;
                            if (oLector["sentido_flujo"].ToString() == "S")
                                ddlSentidoFlujo.ForeColor = Color.Red;
                            TxtPresion.Enabled = false;
                            if (oLector["presion_punto_fin"].ToString() == "S")
                                TxtPresion.ForeColor = Color.Red;
                            ddlConecSnt.Enabled = false;
                            if (oLector["conectado_snt"].ToString() == "S")
                                ddlConecSnt.ForeColor = Color.Red;
                            ddlEntBocPozo.Enabled = false;
                            if (oLector["ent_boca_pozo"].ToString() == "S")
                                ddlEntBocPozo.ForeColor = Color.Red;
                            ddlCentro.Enabled = false;
                            if (oLector["codigo_centro_pob"].ToString() == "S")
                                ddlCentro.ForeColor = Color.Red;
                            ddlFuente.Enabled = false;
                            if (oLector["codigo_fuente"].ToString() == "S")
                                ddlFuente.ForeColor = Color.Red;
                            ddlPeriodo.Enabled = false;
                            if (oLector["codigo_periodo"].ToString() == "S")
                                ddlPeriodo.ForeColor = Color.Red;
                            ddlIndVar.Enabled = false;
                            if (oLector["ind_contrato_var"].ToString() == "S")
                                ddlIndVar.ForeColor = Color.Red;
                            DdlClasifica.Enabled = false;
                            if (oLector["clasificacion_transp"].ToString() == "S")
                                DdlClasifica.ForeColor = Color.Red;
                            ddlTrm.Enabled = false;
                            if (oLector["codigo_trm"].ToString() == "S")
                                ddlTrm.ForeColor = Color.Red;
                            ddlMoneda.Enabled = false;
                            if (oLector["tipo_moneda"].ToString() == "S")
                                ddlMoneda.ForeColor = Color.Red;
                        }
                    }
                    oLector.Close();
                    oLector.Dispose();
                    // Compraro los datos para inhabilitar las cajas de texto
                    lConexion1.Cerrar();
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke($"Se presentó un Error en el bloqueo de los Campos de la Corrección. {ex.Message}", EnumTypeToastr.Error);
                }
            }
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_operador)
        {
            var lsCondicion = "nombre_tabla='t_contrato_verificacion' and llave_registro='codigo_verif_contrato=" + lscodigo_operador + "'";
            var lsCondicion1 = "codigo_verif_contrato=" + lscodigo_operador;
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                var lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_contrato_verificacion";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_contrato_verificacion", lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// Realiza las Validaciones de los Tipos de Datos y Campos Obligatorios de la Pantalla
        /// </summary>
        /// <returns></returns>
        protected string validaciones()
        {
            decimal ldValor = 0;
            var liValor = 0;
            DateTime lFecha;
            var lsError = "";
            string[] lsPrecio;
            string[] lsFecha;

            try
            {
                if (TxtNumContrato.Text.Trim().Length <= 0)
                    lsError += "Debe Ingresar el No. de Contrato. <br>";
                if (TxtFechaSus.Text.Trim().Length <= 0)
                    lsError += "Debe Ingresar la Fecha de Suscripción. <br>";
                else
                {
                    try
                    {
                        lFecha = Convert.ToDateTime(TxtFechaSus.Text.Trim());
                        lsFecha = TxtFechaSus.Text.Trim().Split('/');
                        if (lsFecha[0].Trim().Length != 4)
                            lsError += "Formato Inválido en Fecha de Suscripción. <br>";
                    }
                    catch (Exception)
                    {
                        lsError += "Valor Inválido en Fecha de Suscripción. <br>";
                    }
                }
                if (ddlModalidad.SelectedValue == "0" && TxtOtrModalidad.Text.Trim().Length <= 0)
                    lsError += "Debe Ingresar la Modalidad de Contrato. <br>";
                //20170814 RQ036-17
                if (!trSum.Visible || !ddlCentro.Visible)
                {
                    if (ddlPuntoEntr.SelectedValue == "0" && TxtOtrPuntoE.Text.Trim().Length <= 0)
                        lsError += "Debe Ingresar el Punto de Entrega. <br>";
                }
                if (TxtCantidad.Text.Trim().Length <= 0)
                    lsError += "Debe Ingresar " + lblCantidad.Text + " . <br>";
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidad.Text.Trim());
                        if (liValor <= 0)
                            lsError += "La " + lblCantidad.Text + " debe ser mayor que cero. <br>";
                        // Validacion nueva de cantidad requerimiento varios verificacion 20151117
                        if (hdfCodTipoSub.Value != "5")
                        {
                            if (liValor > Convert.ToInt32(hdfCantCnt.Value))
                                lsError += "La " + lblCantidad.Text + " No puede ser Mayor a la Cantidad adjudicada. <br>";
                        }
                    }
                    catch (Exception)
                    {
                        lsError += "Valor Inválido en " + lblCantidad.Text + ". <br>";
                    }
                }
                if (TxtPrecio.Text.Trim().Length <= 0)
                    lsError += "Debe Ingresar " + lblPrecio.Text + " . <br>";
                else
                {
                    try
                    {
                        ldValor = Convert.ToDecimal(TxtPrecio.Text.Trim());
                        if (ldValor < 0)
                            lsError += "Valor Inválido en " + lblPrecio.Text + ". <br>";
                        else
                        {
                            lsPrecio = TxtPrecio.Text.Trim().Split('.');
                            if (lsPrecio.Length > 1)
                            {
                                if (lsPrecio[1].Trim().Length > 2)
                                    lsError += "Valor Inválido en " + lblPrecio.Text + ". <br>";
                            }
                        }
                    }
                    catch (Exception)
                    {
                        lsError += "Valor Inválido en " + lblPrecio.Text + ". <br>";
                    }
                }
                if (TxtFechaInicial.Text.Trim().Length <= 0)
                    lsError += "Debe Ingresar " + lblFechaInc.Text + ". <br>";
                else
                {
                    try
                    {
                        lFecha = Convert.ToDateTime(TxtFechaInicial.Text.Trim());
                        lsFecha = TxtFechaInicial.Text.Trim().Split('/');
                        if (lsFecha[0].Trim().Length != 4)
                            lsError += "Formato Inválido en " + lblFechaInc.Text + ". <br>";
                        //if (Convert.ToDateTime(TxtFechaInicial.Text.Trim()) < Convert.ToDateTime(TxtFechaSus.Text.Trim()))
                        //    lsError += "La " + lblFechaInc.Text + " NO puede ser menor que la Fecha de Suscripcion. <br>";
                    }
                    catch (Exception)
                    {
                        lsError += "Valor Inválido en " + lblFechaInc.Text + ". <br>";
                    }
                }
                if (TxtFechaFinal.Text.Trim().Length <= 0)
                    lsError += "Debe Ingresar " + lblFechaFin.Text + ". <br>";
                else
                {
                    try
                    {
                        lFecha = Convert.ToDateTime(TxtFechaFinal.Text.Trim());
                        if (Convert.ToDateTime(TxtFechaFinal.Text.Trim()) < Convert.ToDateTime(TxtFechaInicial.Text.Trim()))
                            lsError += "La " + lblFechaFin.Text + " No puede ser menor que " + lblFechaInc.Text + " . <br>";
                        lsFecha = TxtFechaFinal.Text.Trim().Split('/');
                        if (lsFecha[0].Trim().Length != 4)
                            lsError += "Formato Inválido en " + lblFechaFin.Text + ". <br>";
                    }
                    catch (Exception)
                    {
                        lsError += "Valor Inválido en " + lblFechaFin.Text + ". <br>";
                    }
                }
                if (hdfAccion.Value == "C")
                {
                    //if (FuCopiaCont.FileName.Trim().Length <= 0)
                    //    lsError += "Debe Seleccionar el Archivo del Contrato. <br>";
                }
                else
                {
                    if (ddlMotivoModifi.SelectedValue == "0")
                        lsError += "Debe Seleccionar el Motivo de Modificación. <br>"; // 20161207 rq102 conformacion de rutas
                                                                                       //20171130 rq026-17
                                                                                       //else
                                                                                       //{
                                                                                       //    if (DelegadaBase.Servicios.ValidarExistencia("m_motivos_modificacion", " codigo_motivo_modifica = " + ddlMotivoModifi.SelectedValue + " And cesion = 'S' ", goInfo))
                                                                                       //    {
                                                                                       //        if (ddlCesionario.SelectedValue == "0")
                                                                                       //            lsError += "Debe Seleccionar el Cesionario YA que el Motivo de Modificación es Cesión. <br>"; // 20161207 rq102 conformacion de rutas
                                                                                       //    }
                                                                                       //}
                                                                                       //20220418 correcion contrato
                    if (txtObservaModif.Text == "")
                        lsError += "Debe digitar las observaciones de modificación. <br>";
                }
                if (hdfPunta.Value == "C")
                {
                    //if (dlTipoDemanda.SelectedValue == "0")
                    //    lsError += "Debe Seleccionar El Tipo de Demanda a Atender. <br>";
                    //else
                    //{
                    var lsCont = "0";
                    if (hdfCodVerifCont.Value.Trim().Length > 0)
                        lsCont = hdfCodVerifCont.Value;
                    if (lsCont == "0")
                    {
                        if (!DelegadaBase.Servicios.ValidarExistencia("t_contrato_datos_usuarios", " codigo_cont_datos = " + lsCont + " And login_usuario = '" + goInfo.Usuario + "'", goInfo))
                            lsError += "No ha Ingresado Información de Usuarios Finales. <br>"; //20161207 rq102 conformacion de rutas
                        else
                        {
                            //if (HdfDestinoRueda == "G")
                            //{
                            if (Convert.ToDecimal(TxtCantidad.Text) != Convert.ToDecimal(lblTotlCantidad.Text))
                                lsError += "Hay Diferencias entre la Cantidad del Contrato y la Cantidad de los usuarios Finales. <br>";
                            //}
                            //else // Valicacion agregada para mercado relevante secto 20160125
                            //{
                            //    if (Convert.ToDecimal(TxtCantidad.Text) < Convert.ToDecimal(lblTotlCantidad.Text))
                            //        lsError += "La Cantidad del Contrato no puede ser menor que la Cantidad de los usuarios Finales. <br>";
                            //}
                        }
                    }
                    else
                    {
                        if (!DelegadaBase.Servicios.ValidarExistencia("t_contrato_datos_usuarios", " codigo_cont_datos = " + lsCont + " ", goInfo))
                            lsError += "No ha Ingresado Información de Usuarios Finales. <br>"; //20161207 rq102 conformacion de rutas
                        else
                        {
                            if (Convert.ToDecimal(TxtCantidad.Text) != Convert.ToDecimal(lblTotlCantidad.Text))
                                lsError += "Hay Diferencias entre la Cantidad del Contrato y la Cantidad de los usuarios Finales. <br>"; //20161207 rq102 conformacion de rutas //20180126 rq107-16
                        }
                    }
                    //}
                }
                //20160810 valida fecha
                //20190520
                //if (DelegadaBase.Servicios.ValidarExistencia("t_contrato con, t_id_rueda idr, m_periodos_entrega per ", " con.numero_contrato =" + lblOperacion.Text + " and con.numero_id = idr.numero_id  and idr.codigo_periodo = per.codigo_periodo and case when per.medida_tiempo ='I' then con.fecha_creacion else convert(varchar(10),con.fecha_creacion,112) end > case when per.medida_tiempo ='I' then '" + TxtFechaInicial.Text + " " + TxtHoraInicial.Text + "' else '" + TxtFechaInicial.Text + "' end ", goInfo)) //20181016 ajsute
                //    lsError += "La fecha Inicial debe ser mayor que la fecha de negociación. <br>";

                if (HdfDestinoRueda == "T" && HdfTipoMerc == "P")
                {
                    if (ddlSentidoFlujo.SelectedValue == "")
                        lsError += "Debe Ingresar Sentido Contratado para el flujo de Gas Natural. <br>";
                    if (TxtPresion.Text.Trim().Length <= 0)
                        lsError += "Debe Ingresar Presión para el punto de terminación del servicio (psig). <br>";
                    else
                    {
                        ////////////////////////////////////////////////////////////////////////////////////////////////
                        //// Validacion Nueva sentido del flujo Requerimiento Ajuste presion transporte 20151006 ///////
                        ////////////////////////////////////////////////////////////////////////////////////////////////
                        string[] lsPresion;
                        try
                        {
                            if (TxtPresion.Text.Trim().Length > 500)
                                lsError += "Valor Inválido en Presión para el punto de terminación del servicio (psig). <br>"; //20161207 rq102 conformacion de rutas
                            else
                            {
                                lsPresion = TxtPresion.Text.Trim().Split('-');
                                foreach (var Presion in lsPresion)
                                {
                                    try
                                    {
                                        ldValor = Convert.ToDecimal(Presion.Trim());
                                    }
                                    catch (Exception)
                                    {
                                        lsError += "Valor Inválido en Presión para el punto de terminación del servicio (psig). <br>";
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {
                            lsError += "Valor Inválido en Presión para el punto de terminación del servicio (psig). <br>";
                        }

                    }
                }
                //Se agrega valiadación fuente-campo MP 20160601
                if (HdfDestinoRueda == "G" && ddlFuente.SelectedValue == "0" && HdfTipoMerc == "P")
                {
                    lsError += "Debe seleccionar la fuente. <br>";
                }

                //validacion para rueda neg directa 20151228 
                if (hdfCodTipoSub.Value == "5")
                {
                    if (ddlModalidad.SelectedValue != "0")
                    {
                        if (!DelegadaBase.Servicios.ValidarExistencia("m_caracteristica_sub", " codigo_tipo_subasta = 5 and tipo_caracteristica ='M' and destino_rueda ='" + HdfDestinoRueda + "' and tipo_mercado='" + HdfTipoMerc + "' and estado ='A' and codigo_caracteristica =" + ddlModalidad.SelectedValue, goInfo))
                            lsError += "La modalidad de contrto no está parametrizada para el tipo de mercado y producto. <br>";
                    }
                    // se cambia la validacion para MS por fuente campo MP 20160601
                    if (HdfDestinoRueda == "G" && ddlPuntoEntr.SelectedValue != "0" && HdfTipoMerc == "S")
                    {
                        if (!DelegadaBase.Servicios.ValidarExistencia("m_caracteristica_sub", " codigo_tipo_subasta = 5 and tipo_caracteristica ='T' and destino_rueda ='G' and tipo_mercado='S' and estado ='A' and codigo_caracteristica =" + ddlPuntoEntr.SelectedValue, goInfo))
                            lsError += "El punto de entrega no está parametrizado para el tipo de mercado y producto. <br>";
                        //Valida restricciones de subasta 20160127
                        //20180802 ajuste
                        //if (DelegadaBase.Servicios.ValidarExistencia("m_pozo pozo, m_tipos_rueda tpo, t_contrato con, t_id_rueda idr, m_restriccion_subasta rest", " pozo.codigo_pozo = " + ddlPuntoEntr.SelectedValue + " And rest.codigo_tipo_subasta = 5 and tpo.codigo_tipo_subasta = 5 and tpo.destino_rueda ='" + HdfDestinoRueda + "' and tpo.tipo_mercado = '" + HdfTipoMerc + "' and con.numero_contrato = " + lblOperacion.Text + " and con.numero_id = idr.numero_id  and tpo.codigo_tipo_rueda = rest.codigo_tipo_rueda and rest.codigo_periodo = idr.codigo_periodo and rest.codigo_modalidad =" + ddlModalidad.SelectedValue + " and (pozo.codigo_tipo_campo = rest.codigo_tipo_campo or rest.codigo_tipo_campo=0) And rest.estado ='A'", goInfo))
                        //    lsError += "La Modalidad, Periodo, Tipo de Mercado y Tipo de Campo atado al Punto de Entrega está en los parametros de restricción de subasta para Bilateral. <br>";
                    }
                    // se agrega validacion para MP fuente campo MP 20160601
                    if (HdfDestinoRueda == "G" && HdfTipoMerc == "P")
                    {
                        if (!DelegadaBase.Servicios.ValidarExistencia("m_pozo poz, m_caracteristica_sub carC", " poz.codigo_pozo = " + ddlFuente.SelectedValue + "  and carC.codigo_tipo_subasta = 5 and carC.tipo_caracteristica ='C' and carC.destino_rueda ='G' and carC.tipo_mercado='" + HdfTipoMerc + "' and carC.estado ='A' and codigo_caracteristica = poz.codigo_tipo_campo", goInfo))
                            lsError += "El tipo de campo no está parametrizado para el tipo de mercado y producto. <br>";
                        //Valida restricciones de subasta 20160127
                        //20180802 ajuste
                        //if (DelegadaBase.Servicios.ValidarExistencia("m_pozo pozo, m_tipos_rueda tpo, t_contrato con, t_id_rueda idr, m_restriccion_subasta rest", " pozo.codigo_pozo = " + ddlFuente.SelectedValue + " And rest.codigo_tipo_subasta = 5 and tpo.codigo_tipo_subasta = 5 and tpo.destino_rueda ='" + HdfDestinoRueda + "' and tpo.tipo_mercado = '" + HdfTipoMerc + "' and con.numero_contrato = " + lblOperacion.Text + " and con.numero_id = idr.numero_id  and tpo.codigo_tipo_rueda = rest.codigo_tipo_rueda and rest.codigo_periodo = idr.codigo_periodo and rest.codigo_modalidad =" + ddlModalidad.SelectedValue + " and (pozo.codigo_tipo_campo = rest.codigo_tipo_campo or rest.codigo_tipo_campo=0) And rest.estado ='A'", goInfo))
                        //    lsError += "La Modalidad, Periodo, Tipo de Mercado y Tipo de Campo atado a la fuente está en los parametros de restricción de subasta para Bilateral. <br>";
                        if (!DelegadaBase.Servicios.ValidarExistencia("m_pozo poz, m_campo_modalidad cam", " poz.codigo_pozo = " + ddlFuente.SelectedValue + "  and poz.codigo_tipo_campo= cam.codigo_tipo_campo and cam.codigo_modalidad =" + ddlModalidad.SelectedValue + " and cam.tipo_mercado='P' and cam.estado='A'", goInfo))
                            lsError += "El tipo de contrato no está parametrizado para el tipo de campo. <br>";
                        if (DelegadaBase.Servicios.ValidarExistencia("m_pozo poz, m_campo_periodo cam, t_contrato con", " poz.codigo_pozo = " + ddlFuente.SelectedValue + "  and poz.codigo_tipo_campo= cam.codigo_tipo_campo and cam.codigo_tipo_subasta = 5 and cam.destino_rueda='G' and  cam.tipo_mercado='P' and cam.estado ='A' and con.numero_contrato =" + lblOperacion.Text + " and (con.fecha_creacion < cam.fecha_inicial or convert(varchar(10),con.fecha_creacion, 111 ) > convert(varchar(10),cam.fecha_final,111))", goInfo))
                            lsError += "La fecha de negociación no está dentro del periodo para registro de negociaciones para el tipo de campo. <br>";
                        //20160808 modalidad campo
                        //20190404 rq018-19 fase II    
                        //lConexion.Abrir();
                        //lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_pozo poz, m_tipo_campo tpo, m_modalidad_contractual mod, m_campo_modalidad cam", " poz.codigo_pozo =" + ddlFuente.SelectedValue + " and poz.codigo_tipo_campo = tpo.codigo_tipo_campo and tpo.estado ='A' and mod.codigo_modalidad =" + ddlModalidad.SelectedValue + " and mod.ind_contingencia ='N' and poz.codigo_tipo_campo = cam.codigo_tipo_campo and mod.codigo_modalidad = cam.codigo_modalidad and cam.tipo_mercado = 'P' and cam.estado='A' and cam.ind_restriccion ='S'");  //fuente campo MP 20160601
                        //if (lLector.HasRows)
                        //{
                        //    lLector.Read();
                        //    if (lLector["mes_inicio"].ToString() != "0")
                        //        if (Convert.ToInt32(lLector["mes_inicio"].ToString()) != Convert.ToDateTime(TxtFechaInicial.Text).Month || Convert.ToInt32(lLector["dia_inicio"].ToString()) != Convert.ToDateTime(TxtFechaInicial.Text).Day)
                        //            lsError += "La fecha de inicio (" + TxtFechaInicial.Text + ") no corresponde con el mes y día parametrizado para el tipo de campo " + lLector["mes_inicio"].ToString() + "-" + lLector["dia_inicio"].ToString() + "<br>";
                        //    if (lLector["mes_fin"].ToString() != "0")
                        //        if (Convert.ToInt32(lLector["mes_fin"].ToString()) != Convert.ToDateTime(TxtFechaFinal.Text).Month || Convert.ToInt32(lLector["dia_fin"].ToString()) != Convert.ToDateTime(TxtFechaFinal.Text).Day)
                        //            lsError += "La fecha de finalización (" + TxtFechaFinal.Text + ") no corresponde con el mes y día parametrizado para el tipo de campo " + lLector["mes_fin"].ToString() + "-" + lLector["dia_fin"].ToString() + "<br>";
                        //    if (lLector["mes_inicio"].ToString() != "0" && lLector["mes_fin"].ToString() != "0")
                        //    {
                        //        int ldAnos = Convert.ToDateTime(TxtFechaFinal.Text).Year - Convert.ToDateTime(TxtFechaInicial.Text).Year;
                        //        if (ldAnos <= 0 || ldAnos > 1 && ldAnos < 5)
                        //            lsError += "La duración para este campo debe ser de 1 o mayor o igual que 5 años <br>";
                        //    }
                        //}
                        //lLector.Close();
                        //lLector.Dispose();
                        //lConexion.Cerrar();
                        //20190404 fin rq018-19 fase II

                        ////20190301 ajuste
                        /////20190404 rq018-19 fase II
                        //if (ddlModalidad.SelectedValue == "2" || ddlModalidad.SelectedValue == "3")
                        //{
                        //    if (Convert.ToDateTime(TxtFechaFinal.Text) > Convert.ToDateTime("2019/11/30"))
                        //        if (DelegadaBase.Servicios.ValidarExistencia("t_contrato ", " numero_contrato = " + lblOperacion.Text + "  and fecha_creacion < '2019/07/01'", goInfo))
                        //            lsError += "La fecha de finalización debe ser máximo hasta el 2019/11/30. <br>";
                        //}
                    }
                    //20180802 ajuste
                    if (DelegadaBase.Servicios.ValidarExistencia("m_tipos_rueda tpo, t_contrato con, t_id_rueda idr, m_restriccion_subasta rest left join m_pozo pozo on (pozo.codigo_pozo =" + ddlPuntoEntr.SelectedValue + ")", " rest.codigo_tipo_subasta = 5 and tpo.codigo_tipo_subasta = 5 and tpo.destino_rueda ='" + HdfDestinoRueda + "' and tpo.tipo_mercado = '" + HdfTipoMerc + "' and con.numero_contrato = " + lblOperacion.Text + " and con.numero_id = idr.numero_id  and tpo.codigo_tipo_rueda = rest.codigo_tipo_rueda and rest.codigo_periodo = idr.codigo_periodo and rest.codigo_modalidad =" + ddlModalidad.SelectedValue + " and (pozo.codigo_tipo_campo = rest.codigo_tipo_campo or rest.codigo_tipo_campo=0) And rest.estado ='A'", goInfo))
                        lsError += "La Modalidad, Periodo de entrega y tipo de mercado se encuentra dentro de los parámetros de restricción por subasta. <br>";
                }
                //20190301 ajuste
                //20190404 rq018-19 fase II
                //if (HdfTipoMerc == "S" && Convert.ToDateTime(TxtFechaInicial.Text) > Convert.ToDateTime("2019/11/30"))
                //{
                //    if (DelegadaBase.Servicios.ValidarExistencia("m_periodos_entrega ", " codigo_periodo = " + ddlPeriodo.SelectedValue + "  and medida_tiempo ='U'", goInfo))
                //        lsError += "La fecha de inicio debe ser máximo hasta el 2019/11/30. <br>";
                //}
                //20160823 validacion de la PTDVF
                //20220221 registro extemporaneo
                //if (HdfDestinoRueda == "G" && HdfTipoMerc == "P" && (hdfCodTipoSub.Value == "5" || hdfCodTipoSub.Value == "6"))
                //{
                //    if (ddlPuntoEntr.SelectedValue != "0" && TxtFechaInicial.Text != "" && TxtFechaFinal.Text != "" && TxtCantidad.Text != "")
                //    {
                //        //if (ddlModalidad.SelectedValue == "1" || ddlModalidad.SelectedValue == "2" || ddlModalidad.SelectedValue == "3" || ddlModalidad.SelectedValue == "4") //20200803 res 138
                //        //{
                //        try
                //        {
                //            string[] lsNombreParametros = { "@P_codigo_operador", "@P_codigo_punto", "@P_fecha_ini", "@P_fecha_fin", "@P_cantidad", "@P_codigo_modalidad", "@P_numero_contrato" };
                //            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                //            string[] lValorParametros = { hdfOPeraV.Value, ddlPuntoEntr.SelectedValue, TxtFechaInicial.Text, TxtFechaFinal.Text, TxtCantidad.Text, ddlModalidad.SelectedValue, lblOperacion.Text };

                //            lConexion.Abrir();
                //            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetValidaPtdvf", lsNombreParametros, lTipoparametros, lValorParametros);
                //            if (lLector.HasRows)
                //            {
                //                while (lLector.Read())
                //                    lsError += lLector["error"] + "<br>";
                //            }
                //            lLector.Close();
                //            lConexion.Cerrar();
                //        }
                //        catch (Exception)
                //        {
                //            lLector.Close();
                //            lConexion.Cerrar();
                //        }
                //        //} //20200803 res 138
                //    }
                //}
                //20170425 rq011-17
                //if (HdfDestinoRueda == "G" && ddlPuntoEntr.SelectedValue != "0")  //20170814 rq036-17
                //{
                //    //vaida que el punto de entrega este parametizado para a subasta
                //    if (!DelegadaBase.Servicios.ValidarExistencia("m_caracteristica_sub ", " codigo_tipo_subasta =" + hdfCodTipoSub.Value + " and tipo_caracteristica ='T' and destino_rueda ='G' and estado ='A' and tipo_mercado ='" + HdfTipoMerc + "' and codigo_caracteristica = " + ddlPuntoEntr.SelectedValue, goInfo))
                //        lsError += "El punto de entrega no está habilitado para la subasta-mercado. <br>";
                //}
                //fin 20170425 rq011-17
                //20170814 rq036-17
                if (trSum.Visible)
                {
                    if (ddlConecSnt.SelectedValue == "N" && ddlEntBocPozo.SelectedValue == "N" && ddlCentro.SelectedValue == "0")
                        lsError += "Debe seleccionar el centro poblado. <br>";
                }
                //validaciones rq026-15 20171130
                if (ddlPeriodo.SelectedValue == "0")
                    lsError += "Debe seleccionar el periodo de entrega. <br>";
                string[] lsNombreParametrosVal = { "@P_numero_contrato", "@P_contrato_definitivo", "@P_fecha_inicial", "@P_fecha_final", "@P_ind_contrato_var", "@P_codigo_periodo", "@P_fecha_suscripcion", "@P_codigo_modalidad", "@P_codigo_fuente", "@P_cantidad", "@P_clasificacion_transp", "@P_valida", "@P_hora_inicial", "@P_hora_final", "@P_contratos_ms_tra" }; //20190404 rq018-19 fase II //20201224 //20210224 //20210308 //20210305 broker
                SqlDbType[] lTipoparametrosVal = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar }; //20190404 rq018-19 fase II //20201224 //20210224 //20210308//20210305 broker
                string[] lValorParametrosVal = { lblOperacion.Text, TxtNumContrato.Text, TxtFechaInicial.Text, TxtFechaFinal.Text, ddlIndVar.SelectedValue, ddlPeriodo.SelectedValue, TxtFechaSus.Text, ddlModalidad.SelectedValue, ddlFuente.SelectedValue, TxtCantidad.Text, DdlClasifica.SelectedValue, "S", TxtHoraInicial.Text, TxtHoraFinal.Text, TxtIdTraMp.Text }; //20190404 rq018-19 fase II //20201224 //20210224 //20210308//20210305 broker
                string lsErrror = "N"; //20201124
                //20201124
                if (lsError != "")
                    lsErrror = "S";
                string lsConfirma = "N"; //20201124
                lblValidacion.Text = "";//20201124
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_ValidaRegSist", lsNombreParametrosVal, lTipoparametrosVal, lValorParametrosVal, goInfo);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                    {//20201124
                        if (lLector["ind_error"].ToString() == "S")
                        {
                            lsError += lLector["error"] + "<br>";
                            lsErrror = "S";
                        }
                        else
                        {
                            lblValidacion.Text += lLector["error"] + "<br>";
                            lsConfirma = "S";
                        }
                    }
                    //Se él modal de confirmación
                    //20210224
                    if (lsErrror == "N" && lsConfirma == "S")
                        ModalEvent?.Invoke(mdlConfirma.ID, mdlConfirmaInside.ID, EnumTypeModal.Abrir, Modal.Size.Normal);

                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                //fin validaciones rq026-15 20171130
                //20210707 trm moneda
                if (ddlTrm.SelectedValue == "0")
                    lsError += "Debe seleccionar la tasa de cambio";
                if (TxtObsTrm.Text == "" && TxtObsTrm.Enabled)
                    lsError += "Debe digitar las observaciones de la tasa de cambio";
                //20210707 fin trm moneda
                return lsError;
            }
            catch (Exception ex)
            {
                if (lsError == "")
                    return "Problemas en la Validación de la Información. " + ex.Message;
                else
                    return lsError;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20210224
        protected void BtnAceptarConf_Click(object sender, EventArgs e)
        {
            ModalEvent?.Invoke(mdlConfirma.ID, mdlConfirmaInside.ID, EnumTypeModal.Cerrar);
            lblValidacion.Text = "";
            HdfValida = "N";
            if (hdfAccion.Value == "C")
                btnCrear_Click(null, null);
            else
                btnActualizar_Click(null, null);

        }

        /// <summary>
        /// Realiza la Limpieza de los Campo de Captura en la Pantalla luego de la Creación o Actualizacion
        /// </summary>
        protected void LimpiarCampos()
        {
            TxtNumContrato.Text = "";
            TxtFechaSus.Text = "";
            ddlModalidad.SelectedValue = "0";
            ChkOtrModa.Checked = false;
            TxtOtrModalidad.Text = "";
            ddlPuntoEntr.SelectedValue = "0";
            ChkOtrPuntoE.Checked = false;
            TxtOtrPuntoE.Text = "";
            TxtCantidad.Text = "";
            TxtPrecio.Text = "";
            TxtFechaInicial.Text = "";
            TxtFechaFinal.Text = "";
            //dlTipoDemanda.SelectedValue = "0";
            ddlSentidoFlujo.SelectedValue = "";
            TxtPresion.Text = "";
            ddlFuente.SelectedValue = "0"; // campo o fuente MP 20160601
        }

        /// <summary>
        /// Nombre: dtgUsuarios_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgUsuarios_EditCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals("Modificar"))
            {
                try
                {
                    ////////////////////////////////////////////////////////////////////////////////////////
                    /// OJO Cambios de los Indices por adicion de Campo Req. 009-17 Indicadores 20170321 ///
                    ////////////////////////////////////////////////////////////////////////////////////////
                    var lsTipoDoc = "";
                    if (dtgUsuarios.Items[e.Item.ItemIndex].Cells[14].Text == "1")  //rq009-17
                        lsTipoDoc = "Nit";
                    else
                        lsTipoDoc = "Cedula";
                    hdfCodDatUsu.Value = dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text; //rq009-17
                                                                                             //Ajuste para que no muestre infrmacion mala 20160621
                    if (dtgUsuarios.Items[e.Item.ItemIndex].Cells[1].Text == "&nbsp;")
                        TxtUsuarioFinal.Text = lsTipoDoc;
                    else
                        TxtUsuarioFinal.Text = dtgUsuarios.Items[e.Item.ItemIndex].Cells[1].Text + "-" + lsTipoDoc + "-" + dtgUsuarios.Items[e.Item.ItemIndex].Cells[3].Text;
                    TxtCantidadUsu.Text = dtgUsuarios.Items[e.Item.ItemIndex].Cells[6].Text;
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) - Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                    dlTipoDemanda.SelectedValue = dtgUsuarios.Items[e.Item.ItemIndex].Cells[15].Text; //rq009-17
                    dlTipoDemanda_SelectedIndexChanged(null, null);
                    ddlSector.SelectedValue = dtgUsuarios.Items[e.Item.ItemIndex].Cells[12].Text; //rq009-17
                    ddlMercadoRel.SelectedValue = dtgUsuarios.Items[e.Item.ItemIndex].Cells[16].Text; //rq009-17
                    ddlPuntoSalida.SelectedValue = dtgUsuarios.Items[e.Item.ItemIndex].Cells[13].Text; //rq009-17
                    btnCrearUsu.Visible = false;
                    btnActualUsu.Visible = true;
                    /////////////////////////////////////////////////////////////////////////
                    /// Habilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///  
                    /////////////////////////////////////////////////////////////////////////
                    if (HdfDestinoRueda == "G" && HdfTipoMerc == "P")
                    {
                        TxtEquivaleKpcd.Text = dtgUsuarios.Items[e.Item.ItemIndex].Cells[7].Text;
                        TdIndica.Visible = true;
                        //TdIndica1.Visible = true;
                    }
                    else
                    {
                        TdIndica.Visible = false;
                        //TdIndica1.Visible = false;
                    }
                    /////////////////////////////////////////////////////////////////////////
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke($"Problemas al Recuperar el Registro.! {ex.Message}", EnumTypeToastr.Error);
                }

            }
            if (e.CommandName.Equals("Eliminar"))
            {
                string[] lsNombreParametros = { "@P_codigo_datos_usuarios", "@P_codigo_cont_datos", "@P_no_identificacion_usr", "@P_codigo_tipo_doc",
                    "@P_nombre_usuario", "@P_codigo_sector_consumo", "@P_codigo_punto_salida","@P_cantidad_contratada","@P_accion", "@P_numero_operacion" };//20210503 ajsutes usuarios
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                    SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int}; //20210503 ajsutes usuarios
                string[] lValorParametros = { hdfCodDatUsu.Value, "0", "", "", "", "0", "0", " 0", "3", lblOperacion.Text }; //20210503 ajsutes usuarios

                try
                {
                    ////////////////////////////////////////////////////////////////////////////////////////
                    /// OJO Cambios de los Indices por adicion de Campo Req. 009-17 Indicadores 20170321 ///
                    ////////////////////////////////////////////////////////////////////////////////////////
                    hdfCodDatUsu.Value = dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text;
                    lValorParametros[0] = hdfCodDatUsu.Value;
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetUsuFinalCont", lsNombreParametros, lTipoparametros, lValorParametros, goInfo); //20210503 ajsutes usuarios
                    if (lLector.HasRows)//20210503 ajsutes usuarios
                    {
                        while (lLector.Read())
                            ToastrEvent?.Invoke(lLector["error"].ToString(), EnumTypeToastr.Error);//20210503 ajsutes usuarios
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) - Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                        lConexion.Cerrar();
                        ToastrEvent?.Invoke("Registro Eliminado Correctamente.!", EnumTypeToastr.Success);
                        CargarDatosUsu();
                    }
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke($"Problemas al Eliminar el Registro.! {ex.Message}", EnumTypeToastr.Error);
                }
            }
        }

        /// <summary>
        /// Nombre: CargarDatosUsu
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid de Usuarios.
        /// Modificacion:
        /// </summary>
        private void CargarDatosUsu()
        {
            string[] lsNombreParametros = { "@P_codigo_cont_datos", "@P_no_identificacion_usr", "@P_login" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "-1", "", "" };
            try
            {
                if (hdfCodVerifCont.Value.Trim().Length > 0)
                    lValorParametros[0] = hdfCodVerifCont.Value;
                if (Session["tipoPerfil"].ToString() == "N")
                    lValorParametros[2] = goInfo.Usuario;
                lConexion.Abrir();
                dtgUsuarios.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRegistroContratosUsu", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgUsuarios.DataBind();
                lConexion.Cerrar();
                lblTotlCantidad.Text = "0";
                if (dtgUsuarios.Items.Count > 0)
                {
                    foreach (DataGridItem Grilla in dtgUsuarios.Items)
                    {
                        lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(Grilla.Cells[6].Text.Trim())).ToString();
                    }
                    /////////////////////////////////////////////////////////////////////////
                    /// Habilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///
                    /////////////////////////////////////////////////////////////////////////
                    if (HdfDestinoRueda == "G" && HdfTipoMerc == "P")
                        dtgUsuarios.Columns[7].Visible = true;
                    else
                        dtgUsuarios.Columns[7].Visible = false;
                    /////////////////////////////////////////////////////////////////////////
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke($"No se Pudo Generar el Informe.! {ex.Message}", EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Realiza la actualizacion del estado y envia la Alerta para solicitar correccion de los datos del Registro.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSolCorr_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_estado", "@P_observacion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char, SqlDbType.VarChar };
            String[] lValorParametros = { hdfIdVerif.Value, "E", TxtObservacion.Text };

            try
            {
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetActualVerifDatosReg", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    ToastrEvent?.Invoke($"Se presentó un Problema en la Actualización de la Información del Usuario.! {goInfo.mensaje_error}", EnumTypeToastr.Error);
                    lConexion.Cerrar();
                }
                else
                {
                    divDemanda.Visible = false;
                    var lsAsunto = "";
                    var lsMensaje = "";
                    var lsMensajeC = "";
                    var lsMailC = "";
                    var lsNomOperadorC = "";
                    var lsMailV = "";
                    var lsNomOperadorV = "";
                    /// Obtengo el mail del Operador Compra
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraC.Value + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsMailC = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" + lLector["e_mail3"];  //20180228 rq032-17
                        if (lLector["tipo_persona"].ToString() == "J")
                            lsNomOperadorC = lLector["razon_social"].ToString();
                        else
                            lsNomOperadorC = lLector["nombres"] + " " + lLector["apellidos"];
                    }
                    lLector.Close();
                    lLector.Dispose();
                    /// Obtengo el mail del Operador Venta
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraV.Value + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsMailV = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" + lLector["e_mail2"]; // 20180228 rq032-17
                        if (lLector["tipo_persona"].ToString() == "J")
                            lsNomOperadorV = lLector["razon_social"].ToString();
                        else
                            lsNomOperadorV = lLector["nombres"] + " " + lLector["apellidos"];
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    ///// Envio del Mail de la Solicitud de la Correcion
                    lsAsunto = "Notificación Solicitud Corrección Registro y Verificación";
                    lsMensaje += "Nos permitimos  informarle que el Administrador del SEGAS acaba de Solicitar la Corrección de la Información transaccional de la Operación No: " + lblOperacion.Text + " con número de contrato: " + TxtNumContrato.Text + ". Por favor revisar los siguientes campos: <br><br>"; //20220221 registro extemporane
                    if (lblDat1Ver.Text != "Correcta")
                        lsMensaje += " - " + lblTit1.Text + "<br>";
                    if (lblDat2Ver.Text != "Correcta")
                        lsMensaje += " - " + lblTit2.Text + "<br>";
                    if (lblDat3Ver.Text != "Correcta")
                        lsMensaje += " - " + lblTit3.Text + "<br>";
                    if (lblDat4Ver.Text != "Correcta")
                        lsMensaje += " - " + lblTit4.Text + "<br>";
                    if (lblDat6Ver.Text != "Correcta")
                        lsMensaje += " - " + lblTit5.Text + "<br>";
                    if (lblDat6Ver.Text != "Correcta")
                        lsMensaje += " - " + lblTit6.Text + "<br>";
                    if (lblDat7Ver.Text != "Correcta")
                        lsMensaje += " - " + lblTit7.Text + "<br>";
                    if (lblDat8Ver.Text != "Correcta")
                        lsMensaje += " - " + lblTit8.Text + "<br>";
                    if (HdfDestinoRueda == "T" && HdfTipoMerc == "P")
                    {
                        if (lblDat9Ver.Text != "Correcta")
                            lsMensaje += " - " + lblTit9.Text + "<br>";
                        if (lblDat10Ver.Text != "Correcta")
                            lsMensaje += " - " + lblTit10.Text + "<br>";
                    }
                    lsMensaje += "<br><br>Observaciones: " + TxtObservacion.Text + "<br><br>";
                    lsMensaje += "Cordialmente, <br><br><br>";
                    lsMensaje += "Administrador SEGAS <br>";
                    lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                    var mailC = new clEmail(lsMailC, lsAsunto, lsMensaje, "");
                    var error1 = mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas");
                    if (!string.IsNullOrEmpty(error1))
                        ToastrEvent?.Invoke(error1, EnumTypeToastr.Error);
                    ////
                    lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                    var mailV = new clEmail(lsMailV, lsAsunto, lsMensaje, "");
                    var error2 = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas");
                    if (!string.IsNullOrEmpty(error2))
                        ToastrEvent?.Invoke(error2, EnumTypeToastr.Error);

                    ToastrEvent?.Invoke("Registro Actualizado Correctamente.!", EnumTypeToastr.Success);
                    CargarDatos();
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke($"Problemas en la Actualización de la Información. {ex.Message}", EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRegCont_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_estado", "@P_observacion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char, SqlDbType.VarChar };
            String[] lValorParametros = { hdfIdVerif.Value, "R", TxtObservacion.Text };

            try
            {
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetActualVerifDatosReg", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    ToastrEvent?.Invoke($"Se presentó un Problema en la Actualización de la Información del Usuario.! {goInfo.mensaje_error}", EnumTypeToastr.Error);
                    lConexion.Cerrar();
                }
                else
                {
                    ToastrEvent?.Invoke("Registro Actualizado Correctamente.!", EnumTypeToastr.Success);
                    divDemanda.Visible = false;
                    var lsAsunto = "";
                    var lsMensaje = "";
                    var lsMensajeC = "";
                    var lsMailC = "";
                    var lsNomOperadorC = "";
                    var lsMailV = "";
                    var lsNomOperadorV = "";
                    /// Obtengo el mail del Operador Compra
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraC.Value + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsMailC = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" + lLector["e_mail2"]; // 20180228 rq032-17
                        if (lLector["tipo_persona"].ToString() == "J")
                            lsNomOperadorC = lLector["razon_social"].ToString();
                        else
                            lsNomOperadorC = lLector["nombres"] + " " + lLector["apellidos"];
                    }
                    lLector.Close();
                    lLector.Dispose();
                    /// Obtengo el mail del Operador Venta
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraV.Value + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsMailV = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" + lLector["e_mail2"]; // 20180228 rq032-17
                        if (lLector["tipo_persona"].ToString() == "J")
                            lsNomOperadorV = lLector["razon_social"].ToString();
                        else
                            lsNomOperadorV = lLector["nombres"] + " " + lLector["apellidos"];
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    ///// Envio del Mail de la Solicitud de la Correcion
                    lsAsunto = "Notificación Registro Contrato";
                    lsMensaje += "Nos permitimos informarle que el Administrador de la plataforma SEGAS acaba de realizar exitosamente el Registro del Contrato de la Operación No: " + lblOperacion.Text + " con número de contrato: " + TxtNumContrato.Text + " y con No. de Registro: " + hdfIdVerif.Value + ".<br><br>"; //20220221 registro extemporaneo
                    lsMensaje += "<br><br>Observaciones: " + TxtObservacion.Text + "<br><br>";
                    lsMensaje += "Cordialmente, <br><br><br>";
                    lsMensaje += "Administrador SEGAS <br>";
                    lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                    var mailC = new clEmail(lsMailC, lsAsunto, lsMensaje, "");
                    var error1 = mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas");
                    if (!string.IsNullOrEmpty(error1))
                        ToastrEvent?.Invoke(error1, EnumTypeToastr.Error);
                    ////
                    lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                    var mailV = new clEmail(lsMailV, lsAsunto, lsMensaje, "");
                    var error2 = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas");
                    if (!string.IsNullOrEmpty(error2))
                        ToastrEvent?.Invoke(error2, EnumTypeToastr.Error);
                    CargarDatos();
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke($"Problemas en la Actualización de la Información. {ex.Message}", EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImmSalirVer_Click(object sender, ImageClickEventArgs e)
        {
            divDemanda.Visible = false;
            CargarDatos();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dlTipoDemanda_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hdfTipoDemanda.Value = dlTipoDemanda.SelectedValue;
                if (hdfTipoDemanda.Value == "1")
                {
                    TxtUsuarioFinal.Enabled = false;
                    ddlMercadoRel.Enabled = true; //20160303
                    lblSector.Text = "Sector de Consumo Usuario Regulado";
                    if (HdfDestinoRueda == "G")
                        lblCantContra.Text = "Cantidad a entregar";
                    else
                        lblCantContra.Text = "Capacidad Contratada";
                }
                else
                {
                    TxtUsuarioFinal.Enabled = true;
                    ddlMercadoRel.Enabled = false; //20160303
                    ddlMercadoRel.SelectedValue = "0"; //20160303
                    if (HdfDestinoRueda == "G")
                    {
                        lblCantContra.Text = "Cantidad Contratada por usuario (MBTUD)";
                        lblSector.Text = "Sector Consumo Usuario No Regulado";
                    }
                    else
                    {
                        lblSector.Text = "Sector de Consumo Transporte Usuario No Regulado";
                        lblCantContra.Text = "Capacidad Contratada por usuario (KPCD)";
                    }
                }
                lConexion.Abrir();
                ddlSector.Items.Clear();
                ddlPuntoSalida.Items.Clear();
                LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo sec, m_demanda_sector dem", " sec.estado ='A' and sec.codigo_sector_consumo = dem.codigo_sector_consumo  and dem.estado ='A' and dem.codigo_tipo_demanda =" + dlTipoDemanda.SelectedValue + "  and dem.ind_uso ='T' order by descripcion", 0, 1); //20160706
                LlenarControles(lConexion.gObjConexion, ddlPuntoSalida, "m_punto_salida_snt", " estado = 'A'  order by descripcion", 0, 2);
                lConexion.Cerrar();
            }
            catch (Exception)
            {

            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCrearUsu_Click(object sender, EventArgs e)
        {
            var valido = true;

            string[] lsNombreParametros = { "@P_codigo_datos_usuarios", "@P_codigo_cont_datos", "@P_no_identificacion_usr", "@P_codigo_tipo_doc",
                "@P_nombre_usuario", "@P_codigo_sector_consumo", "@P_codigo_punto_salida","@P_cantidad_contratada","@P_accion","@P_destino_rueda", "@P_tipo_demanda" ,
                "@P_codigo_mercado_relevante",  // Se agrrega campo para mercado relevante-sector consumo 20160126
                "@P_equivalente_kpcd", "@P_numero_operacion"// Campo nuevo Req. 009-17 Indicasdores 20170321 //20210503 ajsute usuarios
            };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int,
                SqlDbType.Int,SqlDbType.Int // Campo nuevo Req. 009-17 Indicasdores 20170321 //20210503 ajsute usuarios
            };
            string[] lValorParametros = { "0", "0", "", "", "", "0", "0", " 0", "1", HdfDestinoRueda, dlTipoDemanda.SelectedValue, ddlMercadoRel.SelectedValue,
                "0",lblOperacion.Text    // Campo nuevo Req. 009-17 Indicasdores 20170321 //20210503 ajsute usuarios
            };

            string[] lsUsuario;
            var lsNombre = "";
            var lsTipoDoc = "";
            var liValor = 0;
            try
            {
                lConexion.Abrir();
                lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
                if (dlTipoDemanda.SelectedValue == "0")
                    ToastrEvent?.Invoke("Debe Seleccionar Tipo Demanda Atender.", EnumTypeToastr.Error);
                else
                {
                    if (dlTipoDemanda.SelectedValue == "2")
                    {
                        if (TxtUsuarioFinal.Text.Trim().Length <= 0)
                        {
                            ToastrEvent?.Invoke("Debe Ingresar el Usuario Final", EnumTypeToastr.Error);
                            valido = false;
                        }
                        else
                        {
                            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                            if (!lLector.HasRows)
                            {
                                ToastrEvent?.Invoke("El Usuario Ingresado NO existe en la Base de Datos.!", EnumTypeToastr.Error);
                                valido = false;
                            }
                            else
                            {
                                lLector.Read();
                                lsNombre = lLector["nombre"].ToString();
                                lsTipoDoc = lLector["codigo_tipo_doc"].ToString();
                            }
                            lLector.Close();
                            lLector.Dispose();
                        }
                    }
                    else //20160303
                    {
                        if (ddlMercadoRel.SelectedValue == "0")
                        {
                            ToastrEvent?.Invoke("Debe Seleccionar el mercado relevante.", EnumTypeToastr.Error);
                            valido = false;
                        }
                    }
                }

                if (ddlSector.SelectedValue == "0")
                {
                    ToastrEvent?.Invoke($"Debe Seleccionar {lblSector.Text}", EnumTypeToastr.Error);
                    valido = false;
                }

                if (ddlPuntoSalida.SelectedValue == "0")
                {
                    ToastrEvent?.Invoke("Debe Seleccionar  Punto de Salida en SNT.", EnumTypeToastr.Error);
                    valido = false;
                }

                if (TxtCantidadUsu.Text.Trim().Length <= 0)
                {
                    ToastrEvent?.Invoke($"Debe Ingresar {lblCantContra.Text}.", EnumTypeToastr.Error);
                    valido = false;
                }
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidadUsu.Text.Trim());
                        if (liValor <= 0)
                        {
                            ToastrEvent?.Invoke($"{lblCantContra.Text} debe ser mayor que cero.", EnumTypeToastr.Error);
                            valido = false;
                        }
                        else
                        {
                            if (liValor > Convert.ToDecimal(TxtCantidad.Text.Trim()))
                            {
                                ToastrEvent?.Invoke($"La {lblCantContra.Text} No puede ser Mayor que {lblCantidad.Text}.", EnumTypeToastr.Error);
                                valido = false;
                            }
                            else
                            {
                                if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(TxtCantidad.Text))
                                {
                                    ToastrEvent?.Invoke($"El acumulado de Usuarios No puede ser Mayor que {lblCantidad.Text}.", EnumTypeToastr.Error);
                                    valido = false;
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        ToastrEvent?.Invoke($"Valor Inválido {lblCantContra.Text}", EnumTypeToastr.Error);
                        valido = false;
                    }
                }
                /////////////////////////////////////////////////////////////////////////
                /// Habilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///
                /////////////////////////////////////////////////////////////////////////
                if (HdfDestinoRueda == "G" && HdfTipoMerc == "P")
                {
                    if (TxtEquivaleKpcd.Text.Trim().Length <= 0)
                    {
                        ToastrEvent?.Invoke("Debe Ingresar Equivalente Kpcd.", EnumTypeToastr.Error);
                        valido = false;
                    }
                    else
                    {
                        try
                        {
                            liValor = Convert.ToInt32(TxtEquivaleKpcd.Text.Trim());
                            if (liValor < 0)
                            {
                                ToastrEvent?.Invoke("Valor Inválido Equivalente Kpcd.", EnumTypeToastr.Error);
                                valido = false;
                            }
                        }
                        catch (Exception)
                        {
                            ToastrEvent?.Invoke("Valor Inválido " + lblCantContra.Text + ".", EnumTypeToastr.Error);
                            valido = false;
                        }
                    }
                }
                //20170814 rq036-17
                //20170814 rq036-17
                //20170814 rq036-17
                //20170814 rq036-17
                //20170814 rq036-17

                if (valido)
                {
                    if (hdfCodVerifCont.Value != "")
                        lValorParametros[1] = hdfCodVerifCont.Value;
                    if (hdfTipoDemanda.Value == "2")
                    {
                        lValorParametros[2] = lsUsuario[0].Trim();
                        lValorParametros[3] = lsTipoDoc;
                        lValorParametros[4] = lsNombre;
                    }
                    lValorParametros[5] = ddlSector.SelectedValue;
                    lValorParametros[6] = ddlPuntoSalida.SelectedValue;
                    lValorParametros[7] = TxtCantidadUsu.Text.Trim();
                    /////////////////////////////////////////////////////////////////////////
                    /// Habilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///
                    /////////////////////////////////////////////////////////////////////////
                    if (HdfDestinoRueda == "G" && HdfTipoMerc == "P")
                        lValorParametros[12] = TxtEquivaleKpcd.Text.Trim();
                    /////////////////////////////////////////////////////////////////////////
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetUsuFinalCont", lsNombreParametros, lTipoparametros, lValorParametros, goInfo); //20210503 ajuste suuarios
                    if (lLector.HasRows)//20210503 ajuste suuarios
                    {
                        while (lLector.Read())//20210503 ajuste suuarios
                            ToastrEvent?.Invoke(lLector["Error"].ToString(), EnumTypeToastr.Error);//20210503 ajuste suuarios
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(TxtCantidadUsu.Text.Trim())).ToString();
                        ToastrEvent?.Invoke("Información de Usuarios Finales Ingresada Correctamente.!", EnumTypeToastr.Success);
                        TxtUsuarioFinal.Text = "";
                        ddlSector.SelectedValue = "0";
                        ddlPuntoSalida.SelectedValue = "0";
                        ddlMercadoRel.SelectedValue = "0";
                        TxtCantidadUsu.Text = "0";
                        TxtEquivaleKpcd.Text = "0"; // Campos nuevos Req. 009-17 Indicadores 20170321
                        CargarDatosUsu();
                    }
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnActualUsu_Click(object sender, EventArgs e)
        {
            var valido = true;

            string[] lsNombreParametros = { "@P_codigo_datos_usuarios", "@P_codigo_cont_datos", "@P_no_identificacion_usr", "@P_codigo_tipo_doc",
                "@P_nombre_usuario", "@P_codigo_sector_consumo", "@P_codigo_punto_salida","@P_cantidad_contratada","@P_accion","@P_destino_rueda", "@P_tipo_demanda" ,//20171130 rq026-17
                "@P_codigo_mercado_relevante", // Se agrrega campo para mercado relevante-sector consumo 20160126
                "@P_equivalente_kpcd", "@P_numero_operacion" // Campo nuevo Req. 009-17 Indicasdores 20170321 //20210503 ajsute usaurios
            };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int,
                SqlDbType.Int, SqlDbType.Int // Campo nuevo Req. 009-17 Indicasdores 20170321//20210503 ajsute usaurios
            };
            string[] lValorParametros = { hdfCodDatUsu.Value, "0", "", "", "", "0", "0", " 0", "2", HdfDestinoRueda, dlTipoDemanda.SelectedValue, ddlMercadoRel.SelectedValue ,
                "0",lblOperacion.Text     // Campo nuevo Req. 009-17 Indicasdores 20170321 //20210503 ajsute usaurios
            };
            string[] lsUsuario;
            var liValor = 0;
            var lsNombre = "";
            var lsTipoDoc = "";
            try
            {
                lConexion.Abrir();
                lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
                if (dlTipoDemanda.SelectedValue == "0")
                {
                    ToastrEvent?.Invoke("Debe Seleccionar  Tipo Demanda Atender.", EnumTypeToastr.Error);
                    valido = false;
                }
                else
                {
                    if (hdfTipoDemanda.Value == "2")
                    {
                        if (TxtUsuarioFinal.Text.Trim().Length <= 0)
                        {
                            ToastrEvent?.Invoke("Debe Ingresar el Usuario Final", EnumTypeToastr.Error);
                            valido = false;
                        }
                        else
                        {
                            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                            if (!lLector.HasRows)
                            {
                                ToastrEvent?.Invoke("El Usuario Ingresado NO existe en la Base de Datos.!", EnumTypeToastr.Error);
                                valido = false;
                            }
                            else
                            {
                                lLector.Read();
                                lsNombre = lLector["nombre"].ToString();
                                lsTipoDoc = lLector["codigo_tipo_doc"].ToString();
                            }
                            lLector.Close();
                            lLector.Dispose();
                        }
                    }
                    else //20160303
                    {
                        if (ddlMercadoRel.SelectedValue == "0")
                        {
                            ToastrEvent?.Invoke("Debe Seleccionar el mercado relevante.", EnumTypeToastr.Error);
                            valido = false;
                        }
                    }

                }
                if (ddlSector.SelectedValue == "0")
                {
                    ToastrEvent?.Invoke("Debe Seleccionar " + lblSector.Text + ".", EnumTypeToastr.Error);
                    valido = false;
                }
                if (ddlPuntoSalida.SelectedValue == "0")
                {
                    ToastrEvent?.Invoke("Debe Seleccionar  Punto de Salida en SNT.", EnumTypeToastr.Error);
                    valido = false;
                }
                if (TxtCantidadUsu.Text.Trim().Length <= 0)
                {
                    ToastrEvent?.Invoke("Debe Ingresar " + lblCantContra.Text + ".", EnumTypeToastr.Error);
                    valido = false;
                }
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidadUsu.Text.Trim());
                        if (liValor <= 0)
                        {
                            ToastrEvent?.Invoke(lblCantContra.Text + " debe ser mayor que cero.", EnumTypeToastr.Error);
                            valido = false;
                        }
                        else
                        {
                            if (liValor > Convert.ToDecimal(TxtCantidad.Text.Trim()))
                            {
                                ToastrEvent?.Invoke("La " + lblCantContra.Text + " No puede ser Mayor que " + lblCantidad.Text + ".", EnumTypeToastr.Error);
                                valido = false;
                            }
                            else
                            {
                                if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(TxtCantidad.Text))
                                {
                                    ToastrEvent?.Invoke("El Acumulado de Usuarios No puede ser Mayor que " + lblCantidad.Text + ".", EnumTypeToastr.Error); //20180126 rq107-16
                                    valido = false;
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        ToastrEvent?.Invoke("Valor Inválido " + lblCantContra.Text + ".", EnumTypeToastr.Error);
                        valido = false;
                    }
                }
                /////////////////////////////////////////////////////////////////////////
                /// Habilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///
                /////////////////////////////////////////////////////////////////////////
                if (HdfDestinoRueda == "G" && HdfTipoMerc == "P")
                {
                    if (TxtEquivaleKpcd.Text.Trim().Length <= 0)
                    {
                        ToastrEvent?.Invoke("Debe Ingresar Equivalente Kpcd.", EnumTypeToastr.Error);
                        valido = false;
                    }
                    else
                    {
                        try
                        {
                            liValor = Convert.ToInt32(TxtEquivaleKpcd.Text.Trim());
                            if (liValor < 0)
                            {
                                ToastrEvent?.Invoke("Valor Inválido Equivalente Kpcd.", EnumTypeToastr.Error);
                                valido = false;
                            }
                        }
                        catch (Exception)
                        {
                            ToastrEvent?.Invoke("Valor Inválido " + lblCantContra.Text + ".", EnumTypeToastr.Error);
                            valido = false;
                        }
                    }
                }
                //20170814 rq036-17
                //20170814 rq036-17
                //20170814 rq036-17
                //20170814 rq036-17
                //20170814 rq036-17

                if (valido)
                {
                    if (hdfCodVerifCont.Value.Trim().Length > 0)
                        lValorParametros[1] = hdfCodVerifCont.Value;
                    if (hdfTipoDemanda.Value == "2")
                    {
                        lValorParametros[2] = lsUsuario[0].Trim();
                        lValorParametros[3] = lsTipoDoc;
                        lValorParametros[4] = lsNombre;
                    }
                    lValorParametros[5] = ddlSector.SelectedValue;
                    lValorParametros[6] = ddlPuntoSalida.SelectedValue;
                    lValorParametros[7] = TxtCantidadUsu.Text.Trim();
                    /////////////////////////////////////////////////////////////////////////
                    /// Habilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///
                    /////////////////////////////////////////////////////////////////////////
                    if (HdfDestinoRueda == "G" && HdfTipoMerc == "P")
                        lValorParametros[12] = TxtEquivaleKpcd.Text.Trim();
                    /////////////////////////////////////////////////////////////////////////
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetUsuFinalCont", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);//20210503 ajsute usuarios
                    if (lLector.HasRows)//20210503 ajsute usuarios
                    {
                        while (lLector.Read())//20210503 ajsute usuarios
                            ToastrEvent?.Invoke(lLector["error"].ToString(), EnumTypeToastr.Error);//20210503 ajsute usuarios
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                        ToastrEvent?.Invoke("Información de Usuarios Finales Actualizada Correctamente.!", EnumTypeToastr.Success);
                        TxtUsuarioFinal.Text = "";
                        ddlSector.SelectedValue = "0";
                        ddlPuntoSalida.SelectedValue = "0";
                        ddlMercadoRel.SelectedValue = "0";
                        TxtCantidadUsu.Text = "0";
                        TxtEquivaleKpcd.Text = "0"; // Campos nuevos Req. 009-17 Indicadores 20170321
                        btnCrearUsu.Visible = true;
                        btnActualUsu.Visible = false;
                        CargarDatosUsu();
                    }
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPuntoEntr_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (hdfPunta.Value == "C")
            {
                lConexion.Abrir();
                //ddlMercadoRelev.Items.Clear();
                ddlPuntoSalida.Items.Clear();
                //LlenarControles(lConexion.gObjConexion, ddlMercadoRelev, "m_mercado_relevante", " estado = 'A' And destino_rueda = '" + HdfDestinoRueda + "' order by descripcion", 0, 2);
                //if (HdfDestinoRueda == "G")
                //    LlenarControles(lConexion.gObjConexion, ddlPuntoSalida, "m_pozo", " estado = 'A'  order by descripcion", 0, 1);
                //else
                LlenarControles(lConexion.gObjConexion, ddlPuntoSalida, "m_punto_salida_snt", " estado = 'A' order by descripcion", 0, 2);
                lConexion.Cerrar();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCrear_Click(object sender, EventArgs e)
        {
            var valido = true;

            string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_ind_contrato", "@P_numero_contrato", "@P_contrato_definitivo", "@P_punta_contrato",
                "@P_fecha_suscripcion_cont", "@P_nombre_operador", "@P_codigo_tipo_doc", "@P_no_identificacion_operador", "@P_codigo_modalidad",
                "@P_desc_otro_moda", "@P_codigo_punto_entrega", "@P_desc_otro_punto", "@P_cantidad", "@P_precio", "@P_fecha_inicial",
                "@P_fecha_final", "@P_codigo_tipo_demanda", "@P_sentido_flujo", "@P_presion_punto_fin", "@P_codigo_motivo_modificacion", //20210707
                "@P_codigo_cesionario","@P_estado_act","@P_codigo_tipo_subasta","@P_destino_rueda","@P_accion" ,
                "@P_codigo_fuente", // campo o fuente 20160601
                "@P_conectado_snt", "@P_ent_boca_pozo", "@P_codigo_centro_pob", //20170814 rq036-17
                "@P_codigo_periodo", "@P_ind_contrato_var",//,  //20171130 rq026-17
                                                           //  "@P_maneja_minuta", //2020925 bimestral c1
                                                        "@P_clasifica_transp",         //20201124 
                                                        "@P_contratos_ms_tra", //20210305 broker
            "@P_codigo_trm", "@P_observacion_trm", "@P_tipo_moneda"}; //20210707 trm moneda

            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int,SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar,
                SqlDbType.VarChar,SqlDbType.Int, SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, // Cambio del tipo de datos de la presion del punto 20151006 //20210707
                SqlDbType.Int,SqlDbType.VarChar,SqlDbType.Int,
                SqlDbType.Int, // campo o fuente 20160601
                SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,//20170814 rq036-17
                SqlDbType.Int,SqlDbType.VarChar,//20171130 rq026-17
                                                 // SqlDbType.VarChar ,//2020925 bimestral c1
                                                  SqlDbType.Int,//20201124 
                                                  SqlDbType.VarChar,//20210305 broker
            SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar};//20210707 trm moneda
            string[] lValorParametros = { "0", "S", "0", "0", "", "", "", "", "", "0", "", "0", "", "0", "0", "",  "", "0", "", "0", "0", "0", "V", "0", "0", "1", "0" , // campo o fuente 20160601 //20210707
                "S","N","0",//20170814 rq036-17
                "0","", //20171130 rq026-17 
                         // "N",//2020925 bimestral c1
                          "0",//20201124 
                          TxtIdTraMp.Text,//20210305 broker
            ddlTrm.SelectedValue, TxtObsTrm.Text , ddlMoneda.SelectedValue }; //20210707 trm moneda

            var lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "-" + DateTime.Now.ToShortDateString().Substring(3, 2) + "-" + DateTime.Now.ToShortDateString().Substring(0, 2);
            var sRuta = sRutaArc + lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\";

            var lsMensajeMail = ""; // Campo nuevo requerimiento algoritmo y verificacion 20150529
            var lsEnviaMail = ""; // Campo nuevo requerimiento algoritmo y verificacion 20150529


            try
            {

                var val = ""; //20210224
                //20210224
                if (HdfValida == "S")
                    val = validaciones();
                if (!string.IsNullOrEmpty(val))
                {
                    ToastrEvent?.Invoke(val, EnumTypeToastr.Warning);
                    valido = false;
                }
                //20210224
                if (lblValidacion.Text != "")
                    valido = false;

                //20210707
                //if (FuCopiaCont.FileName != "")
                //{

                //    try
                //    {
                //        Directory.CreateDirectory(sRuta);
                //        FuCopiaCont.SaveAs(sRuta + FuCopiaCont.FileName);
                //    }
                //    catch (Exception ex)
                //    {
                //        ToastrEvent?.Invoke("Problemas en la Carga del Archivo. " + ex.Message, EnumTypeToastr.Error);
                //        lConexion.Cerrar();
                //        valido = false;
                //    }

                //}
                if (valido)
                {
                    lValorParametros[0] = hdfIdVerif.Value;
                    lValorParametros[2] = lblOperacion.Text.Trim();
                    lValorParametros[3] = TxtNumContrato.Text.Trim().ToUpper();
                    lValorParametros[4] = hdfPunta.Value;
                    lValorParametros[5] = TxtFechaSus.Text.Trim();
                    lValorParametros[6] = lblNomOperador.Text.Trim();
                    if (lblTipoDocOperador.Text.Trim() == "Nit")
                        lValorParametros[7] = "1";
                    else
                        lValorParametros[7] = "2";
                    lValorParametros[8] = lblNoDocumentoOperaqdor.Text.Trim();
                    lValorParametros[9] = ddlModalidad.SelectedValue;
                    lValorParametros[10] = TxtOtrModalidad.Text.Trim().ToUpper();
                    lValorParametros[11] = ddlPuntoEntr.SelectedValue;
                    lValorParametros[12] = TxtOtrPuntoE.Text.Trim().ToUpper();
                    lValorParametros[13] = TxtCantidad.Text.Trim();
                    lValorParametros[14] = TxtPrecio.Text.Trim();
                    lValorParametros[15] = TxtFechaInicial.Text.Trim() + " " + TxtHoraInicial.Text;
                    lValorParametros[16] = TxtFechaFinal.Text.Trim() + " " + TxtHoraFinal.Text;
                    //20210707
                    //if (FuCopiaCont.FileName != "")
                    //    lValorParametros[17] = lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\" + FuCopiaCont.FileName;
                    //else
                    //    lValorParametros[17] = "";
                    //20210707 ajsute de items
                    lValorParametros[17] = "0"; // Ajuste requerimiento ajuste registro de contratos 148 - 20150915
                    if (HdfDestinoRueda == "T" && HdfTipoMerc == "P")
                    {
                        lValorParametros[18] = ddlSentidoFlujo.SelectedValue;
                        lValorParametros[19] = TxtPresion.Text.Trim();
                    }
                    lValorParametros[20] = "0"; // Motivo de Modificacion
                    lValorParametros[21] = "0"; // Codigo Cesionario
                    lValorParametros[22] = hdfEstadoAct.Value;
                    lValorParametros[23] = hdfCodTipoSub.Value;
                    lValorParametros[24] = HdfDestinoRueda;
                    lValorParametros[26] = ddlFuente.SelectedValue; // campo o fuente MP 20160601
                                                                    //hdfTipoDemanda.Value = dlTipoDemanda.SelectedValue;
                                                                    //20170814 rq036-17
                    if (trSum.Visible)
                    {
                        lValorParametros[27] = ddlConecSnt.SelectedValue;
                        lValorParametros[28] = ddlEntBocPozo.SelectedValue;
                        lValorParametros[29] = ddlCentro.SelectedValue;
                    }
                    lValorParametros[30] = ddlPeriodo.SelectedValue; //20171130 rq026-17
                    lValorParametros[31] = ddlIndVar.SelectedValue; //20171130 rq026-17
                    lValorParametros[32] = DdlClasifica.SelectedValue; //20201124
                    //20210707 fin ajsute de items
                    SqlTransaction oTransaccion;
                    lConexion.Abrir();
                    oTransaccion = lConexion.gObjConexion.BeginTransaction(IsolationLevel.Serializable);
                    goInfo.mensaje_error = "";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetRegistroContrato", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        ToastrEvent?.Invoke("Se presentó un Problema en la Creación del Registro del Contrato.! ", EnumTypeToastr.Error);
                        oTransaccion.Rollback();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        var lsOperacion = "";
                        var lsIds = "";
                        while (lLector.Read())
                        {
                            hdfCodVerifCont.Value = lLector["codigo_cont_dato"].ToString();
                            lsMensajeMail = lLector["diferencia"].ToString(); // Campo nuevo requerimiento algoritmo y verificacion 20150529
                            lsEnviaMail = lLector["enviaMail"].ToString(); // Campo nuevo requerimiento algoritmo y verificacion 20150529
                            if (lsOperacion == "")
                            {
                                lsOperacion = lLector["operacion"].ToString();
                                lsIds = lLector["codigo_verif"].ToString();
                            }
                            else
                            {
                                lsOperacion += ", " + lLector["operacion"];
                                lsIds += ", " + lLector["codigo_verif"];
                            }
                        }
                        lLector.Close();
                        lLector.Dispose();
                        oTransaccion.Commit();

                        ///// Envio de la alerta al ingresar la informacion por ambas puntas 20150529
                        ///// Requerimiento Algoritmo y Verificacion
                        var lsAsunto = "";
                        var lsMensaje = "";
                        var lsMensajeC = "";
                        var lsMailC = "";
                        var lsNomOperadorC = "";
                        var lsMailV = "";
                        var lsNomOperadorV = "";
                        /// Obtengo el mail del Operador Compra
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraC.Value + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lsMailC = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" + lLector["e_mail2"]; // 20180228 rq032-17
                            if (lLector["tipo_persona"].ToString() == "J")
                                lsNomOperadorC = lLector["razon_social"].ToString();
                            else
                                lsNomOperadorC = lLector["nombres"] + " " + lLector["apellidos"];
                        }
                        lLector.Close();
                        lLector.Dispose();
                        /// Obtengo el mail del Operador Venta
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraV.Value + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lsMailV = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" + lLector["e_mail2"]; // 20180228 rq032-17
                            if (lLector["tipo_persona"].ToString() == "J")
                                lsNomOperadorV = lLector["razon_social"].ToString();
                            else
                                lsNomOperadorV = lLector["nombres"] + " " + lLector["apellidos"];
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Envio de la alerta del ingreso de la información
                        lsAsunto = "Notificación Ingreso Información Contrato";
                        if (hdfPunta.Value == "C")
                            lsMensaje = "Nos permitimos informarle que el operador " + lsNomOperadorC + " acaba de Ingresar satisfactoriamente la información de la Operación No: " + lblOperacion.Text + " con número de contrato: " + TxtNumContrato.Text + ".<br><br>"; //20220221 registro extemporaneo
                        else
                            lsMensaje = "Nos permitimos  informarle que el operador " + lsNomOperadorV + " acaba de Ingresar satisfactoriamente la información de la Operación No: " + lblOperacion.Text + " con número de contrato: " + TxtNumContrato.Text + ".<br><br>"; //20220221 registro extemporaneo
                        lsMensaje += "\n\nObservaciones: Por favor proceda a ingresar la información transaccional e informar a su contraparte para culminar el proceso de registro. <br><br>";
                        lsMensaje += "Cordialmente, <br><br><br>";
                        lsMensaje += "Administrador SEGAS <br>";

                        lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                        var mailC = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                        var error1 = mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas");
                        if (!string.IsNullOrEmpty(error1))
                            ToastrEvent?.Invoke(error1, EnumTypeToastr.Error);
                        ////
                        lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                        var mailV = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                        var error2 = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas");
                        if (!string.IsNullOrEmpty(error2))
                            ToastrEvent?.Invoke(error2, EnumTypeToastr.Error);
                        if (lsEnviaMail == "S")
                        {
                            ///// Envio del Mail de la Solicitud de la Correcion o Registro del Contrato
                            if (lsMensajeMail != "")
                            {
                                lsAsunto = "Notificación Solicitud Corrección Registro y Verificación";
                                lsMensaje = "Nos permitimos informarle que el Administrador de SEGAS acaba de Solicitar la corrección de la información transaccional de la Operación No: " + lblOperacion.Text + " con número de contrato: " + TxtNumContrato.Text + ". Por favor revisar los siguientes campos: <br><br> " + lsMensajeMail + "<br><br>"; //20220221 registro extemporaneo
                                lsMensaje += "Observaciones: Por favor validar los campos objeto de corrección con su contraparte. <br><br>";
                            }
                            else
                            {
                                if (ddlIndVar.SelectedValue == "N")
                                {
                                    lsAsunto = "Notificación Registro Contrato";
                                    lsMensaje = "Nos permitimos informarle que el Administrador de la plataforma SEGAS acaba de realizar exitosamente el Registro del Contrato de la Operación No: " + lblOperacion.Text + " con número de contrato: " + TxtNumContrato.Text + " y con No. de Registro: " + hdfIdVerif.Value + ".<br><br>"; //20220221 registro extemporaneo
                                }
                                else
                                {
                                    if (lsIds != hdfIdVerif.Value)
                                    {
                                        lsAsunto = "Notificación Registro Contrato";
                                        lsMensaje = "Nos permitimos informarle que el Administrador de la plataforma SEGAS acaba de realizar exitosamente el Registro del Contrato de la Operación No: " + lblOperacion.Text + " con número de contrato: " + TxtNumContrato.Text + " y con No. de Registro: " + hdfIdVerif.Value + ".<br><br>"; //20220221 registro extemporaneo
                                        lsMensaje += "Debido a que es un contrato variable quedaron registrados los siguientes Ids de registro: " + lsIds + "<br><br>";
                                    }
                                    else
                                    {
                                        lsAsunto = "Notificación Registro Contrato";
                                        lsMensaje = "Nos permitimos informarle que el Administrador de la plataforma SEGAS acaba de realizar exitosamente el Registro del Contrato de la Operación No: " + lblOperacion.Text + " con número de contrato: " + TxtNumContrato.Text + " y con No. de Registro: " + hdfIdVerif.Value + ".<br><br>"; //20220221 registro extemporaneo
                                        lsMensaje += "Debido a que es un contrato variable sólo quedará en firme cuando se registren todos los contrato asociadosa este.<br><br>";
                                    }
                                }
                            }


                            lsMensaje += "Cordialmente, <br><br>";
                            lsMensaje += "Administrador SEGAS <br>";
                            lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                            var mailC1 = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                            var error3 = mailC1.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas");
                            if (!string.IsNullOrEmpty(error3))
                                ToastrEvent?.Invoke(error3, EnumTypeToastr.Error);
                            ////
                            lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                            var mailV1 = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                            var error4 = mailV1.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas");
                            if (!string.IsNullOrEmpty(error4))
                                ToastrEvent?.Invoke(error4, EnumTypeToastr.Error);
                        }
                        lConexion.Cerrar();
                        ToastrEvent?.Invoke("Registro Ingresado Correctamente.!", EnumTypeToastr.Success);
                        divDemanda.Visible = false;
                        LimpiarCampos();
                        //Se cierra el modal de registro
                        ModalEvent?.Invoke(registroContratoMod.ID, registroContratoModInside.ID, EnumTypeModal.Cerrar);
                        CargarDatos();
                    }
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            var valido = true;

            string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_ind_contrato", "@P_numero_contrato", "@P_contrato_definitivo", "@P_punta_contrato",
                "@P_fecha_suscripcion_cont", "@P_nombre_operador", "@P_codigo_tipo_doc", "@P_no_identificacion_operador", "@P_codigo_modalidad",
                "@P_desc_otro_moda", "@P_codigo_punto_entrega", "@P_desc_otro_punto", "@P_cantidad", "@P_precio", "@P_fecha_inicial",
                "@P_fecha_final","@P_codigo_tipo_demanda", "@P_sentido_flujo", "@P_presion_punto_fin", "@P_codigo_motivo_modificacion", //20210707
                "@P_codigo_cesionario","@P_estado_act","@P_codigo_tipo_subasta","@P_destino_rueda","@P_accion" ,
                "@P_codigo_fuente", //campo o fuente MP 20160601
                "@P_conectado_snt", "@P_ent_boca_pozo", "@P_codigo_centro_pob", //20170814 rq036-17
                "@P_codigo_periodo", "@P_ind_contrato_var",  //20171130 rq026-17
                "@P_clasifica_transp", //20201124
                "@P_contratos_ms_tra", //20210305 broker
                "@P_codigo_trm", "@P_observacion_trm", "@P_tipo_moneda", //20210707 trm moneda
                "@P_observacion_correccion" }; //20220418 correccion  contrato 
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int,SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar,
                SqlDbType.VarChar,SqlDbType.Int, SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, // Cambio del tipo de datos de la presion del punto 20151006 //20210707
                SqlDbType.Int,SqlDbType.VarChar,SqlDbType.Int,
                SqlDbType.Int, // campo o fuente MP 20160601
                SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,//20170814 rq036-17
                SqlDbType.Int,SqlDbType.VarChar,//20171130 rq026-17
                SqlDbType.Int,//20201224
                SqlDbType.VarChar,//20210305 broker
                SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,//20210707 trm moneda
                SqlDbType.VarChar}; //20220418 correccion contrato
            string[] lValorParametros = { "0", "S", "0", "0", "", "", "", "", "", "0", "", "0", "", "0", "0", "", "", "0", "", "0", "0", "0", "V", "0", "0", "2", "0", //campo o fuente MP 20160601 //20210707
                "S","N","0",//20170814 rq036-17
                "0","",//20171130 rq026-17
                        "0",//20201224
                        TxtIdTraMp.Text, //20210305 broker
                ddlTrm.SelectedValue, TxtObsTrm.Text , ddlMoneda.SelectedValue, //20210707 trm moneda
                txtObservaModif.Text }; //20220418 correcion contrato
            var lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "-" + DateTime.Now.ToShortDateString().Substring(3, 2) + "-" + DateTime.Now.ToShortDateString().Substring(0, 2);
            var sRuta = sRutaArc + lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\";

            var lsMensajeMail = ""; // Campo nuevo requerimiento algoritmo y verificacion 20150529
            var lsEnviaMail = ""; // Campo nuevo requerimiento algoritmo y verificacion 20150529

            try
            {
                var val = ""; //20210224
                //20210224
                if (HdfValida == "S")
                    val = validaciones();
                if (!string.IsNullOrEmpty(val))
                {
                    ToastrEvent?.Invoke(val, EnumTypeToastr.Warning);
                    valido = false;
                }
                //20210224
                if (lblValidacion.Text != "")
                    valido = false;
                if (valido)
                {
                    lValorParametros[0] = hdfIdVerif.Value;
                    lValorParametros[2] = lblOperacion.Text.Trim();
                    lValorParametros[3] = TxtNumContrato.Text.Trim().ToUpper();
                    lValorParametros[4] = hdfPunta.Value;
                    lValorParametros[5] = TxtFechaSus.Text.Trim();
                    lValorParametros[6] = lblNomOperador.Text.Trim();
                    if (lblTipoDocOperador.Text.Trim() == "Nit")
                        lValorParametros[7] = "1";
                    else
                        lValorParametros[7] = "2";
                    lValorParametros[8] = lblNoDocumentoOperaqdor.Text.Trim();
                    lValorParametros[9] = ddlModalidad.SelectedValue;
                    lValorParametros[10] = TxtOtrModalidad.Text.Trim().ToUpper();
                    lValorParametros[11] = ddlPuntoEntr.SelectedValue;
                    lValorParametros[12] = TxtOtrPuntoE.Text.Trim().ToUpper();
                    lValorParametros[13] = TxtCantidad.Text.Trim();
                    lValorParametros[14] = TxtPrecio.Text.Trim();
                    lValorParametros[15] = TxtFechaInicial.Text.Trim() + " " + TxtHoraInicial.Text;
                    lValorParametros[16] = TxtFechaFinal.Text.Trim() + " " + TxtHoraFinal.Text;
                    //20210707 cambio de items
                    lValorParametros[17] = "0"; // Ajuste requerimiento ajuste registro de contratos 148 - 20150915
                    if (HdfDestinoRueda == "T" && HdfTipoMerc == "P")
                    {
                        lValorParametros[18] = ddlSentidoFlujo.SelectedValue;
                        lValorParametros[19] = TxtPresion.Text.Trim();
                    }
                    lValorParametros[20] = ddlMotivoModifi.SelectedValue; // Motivo de Modificacion
                    lValorParametros[21] = "0";// ddlCesionario.SelectedValue; // Codigo Cesionario//20210224
                    lValorParametros[22] = hdfEstadoAct.Value;
                    lValorParametros[23] = hdfCodTipoSub.Value;
                    lValorParametros[24] = HdfDestinoRueda;
                    lValorParametros[26] = ddlFuente.SelectedValue; //campo o fuente MP 20160601
                                                                    //hdfTipoDemanda.Value = dlTipoDemanda.SelectedValue;
                                                                    //20170814 rq036-17
                    if (trSum.Visible)
                    {
                        lValorParametros[27] = ddlConecSnt.SelectedValue;
                        lValorParametros[28] = ddlEntBocPozo.SelectedValue;
                        lValorParametros[29] = ddlCentro.SelectedValue;
                    }
                    lValorParametros[30] = ddlPeriodo.SelectedValue;//20171130 rq026-17
                    lValorParametros[31] = ddlIndVar.SelectedValue;//20171130 rq026-17
                    lValorParametros[32] = DdlClasifica.SelectedValue;//20201224
                    //20210707 ajuste items
                    SqlTransaction oTransaccion;
                    lConexion.Abrir();
                    oTransaccion = lConexion.gObjConexion.BeginTransaction(IsolationLevel.Serializable);
                    goInfo.mensaje_error = "";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetRegistroContrato", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        ToastrEvent?.Invoke("Se presentó un Problema en la Creación del Registro del Contrato.! " + goInfo.mensaje_error, EnumTypeToastr.Error);  //20180126 rq107-16
                        oTransaccion.Rollback();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        var lsOperacion = "";
                        var lsIds = "";

                        while (lLector.Read())
                        {
                            hdfCodVerifCont.Value = lLector["codigo_cont_dato"].ToString();
                            lsMensajeMail = lLector["diferencia"].ToString(); // Campo nuevo requerimiento algoritmo y verificacion 20150529
                            lsEnviaMail = lLector["enviaMail"].ToString(); // Campo nuevo requerimiento algoritmo y verificacion 20150529
                            if (lsOperacion == "")
                            {
                                lsOperacion = lLector["operacion"].ToString();
                                lsIds = lLector["codigo_verif"].ToString();
                            }
                            else
                            {
                                lsOperacion += ", " + lLector["operacion"];
                                lsIds += ", " + lLector["codigo_verif"];
                            }
                        }

                        lLector.Close();
                        lLector.Dispose();
                        oTransaccion.Commit();
                        ///// Envio de la alerta al ingresar la informacion por ambas puntas 20150529
                        ///// Requerimiento Algoritmo y Verificacion
                        var lsAsunto = "";
                        var lsMensaje = "";
                        var lsMensajeC = "";
                        var lsMailC = "";
                        var lsNomOperadorC = "";
                        var lsMailV = "";
                        var lsNomOperadorV = "";
                        /// Obtengo el mail del Operador Compra
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraC.Value + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lsMailC = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" + lLector["e_mail2"]; // 20180228 rq032-17
                            if (lLector["tipo_persona"].ToString() == "J")
                                lsNomOperadorC = lLector["razon_social"].ToString();
                            else
                                lsNomOperadorC = lLector["nombres"] + " " + lLector["apellidos"];
                        }
                        lLector.Close();
                        lLector.Dispose();
                        /// Obtengo el mail del Operador Venta
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraV.Value + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lsMailV = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" + lLector["e_mail2"]; // 20180228 rq032-17
                            if (lLector["tipo_persona"].ToString() == "J")
                                lsNomOperadorV = lLector["razon_social"].ToString();
                            else
                                lsNomOperadorV = lLector["nombres"] + " " + lLector["apellidos"];
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Envio de la alerta del ingreso de la información
                        lsAsunto = "Notificación Actualización Información Contrato";
                        if (hdfPunta.Value == "C")
                            lsMensaje = "Nos permitimos  informarle que el operador " + lsNomOperadorC + " acaba de Actualizar la información transaccional del Registro del Contrato de la Operación No: " + lblOperacion.Text + " con número de contrato: " + TxtNumContrato.Text + ".<br><br>"; //20220221 regsitro extemporaneo
                        else
                            lsMensaje = "Nos permitimos  informarle que el operador " + lsNomOperadorV + " acaba de Actualizar la información transaccional del Registro del Contrato de la Operación No: " + lblOperacion.Text + " con número de contrato: " + TxtNumContrato.Text + ".<br><br>"; //20220221 registro extemporaeno
                        lsMensaje += "Observaciones: Por favor validar la información en el Módulo Registro de Contratos. <br><br>";
                        lsMensaje += "Cordialmente, <br><br><br>";
                        lsMensaje += "Administrador SEGAS <br>";
                        lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                        var mailC = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                        var error1 = mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas");
                        if (!string.IsNullOrEmpty(error1))
                            ToastrEvent?.Invoke(error1, EnumTypeToastr.Error);
                        ////
                        lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                        var mailV = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                        var error2 = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas");
                        if (!string.IsNullOrEmpty(error2))
                            ToastrEvent?.Invoke(error2, EnumTypeToastr.Error);
                        if (lsEnviaMail == "S")
                        {
                            ///// Envio del Mail de la Solicitud de la Correcion o Registro del Contrato
                            if (lsMensajeMail != "")
                            {
                                lsAsunto = "Notificación Solicitud Corrección Registro y Verificación";
                                lsMensaje = "Nos permitimos  informarle que el Administrador del SEGAS acaba de Solicitar la Correción de la Información transaccional de la Operación No: " + lblOperacion.Text + " con número de contrato: " + TxtNumContrato.Text + ". Por favor revisar los siguientes campos: <br><br> " + lsMensajeMail + "<br><br>"; //20220221 registro extemporaeno
                                lsMensaje += "Observaciones: Por favor validar los campos objeto de corrección con su contraparte. <br><br>";
                            }
                            else
                            {
                                if (ddlIndVar.SelectedValue == "N")
                                {
                                    lsAsunto = "Notificación Registro Contrato";
                                    lsMensaje = "Nos permitimos informarle que el Administrador de la plataforma SEGAS acaba de realizar exitosamente el Registro del Contrato de la Operación No: " + lblOperacion.Text + " con número de contrato: " + TxtNumContrato.Text + " y con No. de Registro: " + hdfIdVerif.Value + ".<br><br>"; //20220221 regsitro extemporaneo
                                }
                                else
                                {
                                    if (lsIds != hdfIdVerif.Value)
                                    {
                                        lsAsunto = "Notificación Registro Contrato";
                                        lsMensaje = "Nos permitimos informarle que el Administrador de la plataforma SEGAS acaba de realizar exitosamente el Registro del Contrato de la Operación No: " + lblOperacion.Text + " con número de contrato: " + TxtNumContrato.Text + " y con No. de Registro: " + hdfIdVerif.Value + ".<br><br>"; //20220221 registro extemporaneo
                                        lsMensaje += "Debido a que es un contrato variable quedaron registrados los siguientes Ids de registro: " + lsIds + "<br><br>";
                                    }
                                    else
                                    {
                                        lsAsunto = "Notificación Registro Contrato";
                                        lsMensaje = "Nos permitimos informarle que el Administrador de la plataforma SEGAS acaba de realizar exitosamente el Registro del Contrato de la Operación No: " + lblOperacion.Text + " con número de contrato: " + TxtNumContrato.Text + " y con No. de Registro: " + hdfIdVerif.Value + ".<br><br>"; //20220221 registro extemporaeno
                                        lsMensaje += "Debido a que es un contrato variable sólo quedará en firme cuando se registren todos los contratos asociados a este.<br><br>";
                                    }
                                }
                            }
                            lsMensaje += "Cordialmente, <br><br><br>";
                            lsMensaje += "Administrador SEGAS <br>";
                            lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                            var mailC1 = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                            var error3 = mailC1.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas");
                            if (!string.IsNullOrEmpty(error3))
                                Toastr.Error(this, error3);
                            ////
                            lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                            var mailV1 = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                            var error4 = mailV1.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas");
                            if (!string.IsNullOrEmpty(error4))
                                ToastrEvent?.Invoke(error4, EnumTypeToastr.Error);
                        }
                        else
                        {
                            lConexion.Cerrar();
                        }
                        ToastrEvent?.Invoke("Registro Ingresado Correctamente.!", EnumTypeToastr.Success);
                        divDemanda.Visible = false;
                        LimpiarCampos();
                        //Se cierra el modal de Modificación
                        ModalEvent?.Invoke(registroContratoMod.ID, registroContratoModInside.ID, EnumTypeModal.Cerrar);
                        CargarDatos();
                    }
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20170814 rq036-17
        protected void ddlConecSnt_SelectedIndexChanged(object sender, EventArgs e)
        {
            var lsPunto = ddlPuntoEntr.SelectedValue;//20171114 rq036-17
            if (ddlConecSnt.SelectedValue == "S")
            {
                lConexion.Abrir();
                ddlPuntoEntr.Items.Clear();
                LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_pozo", " ind_estandar='S' and  estado = 'A'  order by descripcion", 0, 1);
                lConexion.Cerrar();
                //20171114 rq036-17
                try
                {
                    ddlPuntoEntr.SelectedValue = lsPunto;
                }
                catch (Exception)
                {
                    ddlPuntoEntr.SelectedValue = "0";
                }
                //20171114 rq036-17
                if (ddlPuntoEntr.SelectedValue == "0")
                    ddlPuntoEntr.Enabled = true;
                lblCentro.Visible = false;
                ddlCentro.Visible = false;
                ddlCentro.SelectedValue = "0";
                lblPuntoE.Visible = true;
                ddlPuntoEntr.Visible = true;
            }
            else
            {
                if (ddlEntBocPozo.SelectedValue == "S")
                {
                    lConexion.Abrir();
                    ddlPuntoEntr.Items.Clear();
                    LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_pozo", " codigo_tipo_campo = 6 and  estado = 'A'  order by descripcion", 0, 1);
                    lConexion.Cerrar();
                    //20171114 rq036-17
                    try
                    {
                        ddlPuntoEntr.SelectedValue = lsPunto;
                    }
                    catch (Exception)
                    {
                        ddlPuntoEntr.SelectedValue = "0";
                    }
                    //20171114 rq036-17
                    if (ddlPuntoEntr.SelectedValue == "0")
                        ddlPuntoEntr.Enabled = true;

                    lblCentro.Visible = false;
                    ddlCentro.Visible = false;
                    //ddlCentro.SelectedValue = "0"; //20171114 rq036-17
                    lblPuntoE.Visible = true;
                    ddlPuntoEntr.Visible = true;
                }
                else
                {
                    lblCentro.Visible = true;
                    ddlCentro.Visible = true;
                    ddlCentro.SelectedValue = "0";
                    lblPuntoE.Visible = false;
                    ddlPuntoEntr.Visible = false;
                    ddlPuntoEntr.SelectedValue = "0";
                    //20171114 rq036-17
                    if (ddlCentro.SelectedValue == "0")
                        ddlCentro.Enabled = true;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20170814 rq036-17
        protected void ddlEntBocPozo_SelectedIndexChanged(object sender, EventArgs e)
        {
            var lsPunto = ddlPuntoEntr.SelectedValue;//20171114 rq036-17
            if (ddlConecSnt.SelectedValue == "S")
            {
                lConexion.Abrir();
                ddlPuntoEntr.Items.Clear();
                LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_pozo", " ind_estandar='S' and  estado = 'A'  order by descripcion", 0, 1);
                lConexion.Cerrar();
                //20171114 rq036-17
                try
                {
                    ddlPuntoEntr.SelectedValue = lsPunto;
                }
                catch (Exception)
                {
                    ddlPuntoEntr.SelectedValue = "0";
                }
                //20171114 rq036-17
                if (ddlPuntoEntr.SelectedValue == "0")
                    ddlPuntoEntr.Enabled = true;

                lblCentro.Visible = false;
                ddlCentro.Visible = false;
                ddlCentro.SelectedValue = "0";
                lblPuntoE.Visible = true;
                ddlPuntoEntr.Visible = true;
            }
            else
            {
                if (ddlEntBocPozo.SelectedValue == "S")
                {
                    lConexion.Abrir();
                    ddlPuntoEntr.Items.Clear();
                    LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_pozo", " codigo_tipo_campo = 6 and  estado = 'A'  order by descripcion", 0, 1);
                    lConexion.Cerrar();
                    //20171114 rq036-17
                    try
                    {
                        ddlPuntoEntr.SelectedValue = lsPunto;
                    }
                    catch (Exception)
                    {
                        ddlPuntoEntr.SelectedValue = "0";
                    }
                    //20171114 rq036-17
                    if (ddlPuntoEntr.SelectedValue == "0")
                        ddlPuntoEntr.Enabled = true;

                    lblCentro.Visible = false;
                    ddlCentro.Visible = false;
                    ddlCentro.SelectedValue = "0";
                    lblPuntoE.Visible = true;
                    ddlPuntoEntr.Visible = true;
                }
                else
                {
                    lblCentro.Visible = true;
                    ddlCentro.Visible = true;
                    //ddlCentro.SelectedValue = "0"; //20171114 rq036-17
                    lblPuntoE.Visible = false;
                    ddlPuntoEntr.Visible = false;
                    ddlPuntoEntr.SelectedValue = "0";
                    //20171114 rq036-17
                    if (ddlCentro.SelectedValue == "0")
                        ddlCentro.Enabled = true;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20170814 rq036-17
        protected void ddlPeriodo_SelectedIndexChanged(object sender, EventArgs e)
        {
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_periodos_entrega", " codigo_periodo= " + ddlPeriodo.SelectedValue);
            if (lLector.HasRows)
            {
                lLector.Read();
                if (lLector["medida_tiempo"].ToString() != "I")
                {
                    TxtHoraInicial.Visible = false;
                    TxtHoraFinal.Visible = false;
                    TxtHoraInicial.Text = "00:00";
                    TxtHoraFinal.Text = "00:00";
                }
                else
                {
                    TxtHoraInicial.Visible = true;
                    TxtHoraFinal.Visible = true;
                }
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnConsultar_Click(object sender, EventArgs e)
        {
            dtgConsulta.CurrentPageIndex = 0;//20190130 ajuste
            CargarDatos();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void lkbExcel_Click(object sender, EventArgs e)
        {
            var valido = true;

            DateTime ldFecha;
            var liValor = 0;
            string[] lsNombreParametros = { "@PP_tipo_subasta", "@PP_numero_contrato", "@PP_fecha_contrato", "@PP_mercado", "@PP_codigo_producto",
                "@PP_operador_compra", "@PP_operador_venta","@PP_tipo_perfil","@PP_codigo_operador", "@PP_estado",
                "@PP_fecha_contrato_fin", "@PP_numero_contrato_fin", //20161207 rq102 conformacion de rutas
                "@PP_codigo_causa", "@PP_codigo_verif"}; //20171130 rq026-17
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int,
                SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar ,
                SqlDbType.VarChar, SqlDbType.Int,  //20161207 rq102 conformacion de rutas
                SqlDbType.VarChar,SqlDbType.Int};//20171130 rq026-17
            string[] lValorParametros = { "0", "0", "", "", "0", "0", "0", Session["tipoPerfil"].ToString(), goInfo.cod_comisionista, "0", "", "0", "0", "0" }; //20161207 rq102 conformacion de rutas //20171130 rq026-17

            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception)
                {
                    ToastrEvent?.Invoke("Formato Inválido en el Campo Fecha Inicial de Negociación.", EnumTypeToastr.Error);  //20161207 rq102 conformacion de rutas
                    valido = false;
                }
            }
            //20161207 rq102 conformacion de rutas
            if (TxtFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaFin.Text);
                }
                catch (Exception)
                {
                    ToastrEvent?.Invoke("Formato Inválido en el Campo Fecha Final de Negociación.", EnumTypeToastr.Error);
                    valido = false;
                }
            }
            //20161205 rq102 conformacion de rutas
            if (TxtFechaIni.Text.Trim().Length == 0 && TxtFechaFin.Text.Trim().Length > 0)
            {
                ToastrEvent?.Invoke("Debe digitar la Fecha inicial de Negociación.", EnumTypeToastr.Warning);
                valido = false;
            }
            //20161207 rq102 conformacion de rutas
            if (TxtFechaIni.Text.Trim().Length > 0 && TxtFechaFin.Text.Trim().Length > 0)
                try
                {
                    if (Convert.ToDateTime(TxtFechaFin.Text) < Convert.ToDateTime(TxtFechaIni.Text))
                    {
                        ToastrEvent?.Invoke("La Fecha inicial de Negociación debe ser menor o igual que la fecha final.", EnumTypeToastr.Warning);
                        valido = false;
                    }
                }
                catch (Exception)
                { }

            if (TxtNoContrato.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoContrato.Text);
                }
                catch (Exception)
                {
                    ToastrEvent?.Invoke("Formato Inválido en el Campo No Operación.", EnumTypeToastr.Error); //20161205 rq102 conformacion de rutas
                    valido = false;
                }

            }
            // 20161207 rq102 conformación de rutas
            if (TxtNoContratoFin.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoContratoFin.Text);
                }
                catch (Exception)
                {
                    ToastrEvent?.Invoke("Formato Inválido en el Campo No Operación final.", EnumTypeToastr.Error);
                    valido = false;
                }

            }
            // 20161207 rq102 conformación de rutas
            if (TxtNoContrato.Text.Trim().Length == 0 && TxtNoContratoFin.Text.Trim().Length > 0)
            {
                ToastrEvent?.Invoke("Debe digitar el No de operación inicial antes que el final.", EnumTypeToastr.Warning);
                valido = false;
            }
            // 20161207 rq102 conformación de rutas
            if (TxtNoContrato.Text.Trim().Length > 0 && TxtNoContratoFin.Text.Trim().Length > 0)
            {
                try
                {
                    if (Convert.ToInt32(TxtNoContratoFin.Text) < Convert.ToInt32(TxtNoContrato.Text))
                    {
                        ToastrEvent?.Invoke("El No Operación inicial debe ser menor o igual que el final.", EnumTypeToastr.Warning);
                        valido = false;
                    }
                }
                catch (Exception)
                {

                }
            }

            if (valido)
            {
                try
                {
                    if (TxtNoContrato.Text.Trim().Length > 0)
                        lValorParametros[1] = TxtNoContrato.Text.Trim();
                    if (ddlSubasta.SelectedValue != "0")
                        lValorParametros[0] = ddlSubasta.SelectedValue;
                    if (TxtFechaIni.Text.Trim().Length > 0)
                        lValorParametros[2] = TxtFechaIni.Text.Trim();
                    if (ddlMercado.SelectedValue != "")
                        lValorParametros[3] = ddlMercado.SelectedValue;
                    if (ddlProducto.SelectedValue != "0")
                        lValorParametros[4] = ddlProducto.SelectedValue;
                    if (ddlComprador.SelectedValue != "0")
                        lValorParametros[5] = ddlComprador.SelectedValue;
                    if (ddlVendedor.SelectedValue != "0")
                        lValorParametros[6] = ddlVendedor.SelectedValue;
                    if (ddlEstado.SelectedValue != "0")
                        lValorParametros[9] = ddlEstado.SelectedValue;
                    //20161207 rq 102 conformacion de rutas
                    if (TxtFechaFin.Text.Trim().Length > 0)
                        lValorParametros[10] = TxtFechaFin.Text.Trim();
                    else
                        lValorParametros[10] = lValorParametros[2];
                    //20161207 rq 102 conformacion de rutas
                    if (TxtNoContratoFin.Text.Trim().Length > 0)
                        lValorParametros[11] = TxtNoContratoFin.Text.Trim();
                    else
                        lValorParametros[11] = lValorParametros[1];

                    lValorParametros[12] = ddlCausa.SelectedValue;
                    //20171130 rq026-17
                    if (TxtNoId.Text.Trim().Length > 0)
                        lValorParametros[13] = TxtNoId.Text.Trim();

                    lConexion.Abrir();
                    dtgConsultaExcel.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetVerificaContratos", lsNombreParametros, lTipoparametros, lValorParametros); //20210915
                    dtgConsultaExcel.DataBind();
                    lConexion.Cerrar();

                    var lsNombreArchivo = Session["login"] + "InfExcelReg" + DateTime.Now + ".xls";
                    var lsb = new StringBuilder();
                    var lsw = new StringWriter(lsb);
                    var lhtw = new HtmlTextWriter(lsw);
                    var lpagina = new Page();
                    var lform = new HtmlForm();
                    dtgConsultaExcel.Visible = true;
                    lpagina.EnableEventValidation = false;
                    lpagina.Controls.Add(lform);
                    dtgConsultaExcel.EnableViewState = false;
                    lform.Controls.Add(dtgConsultaExcel);
                    lpagina.RenderControl(lhtw);
                    Response.Clear();

                    Response.Buffer = true;
                    Response.ContentType = "aplication/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                    Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                    Response.ContentEncoding = Encoding.Default;

                    Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                    Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
                    Response.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + "Consulta Registro de Contratos" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                    Response.Write(lsb.ToString());
                    dtgConsultaExcel.Visible = false;
                    Response.End();
                    Response.Flush();
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Problemas al Consultar los Contratos. " + ex.Message, EnumTypeToastr.Error);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSalir_Click(object sender, EventArgs e)
        {
            divDemanda.Visible = false;
            CargarDatos();
        }
        // <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 2021020
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            ModalEvent?.Invoke(registroContratoMod.ID, registroContratoModInside.ID, EnumTypeModal.Cerrar);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20220418 correccion contrato
        protected void btnRechazar_Click(object sender, EventArgs e)
        {
            string lsMensaje = "";
            //if (ddlMotivoModifi.SelectedValue =="0")
            //    lsMensaje += "Debe seleccionar el motivo de modificación";
            if (txtObservaModif.Text == "")
                lsMensaje += "Debe ingresar las observaciones";
            if (lsMensaje == "")
            {
                try
                {
                    string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_punta", "@P_observaciones" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
                    string[] lValorParametros = { hdfIdVerif.Value, hdfPunta.Value, txtObservaModif.Text };
                    lConexion.Abrir();
                    DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetRechazaVerifica", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    lConexion.Cerrar();
                    ToastrEvent?.Invoke("Rechazo realizado correctamente.!", EnumTypeToastr.Success);
                    divDemanda.Visible = false;
                    LimpiarCampos();
                    //Se cierra el modal de Modificación
                    ModalEvent?.Invoke(registroContratoMod.ID, registroContratoModInside.ID, EnumTypeModal.Cerrar);
                    CargarDatos();
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Error en el rechazo. " + ex.Message.ToString(), EnumTypeToastr.Warning);
                }
            }
            else
                ToastrEvent?.Invoke(lsMensaje, EnumTypeToastr.Warning);
        }
    }
}