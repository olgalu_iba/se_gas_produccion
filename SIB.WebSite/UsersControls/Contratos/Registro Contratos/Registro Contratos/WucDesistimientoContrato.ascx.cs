﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;
using System.Linq;

namespace UsersControls.Contratos.Registro_Contratos.Registro_Contratos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class WucDesistimientoContrato : UserControl
    {
        #region EventHandler

        /// <summary>
        /// Delegado para el manejo de los modals
        /// </summary>
        public delegate void EventHandlerModal(string id, string insideId, EnumTypeModal typeModal, Modal.Size size = Modal.Size.Large);

        /// <summary>
        ///  EventHandler para la selección de los los modals
        /// </summary>
        public event EventHandlerModal ModalEvent;

        /// <summary>
        ///  Delegado para el manejo de los toastr
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        /// EventHandler para la selección de los toastr 
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        ///Delegado para la apertura de un documento en una nueva ventana del navegador 
        /// </summary>
        public delegate void EventHandlerDocument(string pathDocument);

        /// <summary>
        ///EventHandler para la apertura de un documento en una nueva ventana del navegador    
        /// </summary>
        public event EventHandlerDocument DocumentEvent;

        /// <summary>
        /// Delegado para el manejo del log de cargue de archivos
        /// </summary>
        //public delegate void EventHandlerLogCargaArchivo(StringBuilder message);

        /// <summary>
        /// EventHandler para el log de cargue de archivos
        /// </summary>
        //public event EventHandlerLogCargaArchivo LogCargaArchivoEvent;

        /// <summary>
        /// Delegado para la selección de los botones  
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get { return ViewState["NameController"] != null && !string.IsNullOrEmpty(ViewState["NameController"].ToString()) ? ViewState["NameController"].ToString() : string.Empty; }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        #endregion Propiedades

        InfoSessionVO goInfo = null;
        static string lsTitulo = "Desistimiento de contratos de subasta sin registro";
        clConexion lConexion = null;
        clConexion lConexion1 = null;
        SqlDataReader lLector;
        DataSet lds = new DataSet();
        String strRutaCarga;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lConexion = new clConexion(goInfo);
            lConexion1 = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["RutaDesistimiento"].ToString();

            if (!IsPostBack)
            {
                /// Llenar controles del Formulario
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlObservacionSol, "m_observacion_sin_reg", " estado = 'A' order by observacion", 0, 1);
                //LlenarControles(lConexion.gObjConexion, ddlObservacionApro, "m_observacion_sin_reg", " estado = 'A' order by observacion", 0, 1);
                lConexion.Cerrar();
            }

        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            EnumBotones[] botones = { EnumBotones.Buscar};
            ButtonsEvent?.Invoke(botones, null, NameController);
            /// Llenar controles del Formulario
            //Se establece que el controlador ya se inicializo    
            ButtonsEvent?.Invoke(botones.ToArray(), null, NameController);
            Inicializado = true;
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnConsultar_Click(object sender, EventArgs e)
        {
            dtgConsulta.CurrentPageIndex = 0;
            CargarDatos();
        }

        protected void dtgConsulta_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            this.dtgConsulta.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();

        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            var lblMensaje = new StringBuilder();
            DateTime ldFechaI = DateTime.Now;
            DateTime ldFechaF = DateTime.Now;
            string[] lsNombreParametros = { "@P_fecha_ini", "@P_fecha_fin", "@P_operacion", "@P_codigo_operador" };
            SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { "", "", "0", goInfo.cod_comisionista };

            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo fecha inicial <br>");
                }
            }
            if (TxtFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo fecha final<br>");
                }
            }
            if (TxtFechaFin.Text.Trim().Length > 0 && TxtFechaIni.Text.Trim().Length == 0)
                lblMensaje.Append("Debe digitar la fecha inicial antes que la final<br>");

            if (TxtFechaFin.Text.Trim().Length > 0 && TxtFechaIni.Text.Trim().Length > 0)
                if (ldFechaI > ldFechaF)
                    lblMensaje.Append("La fecha inicial debe ser menor o igual  que la final<br>");

            if (lblMensaje.ToString() == "")
            {
                try
                {
                    if (TxtFechaIni.Text != "")
                    {
                        lValorParametros[0] = TxtFechaIni.Text.Trim();
                        if (TxtFechaFin.Text != "")
                            lValorParametros[1] = TxtFechaFin.Text.Trim();
                        else
                            lValorParametros[1] = TxtFechaIni.Text.Trim();
                    }
                    if (TxtNoOper.Text.Trim().Length > 0)
                        lValorParametros[2] = TxtNoOper.Text.Trim();

                    lConexion.Abrir();
                    dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContDesistimiento", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgConsulta.DataBind();
                    //20190607 rq036-19
                    //dtgExcel.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContDesistimiento", lsNombreParametros, lTipoparametros, lValorParametros);
                    //20190607 rq036-19
                    //dtgExcel.DataBind();
                    lConexion.Cerrar();

                    
                    if (dtgConsulta.Items.Count == 0)
                    {
                        lblMensaje.Append("No se encontraron Registros. Las operaciones de subasta vencidas necesitan tener aprobación previa para ejecutar el proceso de desistimiento");
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("No se Pudo Generar el Informe.! " + ex.Message.ToString());
                }
            }
            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page")) return;

            var lblMensaje = new StringBuilder();
            if (e.CommandName.Equals("Ver"))
            {
                try
                {
                    if (e.Item.Cells[9].Text != "&nbsp;" && e.Item.Cells[9].Text != "")
                    {
                        string lsCarpetaAnt = "";
                        string[] lsCarperta;
                        lsCarperta = strRutaCarga.Split('\\');
                        lsCarpetaAnt = lsCarperta[lsCarperta.Length - 2];
                        var lsRuta = "../../" + lsCarpetaAnt + "/" + e.Item.Cells[9].Text.Replace(@"\", "/");
                        if (File.Exists(strRutaCarga + e.Item.Cells[9].Text) )
                            DocumentEvent?.Invoke(lsRuta);
                        else
                            lblMensaje.Append("El archivo de desistimiento no existe.");
                    }
                    else
                        lblMensaje.Append("No hay archivo de desisitimiento cargado para el contrato. ");
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Error al visualizar el archivo. " + ex.Message.ToString());
                }
            }
            if (e.CommandName.Equals("Desistir"))
            {
                if (e.Item.Cells[8].Text == "N")
                    lblMensaje.Append("No puede hacer el desistimiento del contrato");
                //if (e.Item.Cells[8].Text == "A" && e.Item.Cells[14].Text =="S")
                //    lblMensaje.Append("Se venció el plazo para aprobar el desistimiento");
                if (lblMensaje.ToString() == "")
                {
                    try
                    {
                        lblOperacion.Text = e.Item.Cells[0].Text;
                        lblFechaNeg.Text = e.Item.Cells[1].Text;
                        lblComprador.Text = e.Item.Cells[3].Text;
                        lblVendedor.Text = e.Item.Cells[2].Text;
                        lblCantidad.Text = e.Item.Cells[6].Text;
                        lblPrecio.Text = e.Item.Cells[7].Text;
                        hndVendedor.Value = e.Item.Cells[2].Text;
                        hndArchivo.Value = e.Item.Cells[9].Text;
                        BtnCrear.Visible = false;
                        BtnModificar.Visible = false;
                        BtnAprobar.Visible = false;
                        BtnRechazar.Visible = false;
                        if (e.Item.Cells[8].Text == "C")
                        {
                            BtnCrear.Visible = true;
                            ddlObservacionSol.SelectedValue = "0";
                            //divObsApr.Visible = false;
                            ddlObservacionSol.Enabled = true;
                            BtnVer.Visible = false;
                            FuArchivo.Visible = true;
                        }
                        if (e.Item.Cells[8].Text == "M")
                        {
                            BtnModificar.Visible = true;
                            //divObsApr.Visible = false;
                            ddlObservacionSol.Enabled = true;
                            BtnVer.Visible = true;
                            FuArchivo.Visible = true;
                            try
                            {
                                ddlObservacionSol.SelectedValue = e.Item.Cells[12].Text;
                            }
                            catch (Exception ex)
                            { }
                        }
                        if (e.Item.Cells[8].Text == "A")
                        {
                            BtnAprobar.Visible = true;
                            BtnRechazar.Visible = true;
                            ddlObservacionSol.Enabled = false;
                            FuArchivo.Visible = false;
                            //divObsApr.Visible = true;
                            BtnVer.Visible = true;
                            try
                            {
                                ddlObservacionSol.SelectedValue = e.Item.Cells[12].Text;
                                //ddlObservacionApro.SelectedValue = "0";
                            }
                            catch (Exception ex)
                            { }
                        }
                        ModalEvent?.Invoke(modContratosPdf.ID, modContratosPdfInside.ID, EnumTypeModal.Abrir);
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Append("Problemas en la Recuperación de la Información. " + ex.Message.ToString());
                    }
                }
            }
            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            //Cierra el modal de Registro
            ModalEvent?.Invoke(modContratosPdf.ID, modContratosPdfInside.ID, EnumTypeModal.Cerrar);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void BtnCrear_Click(object sender, EventArgs e)
        {
            var lblMensaje = new StringBuilder();
            try
            {
                if (ddlObservacionSol.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar las observaciones del desistimiento<br>");
                if (FuArchivo.FileName.ToString() == "")
                    lblMensaje.Append("Debe seleccionar el archivo<br>");
                else
                {
                    string lsExtension = Path.GetExtension(FuArchivo.FileName).ToUpper();
                    if (lsExtension != ".PDF" && lsExtension != ".PDFA")
                        lblMensaje.Append("El archivo cargado debe ser tipo PDF<br>");
                }
                //20200430 ajuste nombre archivos
                if (!(Directory.Exists(strRutaCarga)))
                    Directory.CreateDirectory(strRutaCarga);
                string lsNumero = "";
                string lsArchivo = "";
                int liConta = 0;
                while (lsArchivo == "" && liConta < 10)
                {
                    lsNumero = DateTime.Now.Millisecond.ToString();
                    lsArchivo = DateTime.Now.ToString("yyyy_MM_dd") + "_" + lblOperacion.Text + "_" + lsNumero + Path.GetExtension(FuArchivo.FileName).ToUpper();
                    if (File.Exists(strRutaCarga + lsArchivo))
                    {
                        lsArchivo = "";
                    }
                    liConta++;
                }
                if (lsArchivo == "")
                    lblMensaje.Append("No se puede cargar el archivo. Intente de nuevo<br>");

                if (lblMensaje.ToString() == "")
                {
                    string[] lsNombreParametros = { "@P_numero_contrato", "@P_codigo_operador", "@P_codigo_observacion", "@P_archivo", "@P_accion" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
                    string[] lValorParametros = { lblOperacion.Text, goInfo.cod_comisionista, ddlObservacionSol.SelectedValue, lsArchivo, "1" };

                    lConexion.Abrir();
                    goInfo.mensaje_error = "";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetContDesistimiento", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        lblMensaje.Append("Se presentó un Problema en la creación del desistimiento.! " + goInfo.mensaje_error.ToString());
                        lConexion.Cerrar();
                    }
                    else
                    {
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                                lblMensaje.Append(lLector["error"].ToString() + "<br>");
                        }
                        else
                        {
                            try
                            {
                                FuArchivo.SaveAs(strRutaCarga + lsArchivo);
                                //ModalEvent?.Invoke(modContratosPdf.ID, modContratosPdfInside.ID, EnumTypeModal.Abrir);
                            }
                            catch (Exception ex)
                            {
                            }
                            ToastrEvent?.Invoke("Se creo correctamente el desistimiento", EnumTypeToastr.Success );
                            ModalEvent?.Invoke(modContratosPdf.ID, modContratosPdfInside.ID, EnumTypeModal.Cerrar);
                            CargarDatos();
                        }
                        lConexion.Cerrar();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Append(ex.Message);
                lConexion.Cerrar();
            }
            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                ModalEvent?.Invoke(modContratosPdf.ID, modContratosPdfInside.ID, EnumTypeModal.Abrir);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void BtnModificar_Click(object sender, EventArgs e)
        {
            var lblMensaje = new StringBuilder();
            try
            {
                if (ddlObservacionSol.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar las observaciones del desistimiento<br>");
                string lsNumero = "";
                string lsArchivo = "";
                int liConta = 0;
                if (FuArchivo.FileName != "")
                {
                    while (lsArchivo == "" && liConta < 10)
                    {
                        lsNumero = DateTime.Now.Millisecond.ToString();
                        lsArchivo = DateTime.Now.ToString("yyyy_MM_dd") + "_" + lblOperacion.Text + "_" + lsNumero + Path.GetExtension(FuArchivo.FileName).ToUpper();
                        if (File.Exists(strRutaCarga + lsArchivo))
                        {
                            lsArchivo = "";
                        }
                        liConta++;
                    }
                }
                if (lblMensaje.ToString() == "")
                {
                    string[] lsNombreParametros = { "@P_numero_contrato", "@P_codigo_operador", "@P_codigo_observacion", "@P_archivo", "@P_accion" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
                    string[] lValorParametros = { lblOperacion.Text, goInfo.cod_comisionista, ddlObservacionSol.SelectedValue, lsArchivo, "2" };

                    lConexion.Abrir();
                    goInfo.mensaje_error = "";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetContDesistimiento", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        lblMensaje.Append("Se presentó un Problema en la creación del desistimiento.! " + goInfo.mensaje_error.ToString());
                        lConexion.Cerrar();
                    }
                    else
                    {
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                                lblMensaje.Append(lLector["error"].ToString() + "<br>");
                        }
                        else
                        {
                            if (lsArchivo != "")
                            {
                                try
                                {
                                    FuArchivo.SaveAs(strRutaCarga + lsArchivo);
                                }
                                catch (Exception ex)
                                {
                                }
                            }
                            ToastrEvent?.Invoke("Se modificó correctamente el desistimiento", EnumTypeToastr.Success);
                            ModalEvent?.Invoke(modContratosPdf.ID, modContratosPdfInside.ID, EnumTypeModal.Cerrar);
                            CargarDatos();
                        }
                        lConexion.Cerrar();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Append(ex.Message);
                lConexion.Cerrar();
            }
            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                ModalEvent?.Invoke(modContratosPdf.ID, modContratosPdfInside.ID, EnumTypeModal.Abrir);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //public void lkbExcel_Click(object sender, EventArgs e)
        //{
        //    string lsNombreArchivo = Session["login"] + "InfDesistimiento" + DateTime.Now + ".xls";
        //    var lblMensaje = new StringBuilder();
        //    try
        //    {
        //        decimal ldCapacidad = 0;
        //        StringBuilder lsb = new StringBuilder();
        //        ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
        //        StringWriter lsw = new StringWriter(lsb);
        //        HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
        //        Page lpagina = new Page();
        //        HtmlForm lform = new HtmlForm();
        //        dtgExcel.Visible = true;
        //        dtgExcel.EnableViewState = false;
        //        lpagina.EnableEventValidation = false;
        //        lpagina.DesignerInitialize();
        //        lpagina.Controls.Add(lform);
        //        lform.Controls.Add(dtgExcel);
        //        lpagina.RenderControl(lhtw);
        //        Response.Clear();

        //        Response.Buffer = true;
        //        Response.ContentType = "aplication/vnd.ms-excel";
        //        Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
        //        Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
        //        Response.ContentEncoding = System.Text.Encoding.Default;

        //        Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
        //        Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
        //        Response.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + "Consulta Desistimiento" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
        //        Response.Write(lsb.ToString());
        //        Response.End();
        //        Response.Flush();
        //        dtgExcel.Visible = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        lblMensaje.Append("Problemas al Consultar los Registros. " + ex.Message.ToString());
        //    }
        //    if (!string.IsNullOrEmpty(lblMensaje.ToString()))
        //        ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void BtnAprobar_Click(object sender, EventArgs e)
        {
            var lblMensaje = new StringBuilder();
            try
            {
                //if (ddlObservacionApro.SelectedValue == "0")
                //    lblMensaje.Append("Debe seleccionar las observaciones del desistimiento<br>");
                if (lblMensaje.ToString() == "")
                {
                    string[] lsNombreParametros = { "@P_numero_contrato", "@P_codigo_operador", "@P_accion" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int,  SqlDbType.Int };
                    string[] lValorParametros = { lblOperacion.Text, goInfo.cod_comisionista, "3" };

                    lConexion.Abrir();
                    goInfo.mensaje_error = "";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetContDesistimiento", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        lblMensaje.Append("Se presentó un Problema en la aprobación del desistimiento.! " + goInfo.mensaje_error.ToString());
                        lConexion.Cerrar();
                    }
                    else
                    {
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                                lblMensaje.Append(lLector["error"].ToString() + "<br>");
                        }
                        else
                        {
                            ToastrEvent?.Invoke("Se aprobó correctamente el desistimiento", EnumTypeToastr.Success);
                            ModalEvent?.Invoke(modContratosPdf.ID, modContratosPdfInside.ID, EnumTypeModal.Cerrar);
                            CargarDatos();
                        }
                        lConexion.Cerrar();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Append(ex.Message);
                lConexion.Cerrar();
            }
            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                ModalEvent?.Invoke(modContratosPdf.ID, modContratosPdfInside.ID, EnumTypeModal.Abrir);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void BtnRechazar_Click(object sender, EventArgs e)
        {
            var lblMensaje = new StringBuilder();
            try
            {
                if (lblMensaje.ToString() == "")
                {
                    string[] lsNombreParametros = { "@P_numero_contrato", "@P_codigo_operador", "@P_accion" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                    string[] lValorParametros = { lblOperacion.Text, goInfo.cod_comisionista, "4" };

                    lConexion.Abrir();
                    goInfo.mensaje_error = "";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetContDesistimiento", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        lblMensaje.Append("Se presentó un Problema en el rechazo del desistimiento.! " + goInfo.mensaje_error.ToString());
                        lConexion.Cerrar();
                    }
                    else
                    {
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                                lblMensaje.Append(lLector["error"].ToString() + "<br>");
                        }
                        else
                        {
                            ToastrEvent?.Invoke("Se rechazó correctamente el desistimiento", EnumTypeToastr.Success);
                            ModalEvent?.Invoke(modContratosPdf.ID, modContratosPdfInside.ID, EnumTypeModal.Cerrar);
                            CargarDatos();
                        }
                        lConexion.Cerrar();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Append(ex.Message);
                lConexion.Cerrar();
            }
            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                ModalEvent?.Invoke(modContratosPdf.ID, modContratosPdfInside.ID, EnumTypeModal.Abrir);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void BtnVer_Click(object sender, EventArgs e)
        {
            try
            {
                string lsCarpetaAnt = "";
                string[] lsCarperta;
                lsCarperta = strRutaCarga.Split('\\');
                lsCarpetaAnt = lsCarperta[lsCarperta.Length - 2];
                var lsRuta = "../../" + lsCarpetaAnt + "/" + hndArchivo.Value.Replace(@"\", "/");
                DocumentEvent?.Invoke(lsRuta);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Error al visualizar el archivo. " + ex.Message.ToString(), EnumTypeToastr.Error);
            }
        }

    }
}