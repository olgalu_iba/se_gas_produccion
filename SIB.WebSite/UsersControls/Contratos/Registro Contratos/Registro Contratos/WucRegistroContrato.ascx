﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucRegistroContrato.ascx.cs" Inherits="UsersControls.Contratos.Registro_Contratos.Registro_Contratos.WucRegistroContrato" %>

<%--Contenido--%>
<div class="row">
    <%--20161207 rq102--%>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <label>Número Operación Inicial</label>
            <asp:TextBox ID="TxtNoContrato" type="number" runat="server" class="form-control" ValidationGroup="detalle" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <label>Número Operación Final</label>
            <asp:TextBox ID="TxtNoContratoFin" type="number" runat="server" class="form-control" ValidationGroup="detalle" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" />
            <%--20161207 rq102--%>
            <asp:HiddenField ID="hdfNoHorasPrim" runat="server" />
            <asp:HiddenField ID="hdfNoDiasSecun" runat="server" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <label>Fecha Negociación Inicial</label>
            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <label>Fecha Negociación Final</label>
            <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <label>Subasta</label>
            <asp:DropDownList ID="ddlSubasta" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
            <%--20161207 rq102--%>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <label>Mercado</label>
            <asp:DropDownList ID="ddlMercado" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                <%--20161207 rq102--%>
                <asp:ListItem Value="" Text="Seleccione"></asp:ListItem>
                <asp:ListItem Value="P" Text="Primario"></asp:ListItem>
                <asp:ListItem Value="S" Text="Secundario"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <label>Comprador</label>
            <asp:DropDownList ID="ddlComprador" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <label>Vendedor</label>
            <asp:DropDownList ID="ddlVendedor" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <label>Producto</label>
            <asp:DropDownList ID="ddlProducto" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <label>Estado</label>
            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <%--20171130 rq026-27--%>
            <label>Causa Modificación</label>
            <asp:DropDownList ID="ddlCausa" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <%--20171130 rq026-27--%>
            <label>Id Registro</label>
            <asp:TextBox ID="TxtNoId" type="number" runat="server" class="form-control" ValidationGroup="detalle" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))"></asp:TextBox>
        </div>
    </div>
</div>
<%--Grilla--%>
<div class="table table-responsive">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                Width="100%" CssClass="table-bordered" OnItemCommand="dtgConsulta_EditCommand"
                AllowPaging="true" PageSize="10" OnPageIndexChanged="dtgConsulta_PageIndexChanged">
                <Columns>
                    <%--0--%>
                    <asp:BoundColumn DataField="numero_operacion" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--1--%>
                    <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Width="100px"></asp:BoundColumn>
                    <%--2--%>
                    <asp:BoundColumn DataField="tipo_subasta" HeaderText="Tipo Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--3--%>
                    <asp:BoundColumn DataField="mercado" HeaderText="Mercado" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Width="80px"></asp:BoundColumn>
                    <%--4--%>
                    <asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Width="100px"></asp:BoundColumn>
                    <%--5--%>
                    <asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Width="100px"></asp:BoundColumn>
                    <%--6--%>
                    <asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Width="100px"></asp:BoundColumn>
                    <%--7--%>
                    <asp:BoundColumn DataField="lugar_entrega" HeaderText="Lugar Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--20170814 rq036-17--%> <%--cambio de indieces de aqui en adelante--%>
                    <%--8--%>
                    <asp:BoundColumn DataField="nombre_centro" HeaderText="Centro Poblado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--9--%>
                    <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--10--%>
                    <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--11--%>
                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                    <%--12--%>
                    <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                    <%--13--%>
                    <asp:BoundColumn DataField="fecha_limite_registro" HeaderText="Fecha Límite Registro"
                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                    <%--14--%>
                    <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--15--%>
                    <asp:BoundColumn DataField="id" Visible="false"></asp:BoundColumn>
                    <%--16--%>
                    <asp:BoundColumn DataField="operador_compra" Visible="false"></asp:BoundColumn>
                    <%--17--%>
                    <asp:BoundColumn DataField="operador_venta" Visible="false"></asp:BoundColumn>
                    <%--18--%>
                    <asp:BoundColumn DataField="codigo_tipo_subasta" Visible="false"></asp:BoundColumn>
                    <%--19--%>
                    <asp:BoundColumn DataField="tipo_doc_comprador" Visible="false"></asp:BoundColumn>
                    <%--20--%>
                    <asp:BoundColumn DataField="tipo_doc_vendedor" Visible="false"></asp:BoundColumn>
                    <%--21--%>
                    <asp:BoundColumn DataField="documento_c" Visible="false"></asp:BoundColumn>
                    <%--22--%>
                    <asp:BoundColumn DataField="documento_v" Visible="false"></asp:BoundColumn>
                    <%--23--%>
                    <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="No. Registro"></asp:BoundColumn>
                    <%--24--%>
                    <asp:BoundColumn DataField="destino_rueda" Visible="false"></asp:BoundColumn>
                    <%--25--%>
                    <asp:BoundColumn DataField="sigla_estado" Visible="false"></asp:BoundColumn>
                    <%--26--%>
                    <asp:BoundColumn DataField="tipo_mercado" Visible="false"></asp:BoundColumn>
                    <%--27 20160603-cambio fuente--%>
                    <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente" Visible="false"></asp:BoundColumn>
                    <%--28 20160603-cambio fuente--%>
                    <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente"></asp:BoundColumn>
                    <%--29--%>
                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                        <ItemTemplate>
                            <div class="dropdown dropdown-inline">
                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="flaticon-more-1"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                    <!--begin::Nav-->
                                    <ul class="kt-nav">
                                        <li class="kt-nav__item">
                                            <asp:LinkButton ID="lkbSeleccionar" CssClass="kt-nav__link" CommandName="Seleccionar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-expand"></i>
                                                            <span class="kt-nav__link-text">Seleccionar</span>
                                            </asp:LinkButton>
                                        </li>
                                        <li class="kt-nav__item">
                                            <asp:LinkButton ID="lkbVerificacion" CssClass="kt-nav__link" CommandName="Verificar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Verificar</span>
                                            </asp:LinkButton>
                                        </li>
                                    </ul>
                                    <!--end::Nav-->
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <%--30--%>
                    <asp:BoundColumn DataField="hora_negociacion" Visible="false"></asp:BoundColumn>
                    <%--31--%>
                    <asp:BoundColumn DataField="ind_verifica" Visible="false"></asp:BoundColumn>
                    <%--32--%>
                    <asp:BoundColumn DataField="ind_modifica" Visible="false"></asp:BoundColumn>
                    <%--33--%>
                    <asp:BoundColumn DataField="fecha_maxima_correccion" Visible="false"></asp:BoundColumn>
                    <%--34--%>
                    <asp:BoundColumn DataField="fecha_maxima_modificacion" Visible="false"></asp:BoundColumn>
                    <%--35--%>
                    <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad contractual" Visible="false"></asp:BoundColumn>
                    <%--36--%>
                    <asp:BoundColumn DataField="fecha_inicial" Visible="false"></asp:BoundColumn>
                    <%--37--%>
                    <asp:BoundColumn DataField="fecha_final" Visible="false"></asp:BoundColumn>
                    <%--38--%>
                    <asp:BoundColumn DataField="codigo_modalidad" Visible="false"></asp:BoundColumn>
                    <%--39--%>
                    <asp:BoundColumn DataField="codigo_punto_entrega" Visible="false"></asp:BoundColumn>
                    <%--40--%>
                    <asp:BoundColumn DataField="cantiad_cnt" Visible="false"></asp:BoundColumn>
                    <%--41--%>
                    <asp:BoundColumn DataField="precio_cnt" Visible="false"></asp:BoundColumn>
                    <%--42--%>
                    <asp:BoundColumn DataField="codigo_ruta" Visible="false"></asp:BoundColumn>
                    <%--20170814 rq036-17--%>
                    <%--43--%>
                    <asp:BoundColumn DataField="ind_bilateral" Visible="false"></asp:BoundColumn>
                    <%--20170814 rq036-17--%>
                    <%--44--%>
                    <asp:BoundColumn DataField="conectado_snt" Visible="false"></asp:BoundColumn>
                    <%--20170814 rq036-17--%>
                    <%--45--%>
                    <asp:BoundColumn DataField="ent_boca_pozo" Visible="false"></asp:BoundColumn>
                    <%--20170814 rq036-17--%>
                    <%--46--%>
                    <asp:BoundColumn DataField="codigo_centro_pob" Visible="false"></asp:BoundColumn>
                    <%--20171130 rq026-17--%>
                    <%--47--%>
                    <asp:BoundColumn DataField="codigo_periodo_reg" Visible="false"></asp:BoundColumn>
                    <%--20171130 rq026-17--%>
                    <%--48--%>
                    <asp:BoundColumn DataField="ind_contrato_var" Visible="false"></asp:BoundColumn>
                    <%--20171130 rq026-17--%>
                    <%--49--%>
                    <asp:BoundColumn DataField="fecha_suscripcion" Visible="false"></asp:BoundColumn>
                    <%--20171130 rq026-17--%>
                    <%--50--%>
                    <asp:BoundColumn DataField="contrato_definitivo" Visible="false"></asp:BoundColumn>
                    <%--20171130 rq026-17--%>
                    <%--51--%>
                    <asp:BoundColumn DataField="desc_causa" HeaderText="Causa Modificación"></asp:BoundColumn>
                    <%--52--%> <%--20201124--%>
                    <asp:BoundColumn DataField="sentido_flujo" Visible="false"></asp:BoundColumn>
                    <%--53--%> <%--20201124--%>
                    <asp:BoundColumn DataField="presion_punto_fin" Visible="false"></asp:BoundColumn>
                    <%--54--%> <%--20201124--%>
                    <asp:BoundColumn DataField="codigo_clasificacion" HeaderText="Clasificación transporte"></asp:BoundColumn>
                    <%--55--%> <%--20210305 broker--%>
                    <asp:BoundColumn DataField="ind_broker" Visible="false"></asp:BoundColumn>
                    <%--56--%> <%--20210305 broker--%> <%--20210915--%>
                    <asp:BoundColumn DataField="numero_id_tra" HeaderText="Id MP Contrato MS Transporte"></asp:BoundColumn>
                    <%--57--%> <%--20210707--%>
                    <asp:BoundColumn DataField="codigo_trm" HeaderText="Código Tipo TRM"></asp:BoundColumn>
                    <%--58 20210707 trm moneda--%>
                    <asp:BoundColumn DataField="desc_trm" HeaderText="Tipo TRM"></asp:BoundColumn>
                    <%--59--%> <%--20210707--%>
                    <asp:BoundColumn DataField="observacion_trm" HeaderText="Observación TRM"></asp:BoundColumn>
                    <%--60--%> <%--20210707--%>
                    <asp:BoundColumn DataField="tipo_moneda" HeaderText="Moneda"></asp:BoundColumn>
                    <%--20210915--%><%--61--%>
                    <asp:BoundColumn DataField="ind_contrato_var" HeaderText="Contrato Variable"></asp:BoundColumn>
                    <%--20220418--%><%--62--%>
                    <asp:BoundColumn DataField="cambio_datos" visible ="false" ></asp:BoundColumn>
                    <%--20220418--%><%--63--%>
                    <asp:BoundColumn DataField="permite_rechazo" visible ="false" ></asp:BoundColumn>
                </Columns>
                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
            </asp:DataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%-- Impresión  --%>
</div>
<%--Excel--%>
<asp:DataGrid ID="dtgConsultaExcel" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
    Width="100%" CssClass="table-bordered" Visible="false">
    <Columns>
        <asp:BoundColumn DataField="numero_operacion" HeaderText="No. Operación"></asp:BoundColumn>
        <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación"></asp:BoundColumn>
        <asp:BoundColumn DataField="hora_negociacion" HeaderText="Hora Negociación"></asp:BoundColumn>
        <asp:BoundColumn DataField="tipo_subasta" HeaderText="Tipo Subasta"></asp:BoundColumn>
        <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Contrato"></asp:BoundColumn>
        <asp:BoundColumn DataField="mercado" HeaderText="Mercado"></asp:BoundColumn>
        <asp:BoundColumn DataField="producto" HeaderText="Producto"></asp:BoundColumn>
        <asp:BoundColumn DataField="comprador" HeaderText="Nombre Comprador"></asp:BoundColumn>
        <asp:BoundColumn DataField="operador_compra" HeaderText="Código Comprador"></asp:BoundColumn>
        <asp:BoundColumn DataField="rol_comprador" HeaderText="Rol Comprador"></asp:BoundColumn>
        <asp:BoundColumn DataField="vendedor" HeaderText="Nombre Vendedor"></asp:BoundColumn>
        <asp:BoundColumn DataField="operador_venta" HeaderText="Código Vendedor"></asp:BoundColumn>
        <asp:BoundColumn DataField="rol_vendedor" HeaderText="Rol Vendedor"></asp:BoundColumn>
        <%--20170814 rq036-17--%>
        <asp:BoundColumn DataField="codigo_punto_entrega" HeaderText="Código Entrega"></asp:BoundColumn>
        <asp:BoundColumn DataField="lugar_entrega" HeaderText="Lugar Entrega"></asp:BoundColumn>
        <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial"></asp:BoundColumn>
        <%--20161207 rq102--%>
        <asp:BoundColumn DataField="hora_ini" HeaderText="Hora Inicial"></asp:BoundColumn>
        <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final"></asp:BoundColumn>
        <%--20161207 rq102--%>
        <asp:BoundColumn DataField="hora_fin" HeaderText="Hora Final"></asp:BoundColumn>
        <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad"></asp:BoundColumn>
        <asp:BoundColumn DataField="precio" HeaderText="Precio"></asp:BoundColumn>
        <asp:BoundColumn DataField="fecha_limite_registro" HeaderText="Fecha Límite Registro"></asp:BoundColumn>
        <asp:BoundColumn DataField="estado" HeaderText="Estado"></asp:BoundColumn>
        <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="No. Registro"></asp:BoundColumn>
        <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad contractual"></asp:BoundColumn>
        <%--20180126 rq107-16--%>
        <asp:BoundColumn DataField="ind_extemporaneo" HeaderText="Actualización/Extemporánea"></asp:BoundColumn>
        <%--20160603-cambio fuente--%>
        <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente"></asp:BoundColumn>
        <%--20160603-cambio fuente--%>
        <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente"></asp:BoundColumn>
        <%--20160711--%>
        <%--20161207 rq102--%>
        <asp:BoundColumn DataField="presion_punto_fin" HeaderText="Presión"></asp:BoundColumn>
        <%--20161207 rq102--%>
        <asp:BoundColumn DataField="sentido_flujo" HeaderText="Sentido Flujo"></asp:BoundColumn>
        <%--20161207 rq102--%>
        <asp:BoundColumn DataField="codigo_periodo" HeaderText="Cd Periodo"></asp:BoundColumn>
        <%--20161207 rq102--%>
        <asp:BoundColumn DataField="desc_periodo" HeaderText="Periodo Entrega"></asp:BoundColumn>
        <%--20170814 rq036-17--%>
        <asp:BoundColumn DataField="conectado_snt" HeaderText="Ind Conectado SNT"></asp:BoundColumn>
        <%--20170814 rq036-17--%>
        <asp:BoundColumn DataField="ent_boca_pozo" HeaderText="Ind Entrega Boca Pozo"></asp:BoundColumn>
        <%--20170814 rq036-17--%>
        <%--20180126 rq107-16--%>
        <asp:BoundColumn DataField="codigo_centro_pob" HeaderText="Código Centro"></asp:BoundColumn>
        <%--20170814 rq036-17--%>
        <asp:BoundColumn DataField="nombre_centro" HeaderText="Centro Poblado"></asp:BoundColumn>
        <%--20171130 rq026-17--%>
        <asp:BoundColumn DataField="desc_causa" HeaderText="Causa Modificación"></asp:BoundColumn>
        <%--20200727 ajsutes front end--%>
        <asp:BoundColumn DataField="codigo_trm" HeaderText="Código Tipo TRM"></asp:BoundColumn>
        <asp:BoundColumn DataField="desc_trm" HeaderText="Tipo TRM"></asp:BoundColumn>
        <%--20210707 trm moneda--%>
        <asp:BoundColumn DataField="tipo_moneda" HeaderText="Moneda"></asp:BoundColumn>
        <%--20220505--%>
        <asp:BoundColumn DataField="observacion_trm" HeaderText="Observaciones"></asp:BoundColumn>
        <%--20200727 ajsutes front end--%>
        <%--20200803 res 138--%>
        <asp:BoundColumn DataField="fecha_hora_verifica" HeaderText="Fecha Registro"></asp:BoundColumn>
        <%--20201124--%>
        <asp:BoundColumn DataField="clasif_transporte" HeaderText="Clasificación Transporte"></asp:BoundColumn>
        <%--55--%> <%--20210305 broker--%>
        <asp:BoundColumn DataField="ind_broker" HeaderText="Contrato Broker"></asp:BoundColumn>
        <%--56--%> <%--20210305 broker--%>
        <asp:BoundColumn DataField="numero_id_tra" HeaderText="Id MP Contrato MS Transporte"></asp:BoundColumn>
        <%--20210915--%>
        <asp:BoundColumn DataField="ind_contrato_var" HeaderText="Contrato Variable"></asp:BoundColumn>
        <%--20220505--%>
        <asp:BoundColumn DataField="observacion_sol" HeaderText="Obs. Solicitud Desistimiento"></asp:BoundColumn>
    </Columns>
    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
</asp:DataGrid>

<%--Modals--%>
<%--Registro/Modificacion--%>
<div class="modal fade" id="registroContratoMod" tabindex="-1" role="dialog" aria-labelledby="mdlRegistroContratoLabel" aria-hidden="true" clientidmode="Static" runat="server">
    <div class="modal-dialog" id="registroContratoModInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="mdlRegistroContratoLabel" runat="server">Registro</h5>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <%--Infirmacion Basica--%>
                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5>Información Básica</h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Número Operación Inicial</label>
                                                    <%--20201124--%>
                                                    <asp:TextBox ID="lblOperacion" disabled runat="server" ForeColor="#0066FF" CssClass="form-control" />
                                                    <asp:HiddenField ID="hdfPunta" runat="server" />
                                                    <asp:HiddenField ID="hdfIdVerif" runat="server" />
                                                    <asp:HiddenField ID="hdfCodTipoSub" runat="server" />
                                                    <asp:HiddenField ID="hdfRutaArchivo" runat="server" />
                                                    <asp:HiddenField ID="hdfEstadoAct" runat="server" />
                                                    <asp:HiddenField ID="hdfAccion" runat="server" />
                                                    <asp:HiddenField ID="hdfCodVerifCont" runat="server" />
                                                    <asp:HiddenField ID="hdfOPeraC" runat="server" />
                                                    <asp:HiddenField ID="hdfOPeraV" runat="server" />
                                                    <asp:HiddenField ID="hdfCantCnt" runat="server" />
                                                    <%--20220418--%>
                                                    <asp:HiddenField ID="hdfCambioDatos" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Punta</label>
                                                    <asp:TextBox ID="lblPunta" disabled runat="server" ForeColor="#0066FF"  CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Subasta</label>
                                                    <asp:TextBox ID="lblSubasta" disabled runat="server" ForeColor="#0066FF" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Producto</label>
                                                    <asp:TextBox ID="lblProducto" disabled runat="server" ForeColor="#0066FF" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="lnlNomOpera" runat="server"></asp:Label>
                                                    <asp:TextBox ID="lblNomOperador" disabled runat="server" ForeColor="#0066FF" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="lblTipoDocOpera" runat="server"></asp:Label>
                                                    <asp:TextBox ID="lblTipoDocOperador" disabled runat="server" ForeColor="#0066FF" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="lblNoDocOPera" runat="server"></asp:Label>
                                                    <asp:TextBox ID="lblNoDocumentoOperaqdor" disabled runat="server" ForeColor="#0066FF" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-sm-12 col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <label>Número Contrato <%--20180126 rq107-16--%></label>
                                                    <asp:TextBox ID="TxtNumContrato" runat="server" class="form-control" ValidationGroup="detalle" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <label>Fecha Suscripción</label>
                                                    <asp:TextBox ID="TxtFechaSus" Width="100%" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                                <asp:Label Text="Modalidad de Contrato" AssociatedControlID="ddlModalidad" runat="server" />
                                                <div class="form-group row">
                                                    <div class="col-sm-12 col-md-4 col-lg-5">
                                                        <asp:DropDownList ID="ddlModalidad" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                    </div>
                                                    <div class="col-sm-12 col-md-4 col-lg-3">
                                                        <div class="form-group row">
                                                            <div class="col-sm-12 col-md-6 col-lg-8">
                                                                <input type="text" readonly class="form-control-plaintext" id="unpOtro" value="Otro">
                                                            </div>
                                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                                <label class="kt-checkbox">
                                                                    <asp:CheckBox ID="ChkOtrModa" runat="server" />
                                                                    <span></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-4 col-lg-4">
                                                        <asp:TextBox ID="TxtOtrModalidad" class="form-control" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <%--20210707 trm y moneda--%>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label>Tasa de Cambio</asp:Label>
                                                    <asp:DropDownList ID="ddlTrm" runat="server" CssClass="form-control" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <%--20210707 trm y moneda--%>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label>Observaciones Tasa de Cambio</asp:Label>
                                                    <asp:TextBox ID="TxtObsTrm" runat="server" CssClass="form-control" MaxLength="300" />
                                                </div>
                                            </div>
                                            <%--20210707 trm y moneda--%>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label>Tipo Moneda</asp:Label>
                                                    <asp:DropDownList ID="ddlMoneda" runat="server" CssClass="form-control" data-live-search="true">
                                                        <asp:ListItem Value="USD" Text="USD"></asp:ListItem>
                                                        <asp:ListItem Value="COP" Text="COP"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <%--20170814 rq036-17--%>
                                        <div class="row" id="trSum" visible="false" runat="server">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Punto entrega Conectado al SNT?</label>
                                                    <asp:DropDownList ID="ddlConecSnt" runat="server" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="ddlConecSnt_SelectedIndexChanged"
                                                        AutoPostBack="true">
                                                        <asp:ListItem Value="S">Si</asp:ListItem>
                                                        <asp:ListItem Value="N">No</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Entrega en boca de pozo?</label>
                                                    <asp:DropDownList ID="ddlEntBocPozo" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlEntBocPozo_SelectedIndexChanged"
                                                        AutoPostBack="true">
                                                        <asp:ListItem Value="S">Si</asp:ListItem>
                                                        <asp:ListItem Value="N">No</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="lblCentro" runat="server">Centro problado</asp:Label>
                                                    <asp:DropDownList ID="ddlCentro" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="lblPuntoE" runat="server"></asp:Label>
                                                    <asp:DropDownList ID="ddlPuntoEntr" runat="server" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="ddlPuntoEntr_SelectedIndexChanged" AutoPostBack="true" />
                                                    <label class="kt-checkbox" visible="False" runat="server">
                                                        <asp:CheckBox ID="ChkOtrPuntoE" runat="server" Visible="false" />
                                                        <span></span>
                                                    </label>
                                                    <asp:TextBox ID="TxtOtrPuntoE" class="form-control" runat="server" Visible="false" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="lblCantidad" runat="server"></asp:Label>
                                                    <asp:TextBox ID="TxtCantidad" runat="server" class="form-control" ValidationGroup="detalle" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="lblPrecio" runat="server"></asp:Label>
                                                    <asp:TextBox ID="TxtPrecio" runat="server" class="form-control" ValidationGroup="detalle" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="lblFechaInc" runat="server"></asp:Label>
                                                    <asp:TextBox ID="TxtFechaInicial" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>
                                                    -
                                                   
                                                   

                                                    <asp:TextBox ID="TxtHoraInicial" runat="server" class="form-control" ValidationGroup="detalle" MaxLength="5"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RevTxtHoraInicial" ControlToValidate="TxtHoraInicial"
                                                        runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                                                        ErrorMessage="Formato Incorrecto para la hora inicial "> * </asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="lblFechaFin" runat="server"></asp:Label>
                                                    <asp:TextBox ID="TxtFechaFinal" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>
                                                    -
                                                   
                                                   

                                                    <asp:TextBox ID="TxtHoraFinal" runat="server" class="form-control" ValidationGroup="detalle" MaxLength="5"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RevTxtHoraFinal" ControlToValidate="TxtHoraFinal"
                                                        runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                                                        ErrorMessage="Formato Incorrecto para la hora final "> * </asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <%--20210707--%>
                                            <%--<div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Copia del Contrato</label>
                                                    <asp:FileUpload ID="FuCopiaCont" CssClass="form-control" runat="server" EnableTheming="true" />
                                                </div>
                                            </div>--%>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Periodo de entrega</label>
                                                    <asp:DropDownList ID="ddlPeriodo" runat="server" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="ddlPeriodo_SelectedIndexChanged" AutoPostBack="true" />

                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Indicador contrato variable</label>
                                                    <asp:DropDownList ID="ddlIndVar" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="N" Text="NO"></asp:ListItem>
                                                        <asp:ListItem Value="S" Text="SI"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                        </div>
                                        <%--20170814 rq036-17--%>
                                        <div class="row" id="TrTrans" visible="false" runat="server">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Sentido Contratado para el flujo de Gas Natural</label>
                                                    <asp:DropDownList ID="ddlSentidoFlujo" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="" Text="Seleccione"></asp:ListItem>
                                                        <asp:ListItem Value="NORMAL" Text="NORMAL"></asp:ListItem>
                                                        <asp:ListItem Value="CONTRAFLUJO" Text="CONTRA FLUJO"></asp:ListItem>
                                                        <%--20201124--%>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Presión para el punto de terminación del servicio (psig)</label>
                                                    <asp:TextBox ID="TxtPresion" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                                                </div>
                                            </div>
                                            <%--20201124--%>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label AssociatedControlID="DdlClasifica" runat="server">Clasificación contrato transporte</asp:Label>
                                                    <asp:DropDownList ID="DdlClasifica" CssClass="form-control selectpicker" data-live-search="true" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <%--20160601 campo o fuente MP--%>
                                        <div class="row" id="TrFuente" visible="false" runat="server">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Fuente</label>
                                                    <asp:DropDownList ID="ddlFuente" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <%--20210305 broker--%>
                                        <div class="row" id="trMsTra" visible="false" runat="server">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Ids MP Transporte, Separados por guiones(-)</label>
                                                    <asp:TextBox ID="TxtIdTraMp" runat="server" CssClass="form-control" />
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtIdTraMp" runat="server" TargetControlID="TxtIdTraMp"
                                                        FilterType="Custom, Numbers" ValidChars="-"></ajaxToolkit:FilteredTextBoxExtender>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" id="TrModifi" visible="false" runat="server">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <%--20171130 rq026-27--%>
                                                    <label>Motivo Modificación</label>
                                                    <asp:DropDownList ID="ddlMotivoModifi" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                </div>
                                            </div>
                                            <%--20220418--%>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Observaciones modificación</label>
                                                    <asp:TextBox ID="txtObservaModif" runat="server" CssClass="form-control" MaxLength ="1000"/>
                                                </div>
                                            </div>
                                            <%--20210224--%>
                                            <%--<div class="col-sm-12 col-md-6 col-lg-4" visible="false">
                                                <div class="form-group">
                                                    <label>Cesionario</label>
                                                    <asp:DropDownList ID="ddlCesionario" runat="server" CssClass="form-control" />
                                                </div>
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--Usuarios Finales--%>
                            <div id="divDemanda" visible="False" class="panel-group" runat="server">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5>Usuarios Finales</h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Tipo Demanda a Atender</label>
                                                    <asp:DropDownList ID="dlTipoDemanda" runat="server" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="dlTipoDemanda_SelectedIndexChanged" AutoPostBack="true" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Nit Usuario Final</label>
                                                    <%--20201020--%>
                                                    <asp:TextBox ID="TxtUsuarioFinal" runat="server" CssClass="form-control" autocomplete="true"/>
                                                    <ajaxToolkit:AutoCompleteExtender runat="server" CompletionListElementID="ListDivisor" BehaviorID="AutoCompleteExUsuF" ID="AutoCompleteExtender1"
                                                        TargetControlID="TxtUsuarioFinal" ServicePath="~/WebService/AutoComplete.asmx"
                                                        ServiceMethod="GetCompletionListUsuarioFinal" MinimumPrefixLength="3" CompletionInterval="1000"
                                                        EnableCaching="true" CompletionSetCount="20" CompletionListCssClass="autocomplete_completionListElement"
                                                        CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                        DelimiterCharacters=";,:">  
                                                        <Animations>
                                                                    <OnShow>
                                                                        <Sequence>
                                                                            <%-- Make the completion list transparent and then show it --%>
                                                                            <OpacityAction Opacity="0" />
                                                                            <HideAction Visible="true" />
                                                                            
                                                                            <%--Cache the original size of the completion list the first time
                                                                                the animation is played and then set it to zero --%>
                                                                            <ScriptAction Script="
                                                                                // Cache the size and setup the initial size
                                                                                var behavior = $find('AutoCompleteExUsuF');
                                                                                if (!behavior._height) {
                                                                                    var target = behavior.get_completionList();
                                                                                    behavior._height = target.offsetHeight - 2;
                                                                                    target.style.height = '0px';
                                                                                }" />
                                                                            
                                                                            <%-- Expand from 0px to the appropriate size while fading in --%>
                                                                            <Parallel Duration=".4">
                                                                                <FadeIn />
                                                                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteExUsuF')._height" />
                                                                            </Parallel>
                                                                        </Sequence>
                                                                    </OnShow>
                                                                    <OnHide>
                                                                    <%-- Collapse down to 0px and fade out --%>
                                                                    <Parallel Duration=".4">
                                                                        <FadeOut />
                                                                        <Length PropertyKey="height" StartValueScript="$find('AutoCompleteExUsuF')._height" EndValue="0" />
                                                                    </Parallel>
                                                                    </OnHide>
                                                        </Animations>
                                                    </ajaxToolkit:AutoCompleteExtender>
                                                    <%--20220505 --%>
                                                    <div ID="ListDivisor"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="lblSector" Text="Sector de Consumo Usuario Regulado" runat="server"></asp:Label>
                                                    <asp:DropDownList ID="ddlSector" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Punto de Salida en SNT</label>
                                                    <asp:DropDownList ID="ddlPuntoSalida" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                    <asp:HiddenField ID="hdfTipoDemanda" runat="server" />
                                                    <asp:HiddenField ID="hdfCodDatUsu" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Mercado Relevante</label>
                                                    <asp:DropDownList ID="ddlMercadoRel" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="lblCantContra" Text="Cantidad a entregar" runat="server"></asp:Label>
                                                    <asp:TextBox ID="TxtCantidadUsu" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4" runat="server" id="TdIndica">
                                                <div class="form-group">
                                                    <asp:Label runat="server">Equivalente Kpcd</asp:Label>
                                                    <asp:TextBox ID="TxtEquivaleKpcd" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <asp:Button ID="btnCrearUsu" runat="server" CssClass="btn btn-primary btn-block" Text="Agregar Usuario" OnClick="btnCrearUsu_Click" Visible="false" />
                                        <asp:Button ID="btnActualUsu" runat="server" CssClass="btn btn-primary btn-lg btn-block" Text="Actualiza Usuario" OnClick="btnActualUsu_Click" Visible="false" />

                                        <hr>

                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <b>TOTAL CANTIDAD:</b><asp:Label ID="lblTotlCantidad" runat="server" ForeColor="Red" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table table-responsive">
                                            <asp:DataGrid ID="dtgUsuarios" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                                Width="100%" CssClass="table-bordered" OnItemCommand="dtgUsuarios_EditCommand">
                                                <Columns>
                                                    <%--0--%>
                                                    <asp:BoundColumn DataField="tipo_demanda" HeaderText="Tipo Demanda Atender" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--1--%>
                                                    <asp:BoundColumn DataField="no_identificacion_usr" HeaderText="Identificación Usuario Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--2--%>
                                                    <asp:BoundColumn DataField="tipo_documento" HeaderText="Tipo Identificación Usuario Final"
                                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                                                    <%--3--%>
                                                    <asp:BoundColumn DataField="nombre_usuario" HeaderText="Nombre Usuario Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--4--%>
                                                    <asp:BoundColumn DataField="sector_consumo" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="80px"></asp:BoundColumn>
                                                    <%--5--%>
                                                    <asp:BoundColumn DataField="punto_salida" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left" />
                                                    <%--6--%>
                                                    <asp:BoundColumn DataField="cantidad_contratada" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Left" />
                                                    <%--Campo Nuevo Req. 009-17 Indsicadores 20170321--%>
                                                    <asp:BoundColumn DataField="equivalente_kpcd" HeaderText="Equivalente Kpcd" ItemStyle-HorizontalAlign="Left " Visible="false" />
                                                    <%--7--%>
                                                    <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" Visible="False"></asp:EditCommandColumn>
                                                    <%--8--%>
                                                    <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar" Visible="False"></asp:EditCommandColumn>
                                                    <%--9--%>
                                                    <asp:BoundColumn DataField="codigo_datos_usuarios" Visible="false"></asp:BoundColumn>
                                                    <%--10--%>
                                                    <asp:BoundColumn DataField="codigo_cont_datos" Visible="false"></asp:BoundColumn>
                                                    <%--11--%>
                                                    <asp:BoundColumn DataField="codigo_sector_consumo" Visible="false"></asp:BoundColumn>
                                                    <%--12--%>
                                                    <asp:BoundColumn DataField="codigo_punto_salida" Visible="false"></asp:BoundColumn>
                                                    <%--13--%>
                                                    <asp:BoundColumn DataField="codigo_tipo_doc" Visible="false"></asp:BoundColumn>
                                                    <%--14--%>
                                                    <asp:BoundColumn DataField="codigo_tipo_demanda" Visible="false"></asp:BoundColumn>
                                                    <%--15--%>
                                                    <asp:BoundColumn DataField="codigo_mercado_relevante" Visible="false"></asp:BoundColumn>
                                                    <%--16--%>
                                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                                        <ItemTemplate>
                                                            <div class="dropdown dropdown-inline">
                                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="flaticon-more-1"></i>
                                                                </button>
                                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                                    <!--begin::Nav-->
                                                                    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <ul class="kt-nav">
                                                                                <li class="kt-nav__item">
                                                                                    <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                                                         <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                                                        <span class="kt-nav__link-text">Modificar</span>
                                                                                    </asp:LinkButton>
                                                                                </li>
                                                                                <li class="kt-nav__item">
                                                                                    <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                                                        <i class="kt-nav__link-icon flaticon-delete"></i>
                                                                                        <span class="kt-nav__link-text">Eliminar</span>
                                                                                    </asp:LinkButton>
                                                                                </li>
                                                                            </ul>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                    <!--end::Nav-->
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <%--<button type="button" class="btn btn-secondary" data-dismiss="modal" >Cancelar</button>--%> <%--20201014--%>
                        <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" CssClass="btn btn-secondary" OnClientClick="return confirm('Está seguro de cancelar el proceso?')" /><%--20201014--%>
                        <asp:Button ID="btnCrear" runat="server" CssClass="btn btn-primary" Text="Insertar" OnClick="btnCrear_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnActualizar" runat="server" CssClass="btn btn-primary" Text="Actualizar" OnClick="btnActualizar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <%--20220418--%>
                        <asp:Button ID="btnRechazar" runat="server" CssClass="btn btn-primary" Text="Rechazar" OnClick="btnRechazar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" Visible ="false"  />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>

<%--Verificacion--%>
<div class="modal fade" id="registroContratoVer" tabindex="-1" role="dialog" aria-labelledby="mdlregistroContratoVerLabel" aria-hidden="true" clientidmode="Static" runat="server">
    <div class="modal-dialog" id="registroContratoVerInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="mdlregistroContratoVerLabel">Verificación</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h5>
                            <asp:Label ID="lblVerificacion" runat="server"></asp:Label>
                        </h5>

                        <hr>

                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label><b>VERIFICACION REGISTRO</b></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label>Dato</label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label>Sistema</label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label>Comprador</label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label>Vendedor</label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label>Verificación</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label></label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTit1" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat1C" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat1V" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat1Ver" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label></label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTit2" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat2C" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat2V" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat2Ver" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTit3" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat3Op" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat3C" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat3V" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat3Ver" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTit4" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat4Op" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat4C" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat4V" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat4Ver" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTit5" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat5Op" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat5C" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat5V" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat5Ver" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTit6" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat6Op" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat6C" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat6V" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat6Ver" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTit7" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat7Op" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat7C" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat7V" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat7Ver" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTit8" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat8Op" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat8C" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat8V" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat8Ver" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row" runat="server" id="TrTra1" visible="false">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTit9" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat9C" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat9V" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat9Ver" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row" runat="server" id="TrTra2" visible="false">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTit10" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat10C" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat10V" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDat10Ver" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTit11" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="Label4" runat="server" class="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <a id="LinkC" target="_blank" runat="server" class="btn btn-outline-brand btn-square">Ver Contrato</a>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <a id="LinkV" target="_blank" runat="server" class="btn btn-outline-brand btn-square">Ver Contrato</a>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="Label7" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="Observación" runat="server"></asp:Label>
                                    <asp:TextBox ID="TxtObservacion" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <asp:Button ID="btnSolCorr" runat="server" CssClass="btn btn-primary" Text="Solicitar Correccion" OnClick="btnSolCorr_Click" />
                        <asp:Button ID="btnRegCont" runat="server" CssClass="btn btn-primary" Text="Registro Contrato" OnClick="btnRegCont_Click" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>


<%--20210224 pasa validaciones--%>
<div class="modal fade" id="mdlConfirma" tabindex="-1" role="dialog" aria-labelledby="mdlConfirmaLabel" aria-hidden="true" clientidmode="Static" runat="server">
    <div class="modal-dialog" id="mdlConfirmaInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                <ContentTemplate>
                    <div class="modal-header" style="background-color: #3E5F8A;">
                        <h5 class="modal-title" id="mdlConfirmaLabel" runat="server" style="color: white;">Confirmación</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff; opacity: 1;">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="background-color: #D3D3D3">
                        <asp:Label ID="lblMensajeCOnf" runat="server">Se presentan las siguientes validaciones:</asp:Label>
                    </div>
                    <div class="modal-body" style="background-color: #D3D3D3">
                        <asp:Label ID="lblValidacion" runat="server"></asp:Label>
                    </div>
                    <div class="modal-body" style="background-color: #D3D3D3">
                        <asp:Label ID="lblConforma" runat="server">Desea continuar de todas formas?</asp:Label>
                    </div>
                    <div class="modal-footer" style="background-color: #3E5F8A">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <asp:Button ID="btnAceptarConf" CssClass="btn btn-primary" Text="Aceptar" OnClick="BtnAceptarConf_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
