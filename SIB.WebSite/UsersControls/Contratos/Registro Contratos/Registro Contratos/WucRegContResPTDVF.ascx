﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucRegContResPTDVF.ascx.cs" Inherits="UsersControls.Contratos.Registro_Contratos.Registro_Contratos.WucRegContResPTDVF" %>

<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="ID Inicial" AssociatedControlID="TxtNoContrato" runat="server" />
            <asp:TextBox ID="TxtNoContrato" runat="server" class="form-control" ValidationGroup="detalle" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="ID Final" AssociatedControlID="TxtNoContratoFin" runat="server" />
            <asp:TextBox ID="TxtNoContratoFin" runat="server" class="form-control" ValidationGroup="detalle" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
            <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Fuente" AssociatedControlID="ddlFuente" runat="server" />
            <asp:DropDownList ID="ddlFuente" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Estado" AssociatedControlID="ddlEstado" runat="server" />
            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
</div>
<div class="table table-responsive">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:DataGrid ID="dtgConsulta" Visible="False" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                Width="100%" CssClass="table-bordered"
                OnItemCommand="dtgConsulta_EditCommand" AllowPaging="true" PageSize="10" OnPageIndexChanged="dtgConsulta_PageIndexChanged">
                <Columns>
                    <%--0--%>
                    <asp:BoundColumn DataField="numero_contrato" HeaderText="ID" ItemStyle-HorizontalAlign="Left" />
                    <%--1--%>
                    <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}" />
                    <%--2--%>
                    <asp:BoundColumn DataField="operador_venta" HeaderText="Operador Venta" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px" />
                    <%--3--%>
                    <asp:BoundColumn DataField="nombre_venta" HeaderText="vendedor" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px" />
                    <%--4--%>
                    <asp:BoundColumn DataField="operador_compra" HeaderText="Operador Compra" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px" />
                    <%--5--%>
                    <asp:BoundColumn DataField="nombre_compra" HeaderText="comprador" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px" />
                    <%--6--%>
                    <asp:BoundColumn DataField="codigo_fuente" HeaderText="Codigo Fuente" ItemStyle-HorizontalAlign="Left" />
                    <%--7--%>
                    <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left" />
                    <%--8--%>
                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}" />
                    <%--9--%>
                    <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0.00}" />
                    <%--10--%>
                    <asp:BoundColumn DataField="fecha_max_registro" HeaderText="Fecha Limite Registro" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px" />
                    <%--11--%>
                    <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left" />
                    <%--12--%>
                    <asp:BoundColumn DataField="estado" Visible="false" />
                    <%--13--%>
                    <asp:BoundColumn DataField="punta" Visible="false" />
                    <%--14--%>
                    <asp:BoundColumn DataField="cantidad" Visible="false" />
                    <%--15--%>
                    <asp:BoundColumn DataField="contrato_definitivo" Visible="false" />
                    <%--16--%>
                    <asp:BoundColumn DataField="fecha_suscripcion" Visible="false" />
                    <%--17--%>
                    <asp:BoundColumn DataField="ind_reg" Visible="false" />
                    <%--18--%>
                    <asp:BoundColumn DataField="codigo_verif" Visible="false" />
                    <%--19--%>
                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                        <ItemTemplate>
                            <div class="dropdown dropdown-inline">
                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="flaticon-more-1"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                    <!--begin::Nav-->
                                    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <asp:LinkButton ID="lkbSeleccionar" CssClass="kt-nav__link" CommandName="Seleccionar" runat="server">
                                                        <i class="kt-nav__link-icon flaticon2-expand"></i>
                                                        <span class="kt-nav__link-text">Seleccionar</span>
                                                    </asp:LinkButton>
                                                </li>
                                            </ul>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <!--end::Nav-->
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
            </asp:DataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--20190607 rq036-19--%>
</div>
<asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False" Visible="false"
    Width="100%" CssClass="table-bordered">
    <Columns>
        <asp:BoundColumn DataField="numero_contrato" HeaderText="ID" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
        <asp:BoundColumn DataField="operador_venta" HeaderText="Operador Venta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <asp:BoundColumn DataField="nombre_venta" HeaderText="vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <asp:BoundColumn DataField="operador_compra" HeaderText="Operador Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <asp:BoundColumn DataField="nombre_compra" HeaderText="comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <asp:BoundColumn DataField="codigo_fuente" HeaderText="Codigo Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
        <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
        <asp:BoundColumn DataField="fecha_max_registro" HeaderText="Fecha Limite Registro" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
        <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
    </Columns>
    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
</asp:DataGrid>

<%--Modals--%>
<%--Registro/Modificacion--%>
<div class="modal fade" id="registroContratoMod" tabindex="-1" role="dialog" aria-labelledby="mdlRegistroContratoLabel" aria-hidden="true" clientidmode="Static" runat="server">
    <div class="modal-dialog" id="registroContratoModInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="mdlRegistroContratoLabel">Registro</h5>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <%--Infirmacion Basica--%>
                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5>Información Básica</h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="ID" AssociatedControlID="lblcontrato" runat="server" />
                                                    <asp:Label ID="lblcontrato" runat="server" CssClass="form-control"></asp:Label>-<asp:Label ID="lblPunta" runat="server" CssClass="form-control"></asp:Label>
                                                    <asp:HiddenField ID="hdfPunta" runat="server" />
                                                    <asp:HiddenField ID="hdfCodigoVer" runat="server" />
                                                    <asp:HiddenField ID="hdfCodigoVerUsr" runat="server" />
                                                    <asp:HiddenField ID="hdfCantidad" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Fecha Negociación" AssociatedControlID="lblFechaNeg" runat="server" />
                                                    <asp:Label ID="lblFechaNeg" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Comprador" AssociatedControlID="lblComprador" runat="server" />
                                                    <asp:Label ID="lblComprador" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Vendedor" AssociatedControlID="lblVendedor" runat="server" />
                                                    <asp:Label ID="lblVendedor" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Fuente" AssociatedControlID="lblFuente" runat="server" />
                                                    <asp:Label ID="lblFuente" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Cantidad" AssociatedControlID="lblCantidad" runat="server" />
                                                    <asp:Label ID="lblCantidad" runat="server" CssClass="form-control"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Número Contrato" AssociatedControlID="TxtNumContrato" runat="server" />
                                                    <asp:TextBox ID="TxtNumContrato" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Fecha Suscripción" AssociatedControlID="TxtFechaSus" runat="server" />
                                                    <asp:TextBox ID="TxtFechaSus" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control" ClientIDMode="Static" MaxLength="10" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--Usuarios Finales--%>
                            <div id="divDemanda" class="panel-group" runat="server">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5>Usuarios Finales</h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Tipo Demanda a Atender" AssociatedControlID="dlTipoDemanda" runat="server" />
                                                    <asp:DropDownList ID="dlTipoDemanda" runat="server" CssClass="form-control selectpicker" OnSelectedIndexChanged="dlTipoDemanda_SelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="1">Regulado</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="lblSector" Text="Sector Consumo Usuario Regulado" AssociatedControlID="ddlSector" runat="server" />
                                                    <asp:DropDownList ID="ddlSector" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4" runat="server" visible="false">
                                                <div class="form-group">
                                                    <asp:Label Text="Usuario Final" AssociatedControlID="TxtUsuarioFinal" runat="server" />
                                                    <asp:TextBox ID="TxtUsuarioFinal" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <ajaxToolkit:AutoCompleteExtender runat="server" BehaviorID="AutoCompleteExUsuF" ID="AutoCompleteExtender1"
                                                        TargetControlID="TxtUsuarioFinal" ServicePath="~/WebService/AutoComplete.asmx"
                                                        ServiceMethod="GetCompletionListUsuarioFinal" MinimumPrefixLength="3" CompletionInterval="1000"
                                                        EnableCaching="true" CompletionSetCount="20" CompletionListCssClass="autocomplete_completionListElement"
                                                        CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                        DelimiterCharacters=";,:">
                                                        <Animations>
                        <OnShow>
                            <Sequence>
                                <OpacityAction Opacity="0" />
                                <HideAction Visible="true" />
                                <ScriptAction Script="
                                    // Cache the size and setup the initial size
                                    var behavior = $find('AutoCompleteExUsuF');
                                    if (!behavior._height) {
                                        var target = behavior.get_completionList();
                                        behavior._height = target.offsetHeight - 2;
                                        target.style.height = '0px';
                                    }" />
                                <Parallel Duration=".4">
                                    <FadeIn />
                                    <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteExUsuF')._height" />
                                </Parallel>
                            </Sequence>
                        </OnShow>
                        <OnHide>
                            <Parallel Duration=".4">
                                <FadeOut />
                                <Length PropertyKey="height" StartValueScript="$find('AutoCompleteExUsuF')._height" EndValue="0" />
                            </Parallel>
                        </OnHide>
                                                        </Animations>
                                                    </ajaxToolkit:AutoCompleteExtender>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Punto de Salida en SNT" AssociatedControlID="ddlPuntoSalida" runat="server" />
                                                    <asp:DropDownList ID="ddlPuntoSalida" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                    <asp:HiddenField ID="hdfTipoDemanda" runat="server" />
                                                    <asp:HiddenField ID="hdfCodDatUsu" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Mercado Relevante" AssociatedControlID="ddlMercadoRel" runat="server" />
                                                    <asp:DropDownList ID="ddlMercadoRel" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="lblCantContra" Text="Cantidad a entregar" AssociatedControlID="TxtCantidadUsu" runat="server" />
                                                    <asp:TextBox ID="TxtCantidadUsu" runat="server" ValidationGroup="detalle" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4" runat="server" id="TdIndica">
                                                <div class="form-group">
                                                    <asp:Label Text="Equivalente Kpcd" AssociatedControlID="TxtEquivaleKpcd" runat="server" />
                                                    <asp:TextBox ID="TxtEquivaleKpcd" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <asp:Button ID="btnCrearUsu" runat="server" CssClass="btn btn-primary btn-block" Text="Agregar Usuario" OnClick="btnCrearUsu_Click" Visible="false" />
                                        <asp:Button ID="btnActualUsu" runat="server" CssClass="btn btn-primary btn-lg btn-block" Text="Actualiza Usuario" OnClick="btnActualUsu_Click" Visible="false" />

                                        <hr>

                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <b>TOTAL CANTIDAD:</b><asp:Label ID="lblTotlCantidad" runat="server" ForeColor="Red" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table table-responsive">
                                            <div class="table table-responsive">
                                                <div style="overflow: scroll; height: 450px;">
                                                    <asp:DataGrid ID="dtgUsuarios" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                                        Width="100%" CssClass="table-bordered" OnEditCommand="dtgUsuarios_EditCommand">
                                                        <Columns>
                                                            <%--0--%>
                                                            <asp:BoundColumn DataField="tipo_demanda" HeaderText="Tipo Demanda Atender" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                            <%--1--%>
                                                            <asp:BoundColumn DataField="no_identificacion_usr" HeaderText="Identificacion Usuario Final"
                                                                ItemStyle-HorizontalAlign="Left" Visible="false"></asp:BoundColumn>
                                                            <%--2--%>
                                                            <asp:BoundColumn DataField="tipo_documento" HeaderText="Tipo Identificacion Usuario Final"
                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px" Visible="false"></asp:BoundColumn>
                                                            <%--3--%>
                                                            <asp:BoundColumn DataField="nombre_usuario" HeaderText="Nombre Usuario Final" ItemStyle-HorizontalAlign="Left" Visible="false"></asp:BoundColumn>
                                                            <%--4--%>
                                                            <asp:BoundColumn DataField="sector_consumo" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="80px"></asp:BoundColumn>
                                                            <%--5--%>
                                                            <asp:BoundColumn DataField="punto_salida" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="100px"></asp:BoundColumn>
                                                            <%--6--%>
                                                            <asp:BoundColumn DataField="cantidad_contratada" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="100px"></asp:BoundColumn>
                                                            <%--7--%>
                                                            <asp:BoundColumn DataField="equivalente_kpcd" HeaderText="Equivalente Kpcd" ItemStyle-HorizontalAlign="Left"
                                                                ItemStyle-Width="100px"></asp:BoundColumn>
                                                            <%--8--%>
                                                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar"></asp:EditCommandColumn>
                                                            <%--9--%>
                                                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                                                            <%--10--%>
                                                            <asp:BoundColumn DataField="codigo_verif_usr" Visible="false"></asp:BoundColumn>
                                                            <%--11--%>
                                                            <asp:BoundColumn DataField="codigo_verif" Visible="false"></asp:BoundColumn>
                                                            <%--12--%>
                                                            <asp:BoundColumn DataField="codigo_sector_consumo" Visible="false"></asp:BoundColumn>
                                                            <%--13--%>
                                                            <asp:BoundColumn DataField="codigo_punto_salida" Visible="false"></asp:BoundColumn>
                                                            <%--14--%>
                                                            <asp:BoundColumn DataField="codigo_tipo_doc" Visible="false"></asp:BoundColumn>
                                                            <%--15--%>
                                                            <asp:BoundColumn DataField="codigo_tipo_demanda" Visible="false"></asp:BoundColumn>
                                                            <%--17--%>
                                                            <asp:BoundColumn DataField="codigo_mercado_relevante" Visible="false"></asp:BoundColumn>
                                                            <%--18--%>
                                                            <asp:BoundColumn DataField="codigo_verif_usr" Visible="false"></asp:BoundColumn>
                                                        </Columns>
                                                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                                    </asp:DataGrid>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <asp:Button ID="btnRegresar" runat="server" CssClass="btn btn-secondary" Text="Regresar" OnClick="btnRegresar_Click" Visible="false" />
                        <asp:Button ID="btnCrear" runat="server" CssClass="btn btn-primary" Text="Insertar" OnClick="btnCrear_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnActualizar" runat="server" CssClass="btn btn-primary" Text="Actualizar" OnClick="btnActualizar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
