﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucCargaArchivoNegBila.ascx.cs" Inherits="UsersControls.Contratos.Registro_Contratos.Registro_Contratos.WucCargaArchivoNegBila" %>

<script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/dev/modal.js")%>"></script>

<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label ID="lblFuArchivo" AssociatedControlID="FuArchivo" runat="server">Archivo</asp:Label>
            <asp:FileUpload ID="FuArchivo" runat="server" CssClass="form-control" EnableTheming="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4" style="display: none">
        <div class="form-group">
            <asp:Label AssociatedControlID="ddlTipoCargue" runat="server">Tipo de Cargue</asp:Label>
            <asp:DropDownList ID="ddlTipoCargue" CssClass="form-control selectpicker" runat="server">
                <asp:ListItem Value="N" Text="Normal"></asp:ListItem>
                <asp:ListItem Value="C" Text="Según Resolución CREG 170 de 2015"></asp:ListItem>
            </asp:DropDownList>
            <%--20210224--%>
            <asp:HiddenField ID="hndArchivo" runat="server" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label AssociatedControlID="display" runat="server">Tiempo Transcurrido</asp:Label>
            <input type="text" name="display" id="display" size="8" value="00:00:0" class="form-control" readonly="true" clientidmode="Static" runat="server" />
        </div>
    </div>
</div>

<%--Confirmación--%>
<div class="modal fade" id="mdlFechaOpBil" tabindex="-1" role="dialog" aria-labelledby="mdlFechaOpBilLabel" aria-hidden="true" clientidmode="Static" runat="server">
    <div class="modal-dialog" id="mdlFechaOpBilInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="mdlFechaOpBilLabel" runat="server">Fecha Máxima de Registro de Operaciones Bilaterales</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblMensaje" runat="server"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <asp:Button ID="btnAceptar" CssClass="btn btn-primary" Text="Aceptar" OnClick="BtnAceptar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>


<%--20210224 pasa validaciones--%>
<div class="modal fade" id="mdlConfirma" tabindex="-1" role="dialog" aria-labelledby="mdlConfirmaLabel" aria-hidden="true" clientidmode="Static" runat="server">
    <div class="modal-dialog" id="mdlConfirmaInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                <ContentTemplate>
                    <div class="modal-header" style="background-color: #3E5F8A;" >
                        <h5 class="modal-title" id="mdlConfirmaLabel" runat="server" style ="color: white;">Confirmación</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff; opacity: 1;">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="background-color: #D3D3D3">
                        <asp:Label ID="lblMensajeCOnf" runat="server">Se presentan las siguientes validaciones:</asp:Label>
                    </div>
                    <div class="modal-body" style="background-color: #D3D3D3">
                        <asp:Label ID="lblValidacion" runat="server"></asp:Label>
                    </div>
                    <div class="modal-body" style="background-color: #D3D3D3">
                        <asp:Label ID="lblConforma" runat="server">Desea continuar de todas formas?</asp:Label>
                    </div>
                    <div class="modal-footer" style="background-color: #3E5F8A">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <asp:Button ID="btnAceptarConf" CssClass="btn btn-primary" Text="Aceptar" OnClick="BtnAceptarConf_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
