﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucArchivoNegLargoPlazo.ascx.cs" Inherits="UsersControls.Contratos.Registro_Contratos.Registro_Contratos.WucArchivoNegLargoPlazo" %>

<%--Contenido--%>
<div class="kt-portlet__body">
    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <asp:Label ID="lblArchivo" Text="Archivo" AssociatedControlID="FuArchivo" runat="server" />
                <asp:FileUpload ID="FuArchivo" CssClass="form-control" runat="server" EnableTheming="true" />
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <asp:Label AssociatedControlID="display" runat="server">Tiempo Transcurrido</asp:Label>
                <input type="text" name="display" id="display" size="8" value="00:00:0" class="form-control" readonly="true" clientidmode="Static" runat="server" />
            </div>
        </div>
    </div>
</div>

<%--Modals--%>
<%--Aut--%>
<div class="modal fade" id="modAut" tabindex="-1" role="dialog" aria-labelledby="modAutLabel" aria-hidden="true" clientidmode="Static" runat="server">
    <div class="modal-dialog" id="modAutInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="modAutLabel">Fecha Máxima de Registro de Negociaciones de Largo Plazo</h5>
                    </div>
                    <div class="modal-body">
                        <p id="parFechaLArgoPlazo" runat="server"></p>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="BtnCancelar" CssClass="btn btn-secondary" data-dismiss="modal" Text="Cancelar" OnClick="BtnCancelar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" runat="server" />
                        <asp:Button ID="BtnAceptar" CssClass="btn btn-primary" Text="Aceptar" OnClick="BtnAceptar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
