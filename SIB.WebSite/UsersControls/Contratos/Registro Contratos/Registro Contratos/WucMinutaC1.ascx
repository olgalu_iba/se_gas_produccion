﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucMinutaC1.ascx.cs" Inherits="UsersControls.Contratos.Registro_Contratos.Registro_Contratos.WucMinutaC1" %>

<%--Contenido--%>
<div class="row">
    <%--20161207 rq102--%>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <label>Número Operación Inicial</label>
            <asp:TextBox ID="TxtNoContrato" type="number" runat="server" class="form-control" ValidationGroup="detalle" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <label>Número Operación Final</label>
            <asp:TextBox ID="TxtNoContratoFin" type="number" runat="server" class="form-control" ValidationGroup="detalle" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" />
            <%--20161207 rq102--%>
            <asp:HiddenField ID="hdfNoHorasPrim" runat="server" />
            <asp:HiddenField ID="hdfNoDiasSecun" runat="server" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <label>Fecha Negociación Inicial</label>
            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <label>Fecha Negociación Final</label>
            <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
        </div>
    </div>
</div>
<%--Grilla--%>
<div class="table table-responsive">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                Width="100%" CssClass="table-bordered" OnItemCommand="dtgConsulta_EditCommand"
                AllowPaging="true" PageSize="10" OnPageIndexChanged="dtgConsulta_PageIndexChanged">
                <Columns>
                    <%--0--%>
                    <asp:BoundColumn DataField="numero_contrato" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--1--%>
                    <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Width="100px"></asp:BoundColumn>
                    <%--2--%>
                    <asp:BoundColumn DataField="mercado" HeaderText="Mercado" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Width="80px"></asp:BoundColumn>
                    <%--3--%>
                    <asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Width="100px"></asp:BoundColumn>
                    <%--4--%>
                    <asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Width="100px"></asp:BoundColumn>
                    <%--5--%>
                    <asp:BoundColumn DataField="punto_entrega" HeaderText="Lugar Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--6--%>
                    <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--7--%>
                    <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--8--%>
                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                    <%--9--%>
                    <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                    <%--10--%>
                    <asp:BoundColumn DataField="fecha_max_registro" HeaderText="Fecha Límite Registro"
                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px" DataFormatString="{0:yyyy/MM/dd hh:mm}"></asp:BoundColumn>
                    <%--11--%>
                    <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--12--%>
                    <asp:BoundColumn DataField="puede_ingresar" Visible="false"></asp:BoundColumn>
                    <%--13--%>
                    <asp:BoundColumn DataField="estado" Visible="false"></asp:BoundColumn>
                    <%--14--%>
                    <asp:BoundColumn DataField="punta" Visible="false"></asp:BoundColumn>

                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                        <ItemTemplate>
                            <div class="dropdown dropdown-inline">
                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="flaticon-more-1"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                    <!--begin::Nav-->
                                    <ul class="kt-nav">
                                        <li class="kt-nav__item">
                                            <asp:LinkButton ID="lkbSeleccionar" CssClass="kt-nav__link" CommandName="Seleccionar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-expand"></i>
                                                            <span class="kt-nav__link-text">Seleccionar</span>
                                            </asp:LinkButton>
                                        </li>
                                    </ul>
                                    <!--end::Nav-->
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
            </asp:DataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%-- Impresión  --%>
</div>
<%--Excel--%>
<asp:DataGrid ID="dtgConsultaExcel" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
    Width="100%" CssClass="table-bordered" Visible="false">
    <Columns>
        <%--0--%>
        <asp:BoundColumn DataField="numero_contato" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <%--1--%>
        <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left"
            ItemStyle-Width="100px"></asp:BoundColumn>
        <%--2--%>
        <asp:BoundColumn DataField="mercado" HeaderText="Mercado" ItemStyle-HorizontalAlign="Left"
            ItemStyle-Width="80px"></asp:BoundColumn>
        <%--3--%>
        <asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"
            ItemStyle-Width="100px"></asp:BoundColumn>
        <%--4--%>
        <asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"
            ItemStyle-Width="100px"></asp:BoundColumn>
        <%--5--%>
        <asp:BoundColumn DataField="punto_entrega" HeaderText="Lugar Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <%--6--%>
        <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <%--7--%>
        <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <%--8--%>
        <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
        <%--9--%>
        <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
        <%--10--%>
        <asp:BoundColumn DataField="fecha_limite_registro" HeaderText="Fecha Límite Registro"
            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
    </Columns>
    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
</asp:DataGrid>

<%--Modals--%>
<%--Registro/Modificacion--%>
<div class="modal fade" id="registroContratoMod" tabindex="-1" role="dialog" aria-labelledby="mdlRegistroContratoLabel" aria-hidden="true" clientidmode="Static" runat="server">
    <div class="modal-dialog" id="registroContratoModInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="mdlRegistroContratoLabel" runat="server">Registro</h5>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <%--Infirmacion Basica--%>
                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5>Información Básica</h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Vendedor</label>
                                                    <asp:TextBox ID="lblVendedor" disabled runat="server" ForeColor="Red" CssClass="form-control" />
                                                    <asp:HiddenField ID="hdfOperacion" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Comprador</label>
                                                    <asp:TextBox ID="lblComprador" disabled runat="server" ForeColor="Red" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Punto Entrega</label>
                                                    <asp:TextBox ID="lblPunto" disabled runat="server" ForeColor="Red" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Cantidad</label>
                                                    <asp:TextBox ID="lblCantidad" disabled runat="server" ForeColor="Red" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Precio</label>
                                                    <asp:TextBox ID="lblPrecio" disabled runat="server" ForeColor="Red" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Garantía de cumplimiento</label>
                                                    <asp:CheckBox ID="chkGarantia1" runat="server" class="form-control" Text="Prepago" />
                                                    <asp:CheckBox ID="chkGarantia2" runat="server" class="form-control" Text="Garantía Bancaria" />
                                                    <asp:CheckBox ID="chkGarantia3" runat="server" class="form-control" Text="Carta de crédito stand by nacional" />
                                                    <asp:CheckBox ID="chkGarantia4" runat="server" class="form-control" Text="Carta de crédito stand by internacional" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Valor garantía</label>
                                                    <asp:TextBox ID="txtValorGrtia" TextMode="Number" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Moneda</label>
                                                    <asp:TextBox ID="txtMoneda" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Plazo Entrega</label>
                                                    <asp:TextBox ID="lblPlazo" disabled runat="server" ForeColor="Red" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Fecha Inicio</label>
                                                    <asp:TextBox ID="txtFechaIniE" disabled runat="server" ForeColor="Red" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Fecha Finalización</label>
                                                    <asp:TextBox ID="txtFechaFinE" disabled runat="server" ForeColor="Red" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Fecha Suscripción</label>
                                                    <asp:TextBox ID="txtFechaSus" disabled runat="server" ForeColor="Red" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4" id="trPago" runat="server">
                                                <div class="form-group">
                                                    <label>Cuenta Para pagos (Texto Descriptivo)</label>
                                                    <asp:TextBox ID="txtCuenta" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Atención A:</label>
                                                    <asp:TextBox ID="txtAtencion" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Dirección:</label>
                                                    <asp:TextBox ID="txtDireccion" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Correo Electrónico:</label>
                                                    <asp:TextBox ID="txtCorreo" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Confirmación correo:</label>
                                                    <asp:TextBox ID="TxtConfCorreo" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Teléfono:</label>
                                                    <asp:TextBox ID="TxtTelefono" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Fax:</label>
                                                    <asp:TextBox ID="TxtFax" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Correo Electrónico Nominación:</label>
                                                    <asp:TextBox ID="txtCorreoNom" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Resolución de Conflictos:</label>
                                                    <asp:CheckBox ID="chkConflicto1" runat="server" class="form-control" Text="Arbitraje" />
                                                    <asp:CheckBox ID="chkConflicto2" runat="server" class="form-control" Text="Jurisdicción Ordinaria/Contenciosa" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Moneda Pago:</label>
                                                    <asp:CheckBox ID="chkMoneda1" runat="server" class="form-control" Text="Pesos Colombianos" />
                                                    <asp:CheckBox ID="chkMoneda2" runat="server" class="form-control" Text="Dólares Americanos" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="table table-responsive">
                                                <asp:DataGrid ID="dtgUsuarios" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                                    Width="100%" CssClass="table-bordered">
                                                    <Columns>
                                                        <%--0--%>
                                                        <asp:BoundColumn DataField="tipo_demanda" HeaderText="Tipo Demanda Atender" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                        <%--1--%>
                                                        <asp:BoundColumn DataField="no_identificacion_usr" HeaderText="Identificación Usuario Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                        <%--2--%>
                                                        <asp:BoundColumn DataField="nombre_usuario" HeaderText="Nombre Usuario Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                        <%--3--%>
                                                        <asp:BoundColumn DataField="sector_consumo" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="80px"></asp:BoundColumn>
                                                        <%--4--%>
                                                        <asp:BoundColumn DataField="punto_salida" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left" />
                                                        <%--5--%>
                                                        <asp:BoundColumn DataField="cantidad_contratada" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Left" />
                                                        <%--Campo Nuevo Req. 009-17 Indsicadores 20170321--%>
                                                        <asp:BoundColumn DataField="equivalente_kpcd" HeaderText="Equivalente Kpcd" ItemStyle-HorizontalAlign="Left " Visible="false" />

                                                    </Columns>
                                                    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                                </asp:DataGrid>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <asp:Button ID="btnCrear" runat="server" CssClass="btn btn-primary" Text="Insertar" OnClick="btnCrear_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnActualizar" runat="server" CssClass="btn btn-primary" Text="Actualizar" OnClick="btnActualizar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>

