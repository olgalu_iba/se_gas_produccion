﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;
using System.Linq;
using Segas.Web.Elements;

namespace UsersControls.Contratos.Registro_Contratos.Registro_Contratos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class WucCargaContratoPdf : UserControl
    {
        #region EventHandler

        /// <summary>
        /// Delegado para el manejo de los modals
        /// </summary>
        public delegate void EventHandlerModal(string id, string insideId, EnumTypeModal typeModal, Modal.Size size = Modal.Size.Large);

        /// <summary>
        ///  EventHandler para la selección de los los modals
        /// </summary>
        public event EventHandlerModal ModalEvent;

        /// <summary>
        ///  Delegado para el manejo de los toastr
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        /// EventHandler para la selección de los toastr 
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        ///Delegado para la apertura de un documento en una nueva ventana del navegador 
        /// </summary>
        public delegate void EventHandlerDocument(string pathDocument);

        /// <summary>
        ///EventHandler para la apertura de un documento en una nueva ventana del navegador    
        /// </summary>
        public event EventHandlerDocument DocumentEvent;

        /// <summary>
        /// Delegado para el manejo del log de cargue de archivos
        /// </summary>
        //public delegate void EventHandlerLogCargaArchivo(StringBuilder message);

        /// <summary>
        /// EventHandler para el log de cargue de archivos
        /// </summary>
        //public event EventHandlerLogCargaArchivo LogCargaArchivoEvent;

        /// <summary>
        /// Delegado para la selección de los botones  
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get { return ViewState["NameController"] != null && !string.IsNullOrEmpty(ViewState["NameController"].ToString()) ? ViewState["NameController"].ToString() : string.Empty; }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        #endregion Propiedades

        InfoSessionVO goInfo = null;
        static string lsTitulo = "carga PDF de contratos registrados";
        clConexion lConexion = null;
        clConexion lConexion1 = null;
        SqlDataReader lLector;
        DataSet lds = new DataSet();
        String strRutaCarga;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lConexion = new clConexion(goInfo);
            lConexion1 = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["RutaCargaContPdf"].ToString();

            if (!IsPostBack)
            {
                /// Llenar controles del Formulario

            }

        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Excel };
            ButtonsEvent?.Invoke(botones, null, NameController);
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlComprador, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlVendedor, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4);

            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlVendedor.SelectedValue = goInfo.cod_comisionista;
                ddlVendedor.Enabled = false;
            }
            //Se establece que el controlador ya se inicializo    
            ButtonsEvent?.Invoke(botones.ToArray(), null, NameController);
            Inicializado = true;
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnConsultar_Click(object sender, EventArgs e)
        {
            CargarDatos();
        }

        protected void dtgConsulta_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            this.dtgConsulta.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();

        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            var lblMensaje = new StringBuilder();
            DateTime ldFechaI = DateTime.Now;
            DateTime ldFechaF = DateTime.Now;
            string[] lsNombreParametros = { "@P_fecha_ini", "@P_fecha_fin", "@P_operacion", "@P_contrato_def", "@P_codigo_vendedor", "@P_codigo_comprador", "@P_tipo_mercado" };
            SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
            string[] lValorParametros = { "", "", "0", "", "0", "0", "" };

            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo fecha inicial <br>");
                }

            }
            if (TxtFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo fecha final<br>");
                }
            }
            if (TxtFechaFin.Text.Trim().Length > 0 && TxtFechaIni.Text.Trim().Length == 0)
                lblMensaje.Append("Debe digitar la fecha inicial antes que la final<br>");

            if (TxtFechaFin.Text.Trim().Length > 0 && TxtFechaIni.Text.Trim().Length > 0)
                if (ldFechaI > ldFechaF)
                    lblMensaje.Append("La fecha inicial debe ser menor o igual  que la final<br>");

            if (lblMensaje.ToString() == "")
            {
                try
                {
                    if (TxtFechaIni.Text != "")
                    {
                        lValorParametros[0] = TxtFechaIni.Text.Trim();
                        if (TxtFechaFin.Text != "")
                            lValorParametros[1] = TxtFechaFin.Text.Trim();
                        else
                            lValorParametros[1] = TxtFechaIni.Text.Trim();
                    }
                    if (TxtNoOper.Text.Trim().Length > 0)
                        lValorParametros[2] = TxtNoOper.Text.Trim();
                    lValorParametros[3] = TxtContratoDef.Text.Trim();
                    lValorParametros[4] = ddlVendedor.SelectedValue;
                    lValorParametros[5] = ddlComprador.SelectedValue;
                    lValorParametros[6] = ddlMercado.SelectedValue;

                    lConexion.Abrir();
                    dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContCargaPdf", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgConsulta.DataBind();
                    //20190607 rq036-19
                    dtgExcel.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContCargaPdf", lsNombreParametros, lTipoparametros, lValorParametros);
                    //20190607 rq036-19
                    dtgExcel.DataBind();
                    lConexion.Cerrar();
                    
                    dtgConsulta.CurrentPageIndex = 0;
                    if (dtgConsulta.Items.Count == 0)
                    {
                        lblMensaje.Append("No se encontraron Registros.");
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("No se Pudo Generar el Informe.! " + ex.Message.ToString());
                }
            }
            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page")) return;

            var lblMensaje = new StringBuilder();
            try
            {
                lblOperacion.Text = e.Item.Cells[0].Text;
                lblFechaNeg.Text = e.Item.Cells[3].Text;
                lblIdRegistro.Text = e.Item.Cells[1].Text;
                lblContrato.Text = e.Item.Cells[2].Text;
                lblComprador.Text = e.Item.Cells[5].Text;
                lblVendedor.Text = e.Item.Cells[4].Text;
                lblCantidad.Text = e.Item.Cells[8].Text;
                lblPrecio.Text = e.Item.Cells[9].Text;
                lblFechaIni.Text = e.Item.Cells[6].Text;
                lblFechaFin.Text = e.Item.Cells[7].Text;
                hndVendedor.Value = e.Item.Cells[11].Text;
                CargarDet();

                ModalEvent?.Invoke(modContratosPdf.ID, modContratosPdfInside.ID, EnumTypeModal.Abrir);
                //tblPdf.Visible = true;
                //tblDatos.Visible = false;
                //tblGrilla.Visible = false;
                //imbExcel.Visible = false;
            }
            catch (Exception ex)
            {
                lblMensaje.Append("Problemas en la Recuperación de la Información. " + ex.Message.ToString());
            }
            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            //Cierra el modal de Registro
            ModalEvent?.Invoke(modContratosPdf.ID, modContratosPdfInside.ID, EnumTypeModal.Cerrar);
            //tblPdf.Visible = false;
            //tblDatos.Visible = true;
            //tblGrilla.Visible = true;
            //imbExcel.Visible = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void BtnCargar_Click(object sender, EventArgs e)
        {
            var lblMensaje = new StringBuilder();
            try
            {
                if (FuArchivo.FileName.ToString() == "")
                    lblMensaje.Append("Debe seleccionar el archivo");
                //20200430 ajuste nombre archivos
                if (!(Directory.Exists(strRutaCarga + hndVendedor.Value)))
                    Directory.CreateDirectory(strRutaCarga + hndVendedor.Value);
                int liNumero = dtgPdf.Items.Count + 1;
                string lsNumero = "";
                string lsArchivo = "";
                int liCOnta = 0;
                while (lsArchivo == "" && liCOnta < 10)
                {
                    lsNumero = DateTime.Now.Millisecond.ToString();
                    lsArchivo = DateTime.Now.ToString("yyyy_MM_dd") + "_" + hndVendedor.Value + "_" + lblIdRegistro.Text + "_" + liNumero.ToString() + "_" + lsNumero + Path.GetExtension(FuArchivo.FileName).ToUpper();
                    if (File.Exists(strRutaCarga + hndVendedor.Value + "\\" + lsArchivo))
                    {
                        lsArchivo = "";
                    }
                    liCOnta++;
                }
                if (lsArchivo == "")
                    lblMensaje.Append("No se pueden cargar los archivos. Intente de nuevo<br>");

                //20200430 FIN ajuste nombre archivos
                if (lblMensaje.ToString() == "")
                {
                    string[] lsNombreParametros = { "@P_codigo_verif", "@P_nombre_archivo", "@P_nombre_archivo_org", "@P_indica" };//20200430 ajuste nombre archivos
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };//20200430 ajuste nombre archivos
                    string[] lValorParametros = { lblIdRegistro.Text, lsArchivo, FuArchivo.FileName.ToString(), "C" }; //20200430 ajuste nombre archivos

                    lConexion.Abrir();
                    goInfo.mensaje_error = "";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetContratoPdf", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        lblMensaje.Append("Se presentó un Problema en la carga del contrato.! " + goInfo.mensaje_error.ToString());
                        lConexion.Cerrar();
                    }
                    else
                    {
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                                lblMensaje.Append(lLector["error"].ToString() + "<br>");
                        }
                        else
                        {
                            try
                            {
                                //20200430 ajuste nombre archivos
                                //if (!(Directory.Exists(strRutaCarga + hndVendedor.Value )))
                                //    Directory.CreateDirectory(strRutaCarga + hndVendedor.Value );
                                //FuArchivo.SaveAs(strRutaCarga + hndVendedor.Value  + "\\" + FuArchivo.FileName.ToString());//20200430 ajuste nombre archivos
                                FuArchivo.SaveAs(strRutaCarga + hndVendedor.Value + "\\" + lsArchivo);//20200430 ajuste nombre archivos
                                CargarDet();
                                ModalEvent?.Invoke(modContratosPdf.ID, modContratosPdfInside.ID, EnumTypeModal.Abrir);
                            }
                            catch (Exception ex)
                            {
                                string[] lsNombreParametrosA = { "@P_codigo_verif", "@P_nombre_archivo", "@P_indica" };
                                SqlDbType[] lTipoparametrosA = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
                                string[] lValorParametrosA = { lblIdRegistro.Text, lsArchivo, "A" };//20200430 ajuste nombre archivos

                                DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContratoPdf", lsNombreParametrosA, lTipoparametrosA, lValorParametrosA, goInfo);
                            }
                        }
                        lConexion.Cerrar();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Append(ex.Message);
                lConexion.Cerrar();
            }
            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void lkbExcel_Click(object sender, EventArgs e)
        {
            string lsNombreArchivo = Session["login"] + "InfContPdf" + DateTime.Now + ".xls";
            var lblMensaje = new StringBuilder();
            try
            {
                decimal ldCapacidad = 0;
                StringBuilder lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                dtgExcel.Visible = true;
                dtgExcel.EnableViewState = false;
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                lform.Controls.Add(dtgExcel);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = System.Text.Encoding.Default;

                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + "Consulta PDF de contratos registrados" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write(lsb.ToString());
                Response.End();
                Response.Flush();
                dtgExcel.Visible = false;
            }
            catch (Exception ex)
            {
                lblMensaje.Append("Problemas al Consultar los Registros. " + ex.Message.ToString());
            }
            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
        }


        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected void CargarDet()
        {
            string[] lsNombreParametros = { "@P_codigo_verif" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int };
            string[] lValorParametros = { lblIdRegistro.Text };
            lConexion.Abrir();
            dtgPdf.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContCargaPdfDet", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgPdf.DataBind();

        }

        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgPdf_EditCommand(object source, DataGridCommandEventArgs e)
        {
            var lblMensaje = new StringBuilder();
            if (e.CommandName.Equals("Ver"))
            {
                try
                {
                    string lsCarpetaAnt = "";
                    string[] lsCarperta;
                    lsCarperta = strRutaCarga.Split('\\');
                    lsCarpetaAnt = lsCarperta[lsCarperta.Length - 2];
                    var lsRuta = "../../" + lsCarpetaAnt + "/" + hndVendedor.Value + '/' + e.Item.Cells[1].Text.Replace(@"\", "/");
                    DocumentEvent?.Invoke(lsRuta);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Error al visualizar el archivo. " + ex.Message.ToString());
                }
            }
            if (e.CommandName.Equals("Eliminar"))
            {
                try
                {
                    string[] lsNombreParametros = { "@P_codigo_contrato_pdf", "@P_indica" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
                    string[] lValorParametros = { e.Item.Cells[0].Text, "E" };

                    File.Delete(strRutaCarga + hndVendedor.Value + '/' + e.Item.Cells[1].Text.Replace(@"\", "/"));
                    lConexion.Abrir();
                    DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContratoPdf", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    lConexion.Cerrar();
                    CargarDet();
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Error al eliminar el archivo. " + ex.Message.ToString());
                }
            }
            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
        }

    }
}