﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace UsersControls.Contratos.Registro_Contratos.Registro_Contratos
{
    public partial class WucCargaTasaCambioCont : UserControl
    {
        #region EventHandler

        /// <summary>
        /// Delegado para el manejo de los toastr
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        /// EventHandler para la selección de los toastr
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        /// Delegado para el manejo del log de cargue de archivos
        /// </summary>
        public delegate void EventHandlerLogCargaArchivo(StringBuilder message);

        /// <summary>
        /// EventHandler para el log de cargue de archivos
        /// </summary>
        public event EventHandlerLogCargaArchivo LogCargaArchivoEvent;

        /// <summary>
        /// Delegado para la selección de los botones  
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get { return ViewState["NameController"] != null && !string.IsNullOrEmpty(ViewState["NameController"].ToString()) ? ViewState["NameController"].ToString() : string.Empty; }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        #endregion Propiedades

        private InfoSessionVO goInfo = null;
        private clConexion lConexion = null;
        private string strRutaCarga;
        private string strRutaFTP;
        private SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
            strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            EnumBotones[] botones = { EnumBotones.Cargue };
            ButtonsEvent?.Invoke(botones, null, NameController);
            //Se establece que el controlador ya se inicializo    
            Inicializado = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void BtnCargar_Click(object sender, EventArgs e)
        {
            string lsRutaArchivo = "";
            string lsNombre = "";
            string[] lsErrores = { "", "" };
            var lsCadenaArchivo = new StringBuilder();
            bool oTransOK = true;
            bool oCargaOK = false;
            SqlDataReader lLector;
            SqlCommand lComando = new SqlCommand();
            Int32 liNumeroParametros = 0;
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_codigo_operador", "@P_ruta_ftp" };
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar };
            Object[] lValorParametrosO = { "", goInfo.cod_comisionista, strRutaFTP };

            lsNombre = DateTime.Now.Millisecond.ToString() + FuArchivo.FileName.ToString();
            try
            {
                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Ocurrio un Problema al Cargar el Archivo. " + ex.Message, EnumTypeToastr.Error);
                oTransOK = false;
            }
            /// Realiza las Validaciones de los Archivos
            if (oTransOK)
            {
                try
                {
                    //trMensaje.Visible = true;
                    //lsCadenaArchivo = lsCadenaArchivo + "<TR><TD colspan='2'> <font color='#FF0000' size='-1'>" + "***LOG ARCHIVO DE CARGA *** </FONT> </TD></TR>";
                    if (FuArchivo.FileName.ToString() != "")
                        lsErrores = ValidarArchivo(lsRutaArchivo);
                    else
                        lsErrores[0] += "Debe seleccionar el archivo<br>";
                    if (lsErrores[0] == "")
                    {
                        oCargaOK = DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"].ToString() + lsNombre, ConfigurationManager.AppSettings["UserFtp"].ToString(), ConfigurationManager.AppSettings["PwdFtp"].ToString());

                        if (oCargaOK)
                        {
                            lValorParametrosO[0] = lsNombre;
                            lConexion.Abrir();
                            lComando.Connection = lConexion.gObjConexion;
                            lComando.CommandType = CommandType.StoredProcedure;
                            lComando.CommandText = "pa_ValidaPlanoTrmContrato";
                            lComando.CommandTimeout = 3600;
                            if (lsNombreParametrosO != null)
                            {
                                for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                                {
                                    lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                                }
                            }
                            lLector = lComando.ExecuteReader();
                            if (lLector.HasRows)
                            {
                                while (lLector.Read())
                                {
                                    lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                                }
                            }
                            else
                            {
                                ToastrEvent?.Invoke("Archivo cargado satisfactoriamente.!", EnumTypeToastr.Success);
                            }
                            lLector.Close();
                            lLector.Dispose();
                            lConexion.Cerrar();
                        }
                        else
                        {
                            ToastrEvent?.Invoke("Se presentó un problema en el {FTP} del archivo al servidor.!", EnumTypeToastr.Error);
                        }
                    }
                    else
                    {
                        lsCadenaArchivo.Append(lsErrores[0]);
                        DelegadaBase.Servicios.registrarProceso(goInfo, "Finalizó la carga con errores", "Usuario : " + goInfo.nombre);
                    }

                    if (!string.IsNullOrEmpty(lsCadenaArchivo.ToString()) &&
                    lsCadenaArchivo.ToString().Contains("Se crearon correctamente"))
                    {
                        string[] delim = { Environment.NewLine, "\n" }; // "\n" added in case you manually appended a newline
                        var lines = lsCadenaArchivo.ToString().Split(delim, StringSplitOptions.None);
                        lsCadenaArchivo.Clear();
                        foreach (var line in lines)
                        {
                            if (!line.Contains("Se crearon correctamente"))
                                lsCadenaArchivo.Append(line);
                            else
                                ToastrEvent?.Invoke(line, EnumTypeToastr.Success);
                        }
                    }

                    if (!string.IsNullOrEmpty(lsCadenaArchivo.ToString()))
                        //Se descarga el log para el usuario
                        LogCargaArchivoEvent?.Invoke(lsCadenaArchivo);
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Se presentó un problema al cargar los datos del archivo.! <br>" + ex.Message, EnumTypeToastr.Error);
                }
            }

        }

        /// <summary>
        /// Validacion del Archivo de Contratos
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            Int32 liNumeroLinea = 0;
            decimal ldValor = 0;
            int liTotalRegistros = 0;
            Int64 liValor = 0;
            string[] lsFecha;
            string[] lsPrecio;
            string lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            StreamReader lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 4))
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 4 },  en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<BR>";
                    }
                    else
                    {
                        //contato
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(0).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en número de operación {" + oArregloLinea.GetValue(0).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// tipo TRM
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(1).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en código del tipo de tasa de cambio {" + oArregloLinea.GetValue(1).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// moneda
                        if (oArregloLinea.GetValue(2).ToString() != "1" && oArregloLinea.GetValue(2).ToString() != "2")
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la moneda {" + oArregloLinea.GetValue(2).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        /// observacion
                        if (oArregloLinea.GetValue(3).ToString().Length > 200)
                            lsCadenaErrores = lsCadenaErrores + "Valor demasiado extenso para las observaciones.{" + oArregloLinea.GetValue(3).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }

    }
}