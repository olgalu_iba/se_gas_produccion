﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace UsersControls.Contratos.Registro_Contratos.Registro_Contratos
{
    public partial class WucInfTransaccionalGestor : UserControl
    {
        #region EventHandler

        /// <summary>
        /// Delegado para el manejo de los toastr
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        //20210224
        public delegate void EventHandlerModal(string id, string insideId, EnumTypeModal typeModal, Modal.Size size = Modal.Size.Large);
        //20210224
        public event EventHandlerModal ModalEvent;
        /// <summary>
        /// EventHandler para la selección de los toastr
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        /// Delegado para el manejo del log de cargue de archivos
        /// </summary>
        public delegate void EventHandlerLogCargaArchivo(StringBuilder message);

        /// <summary>
        /// EventHandler para el log de cargue de archivos
        /// </summary>
        public event EventHandlerLogCargaArchivo LogCargaArchivoEvent;

        /// <summary>
        /// Delegado para la selección de los botones  
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get { return ViewState["NameController"] != null && !string.IsNullOrEmpty(ViewState["NameController"].ToString()) ? ViewState["NameController"].ToString() : string.Empty; }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        #endregion Propiedades

        private InfoSessionVO goInfo = null;
        private clConexion lConexion = null;
        private string strRutaCarga;
        private string strRutaFTP;
        private SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
            strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            EnumBotones[] botones = { EnumBotones.Cargue };
            ButtonsEvent?.Invoke(botones, null, NameController);
            //Se establece que el controlador ya se inicializo    
            Inicializado = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void BtnCargar_Click(object sender, EventArgs e)
        {
            string lsRutaArchivo = "";
            string lsRutaArchivoU = "";
            string lsNombre = "";
            string lsNombreU = "";
            string[] lsErrores = { "", "" };
            var lsCadenaArchivo = new StringBuilder();
            bool oTransOK = true;
            bool oCargaOK = false;
            SqlDataReader lLector;
            SqlCommand lComando = new SqlCommand();
            Int32 liNumeroParametros = 0;
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_archivoUsuarios", "@P_codigo_operador", "@P_ruta_ftp", "@P_codigo_usuario", "@P_valida" }; //20210224
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar };//20210224
            Object[] lValorParametrosO = { "", "", goInfo.cod_comisionista, strRutaFTP, goInfo.codigo_usuario, "S" };//20210224

            lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;
            hndArchivo.Value = lsNombre; //20210224
            try
            {
                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Ocurrio un Problema al Cargar el Archivo de Contratos. " + ex.Message, EnumTypeToastr.Error);
                oTransOK = false;
            }
            if (FuArchivoUsuarios.FileName != "")
            {
                lsNombreU = DateTime.Now.Millisecond + FuArchivoUsuarios.FileName;
                hndArchivoUsu.Value = lsNombreU; //20210224
                try
                {
                    lsRutaArchivoU = strRutaCarga + lsNombreU;
                    FuArchivoUsuarios.SaveAs(lsRutaArchivoU);
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Ocurrio un Problema al Cargar el Archivo de Usuarios. " + ex.Message, EnumTypeToastr.Error);
                    oTransOK = false;
                }
            }
            /// Realiza las Validaciones de los Archivos
            if (!oTransOK) return;
            {
                try
                {
                    string lsErrror = "N"; //20201124
                    string lsConfirma = "N"; //20201124

                    if (FuArchivo.FileName != "")
                        lsErrores = ValidarArchivo(lsRutaArchivo);
                    else
                    {
                        ToastrEvent?.Invoke("Debe seleccionar el archivo de los contratos<br>", EnumTypeToastr.Warning);
                        return;
                    }

                    if (lsErrores[0] == "")
                    {
                        if (FuArchivoUsuarios.FileName != "")
                            lsErrores = ValidarArchivoUsuarios(lsRutaArchivoU);
                    }

                    if (lsErrores[0] == "")
                    {
                        oCargaOK = DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]);
                        if (FuArchivoUsuarios.FileName != "")
                            oCargaOK = DelegadaBase.Servicios.put_archivo(lsRutaArchivoU, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombreU, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]);

                        if (oCargaOK)
                        {

                            lValorParametrosO[0] = lsNombre;
                            lValorParametrosO[1] = lsNombreU;
                            lConexion.Abrir();
                            lComando.Connection = lConexion.gObjConexion;
                            lComando.CommandType = CommandType.StoredProcedure;
                            lComando.CommandText = "pa_ValidaPlanoVerifContSistema";
                            lComando.CommandTimeout = 3600;
                            if (lsNombreParametrosO != null)
                            {
                                for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                                {
                                    lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                                }
                            }
                            string lsErrorMail = "";
                            lblValidacion.Text = "";//20201124
                            lLector = lComando.ExecuteReader();
                            if (lLector.HasRows)
                            {
                                while (lLector.Read())
                                {
                                    //20210224
                                    //if (lLector["ind_error"].ToString() == "S")
                                    //    lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                                    //else
                                    //{
                                    //    clEmail mailV = new clEmail(lLector["mail"].ToString(), "Registro de contratos", lLector["Mensaje"].ToString(), "");
                                    //    lsErrorMail = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas") + "<br>";
                                    //}
                                    //20201124
                                    if (lLector["ind_error"].ToString() == "S")
                                    {
                                        lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                                        lsErrror = "S";
                                    }
                                    if (lLector["ind_error"].ToString() == "C")
                                    {
                                        lblValidacion.Text += lLector["mensaje"] + "<br>";
                                        lsConfirma = "S";
                                    }
                                    if (lLector["ind_error"].ToString() == "N")
                                    {
                                        clEmail mailV = new clEmail(lLector["mail"].ToString(), "Registro de contratos", lLector["Mensaje"].ToString(), "");
                                        lsErrorMail = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas") + "<br>";
                                    }
                                }
                                //Se él modal de confirmación
                                //20210224
                                if (lsErrror == "N" && lsConfirma == "S")
                                    ModalEvent?.Invoke(mdlConfirma.ID, mdlConfirmaInside.ID, EnumTypeModal.Abrir, Modal.Size.Normal);
                            }
                            else
                            {
                                ToastrEvent?.Invoke("Archivo cargado satisfactoriamente.!", EnumTypeToastr.Success);
                            }
                            lLector.Close();
                            lLector.Dispose();
                            lConexion.Cerrar();
                        }
                        else
                        {
                            ToastrEvent?.Invoke("Se presentó un problema en el {FTP} del archivo al servidor.!", EnumTypeToastr.Error);
                        }
                    }
                    else
                    {
                        lsCadenaArchivo.Append(lsErrores[0]);
                        DelegadaBase.Servicios.registrarProceso(goInfo, "Finalizó la carga con errores", "Usuario : " + goInfo.nombre);
                    }

                    if (lsErrror != "N" || lsConfirma != "S") //20210224
                    {
                        if (!string.IsNullOrEmpty(lsCadenaArchivo.ToString()) && lsCadenaArchivo.ToString().Contains("Se crearon correctamente"))
                        {
                            string[] delim = { Environment.NewLine, "\n" }; // "\n" added in case you manually appended a newline
                            var lines = lsCadenaArchivo.ToString().Split(delim, StringSplitOptions.None);
                            lsCadenaArchivo.Clear();
                            foreach (var line in lines)
                            {
                                if (!line.Contains("Se crearon correctamente"))
                                    lsCadenaArchivo.Append(line);
                                else
                                    ToastrEvent?.Invoke(line, EnumTypeToastr.Success);
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(lsCadenaArchivo.ToString()))
                        //Se descarga el log para el usuario
                        LogCargaArchivoEvent?.Invoke(lsCadenaArchivo);
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Se presentó un problema al cargar los datos del archivo.! <br>" + ex.Message, EnumTypeToastr.Error);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20210224
        protected void BtnAceptarConf_Click(object sender, EventArgs e)
        {
            ModalEvent?.Invoke(mdlConfirma.ID, mdlConfirmaInside.ID, EnumTypeModal.Cerrar);

            string lsNombre = "";
            string lsNombreU = "";
            string[] lsErrores = { "", "" };
            var lsCadenaArchivo = new StringBuilder();
            SqlDataReader lLector;
            SqlCommand lComando = new SqlCommand();
            Int32 liNumeroParametros = 0;
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_archivoUsuarios", "@P_codigo_operador", "@P_ruta_ftp", "@P_codigo_usuario", "@P_valida" }; //20210224
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar };//20210224
            Object[] lValorParametrosO = { hndArchivo.Value , hndArchivoUsu.Value , goInfo.cod_comisionista, strRutaFTP, goInfo.codigo_usuario, "N" };//20210224

            try
            {

                lConexion.Abrir();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_ValidaPlanoVerifContSistema";
                lComando.CommandTimeout = 3600;
                if (lsNombreParametrosO != null)
                {
                    for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                    {
                        lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                    }
                }
                string lsErrorMail = "";
                lLector = lComando.ExecuteReader();
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                    {
                        if (lLector["ind_error"].ToString() == "S")
                            lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                        else
                        {
                            clEmail mailV = new clEmail(lLector["mail"].ToString(), "Registro de contratos", lLector["Mensaje"].ToString(), "");
                            lsErrorMail = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas") + "<br>";
                        }
                    }
                }
                else
                {
                    ToastrEvent?.Invoke("Archivo cargado satisfactoriamente.!", EnumTypeToastr.Success);
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();


                if (!string.IsNullOrEmpty(lsCadenaArchivo.ToString()) && lsCadenaArchivo.ToString().Contains("Se crearon correctamente"))
                {
                    string[] delim = { Environment.NewLine, "\n" }; // "\n" added in case you manually appended a newline
                    var lines = lsCadenaArchivo.ToString().Split(delim, StringSplitOptions.None);
                    lsCadenaArchivo.Clear();
                    foreach (var line in lines)
                    {
                        if (!line.Contains("Se crearon correctamente"))
                            lsCadenaArchivo.Append(line);
                        else
                            ToastrEvent?.Invoke(line, EnumTypeToastr.Success);
                    }
                }

                if (!string.IsNullOrEmpty(lsCadenaArchivo.ToString()))
                    //Se descarga el log para el usuario
                    LogCargaArchivoEvent?.Invoke(lsCadenaArchivo);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Se presentó un problema al cargar los datos del archivo.! <br>" + ex.Message, EnumTypeToastr.Error);
            }
        }


        /// <summary>
        /// Validacion del Archivo de Contratos
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            Int32 liNumeroLinea = 0;
            decimal ldValor = 0;
            int liTotalRegistros = 0;
            Int64 liValor = 0;
            DateTime ldFecha;
            string[] lsFecha;
            string lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            StreamReader lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la Línea  sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 24)) // Fuente o campo MP 20160603  //20170816 rq036-17  //20171130 rq026-17 //20201124 //20210707
                    {
                        lsCadenaErrores = lsCadenaErrores + "El número de Campos no corresponde con la estructura del Plano { 24 },  en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; // Fuente o campo MP 20160603  //20170816 rq036-17  //20171130 rq026-17 //2020819 ajuste OM //20201124 //20210707
                    }
                    else
                    {
                        /// Validar Operacion
                        if (oArregloLinea.GetValue(0).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar el número de la operación {" + oArregloLinea.GetValue(0).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(0).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en el número de la operación {" + oArregloLinea.GetValue(0).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                            }
                        }
                        /// Validar No. Contrato
                        if (oArregloLinea.GetValue(1).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar el número de contrato {" + oArregloLinea.GetValue(1).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //20200701 ajuste auditoria //2020819 ajuste OM

                        /// Valida Fecha de Suscripcion
                        if (oArregloLinea.GetValue(2).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar la fecha de suscripción {" + oArregloLinea.GetValue(2).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(2).ToString());
                                lsFecha = oArregloLinea.GetValue(2).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "Formato inválido en la fecha de suscripción {" + oArregloLinea.GetValue(2).ToString() + "},debe ser {YYYY/MM/DD}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La fecha de suscripción {" + oArregloLinea.GetValue(2).ToString() + "} es inválida, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            }
                        }
                        /// Validar Codigo Punto de Entega
                        if (oArregloLinea.GetValue(3).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar el código del punto de entrega / tramo {" + oArregloLinea.GetValue(3).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(3).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en el código del punto de entrega / tramo {" + oArregloLinea.GetValue(3).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                            }
                        }
                        /// Validar Codigo Modalidad Contractual
                        if (oArregloLinea.GetValue(4).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar el código de la modalidad contractual {" + oArregloLinea.GetValue(4).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(4).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en el código de la modalidad contractual {" + oArregloLinea.GetValue(4).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                            }
                        }
                        /// Validar Cantiad
                        if (oArregloLinea.GetValue(5).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar la cantidad / capacidad {" + oArregloLinea.GetValue(5).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(5).ToString().Trim());
                                if (liValor <= 0)
                                    lsCadenaErrores = lsCadenaErrores + "La cantidad / capacidad {" + oArregloLinea.GetValue(5).ToString() + "} no puede ser 0, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en la cantidad / capacidad {" + oArregloLinea.GetValue(5).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            }
                        }
                        /// Validar Precio
                        if (oArregloLinea.GetValue(6).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar el precio {" + oArregloLinea.GetValue(6).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        else
                        {
                            try
                            {
                                ldValor = Convert.ToDecimal(oArregloLinea.GetValue(6).ToString().Trim());
                                lsFecha = oArregloLinea.GetValue(6).ToString().Trim().Split('.');
                                if (lsFecha.Length > 1)
                                {
                                    if (lsFecha[1].Trim().Length > 2)
                                        lsCadenaErrores = lsCadenaErrores + "Valor inválido en el precio {" + oArregloLinea.GetValue(6).ToString() + "}, solo debe tener máximo 2 decimales, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                                }
                                if (ldValor <= 0)
                                    lsCadenaErrores = lsCadenaErrores + "El precio {" + oArregloLinea.GetValue(6).ToString() + "} no puede ser 0, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en el precio {" + oArregloLinea.GetValue(6).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            }
                        }
                        /// Valida Fecha Inicial
                        if (oArregloLinea.GetValue(7).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar la fecha inicial {" + oArregloLinea.GetValue(7).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(7).ToString());
                                lsFecha = oArregloLinea.GetValue(7).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "Formato inválido en la fecha inicial {" + oArregloLinea.GetValue(7).ToString() + "},debe ser {YYYY/MM/DD}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La fecha Inicial {" + oArregloLinea.GetValue(7).ToString() + "} es inválida, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            }
                        }
                        /// Valida Fecha Final
                        if (oArregloLinea.GetValue(8).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar la fecha final {" + oArregloLinea.GetValue(8).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(8).ToString());
                                lsFecha = oArregloLinea.GetValue(8).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "Formato inválido en la fecha final {" + oArregloLinea.GetValue(8).ToString() + "},debe ser {YYYY/MM/DD}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La fecha final {" + oArregloLinea.GetValue(8).ToString() + "} es inválida, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            }
                        }
                        /// Validar Codigo Tipo Demanda
                        //if (oArregloLinea.GetValue(10).ToString().Trim().Length <= 0)
                        //    lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Codigo Tipo Demanda (Si es venta debe ingresar 0) {" + oArregloLinea.GetValue(10).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        //else
                        //{
                        //    try
                        //    {
                        //        liValor = Convert.ToInt64(oArregloLinea.GetValue(10).ToString().Trim());
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Codigo Tipo Demanda {" + oArregloLinea.GetValue(10).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        //    }
                        //}
                        /// Validar Sentido del Flujo
                        /// 20210707 ajsute indice por trm 
                        if (oArregloLinea.GetValue(9).ToString().Trim().Length > 0)
                        {
                            if (oArregloLinea.GetValue(9).ToString().Trim() != "NORMAL" && oArregloLinea.GetValue(9).ToString().Trim() != "CONTRAFLUJO") //20210915
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en el sentido del flujo, valores válidos {NORMAL o CONTRAFLUJO} {" + oArregloLinea.GetValue(9).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM //20210915
                        }

                        /// Validar Presion Punto Terminacion
                        /// /// 20210707 ajsute indice por trm 
                        if (oArregloLinea.GetValue(10).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar la presión del punto de terminación (Valor por defecto 0) {" + oArregloLinea.GetValue(10).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                        else
                        {
                            ////////////////////////////////////////////////////////////////////////////////////////////////
                            //// Validacion Nueva sentido del flujo Requerimiento Ajuste presion transporte 20151006 ///////
                            ////////////////////////////////////////////////////////////////////////////////////////////////
                            string[] lsPresion;
                            try
                            {
                                if (oArregloLinea.GetValue(10).ToString().Trim().Length > 500)
                                    lsCadenaErrores = lsCadenaErrores + "Longitud del campo presión del punto de terminación {" + oArregloLinea.GetValue(10).ToString() + "} supera el máximo permitido (500 caracteres), en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                                else
                                {
                                    lsPresion = oArregloLinea.GetValue(10).ToString().Trim().Split('-');
                                    foreach (string Presion in lsPresion)
                                    {
                                        try
                                        {
                                            ldValor = Convert.ToDecimal(Presion.Trim());
                                        }
                                        catch (Exception)
                                        {
                                            lsCadenaErrores = lsCadenaErrores + "Valor inválido en la presión del punto de terminación {" + Presion.Trim() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en la presión del punto de terminación {" + oArregloLinea.GetValue(10).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            }
                            /// Valida Fuente o campo MP 20160603
                            if (oArregloLinea.GetValue(11).ToString().Trim().Length <= 0)
                                lsCadenaErrores = lsCadenaErrores + "Debe ingresar el código de la fuente {" + oArregloLinea.GetValue(11).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            else
                            {
                                try
                                {
                                    liValor = Convert.ToInt16(oArregloLinea.GetValue(11).ToString().Trim());
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "Valor inválido en el código de la fuente {" + oArregloLinea.GetValue(11).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                                }
                            }
                            //20170816 rq036-17
                            if (oArregloLinea.GetValue(12).ToString().Trim() != "N" && oArregloLinea.GetValue(12).ToString().Trim() != "S")
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en el indicador de conexión al SNT {" + oArregloLinea.GetValue(12).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                                                                                                                                                                                                                                                 //20170816 rq036-17
                            if (oArregloLinea.GetValue(13).ToString().Trim() != "N" && oArregloLinea.GetValue(13).ToString().Trim() != "S")
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en la entrega en boca de pozo {" + oArregloLinea.GetValue(13).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                            //20170816 rq036-17
                            if (oArregloLinea.GetValue(14).ToString().Trim().Length <= 0)
                                lsCadenaErrores = lsCadenaErrores + "Debe ingresar el código del Centro Poblado {" + oArregloLinea.GetValue(14).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            else
                            {
                                try
                                {
                                    liValor = Convert.ToInt64(oArregloLinea.GetValue(14).ToString().Trim());
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "Valor inválido en el código del Centro Poblado {" + oArregloLinea.GetValue(14).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                                }
                            }
                            /// 20171130 rq026-17
                            if (oArregloLinea.GetValue(15).ToString().Trim().Length <= 0)
                                lsCadenaErrores = lsCadenaErrores + "Debe ingresar el código del periodo de entrega {" + oArregloLinea.GetValue(15).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                            else
                            {
                                try
                                {
                                    liValor = Convert.ToInt64(oArregloLinea.GetValue(15).ToString().Trim());
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "Valor inválido en el código del periodo de entrega {" + oArregloLinea.GetValue(15).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";//20200701 ajuste auditoria //2020819 ajuste OM
                                }
                            }
                            /// 20171130 rq026-17
                            if (oArregloLinea.GetValue(16).ToString().Trim() != "N" && oArregloLinea.GetValue(16).ToString().Trim() != "S")
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en el indicador de contrato variable {" + oArregloLinea.GetValue(16).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            /// 20171130 rq026-17
                            if (oArregloLinea.GetValue(17).ToString() != "0")
                            {
                                try
                                {
                                    ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(17).ToString());
                                    if (oArregloLinea.GetValue(17).ToString().Length != 5)
                                        lsCadenaErrores = lsCadenaErrores + "La hora de entrega inicial {" + oArregloLinea.GetValue(17).ToString() + "} es inválida, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "La hora de entrega inicial {" + oArregloLinea.GetValue(17).ToString() + "} es inválida, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                                }
                            }
                            /// 20171130 rq026-17
                            if (oArregloLinea.GetValue(18).ToString() != "0")
                            {
                                try
                                {
                                    ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(18).ToString());
                                    if (oArregloLinea.GetValue(18).ToString().Length != 5)
                                        lsCadenaErrores = lsCadenaErrores + "La hora de entrega final {" + oArregloLinea.GetValue(18).ToString() + "} es inválida, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "La hora de entrega final {" + oArregloLinea.GetValue(18).ToString() + "} es inválida, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                                }
                            }
                            /// 20200925 subasta Bimestral C1
                            //if (oArregloLinea.GetValue(20).ToString() != "N" && oArregloLinea.GetValue(20).ToString() != "S")
                            //{
                            //    lsCadenaErrores = lsCadenaErrores + "El indicador de manejo de minuta  {" + oArregloLinea.GetValue(20).ToString() + "} es inválido, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM
                            //}
                            // 20201124
                            /// Calsificacion de transporte
                            try
                            {
                                liValor = Convert.ToInt32(oArregloLinea.GetValue(19).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La clasificación de transporte {" + oArregloLinea.GetValue(19).ToString() + "} es inválida, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                            }
                            /// 202010707
                            /// tipo tasa
                            try
                            {
                                liValor = Convert.ToInt32(oArregloLinea.GetValue(21).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La tasa de cambio {" + oArregloLinea.GetValue(21).ToString() + "} es inválida, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                            }
                            /// 202010707
                            /// tipo moneda
                            if (oArregloLinea.GetValue(23).ToString() != "USD" && oArregloLinea.GetValue(23).ToString() != "COP")
                                lsCadenaErrores = lsCadenaErrores + "El tipo de moneda {" + oArregloLinea.GetValue(23).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }

        /// <summary>
        /// Validacion del Archivo de Usuarios Finales
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivoUsuarios(string lsRutaArchivo)
        {
            Int32 liNumeroLinea = 0;
            int liTotalRegistros = 0;
            Int64 liValor = 0;
            string lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            StreamReader lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la Línea  sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    /// Campop nuevo Req. 009-17 Indicadores 20170324 
                    /// Cantidad Ekivalente KPCD Solo para mercado primario de Gas
                    if (oArregloLinea.Length != 8)
                    {
                        lsCadenaErrores = lsCadenaErrores + "El número de campos no corresponde con la estructura del plano { 8 },  en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano de Usuarios<br>"; //rq009-17 //2020819 ajuste OM
                    }
                    else
                    {
                        /// Validar Operacion
                        if (oArregloLinea.GetValue(0).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar el número de la operación {" + oArregloLinea.GetValue(0).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano de Usuarios<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(0).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en el número de la operación {" + oArregloLinea.GetValue(0).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano de Usuarios<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                            }
                        }
                        /// Validar Documento Usuario Final
                        if (oArregloLinea.GetValue(1).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar el Nit del usuario final (valor por defecto 0) {" + oArregloLinea.GetValue(1).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano de Usuarios<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(1).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en el Nit del usuario final {" + oArregloLinea.GetValue(1).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano de Usuarios<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                            }
                        }
                        /// Validar Codigo Sector COnsumo 
                        if (oArregloLinea.GetValue(2).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar el código del sector de consumo {" + oArregloLinea.GetValue(2).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano de Usuarios<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(2).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en el código del sector de consumo {" + oArregloLinea.GetValue(2).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano de Usuarios<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                            }
                        }
                        /// Validar Codigo Punto de Salida del SNT
                        if (oArregloLinea.GetValue(3).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar el código del Punto de Salida del SNT {" + oArregloLinea.GetValue(3).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano de Usuarios<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(3).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en el código del Punto de Salida del SNT {" + oArregloLinea.GetValue(3).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano de Usuarios<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                            }
                        }
                        /// Validar Cantiad
                        if (oArregloLinea.GetValue(4).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar la cantidad / capacidad {" + oArregloLinea.GetValue(4).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano de Usuarios<br>"; //2020819 ajuste OM
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(4).ToString().Trim());
                                if (liValor <= 0)
                                    lsCadenaErrores = lsCadenaErrores + "La cantidad / capacidad {" + oArregloLinea.GetValue(4).ToString() + "} no puede ser 0, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en la cantidad / capacidad {" + oArregloLinea.GetValue(4).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano de Usuarios<br>"; //2020819 ajuste OM
                            }
                        }
                        /// Validar TIpo Demanda
                        if (oArregloLinea.GetValue(5).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar el código del tipo de demanda {" + oArregloLinea.GetValue(5).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano de Usuarios<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(5).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en el código del tipo de demanda {" + oArregloLinea.GetValue(5).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano de Usuarios<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                            }
                        }
                        /// Validar Mercado relevante
                        if (oArregloLinea.GetValue(6).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar el código del mercado relevante {" + oArregloLinea.GetValue(6).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano de Usuarios<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(6).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en el código del mercado relevante {" + oArregloLinea.GetValue(6).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano de Usuarios<br>"; //20200701 ajuste auditoria //2020819 ajuste OM
                            }
                        }
                        /// Campop nuevo Req. 009-17 Indicadores 20170324 
                        /// Cantidad Ekivalente KPCD Solo para mercado primario de Gas
                        if (oArregloLinea.GetValue(7).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar la cantidad equivalente en KPCD {" + oArregloLinea.GetValue(7).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano de Usuarios<br>"; //2020819 ajuste OM
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(7).ToString().Trim());
                                if (liValor < 0)
                                    lsCadenaErrores = lsCadenaErrores + "La cantidad equivalente KPCD {" + oArregloLinea.GetValue(7).ToString() + "} no puede ser menor a 0, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; //2020819 ajuste OM

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor inválido en la cantidad equivalente KPCD {" + oArregloLinea.GetValue(7).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano de Usuarios<br>"; //2020819 ajuste OM
                            }
                        }
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }
    }
}