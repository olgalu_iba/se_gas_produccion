﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucSolEliContrato.ascx.cs" Inherits="UsersControls.Contratos.Registro_Contratos.Eliminaciones_o_Modificaciones.WucSolEliContrato" %>

<%--Contenido--%>
<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Número Operación" AssociatedControlID="TxtNoContrato" runat="server" />
            <asp:TextBox ID="TxtNoContrato" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Número Id Registro" AssociatedControlID="TxtNoIdRegistro" runat="server" />
            <asp:TextBox ID="TxtNoIdRegistro" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Estado" AssociatedControlID="ddlEstado" runat="server" />
            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control selectpicker">
                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                <asp:ListItem Value="I" Text="Pendiente Aprobacion Operador"></asp:ListItem>
                <asp:ListItem Value="P" Text="Pendiente Aprobacion Bmc"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
            <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
</div>
<div class="row" runat="server" id="tblGrilla" visible="false">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="form-group">
            <div class="alert alert-info" role="alert">
                Aprobación / Desaprobación 
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label ID="lblArchivo" Text="Archivo Pdf" AssociatedControlID="FuArchivo" runat="server" />
            <ajaxToolkit:AsyncFileUpload ID="FuArchivo" CssClass="form-control" OnUploadedComplete="FileUpload_UploadedComplete" runat="server" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label ID="lblObservacion" Text="Observacion" AssociatedControlID="TxtObservacion" runat="server" />
            <asp:TextBox ID="TxtObservacion" runat="server" CssClass="form-control" />
        </div>
    </div>
</div>
<%--Grilla--%>
<div class="table table-responsive">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                Width="100%" CssClass="table-bordered" OnItemCommand="dtgConsulta_EditCommand">
                <Columns>
                    <%--0--%>
                    <asp:TemplateColumn HeaderText="Sele.">
                        <ItemTemplate>
                            <label class="kt-checkbox">
                                <asp:CheckBox ID="chkRecibir" runat="server" />
                                <span></span>
                            </label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <%--1--%>
                    <asp:BoundColumn DataField="numero_operacion" HeaderText="No. Operacion" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--2--%>
                    <asp:BoundColumn DataField="numero_id_registro" HeaderText="No. Registro"></asp:BoundColumn>
                    <%--3--%>
                    <asp:BoundColumn DataField="tipo_subasta" HeaderText="Tipo Subasta" ItemStyle-HorizontalAlign="Left"
                        Visible="false"></asp:BoundColumn>
                    <%--4--%>
                    <asp:BoundColumn DataField="mercado" HeaderText="Mercado" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Width="80px"></asp:BoundColumn>
                    <%--5--%>
                    <asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Width="100px"></asp:BoundColumn>
                    <%--6--%>
                    <asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Width="100px"></asp:BoundColumn>
                    <%--7--%>
                    <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--8--%>
                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                    <%--9--%>
                    <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                    <%--10--%>
                    <asp:BoundColumn DataField="codigo_solicitud" HeaderText="No. Solicitud" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                    <%--11--%>
                    <asp:BoundColumn DataField="observacion_compra" HeaderText="Observacion Compra" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                    <%--12--%>
                    <asp:BoundColumn DataField="observacion_venta" HeaderText="Observacion Venta" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                    <%--13--%>
                    <asp:BoundColumn DataField="aprobado_bmc" HeaderText="Aprobado BMC" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                    <%--14--%>
                    <asp:BoundColumn DataField="aprobado_operador" HeaderText="Aprobado Operador" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                    <%--15--%>
                    <asp:BoundColumn DataField="observacion_bmc" HeaderText="Observacion BMC" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                    <%--16--%>
                    <asp:BoundColumn DataField="operador_compra" Visible="false"></asp:BoundColumn>
                    <%--17--%>
                    <asp:BoundColumn DataField="operador_venta" Visible="false"></asp:BoundColumn>
                    <%--18--%>
                    <asp:BoundColumn DataField="operador_solicitud" Visible="false"></asp:BoundColumn>
                    <%--19--%>
                    <asp:BoundColumn DataField="pdf_compra" Visible="false"></asp:BoundColumn>
                    <%--20--%>
                    <asp:BoundColumn DataField="pdf_venta" Visible="false"></asp:BoundColumn>
                    <%--21--%>
                    <asp:EditCommandColumn HeaderText="Pdf Compra" EditText="Pdf_C"></asp:EditCommandColumn>
                    <%--22--%>
                    <asp:EditCommandColumn HeaderText="Pdf Venta" EditText="Pdf_V"></asp:EditCommandColumn>
                    <%--23--%>
                    <asp:BoundColumn DataField="sigla_estado" Visible="false"></asp:BoundColumn>
                    <%--24--%><%--20170601 rq020-17--%>
                    <asp:BoundColumn DataField="cnt_reg" Visible="false"></asp:BoundColumn>
                    <%--20180126 rq107-16--%>
                    <asp:BoundColumn DataField="desc_subasta" HeaderText="Tipo Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                </Columns>
                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
            </asp:DataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
