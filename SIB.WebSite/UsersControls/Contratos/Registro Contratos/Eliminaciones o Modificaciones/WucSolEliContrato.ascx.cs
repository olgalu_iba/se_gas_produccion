﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace UsersControls.Contratos.Registro_Contratos.Eliminaciones_o_Modificaciones
{
    public partial class WucSolEliContrato : UserControl
    {
        #region EventHandler

        /// <summary>
        /// Delegado para el manejo de los toastr
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        /// EventHandler para la selección de los toastr 
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        /// Delegado para la selección de los botones  
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get { return ViewState["NameController"] != null && !string.IsNullOrEmpty(ViewState["NameController"].ToString()) ? ViewState["NameController"].ToString() : string.Empty; }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        /// <summary>
        /// Ruta del archivo cargado temporalmente en el servidor 
        /// </summary>
        public string FileUploadTemp
        {
            get { return Session["FileUpload_Temp"] != null && !string.IsNullOrEmpty(Session["FileUpload_Temp"].ToString()) ? Session["FileUpload_Temp"].ToString() : string.Empty; }
            set { Session["FileUpload_Temp"] = value; }
        }

        #endregion Propiedades

        private InfoSessionVO goInfo;
        private static string lsTitulo = "Solicitud Eliminacion Contrato";
        private clConexion lConexion;
        private SqlDataReader lLector;
        private DataSet _lds = new DataSet();
        private string sRutaArc = ConfigurationManager.AppSettings["rutaCarga"];
        private string[] lsCarperta;
        private string _carpeta;

        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lConexion = new clConexion(goInfo);
            lsCarperta = sRutaArc.Split('\\');
            _carpeta = lsCarperta[lsCarperta.Length - 2];

            if (IsPostBack) return;
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            // Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador !=0 order by codigo_operador", 0, 4);
            lConexion.Cerrar();
            if (Session["tipoPerfil"].ToString() != "B")
            {
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
            }

            ButtonsEvent?.Invoke(new[] { EnumBotones.Buscar, EnumBotones.Excel, EnumBotones.Aprobar, EnumBotones.Rechazar, EnumBotones.SolicitudEliminacion }, null, NameController);
            //Se establece que el controlador ya se inicializo    
            Inicializado = true;
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        private void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"] + "-" + lLector["razon_social"];
                }
                lDdl.Items.Add(lItem1);

            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            var lblMensaje = new StringBuilder();
            int liValor = 0;
            string[] lsNombreParametros = { "@P_numero_contrato", "@P_numero_registro", "@P_codigo_operador", "@P_estado", "@P_aprobado_operdor", "@P_aprobado_bmc" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "0", ddlOperador.SelectedValue, ddlEstado.SelectedValue, "0", "0" };//20170906 ajuste

            if (TxtNoContrato.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoContrato.Text);
                }
                catch (Exception)
                {
                    lblMensaje.Append("Formato Inválido en el Campo No. Id Registro. <br>"); //20170601 rq020-17
                }
            }
            if (TxtNoIdRegistro.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoIdRegistro.Text);
                }
                catch (Exception)
                {
                    lblMensaje.Append("Formato Inválido en el Campo No Operación. <br>"); //20170601 rq020-17
                }
            }

            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                return;
            }
            try
            {
                if (TxtNoContrato.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtNoContrato.Text.Trim();
                if (TxtNoIdRegistro.Text.Trim().Length > 0)
                    lValorParametros[1] = TxtNoIdRegistro.Text.Trim();

                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRegSolicEliminaCont", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                lConexion.Cerrar();
                if (dtgConsulta.Items.Count > 0)
                {
                    tblGrilla.Visible = true;

                    foreach (DataGridItem Grilla in dtgConsulta.Items)
                    {
                        if (Grilla.Cells[19].Text.Trim().Replace("&nbsp;", "").Length > 0)
                        {
                            if (Grilla.Cells[19].Text.ToUpper().Trim().Replace("&nbsp;", "").Contains("PDF"))
                                Grilla.Cells[21].Enabled = true; //20180126 rq107 - 16
                            else
                                Grilla.Cells[21].Enabled = false;//20180126 rq107 - 16
                        }
                        else
                            Grilla.Cells[21].Enabled = false; //20180126 rq107 - 16
                        if (Grilla.Cells[20].Text.Trim().Replace("&nbsp;", "").Length > 0)
                        {
                            if (Grilla.Cells[20].Text.ToUpper().Trim().Replace("&nbsp;", "").Contains("PDF"))
                                Grilla.Cells[22].Enabled = true;//20180126 rq107 - 16
                            else
                                Grilla.Cells[22].Enabled = false;//20180126 rq107 - 16
                        }
                        else
                            Grilla.Cells[22].Enabled = false;//20180126 rq107 - 16
                    }
                }
                else
                {
                    tblGrilla.Visible = false;
                    ToastrEvent?.Invoke("No se encontraron Registros.", EnumTypeToastr.Success);
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("No se Pudo Generar el Informe.!" + ex.Message, EnumTypeToastr.Info);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnSolicitud_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_codigo_verif_contrato", "@P_numero_contrato", "@P_operador", "@P_pdf", "@P_aprobacion", "@P_observacion", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0", "0", "0", "", "", "1" }; // Registro de la Solicitud
            var lblMensaje = new StringBuilder();
            int liReg = 0;
            int liNoSol = 0;
            int liNoOpera = 0;
            int liNoIdReg = 0;
            string lsPuntaSol = "";
            string lsCodOPeraC = "";
            string lsCodOPeraV = "";
            string lsNoContra = "";
            SortedList oListaOperador = new SortedList();
            string[] lsPrecio;
            string lsFecha = DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day;
            //string lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "-" + DateTime.Now.ToShortDateString().Substring(3, 2) + "-" + DateTime.Now.ToShortDateString().Substring(0, 2);
            string sRuta = sRutaArc + lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\";
            string lsContReg = "N";
            string[] file;
            string fileName = string.Empty;

            try
            {
                foreach (DataGridItem Grilla1 in dtgConsulta.Items)
                {
                    CheckBox Checkbox1 = null;
                    Checkbox1 = (CheckBox)Grilla1.Cells[0].Controls[1];
                    if (Checkbox1.Checked == true)
                    {
                        liReg++;
                        liNoSol = Convert.ToInt32(Grilla1.Cells[10].Text);
                        liNoOpera = Convert.ToInt32(Grilla1.Cells[1].Text);
                        if (liNoSol != 0)
                            lblMensaje.Append("La Operación " + liNoOpera + " Ya tiene una Solicitud en Curso No puede hacer otra solicitud. <br>"); //20170601 rq020-17
                        if (Grilla1.Cells[23].Text == "R")
                            lsContReg = "S";
                    }
                }
                if (liReg == 0)
                    lblMensaje.Append("Debe Seleccionar Registros para Ejecutar el Proceso. <br>"); //20170601 rq020-17

                if (!string.IsNullOrEmpty(FileUploadTemp))
                {
                    try
                    {
                        Directory.CreateDirectory(sRuta);
                        file = FileUploadTemp.Split('\\');
                        fileName = file[file.Length - 1];
                        lsPrecio = fileName.Split('.');
                        liReg = lsPrecio.Length;
                        if (lsPrecio[liReg - 1].ToUpper() != "PDF")
                            lblMensaje.Append("El Archivo Cargado debe ser PDF. <br>");
                        else
                        {
                            var fi1 = new FileInfo(FileUploadTemp);
                            File.Copy(FileUploadTemp, FileUploadTemp.Replace("temp\\", ""), true);
                            fi1.Delete();
                        }
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Append("Problemas en la Carga del Archivo. " + ex.Message);
                        lConexion.Cerrar();
                    }
                }
                else
                {
                    if (lsContReg == "S")
                        lblMensaje.Append("Debe Seleccionar el Archivo PDF para eliminación de contratos Registrados. <br>");
                }

                if (TxtObservacion.Text.Trim().Length <= 0)
                    lblMensaje.Append("Debe Ingresar la Observación de la Solicitud. <br>"); //20170601 rq020-17

                if (lblMensaje.ToString() != "")
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }
                {
                    SqlTransaction oTransaccion;
                    lConexion.Abrir();
                    oTransaccion = lConexion.gObjConexion.BeginTransaction(IsolationLevel.Serializable);
                    try
                    {
                        //oListaOperador.Add(goInfo.cod_comisionista, goInfo.cod_comisionista);
                        foreach (DataGridItem Grilla in dtgConsulta.Items)
                        {
                            CheckBox Checkbox = null;
                            Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                            if (Checkbox.Checked == true)
                            {
                                liReg++;
                                liNoSol = Convert.ToInt32(Grilla.Cells[10].Text);
                                liNoOpera = Convert.ToInt32(Grilla.Cells[1].Text);
                                liNoIdReg = Convert.ToInt32(Grilla.Cells[2].Text);
                                lsNoContra += liNoOpera + ",";
                                lsCodOPeraC = Grilla.Cells[16].Text.Trim();
                                lsCodOPeraV = Grilla.Cells[17].Text.Trim();
                                //20170601 rq020-17
                                if (goInfo.cod_comisionista == lsCodOPeraC)
                                {
                                    lsPuntaSol = "C";
                                    oListaOperador.Add(liNoOpera + "-" + lsCodOPeraC, "");
                                    if (Grilla.Cells[24].Text.Trim() == "2")
                                    {
                                        oListaOperador.Add(liNoOpera + "-" + lsCodOPeraV, "");
                                        ToastrEvent?.Invoke("La Operación " + liNoOpera + " queda pendiente de aprobación de la contraparte\\n", EnumTypeToastr.Success);
                                    }
                                    else
                                        ToastrEvent?.Invoke("La Operación " + liNoOpera + " queda pendiente de aprobación del gestor\\n", EnumTypeToastr.Success);
                                }
                                else
                                {
                                    lsPuntaSol = "V";
                                    oListaOperador.Add(liNoOpera + "-" + lsCodOPeraV, "");
                                    if (Grilla.Cells[24].Text.Trim() == "2")
                                    {
                                        oListaOperador.Add(liNoOpera + "-" + lsCodOPeraC, "");
                                        ToastrEvent?.Invoke("La Operación " + liNoOpera + " queda pendiente de aprobación de la contraparte\\n", EnumTypeToastr.Success);
                                    }
                                    else
                                        ToastrEvent?.Invoke("La Operación " + liNoOpera + " queda pendiente de aprobación del gestor\\n", EnumTypeToastr.Success);
                                }

                                //oListaOperador.Add(lsCodOPeraC, Grilla.Cells[1].Text);
                                //oListaOperador.Add(lsCodOPeraV, Grilla.Cells[1].Text);
                                goInfo.mensaje_error = "";
                                lValorParametros[0] = liNoSol.ToString();
                                lValorParametros[1] = liNoIdReg.ToString();
                                lValorParametros[2] = liNoOpera.ToString();
                                lValorParametros[3] = goInfo.cod_comisionista;
                                lValorParametros[4] = lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\" + FileUploadTemp;
                                lValorParametros[6] = TxtObservacion.Text.Trim();

                                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetSolicitudEliminaCont", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                                if (goInfo.mensaje_error != "")
                                {
                                    lblMensaje.Append("Se presentó un Problema en la Creación del Registro.! " + goInfo.mensaje_error); //20170601 rq020-17
                                    oTransaccion.Rollback();
                                    lConexion.Cerrar();
                                    break;
                                }
                                lLector.Close();
                                lLector.Dispose();
                            }
                        }
                        if (lblMensaje.ToString() == "")
                        {
                            oTransaccion.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        oTransaccion.Rollback();
                        lConexion.Cerrar();
                        lblMensaje.Append("Se presentó un Error en la Creación de las Solicitudes. " + ex.Message); //20170601 rq020-17
                    }

                    if (lblMensaje.ToString() != "")
                    {
                        ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                        return;
                    }

                    //// Envio Alerta a los Operadores
                    string lsAsunto = "";
                    string lsMensaje = "";
                    string lsMensajeC = "";
                    string lsMailC = "";
                    string lsNomOperadorC = "";
                    string lsMailV = "";
                    string lsNomOperadorV = "";

                    /// Obtengo el mail del Operador que Realiza la Solicitud
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + lsCodOPeraC + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsMailC = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" +
                                  lLector["e_mail2"]; // 20180228 rq032-17
                        if (lLector["tipo_persona"].ToString() == "J")
                            lsNomOperadorC = lLector["razon_social"].ToString();
                        else
                            lsNomOperadorC = lLector["nombres"] + " " + lLector["apellidos"];
                    }

                    lLector.Close();
                    lLector.Dispose();

                    /// Obtengo el mail del Operador que Realiza la Solicitud
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + lsCodOPeraV + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsMailV = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" +
                                  lLector["e_mail2"]; // 20180228 rq032-17
                        if (lLector["tipo_persona"].ToString() == "J")
                            lsNomOperadorV = lLector["razon_social"].ToString();
                        else
                            lsNomOperadorV = lLector["nombres"] + " " + lLector["apellidos"];
                    }

                    lLector.Close();
                    lLector.Dispose();
                    string lsNombreDes = "";
                    string lsNombreRem = "";
                    if (lsPuntaSol == "C")
                    {
                        lsNombreDes = lsNomOperadorV;
                        lsNombreRem = lsNomOperadorC;
                    }
                    else
                    {
                        lsNombreDes = lsNomOperadorC;
                        lsNombreRem = lsNomOperadorV;
                    }

                    /////// Envio del Mail de la Solicitud de la Correcion
                    //lsAsunto = "Notificación Solicitud Eliminacion Contrato";
                    //lsMensaje = "El Gestor del Mercado de Gas le informa que " + lsNombreRem + ", esta solicitando la eliminación de la Operacion No. " + lsNoContra + ". Por favor ingrese a la plataforma SEGAS para confirmar o rechazar la solicitud. <br><br>";
                    //lsMensaje += "<br><br>Las Observaciones de su contraparte son: " + "" + "<br>" + TxtObservacion.Text.Trim() + "<br><br>";
                    //lsMensaje += "Cordialmente, <br><br><br>";
                    //lsMensaje += "Administrador SEGAS <br>";
                    //lsMensajeC = "Señores: " + lsNombreDes + " <br><br>" + lsMensaje;
                    //clEmail mailC = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                    //lblMensaje.Text = mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");

                    try
                    {
                        //// Envio mail a la BMC
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia",
                            "m_mail_procesos", " codigo_mail_proceso = 4 ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lsMailC = lLector["mail_destinatario"].ToString();
                        }

                        lLector.Close();
                        lLector.Dispose();

                        ///// Envio del Mail de la Solicitud de la Correcion
                        lsAsunto = "Notificación Solicitud Eliminación Contrato"; //20170601 rq020-17
                        lsMensaje = "El Gestor del Mercado de Gas le informa que " + lsNombreRem +
                                    ", esta solicitando la eliminación de la Operación No. " + lsNoContra +
                                    ". Por favor ingrese a la plataforma SEGAS para confirmar o rechazar la solicitud. <br><br>";
                        lsMensaje += "<br><br>Las Observaciones de la Solicitud son: " + "" + "<br>" +
                                     TxtObservacion.Text.Trim() + "<br><br>";
                        lsMensaje += "Cordialmente, <br><br><br>";
                        lsMensaje += "Administrador SEGAS <br>";
                        lsMensajeC = "Señores: Gestor del Mercado de Gas <br><br>" + lsMensaje;
                        clEmail mailBMC = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                        lblMensaje.Append(mailBMC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"],
                            ConfigurationManager.AppSettings["PwdSmtp"],
                            ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario,
                            "Sistema de Gas"));
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Append("Error en el Envio de la Alerta a la BMC. " + ex.Message);
                    }

                    try
                    {
                        /// Envio de los Mail de los Operadores Contrarios
                        string lsCodOpera1 = "0";
                        string lsOpera = "";
                        string[] lsLlave;
                        /// Envio de los Mail de los Operadores Contrarios
                        for (int i = 0; i < oListaOperador.Count; i++)
                        {
                            lsLlave = oListaOperador.GetKey(i).ToString().Split('-');
                            lsOpera = lsLlave[0].Trim();
                            lsCodOpera1 = lsLlave[1].Trim();
                            if (lsCodOpera1 != "0")
                            {
                                /// Obtengo el mail del Operador
                                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion,
                                    "pa_ValidarExistencia", "m_operador", " codigo_operador = " + lsCodOpera1 + " ");
                                if (lLector.HasRows)
                                {
                                    lLector.Read();
                                    lsMailV = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" +
                                              lLector["e_mail2"]; // 20180228 rq032-17
                                    if (lLector["tipo_persona"].ToString() == "J")
                                        lsNomOperadorV = lLector["razon_social"].ToString();
                                    else
                                        lsNomOperadorV =
                                            lLector["nombres"] + " " + lLector["apellidos"];
                                }

                                lLector.Close();
                                lLector.Dispose();
                                ////
                                lsAsunto = "Notificación Solicitud Eliminación Contrato"; //20170601 rq020-17
                                lsMensaje = "El Gestor del Mercado de Gas le informa que " + lsNombreRem +
                                            ", esta solicitando la eliminación de la Operación No. " + lsOpera +
                                            ". Por favor ingrese a la plataforma SEGAS para confirmar o rechazar la solicitud. <br><br>";
                                lsMensaje += "<br><br>Las observaciones de su contraparte son: " + "" + "<br>" +
                                             TxtObservacion.Text.Trim() + "<br><br>";
                                lsMensaje += "Cordialmente, <br><br><br>";
                                lsMensaje += "Administrador SEGAS <br>";
                                lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                                clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                                lblMensaje.Append(mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas"));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Append("Error en el Envio de la Alerta a los Operadores. " + ex.Message);
                    }

                    lConexion.Cerrar();
                    ToastrEvent?.Invoke("Solicitudes Creadas Correctamente.!", EnumTypeToastr.Success);
                    CargarDatos();
                    if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                        ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnAprobRechaOpera_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_codigo_verif_contrato", "@P_numero_contrato", "@P_operador", "@P_pdf", "@P_aprobacion", "@P_observacion", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0", "0", "0", "S", "", "2" }; // Aprobacion del Operador
            var lblMensaje = new StringBuilder();
            int liReg = 0;
            int liNoSol = 0;
            int liNoOpera = 0;
            int liNoIdReg = 0;
            string lsCodOpera = "0";
            string lsAprobOpera = "N";
            string lsCodOPeraC = "";
            string lsCodOPeraV = "";
            string lsPuntaSol = "";
            string lsNoContra = "";
            SortedList oListaOperador = new SortedList();
            string lsResultado = "";
            string[] lsPrecio;
            string lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "-" + DateTime.Now.ToShortDateString().Substring(3, 2) + "-" + DateTime.Now.ToShortDateString().Substring(0, 2);
            string sRuta = sRutaArc + lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\";
            string lsContReg = "N";
            string fileName = string.Empty;

            try
            {
                if (TxtObservacion.Text.Trim().Length <= 0)
                    lblMensaje.Append("Debe Ingresar la Observación de la Aprobación. <br>"); //20170601 rq020-17

                foreach (DataGridItem Grilla1 in dtgConsulta.Items)
                {
                    CheckBox Checkbox1 = null;
                    Checkbox1 = (CheckBox)Grilla1.Cells[0].Controls[1];
                    if (Checkbox1.Checked == true)
                    {
                        liReg++;
                        liNoSol = Convert.ToInt32(Grilla1.Cells[10].Text);
                        liNoOpera = Convert.ToInt32(Grilla1.Cells[1].Text);
                        lsCodOpera = Grilla1.Cells[18].Text;
                        lsAprobOpera = Grilla1.Cells[14].Text;

                        if (lsCodOpera == goInfo.cod_comisionista)
                            lblMensaje.Append("El Operador NO puede Aprobar YA que fue el mismo que hizo la solicitud en la Operación No. " + liNoOpera + " <br>"); //20170601 rq020-17
                        if (liNoSol == 0)
                            lblMensaje.Append("La Operación " + liNoOpera + " No Tiene una Solicitud Pendiente de Aprobación por parte del Operador. <br>"); //20170601 rq020-17
                        else
                        {
                            if (lsAprobOpera != "N")
                                lblMensaje.Append("El Operador NO puede Aprobar YA que Ya realizó la Aprobación en la Operación No. " + liNoOpera + " <br>"); //20170601 rq020-17
                            else //20170906 ajuste
                                if (Grilla1.Cells[24].Text == "1")
                                lblMensaje.Append("La Operación No. " + liNoOpera + " no requiere de su aprobación, pendiente aprobación de la BMC <br>");
                        }
                        if (Grilla1.Cells[23].Text == "R")
                            lsContReg = "S";
                    }
                }
                if (!string.IsNullOrEmpty(FileUploadTemp))
                {
                    try
                    {
                        Directory.CreateDirectory(sRuta);
                        var file = FileUploadTemp.Split('\\');
                        fileName = file[file.Length - 1];
                        lsPrecio = fileName.Split('.');
                        liReg = lsPrecio.Length;
                        if (lsPrecio[liReg - 1].ToUpper() != "PDF")
                            lblMensaje.Append("El Archivo Cargado debe ser PDF. <br>");
                        else
                        {
                            var fi1 = new FileInfo(FileUploadTemp);
                            File.Copy(FileUploadTemp, FileUploadTemp.Replace("temp\\", ""), true);
                            fi1.Delete();
                        }
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Append("Problemas en la Carga del Archivo. " + ex.Message);
                        lConexion.Cerrar();
                    }
                }
                else
                {
                    if (lsContReg == "S")
                        lblMensaje.Append("Debe Seleccionar el Archivo PDF Para eliminación de contratos registrados. <br>"); //20170601 rq020-17
                }

                if (liReg == 0)
                    lblMensaje.Append("Debe Seleccionar Registros para Ejecutar el Proceso. <br>"); //20170601 rq020-17

                if (lblMensaje.ToString() == "")
                {
                    SqlTransaction oTransaccion;
                    lConexion.Abrir();
                    oTransaccion = lConexion.gObjConexion.BeginTransaction(IsolationLevel.Serializable);
                    try
                    {
                        liNoOpera = 0;
                        lsResultado = "Aprobar";
                        //oListaOperador.Add(goInfo.cod_comisionista, goInfo.cod_comisionista);
                        foreach (DataGridItem Grilla in dtgConsulta.Items)
                        {
                            CheckBox Checkbox = null;
                            Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                            if (Checkbox.Checked == true)
                            {
                                liReg++;
                                liNoSol = Convert.ToInt32(Grilla.Cells[10].Text);
                                liNoOpera = Convert.ToInt32(Grilla.Cells[1].Text);
                                liNoIdReg = Convert.ToInt32(Grilla.Cells[2].Text);
                                lsNoContra += liNoOpera + ",";
                                lsCodOPeraC = Grilla.Cells[16].Text.Trim();
                                lsCodOPeraV = Grilla.Cells[17].Text.Trim();
                                oListaOperador.Add(liNoOpera + "-" + lsCodOPeraC, "");
                                oListaOperador.Add(liNoOpera + "-" + lsCodOPeraV, "");
                                //oListaOperador.Add(lsCodOPeraC, liNoOpera.ToString());
                                //oListaOperador.Add(lsCodOPeraV, liNoOpera.ToString());
                                if (lsCodOPeraC == goInfo.cod_comisionista)
                                    lsPuntaSol = "C";
                                else
                                    lsPuntaSol = "V";
                                goInfo.mensaje_error = "";
                                lValorParametros[0] = liNoSol.ToString();
                                lValorParametros[1] = liNoIdReg.ToString();
                                lValorParametros[2] = liNoOpera.ToString();
                                lValorParametros[3] = goInfo.cod_comisionista;
                                lValorParametros[4] = lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\" + fileName;
                                lValorParametros[6] = TxtObservacion.Text.Trim();

                                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetSolicitudEliminaCont", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                                if (goInfo.mensaje_error != "")
                                {
                                    lblMensaje.Append("Se presentó un Problema en la Aprobación del Registro.! " + goInfo.mensaje_error); //20170601 rq020-17
                                    oTransaccion.Rollback();
                                    lConexion.Cerrar();
                                    break;
                                }
                                lLector.Close();
                                lLector.Dispose();
                            }
                        }
                        if (lblMensaje.ToString() == "")
                        {
                            oTransaccion.Commit();
                        }
                        else
                        {
                            ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                        }
                    }
                    catch (Exception ex)
                    {
                        oTransaccion.Rollback();
                        lConexion.Cerrar();
                        lblMensaje.Append("Se presentó un Error en la Creación de las Solicitudes. " + ex.Message); //20170601 rq020-17
                    }
                }
                if (lblMensaje.ToString() == "")
                {
                    //// Envio Alerta a los Operadores
                    string lsAsunto = "";
                    string lsMensaje = "";
                    string lsMensajeC = "";
                    string lsMailC = "";
                    string lsNomOperadorC = "";
                    string lsMailV = "";
                    string lsNomOperadorV = "";
                    string lsNombreRem = "";
                    /// Obtengo el mail del Operador que Realiza la Solicitud
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + goInfo.cod_comisionista + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        if (lLector["tipo_persona"].ToString() == "J")
                            lsNombreRem = lLector["razon_social"].ToString();
                        else
                            lsNombreRem = lLector["nombres"] + " " + lLector["apellidos"];
                    }
                    lLector.Close();
                    lLector.Dispose();

                    //// Envio mail a la BMC
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_mail_procesos", " codigo_mail_proceso = 4 ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsMailC = lLector["mail_destinatario"].ToString();
                    }
                    lLector.Close();
                    lLector.Dispose();



                    ///// Envio del Mail de la Solicitud de la Correcion
                    lsAsunto = "Notificación Solicitud Eliminación Contrato";
                    lsMensaje = "Nos permitimos  informarle que " + lsNombreRem + " acaba de " + lsResultado + " la Solicitud de Eliminación de la Operacion No. " + lsNoContra + " en la plataforma SEGAS. <br><br>";
                    lsMensaje += "<br><br>Observaciones: " + "" + "<br>" + TxtObservacion.Text.Trim() + "<br><br>";
                    lsMensaje += "Cordialmente, <br><br><br>";
                    lsMensaje += "Administrador SEGAS <br>";
                    lsMensajeC = "Señores: Gestor del Mercado de Gas <br><br>" + lsMensaje;
                    clEmail mailBMC = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                    lblMensaje.Append(mailBMC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas"));

                    /// Obtengo el mail del Operador que Realiza la Solicitud
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + goInfo.cod_comisionista + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsMailC = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" + lLector["e_mail2"]; // 20180228 rq032-17
                        if (lLector["tipo_persona"].ToString() == "J")
                            lsNombreRem = lLector["razon_social"].ToString();
                        else
                            lsNombreRem = lLector["nombres"] + " " + lLector["apellidos"];
                    }
                    lLector.Close();
                    lLector.Dispose();


                    /// Envio de los Mail de los Operadores Contrarios
                    string lsCodOpera1 = "0";
                    string lsNoCont = "";
                    string[] lsLlave;
                    /// Envio de los Mail de los Operadores Contrarios
                    for (int i = 0; i < oListaOperador.Count; i++)
                    {
                        lsLlave = oListaOperador.GetKey(i).ToString().Split('-');
                        lsNoCont = lsLlave[0].Trim();
                        lsCodOpera1 = lsLlave[1].Trim();
                        //lsCodOpera1 = oListaOperador.GetKey(i).ToString();
                        //lsNoCont = oListaOperador[lsCodOpera1].ToString();
                        if (lsCodOpera1 != "0")
                        {
                            /// Obtengo el mail del Operador
                            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + lsCodOpera1 + " ");
                            if (lLector.HasRows)
                            {
                                lLector.Read();
                                lsMailV = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" + lLector["e_mail2"]; // 20180228 rq032-17
                                if (lLector["tipo_persona"].ToString() == "J")
                                    lsNomOperadorV = lLector["razon_social"].ToString();
                                else
                                    lsNomOperadorV = lLector["nombres"] + " " + lLector["apellidos"];
                            }
                            lLector.Close();
                            lLector.Dispose();
                            ////
                            lsAsunto = "Notificación Solicitud Eliminación Contrato";
                            lsMensaje = "Nos permitimos  informarle que " + lsNombreRem + " acaba de " + lsResultado + " la Solicitud de Eliminación de la Operacion No.  " + lsNoCont + " en la plataforma SEGAS. <br><br>";
                            lsMensaje += "<br><br>Observaciones: " + "" + "<br>" + TxtObservacion.Text.Trim() + "<br><br>";
                            lsMensaje += "Cordialmente, <br><br><br>";
                            lsMensaje += "Administrador SEGAS <br>";
                            lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                            clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                            lblMensaje.Append(mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas"));
                        }
                    }
                    lConexion.Cerrar();
                }
                if (lblMensaje.ToString() == "")
                {
                    ToastrEvent?.Invoke("Solicitudes Aprobadas Correctamente.!", EnumTypeToastr.Success);
                    CargarDatos();
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnAprobRechaBmc_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_codigo_verif_contrato", "@P_numero_contrato", "@P_operador", "@P_pdf", "@P_aprobacion", "@P_observacion", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0", "0", "0", "S", "", "3" }; // Aprobacion de la BMC
            var lblMensaje = new StringBuilder();
            int liReg = 0;
            int liNoSol = 0;
            int liNoOpera = 0;
            int liNoIdReg = 0;
            string lsCodOpera = "0";
            string lsAprobOpera = "N";
            string lsCodOPeraC = "";
            string lsCodOPeraV = "";
            SortedList oListaOperador = new SortedList();
            string lsResultado = "";

            try
            {
                if (TxtObservacion.Text.Trim().Length <= 0)
                {
                    ToastrEvent?.Invoke("Debe Ingresar la Observación de la Aprobación. <br>", EnumTypeToastr.Error);
                    return;
                }

                foreach (DataGridItem Grilla in dtgConsulta.Items)
                {
                    CheckBox Checkbox;
                    Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                    if (Checkbox.Checked)
                    {
                        liReg++;
                        liNoSol = Convert.ToInt32(Grilla.Cells[10].Text);
                        liNoOpera = Convert.ToInt32(Grilla.Cells[1].Text);
                        lsCodOpera = Grilla.Cells[17].Text;
                        lsAprobOpera = Grilla.Cells[14].Text;

                        if (liNoSol == 0)
                            lblMensaje.Append("La Operación " + liNoOpera + " No Tiene una Solicitud Pendiente de Aprobación por parte de la BMC. <br>"); //20170601 rq020-17
                        else
                        {
                            if (lsAprobOpera == "N" && Grilla.Cells[24].Text != "1") //20170806 ajuste
                                lblMensaje.Append("La BMC NO puede Aprobar YA que el Operador No ha realizado la Aprobación en la Operación No. " + liNoOpera + " <br>"); //20170601 rq020-17
                        }
                    }
                }
                if (liReg == 0)
                    lblMensaje.Append("Debe Seleccinar Registros para Ejecutar el Proceso. <br>");

                if (lblMensaje.ToString() != "")
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Info);
                    return;
                }

                SqlTransaction oTransaccion;
                lConexion.Abrir();
                oTransaccion = lConexion.gObjConexion.BeginTransaction(IsolationLevel.Serializable);
                try
                {
                    lsResultado = "Aprobar";
                    foreach (DataGridItem Grilla in dtgConsulta.Items)
                    {
                        CheckBox Checkbox = null;
                        Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                        if (Checkbox.Checked == true)
                        {
                            liReg++;
                            liNoSol = Convert.ToInt32(Grilla.Cells[10].Text);
                            liNoOpera = Convert.ToInt32(Grilla.Cells[1].Text);
                            liNoIdReg = Convert.ToInt32(Grilla.Cells[2].Text);
                            lsCodOPeraC = Grilla.Cells[16].Text.Trim();
                            lsCodOPeraV = Grilla.Cells[17].Text.Trim();
                            oListaOperador.Add(liNoOpera + "-" + lsCodOPeraC, "");
                            oListaOperador.Add(liNoOpera + "-" + lsCodOPeraV, "");
                            goInfo.mensaje_error = "";
                            lValorParametros[0] = liNoSol.ToString();
                            lValorParametros[1] = liNoIdReg.ToString();
                            lValorParametros[2] = liNoOpera.ToString();
                            lValorParametros[3] = goInfo.cod_comisionista;
                            lValorParametros[6] = TxtObservacion.Text.Trim();

                            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(
                                lConexion.gObjConexion, "pa_SetSolicitudEliminaCont", lsNombreParametros,
                                lTipoparametros, lValorParametros, oTransaccion, goInfo);
                            if (goInfo.mensaje_error != "")
                            {
                                lblMensaje.Append("Se presentó un Problema en la Aprobación del Registro.! " + goInfo.mensaje_error); //20170601 rq020-17
                                oTransaccion.Rollback();
                                lConexion.Cerrar();
                                break;
                            }

                            lLector.Close();
                            lLector.Dispose();
                        }
                    }

                    if (lblMensaje.ToString() == "")
                    {
                        oTransaccion.Commit();
                    }
                }
                catch (Exception ex)
                {
                    oTransaccion.Rollback();
                    lConexion.Cerrar();
                    lblMensaje.Append("Se presentó un Error en la Aprobación de las Solicitudes. " + ex.Message); //20170601 rq020-17
                }

                if (lblMensaje.ToString() != "")
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }
                //// Envio Alerta a los Operadores
                string lsAsunto = "";
                string lsMensaje = "";
                string lsMensajeC = "";
                string lsMailV = "";
                string lsMailC = "";
                string lsNomOperadorV = "";
                string lsCodOpera1 = "0";
                string lsNoCont = "";
                //// Envio mail a la BMC
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia",
                    "m_mail_procesos", " codigo_mail_proceso = 4 ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsMailC = lLector["mail_destinatario"].ToString();
                }

                lLector.Close();
                lLector.Dispose();
                string lsOp1 = "";
                string[] lsLlave;
                /// Envio de los Mail de los Operadores Contrarios
                for (int i = 0; i < oListaOperador.Count; i++)
                {
                    lsLlave = oListaOperador.GetKey(i).ToString().Split('-');
                    lsNoCont = lsLlave[0].Trim();
                    lsCodOpera1 = lsLlave[1].Trim();
                    //
                    if (lsCodOpera1 != "0")
                    {
                        /// Obtengo el mail del Operador
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion,
                            "pa_ValidarExistencia", "m_operador", " codigo_operador = " + lsCodOpera1 + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lsMailV = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" +
                                      lLector["e_mail2"]; // 20180228 rq032-17
                            if (lLector["tipo_persona"].ToString() == "J")
                                lsNomOperadorV = lLector["razon_social"].ToString();
                            else
                                lsNomOperadorV =
                                    lLector["nombres"] + " " + lLector["apellidos"];
                        }

                        lLector.Close();
                        lLector.Dispose();
                        ////
                        lsAsunto = "Notificación Solicitud Eliminación Contrato";
                        lsMensaje = "Nos permitimos  informarle que el Gestor del Mercado de Gas acaba de " +
                                    lsResultado + " la Solicitud de Eliminación de la Operacion No. " + lsNoCont +
                                    " en la plataforma SEGAS. <br><br>";
                        lsMensaje += "<br><br>Observaciones: " + "" + "<br>" + TxtObservacion.Text.Trim() +
                                     "<br><br>";
                        lsMensaje += "Cordialmente, <br><br><br>";
                        lsMensaje += "Administrador SEGAS <br>";
                        lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                        clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                        lblMensaje.Append(mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas"));
                        if (lsOp1 != lsNoCont)
                        {
                            lsOp1 = lsNoCont;
                            lsAsunto = "Notificación Solicitud Eliminación Contrato";
                            lsMensaje = "Nos permitimos  informarle que el Gestor del Mercado de Gas acaba de " +
                                        lsResultado + " la Solicitud de Eliminación de la Operacion No. " +
                                        lsNoCont + " en la plataforma SEGAS. <br><br>";
                            lsMensaje += "<br><br>Observaciones: " + "" + "<br>" + TxtObservacion.Text.Trim() +
                                         "<br><br>";
                            lsMensaje += "Cordialmente, <br><br><br>";
                            lsMensaje += "Administrador SEGAS <br>";
                            lsMensajeC = "Señores: Gestor del Mercado de Gas <br><br>" + lsMensaje;
                            clEmail mailBmc = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                            lblMensaje.Append(mailBmc.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas"));
                        }
                    }
                }

                lConexion.Cerrar();
                ToastrEvent?.Invoke("Solicitudes Aprobadas Correctamente.!", EnumTypeToastr.Success);
                CargarDatos();

                if (lblMensaje.ToString() == "") return;
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: dtgReqHab_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos Visualizacion de Imagen de la Grilla de Requisitos
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals("Pdf_C"))
            {
                try
                {
                    string lsRuta = "../archivos/" + dtgConsulta.Items[e.Item.ItemIndex].Cells[19].Text.Replace(@"\", "/");
                    if (dtgConsulta.Items[e.Item.ItemIndex].Cells[19].Text.Trim().Replace("&nbsp;", "").Length > 0)
                        ScriptManager.RegisterStartupScript(this, GetType(), "StartupAlert", "window.open('" + lsRuta + "');", true);
                    else
                        ToastrEvent?.Invoke("No han Realizado la Carga del Archivo.!", EnumTypeToastr.Error);
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Problemas al Recuperar el Archivo.! " + ex.Message, EnumTypeToastr.Error);
                }
            }
            if (e.CommandName.Equals("Pdf_V"))
            {
                try
                {
                    string lsRuta = "../archivos/" + dtgConsulta.Items[e.Item.ItemIndex].Cells[20].Text.Replace(@"\", "/");
                    if (dtgConsulta.Items[e.Item.ItemIndex].Cells[20].Text.Trim().Replace("&nbsp;", "").Length > 0)
                        ScriptManager.RegisterStartupScript(this, GetType(), "StartupAlert", "window.open('" + lsRuta + "');", true);
                    else
                        ToastrEvent?.Invoke("No han Realizado la Carga del Archivo.!", EnumTypeToastr.Error);
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Problemas al Recuperar el Archivo.! " + ex.Message, EnumTypeToastr.Error);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnRFechazaOpera_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_codigo_verif_contrato", "@P_numero_contrato", "@P_operador", "@P_pdf", "@P_aprobacion", "@P_observacion", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0", "0", "0", "N", "", "2" }; // Aprobacion del Operador
            var lblMensaje = new StringBuilder();
            int liReg = 0;
            int liNoSol = 0;
            int liNoOpera = 0;
            int liNoIdReg = 0;
            string lsCodOpera = "0";
            string lsAprobOpera = "N";
            string lsCodOPeraC = "";
            string lsCodOPeraV = "";
            string lsNoContra = "";
            SortedList oListaOperador = new SortedList();
            string lsResultado = "";
            string[] lsPrecio;
            string lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "-" + DateTime.Now.ToShortDateString().Substring(3, 2) + "-" + DateTime.Now.ToShortDateString().Substring(0, 2);
            string sRuta = sRutaArc + lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\";
            string lsContReg = "N";
            var fileName = string.Empty;

            try
            {
                if (TxtObservacion.Text.Trim().Length <= 0)
                    lblMensaje.Append("Debe Ingresar la Observación del Rechazo. <br>"); //20170601 rq020-17

                foreach (DataGridItem Grilla1 in dtgConsulta.Items)
                {
                    CheckBox Checkbox1 = null;
                    Checkbox1 = (CheckBox)Grilla1.Cells[0].Controls[1];
                    if (Checkbox1.Checked == true)
                    {
                        liReg++;
                        liNoSol = Convert.ToInt32(Grilla1.Cells[10].Text);
                        liNoOpera = Convert.ToInt32(Grilla1.Cells[1].Text);
                        lsCodOpera = Grilla1.Cells[18].Text;
                        lsAprobOpera = Grilla1.Cells[14].Text;

                        if (lsCodOpera == goInfo.cod_comisionista)
                            lblMensaje.Append("El Operador NO puede Aprobar YA que fue el mismo que hizo la solicitud en la Operación No. " + liNoOpera + " <br>"); //20170601 rq020-17
                        if (liNoSol == 0)
                            lblMensaje.Append("La Operación " + liNoOpera + " No Tiene una Solicitud Pendiente de Aprobación por parte del Operador. <br>"); //20170601 rq020-17
                        else
                        {
                            if (lsAprobOpera != "N")
                                lblMensaje.Append("El Operador NO puede Aprobar YA que Ya realizó la Aprobación en la Operación No. " + liNoOpera + " <br>"); //20170601 rq020-17
                            else
                                if (Grilla1.Cells[24].Text == "1")
                                lblMensaje.Append("La Operación No. " + liNoOpera + " no requiere de su aprobación, pendiente aprobación de la BMC<br>"); //20170601 rq020-17
                        }
                        if (Grilla1.Cells[23].Text == "R")
                            lsContReg = "S";
                    }

                }
                if (!string.IsNullOrEmpty(FileUploadTemp))
                {
                    try
                    {
                        Directory.CreateDirectory(sRuta);
                        Directory.CreateDirectory(sRuta);
                        var file = FileUploadTemp.Split('\\');
                        fileName = file[file.Length - 1];
                        lsPrecio = fileName.Split('.');
                        liReg = lsPrecio.Length;
                        if (lsPrecio[liReg - 1].ToUpper() != "PDF")
                            lblMensaje.Append("El Archivo Cargado debe ser PDF. <br>");
                        else
                        {
                            var fi1 = new FileInfo(FileUploadTemp);
                            File.Copy(FileUploadTemp, FileUploadTemp.Replace("temp\\", ""), true);
                            fi1.Delete();
                        }
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Append("Problemas en la Carga del Archivo. " + ex.Message);
                        lConexion.Cerrar();
                    }
                }
                else
                    if (lsContReg == "S")
                    lblMensaje.Append("Debe Seleccionar el Archivo PDF Para eliminación de contratos registrados. <br>"); //20170601 rq020-17
                if (liReg == 0)
                    lblMensaje.Append("Debe Seleccionar Registros para Ejecutar el Proceso. <br>"); //20170601 rq020-17

                if (lblMensaje.ToString() != "")
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }

                SqlTransaction oTransaccion;
                lConexion.Abrir();
                oTransaccion = lConexion.gObjConexion.BeginTransaction(IsolationLevel.Serializable);
                try
                {
                    lsResultado = "no ha aprobado";
                    //oListaOperador.Add(goInfo.cod_comisionista, goInfo.cod_comisionista);
                    foreach (DataGridItem Grilla in dtgConsulta.Items)
                    {
                        CheckBox Checkbox;
                        Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                        if (!Checkbox.Checked) continue;
                        liReg++;
                        liNoSol = Convert.ToInt32(Grilla.Cells[10].Text);
                        liNoOpera = Convert.ToInt32(Grilla.Cells[1].Text);
                        liNoIdReg = Convert.ToInt32(Grilla.Cells[2].Text);
                        lsNoContra += liNoOpera + ",";
                        lsCodOPeraC = Grilla.Cells[16].Text.Trim();
                        lsCodOPeraV = Grilla.Cells[17].Text.Trim();
                        oListaOperador.Add(liNoOpera + "-" + lsCodOPeraC, "");
                        oListaOperador.Add(liNoOpera + "-" + lsCodOPeraV, "");
                        //oListaOperador.Add(lsCodOPeraC, liNoOpera.ToString());
                        //oListaOperador.Add(lsCodOPeraV, liNoOpera.ToString());
                        goInfo.mensaje_error = "";
                        lValorParametros[0] = liNoSol.ToString();
                        lValorParametros[1] = liNoIdReg.ToString();
                        lValorParametros[2] = liNoOpera.ToString();
                        lValorParametros[3] = goInfo.cod_comisionista;
                        lValorParametros[4] = lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\" + fileName;
                        lValorParametros[6] = TxtObservacion.Text.Trim();

                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(
                            lConexion.gObjConexion, "pa_SetSolicitudEliminaCont", lsNombreParametros,
                            lTipoparametros, lValorParametros, oTransaccion, goInfo);
                        if (goInfo.mensaje_error != "")
                        {
                            lblMensaje.Append("Se presentó un Problema en el Rechazo del Registro.! " + goInfo.mensaje_error); //20170601 rq020-17
                            oTransaccion.Rollback();
                            lConexion.Cerrar();
                            break;
                        }

                        lLector.Close();
                        lLector.Dispose();
                    }

                    if (lblMensaje.ToString() == "")
                    {
                        oTransaccion.Commit();
                    }
                    else
                    {
                        ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    oTransaccion.Rollback();
                    lConexion.Cerrar();
                    lblMensaje.Append("Se presentó un Error en el Rechazo de los Contratos. " + ex.Message); //20170601 rq020-17
                }

                if (lblMensaje.ToString() != "")
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }
                //// Envio Alerta a los Operadores
                string lsAsunto = "";
                string lsMensaje = "";
                string lsMensajeC = "";
                string lsMailC = "";
                string lsNomOperadorC = "";
                string lsMailV = "";
                string lsNomOperadorV = "";

                string lsNombreRem = "";
                /// Obtengo el mail del Operador que Realiza la Solicitud
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia",
                    "m_operador", " codigo_operador = " + goInfo.cod_comisionista + " ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsMailC = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" +
                              lLector["e_mail2"]; // 20180228 rq032-17
                    if (lLector["tipo_persona"].ToString() == "J")
                        lsNombreRem = lLector["razon_social"].ToString();
                    else
                        lsNombreRem = lLector["nombres"] + " " + lLector["apellidos"];
                }

                lLector.Close();
                lLector.Dispose();


                //// Envio mail a la BMC
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia",
                    "m_mail_procesos", " codigo_mail_proceso = 4 ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsMailC = lLector["mail_destinatario"].ToString();
                }

                lLector.Close();
                lLector.Dispose();

                ///// Envio del Mail de la Solicitud de la Correcion
                lsAsunto = "Notificación Solicitud Eliminación Contrato";
                lsMensaje = "Nos permitimos  informarle que " + lsNombreRem + " " + lsResultado +
                            " la Solicitud de Eliminación de la Operacion No. " + lsNoContra +
                            " en la plataforma SEGAS. <br><br>";
                lsMensaje += "<br><br>Observaciones: " + "" + "<br>" + TxtObservacion.Text.Trim() + "<br><br>";
                lsMensaje += "Cordialmente, <br><br><br>";
                lsMensaje += "Administrador SEGAS <br>";
                lsMensajeC = "Señores: Gestor del Mercado de Gas <br><br>" + lsMensaje;
                clEmail mailBMC = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                lblMensaje.Append(mailBMC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas"));

                /// Envio de los Mail de los Operadores Contrarios
                string lsCodOpera1 = "0";
                string lsNoCont = "";
                string[] lsLlave;
                /// Envio de los Mail de los Operadores Contrarios
                for (int i = 0; i < oListaOperador.Count; i++)
                {
                    lsLlave = oListaOperador.GetKey(i).ToString().Split('-');
                    lsNoCont = lsLlave[0].Trim();
                    lsCodOpera1 = lsLlave[1].Trim();
                    //lsCodOpera1 = oListaOperador.GetKey(i).ToString();
                    //lsNoCont = oListaOperador[lsCodOpera1].ToString();
                    if (lsCodOpera1 != "0")
                    {
                        /// Obtengo el mail del Operador
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion,
                            "pa_ValidarExistencia", "m_operador", " codigo_operador = " + lsCodOpera1 + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lsMailV = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" +
                                      lLector["e_mail2"]; // 20180228 rq032-17
                            if (lLector["tipo_persona"].ToString() == "J")
                                lsNomOperadorV = lLector["razon_social"].ToString();
                            else
                                lsNomOperadorV = lLector["nombres"] + " " + lLector["apellidos"];
                        }

                        lLector.Close();
                        lLector.Dispose();
                        ////
                        lsAsunto = "Notificación Solicitud Eliminación Contrato";
                        lsMensaje = "Nos permitimos  informarle que " + lsNombreRem + " " + lsResultado +
                                    " la Solicitud de Eliminación de la Operacion No. " + lsNoCont +
                                    " en la plataforma SEGAS. <br><br>";
                        lsMensaje += "<br><br>Observaciones: " + "" + "<br>" + TxtObservacion.Text.Trim() +
                                     "<br><br>";
                        lsMensaje += "Cordialmente, <br><br><br>";
                        lsMensaje += "Administrador SEGAS <br>";
                        lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                        clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                        lblMensaje.Append(mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas"));
                    }
                }

                lConexion.Cerrar();
                ToastrEvent?.Invoke("Solicitudes Rechazadas Correctamente.!", EnumTypeToastr.Success);
                CargarDatos();

                if (lblMensaje.ToString() != "")
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnRechzoBmc_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_codigo_verif_contrato", "@P_numero_contrato", "@P_operador", "@P_pdf", "@P_aprobacion", "@P_observacion", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0", "0", "0", "N", "", "3" }; // Aprobacion de la BMC
            var lblMensaje = new StringBuilder();
            int liReg = 0;
            int liNoSol = 0;
            int liNoOpera = 0;
            int liNoIdReg = 0;
            string lsCodOpera = "0";
            string lsAprobOpera = "N";
            string lsCodOPeraC = "";
            string lsCodOPeraV = "";
            SortedList oListaOperador = new SortedList();
            string lsResultado = "";

            try
            {
                if (TxtObservacion.Text.Trim().Length <= 0)
                    lblMensaje.Append("Debe Ingresar la Observación de la Aprobación. <br>"); //20170601 rq020-17

                if (lblMensaje.ToString() != "")
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }
                foreach (DataGridItem Grilla in dtgConsulta.Items)
                {
                    CheckBox Checkbox = null;
                    Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                    if (Checkbox.Checked == true)
                    {
                        liReg++;
                        liNoSol = Convert.ToInt32(Grilla.Cells[10].Text);
                        liNoOpera = Convert.ToInt32(Grilla.Cells[1].Text);
                        lsCodOpera = Grilla.Cells[17].Text;
                        lsAprobOpera = Grilla.Cells[14].Text;

                        if (liNoSol == 0)
                            lblMensaje.Append("La Operación " + liNoOpera + " No Tiene una Solicitud Pendiente de Aprobación por parte de la BMC. <br>"); //20170601 rq020-17
                        else
                        {
                            if (lsAprobOpera == "N" && Grilla.Cells[24].Text != "1")  //20170906 ajuste
                                lblMensaje.Append("La BMC NO puede Aprobar YA que el Operador No ha realizado la Aprobación en la Operación No. " + liNoOpera + " <br>"); //20170601 rq020-17
                        }
                    }
                }
                if (liReg == 0)
                    lblMensaje.Append("Debe Seleccionar Registros para Ejecutar el Proceso. <br>"); //20170601 rq020-17
                if (lblMensaje.ToString() != "")
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }

                SqlTransaction oTransaccion;
                lConexion.Abrir();
                oTransaccion = lConexion.gObjConexion.BeginTransaction(IsolationLevel.Serializable);
                try
                {
                    lsResultado = "no ha aprobado";
                    foreach (DataGridItem Grilla in dtgConsulta.Items)
                    {
                        CheckBox Checkbox = null;
                        Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                        if (Checkbox.Checked == true)
                        {
                            liReg++;
                            liNoSol = Convert.ToInt32(Grilla.Cells[10].Text);
                            liNoOpera = Convert.ToInt32(Grilla.Cells[1].Text);
                            liNoIdReg = Convert.ToInt32(Grilla.Cells[2].Text);
                            lsCodOPeraC = Grilla.Cells[16].Text.Trim();
                            lsCodOPeraV = Grilla.Cells[17].Text.Trim();
                            oListaOperador.Add(liNoOpera + "-" + lsCodOPeraC, "");
                            oListaOperador.Add(liNoOpera + "-" + lsCodOPeraV, "");
                            goInfo.mensaje_error = "";
                            lValorParametros[0] = liNoSol.ToString();
                            lValorParametros[1] = liNoIdReg.ToString();
                            lValorParametros[2] = liNoOpera.ToString();
                            lValorParametros[3] = goInfo.cod_comisionista;
                            lValorParametros[6] = TxtObservacion.Text.Trim();

                            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(
                                lConexion.gObjConexion, "pa_SetSolicitudEliminaCont", lsNombreParametros,
                                lTipoparametros, lValorParametros, oTransaccion, goInfo);
                            if (goInfo.mensaje_error != "")
                            {
                                lblMensaje.Append("Se presento un Problema en el Rechazo del Registro.! " + goInfo.mensaje_error);
                                oTransaccion.Rollback();
                                lConexion.Cerrar();
                                break;
                            }

                            lLector.Close();
                            lLector.Dispose();
                        }
                    }

                    if (lblMensaje.ToString() == "")
                    {
                        oTransaccion.Commit();
                    }
                }
                catch (Exception ex)
                {
                    oTransaccion.Rollback();
                    lConexion.Cerrar();
                    lblMensaje.Append("Se presento un Error en Rechazo de las Solicitudes. " + ex.Message);
                }

                if (lblMensaje.ToString() != "")
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }
                //// Envio Alerta a los Operadores
                string lsAsunto = "";
                string lsMensaje = "";
                string lsMensajeC = "";
                string lsMailV = "";
                string lsMailC = "";
                string lsNomOperadorV = "";
                string lsCodOpera1 = "0";
                string lsNoCont = "";

                //// Envio mail a la BMC
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia",
                    "m_mail_procesos", " codigo_mail_proceso = 4 ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsMailC = lLector["mail_destinatario"].ToString();
                }

                lLector.Close();
                lLector.Dispose();
                string lsOp1 = "";

                string[] lsLlave;
                /// Envio de los Mail de los Operadores Contrarios
                for (int i = 0; i < oListaOperador.Count; i++)
                {
                    lsLlave = oListaOperador.GetKey(i).ToString().Split('-');
                    lsNoCont = lsLlave[0].Trim();
                    lsCodOpera1 = lsLlave[1].Trim();

                    if (lsCodOpera1 != "0")
                    {
                        /// Obtengo el mail del Operador
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion,
                            "pa_ValidarExistencia", "m_operador", " codigo_operador = " + lsCodOpera1 + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lsMailV = lLector["e_mail"] + ";" + lLector["e_mail2"] + ";" +
                                      lLector["e_mail2"]; // 20180228 rq032-17
                            if (lLector["tipo_persona"].ToString() == "J")
                                lsNomOperadorV = lLector["razon_social"].ToString();
                            else
                                lsNomOperadorV = lLector["nombres"] + " " + lLector["apellidos"];
                        }

                        lLector.Close();
                        lLector.Dispose();
                        ////
                        lsAsunto = "Notificación Solicitud Eliminación Contrato";
                        lsMensaje = "Nos permitimos  informarle que el Gestor del Mercado de Gas " + lsResultado +
                                    " la Solicitud de Eliminación de la Operacion No. " + lsNoCont +
                                    " en la plataforma SEGAS. <br><br>";
                        lsMensaje += "<br><br>Observaciones: " + "" + "<br>" + TxtObservacion.Text.Trim() +
                                     "<br><br>";
                        lsMensaje += "Cordialmente, <br><br><br>";
                        lsMensaje += "Administrador SEGAS <br>";
                        lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                        clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                        lblMensaje.Append(mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas"));
                        if (lsOp1 != lsNoCont)
                        {
                            lsOp1 = lsNoCont;
                            lsAsunto = "Notificación Solicitud Eliminación Contrato";
                            lsMensaje = "Nos permitimos  informarle que el Gestor del Mercado de Gas " +
                                        lsResultado + " la Solicitud de Eliminación de la Operacion No. " +
                                        lsNoCont + " en la plataforma SEGAS. <br><br>";
                            lsMensaje += "<br><br>Observaciones: " + "" + "<br>" + TxtObservacion.Text.Trim() +
                                         "<br><br>";
                            lsMensaje += "Cordialmente, <br><br><br>";
                            lsMensaje += "Administrador SEGAS <br>";
                            lsMensajeC = "Señores: Gestor del Mercado de Gas <br><br>" + lsMensaje;
                            clEmail mailBmc = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                            lblMensaje.Append(mailBmc.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas"));
                        }
                    }
                }

                lConexion.Cerrar();
                ToastrEvent?.Invoke("Solicitudes Rechazadas Correctamente.!", EnumTypeToastr.Success);
                CargarDatos();

                if (lblMensaje.ToString() == "") return;
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void lkbExcel_Click(object sender, EventArgs e)
        {
            try
            {

                string lsNombreArchivo = Session["login"] + "InfExcelReg" + DateTime.Now + ".xls";
                StringBuilder lsb = new StringBuilder();
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                lpagina.EnableEventValidation = false;
                lpagina.Controls.Add(lform);
                dtgConsulta.EnableViewState = false;
                dtgConsulta.Columns[0].Visible = false;
                dtgConsulta.Columns[21].Visible = false;
                dtgConsulta.Columns[22].Visible = false;
                lform.Controls.Add(dtgConsulta);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = Encoding.Default;

                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
                //20170601 rq020-17
                Response.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + "Consulta Solicitud Eliminación Contratos" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write(lsb.ToString());
                Response.End();
                Response.Flush();
                dtgConsulta.Columns[0].Visible = true;
                dtgConsulta.Columns[21].Visible = true;
                dtgConsulta.Columns[22].Visible = true;
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Problemas al Consultar los Registros. " + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnConsultar_Click(object sender, EventArgs e)
        {
            CargarDatos();
        }

        /// <summary>
        /// Evento que guarda temporalmente el archivo cargado al servidor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FileUpload_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                // Se valida si se cargo el archivo original  
                if (string.IsNullOrEmpty(FuArchivo.FileName)) return;
                // Se crea la ruta de los archivos temporales
                Directory.CreateDirectory(sRutaArc + "temp\\");
                string fileUploadPath = Path.Combine(sRutaArc, "temp\\", FuArchivo.FileName);
                FuArchivo.SaveAs(fileUploadPath);
                FileUploadTemp = fileUploadPath;
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }
    }
}