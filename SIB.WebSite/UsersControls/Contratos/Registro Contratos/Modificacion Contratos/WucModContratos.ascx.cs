﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace UsersControls.Contratos.Registro_Contratos.Modificacion_Contratos
{
    public partial class WucModContratos : UserControl
    {
        #region EventHandler

        /// <summary>
        /// Delegado para el manejo de los modals
        /// </summary>
        public delegate void EventHandlerModal(string id, string insideId, EnumTypeModal typeModal, Modal.Size size = Modal.Size.Large);

        /// <summary>
        /// EventHandler para la selección de los los modals
        /// </summary>
        public event EventHandlerModal ModalEvent;

        /// <summary>
        /// Delegado para el manejo de los toastr
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        /// EventHandler para la selección de los toastr 
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        ///Delegado para la apertura de un documento en una nueva ventana del navegador 
        /// </summary>
        public delegate void EventHandlerDocument(string pathDocument);

        /// <summary>
        ///EventHandler para la apertura de un documento en una nueva ventana del navegador    
        /// </summary>
        public event EventHandlerDocument DocumentEvent;

        /// <summary>
        /// Delegado para la selección de los botones  
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get { return ViewState["NameController"] != null && !string.IsNullOrEmpty(ViewState["NameController"].ToString()) ? ViewState["NameController"].ToString() : string.Empty; }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfIdRegistro
        {
            get { return ViewState["HdfIdRegistro"] != null && !string.IsNullOrEmpty(ViewState["HdfIdRegistro"].ToString()) ? ViewState["HdfIdRegistro"].ToString() : string.Empty; }
            set { ViewState["HdfIdRegistro"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfCodModif
        {
            get { return ViewState["HdfCodModif"] != null && !string.IsNullOrEmpty(ViewState["HdfCodModif"].ToString()) ? ViewState["HdfCodModif"].ToString() : string.Empty; }
            set { ViewState["HdfCodModif"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfPunta
        {
            get { return ViewState["HdfPunta"] != null && !string.IsNullOrEmpty(ViewState["HdfPunta"].ToString()) ? ViewState["HdfPunta"].ToString() : string.Empty; }
            set { ViewState["HdfPunta"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfNoSolicitud
        {
            get { return ViewState["HdfNoSolicitud"] != null && !string.IsNullOrEmpty(ViewState["HdfNoSolicitud"].ToString()) ? ViewState["HdfNoSolicitud"].ToString() : string.Empty; }
            set { ViewState["HdfNoSolicitud"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfCausa
        {
            get { return ViewState["HdfCausa"] != null && !string.IsNullOrEmpty(ViewState["HdfCausa"].ToString()) ? ViewState["HdfCausa"].ToString() : string.Empty; }
            set { ViewState["HdfCausa"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfTipoCausa
        {
            get { return ViewState["HdfTipoCausa"] != null && !string.IsNullOrEmpty(ViewState["HdfTipoCausa"].ToString()) ? ViewState["HdfTipoCausa"].ToString() : string.Empty; }
            set { ViewState["HdfTipoCausa"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfConsec
        {
            get { return ViewState["HdfConsec"] != null && !string.IsNullOrEmpty(ViewState["HdfConsec"].ToString()) ? ViewState["HdfConsec"].ToString() : string.Empty; }
            set { ViewState["HdfConsec"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfOperador
        {
            get { return ViewState["HdfOperador"] != null && !string.IsNullOrEmpty(ViewState["HdfOperador"].ToString()) ? ViewState["HdfOperador"].ToString() : string.Empty; }
            set { ViewState["HdfOperador"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfContratoDef
        {
            get { return ViewState["HdfContratoDef"] != null && !string.IsNullOrEmpty(ViewState["HdfContratoDef"].ToString()) ? ViewState["HdfContratoDef"].ToString() : string.Empty; }
            set { ViewState["HdfContratoDef"] = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string HdfCodigoVerUsr
        {
            get { return ViewState["HdfCodigoVerUsr"] != null && !string.IsNullOrEmpty(ViewState["HdfCodigoVerUsr"].ToString()) ? ViewState["HdfCodigoVerUsr"].ToString() : string.Empty; }
            set { ViewState["HdfCodigoVerUsr"] = value; }
        }
        //20200820 ajsute 
        /// <summary>
        /// 
        /// </summary>
        public string HdfModificaFin
        {
            get { return ViewState["HdfModificaFin"] != null && !string.IsNullOrEmpty(ViewState["HdfModificaFin"].ToString()) ? ViewState["HdfModificaFin"].ToString() : string.Empty; }
            set { ViewState["HdfModificaFin"] = value; }
        }
        /// <summary>
        /// Ruta del archivo original cargado temporalmente en el servidor 
        /// </summary>
        public string FileUploadOriginal_Temp
        {
            get { return Session["FileUploadOriginal_Temp"] != null && !string.IsNullOrEmpty(Session["FileUploadOriginal_Temp"].ToString()) ? Session["FileUploadOriginal_Temp"].ToString() : string.Empty; }
            set { Session["FileUploadOriginal_Temp"] = value; }
        }

        /// <summary>
        /// Ruta del archivo temporal cargado temporalmente en el servidor 
        /// </summary>
        public string FileUploadModificado_Temp
        {
            get { return Session["FileUploadModificado_Temp"] != null && !string.IsNullOrEmpty(Session["FileUploadModificado_Temp"].ToString()) ? Session["FileUploadModificado_Temp"].ToString() : string.Empty; }
            set { Session["FileUploadModificado_Temp"] = value; }
        }

        #endregion Propiedades

        private InfoSessionVO goInfo = null;
        private static string lsTitulo = "Modificación de registro de contratos";
        private clConexion lConexion = null;
        private SqlDataReader lLector;
        private DataSet lds = new DataSet();
        private string[] lsCarperta;
        private string Carpeta;
        private string sRutaArc = ConfigurationManager.AppSettings["rutaModif"];
        private string lsCarpetaAnt = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            goInfo.Programa = lsTitulo;
            lConexion = new clConexion(goInfo);

            if (IsPostBack) return;
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            /// Llenar controles del Formulario 
            lConexion.Abrir();
            LlenarControles2(lConexion.gObjConexion, DdlBusVenta, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20190425 rq022-19
            LlenarControles2(lConexion.gObjConexion, DdlBusCompra, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20190425 rq022-19
            LlenarControles2(lConexion.gObjConexion, DdlBusContra, "m_operador ope", " estado = 'A' and codigo_operador !=0 and exists (select 1 from t_contrato_verificacion ver where (ope.codigo_operador= ver.operador_compra or ope.codigo_operador= ver.operador_venta) and (ver.operador_compra = " + goInfo.cod_comisionista + " or ver.operador_venta = " + goInfo.cod_comisionista + ")) and ope.codigo_operador <> " + goInfo.cod_comisionista + " order by razon_social", 0, 4); //20190425 rq022-19
            LlenarControles(lConexion.gObjConexion, DdlBusCausa, "m_causa_modificacion", "  estado ='A' and tipo_causa<> 'C' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlBusEstado, "m_estado_gas", "  tipo_estado ='M'  and sigla_estado not in ('R','L')  order by descripcion_estado", 2, 3);
            LlenarControles(lConexion.gObjConexion, DdlBusModalidad, "m_modalidad_contractual", "  estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlBusCausa1, "m_causa_modificacion", "  estado ='A' and tipo_causa <> 'C' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlBusEstado1, "m_estado_gas", "  tipo_estado ='M'  and sigla_estado not in ('R','L') order by descripcion_estado", 2, 3);
            LlenarControles(lConexion.gObjConexion, DdlIngCausa, "m_causa_modificacion", "  estado ='A' and tipo_causa not in  ('C','R') order by descripcion", 0, 1);
            LlenarControles2(lConexion.gObjConexion, DdlVendedor, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20190425 rq022-19
            LlenarControles2(lConexion.gObjConexion, DdlComprador, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20190425 rq022-19
            LlenarControles(lConexion.gObjConexion, DdlFuente, "m_pozo", " ind_campo_pto='C' and estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlPunto, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles1(lConexion.gObjConexion, DdlCentro, "m_divipola", " codigo_centro <> '0' and estado ='A' order by nombre_centro", 5, 6);
            LlenarControles(lConexion.gObjConexion, DdlModalidad, "m_modalidad_contractual", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlPeriodo, "m_periodos_entrega", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, dlTipoDemanda, "m_tipo_demanda_atender", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo sec, m_demanda_sector dem", " sec.estado ='A' and sec.codigo_sector_consumo = dem.codigo_sector_consumo  and dem.estado ='A' and dem.codigo_tipo_demanda =1 and dem.ind_uso ='T' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMercadoRel, "m_mercado_relevante", " estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlDeptoPuntoSal, "m_divipola", " codigo_ciudad = '0' and estado ='A' order by nombre_departamento", 1, 2);
            LlenarControles(lConexion.gObjConexion, ddlMercadoRelevante, "m_mercado_relevante", " estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlSectorConsumo, "m_sector_consumo sec", " sec.estado ='A' and estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlRutaMay, "m_ruta_snt", " estado = 'A' order by descripcion", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlTrm, "m_tipo_trm", " estado = 'A' order by descripcion", 0, 1); //20210707 trm moneda
            lConexion.Cerrar();
            HdfOperador = goInfo.cod_comisionista;

            if (goInfo.cod_comisionista == "0")
            {
                tr01.Visible = false;
                tr02.Visible = true;
                tr03.Visible = true;
            }
            else
            {
                tr01.Visible = true;
                tr02.Visible = false;
                tr03.Visible = false;
            }

            EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Excel };
            ButtonsEvent?.Invoke(botones, null, NameController);
            //Se establece que el controlador ya se inicializo    
            Inicializado = true;
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles2
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles2(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"] + "-" + lLector["razon_social"];
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles1
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// 20170814 rq036-17
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["nombre_centro"] + "-" + lLector["nombre_ciudad"] + "-" + lLector["nombre_departamento"];
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnConsultar_Click(object sender, EventArgs e)
        {
            CargarDatos();
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            var valido = true;
            List<EnumBotones> botones;

            DateTime ldFecha;
            string[] lsNombreParametros = { "@P_numero_contrato", "@P_codigo_verif", "@P_contrato_definitivo", "@P_fecha_inicial", "@P_fecha_final",
                "@P_codigo_operador", "@P_codigo_contraparte", "@P_operador_compra", "@P_operador_venta","@P_tipo_mercado","@P_destino_rueda",
                "@P_codigo_modalidad","@P_codigo_causa","@P_estado_modif" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar , SqlDbType.VarChar,
                SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar};
            string[] lValorParametros = { "0", "0", "", "", "", "0", "0", "0", "0", "", "", "0", "0", "" };

            if (TxtBusFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtBusFechaIni.Text);
                }
                catch (Exception)
                {
                    ToastrEvent?.Invoke("Formato Inválido en el Campo Fecha Inicial de Negociación.", EnumTypeToastr.Error);
                    valido = false;
                }
            }
            if (TxtBusFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtBusFechaFin.Text);
                }
                catch (Exception)
                {
                    ToastrEvent?.Invoke("Formato Inválido en el Campo Fecha Final de Negociación.", EnumTypeToastr.Error);
                    valido = false;
                }
            }

            if (TxtBusFechaIni.Text.Trim().Length == 0 && TxtBusFechaFin.Text.Trim().Length > 0)
            {
                ToastrEvent?.Invoke("Debe digitar la Fecha Inicial de Negociación antes que la final.", EnumTypeToastr.Error);
                valido = false;
            }

            if (TxtBusFechaIni.Text.Trim().Length > 0 && TxtBusFechaFin.Text.Trim().Length > 0)
                try
                {
                    if (Convert.ToDateTime(TxtFechaFin.Text) < Convert.ToDateTime(TxtFechaIni.Text))
                    {
                        ToastrEvent?.Invoke("La Fecha inicial de Negociación debe ser menor o igual que la fecha final.", EnumTypeToastr.Error);
                        valido = false;
                    }
                }
                catch (Exception)
                {
                }

            if (valido)
            {
                try
                {
                    if (TxtBusOperacion.Text.Trim().Length > 0)
                        lValorParametros[0] = TxtBusOperacion.Text.Trim();
                    if (TxtBusId.Text.Trim().Length > 0)
                        lValorParametros[1] = TxtBusId.Text.Trim();
                    lValorParametros[2] = TxtBusContratoDef.Text.Trim();
                    if (TxtBusFechaIni.Text.Trim().Length > 0)
                    {
                        lValorParametros[3] = TxtBusFechaIni.Text.Trim();
                        if (TxtBusFechaFin.Text.Trim().Length > 0)
                            lValorParametros[4] = TxtBusFechaFin.Text.Trim();
                        else
                            lValorParametros[4] = TxtBusFechaIni.Text.Trim();
                    }
                    lValorParametros[5] = goInfo.cod_comisionista;
                    lValorParametros[6] = DdlBusContra.SelectedValue;
                    lValorParametros[7] = DdlBusCompra.SelectedValue;
                    lValorParametros[8] = DdlBusVenta.SelectedValue;
                    lValorParametros[9] = DdlBusMercado.SelectedValue;
                    lValorParametros[10] = DdlBusProducto.SelectedValue;
                    lValorParametros[11] = DdlBusModalidad.SelectedValue;
                    if (goInfo.cod_comisionista == "0")
                    {
                        lValorParametros[12] = DdlBusCausa1.SelectedValue;
                        lValorParametros[13] = DdlBusEstado1.SelectedValue;
                    }
                    else
                    {
                        lValorParametros[12] = DdlBusCausa.SelectedValue;
                        lValorParametros[13] = DdlBusEstado.SelectedValue;
                    }
                    lConexion.Abrir();
                    dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContSolMod", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgConsulta.DataBind();
                    dtgConsultaExcel.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContSolMod", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgConsultaExcel.DataBind();

                    lConexion.Cerrar();
                    if (dtgConsulta.Items.Count > 0)
                    {
                        botones = (new[] { EnumBotones.Buscar, EnumBotones.Excel }).OfType<EnumBotones>().ToList();
                    }
                    else
                    {
                        botones = new[] { EnumBotones.Buscar }.OfType<EnumBotones>().ToList();
                        ToastrEvent?.Invoke("No se encontraron Registros.", EnumTypeToastr.Info);
                    }
                    ButtonsEvent?.Invoke(botones.ToArray(), null, NameController);
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("No se Pudo consultar la información.! " + ex.Message, EnumTypeToastr.Error);
                }
            }
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
        {
            var valido = true;
            HdfNoSolicitud = "0";
            HdfTipoCausa = e.Item.Cells[29].Text;
            trReestructura.Visible = false;
            HdfConsec = "0";
            if (e.CommandName.Equals("Modificar"))
            {
                HdfIdRegistro = e.Item.Cells[4].Text;
                HdfContratoDef = e.Item.Cells[6].Text; //20200430 ajsute nombre archivo //20200727 ajsute front-end
                if (goInfo.cod_comisionista == "0")
                {
                    if (e.Item.Cells[28].Text != "P")
                    {
                        ToastrEvent?.Invoke("No está pendiente de aprobación de la BMC", EnumTypeToastr.Error);
                        valido = false;
                    }
                    //20190607 rq036-19
                    if (goInfo.codigo_grupo_usuario != 1)
                    {
                        ToastrEvent?.Invoke("Solo el administrador puede aprobar la modificación", EnumTypeToastr.Error);
                        valido = false;
                    }
                }
                else
                {
                    if (e.Item.Cells[28].Text != "I" && e.Item.Cells[28].Text != "E" && e.Item.Cells[28].Text != "S" && e.Item.Cells[28].Text != "M" && e.Item.Cells[28].Text != "C")
                    {
                        ToastrEvent?.Invoke("No se puede modificar porque ya se aprobó o se rechazó", EnumTypeToastr.Error);
                        valido = false;
                    }
                    else
                    {
                        if (e.Item.Cells[28].Text == "C" && e.Item.Cells[22].Text == "A")
                        {
                            ToastrEvent?.Invoke(
                                "No se puede aprobar porque no se han ingresado los datos de la modificación", EnumTypeToastr.Error);
                            valido = false;
                        }

                        if (e.Item.Cells[28].Text == "E" && e.Item.Cells[22].Text == "A")
                        {
                            ToastrEvent?.Invoke("No se puede aprobar porque está pendiente de corrección", EnumTypeToastr.Error);
                            valido = false;
                        }
                    }

                    if (e.Item.Cells[29].Text == "R")
                    {
                        ToastrEvent?.Invoke("La modificación por reestructuración no se puede manejar por pantalla", EnumTypeToastr.Error);
                        valido = false;
                    }

                    if (e.Item.Cells[30].Text == "S")
                    {
                        ToastrEvent?.Invoke("Hay una solicitud de modificación en curso para el contrato", EnumTypeToastr.Error);
                        valido = false;
                    }
                }

                if (e.Item.Cells[29].Text == "C")
                {
                    ToastrEvent?.Invoke("La operación está en proceso de modificación por cesión", EnumTypeToastr.Success);
                    valido = false;
                }

                if (manejo_bloqueo("V", HdfIdRegistro))
                {
                    ToastrEvent?.Invoke("Se está modificando la operación", EnumTypeToastr.Info);
                    valido = false;
                }

                if (valido)
                {
                    manejo_bloqueo("A", HdfIdRegistro);
                    DdlDestino.SelectedValue = e.Item.Cells[24].Text;
                    DdlMercado.SelectedValue = e.Item.Cells[23].Text;
                    //20220828
                    if (DdlMercado.SelectedValue == "O")
                    {
                        if (DdlDestino.SelectedValue == "G")
                        {
                            LblPrecio.Text = "Precio a la fecha de suscripción del contrato (USD/MBTU)";
                        }
                        else
                        {
                            LblPrecio.Text = "Precio de la capacidad de transporte (Moneda vigente/KPC)";
                        }
                        LblPunto.Text = "Punto de salida del SNT";
                    }
                    else
                    {
                        if (DdlDestino.SelectedValue == "G")
                        {
                            LblPrecio.Text = "Precio a la fecha de suscripción del contrato (USD/MBTU)";
                            LblPunto.Text = "Punto de entrega de la energía al comprador";
                        }
                        else
                        {
                            LblPrecio.Text = "Precio a la fecha de suscripción del contrato (Moneda vigente/KPC)";
                            LblPunto.Text = "Ruta";
                        }
                    }
                    HdfPunta = e.Item.Cells[25].Text;
                    HdfNoSolicitud = e.Item.Cells[2].Text;
                    HdfCausa = e.Item.Cells[26].Text;
                    HdfCodModif = e.Item.Cells[27].Text;

                    trae_usr_fin(HdfCodModif, HdfIdRegistro);
                    if (e.Item.Cells[22].Text == "C")
                    {
                        color_label("N"); // 20190425 rq022-19 ajuste modificacion
                        //Abre el modal de Causa Modificación
                        ModalEvent?.Invoke(modContratoCausaMod.ID, modContratoCausaModInside.ID, EnumTypeModal.Abrir);
                    }
                    if (e.Item.Cells[22].Text == "M")
                    {
                        color_label("N"); // 20190425 rq022-19 ajuste modificacion
                        modificar();
                    }
                    if (e.Item.Cells[22].Text == "A")
                    {
                        color_label("R"); // 20190425 rq022-19 ajuste modificacion
                        aprobar();
                    }
                    if (e.Item.Cells[22].Text == "B")
                    {
                        color_label("R"); // 20190425 rq022-19 ajuste modificacion
                        consultar("A");
                    }

                    dtgUsuarios.Columns[18].Visible = !(HdfTipoCausa == "T" || HdfTipoCausa == "A");
                    if (HdfTipoCausa == "T" || HdfTipoCausa == "A") btnCrearUsu.Visible = false;
                }
            }
            if (e.CommandName.Equals("Consultar"))
            {
                DdlDestino.SelectedValue = e.Item.Cells[24].Text;
                DdlMercado.SelectedValue = e.Item.Cells[23].Text;
                //20220828
                if (DdlMercado.SelectedValue == "O")
                {
                    if (DdlDestino.SelectedValue == "G")
                    {
                        LblPrecio.Text = "Precio a la fecha de suscripción del contrato (USD/MBTU)";
                    }
                    else
                    {
                        LblPrecio.Text = "Precio a la fecha de suscripción del contrato (USD/KPC)";
                    }
                    LblPunto.Text = "Punto de salida del SNT";
                }
                else
                {
                    if (DdlDestino.SelectedValue == "G")
                    {
                        LblPrecio.Text = "Precio a la fecha de suscripción del contrato (USD/MBTU)";
                        LblPunto.Text = "Punto de entrega de la energía al comprador";
                    }
                    else
                    {
                        LblPrecio.Text = "Precio a la fecha de suscripción del contrato (Moneda vigente/KPC)";
                        LblPunto.Text = "Ruta";
                    }
                }

                if (e.Item.Cells[29].Text == "R")
                {
                    string[] lsNombreParametros = { "@P_codigo_solicitud" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int };
                    string[] lValorParametros = { e.Item.Cells[2].Text };

                    lConexion.Abrir();
                    dtgReestructura.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetDatosContModRes", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgReestructura.DataBind();
                    lConexion.Cerrar();
                    trReestructura.Visible = true;
                }

                HdfIdRegistro = e.Item.Cells[4].Text;
                color_label("R"); // 20190425 rq022-19 ajuste modificacion
                consultar("C");
                btnCrearUsu.Visible = false;
                dtgUsuarios.Columns[18].Visible = false;

            }
            if (e.CommandName.Equals("Original"))
            {
                try
                {
                    if (e.Item.Cells[19].Text.Trim() != "&nbsp;" && e.Item.Cells[19].Text.Trim() != "")
                    {
                        lsCarperta = sRutaArc.Split('\\');
                        lsCarpetaAnt = lsCarperta[lsCarperta.Length - 2];
                        var lsRuta = "../../" + lsCarpetaAnt + "/" + e.Item.Cells[19].Text.Replace(@"\", "/");
                        DocumentEvent?.Invoke(lsRuta);
                    }
                    else
                        ToastrEvent?.Invoke("No hay archivo para visualizar", EnumTypeToastr.Info);
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Problemas al mostrar el archivo. " + ex.Message, EnumTypeToastr.Error);
                }
            }
            if (e.CommandName.Equals("Modificado"))
            {
                try
                {
                    if (e.Item.Cells[20].Text.Trim() != "&nbsp;" && e.Item.Cells[20].Text.Trim() != "")
                    {
                        lsCarperta = sRutaArc.Split('\\');
                        lsCarpetaAnt = lsCarperta[lsCarperta.Length - 2];
                        var lsRuta = "../../" + lsCarpetaAnt + "/" + e.Item.Cells[20].Text.Replace(@"\", "/");
                        DocumentEvent?.Invoke(lsRuta);
                    }
                    else
                        ToastrEvent?.Invoke("No hay archivo para visualizar", EnumTypeToastr.Info);
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Problemas al mostrar el archivo. " + ex.Message, EnumTypeToastr.Error);
                }
            }
        }

        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void crear()
        {
            //Cierra el modal de causa modificación
            ModalEvent?.Invoke(modContratoCausaMod.ID, modContratoCausaMod.ID, EnumTypeModal.Cerrar);
            //Abre el modal de Modificación
            ModalEvent?.Invoke(modContratosMod.ID, modContratosModInside.ID, EnumTypeModal.Abrir);
            btnModificar.Visible = true;
            btnAprobar.Visible = false;
            btnRechazar.Visible = false;
            btnAprobarBmc.Visible = false;
            btnRechazarBmc.Visible = false;
            trObs.Visible = false;
            mostrar_datos("C");
        }

        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void mostrar_datos(string lsAccion)
        {

            DdlComprador.SelectedValue = "0";
            TxNitComprador.Text = "";
            DdlVendedor.SelectedValue = "0";
            TxtFechaNeg.Text = "";
            TxtHoraNeg.Text = "";
            TxtFechaSus.Text = "";
            TxtContDef.Text = "";
            DdlFuente.SelectedValue = "0";
            DdlVariable.SelectedValue = "N";
            DdlConectado.SelectedValue = "S";
            DdlBoca.SelectedValue = "N";
            DdlPunto.SelectedValue = "0";
            DdlCentro.SelectedValue = "0";
            DdlModalidad.SelectedValue = "0";
            DdlPeriodo.SelectedValue = "0";
            TxtNoAños.Text = "";
            TxtFechaIni.Text = "";
            TxtHoraIni.Text = "";
            TxtFechaFin.Text = "";
            TxtHoraFin.Text = "";
            TxtCantidad.Text = "";
            TxtPrecio.Text = "";
            DdlSentido.SelectedValue = "NORMAL";
            TxtPresion.Text = "";
            TxtGarantia.Text = "";
            TxtVlrGrtia.Text = "0";
            TxtFechaGrtia.Text = "";
            ddlDeptoPuntoSal.SelectedValue = "0";
            DdlMunPuntoSal_SelectedIndexChanged(null, null);
            ddlSectorConsumo.SelectedValue = "0";
            ddlConexion.SelectedValue = "S";
            ddlMercadoRelevante.SelectedValue = "0";
            TxtCapacOtm.Text = "";
            ddlRutaMay.SelectedValue = "0";
            ddlTrm.SelectedValue = "0"; // 20210707 trm moneda
            txtObsTrm.Text = ""; // 20210707 trm moneda

            DdlComprador.Enabled = true;
            TxNitComprador.Enabled = true;
            DdlVendedor.Enabled = true;
            TxtFechaNeg.Enabled = true;
            TxtHoraNeg.Enabled = true;
            TxtFechaSus.Enabled = true;
            TxtContDef.Enabled = true;
            DdlFuente.Enabled = true;
            DdlVariable.Enabled = true;
            DdlConectado.Enabled = true;
            DdlBoca.Enabled = true;
            DdlPunto.Enabled = true;
            DdlCentro.Enabled = true;
            DdlModalidad.Enabled = true;
            DdlPeriodo.Enabled = true;
            TxtNoAños.Enabled = true;
            TxtFechaIni.Enabled = true;
            TxtHoraIni.Enabled = true;
            TxtFechaFin.Enabled = true;
            TxtHoraFin.Enabled = true;
            TxtCantidad.Enabled = true;
            TxtPrecio.Enabled = true;
            DdlSentido.Enabled = true;
            TxtPresion.Enabled = true;
            TxtGarantia.Enabled = true;
            TxtVlrGrtia.Enabled = true;
            TxtFechaGrtia.Enabled = true;
            ddlSectorConsumo.Enabled = true;
            ddlConexion.Enabled = true;
            ddlDeptoPuntoSal.Enabled = true;
            DdlMunPuntoSal.Enabled = true;
            ddlCentroPuntoSal.Enabled = true;
            TxtCapacOtm.Enabled = true;
            ddlRutaMay.Enabled = false;
            ddlTrm.Enabled = true; //20210707 trm moneda
            txtObsTrm.Enabled = true; //20210707 trm moneda
            ddlMoneda.Enabled = true; //20210707 trm moneda

            try
            {
                //Abre el modal de Modificación
                ModalEvent?.Invoke(modContratosMod.ID, modContratosModInside.ID, EnumTypeModal.Abrir);
                EnumBotones[] botones = { EnumBotones.Buscar };
                ButtonsEvent?.Invoke(botones, null, NameController);

                if (HdfPunta == "C" && DdlMercado.SelectedValue != "O") //20210824 ajuste
                {
                    // Se muestra la sección de demanda
                    tblDemanda.Visible = true;
                    btnCrearUsu.Visible = true;
                }
                else
                {
                    tblDemanda.Visible = false;//20210824 ajuste
                    btnCrearUsu.Visible = false;
                }

                if (DdlMercado.SelectedValue != "O")
                {
                    if (DdlDestino.SelectedValue == "G")
                    {
                        DdlPunto.Items.Clear();
                        lConexion.Abrir();
                        LlenarControles(lConexion.gObjConexion, DdlPunto, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
                        lConexion.Cerrar();
                    }
                    else
                    {
                        DdlPunto.Items.Clear();
                        lConexion.Abrir();
                        LlenarControles(lConexion.gObjConexion, DdlPunto, "m_ruta_snt", " estado = 'A' order by descripcion", 0, 4);
                        lConexion.Cerrar();
                    }
                }
                else
                {
                    LblPunto.Text = "Punto de Salida SNT";
                    DdlPunto.Items.Clear();
                    lConexion.Abrir();
                    LlenarControles(lConexion.gObjConexion, DdlPunto, "m_punto_salida_snt", " estado = 'A'  order by descripcion", 0, 2);
                    lConexion.Cerrar();
                }

                if (DdlDestino.SelectedValue == "G" && DdlMercado.SelectedValue == "P")
                {
                    LblFuente.Visible = true;
                    DdlFuente.Visible = true;
                    trSumSec.Visible = false;
                    DdlConectado.SelectedValue = "S";
                    DdlBoca.SelectedValue = "N";
                    DdlPunto.Visible = true;
                    DdlCentro.Visible = false;
                    DdlCentro.SelectedValue = "0";
                    trFlujo.Visible = false;
                    TxtPresion.Text = "0";
                    trPeriodo.Visible = true;
                    trMayor1.Visible = false;
                    trMayor2.Visible = false;
                    trMayor3.Visible = false;
                    trMayor4.Visible = false;
                    TxtVlrGrtia.Text = "0";
                    TxtCapacOtm.Visible = false;
                }
                if (DdlDestino.SelectedValue == "G" && DdlMercado.SelectedValue == "S")
                {
                    LblFuente.Visible = false;
                    DdlFuente.Visible = false;
                    DdlFuente.SelectedValue = "0";
                    trSumSec.Visible = true;
                    DdlConectado.SelectedValue = "S";
                    DdlBoca.SelectedValue = "N";
                    DdlPunto.Visible = true;
                    DdlCentro.Visible = false;
                    DdlCentro.SelectedValue = "0";
                    trFlujo.Visible = false;
                    TxtPresion.Text = "0";
                    trPeriodo.Visible = true;
                    trMayor1.Visible = false;
                    trMayor2.Visible = false;
                    trMayor3.Visible = false;
                    trMayor4.Visible = false;
                    TxtVlrGrtia.Text = "0";
                    TxtCapacOtm.Visible = false;
                }
                if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "P")
                {
                    LblFuente.Visible = false;
                    DdlFuente.Visible = false;
                    DdlFuente.SelectedValue = "0";
                    trSumSec.Visible = false;
                    DdlConectado.SelectedValue = "S";
                    DdlBoca.SelectedValue = "N";
                    DdlPunto.Visible = true;
                    DdlCentro.Visible = false;
                    DdlCentro.SelectedValue = "0";
                    trFlujo.Visible = true;
                    TxtPresion.Text = "";
                    trPeriodo.Visible = true;
                    trMayor1.Visible = false;
                    trMayor2.Visible = false;
                    trMayor3.Visible = false;
                    trMayor4.Visible = false;
                    TxtVlrGrtia.Text = "0";
                    TxtCapacOtm.Visible = false;
                }
                if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "S")
                {
                    LblFuente.Visible = false;
                    DdlFuente.Visible = false;
                    DdlFuente.SelectedValue = "0";
                    trSumSec.Visible = false;
                    DdlConectado.SelectedValue = "S";
                    DdlBoca.SelectedValue = "N";
                    DdlPunto.Visible = true;
                    DdlCentro.Visible = false;
                    DdlCentro.SelectedValue = "0";
                    trFlujo.Visible = false;
                    TxtPresion.Text = "0";
                    trPeriodo.Visible = true;
                    trMayor1.Visible = false;
                    trMayor2.Visible = false;
                    trMayor3.Visible = false;
                    trMayor4.Visible = false;
                    TxtVlrGrtia.Text = "0";
                    TxtCapacOtm.Visible = false;
                }
                if (DdlMercado.SelectedValue == "O")
                {
                    LblFuente.Visible = false;
                    DdlFuente.Visible = false;
                    DdlFuente.SelectedValue = "0";
                    trSumSec.Visible = false;
                    DdlConectado.SelectedValue = "S";
                    DdlBoca.SelectedValue = "N";
                    DdlPunto.Visible = true;
                    DdlCentro.Visible = false;
                    DdlCentro.SelectedValue = "0";
                    trFlujo.Visible = false;
                    TxtPresion.Text = "0";
                    trPeriodo.Visible = false;
                    trMayor1.Visible = true;
                    trMayor2.Visible = true;
                    TxtVlrGrtia.Text = "";
                    btnCrearUsu.Visible = false;
                    if (DdlDestino.SelectedValue == "A")
                        TxtCapacOtm.Visible = true;
                    else
                        TxtCapacOtm.Visible = false;
                    if (DdlDestino.SelectedValue != "G")
                    {
                        tdMayT01.Visible = true;
                        //tdMayT02.Visible = true;
                    }
                    else
                    {
                        tdMayT01.Visible = false;
                        //tdMayT02.Visible = false;
                    }
                }
                if (DdlDestino.SelectedValue == "G")
                    LblCantidad.Text = "Cantidad de energía contratada (MBTUD)";
                else
                {
                    if (DdlDestino.SelectedValue == "T")
                        LblCantidad.Text = "Capacidad de transporte contratada (KPCD)";
                    else
                        LblCantidad.Text = "Cantidad y Capacidad contratada";
                }

                //habilita los campos permitidos
                string[] lsNombreParametros = { "@P_codigo_campo", "@P_codigo_causa" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { "0", HdfCausa };

                try
                {
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetCampoMod", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        DdlComprador.Enabled = true;
                        TxNitComprador.Enabled = true;
                        DdlVendedor.Enabled = true;
                        TxtFechaNeg.Enabled = true;
                        TxtHoraNeg.Enabled = true;
                        TxtFechaSus.Enabled = true;
                        TxtContDef.Enabled = true;
                        DdlFuente.Enabled = true;
                        DdlVariable.Enabled = true;
                        DdlConectado.Enabled = true;
                        DdlConectado_SelectedIndexChanged(null, null);
                        DdlBoca.Enabled = true;
                        DdlBoca_SelectedIndexChanged(null, null);
                        DdlPunto.Enabled = true;
                        DdlCentro.Enabled = true;
                        DdlModalidad.Enabled = true;
                        DdlPeriodo.Enabled = true;
                        TxtNoAños.Enabled = true;
                        TxtFechaIni.Enabled = true;
                        TxtHoraIni.Enabled = true;
                        TxtFechaFin.Enabled = true;
                        TxtHoraFin.Enabled = true;
                        TxtCantidad.Enabled = true;
                        TxtCapacOtm.Enabled = true;
                        TxtPrecio.Enabled = true;
                        DdlSentido.Enabled = true;
                        TxtPresion.Enabled = true;
                        TxtGarantia.Enabled = true;
                        TxtVlrGrtia.Enabled = true;
                        TxtFechaGrtia.Enabled = true;
                        ddlSectorConsumo.Enabled = true;
                        ddlConexion.Enabled = true;
                        ddlDeptoPuntoSal.Enabled = true;
                        DdlMunPuntoSal.Enabled = true;
                        ddlCentroPuntoSal.Enabled = true;
                        ddlRutaMay.Enabled = true;

                        if (lLector["operador_compra"].ToString() == "N")
                        {
                            DdlComprador.Enabled = false;
                            TxNitComprador.Enabled = false;
                        }
                        if (lLector["operador_venta"].ToString() == "N")
                            DdlVendedor.Enabled = false;
                        if (lLector["fecha_negociacion"].ToString() == "N")
                            TxtFechaNeg.Enabled = false;
                        if (lLector["hora_negociacion"].ToString() == "N")
                            TxtHoraNeg.Enabled = false;
                        if (lLector["fecha_suscripcion"].ToString() == "N")
                            TxtFechaSus.Enabled = false;
                        if (lLector["contrato_definitivo"].ToString() == "N")
                            TxtContDef.Enabled = false;
                        if (lLector["codigo_fuente"].ToString() == "N")
                            DdlFuente.Enabled = false;
                        if (lLector["conectado_snt"].ToString() == "N")
                            DdlConectado.Enabled = false;
                        if (lLector["ent_boca_pozo"].ToString() == "N")
                            DdlBoca.Enabled = false;
                        if (lLector["punto_ruta"].ToString() == "N")
                            DdlPunto.Enabled = false;
                        if (lLector["codigo_centro"].ToString() == "N")
                            DdlCentro.Enabled = false;
                        if (lLector["codigo_modalidad"].ToString() == "N")
                            DdlModalidad.Enabled = false;
                        if (lLector["periodo_entrega"].ToString() == "N")
                            DdlPeriodo.Enabled = false;
                        if (lLector["numero_años"].ToString() == "N")
                            TxtNoAños.Enabled = false;
                        if (lLector["fecha_inicial"].ToString() == "N")
                            TxtFechaIni.Enabled = false;
                        if (lLector["hora_inicial"].ToString() == "N")
                            TxtHoraIni.Enabled = false;
                        //20200820 ajsute
                        if (lLector["fecha_final"].ToString() == "N")
                        {
                            TxtFechaFin.Enabled = false;
                            HdfModificaFin = "N";
                        }
                        else
                            HdfModificaFin = "S";
                        if (lLector["hora_final"].ToString() == "N")
                            TxtHoraFin.Enabled = false;
                        if (lLector["cantidad"].ToString() == "N")
                        {
                            TxtCantidad.Enabled = false;
                            TxtCapacOtm.Enabled = false;
                        }
                        if (lLector["precio"].ToString() == "N")
                            TxtPrecio.Enabled = false;
                        if (lLector["sentido_flujo"].ToString() == "N")
                            DdlSentido.Enabled = false;
                        if (lLector["presion_punto_final"].ToString() == "N")
                            TxtPresion.Enabled = false;
                        if (lLector["tipo_garantia"].ToString() == "N")
                            TxtGarantia.Enabled = false;
                        if (lLector["valor_garantia"].ToString() == "N")
                            TxtVlrGrtia.Enabled = false;
                        if (lLector["fecha_pago_garantia"].ToString() == "N")
                            TxtFechaGrtia.Enabled = false;
                        if (lLector["codigo_sector_consumo"].ToString() == "N")
                            ddlSectorConsumo.Enabled = false;
                        if (lLector["usuario_conectado"].ToString() == "N")
                            ddlConexion.Enabled = false;
                        if (lLector["codigo_departamento"].ToString() == "N")
                            ddlDeptoPuntoSal.Enabled = false;
                        if (lLector["codigo_ciudad"].ToString() == "N")
                            DdlMunPuntoSal.Enabled = false;
                        if (lLector["codigo_centro"].ToString() == "N")
                            ddlCentroPuntoSal.Enabled = false;
                        if (lLector["codigo_ruta_may"].ToString() == "N")
                            ddlRutaMay.Enabled = false;
                        if (lLector["ind_contrato_var"].ToString() == "N")
                            DdlVariable.Enabled = false;
                        if (lLector["cod_mercado_relev"].ToString() == "N")
                            ddlMercadoRelevante.Enabled = false;
                        //20210707 trmo moneda
                        if (lLector["codigo_trm"].ToString() == "N")
                            ddlTrm.Enabled = false;
                        if (lLector["observacion_trm"].ToString() == "N" )
                            txtObsTrm.Enabled = false;
                        if (lLector["tipo_moneda"].ToString() == "N")
                            ddlMoneda.Enabled = false;
                        //20210707 trmo moneda
                    }
                    else
                    {
                        DdlComprador.Enabled = false;
                        TxNitComprador.Enabled = false;
                        DdlVendedor.Enabled = false;
                        TxtFechaNeg.Enabled = false;
                        TxtHoraNeg.Enabled = false;
                        TxtFechaSus.Enabled = false;
                        TxtContDef.Enabled = false;
                        DdlFuente.Enabled = false;
                        DdlVariable.Enabled = false;
                        DdlConectado.Enabled = false;
                        DdlBoca.Enabled = false;
                        DdlPunto.Enabled = false;
                        DdlCentro.Enabled = false;
                        DdlModalidad.Enabled = false;
                        DdlPeriodo.Enabled = false;
                        TxtNoAños.Enabled = false;
                        TxtFechaIni.Enabled = false;
                        TxtHoraIni.Enabled = false;
                        TxtFechaFin.Enabled = false;
                        TxtHoraFin.Enabled = false;
                        TxtCantidad.Enabled = false;
                        TxtCapacOtm.Enabled = false;
                        TxtPrecio.Enabled = false;
                        DdlSentido.Enabled = false;
                        TxtPresion.Enabled = false;
                        TxtGarantia.Enabled = false;
                        TxtVlrGrtia.Enabled = false;
                        TxtFechaGrtia.Enabled = false;
                        ddlSectorConsumo.Enabled = false;
                        ddlConexion.Enabled = false;
                        ddlDeptoPuntoSal.Enabled = false;
                        DdlMunPuntoSal.Enabled = false;
                        ddlCentroPuntoSal.Enabled = false;
                        ddlRutaMay.Enabled = false;
                        ddlTrm.Enabled = false; //20210707 trm moneda
                        txtObsTrm.Enabled = false; //20210707 trm moneda
                        ddlMoneda.Enabled = false; //20210707 trm moneda
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Problemas al traer la información.! " + ex.Message, EnumTypeToastr.Error);
                }

                //trae el detalle de contrados
                CargarDatosOper();
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Problemas en la Recuperación de la Información. " + ex.Message, EnumTypeToastr.Error);
            }

            dtgUsuarios.Columns[18].Visible = !(HdfTipoCausa == "T" || HdfTipoCausa == "A");
            if (HdfTipoCausa == "T" || HdfTipoCausa == "A") btnCrearUsu.Visible = false;
        }

        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void aprueba_datos()
        {

            DdlComprador.SelectedValue = "0";
            TxNitComprador.Text = "";
            DdlVendedor.SelectedValue = "0";
            TxtFechaNeg.Text = "";
            TxtHoraNeg.Text = "";
            TxtFechaSus.Text = "";
            TxtContDef.Text = "";
            DdlFuente.SelectedValue = "0";
            DdlVariable.SelectedValue = "N";
            DdlConectado.SelectedValue = "S";
            DdlBoca.SelectedValue = "N";
            DdlPunto.SelectedValue = "0";
            DdlCentro.SelectedValue = "0";
            DdlModalidad.SelectedValue = "0";
            DdlPeriodo.SelectedValue = "0";
            TxtNoAños.Text = "";
            TxtFechaIni.Text = "";
            TxtHoraIni.Text = "";
            TxtFechaFin.Text = "";
            TxtHoraFin.Text = "";
            TxtCantidad.Text = "";
            TxtPrecio.Text = "";
            DdlSentido.SelectedValue = "NORMAL";
            TxtPresion.Text = "";
            TxtGarantia.Text = "";
            TxtVlrGrtia.Text = "0";
            TxtFechaGrtia.Text = "";
            ddlDeptoPuntoSal.SelectedValue = "0";
            DdlMunPuntoSal_SelectedIndexChanged(null, null);
            ddlSectorConsumo.SelectedValue = "0";
            ddlConexion.SelectedValue = "S";
            ddlMercadoRelevante.SelectedValue = "0";
            TxtCapacOtm.Text = "";
            ddlRutaMay.SelectedValue = "0";
            ddlTrm.SelectedValue = "0"; //20210707 trm moneda
            txtObsTrm.Text = ""; //20210707 trm moneda
            ddlMoneda.SelectedValue = "USD"; //20210707 trm moneda

            DdlComprador.Enabled = true;
            TxNitComprador.Enabled = true;
            DdlVendedor.Enabled = true;
            TxtFechaNeg.Enabled = true;
            TxtHoraNeg.Enabled = true;
            TxtFechaSus.Enabled = true;
            TxtContDef.Enabled = true;
            DdlFuente.Enabled = true;
            DdlVariable.Enabled = true;
            DdlConectado.Enabled = true;
            DdlBoca.Enabled = true;
            DdlPunto.Enabled = true;
            DdlCentro.Enabled = true;
            DdlModalidad.Enabled = true;
            DdlPeriodo.Enabled = true;
            TxtNoAños.Enabled = true;
            TxtFechaIni.Enabled = true;
            TxtHoraIni.Enabled = true;
            TxtFechaFin.Enabled = true;
            TxtHoraFin.Enabled = true;
            TxtCantidad.Enabled = true;
            TxtPrecio.Enabled = true;
            DdlSentido.Enabled = true;
            TxtPresion.Enabled = true;
            TxtGarantia.Enabled = true;
            TxtVlrGrtia.Enabled = true;
            TxtFechaGrtia.Enabled = true;
            ddlSectorConsumo.Enabled = true;
            ddlConexion.Enabled = true;
            ddlDeptoPuntoSal.Enabled = true;
            DdlMunPuntoSal.Enabled = true;
            ddlCentroPuntoSal.Enabled = true;
            TxtCapacOtm.Enabled = true;
            ddlRutaMay.Enabled = false;
            ddlTrm.Enabled = true; //20210707 trm moneda
            txtObsTrm.Enabled = true; //20210707 trm moneda
            ddlMoneda.Enabled = true; //20210707 trm moneda

            try
            {
                //Abre el modal de Modificación
                ModalEvent?.Invoke(modContratosMod.ID, modContratosModInside.ID, EnumTypeModal.Abrir);
                EnumBotones[] botones = { EnumBotones.Buscar };
                ButtonsEvent?.Invoke(botones, null, NameController);

                if (HdfPunta == "C" && DdlMercado.SelectedValue != "O") //20210824 ajuste
                {
                    // Se muestra la sección de demanda
                    tblDemanda.Visible = true;
                    btnCrearUsu.Visible = true;
                }
                else
                {
                    tblDemanda.Visible = false;//20210824 ajuste
                    btnCrearUsu.Visible = false;
                }

                if (DdlMercado.SelectedValue != "O")
                {
                    if (DdlDestino.SelectedValue == "G")
                    {
                        DdlPunto.Items.Clear();
                        lConexion.Abrir();
                        LlenarControles(lConexion.gObjConexion, DdlPunto, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
                        lConexion.Cerrar();
                    }
                    else
                    {
                        DdlPunto.Items.Clear();
                        lConexion.Abrir();
                        LlenarControles(lConexion.gObjConexion, DdlPunto, "m_ruta_snt", " estado = 'A' order by descripcion", 0, 4);
                        lConexion.Cerrar();
                    }
                }
                else
                {
                    LblPunto.Text = "Punto de Salida SNT";
                    DdlPunto.Items.Clear();
                    lConexion.Abrir();
                    LlenarControles(lConexion.gObjConexion, DdlPunto, "m_punto_salida_snt", " estado = 'A'  order by descripcion", 0, 2);
                    lConexion.Cerrar();
                }

                if (DdlDestino.SelectedValue == "G" && DdlMercado.SelectedValue == "P")
                {
                    LblFuente.Visible = true;
                    DdlFuente.Visible = true;
                    trSumSec.Visible = false;
                    DdlConectado.SelectedValue = "S";
                    DdlBoca.SelectedValue = "N";
                    DdlPunto.Visible = true;
                    DdlCentro.Visible = false;
                    DdlCentro.SelectedValue = "0";
                    trFlujo.Visible = false;
                    TxtPresion.Text = "0";
                    trPeriodo.Visible = true;
                    trMayor1.Visible = false;
                    trMayor2.Visible = false;
                    trMayor3.Visible = false;
                    trMayor4.Visible = false;
                    TxtVlrGrtia.Text = "0";
                    TxtCapacOtm.Visible = false;
                }
                if (DdlDestino.SelectedValue == "G" && DdlMercado.SelectedValue == "S")
                {
                    LblFuente.Visible = false;
                    DdlFuente.Visible = false;
                    DdlFuente.SelectedValue = "0";
                    trSumSec.Visible = true;
                    DdlConectado.SelectedValue = "S";
                    DdlBoca.SelectedValue = "N";
                    DdlPunto.Visible = true;
                    DdlCentro.Visible = false;
                    DdlCentro.SelectedValue = "0";
                    trFlujo.Visible = false;
                    TxtPresion.Text = "0";
                    trPeriodo.Visible = true;
                    trMayor1.Visible = false;
                    trMayor2.Visible = false;
                    trMayor3.Visible = false;
                    trMayor4.Visible = false;
                    TxtVlrGrtia.Text = "0";
                    TxtCapacOtm.Visible = false;
                }
                if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "P")
                {
                    LblFuente.Visible = false;
                    DdlFuente.Visible = false;
                    DdlFuente.SelectedValue = "0";
                    trSumSec.Visible = false;
                    DdlConectado.SelectedValue = "S";
                    DdlBoca.SelectedValue = "N";
                    DdlPunto.Visible = true;
                    DdlCentro.Visible = false;
                    DdlCentro.SelectedValue = "0";
                    trFlujo.Visible = true;
                    TxtPresion.Text = "";
                    trPeriodo.Visible = true;
                    trMayor1.Visible = false;
                    trMayor2.Visible = false;
                    trMayor3.Visible = false;
                    trMayor4.Visible = false;
                    TxtVlrGrtia.Text = "0";
                    TxtCapacOtm.Visible = false;
                }
                if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "S")
                {
                    LblFuente.Visible = false;
                    DdlFuente.Visible = false;
                    DdlFuente.SelectedValue = "0";
                    trSumSec.Visible = false;
                    DdlConectado.SelectedValue = "S";
                    DdlBoca.SelectedValue = "N";
                    DdlPunto.Visible = true;
                    DdlCentro.Visible = false;
                    DdlCentro.SelectedValue = "0";
                    trFlujo.Visible = false;
                    TxtPresion.Text = "0";
                    trPeriodo.Visible = true;
                    trMayor1.Visible = false;
                    trMayor2.Visible = false;
                    trMayor3.Visible = false;
                    trMayor4.Visible = false;
                    TxtVlrGrtia.Text = "0";
                    TxtCapacOtm.Visible = false;
                }
                if (DdlMercado.SelectedValue == "O")
                {
                    LblFuente.Visible = false;
                    DdlFuente.Visible = false;
                    DdlFuente.SelectedValue = "0";
                    trSumSec.Visible = false;
                    DdlConectado.SelectedValue = "S";
                    DdlBoca.SelectedValue = "N";
                    DdlPunto.Visible = true;
                    DdlCentro.Visible = false;
                    DdlCentro.SelectedValue = "0";
                    trFlujo.Visible = false;
                    TxtPresion.Text = "0";
                    trPeriodo.Visible = false;
                    trMayor1.Visible = true;
                    trMayor2.Visible = true;
                    TxtVlrGrtia.Text = "";
                    btnCrearUsu.Visible = false;
                    if (DdlDestino.SelectedValue == "A")
                        TxtCapacOtm.Visible = true;
                    else
                        TxtCapacOtm.Visible = false;
                    if (DdlDestino.SelectedValue != "G")
                    {
                        tdMayT01.Visible = true;
                        //tdMayT02.Visible = true;
                    }
                    else
                    {
                        tdMayT01.Visible = false;
                        //tdMayT02.Visible = false;
                    }
                }
                if (DdlDestino.SelectedValue == "G")
                    LblCantidad.Text = "Cantidad de energía contratada (MBTUD)";
                else
                {
                    if (DdlDestino.SelectedValue == "T")
                        LblCantidad.Text = "Capacidad de transporte contratada (KPCD)";
                    else
                        LblCantidad.Text = "Cantidad y Capacidad contratada";
                }

                //habilita los campos permitidos
                string[] lsNombreParametros = { "@P_codigo_campo", "@P_codigo_causa" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { "0", HdfCausa };

                try
                {
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetCampoMod", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        DdlComprador.Enabled = false;
                        TxNitComprador.Enabled = false;
                        DdlVendedor.Enabled = false;
                        TxtFechaNeg.Enabled = false;
                        TxtHoraNeg.Enabled = false;
                        TxtFechaSus.Enabled = false;
                        TxtContDef.Enabled = false;
                        DdlFuente.Enabled = false;
                        DdlVariable.Enabled = false;
                        DdlConectado.Enabled = false;
                        DdlConectado_SelectedIndexChanged(null, null);
                        DdlBoca.Enabled = false;
                        DdlBoca_SelectedIndexChanged(null, null);
                        DdlPunto.Enabled = false;
                        DdlCentro.Enabled = false;
                        DdlModalidad.Enabled = false;
                        DdlPeriodo.Enabled = false;
                        TxtNoAños.Enabled = false;
                        TxtFechaIni.Enabled = false;
                        TxtHoraIni.Enabled = false;
                        TxtFechaFin.Enabled = false;
                        TxtHoraFin.Enabled = false;
                        TxtCantidad.Enabled = false;
                        TxtCapacOtm.Enabled = false;
                        TxtPrecio.Enabled = false;
                        DdlSentido.Enabled = false;
                        TxtPresion.Enabled = false;
                        TxtGarantia.Enabled = false;
                        TxtVlrGrtia.Enabled = false;
                        TxtFechaGrtia.Enabled = false;
                        ddlSectorConsumo.Enabled = false;
                        ddlConexion.Enabled = false;
                        ddlDeptoPuntoSal.Enabled = false;
                        DdlMunPuntoSal.Enabled = false;
                        ddlCentroPuntoSal.Enabled = false;
                        ddlRutaMay.Enabled = false;
                        ddlTrm.Enabled = false; //20210707 trm moneda
                        txtObsTrm.Enabled = false; //20210707 trm moneda
                        ddlMoneda.Enabled = false; //20210707 trm moneda
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Problemas al traer la información.! " + ex.Message, EnumTypeToastr.Error);
                }

                //trae el detalle de contrados
                CargarDatosOper();
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Problemas en la Recuperación de la Información. " + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void consulta_datos()
        {

            DdlComprador.SelectedValue = "0";
            TxNitComprador.Text = "";
            DdlVendedor.SelectedValue = "0";
            TxtFechaNeg.Text = "";
            TxtHoraNeg.Text = "";
            TxtFechaSus.Text = "";
            TxtContDef.Text = "";
            DdlFuente.SelectedValue = "0";
            DdlVariable.SelectedValue = "N";
            DdlConectado.SelectedValue = "S";
            DdlBoca.SelectedValue = "N";
            DdlPunto.SelectedValue = "0";
            DdlCentro.SelectedValue = "0";
            DdlModalidad.SelectedValue = "0";
            DdlPeriodo.SelectedValue = "0";
            TxtNoAños.Text = "";
            TxtFechaIni.Text = "";
            TxtHoraIni.Text = "";
            TxtFechaFin.Text = "";
            TxtHoraFin.Text = "";
            TxtCantidad.Text = "";
            TxtPrecio.Text = "";
            DdlSentido.SelectedValue = "NORMAL";
            TxtPresion.Text = "";
            TxtGarantia.Text = "";
            TxtVlrGrtia.Text = "0";
            TxtFechaGrtia.Text = "";
            ddlDeptoPuntoSal.SelectedValue = "0";
            DdlMunPuntoSal_SelectedIndexChanged(null, null);
            ddlSectorConsumo.SelectedValue = "0";
            ddlConexion.SelectedValue = "S";
            ddlMercadoRelevante.SelectedValue = "0";
            TxtCapacOtm.Text = "";
            ddlRutaMay.SelectedValue = "0";
            ddlTrm.SelectedValue = "0"; //20210707 trm moneda
            txtObsTrm.Text= ""; //20210707 trm moneda
            ddlMoneda.SelectedValue = "USD"; //20210707 trm moneda

            DdlComprador.Enabled = true;
            TxNitComprador.Enabled = true;
            DdlVendedor.Enabled = true;
            TxtFechaNeg.Enabled = true;
            TxtHoraNeg.Enabled = true;
            TxtFechaSus.Enabled = true;
            TxtContDef.Enabled = true;
            DdlFuente.Enabled = true;
            DdlVariable.Enabled = true;
            DdlConectado.Enabled = true;
            DdlBoca.Enabled = true;
            DdlPunto.Enabled = true;
            DdlCentro.Enabled = true;
            DdlModalidad.Enabled = true;
            DdlPeriodo.Enabled = true;
            TxtNoAños.Enabled = true;
            TxtFechaIni.Enabled = true;
            TxtHoraIni.Enabled = true;
            TxtFechaFin.Enabled = true;
            TxtHoraFin.Enabled = true;
            TxtCantidad.Enabled = true;
            TxtPrecio.Enabled = true;
            DdlSentido.Enabled = true;
            TxtPresion.Enabled = true;
            TxtGarantia.Enabled = true;
            TxtVlrGrtia.Enabled = true;
            TxtFechaGrtia.Enabled = true;
            ddlSectorConsumo.Enabled = true;
            ddlConexion.Enabled = true;
            ddlDeptoPuntoSal.Enabled = true;
            DdlMunPuntoSal.Enabled = true;
            ddlCentroPuntoSal.Enabled = true;
            TxtCapacOtm.Enabled = true;
            ddlRutaMay.Enabled = false;
            ddlTrm.Enabled = true; //20210707 trm moneda
            txtObsTrm.Enabled = true; //20210707 trm moneda
            ddlMoneda.Enabled = true; //20210707 trm moneda

            try
            {
                //Abre el modal de Modificación
                ModalEvent?.Invoke(modContratosMod.ID, modContratosModInside.ID, EnumTypeModal.Abrir);
                EnumBotones[] botones = { EnumBotones.Buscar };
                ButtonsEvent?.Invoke(botones, null, NameController);

                if (HdfPunta == "C" && DdlMercado.SelectedValue != "O") //20210824 ajuste
                {
                    // Se muestra la sección de demanda
                    tblDemanda.Visible = true;
                    btnCrearUsu.Visible = true;
                }
                else
                {
                    tblDemanda.Visible = false;//20210824 ajuste
                    btnCrearUsu.Visible = false;
                }

                if (DdlMercado.SelectedValue != "O")
                {
                    if (DdlDestino.SelectedValue == "G")
                    {
                        DdlPunto.Items.Clear();
                        lConexion.Abrir();
                        LlenarControles(lConexion.gObjConexion, DdlPunto, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
                        lConexion.Cerrar();
                    }
                    else
                    {
                        DdlPunto.Items.Clear();
                        lConexion.Abrir();
                        LlenarControles(lConexion.gObjConexion, DdlPunto, "m_ruta_snt", " estado = 'A' order by descripcion", 0, 4);
                        lConexion.Cerrar();
                    }
                }
                else
                {
                    LblPunto.Text = "Punto de Salida SNT";
                    DdlPunto.Items.Clear();
                    lConexion.Abrir();
                    LlenarControles(lConexion.gObjConexion, DdlPunto, "m_punto_salida_snt", " estado = 'A'  order by descripcion", 0, 2);
                    lConexion.Cerrar();
                }

                if (DdlDestino.SelectedValue == "G" && DdlMercado.SelectedValue == "P")
                {
                    LblFuente.Visible = true;
                    DdlFuente.Visible = true;
                    trSumSec.Visible = false;
                    DdlConectado.SelectedValue = "S";
                    DdlBoca.SelectedValue = "N";
                    DdlPunto.Visible = true;
                    DdlCentro.Visible = false;
                    DdlCentro.SelectedValue = "0";
                    trFlujo.Visible = false;
                    TxtPresion.Text = "0";
                    trPeriodo.Visible = true;
                    trMayor1.Visible = false;
                    trMayor2.Visible = false;
                    trMayor3.Visible = false;
                    trMayor4.Visible = false;
                    TxtVlrGrtia.Text = "0";
                    TxtCapacOtm.Visible = false;
                }
                if (DdlDestino.SelectedValue == "G" && DdlMercado.SelectedValue == "S")
                {
                    LblFuente.Visible = false;
                    DdlFuente.Visible = false;
                    DdlFuente.SelectedValue = "0";
                    trSumSec.Visible = true;
                    DdlConectado.SelectedValue = "S";
                    DdlBoca.SelectedValue = "N";
                    DdlPunto.Visible = true;
                    DdlCentro.Visible = false;
                    DdlCentro.SelectedValue = "0";
                    trFlujo.Visible = false;
                    TxtPresion.Text = "0";
                    trPeriodo.Visible = true;
                    trMayor1.Visible = false;
                    trMayor2.Visible = false;
                    trMayor3.Visible = false;
                    trMayor4.Visible = false;
                    TxtVlrGrtia.Text = "0";
                    TxtCapacOtm.Visible = false;
                }
                if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "P")
                {
                    LblFuente.Visible = false;
                    DdlFuente.Visible = false;
                    DdlFuente.SelectedValue = "0";
                    trSumSec.Visible = false;
                    DdlConectado.SelectedValue = "S";
                    DdlBoca.SelectedValue = "N";
                    DdlPunto.Visible = true;
                    DdlCentro.Visible = false;
                    DdlCentro.SelectedValue = "0";
                    trFlujo.Visible = true;
                    TxtPresion.Text = "";
                    trPeriodo.Visible = true;
                    trMayor1.Visible = false;
                    trMayor2.Visible = false;
                    trMayor3.Visible = false;
                    trMayor4.Visible = false;
                    TxtVlrGrtia.Text = "0";
                    TxtCapacOtm.Visible = false;
                }
                if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "S")
                {
                    LblFuente.Visible = false;
                    DdlFuente.Visible = false;
                    DdlFuente.SelectedValue = "0";
                    trSumSec.Visible = false;
                    DdlConectado.SelectedValue = "S";
                    DdlBoca.SelectedValue = "N";
                    DdlPunto.Visible = true;
                    DdlCentro.Visible = false;
                    DdlCentro.SelectedValue = "0";
                    trFlujo.Visible = false;
                    TxtPresion.Text = "0";
                    trPeriodo.Visible = true;
                    trMayor1.Visible = false;
                    trMayor2.Visible = false;
                    trMayor3.Visible = false;
                    trMayor4.Visible = false;
                    TxtVlrGrtia.Text = "0";
                    TxtCapacOtm.Visible = false;
                }
                if (DdlMercado.SelectedValue == "O")
                {
                    LblFuente.Visible = false;
                    DdlFuente.Visible = false;
                    DdlFuente.SelectedValue = "0";
                    trSumSec.Visible = false;
                    DdlConectado.SelectedValue = "S";
                    DdlBoca.SelectedValue = "N";
                    DdlPunto.Visible = true;
                    DdlCentro.Visible = false;
                    DdlCentro.SelectedValue = "0";
                    trFlujo.Visible = false;
                    TxtPresion.Text = "0";
                    trPeriodo.Visible = false;
                    trMayor1.Visible = true;
                    trMayor2.Visible = true;
                    TxtVlrGrtia.Text = "";
                    btnCrearUsu.Visible = false;
                    if (DdlDestino.SelectedValue == "A")
                        TxtCapacOtm.Visible = true;
                    else
                        TxtCapacOtm.Visible = false;
                    if (DdlDestino.SelectedValue != "G")
                    {
                        tdMayT01.Visible = true;
                        //tdMayT02.Visible = true;
                    }
                    else
                    {
                        tdMayT01.Visible = false;
                        //tdMayT02.Visible = false;
                    }
                }
                if (DdlDestino.SelectedValue == "G")
                    LblCantidad.Text = "Cantidad de energía contratada (MBTUD)";
                else
                {
                    if (DdlDestino.SelectedValue == "T")
                        LblCantidad.Text = "Capacidad de transporte contratada (KPCD)";
                    else
                        LblCantidad.Text = "Cantidad y Capacidad contratada";
                }

                DdlComprador.Enabled = false;
                TxNitComprador.Enabled = false;
                DdlVendedor.Enabled = false;
                TxtFechaNeg.Enabled = false;
                TxtHoraNeg.Enabled = false;
                TxtFechaSus.Enabled = false;
                TxtContDef.Enabled = false;
                DdlFuente.Enabled = false;
                DdlVariable.Enabled = false;
                DdlConectado.Enabled = false;
                DdlConectado_SelectedIndexChanged(null, null);
                DdlBoca.Enabled = false;
                DdlBoca_SelectedIndexChanged(null, null);
                DdlPunto.Enabled = false;
                DdlCentro.Enabled = false;
                DdlModalidad.Enabled = false;
                DdlPeriodo.Enabled = false;
                DdlPeriodo_SelectedIndexChanged(null, null);
                TxtNoAños.Enabled = false;
                TxtFechaIni.Enabled = false;
                TxtHoraIni.Enabled = false;
                TxtFechaFin.Enabled = false;
                TxtHoraFin.Enabled = false;
                TxtCantidad.Enabled = false;
                TxtCapacOtm.Enabled = false;
                TxtPrecio.Enabled = false;
                DdlSentido.Enabled = false;
                TxtPresion.Enabled = false;
                TxtGarantia.Enabled = false;
                TxtVlrGrtia.Enabled = false;
                TxtFechaGrtia.Enabled = false;
                ddlSectorConsumo.Enabled = false;
                ddlConexion.Enabled = false;
                ddlDeptoPuntoSal.Enabled = false;
                DdlMunPuntoSal.Enabled = false;
                ddlCentroPuntoSal.Enabled = false;
                ddlRutaMay.Enabled = false;
                ddlMercadoRelevante.Enabled = false; //20190425 rq022-19 ajsute modoificacion
                ddlTrm.Enabled = false;  //20210707 trm moneda
                txtObsTrm.Enabled = false;  //20210707 trm moneda
                ddlMoneda.Enabled = false;  //20210707 trm moneda

                CargarDatosOper();
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Problemas en la Recuperación de la Información. " + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void modificar()
        {
            btnModificar.Visible = true;
            btnAprobar.Visible = false;
            btnRechazar.Visible = false;
            btnAprobarBmc.Visible = false;
            btnRechazarBmc.Visible = false;
            trObs.Visible = false;
            crear();
            //if (tblDemanda.Visible)
            //    CargarDatosUsu();
        }

        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void aprobar()
        {
            btnModificar.Visible = false;
            btnAprobar.Visible = true;
            btnRechazar.Visible = true;
            btnAprobarBmc.Visible = false;
            btnRechazarBmc.Visible = false;
            trObs.Visible = true;
            aprueba_datos();
            //if (tblDemanda.Visible)
            //    CargarDatosUsu();
        }

        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void consultar(string lsAccion)
        {
            if (lsAccion == "A")
            {
                btnModificar.Visible = false;
                btnAprobar.Visible = false;
                btnRechazar.Visible = false;
                btnAprobarBmc.Visible = true;
                btnRechazarBmc.Visible = true;
                trObs.Visible = true;
            }
            else
            {
                btnModificar.Visible = false;
                btnAprobar.Visible = false;
                btnRechazar.Visible = false;
                btnAprobarBmc.Visible = false;
                btnRechazarBmc.Visible = false;
                trObs.Visible = false;
            }
            consulta_datos();
            //if (tblDemanda.Visible)
            //    CargarDatosUsu();
        }

        /// <summary>
        /// Nombre: dtgOperacion_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgOperacion_EditCommand(object source, DataGridCommandEventArgs e)
        {
            var lblMensaje = new StringBuilder();
            if (((LinkButton)e.CommandSource).Text == "Modificar")
            {
                if (lblMensaje.ToString() == "")
                {
                }
            }
            if (((LinkButton)e.CommandSource).Text == "Copiar")
            {
                if (lblMensaje.ToString() == "")
                {
                    try
                    {
                        HdfCodModif = e.Item.Cells[4].Text;
                        btnModificar.Text = "Crear";

                        lConexion.Abrir();
                        string[] lsNombreParametros = { "@P_codigo_verif", "@P_codigo_modif" };
                        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                        string[] lValorParametros = { HdfIdRegistro, HdfCodModif };
                        try
                        {
                            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetDatosContMod", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                            if (goInfo.mensaje_error != "")
                            {
                                ToastrEvent?.Invoke("Se presentó un Problema en la selección de la información.! " + goInfo.mensaje_error, EnumTypeToastr.Error);
                                lConexion.Cerrar();
                            }
                            else
                            {
                                if (lLector.HasRows)
                                {
                                    lLector.Read();

                                    DdlDestino.SelectedValue = lLector["destino_rueda"].ToString();
                                    DdlMercado.SelectedValue = lLector["tipo_mercado"].ToString();
                                    try
                                    {
                                        DdlVendedor.SelectedValue = lLector["operador_venta"].ToString();
                                    }
                                    catch (Exception)
                                    { }
                                    try
                                    {
                                        DdlComprador.SelectedValue = lLector["operador_compra"].ToString();
                                        TxNitComprador.Text = lLector["nit_usuario_cont"].ToString();
                                        if (DdlComprador.SelectedValue != "0")
                                        {
                                            DdlComprador.Visible = true;
                                            TxNitComprador.Visible = false;
                                            TxNitComprador.Text = "";
                                        }
                                        else
                                        {
                                            DdlComprador.Visible = false;
                                            TxNitComprador.Visible = true;
                                        }
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    TxtFechaNeg.Text = lLector["fecha_neg"].ToString();
                                    TxtHoraNeg.Text = lLector["hora_neg"].ToString();
                                    TxtFechaSus.Text = lLector["fecha_suscripcion"].ToString();
                                    TxtContDef.Text = lLector["contrato_definitivo"].ToString();
                                    DdlVariable.SelectedValue = lLector["ind_contrato_var"].ToString();
                                    try
                                    {
                                        DdlFuente.SelectedValue = lLector["codigo_fuente"].ToString();
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    DdlConectado.SelectedValue = lLector["conectado_snt"].ToString();
                                    DdlBoca.SelectedValue = lLector["ent_boca_pozo"].ToString();
                                    DdlBoca_SelectedIndexChanged(null, null); // 20191230 //2020727 ajsute front end
                                    try
                                    {
                                        DdlPunto.SelectedValue = lLector["codigo_punto_entrega"].ToString();
                                    }
                                    catch (Exception)
                                    { }
                                    try
                                    {
                                        DdlCentro.SelectedValue = lLector["codigo_centro_poblado"].ToString();
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    try
                                    {
                                        DdlModalidad.SelectedValue = lLector["codigo_modalidad"].ToString();
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    try
                                    {
                                        DdlPeriodo.SelectedValue = lLector["codigo_periodo"].ToString();
                                        DdlPeriodo_SelectedIndexChanged(null, null); //20200831
                                    }
                                    catch (Exception)
                                    { }
                                    TxtNoAños.Text = lLector["no_años"].ToString();
                                    TxtFechaIni.Text = lLector["fecha_ini"].ToString();
                                    TxtHoraIni.Text = lLector["hora_ini"].ToString();
                                    TxtFechaFin.Text = lLector["fecha_fin"].ToString();
                                    TxtHoraFin.Text = lLector["hora_fin"].ToString();
                                    TxtCantidad.Text = lLector["cantidad"].ToString();
                                    TxtCapacOtm.Text = lLector["capacidad"].ToString();
                                    TxtPrecio.Text = lLector["precio"].ToString();
                                    if (lLector["sentido_flujo"].ToString().Trim() != "")
                                        DdlSentido.SelectedValue = lLector["sentido_flujo"].ToString();
                                    TxtPresion.Text = lLector["presion_punto_fin"].ToString();
                                    TxtGarantia.Text = lLector["tipo_garantia"].ToString();
                                    TxtVlrGrtia.Text = lLector["valor_garantia"].ToString();
                                    TxtFechaGrtia.Text = lLector["fecha_pago"].ToString();
                                    try
                                    {
                                        ddlSectorConsumo.SelectedValue = lLector["codigo_sector_consumo"].ToString();
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    try
                                    {
                                        ddlConexion.SelectedValue = lLector["usuario_no_reg_snt"].ToString();
                                        ddlConexion_SelectedIndexChanged(null, null); //20200831 ajsute OTMM
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    try
                                    {
                                        ddlDeptoPuntoSal.SelectedValue = lLector["codigo_depto"].ToString();
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    try
                                    {
                                        DdlMunPuntoSal.SelectedValue = lLector["codigo_municipio"].ToString();
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    try
                                    {
                                        ddlCentroPuntoSal.SelectedValue = lLector["codigo_centro_poblado"].ToString();
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    try
                                    {
                                        ddlRutaMay.SelectedValue = lLector["codigo_ruta_may"].ToString();
                                    }
                                    catch (Exception)
                                    { }
                                    //20210707 moneda trm
                                    try
                                    {
                                        ddlTrm.SelectedValue = lLector["codigo_trm"].ToString();
                                    }
                                    catch (Exception)
                                    { }
                                    txtObsTrm.Text = lLector["observacion_trm"].ToString();
                                    try
                                    {
                                        ddlMoneda.SelectedValue = lLector["tipo_moneda"].ToString();
                                    }
                                    catch (Exception)
                                    { }
                                    //20210707 fin moneda trm
                                }
                            }
                            lLector.Close();
                            if (HdfPunta == "C" && DdlMercado.SelectedValue != "M")
                                CargarDatosUsu();
                        }
                        catch (Exception ex)
                        {
                            lLector.Close();
                            ToastrEvent?.Invoke("Error al recuperar información." + ex.Message, EnumTypeToastr.Error);
                            lConexion.Cerrar();
                        }
                    }
                    catch (Exception ex)
                    {
                        ToastrEvent?.Invoke("Problemas en la Recuperación de la Información. " + ex.Message, EnumTypeToastr.Error);
                    }
                }
            }
            if (((LinkButton)e.CommandSource).Text == "Eliminar")
            {
                if (lblMensaje.ToString() == "")
                {
                }
            }
        }

        /// <summary>
        /// Guarda temporalmente el archivo original o modificado en el servidor
        /// </summary>
        private void FileTemp_Save(AjaxControlToolkit.AsyncFileUploadEventArgs e, AjaxControlToolkit.AsyncFileUpload asyncFileUpload, string control)
        {
            // Se crea la ruta de los archivos temporales
            Directory.CreateDirectory(sRutaArc + "temp\\");
            string fileUploadPath;
            if (control.Equals("original"))
            {
                fileUploadPath = Path.Combine(sRutaArc, "temp\\", FuArchivoOrg.FileName);
                FuArchivoOrg.SaveAs(fileUploadPath);
                FileUploadOriginal_Temp = fileUploadPath;
            }
            else
            {
                fileUploadPath = Path.Combine(sRutaArc, "temp\\", FuArchivoMod.FileName);
                FuArchivoMod.SaveAs(fileUploadPath);
                FileUploadModificado_Temp = fileUploadPath;
            }
        }

        /// <summary>
        /// Evento que guarda temporalmente el archivo original o modificado en el servidor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FileUpload_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                // Se valida si se cargo el archivo original  
                if (!string.IsNullOrEmpty(FuArchivoOrg.FileName))
                    // Se carga el archivo original
                    FileTemp_Save(e, FuArchivoOrg, "original");
                // Se valida si se cargo el archivo modificado   
                if (!string.IsNullOrEmpty(FuArchivoMod.FileName))
                    // Se carga el archivo modificado
                    FileTemp_Save(e, FuArchivoMod, "modificado");
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnAceptar_Click(object sender, EventArgs e)
        {
            var lblMensajeMod = new StringBuilder();
            List<string> liExtension = new List<string>();
            liExtension.Add(".PDF");
            liExtension.Add(".PDFA");
            liExtension.Add(".GIF");
            liExtension.Add(".JPG");
            liExtension.Add(".PNG");
            liExtension.Add(".TIFF");
            var lsExtValidas = ".pdf, .pdfa, .gif, .jpg, .png, .tiff";
            string[] fileOriginal;
            string fileNameOriginal = string.Empty;
            string[] fileModificado;
            string fileNameModificado = string.Empty;

            string lsExtension;
            if (DdlIngCausa.SelectedValue == "0")
                lblMensajeMod.Append("Debe seleccionar la causa de modificación<br>");
            if (string.IsNullOrEmpty(FileUploadOriginal_Temp))
            {
                lblMensajeMod.Append("Debe seleccionar el archivo del contrato original<br>");
            }
            else
            {
                fileOriginal = FileUploadOriginal_Temp.Split('\\');
                fileNameOriginal = fileOriginal[fileOriginal.Length - 1];
                lsExtension = Path.GetExtension(fileNameOriginal).ToUpper();
                if (!liExtension.Contains(lsExtension))
                    lblMensajeMod.Append("La extensión del archivo original no es válida: " + lsExtValidas + "<br>");
                //20200430 ajuste nombre archivos
                //if (File.Exists(FileUploadOriginal_Temp.Replace("temp\\", "")))
                //    lblMensajeMod.Append("El archivo original ya está cargado en el sistema<br>");
            }
            if (string.IsNullOrEmpty(FileUploadModificado_Temp))
            {
                lblMensajeMod.Append("Debe seleccionar el archivo del contrato modificado<br>");
            }
            else
            {
                fileModificado = FileUploadModificado_Temp.Split('\\');
                fileNameModificado = fileModificado[fileModificado.Length - 1];
                lsExtension = Path.GetExtension(fileNameModificado).ToUpper();
                if (!liExtension.Contains(lsExtension))
                    lblMensajeMod.Append("La extensión del archivo modificado no es válida: " + lsExtValidas + "<br>");
                //20200430 ajuste nombre archivos
                //if (File.Exists(FileUploadModificado_Temp.Replace("temp\\", "")))
                //    lblMensajeMod.Append("El archivo modificado ya está cargado en el sistema<br>");
            }
            if (FileUploadOriginal_Temp == FileUploadModificado_Temp && FileUploadOriginal_Temp != "")
                lblMensajeMod.Append("Los nombre de los archivos de los contratos original y modificado deben ser diferentes<br>");

            //20200430 ajuste nombre archivos
            string lsNumero = "";
            string lsArchivoOrg = "";
            string lsArchivoMod = "";
            string lsArchivoOrgExt = "";
            string lsArchivoModExt = "";
            int liCOnta = 0;
            string lsContrato = HdfContratoDef.Replace('/', '_').Replace('\\', '_'); //20210216
            while (lsArchivoOrg == "" && liCOnta < 10)
            {
                lsNumero = DateTime.Now.Millisecond.ToString();
                lsArchivoOrg = DateTime.Now.ToString("yyyy_MM_dd") + "_" + HdfOperador + "_" + lsContrato + "_Org_" + lsNumero + Path.GetExtension(fileNameOriginal).ToUpper(); //20210216
                lsArchivoMod = DateTime.Now.ToString("yyyy_MM_dd") + "_" + HdfOperador + "_" + lsContrato + "_Mod_" + lsNumero + Path.GetExtension(fileNameModificado).ToUpper(); //20210216
                if (File.Exists(sRutaArc + lsArchivoOrg) || File.Exists(sRutaArc + lsArchivoMod))
                {
                    lsArchivoOrg = "";
                    lsArchivoMod = "";
                }
                liCOnta++;
            }
            if (lsArchivoMod == "")
                lblMensajeMod.Append("No se pueden cargar los archivos. Intente de nuevo<br>");
            else
            {
                lsArchivoOrgExt = sRutaArc + lsArchivoOrg;
                lsArchivoModExt = sRutaArc + lsArchivoMod;
            }
            //20200430 ajuste nombre archivos


            if (!string.IsNullOrEmpty(lblMensajeMod.ToString()))
            {
                ToastrEvent?.Invoke(lblMensajeMod.ToString(), EnumTypeToastr.Error);
                manejo_bloqueo("E", HdfIdRegistro);
                return;
            }

            try
            {
                HdfCausa = "0";
                string[] lsNombreParametros =
                {
                    "@P_codigo_solicitud", "@P_codigo_operador", "@P_codigo_verif", "@P_contrato_definitivo",
                    "@P_codigo_causa", "@P_contrato_original", "@P_contrato_nuevo"
                };
                SqlDbType[] lTipoparametros =
                {
                    SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar,
                    SqlDbType.VarChar
                };
                string[] lValorParametros =
                {
                    "0", HdfOperador, HdfIdRegistro, HdfContratoDef, DdlIngCausa.SelectedValue, lsArchivoOrg, lsArchivoMod //20200430 ajuste nombre archivos
                };

                lConexion.Abrir();
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion,
                    "pa_SetSolModCont", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensajeMod.Append("Se presentó un Problema en la creación de la solicitud de modificación.! " +
                                         goInfo.mensaje_error);
                    lConexion.Cerrar();
                }
                else
                {
                    HdfCausa = DdlIngCausa.SelectedValue;
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                        {
                            if (lLector["Error"].ToString() != "NO")
                                lblMensajeMod.Append(lLector["Error"]);
                            else
                            {
                                HdfNoSolicitud = lLector["codigo_sol"].ToString();
                                HdfTipoCausa = lLector["tipo_causa"].ToString();
                                try
                                {
                                    var fi1 = new FileInfo(FileUploadOriginal_Temp);
                                    //File.Copy(FileUploadOriginal_Temp, FileUploadOriginal_Temp.Replace("temp\\", ""), true);  //2020727 ajsute front-end
                                    File.Copy(FileUploadOriginal_Temp, lsArchivoOrgExt, true); //2020727 ajsute front-end
                                    fi1.Delete();
                                    var fi2 = new FileInfo(FileUploadModificado_Temp);
                                    //File.Copy(FileUploadModificado_Temp, FileUploadModificado_Temp.Replace("temp\\", ""), true); //2020727 ajsute front-end
                                    File.Copy(FileUploadModificado_Temp, lsArchivoModExt, true); //2020727 ajsute front-end
                                    fi2.Delete();
                                }
                                catch (Exception ex)
                                {
                                    ToastrEvent?.Invoke("Error al cargar los archivos." + ex.Message, EnumTypeToastr.Error);
                                }
                            }
                        }

                        if (string.IsNullOrEmpty(lblMensajeMod.ToString()))
                        {
                            crear();
                        }
                    }

                    lLector.Close();
                    lLector.Dispose();
                }

                lConexion.Cerrar();

                if (!string.IsNullOrEmpty(lblMensajeMod.ToString()))
                {
                    ToastrEvent?.Invoke(lblMensajeMod.ToString(), EnumTypeToastr.Error);
                    return;
                }
                manejo_bloqueo("E", HdfIdRegistro);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
            finally
            {
                FileUploadOriginal_Temp = string.Empty;
                FileUploadModificado_Temp = string.Empty;
            }
        }

        /// <summary>
        /// Metodo que realiza la actualizacion de la informacion al solicitar cambio de precio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCancelar_Click(object sender, EventArgs e)
        {
            manejo_bloqueo("E", HdfIdRegistro);
            //Cierra el modal de Causa Modificación
            ModalEvent?.Invoke(modContratoCausaMod.ID, modContratoCausaModInside.ID, EnumTypeModal.Cerrar);
        }

        /// <summary>
        /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnModificar_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_solicitud","@P_codigo_verif","@P_codigo_modif","@P_codigo_comprador","@P_codigo_vendedor","@P_punta_modifica", "@P_contrato_definitivo",
                "@P_fecha_negociacion","@P_hora_negociacion","@P_fecha_suscripcion", "@P_conectado_snt", "@P_ent_boca_pozo", "@P_ind_contrato_var", "@P_codigo_centro", "@P_codigo_punto_ent",
                "@P_codigo_modalidad","@P_codigo_periodo_ent","@P_no_anos","@P_fecha_inicial","@P_hora_inicial","@P_fecha_final", "@P_hora_final","@P_precio","@P_cantidad","@P_capacidad","@P_sentido_flujo",
                "@P_presion_punto_fin", "@P_codigo_fuente","@P_tipo_garantia","@P_valor_garantia","@P_fecha_pago","@P_codigo_depto","@P_codigo_municipio",
                "@P_sector_consumo","@P_conexion_snt","@P_mercado_relevante", "@P_codigo_ruta_may", "@P_nit_usuario_cont",
                "@P_codigo_trm","@P_observacion_trm","@P_tipo_moneda" //20210707 trm moneda
                                      };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,SqlDbType.Int,
                                        SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.Decimal,SqlDbType.Decimal,SqlDbType.Decimal,SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.Int,SqlDbType.VarChar,SqlDbType.Decimal,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,
                                        SqlDbType.Int,SqlDbType.VarChar,SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar,
                                        SqlDbType.Int,SqlDbType.VarChar,SqlDbType.VarChar//20210707 trm moneda
                                        };
            string[] lValorParametros = { "0", "0","0", "0","0", "","", "","", "","", "","", "","0", "0","0", "0","", "","", "","0", "0","0","", "","0", "","0", "","", "", "0","S","0" , "0", "",
                            "0", "",""//20210707 trm moneda
                                    };
            DateTime ldFecha;
            DateTime ldFechaI = DateTime.Now;
            int liValor = 0;
            decimal ldValor;
            string[] lsPrecio;
            var lblMensaje = new StringBuilder();
            
            try
            {
                if (DdlComprador.SelectedValue == "0" && DdlMercado.SelectedValue != "O")
                    lblMensaje.Append("Debe seleccionar el Comprador.<br>");
                if (DdlVendedor.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el vendedor.<br>");
                if (DdlMercado.SelectedValue == "O" && DdlComprador.SelectedValue == "0" && TxNitComprador.Text == "")
                    lblMensaje.Append("Debe seleccionar el Comprador.<br>");
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaNeg.Text.Trim());
                }
                catch (Exception)
                {
                    lblMensaje.Append("Fecha de negociación inválida.<br>");
                }
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaSus.Text.Trim());
                }
                catch (Exception)
                {
                    lblMensaje.Append("Fecha de suscripción inválida.<br>");
                }
                if (DdlModalidad.SelectedValue == "0")
                    lblMensaje.Append(" Debe seleccionar la modalidad del contrato. <br>");
                //20200831
                //if (HdfTipoCausa != "T" && HdfTipoCausa != "A")
                //{
                //    if (DdlMercado.SelectedValue != "O" && DdlPeriodo.SelectedValue == "0" && HdfTipoCausa != "T" && HdfTipoCausa != "A")
                //        lblMensaje.Append(" Debe seleccionar el periodo de entrega. <br>");
                //}
                try
                {
                    ldFechaI = Convert.ToDateTime(TxtFechaIni.Text.Trim());
                }
                catch (Exception)
                {
                    lblMensaje.Append("Fecha de entrega inicial inválida.<br>");
                }

                if (TxtCantidad.Text.Trim().Length <= 0)
                    lblMensaje.Append(" Debe Ingresar la cantidad. <br>");
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidad.Text);
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append(" Valor Inválido en la cantidad/capacidad. <br>");
                    }
                }
                if (TxtCapacOtm.Text.Trim().Length <= 0)
                    lblMensaje.Append(" Debe Ingresar la capacidad de transporte. <br>");
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCapacOtm.Text);
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append(" Valor Inválido en la capacidad de transporte. <br>");
                    }
                }
                if (TxtPrecio.Text.Trim().Length <= 0)
                    lblMensaje.Append(" Debe Ingresar el precio. <br>");
                else
                {
                    try
                    {
                        ldValor = Convert.ToDecimal(TxtPrecio.Text.Trim());
                        if (ldValor < 0)
                            lblMensaje.Append("Valor Inválido en el precio. <br>");
                        else
                        {
                            lsPrecio = TxtPrecio.Text.Trim().Split('.');
                            if (lsPrecio.Length > 1)
                            {
                                if (lsPrecio[1].Trim().Length > 2)
                                    lblMensaje.Append("Valor Inválido en el precio. <br>");
                            }
                        }
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append("Valor Inválido en el precio. <br>");
                    }
                }
                if (TxtContDef.Text.Trim().Length <= 0)
                    lblMensaje.Append(" Debe Ingresar el número del contrato definitivo. <br>");
                if (DdlFuente.Visible && DdlFuente.SelectedValue == "0")
                    lblMensaje.Append(" Debe seleccionar la fuente. <br>");
                if (DdlMercado.SelectedValue != "O")
                {
                    if (DdlDestino.SelectedValue == "G")
                    {
                        if (DdlPunto.Visible && DdlPunto.SelectedValue == "0")
                            lblMensaje.Append(" Debe seleccionar el punto de entrega. <br>");
                        if (DdlCentro.Visible && DdlCentro.SelectedValue == "0")
                            lblMensaje.Append(" Debe seleccionar el centro poblado. <br>");
                    }
                    else
                        if (DdlPunto.SelectedValue == "0")
                        lblMensaje.Append(" Debe seleccionar el tramo o grupo de gasoductos. <br>");
                }
                else
                {
                    if (DdlPunto.SelectedValue == "0")
                        lblMensaje.Append(" Debe seleccionar el punto de salida del SNT. <br>");
                    if (DdlDestino.SelectedValue != "G")
                    {
                        if (ddlRutaMay.SelectedValue == "0")
                            lblMensaje.Append(" Debe seleccionar la ruta de transporte. <br>");
                    }
                }
                if (TxtNoAños.Visible && TxtNoAños.Text == "")
                    lblMensaje.Append(" Debe digitar el número de años. <br>");
                if (TxtHoraIni.Visible && TxtHoraIni.Text == "")
                    lblMensaje.Append(" Debe digitar la hora inicial. <br>");
                if (TxtFechaFin.Enabled)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtFechaFin.Text.Trim());
                        if (ldFechaI > ldFecha)
                            lblMensaje.Append("la fecha de entrega inicial debe ser menor o igual que la final.<br>");
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append("Fecha de entrega final inválida.<br>");
                    }
                }
                if (TxtHoraFin.Visible && TxtHoraFin.Text == "")
                    lblMensaje.Append(" Debe digitar la hora final. <br>");
                if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "P")
                {
                    if (TxtPresion.Text == "")
                        lblMensaje.Append(" Debe digitar la presión del punto final. <br>");
                    else
                    {
                        string[] lsPresion;
                        try
                        {
                            if (TxtPresion.Text.Trim().Length > 500)
                                lblMensaje.Append("Valor Inválido en la presión del punto final. <br>");
                            else
                            {
                                lsPresion = TxtPresion.Text.Trim().Split('-');
                                foreach (string Presion in lsPresion)
                                {
                                    try
                                    {
                                        ldValor = Convert.ToDecimal(Presion.Trim());
                                    }
                                    catch (Exception)
                                    {
                                        lblMensaje.Append("Valor Inválido en la presión del punto final. <br>");
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Append("Valor Inválido en la presión del punto final. <br>");
                        }

                    }
                }
                calcula_fecha("C"); //20200831

                //20210707 trm moneda
                if (ddlTrm.SelectedValue =="0")
                    lblMensaje.Append("Debe seleccionar la tasa de cambio. <br>");
                if (txtObsTrm.Text == "")
                    lblMensaje.Append("Debe digitar las observaciones de la tasa de cambio. <br>");
                //20210707 fin trm moneda
                if (lblMensaje.ToString() != "")
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }
                if (hdfErrorFecha.Value == "") //20200830
                {
                    lValorParametros[0] = HdfNoSolicitud;
                    lValorParametros[1] = HdfIdRegistro;
                    lValorParametros[2] = "0";
                    lValorParametros[3] = DdlComprador.SelectedValue;
                    lValorParametros[4] = DdlVendedor.SelectedValue;
                    lValorParametros[5] = HdfPunta;
                    lValorParametros[6] = TxtContDef.Text.Trim();
                    lValorParametros[7] = TxtFechaNeg.Text;
                    lValorParametros[8] = TxtHoraNeg.Text;
                    lValorParametros[9] = TxtFechaSus.Text.Trim();
                    lValorParametros[10] = DdlConectado.SelectedValue;
                    lValorParametros[11] = DdlBoca.SelectedValue;
                    lValorParametros[12] = DdlVariable.SelectedValue;
                    if (DdlMercado.SelectedValue != "O")
                        lValorParametros[13] = DdlCentro.SelectedValue;
                    else
                        lValorParametros[13] = ddlCentroPuntoSal.SelectedValue;
                    lValorParametros[14] = DdlPunto.SelectedValue;
                    lValorParametros[15] = DdlModalidad.SelectedValue;
                    lValorParametros[16] = DdlPeriodo.SelectedValue;
                    lValorParametros[17] = TxtNoAños.Text.Trim();
                    lValorParametros[18] = TxtFechaIni.Text.Trim();
                    if (TxtHoraIni.Visible)
                        lValorParametros[19] = TxtHoraIni.Text.Trim();
                    else
                        lValorParametros[19] = "";
                    lValorParametros[20] = TxtFechaFin.Text.Trim();
                    if (TxtHoraFin.Visible)
                        lValorParametros[21] = TxtHoraFin.Text.Trim();
                    else
                        lValorParametros[21] = "";
                    lValorParametros[22] = TxtPrecio.Text.Trim();
                    lValorParametros[23] = TxtCantidad.Text.Trim();
                    lValorParametros[24] = TxtCapacOtm.Text.Trim();
                    if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "P")
                        lValorParametros[25] = DdlSentido.SelectedValue;
                    else
                        lValorParametros[25] = "";
                    lValorParametros[26] = TxtPresion.Text.Trim();
                    lValorParametros[27] = DdlFuente.SelectedValue;
                    lValorParametros[28] = TxtGarantia.Text;
                    lValorParametros[29] = TxtVlrGrtia.Text;
                    lValorParametros[30] = TxtFechaGrtia.Text;
                    lValorParametros[31] = ddlDeptoPuntoSal.SelectedValue;
                    lValorParametros[32] = DdlMunPuntoSal.SelectedValue;
                    lValorParametros[33] = ddlSectorConsumo.SelectedValue;
                    lValorParametros[34] = ddlConexion.SelectedValue;
                    lValorParametros[35] = ddlMercadoRelevante.SelectedValue;
                    lValorParametros[36] = ddlRutaMay.SelectedValue;
                    lValorParametros[37] = TxNitComprador.Text;
                    lValorParametros[38] = ddlTrm.SelectedValue; //20210707 trm moneda
                    lValorParametros[39] = txtObsTrm.Text; //20210707 trm moneda
                    lValorParametros[40] = ddlMoneda.SelectedValue; //20210707 trm moneda

                    lConexion.Abrir();
                    SqlDataReader lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModContReg", lsNombreParametros, lTipoparametros, lValorParametros);
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                        {
                            if (lLector["ind_error"].ToString() == "S")
                                lblMensaje.Append(lLector["error"] + "<br>");
                            else
                                HdfCodModif = lLector["CodModif"].ToString();
                        }
                    }
                    lLector.Close();
                    lConexion.Cerrar();

                    if (lblMensaje.ToString() != "")
                    {
                        ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                        return;
                    }
                    CargarDatosOper();
                    ToastrEvent?.Invoke("Operación modificada correctamente", EnumTypeToastr.Success);
                    manejo_bloqueo("E", HdfIdRegistro);
                    //Cierra el modal de Causa Modificación
                    ModalEvent?.Invoke(modContratosMod.ID, modContratosModInside.ID, EnumTypeModal.Cerrar);
                    CargarDatos();
                }//20200830
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAprobar_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_codigo_verif", "@P_ingresa_usr_fin", "@P_observaciones", "@P_estado" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "0", "N", "", "A" };
            var lblMensaje = new StringBuilder();

            if (TxtObs.Text == "")
                lblMensaje.Append("Debe ingresar las observaciones para hacer la aprobación");
            try
            {

                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[0] = HdfNoSolicitud;
                    lValorParametros[1] = HdfIdRegistro;
                    if (DdlMercado.SelectedValue != "O")
                        lValorParametros[2] = "S";
                    lValorParametros[3] = TxtObs.Text;

                    lConexion.Abrir();
                    SqlDataReader lLector;
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModContRegAprobCont", lsNombreParametros, lTipoparametros, lValorParametros);
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                            lblMensaje.Append(lLector["error"] + "<br>");
                    }
                    lLector.Close();
                    lConexion.Cerrar();

                    if (lblMensaje.ToString() == "")
                    {
                        CargarDatosOper();
                        ToastrEvent?.Invoke("Modificación aprobada correctamente", EnumTypeToastr.Success);
                        btnCancelarM_Click(null, null);
                        return;
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRechazar_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_codigo_verif", "@P_ingresa_usr_fin", "@P_observaciones", "@P_estado" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "0", "N", "", "R" };
            var lblMensaje = new StringBuilder();

            if (TxtObs.Text == "")
            {
                ToastrEvent?.Invoke("Debe ingresar las observaciones para hacer el rechazo", EnumTypeToastr.Error);
                return;
            }

            try
            {
                lValorParametros[0] = HdfNoSolicitud;
                lValorParametros[1] = HdfIdRegistro;
                if (DdlMercado.SelectedValue != "O")
                    lValorParametros[2] = "S";
                lValorParametros[3] = TxtObs.Text;

                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModContRegAprobCont", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Append(lLector["error"] + "<br>");
                }
                lLector.Close();
                lConexion.Cerrar();

                if (lblMensaje.ToString() == "")
                {
                    CargarDatosOper();
                    ToastrEvent?.Invoke("Modificación rechazada correctamente", EnumTypeToastr.Success);
                    btnCancelarM_Click(null, null);
                    return;
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAprobarBmc_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_solicitud","@P_codigo_verif","@P_observaciones", "@P_estado"
                                      };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar
                                        };
            string[] lValorParametros = { "0","0", "", "A"
                                    };
            var lblMensaje = new StringBuilder();


            if (TxtObs.Text == "")
            {
                ToastrEvent?.Invoke("Debe ingresar las observaciones para hacer la aprobación", EnumTypeToastr.Error);
                return;
            }

            try
            {
                lValorParametros[0] = HdfNoSolicitud;
                lValorParametros[1] = HdfIdRegistro;
                lValorParametros[2] = TxtObs.Text;
                lConexion.Abrir();
                SqlDataReader lLector;

                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModContRegAprobBmc", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Append(lLector["error"] + "<br>");
                }
                lLector.Close();
                lConexion.Cerrar();

                if (lblMensaje.ToString() == "")
                {
                    manejo_bloqueo("E", HdfIdRegistro);
                    CargarDatosOper();
                    ToastrEvent?.Invoke("Modificación aprobada correctamente", EnumTypeToastr.Success);
                    btnCancelarM_Click(null, null);
                    return;
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Error al aprobar la solicitud. " + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>hdfContratoDef
        protected void btnRechazarBmc_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_solicitud","@P_codigo_verif","@P_observaciones", "@P_estado"
                                      };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar
                                        };
            string[] lValorParametros = { "0","0", "", "R"
                                    };
            var lblMensaje = new StringBuilder();

            if (TxtObs.Text == "")
            {
                ToastrEvent?.Invoke("Debe ingresar las observaciones para hacer el rechazo", EnumTypeToastr.Error);
                return;
            }

            try
            {
                lValorParametros[0] = HdfNoSolicitud;
                lValorParametros[1] = HdfIdRegistro;
                lValorParametros[2] = TxtObs.Text;
                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModContRegAprobBmc", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Append(lLector["error"] + "<br>");
                }
                lLector.Close();
                lConexion.Cerrar();

                if (lblMensaje.ToString() == "")
                {
                    manejo_bloqueo("E", HdfIdRegistro);
                    CargarDatosOper();
                    ToastrEvent?.Invoke("Modificación rechazada correctamente", EnumTypeToastr.Success);
                    btnCancelarM_Click(null, null);
                    return;
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Error al rechazar la solicitud. " + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: btnRegresar_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        protected void btnCancelarM_Click(object sender, EventArgs e)
        {
            manejo_bloqueo("E", HdfIdRegistro);
            CargarDatos();
            //Cierra el modal de Causa Modificación
            ModalEvent?.Invoke(modContratosMod.ID, modContratosModInside.ID, EnumTypeModal.Cerrar);
        }

        /// <summary>
        /// Nombre: CargarDatosOper
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatosOper()
        {
            lConexion.Abrir();
            string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_consecutivo" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { HdfIdRegistro, HdfConsec };
            try
            {
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetDatosContMod", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    ToastrEvent?.Invoke("Se presentó un Problema en la selección de la información.! " + goInfo.mensaje_error, EnumTypeToastr.Error);
                    lConexion.Cerrar();
                }
                else
                {
                    if (lLector.HasRows)
                    {
                        lLector.Read();

                        DdlDestino.SelectedValue = lLector["destino_rueda"].ToString();
                        DdlMercado.SelectedValue = lLector["tipo_mercado"].ToString();
                        try
                        {
                            DdlVendedor.SelectedValue = lLector["operador_venta"].ToString();
                        }
                        catch (Exception)
                        {
                        }
                        try
                        {
                            DdlComprador.SelectedValue = lLector["operador_compra"].ToString();
                            TxNitComprador.Text = lLector["nit_usuario_cont"].ToString();
                            if (DdlComprador.SelectedValue != "0")
                            {
                                DdlComprador.Visible = true;
                                TxNitComprador.Visible = false;
                                TxNitComprador.Text = "0";
                            }
                            else
                            {
                                DdlComprador.Visible = false;
                                TxNitComprador.Visible = true;
                            }
                        }
                        catch (Exception)
                        {
                        }
                        TxtFechaNeg.Text = lLector["fecha_neg"].ToString();
                        TxtHoraNeg.Text = lLector["hora_neg"].ToString();
                        TxtFechaSus.Text = lLector["fecha_suscripcion"].ToString();
                        TxtContDef.Text = lLector["contrato_definitivo"].ToString();
                        DdlVariable.SelectedValue = lLector["ind_contrato_var"].ToString();
                        try
                        {
                            DdlFuente.SelectedValue = lLector["codigo_fuente"].ToString();
                        }
                        catch (Exception)
                        { }
                        DdlConectado.SelectedValue = lLector["conectado_snt"].ToString();
                        DdlBoca.SelectedValue = lLector["ent_boca_pozo"].ToString();
                        DdlBoca_SelectedIndexChanged(null, null); //20191230 //20200727 ajsue front-end 
                        try
                        {
                            DdlPunto.SelectedValue = lLector["codigo_punto_entrega"].ToString();
                        }
                        catch (Exception)
                        { }
                        try
                        {
                            DdlCentro.SelectedValue = lLector["codigo_centro_poblado"].ToString();
                        }
                        catch (Exception)
                        {
                        }
                        try
                        {
                            DdlModalidad.SelectedValue = lLector["codigo_modalidad"].ToString();
                        }
                        catch (Exception)
                        {
                        }
                        try
                        {
                            DdlPeriodo.SelectedValue = lLector["codigo_periodo"].ToString();
                        }
                        catch (Exception)
                        { }
                        TxtNoAños.Text = lLector["no_años"].ToString();
                        DdlPeriodo_SelectedIndexChanged(null, null);
                        TxtFechaIni.Text = lLector["fecha_ini"].ToString();
                        TxtHoraIni.Text = lLector["hora_ini"].ToString();
                        TxtFechaFin.Text = lLector["fecha_fin"].ToString();
                        TxtHoraFin.Text = lLector["hora_fin"].ToString();
                        TxtCantidad.Text = lLector["cantidad"].ToString();
                        TxtCapacOtm.Text = lLector["capacidad"].ToString();
                        TxtPrecio.Text = lLector["precio"].ToString();
                        if (lLector["sentido_flujo"].ToString().Trim() != "")
                            DdlSentido.SelectedValue = lLector["sentido_flujo"].ToString();
                        TxtPresion.Text = lLector["presion_punto_fin"].ToString();
                        TxtGarantia.Text = lLector["tipo_garantia"].ToString();
                        TxtVlrGrtia.Text = lLector["valor_garantia"].ToString();
                        TxtFechaGrtia.Text = lLector["fecha_pago"].ToString();
                        
                        try
                        {
                            ddlConexion.SelectedValue = lLector["conexion_snt"].ToString();
                            ddlConexion_SelectedIndexChanged(null, null);
                        }
                        catch (Exception)
                        {
                        }
                        try
                        {
                            ddlSectorConsumo.SelectedValue = lLector["sector_consumo"].ToString();
                        }
                        catch (Exception)
                        {
                        }
                        try
                        {
                            ddlMercadoRelevante.SelectedValue = lLector["mercado_relevante"].ToString();
                        }
                        catch (Exception)
                        {
                        }
                        try
                        {
                            ddlRutaMay.SelectedValue = lLector["codigo_ruta_may"].ToString();
                        }
                        catch (Exception)
                        {
                        }
                        try
                        {
                            ddlDeptoPuntoSal.SelectedValue = lLector["codigo_depto"].ToString();
                            ddlDeptoPuntoSal_SelectedIndexChanged(null, null); //20200831 ajsut OTMM
                        }
                        catch (Exception)
                        {
                        }
                        try
                        {
                            DdlMunPuntoSal.SelectedValue = lLector["codigo_municipio"].ToString();
                            DdlMunPuntoSal_SelectedIndexChanged(null, null);
                        }
                        catch (Exception)
                        {
                        }
                        try
                        {
                            ddlCentroPuntoSal.SelectedValue = lLector["codigo_centro_poblado"].ToString();
                        }
                        catch (Exception)
                        {
                        }
                        //20210707 trm moneda
                        try
                        {
                            ddlTrm.SelectedValue = lLector["codigo_trm"].ToString();
                        }
                        catch (Exception)
                        {
                            ddlTrm.SelectedValue = "0";
                        }
                        txtObsTrm.Text = lLector["observacion_trm"].ToString();
                        try
                        {
                            ddlMoneda.SelectedValue = lLector["tipo_moneda"].ToString();
                        }
                        catch (Exception)
                        {
                        }
                        //20210707 fin trm moneda
                        //20200831 ajsut OTMM
                        if (HdfPunta == "C" && DdlMercado.SelectedValue != "M")
                            CargarDatosUsu();
                    }
                }
                lLector.Close();
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Error al recuperar información." + ex.Message, EnumTypeToastr.Error);
                lConexion.Cerrar();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20170814 rq036-17
        protected void DdlConectado_SelectedIndexChanged(object sender, EventArgs e)
        {
            valida_conec_boca();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20170814 rq036-17
        protected void DdlBoca_SelectedIndexChanged(object sender, EventArgs e)
        {
            valida_conec_boca();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20170814 rq036-17
        protected void DdlPeriodo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (DdlMercado.SelectedValue != "O" && HdfTipoCausa != "T") //20181206 ajuste  //20190109 ajuste //20190109 ajuste 20200831
            {
                TxtHoraIni.Text = "";
                TxtHoraIni.Visible = false;
                TxtHoraFin.Text = "";
                TxtHoraFin.Visible = false;
                //TxtFechaFin.Text = ""; //20200831
                //TxtFechaFin.Enabled = false; //20200831
                LblNoAños.Visible = false;
                TxtNoAños.Visible = false;
                SqlDataReader lLector;
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_periodos_entrega", "codigo_periodo=" + DdlPeriodo.SelectedValue);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    HndTiempo.Value = lLector["medida_tiempo"].ToString();
                    hdfMesInicialPeriodo.Value = lLector["mes_inicial_periodo"].ToString();
                    //20200811 ajuste  //20200820 ajuste
                    //20200831
                    //if (HndTiempo.Value == "U") //20200831
                    //{
                    //    if (HdfModificaFin== "S")
                    //        TxtFechaFin.Enabled = true;
                    //}
                    if (HndTiempo.Value == "I")
                    {
                        TxtHoraIni.Visible = true;
                        TxtHoraFin.Visible = true;
                    }
                    if (HndTiempo.Value == "L")
                    {
                        LblNoAños.Visible = true;
                        TxtNoAños.Visible = true;
                    }
                    else
                        TxtNoAños.Text = "0";
                }
                else
                {
                    HndTiempo.Value = "";
                    hdfMesInicialPeriodo.Value = "0";
                }
                lLector.Close();
                lConexion.Cerrar();
                //lblMensaje.Text = ""; //20190401 ajuste
                if (DdlMercado.SelectedValue != "O" && HdfTipoCausa != "T")  //20200831
                    calcula_fecha("M"); //20200831 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20170814 rq036-17
        protected void valida_conec_boca()
        {
            if (DdlMercado.SelectedValue == "S" && DdlDestino.SelectedValue == "G")
            {
                if (DdlConectado.SelectedValue == "S")
                {
                    lConexion.Abrir();
                    DdlPunto.Items.Clear();
                    LlenarControles(lConexion.gObjConexion, DdlPunto, "m_pozo", " ind_estandar='S' and  estado = 'A'  order by descripcion", 0, 1);
                    lConexion.Cerrar();
                    DdlCentro.Visible = false;
                    DdlCentro.SelectedValue = "0";
                    DdlBoca.SelectedValue = "N";
                    DdlPunto.Visible = true;
                    LblPunto.Text = "Punto de entrega de la energía al comprador";
                }
                else
                {
                    if (DdlBoca.SelectedValue == "S")
                    {
                        lConexion.Abrir();
                        DdlPunto.Items.Clear();
                        LlenarControles(lConexion.gObjConexion, DdlPunto, "m_pozo", " codigo_tipo_campo = 6 and  estado = 'A'  order by descripcion", 0, 1);
                        lConexion.Cerrar();
                        LblPunto.Text = "Punto de entrega de la energía al comprador";
                        DdlPunto.Visible = true;
                        DdlCentro.Visible = false;
                        DdlCentro.SelectedValue = "0";

                    }
                    else
                    {
                        LblPunto.Text = "Centro Poblado";
                        DdlCentro.Visible = true;
                        DdlCentro.SelectedValue = "0";
                        DdlPunto.SelectedValue = "0";
                        DdlPunto.Visible = false;
                    }
                }
            }
        }
        protected void TxtFechaIni_TextChanged(object sender, EventArgs e)
        {
            //lblMensaje.Text = "";//20200831 ajsute
            calcula_fecha("M"); //20200831 ajsute
            ModalEvent?.Invoke(modContratosMod.ID, modContratosModInside.ID, EnumTypeModal.Abrir); //20201021 ajuste compoenente
        }
        //2020831
        protected void TxtFechaFin_TextChanged(object sender, EventArgs e)
        {
            //lblMensaje.Text = "";
            calcula_fecha("C"); //20200831 ajsute
            ModalEvent?.Invoke(modContratosMod.ID, modContratosModInside.ID, EnumTypeModal.Abrir); //20201021 ajuste compoenente
        }
        protected void calcula_fecha(string lsIndica) //20200831
        {
            DateTime ldFecha;
            string lsDisSemana = "0";
            string lsFecha = "";
            string lsFecha1 = "";
            int liAnos = 0;
            int liMes2Tr = 0;
            int liMes3Tr = 0;
            int liMes4Tr = 0;
            hdfErrorFecha.Value = "";


            //lblMensaje.Text = ""; 20190401 ajuste
            if (TxtFechaIni.Text.Trim().Length > 0 && DdlPeriodo.SelectedValue != "0")
            {
                try
                {

                    ldFecha = Convert.ToDateTime(TxtFechaIni.Text.Trim());
                    if (HndTiempo.Value != "I")
                    {
                        //// Valculo Fecha Final cuando es Intradiario
                        if (HndTiempo.Value == "D")
                        {
                            TxtFechaFin.Text = TxtFechaIni.Text.Trim();
                            lsFecha = TxtFechaFin.Text; //20210112
                        }
                        if (HndTiempo.Value == "S")
                        {
                            lsDisSemana = Convert.ToDateTime(TxtFechaIni.Text.Trim()).DayOfWeek.ToString();
                            if (lsDisSemana != "Monday")
                                hdfErrorFecha.Value = "La Fecha de Inicio debe ser el primer día de la semana, ya que escogió periodo semanal";
                            else
                            {
                                lsFecha = Convert.ToDateTime(TxtFechaIni.Text.Trim()).AddDays(6).ToString();
                            }
                        }
                        if (HndTiempo.Value == "M")
                        {
                            if (Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(8, 2)) != 1)
                                hdfErrorFecha.Value = "La Fecha de Inicio debe ser el primer día del mes, ya que escogió periodo mensual";
                            else
                            {
                                lsFecha = Convert.ToDateTime(TxtFechaIni.Text.Trim()).AddMonths(1).AddDays(-1).ToString();
                            }
                        }
                        if (HndTiempo.Value == "T")
                        {
                            lsFecha1 = DateTime.Now.Year + "/" + hdfMesInicialPeriodo.Value + "/01";
                            liMes2Tr = Convert.ToDateTime(lsFecha1).AddMonths(3).Month;
                            liMes3Tr = Convert.ToDateTime(lsFecha1).AddMonths(6).Month;
                            liMes4Tr = Convert.ToDateTime(lsFecha1).AddMonths(9).Month;
                            if (Convert.ToInt32(hdfMesInicialPeriodo.Value) != Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(5, 2)) && Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(5, 2)) != liMes2Tr && Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(5, 2)) != liMes3Tr && Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(5, 2)) != liMes4Tr)
                                hdfErrorFecha.Value = "El Mes de la Fecha de Inicio es diferente al mes parametrizado de inicio {" + hdfMesInicialPeriodo.Value + "-" + liMes2Tr + "-" + liMes3Tr + "-" + liMes4Tr + "}, ya que escogió periodo trimestral.";
                            else
                            {
                                if (Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(8, 2)) != 1)
                                    hdfErrorFecha.Value = "La Fecha de Inicio debe ser el primer día del mes, ya que escogió periodo trimestral.";
                                else
                                {
                                    lsFecha = Convert.ToDateTime(TxtFechaIni.Text.Trim()).AddMonths(3).AddDays(-1).ToString();
                                }

                            }
                        }
                        if (HndTiempo.Value == "A")
                        {
                            if (Convert.ToInt32(hdfMesInicialPeriodo.Value) != Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(5, 2)))
                                hdfErrorFecha.Value = "El Mes de la Fecha de inicio es diferente al mes parametrizado de inicio {" + hdfMesInicialPeriodo.Value + "}, ya que escogió periodo anual.";
                            else
                            {
                                if (Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(8, 2)) != 1)
                                    hdfErrorFecha.Value = "La Fecha de inicio debe ser el primer día del mes, ya que escogió periodo anual.";
                                else
                                {
                                    lsFecha = Convert.ToDateTime(TxtFechaIni.Text.Trim()).AddMonths(12).AddDays(-1).ToString();
                                }

                            }
                        }
                        if (HndTiempo.Value == "L")
                        {
                            if (TxtNoAños.Text.Trim().Length <= 0)
                                hdfErrorFecha.Value = "Debe ingresar el número de años, ya que escogió periodo multiAnual.";
                            if (TxtNoAños.Text.Trim().Length > 0)
                            {
                                try
                                {
                                    liAnos = Convert.ToInt32(TxtNoAños.Text.Trim());
                                    if (Convert.ToInt32(hdfMesInicialPeriodo.Value) != Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(5, 2)))
                                        hdfErrorFecha.Value = "El mes de la fecha de inicio es diferente al mes parametrizado de inicio {" + hdfMesInicialPeriodo.Value + "}, ya que escogió periodo multi-anual.";
                                    else
                                    {
                                        if (Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(8, 2)) != 1)
                                            hdfErrorFecha.Value = "La Fecha de inicio debe ser el primer día del mes, ya que escogió periodo multi-anual.";
                                        else
                                        {
                                            lsFecha = Convert.ToDateTime(TxtFechaIni.Text.Trim()).AddYears(Convert.ToInt32(TxtNoAños.Text.Trim())).AddDays(-1).ToString();
                                        }
                                    }
                                }
                                catch (Exception)
                                {
                                    hdfErrorFecha.Value = "Valor inválido en el campo de número de años del periodo multi-anual.";
                                }
                            }
                        }
                    }
                    else
                    {
                        TxtFechaFin.Text = TxtFechaIni.Text.Trim();
                    }
                    //else
                    //{
                    //    TxtFechaFin.Text = TxtFechaIni.Text.Trim();
                    //    if (ldFecha < Convert.ToDateTime(TxtFechaNeg.Text))
                    //        hdfErrorFecha.Value += "La Fecha de Inicio debe ser mayor o igual que la fecha de negociación.<br>";
                    //    if (TxtHoraIni.Text != "")
                    //        if (Convert.ToDateTime(TxtFechaIni.Text + ' ' + TxtHoraIni.Text) <= Convert.ToDateTime(TxtFechaNeg.Text + ' ' + TxtHoraNeg.Text))
                    //            hdfErrorFecha.Value += "La Fecha-hora de Inicio debe ser mayor que la fecha-hora de negociación.<br>";
                    //}

                    //20200831


                    if (hdfErrorFecha.Value != "")
                        //lblMensaje.Text += hdfErrorFecha.Value; //20190401 ajustes
                        ToastrEvent?.Invoke(hdfErrorFecha.Value, EnumTypeToastr.Error);
                    else
                    {
                        if (lsIndica == "M") //20200831
                        {
                            if (lsFecha != "")
                                TxtFechaFin.Text = Convert.ToDateTime(lsFecha).ToString("yyyy/MM/dd"); //20200831
                        }
                        else
                        {
                            //20200831
                            try
                            {
                                if (HndTiempo.Value != "U" && Convert.ToDateTime(lsFecha) != Convert.ToDateTime(TxtFechaFin.Text)) //2020831
                                {
                                    ToastrEvent?.Invoke("La fecha final no corresponde con el periodo de entrega", EnumTypeToastr.Error);//20200830
                                    hdfErrorFecha.Value = "La fecha final no corresponde con el periodo de entrega";
                                }
                                //lblMensaje.Text += "<br>" + "La fecha final no corresponde con el periodo de entrega"; //20190401 ajustes
                            }
                            catch (Exception ex)
                            {
                                hdfErrorFecha.Value = "Valor Inválido en la fecha final";//20200830
                                ToastrEvent?.Invoke("Valor Inválido en la fecha final", EnumTypeToastr.Error);
                                //lblMensaje.Text = "Valor Inválido en la fecha final";
                            }
                        }

                    }
                }
                catch (Exception)
                {
                    hdfErrorFecha.Value = "Valor Inválido en la fecha inicial"; //20200830
                    ToastrEvent?.Invoke("Valor Inválido en la fecha inicial", EnumTypeToastr.Error);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlDeptoPuntoSal_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlDeptoPuntoSal.SelectedValue != "0")  //20170530 divipola
            //{  //20170530 divipola
            DdlMunPuntoSal.Items.Clear();
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, DdlMunPuntoSal, "m_divipola", " estado = 'A'  And codigo_ciudad != '0' And codigo_departamento = '" + ddlDeptoPuntoSal.SelectedValue + "' and codigo_centro ='0' order by nombre_ciudad", 3, 4); //20170530 divipola
            lConexion.Cerrar();
            DdlMunPuntoSal_SelectedIndexChanged(null, null); //20170530 divipola
                                                             //}  //20170530 divipola
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20170530 divipola
        protected void DdlMunPuntoSal_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCentroPuntoSal.Items.Clear();
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlCentroPuntoSal, "m_divipola", " estado = 'A' And codigo_ciudad = '" + DdlMunPuntoSal.SelectedValue + "' and codigo_centro <> '0' order by nombre_centro", 5, 6);
            lConexion.Cerrar();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCrearUsu_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_cont_usr", "@P_codigo_modif","@P_codigo_verif", "@P_no_identificacion_usr", "@P_codigo_tipo_doc",
                                        "@P_nombre_usuario", "@P_codigo_sector_consumo", "@P_codigo_punto_salida","@P_cantidad_contratada","@P_tipo_demanda",
                                        "@P_codigo_mercado_relevante","@P_equivalente_kpcd", "@P_accion","@P_destino_rueda", "@P_codigo_solicitud"
                                      };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,
                                         SqlDbType.VarChar, SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,
                                         SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int
                                      };
            string[] lValorParametros = { "0", "0", "0", "", "", "", "0", " 0", "0", "0", "0","0","1","","0"
                                    };

            string[] lsUsuario;
            string lsNombre = "";
            string lsTipoDoc = "";
            int liValor = 0;
            var lblMensaje = new StringBuilder();

            try
            {
                lConexion.Abrir();
                lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
                if (dlTipoDemanda.SelectedValue == "0")
                    lblMensaje.Append("Debe Seleccionar el Tipo de Demanda a Atender. <br> ");
                else
                {
                    if (dlTipoDemanda.SelectedValue == "2")
                    {
                        if (TxtUsuarioFinal.Text.Trim().Length <= 0)
                            lblMensaje.Append("Debe Ingresar el Usuario Final <br>");
                        else
                        {
                            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                            if (!lLector.HasRows)
                                lblMensaje.Append("El Usuario Ingresado NO existe en la Base de Datos.! <br>");
                            else
                            {
                                lLector.Read();
                                lsNombre = lLector["nombre"].ToString();
                                lsTipoDoc = lLector["codigo_tipo_doc"].ToString();
                            }
                            lLector.Close();
                            lLector.Dispose();
                        }
                    }
                    else
                    {
                        if (ddlMercadoRel.SelectedValue == "0")
                            lblMensaje.Append("Debe Seleccionar el mercado relevante. <br> ");
                    }
                }
                if (ddlSector.SelectedValue == "0")
                    lblMensaje.Append("Debe Seleccionar " + lblSector.Text + ". <br> ");
                if (ddlPuntoSalida.SelectedValue == "0")
                    lblMensaje.Append("Debe Seleccionar  Punto de Salida en SNT. <br> ");
                if (TxtCantidadUsu.Text.Trim().Length <= 0)
                    lblMensaje.Append("Debe Ingresar " + lblCantContra.Text + ". <br> ");
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidadUsu.Text.Trim());
                        if (liValor < 1)
                            lblMensaje.Append("Valor Inválido " + lblCantContra.Text + ". <br> ");
                        else
                        {
                            if (liValor > Convert.ToDecimal(TxtCantidad.Text))
                                lblMensaje.Append("La " + lblCantContra.Text + " No puede ser Mayor que " + TxtCantidad.Text + ". <br> ");
                            else
                            {
                                if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(TxtCantidad.Text))
                                    lblMensaje.Append("La cantidad Acumulada de Usuarios No puede ser Mayor que " + TxtCantidad.Text + ". <br> ");
                            }
                        }
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append("Valor Inválido " + lblCantContra.Text + ". <br> ");
                    }
                }
                if (TxtEquivaleKpcd.Text.Trim().Length <= 0)
                    lblMensaje.Append("Debe Ingresar Equivalente Kpcd. <br> ");
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtEquivaleKpcd.Text.Trim());
                        if (liValor < 0)
                            lblMensaje.Append("Valor Inválido Equivalente Kpcd. <br> ");
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append("Valor Inválido " + lblCantContra.Text + ". <br> ");
                    }
                }

                if (lblMensaje.ToString() != "")
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }

                lValorParametros[0] = "0";
                if (HdfCodModif != "")
                    lValorParametros[1] = HdfCodModif;
                if (HdfIdRegistro != "")
                    lValorParametros[2] = HdfIdRegistro;
                if (hdfTipoDemanda.Value == "2")
                {
                    lValorParametros[3] = lsUsuario[0].Trim();
                    lValorParametros[4] = lsTipoDoc;
                    lValorParametros[5] = lsNombre;
                }
                lValorParametros[6] = ddlSector.SelectedValue;
                lValorParametros[7] = ddlPuntoSalida.SelectedValue;
                lValorParametros[8] = TxtCantidadUsu.Text.Trim();
                lValorParametros[9] = dlTipoDemanda.SelectedValue;
                lValorParametros[10] = ddlMercadoRel.SelectedValue;
                lValorParametros[11] = TxtEquivaleKpcd.Text.Trim();
                lValorParametros[13] = DdlDestino.SelectedValue;
                lValorParametros[14] = HdfNoSolicitud;

                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetModContRegUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    ToastrEvent?.Invoke("Se presentó un Problema en la Creación de la Información del Usuario.!" + goInfo.mensaje_error, EnumTypeToastr.Error);
                    lConexion.Cerrar();
                }
                else
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(TxtCantidadUsu.Text.Trim())).ToString();
                    Toastr.Success(this, "Información de Usuarios Finales Ingresada Correctamente.!");
                    TxtUsuarioFinal.Text = "";
                    ddlSector.SelectedValue = "0";
                    ddlPuntoSalida.SelectedValue = "0";
                    ddlMercadoRel.SelectedValue = "0";
                    TxtCantidadUsu.Text = "0";
                    TxtEquivaleKpcd.Text = "0"; // Campos nuevos Req. 009-17 Indicadores 20170321
                    CargarDatosUsu();
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnActualUsu_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_cont_usr", "@P_codigo_modif","@P_codigo_verif", "@P_no_identificacion_usr", "@P_codigo_tipo_doc",
                                        "@P_nombre_usuario", "@P_codigo_sector_consumo", "@P_codigo_punto_salida","@P_cantidad_contratada","@P_tipo_demanda",
                                        "@P_codigo_mercado_relevante","@P_equivalente_kpcd", "@P_accion","@P_destino_rueda", "@P_codigo_solicitud"
                                      };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,
                                         SqlDbType.VarChar, SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,
                                         SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int
                                      };
            string[] lValorParametros = { "0", "0", "0", "", "", "", "0", " 0", "0", "0", "0","0","2","", "0"
                                    };

            string[] lsUsuario;
            int liValor = 0;
            string lsNombre = "";
            string lsTipoDoc = "";
            var lblMensaje = new StringBuilder();

            try
            {
                lConexion.Abrir();
                lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
                if (dlTipoDemanda.SelectedValue == "0")
                    lblMensaje.Append("Debe Seleccionar el Tipo Demanda Atender. <br> ");
                else
                {
                    if (hdfTipoDemanda.Value == "2")
                    {
                        if (TxtUsuarioFinal.Text.Trim().Length <= 0)
                            lblMensaje.Append("Debe Ingresar el Usuario Final <br>");
                        else
                        {
                            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                            if (!lLector.HasRows)
                                lblMensaje.Append("El Usuario Ingresado NO existe en la Base de Datos.! <br>");
                            else
                            {
                                lLector.Read();
                                lsNombre = lLector["nombre"].ToString();
                                lsTipoDoc = lLector["codigo_tipo_doc"].ToString();
                            }
                            lLector.Close();
                            lLector.Dispose();
                        }
                    }
                    else
                    {
                        if (ddlMercadoRel.SelectedValue == "0")
                            lblMensaje.Append("Debe Seleccionar el mercado relevante. <br> ");
                    }

                }
                if (ddlSector.SelectedValue == "0")
                    lblMensaje.Append("Debe Seleccionar " + lblSector.Text + ". <br> ");
                if (ddlPuntoSalida.SelectedValue == "0")
                    lblMensaje.Append("Debe Seleccionar  Punto de Salida en SNT. <br> ");
                if (TxtCantidadUsu.Text.Trim().Length <= 0)
                    lblMensaje.Append("Debe Ingresar " + lblCantContra.Text + ". <br> ");
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidadUsu.Text.Trim());
                        if (liValor < 1)
                            lblMensaje.Append("Valor Inválido " + lblCantContra.Text + ". <br> ");
                        else
                        {
                            if (liValor > Convert.ToDecimal(TxtCantidad.Text))
                                lblMensaje.Append("La " + lblCantContra.Text + " No puede ser Mayor que " + TxtCantidad.Text + ". <br> ");
                            else
                            {
                                if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(TxtCantidad.Text))
                                    lblMensaje.Append("La Cantidad Acumulada de Usuarios No puede ser Mayor que " + TxtCantidad.Text + ". <br> ");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Append("Valor Inválido " + lblCantContra.Text + ". <br> ");
                    }
                }
                if (TxtEquivaleKpcd.Text.Trim().Length <= 0)
                    lblMensaje.Append("Debe Ingresar el Equivalente Kpcd. <br> ");
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtEquivaleKpcd.Text.Trim());
                        if (liValor < 0)
                            lblMensaje.Append("Valor Inválido en Equivalente Kpcd. <br> ");
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Append("Valor Inválido en " + lblCantContra.Text + ". <br> ");
                    }
                }

                if (lblMensaje.ToString() != "")
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }
                if (hdfCodDatUsu.Value != "")
                    lValorParametros[0] = hdfCodDatUsu.Value;
                if (HdfCodModif != "")
                    lValorParametros[1] = HdfCodModif;
                if (HdfIdRegistro != "")
                    lValorParametros[1] = HdfIdRegistro;
                if (hdfTipoDemanda.Value == "2")
                {
                    lValorParametros[3] = lsUsuario[0].Trim();
                    lValorParametros[4] = lsTipoDoc;
                    lValorParametros[5] = lsNombre;
                }
                lValorParametros[6] = ddlSector.SelectedValue;
                lValorParametros[7] = ddlPuntoSalida.SelectedValue;
                lValorParametros[8] = TxtCantidadUsu.Text.Trim();
                lValorParametros[9] = dlTipoDemanda.SelectedValue;
                lValorParametros[10] = ddlMercadoRel.SelectedValue;
                lValorParametros[11] = TxtEquivaleKpcd.Text.Trim();
                lValorParametros[13] = DdlDestino.SelectedValue;
                lValorParametros[14] = HdfNoSolicitud;

                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetModContRegUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    ToastrEvent?.Invoke("Se presentó un Problema en la Actualización de la Información del Usuario.! " + goInfo.mensaje_error, EnumTypeToastr.Error);
                    lConexion.Cerrar();
                }
                else
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                    Toastr.Success(this, "Información de Usuarios Finales Actualizada Correctamente.!");
                    TxtUsuarioFinal.Text = "";
                    ddlSector.SelectedValue = "0";
                    ddlPuntoSalida.SelectedValue = "0";
                    ddlMercadoRel.SelectedValue = "0";
                    TxtCantidadUsu.Text = "0";
                    TxtEquivaleKpcd.Text = "0"; // Campos nuevos Req. 009-17 Indicadores 20170321
                    btnCrearUsu.Visible = true;
                    btnActualUsu.Visible = false;
                    CargarDatosUsu();
                    btnActualUsu.Visible = false;
                    btnCrearUsu.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dlTipoDemanda_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hdfTipoDemanda.Value = dlTipoDemanda.SelectedValue;
                if (hdfTipoDemanda.Value == "1")
                {
                    TxtUsuarioFinal.Enabled = false;
                    ddlMercadoRel.Enabled = true; //20160303
                    lblSector.Text = "Sector de Consumo Usuario Regulado";
                    lblCantContra.Text = "Cantidad a entregar";
                }
                else
                {
                    TxtUsuarioFinal.Enabled = true;
                    ddlMercadoRel.Enabled = false; //20160303
                    ddlMercadoRel.SelectedValue = "0"; //20160303
                    lblCantContra.Text = "Cantidad Contratada por usuario (MBTUD)";
                    lblSector.Text = "Sector Consumo Usuario No Regulado";
                }
                lConexion.Abrir();
                ddlSector.Items.Clear();
                ddlPuntoSalida.Items.Clear();
                LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo sec, m_demanda_sector dem", " sec.estado ='A' and sec.codigo_sector_consumo = dem.codigo_sector_consumo  and dem.estado ='A' and dem.codigo_tipo_demanda =" + dlTipoDemanda.SelectedValue + "  and dem.ind_uso ='T' order by descripcion", 0, 1); //20160706
                LlenarControles(lConexion.gObjConexion, ddlPuntoSalida, "m_punto_salida_snt", " estado = 'A'  order by descripcion", 0, 2);
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {

            }
        }
        /// <summary>
        /// Nombre: CargarDatosUsu
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid de Usuarios.
        /// Modificacion:
        /// </summary>
        private void CargarDatosUsu()
        {
            string[] lsNombreParametros = { "@P_codigo_modif", "@P_codigo_verif", "@P_no_identificacion_usr", "@P_login" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "0", "", "" };
            try
            {
                if (HdfCodModif.Trim().Length > 0 && HdfCodModif.Trim() != "0")
                    lValorParametros[0] = HdfCodModif;
                if (HdfIdRegistro.Trim().Length > 0)
                    lValorParametros[1] = HdfIdRegistro;
                if (Session["tipoPerfil"].ToString() == "N")
                    lValorParametros[3] = goInfo.Usuario;
                lConexion.Abrir();
                dtgUsuarios.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContModUsuFin", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgUsuarios.DataBind();
                lConexion.Cerrar();
                lblTotlCantidad.Text = "0";
                if (dtgUsuarios.Items.Count > 0)
                {
                    foreach (DataGridItem Grilla in this.dtgUsuarios.Items)
                    {
                        lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(Grilla.Cells[6].Text.Trim())).ToString();
                    }
                    dtgUsuarios.Columns[7].Visible = true;
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("No se Pudo Generar el Informe.! " + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: dtgUsuarios_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgUsuarios_EditCommand(object source, DataGridCommandEventArgs e)
        {

            if (e.CommandName.Equals("Modificar"))
            {
                try
                {
                    ////////////////////////////////////////////////////////////////////////////////////////
                    /// OJO Cambios de los Indices por adicion de Campo Req. 009-17 Indicadores 20170321 ///
                    ////////////////////////////////////////////////////////////////////////////////////////
                    string lsTipoDoc = "";
                    if (this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[14].Text == "1")  //rq009-17
                        lsTipoDoc = "Nit";
                    else
                        lsTipoDoc = "Cédula";
                    hdfCodDatUsu.Value = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text; //rq009-17
                                                                                                  //Ajuste para que no muestre infrmacion mala 20160621
                    if (this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[1].Text == "&nbsp;")
                        TxtUsuarioFinal.Text = lsTipoDoc;
                    else
                        TxtUsuarioFinal.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[1].Text + "-" + lsTipoDoc + "-" + this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[3].Text;
                    TxtCantidadUsu.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[6].Text;
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) - Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                    dlTipoDemanda.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[15].Text; //rq009-17
                    dlTipoDemanda_SelectedIndexChanged(null, null);
                    ddlSector.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[12].Text; //rq009-17
                    ddlMercadoRel.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[16].Text; //rq009-17
                    ddlPuntoSalida.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[13].Text; //rq009-17
                    btnCrearUsu.Visible = false;
                    btnActualUsu.Visible = true;
                    TxtEquivaleKpcd.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[7].Text;
                    TdIndica.Visible = true;
                    ///TdIndica1.Visible = true;
                    btnActualUsu.Visible = true;
                    btnCrearUsu.Visible = false;
                    HdfCodigoVerUsr = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text;
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Problemas al Recuperar el Registro.! " + ex.Message, EnumTypeToastr.Error);
                }
            }
            if (e.CommandName.Equals("Eliminar"))
            {
                string[] lsNombreParametros = { "@P_codigo_cont_usr", "@P_accion", "@P_codigo_solicitud" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { hdfCodDatUsu.Value, "3", "0" };

                TxtCantidadUsu.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[6].Text; // 20200402 Ajuste //20200727 ajsute front-ned
                try
                {
                    hdfCodDatUsu.Value = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text;
                    lValorParametros[0] = hdfCodDatUsu.Value;
                    lValorParametros[2] = HdfNoSolicitud;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetModContRegUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                    {
                        ToastrEvent?.Invoke("Se presentó un Problema en la Eliminación de la Información del Usuario.!" + goInfo.mensaje_error, EnumTypeToastr.Error);
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) - Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                        lConexion.Cerrar();
                        Toastr.Success(this, "Registro Eliminado Correctamente.!");
                        CargarDatosUsu();
                    }
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Problemas al Eliminar el Registro.! " + ex.Message, EnumTypeToastr.Error);
                }
            }
        }

        /// <summary>
        /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnGrabarDef_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_solicitud","@P_ind_reestructuracion","@P_punta"
                                      };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar
                                        };
            string[] lValorParametros = { HdfNoSolicitud , "N",HdfPunta
                                    };

            var lblMensaje = new StringBuilder();

            try
            {
                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModContRegDef", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Append(lLector["error"] + "<br>");
                }
                lLector.Close();
                lConexion.Cerrar();

                if (lblMensaje.ToString() == "")
                {
                    btnCancelarM_Click(null, null);
                }
                else
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlConexion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DdlMercado.SelectedValue == "O")
            {
                trMayor3.Visible = true; //20200831 ajuste OTMM
                if (ddlConexion.SelectedValue == "S")
                {
                    //trMayor3.Visible = true; //20200831 ajuste OTMM
                    tdCentro1.Visible = true; //20200831 ajuste OTMM
                    //tdCentro2.Visible = true; //20200831 ajuste OTMM
                    trMayor4.Visible = false;
                    ddlMercadoRelevante.SelectedValue = "0";
                }
                else
                {
                    //trMayor3.Visible = false;  //20200831 ajuste OTMM
                    tdCentro1.Visible = false; //20200831 ajuste OTMM
                    //tdCentro2.Visible = false; //20200831 ajuste OTMM
                    trMayor4.Visible = true;
                    ddlDeptoPuntoSal.SelectedValue = "0";
                    ddlDeptoPuntoSal_SelectedIndexChanged(null, null);
                }
            }
            //2020831 ajsute OTMM
            else
            {
                trMayor3.Visible = false;
                trMayor4.Visible = false;
            }

        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='t_contrato_modificacion' and llave_registro='codigo_verif_contrato=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_verif_contrato=" + lscodigo_registro;
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_contrato_modificacion";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_contrato_modificacion", lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20170814 rq036-17
        protected void DdlBusMercado_SelectedIndexChanged(object sender, EventArgs e)
        {
            DdlBusProducto.Items.Clear();
            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "";
            lItem.Text = "Seleccione";
            DdlBusProducto.Items.Add(lItem);
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = "G";
            lItem1.Text = "Suministro de gas";
            DdlBusProducto.Items.Add(lItem1);
            System.Web.UI.WebControls.ListItem lItem2 = new System.Web.UI.WebControls.ListItem();
            lItem2.Value = "T";
            lItem2.Text = "Capacidad de transporte";
            DdlBusProducto.Items.Add(lItem2);
            if (DdlBusMercado.SelectedValue == "" || DdlBusMercado.SelectedValue == "O")
            {
                System.Web.UI.WebControls.ListItem lItem3 = new System.Web.UI.WebControls.ListItem();
                lItem3.Value = "A";
                lItem3.Text = "Suministro y transporte";
                DdlBusProducto.Items.Add(lItem3);
            }
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected void trae_usr_fin(string lsModif, string lsVerif)
        {
            string[] lsNombreParametros = { "@P_codigo_modif","@P_codigo_verif"
                                      };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int
                                        };
            string[] lValorParametros = { lsModif, lsVerif
                                    };
            try
            {
                lConexion.Abrir();
                DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetUsrFinMod", lsNombreParametros, lTipoparametros, lValorParametros);
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Se presentó un error al recuperar los usuarios finales. " + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgReestructura_EditCommand(object source, DataGridCommandEventArgs e)
        {
            HdfIdRegistro = e.Item.Cells[2].Text;
            HdfConsec = e.Item.Cells[3].Text;
            color_label("R"); //20190425 rq022-19 ajsute modificacion
            consultar("C");
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        /// //20190425 rq022-19 ajsute modificaciones
        protected void color_label(string lsColor)
        {
            if (lsColor == "N")
            {
                LblOperadorC.ForeColor = System.Drawing.Color.Black;
                LblOperadorV.ForeColor = System.Drawing.Color.Black;
                LblFechaNeg.ForeColor = System.Drawing.Color.Black;
                LblFechaSus.ForeColor = System.Drawing.Color.Black;
                LblContratoDef.ForeColor = System.Drawing.Color.Black;
                LblContratoVar.ForeColor = System.Drawing.Color.Black;
                LblFuente.ForeColor = System.Drawing.Color.Black;
                LblConectadoSnt.ForeColor = System.Drawing.Color.Black;
                LblEntregaBoca.ForeColor = System.Drawing.Color.Black;
                LblPunto.ForeColor = System.Drawing.Color.Black;
                LblModalidad.ForeColor = System.Drawing.Color.Black;
                LblPeriodo.ForeColor = System.Drawing.Color.Black;
                LblNoAños.ForeColor = System.Drawing.Color.Black;
                LblFechaIni.ForeColor = System.Drawing.Color.Black;
                LblFechaFin.ForeColor = System.Drawing.Color.Black;
                LblCantidad.ForeColor = System.Drawing.Color.Black;
                LblPrecio.ForeColor = System.Drawing.Color.Black;
                LblSentido.ForeColor = System.Drawing.Color.Black;
                LblPresion.ForeColor = System.Drawing.Color.Black;
                LblGarantia.ForeColor = System.Drawing.Color.Black;
                LblValorGrtia.ForeColor = System.Drawing.Color.Black;
                LblFechaPagoGrtia.ForeColor = System.Drawing.Color.Black;
                LblSectorCon.ForeColor = System.Drawing.Color.Black;
                LblUsuaroNoReg.ForeColor = System.Drawing.Color.Black;
                LblRutaMay.ForeColor = System.Drawing.Color.Black;
                LblDepto.ForeColor = System.Drawing.Color.Black;
                LblMunicipio.ForeColor = System.Drawing.Color.Black;
                LblCentro.ForeColor = System.Drawing.Color.Black;
                LblMercadoRel.ForeColor = System.Drawing.Color.Black;
                LblTrm.ForeColor = System.Drawing.Color.Black;
                LblObsTrm.ForeColor = System.Drawing.Color.Black;
                LblMoneda.ForeColor = System.Drawing.Color.Black;
                trCambioUsrFin.Visible = false;
            }
            else
            {
                string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_consecutivo" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { HdfIdRegistro, HdfConsec };
                try
                {
                    lConexion.Abrir();
                    SqlDataReader lLector;
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetDatoModifCont", lsNombreParametros, lTipoparametros, lValorParametros);
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        if (lLector["mod_operador_c"].ToString() == "S")
                            LblOperadorC.ForeColor = System.Drawing.Color.Red;
                        else
                            LblOperadorC.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_operador_v"].ToString() == "S")
                            LblOperadorV.ForeColor = System.Drawing.Color.Red;
                        else
                            LblOperadorV.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_fecha_neg"].ToString() == "S")
                            LblFechaNeg.ForeColor = System.Drawing.Color.Red;
                        else
                            LblFechaNeg.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_fecha_suscripcion"].ToString() == "S")
                            LblFechaSus.ForeColor = System.Drawing.Color.Red;
                        else
                            LblFechaSus.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_contrato_definitivo"].ToString() == "S")
                            LblContratoDef.ForeColor = System.Drawing.Color.Red;
                        else
                            LblContratoDef.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_ind_contrato_var"].ToString() == "S")
                            LblContratoVar.ForeColor = System.Drawing.Color.Red;
                        else
                            LblContratoVar.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_codigo_fuente"].ToString() == "S")
                            LblFuente.ForeColor = System.Drawing.Color.Red;
                        else
                            LblFuente.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_conectado_snt"].ToString() == "S")
                            LblConectadoSnt.ForeColor = System.Drawing.Color.Red;
                        else
                            LblConectadoSnt.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_ent_boca_pozo"].ToString() == "S")
                            LblEntregaBoca.ForeColor = System.Drawing.Color.Red;
                        else
                            LblEntregaBoca.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_codigo_punto_entrega"].ToString() == "S")
                            LblPunto.ForeColor = System.Drawing.Color.Red;
                        else
                            LblPunto.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_codigo_modalidad"].ToString() == "S")
                            LblModalidad.ForeColor = System.Drawing.Color.Red;
                        else
                            LblModalidad.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_codigo_periodo"].ToString() == "S")
                            LblPeriodo.ForeColor = System.Drawing.Color.Red;
                        else
                            LblPeriodo.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_no_años"].ToString() == "S")
                            LblNoAños.ForeColor = System.Drawing.Color.Red;
                        else
                            LblNoAños.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_fecha_ini"].ToString() == "S")
                            LblFechaIni.ForeColor = System.Drawing.Color.Red;
                        else
                            LblFechaIni.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_fecha_fin"].ToString() == "S")
                            LblFechaFin.ForeColor = System.Drawing.Color.Red;
                        else
                            LblFechaFin.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_cantidad"].ToString() == "S")
                            LblCantidad.ForeColor = System.Drawing.Color.Red;
                        else
                            LblCantidad.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_precio"].ToString() == "S")
                            LblPrecio.ForeColor = System.Drawing.Color.Red;
                        else
                            LblPrecio.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_sentido_flujo"].ToString() == "S")
                            LblSentido.ForeColor = System.Drawing.Color.Red;
                        else
                            LblSentido.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_presion_punto_fin"].ToString() == "S")
                            LblPresion.ForeColor = System.Drawing.Color.Red;
                        else
                            LblPresion.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_tipo_garantia"].ToString() == "S")
                            LblGarantia.ForeColor = System.Drawing.Color.Red;
                        else
                            LblGarantia.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_valor_garantia"].ToString() == "S")
                            LblValorGrtia.ForeColor = System.Drawing.Color.Red;
                        else
                            LblValorGrtia.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_fecha_pago"].ToString() == "S")
                            LblFechaPagoGrtia.ForeColor = System.Drawing.Color.Red;
                        else
                            LblFechaPagoGrtia.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_sector_consumo"].ToString() == "S")
                            LblSectorCon.ForeColor = System.Drawing.Color.Red;
                        else
                            LblSectorCon.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_conectado_snt"].ToString() == "S") //20210707 trm moneda
                            LblUsuaroNoReg.ForeColor = System.Drawing.Color.Red;
                        else
                            LblUsuaroNoReg.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_codigo_ruta_may"].ToString() == "S")
                            LblRutaMay.ForeColor = System.Drawing.Color.Red;
                        else
                            LblRutaMay.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_codigo_depto"].ToString() == "S")
                            LblDepto.ForeColor = System.Drawing.Color.Red;
                        else
                            LblDepto.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_codigo_municipio"].ToString() == "S")
                            LblMunicipio.ForeColor = System.Drawing.Color.Red;
                        else
                            LblMunicipio.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_codigo_centro_poblado"].ToString() == "S")
                            LblCentro.ForeColor = System.Drawing.Color.Red;
                        else
                            LblCentro.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_mercado_relevante"].ToString() == "S")
                            LblMercadoRel.ForeColor = System.Drawing.Color.Red;
                        else
                            LblMercadoRel.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_usr_final"].ToString() == "S")
                            trCambioUsrFin.Visible = true;
                        else
                            trCambioUsrFin.Visible = false;
                        //20210707 trm moneda
                        if (lLector["mod_codigo_trm"].ToString() == "S")
                            LblTrm.ForeColor = System.Drawing.Color.Red;
                        else
                            LblTrm.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_observacion_trm"].ToString() == "S")
                            LblObsTrm.ForeColor = System.Drawing.Color.Red;
                        else
                            LblObsTrm.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_tipo_moneda"].ToString() == "S")
                            LblMoneda.ForeColor = System.Drawing.Color.Red;
                        else
                            LblMoneda.ForeColor = System.Drawing.Color.Black;
                        //20210707 fin trm moneda
                    }
                    else
                    {
                        LblOperadorC.ForeColor = System.Drawing.Color.Black;
                        LblOperadorV.ForeColor = System.Drawing.Color.Black;
                        LblFechaNeg.ForeColor = System.Drawing.Color.Black;
                        LblFechaSus.ForeColor = System.Drawing.Color.Black;
                        LblContratoDef.ForeColor = System.Drawing.Color.Black;
                        LblContratoVar.ForeColor = System.Drawing.Color.Black;
                        LblFuente.ForeColor = System.Drawing.Color.Black;
                        LblConectadoSnt.ForeColor = System.Drawing.Color.Black;
                        LblEntregaBoca.ForeColor = System.Drawing.Color.Black;
                        LblPunto.ForeColor = System.Drawing.Color.Black;
                        LblModalidad.ForeColor = System.Drawing.Color.Black;
                        LblPeriodo.ForeColor = System.Drawing.Color.Black;
                        LblNoAños.ForeColor = System.Drawing.Color.Black;
                        LblFechaIni.ForeColor = System.Drawing.Color.Black;
                        LblFechaFin.ForeColor = System.Drawing.Color.Black;
                        LblCantidad.ForeColor = System.Drawing.Color.Black;
                        LblPrecio.ForeColor = System.Drawing.Color.Black;
                        LblSentido.ForeColor = System.Drawing.Color.Black;
                        LblPresion.ForeColor = System.Drawing.Color.Black;
                        LblGarantia.ForeColor = System.Drawing.Color.Black;
                        LblValorGrtia.ForeColor = System.Drawing.Color.Black;
                        LblFechaPagoGrtia.ForeColor = System.Drawing.Color.Black;
                        LblSectorCon.ForeColor = System.Drawing.Color.Black;
                        LblUsuaroNoReg.ForeColor = System.Drawing.Color.Black;
                        LblRutaMay.ForeColor = System.Drawing.Color.Black;
                        LblDepto.ForeColor = System.Drawing.Color.Black;
                        LblMunicipio.ForeColor = System.Drawing.Color.Black;
                        LblCentro.ForeColor = System.Drawing.Color.Black;
                        LblMercadoRel.ForeColor = System.Drawing.Color.Black;
                        trCambioUsrFin.Visible = false;
                        LblTrm.ForeColor = System.Drawing.Color.Black; //20210707 trm moneda
                        LblObsTrm.ForeColor = System.Drawing.Color.Black; //20210707 trm moneda
                        LblMoneda.ForeColor = System.Drawing.Color.Black; //20210707 trm moneda
                    }
                    lLector.Close();
                    lConexion.Cerrar();
                }
                catch (Exception)
                {
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void lkbExcel_Click(object sender, EventArgs e)
        {
            try
            {
                string lsNombreArchivo = Session["login"] + "InfExcelReg" + DateTime.Now + ".xls";
                StringBuilder lsb = new StringBuilder();
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                dtgConsultaExcel.Visible = true;
                lpagina.EnableEventValidation = false;
                lpagina.Controls.Add(lform);
                dtgConsultaExcel.EnableViewState = false;
                lform.Controls.Add(dtgConsultaExcel);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = System.Text.Encoding.Default;

                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + "Consulta de contratos para modificar" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write(lsb.ToString());
                dtgConsultaExcel.Visible = false;
                Response.End();
                Response.Flush();
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Problemas al Consultar los Registros. " + ex.Message, EnumTypeToastr.Error);
            }
        }
    }
}