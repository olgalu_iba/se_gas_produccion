﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace UsersControls.Contratos.Registro_Contratos.Modificacion_Contratos
{
    public partial class WucCargaModContratos : UserControl
    {
        #region EventHandler

        /// <summary>
        /// Delegado para el manejo de los modals
        /// </summary>
        public delegate void EventHandlerModal(string id, string insideId, EnumTypeModal typeModal, Modal.Size size = Modal.Size.Large);

        /// <summary>
        /// EventHandler para la selección de los los modals
        /// </summary>
        public event EventHandlerModal ModalEvent;

        /// <summary>
        /// Delegado para el manejo de los toastr
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        /// EventHandler para la selección de los toastr
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        /// Delegado para el manejo del log de cargue de archivos
        /// </summary>
        public delegate void EventHandlerLogCargaArchivo(StringBuilder message);

        /// <summary>
        /// EventHandler para el log de cargue de archivos
        /// </summary>
        public event EventHandlerLogCargaArchivo LogCargaArchivoEvent;

        /// <summary>
        /// Delegado para la selección de los botones  
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get
            {
                return ViewState["NameController"] != null &&
                       !string.IsNullOrEmpty(ViewState["NameController"].ToString())
                    ? ViewState["NameController"].ToString()
                    : string.Empty;
            }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        /// <summary>
        /// Propiedad que establece si la sección 1 se ve 
        /// </summary>
        public bool Tr01
        {
            get { return (bool?)ViewState["Tr01"] ?? false; }
            set { ViewState["Tr01"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si la sección 2 se ve 
        /// </summary>
        public bool Tr02
        {
            get { return (bool?)ViewState["Tr02"] ?? false; }
            set { ViewState["Tr02"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si la sección 3 se ve 
        /// </summary>
        public bool Tr03
        {
            get { return (bool?)ViewState["Tr03"] ?? false; }
            set { ViewState["Tr03"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si la sección 4 se ve 
        /// </summary>
        public bool Tr04
        {
            get { return (bool?)ViewState["Tr04"] ?? false; }
            set { ViewState["Tr04"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si la sección 5 se ve 
        /// </summary>
        public bool Tr05
        {
            get { return (bool?)ViewState["Tr05"] ?? false; }
            set { ViewState["Tr05"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si la sección 6 se ve 
        /// </summary>
        public bool Tr06
        {
            get { return (bool?)ViewState["Tr06"] ?? false; }
            set { ViewState["Tr06"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si la sección 7 se ve 
        /// </summary>
        public bool Tr07
        {
            get { return (bool?)ViewState["Tr07"] ?? false; }
            set { ViewState["Tr07"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si la sección 8 se ve 
        /// </summary>
        public bool Tr08
        {
            get { return (bool?)ViewState["Tr08"] ?? false; }
            set { ViewState["Tr08"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si la sección 9 se ve 
        /// </summary>
        public bool Tr09
        {
            get { return (bool?)ViewState["Tr09"] ?? false; }
            set { ViewState["Tr09"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si la sección 10 se ve 
        /// </summary>
        public bool Tr10
        {
            get { return (bool?)ViewState["Tr10"] ?? false; }
            set { ViewState["Tr10"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si la sección 11 se ve 
        /// </summary>
        public bool Tr11
        {
            get { return (bool?)ViewState["Tr11"] ?? false; }
            set { ViewState["Tr11"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si la sección 12 se ve 
        /// </summary>
        public bool Tr12
        {
            get { return (bool?)ViewState["Tr12"] ?? false; }
            set { ViewState["Tr12"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si la sección 13 se ve 
        /// </summary>
        public bool Tr13
        {
            get { return (bool?)ViewState["Tr13"] ?? false; }
            set { ViewState["Tr13"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si la sección 14 se ve 
        /// </summary>
        public bool Tr14
        {
            get { return (bool?)ViewState["Tr14"] ?? false; }
            set { ViewState["Tr14"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si la sección 15 se ve 
        /// </summary>
        public bool Tr15
        {
            get { return (bool?)ViewState["Tr15"] ?? false; }
            set { ViewState["Tr15"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si la sección 16 se ve 
        /// </summary>
        public bool Tr16
        {
            get { return (bool?)ViewState["Tr16"] ?? false; }
            set { ViewState["Tr16"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si la sección 17 se ve 
        /// </summary>
        public bool Tr17
        {
            get { return (bool?)ViewState["Tr17"] ?? false; }
            set { ViewState["Tr17"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si la sección 18 se ve 
        /// </summary>
        public bool Tr18
        {
            get { return (bool?)ViewState["Tr18"] ?? false; }
            set { ViewState["Tr18"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si la sección 19 se ve 
        /// </summary>
        public bool Tr19
        {
            get { return (bool?)ViewState["Tr19"] ?? false; }
            set { ViewState["Tr19"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si la sección 20 se ve 
        /// </summary>
        public bool Tr20
        {
            get { return (bool?)ViewState["Tr20"] ?? false; }
            set { ViewState["Tr20"] = value; }
        }

        #endregion Propiedades

        private InfoSessionVO goInfo = null;
        private clConexion lConexion = null;
        private string strRutaCarga;
        private string strRutaFTP;
        private SqlDataReader lLector;
        private string sRutaArc = ConfigurationManager.AppSettings["rutaModif"];

        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
            strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            EnumBotones[] botones = { EnumBotones.Cargue };
            ButtonsEvent?.Invoke(botones, null, NameController);
            //Se establece que el controlador ya se inicializo    
            Inicializado = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void BtnCargar_Click(object sender, EventArgs e)
        {
            try
            {
                var lsRutaArchivo = "";
                var lsRutaArchivoU = "";
                var lsNombre = "";
                var lsNombreU = "";
                string[] lsErrores = { "", "" };
                var lsCadenaArchivo = new StringBuilder();
                var oCargaOK = false;
                SqlDataReader lLector;
                var lComando = new SqlCommand();
                var liNumeroParametros = 0;
                lConexion = new clConexion(goInfo);
                string[] lsNombreParametrosO = { "@P_archivo", "@P_archivoUsuarios", "@P_codigo_operador", "@P_ruta_ftp", "@P_codigo_usuario" };
                SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
                object[] lValorParametrosO = { "", "", goInfo.cod_comisionista, strRutaFTP, goInfo.codigo_usuario };

                lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;
                try
                {
                    lsRutaArchivo = strRutaCarga + lsNombre;
                    FuArchivo.SaveAs(lsRutaArchivo);
                }
                catch (Exception ex)
                {
                    throw new Exception($"{HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivoContratos", CultureInfo.CurrentCulture)}: {ex.Message}");
                }

                if (FuArchivoUsuarios.FileName != "")
                {
                    lsNombreU = DateTime.Now.Millisecond + FuArchivoUsuarios.FileName;
                    try
                    {
                        lsRutaArchivoU = strRutaCarga + lsNombreU;
                        FuArchivoUsuarios.SaveAs(lsRutaArchivoU);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"{HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivoUsuarios", CultureInfo.CurrentCulture)}: {ex.Message}");
                    }
                }

                try
                {
                    // Realiza las Validaciones de los Archivos
                    if (FuArchivo.FileName != "")
                        lsErrores = ValidarArchivo(lsRutaArchivo);
                    else
                    {
                        ToastrEvent?.Invoke(HttpContext.GetGlobalResourceObject("AppResources", "ArchivoEjecucionContratosRequerido", CultureInfo.CurrentCulture)?.ToString(), EnumTypeToastr.Error);
                        return;
                    }

                    if (lsErrores[0] == "")
                    {
                        if (FuArchivoUsuarios.FileName != "")
                            lsErrores = ValidarArchivoUsuarios(lsRutaArchivoU);
                    }

                    if (lsErrores[0] == "")
                    {
                        oCargaOK = DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]);
                        if (FuArchivoUsuarios.FileName != "")
                            oCargaOK = DelegadaBase.Servicios.put_archivo(lsRutaArchivoU, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombreU, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]);

                        if (oCargaOK)
                        {
                            AbrirTr01(false);
                            AbrirTr02(false);
                            AbrirTr03(false);
                            AbrirTr04(false);
                            AbrirTr05(false);
                            AbrirTr06(false);
                            AbrirTr07(false);
                            AbrirTr08(false);
                            AbrirTr09(false);
                            AbrirTr10(false);
                            AbrirTr11(false);
                            AbrirTr12(false);
                            AbrirTr13(false);
                            AbrirTr14(false);
                            AbrirTr15(false);
                            AbrirTr16(false);
                            AbrirTr17(false);
                            AbrirTr18(false);
                            AbrirTr19(false);
                            AbrirTr20(false);
                            var liConta = 0;

                            lValorParametrosO[0] = lsNombre;
                            lValorParametrosO[1] = lsNombreU;
                            lConexion.Abrir();
                            lComando.Connection = lConexion.gObjConexion;
                            lComando.CommandType = CommandType.StoredProcedure;
                            lComando.CommandText = "pa_ValidaPlanoModContReg";
                            lComando.CommandTimeout = 3600;
                            if (lsNombreParametrosO != null)
                            {
                                for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                                {
                                    lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                                }
                            }

                            lLector = lComando.ExecuteReader();
                            if (lLector.HasRows)
                            {
                                while (lLector.Read())
                                {
                                    if (lLector["ind_error"].ToString() == "S")
                                        lsCadenaArchivo.Append(lLector["Mensaje"] + "<br>");
                                    else
                                    {
                                        liConta++;
                                        switch (liConta)
                                        {
                                            case 1:
                                                AbrirTr01(true);
                                                LblContrato01.Text = lLector["contrato_definitivo"].ToString();
                                                break;
                                            case 2:
                                                AbrirTr02(true);
                                                LblContrato02.Text = lLector["contrato_definitivo"].ToString();
                                                break;
                                            case 3:
                                                AbrirTr03(true);
                                                LblContrato03.Text = lLector["contrato_definitivo"].ToString();
                                                break;
                                            case 4:
                                                AbrirTr04(true);
                                                LblContrato04.Text = lLector["contrato_definitivo"].ToString();
                                                break;
                                            case 5:
                                                AbrirTr05(true);
                                                LblContrato05.Text = lLector["contrato_definitivo"].ToString();
                                                break;
                                            case 6:
                                                AbrirTr06(true);
                                                LblContrato06.Text = lLector["contrato_definitivo"].ToString();
                                                break;
                                            case 7:
                                                AbrirTr07(true);
                                                LblContrato07.Text = lLector["contrato_definitivo"].ToString();
                                                break;
                                            case 8:
                                                AbrirTr08(true);
                                                LblContrato08.Text = lLector["contrato_definitivo"].ToString();
                                                break;
                                            case 9:
                                                AbrirTr09(true);
                                                LblContrato09.Text = lLector["contrato_definitivo"].ToString();
                                                break;
                                            case 10:
                                                AbrirTr10(true);
                                                LblContrato10.Text = lLector["contrato_definitivo"].ToString();
                                                break;
                                            case 11:
                                                AbrirTr11(true);
                                                LblContrato11.Text = lLector["contrato_definitivo"].ToString();
                                                break;
                                            case 12:
                                                AbrirTr12(true);
                                                LblContrato12.Text = lLector["contrato_definitivo"].ToString();
                                                break;
                                            case 13:
                                                AbrirTr13(true);
                                                LblContrato13.Text = lLector["contrato_definitivo"].ToString();
                                                break;
                                            case 14:
                                                AbrirTr14(true);
                                                LblContrato14.Text = lLector["contrato_definitivo"].ToString();
                                                break;
                                            case 15:
                                                AbrirTr15(true);
                                                LblContrato15.Text = lLector["contrato_definitivo"].ToString();
                                                break;
                                            case 16:
                                                AbrirTr16(true);
                                                LblContrato16.Text = lLector["contrato_definitivo"].ToString();
                                                break;
                                            case 17:
                                                AbrirTr17(true);
                                                LblContrato17.Text = lLector["contrato_definitivo"].ToString();
                                                break;
                                            case 18:
                                                AbrirTr18(true);
                                                LblContrato18.Text = lLector["contrato_definitivo"].ToString();
                                                break;
                                            case 19:
                                                AbrirTr19(true);
                                                LblContrato19.Text = lLector["contrato_definitivo"].ToString();
                                                break;
                                            case 20:
                                                AbrirTr20(true);
                                                LblContrato20.Text = lLector["contrato_definitivo"].ToString();
                                                break;
                                        }
                                    }
                                }

                                ModalEvent?.Invoke(mdlSolicitud.ID, mdlSolicitudInside.ID, liConta > 0 ? EnumTypeModal.Abrir : EnumTypeModal.Cerrar);
                            }
                            else
                            {
                                try
                                {
                                    lConexion.Abrir();
                                    lComando.Connection = lConexion.gObjConexion;
                                    lComando.CommandType = CommandType.StoredProcedure;
                                    lComando.Parameters.Clear();
                                    lComando.CommandText = "pa_ActPlanoModContReg";
                                    lComando.CommandTimeout = 3600;
                                    //lLector = lComando.ExecuteNonQuery();
                                    lLector = lComando.ExecuteReader();
                                    lsCadenaArchivo.Append(HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga", CultureInfo.CurrentCulture));
                                    if (lLector.HasRows)
                                        while (lLector.Read())
                                            lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");

                                    if (lsCadenaArchivo.ToString().Contains(HttpContext.GetGlobalResourceObject("AppResources", "EstadoPendienteContraparte", CultureInfo.CurrentCulture)?.ToString()) || lsCadenaArchivo.ToString().Contains(HttpContext.GetGlobalResourceObject("AppResources", "EstadoPendienteGestor", CultureInfo.CurrentCulture)?.ToString()) || lsCadenaArchivo.ToString().Contains(HttpContext.GetGlobalResourceObject("AppResources", "EstadoPendienteCorrecion", CultureInfo.CurrentCulture)?.ToString()) || lsCadenaArchivo.ToString().Contains(" pendiente otras aprobaciones"))
                                    {
                                        ToastrEvent?.Invoke(lsCadenaArchivo.ToString(), EnumTypeToastr.Success);
                                        return;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaArchivo.Append($"{HttpContext.GetGlobalResourceObject("AppResources", "ErrorModificaciones", CultureInfo.CurrentCulture)}: { ex.Message}");
                                }
                            }
                            lLector.Close();
                            lLector.Dispose();
                            lConexion.Cerrar();
                        }
                        else
                        {
                            ToastrEvent?.Invoke(HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString(), EnumTypeToastr.Error);
                        }
                    }
                    else
                    {
                        lsCadenaArchivo.Append(lsErrores[0]);
                        DelegadaBase.Servicios.registrarProceso(goInfo, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCarga", CultureInfo.CurrentCulture)?.ToString(), "Usuario : " + goInfo.nombre);
                    }
                    //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                    LogCargaArchivoEvent?.Invoke(lsCadenaArchivo);
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke(HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message, EnumTypeToastr.Error);
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Validacion del Archivo de Contratos
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            decimal ldValor = 0;
            var liTotalRegistros = 0;
            Int64 liValor = 0;
            DateTime ldFecha;
            string[] lsFecha;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 42) //20210707 trm moneda
                    ) // Fuente o campo MP 20160603  //20170816 rq036-17  //20171130 rq026-17
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 42 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; // Fuente o campo MP 20160603  //20170816 rq036-17  //20171130 rq026-17 //trm moneda
                    }
                    else
                    {
                        /// Validar Operacion
                        if (oArregloLinea.GetValue(0).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la causa de modificación {" + oArregloLinea.GetValue(0) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(0).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la cuasa de modificación  {" +
                                                  oArregloLinea.GetValue(0) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            }
                        }

                        /// Validar Operacion
                        if (oArregloLinea.GetValue(1).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Id de registro {" +
                                              oArregloLinea.GetValue(1) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(1).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Id de registro {" +
                                                  oArregloLinea.GetValue(1) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            }
                        }

                        /// Validar Operacion
                        if (oArregloLinea.GetValue(2).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el consecutivo {" +
                                              oArregloLinea.GetValue(2) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(2).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el consecutivo {" +
                                                  oArregloLinea.GetValue(2) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            }
                        }

                        /// Validar Operacion
                        if (oArregloLinea.GetValue(3).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el código del comprador {" +
                                              oArregloLinea.GetValue(3) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(3).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el código del comprador {" +
                                                  oArregloLinea.GetValue(3) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            }
                        }

                        /// Validar Operacion
                        if (oArregloLinea.GetValue(4).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el código del vendedor {" +
                                              oArregloLinea.GetValue(4) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(4).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el consecutivo {" +
                                                  oArregloLinea.GetValue(4) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            }
                        }

                        /// Validar No. Contrato
                        if (oArregloLinea.GetValue(5).ToString() != "C" && oArregloLinea.GetValue(5).ToString() != "V")
                            lsCadenaErrores = lsCadenaErrores + "La punta {" + oArregloLinea.GetValue(5) +
                                              "}, es inválida en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano<br>";

                        /// Valida Fecha de Suscripcion
                        if (oArregloLinea.GetValue(6).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el contrato definitivo  {" +
                                              oArregloLinea.GetValue(6) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano<br>";


                        /// Valida Fecha de Suscripcion
                        if (oArregloLinea.GetValue(7).ToString().Length != 10)
                            lsCadenaErrores = lsCadenaErrores + "La Fecha de negociación  {" +
                                              oArregloLinea.GetValue(7) +
                                              "}, debe tener 10 caracteres en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(7).ToString());
                                lsFecha = oArregloLinea.GetValue(7).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores =
                                        lsCadenaErrores + "Formato Inválido en la Fecha de negociación {" +
                                        oArregloLinea.GetValue(7) + "},debe ser {YYYY/MM/DD}, en la Línea No. " +
                                        liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La Fecha de negociación {" +
                                                  oArregloLinea.GetValue(7) + "} es inválida, en la Línea No. " +
                                                  liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }

                        /// Validar Codigo Punto de Entega
                        if (oArregloLinea.GetValue(8).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar hora de negociación  {" +
                                              oArregloLinea.GetValue(8) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano<br>";
                        /// Valida Fecha de Suscripcion
                        if (oArregloLinea.GetValue(9).ToString().Length != 10)
                            lsCadenaErrores = lsCadenaErrores + "La Fecha de suscripción  {" +
                                              oArregloLinea.GetValue(9) +
                                              "}, debe tener 10 caracteres en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(9).ToString());
                                lsFecha = oArregloLinea.GetValue(9).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores =
                                        lsCadenaErrores + "Formato Inválido en la Fecha de suscripción {" +
                                        oArregloLinea.GetValue(9) + "},debe ser {YYYY/MM/DD}, en la Línea No. " +
                                        liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La Fecha de suscripción {" +
                                                  oArregloLinea.GetValue(9) + "} es inválida, en la Línea No. " +
                                                  liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }

                        /// Validar No. Contrato
                        if (oArregloLinea.GetValue(10).ToString() != "S" &&
                            oArregloLinea.GetValue(10).ToString() != "N")
                            lsCadenaErrores = lsCadenaErrores + "EL indicador de conectado al SNT {" +
                                              oArregloLinea.GetValue(10) + "}, es inválido en la Línea No. " +
                                              liNumeroLinea + " del Archivo Plano<br>";

                        /// Validar No. Contrato
                        if (oArregloLinea.GetValue(11).ToString() != "S" &&
                            oArregloLinea.GetValue(11).ToString() != "N")
                            lsCadenaErrores = lsCadenaErrores + "EL indicador de entrega en boca de pozo  {" +
                                              oArregloLinea.GetValue(11) + "}, es inválido en la Línea No. " +
                                              liNumeroLinea + " del Archivo Plano<br>";
                        /// Validar No. Contrato
                        if (oArregloLinea.GetValue(12).ToString() != "S" &&
                            oArregloLinea.GetValue(12).ToString() != "N")
                            lsCadenaErrores = lsCadenaErrores + "EL indicador de contrato variable  {" +
                                              oArregloLinea.GetValue(12) + "}, es inválido en la Línea No. " +
                                              liNumeroLinea + " del Archivo Plano<br>";
                        /// Validar No. Contrato
                        if (oArregloLinea.GetValue(13).ToString() != "0" &&
                            oArregloLinea.GetValue(13).ToString().Length != 8)
                            lsCadenaErrores = lsCadenaErrores + "El código del centro poblado  {" +
                                              oArregloLinea.GetValue(13) + "}, es inválido en la Línea No. " +
                                              liNumeroLinea + " del Archivo Plano<br>";
                        /// Validar Codigo Punto de Entega
                        if (oArregloLinea.GetValue(14).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Punto de Entrega / Tramo {" +
                                              oArregloLinea.GetValue(14) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(14).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Punto de Entrega / Tramo {" +
                                                  oArregloLinea.GetValue(14) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            }
                        }

                        /// Validar Codigo Modalidad Contractual
                        if (oArregloLinea.GetValue(15).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la Modalidad Contractual {" +
                                              oArregloLinea.GetValue(15) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(15).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Modalidad Contractual {" +
                                                  oArregloLinea.GetValue(15) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            }
                        }

                        /// Validar Codigo Modalidad Contractual
                        if (oArregloLinea.GetValue(16).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el periodo de entrega {" +
                                              oArregloLinea.GetValue(16) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(16).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el periodo de entrega {" +
                                                  oArregloLinea.GetValue(16) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            }
                        }

                        /// Validar Codigo Modalidad Contractual
                        if (oArregloLinea.GetValue(17).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el número de años {" +
                                              oArregloLinea.GetValue(17) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(17).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el número de años {" +
                                                  oArregloLinea.GetValue(17) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            }
                        }

                        /// Valida Fecha de Suscripcion
                        if (oArregloLinea.GetValue(18).ToString().Length != 10)
                            lsCadenaErrores = lsCadenaErrores + "La Fecha inicial  {" + oArregloLinea.GetValue(18) +
                                              "} debe tener 10 caracteres, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(18).ToString());
                                lsFecha = oArregloLinea.GetValue(18).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "Formato Inválido en la Fecha inicial {" +
                                                      oArregloLinea.GetValue(18) +
                                                      "},debe ser {YYYY/MM/DD}, en la Línea No. " + liNumeroLinea +
                                                      " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La Fecha inicial {" + oArregloLinea.GetValue(18) +
                                                  "} es inválido, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            }
                        }

                        /// Validar Codigo Punto de Entega
                        if (oArregloLinea.GetValue(19).ToString().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar hora inicial  {" +
                                              oArregloLinea.GetValue(19) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano<br>";
                        /// Valida Fecha de Suscripcion
                        if (oArregloLinea.GetValue(20).ToString().Length != 10)
                            lsCadenaErrores = lsCadenaErrores + "La Fecha final  {" + oArregloLinea.GetValue(20) +
                                              "}, debe tener 10 caracteres en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(20).ToString());
                                lsFecha = oArregloLinea.GetValue(20).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "Formato Inválido en la Fecha final {" +
                                                      oArregloLinea.GetValue(20) +
                                                      "},debe ser {YYYY/MM/DD}, en la Línea No. " + liNumeroLinea +
                                                      " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La Fecha final {" + oArregloLinea.GetValue(20) +
                                                  "} es inválido, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            }
                        }

                        /// Validar Codigo Punto de Entega
                        if (oArregloLinea.GetValue(21).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar hora final  {" +
                                              oArregloLinea.GetValue(21) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano<br>";

                        /// Validar Cantiad
                        if (oArregloLinea.GetValue(22).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la Cantidad / Capacidad {" +
                                              oArregloLinea.GetValue(22) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(22).ToString().Trim());
                                if (liValor <= 0)
                                    lsCadenaErrores = lsCadenaErrores + "La Cantidad / Capacidad {" +
                                                      oArregloLinea.GetValue(22) +
                                                      "} no puder ser 0, en la Línea No. " + liNumeroLinea +
                                                      " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Cantidad / Capacidad {" +
                                                  oArregloLinea.GetValue(22) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            }
                        }

                        /// Validar Precio
                        if (oArregloLinea.GetValue(23).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Precio {" +
                                              oArregloLinea.GetValue(23) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldValor = Convert.ToDecimal(oArregloLinea.GetValue(23).ToString().Trim());
                                lsFecha = oArregloLinea.GetValue(23).ToString().Trim().Split('.');
                                if (lsFecha.Length > 1)
                                {
                                    if (lsFecha[1].Trim().Length > 2)
                                        lsCadenaErrores =
                                            lsCadenaErrores + "Valor Inválido en el Precio {" +
                                            oArregloLinea.GetValue(23) +
                                            "}, solo debe tener máximo 2 decímales, en la Línea No. " + liNumeroLinea +
                                            " del Archivo Plano<br>";
                                }

                                if (ldValor <= 0)
                                    lsCadenaErrores = lsCadenaErrores + "El Precio {" + oArregloLinea.GetValue(23) +
                                                      "} no puede ser 0, en la Línea No. " + liNumeroLinea +
                                                      " del Archivo Plano<br>";

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Precio {" +
                                                  oArregloLinea.GetValue(23) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            }
                        }

                        /// Validar Sentido del Flujo
                        if (oArregloLinea.GetValue(24).ToString().Trim().Length > 0)
                        {
                            if (oArregloLinea.GetValue(24).ToString().Trim() != "NORMAL" &&
                                oArregloLinea.GetValue(24).ToString().Trim() != "CONTRAFLUJO") //20210915
                                lsCadenaErrores = lsCadenaErrores +
                                                  "Valor Inválido en Sentido del Flujo, valores válidos {NORMAL o CONTRAFLUJO} {" +
                                                  oArregloLinea.GetValue(24) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                        }

                        /// Validar Presion Punto Terminacion
                        if (oArregloLinea.GetValue(25).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores +
                                              "Debe Ingresar Presión Punto Terminación (Valor por defecto 0) {" +
                                              oArregloLinea.GetValue(25) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano<br>";
                        else
                        {
                            ////////////////////////////////////////////////////////////////////////////////////////////////
                            //// Validacion Nueva sentido del flujo Requerimiento Ajuste presion transporte 20151006 ///////
                            ////////////////////////////////////////////////////////////////////////////////////////////////
                            string[] lsPresion;
                            try
                            {
                                if (oArregloLinea.GetValue(25).ToString().Trim().Length > 500)
                                    lsCadenaErrores = lsCadenaErrores +
                                                      "Longitud del Campo Presión Punto de Terminación {" +
                                                      oArregloLinea.GetValue(25) +
                                                      "} supera el máximo permitido (500 caracteres), en la Línea No. " +
                                                      liNumeroLinea + " del Archivo Plano<br>";
                                else
                                {
                                    lsPresion = oArregloLinea.GetValue(25).ToString().Trim().Split('-');
                                    foreach (string Presion in lsPresion)
                                    {
                                        try
                                        {
                                            ldValor = Convert.ToDecimal(Presion.Trim());
                                        }
                                        catch (Exception)
                                        {
                                            lsCadenaErrores =
                                                lsCadenaErrores + "Valor Inválido en Presión Punto de Terminación {" +
                                                Presion.Trim() + "}, en la Línea No. " + liNumeroLinea +
                                                " del Archivo Plano<br>";
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Presión Punto de Terminación {" +
                                                  oArregloLinea.GetValue(25) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            }

                            /// Valida Fuente o campo MP 20160603
                            if (oArregloLinea.GetValue(26).ToString().Trim().Length <= 0)
                                lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el código de la Fuente {" +
                                                  oArregloLinea.GetValue(26) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            else
                            {
                                try
                                {
                                    liValor = Convert.ToInt16(oArregloLinea.GetValue(26).ToString().Trim());
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Fuente {" +
                                                      oArregloLinea.GetValue(26) + "}, en la Línea No. " +
                                                      liNumeroLinea + " del Archivo Plano<br>";
                                }
                            }

                            /// Valida Fuente o campo MP 20160603
                            if (oArregloLinea.GetValue(28).ToString().Trim().Length <= 0)
                                lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el valor de la garantía {" +
                                                  oArregloLinea.GetValue(28) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            else
                            {
                                try
                                {
                                    ldValor = Convert.ToDecimal(oArregloLinea.GetValue(28).ToString().Trim());
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el valor de la garantía {" +
                                                      oArregloLinea.GetValue(28) + "}, en la Línea No. " +
                                                      liNumeroLinea + " del Archivo Plano<br>";
                                }
                            }

                            if (oArregloLinea.GetValue(29).ToString().Length > 0)
                                try
                                {
                                    if (oArregloLinea.GetValue(29).ToString().Length != 10)
                                        lsCadenaErrores =
                                            lsCadenaErrores + "La Fecha de pago {" + oArregloLinea.GetValue(29) +
                                            "}, debe tener 10 caracteres en la Línea No. " + liNumeroLinea +
                                            " del Archivo Plano<br>";
                                    else
                                        Convert.ToDateTime(oArregloLinea.GetValue(29).ToString().Trim());
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la fecha de pago {" +
                                                      oArregloLinea.GetValue(29) + "}, en la Línea No. " +
                                                      liNumeroLinea + " del Archivo Plano<br>";
                                }

                            //20170816 rq036-17
                            if (oArregloLinea.GetValue(30).ToString() != "0" &&
                                oArregloLinea.GetValue(30).ToString().Length != 2)
                                lsCadenaErrores = lsCadenaErrores + "El código del departamento  {" +
                                                  oArregloLinea.GetValue(30) + "}, es inválido en la Línea No. " +
                                                  liNumeroLinea + " del Archivo Plano<br>";
                            //20170816 rq036-17
                            if (oArregloLinea.GetValue(31).ToString() != "0" &&
                                oArregloLinea.GetValue(31).ToString().Length != 5)
                                lsCadenaErrores = lsCadenaErrores + "El código del municipio {" +
                                                  oArregloLinea.GetValue(31) + "}, es inválido en la Línea No. " +
                                                  liNumeroLinea + " del Archivo Plano<br>";
                            /// Valida Fuente o campo MP 20160603
                            if (oArregloLinea.GetValue(32).ToString().Trim().Length <= 0)
                                lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el sector de consumo {" +
                                                  oArregloLinea.GetValue(32) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            else
                            {
                                try
                                {
                                    liValor = Convert.ToInt16(oArregloLinea.GetValue(32).ToString().Trim());
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el sector de consumo {" +
                                                      oArregloLinea.GetValue(32) + "}, en la Línea No. " +
                                                      liNumeroLinea + " del Archivo Plano<br>";
                                }
                            }

                            if (oArregloLinea.GetValue(33).ToString() != "N" &&
                                oArregloLinea.GetValue(33).ToString() != "S")
                                lsCadenaErrores = lsCadenaErrores +
                                                  "Valor inválido en el indicador de conexión al SNT {" +
                                                  oArregloLinea.GetValue(33) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            if (oArregloLinea.GetValue(34).ToString().Trim().Length <= 0)
                                lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el mercado relevante {" +
                                                  oArregloLinea.GetValue(34) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            else
                            {
                                try
                                {
                                    liValor = Convert.ToInt16(oArregloLinea.GetValue(34).ToString().Trim());
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el mercado relevante {" +
                                                      oArregloLinea.GetValue(34) + "}, en la Línea No. " +
                                                      liNumeroLinea + " del Archivo Plano<br>";
                                }
                            }

                            if (oArregloLinea.GetValue(35).ToString().Trim().Length <= 0)
                                lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la capacidad de transporte {" +
                                                  oArregloLinea.GetValue(35) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            else
                            {
                                try
                                {
                                    liValor = Convert.ToInt32(oArregloLinea.GetValue(35).ToString().Trim());
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores =
                                        lsCadenaErrores + "Valor Inválido en la capacidad de transporte {" +
                                        oArregloLinea.GetValue(35) + "}, en la Línea No. " + liNumeroLinea +
                                        " del Archivo Plano<br>";
                                }
                            }

                            if (oArregloLinea.GetValue(36).ToString().Trim().Length <= 0)
                                lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la ruta de transporte {" +
                                                  oArregloLinea.GetValue(36) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            else
                            {
                                try
                                {
                                    liValor = Convert.ToInt16(oArregloLinea.GetValue(36).ToString().Trim());
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la ruta de transporte {" +
                                                      oArregloLinea.GetValue(36) + "}, en la Línea No. " +
                                                      liNumeroLinea + " del Archivo Plano<br>";
                                }
                            }

                            //valida nit usuario no regulado
                            if (oArregloLinea.GetValue(37).ToString().Trim().Length > 15)
                                lsCadenaErrores = lsCadenaErrores +
                                                  "Valor inválido en el nit del usuario no regulado {" +
                                                  oArregloLinea.GetValue(37) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            //valida observaciones
                            if (oArregloLinea.GetValue(38).ToString().Trim().Length <= 0)
                                lsCadenaErrores = lsCadenaErrores +
                                                  "Debe Ingresar las observaciones, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";

                            //20210707 trm moneda
                            if (oArregloLinea.GetValue(39).ToString().Trim().Length <= 0)
                                lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el código de la tasa de cambio {" +
                                                  oArregloLinea.GetValue(39) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            else
                            {
                                try
                                {
                                    liValor = Convert.ToInt16(oArregloLinea.GetValue(39).ToString().Trim());
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "Valor Inválido en código de la tasa de cambio {" +
                                                      oArregloLinea.GetValue(39) + "}, en la Línea No. " +
                                                      liNumeroLinea + " del Archivo Plano<br>";
                                }
                            }
                            //20210707 trm moneda
                            if (oArregloLinea.GetValue(40).ToString().Trim().Length <= 0)
                                lsCadenaErrores = lsCadenaErrores +
                                                  "Debe Ingresar las observaciones de la tasa de cambio, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                            //20210707 trm moneda
                            if (oArregloLinea.GetValue(41).ToString() != "USD" && oArregloLinea.GetValue(41).ToString() != "COP")
                                lsCadenaErrores = lsCadenaErrores +
                                                  "Valor inválido en el tipo de omneda {"+ oArregloLinea.GetValue(41).ToString() + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano<br>";
                        }
                    }
                }

                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }

            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }

        /// <summary>
        /// Validacion del Archivo de Usuarios Finales
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivoUsuarios(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            var liTotalRegistros = 0;
            Int64 liValor = 0;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    /// Campop nuevo Req. 009-17 Indicadores 20170324 
                    /// Cantidad Ekivalente KPCD Solo para mercado primario de Gas
                    if (oArregloLinea.Length != 9)
                    {
                        lsCadenaErrores = lsCadenaErrores +
                                          "El Número de Campos no corresponde con la estructura del Plano { 9 },  en la Línea No. " +
                                          liNumeroLinea + " del Archivo Plano de Usuarios<br>"; //rq009-17
                    }
                    else
                    {
                        /// Validar Operacion
                        if (oArregloLinea.GetValue(0).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el No. de Operación {" +
                                              oArregloLinea.GetValue(0) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(0).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el No. de Operación {" +
                                                  oArregloLinea.GetValue(0) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano de Usuarios<br>";
                            }
                        }

                        /// Validar consecutivo
                        if (oArregloLinea.GetValue(0).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el No. del consecutivo {" +
                                              oArregloLinea.GetValue(1) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(1).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el No. del consecutivo {" +
                                                  oArregloLinea.GetValue(1) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano de Usuarios<br>";
                            }
                        }

                        /// Validar Documento Usuario Final
                        if (oArregloLinea.GetValue(2).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores +
                                              "Debe Ingresar el DOcumento de Usuario Final (Valor por defecto 0) {" +
                                              oArregloLinea.GetValue(2) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(2).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores +
                                                  "Valor Inválido en el Documento de Usuario Final {" +
                                                  oArregloLinea.GetValue(2) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano de Usuarios<br>";
                            }
                        }

                        /// Validar Codigo Sector COnsumo 
                        if (oArregloLinea.GetValue(3).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Sector de Consumo {" +
                                              oArregloLinea.GetValue(3) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(3).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Sector de Consumo {" +
                                                  oArregloLinea.GetValue(3) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano de Usuarios<br>";
                            }
                        }

                        /// Validar Codigo Punto de Salida del SNT
                        if (oArregloLinea.GetValue(4).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Punto Salida SNT {" +
                                              oArregloLinea.GetValue(4) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(4).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Punto Salida SNT {" +
                                                  oArregloLinea.GetValue(4) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano de Usuarios<br>";
                            }
                        }

                        /// Validar Cantiad
                        if (oArregloLinea.GetValue(5).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la Cantidad / Capacidad {" +
                                              oArregloLinea.GetValue(5) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(5).ToString().Trim());
                                if (liValor <= 0)
                                    lsCadenaErrores = lsCadenaErrores + "La Cantidad / Capacidad {" +
                                                      oArregloLinea.GetValue(5) + "} no puede ser 0, en la Línea No. " +
                                                      liNumeroLinea + " del Archivo Plano<br>";

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Cantidad / Capacidad {" +
                                                  oArregloLinea.GetValue(5) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano de Usuarios<br>";
                            }
                        }

                        /// Validar TIpo Demanda
                        if (oArregloLinea.GetValue(6).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el tipo de demanda {" +
                                              oArregloLinea.GetValue(6) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(6).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en tipo de demanda {" +
                                                  oArregloLinea.GetValue(6) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano de Usuarios<br>";
                            }
                        }

                        /// Validar Mercado relevante
                        if (oArregloLinea.GetValue(7).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el mercado relevante {" +
                                              oArregloLinea.GetValue(7) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(7).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el mercado relevante {" +
                                                  oArregloLinea.GetValue(7) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano de Usuarios<br>";
                            }
                        }

                        /// Campop nuevo Req. 009-17 Indicadores 20170324 
                        /// Cantidad Ekivalente KPCD Solo para mercado primario de Gas
                        if (oArregloLinea.GetValue(8).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la Cantidad equivalente en KPCD {" +
                                              oArregloLinea.GetValue(8) + "}, en la Línea No. " + liNumeroLinea +
                                              " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(8).ToString().Trim());
                                if (liValor < 0)
                                    lsCadenaErrores = lsCadenaErrores + "La Cantidad Equivalente Kpcd {" +
                                                      oArregloLinea.GetValue(8) +
                                                      "} no puede ser menor a 0, en la Línea No. " + liNumeroLinea +
                                                      " del Archivo Plano<br>";

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Cantidad Equivalente Kpcd {" +
                                                  oArregloLinea.GetValue(8) + "}, en la Línea No. " + liNumeroLinea +
                                                  " del Archivo Plano de Usuarios<br>";
                            }
                        }
                    }
                }

                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }

            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }

        /// <summary>
        /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnAceptar_Click(object sender, EventArgs e)
        {
            var ltCargaArchivo = new StringBuilder();
            var lblMensajeArch = new StringBuilder();
            var liArchivo = new List<string>();
            var liExtension = new List<string>();
            liExtension.Add(".PDF");
            liExtension.Add(".PDFA");
            liExtension.Add(".GIF");
            liExtension.Add(".JPG");
            liExtension.Add(".PNG");
            liExtension.Add(".TIFF");
            var lsExtValidas = ".pdf, .pdfa, .gif, .jpg, .png, .tiff";
            string lsExtension;

            if (Tr01)
            {
                if (FuArchivoOrg01.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo original  del contrato " + LblContrato01.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoOrg01.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo original del contratro" + LblContrato01.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoOrg01.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato01.Text + " ya está cargado en el sistema<br>";
                    //else
                    //liArchivo.Add(FuArchivoOrg01.FileName); ;
                }
                if (FuArchivoMod01.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo modificado del contrato " + LblContrato01.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoMod01.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo modificado del contratro" + LblContrato01.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoMod01.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato01.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{ 
                    //if (liArchivo.Contains(FuArchivoMod01.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato01.Text + " ya se seleccionó anteriormente<br>";
                    //else
                    //liArchivo.Add(FuArchivoMod01.FileName); ;
                    //}

                }

            }
            if (Tr02)
            {
                if (FuArchivoOrg02.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo original  del contrato " + LblContrato02.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoOrg02.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo original del contratro" + LblContrato02.Text + " no es válida: " + lsExtValidas + "<br>");

                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoOrg02.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato02.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoOrg02.FileName))
                    //        lblMensajeArch.Text += "El archivo original del contrato " + LblContrato02.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoOrg02.FileName); ;
                    //}
                }
                if (FuArchivoMod02.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo modificado del contrato " + LblContrato02.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoMod02.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo modificado del contratro" + LblContrato02.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoMod02.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato02.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //if (liArchivo.Contains(FuArchivoMod02.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato02.Text + " ya se seleccionó anteriormente<br>";
                    //else
                    //liArchivo.Add(FuArchivoMod02.FileName); ;
                    //}

                }
            }
            if (Tr03)
            {
                if (FuArchivoOrg03.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo original  del contrato " + LblContrato03.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoOrg03.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo original del contratro" + LblContrato03.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoOrg03.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato03.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //if (liArchivo.Contains(FuArchivoOrg03.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato03.Text + " ya se seleccionó anteriormente<br>";
                    //else
                    //     liArchivo.Add(FuArchivoOrg03.FileName); ;
                    // }
                }
                if (FuArchivoMod03.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo modificado del contrato " + LblContrato03.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoMod03.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo modificado del contratro" + LblContrato03.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoMod03.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato03.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //if (liArchivo.Contains(FuArchivoMod03.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato03.Text + " ya se seleccionó anteriormente<br>";
                    //else
                    //        liArchivo.Add(FuArchivoMod03.FileName); ;
                    //}
                }
            }
            if (Tr04)
            {
                if (FuArchivoOrg04.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo original  del contrato " + LblContrato04.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoOrg04.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo original del contratro" + LblContrato04.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoOrg04.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato04.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //if (liArchivo.Contains(FuArchivoOrg04.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato04.Text + " ya se seleccionó anteriormente<br>";
                    //else
                    //        liArchivo.Add(FuArchivoOrg04.FileName); ;
                    //}
                }
                if (FuArchivoMod04.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo modificado del contrato " + LblContrato04.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoMod04.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo modificado del contratro" + LblContrato04.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoMod04.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato04.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //if (liArchivo.Contains(FuArchivoMod04.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato04.Text + " ya se seleccionó anteriormente<br>";
                    //else
                    //        liArchivo.Add(FuArchivoMod04.FileName); ;
                    //}
                }
            }
            if (Tr05)
            {
                if (FuArchivoOrg05.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo original  del contrato " + LblContrato05.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoOrg05.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo original del contratro" + LblContrato05.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoOrg05.FileName))
                    //lblMensajeArch.Text += "El archivo original del contrato " + LblContrato05.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //if (liArchivo.Contains(FuArchivoOrg05.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato05.Text + " ya se seleccionó anteriormente<br>";
                    //else
                    //    liArchivo.Add(FuArchivoOrg05.FileName); ;
                    //}
                }
                if (FuArchivoMod05.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo modificado del contrato " + LblContrato05.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoMod05.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo modificado del contratro" + LblContrato05.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoMod05.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato05.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //if (liArchivo.Contains(FuArchivoMod05.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato05.Text + " ya se seleccionó anteriormente<br>";
                    //else
                    //    liArchivo.Add(FuArchivoMod05.FileName); ;
                    //}
                }
            }
            if (Tr06)
            {
                if (FuArchivoOrg06.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo original  del contrato " + LblContrato06.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoOrg06.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo original del contratro" + LblContrato06.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoOrg06.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato06.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoOrg06.FileName))
                    //        lblMensajeArch.Text += "El archivo original del contrato " + LblContrato06.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoOrg06.FileName); ;
                    //}
                }
                if (FuArchivoMod06.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo modificado del contrato " + LblContrato06.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoMod06.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo modificado del contratro" + LblContrato06.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoMod06.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato06.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoMod06.FileName))
                    //        lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato06.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoMod06.FileName); ;
                    //}
                }
            }
            if (Tr07)
            {
                if (FuArchivoOrg07.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo original  del contrato " + LblContrato07.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoOrg07.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo original del contratro" + LblContrato07.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoOrg07.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato07.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoOrg07.FileName))
                    //        lblMensajeArch.Text += "El archivo original del contrato " + LblContrato07.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoOrg07.FileName); ;
                    //}
                }
                if (FuArchivoMod07.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo modificado del contrato " + LblContrato07.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoMod07.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo modificado del contratro" + LblContrato07.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoMod07.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato07.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoMod07.FileName))
                    //        lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato07.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoMod07.FileName); ;
                    //}
                }
            }
            if (Tr08)
            {
                if (FuArchivoOrg08.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo original  del contrato " + LblContrato08.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoOrg08.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo original del contratro" + LblContrato08.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoOrg08.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato08.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoOrg08.FileName))
                    //        lblMensajeArch.Text += "El archivo original del contrato " + LblContrato08.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoOrg08.FileName); ;
                    //}
                }
                if (FuArchivoMod08.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo modificado del contrato " + LblContrato08.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoMod08.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo modificado del contratro" + LblContrato08.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoMod08.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato08.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoMod08.FileName))
                    //        lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato08.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoMod08.FileName); ;
                    //}
                }
            }
            if (Tr09)
            {
                if (FuArchivoOrg09.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo original  del contrato " + LblContrato09.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoOrg09.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo original del contratro" + LblContrato09.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoOrg09.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato09.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoOrg09.FileName))
                    //        lblMensajeArch.Text += "El archivo original del contrato " + LblContrato09.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoOrg09.FileName); ;
                    //}
                }
                if (FuArchivoMod09.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo modificado del contrato " + LblContrato09.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoMod09.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo modificado del contratro" + LblContrato09.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoMod09.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato09.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoMod09.FileName))
                    //        lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato09.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoMod09.FileName); ;
                    //}
                }
            }
            if (Tr10)
            {
                if (FuArchivoOrg10.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo original  del contrato " + LblContrato10.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoOrg10.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo original del contratro" + LblContrato10.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoOrg10.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato10.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoOrg10.FileName))
                    //        lblMensajeArch.Text += "El archivo original del contrato " + LblContrato10.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoOrg10.FileName); ;
                    //}
                }
                if (FuArchivoMod10.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo modificado del contrato " + LblContrato10.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoMod10.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo modificado del contratro" + LblContrato10.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoMod10.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato10.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoMod10.FileName))
                    //        lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato10.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoMod10.FileName); ;
                    //}
                }
            }
            if (Tr11)
            {
                if (FuArchivoOrg11.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo original  del contrato " + LblContrato11.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoOrg11.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo original del contratro" + LblContrato11.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoOrg11.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato11.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoOrg11.FileName))
                    //        lblMensajeArch.Text += "El archivo original del contrato " + LblContrato11.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoOrg11.FileName); ;
                    //}
                }
                if (FuArchivoMod11.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo modificado del contrato " + LblContrato11.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoMod11.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo modificado del contratro" + LblContrato11.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoMod11.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato11.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoMod11.FileName))
                    //        lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato11.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoMod11.FileName); ;
                    //}
                }
            }
            if (Tr12)
            {
                if (FuArchivoOrg12.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo original  del contrato " + LblContrato12.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoOrg12.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo original del contratro" + LblContrato12.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoOrg12.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato12.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoOrg12.FileName))
                    //        lblMensajeArch.Text += "El archivo original del contrato " + LblContrato12.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoOrg12.FileName); ;
                    //}
                }
                if (FuArchivoMod12.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo modificado del contrato " + LblContrato12.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoMod12.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo modificado del contratro" + LblContrato12.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoMod12.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato12.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoMod12.FileName))
                    //        lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato12.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoMod12.FileName); ;
                    //}
                }
            }
            if (Tr13)
            {
                if (FuArchivoOrg13.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo original  del contrato " + LblContrato13.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoOrg13.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo original del contratro" + LblContrato13.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoOrg13.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato13.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoOrg13.FileName))
                    //        lblMensajeArch.Text += "El archivo original del contrato " + LblContrato13.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoOrg13.FileName); ;
                    //}
                }
                if (FuArchivoMod13.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo modificado del contrato " + LblContrato13.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoMod13.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo modificado del contratro" + LblContrato13.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoMod13.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato13.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoMod13.FileName))
                    //        lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato13.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoMod13.FileName); ;
                    //}
                }
            }
            if (Tr14)
            {
                if (FuArchivoOrg14.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo original  del contrato " + LblContrato14.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoOrg14.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo original del contratro" + LblContrato14.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoOrg14.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato14.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoOrg14.FileName))
                    //        lblMensajeArch.Text += "El archivo original del contrato " + LblContrato14.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoOrg14.FileName); ;
                    //}
                }
                if (FuArchivoMod14.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo modificado del contrato " + LblContrato14.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoMod14.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo modificado del contratro" + LblContrato14.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoMod14.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato14.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoMod14.FileName))
                    //        lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato14.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoMod14.FileName); ;
                    //}
                }
            }
            if (Tr15)
            {
                if (FuArchivoOrg15.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo original  del contrato " + LblContrato15.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoOrg15.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo original del contratro" + LblContrato15.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoOrg15.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato15.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoOrg15.FileName))
                    //        lblMensajeArch.Text += "El archivo original del contrato " + LblContrato15.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoOrg15.FileName); ;
                    //}
                }
                if (FuArchivoMod15.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo modificado del contrato " + LblContrato15.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoMod15.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo modificado del contratro" + LblContrato15.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoMod15.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato15.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoMod15.FileName))
                    //        lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato15.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoMod15.FileName); ;
                    //}

                }
            }
            if (Tr16)
            {
                if (FuArchivoOrg16.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo original  del contrato " + LblContrato16.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoOrg16.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo original del contratro" + LblContrato16.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoOrg16.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato16.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoOrg16.FileName))
                    //        lblMensajeArch.Text += "El archivo original del contrato " + LblContrato16.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoOrg16.FileName); ;
                    //}
                }
                if (FuArchivoMod16.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo modificado del contrato " + LblContrato16.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoMod16.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo modificado del contratro" + LblContrato16.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoMod16.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato16.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoMod16.FileName))
                    //        lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato16.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoMod16.FileName); ;
                    //}
                }
            }
            if (Tr17)
            {
                if (FuArchivoOrg17.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo original  del contrato " + LblContrato17.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoOrg17.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo original del contratro" + LblContrato17.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoOrg17.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato17.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoOrg17.FileName))
                    //        lblMensajeArch.Text += "El archivo original del contrato " + LblContrato17.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoOrg17.FileName); ;
                    //}
                }
                if (FuArchivoMod17.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo modificado del contrato " + LblContrato17.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoMod17.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo modificado del contratro" + LblContrato17.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoMod17.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato17.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoMod17.FileName))
                    //        lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato17.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoMod17.FileName); ;
                    //}
                }
            }
            if (Tr18)
            {
                if (FuArchivoOrg18.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo original  del contrato " + LblContrato18.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoOrg18.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo original del contratro" + LblContrato18.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoOrg18.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato18.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoOrg18.FileName))
                    //        lblMensajeArch.Text += "El archivo original del contrato " + LblContrato18.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoOrg18.FileName); ;
                    //}
                }
                if (FuArchivoMod18.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo modificado del contrato " + LblContrato18.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoMod18.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo modificado del contratro" + LblContrato18.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoMod18.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato18.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoMod18.FileName))
                    //        lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato18.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoMod18.FileName); ;
                    //}
                }
            }
            if (Tr19)
            {
                if (FuArchivoOrg19.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo original  del contrato " + LblContrato19.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoOrg19.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo original del contratro" + LblContrato19.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoOrg19.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato19.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoOrg19.FileName))
                    //        lblMensajeArch.Text += "El archivo original del contrato " + LblContrato19.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoOrg19.FileName); ;
                    //}
                }
                if (FuArchivoMod19.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo modificado del contrato " + LblContrato19.Text + "<br>");
                }
                else
                {
                    if (File.Exists(sRutaArc + FuArchivoMod19.FileName))
                        lblMensajeArch.Append("El archivo modificado del contrato " + LblContrato19.Text + " ya está cargado en el sistema<br>");
                    else
                    {
                        lsExtension = Path.GetExtension(FuArchivoOrg19.FileName).ToUpper();
                        if (!liExtension.Contains(lsExtension))
                            lblMensajeArch.Append("La extensión del archivo modificado del contratro" + LblContrato19.Text + " no es válida: " + lsExtValidas + "<br>");
                        //20200430 ajuste nombre archivos
                        //if (liArchivo.Contains(FuArchivoMod19.FileName))
                        //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato19.Text + " ya se seleccionó anteriormente<br>";
                        //else
                        //    liArchivo.Add(FuArchivoMod19.FileName); ;
                    }

                }
            }
            if (Tr20)
            {
                if (FuArchivoOrg20.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo original  del contrato " + LblContrato20.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoOrg20.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo original del contratro" + LblContrato20.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoOrg20.FileName))
                    //    lblMensajeArch.Text += "El archivo original del contrato " + LblContrato20.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoOrg20.FileName))
                    //        lblMensajeArch.Text += "El archivo original del contrato " + LblContrato20.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoOrg20.FileName); ;
                    //}
                }
                if (FuArchivoMod20.FileName == "")
                {
                    lblMensajeArch.Append("Debe seleccionar el archivo modificado del contrato " + LblContrato20.Text + "<br>");
                }
                else
                {
                    lsExtension = Path.GetExtension(FuArchivoMod20.FileName).ToUpper();
                    if (!liExtension.Contains(lsExtension))
                        lblMensajeArch.Append("La extensión del archivo modificado del contratro" + LblContrato20.Text + " no es válida: " + lsExtValidas + "<br>");
                    //20200430 ajuste nombre archivos
                    //if (File.Exists(sRutaArc + FuArchivoMod20.FileName))
                    //    lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato20.Text + " ya está cargado en el sistema<br>";
                    //else
                    //{
                    //    if (liArchivo.Contains(FuArchivoMod20.FileName))
                    //        lblMensajeArch.Text += "El archivo modificado del contrato " + LblContrato20.Text + " ya se seleccionó anteriormente<br>";
                    //    else
                    //        liArchivo.Add(FuArchivoMod20.FileName); ;
                    //}
                }
            }

            //20200430 ajuste nombre archivos
            string lsNumero = "";
            string lsArchivoOrg01 = "";
            string lsArchivoMod01 = "";
            string lsArchivoOrg02 = "";
            string lsArchivoMod02 = "";
            string lsArchivoOrg03 = "";
            string lsArchivoMod03 = "";
            string lsArchivoOrg04 = "";
            string lsArchivoMod04 = "";
            string lsArchivoOrg05 = "";
            string lsArchivoMod05 = "";
            string lsArchivoOrg06 = "";
            string lsArchivoMod06 = "";
            string lsArchivoOrg07 = "";
            string lsArchivoMod07 = "";
            string lsArchivoOrg08 = "";
            string lsArchivoMod08 = "";
            string lsArchivoOrg09 = "";
            string lsArchivoMod09 = "";
            string lsArchivoOrg10 = "";
            string lsArchivoMod10 = "";
            string lsArchivoOrg11 = "";
            string lsArchivoMod11 = "";
            string lsArchivoOrg12 = "";
            string lsArchivoMod12 = "";
            string lsArchivoOrg13 = "";
            string lsArchivoMod13 = "";
            string lsArchivoOrg14 = "";
            string lsArchivoMod14 = "";
            string lsArchivoOrg15 = "";
            string lsArchivoMod15 = "";
            string lsArchivoOrg16 = "";
            string lsArchivoMod16 = "";
            string lsArchivoOrg17 = "";
            string lsArchivoMod17 = "";
            string lsArchivoOrg18 = "";
            string lsArchivoMod18 = "";
            string lsArchivoOrg19 = "";
            string lsArchivoMod19 = "";
            string lsArchivoOrg20 = "";
            string lsArchivoMod20 = "";
            string lsArchivo = "";
            int liCOnta = 0;
            while (lsArchivo == "" && liCOnta < 10)
            {
                liCOnta++;
                lsArchivo = "A";
                lsNumero = DateTime.Now.Millisecond.ToString();
                if (FuArchivoOrg01.FileName != "")
                {
                    lsArchivoOrg01 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato01.Text.Replace('/', '_').Replace('\\', '_') + "_Org_" + lsNumero + Path.GetExtension(FuArchivoOrg01.FileName).ToUpper();//20210216
                    lsArchivoMod01 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato01.Text.Replace('/', '_').Replace('\\', '_') + "_Mod_" + lsNumero + Path.GetExtension(FuArchivoMod01.FileName).ToUpper();//20210216
                    if (File.Exists(sRutaArc + lsArchivoOrg01) || File.Exists(sRutaArc + lsArchivoMod01))
                    {
                        lsArchivo = "";
                        continue;
                    }
                }
                if (FuArchivoOrg02.FileName != "")
                {
                    lsArchivoOrg02 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato02.Text.Replace('/', '_').Replace('\\', '_') + "_Org_" + lsNumero + Path.GetExtension(FuArchivoOrg02.FileName).ToUpper();//20210216
                    lsArchivoMod02 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato02.Text.Replace('/', '_').Replace('\\', '_') + "_Mod_" + lsNumero + Path.GetExtension(FuArchivoMod02.FileName).ToUpper();//20210216
                    if (File.Exists(sRutaArc + lsArchivoOrg02) || File.Exists(sRutaArc + lsArchivoMod02))
                    {
                        lsArchivo = "";
                        continue;
                    }
                }
                if (FuArchivoOrg03.FileName != "")
                {
                    lsArchivoOrg03 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato03.Text.Replace('/', '_').Replace('\\', '_') + "_Org_" + lsNumero + Path.GetExtension(FuArchivoOrg03.FileName).ToUpper();//20210216
                    lsArchivoMod03 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato03.Text.Replace('/', '_').Replace('\\', '_') + "_Mod_" + lsNumero + Path.GetExtension(FuArchivoMod03.FileName).ToUpper();//20210216
                    if (File.Exists(sRutaArc + lsArchivoOrg03) || File.Exists(sRutaArc + lsArchivoMod03))
                    {
                        lsArchivo = "";
                        continue;
                    }
                }
                if (FuArchivoOrg04.FileName != "")
                {
                    lsArchivoOrg04 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato04.Text.Replace('/', '_').Replace('\\', '_') + "_Org_" + lsNumero + Path.GetExtension(FuArchivoOrg04.FileName).ToUpper();//20210216
                    lsArchivoMod04 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato04.Text.Replace('/', '_').Replace('\\', '_') + "_Mod_" + lsNumero + Path.GetExtension(FuArchivoMod04.FileName).ToUpper();//20210216
                    if (File.Exists(sRutaArc + lsArchivoOrg04) || File.Exists(sRutaArc + lsArchivoMod04))
                    {
                        lsArchivo = "";
                        continue;
                    }
                }
                if (FuArchivoOrg05.FileName != "")
                {
                    lsArchivoOrg05 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato05.Text.Replace('/', '_').Replace('\\', '_') + "_Org_" + lsNumero + Path.GetExtension(FuArchivoOrg05.FileName).ToUpper();//20210216
                    lsArchivoMod05 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato05.Text.Replace('/', '_').Replace('\\', '_') + "_Mod_" + lsNumero + Path.GetExtension(FuArchivoMod05.FileName).ToUpper();//20210216
                    if (File.Exists(sRutaArc + lsArchivoOrg05) || File.Exists(sRutaArc + lsArchivoMod05))
                    {
                        lsArchivo = "";
                        continue;
                    }
                }
                if (FuArchivoOrg06.FileName != "")
                {
                    lsArchivoOrg06 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato06.Text.Replace('/', '_').Replace('\\', '_') + "_Org_" + lsNumero + Path.GetExtension(FuArchivoOrg06.FileName).ToUpper();//20210216
                    lsArchivoMod06 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato06.Text.Replace('/', '_').Replace('\\', '_') + "_Mod_" + lsNumero + Path.GetExtension(FuArchivoMod06.FileName).ToUpper();//20210216
                    if (File.Exists(sRutaArc + lsArchivoOrg06) || File.Exists(sRutaArc + lsArchivoMod06))
                    {
                        lsArchivo = "";
                        continue;
                    }
                }
                if (FuArchivoOrg07.FileName != "")
                {
                    lsArchivoOrg07 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato07.Text.Replace('/', '_').Replace('\\', '_') + "_Org_" + lsNumero + Path.GetExtension(FuArchivoOrg07.FileName).ToUpper();//20210216
                    lsArchivoMod07 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato07.Text.Replace('/', '_').Replace('\\', '_') + "_Mod_" + lsNumero + Path.GetExtension(FuArchivoMod07.FileName).ToUpper();//20210216
                    if (File.Exists(sRutaArc + lsArchivoOrg07) || File.Exists(sRutaArc + lsArchivoMod07))
                    {
                        lsArchivo = "";
                        continue;
                    }
                }
                if (FuArchivoOrg08.FileName != "")
                {
                    lsArchivoOrg08 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato08.Text.Replace('/', '_').Replace('\\', '_') + "_Org_" + lsNumero + Path.GetExtension(FuArchivoOrg08.FileName).ToUpper();//20210216
                    lsArchivoMod08 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato08.Text.Replace('/', '_').Replace('\\', '_') + "_Mod_" + lsNumero + Path.GetExtension(FuArchivoMod08.FileName).ToUpper();//20210216
                    {
                        lsArchivo = "";
                        continue;
                    }
                }
                if (FuArchivoOrg09.FileName != "")
                {
                    lsArchivoOrg09 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato09.Text.Replace('/', '_').Replace('\\', '_') + "_Org_" + lsNumero + Path.GetExtension(FuArchivoOrg09.FileName).ToUpper();//20210216
                    lsArchivoMod09 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato09.Text.Replace('/', '_').Replace('\\', '_') + "_Mod_" + lsNumero + Path.GetExtension(FuArchivoMod09.FileName).ToUpper();//20210216
                    if (File.Exists(sRutaArc + lsArchivoOrg09) || File.Exists(sRutaArc + lsArchivoMod09))
                    {
                        lsArchivo = "";
                        continue;
                    }
                }
                if (FuArchivoOrg10.FileName != "")
                {
                    lsArchivoOrg10 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato10.Text.Replace('/', '_').Replace('\\', '_') + "_Org_" + lsNumero + Path.GetExtension(FuArchivoOrg10.FileName).ToUpper();//20210216
                    lsArchivoMod10 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato10.Text.Replace('/', '_').Replace('\\', '_') + "_Mod_" + lsNumero + Path.GetExtension(FuArchivoMod10.FileName).ToUpper();//20210216
                    if (File.Exists(sRutaArc + lsArchivoOrg10) || File.Exists(sRutaArc + lsArchivoMod10))
                    {
                        lsArchivo = "";
                        continue;
                    }
                }
                if (FuArchivoOrg11.FileName != "")
                {
                    lsArchivoOrg11 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato11.Text.Replace('/', '_').Replace('\\', '_') + "_Org_" + lsNumero + Path.GetExtension(FuArchivoOrg11.FileName).ToUpper();//20210216
                    lsArchivoMod11 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato11.Text.Replace('/', '_').Replace('\\', '_') + "_Mod_" + lsNumero + Path.GetExtension(FuArchivoMod11.FileName).ToUpper();//20210216
                    if (File.Exists(sRutaArc + lsArchivoOrg11) || File.Exists(sRutaArc + lsArchivoMod11))
                    {
                        lsArchivo = "";
                        continue;
                    }
                }
                if (FuArchivoOrg12.FileName != "")
                {
                    lsArchivoOrg12 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato12.Text.Replace('/', '_').Replace('\\', '_') + "_Org_" + lsNumero + Path.GetExtension(FuArchivoOrg12.FileName).ToUpper();//20210216
                    lsArchivoMod12 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato12.Text.Replace('/', '_').Replace('\\', '_') + "_Mod_" + lsNumero + Path.GetExtension(FuArchivoMod12.FileName).ToUpper();//20210216
                    if (File.Exists(sRutaArc + lsArchivoOrg12) || File.Exists(sRutaArc + lsArchivoMod12))
                    {
                        lsArchivo = "";
                        continue;
                    }
                }
                if (FuArchivoOrg13.FileName != "")
                {
                    lsArchivoOrg13 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato13.Text.Replace('/', '_').Replace('\\', '_') + "_Org_" + lsNumero + Path.GetExtension(FuArchivoOrg13.FileName).ToUpper();//20210216
                    lsArchivoMod13 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato13.Text.Replace('/', '_').Replace('\\', '_') + "_Mod_" + lsNumero + Path.GetExtension(FuArchivoMod13.FileName).ToUpper();//20210216
                    if (File.Exists(sRutaArc + lsArchivoOrg13) || File.Exists(sRutaArc + lsArchivoMod13))
                    {
                        lsArchivo = "";
                        continue;
                    }
                }
                if (FuArchivoOrg14.FileName != "")
                {
                    lsArchivoOrg14 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato14.Text.Replace('/', '_').Replace('\\', '_') + "_Org_" + lsNumero + Path.GetExtension(FuArchivoOrg14.FileName).ToUpper();//20210216
                    lsArchivoMod14 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato14.Text.Replace('/', '_').Replace('\\', '_') + "_Mod_" + lsNumero + Path.GetExtension(FuArchivoMod14.FileName).ToUpper();//20210216
                    if (File.Exists(sRutaArc + lsArchivoOrg14) || File.Exists(sRutaArc + lsArchivoMod14))
                    {
                        lsArchivo = "";
                        continue;
                    }
                }
                if (FuArchivoOrg15.FileName != "")
                {
                    lsArchivoOrg15 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato15.Text.Replace('/', '_').Replace('\\', '_') + "_Org_" + lsNumero + Path.GetExtension(FuArchivoOrg15.FileName).ToUpper();//20210216
                    lsArchivoMod15 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato15.Text.Replace('/', '_').Replace('\\', '_') + "_Mod_" + lsNumero + Path.GetExtension(FuArchivoMod15.FileName).ToUpper();//20210216
                    if (File.Exists(sRutaArc + lsArchivoOrg15) || File.Exists(sRutaArc + lsArchivoMod15))
                    {
                        lsArchivo = "";
                        continue;
                    }
                }
                if (FuArchivoOrg16.FileName != "")
                {
                    lsArchivoOrg16 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato16.Text.Replace('/', '_').Replace('\\', '_') + "_Org_" + lsNumero + Path.GetExtension(FuArchivoOrg16.FileName).ToUpper();//20210216
                    lsArchivoMod16 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato16.Text.Replace('/', '_').Replace('\\', '_') + "_Mod_" + lsNumero + Path.GetExtension(FuArchivoMod16.FileName).ToUpper();//20210216
                    if (File.Exists(sRutaArc + lsArchivoOrg16) || File.Exists(sRutaArc + lsArchivoMod16))
                    {
                        lsArchivo = "";
                        continue;
                    }
                }
                if (FuArchivoOrg17.FileName != "")
                {
                    lsArchivoOrg17 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato17.Text.Replace('/', '_').Replace('\\', '_') + "_Org_" + lsNumero + Path.GetExtension(FuArchivoOrg17.FileName).ToUpper();//20210216
                    lsArchivoMod17 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato17.Text.Replace('/', '_').Replace('\\', '_') + "_Mod_" + lsNumero + Path.GetExtension(FuArchivoMod17.FileName).ToUpper();//20210216
                    if (File.Exists(sRutaArc + lsArchivoOrg17) || File.Exists(sRutaArc + lsArchivoMod17))
                    {
                        lsArchivo = "";
                        continue;
                    }
                }
                if (FuArchivoOrg18.FileName != "")
                {
                    lsArchivoOrg18 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato18.Text.Replace('/', '_').Replace('\\', '_') + "_Org_" + lsNumero + Path.GetExtension(FuArchivoOrg18.FileName).ToUpper();//20210216
                    lsArchivoMod18 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato18.Text.Replace('/', '_').Replace('\\', '_') + "_Mod_" + lsNumero + Path.GetExtension(FuArchivoMod18.FileName).ToUpper();//20210216
                    if (File.Exists(sRutaArc + lsArchivoOrg18) || File.Exists(sRutaArc + lsArchivoMod18))
                    {
                        lsArchivo = "";
                        continue;
                    }
                }
                if (FuArchivoOrg19.FileName != "")
                {
                    lsArchivoOrg19 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato19.Text.Replace('/', '_').Replace('\\', '_') + "_Org_" + lsNumero + Path.GetExtension(FuArchivoOrg19.FileName).ToUpper();//20210216
                    lsArchivoMod19 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato19.Text.Replace('/', '_').Replace('\\', '_') + "_Mod_" + lsNumero + Path.GetExtension(FuArchivoMod19.FileName).ToUpper();//20210216
                    if (File.Exists(sRutaArc + lsArchivoOrg19) || File.Exists(sRutaArc + lsArchivoMod19))
                    {
                        lsArchivo = "";
                        continue;
                    }
                }
                if (FuArchivoOrg20.FileName != "")
                {
                    lsArchivoOrg20 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato20.Text.Replace('/', '_').Replace('\\', '_') + "_Org_" + lsNumero + Path.GetExtension(FuArchivoOrg20.FileName).ToUpper();//20210216
                    lsArchivoMod20 = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + LblContrato20.Text.Replace('/', '_').Replace('\\', '_') + "_Mod_" + lsNumero + Path.GetExtension(FuArchivoMod20.FileName).ToUpper(); //20210216
                    if (File.Exists(sRutaArc + lsArchivoOrg20) || File.Exists(sRutaArc + lsArchivoMod20))
                    {
                        lsArchivo = "";
                        continue;
                    }
                }

            }
            if (lsArchivo == "")
                lblMensajeArch.Append("No se pueden cargar los archivos. Intente de nuevo<br>");
            //20200430 ajuste nombre archivos

            if (lblMensajeArch.ToString() == "")
            {
                var lComando = new SqlCommand();
                try
                {
                    if (FuArchivoOrg01.FileName != "")
                        FuArchivoOrg01.SaveAs(sRutaArc + lsArchivoOrg01);//20200430 ajuste nombre archivos
                    if (FuArchivoMod01.FileName != "")
                        FuArchivoMod01.SaveAs(sRutaArc + lsArchivoMod01);//20200430 ajuste nombre archivos
                    if (FuArchivoOrg02.FileName != "")
                        FuArchivoOrg02.SaveAs(sRutaArc + lsArchivoOrg02);//20200430 ajuste nombre archivos
                    if (FuArchivoMod02.FileName != "")
                        FuArchivoMod02.SaveAs(sRutaArc + lsArchivoMod02);//20200430 ajuste nombre archivos
                    if (FuArchivoOrg03.FileName != "")
                        FuArchivoOrg03.SaveAs(sRutaArc + lsArchivoOrg03);//20200430 ajuste nombre archivos
                    if (FuArchivoMod03.FileName != "")
                        FuArchivoMod03.SaveAs(sRutaArc + lsArchivoMod03);//20200430 ajuste nombre archivos
                    if (FuArchivoOrg04.FileName != "")
                        FuArchivoOrg04.SaveAs(sRutaArc + lsArchivoOrg04);//20200430 ajuste nombre archivos
                    if (FuArchivoMod04.FileName != "")
                        FuArchivoMod04.SaveAs(sRutaArc + lsArchivoMod04);//20200430 ajuste nombre archivos
                    if (FuArchivoOrg05.FileName != "")
                        FuArchivoOrg05.SaveAs(sRutaArc + lsArchivoOrg05);//20200430 ajuste nombre archivos
                    if (FuArchivoMod05.FileName != "")
                        FuArchivoMod05.SaveAs(sRutaArc + lsArchivoMod05);//20200430 ajuste nombre archivos
                    if (FuArchivoOrg06.FileName != "")
                        FuArchivoOrg06.SaveAs(sRutaArc + lsArchivoOrg06);//20200430 ajuste nombre archivos
                    if (FuArchivoMod06.FileName != "")
                        FuArchivoMod06.SaveAs(sRutaArc + lsArchivoMod06);//20200430 ajuste nombre archivos
                    if (FuArchivoOrg07.FileName != "")
                        FuArchivoOrg07.SaveAs(sRutaArc + lsArchivoOrg07);//20200430 ajuste nombre archivos
                    if (FuArchivoMod07.FileName != "")
                        FuArchivoMod07.SaveAs(sRutaArc + lsArchivoMod07);//20200430 ajuste nombre archivos
                    if (FuArchivoOrg08.FileName != "")
                        FuArchivoOrg08.SaveAs(sRutaArc + lsArchivoOrg08);//20200430 ajuste nombre archivos
                    if (FuArchivoMod08.FileName != "")
                        FuArchivoMod08.SaveAs(sRutaArc + lsArchivoMod08);//20200430 ajuste nombre archivos
                    if (FuArchivoOrg09.FileName != "")
                        FuArchivoOrg09.SaveAs(sRutaArc + lsArchivoOrg09);//20200430 ajuste nombre archivos
                    if (FuArchivoMod09.FileName != "")
                        FuArchivoMod09.SaveAs(sRutaArc + lsArchivoMod09);//20200430 ajuste nombre archivos
                    if (FuArchivoOrg10.FileName != "")
                        FuArchivoOrg10.SaveAs(sRutaArc + lsArchivoOrg10);//20200430 ajuste nombre archivos
                    if (FuArchivoMod10.FileName != "")
                        FuArchivoMod10.SaveAs(sRutaArc + lsArchivoMod10);//20200430 ajuste nombre archivos
                    if (FuArchivoOrg11.FileName != "")
                        FuArchivoOrg11.SaveAs(sRutaArc + lsArchivoOrg11);//20200430 ajuste nombre archivos
                    if (FuArchivoMod11.FileName != "")
                        FuArchivoMod11.SaveAs(sRutaArc + lsArchivoMod11);//20200430 ajuste nombre archivos
                    if (FuArchivoOrg12.FileName != "")
                        FuArchivoOrg12.SaveAs(sRutaArc + lsArchivoOrg12);//20200430 ajuste nombre archivos
                    if (FuArchivoMod12.FileName != "")
                        FuArchivoMod12.SaveAs(sRutaArc + lsArchivoMod12);//20200430 ajuste nombre archivos
                    if (FuArchivoOrg13.FileName != "")
                        FuArchivoOrg13.SaveAs(sRutaArc + lsArchivoOrg13);//20200430 ajuste nombre archivos
                    if (FuArchivoMod13.FileName != "")
                        FuArchivoMod13.SaveAs(sRutaArc + lsArchivoMod13);//20200430 ajuste nombre archivos
                    if (FuArchivoOrg14.FileName != "")
                        FuArchivoOrg14.SaveAs(sRutaArc + lsArchivoOrg14);//20200430 ajuste nombre archivos
                    if (FuArchivoMod14.FileName != "")
                        FuArchivoMod14.SaveAs(sRutaArc + lsArchivoMod14);//20200430 ajuste nombre archivos
                    if (FuArchivoOrg15.FileName != "")
                        FuArchivoOrg15.SaveAs(sRutaArc + lsArchivoOrg15);//20200430 ajuste nombre archivos
                    if (FuArchivoMod15.FileName != "")
                        FuArchivoMod15.SaveAs(sRutaArc + lsArchivoMod15);//20200430 ajuste nombre archivos
                    if (FuArchivoOrg16.FileName != "")
                        FuArchivoOrg16.SaveAs(sRutaArc + lsArchivoOrg16);//20200430 ajuste nombre archivos
                    if (FuArchivoMod16.FileName != "")
                        FuArchivoMod16.SaveAs(sRutaArc + lsArchivoMod16);//20200430 ajuste nombre archivos
                    if (FuArchivoOrg17.FileName != "")
                        FuArchivoOrg17.SaveAs(sRutaArc + lsArchivoOrg17);//20200430 ajuste nombre archivos
                    if (FuArchivoMod17.FileName != "")
                        FuArchivoMod17.SaveAs(sRutaArc + lsArchivoMod17);//20200430 ajuste nombre archivos
                    if (FuArchivoOrg18.FileName != "")
                        FuArchivoOrg18.SaveAs(sRutaArc + lsArchivoOrg18);//20200430 ajuste nombre archivos
                    if (FuArchivoMod18.FileName != "")
                        FuArchivoMod18.SaveAs(sRutaArc + lsArchivoMod18);//20200430 ajuste nombre archivos
                    if (FuArchivoOrg19.FileName != "")
                        FuArchivoOrg19.SaveAs(sRutaArc + lsArchivoOrg19);//20200430 ajuste nombre archivos
                    if (FuArchivoMod19.FileName != "")
                        FuArchivoMod19.SaveAs(sRutaArc + lsArchivoMod19);//20200430 ajuste nombre archivos
                    if (FuArchivoOrg20.FileName != "")
                        FuArchivoOrg20.SaveAs(sRutaArc + lsArchivoOrg20);//20200430 ajuste nombre archivos
                    if (FuArchivoMod20.FileName != "")
                        FuArchivoMod20.SaveAs(sRutaArc + lsArchivoMod20);//20200430 ajuste nombre archivos

                    lConexion.Abrir();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_ActPlanoModContReg";
                    lComando.Parameters.Add("@P_archivo_org_01", SqlDbType.VarChar).Value = lsArchivoOrg01; //20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_nv_01", SqlDbType.VarChar).Value = lsArchivoMod01; //20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_org_02", SqlDbType.VarChar).Value = lsArchivoOrg02;//20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_nv_02", SqlDbType.VarChar).Value = lsArchivoMod02;//20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_org_03", SqlDbType.VarChar).Value = lsArchivoOrg03;//20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_nv_03", SqlDbType.VarChar).Value = lsArchivoMod03;//20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_org_04", SqlDbType.VarChar).Value = lsArchivoOrg04;//20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_nv_04", SqlDbType.VarChar).Value = lsArchivoMod04;//20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_org_05", SqlDbType.VarChar).Value = lsArchivoOrg05;//20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_nv_05", SqlDbType.VarChar).Value = lsArchivoMod05;//20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_org_06", SqlDbType.VarChar).Value = lsArchivoOrg06;//20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_nv_06", SqlDbType.VarChar).Value = lsArchivoMod06;//20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_org_07", SqlDbType.VarChar).Value = lsArchivoOrg07;//20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_nv_07", SqlDbType.VarChar).Value = lsArchivoMod07;//20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_org_08", SqlDbType.VarChar).Value = lsArchivoOrg08;//20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_nv_08", SqlDbType.VarChar).Value = lsArchivoMod08;//20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_org_09", SqlDbType.VarChar).Value = lsArchivoOrg09;//20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_nv_09", SqlDbType.VarChar).Value = lsArchivoMod09;//20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_org_10", SqlDbType.VarChar).Value = lsArchivoOrg10;//20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_nv_10", SqlDbType.VarChar).Value = lsArchivoMod10;//20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_org_11", SqlDbType.VarChar).Value = lsArchivoOrg11;//20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_nv_11", SqlDbType.VarChar).Value = lsArchivoMod11;//20200430 ajuste nombre archivos//20200430 ajuste nombre archivos
                    lComando.Parameters.Add("@P_archivo_org_12", SqlDbType.VarChar).Value = lsArchivoOrg12;
                    lComando.Parameters.Add("@P_archivo_nv_12", SqlDbType.VarChar).Value = lsArchivoMod12;
                    lComando.Parameters.Add("@P_archivo_org_13", SqlDbType.VarChar).Value = lsArchivoOrg13;
                    lComando.Parameters.Add("@P_archivo_nv_13", SqlDbType.VarChar).Value = lsArchivoMod13;
                    lComando.Parameters.Add("@P_archivo_org_14", SqlDbType.VarChar).Value = lsArchivoOrg14;
                    lComando.Parameters.Add("@P_archivo_nv_14", SqlDbType.VarChar).Value = lsArchivoMod14;
                    lComando.Parameters.Add("@P_archivo_org_15", SqlDbType.VarChar).Value = lsArchivoOrg15;
                    lComando.Parameters.Add("@P_archivo_nv_15", SqlDbType.VarChar).Value = lsArchivoMod15;
                    lComando.Parameters.Add("@P_archivo_org_16", SqlDbType.VarChar).Value = lsArchivoOrg16;
                    lComando.Parameters.Add("@P_archivo_nv_16", SqlDbType.VarChar).Value = lsArchivoMod16;
                    lComando.Parameters.Add("@P_archivo_org_17", SqlDbType.VarChar).Value = lsArchivoOrg17;
                    lComando.Parameters.Add("@P_archivo_nv_17", SqlDbType.VarChar).Value = lsArchivoMod17;
                    lComando.Parameters.Add("@P_archivo_org_18", SqlDbType.VarChar).Value = lsArchivoOrg18;
                    lComando.Parameters.Add("@P_archivo_nv_18", SqlDbType.VarChar).Value = lsArchivoMod18;
                    lComando.Parameters.Add("@P_archivo_org_19", SqlDbType.VarChar).Value = lsArchivoOrg19;
                    lComando.Parameters.Add("@P_archivo_nv_19", SqlDbType.VarChar).Value = lsArchivoMod19;
                    lComando.Parameters.Add("@P_archivo_org_20", SqlDbType.VarChar).Value = lsArchivoOrg20;
                    lComando.Parameters.Add("@P_archivo_nv_20", SqlDbType.VarChar).Value = lsArchivoMod20;
                    lComando.CommandTimeout = 3600;
                    lLector = lComando.ExecuteReader();
                    ToastrEvent?.Invoke(HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga", CultureInfo.CurrentCulture)?.ToString(), EnumTypeToastr.Success);
                    if (lLector.HasRows)
                        while (lLector.Read())
                            ltCargaArchivo.Append(lLector["mensaje"] + "<br>");
                    if (ltCargaArchivo.ToString().Contains(HttpContext.GetGlobalResourceObject("AppResources", "EstadoPendienteContraparte", CultureInfo.CurrentCulture)?.ToString()) || ltCargaArchivo.ToString().Contains(HttpContext.GetGlobalResourceObject("AppResources", "EstadoPendienteGestor", CultureInfo.CurrentCulture)?.ToString()) || ltCargaArchivo.ToString().Contains(HttpContext.GetGlobalResourceObject("AppResources", "EstadoPendienteCorrecion", CultureInfo.CurrentCulture)?.ToString()) || ltCargaArchivo.ToString().Contains(" pendiente otras aprobaciones"))
                    {
                        ToastrEvent?.Invoke(ltCargaArchivo.ToString(), EnumTypeToastr.Success);
                        return;
                    }
                    if (!string.IsNullOrEmpty(ltCargaArchivo.ToString()))
                    {
                        ToastrEvent?.Invoke(ltCargaArchivo.ToString(), EnumTypeToastr.Error);
                    }
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke($"{HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivoModificaciones", CultureInfo.CurrentCulture)?.ToString()}: {ex.Message}", EnumTypeToastr.Error);
                }

                ModalEvent?.Invoke(mdlSolicitud.ID, mdlSolicitudInside.ID, EnumTypeModal.Cerrar);
            }
            else
            {
                //if (!string.IsNullOrEmpty(lblMensajeArch.ToString()) && lblMensajeArch.ToString().Contains("Se crearon correctamente"))
                //{
                //    string[] delim = { Environment.NewLine, "\n" }; // "\n" added in case you manually appended a newline
                //    var lines = lblMensajeArch.ToString().Split(delim, StringSplitOptions.None);
                //    lblMensajeArch.Clear();
                //    foreach (var line in lines)
                //    {
                //        if (!line.Contains("Se crearon correctamente"))
                //            lblMensajeArch.Append(line);
                //        else
                //            ToastrEvent?.Invoke(line, EnumTypeToastr.Success);
                //    }
                //}

                //if (!string.IsNullOrEmpty(lblMensajeArch.ToString()))
                //{
                //    lblMensajeArch.Insert(0, "*** LOG ARCHIVO DE CARGA ***<br>");
                //    //Se descarga el log para el usuario
                //    LogCargaArchivoEvent?.Invoke(lblMensajeArch.ToString());
                //    ToastrEvent?.Invoke("Finalizó la carga con errores", EnumTypeToastr.Error);
                //}
                ToastrEvent?.Invoke(lblMensajeArch.ToString(), EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCancelar_Click(object sender, EventArgs e)
        {
            ModalEvent?.Invoke(mdlSolicitud.ID, mdlSolicitudInside.ID, EnumTypeModal.Cerrar);
            //ltCargaArchivo.Text = "Carga de archivo cancelada por el usuario";
        }

        /// <summary>
        /// Establece el estado de visibilidad de la sección 1
        /// </summary>
        /// <param name="estado"></param>
        private void AbrirTr01(bool estado)
        {
            Tr01 = estado;
            LblContrato01.Visible = estado;
            FuArchivoOrg01.Visible = estado;
            FuArchivoMod01.Visible = estado;
        }

        /// <summary>
        /// Establece el estado de visibilidad de la sección 2
        /// </summary>
        /// <param name="estado"></param>
        private void AbrirTr02(bool estado)
        {
            Tr02 = estado;
            LblContrato02.Visible = estado;
            FuArchivoOrg02.Visible = estado;
            FuArchivoMod02.Visible = estado;
        }

        /// <summary>
        /// Establece el estado de visibilidad de la sección 3
        /// </summary>
        /// <param name="estado"></param>
        /// <param name="estado"></param>
        private void AbrirTr03(bool estado)
        {
            Tr03 = estado;
            LblContrato03.Visible = estado;
            FuArchivoOrg03.Visible = estado;
            FuArchivoMod03.Visible = estado;
        }

        /// <summary>
        /// Establece el estado de visibilidad de la sección 4
        /// </summary>
        /// <param name="estado"></param>
        private void AbrirTr04(bool estado)
        {
            Tr04 = estado;
            LblContrato04.Visible = estado;
            FuArchivoOrg04.Visible = estado;
            FuArchivoMod04.Visible = estado;
        }

        /// <summary>
        /// Establece el estado de visibilidad de la sección 5
        /// </summary>
        /// <param name="estado"></param>
        private void AbrirTr05(bool estado)
        {
            Tr05 = estado;
            LblContrato05.Visible = estado;
            FuArchivoOrg05.Visible = estado;
            FuArchivoMod05.Visible = estado;
        }

        /// <summary>
        /// Establece el estado de visibilidad de la sección 6
        /// </summary>
        /// <param name="estado"></param>
        private void AbrirTr06(bool estado)
        {
            Tr06 = estado;
            LblContrato06.Visible = estado;
            FuArchivoOrg06.Visible = estado;
            FuArchivoMod06.Visible = estado;
        }

        /// <summary>
        /// Establece el estado de visibilidad de la sección 7
        /// </summary>
        /// <param name="estado"></param>
        private void AbrirTr07(bool estado)
        {
            Tr07 = estado;
            LblContrato07.Visible = estado;
            FuArchivoOrg07.Visible = estado;
            FuArchivoMod07.Visible = estado;
        }

        /// <summary>
        /// Establece el estado de visibilidad de la sección 8
        /// </summary>
        /// <param name="estado"></param>
        private void AbrirTr08(bool estado)
        {
            Tr08 = estado;
            LblContrato08.Visible = estado;
            FuArchivoOrg08.Visible = estado;
            FuArchivoMod08.Visible = estado;
        }

        /// <summary>
        /// Establece el estado de visibilidad de la sección 9
        /// </summary>
        /// <param name="estado"></param>
        private void AbrirTr09(bool estado)
        {
            Tr09 = estado;
            LblContrato09.Visible = estado;
            FuArchivoOrg09.Visible = estado;
            FuArchivoMod09.Visible = estado;
        }

        /// <summary>
        /// Establece el estado de visibilidad de la sección 10
        /// </summary>
        /// <param name="estado"></param>
        private void AbrirTr10(bool estado)
        {
            Tr10 = estado;
            LblContrato10.Visible = estado;
            FuArchivoOrg10.Visible = estado;
            FuArchivoMod10.Visible = estado;
        }

        /// <summary>
        /// Establece el estado de visibilidad de la sección 11
        /// </summary>
        /// <param name="estado"></param>
        private void AbrirTr11(bool estado)
        {
            Tr11 = estado;
            LblContrato11.Visible = estado;
            FuArchivoOrg11.Visible = estado;
            FuArchivoMod11.Visible = estado;
        }

        /// <summary>
        /// Establece el estado de visibilidad de la sección 12
        /// </summary>
        /// <param name="estado"></param>
        private void AbrirTr12(bool estado)
        {
            Tr12 = estado;
            LblContrato12.Visible = estado;
            FuArchivoOrg12.Visible = estado;
            FuArchivoMod12.Visible = estado;
        }

        /// <summary>
        /// Establece el estado de visibilidad de la sección 13
        /// </summary>
        /// <param name="estado"></param>
        private void AbrirTr13(bool estado)
        {
            Tr13 = estado;
            LblContrato13.Visible = estado;
            FuArchivoOrg13.Visible = estado;
            FuArchivoMod13.Visible = estado;
        }

        /// <summary>
        /// Establece el estado de visibilidad de la sección 14
        /// </summary>
        /// <param name="estado"></param>
        private void AbrirTr14(bool estado)
        {
            Tr14 = estado;
            LblContrato14.Visible = estado;
            FuArchivoOrg14.Visible = estado;
            FuArchivoMod14.Visible = estado;
        }

        /// <summary>
        /// Establece el estado de visibilidad de la sección 15
        /// </summary>
        /// <param name="estado"></param>
        private void AbrirTr15(bool estado)
        {
            Tr15 = estado;
            LblContrato15.Visible = estado;
            FuArchivoOrg15.Visible = estado;
            FuArchivoMod15.Visible = estado;
        }

        /// <summary>
        /// Establece el estado de visibilidad de la sección 16
        /// </summary>
        /// <param name="estado"></param>
        private void AbrirTr16(bool estado)
        {
            Tr16 = estado;
            LblContrato16.Visible = estado;
            FuArchivoOrg16.Visible = estado;
            FuArchivoMod16.Visible = estado;
        }

        /// <summary>
        /// Establece el estado de visibilidad de la sección 17
        /// </summary>
        /// <param name="estado"></param>
        private void AbrirTr17(bool estado)
        {
            Tr17 = estado;
            LblContrato17.Visible = estado;
            FuArchivoOrg17.Visible = estado;
            FuArchivoMod17.Visible = estado;
        }

        /// <summary>
        /// Establece el estado de visibilidad de la sección 18
        /// </summary>
        /// <param name="estado"></param>
        private void AbrirTr18(bool estado)
        {
            Tr18 = estado;
            LblContrato18.Visible = estado;
            FuArchivoOrg18.Visible = estado;
            FuArchivoMod18.Visible = estado;
        }

        /// <summary>
        /// Establece el estado de visibilidad de la sección 19
        /// </summary>
        /// <param name="estado"></param>
        private void AbrirTr19(bool estado)
        {
            Tr19 = estado;
            LblContrato19.Visible = estado;
            FuArchivoOrg19.Visible = estado;
            FuArchivoMod19.Visible = estado;
        }

        /// <summary>
        /// Establece el estado de visibilidad de la sección 20
        /// </summary>
        /// <param name="estado"></param>
        private void AbrirTr20(bool estado)
        {
            Tr20 = estado;
            LblContrato20.Visible = estado;
            FuArchivoOrg20.Visible = estado;
            FuArchivoMod20.Visible = estado;
        }
    }
}