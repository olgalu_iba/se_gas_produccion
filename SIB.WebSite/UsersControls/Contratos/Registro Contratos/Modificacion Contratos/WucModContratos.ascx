﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucModContratos.ascx.cs" Inherits="UsersControls.Contratos.Registro_Contratos.Modificacion_Contratos.WucModContratos" %>

<%--Contenido--%>
<div id="tblMensaje" runat="server">
    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <asp:Label AssociatedControlID="TxtBusContratoDef" runat="server">Número Contrato</asp:Label>
                <asp:TextBox ID="TxtBusContratoDef" CssClass="form-control" ValidationGroup="detalle" runat="server" />
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <asp:Label AssociatedControlID="TxtBusOperacion" runat="server">Número Operación</asp:Label>
                <asp:TextBox ID="TxtBusOperacion" type="number" runat="server" class="form-control" ValidationGroup="detalle" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" />
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <asp:Label AssociatedControlID="TxtBusId" runat="server">Id Registro</asp:Label>
                <asp:TextBox ID="TxtBusId" type="number" runat="server" class="form-control" ValidationGroup="detalle" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" />
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <asp:Label AssociatedControlID="TxtBusFechaIni" runat="server">Fecha Negociación Ini.</asp:Label>
                <asp:TextBox ID="TxtBusFechaIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <asp:Label AssociatedControlID="TxtBusFechaFin" runat="server">Fecha Negociación Fin</asp:Label>
                <asp:TextBox ID="TxtBusFechaFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="row" runat="server" id="tr01">
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <asp:Label AssociatedControlID="TxtBusId" runat="server">Operador Contraparte</asp:Label>
                <asp:DropDownList ID="DdlBusContra" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <asp:Label AssociatedControlID="TxtBusId" runat="server">Causa</asp:Label>
                <asp:DropDownList ID="DdlBusCausa" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <asp:Label AssociatedControlID="DdlBusEstado" runat="server">Estado</asp:Label>
                <asp:DropDownList ID="DdlBusEstado" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
            </div>
        </div>
    </div>
    <div class="row" runat="server" id="tr02">
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <label>Comprador</label>
                <asp:DropDownList ID="DdlBusCompra" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <label>Vendedor</label>
                <asp:DropDownList ID="DdlBusVenta" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
            </div>
        </div>
    </div>
    <div class="row" runat="server" id="tr03">
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <label>Tipo Mercado</label>
                <asp:DropDownList ID="DdlBusMercado" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true" OnSelectedIndexChanged="DdlBusMercado_SelectedIndexChanged">
                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                    <asp:ListItem Value="P">Primario</asp:ListItem>
                    <asp:ListItem Value="S">Secundario</asp:ListItem>
                    <asp:ListItem Value="O">Otras transacciones del mercado mayorista</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <label>Producto</label>
                <asp:DropDownList ID="DdlBusProducto" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                    <asp:ListItem Value="G">Suministro de gas</asp:ListItem>
                    <asp:ListItem Value="T">Capacidad de transporte</asp:ListItem>
                    <asp:ListItem Value="A">Suministro y transporte</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <label>Tipo de contrato</label>
                <asp:DropDownList ID="DdlBusModalidad" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <label>Causa</label>
                <asp:DropDownList ID="DdlBusCausa1" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
                <label>Estado Modificación</label>
                <asp:DropDownList ID="DdlBusEstado1" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
            </div>
        </div>
    </div>
    <%--Grilla--%>
    <div class="table table-responsive">
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                    Width="100%" CssClass="table-bordered" OnItemCommand="dtgConsulta_EditCommand">
                    <Columns>
                        <%--0--%>
                        <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" Visible="False"></asp:EditCommandColumn>
                        <%--1--%>
                        <asp:EditCommandColumn HeaderText="Consultar" EditText="Consultar" Visible="False"></asp:EditCommandColumn>
                        <%--2--%>
                        <asp:BoundColumn DataField="codigo_solicitud" Visible="false"></asp:BoundColumn>
                        <%--3--%>
                        <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--4--%>
                        <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--5--%>
                        <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                        <%--6--%>
                        <asp:BoundColumn DataField="contrato_definitivo" HeaderText="contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--7--%>
                        <asp:BoundColumn DataField="operador_compra" HeaderText="Cod. Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--8--%>
                        <asp:BoundColumn DataField="nombre_compra" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--9--%>
                        <asp:BoundColumn DataField="operador_venta" HeaderText="Cod. Venta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--10--%>
                        <asp:BoundColumn DataField="nombre_venta" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--11--%>
                        <asp:BoundColumn DataField="desc_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--12--%>
                        <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--13--%>
                        <asp:BoundColumn DataField="codigo_tipo_subasta" Visible="false"></asp:BoundColumn>
                        <%--14--%>
                        <asp:BoundColumn DataField="ind_contrato_var" HeaderText="Contrato Variable" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--15--%>
                        <asp:BoundColumn DataField="desc_estado" HeaderText="Estado Modificación"></asp:BoundColumn>
                        <%--16--%>
                        <asp:BoundColumn DataField="desc_causa" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                        <%--17--%>
                        <asp:EditCommandColumn HeaderText="Contrato original" EditText="Original" Visible="False"></asp:EditCommandColumn>
                        <%--18--%>
                        <asp:EditCommandColumn HeaderText="Contrato Modificado" EditText="Modificado" Visible="False"></asp:EditCommandColumn>
                        <%--19--%>
                        <asp:BoundColumn DataField="contrato_original" Visible="false"></asp:BoundColumn>
                        <%--20--%>
                        <asp:BoundColumn DataField="contrato_nuevo" Visible="false"></asp:BoundColumn>
                        <%--21--%>
                        <asp:BoundColumn DataField="punta_solicitud" Visible="false"></asp:BoundColumn>
                        <%--22--%>
                        <asp:BoundColumn DataField="ind_accion" Visible="false"></asp:BoundColumn>
                        <%--23--%>
                        <asp:BoundColumn DataField="tipo_mercado" Visible="false"></asp:BoundColumn>
                        <%--24--%>
                        <asp:BoundColumn DataField="destino_rueda" Visible="false"></asp:BoundColumn>
                        <%--25--%>
                        <asp:BoundColumn DataField="punta" Visible="false"></asp:BoundColumn>
                        <%--26--%>
                        <asp:BoundColumn DataField="codigo_causa" Visible="false"></asp:BoundColumn>
                        <%--27--%>
                        <asp:BoundColumn DataField="codigo_modif" Visible="false"></asp:BoundColumn>
                        <%--28--%>
                        <asp:BoundColumn DataField="codigo_estado" Visible="false"></asp:BoundColumn>
                        <%--29--%>
                        <asp:BoundColumn DataField="tipo_causa" Visible="false"></asp:BoundColumn>
                        <%--30--%>
                        <asp:BoundColumn DataField="ind_otras_mod" Visible="false"></asp:BoundColumn>

                        <asp:BoundColumn DataField="hora_neg" HeaderText="Hora Negociación"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fecha_suscripcion_cont" HeaderText="Fecha Suscripcion" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                        <asp:BoundColumn DataField="conectado_snt" HeaderText="Cnectado SNT"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ent_boca_pozo" HeaderText="Entrega Boca Pozo"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ind_contrato_var" HeaderText="Ind Contrato Variable"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_centro_pob" HeaderText="Cod. centro poblado"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_punto_entrega" HeaderText="Cod. Punto Entrega"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Cod. Modalidad"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_periodo" HeaderText="Cod. Periodo Entrega"></asp:BoundColumn>
                        <asp:BoundColumn DataField="no_años" HeaderText="Número Años"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                        <asp:BoundColumn DataField="hora_inicial" HeaderText="Hora Inicial"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                        <asp:BoundColumn DataField="hora_final" HeaderText="Hora Final"></asp:BoundColumn>
                        <asp:BoundColumn DataField="cantidad" HeaderText="cantidad" DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
                        <asp:BoundColumn DataField="precio" HeaderText="Precio" DataFormatString="{0:###,##0.00}"></asp:BoundColumn>
                        <asp:BoundColumn DataField="sentido_flujo" HeaderText="Sentido Flujo"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Presion_punto_fin" HeaderText="Presión Punto Final"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_fuente" HeaderText="Código Fuente"></asp:BoundColumn>
                        <asp:BoundColumn DataField="tipo_garantia" HeaderText="Tipo Garantía"></asp:BoundColumn>
                        <asp:BoundColumn DataField="valor_garantia" HeaderText="Valor Garantía"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fecha_pago" HeaderText="Fecha Pago"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_depto_punto_sal" HeaderText="Cod. Depto."></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_municipio_pto_sal" HeaderText="Cod. Municipio"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_sector_consumo" HeaderText="Cod. Sector Consumo"></asp:BoundColumn>
                        <asp:BoundColumn DataField="usuario_no_reg_snt" HeaderText="Conexión SNT"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_mercado_relev_sistema" HeaderText="Cod. Mercado Relevante"></asp:BoundColumn>
                        <asp:BoundColumn DataField="capacidad_transporte" HeaderText="Capacidad Transporte"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_ruta_may" HeaderText="Cod. Ruta Mayorista"></asp:BoundColumn>
                        <asp:BoundColumn DataField="observacion" HeaderText="observaciones"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fecha_solicitud" HeaderText="Fecha Solicitud" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_subasta" HeaderText="Tipo Subasta"></asp:BoundColumn>
                        <%--20210707--%>
                        <asp:BoundColumn DataField="codigo_trm" HeaderText="Cod. Tasa Cambio"></asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_trm" HeaderText="Tasa Cambio"></asp:BoundColumn>
                        <asp:BoundColumn DataField="observacion_trm" HeaderText="Observaciones Tasa Cambio"></asp:BoundColumn>
                        <asp:BoundColumn DataField="tipo_moneda" HeaderText="Tipo Moneda"></asp:BoundColumn>
                        <%--fin 20210707--%>

                        <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                            <ItemTemplate>
                                <div class="dropdown dropdown-inline">
                                    <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="flaticon-more-1"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                        <!--begin::Nav-->
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <ul class="kt-nav">
                                                    <li class="kt-nav__item">
                                                        <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Modificar</span>
                                                        </asp:LinkButton>
                                                    </li>
                                                    <li class="kt-nav__item">
                                                        <asp:LinkButton ID="lkbConsultar" CssClass="kt-nav__link" CommandName="Consultar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-expand"></i>
                                                            <span class="kt-nav__link-text">Consultar</span>
                                                        </asp:LinkButton>
                                                    </li>
                                                    <li class="kt-nav__item">
                                                        <asp:LinkButton ID="lkbOriginal" CssClass="kt-nav__link" CommandName="Original" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-crisp-icons"></i>
                                                            <span class="kt-nav__link-text">Original</span>
                                                        </asp:LinkButton>
                                                    </li>
                                                    <li class="kt-nav__item">
                                                        <asp:LinkButton ID="lkbModificado" CssClass="kt-nav__link" CommandName="Modificado" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-notepad"></i>
                                                            <span class="kt-nav__link-text">Modificado</span>
                                                        </asp:LinkButton>
                                                    </li>
                                                </ul>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <!--end::Nav-->
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                </asp:DataGrid>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--Excel--%>
    <asp:DataGrid ID="dtgConsultaExcel" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
        Width="100%" CssClass="table-bordered" Visible="false">
        <Columns>
            <%--2--%>
            <asp:BoundColumn DataField="codigo_solicitud" Visible="false"></asp:BoundColumn>
            <%--3--%>
            <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
            <%--4--%>
            <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
            <%--5--%>
            <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
            <%--6--%>
            <asp:BoundColumn DataField="contrato_definitivo" HeaderText="contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
            <%--7--%>
            <asp:BoundColumn DataField="operador_compra" HeaderText="Cod. Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
            <%--8--%>
            <asp:BoundColumn DataField="nombre_compra" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
            <%--9--%>
            <asp:BoundColumn DataField="operador_venta" HeaderText="Cod. Venta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
            <%--10--%>
            <asp:BoundColumn DataField="nombre_venta" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
            <%--11--%>
            <asp:BoundColumn DataField="desc_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
            <%--12--%>
            <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
            <%--13--%>
            <asp:BoundColumn DataField="codigo_tipo_subasta" Visible="false"></asp:BoundColumn>
            <%--14--%>
            <asp:BoundColumn DataField="ind_contrato_var" HeaderText="Contrato Variable" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
            <%--15--%>
            <asp:BoundColumn DataField="desc_estado" HeaderText="Estado Modificación"></asp:BoundColumn>
            <%--16--%>
            <asp:BoundColumn DataField="desc_causa" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
            <%--19--%>
            <asp:BoundColumn DataField="contrato_original" Visible="false"></asp:BoundColumn>
            <%--20--%>
            <asp:BoundColumn DataField="contrato_nuevo" Visible="false"></asp:BoundColumn>
            <%--21--%>
            <asp:BoundColumn DataField="punta_solicitud" Visible="false"></asp:BoundColumn>
            <%--22--%>
            <asp:BoundColumn DataField="ind_accion" Visible="false"></asp:BoundColumn>
            <%--23--%>
            <asp:BoundColumn DataField="tipo_mercado" Visible="false"></asp:BoundColumn>
            <%--24--%>
            <asp:BoundColumn DataField="destino_rueda" Visible="false"></asp:BoundColumn>
            <%--25--%>
            <asp:BoundColumn DataField="punta" Visible="false"></asp:BoundColumn>
            <%--26--%>
            <asp:BoundColumn DataField="codigo_causa" Visible="false"></asp:BoundColumn>
            <%--27--%>
            <asp:BoundColumn DataField="codigo_modif" Visible="false"></asp:BoundColumn>
            <%--28--%>
            <asp:BoundColumn DataField="codigo_estado" Visible="false"></asp:BoundColumn>
            <%--29--%>
            <asp:BoundColumn DataField="tipo_causa" Visible="false"></asp:BoundColumn>
            <%--30--%>
            <asp:BoundColumn DataField="ind_otras_mod" Visible="false"></asp:BoundColumn>
            <asp:BoundColumn DataField="hora_neg" HeaderText="Hora Negociación"></asp:BoundColumn>
            <asp:BoundColumn DataField="fecha_suscripcion_cont" HeaderText="Fecha Suscripcion" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
            <asp:BoundColumn DataField="conectado_snt" HeaderText="Cnectado SNT"></asp:BoundColumn>
            <asp:BoundColumn DataField="ent_boca_pozo" HeaderText="Entrega Boca Pozo"></asp:BoundColumn>
            <asp:BoundColumn DataField="ind_contrato_var" HeaderText="Ind Contrato Variable"></asp:BoundColumn>
            <asp:BoundColumn DataField="codigo_centro_pob" HeaderText="Cod. centro poblado"></asp:BoundColumn>
            <asp:BoundColumn DataField="codigo_punto_entrega" HeaderText="Cod. Punto Entrega"></asp:BoundColumn>
            <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Cod. Modalidad"></asp:BoundColumn>
            <asp:BoundColumn DataField="codigo_periodo" HeaderText="Cod. Periodo Entrega"></asp:BoundColumn>
            <asp:BoundColumn DataField="no_años" HeaderText="Número Años"></asp:BoundColumn>
            <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
            <asp:BoundColumn DataField="hora_inicial" HeaderText="Hora Inicial"></asp:BoundColumn>
            <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
            <asp:BoundColumn DataField="hora_final" HeaderText="Hora Final"></asp:BoundColumn>
            <asp:BoundColumn DataField="cantidad" HeaderText="cantidad" DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
            <asp:BoundColumn DataField="precio" HeaderText="Precio" DataFormatString="{0:###,##0.00}"></asp:BoundColumn>
            <asp:BoundColumn DataField="sentido_flujo" HeaderText="Sentido Flujo"></asp:BoundColumn>
            <asp:BoundColumn DataField="Presion_punto_fin" HeaderText="Presión Punto Final"></asp:BoundColumn>
            <asp:BoundColumn DataField="codigo_fuente" HeaderText="Código Fuente"></asp:BoundColumn>
            <asp:BoundColumn DataField="tipo_garantia" HeaderText="Tipo Garantía"></asp:BoundColumn>
            <asp:BoundColumn DataField="valor_garantia" HeaderText="Valor Garantía"></asp:BoundColumn>
            <asp:BoundColumn DataField="fecha_pago" HeaderText="Fecha Pago"></asp:BoundColumn>
            <asp:BoundColumn DataField="codigo_depto_punto_sal" HeaderText="Cod. Depto."></asp:BoundColumn>
            <asp:BoundColumn DataField="codigo_municipio_pto_sal" HeaderText="Cod. Municipio"></asp:BoundColumn>
            <asp:BoundColumn DataField="codigo_sector_consumo" HeaderText="Cod. Sector Consumo"></asp:BoundColumn>
            <asp:BoundColumn DataField="usuario_no_reg_snt" HeaderText="Conexión SNT"></asp:BoundColumn>
            <asp:BoundColumn DataField="codigo_mercado_relev_sistema" HeaderText="Cod. Mercado Relevante"></asp:BoundColumn>
            <asp:BoundColumn DataField="capacidad_transporte" HeaderText="Capacidad Transporte"></asp:BoundColumn>
            <asp:BoundColumn DataField="codigo_ruta_may" HeaderText="Cod. Ruta Mayorista"></asp:BoundColumn>
            <asp:BoundColumn DataField="observacion" HeaderText="observaciones"></asp:BoundColumn>
            <asp:BoundColumn DataField="fecha_solicitud" HeaderText="Fecha Solicitud" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
            <asp:BoundColumn DataField="desc_subasta" HeaderText="Tipo Subasta"></asp:BoundColumn>
            <%--20210707--%>
            <asp:BoundColumn DataField="codigo_trm" HeaderText="Cod. Tasa Cambio"></asp:BoundColumn>
            <asp:BoundColumn DataField="desc_trm" HeaderText="Tasa Cambio"></asp:BoundColumn>
            <asp:BoundColumn DataField="observacion_trm" HeaderText="Observaciones Tasa Cambio"></asp:BoundColumn>
            <asp:BoundColumn DataField="tipo_moneda" HeaderText="Tipo Moneda"></asp:BoundColumn>
            <%--fin 20210707--%>
        </Columns>
        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
    </asp:DataGrid>
</div>

<%--Modals--%>

<%--Modal Causa Modifica--%>
<div class="modal fade" id="modContratoCausaMod" tabindex="-1" role="dialog" aria-labelledby="mdlmodContratoCausaModLabel" aria-hidden="true" clientidmode="Static" runat="server">
    <div class="modal-dialog" id="modContratoCausaModInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="mdlmodContratoCausaModLabel">Causa de Modificación</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label Text="Causa de Modificación" AssociatedControlID="DdlIngCausa" runat="server" />
                                    <asp:DropDownList ID="DdlIngCausa" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label Text="Contrato original" AssociatedControlID="FuArchivoOrg" runat="server" />
                                    <ajaxToolkit:AsyncFileUpload ID="FuArchivoOrg" CssClass="form-control" OnUploadedComplete="FileUpload_UploadedComplete" runat="server" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label Text="Contrato Modificación" AssociatedControlID="FuArchivoMod" runat="server" />
                                    <ajaxToolkit:AsyncFileUpload ID="FuArchivoMod" CssClass="form-control" OnUploadedComplete="FileUpload_UploadedComplete" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="BtnCancelar" runat="server" CssClass="btn btn-secondary" data-dismiss="modal" Text="Cancelar" OnClick="BtnCancelar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="BtnAceptar" runat="server" CssClass="btn btn-primary" Text="Aceptar" OnClick="BtnAceptar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>

<%--Modal Modificacion--%>
<div class="modal fade" id="modContratosMod" tabindex="-1" role="dialog" aria-labelledby="mdlModContratosModLabel" aria-hidden="true" clientidmode="Static" style="overflow-y: auto" runat="server">
    <div class="modal-dialog" id="modContratosModInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="mdlModContratosModLabel">Modificación de Registro de Contratos</h5>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <%--Infirmacion Basica--%>
                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5>Información Básica</h5>
                                    </div>
                                    <div class="panel-body">
                                        <%--Contenido--%>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Destino Rueda</label>
                                                    <asp:DropDownList ID="DdlDestino" runat="server" CssClass="form-control" Enabled="false">
                                                        <asp:ListItem Value="G" Text="Suministro de Gas"></asp:ListItem>
                                                        <asp:ListItem Value="T" Text="Capacidad de Transporte"></asp:ListItem>
                                                        <asp:ListItem Value="A" Text="Suministro y Transporte"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Tipo Mercado</label>
                                                    <asp:DropDownList ID="DdlMercado" runat="server" CssClass="form-control" Enabled="false">
                                                        <asp:ListItem Value="P" Text="Primario"></asp:ListItem>
                                                        <asp:ListItem Value="S" Text="Secundario"></asp:ListItem>
                                                        <asp:ListItem Value="O" Text="Otras Transacciones del Mercado Mayorista"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblOperadorC">Operador Compra<%--20190425 rq022-19--%></asp:Label>
                                                    <asp:DropDownList ID="DdlComprador" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                    <asp:TextBox ID="TxNitComprador" runat="server" MaxLength="15" CssClass="form-control" Visible="false"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblOperadorV">Operador Venta<%--20190425 rq022-19--%></asp:Label>
                                                    <asp:DropDownList ID="DdlVendedor" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblFechaNeg" Text="Fecha Negociación" AssociatedControlID="TxtFechaNeg" />
                                                    <asp:TextBox ID="TxtFechaNeg" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="Label1" Text="Hora Negociación" AssociatedControlID="TxtHoraNeg" />
                                                    <asp:TextBox ID="TxtHoraNeg" runat="server" class="form-control" ValidationGroup="detalle" MaxLength="5"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="revTxtHoraNeg" ControlToValidate="TxtHoraNeg" ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ErrorMessage="Formato Incorrecto para la Hora de negociación"> * </asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblFechaSus">Fecha Suscripción<%--20190425 rq022-19--%></asp:Label>
                                                    <asp:TextBox ID="TxtFechaSus" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblContratoDef">Contrato definitivo<%--20190425 rq022-19--%></asp:Label>
                                                    <asp:TextBox ID="TxtContDef" runat="server" class="form-control" ValidationGroup="detalle" MaxLength="30"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblPunto">Punto de entrega de la energía al comprador</asp:Label>
                                                    <asp:DropDownList ID="DdlPunto" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="DdlCentro" runat="server" CssClass="form-control selectpicker" data-live-search="true" Visible="false">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblModalidad">Modalidad de Entrega<%--20190425 rq022-19--%></asp:Label>
                                                    <asp:DropDownList ID="DdlModalidad" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblFechaIni">Fecha Entrega Inicial<%--20190425 rq022-19--%></asp:Label>
                                                    <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%" OnTextChanged="TxtFechaIni_TextChanged"
                                                        AutoPostBack="true"></asp:TextBox>-
                            
                                                   

                                                    <asp:TextBox ID="TxtHoraIni" runat="server" class="form-control" ValidationGroup="detalle" MaxLength="5"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RevTxtHoraIni" ControlToValidate="TxtHoraIni"
                                                        ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                                                        ErrorMessage="Formato Incorrecto para la Hora de entrega inicial"> * </asp:RegularExpressionValidator>
                                                    <asp:HiddenField ID="hdfErrorFecha" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <%--20200831--%>
                                                    <asp:Label runat="server" ID="LblFechaFin">Fecha Entrega Final<%--20190425 rq022-19--%></asp:Label>
                                                    <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%" OnTextChanged="TxtFechaFin_TextChanged"
                                                        AutoPostBack="true"></asp:TextBox>
                                                    <asp:TextBox ID="TxtHoraFin" runat="server" class="form-control" ValidationGroup="detalle" MaxLength="5"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="revTxtHoraFin" ControlToValidate="TxtHoraFin"
                                                        ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                                                        ErrorMessage="Formato Incorrecto para la Hora de entrega final"> * </asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblCantidad">Cantidad de energía contratada (MBTUD)</asp:Label>
                                                    <asp:TextBox ID="TxtCantidad" runat="server" CssClass="form-control" MaxLength="6"></asp:TextBox>
                                                    <asp:CompareValidator ID="CvTxtCantidad" runat="server" ControlToValidate="TxtCantidad"
                                                        Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Cantidad de energía contratada (MBTUD) debe Ser numérico">*</asp:CompareValidator>
                                                    <asp:TextBox ID="TxtCapacOtm" runat="server" CssClass="form-control" MaxLength="6" Visible="false"></asp:TextBox>
                                                    <asp:CompareValidator ID="CvTxtCapacOtm" runat="server" ControlToValidate="TxtCapacOtm"
                                                        Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo >Cantidad de energía contratada (MBTUD) debe Ser numérico">*</asp:CompareValidator>

                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblPrecio">Precio a la fecha de suscripción del contrato (USD/MBTU)</asp:Label>
                                                    <asp:TextBox ID="TxtPrecio" runat="server" CssClass="form-control" MaxLength="6"></asp:TextBox>
                                                    <%--20220828--%>
                                                    <asp:CompareValidator ID="CvTxtPrecio" runat="server" ControlToValidate="TxtPrecio"
                                                        Type="Double" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Precio a la fecha de suscripción del contrato debe Ser numérico">*</asp:CompareValidator>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblContratoVar">Contrato Variable<%--20190425 rq022-19--%></asp:Label>
                                                    <asp:DropDownList ID="DdlVariable" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                                        <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4" runat="server" id="trFuente">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblFuente">Fuente</asp:Label>
                                                    <asp:DropDownList ID="DdlFuente" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <%--20210707--%>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblTrm">Tasa de Cambio</asp:Label>
                                                    <asp:DropDownList ID="ddlTrm" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <%--20210707--%>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblObsTrm">Observaciones Tasa de Cambio</asp:Label>
                                                    <asp:TextBox ID="txtObsTrm" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <%--20210707--%>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblMoneda">Tipo Moneda</asp:Label>
                                                    <asp:DropDownList ID="ddlMoneda" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="USD" Text="USD"></asp:ListItem>
                                                        <asp:ListItem Value="COP" Text="COP"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" runat="server" id="trSumSec" visible="false">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblConectadoSnt">Conectado al SNT<%--20190425 rq022-19--%></asp:Label>
                                                    <asp:DropDownList ID="DdlConectado" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DdlConectado_SelectedIndexChanged">
                                                        <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                                                        <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblEntregaBoca">Entrega en boca de bozo<%--20190425 rq022-19--%></asp:Label>
                                                    <asp:DropDownList ID="DdlBoca" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DdlBoca_SelectedIndexChanged">
                                                        <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                                                        <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" runat="server" id="trPeriodo" visible="false">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblPeriodo">Periodo de Entrega<%--20190425 rq022-19--%></asp:Label>
                                                    <asp:DropDownList ID="DdlPeriodo" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true" OnSelectedIndexChanged="DdlPeriodo_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:HiddenField ID="HndTiempo" runat="server" />
                                                    <asp:HiddenField ID="hdfMesInicialPeriodo" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblNoAños" Visible="false">No Años<%--20190425 rq022-19--%></asp:Label>
                                                    <asp:TextBox ID="TxtNoAños" runat="server" class="form-control" ValidationGroup="detalle" MaxLength="3" Visible="false"></asp:TextBox>
                                                    <asp:CompareValidator ID="CvTxtNoAños" runat="server" ControlToValidate="TxtNoAños"
                                                        Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El CampoNo No. Años debe Ser numérico">*</asp:CompareValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" runat="server" id="trFlujo" visible="false">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblSentido">Sentido del flujo<%--20190425 rq022-19--%></asp:Label>
                                                    <asp:DropDownList ID="DdlSentido" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="NORMAL" Text="NORMAL"></asp:ListItem>
                                                        <%--<asp:ListItem Value="CONTRA FLUJO" Text="CONTRA FLUJO"></asp:ListItem>--%><%--20210915--%>
                                                        <asp:ListItem Value="CONTRAFLUJO" Text="CONTRA FLUJO"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblPresion">Presión del punto final</asp:Label>
                                                    <asp:TextBox ID="TxtPresion" runat="server" CssClass="form-control" MaxLength="500" Text="0"></asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtPresion" runat="server" Enabled="True" FilterType="Custom, Numbers"
                                                        TargetControlID="TxtPresion" ValidChars="-"></ajaxToolkit:FilteredTextBoxExtender>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" runat="server" id="trMayor1" visible="false">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblGarantia">Tipo Garantía</asp:Label>
                                                    <asp:TextBox ID="TxtGarantia" runat="server" CssClass="form-control" MaxLength="500"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblValorGrtia">Valor Garantía</asp:Label>
                                                    <asp:TextBox ID="TxtVlrGrtia" runat="server" CssClass="form-control" MaxLength="500"></asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FtebTxtVlrGrtia" runat="server" Enabled="True" FilterType="Custom, Numbers"
                                                        TargetControlID="TxtVlrGrtia"></ajaxToolkit:FilteredTextBoxExtender>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblFechaPagoGrtia">Fecha pago garantía</asp:Label>
                                                    <asp:TextBox ID="TxtFechaGrtia" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" runat="server" id="trMayor2" visible="false">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblSectorCon">Sector Consumo</asp:Label>
                                                    <asp:DropDownList ID="ddlSectorConsumo" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblUsuaroNoReg">Usuario no Regulado con Conexion a SNT</asp:Label>
                                                    <asp:DropDownList ID="ddlConexion" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlConexion_SelectedIndexChanged"
                                                        AutoPostBack="true">
                                                        <asp:ListItem Value="S">Si</asp:ListItem>
                                                        <asp:ListItem Value="N">No</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group" runat="server" id="tdMayT01">
                                                    <asp:Label runat="server" ID="LblRutaMay">Código ruta mayorista</asp:Label>
                                                    <asp:DropDownList ID="ddlRutaMay" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" runat="server" id="trMayor3" visible="false">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblDepto">Departamento punto de salida</asp:Label>
                                                    <asp:DropDownList ID="ddlDeptoPuntoSal" runat="server" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="ddlDeptoPuntoSal_SelectedIndexChanged"
                                                        AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblMunicipio">Municipio punto de salida</asp:Label>
                                                    <asp:DropDownList ID="DdlMunPuntoSal" runat="server" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="DdlMunPuntoSal_SelectedIndexChanged"
                                                        AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4" id="tdCentro1" runat="server">
                                                <%--20200831 --%>
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblCentro">Centro Poblado</asp:Label>
                                                    <asp:DropDownList ID="ddlCentroPuntoSal" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" runat="server" id="trMayor4" visible="false">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="LblMercadoRel">Mercado Relevante</asp:Label>
                                                    <asp:DropDownList ID="ddlMercadoRelevante" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" id="trObs" runat="server" visible="false">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Observaciones</label>
                                                    <asp:TextBox ID="TxtObs" runat="server" CssClass="form-control" MaxLength="1000" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <%--Grilla--%>
                                        <div class="table table-responsive" runat="server" id="trReestructura" visible="false">
                                            <asp:DataGrid ID="dtgReestructura" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                                Width="100%" CssClass="table-bordered" OnItemCommand="dtgReestructura_EditCommand">
                                                <Columns>
                                                    <%--0--%>
                                                    <asp:EditCommandColumn HeaderText="Consultar" EditText="consultar" Visible="False" />
                                                    <%--1--%>
                                                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--2--%>
                                                    <asp:BoundColumn DataField="codigo_verif" HeaderText="id Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--3--%>
                                                    <asp:BoundColumn DataField="consec_enlace" Visible="false"></asp:BoundColumn>
                                                    <%--4--%>
                                                    <asp:BoundColumn DataField="contrato_definitivo" HeaderText="contrato"
                                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                                                    <%--5--%>
                                                    <asp:BoundColumn DataField="operador_compra" HeaderText="comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--6--%>
                                                    <asp:BoundColumn DataField="operador_venta" HeaderText="vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--7--%>
                                                    <asp:BoundColumn DataField="tipo_mercado" HeaderText="Tipo mercado" ItemStyle-HorizontalAlign="Left"
                                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                                    <%--8--%>
                                                    <asp:BoundColumn DataField="destino_rueda" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"
                                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                                    <%--9--%>
                                                    <asp:BoundColumn DataField="cantidad" HeaderText="cantidad" ItemStyle-HorizontalAlign="Right"
                                                        DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
                                                    <%--10--%>
                                                    <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                                        DataFormatString="{0:###,###,###,##0.00}"></asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                                        <ItemTemplate>
                                                            <div class="dropdown dropdown-inline">
                                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="flaticon-more-1"></i>
                                                                </button>
                                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                                    <!--begin::Nav-->
                                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                                        <ContentTemplate>
                                                                            <ul class="kt-nav">
                                                                                <li class="kt-nav__item">
                                                                                    <asp:LinkButton ID="lkbConsultarM" CssClass="kt-nav__link" CommandName="Consultar" runat="server">
                                                                                        <i class="kt-nav__link-icon flaticon2-expand"></i>
                                                                                        <span class="kt-nav__link-text">Consultar</span>
                                                                                    </asp:LinkButton>
                                                                                </li>
                                                                            </ul>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                    <!--end::Nav-->
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" runat="server" id="trCambioUsrFin" visible="false">
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label ID="LblModUsrFin" runat="server" ForeColor="Red" CssClass="form-control">LOS USUARIOS FINALES HAN SIDO MODIFICADOS</asp:Label>
                                    </div>
                                </div>
                            </div>

                            <%--Demanda--%>
                            <div id="tblDemanda" visible="false" runat="server">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5>Usuarios Finales</h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Tipo Demanda a Atender</label>
                                                    <asp:DropDownList ID="dlTipoDemanda" runat="server" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="dlTipoDemanda_SelectedIndexChanged" AutoPostBack="true" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Usuario Final</label>
                                                    <asp:TextBox ID="TxtUsuarioFinal" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <ajaxToolkit:AutoCompleteExtender runat="server" BehaviorID="AutoCompleteExUsuF" ID="AutoCompleteExtender1"
                                                        TargetControlID="TxtUsuarioFinal" ServicePath="~/WebService/AutoComplete.asmx"
                                                        ServiceMethod="GetCompletionListUsuarioFinal" MinimumPrefixLength="3" CompletionInterval="1000"
                                                        EnableCaching="true" CompletionSetCount="20" CompletionListCssClass="autocomplete_completionListElement"
                                                        CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                        DelimiterCharacters=";,:">
                                                        <Animations>
                        <OnShow>
                            <Sequence>
                                <OpacityAction Opacity="0" />
                                <HideAction Visible="true" />
                                <ScriptAction Script="
                                    // Cache the size and setup the initial size
                                    var behavior = $find('AutoCompleteExUsuF');
                                    if (!behavior._height) {
                                        var target = behavior.get_completionList();
                                        behavior._height = target.offsetHeight - 2;
                                        target.style.height = '0px';
                                    }" />
                                <Parallel Duration=".4">
                                    <FadeIn />
                                    <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteExUsuF')._height" />
                                </Parallel>
                            </Sequence>
                        </OnShow>
                        <OnHide>
                            <Parallel Duration=".4">
                                <FadeOut />
                                <Length PropertyKey="height" StartValueScript="$find('AutoCompleteExUsuF')._height" EndValue="0" />
                            </Parallel>
                        </OnHide>
                                                        </Animations>
                                                    </ajaxToolkit:AutoCompleteExtender>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="lblSector">Sector Consumo Usuario Regulado</asp:Label>
                                                    <asp:DropDownList ID="ddlSector" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Punto de Salida en SNT</label>
                                                    <asp:DropDownList ID="ddlPuntoSalida" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                    <asp:HiddenField ID="hdfTipoDemanda" runat="server" />
                                                    <asp:HiddenField ID="hdfCodDatUsu" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label>Mercado Relevante</label>
                                                    <asp:DropDownList ID="ddlMercadoRel" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label runat="server" ID="lblCantContra">Cantidad a entregar</asp:Label>
                                                    <asp:TextBox ID="TxtCantidadUsu" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4" runat="server" id="TdIndica">
                                                <div class="form-group">
                                                    <asp:Label runat="server">Equivalente Kpcd</asp:Label>
                                                    <asp:TextBox ID="TxtEquivaleKpcd" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <asp:Button ID="btnCrearUsu" runat="server" CssClass="btn btn-primary btn-block" Text="Agregar Usuario" OnClick="btnCrearUsu_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" Visible="false" />
                                        <asp:Button ID="btnActualUsu" runat="server" CssClass="btn btn-primary btn-block" Text="Actualiza Usuario" OnClick="btnActualUsu_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" Visible="false" />

                                        <hr>

                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <b>TOTAL CANTIDAD:</b><asp:Label ID="lblTotlCantidad" runat="server" ForeColor="Red"
                                                        Font-Bold="true"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table table-responsive">
                                            <asp:DataGrid ID="dtgUsuarios" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                                Width="100%" CssClass="table-bordered" OnItemCommand="dtgUsuarios_EditCommand">
                                                <Columns>
                                                    <%--0--%>
                                                    <asp:BoundColumn DataField="tipo_demanda" HeaderText="Tipo Demanda Atender" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--1--%>
                                                    <asp:BoundColumn DataField="no_identificacion_usr" HeaderText="Identificación Usuario Final"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--2--%>
                                                    <asp:BoundColumn DataField="tipo_documento" HeaderText="Tipo Identificación Usuario Final"
                                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                                                    <%--3--%>
                                                    <asp:BoundColumn DataField="nombre_usuario" HeaderText="Nombre Usuario Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--4--%>
                                                    <asp:BoundColumn DataField="sector_consumo" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"
                                                        ItemStyle-Width="80px"></asp:BoundColumn>
                                                    <%--5--%>
                                                    <asp:BoundColumn DataField="punto_salida" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"
                                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                                    <%--6--%>
                                                    <asp:BoundColumn DataField="cantidad_contratada" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Left"
                                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                                    <%--7--%>
                                                    <asp:BoundColumn DataField="equivalente_kpcd" HeaderText="Equivalente Kpcd" ItemStyle-HorizontalAlign="Left"
                                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                                    <%--8--%>
                                                    <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" Visible="False" />
                                                    <%--9--%>
                                                    <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar" Visible="False" />
                                                    <%--10--%>
                                                    <asp:BoundColumn DataField="codigo_cont_usr" Visible="false"></asp:BoundColumn>
                                                    <%--11--%>
                                                    <asp:BoundColumn DataField="codigo_modif" Visible="false"></asp:BoundColumn>
                                                    <%--12--%>
                                                    <asp:BoundColumn DataField="codigo_sector_consumo" Visible="false"></asp:BoundColumn>
                                                    <%--13--%>
                                                    <asp:BoundColumn DataField="codigo_punto_salida" Visible="false"></asp:BoundColumn>
                                                    <%--14--%>
                                                    <asp:BoundColumn DataField="codigo_tipo_doc" Visible="false"></asp:BoundColumn>
                                                    <%--15--%>
                                                    <asp:BoundColumn DataField="codigo_tipo_demanda" Visible="false"></asp:BoundColumn>
                                                    <%--17--%>
                                                    <asp:BoundColumn DataField="codigo_mercado_relevante" Visible="false"></asp:BoundColumn>
                                                    <%--18--%>
                                                    <asp:BoundColumn DataField="codigo_cont_usr" Visible="false"></asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                                        <ItemTemplate>
                                                            <div class="dropdown dropdown-inline">
                                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="flaticon-more-1"></i>
                                                                </button>
                                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                                    <!--begin::Nav-->
                                                                    <ul class="kt-nav">
                                                                        <li class="kt-nav__item">
                                                                            <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                                                         <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                                                        <span class="kt-nav__link-text">Modificar</span>
                                                                            </asp:LinkButton>
                                                                        </li>
                                                                        <li class="kt-nav__item">
                                                                            <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                                                        <i class="kt-nav__link-icon flaticon-delete"></i>
                                                                                        <span class="kt-nav__link-text">Eliminar</span>
                                                                            </asp:LinkButton>
                                                                        </li>
                                                                    </ul>
                                                                    <!--end::Nav-->
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnCancelarM" runat="server" class="btn btn-secondary" Text="Cancelar" OnClick="btnCancelarM_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnModificar" runat="server" CssClass="btn btn-primary" Text="Modificar" OnClick="btnModificar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnAprobar" runat="server" CssClass="btn btn-primary" Text="Aprobar" OnClick="btnAprobar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnRechazar" runat="server" CssClass="btn btn-primary" Text="Rechazar" OnClick="btnRechazar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnAprobarBmc" runat="server" CssClass="btn btn-primary" Text="Aprobar BMC" OnClick="btnAprobarBmc_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnRechazarBmc" runat="server" CssClass="btn btn-primary" Text="Rechazar" OnClick="btnRechazarBmc_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
