﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace UsersControls.Contratos.Registro_Contratos.Registro_Contratos_Extemporaneo
{
    public partial class WucContratoBilateralExt : UserControl
    {
        #region EventHandler

        /// <summary>
        /// Delegado para el manejo de los modals
        /// </summary>
        public delegate void EventHandlerModal(string id, string insideId, EnumTypeModal typeModal, Modal.Size size = Modal.Size.Large);

        /// <summary>
        /// EventHandler para la selección de los los modals
        /// </summary>
        public event EventHandlerModal ModalEvent;

        /// <summary>
        /// Delegado para el manejo de los toastr
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        /// EventHandler para la selección de los toastr
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        /// Delegado para la selección de los botones    
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get { return ViewState["NameController"] != null && !string.IsNullOrEmpty(ViewState["NameController"].ToString()) ? ViewState["NameController"].ToString() : string.Empty; }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        #endregion Propiedades

        InfoSessionVO goInfo = null;
        static string lsTitulo = "Contrato Bilateral Extemporáneo";
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        clConexion lConexion = null;
        clConexion lConexion1 = null;
        SqlDataReader lLector;
        string gsTabla = "";
        static Dictionary<string, id_bilateral> lista_bil;

        /// <summary>
        /// 
        /// </summary>
        private string lsIndica
        {
            get
            {
                if (ViewState["lsIndica"] == null)
                    return "";
                else
                    return (string)ViewState["lsIndica"];
            }
            set
            {
                ViewState["lsIndica"] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lConexion = new clConexion(goInfo);
            lConexion1 = new clConexion(goInfo);
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            try
            {
                var lControl = new clControlUsuario();
                if (!lControl.validaAcceso("Carga Archivo Negociación Bilateral Extemporánea", goInfo.Usuario, goInfo))
                {
                    ToastrEvent?.Invoke("El Usuario No tiene Permisos para Acceder a esta Pantalla.!", EnumTypeToastr.Warning);
                }
                else
                {

                    var botones = new[] { EnumBotones.Buscar, EnumBotones.Crear }.OfType<EnumBotones>().ToList();

                    lista_bil = new Dictionary<string, id_bilateral>();

                    //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
                    lConexion.Abrir();
                    LlenarControles2(lConexion.gObjConexion, ddlBusComprador, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4);
                    LlenarControles2(lConexion.gObjConexion, ddlBusVendedor, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4);
                    LlenarControles2(lConexion.gObjConexion, ddlBusContra, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4);
                    LlenarControles(lConexion.gObjConexion, ddlBusModalidad, "m_modalidad_contractual", " estado = 'A' order by descripcion", 0, 1);
                    LlenarControles(lConexion.gObjConexion, ddlBusModalidad1, "m_modalidad_contractual", " estado = 'A' order by descripcion", 0, 1);
                    LlenarControles(lConexion.gObjConexion, ddlBusEstado, "m_estado_gas", " tipo_estado='V' and sigla_estado <> 'M' order by descripcion_estado", 2, 3);
                    LlenarControles2(lConexion.gObjConexion, DdlOperador, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4);
                    LlenarControles(lConexion.gObjConexion, DdlFuente, "m_pozo", " ind_campo_pto='C' and estado ='A' order by descripcion", 0, 1);
                    LlenarControles1(lConexion.gObjConexion, DdlCentro, "m_divipola", " codigo_centro <> '0' order by nombre_centro", 5, 6);
                    LlenarControles(lConexion.gObjConexion, DdlModalidad, "m_modalidad_contractual", " estado = 'A' order by descripcion", 0, 1);
                    LlenarControles(lConexion.gObjConexion, DdlPeriodo, "m_periodos_entrega", " estado = 'A' order by descripcion", 0, 1);
                    LlenarControles(lConexion.gObjConexion, DdlPunto, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
                    LlenarControles(lConexion.gObjConexion, DdlClasifica, "m_clasifica_transporte", " 1=1  order by codigo_clasificacion", 0, 1);//20201124 sub tansporte
                    LlenarControles(lConexion.gObjConexion, ddlTrm, "m_tipo_trm", " estado ='A'  order by descripcion", 0, 1);//20210707 trm moneda
                    lConexion.Cerrar();
                    if (goInfo.cod_comisionista == "0")
                    {
                        //Grupo 2
                        divComprador.Visible = true;
                        divVendedor.Visible = true;
                        divMercado.Visible = true;
                        divProducto.Visible = true;
                        divModalidad.Visible = true;
                        //Grupo 3
                        divContraparte.Visible = false;
                        divMercado2.Visible = false;
                        divProducto2.Visible = false;
                        divModalidad2.Visible = false;
                        divEstado.Visible = false;
                        botones = (new[] { EnumBotones.Buscar }).OfType<EnumBotones>().ToList();
                    }
                    else
                    {
                        //Grupo 2
                        divComprador.Visible = false;
                        divVendedor.Visible = false;
                        divMercado.Visible = false;
                        divProducto.Visible = false;
                        divModalidad.Visible = false;
                        //Grupo 3
                        divContraparte.Visible = true;
                        divMercado2.Visible = true;
                        divProducto2.Visible = true;
                        divModalidad2.Visible = true;
                        divEstado.Visible = true;
                    }

                    ButtonsEvent?.Invoke(botones.ToArray(), null, NameController);
                }
                //Se establece que el controlador ya se inicializo    
                Inicializado = true;
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            var lblMensaje = new StringBuilder();
            DateTime ldFecha;
            int liValor = 0;
            string[] lsNombreParametros = { "@P_contrato_ini", "@P_contrato_fin", "@P_fecha_ini", "@P_fecha_fin", "@P_contrato_definitivo",
                                        "@P_codigo_comprador", "@P_codigo_vendedor", "@P_codigo_operador", "@P_codigo_contraparte",
                                        "@P_tipo_mercado", "@P_destino_rueda","@P_codigo_modalidad","@P_estado"
        };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                                        SqlDbType.Int, SqlDbType.Int, SqlDbType.Int,  SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar};
            string[] lValorParametros = { "0", "0", "", "", "", "0", "0", "0", "0", "", "", "0", "0" };

            if (TxtBusFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtBusFechaIni.Text);
                }
                catch (Exception ex)
                {

                    lblMensaje.Append("Formato Inválido en el Campo Fecha Inicial de Negociación. <br>");
                }
            }
            if (TxtBusFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtBusFechaFin.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo Fecha Final de Negociación. <br>");
                }
            }
            if (TxtBusFechaIni.Text.Trim().Length == 0 && TxtBusFechaFin.Text.Trim().Length > 0)
                lblMensaje.Append("Debe digitar la Fecha Inicial de Negociación antes que la final. <br>");

            if (TxtBusFechaIni.Text.Trim().Length > 0 && TxtBusFechaFin.Text.Trim().Length > 0)
                try
                {
                    if (Convert.ToDateTime(TxtFechaFin.Text) < Convert.ToDateTime(TxtFechaIni.Text))
                        lblMensaje.Append("La Fecha inicial de Negociación debe ser menor o igual que la fecha final. <br>");
                }
                catch (Exception ex)
                {
                }
            if (TxtBusContratoIni.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtBusContratoIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo No Operación inicial. <br>");
                }

            }
            if (TxtBusContratoFin.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtBusContratoFin.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo No Operación final. <br>");
                }

            }
            if (TxtBusContratoIni.Text.Trim().Length == 0 && TxtBusContratoFin.Text.Trim().Length > 0)
            {
                lblMensaje.Append("Debe digitar el No de operación inicial antes que el final. <br>");
            }
            if (TxtBusContratoIni.Text.Trim().Length > 0 && TxtBusContratoFin.Text.Trim().Length > 0)
            {
                try
                {
                    if (Convert.ToInt32(TxtBusContratoFin.Text) < Convert.ToInt32(TxtBusContratoIni.Text))
                        lblMensaje.Append("El No Operación inicial debe ser menor o igual que el final. <br>");
                }
                catch (Exception ex)
                {

                }
            }

            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                return;
            }

            try
            {
                if (TxtBusContratoIni.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtBusContratoIni.Text.Trim();
                else
                    lValorParametros[0] = "0";
                if (TxtBusContratoFin.Text.Trim().Length > 0)
                    lValorParametros[1] = TxtBusContratoFin.Text.Trim();
                else
                {
                    if (TxtBusContratoIni.Text.Trim().Length > 0)
                        lValorParametros[1] = TxtBusContratoIni.Text.Trim();
                    else
                        lValorParametros[1] = "0";
                }

                lValorParametros[2] = TxtBusFechaIni.Text.Trim();
                lValorParametros[3] = TxtBusFechaFin.Text.Trim();
                if (TxtBusFechaIni.Text.Trim() != "" && TxtBusFechaFin.Text.Trim() == "")
                    lValorParametros[3] = TxtBusFechaIni.Text.Trim();
                lValorParametros[4] = TxtBusContDef.Text.Trim();
                lValorParametros[5] = ddlBusComprador.SelectedValue;
                lValorParametros[6] = ddlBusVendedor.SelectedValue;
                lValorParametros[7] = goInfo.cod_comisionista;
                lValorParametros[8] = ddlBusContra.SelectedValue;
                if (goInfo.cod_comisionista == "0")
                {
                    lValorParametros[9] = ddlBusMercado.SelectedValue;
                    lValorParametros[10] = ddlBusProducto.SelectedValue;
                    lValorParametros[11] = ddlBusModalidad.SelectedValue;
                    lValorParametros[12] = "0";
                }
                else
                {
                    lValorParametros[9] = ddlBusMercado1.SelectedValue;
                    lValorParametros[10] = ddlBusProducto1.SelectedValue;
                    lValorParametros[11] = ddlBusModalidad1.SelectedValue;
                    lValorParametros[12] = ddlBusEstado.SelectedValue;
                }

                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion,
                    "pa_GetContratoBilateral", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                lConexion.Cerrar();
                if (dtgConsulta.Items.Count > 0)
                {
                    tblGrilla.Visible = true;
                }
                else
                {
                    tblGrilla.Visible = false;
                    ToastrEvent?.Invoke("No se encontraron Registros", EnumTypeToastr.Info);
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("No se Pudo consultar la información.! " + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
        {
            var valido = true;
            if (e.CommandName.Equals("Consultar"))
            {
                valido = traer_id(e.Item.Cells[2].Text);
                if (valido)
                {
                    btnInsertar.Visible = false;
                    btnCrear.Visible = false;
                    btnModificar.Visible = false;
                    btnLimpiar.Visible = false;
                    //Modal de agregar
                    ModalEvent?.Invoke(mdlContAgregar.ID, mdlContAgregarInside.ID, EnumTypeModal.Abrir);
                }

                //Se actualiza el titulo del modal
                mdlContAgregarLabel.InnerText = "Consultar";
            }
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool traer_id(string liId)
        {
            var valido = true;
            try
            {
                lConexion.Abrir();
                string[] lsNombreParametros = { "@P_numero_id" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int };
                string[] lValorParametros = { liId };
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion,
                    "pa_GetIdRueda", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    DdlDestino.SelectedValue = lLector["destino_rueda"].ToString();
                    DdlDestino_SelectedIndexChanged(null, null);
                    DdlMercado.SelectedValue = lLector["tipo_mercado"].ToString();
                    DdlMercado_SelectedIndexChanged(null, null);
                    DdlOperador.SelectedValue = lLector["operador_compra"].ToString();
                    TxtFechaNeg.Text = lLector["fecha_negociacion"].ToString();
                    TxtHoraNeg.Text = lLector["hora_negociacion"].ToString();
                    TxtFechaSus.Text = lLector["fecha_sus"].ToString();
                    try
                    {
                        DdlConectado.SelectedValue = lLector["conectado_snt"].ToString();
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        DdlConectado_SelectedIndexChanged(null, null);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        DdlBoca.SelectedValue = lLector["ent_boca_pozo"].ToString();
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        DdlBoca_SelectedIndexChanged(null, null);
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        DdlCentro.SelectedValue = lLector["codigo_centro"].ToString();
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        DdlPunto.SelectedValue = lLector["codigo_punto"].ToString();
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        DdlModalidad.SelectedValue = lLector["codigo_modalidad"].ToString();
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        DdlPeriodo.SelectedValue = lLector["codigo_periodo"].ToString();
                        DdlPeriodo_SelectedIndexChanged(null, null);
                    }
                    catch (Exception ex)
                    {
                    }

                    TxtNoAños.Text = lLector["no_años"].ToString();
                    TxtFechaIni.Text = lLector["fecha_ini"].ToString();
                    TxtHoraIni.Text = lLector["hora_fin"].ToString();
                    TxtFechaFin.Text = lLector["fecha_fin"].ToString();
                    TxtHoraFin.Text = lLector["hora_fin"].ToString();
                    TxtPrecio.Text = lLector["precio_adjudicado"].ToString();
                    TxtCantidad.Text = lLector["cantidad_adjudicada"].ToString();
                    if (DdlMercado.SelectedValue == "P" && DdlDestino.SelectedValue == "T")
                    {
                        try
                        {
                            DdlSentido.SelectedValue = lLector["sentido_flujo"].ToString();
                        }
                        catch (Exception ex)
                        {
                        }

                        TxtPresion.Text = lLector["presion_punto_fin"].ToString();
                    }
                    else
                    {
                        TxtPresion.Text = "0";
                    }

                    try
                    {
                        DdlFuente.SelectedValue = lLector["codigo_fuente"].ToString();
                    }
                    catch (Exception ex)
                    {
                    }

                    TxtContratoDef.Text = lLector["contrato_definitivo"].ToString();
                    try
                    {
                        DdlVariable.SelectedValue = lLector["ind_contrato_var"].ToString();
                    }
                    catch (Exception ex)
                    {
                    }
                    //20201124
                    try
                    {
                        DdlClasifica.SelectedValue = lLector["clasificacion_transp"].ToString();
                    }
                    catch (Exception ex)
                    {
                    }
                    //20210707 trm moneda
                    try
                    {
                        ddlTrm.SelectedValue = lLector["codigo_trm"].ToString();
                    }
                    catch (Exception ex)
                    {
                    }
                    TxtObsTrm.Text = lLector["observacion_trm"].ToString();
                    try
                    {
                        ddlMoneda.SelectedValue = lLector["tipo_moneda"].ToString();
                    }
                    catch (Exception ex)
                    {
                    }
                    //20210707 fin trm moneda
                    lLector.Close();
                    lLector.Dispose();
                }
                else
                {
                    ToastrEvent?.Invoke("No hay información del Id", EnumTypeToastr.Error);
                    valido = false;
                }

                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lConexion.Cerrar();
                ToastrEvent?.Invoke("No se pudo consultar el registro. " + ex.Message, EnumTypeToastr.Error); //20210707 trm moneda
                valido = false;
            }

            return valido;
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='t_contrato' and llave_registro='numero_conrato=" + lscodigo_registro + "'";
            string lsCondicion1 = "numero_contrato=" + lscodigo_registro;
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_contrato";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_contrato", lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// Nombre: imbCrear_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Crear.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCrear_Click(object sender, EventArgs e)
        {
            calcula_fecha();
            hndIndica.Value = "1";
            crear();
        }

        /// <summary>
        /// Nombre: imbCrear_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Crear.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnModificar_Click(object sender, EventArgs e)
        {
            calcula_fecha();
            var lblMensaje = new StringBuilder();
            string[] lsNombreParametros = { "@P_numero_contrato", "@P_destino_rueda","@P_tipo_mercado", "@P_codigo_op_contrario","@P_fecha_negociacion","@P_hora_negociacion","@P_fecha_suscripcion", "@P_conectado_snt",
                "@P_ent_boca_pozo", "@P_codigo_centro", "@P_codigo_punto_ent","@P_codigo_modalidad","@P_codigo_periodo_ent","@P_no_anos","@P_fecha_inicial","@P_hora_inicial","@P_fecha_final",
                "@P_hora_final","@P_precio","@P_cantidad","@P_sentido_flujo","@P_presion_punto_fin", "@P_codigo_fuente", "@P_contrato_definitivo", "@P_ind_contrato_var",
                "@P_clasificacion_transp", "@P_contratos_ms_tra",  //20201124
                "@P_codigo_trm","@P_observacion_trm","@P_tipo_moneda" }; //20210707 trm moneda
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.Decimal,SqlDbType.Decimal,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int, SqlDbType.VarChar,SqlDbType.VarChar,
                                        SqlDbType.Int, SqlDbType.VarChar, //20201124
                                        SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar};//20210707 trm moneda
            string[] lValorParametros = {"0", "", "", "0", "", "", "", "", "", "", "0", "0", "0","0", "", "", "", "", "0", "0", "", "", "0", "", "",DdlClasifica.SelectedValue ,TxtIdTraMp.Text,  //20201124
                            ddlTrm.SelectedValue, TxtObsTrm.Text, ddlMoneda.SelectedValue //20210707 trm moneda
                                    };

            var invalido = valida_datos();

            try
            {
                if (invalido) return;
                lValorParametros[0] = hndContrato.Value;
                lValorParametros[1] = DdlDestino.SelectedValue;
                lValorParametros[2] = DdlMercado.SelectedValue;
                lValorParametros[3] = DdlOperador.SelectedValue;
                lValorParametros[4] = TxtFechaNeg.Text;
                lValorParametros[5] = TxtHoraNeg.Text;
                lValorParametros[6] = TxtFechaSus.Text.Trim();
                lValorParametros[7] = DdlConectado.SelectedValue;
                lValorParametros[8] = DdlBoca.SelectedValue;
                lValorParametros[9] = DdlCentro.SelectedValue;
                lValorParametros[10] = DdlPunto.SelectedValue;
                lValorParametros[11] = DdlModalidad.SelectedValue;
                lValorParametros[12] = DdlPeriodo.SelectedValue;
                lValorParametros[13] = TxtNoAños.Text.Trim();
                lValorParametros[14] = TxtFechaIni.Text.Trim();
                lValorParametros[15] = TxtHoraIni.Text.Trim();
                lValorParametros[16] = TxtFechaFin.Text.Trim();
                lValorParametros[17] = TxtHoraFin.Text.Trim();
                lValorParametros[18] = TxtPrecio.Text.Trim();
                lValorParametros[19] = TxtCantidad.Text.Trim();
                if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "P")
                    lValorParametros[20] = DdlSentido.SelectedValue;
                else
                    lValorParametros[20] = "";
                lValorParametros[21] = TxtPresion.Text.Trim();
                lValorParametros[22] = DdlFuente.SelectedValue;
                lValorParametros[23] = TxtContratoDef.Text.Trim();
                lValorParametros[24] = DdlVariable.SelectedValue;
                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_UptContratoBilateral", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Append(lLector["error"] + "<br>");
                }
                else
                {
                    ToastrEvent?.Invoke("Operación modificada correctamente", EnumTypeToastr.Success);
                    if (hndIndModif.Value == "1")
                    {
                        CargarDatos();
                    }
                }
                lConexion.Cerrar();

                if (string.IsNullOrEmpty(lblMensaje.ToString())) return;
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Success);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.ToString(), EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: imbCrear_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Crear.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private bool valida_datos()
        {
            DateTime ldFecha;
            DateTime ldFechaI = DateTime.Now;
            int liValor = 0;
            decimal ldValor;
            string[] lsPrecio;

            var invalido = false;

            if (DdlOperador.SelectedValue == "0")
                ToastrEvent?.Invoke("Debe seleccionar el operador contraparte.", EnumTypeToastr.Error);
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaNeg.Text.Trim());
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Fecha de negociación inválida", EnumTypeToastr.Error);
                invalido = true;
            }
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaSus.Text.Trim());
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Fecha de suscripción inválida", EnumTypeToastr.Error);
                invalido = true;
            }
            if (DdlModalidad.SelectedValue == "0")
            {
                ToastrEvent?.Invoke("Debe seleccionar la modalidad del contrato", EnumTypeToastr.Error);
                invalido = true;
            }
            if (DdlPeriodo.SelectedValue == "0")
            {
                ToastrEvent?.Invoke("Debe seleccionar el periodo de entrega", EnumTypeToastr.Error);
                invalido = true;
            }
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIni.Text.Trim());
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Fecha de entrega inicial inválida", EnumTypeToastr.Error);
                invalido = true;
            }

            if (TxtCantidad.Text.Trim().Length <= 0)
            {
                ToastrEvent?.Invoke("Debe Ingresar la cantidad", EnumTypeToastr.Error);
                invalido = true;
            }
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCantidad.Text);
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Valor Inválido en la cantidad/capacidad", EnumTypeToastr.Error);
                    invalido = true;
                }
            }
            if (TxtPrecio.Text.Trim().Length <= 0)
            {
                ToastrEvent?.Invoke("Debe Ingresar el precio", EnumTypeToastr.Error);
                invalido = true;
            }
            else
            {
                try
                {
                    ldValor = Convert.ToDecimal(TxtPrecio.Text.Trim());
                    if (ldValor < 0)
                    {
                        ToastrEvent?.Invoke("Valor Inválido en el precio", EnumTypeToastr.Error);
                        invalido = true;
                    }
                    else
                    {
                        lsPrecio = TxtPrecio.Text.Trim().Split('.');
                        if (lsPrecio.Length > 1)
                        {
                            if (lsPrecio[1].Trim().Length > 2)
                            {
                                ToastrEvent?.Invoke("Valor Inválido en el precio", EnumTypeToastr.Error);
                                invalido = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Valor Inválido en el precio", EnumTypeToastr.Error);
                    invalido = true;
                }
            }
            if (TxtContratoDef.Text.Trim().Length <= 0)
            {
                ToastrEvent?.Invoke("Debe Ingresar el número del contrato definitivo", EnumTypeToastr.Error);
                invalido = true;
            }
            if (DdlFuente.Visible && DdlFuente.SelectedValue == "0")
            {
                ToastrEvent?.Invoke("Debe seleccionar la fuente", EnumTypeToastr.Error);
                invalido = true;
            }
            if (DdlDestino.SelectedValue == "G")
            {
                if (DdlPunto.Visible && DdlPunto.SelectedValue == "0")
                {
                    ToastrEvent?.Invoke("Debe seleccionar el punto de entrega", EnumTypeToastr.Error);
                    invalido = true;
                }
                if (DdlCentro.Visible && DdlCentro.SelectedValue == "0")
                {
                    ToastrEvent?.Invoke("Debe seleccionar el centro poblado", EnumTypeToastr.Error);
                    invalido = true;
                }
            }
            else
                if (DdlPunto.SelectedValue == "0")
            {
                ToastrEvent?.Invoke("Debe seleccionar el tramo o grupo de gasoductos", EnumTypeToastr.Error);
                invalido = true;
            }

            if (TxtNoAños.Visible && TxtNoAños.Text == "")
            {
                ToastrEvent?.Invoke("Debe digitar el número de años", EnumTypeToastr.Error);
                invalido = true;
            }
            if (TxtNoAños.Text == "")
                TxtNoAños.Text = "0";
            if (divHoraIni.Visible && TxtHoraIni.Text == "")
            {
                ToastrEvent?.Invoke("Debe digitar la hora inicial", EnumTypeToastr.Error);
                invalido = true;
            }

            if (TxtFechaFin.Enabled)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaFin.Text.Trim());
                    if (ldFechaI > ldFecha)
                    {
                        ToastrEvent?.Invoke("la fecha de entrega inicial debe ser menor o igual que la final", EnumTypeToastr.Error);
                        invalido = true;
                    }
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Fecha de entrega final inválida", EnumTypeToastr.Error);
                }
            }
            if (divHoraFin.Visible && TxtHoraFin.Text == "")
            {
                ToastrEvent?.Invoke("Debe digitar la hora final", EnumTypeToastr.Error);
                invalido = true;
            }
            if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "P")
            {
                if (TxtPresion.Text == "")
                {
                    ToastrEvent?.Invoke("Debe digitar la presión del punto final", EnumTypeToastr.Error);
                    invalido = true;
                }
                else
                {
                    string[] lsPresion;
                    try
                    {
                        if (TxtPresion.Text.Trim().Length > 500)
                        {
                            ToastrEvent?.Invoke("Valor Inválido en la presión del punto final", EnumTypeToastr.Error);
                            invalido = true;
                        }
                        else
                        {
                            lsPresion = TxtPresion.Text.Trim().Split('-');
                            foreach (string Presion in lsPresion)
                            {
                                try
                                {
                                    ldValor = Convert.ToDecimal(Presion.Trim());
                                }
                                catch (Exception)
                                {
                                    ToastrEvent?.Invoke("Valor Inválido en la presión del punto final", EnumTypeToastr.Error);
                                    invalido = true;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ToastrEvent?.Invoke("Valor Inválido en la presión del punto final." + ex.Message, EnumTypeToastr.Error);
                        invalido = true;
                    }
                }
            }

            //20210707 trm moneda
            if (ddlTrm.SelectedValue == "0")
            {
                ToastrEvent?.Invoke("Debe seleccionar la tasa de cambio", EnumTypeToastr.Error);
                invalido = true;
            }
            //20210707 trm moneda
            if (TxtObsTrm.Text == "")
            {
                ToastrEvent?.Invoke("Debe digitar las observaciones de la tasa de cambio", EnumTypeToastr.Error);
                invalido = true;
            }
            return invalido;
        }

        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void crear()
        {
            var strError = new StringBuilder();
            string[] lsNombreParametros = { "@P_destino_rueda","@P_tipo_mercado", "@P_codigo_op_contrario","@P_fecha_negociacion","@P_hora_negociacion","@P_fecha_suscripcion", "@P_conectado_snt",
                "@P_ent_boca_pozo", "@P_codigo_centro", "@P_codigo_punto_ent","@P_codigo_modalidad","@P_codigo_periodo_ent","@P_no_anos","@P_fecha_inicial","@P_hora_inicial","@P_fecha_final",
                "@P_hora_final","@P_precio","@P_cantidad","@P_sentido_flujo","@P_presion_punto_fin", "@P_codigo_fuente", "@P_contrato_definitivo", "@P_ind_contrato_var", "@P_clasificacion_transp","@P_contratos_ms_tra", //20201124
                "@P_valida",                       //20210224
                "@P_codigo_trm","@P_observacion_trm","@P_tipo_moneda" , //20210707 trm moneda
                "@P_ingreso_extemporaneo"}; //20220221 ingreso extemporaneo
            SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.Decimal,SqlDbType.Decimal,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int, SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int,SqlDbType.VarChar, //20201124
                                        SqlDbType.VarChar,  //20210224
                                        SqlDbType.Int, SqlDbType.VarChar,SqlDbType.VarChar,//20210707 trm moneda
                                        SqlDbType.VarChar}; //20220221 ingreso extemporaneo
            string[] lValorParametros = { "", "", "0", "", "", "", "", "", "", "0", "0", "0","0", "", "", "", "", "0", "0", "", "", "0", "", "", "0", "", "S", //20201124 //20210224
                    ddlTrm.SelectedValue, TxtObsTrm.Text, ddlMoneda.SelectedValue ,//20210707 trm moneda
                    "S" }; //20220221 ingreso extemporaneo

            var invalido = valida_datos();

            try
            {
                if (!invalido)
                {
                    lblValidacion.Text = ""; //20210224
                    lValorParametros[0] = DdlDestino.SelectedValue;
                    lValorParametros[1] = DdlMercado.SelectedValue;
                    lValorParametros[2] = DdlOperador.SelectedValue;
                    lValorParametros[3] = TxtFechaNeg.Text;
                    lValorParametros[4] = TxtHoraNeg.Text;
                    lValorParametros[5] = TxtFechaSus.Text.Trim();
                    lValorParametros[6] = DdlConectado.SelectedValue;
                    lValorParametros[7] = DdlBoca.SelectedValue;
                    lValorParametros[8] = DdlCentro.SelectedValue;
                    lValorParametros[9] = DdlPunto.SelectedValue;
                    lValorParametros[10] = DdlModalidad.SelectedValue;
                    lValorParametros[11] = DdlPeriodo.SelectedValue;
                    lValorParametros[12] = TxtNoAños.Text.Trim();
                    lValorParametros[13] = TxtFechaIni.Text.Trim();
                    lValorParametros[14] = TxtHoraIni.Text.Trim();
                    lValorParametros[15] = TxtFechaFin.Text.Trim();
                    lValorParametros[16] = TxtHoraFin.Text.Trim();
                    lValorParametros[17] = TxtPrecio.Text.Trim();
                    lValorParametros[18] = TxtCantidad.Text.Trim();
                    if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "P")
                        lValorParametros[19] = DdlSentido.SelectedValue;
                    else
                        lValorParametros[19] = "";
                    lValorParametros[20] = TxtPresion.Text.Trim();
                    lValorParametros[21] = DdlFuente.SelectedValue;
                    lValorParametros[22] = TxtContratoDef.Text.Trim();
                    lValorParametros[23] = DdlVariable.SelectedValue;
                    lValorParametros[24] = DdlClasifica.SelectedValue; //20201124
                    lValorParametros[25] = TxtIdTraMp.Text; //20201124
                    lConexion.Abrir();
                    string lsErrror = "N"; //20201124
                    string lsConfirma = "N"; //20201124
                    SqlDataReader lLector;
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetContratoBilateral", lsNombreParametros, lTipoparametros, lValorParametros);
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                        {//20201124
                            if (lLector["ind_error"].ToString() == "S")
                            {
                                strError.Append(lLector["error"] + "<br>"); //20201124
                                lsErrror = "S";
                            }
                            else
                            {
                                lblValidacion.Text += lLector["error"] + "<br>";
                                lsConfirma = "S";
                            }
                        }
                        //Se él modal de confirmación
                        //20210224
                        if (lsErrror == "N" && lsConfirma == "S")
                            ModalEvent?.Invoke(mdlConfirma.ID, mdlConfirmaInside.ID, EnumTypeModal.Abrir, Modal.Size.Normal);
                    }
                    else
                    {
                        //Se cierra el modal de registro 
                        //20210728 
                        if (hndIndica.Value == "1")
                            ModalEvent?.Invoke(mdlContAgregar.ID, mdlContAgregarInside.ID, EnumTypeModal.Cerrar);
                        //Se él modal de confirmación
                        ModalEvent?.Invoke(mdlFechaOpBil.ID, mdlFechaOpBilInside.ID, EnumTypeModal.Abrir, Modal.Size.Normal);
                        fecha_reg();
                    }
                    lConexion.Cerrar();
                }

                if (!string.IsNullOrEmpty(strError.ToString()))
                    ToastrEvent?.Invoke(strError.ToString(), EnumTypeToastr.Error);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lsTable, string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia(lsTable, lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles2
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles2(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"] + "-" + lLector["razon_social"];
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles1
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// 20170814 rq036-17
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["nombre_centro"] + "-" + lLector["nombre_ciudad"] + "-" + lLector["nombre_departamento"];
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20170814 rq036-17
        protected void DdlDestino_SelectedIndexChanged(object sender, EventArgs e)
        {
            valida_dest_merc();
            if (DdlDestino.SelectedValue == "G")
            {
                LblPunto.Text = "Punto de entrega de la energía al comprador";
                LblCantidad.Text = "Cantidad de energía contratada (MBTUD)";
                LblPrecio.Text = "Precio a la fecha de suscripción del contrato (USD/MBTU)";
                DdlPunto.Items.Clear();
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, DdlPunto, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
                lConexion.Cerrar();
                if (DdlMercado.SelectedValue == "S")
                    valida_conec_boca();
            }
            else
            {
                LblPunto.Text = "Tramo o grupos de gasoductos";
                LblCantidad.Text = "Capacidad Contratada (KPCD)";
                LblPrecio.Text = "Tarifa a la fecha de suscripción del contrato (USD/KPC)";
                DdlPunto.Items.Clear();
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, DdlPunto, "m_ruta_snt", " estado = 'A' order by descripcion", 0, 4);
                lConexion.Cerrar();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20170814 rq036-17
        protected void DdlMercado_SelectedIndexChanged(object sender, EventArgs e)
        {
            valida_dest_merc();
            if (DdlDestino.SelectedValue == "G")
                valida_conec_boca();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20170814 rq036-17
        protected void DdlConectado_SelectedIndexChanged(object sender, EventArgs e)
        {
            valida_conec_boca();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20170814 rq036-17
        protected void DdlBoca_SelectedIndexChanged(object sender, EventArgs e)
        {
            valida_conec_boca();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20170814 rq036-17
        protected void DdlPeriodo_SelectedIndexChanged(object sender, EventArgs e)
        {
            TxtHoraIni.Text = "";
            divHoraIni.Visible = false;
            TxtHoraFin.Text = "";
            divHoraFin.Visible = false;
            TxtFechaFin.Text = "";
            TxtFechaFin.Enabled = false;
            LblNoAños.Visible = false;
            TxtNoAños.Visible = false;
            TxtNoAños.Text = "0";
            SqlDataReader lLector;
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_periodos_entrega", "codigo_periodo=" + DdlPeriodo.SelectedValue);
            if (lLector.HasRows)
            {
                lLector.Read();
                HndTiempo.Value = lLector["medida_tiempo"].ToString();
                hdfMesInicialPeriodo.Value = lLector["mes_inicial_periodo"].ToString();
                hdfDiasIni.Value = lLector["dias_entrega_ini"].ToString();
                if (HndTiempo.Value == "U")
                {
                    TxtFechaFin.Enabled = true;
                }
                if (HndTiempo.Value == "I")
                {
                    divHoraIni.Visible = true;
                    divHoraFin.Visible = true;
                }
                if (HndTiempo.Value == "L")
                {
                    LblNoAños.Visible = true;
                    TxtNoAños.Visible = true;
                    TxtNoAños.Text = "";
                }
            }
            else
            {
                HndTiempo.Value = "";
                hdfMesInicialPeriodo.Value = "0";
                hdfDiasIni.Value = "0";
            }
            lConexion.Cerrar();
            calcula_fecha();

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20170814 rq036-17
        protected void valida_dest_merc()
        {
            if (DdlDestino.SelectedValue == "G" && DdlMercado.SelectedValue == "P")
            {
                LblFuente.Visible = true;
                DdlFuente.Visible = true;
                trSumSec.Visible = false;
                DdlConectado.SelectedValue = "S";
                DdlBoca.SelectedValue = "N";
                DdlPunto.Visible = true;
                DdlCentro.Visible = false;
                DdlCentro.SelectedValue = "0";
                trFlujo.Visible = false;
                TxtPresion.Text = "0";
                divMsTra.Visible = false; //20201124
                TxtIdTraMp.Text = ""; //20201124
            }
            if (DdlDestino.SelectedValue == "G" && DdlMercado.SelectedValue == "S")
            {
                LblFuente.Visible = false;
                DdlFuente.Visible = false;
                DdlFuente.SelectedValue = "0";
                trSumSec.Visible = true;
                DdlConectado.SelectedValue = "S";
                DdlBoca.SelectedValue = "N";
                DdlPunto.Visible = true;
                DdlCentro.Visible = false;
                DdlCentro.SelectedValue = "0";
                trFlujo.Visible = false;
                TxtPresion.Text = "0";
                divMsTra.Visible = false; //20201124
                TxtIdTraMp.Text = ""; //20201124
            }
            if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "P")
            {
                LblFuente.Visible = false;
                DdlFuente.Visible = false;
                DdlFuente.SelectedValue = "0";
                trSumSec.Visible = false;
                DdlConectado.SelectedValue = "S";
                DdlBoca.SelectedValue = "N";
                DdlPunto.Visible = true;
                DdlCentro.Visible = false;
                DdlCentro.SelectedValue = "0";
                trFlujo.Visible = true;
                TxtPresion.Text = "";
                divMsTra.Visible = false; //20201124
                TxtIdTraMp.Text = ""; //20201124
            }
            if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "S")
            {
                LblFuente.Visible = false;
                DdlFuente.Visible = false;
                DdlFuente.SelectedValue = "0";
                trSumSec.Visible = false;
                DdlConectado.SelectedValue = "S";
                DdlBoca.SelectedValue = "N";
                DdlPunto.Visible = true;
                DdlCentro.Visible = false;
                DdlCentro.SelectedValue = "0";
                trFlujo.Visible = false;
                TxtPresion.Text = "0";
                divMsTra.Visible = true; //20201124
                TxtIdTraMp.Text = ""; //20201124
            }
            //20190607 rq036-19
            DdlPeriodo.Items.Clear();
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, DdlPeriodo, "m_periodos_entrega per, m_caracteristica_sub scar", " per.estado = 'A'  and scar.destino_rueda = '" + DdlDestino.SelectedValue + "'  and scar.tipo_mercado= '" + DdlMercado.SelectedValue + "' and scar.codigo_tipo_subasta = 5 and scar.estado ='A' and scar.tipo_caracteristica ='E' and scar.codigo_caracteristica = per.codigo_periodo  order by per.descripcion", 0, 1);
            lConexion.Cerrar();
            //20190607  fin rq036-19
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20170814 rq036-17
        protected void valida_conec_boca()
        {
            if (DdlMercado.SelectedValue == "S" && DdlDestino.SelectedValue == "G")
            {
                if (DdlConectado.SelectedValue == "S")
                {
                    lConexion.Abrir();
                    DdlPunto.Items.Clear();
                    LlenarControles(lConexion.gObjConexion, DdlPunto, "m_pozo", " ind_estandar='S' and  estado = 'A'  order by descripcion", 0, 1);
                    lConexion.Cerrar();
                    DdlCentro.Visible = false;
                    DdlCentro.SelectedValue = "0";
                    DdlBoca.SelectedValue = "N";
                    DdlPunto.Visible = true;
                    LblPunto.Text = "Punto de entrega de la energía al comprador";
                }
                else
                {
                    if (DdlBoca.SelectedValue == "S")
                    {
                        lConexion.Abrir();
                        DdlPunto.Items.Clear();
                        LlenarControles(lConexion.gObjConexion, DdlPunto, "m_pozo", " codigo_tipo_campo = 6 and  estado = 'A'  order by descripcion", 0, 1);
                        lConexion.Cerrar();
                        LblPunto.Text = "Punto de entrega de la energía al comprador";
                        DdlPunto.Visible = true;
                        DdlCentro.Visible = false;
                        DdlCentro.SelectedValue = "0";

                    }
                    else
                    {
                        LblPunto.Text = "Centro Poblado";
                        DdlCentro.Visible = true;
                        DdlCentro.SelectedValue = "0";
                        DdlPunto.SelectedValue = "0";
                        DdlPunto.Visible = false;
                    }
                }
            }
        }

        protected void calcula_fecha()
        {
            DateTime ldFecha;
            string lsDisSemana = "0";
            string lsFecha = "";
            string lsFecha1 = "";
            int liAnos = 0;
            int liMes2Tr = 0;
            int liMes3Tr = 0;
            int liMes4Tr = 0;
            var valido = true;
            if (TxtFechaIni.Text.Trim().Length > 0 && DdlPeriodo.SelectedValue != "0")
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIni.Text.Trim());
                    if (HndTiempo.Value != "I")
                    {
                        //// Valculo Fecha Final cuando es Intradiario
                        if (HndTiempo.Value == "D")
                            TxtFechaFin.Text = TxtFechaIni.Text.Trim();
                        if (HndTiempo.Value == "S")
                        {
                            lsDisSemana = Convert.ToDateTime(TxtFechaIni.Text.Trim()).DayOfWeek.ToString();
                            if (lsDisSemana != "Monday")
                            {
                                ToastrEvent?.Invoke("La Fecha de Inicio debe ser el primer día de la semana, ya que escogió periodo semanal", EnumTypeToastr.Error);
                                valido = true;
                            }
                            {
                                lsFecha = Convert.ToDateTime(TxtFechaIni.Text.Trim()).AddDays(6).ToString("dd/MM/yyyy");
                            }
                        }
                        if (HndTiempo.Value == "M")
                        {
                            if (Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(8, 2)) != 1)
                            {
                                ToastrEvent?.Invoke("La Fecha de Inicio debe ser el primer día del mes, ya que escogió periodo mensual", EnumTypeToastr.Error);
                                valido = false;
                            }
                            else
                            {
                                lsFecha = Convert.ToDateTime(TxtFechaIni.Text.Trim()).AddMonths(Convert.ToInt32(hdfDiasIni.Value)).AddDays(-1).ToString();
                            }
                        }
                        if (HndTiempo.Value == "T")
                        {
                            lsFecha1 = DateTime.Now.Year + "/" + hdfMesInicialPeriodo.Value + "/01";
                            liMes2Tr = Convert.ToDateTime(lsFecha1).AddMonths(3).Month;
                            liMes3Tr = Convert.ToDateTime(lsFecha1).AddMonths(6).Month;
                            liMes4Tr = Convert.ToDateTime(lsFecha1).AddMonths(9).Month;
                            if (Convert.ToInt32(hdfMesInicialPeriodo.Value) != Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(5, 2)) && Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(5, 2)) != liMes2Tr && Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(5, 2)) != liMes3Tr && Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(5, 2)) != liMes4Tr)
                            {
                                ToastrEvent?.Invoke("El Mes de la Fecha de Inicio es diferente al mes parametrizado de inicio {" + hdfMesInicialPeriodo.Value + "-" + liMes2Tr + "-" + liMes3Tr + "-" + liMes4Tr + "}, ya que escogió periodo trimestral", EnumTypeToastr.Error);
                                valido = false;
                            }
                            else
                            {
                                if (Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(8, 2)) != 1)
                                {
                                    ToastrEvent?.Invoke("La Fecha de Inicio debe ser el primer día del mes, ya que escogió periodo trimestral", EnumTypeToastr.Error);
                                    valido = false;
                                }
                                else
                                {
                                    lsFecha = Convert.ToDateTime(TxtFechaIni.Text.Trim()).AddMonths(3).AddDays(-1).ToString(CultureInfo.InvariantCulture);
                                }

                            }
                        }
                        if (HndTiempo.Value == "A")
                        {
                            if (Convert.ToInt32(hdfMesInicialPeriodo.Value) != Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(5, 2)))
                            {
                                ToastrEvent?.Invoke("El Mes de la Fecha de inicio es diferente al mes parametrizado de inicio {" + hdfMesInicialPeriodo.Value + "}, ya que escogió periodo anual", EnumTypeToastr.Error);
                                valido = false;
                            }
                            else
                            {
                                if (Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(8, 2)) != 1)
                                {
                                    ToastrEvent?.Invoke("La Fecha de inicio debe ser el primer día del mes, ya que escogió periodo anual.", EnumTypeToastr.Error);
                                    valido = false;
                                }
                                else
                                {
                                    lsFecha = Convert.ToDateTime(TxtFechaIni.Text.Trim()).AddMonths(12).AddDays(-1).ToString("dd/MM/yyyy");
                                }

                            }
                        }
                        if (HndTiempo.Value == "L")
                        {
                            if (TxtNoAños.Text.Trim().Length <= 0)
                            {
                                ToastrEvent?.Invoke("Debe ingresar el número de años, ya que escogió periodo multiAnual", EnumTypeToastr.Error);
                                valido = false;
                            }
                            if (TxtNoAños.Text.Trim().Length > 0)
                            {
                                try
                                {
                                    liAnos = Convert.ToInt32(TxtNoAños.Text.Trim());
                                    if (Convert.ToInt32(hdfMesInicialPeriodo.Value) != Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(5, 2)))
                                    {
                                        ToastrEvent?.Invoke("El mes de la fecha de inicio es diferente al mes parametrizado de inicio {" + hdfMesInicialPeriodo.Value + "}, ya que escogió periodo multi-anual", EnumTypeToastr.Error);
                                        valido = false;
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(8, 2)) != 1)
                                        {
                                            ToastrEvent?.Invoke("La Fecha de inicio debe ser el primer día del mes, ya que escogió periodo multi-anual", EnumTypeToastr.Error);
                                            valido = false;
                                        }
                                        else
                                        {
                                            lsFecha = Convert.ToDateTime(TxtFechaIni.Text.Trim()).AddYears(Convert.ToInt32(TxtNoAños.Text.Trim())).AddDays(-1).ToString();
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ToastrEvent?.Invoke("Valor inválido en el campo de número de años del periodo multi-anual", EnumTypeToastr.Error);
                                    valido = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        TxtFechaFin.Text = TxtFechaIni.Text.Trim();
                        if (ldFecha < Convert.ToDateTime(TxtFechaNeg.Text))
                        {
                            ToastrEvent?.Invoke("La Fecha de Inicio debe ser mayor o igual que la fecha de negociación", EnumTypeToastr.Error);
                            valido = false;
                        }
                        if (TxtHoraIni.Text != "")
                            if (Convert.ToDateTime(TxtFechaIni.Text + ' ' + TxtHoraIni.Text) <= Convert.ToDateTime(TxtFechaNeg.Text + ' ' + TxtHoraNeg.Text))
                            {
                                ToastrEvent?.Invoke("La Fecha-hora de Inicio debe ser mayor que la fecha-hora de negociación", EnumTypeToastr.Error);
                                valido = false;
                            }
                    }

                    if (!valido) return;
                    if (lsFecha != "")
                        TxtFechaFin.Text = lsFecha.Substring(6, 4) + "/" + lsFecha.Substring(3, 2) + "/" +
                                           lsFecha.Substring(0, 2);
                }
                catch (Exception)
                {
                    ToastrEvent?.Invoke("Valor Inválido en la fecha inicial<br>", EnumTypeToastr.Error);
                }
            }
        }





        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnNuevo_Click(object sender, EventArgs e)
        {
            hndIndModif.Value = "2";
            btnModificar.Visible = false;
            btnCrear.Visible = true;
            btnInsertar.Visible = true; //20210224 //20210728
            lista_bil = new Dictionary<string, id_bilateral>();
            hndActual.Value = "1";
            hndConsec.Value = "1";
            limpiarCampos();

            //Modal de agregar
            //Se actualiza el titulo del modal
            mdlContAgregarLabel.InnerText = "Agregar";
            ModalEvent?.Invoke(mdlContAgregar.ID, mdlContAgregarInside.ID, EnumTypeModal.Abrir);
        }

        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnInsertar_Click(object sender, EventArgs e)
        {
            hndIndica.Value = "2";
            crear();
        }

        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnConsultar_Click(object sender, EventArgs e)
        {
            CargarDatos();
        }

        /// <summary>
        /// Nombre: dtgConsulta_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            this.dtgConsulta.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();
        }

        /// <summary>
        /// 
        /// </summary>
        protected void fecha_reg()
        {
            try
            {
                string[] lsNombreParametros = { "@P_codigo_usuario" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int };
                Object[] lValorParametros = { goInfo.codigo_usuario };
                lblMensajeReg.Text = "";
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetPlanoNegoDirHoraNeg", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensajeReg.Text += lLector["mensaje"].ToString();
                    EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Crear };
                    ButtonsEvent?.Invoke(botones, null, NameController);
                }
                else
                {
                    lblMensajeReg.Text = "No hay registros para cargar";
                    EnumBotones[] botones = { EnumBotones.Buscar };
                    ButtonsEvent?.Invoke(botones, null, NameController);
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                SqlDataReader lLector;
                SqlCommand lComando = new SqlCommand();
                lConexion = new clConexion(goInfo);
                string[] lsNombreParametros = { "@P_codigo_usuario", "@P_codigo_operador" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
                Object[] lValorParametros = { goInfo.codigo_usuario, goInfo.cod_comisionista };
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetPlanoNegoDirecta", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    string lsError = "";
                    while (lLector.Read())
                    {
                        lsError = lLector["mensaje"] + "\\n";  //rq026-17  20171130
                        hndContrato.Value = lLector["numero_contrato"].ToString();
                    }
                    //Se notifica al usuario el valor de la transacción 
                    if (lsError.Contains("Se generó"))
                    {
                        ToastrEvent?.Invoke(lsError, EnumTypeToastr.Success);
                        //Se cierra el modal de la confirmación 
                        ModalEvent?.Invoke(mdlFechaOpBil.ID, mdlFechaOpBilInside.ID, EnumTypeModal.Cerrar);
                    }
                    else
                    {
                        ToastrEvent?.Invoke(lsError, EnumTypeToastr.Error);
                    }
                }
                else
                {
                    ToastrEvent?.Invoke("Operaciones disponibles en el módulo Registro de Contratos, para ingreso de información transaccional.!", EnumTypeToastr.Warning);
                }

                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                //divPregunta.Visible = false;
                if (hndIndica.Value == "1")
                {
                    //Modal de agregar
                    ModalEvent?.Invoke(mdlContAgregar.ID, mdlContAgregarInside.ID, EnumTypeModal.Cerrar);
                }
                //else
                //{
                //    id_bilateral idBilat = new id_bilateral(hndContrato.Value, DdlDestino.SelectedValue, DdlMercado.SelectedValue, DdlOperador.SelectedValue, TxtFechaNeg.Text, TxtHoraNeg.Text, TxtFechaSus.Text, DdlConectado.SelectedValue, DdlBoca.SelectedValue, DdlCentro.SelectedValue, DdlPunto.SelectedValue, DdlModalidad.SelectedValue, DdlPeriodo.SelectedValue, TxtNoAños.Text, TxtFechaIni.Text, TxtHoraIni.Text, TxtFechaFin.Text, TxtHoraFin.Text, TxtPrecio.Text, TxtCantidad.Text, DdlSentido.SelectedValue, TxtPresion.Text, DdlFuente.SelectedValue, TxtContratoDef.Text, DdlVariable.SelectedValue);
                //    lista_bil.Add(hndConsec.Value, idBilat);
                //    int liConsec = Convert.ToInt16(hndConsec.Value) + 1;
                //    hndConsec.Value = liConsec.ToString();
                //    hndActual.Value = hndConsec.Value;
                //}
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Problemas en la Carga del Plano.!", EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20210224
        protected void BtnAceptarConf_Click(object sender, EventArgs e)
        {
            ModalEvent?.Invoke(mdlConfirma.ID, mdlConfirmaInside.ID, EnumTypeModal.Cerrar);

            var strError = new StringBuilder();
            string[] lsNombreParametros = { "@P_destino_rueda","@P_tipo_mercado", "@P_codigo_op_contrario","@P_fecha_negociacion","@P_hora_negociacion","@P_fecha_suscripcion", "@P_conectado_snt",
                "@P_ent_boca_pozo", "@P_codigo_centro", "@P_codigo_punto_ent","@P_codigo_modalidad","@P_codigo_periodo_ent","@P_no_anos","@P_fecha_inicial","@P_hora_inicial","@P_fecha_final",
                "@P_hora_final","@P_precio","@P_cantidad","@P_sentido_flujo","@P_presion_punto_fin", "@P_codigo_fuente", "@P_contrato_definitivo", "@P_ind_contrato_var", "@P_clasificacion_transp","@P_contratos_ms_tra", //20201124
                "@P_valida",                       //20210224
                "@P_codigo_trm", "@P_observacion_trm", "@P_tipo_moneda", //20210707 trm moneda
                "@P_ingreso_bilateral" };//20220221 registro extemporaneno
            SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.Decimal,SqlDbType.Decimal,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int, SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int,SqlDbType.VarChar, //20201124
                                        SqlDbType.VarChar, //20210224
                                        SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, //20210707 trm moneda
                                        SqlDbType.VarChar}; //20220221 registro extemporaneo
            string[] lValorParametros = { "", "", "0", "", "", "", "", "", "", "0", "0", "0","0", "", "", "", "", "0", "0", "", "", "0", "", "", "0", "", "N", //20201124 //20210224
                                            ddlTrm.SelectedValue, TxtObsTrm.Text , ddlMoneda.SelectedValue, //20210707 trm moneda
                                            "S" //20220221 registro extemporaneo
        };

            try
            {
                lValorParametros[0] = DdlDestino.SelectedValue;
                lValorParametros[1] = DdlMercado.SelectedValue;
                lValorParametros[2] = DdlOperador.SelectedValue;
                lValorParametros[3] = TxtFechaNeg.Text;
                lValorParametros[4] = TxtHoraNeg.Text;
                lValorParametros[5] = TxtFechaSus.Text.Trim();
                lValorParametros[6] = DdlConectado.SelectedValue;
                lValorParametros[7] = DdlBoca.SelectedValue;
                lValorParametros[8] = DdlCentro.SelectedValue;
                lValorParametros[9] = DdlPunto.SelectedValue;
                lValorParametros[10] = DdlModalidad.SelectedValue;
                lValorParametros[11] = DdlPeriodo.SelectedValue;
                lValorParametros[12] = TxtNoAños.Text.Trim();
                lValorParametros[13] = TxtFechaIni.Text.Trim();
                lValorParametros[14] = TxtHoraIni.Text.Trim();
                lValorParametros[15] = TxtFechaFin.Text.Trim();
                lValorParametros[16] = TxtHoraFin.Text.Trim();
                lValorParametros[17] = TxtPrecio.Text.Trim();
                lValorParametros[18] = TxtCantidad.Text.Trim();
                if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "P")
                    lValorParametros[19] = DdlSentido.SelectedValue;
                else
                    lValorParametros[19] = "";
                lValorParametros[20] = TxtPresion.Text.Trim();
                lValorParametros[21] = DdlFuente.SelectedValue;
                lValorParametros[22] = TxtContratoDef.Text.Trim();
                lValorParametros[23] = DdlVariable.SelectedValue;
                lValorParametros[24] = DdlClasifica.SelectedValue; //20201124
                lValorParametros[25] = TxtIdTraMp.Text; //20201124
                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetContratoBilateral", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                    {//20201124
                        strError.Append(lLector["error"] + "<br>"); //20201124
                    }
                }
                else
                {
                    //Se cierra el modal de registro 
                    ModalEvent?.Invoke(mdlContAgregar.ID, mdlContAgregarInside.ID, hndIndica.Value == "2" ? EnumTypeModal.Abrir : EnumTypeModal.Cerrar);
                    //Se él modal de confirmación
                    ModalEvent?.Invoke(mdlFechaOpBil.ID, mdlFechaOpBilInside.ID, EnumTypeModal.Abrir, Modal.Size.Normal);
                    fecha_reg();
                }
                lConexion.Cerrar();

                if (!string.IsNullOrEmpty(strError.ToString()))
                    ToastrEvent?.Invoke(strError.ToString(), EnumTypeToastr.Error);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public class id_bilateral
        {
            public string numero_id { get; set; }
            public string destino_rueda { get; set; }
            public string tipo_mercado { get; set; }
            public string codigo_op_contrario { get; set; }
            public string fecha_negociacion { get; set; }
            public string hora_negociacion { get; set; }
            public string fecha_suscripcion { get; set; }
            public string conectado_snt { get; set; }
            public string ent_boca_pozo { get; set; }
            public string codigo_centro { get; set; }
            public string codigo_punto_ent { get; set; }
            public string codigo_modalidad { get; set; }
            public string codigo_periodo_ent { get; set; }
            public string no_anos { get; set; }
            public string fecha_inicial { get; set; }
            public string hora_inicial { get; set; }
            public string fecha_final { get; set; }
            public string hora_final { get; set; }
            public string precio { get; set; }
            public string cantidad { get; set; }
            public string sentido_flujo { get; set; }
            public string presion_punto_fin { get; set; }
            public string codigo_fuente { get; set; }
            public string contrato_definitivo { get; set; }
            public string ind_contrato_var { get; set; }

            public id_bilateral(string vnumero_id, string vdestino_rueda, string vtipo_mercado, string vcodigo_op_contrario, string vfecha_negociacion, string vhora_negociacion, string vfecha_suscripcion, string vconectado_snt, string vent_boca_pozo, string vcodigo_centro, string vcodigo_punto_ent, string vcodigo_modalidad, string vcodigo_periodo_ent, string vno_anos, string vfecha_inicial, string vhora_inicial, string vfecha_final, string vhora_final, string vprecio, string vcantidad, string vsentido_flujo, string vpresion_punto_fin, string vcodigo_fuente, string vcontrato_definitivo, string vind_contrato_var)
            {
                numero_id = vnumero_id;
                destino_rueda = vdestino_rueda;
                tipo_mercado = vtipo_mercado;
                codigo_op_contrario = vcodigo_op_contrario;
                fecha_negociacion = vfecha_negociacion;
                hora_negociacion = vhora_negociacion;
                fecha_suscripcion = vfecha_suscripcion;
                conectado_snt = vconectado_snt;
                ent_boca_pozo = vent_boca_pozo;
                codigo_centro = vcodigo_centro;
                codigo_punto_ent = vcodigo_punto_ent;
                codigo_modalidad = vcodigo_modalidad;
                codigo_periodo_ent = vcodigo_periodo_ent;
                no_anos = vno_anos;
                fecha_inicial = vfecha_inicial;
                hora_inicial = vhora_inicial;
                fecha_final = vfecha_final;
                hora_final = vhora_final;
                precio = vprecio;
                cantidad = vcantidad;
                sentido_flujo = vsentido_flujo;
                presion_punto_fin = vpresion_punto_fin;
                codigo_fuente = vcodigo_fuente;
                contrato_definitivo = vcontrato_definitivo;
                ind_contrato_var = vind_contrato_var;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            limpiarCampos();
        }

        private void limpiarCampos()
        {
            DdlDestino.SelectedValue = "G";
            DdlDestino_SelectedIndexChanged(null, null);
            DdlMercado.SelectedValue = "P";
            DdlMercado_SelectedIndexChanged(null, null);
            DdlOperador.SelectedValue = "0";
            TxtFechaNeg.Text = "";
            TxtHoraNeg.Text = "";
            TxtFechaSus.Text = "";
            DdlConectado.SelectedValue = "S";
            DdlConectado_SelectedIndexChanged(null, null);
            DdlBoca.SelectedValue = "N";
            DdlCentro.SelectedValue = "0";
            DdlPunto.SelectedValue = "0";
            DdlModalidad.SelectedValue = "0";
            DdlPeriodo.SelectedValue = "0";
            DdlPeriodo_SelectedIndexChanged(null, null);
            TxtNoAños.Text = "0";
            TxtFechaIni.Text = "";
            TxtHoraIni.Text = "";
            TxtFechaFin.Text = "";
            TxtHoraFin.Text = "";
            TxtPrecio.Text = "";
            TxtCantidad.Text = "";
            TxtPresion.Text = "0";
            DdlFuente.SelectedValue = "0";
            TxtContratoDef.Text = "";
            DdlVariable.SelectedValue = "N";
            ddlTrm.SelectedValue = "0"; //20210707 trm moneda
            TxtObsTrm.Text = ""; //20210707 trm moneda
        }
    }
}