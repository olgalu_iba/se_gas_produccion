﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucRegistroContratosMayoristaExt.ascx.cs" Inherits="UsersControls.Contratos.Registro_Contratos.Registro_Contratos.UsersControls_Contratos_Registro_Contratos_WucRegistroContratosMayoristaExt" %>

<%--Contenido--%>
<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <%--20161213 rq102--%>
        <div class="form-group">
            <asp:Label Text="Fecha Negociación Inicial" AssociatedControlID="TxtFechaIni" runat="server" />
            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" runat="server" Width="100%" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Fecha Negociación Final" AssociatedControlID="TxtFechaFin" runat="server" />
            <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" runat="server" Width="100%" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Mercado" AssociatedControlID="ddlMercado" runat="server" />
            <asp:DropDownList ID="ddlMercado" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                <asp:ListItem Value="" Text="Seleccione"></asp:ListItem>
                <asp:ListItem Value="O" Text="Otras Transacciones del Mercado Mayorista"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Producto" AssociatedControlID="ddlProducto" runat="server" />
            <asp:DropDownList ID="ddlProducto" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                <%--20170926 rq027-17--%>
                <asp:ListItem Value="0">Seleccione</asp:ListItem>
                <asp:ListItem Value="1">Suministro de Gas</asp:ListItem>
                <asp:ListItem Value="2">Capacidad de Transporte</asp:ListItem>
                <asp:ListItem Value="3">Suministro y Transporte</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Comprador" AssociatedControlID="ddlComprador" runat="server" />
            <asp:DropDownList ID="ddlComprador" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Vendedor" AssociatedControlID="ddlVendedor" runat="server" />
            <asp:DropDownList ID="ddlVendedor" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Estado" AssociatedControlID="ddlEstado" runat="server" />
            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <%--20171130 rq026-27--%>
            <asp:Label Text="Causa Modificación" AssociatedControlID="ddlCausa" runat="server" />
            <asp:DropDownList ID="ddlCausa" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Consumo" AssociatedControlID="ddlBusSector" runat="server" />
            <asp:DropDownList ID="ddlBusSector" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Mercado Relevante" AssociatedControlID="ddlBusMercado" runat="server" />
            <asp:DropDownList ID="ddlBusMercado" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <%--20161213 rq102--%>
            <asp:Label Text="Número Operación Inicial" AssociatedControlID="txtOperacion" runat="server" />
            <asp:TextBox ID="txtOperacion" type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <%--20161213 rq102--%>
            <asp:Label Text="Número Operación Final" AssociatedControlID="txtOperacionFin" runat="server" />
            <asp:TextBox ID="txtOperacionFin" type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <%--20171130 rq026-27--%>
            <asp:Label Text="Id Registro" AssociatedControlID="TxtNoId" runat="server" />
            <asp:TextBox ID="TxtNoId" type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
        </div>
    </div>
</div>
<%--Grilla--%>
<%--20161207 rq102--%>
<div class="table table-responsive">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                Width="100%" CssClass="table-bordered" OnItemCommand="dtgConsulta_EditCommand">
                <Columns>
                    <%--20160811--%>
                    <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--20161213 rq102--%>
                    <%--20180126 rq107-16--%>
                    <%--1--%><asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación"
                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                    <%--2--%><asp:BoundColumn DataField="tipo_subasta" HeaderText="Tipo Subasta" ItemStyle-HorizontalAlign="Left"
                        Visible="false"></asp:BoundColumn>
                    <%--3--%><asp:BoundColumn DataField="mercado" HeaderText="Mercado" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Width="80px"></asp:BoundColumn>
                    <%--4--%><asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Width="100px"></asp:BoundColumn>
                    <%--5--%><asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Width="100px"></asp:BoundColumn>
                    <%--6--%><asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-Width="100px"></asp:BoundColumn>
                    <%--20180126 rq107-16--%>
                    <%--7--%><asp:BoundColumn DataField="fecha_limite_registro" HeaderText="Fecha Límite Registro"
                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                    <%--8--%><asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--9--%><asp:BoundColumn DataField="id" Visible="false"></asp:BoundColumn>
                    <%--10--%><asp:BoundColumn DataField="operador_compra" Visible="false"></asp:BoundColumn>
                    <%--11--%><asp:BoundColumn DataField="operador_venta" Visible="false"></asp:BoundColumn>
                    <%--12--%><asp:BoundColumn DataField="cantidad" HeaderText="Cantidad"></asp:BoundColumn>
                    <%--13--%><asp:BoundColumn DataField="precio" HeaderText="Precio"></asp:BoundColumn>
                    <%--14--%><asp:BoundColumn DataField="codigo_tipo_subasta" Visible="false"></asp:BoundColumn>
                    <%--15--%><asp:BoundColumn DataField="tipo_doc_comprador" Visible="false"></asp:BoundColumn>
                    <%--16--%><asp:BoundColumn DataField="tipo_doc_vendedor" Visible="false"></asp:BoundColumn>
                    <%--17--%><asp:BoundColumn DataField="documento_c" Visible="false"></asp:BoundColumn>
                    <%--18--%><asp:BoundColumn DataField="documento_v" Visible="false"></asp:BoundColumn>
                    <%--19--%><asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="No. Registro"></asp:BoundColumn>
                    <%--20--%><asp:BoundColumn DataField="destino_rueda" Visible="false"></asp:BoundColumn>
                    <%--21--%><asp:BoundColumn DataField="sigla_estado" Visible="false"></asp:BoundColumn>
                    <%--22--%><asp:BoundColumn DataField="tipo_mercado" Visible="false"></asp:BoundColumn>
                    <%--23--%><asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial">
                        <%-- Campo nuevo ajuste reportes 20160405 --%>
                    </asp:BoundColumn>
                    <%--24--%><asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final"></asp:BoundColumn>
                    <%-- Campo nuevo ajuste reportes 20160405 --%>
                    <%--25--%><asp:BoundColumn DataField="numero_contrato" HeaderText="Número Contrato"></asp:BoundColumn>
                    <%-- Campo nuevo ajuste reportes 20160405 --%>
                    <%--26--%><asp:EditCommandColumn HeaderText="Verificar" EditText="Verificar" Visible="False"></asp:EditCommandColumn>
                    <%--27--%><asp:BoundColumn DataField="codigo_producto" Visible="false"></asp:BoundColumn>
                    <%--28--%><asp:EditCommandColumn HeaderText="Accion" EditText="Seleccionar" Visible="False"></asp:EditCommandColumn>
                    <%--29--%><asp:BoundColumn DataField="en_correccion" Visible="false"></asp:BoundColumn>
                    <%--30--%><asp:BoundColumn DataField="fecha_max_registro" Visible="false"></asp:BoundColumn>
                    <%--31--%><asp:BoundColumn DataField="fecha_maxima_correccion" Visible="false"></asp:BoundColumn>
                    <%--32--%><asp:BoundColumn DataField="fecha_maxima_modificacion" Visible="false"></asp:BoundColumn>
                    <%--33--%><asp:BoundColumn DataField="ind_modifica" Visible="false"></asp:BoundColumn>
                    <%--201691215 rq111--%>
                    <%--34--%><asp:BoundColumn DataField="codigo_ruta_may" Visible="false"></asp:BoundColumn>
                    <%--20170530 divipola--%>
                    <%--35--%><asp:BoundColumn DataField="codigo_depto_punto_sal" HeaderText="Cod Depto"></asp:BoundColumn>
                    <%--20170530 divipola--%>
                    <%--36--%><asp:BoundColumn DataField="departamento" HeaderText="Departamento"></asp:BoundColumn>
                    <%--20170530 divipola--%>
                    <%--37--%><asp:BoundColumn DataField="codigo_municipio_pto_sal" HeaderText="Cod Mcpio"></asp:BoundColumn>
                    <%--20170530 divipola--%>
                    <%--38--%><asp:BoundColumn DataField="municipio" HeaderText="Municipio"></asp:BoundColumn>
                    <%--20170530 divipola--%>
                    <%--20180126 rq107-16--%>
                    <%--39--%><asp:BoundColumn DataField="codigo_centro_pob" HeaderText="Cod Centro Poblado"></asp:BoundColumn>
                    <%--20170530 divipola--%>
                    <%--40--%><asp:BoundColumn DataField="centro_poblado" HeaderText="centro poblado"></asp:BoundColumn>
                    <%--20170926 rq027-17--%>
                    <%--41--%>
                    <asp:BoundColumn DataField="capacidad_transporte" HeaderText="Capacidad de Transporte"></asp:BoundColumn>
                    <%--20171130 rq026-17--%>
                    <%--42--%>
                    <asp:BoundColumn DataField="desc_causa" HeaderText="Causa Modificación"></asp:BoundColumn>
                    <%--20210707 ajsute trm y moneda--%>
                    <%--43--%>
                    <asp:BoundColumn DataField="codigo_trm" HeaderText="Código Tipo TRM"></asp:BoundColumn>
                    <%--44--%>
                    <asp:BoundColumn DataField="desc_trm" HeaderText="Tipo TRM"></asp:BoundColumn>
                    <%--45--%>
                    <asp:BoundColumn DataField="tipo_moneda" HeaderText="Moneda"></asp:BoundColumn>
                    <%--46--%>
                    <asp:BoundColumn DataField="observacion_trm" HeaderText="Observación TRM"></asp:BoundColumn>
                    <%--20210707 ajsute trm y moneda--%>

                    <%--47--%>
                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                        <ItemTemplate>
                            <div class="dropdown dropdown-inline">
                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="flaticon-more-1"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                    <!--begin::Nav-->
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Seleccionar" runat="server">
                                                <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                <span class="kt-nav__link-text">Seleccionar</span>
                                                    </asp:LinkButton>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <asp:LinkButton ID="lkbVerificar" CssClass="kt-nav__link" CommandName="Verificar" runat="server">
                                                <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                <span class="kt-nav__link-text">Verificar</span>
                                                    </asp:LinkButton>
                                                </li>
                                            </ul>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <!--end::Nav-->
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
            </asp:DataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<%--Excel--%>
<asp:DataGrid ID="dtgConsultaExcel" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
    Width="100%" CssClass="table-bordered" Visible="false">
    <Columns>
        <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Contrato"></asp:BoundColumn>
        <asp:BoundColumn DataField="numero_id" HeaderText="No. Operación"></asp:BoundColumn>
        <%--20161213 rq102--%>
        <%--20180126 rq107-16--%>
        <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación"></asp:BoundColumn>
        <asp:BoundColumn DataField="vendedor" HeaderText="Nombre Vendedor"></asp:BoundColumn>
        <asp:BoundColumn DataField="operador_venta" HeaderText="Código Vendedor"></asp:BoundColumn>
        <asp:BoundColumn DataField="rol_vendedor" HeaderText="Rol Vendedor"></asp:BoundColumn>
        <asp:BoundColumn DataField="comprador" HeaderText="Nombre Comprador"></asp:BoundColumn>
        <asp:BoundColumn DataField="codigo_comprador" HeaderText="Código Comprador"></asp:BoundColumn>
        <asp:BoundColumn DataField="rol_comprador" HeaderText="Rol Comprador"></asp:BoundColumn>
        <asp:BoundColumn DataField="nit_comprador" HeaderText="Nit Comprador"></asp:BoundColumn>
        <asp:BoundColumn DataField="mercado" HeaderText="Tipo Mercado"></asp:BoundColumn>
        <asp:BoundColumn DataField="producto" HeaderText="Producto"></asp:BoundColumn>
        <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial"></asp:BoundColumn>
        <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final"></asp:BoundColumn>
        <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad Contrato"></asp:BoundColumn>
        <%--20170926 rq027-17--%>
        <asp:BoundColumn DataField="capacidad_transporte" HeaderText="Capacidad de Transporte"></asp:BoundColumn>
        <asp:BoundColumn DataField="precio" HeaderText="Precio"></asp:BoundColumn>
        <asp:BoundColumn DataField="punto_salida" HeaderText="Punto Salida"></asp:BoundColumn>
        <asp:BoundColumn DataField="modalidad" HeaderText="Modalidad"></asp:BoundColumn>
        <%--20170530 divipola--%>
        <asp:BoundColumn DataField="codigo_departamento" HeaderText="Cod Depto"></asp:BoundColumn>
        <asp:BoundColumn DataField="departamento" HeaderText="Departamento"></asp:BoundColumn>
        <%--20170530 divipola--%>
        <asp:BoundColumn DataField="codigo_ciudad" HeaderText="Cod Mcpio"></asp:BoundColumn>
        <asp:BoundColumn DataField="municipio" HeaderText="Municipio"></asp:BoundColumn>
        <%--20170530 divipola--%>
        <asp:BoundColumn DataField="codigo_centro_pob" HeaderText="Cod Centro Poblado"></asp:BoundColumn>
        <%--20170530 divipola--%>
        <asp:BoundColumn DataField="centro_poblado" HeaderText="Centro Poblado"></asp:BoundColumn>
        <asp:BoundColumn DataField="sector_consumo" HeaderText="Sector Consumo"></asp:BoundColumn>
        <asp:BoundColumn DataField="mercado_relevante" HeaderText="Mercado Relevante"></asp:BoundColumn>
        <asp:BoundColumn DataField="tipo_garantia" HeaderText="Tipo Garantía"></asp:BoundColumn>
        <asp:BoundColumn DataField="fecha_pago" HeaderText="Fecha Pago Garantía"></asp:BoundColumn>
        <%--20161213 rq102--%>
        <asp:BoundColumn DataField="estado" HeaderText="Estado"></asp:BoundColumn>
        <%--201691215 rq111--%>
        <asp:BoundColumn DataField="desc_ruta_may" HeaderText="Ruta"></asp:BoundColumn>
        <%--20171130 rq026-17--%>
        <%--41--%>
        <asp:BoundColumn DataField="desc_causa" HeaderText="Causa Modificación"></asp:BoundColumn>
        <%--20200727 ajsute fornt-ned--%>
        <asp:BoundColumn DataField="codigo_trm" HeaderText="Código Tipo TRM"></asp:BoundColumn>
        <asp:BoundColumn DataField="desc_trm" HeaderText="Tipo TRM"></asp:BoundColumn>
        <%--20210707 trm y moneda--%>
        <asp:BoundColumn DataField="tipo_moneda" HeaderText="Moneda"></asp:BoundColumn>
        <asp:BoundColumn DataField="observacion_trm" HeaderText="Observación TRM"></asp:BoundColumn>
        <%--20200727 fin ajsute fornt-ned--%>
        <%--20200803 res 138--%>
        <asp:BoundColumn DataField="fecha_hora_verifica" HeaderText="Fecha Registro"></asp:BoundColumn>
        <%--20210305 broker--%>
        <asp:BoundColumn DataField="ind_broker" HeaderText="Contrato Broker"></asp:BoundColumn>
    </Columns>
    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
</asp:DataGrid>

<%--Modals--%>
<%--Registro Contato--%>
<div class="modal fade" id="registroContratoMayoMod" tabindex="-1" role="dialog" aria-labelledby="mdlregistroContratoMayoModLabel" aria-hidden="true" clientidmode="Static" runat="server">
    <div class="modal-dialog" id="registroContratoMayoModInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="mdlregistroContratoMayoModLabel" runat="server">Modificación</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Fecha de Negociación" AssociatedControlID="TxtCapFechaNeg" runat="server" />
                                    <asp:TextBox ID="TxtCapFechaNeg" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" Width="100%" ClientIDMode="Static" MaxLength="10" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4" visible="False" runat="server">
                                <div class="form-group">
                                    <asp:Label Text="Numero Operación" AssociatedControlID="lblOperacion" runat="server" />
                                    <asp:Label ID="lblOperacion" runat="server" ForeColor="Red" Font-Bold="true" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4" visible="False" runat="server">
                                <div class="form-group">
                                    <asp:Label Text="Punta" AssociatedControlID="lblPunta" runat="server" />
                                    <asp:Label ID="lblPunta" runat="server" ForeColor="Red" Font-Bold="true" CssClass="form-control" Visible="false" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Producto" AssociatedControlID="ddlCapProducto" runat="server" />
                                    <%--20161215 rq111--%>
                                    <asp:DropDownList ID="ddlCapProducto" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlCapProducto_SelectedIndexChanged">
                                        <%--20170926 rq027-17--%>
                                        <asp:ListItem Value="0">Seleccione</asp:ListItem>
                                        <asp:ListItem Value="1">Suministro de Gas</asp:ListItem>
                                        <asp:ListItem Value="2">Capacidad de Transporte</asp:ListItem>
                                        <asp:ListItem Value="3">Suministro y Transporte</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Label ID="lblProducto" runat="server" ForeColor="Red" Font-Bold="true" Visible="false" CssClass="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Punta que Registra" AssociatedControlID="lblPunta" runat="server" />
                                    <asp:DropDownList ID="ddlCapPunta" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                        <asp:ListItem Value="C" Text="Comprador"></asp:ListItem>
                                        <asp:ListItem Value="V" Text="Vendedor"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Tipo de Mercado" AssociatedControlID="lblPunta" runat="server" />
                                    <asp:DropDownList ID="ddlCapTipoMercado" runat="server" OnSelectedIndexChanged="ddlCapTipoMercado_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                        <asp:ListItem Value="O" Text="Otras Transaciones del Mercado Mayorista" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <asp:Label Text="Comprador" AssociatedControlID="ddlCapComprador" runat="server" />
                                <div class="form-group row">
                                    <div class="col-sm-8 col-md-8 col-lg-9">
                                        <asp:DropDownList ID="ddlCapComprador" runat="server" CssClass="form-control" />
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-lg-3">
                                        <div class="row">
                                            <div class="col-sm-6 col-md-6 col-lg-8">
                                                <input type="text" readonly class="form-control-plaintext" id="unpOtro" value="Otro">
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-4">
                                                <label class="kt-checkbox">
                                                    <asp:CheckBox ID="ChekOtro" runat="server" OnCheckedChanged="ChekOtro_CheckedChanged" AutoPostBack="true" runat="server" />
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4" id="divComprador" visible="False" runat="server">
                                <div class="form-group">
                                    <asp:Label ID="lblNIT" Text="NIT Comprador" AssociatedControlID="TxtCapComprador" runat="server" Visible="false" />
                                    <asp:TextBox ID="TxtCapComprador" runat="server" Visible="false" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label AssociatedControlID="ddlCapVendedor" runat="server" Text="Vendedor" />
                                    <asp:DropDownList ID="ddlCapVendedor" runat="server" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4" visible="false" runat="server">
                                <div class="form-group">
                                    <asp:Label ID="lnlNomOpera" runat="server" CssClass="form-control" />
                                    <asp:Label ID="lblNomOperador" runat="server" CssClass="form-control" ForeColor="Red" Font-Bold="true"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4" visible="false" runat="server">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoDocOpera" runat="server" CssClass="form-control" />
                                    <asp:Label ID="lblTipoDocOperador" runat="server" CssClass="form-control" ForeColor="Red" Font-Bold="true"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4" visible="false" runat="server">
                                <div class="form-group">
                                    <asp:Label ID="lblNoDocOPera" runat="server" CssClass="form-control" />
                                    <asp:Label ID="lblNoDocumentoOperaqdor" runat="server" CssClass="form-control" ForeColor="Red" Font-Bold="true"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Número Contrato" AssociatedControlID="TxtNumContrato" runat="server" />
                                    <asp:TextBox ID="TxtNumContrato" runat="server" ValidationGroup="detalle" CssClass="form-control" MaxLength="30"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Fecha Suscripción" AssociatedControlID="TxtFechaSus" runat="server" />
                                    <asp:TextBox ID="TxtFechaSus" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-4">
                                <asp:Label Text="Modalidad de Contrato" AssociatedControlID="ddlModalidad" runat="server" />
                                <div class="form-group row">
                                    <div class="col-sm-5 col-md-4 col-lg-5">
                                        <asp:DropDownList ID="ddlModalidad" runat="server" CssClass="form-control" />
                                    </div>
                                    <%--20200423 ajsute OTMM--%> <%--20200727--%>
                                    <div class="col-sm-2 col-md-2 col-lg-3" runat="server" visible="false">
                                        <div class="form-group row">
                                            <div class="col-sm-6 col-md-6 col-lg-8">
                                                <input type="text" readonly class="form-control-plaintext" id="unpOtro2" value="Otro">
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-4">
                                                <label class="kt-checkbox">
                                                    <asp:CheckBox ID="ChkOtrModa" runat="server" />
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <%--20200423 ajsute OTMM--%> <%--20200727--%>
                                    <div class="col-sm-5 col-md-6 col-lg-4" runat="server" visible="false">
                                        <asp:TextBox ID="TxtOtrModalidad" class="form-control" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblPuntoE" AssociatedControlID="ddlPuntoEntr" runat="server" />
                                    <%--20161215 rq111--%>
                                    <asp:DropDownList ID="ddlPuntoEntr" runat="server" CssClass="form-control" />
                                    <asp:CheckBox ID="ChkOtrPuntoE" runat="server" Visible="false" />
                                    <asp:TextBox ID="TxtOtrPuntoE" runat="server" Visible="false" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblCantidad" AssociatedControlID="TxtCantidad" runat="server" />
                                    <asp:TextBox ID="TxtCantidad" runat="server" class="form-control" ValidationGroup="detalle" />
                                    <%--20170926 rq027-17--%>
                                    <asp:TextBox ID="TxtCantidadT" runat="server" ValidationGroup="detalle" class="form-control" Visible="false" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblPrecio" runat="server" />
                                    <asp:TextBox ID="TxtPrecio" runat="server" class="form-control" ValidationGroup="detalle" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblFechaInc" runat="server" AssociatedControlID="TxtFechaInicial" />
                                    <asp:TextBox ID="TxtFechaInicial" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="lblFechaFin" runat="server" AssociatedControlID="TxtFechaFinal" />
                                    <asp:TextBox ID="TxtFechaFinal" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4" id="trRuta" runat="server" visible="false">
                                <div class="form-group">
                                    <asp:Label Text="Ruta Contratada" ID="Label2" runat="server" AssociatedControlID="ddlRuta" />
                                    <asp:DropDownList ID="ddlRuta" runat="server" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Tipo de Garantía" ID="Label3" runat="server" AssociatedControlID="TxtCapTipoGarantia" />
                                    <asp:TextBox ID="TxtCapTipoGarantia" runat="server" ValidationGroup="detalle" CssClass="form-control" MaxLength="200"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Valor de Garantía" ID="Label5" runat="server" AssociatedControlID="TxtCapValorGarantia" />
                                    <asp:TextBox ID="TxtCapValorGarantia" runat="server" ValidationGroup="detalle" CssClass="form-control" MaxLength="200"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Plazo para realizar el Pago" ID="Label6" runat="server" AssociatedControlID="TxtCapFecPlazo" />
                                    <asp:TextBox ID="TxtCapFecPlazo" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label>Sector de Consumo</label>
                                    <asp:DropDownList ID="ddlSector" runat="server" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label>Usuario no Regulado con Conexión a SNT <%--20180126 rq107-16--%></label>
                                    <asp:DropDownList ID="ddlUsuSnt" runat="server" OnSelectedIndexChanged="ddlUsuSnt_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                        <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                        <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                        <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdfusuarioSnt" runat="server" />
                                </div>
                            </div>
                            <%--20210707--%>
                            <%--<div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Copia del Contrato" AssociatedControlID="FuCopiaCont" runat="server" />
                                    <ajaxToolkit:AsyncFileUpload ID="FuCopiaCont" CssClass="form-control" OnUploadedComplete="FileUpload_UploadedComplete" Width="100%" runat="server" />
                                </div>
                            </div>--%>
                            <%--20210707 trm y moneda--%>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label AssociatedControlID="ddlTrm" runat="server">Tasa de Cambio</asp:Label>
                                    <asp:DropDownList ID="ddlTrm" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <%--20210707 trm y moneda--%>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label AssociatedControlID="TxtObsTrm" runat="server">Observaciones Tasa de Cambio</asp:Label>
                                    <asp:TextBox ID="TxtObsTrm" runat="server" CssClass="form-control" MaxLength="300" />
                                </div>
                            </div>
                            <%--20210707 trm y moneda--%>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label AssociatedControlID="ddlMoneda" runat="server">Tipo Moneda</asp:Label>
                                    <asp:DropDownList ID="ddlMoneda" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                        <asp:ListItem Value="USD" Text="USD"></asp:ListItem>
                                        <asp:ListItem Value="COP" Text="COP"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                        </div>
                        <div class="row" runat="server" id="TrPuntaSal" visible="false">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label>Departamento punto de salida</label>
                                    <asp:DropDownList ID="ddlDeptoPuntoSal" runat="server" OnSelectedIndexChanged="ddlDeptoPuntoSal_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <%--20170530 divipola--%>
                                    <label>Municipio punto de salida</label>
                                    <asp:DropDownList ID="DdlMunPuntoSal" runat="server" OnSelectedIndexChanged="DdlMunPuntoSal_SelectedIndexChanged"
                                        AutoPostBack="true" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <%--20200423 ajuste OTMM--%><%--20200727--%>
                            <div class="col-sm-12 col-md-6 col-lg-4" id="tdCentro" runat="server">
                                <div class="form-group">
                                    <%--20170530 divipola--%>
                                    <label>Centro Poblado</label>
                                    <asp:DropDownList ID="DdlCentro" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row" runat="server" id="TrSisDist" visible="false">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label>Mercado Relevante</label>
                                    <asp:DropDownList ID="ddlMercadoRel" runat="server" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="row" runat="server" id="TrModifi" visible="false">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label>Motivo Modificación</label>
                                    <asp:DropDownList ID="ddlMotivoModifi" runat="server" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <asp:Button ID="btnCrear" runat="server" CssClass="btn btn-primary" Text="Crear" OnClick="btnCrear_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnActualizar" runat="server" CssClass="btn btn-primary" Text="Actualizar" OnClick="btnActualizar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                    </div>
                    <asp:HiddenField ID="hndFechaMax" runat="server" />
                    <asp:HiddenField ID="hdfNomComp" runat="server" />
                    <asp:HiddenField ID="hdfTipoDocCom" runat="server" />
                    <asp:HiddenField ID="hdfPunta" runat="server" />
                    <asp:HiddenField ID="hdfIdVerif" runat="server" />
                    <asp:HiddenField ID="hdfCodTipoSub" runat="server" />
                    <asp:HiddenField ID="hdfDestinoRueda" runat="server" />
                    <asp:HiddenField ID="hdfRutaArchivo" runat="server" />
                    <asp:HiddenField ID="hdfEstadoAct" runat="server" />
                    <asp:HiddenField ID="hdfAccion" runat="server" />
                    <asp:HiddenField ID="hdfCodVerifCont" runat="server" />
                    <asp:HiddenField ID="hdfTipoMerc" runat="server" />
                    <asp:HiddenField ID="hdfOPeraC" runat="server" />
                    <asp:HiddenField ID="hdfOPeraV" runat="server" />
                    <asp:HiddenField ID="hdfCodProd" runat="server" />
                    <asp:HiddenField ID="hdfDiasMaxRegCont" runat="server" />
                    <asp:HiddenField ID="hdfDiasMaxCorCont" runat="server" />
                    <asp:HiddenField ID="hdfVerifManual" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>

<%--Verificacion--%>
<div class="modal fade" id="registroContratoMayoVer" tabindex="-1" role="dialog" aria-labelledby="mdlregistroContratoMayoVerLabel" aria-hidden="true" clientidmode="Static" runat="server">
    <div class="modal-dialog" id="registroContratoMayoVerInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mdlregistroContratoMayoVerLabel">Verificación</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5>
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                </h5>

                <hr>

                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Dato</label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Comprador</label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Vendedor</label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Verificación</label>
                            <%--20180126 rq107-16--%>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit1" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat1C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat1V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat1Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit2" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat2C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat2V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat2Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit3" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat3C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat3V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat3Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit4" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat4C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat4V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat4Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit5" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat5C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat5V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat5Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row" id="trSumTra" runat="server">
                    <%--20170926 rq027-17--%>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit5X" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat5CX" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat5VX" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat5VerX" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit6" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat6C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat6V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat6Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit7" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat7C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat7V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat7Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit8" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat8C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat8V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat8Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row" runat="server" id="TrTra1" visible="false">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit9" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat9C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat9V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat9Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row" runat="server" id="TrTra2" visible="false">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit10" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat10C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat10V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat10Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit11" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <a id="LinkC" runat="server" class="btn btn-outline-brand btn-square">Ver Contrato</a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <a id="LinkV" runat="server" class="btn btn-outline-brand btn-square">Ver Contrato</a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="Label4" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="Observación" runat="server"></asp:Label><%--20180126 rq107-16--%>
                            <asp:TextBox ID="TxtObservacion" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <asp:Button ID="btnSolCorr" runat="server" CssClass="btn btn-primary" Text="Solicitar Correccion" OnClick="btnSolCorr_Click" />
                <asp:Button ID="btnRegCont" runat="server" CssClass="btn btn-primary" Text="Registro Contrato" OnClick="btnRegCont_Click" />
            </div>
        </div>
    </div>
</div>
