﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucContratoBilateralExt.ascx.cs" Inherits="UsersControls.Contratos.Registro_Contratos.Registro_Contratos_Extemporaneo.WucContratoBilateralExt" %>

<div class="row">
    <%--Grupo 1--%>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Número Operación Inicial" AssociatedControlID="TxtBusContratoIni" runat="server" />
            <asp:TextBox ID="TxtBusContratoIni" type="number" runat="server" CssClass="form-control" ValidationGroup="detalle" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Número Operación Final" AssociatedControlID="TxtBusContratoFin" runat="server" />
            <asp:TextBox ID="TxtBusContratoFin" type="number" runat="server" CssClass="form-control" ValidationGroup="detalle" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Fecha Negociación Inicial" AssociatedControlID="TxtBusFechaIni" runat="server" />
            <asp:TextBox ID="TxtBusFechaIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Fecha Negociación Final" AssociatedControlID="TxtBusFechaFin" runat="server" />
            <asp:TextBox ID="TxtBusFechaFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Número Contrato" AssociatedControlID="TxtBusContDef" runat="server" />
            <asp:TextBox ID="TxtBusContDef" runat="server" CssClass="form-control" ValidationGroup="detalle" />
        </div>
    </div>
    <%--end Grupo 1--%>
    <%--Grupo 2--%>
    <div id="divComprador" class="col-sm-12 col-md-6 col-lg-4" runat="server">
        <div class="form-group">
            <asp:Label Text="Comprador" AssociatedControlID="ddlBusComprador" runat="server" />
            <asp:DropDownList ID="ddlBusComprador" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div id="divVendedor" class="col-sm-12 col-md-6 col-lg-4" runat="server">
        <div class="form-group">
            <asp:Label Text="Vendedor" AssociatedControlID="ddlBusVendedor" runat="server" />
            <asp:DropDownList ID="ddlBusVendedor" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div id="divMercado" class="col-sm-12 col-md-6 col-lg-4" runat="server">
        <div class="form-group">
            <asp:Label Text="Mercado" AssociatedControlID="ddlBusMercado" runat="server" />
            <asp:DropDownList ID="ddlBusMercado" CssClass="form-control selectpicker" data-live-search="true" runat="server">
                <asp:ListItem Value="">Seleccione</asp:ListItem>
                <asp:ListItem Value="P">Primario</asp:ListItem>
                <asp:ListItem Value="S">Secundario</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div id="divProducto" class="col-sm-12 col-md-6 col-lg-4" runat="server">
        <div class="form-group">
            <asp:Label Text="Producto" AssociatedControlID="ddlBusProducto" runat="server" />
            <asp:DropDownList ID="ddlBusProducto" CssClass="form-control selectpicker" data-live-search="true" runat="server">
                <asp:ListItem Value="">Seleccione</asp:ListItem>
                <asp:ListItem Value="1">Suministro de gas</asp:ListItem>
                <asp:ListItem Value="2">Capacidad de transporte</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div id="divModalidad" class="col-sm-12 col-md-6 col-lg-4" runat="server">
        <div class="form-group">
            <asp:Label Text="Modalidad" AssociatedControlID="ddlBusModalidad" runat="server" />
            <asp:DropDownList ID="ddlBusModalidad" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <%--end Grupo 2--%>
    <%--Grupo 3--%>
    <div id="divContraparte" class="col-sm-12 col-md-6 col-lg-4" runat="server">
        <div class="form-group">
            <asp:Label Text="Contraparte" AssociatedControlID="ddlBusContra" runat="server" />
            <asp:DropDownList ID="ddlBusContra" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div id="divMercado2" class="col-sm-12 col-md-6 col-lg-4" runat="server">
        <div class="form-group">
            <asp:Label Text="Mercado" AssociatedControlID="ddlBusMercado1" runat="server" />
            <asp:DropDownList ID="ddlBusMercado1" CssClass="form-control selectpicker" data-live-search="true" runat="server">
                <asp:ListItem Value="">Seleccione</asp:ListItem>
                <asp:ListItem Value="P">Primario</asp:ListItem>
                <asp:ListItem Value="S">Secundario</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div id="divProducto2" class="col-sm-12 col-md-6 col-lg-4" runat="server">
        <div class="form-group">
            <asp:Label Text="Producto" AssociatedControlID="ddlBusProducto1" runat="server" />
            <asp:DropDownList ID="ddlBusProducto1" CssClass="form-control selectpicker" data-live-search="true" runat="server" AutoPostBack="true">
                <asp:ListItem Value="">Seleccione</asp:ListItem>
                <asp:ListItem Value="1">Suministro de gas</asp:ListItem>
                <asp:ListItem Value="2">Capacidad de transporte</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div id="divModalidad2" class="col-sm-12 col-md-6 col-lg-4" runat="server">
        <div class="form-group">
            <asp:Label Text="Modalidad" AssociatedControlID="ddlBusModalidad1" runat="server" />
            <asp:DropDownList ID="ddlBusModalidad1" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div id="divEstado" class="col-sm-12 col-md-6 col-lg-4" runat="server">
        <div class="form-group">
            <asp:Label Text="Estado" AssociatedControlID="ddlBusEstado" runat="server" />
            <asp:DropDownList ID="ddlBusEstado" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <%--end Grupo 3--%>
</div>
<%--Grillas --%>
<div id="tblGrilla" class="table table-responsive" runat="server" visible="false">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                Width="100%" CssClass="table-bordered" OnItemCommand="dtgConsulta_EditCommand"
                AllowPaging="true" PageSize="10" OnPageIndexChanged="dtgConsulta_PageIndexChanged">
                <Columns>
                    <asp:BoundColumn DataField="puede_modif" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn DataField="numero_contrato" HeaderText="No. Operacion" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="numero_id" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociacion" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="operador_compra" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn DataField="nombre_comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="operador_venta" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn DataField="nombre_vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="desc_mercado" HeaderText="Mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="desc_punto" HeaderText="Punto/Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                        <ItemTemplate>
                            <div class="dropdown dropdown-inline">
                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="flaticon-more-1"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                    <!--begin::Nav-->
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <asp:LinkButton ID="lkbConsultar" CssClass="kt-nav__link" CommandName="Consultar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-expand"></i>
                                                            <span class="kt-nav__link-text">Consultar</span>
                                                    </asp:LinkButton>
                                                </li>
                                            </ul>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <!--end::Nav-->
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
            </asp:DataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

<%--Modals --%>

<%--Agregar--%>
<%--20210728 --%>
<div class="modal fade" style ="overflow:scroll; " id="mdlContAgregar" tabindex="-1" role="dialog" aria-labelledby="mdlContAgregarLabel" aria-hidden="true" clientidmode="Static" runat="server">
    <div class="modal-dialog" id="mdlContAgregarInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="mdlContAgregarLabel" runat="server">Agreagar</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label Text="Destino Rueda" AssociatedControlID="DdlDestino" runat="server" />
                                    <asp:DropDownList ID="DdlDestino" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlDestino_SelectedIndexChanged" CssClass="form-control selectpicker" data-live-search="true">
                                        <asp:ListItem Value="G" Text="Suministro de Gas"></asp:ListItem>
                                        <asp:ListItem Value="T" Text="Capacidad de Transporte"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label Text="Tipo Mercado" AssociatedControlID="DdlMercado" runat="server" />
                                    <asp:DropDownList ID="DdlMercado" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlMercado_SelectedIndexChanged" CssClass="form-control selectpicker" data-live-search="true">
                                        <asp:ListItem Value="P" Text="Primario"></asp:ListItem>
                                        <asp:ListItem Value="S" Text="Secundario"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label Text="Operador Comprador" AssociatedControlID="DdlMercado" runat="server" />
                                    <asp:DropDownList ID="DdlOperador" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label Text="Fecha Negociación" AssociatedControlID="DdlMercado" runat="server" />
                                    <asp:TextBox ID="TxtFechaNeg" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" ValidationGroup="detalle" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <asp:Label Text="Hora Negociación" AssociatedControlID="TxtHoraNeg" runat="server" />
                                        <asp:RegularExpressionValidator ID="revTxtHoraNeg" ControlToValidate="TxtHoraNeg" ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ErrorMessage="Formato Incorrecto para la hora de negociación"> * </asp:RegularExpressionValidator>
                                    </div>
                                    <asp:TextBox ID="TxtHoraNeg" runat="server" MaxLength="5" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label Text="Fecha Suscripción" AssociatedControlID="TxtFechaSus" runat="server" />
                                    <asp:TextBox ID="TxtFechaSus" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" ValidationGroup="detalle" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label ID="LblFuente" AssociatedControlID="DdlFuente" runat="server">Fuente</asp:Label>
                                    <asp:DropDownList ID="DdlFuente" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                </div>
                            </div>
                        </div>
                        <div id="trSumSec" class="row" runat="server" visible="false">
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label ID="Label1" AssociatedControlID="DdlConectado" runat="server">Conectado al SNT</asp:Label>
                                    <asp:DropDownList ID="DdlConectado" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlConectado_SelectedIndexChanged" CssClass="form-control selectpicker" data-live-search="true">
                                        <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                                        <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label ID="Label2" AssociatedControlID="DdlBoca" runat="server">Entrega en boca de bozo</asp:Label>
                                    <asp:DropDownList ID="DdlBoca" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlBoca_SelectedIndexChanged" CssClass="form-control selectpicker" data-live-search="true">
                                        <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                                        <asp:ListItem Value="N" Text="No" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label ID="LblPunto" AssociatedControlID="DdlPunto" runat="server">Punto de entrega de la energía al comprador</asp:Label>
                                    <asp:DropDownList ID="DdlPunto" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                    <asp:DropDownList ID="DdlCentro" runat="server" Visible="false" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label ID="Label3" AssociatedControlID="DdlModalidad" runat="server">Modalidad de Entregar</asp:Label>
                                    <asp:DropDownList ID="DdlModalidad" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label ID="Label4" AssociatedControlID="DdlPeriodo" runat="server">Periodo de Entrega</asp:Label>
                                    <asp:DropDownList ID="DdlPeriodo" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlPeriodo_SelectedIndexChanged" CssClass="form-control selectpicker" data-live-search="true" />
                                </div>
                            </div>
                            <div id="divAnios" class="col-sm-12 col-md-6 col-lg-6" visible="false">
                                <div class="form-group">
                                    <asp:Label ID="LblNoAños" AssociatedControlID="TxtNoAños" runat="server">No Años</asp:Label>
                                    <asp:TextBox ID="TxtNoAños" runat="server" MaxLength="3" CssClass="form-control" />
                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtNoAños" runat="server" Enabled="True" FilterType="Custom, Numbers" TargetControlID="TxtNoAños"></ajaxToolkit:FilteredTextBoxExtender>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label ID="Label6" AssociatedControlID="TxtFechaIni" runat="server">Fecha Entrega Inicial</asp:Label>
                                    <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/dd/mm" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" ValidationGroup="detalle" />
                                </div>
                            </div>
                            <div id="divHoraIni" cssclass="col-sm-12 col-md-6 col-lg-6" runat="server" visible="false">
                                <div class="form-group">
                                    <asp:Label ID="lblHoraIni" AssociatedControlID="TxtHoraIni" runat="server">Hora Entrega Inicial</asp:Label>
                                    <asp:TextBox ID="TxtHoraIni" runat="server" MaxLength="5" CssClass="form-control"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RevTxtHoraIni" CssClass="form-control" ControlToValidate="TxtHoraIni" ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ErrorMessage="Formato Incorrecto para la Hora de entrega inicial"> * </asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label ID="Label5" AssociatedControlID="TxtFechaFin" runat="server">Fecha Entrega Final</asp:Label>
                                    <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/dd/mm" Width="100%" MaxLength="10" Enabled="false" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" ValidationGroup="detalle" />
                                </div>
                            </div>
                            <div id="divHoraFin" class="col-sm-12 col-md-6 col-lg-6" runat="server">
                                <div class="form-group">
                                    <asp:Label ID="Label7" AssociatedControlID="TxtHoraFin" runat="server">Hora Entrega Final</asp:Label>
                                    <asp:TextBox ID="TxtHoraFin" runat="server" MaxLength="5" CssClass="form-control"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="revTxtHoraFin" ControlToValidate="TxtHoraFin" ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ErrorMessage="Formato Incorrecto para la Hora de entrega final"> * </asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label ID="LblCantidad" AssociatedControlID="TxtCantidad" runat="server">Cantidad de energía contratada (MBTUD)</asp:Label>
                                    <asp:TextBox ID="TxtCantidad" runat="server" MaxLength="6" CssClass="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtCantidad" runat="server" Enabled="True" FilterType="Custom, Numbers" TargetControlID="TxtCantidad"></ajaxToolkit:FilteredTextBoxExtender>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label ID="LblPrecio" AssociatedControlID="TxtPrecio" runat="server">Precio a la fecha de suscripción del contrato (USD/MBTU)</asp:Label>
                                    <asp:TextBox ID="TxtPrecio" runat="server" MaxLength="6" CssClass="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="True" FilterType="Custom, Numbers" TargetControlID="TxtPrecio" ValidChars="."></ajaxToolkit:FilteredTextBoxExtender>
                                </div>
                            </div>
                        </div>
                        <div id="trFlujo" runat="server" class="row" visible="false">
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label runat="server" AssociatedControlID="DdlSentido">Sentido del flujo</asp:Label>
                                    <asp:DropDownList ID="DdlSentido" CssClass="form-control selectpicker" data-live-search="true" runat="server">
                                        <asp:ListItem Value="NORMAL" Text="NORMAL"></asp:ListItem>
                                        <asp:ListItem Value="CONTRAFLUJO" Text="CONTRAFLUJO"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label AssociatedControlID="TxtPresion" runat="server">Presión del punto final</asp:Label>
                                    <asp:TextBox ID="TxtPresion" runat="server" CssClass="form-control" MaxLength="500" Text="0"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtPresion" runat="server" Enabled="True" FilterType="Custom, Numbers" TargetControlID="TxtPresion" ValidChars="-"></ajaxToolkit:FilteredTextBoxExtender>
                                </div>
                            </div>
                            <%--20201124--%>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label AssociatedControlID="DdlClasifica" runat="server">Clasificación contrato transporte</asp:Label>
                                    <asp:DropDownList ID="DdlClasifica" CssClass="form-control selectpicker" data-live-search="true" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label AssociatedControlID="TxtContratoDef" runat="server">Contrato definitivo</asp:Label>
                                    <asp:TextBox ID="TxtContratoDef" runat="server" MaxLength="30" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:Label AssociatedControlID="DdlVariable" runat="server">Contrato Variable</asp:Label>
                                    <asp:DropDownList ID="DdlVariable" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                        <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                        <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <%--20201124--%>
                            <div class="col-sm-12 col-md-6 col-lg-6" id="divMsTra" runat="server">
                                <div class="form-group">
                                    <asp:Label AssociatedControlID="TxtIdTraMp" runat="server">Ids MP Transporte, Separados por guiones(-)</asp:Label>
                                    <asp:TextBox ID="TxtIdTraMp" runat="server" CssClass="form-control" />
                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtIdTraMp" runat="server" TargetControlID="TxtIdTraMp"
                                        FilterType="Custom, Numbers" ValidChars="-"></ajaxToolkit:FilteredTextBoxExtender>
                                </div>
                            </div>
                            <%--20210707 trm y moneda--%>
                            <div class="col-sm-12 col-md-6 col-lg-6" >
                                <div class="form-group">
                                    <asp:Label AssociatedControlID="ddlTrm" runat="server">Tasa de Cambio</asp:Label>
                                    <asp:DropDownList ID="ddlTrm" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <%--20210707 trm y moneda--%>
                            <div class="col-sm-12 col-md-6 col-lg-6" >
                                <div class="form-group">
                                    <asp:Label AssociatedControlID="TxtObsTrm" runat="server">Observaciones Tasa de Cambio</asp:Label>
                                    <asp:TextBox ID="TxtObsTrm" runat="server" CssClass="form-control" MaxLength ="300" />
                                </div>
                            </div>
                            <%--20210707 trm y moneda--%>
                            <div class="col-sm-12 col-md-6 col-lg-6" >
                                <div class="form-group">
                                    <asp:Label AssociatedControlID="ddlMoneda" runat="server">Tipo Moneda</asp:Label>
                                    <asp:DropDownList ID="ddlMoneda" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                        <asp:ListItem Value="USD" Text="USD"></asp:ListItem>
                                        <asp:ListItem Value="COP" Text="COP"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <asp:ValidationSummary ID="ErrorSummary" ValidationGroup="comi" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <asp:Button ID="btnLimpiar" CssClass="btn btn-primary" runat="server" Text="Limpiar Campos" OnClick="btnLimpiar_Click" />
                        <%--20210224--%><%--20210728--%>
                        <asp:Button ID="btnInsertar" CssClass="btn btn-primary" runat="server" Text="Insertar Otro" OnClick="btnInsertar_Click" ValidationGroup="comi"  />
                        <asp:Button ID="btnCrear" CssClass="btn btn-primary" runat="server" Text="Crear" OnClick="btnCrear_Click" ValidationGroup="comi" />
                        <asp:Button ID="btnModificar" CssClass="btn btn-primary" runat="server" Text="Modificar" OnClick="btnModificar_Click" ValidationGroup="comi" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>

<%--Confirmación--%>
<div class="modal fade" id="mdlFechaOpBil" tabindex="-1" role="dialog" aria-labelledby="mdlFechaOpBilLabel" aria-hidden="true" clientidmode="Static" runat="server">
    <div class="modal-dialog" id="mdlFechaOpBilInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="mdlFechaOpBilLabel" runat="server">Fecha Máxima de Registro de Operaciones Bilaterales</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblMensajeReg" runat="server"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <asp:Button ID="btnAceptar" CssClass="btn btn-primary" Text="Aceptar" OnClick="BtnAceptar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" runat="server" />
                    </div>

                    <asp:HiddenField ID="HndTiempo" runat="server" />
                    <asp:HiddenField ID="hndIndica" runat="server" />
                    <asp:HiddenField ID="hdfMesInicialPeriodo" runat="server" />
                    <asp:HiddenField ID="hdfDiasIni" runat="server" />
                    <asp:HiddenField ID="hndContrato" runat="server" />
                    <asp:HiddenField ID="hndConsec" Value="0" runat="server" />
                    <asp:HiddenField ID="hndActual" Value="0" runat="server" />
                    <asp:HiddenField ID="hndIndModif" Value="0" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>

<%--20210224 pasa validaciones--%>
<div class="modal fade" id="mdlConfirma" tabindex="-1" role="dialog" aria-labelledby="mdlConfirmaLabel" aria-hidden="true" clientidmode="Static" runat="server">
    <div class="modal-dialog" id="mdlConfirmaInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                <ContentTemplate>
                    <div class="modal-header" style="background-color: #3E5F8A; "  >
                        <h5 class="modal-title" id="mdlConfirmaLabel" runat="server" style ="color: white;">Confirmación</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"  style="color: #fff; opacity: 1;">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="background-color: #D3D3D3">
                        <asp:Label ID="lblMensajeCOnf" runat="server">Se presentan las siguientes validaciones:</asp:Label>
                    </div>
                    <div class="modal-body" style="background-color: #D3D3D3">
                        <asp:Label ID="lblValidacion" runat="server"></asp:Label>
                    </div>
                    <div class="modal-body" style="background-color: #D3D3D3">
                        <asp:Label ID="lblConforma" runat="server">Desea continuar de todas formas?</asp:Label>
                    </div>
                    <div class="modal-footer" style="background-color: #3E5F8A">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <asp:Button ID="btnAceptarConf" CssClass="btn btn-primary" Text="Aceptar" OnClick="BtnAceptarConf_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
