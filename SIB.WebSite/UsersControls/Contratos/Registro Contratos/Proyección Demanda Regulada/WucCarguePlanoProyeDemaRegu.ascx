﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucCarguePlanoProyeDemaRegu.ascx.cs" Inherits="UsersControls.Contratos.Registro_Contratos.Proyección_Demanda_Regulada.WucCarguePlanoProyeDemaRegu" %>

<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label ID="Label2" Text="Archivo" AssociatedControlID="FuArchivo" runat="server" />
            <asp:FileUpload ID="FuArchivo" runat="server" EnableTheming="true" CssClass="form-control" />
        </div>
    </div>
</div>
