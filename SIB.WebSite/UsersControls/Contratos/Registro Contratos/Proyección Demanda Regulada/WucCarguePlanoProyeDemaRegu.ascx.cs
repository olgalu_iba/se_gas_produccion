﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace UsersControls.Contratos.Registro_Contratos.Proyección_Demanda_Regulada
{
    public partial class WucCarguePlanoProyeDemaRegu : UserControl
    {
        #region EventHandler

        /// <summary>
        /// Delegado para el manejo de los toastr
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        /// EventHandler para la selección de los toastr 
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        /// Delegado para el manejo del log de cargue de archivos
        /// </summary>
        public delegate void EventHandlerLogCargaArchivo(StringBuilder message);

        /// <summary>
        /// EventHandler para el log de cargue de archivos
        /// </summary>
        public event EventHandlerLogCargaArchivo LogCargaArchivoEvent;

        /// <summary>
        /// Delegado para la selección de los botones  
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get { return ViewState["NameController"] != null && !string.IsNullOrEmpty(ViewState["NameController"].ToString()) ? ViewState["NameController"].ToString() : string.Empty; }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        #endregion Propiedades

        private InfoSessionVO goInfo;
        private clConexion lConexion;
        private string strRutaCarga;
        private string strRutaFTP;
        private SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                goInfo = (InfoSessionVO)Session["infoSession"];
                strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
                strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            EnumBotones[] botones = { EnumBotones.Cargue };
            ButtonsEvent?.Invoke(botones, null, NameController);
            //Se establece que el controlador ya se inicializo    
            Inicializado = true;

            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
            /// Validacion Para validar el periodo disponible para hacer la declaracion de la informacion 20170310 ///
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
            var lsFechaIni = "";
            var lsHoraIni = "";
            var lsFechaFin = "";
            var lsHoraFin = "";
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
            if (lLector.HasRows)
            {
                lLector.Read();

                lsFechaIni = lLector["fecha_ini_reg_demanda"].ToString().Substring(6, 4) + lLector["fecha_ini_reg_demanda"].ToString().Substring(2, 4) + lLector["fecha_ini_reg_demanda"].ToString().Substring(0, 2);
                lsHoraIni = lLector["hora_ini_reg_demanda"].ToString();
                lsFechaFin = lLector["fecha_fin_reg_demanda"].ToString().Substring(6, 4) + lLector["fecha_fin_reg_demanda"].ToString().Substring(2, 4) + lLector["fecha_fin_reg_demanda"].ToString().Substring(0, 2);
                lsHoraFin = lLector["hora_fin_reg_demanda"].ToString();
            }
            lLector.Close();
            lLector.Dispose();
            var lsError = "";
            if (Convert.ToDateTime(DateTime.Now.ToShortDateString()) < Convert.ToDateTime(lsFechaIni) || Convert.ToDateTime(DateTime.Now.ToShortDateString()) > Convert.ToDateTime(lsFechaFin))
            {
                lsError = "No está dentro de la Fecha disponible para el registro de la información";
            }
            else
            {
                if (DateTime.Now < Convert.ToDateTime(lsFechaIni + " " + lsHoraIni) || DateTime.Now > Convert.ToDateTime(lsFechaFin + " " + lsHoraFin))
                {
                    lsError = "No está dentro del horario disponible para el registro de la información";
                }
            }
            if (lsError != "")
            {
                ToastrEvent?.Invoke(lsError, EnumTypeToastr.Error);
            }

            lConexion.Cerrar();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void BtnCargar_Click(object sender, EventArgs e)
        {
            string lsRutaArchivo = "";
            string lsNombre = "";
            string[] lsErrores = { "", "" };
            var lsCadenaArchivo = new StringBuilder();
            SqlDataReader lLector;
            SqlCommand lComando = new SqlCommand();
            Int32 liNumeroParametros = 0;
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_codigo_operador", "@P_ruta_ftp" };
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar };
            Object[] lValorParametrosO = { "", goInfo.cod_comisionista, strRutaFTP };
            lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;

            /// Realiza las Validaciones de los Archivos
            try
            {
                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);

                if (FuArchivo.FileName != "")
                    lsErrores = ValidarArchivo(lsRutaArchivo);
                else
                {
                    ToastrEvent?.Invoke("Debe seleccionar el archivo", EnumTypeToastr.Error);
                    return;
                }

                if (lsErrores[0] == "")
                {
                    if (DelegadaBase.Servicios.put_archivo(lsRutaArchivo,
                            ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre,
                            ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]))
                    {
                        lValorParametrosO[0] = lsNombre;
                        lValorParametrosO[1] = goInfo.cod_comisionista;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = "pa_ValidaPlanoInfProyDemReg";
                        lComando.CommandTimeout = 3600;
                        if (lsNombreParametrosO != null)
                        {
                            for (liNumeroParametros = 0;
                                liNumeroParametros <= lsNombreParametrosO.Length - 1;
                                liNumeroParametros++)
                            {
                                lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                            }
                        }

                        lLector = lComando.ExecuteReader();
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                            }
                        }
                        else
                        {
                            ToastrEvent?.Invoke("Archivo cargado satisfactoriamente.!", EnumTypeToastr.Success);
                        }

                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        ToastrEvent?.Invoke("Se presento un Problema en el {FTP} del Archivo al Servidor.!", EnumTypeToastr.Error);
                    }
                }
                else
                {
                    lsCadenaArchivo.Append(lsErrores[0]);
                    DelegadaBase.Servicios.registrarProceso(goInfo, "Finalizó la Carga con Errores", "Usuario : " + goInfo.nombre);
                }

                if (!string.IsNullOrEmpty(lsCadenaArchivo.ToString()) &&
                    lsCadenaArchivo.ToString().Contains("Se crearon correctamente"))
                {
                    string[] delim = { Environment.NewLine, "\n" }; // "\n" added in case you manually appended a newline
                    var lines = lsCadenaArchivo.ToString().Split(delim, StringSplitOptions.None);
                    lsCadenaArchivo.Clear();
                    foreach (var line in lines)
                    {
                        if (!line.Contains("Se crearon correctamente"))
                            lsCadenaArchivo.Append(line);
                        else
                            ToastrEvent?.Invoke(line, EnumTypeToastr.Success);
                    }
                }

                if (!string.IsNullOrEmpty(lsCadenaArchivo.ToString()))
                    //Se descarga el log para el usuario
                    LogCargaArchivoEvent?.Invoke(lsCadenaArchivo);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Se presento un Problema Al cargar los datos del archivo.! <br>" + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            Int32 liNumeroLinea = 0;
            decimal ldValor = 0;
            int liTotalRegistros = 0;
            Int32 liValor = 0;
            DateTime ldFecha;
            string lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            StreamReader lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 6)) //20170705 rq025-17 indicadores MP fase III
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 6 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; //20170705 rq025-17 indicadores MP fase III
                    }
                    else
                    {
                        /// Validar Operador
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(0).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del operador {" + oArregloLinea.GetValue(0) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Ano
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(1).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El Año {" + oArregloLinea.GetValue(1) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida mes
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(2).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El Mes {" + oArregloLinea.GetValue(2) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida  Sector de Consumo
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(3).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El Sector de Consumo {" + oArregloLinea.GetValue(3) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida  Punto de Salida del SNT
                        /// //20170705 rq025-17 indicadores MP fase III
                        //try
                        //{
                        //    liValor = Convert.ToInt32(oArregloLinea.GetValue(4).ToString());
                        //}
                        //catch (Exception ex)
                        //{
                        //    lsCadenaErrores = lsCadenaErrores + "El Punto de Salida {" + oArregloLinea.GetValue(4).ToString() + "} es inválido, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        //}
                        /// Valida Mercado Relevante
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(4).ToString()); //20170705 rq025-17 indicadores MP fase III
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El Mercado Relevante {" + oArregloLinea.GetValue(4) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>"; //20170705 rq025-17 indicadores MP fase III
                        }
                        /// Valida Cantidad / Capacidad
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(5).ToString()); //20170705 rq025-17 indicadores MP fase III
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La Cantidad / Capacidad {" + oArregloLinea.GetValue(5) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>"; //20170705 rq025-17 indicadores MP fase III
                        }
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }
    }
}