﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucProyeDemandaRegulada.ascx.cs" Inherits="UsersControls.Contratos.Registro_Contratos.Proyección_Demanda_Regulada.WucProyeDemandaRegulada" %>

<%--Contenido--%>
<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Código Proyección Demanda Regulada" AssociatedControlID="TxtBusCodigo" runat="server" />
            <asp:TextBox ID="TxtBusCodigo" CssClass="form-control" runat="server" autocomplete="off" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Sector de Consumo" AssociatedControlID="TxtBusCodigo" runat="server" />
            <asp:DropDownList ID="ddbBusSectorConsumo" CssClass="form-control selectpicker" data-live-search="true" runat="server" />
        </div>
    </div>
</div>
<%--Grilla--%>
<div class="table table-responsive" runat="server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True" PagerStyle-HorizontalAlign="Center"
                Width="100%" CssClass="table-bordered" OnItemCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged" PageSize="10">
                <Columns>
                    <asp:BoundColumn DataField="codigo_proy_demreg" HeaderText="Código Proyección Demanda Regulada" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="fecha_registro" HeaderText="Fec Registro" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="nom_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="sector_consumo" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--20170705 rq025-17--%>
                    <%--<asp:BoundColumn DataField="punto_salida" HeaderText="Punto de Salida" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>--%>
                    <asp:BoundColumn DataField="mercado_relevante" HeaderText="Mercado Relevante" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualizacion" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass="" Visible="False" />
                    <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar" Visible="False" />
                    <asp:BoundColumn DataField="codigo_sector_consumo" Visible="false"></asp:BoundColumn>
                    <%--20170705 rq025-17--%>
                    <%--<asp:BoundColumn DataField="codigo_punto_salida" visible ="false">
                            </asp:BoundColumn>--%>
                    <asp:BoundColumn DataField="codigo_mercado_relevante" Visible="false"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                        <ItemTemplate>
                            <div class="dropdown dropdown-inline">
                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="flaticon-more-1"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                    <!--begin::Nav-->
                                    <ul class="kt-nav">
                                        <li class="kt-nav__item">
                                            <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                             <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Modificar</span>
                                            </asp:LinkButton>
                                        </li>
                                        <li class="kt-nav__item">
                                            <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon-delete"></i>
                                                            <span class="kt-nav__link-text">Eliminar</span>
                                            </asp:LinkButton>
                                        </li>
                                    </ul>
                                    <!--end::Nav-->
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
                <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
            </asp:DataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

<%--Madals--%>

<%--Modal Agregar--%>
<div class="modal fade" id="mdlContAgregar" tabindex="-1" role="dialog" aria-labelledby="mdlContAgregarLabel" aria-hidden="true" clientidmode="Static" runat="server">
    <div class="modal-dialog" id="mdlContAgregarInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="mdlContAgregarLabel" runat="server">Agregar</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label>Código Proyección Demanda Regulada</label>
                                    <asp:TextBox ID="TxtCodigoProy" CssClass="form-control" runat="server" MaxLength="3"></asp:TextBox>
                                    <asp:Label ID="LblCodigoProy" CssClass="form-control" runat="server" Visible="False"></asp:Label>
                                    <asp:HiddenField ID="hndPerIni" runat="server" />
                                    <asp:HiddenField ID="hndPerFin" runat="server" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label>Sector de Consumo</label>
                                    <asp:DropDownList ID="ddlSectorConsumo" CssClass="form-control selectpicker" data-live-search="true" runat="server" />
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label>Mercado Relevante</label>
                                    <asp:DropDownList ID="ddlMercadoRelevante" CssClass="form-control selectpicker" data-live-search="true" runat="server" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label>Cantidad </label>
                                    <asp:Label ID="lblMes01" runat="server"></asp:Label>
                                    <asp:TextBox ID="TxtCantidad01" runat="server" type="number" min="1" pattern="^[0-9]+" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label>Cantidad</label>
                                    <asp:Label ID="lblMes02" runat="server"></asp:Label>
                                    <asp:TextBox ID="TxtCantidad02" type="number" min="1" pattern="^[0-9]+" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label>Cantidad</label>
                                    <asp:Label ID="lblMes03" runat="server"></asp:Label>
                                    <asp:TextBox ID="TxtCantidad03" type="number" min="1" pattern="^[0-9]+" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label>Cantidad</label>
                                    <asp:Label ID="lblMes04" runat="server"></asp:Label>
                                    <asp:TextBox ID="TxtCantidad04" type="number" min="1" pattern="^[0-9]+" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label>Cantidad</label>
                                    <asp:Label ID="lblMes05" runat="server"></asp:Label>
                                    <asp:TextBox ID="TxtCantidad05" type="number" min="1" pattern="^[0-9]+" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label>Cantidad</label>
                                    <asp:Label ID="lblMes06" runat="server"></asp:Label>
                                    <asp:TextBox ID="TxtCantidad06" type="number" min="1" pattern="^[0-9]+" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label>Cantidad</label>
                                    <asp:Label ID="lblMes07" runat="server"></asp:Label>
                                    <asp:TextBox ID="TxtCantidad07" type="number" min="1" pattern="^[0-9]+" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label>Cantidad</label>
                                    <asp:Label ID="lblMes08" runat="server"></asp:Label>
                                    <asp:TextBox ID="TxtCantidad08" type="number" min="1" pattern="^[0-9]+" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label>Cantidad</label>
                                    <asp:Label ID="lblMes09" runat="server"></asp:Label>
                                    <asp:TextBox ID="TxtCantidad09" type="number" min="1" pattern="^[0-9]+" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label>Cantidad</label>
                                    <asp:Label ID="lblMes10" runat="server"></asp:Label>
                                    <asp:TextBox ID="TxtCantidad10" type="number" min="1" pattern="^[0-9]+" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label>Cantidad</label>
                                    <asp:Label ID="lblMes11" runat="server"></asp:Label>
                                    <asp:TextBox ID="TxtCantidad11" type="number" min="1" pattern="^[0-9]+" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label>Cantidad</label>
                                    <asp:Label ID="lblMes12" runat="server"></asp:Label>
                                    <asp:TextBox ID="TxtCantidad12" type="number" min="1" pattern="^[0-9]+" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="Button2" runat="server" class="btn btn-secondary" Text="Cancelar" OnClick="BtnCancelar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnCrear" runat="server" Text="Crear" CssClass="btn btn-primary" OnClick="btnCrear_Click" ValidationGroup="comi" />
                        <asp:Button ID="btnActualizar" runat="server" Text="Actualizar" CssClass="btn btn-primary" OnClick="btnActualizar_Click" ValidationGroup="comi" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
