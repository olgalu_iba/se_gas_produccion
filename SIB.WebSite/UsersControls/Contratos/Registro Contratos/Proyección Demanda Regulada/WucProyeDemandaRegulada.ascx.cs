﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace UsersControls.Contratos.Registro_Contratos.Proyección_Demanda_Regulada
{
    public partial class WucProyeDemandaRegulada : UserControl
    {
        #region EventHandler
        /// <summary>
        /// Delegado para el manejo de los modals
        /// </summary>
        public delegate void EventHandlerModal(string id, string insideId, EnumTypeModal typeModal, Modal.Size size = Modal.Size.Large);

        /// <summary>
        /// EventHandler para la selección de los los modals
        /// </summary>
        public event EventHandlerModal ModalEvent;

        /// <summary>
        /// Delegado para el manejo de los toastr
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        /// EventHandler para la selección de los toastr 
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        /// Delegado para la selección de los botones  
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get { return ViewState["NameController"] != null && !string.IsNullOrEmpty(ViewState["NameController"].ToString()) ? ViewState["NameController"].ToString() : string.Empty; }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        #endregion Propiedades

        private InfoSessionVO goInfo = null;
        private static string lsTitulo = "Proyección Demanda Regulada";
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        private clConexion lConexion = null;
        private clConexion lConexion1 = null;
        private SqlDataReader lLector;

        private string lsIndica
        {
            get
            {
                if (ViewState["lsIndica"] == null)
                    return "";
                else
                    return (string)ViewState["lsIndica"];
            }
            set
            {
                ViewState["lsIndica"] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            //Controlador util = new Controlador();
            /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
             *   lsIndica = N -> Nuevo (Creacion)
             *   lsIndica = L -> Listar Registros (Grilla)
             *   lsIndica = M -> Modidificar
             *   lsIndica = B -> Buscar
             * */

            //Establese los permisos del sistema

            lConexion = new clConexion(goInfo);
            lConexion1 = new clConexion(goInfo);

            if (IsPostBack) return;
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            lConexion.Abrir();
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
            /// Validacion Para validar el periodo disponible para hacer la declaracion de la informacion 20170310 ///
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
            string lsFechaIni = "";
            string lsHoraIni = "";
            string lsFechaFin = "";
            string lsHoraFin = "";
            EnumBotones[] botones = null;
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
            if (lLector.HasRows)
            {
                lLector.Read();

                lsFechaIni = lLector["fecha_ini_reg_demanda"].ToString().Substring(6, 4) + lLector["fecha_ini_reg_demanda"].ToString().Substring(2, 4) + lLector["fecha_ini_reg_demanda"].ToString().Substring(0, 2);
                lsHoraIni = lLector["hora_ini_reg_demanda"].ToString();
                lsFechaFin = lLector["fecha_fin_reg_demanda"].ToString().Substring(6, 4) + lLector["fecha_fin_reg_demanda"].ToString().Substring(2, 4) + lLector["fecha_fin_reg_demanda"].ToString().Substring(0, 2);
                lsHoraFin = lLector["hora_fin_reg_demanda"].ToString();
            }
            lLector.Close();
            lLector.Dispose();
            string lsError = "";
            if (Convert.ToDateTime(DateTime.Now.ToShortDateString()) < Convert.ToDateTime(lsFechaIni) || Convert.ToDateTime(DateTime.Now.ToShortDateString()) > Convert.ToDateTime(lsFechaFin))
            {
                EnumBotones[] btn = { EnumBotones.Excel, EnumBotones.Pdf };
                botones = btn;


                dtgMaestro.Visible = false;
                // Se cierra el modal
                ModalEvent?.Invoke(mdlContAgregar.ID, mdlContAgregarInside.ID, EnumTypeModal.Cerrar);
                lsError = "No está dentro de la Fecha disponible para el registro de la información";
            }
            else
            {
                if (DateTime.Now < Convert.ToDateTime(lsFechaIni + " " + lsHoraIni) || DateTime.Now > Convert.ToDateTime(lsFechaFin + " " + lsHoraFin))
                {

                    dtgMaestro.Visible = false;
                    // Se cierra el modal
                    ModalEvent?.Invoke(mdlContAgregar.ID, mdlContAgregarInside.ID, EnumTypeModal.Cerrar);
                    EnumBotones[] btn = { EnumBotones.Excel, EnumBotones.Pdf };
                    botones = btn;
                    lsError = "No está dentro del horario disponible para el registro de la información";
                }
            }
            if (lsError != "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "StartupAlert", "alert('" + lsError + "');", true);
            }
            else
            {
                //////////////////////////////////////////////////////////////////////////////////////////////////////////
                // Carga informacion de combos
                LlenarControles(lConexion.gObjConexion, ddlSectorConsumo, "m_sector_consumo", " estado = 'A'  order by descripcion", 0, 1);
                LlenarControles(lConexion.gObjConexion, ddbBusSectorConsumo, "m_sector_consumo", " estado = 'A'  order by descripcion", 0, 1);
                //LlenarControles(lConexion.gObjConexion, ddlPuntoSalida, "m_punto_salida_snt", " estado = 'A'  order by descripcion", 0, 2);  //20170705 rq025-17 INdicador MP fase III
                //LlenarControles(lConexion.gObjConexion, ddlBusPuntoSalida, "m_punto_salida_snt", " estado = 'A'  order by descripcion", 0, 2); //20170705 rq025-17 INdicador MP fase III
                LlenarControles(lConexion.gObjConexion, ddlMercadoRelevante, "m_mercado_relevante", " estado = 'A'  order by descripcion", 0, 1);
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetProyDemandaReguladaMes", null, null, null);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lblMes01.Text = lLector["mes_01"].ToString();
                    lblMes02.Text = lLector["mes_02"].ToString();
                    lblMes03.Text = lLector["mes_03"].ToString();
                    lblMes04.Text = lLector["mes_04"].ToString();
                    lblMes05.Text = lLector["mes_05"].ToString();
                    lblMes06.Text = lLector["mes_06"].ToString();
                    lblMes07.Text = lLector["mes_07"].ToString();
                    lblMes08.Text = lLector["mes_08"].ToString();
                    lblMes09.Text = lLector["mes_09"].ToString();
                    lblMes10.Text = lLector["mes_10"].ToString();
                    lblMes11.Text = lLector["mes_11"].ToString();
                    lblMes12.Text = lLector["mes_12"].ToString();
                    hndPerIni.Value = lLector["periodo_ini"].ToString();
                    hndPerFin.Value = lLector["periodo_fin"].ToString();
                }
                else
                {
                    hndPerIni.Value = "0";
                    hndPerFin.Value = "0";
                }
                lLector.Close();
                lConexion.Cerrar();
                EstablecerPermisosSistema();
                //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
                if (Request.QueryString["lsIndica"] != null && Request.QueryString["lsIndica"] != "")
                {
                    lsIndica = Request.QueryString["lsIndica"];
                }
                if (lsIndica == null || lsIndica == "" || lsIndica == "L")
                {
                    CargarDatos();
                }
                else if (lsIndica == "N")
                {
                    Nuevo();
                }
            }

            if (botones == null)
            {
                EnumBotones[] btn = { EnumBotones.Crear, EnumBotones.Buscar, EnumBotones.Excel, EnumBotones.Pdf };
                botones = btn;

            }
            ButtonsEvent?.Invoke(botones, null, NameController);

            //Se establece que el controlador ya se inicializo    
            Inicializado = true;
        }

        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "t_proy_demanda_regulada");

            foreach (DataGridItem Grilla in dtgMaestro.Items)
            {
                var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                lkbModificar.Visible = (bool)permisos["UPDATE"];  //20170705 rq025-17 INdicador MP fase III
                var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
                lkbEliminar.Visible = (bool)permisos["DELETE"]; //20170705 rq025-17 INdicador MP fase III
            }
        }

        /// <summary>
        /// Nombre: Nuevo
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
        /// Modificacion:
        /// </summary>
        public void Nuevo()
        {
            // Se abre el modal
            ModalEvent?.Invoke(mdlContAgregar.ID, mdlContAgregarInside.ID, EnumTypeModal.Abrir);
            btnCrear.Visible = true;
            btnActualizar.Visible = false;

            TxtCodigoProy.Visible = false;
            LblCodigoProy.Visible = true;
            LblCodigoProy.Text = "Automatico";
            TxtCantidad01.Enabled = true;
            TxtCantidad02.Enabled = true;
            TxtCantidad03.Enabled = true;
            TxtCantidad04.Enabled = true;
            TxtCantidad05.Enabled = true;
            TxtCantidad06.Enabled = true;
            TxtCantidad07.Enabled = true;
            TxtCantidad08.Enabled = true;
            TxtCantidad09.Enabled = true;
            TxtCantidad10.Enabled = true;
            TxtCantidad11.Enabled = true;
            TxtCantidad12.Enabled = true;
            ddlSectorConsumo.Enabled = true;
            //ddlPuntoSalida.Enabled = true;  //20170705 rq025-17 INdicador MP fase III
            ddlMercadoRelevante.Enabled = true;

            ModalEvent?.Invoke(mdlContAgregar.ID, mdlContAgregarInside.ID, EnumTypeModal.Abrir);
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            var lblMensaje = new StringBuilder();

            if (modificar != null && modificar != "")
            {
                mdlContAgregarLabel.InnerText = "Modificar";
                try
                {
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        string[] lsNombreParametros = { "@P_codigo_proy_demreg" };
                        SqlDbType[] lTipoparametros = { SqlDbType.Int };
                        string[] lValorParametros = { modificar };

                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetProyDemandaReguladaMod", lsNombreParametros, lTipoparametros, lValorParametros);
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            //LblCodigoProy.Text = lLector["codigo_proy_demreg"].ToString();
                            //TxtCodigoProy.Text = lLector["codigo_proy_demreg"].ToString();
                            //TxtAno.Text = lLector["ano"].ToString();
                            //ddlMes.SelectedValue = lLector["mes"].ToString();
                            TxtCantidad01.Enabled = true;
                            TxtCantidad02.Enabled = true;
                            TxtCantidad03.Enabled = true;
                            TxtCantidad04.Enabled = true;
                            TxtCantidad05.Enabled = true;
                            TxtCantidad06.Enabled = true;
                            TxtCantidad07.Enabled = true;
                            TxtCantidad08.Enabled = true;
                            TxtCantidad09.Enabled = true;
                            TxtCantidad10.Enabled = true;
                            TxtCantidad11.Enabled = true;
                            TxtCantidad12.Enabled = true;
                            TxtCantidad01.Text = lLector["cantidad_01"].ToString();
                            TxtCantidad02.Text = lLector["cantidad_02"].ToString();
                            TxtCantidad03.Text = lLector["cantidad_03"].ToString();
                            TxtCantidad04.Text = lLector["cantidad_04"].ToString();
                            TxtCantidad05.Text = lLector["cantidad_05"].ToString();
                            TxtCantidad06.Text = lLector["cantidad_06"].ToString();
                            TxtCantidad07.Text = lLector["cantidad_07"].ToString();
                            TxtCantidad08.Text = lLector["cantidad_08"].ToString();
                            TxtCantidad09.Text = lLector["cantidad_09"].ToString();
                            TxtCantidad10.Text = lLector["cantidad_10"].ToString();
                            TxtCantidad11.Text = lLector["cantidad_11"].ToString();
                            TxtCantidad12.Text = lLector["cantidad_12"].ToString();
                            if (TxtCantidad01.Text == "")
                            {
                                TxtCantidad01.Text = "0";
                                TxtCantidad01.Enabled = false;
                            }
                            if (TxtCantidad02.Text == "")
                            {
                                TxtCantidad02.Text = "0";
                                TxtCantidad02.Enabled = false;
                            }
                            if (TxtCantidad03.Text == "")
                            {
                                TxtCantidad03.Text = "0";
                                TxtCantidad03.Enabled = false;
                            }
                            if (TxtCantidad04.Text == "")
                            {
                                TxtCantidad04.Text = "0";
                                TxtCantidad04.Enabled = false;
                            }
                            if (TxtCantidad05.Text == "")
                            {
                                TxtCantidad05.Text = "0";
                                TxtCantidad05.Enabled = false;
                            }
                            if (TxtCantidad06.Text == "")
                            {
                                TxtCantidad06.Text = "0";
                                TxtCantidad06.Enabled = false;
                            }
                            if (TxtCantidad07.Text == "")
                            {
                                TxtCantidad07.Text = "0";
                                TxtCantidad07.Enabled = false;
                            }
                            if (TxtCantidad08.Text == "")
                            {
                                TxtCantidad08.Text = "0";
                                TxtCantidad08.Enabled = false;
                            }
                            if (TxtCantidad09.Text == "")
                            {
                                TxtCantidad09.Text = "0";
                                TxtCantidad09.Enabled = false;
                            }
                            if (TxtCantidad10.Text == "")
                            {
                                TxtCantidad10.Text = "0";
                                TxtCantidad10.Enabled = false;
                            }
                            if (TxtCantidad11.Text == "")
                            {
                                TxtCantidad11.Text = "0";
                                TxtCantidad11.Enabled = false;
                            }
                            if (TxtCantidad12.Text == "")
                            {
                                TxtCantidad12.Text = "0";
                                TxtCantidad12.Enabled = false;
                            }
                            btnCrear.Visible = false;
                            btnActualizar.Visible = true;
                            TxtCodigoProy.Visible = false;
                            LblCodigoProy.Visible = true;
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Bloquea el Registro a Modificar
                        manejo_bloqueo("A", modificar);
                    }
                    else
                    {
                        CargarDatos();
                        lblMensaje.Append("No se Puede editar el Registro por que esta Bloqueado. Código Proyección Demanda Regulada " + modificar);
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append(ex.Message);
                }
            }

            if (lblMensaje.ToString() != "")
            {
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                return;
            }
            // Se abre el modal
            ModalEvent?.Invoke(mdlContAgregar.ID, mdlContAgregarInside.ID, EnumTypeModal.Abrir);
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_codigo_proy_demreg", "@P_codigo_sector_consumo", "@P_codigo_operador" }; //20170705 rq025-17 INdicador MP fase III
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int }; //20170705 rq025-17 INdicador MP fase III
            string[] lValorParametros = { "0", "0", goInfo.cod_comisionista }; //20170705 rq025-17 INdicador MP fase III

            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                if (ddbBusSectorConsumo.SelectedValue != "0")
                    lValorParametros[1] = ddbBusSectorConsumo.SelectedValue;
                //20170705 rq025-17 INdicador MP fase III
                //if (ddlBusPuntoSalida.SelectedValue != "0" && ddlBusPuntoSalida.SelectedValue != "")
                //    lValorParametros[2] = ddlBusPuntoSalida.SelectedValue;

                lConexion.Abrir();
                dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetProyDemandaRegulada", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgMaestro.DataBind();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnConsultar_Click(object sender, EventArgs e)
        {
            CargarDatos();
        }

        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();

        }
        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            string lCodigoRegistro = "";
            if (e.CommandName.Equals("Modificar"))
            {
                lCodigoRegistro = dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                try
                {
                    ddlSectorConsumo.SelectedValue = dtgMaestro.Items[e.Item.ItemIndex].Cells[12].Text; //20170705 rq025-17 indicadores MP fase III
                }
                catch (Exception ex)
                {
                }
                //20170705 rq025-17 INdicador MP fase III
                //try
                //{
                //    ddlPuntoSalida.SelectedValue = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[14].Text;
                //}
                //catch (Exception ex)
                //{
                //}
                try
                {
                    ddlMercadoRelevante.SelectedValue = dtgMaestro.Items[e.Item.ItemIndex].Cells[13].Text; //20170705 rq025-17 indicadores MP fase III
                }
                catch (Exception ex)
                {
                }
                LblCodigoProy.Text = dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                TxtCodigoProy.Text = dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                //TxtAno.Enabled = false;
                //ddlMes.Enabled = false;
                ddlSectorConsumo.Enabled = false;
                //ddlPuntoSalida.Enabled = false; //20170705 rq025-17 INdicador MP fase III
                ddlMercadoRelevante.Enabled = false;
                Modificar(lCodigoRegistro);
            }
            if (e.CommandName.Equals("Eliminar"))
            {
                lCodigoRegistro = dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                string[] lsNombreParametros = { "@P_codigo_proy_demreg", "@P_accion" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { lCodigoRegistro, "3" };

                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetProyDemandaRegulada", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    ToastrEvent?.Invoke("Se presentó un Problema en la Creación del Registro.!", EnumTypeToastr.Error);
                    lConexion.Cerrar();
                }
                else
                    CargarDatos();
            }
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia("t_proy_demanda_regulada", lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='t_proy_demanda_regulada' and llave_registro='codigo_proy_demreg=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_proy_demreg=" + lscodigo_registro;
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_proy_demanda_regulada";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_proy_demanda_regulada", lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ImgExcel_Click(object sender, EventArgs e)
        {

            string[] lsNombreParametros = { "@P_codigo_proy_demreg", "@P_codigo_sector_consumo", "@P_codigo_operador" }; //20170705 rq025-17 INdicador MP fase III
            string[] lValorParametros = { "0", "0", goInfo.cod_comisionista }; //20170705 rq025-17 INdicador MP fase III
            string lsParametros = "";

            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    lsParametros += " Codigo Proyeccíón : " + TxtBusCodigo.Text;
                }
                if (ddbBusSectorConsumo.SelectedValue != "0")
                {
                    lValorParametros[1] = ddbBusSectorConsumo.SelectedValue;
                    lsParametros += " Sector de Consumo: " + ddbBusSectorConsumo.SelectedItem;
                }
                //20170705 rq025-17 INdicador MP fase III
                //if (ddlBusPuntoSalida.SelectedValue != "0" && ddlBusPuntoSalida.SelectedValue != "")
                //{
                //    lValorParametros[2] = ddlBusPuntoSalida.SelectedValue;
                //    lsParametros += " Punto de Salida: " + ddlBusPuntoSalida.SelectedItem;
                //}
                lsParametros += " Operador: " + Session["NomOperador"];

                Server.Transfer("../../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetProyDemandaRegulada&nombreParametros=@P_codigo_proy_demreg*@P_codigo_sector_consumo*@P_codigo_operador&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "&columnas=codigo_capacidad*codigo_tramo*desc_tramo*codigo_trasportador*nombre_trasportador*capacidad_maxima_dia*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado de Proyección Demanda Regulada&TituloParametros=" + lsParametros); //20170705 rq025-17 INdicador MP fase III
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke($"No se Pudo Generar el Informe: {ex.Message}", EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: ImgPdf_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ImgPdf_Click(object sender, EventArgs e)
        {
            try
            {
                string lsCondicion = " codigo_proy_demreg <> '0'";
                if (TxtBusCodigo.Text.Trim().Length > 0)
                    lsCondicion += " and codigo_proy_demreg = " + TxtBusCodigo.Text.Trim();
                if (ddbBusSectorConsumo.SelectedValue != "0")
                    lsCondicion += " and codigo_sector_consumo =" + ddbBusSectorConsumo.SelectedValue;
                //20170705 rq025-17 INdicador MP fase III
                //if (ddlBusPuntoSalida.SelectedValue != "0" && ddlBusPuntoSalida.SelectedValue != "")
                //    lsCondicion += " and codigo_punto_salida =" + ddbBusSectorConsumo.SelectedValue;
                if (ddbBusSectorConsumo.SelectedValue != "0")
                    Server.Execute("../../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=t_proy_demanda_regulada&procedimiento=pa_ValidarExistencia&columnas=codigo_proy_demreg*fecha_registro*operador*ano*mes*codigo_sector_consumo*codigo_mercado_relevante*cantidad*login_usuario*fecha_hora_actual&condicion=" + lsCondicion); //20170705 rq025-17 INdicador MP fase III
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke($"No se Pudo Generar el Informe: {ex.Message}", EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCrear_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_proy_demreg", "@P_codigo_operador", "@P_codigo_sector_consumo", "@P_codigo_mercado_relevante", "@P_cantidad01", "@P_cantidad02", "@P_cantidad03", "@P_cantidad04", "@P_cantidad05", "@P_cantidad06", "@P_cantidad07", "@P_cantidad08", "@P_cantidad09", "@P_cantidad10", "@P_cantidad11", "@P_cantidad12", "@P_accion" }; //20170705 rq025-17 INdicador MP fase III
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int }; //20170705 rq025-17 INdicador MP fase III
            string[] lValorParametros = { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "1" }; //20170705 rq025-17 INdicador MP fase III
            var lblMensaje = new StringBuilder();

            try
            {
                mdlContAgregarLabel.InnerText = "Agregar";
                //if (Convert.ToInt32(TxtAno.Text) < DateTime.Now.Year)
                //    lblMensaje.Text += "El Año ingresado debe ser mayor o igual al año actual<br>";
                //if (ddlMes.SelectedValue == "0" || ddlMes.SelectedValue == "")
                //    lblMensaje.Text += "Debe seleccionar el Mes<br>";
                //else
                //{
                //    if (Convert.ToInt32(ddlMes.SelectedValue) <= DateTime.Now.Month)
                //        lblMensaje.Text += "El Mes ingresado debe ser mayor al mes actual<br>";
                //}
                if (ddlSectorConsumo.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el Sector de Consumo<br>");
                //20170705 rq025-17 INdicador MP fase III
                //if (ddlPuntoSalida.SelectedValue == "0" || ddlPuntoSalida.SelectedValue == "")
                //    lblMensaje.Text += "Debe seleccionar el Punto de Salida<br>";
                if (ddlMercadoRelevante.SelectedValue == "0" || ddlMercadoRelevante.SelectedValue == "")
                    lblMensaje.Append("Debe seleccionar el Mercado Relevante<br>");
                if (TxtCantidad01.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes01.Text + "<br>");
                if (TxtCantidad02.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes02.Text + "<br>");
                if (TxtCantidad03.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidadd para " + lblMes03.Text + "<br>");
                if (TxtCantidad04.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes04.Text + "<br>");
                if (TxtCantidad05.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes05.Text + "<br>");
                if (TxtCantidad06.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes06.Text + "<br>");
                if (TxtCantidad07.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes07.Text + "<br>");
                if (TxtCantidad08.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes08.Text + "<br>");
                if (TxtCantidad09.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes09.Text + "<br>");
                if (TxtCantidad10.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes10.Text + "<br>");
                if (TxtCantidad11.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes11.Text + "<br>");
                if (TxtCantidad12.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes12.Text + "<br>");

                if (VerificarExistencia(" operador = " + goInfo.cod_comisionista + " And codigo_sector_consumo= " + ddlSectorConsumo.SelectedValue + " And codigo_mercado_relevante = " + ddlMercadoRelevante.SelectedValue + " and ano*100 + mes between " + hndPerIni.Value + " and " + hndPerFin.Value + " and exists (select 1 from t_proy_demanda_regulada pro where pro.operador = " + goInfo.cod_comisionista + " and pro.codigo_sector_consumo = " + ddlSectorConsumo.SelectedValue + " and pro.codigo_mercado_relevante = " + ddlMercadoRelevante.SelectedValue + " and pro.ano*100 + pro.mes between " + hndPerIni.Value + " and " + hndPerFin.Value + " having count(1) >=12)")) //20170705 rq025-17 INdicador MP fase III
                    lblMensaje.Append(" La Proyección de demanda regulada ya está definida para el Operador, sector consumo y mercado relevante<br>"); //20170705 rq025-17 INdicador MP fase III

                if (lblMensaje.ToString() != "")
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }
                lValorParametros[1] = goInfo.cod_comisionista;
                lValorParametros[2] = ddlSectorConsumo.SelectedValue;
                //lValorParametros[3] = ddlPuntoSalida.SelectedValue; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[3] = ddlMercadoRelevante.SelectedValue;//20170705 rq025-17 INdicador MP fase III
                lValorParametros[4] = TxtCantidad01.Text;//20170705 rq025-17 INdicador MP fase III
                lValorParametros[5] = TxtCantidad02.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[6] = TxtCantidad03.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[7] = TxtCantidad04.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[8] = TxtCantidad05.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[9] = TxtCantidad06.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[10] = TxtCantidad07.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[11] = TxtCantidad08.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[12] = TxtCantidad09.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[13] = TxtCantidad10.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[14] = TxtCantidad11.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[15] = TxtCantidad12.Text;  //20170705 rq025-17 INdicador MP fase III
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetProyDemandaRegulada", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    ToastrEvent?.Invoke("Se presentó un Problema en la Creación del Registro.!", EnumTypeToastr.Error);
                    lConexion.Cerrar();
                }
                else
                {
                    ToastrEvent?.Invoke("Se creo exitosamente el registro ", EnumTypeToastr.Success);
                    CargarDatos();
                    BtnCancelar_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_proy_demreg", "@P_codigo_operador", "@P_codigo_sector_consumo", "@P_codigo_mercado_relevante", "@P_cantidad01", "@P_cantidad02", "@P_cantidad03", "@P_cantidad04", "@P_cantidad05", "@P_cantidad06", "@P_cantidad07", "@P_cantidad08", "@P_cantidad09", "@P_cantidad10", "@P_cantidad11", "@P_cantidad12", "@P_accion" }; //20170705 rq025-17 INdicador MP fase III
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int }; //20170705 rq025-17 INdicador MP fase III
            string[] lValorParametros = { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "2" }; //20170705 rq025-17 INdicador MP fase III
            var lblMensaje = new StringBuilder();
            try
            {
                if (ddlSectorConsumo.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el Sector de Consumo<br>");
                //20170705 rq025-17 INdicador MP fase III
                //if (ddlPuntoSalida.SelectedValue == "0" || ddlPuntoSalida.SelectedValue == "")
                //    lblMensaje.Text += "Debe seleccionar el Punto de Salida<br>";
                if (ddlMercadoRelevante.SelectedValue == "0" || ddlMercadoRelevante.SelectedValue == "")
                    lblMensaje.Append("Debe seleccionar el Mercado Relevante<br>");
                if (TxtCantidad01.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes01.Text + "<br>");
                if (TxtCantidad02.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes02.Text + "<br>");
                if (TxtCantidad03.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes03.Text + "<br>");
                if (TxtCantidad04.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes04.Text + "<br>");
                if (TxtCantidad05.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes05.Text + "<br>");
                if (TxtCantidad06.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes06.Text + "<br>");
                if (TxtCantidad07.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes07.Text + "<br>");
                if (TxtCantidad08.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes08.Text + "<br>");
                if (TxtCantidad09.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes09.Text + "<br>");
                if (TxtCantidad10.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes10.Text + "<br>");
                if (TxtCantidad11.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes11.Text + "<br>");
                if (TxtCantidad12.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad para " + lblMes12.Text + "<br>");
                if (lblMensaje.ToString() != "")
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }
                lValorParametros[0] = LblCodigoProy.Text;
                lValorParametros[1] = goInfo.cod_comisionista;
                lValorParametros[2] = ddlSectorConsumo.SelectedValue;
                //lValorParametros[3] = ddlPuntoSalida.SelectedValue; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[3] = ddlMercadoRelevante.SelectedValue; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[4] = TxtCantidad01.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[5] = TxtCantidad02.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[6] = TxtCantidad03.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[7] = TxtCantidad04.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[8] = TxtCantidad05.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[9] = TxtCantidad06.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[10] = TxtCantidad07.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[11] = TxtCantidad08.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[12] = TxtCantidad09.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[13] = TxtCantidad10.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[14] = TxtCantidad11.Text; //20170705 rq025-17 INdicador MP fase III
                lValorParametros[15] = TxtCantidad12.Text; //20170705 rq025-17 INdicador MP fase III

                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetProyDemandaRegulada", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    ToastrEvent?.Invoke("Se presento un Problema en la Actualizacion del Registro.!", EnumTypeToastr.Error);
                    lConexion.Cerrar();
                }
                else
                {
                    BtnCancelar_Click(null, null);
                    //Se notifica a el usuario que el registro fue actualizado de manera exitosa
                    ToastrEvent?.Invoke("Se realizo la actualización de forma exitosa.!", EnumTypeToastr.Success);
                    CargarDatos();
                }
            }
            catch (Exception ex)
            {
                manejo_bloqueo("E", LblCodigoProy.Text);
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCancelar_Click(object sender, EventArgs e)
        {
            if (mdlContAgregarLabel.InnerText.Equals("Modificar"))
                /// Desbloquea el Registro Actualizado
                manejo_bloqueo("E", LblCodigoProy.Text);
            //Se limpia el modal de agregar
            LimpiarModalAgregar();
            ModalEvent?.Invoke(mdlContAgregar.ID, mdlContAgregarInside.ID, EnumTypeModal.Cerrar);
        }

        /// <summary>
        /// Limpia el modal para agregar o modificar una proyección de demanda regulada 
        /// </summary>
        protected void LimpiarModalAgregar()
        {
            mdlContAgregarLabel.InnerHtml = "Agregar";
            // Código Proyección Demanda Regulada
            TxtCodigoProy.Text = string.Empty;
            // Sector de Consumo
            ddlSectorConsumo.SelectedIndex = 0;
            // Mercado Relevante
            ddlMercadoRelevante.SelectedIndex = 0;
            // Cantidad
            TxtCantidad01.Text = string.Empty;
            TxtCantidad02.Text = string.Empty;
            TxtCantidad03.Text = string.Empty;
            TxtCantidad04.Text = string.Empty;
            TxtCantidad05.Text = string.Empty;
            TxtCantidad06.Text = string.Empty;
            TxtCantidad07.Text = string.Empty;
            TxtCantidad08.Text = string.Empty;
            TxtCantidad09.Text = string.Empty;
            TxtCantidad10.Text = string.Empty;
            TxtCantidad11.Text = string.Empty;
            TxtCantidad12.Text = string.Empty;
        }
    }
}