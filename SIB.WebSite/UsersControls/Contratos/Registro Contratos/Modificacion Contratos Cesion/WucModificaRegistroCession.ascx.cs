﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace UsersControls.Contratos.Registro_Contratos.Modificacion_Contratos_Cesion
{
    public partial class WucModificaRegistroCession : UserControl
    {
        #region EventHandler

        /// <summary>
        /// Delegado para el manejo de los modals
        /// </summary>
        public delegate void EventHandlerModal(string id, string insideId, EnumTypeModal typeModal, Modal.Size size = Modal.Size.Large);

        /// <summary>
        /// EventHandler para la selección de los los modals
        /// </summary>
        public event EventHandlerModal ModalEvent;

        /// <summary>
        /// Delegado para el manejo de los toastr
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        ///  EventHandler para la selección de los toastr 
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        /// Delegado para el manejo del log de cargue de archivos
        /// </summary>
        public delegate void EventHandlerLogCargaArchivo(StringBuilder message);

        /// <summary>
        /// EventHandler para el log de cargue de archivos
        /// </summary>
        public event EventHandlerLogCargaArchivo LogCargaArchivoEvent;

        /// <summary>
        /// Delegado para la apertura de un documento en una nueva ventana del navegador    
        /// </summary>
        public delegate void EventHandlerDocument(string pathDocument);

        /// <summary>
        /// EventHandler para la apertura de un documento en una nueva ventana del navegador    
        /// </summary>
        public event EventHandlerDocument DocumentEvent;

        /// <summary>
        /// Delegado para la selección de los botones   
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get { return ViewState["NameController"] != null && !string.IsNullOrEmpty(ViewState["NameController"].ToString()) ? ViewState["NameController"].ToString() : string.Empty; }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool TblDemanda
        {
            get { return (bool?)ViewState["TblDemanda"] ?? false; }
            set { ViewState["TblDemanda"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfIdRegistro
        {
            get { return ViewState["HdfIdRegistro"] != null && !string.IsNullOrEmpty(ViewState["HdfIdRegistro"].ToString()) ? ViewState["HdfIdRegistro"].ToString() : string.Empty; }
            set { ViewState["HdfIdRegistro"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfCodModif
        {
            get { return ViewState["HdfCodModif"] != null && !string.IsNullOrEmpty(ViewState["HdfCodModif"].ToString()) ? ViewState["HdfCodModif"].ToString() : string.Empty; }
            set { ViewState["HdfCodModif"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfPunta
        {
            get { return ViewState["HdfPunta"] != null && !string.IsNullOrEmpty(ViewState["HdfPunta"].ToString()) ? ViewState["HdfPunta"].ToString() : string.Empty; }
            set { ViewState["HdfPunta"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfNoSolicitud
        {
            get { return ViewState["HdfNoSolicitud"] != null && !string.IsNullOrEmpty(ViewState["HdfNoSolicitud"].ToString()) ? ViewState["HdfNoSolicitud"].ToString() : string.Empty; }
            set { ViewState["HdfNoSolicitud"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfCausa
        {
            get { return ViewState["HdfCausa"] != null && !string.IsNullOrEmpty(ViewState["HdfCausa"].ToString()) ? ViewState["HdfCausa"].ToString() : string.Empty; }
            set { ViewState["HdfCausa"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfMostrar
        {
            get { return ViewState["HdfMostrar"] != null && !string.IsNullOrEmpty(ViewState["HdfMostrar"].ToString()) ? ViewState["HdfMostrar"].ToString() : string.Empty; }
            set { ViewState["HdfMostrar"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfTipoCesion
        {
            get { return ViewState["HdfTipoCesion"] != null && !string.IsNullOrEmpty(ViewState["HdfTipoCesion"].ToString()) ? ViewState["HdfTipoCesion"].ToString() : string.Empty; }
            set { ViewState["HdfTipoCesion"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfOperador
        {
            get { return ViewState["HdfOperador"] != null && !string.IsNullOrEmpty(ViewState["HdfOperador"].ToString()) ? ViewState["HdfOperador"].ToString() : string.Empty; }
            set { ViewState["HdfOperador"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfContratoDef
        {
            get { return ViewState["HdfContratoDef"] != null && !string.IsNullOrEmpty(ViewState["HdfContratoDef"].ToString()) ? ViewState["HdfContratoDef"].ToString() : string.Empty; }
            set { ViewState["HdfContratoDef"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfCodigoVerUsr
        {
            get { return ViewState["HdfCodigoVerUsr"] != null && !string.IsNullOrEmpty(ViewState["HdfCodigoVerUsr"].ToString()) ? ViewState["HdfCodigoVerUsr"].ToString() : string.Empty; }
            set { ViewState["HdfCodigoVerUsr"] = value; }
        }

        /// <summary>
        /// Ruta del archivo original cargado temporalmente en el servidor 
        /// </summary>
        public string FileUploadOriginal_Temp
        {
            get { return Session["FileUploadOriginal_Temp"] != null && !string.IsNullOrEmpty(Session["FileUploadOriginal_Temp"].ToString()) ? Session["FileUploadOriginal_Temp"].ToString() : string.Empty; }
            set { Session["FileUploadOriginal_Temp"] = value; }
        }

        /// <summary>
        /// Ruta del archivo modificado cargado temporalmente en el servidor 
        /// </summary>
        public string FileUploadModificado_Temp
        {
            get { return Session["FileUploadModificado_Temp"] != null && !string.IsNullOrEmpty(Session["FileUploadModificado_Temp"].ToString()) ? Session["FileUploadModificado_Temp"].ToString() : string.Empty; }
            set { Session["FileUploadModificado_Temp"] = value; }
        }

        #endregion Propiedades

        private InfoSessionVO goInfo = null;
        private static string lsTitulo = "Modificación de Contratos por Cesión";
        private clConexion lConexion = null;
        private SqlDataReader lLector;
        private DataSet lds = new DataSet();
        private string[] lsCarperta;
        private string Carpeta;
        private string sRutaArc = ConfigurationManager.AppSettings["rutaModif"].ToString();
        private string lsCarpetaAnt = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lConexion = new clConexion(goInfo);
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles2(lConexion.gObjConexion, DdlBusVenta, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20190425 rq022-19
            LlenarControles2(lConexion.gObjConexion, DdlBusCompra, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20190425 rq022-19
            LlenarControles2(lConexion.gObjConexion, DdlBusContra, "m_operador ope", " estado = 'A' and codigo_operador !=0 and exists (select 1 from t_contrato_verificacion ver where (ope.codigo_operador= ver.operador_compra or ope.codigo_operador= ver.operador_venta) and (ver.operador_compra = " + goInfo.cod_comisionista + " or ver.operador_venta = " + goInfo.cod_comisionista + ")) and ope.codigo_operador <> " + goInfo.cod_comisionista + " order by razon_social", 0, 4); //20190425 rq022-19
            LlenarControles(lConexion.gObjConexion, DdlBusCausa, "m_causa_modificacion", "  estado ='A' and tipo_causa ='C' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlBusEstado, "m_estado_gas", "  tipo_estado ='M'  and sigla_estado not in ('R','L') order by descripcion_estado", 2, 3);
            LlenarControles(lConexion.gObjConexion, DdlBusModalidad, "m_modalidad_contractual", "  estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlBusCausa1, "m_causa_modificacion", "  estado ='A' and tipo_causa ='C' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlBusEstado1, "m_estado_gas", "  tipo_estado ='M'  and sigla_estado not in ('R','L')  order by descripcion_estado", 2, 3);
            LlenarControles(lConexion.gObjConexion, DdlIngCausa, "m_causa_modificacion", "  estado ='A' and tipo_causa ='C' order by descripcion", 0, 1);
            LlenarControles2(lConexion.gObjConexion, DdlVendedor, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20190425 rq022-19
            LlenarControles2(lConexion.gObjConexion, DdlComprador, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20190425 rq022-19
            LlenarControles(lConexion.gObjConexion, dlTipoDemanda, "m_tipo_demanda_atender", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo sec, m_demanda_sector dem", " sec.estado ='A' and sec.codigo_sector_consumo = dem.codigo_sector_consumo  and dem.estado ='A' and dem.codigo_tipo_demanda =1 and dem.ind_uso ='T' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMercadoRel, "m_mercado_relevante", " estado ='A' order by descripcion", 0, 1);
            lConexion.Cerrar();
            HdfOperador = goInfo.cod_comisionista;

            if (goInfo.cod_comisionista == "0")
            {
                SeleccionSeccionTr01(false);
                SeleccionSeccionTr02(true);
                SeleccionSeccionTr03(true);
            }
            else
            {
                SeleccionSeccionTr01(true);
                SeleccionSeccionTr02(false);
                SeleccionSeccionTr03(false);
            }

            EnumBotones[] botones = { EnumBotones.Buscar };
            ButtonsEvent?.Invoke(botones, null, NameController);
            //Se establece que el controlador ya se inicializo    
            Inicializado = true;
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles2
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles2(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnConsultar_Click(object sender, EventArgs e)
        {
            CargarDatos();
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            var lblMensaje = new StringBuilder();
            DateTime ldFecha;
            string[] lsNombreParametros = { "@P_numero_contrato", "@P_codigo_verif", "@P_contrato_definitivo", "@P_fecha_inicial", "@P_fecha_final",
                "@P_codigo_operador", "@P_codigo_contraparte", "@P_operador_compra", "@P_operador_venta","@P_tipo_mercado","@P_destino_rueda",
                "@P_codigo_modalidad","@P_codigo_causa","@P_estado_modif" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar , SqlDbType.VarChar,
                SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar};
            string[] lValorParametros = { "0", "0", "", "", "", "0", "0", "0", "0", "", "", "0", "0", "" };

            if (TxtBusFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtBusFechaIni.Text);
                }
                catch (Exception)
                {
                    lblMensaje.Append("Formato Inválido en el Campo Fecha Inicial de Negociación. <br>");
                }
            }
            if (TxtBusFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtBusFechaFin.Text);
                }
                catch (Exception)
                {
                    lblMensaje.Append("Formato Inválido en el Campo Fecha Final de Negociación. <br>");
                }
            }
            if (TxtBusFechaIni.Text.Trim().Length == 0 && TxtBusFechaFin.Text.Trim().Length > 0)
                lblMensaje.Append("Debe digitar la Fecha Inicial de Negociación antes que la final. <br>");

            if (TxtBusFechaIni.Text.Trim().Length > 0 && TxtBusFechaFin.Text.Trim().Length > 0)
                try
                {
                    if (Convert.ToDateTime(TxtFechaFin.Text) < Convert.ToDateTime(TxtFechaIni.Text))
                        lblMensaje.Append("La Fecha inicial de Negociación debe ser menor o igual que la fecha final. <br>");
                }
                catch (Exception)
                {
                }

            if (!string.IsNullOrEmpty(lblMensaje.ToString())) { return; }
            try
            {
                if (TxtBusOperacion.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtBusOperacion.Text.Trim();
                if (TxtBusId.Text.Trim().Length > 0)
                    lValorParametros[1] = TxtBusId.Text.Trim();
                lValorParametros[2] = TxtBusContratoDef.Text.Trim();
                if (TxtBusFechaIni.Text.Trim().Length > 0)
                {
                    lValorParametros[3] = TxtBusFechaIni.Text.Trim();
                    if (TxtBusFechaFin.Text.Trim().Length > 0)
                        lValorParametros[4] = TxtBusFechaFin.Text.Trim();
                    else
                        lValorParametros[4] = TxtBusFechaIni.Text.Trim();
                }
                lValorParametros[5] = goInfo.cod_comisionista;
                lValorParametros[6] = DdlBusContra.SelectedValue;
                lValorParametros[7] = DdlBusCompra.SelectedValue;
                lValorParametros[8] = DdlBusVenta.SelectedValue;
                lValorParametros[9] = DdlBusMercado.SelectedValue;
                lValorParametros[10] = DdlBusProducto.SelectedValue;
                lValorParametros[11] = DdlBusModalidad.SelectedValue;
                if (goInfo.cod_comisionista == "0")
                {
                    lValorParametros[12] = DdlBusCausa1.SelectedValue;
                    lValorParametros[13] = DdlBusEstado1.SelectedValue;
                }
                else
                {
                    lValorParametros[12] = DdlBusCausa.SelectedValue;
                    lValorParametros[13] = DdlBusEstado.SelectedValue;
                }
                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContSolModCes", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                dtgConsultaExcel.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContSolModCes", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsultaExcel.DataBind();
                lConexion.Cerrar();
                if (dtgConsulta.Items.Count > 0)
                {
                    EnumBotones[] botones = { EnumBotones.Excel, EnumBotones.Buscar };
                    ButtonsEvent?.Invoke(botones, null, NameController);
                }
                else
                {
                    EnumBotones[] botones = { EnumBotones.Buscar };
                    ButtonsEvent?.Invoke(botones, null, NameController);
                    ToastrEvent?.Invoke("No se encontraron Registros", EnumTypeToastr.Warning);
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("No se Pudo consultar la información.!", EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
        {
            var lblMensaje = new StringBuilder();
            HdfNoSolicitud = "0";
            //divSolicitud.Visible = false;

            if (e.CommandName.Equals("Modificar"))
            {
                HdfIdRegistro = e.Item.Cells[4].Text;
                HdfTipoCesion = e.Item.Cells[33].Text;
                HdfContratoDef = e.Item.Cells[6].Text; //20200430 ajuste nombre archivo //20200727 ajsute front-end
                if (goInfo.cod_comisionista == "0")
                {
                    if (e.Item.Cells[32].Text != "P")
                        lblMensaje.Append("No está pendiente de aprobación de la BMC");
                    if (e.Item.Cells[7].Text != "Original")
                        lblMensaje.Append("la BMC sólo puede aprobar el contrato original");
                    //20190607 rq036-19
                    if (goInfo.codigo_grupo_usuario != 1)
                        lblMensaje.Append("Solo el administrador puede aprobar la cesión");
                }
                else
                {
                    if (e.Item.Cells[7].Text == "Original")
                    {
                        if (e.Item.Cells[32].Text != "I" && e.Item.Cells[32].Text != "E" && e.Item.Cells[32].Text != "S" && e.Item.Cells[32].Text != "M" && e.Item.Cells[32].Text != "C")
                            lblMensaje.Append("No se puede modificar porque ya se aprobó o se rechazó");
                        if (e.Item.Cells[2].Text != "0" && e.Item.Cells[25].Text != e.Item.Cells[29].Text)
                            lblMensaje.Append("No puede aprobar el contrato original");
                        if (e.Item.Cells[37].Text == "S")
                            lblMensaje.Append("Hay una solicitud de modificación en curso para el contrato<br>");
                    }
                    else
                    {
                        if (e.Item.Cells[25].Text != e.Item.Cells[29].Text && e.Item.Cells[32].Text != "A" && e.Item.Cells[32].Text != "S")
                            lblMensaje.Append("No se puede aprobar el registro porque no ha sido autorizado totalmente o ya está aprobado");
                        if (e.Item.Cells[25].Text == e.Item.Cells[29].Text && (e.Item.Cells[32].Text == "B" || e.Item.Cells[32].Text == "O"))
                            lblMensaje.Append("No puede modificar porque ya se aprobó por la contraparte");
                    }
                }
                if (e.Item.Cells[34].Text.Length == 1 && e.Item.Cells[34].Text != "C")
                    lblMensaje.Append("La operación tiene en proceso una modificación que no es por cesión ");
                if (manejo_bloqueo("V", HdfIdRegistro))
                    lblMensaje.Append("Se está modificando la operación");
                if (string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    manejo_bloqueo("A", HdfIdRegistro);
                    DdlDestino.SelectedValue = e.Item.Cells[28].Text;
                    DdlMercado.SelectedValue = e.Item.Cells[27].Text;
                    HdfPunta = e.Item.Cells[29].Text;
                    HdfNoSolicitud = e.Item.Cells[2].Text;
                    HdfCausa = e.Item.Cells[30].Text;
                    HdfCodModif = e.Item.Cells[31].Text;

                    if (e.Item.Cells[35].Text == "N")
                    {
                        //dtgUsuarios.Columns[8].Visible = false;
                        //dtgUsuarios.Columns[9].Visible = false;
                        btnCrearUsu.Visible = false;
                        // Se oculta la sección de demanda
                        tblDemanda.Visible = false;
                        TblDemanda = false;
                    }
                    else
                    {
                        //dtgUsuarios.Columns[8].Visible = true;
                        //dtgUsuarios.Columns[9].Visible = true;
                        if (e.Item.Cells[27].Text != "O")
                        {
                            btnCrearUsu.Visible = true;
                            // Se muestra la sección de demanda
                            tblDemanda.Visible = true;
                            TblDemanda = true;
                            trae_usr_fin(HdfCodModif, HdfIdRegistro);
                        }
                        else
                        {
                            btnCrearUsu.Visible = false;
                            // Se oculta la sección de demanda
                            tblDemanda.Visible = false;
                            TblDemanda = false;
                        }
                    }

                    if (e.Item.Cells[26].Text == "C")
                    {
                        color_label("N"); // 20190425 rq022-19 ajuste modificacion
                        ModalEvent?.Invoke(mdlCausaMod.ID, mdlCausaModInside.ID, EnumTypeModal.Abrir);
                        //20190425 rq022-19 
                        if (TblDemanda)
                        {
                            HdfMostrar = "S";
                            // Se oculta la sección de demanda
                            tblDemanda.Visible = false;
                            TblDemanda = false;
                        }
                        else
                            HdfMostrar = "N";
                    }
                    if (e.Item.Cells[26].Text == "M")
                    {
                        color_label("N"); // 20190425 rq022-19 ajuste modificacion
                        modificar();
                    }
                    if (e.Item.Cells[26].Text == "A")
                    {
                        color_label("R"); // 20190425 rq022-19 ajuste modificacion
                        aprobar();
                    }
                    if (e.Item.Cells[26].Text == "B")
                    {
                        color_label("R"); // 20190425 rq022-19 ajuste modificacion
                        consultar("A");
                    }
                    //if (HdfTipoCesion == "T" || e.Item.Cells[35].Text=="N")
                }
                else
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                }
            }
            if (e.CommandName.Equals("Consultar"))
            {
                HdfIdRegistro = e.Item.Cells[4].Text;
                HdfCodModif = e.Item.Cells[31].Text;
                color_label("R"); // 20190425 rq022-19 ajuste modificacion
                consultar("C");
            }
            if (e.CommandName.Equals("Original"))
            {
                try
                {
                    if (e.Item.Cells[23].Text.Trim() != "&nbsp;" && e.Item.Cells[23].Text.Trim() != "")
                    {
                        lsCarperta = sRutaArc.Split('\\');
                        lsCarpetaAnt = lsCarperta[lsCarperta.Length - 2];
                        var lsRuta = "../../" + lsCarpetaAnt + "/" + e.Item.Cells[23].Text.Replace(@"\", "/");
                        DocumentEvent?.Invoke(lsRuta);
                    }
                    else
                        ToastrEvent?.Invoke("No hay archivo para visualizar.!", EnumTypeToastr.Info);
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Problemas al mostrar el archivo. ", EnumTypeToastr.Error);
                }
            }
            if (e.CommandName.Equals("Modificado"))
            {
                try
                {
                    if (e.Item.Cells[24].Text.Trim() != "&nbsp;" && e.Item.Cells[24].Text.Trim() != "")
                    {
                        lsCarperta = sRutaArc.Split('\\');
                        lsCarpetaAnt = lsCarperta[lsCarperta.Length - 2];
                        var lsRuta = "../../" + lsCarpetaAnt + "/" + e.Item.Cells[24].Text.Replace(@"\", "/");
                        DocumentEvent?.Invoke(lsRuta);
                    }
                    else
                        ToastrEvent?.Invoke("No hay archivo para visualizar.!", EnumTypeToastr.Info);
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Problemas al mostrar el archivo. ", EnumTypeToastr.Error);
                }
            }
        }

        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void crear()
        {
            btnModificar.Visible = true;
            btnGrabarDef.Visible = true;
            btnAprobar.Visible = false;
            btnAprobarBmc.Visible = false;
            btnRechazo.Visible = false;
            btnRechazoBMC.Visible = false;
            trObs.Visible = true;
            mostrar_datos("C");
        }/// Modificacion:
         /// </summary>
         /// <param name="source"></param>
         /// <param name="e"></param>


        protected void mostrar_datos(string lsAccion)
        {

            //DdlComprador.SelectedValue = "0";
            //DdlVendedor.SelectedValue = "0";
            //TxtFechaNeg.Text = "";
            //TxtHoraNeg.Text = "";
            //TxtFechaSus.Text = "";
            //TxtContDef.Text = "";
            //DdlVariable.SelectedValue = "N";
            //TxtFechaIni.Text = "";
            //TxtFechaFin.Text = "";
            //TxtCantidad.Text = "";
            //TxtPrecio.Text = "";
            //TxtCapacOtm.Text = "";

            //DdlComprador.Enabled = true;
            //DdlVendedor.Enabled = true;
            //TxtFechaNeg.Enabled = false;
            //TxtHoraNeg.Enabled = false;
            //TxtFechaSus.Enabled = false;
            //TxtContDef.Enabled = false;
            //DdlVariable.Enabled = false;
            //TxtFechaIni.Enabled = true;
            //TxtFechaFin.Enabled = true;
            //TxtCantidad.Enabled = true;
            //TxtPrecio.Enabled = false;
            //TxtCapacOtm.Enabled = true;

            try
            {
                //Se abre el modal de modificación  
                ModalEvent?.Invoke(mdlModificacion.ID, mdlModificacionInside.ID, EnumTypeModal.Abrir);
                EnumBotones[] botones = { EnumBotones.Buscar };
                ButtonsEvent?.Invoke(botones, null, NameController);

                //if (HdfTipoCesion == "P" && HdfPunta == "C" && DdlMercado.SelectedValue != "O")
                //{
                //    TblDemanda.Visible = true;
                //    btnCrearUsu.Visible = true;
                //}
                //else
                //{
                //    TblDemanda.Visible = false;
                //    btnCrearUsu.Visible = false;
                //}

                if (DdlDestino.SelectedValue == "A")
                    TxtCapacOtm.Visible = true;

                if (DdlDestino.SelectedValue == "G")
                    LblCantidad.Text = "Cantidad de energía contratada (MBTUD)";
                else
                {
                    if (DdlDestino.SelectedValue == "T")
                        LblCantidad.Text = "Capacidad de transporte contratada (KPCD)";
                    else
                        LblCantidad.Text = "Cantidad y Capacidad contratada";
                }

                //habilita los campos permitidos
                string[] lsNombreParametros = { "@P_codigo_campo", "@P_codigo_causa" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { "0", HdfCausa };

                try
                {
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetCampoMod", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        DdlComprador.Enabled = true;
                        DdlVendedor.Enabled = true;
                        if (HdfTipoCesion == "P")
                        {
                            TxtFechaIni.Enabled = true;
                            TxtFechaFin.Enabled = true;
                            TxtCantidad.Enabled = true;
                            TxtCapacOtm.Enabled = true;
                        }
                        else
                        {
                            TxtFechaIni.Enabled = false;
                            TxtFechaFin.Enabled = false;
                            TxtCantidad.Enabled = false;
                            TxtCapacOtm.Enabled = false;
                        }


                        if (lLector["operador_compra"].ToString() == "N")
                            DdlComprador.Enabled = false;
                        if (lLector["operador_venta"].ToString() == "N")
                            DdlVendedor.Enabled = false;
                        if (lLector["fecha_inicial"].ToString() == "N")
                            TxtFechaIni.Enabled = false;
                        if (lLector["fecha_final"].ToString() == "N")
                            TxtFechaFin.Enabled = false;
                        if (lLector["cantidad"].ToString() == "N")
                        {
                            TxtCantidad.Enabled = false;
                            TxtCapacOtm.Enabled = false;
                        }
                    }
                    else
                    {
                        DdlComprador.Enabled = false;
                        DdlVendedor.Enabled = false;
                        TxtFechaIni.Enabled = false;
                        TxtFechaFin.Enabled = false;
                        TxtCantidad.Enabled = false;
                        TxtCapacOtm.Enabled = false;

                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Problemas al traer la información.! " + ex.Message, EnumTypeToastr.Error);
                }

                //trae el detalle de contrados
                CargarDatosOper();
                revisa_usr_fin();
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Problemas en la Recuperación de la Información. " + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void consulta_datos()
        {

            DdlComprador.SelectedValue = "0";
            DdlVendedor.SelectedValue = "0";
            TxtFechaNeg.Text = "";
            TxtHoraNeg.Text = "";
            TxtFechaSus.Text = "";
            TxtContDef.Text = "";
            DdlVariable.SelectedValue = "N";
            TxtFechaIni.Text = "";
            TxtFechaFin.Text = "";
            TxtCantidad.Text = "";
            TxtPrecio.Text = "";
            TxtCapacOtm.Text = "";

            try
            {
                //Se abre el modal de modificación  
                ModalEvent?.Invoke(mdlModificacion.ID, mdlModificacionInside.ID, EnumTypeModal.Abrir);
                EnumBotones[] botones = { EnumBotones.Ninguno };
                ButtonsEvent?.Invoke(botones, null, NameController);

                //if (HdfPunta == "C" && DdlMercado.SelectedValue != "O")
                //{
                //    TblDemanda.Visible = true;
                //    btnCrearUsu.Visible = true;
                //}
                //else
                //{
                //    TblDemanda.Visible = false;
                //    btnCrearUsu.Visible = false;
                //}
                if (DdlDestino.SelectedValue == "A")
                    TxtCapacOtm.Visible = true;
                else
                    TxtCapacOtm.Visible = false;
                if (DdlDestino.SelectedValue == "G")
                    LblCantidad.Text = "Cantidad de energía contratada (MBTUD)";
                else
                {
                    if (DdlDestino.SelectedValue == "T")
                        LblCantidad.Text = "Capacidad de transporte contratada (KPCD)";
                    else
                        LblCantidad.Text = "Cantidad y Capacidad contratada";
                }

                DdlComprador.Enabled = false;
                DdlVendedor.Enabled = false;
                TxtFechaNeg.Enabled = false;
                TxtHoraNeg.Enabled = false;
                TxtFechaSus.Enabled = false;
                TxtContDef.Enabled = false;
                DdlVariable.Enabled = false;
                TxtFechaIni.Enabled = false;
                TxtFechaFin.Enabled = false;
                TxtCantidad.Enabled = false;
                TxtCapacOtm.Enabled = false;
                TxtPrecio.Enabled = false;

                CargarDatosOper();
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Problemas en la Recuperación de la Información. " + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void modificar()
        {
            btnModificar.Visible = true;
            btnGrabarDef.Visible = true;
            btnAprobar.Visible = false;
            btnAprobarBmc.Visible = false;
            btnRechazo.Visible = false;
            btnRechazoBMC.Visible = false;
            trObs.Visible = false;
            crear();
            if (TblDemanda)
                CargarDatosUsu();
        }

        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void aprobar()
        {
            //Se abre el modal de modificación  
            ModalEvent?.Invoke(mdlModificacion.ID, mdlModificacionInside.ID, EnumTypeModal.Abrir);

            btnModificar.Visible = false;
            btnAprobar.Visible = true;
            btnGrabarDef.Visible = false;
            btnAprobarBmc.Visible = false;
            btnRechazo.Visible = true;
            btnRechazoBMC.Visible = false;
            //btnCrearUsu.Visible = false;

            DdlComprador.Enabled = false;
            DdlVendedor.Enabled = false;
            TxtFechaIni.Enabled = false;
            TxtFechaFin.Enabled = false;
            TxtCantidad.Enabled = false;
            TxtCapacOtm.Enabled = false;
            trObs.Visible = true;
            //if (HdfTipoCesion == "P" && HdfPunta == "C" && DdlMercado.SelectedValue != "O")
            //{
            //    TblDemanda.Visible = true;
            //    btnCrearUsu.Visible = true;
            //}
            //else
            //{
            //    TblDemanda.Visible = false;
            //    btnCrearUsu.Visible = false;
            //}

            CargarDatosOper();
            if (TblDemanda)
                CargarDatosUsu();
        }

        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void consultar(string lsAccion)
        {
            consulta_datos();
            if (lsAccion == "A")
            {
                btnModificar.Visible = false;
                btnAprobar.Visible = false;
                btnAprobarBmc.Visible = true;
                btnRechazo.Visible = false;
                btnRechazoBMC.Visible = true;
                btnGrabarDef.Visible = false;
                btnCrearUsu.Visible = false;
                trObs.Visible = true;
            }
            else
            {
                btnModificar.Visible = false;
                btnAprobar.Visible = false;
                btnAprobarBmc.Visible = false;
                btnGrabarDef.Visible = false;
                btnCrearUsu.Visible = false;
                btnRechazo.Visible = false;
                btnRechazoBMC.Visible = false;
                trObs.Visible = false;
                // Se oculta la sección de demanda
                tblDemanda.Visible = false;
                btnCrearUsu.Visible = false;
            }
            if (TblDemanda)
                CargarDatosUsu();
        }

        /// <summary>
        /// Guarda temporalmente el archivo original o modificado en el servidor
        /// </summary>
        private void FileTemp_Save(AjaxControlToolkit.AsyncFileUploadEventArgs e, AjaxControlToolkit.AsyncFileUpload asyncFileUpload, string control)
        {
            // Se crea la ruta de los archivos temporales
            Directory.CreateDirectory(sRutaArc + "temp\\");
            string fileUploadPath;
            if (control.Equals("original"))
            {
                fileUploadPath = Path.Combine(sRutaArc, "temp\\", FuArchivoOrg.FileName);
                FuArchivoOrg.SaveAs(fileUploadPath);
                FileUploadOriginal_Temp = fileUploadPath;
            }
            else
            {
                fileUploadPath = Path.Combine(sRutaArc, "temp\\", FuArchivoMod.FileName);
                FuArchivoMod.SaveAs(fileUploadPath);
                FileUploadModificado_Temp = fileUploadPath;
            }
        }

        /// <summary>
        /// Evento que guarda temporalmente el archivo original o modificado en el servidor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FileUpload_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                // Se valida si se cargo el archivo original  
                if (!string.IsNullOrEmpty(FuArchivoOrg.FileName))
                    // Se carga el archivo original
                    FileTemp_Save(e, FuArchivoOrg, "original");
                // Se valida si se cargo el archivo modificado   
                if (!string.IsNullOrEmpty(FuArchivoMod.FileName))
                    // Se carga el archivo modificado
                    FileTemp_Save(e, FuArchivoMod, "modificado");
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnAceptar_Click(object sender, EventArgs e)
        {
            var lblMensajeMod = new StringBuilder();
            List<string> liExtension = new List<string>();
            liExtension.Add(".PDF");
            liExtension.Add(".PDFA");
            liExtension.Add(".GIF");
            liExtension.Add(".JPG");
            liExtension.Add(".PNG");
            liExtension.Add(".TIFF");
            string lsExtValidas = ".pdf, .pdfa, .gif, .jpg, .png, .tiff";
            string[] fileOriginal;
            string fileNameOriginal = string.Empty;
            string[] fileModificado;
            string fileNameModificado = string.Empty;

            string lsExtension;
            if (DdlIngCausa.SelectedValue == "0")
                lblMensajeMod.Append("Debe seleccionar la causa de modificación<br>");
            if (string.IsNullOrEmpty(FileUploadOriginal_Temp))
            {
                lblMensajeMod.Append("Debe seleccionar el archivo del contrato original<br>");
            }
            else
            {
                fileOriginal = FileUploadOriginal_Temp.Split('\\');
                fileNameOriginal = fileOriginal[fileOriginal.Length - 1];
                lsExtension = Path.GetExtension(fileNameOriginal).ToUpper();
                if (!liExtension.Contains(lsExtension))
                    lblMensajeMod.Append("La extensión del archivo original no es válida: " + lsExtValidas + "<br>");
                //20200430 ajuste nombre archivos
                //if (File.Exists(FileUploadOriginal_Temp.Replace("temp\\", "")))
                //    lblMensajeMod.Append("El archivo original ya está cargado en el sistema<br>");
            }
            if (string.IsNullOrEmpty(FileUploadModificado_Temp))
            {
                lblMensajeMod.Append("Debe seleccionar el archivo del contrato modificado<br>");
            }
            else
            {
                fileModificado = FileUploadModificado_Temp.Split('\\');
                fileNameModificado = fileModificado[fileModificado.Length - 1];
                lsExtension = Path.GetExtension(fileNameModificado).ToUpper();
                if (!liExtension.Contains(lsExtension))
                    lblMensajeMod.Append("La extensión del archivo modificado no es válida: " + lsExtValidas + "<br>");
                //20200430 ajuste nombre archivos
                //if (File.Exists(FileUploadModificado_Temp.Replace("temp\\", "")))
                //    lblMensajeMod.Append("El archivo modificado ya está cargado en el sistema<br>");
            }
            if (FileUploadOriginal_Temp == FileUploadModificado_Temp && FileUploadOriginal_Temp != "")
                lblMensajeMod.Append("Los nombres de los archivos de los contratos original y modificado deben ser diferentes<br>");

            //20200430 ajuste nombre archivos
            string lsNumero = "";
            string lsArchivoOrg = "";
            string lsArchivoMod = "";
            string lsArchivoOrgExt = "";
            string lsArchivoModExt = "";
            int liCOnta = 0;
            string lsContrato = HdfContratoDef.Replace('/', '_').Replace('\\', '_'); //20210216
            while (lsArchivoOrg == "" && liCOnta < 10)
            {
                lsNumero = DateTime.Now.Millisecond.ToString();
                lsArchivoOrg = DateTime.Now.ToString("yyyy_MM_dd") + "_" + HdfOperador + "_" + lsContrato + "_Org_" + lsNumero + Path.GetExtension(fileNameOriginal).ToUpper(); //20210216
                lsArchivoMod = DateTime.Now.ToString("yyyy_MM_dd") + "_" + HdfOperador + "_" + lsContrato + "_Mod_" + lsNumero + Path.GetExtension(fileNameModificado).ToUpper(); //20210216
                if (File.Exists(sRutaArc + lsArchivoOrg) || File.Exists(sRutaArc + lsArchivoMod))
                {
                    lsArchivoOrg = "";
                    lsArchivoMod = "";
                }
                liCOnta++;
            }
            if (lsArchivoMod == "")
                lblMensajeMod.Append("No se pueden cargar los archivos. Intente de nuevo<br>");
            else
            {
                lsArchivoOrgExt = sRutaArc + lsArchivoOrg;
                lsArchivoModExt = sRutaArc + lsArchivoMod;
            }
            //20200430 ajuste nombre archivos

            if (!string.IsNullOrEmpty(lblMensajeMod.ToString()))
            {
                ToastrEvent?.Invoke(lblMensajeMod.ToString(), EnumTypeToastr.Error);
                manejo_bloqueo("E", HdfIdRegistro);
                return;
            }
            try
            {
                HdfCausa = "0";
                HdfTipoCesion = ddlTipo.SelectedValue;
                string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_codigo_operador", "@P_codigo_verif", "@P_contrato_definitivo", "@P_codigo_causa", "@P_contrato_original", "@P_contrato_nuevo", "@P_tipo_cesion", "@P_estado" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
                string[] lValorParametros = { "0", HdfOperador, HdfIdRegistro, HdfContratoDef, DdlIngCausa.SelectedValue, lsArchivoOrg, lsArchivoMod, ddlTipo.SelectedValue, "C" };//20200430 ajuste nombre archivos

                lConexion.Abrir();
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetSolModCont", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensajeMod.Append("Se presentó un Problema en la Creación de la solicitud de modificación.! " + goInfo.mensaje_error.ToString());
                    lConexion.Cerrar();
                }
                else
                {
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                        {
                            if (lLector["Error"].ToString() != "NO")
                            {
                                lblMensajeMod.Append(lLector["Error"]);
                            }
                            else
                            {
                                HdfNoSolicitud = lLector["codigo_sol"].ToString();
                                try
                                {
                                    var fi1 = new FileInfo(FileUploadOriginal_Temp);
                                    //File.Copy(FileUploadOriginal_Temp, FileUploadOriginal_Temp.Replace("temp\\", ""), true);//2020727 ajsute front-end
                                    File.Copy(FileUploadOriginal_Temp, lsArchivoOrgExt, true); //2020727 ajsute front-end
                                    fi1.Delete();
                                    var fi2 = new FileInfo(FileUploadModificado_Temp);
                                    //File.Copy(FileUploadModificado_Temp, FileUploadModificado_Temp.Replace("temp\\", ""), true);//2020727 ajsute front-end
                                    File.Copy(FileUploadModificado_Temp, lsArchivoModExt, true); //2020727 ajsute front-end
                                    fi2.Delete();
                                }
                                catch (Exception ex)
                                {
                                    ToastrEvent?.Invoke("Error al cargar los archivos." + ex.Message, EnumTypeToastr.Error);
                                }
                                HdfCausa = DdlIngCausa.SelectedValue;
                                //Se cierra el modal de Causa Modifica  
                                ModalEvent?.Invoke(mdlCausaMod.ID, mdlCausaModInside.ID, EnumTypeModal.Cerrar);
                                crear();
                            }
                        }
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();

                    if (!string.IsNullOrEmpty(lblMensajeMod.ToString()))
                    {
                        ToastrEvent?.Invoke(lblMensajeMod.ToString(), EnumTypeToastr.Error);
                        return;
                    }
                    manejo_bloqueo("E", HdfIdRegistro);

                }
                lConexion.Cerrar();
                HdfCausa = DdlIngCausa.SelectedValue;
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
            finally
            {
                FileUploadOriginal_Temp = string.Empty;
                FileUploadModificado_Temp = string.Empty;
            }
            //20190425 rq022-19 
            if (HdfMostrar == "S")
            {
                // Se muestra la sección de demanda
                tblDemanda.Visible = true;
                TblDemanda = true;
            }
        }

        /// <summary>
        /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCancelar_Click(object sender, EventArgs e)
        {
            manejo_bloqueo("E", HdfIdRegistro);
            ModalEvent?.Invoke(mdlCausaMod.ID, mdlCausaModInside.ID, EnumTypeModal.Cerrar);

        }

        /// <summary>
        /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnModificar_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_solicitud","@P_codigo_verif","@P_codigo_modif","@P_codigo_comprador","@P_codigo_vendedor","@P_punta_modifica",
                "@P_fecha_inicial","@P_fecha_final", "@P_cantidad","@P_capacidad_otm"
                                      };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int
                                        };
            string[] lValorParametros = { "0", "0","0", "0","0", "","","", "0","0"
                                    };
            var lblMensaje = new StringBuilder();
            DateTime ldFecha;
            DateTime ldFechaI = DateTime.Now;
            int liValor = 0;

            try
            {
                if (DdlComprador.SelectedValue == "0" && DdlMercado.SelectedValue != "O")
                    lblMensaje.Append("Debe seleccionar el Comprador.<br>");
                if (DdlVendedor.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el vendedor.<br>");
                try
                {
                    ldFechaI = Convert.ToDateTime(TxtFechaIni.Text.Trim());
                }
                catch (Exception)
                {
                    lblMensaje.Append("Fecha de entrega inicial inválida.<br>");
                }
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaFin.Text.Trim());
                    if (ldFechaI > ldFecha)
                        lblMensaje.Append("la fecha de entrega inicial debe ser menor o igual que la final.<br>");
                }
                catch (Exception)
                {
                    lblMensaje.Append("Fecha de entrega final inválida.<br>");
                }
                if (TxtCantidad.Text.Trim().Length <= 0)
                    lblMensaje.Append(" Debe Ingresar la cantidad. <br>");
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidad.Text);
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append("Valor Inválido en la cantidad/capacidad. <br>");
                    }
                }
                if (TxtCapacOtm.Text.Trim().Length <= 0)
                    lblMensaje.Append(" Debe Ingresar la capacidad de transporte. <br>");
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCapacOtm.Text);
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append(" Valor Inválido en la capacidad de transporte. <br>");
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }
                lValorParametros[0] = HdfNoSolicitud;
                lValorParametros[1] = HdfIdRegistro;
                lValorParametros[2] = HdfCodModif; ;
                lValorParametros[3] = DdlComprador.SelectedValue;
                lValorParametros[4] = DdlVendedor.SelectedValue;
                lValorParametros[5] = HdfPunta;
                lValorParametros[6] = TxtFechaIni.Text.Trim();
                lValorParametros[7] = TxtFechaFin.Text.Trim();
                lValorParametros[8] = TxtCantidad.Text.Trim();
                lValorParametros[9] = TxtCapacOtm.Text.Trim();

                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModContCesion", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Append(lLector["error"] + "<br>");
                }
                lConexion.Cerrar();

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }
                CargarDatosOper();
                manejo_bloqueo("E", HdfIdRegistro);
                //Se cierra el modal de Modificacion  
                ModalEvent?.Invoke(mdlModificacion.ID, mdlModificacionInside.ID, EnumTypeModal.Cerrar);
                ToastrEvent?.Invoke("Operación modificada correctamente", EnumTypeToastr.Success);
                CargarDatos();
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAprobar_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_solicitud","@P_codigo_modif","@P_codigo_operador","@P_estado" ,"@P_observaciones"
                                      };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar
                                        };
            string[] lValorParametros = { "0", "0", "0", "A",""
                                    };
            var lblMensaje = new StringBuilder();
            DateTime ldFechaI = DateTime.Now;

            if (TxtObs.Text == "")
            {
                ToastrEvent?.Invoke("Debe ingresar las observaciones para hacer la aprobación", EnumTypeToastr.Error);
                return;
            }
            try
            {
                lValorParametros[0] = HdfNoSolicitud;
                lValorParametros[1] = HdfCodModif;
                lValorParametros[2] = goInfo.cod_comisionista;
                lValorParametros[4] = TxtObs.Text;

                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModCesionApr", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Append(lLector["error"].ToString() + "<br>");
                }
                lConexion.Cerrar();

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }
                CargarDatosOper();
                ToastrEvent?.Invoke("Modificación aprobada correctamente", EnumTypeToastr.Success);
                btnRegresar_Click(null, null);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRechazar_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_solicitud","@P_codigo_modif","@P_estado" ,"@P_observaciones"
                                      };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar
                                        };
            string[] lValorParametros = { "0", "0", "R",""
                                    };
            var lblMensaje = new StringBuilder();
            DateTime ldFechaI = DateTime.Now;

            if (TxtObs.Text == "")
            {
                ToastrEvent?.Invoke("Debe ingresar las observaciones para hacer el rechazo", EnumTypeToastr.Error);
                return;
            }

            try
            {
                lValorParametros[0] = HdfNoSolicitud;
                lValorParametros[1] = HdfCodModif;
                lValorParametros[3] = TxtObs.Text;

                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModCesionApr", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Append(lLector["error"] + "<br>");
                }
                lConexion.Cerrar();

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }
                CargarDatosOper();
                ToastrEvent?.Invoke("Modificación rechazada correctamente", EnumTypeToastr.Success);
                btnRegresar_Click(null, null);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAprobarBmc_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_estado","@P_observaciones"
                                      };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar
                                        };
            string[] lValorParametros = { "0","A",""
                                    };
            var lblMensaje = new StringBuilder();
            DateTime ldFechaI = DateTime.Now;

            if (TxtObs.Text == "")
            {
                ToastrEvent?.Invoke("Debe ingresar las observaciones para hacer la aprobación", EnumTypeToastr.Error);
                return;
            }
            try
            {
                lValorParametros[0] = HdfNoSolicitud;
                lValorParametros[2] = TxtObs.Text;
                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModCesionAprBmc", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Append(lLector["error"] + "<br>");
                }
                lConexion.Cerrar();

                if (string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    manejo_bloqueo("E", HdfIdRegistro);
                    CargarDatosOper();
                    ToastrEvent?.Invoke("Modificación aprobada correctamente", EnumTypeToastr.Success);
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Error al aprobar la solicitud. " + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRechazarBmc_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_estado","@P_observaciones"
                                      };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar
                                        };
            string[] lValorParametros = { "0", "R", ""
                                    };
            var lblMensaje = new StringBuilder();
            DateTime ldFechaI = DateTime.Now;

            if (TxtObs.Text == "")
                lblMensaje.Append("Debe ingresar las observaciones para hacer el rechazo");
            try
            {
                lValorParametros[0] = HdfNoSolicitud;
                lValorParametros[2] = TxtObs.Text;
                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModCesionAprBmc", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Append(lLector["error"] + "<br>");
                }
                lConexion.Cerrar();

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }
                manejo_bloqueo("E", HdfIdRegistro);
                CargarDatosOper();
                ToastrEvent?.Invoke("Modificación rechazada correctamente", EnumTypeToastr.Success);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Error al rechazar la solicitud. " + ex.Message, EnumTypeToastr.Warning);
            }
        }

        /// <summary>
        /// Nombre: btnRegresar_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            manejo_bloqueo("E", HdfIdRegistro);
            // Se oculta la sección de demanda
            tblDemanda.Visible = false;
            //Se cierra el modal de modificación  
            ModalEvent?.Invoke(mdlModificacion.ID, mdlModificacionInside.ID, EnumTypeModal.Cerrar);
            TblDemanda = false;
            CargarDatos();
        }

        /// <summary>
        /// Nombre: CargarDatosOper
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatosOper()
        {
            lConexion.Abrir();
            string[] lsNombreParametros = { "@P_codigo_verif", "@P_codigo_modif" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { HdfIdRegistro, HdfCodModif };
            try
            {
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetDatosContModCesion", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    ToastrEvent?.Invoke("Se presentó un Problema en la selección de la información.! " + goInfo.mensaje_error, EnumTypeToastr.Error);
                    lConexion.Cerrar();
                }
                else
                {
                    if (lLector.HasRows)
                    {
                        lLector.Read();

                        DdlDestino.SelectedValue = lLector["destino_rueda"].ToString();
                        DdlMercado.SelectedValue = lLector["tipo_mercado"].ToString();
                        DdlVendedor.SelectedValue = lLector["operador_venta"].ToString();
                        DdlComprador.SelectedValue = lLector["operador_compra"].ToString();
                        TxtFechaNeg.Text = lLector["fecha_neg"].ToString();
                        TxtHoraNeg.Text = lLector["hora_neg"].ToString();
                        TxtFechaSus.Text = lLector["fecha_suscripcion"].ToString();
                        TxtContDef.Text = lLector["contrato_definitivo"].ToString();
                        DdlVariable.SelectedValue = lLector["ind_contrato_var"].ToString();
                        TxtFechaIni.Text = lLector["fecha_ini"].ToString();
                        TxtFechaFin.Text = lLector["fecha_fin"].ToString();
                        TxtCantidad.Text = lLector["cantidad"].ToString();
                        TxtCapacOtm.Text = lLector["capacidad_otm"].ToString();
                        TxtPrecio.Text = lLector["precio"].ToString();
                    }
                }
                if (HdfPunta == "C" && DdlMercado.SelectedValue != "M")
                    CargarDatosUsu();
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Error al recuperar información." + ex.Message.ToString(), EnumTypeToastr.Error);
                lConexion.Cerrar();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCrearUsu_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_cont_usr", "@P_codigo_modif","@P_codigo_verif", "@P_no_identificacion_usr", "@P_codigo_tipo_doc",
                                        "@P_nombre_usuario", "@P_codigo_sector_consumo", "@P_codigo_punto_salida","@P_cantidad_contratada","@P_tipo_demanda",
                                        "@P_codigo_mercado_relevante","@P_equivalente_kpcd", "@P_accion","@P_destino_rueda", "@P_codigo_solicitud"
                                      };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,
                                         SqlDbType.VarChar, SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,
                                         SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int
                                      };
            string[] lValorParametros = { "0", "0", "0", "", "", "", "0", " 0", "0", "0", "0","0","1","","0"
                                    };

            var lblMensaje = new StringBuilder();
            string[] lsUsuario;
            string lsNombre = "";
            string lsTipoDoc = "";
            int liValor = 0;
            try
            {
                lConexion.Abrir();
                lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
                if (dlTipoDemanda.SelectedValue == "0")
                    lblMensaje.Append("Debe Seleccionar el Tipo de Demanda a Atender. <br> ");
                else
                {
                    if (dlTipoDemanda.SelectedValue == "2")
                    {
                        if (TxtUsuarioFinal.Text.Trim().Length <= 0)
                            lblMensaje.Append("Debe Ingresar el Usuario Final <br>");
                        else
                        {
                            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                            if (!lLector.HasRows)
                                lblMensaje.Append("El Usuario Ingresado NO existe en la Base de Datos.! <br>");
                            else
                            {
                                lLector.Read();
                                lsNombre = lLector["nombre"].ToString();
                                lsTipoDoc = lLector["codigo_tipo_doc"].ToString();
                            }
                            lLector.Close();
                            lLector.Dispose();
                        }
                    }
                    else
                    {
                        if (ddlMercadoRel.SelectedValue == "0")
                            lblMensaje.Append("Debe Seleccionar el mercado relevante. <br> ");
                    }
                }
                if (ddlSector.SelectedValue == "0")
                    lblMensaje.Append("Debe Seleccionar " + lblSector.Text + ". <br> ");
                if (ddlPuntoSalida.SelectedValue == "0")
                    lblMensaje.Append("Debe Seleccionar  Punto de Salida en SNT. <br> ");
                if (TxtCantidadUsu.Text.Trim().Length <= 0)
                    lblMensaje.Append("Debe Ingresar " + lblCantContra.Text + ". <br> ");
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidadUsu.Text.Trim());
                        if (liValor < 1)
                            lblMensaje.Append("Valor Inválido " + lblCantContra.Text + ". <br> ");
                        else
                        {
                            if (liValor > Convert.ToDecimal(TxtCantidad.Text))
                                lblMensaje.Append("La " + lblCantContra.Text + " No puede ser Mayor que " + TxtCantidad.Text + ". <br> ");
                            else
                            {
                                if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(TxtCantidad.Text))
                                    lblMensaje.Append("La cantidad Acumulada de Usuarios No puede ser Mayor que " + TxtCantidad.Text + ". <br> ");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Append("Valor Inválido " + lblCantContra.Text + ". <br> ");
                    }
                }
                if (TxtEquivaleKpcd.Text.Trim().Length <= 0)
                    lblMensaje.Append("Debe Ingresar Equivalente Kpcd. <br> ");
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtEquivaleKpcd.Text.Trim());
                        if (liValor < 0)
                            lblMensaje.Append("Valor Inválido Equivalente Kpcd. <br> ");
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Append("Valor Inválido " + lblCantContra.Text + ". <br> ");
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }
                lValorParametros[0] = "0";
                if (HdfCodModif != "")
                    lValorParametros[1] = HdfCodModif;
                if (HdfIdRegistro != "")
                    lValorParametros[2] = HdfIdRegistro;
                if (hdfTipoDemanda.Value == "2")
                {
                    lValorParametros[3] = lsUsuario[0].Trim();
                    lValorParametros[4] = lsTipoDoc;
                    lValorParametros[5] = lsNombre;
                }
                lValorParametros[6] = ddlSector.SelectedValue;
                lValorParametros[7] = ddlPuntoSalida.SelectedValue;
                lValorParametros[8] = TxtCantidadUsu.Text.Trim();
                lValorParametros[9] = dlTipoDemanda.SelectedValue;
                lValorParametros[10] = ddlMercadoRel.SelectedValue;
                lValorParametros[11] = TxtEquivaleKpcd.Text.Trim();
                lValorParametros[13] = DdlDestino.SelectedValue;
                lValorParametros[14] = HdfNoSolicitud;

                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetModContCesionRegUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    ToastrEvent?.Invoke("Se presentó un Problema en la Creación de la Información del Usuario.!" + goInfo.mensaje_error, EnumTypeToastr.Error);
                    lConexion.Cerrar();
                }
                else
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(TxtCantidadUsu.Text.Trim())).ToString();
                    ToastrEvent?.Invoke("Información de Usuarios Finales Ingresada Correctamente.!", EnumTypeToastr.Success);
                    TxtUsuarioFinal.Text = "";
                    ddlSector.SelectedValue = "0";
                    ddlPuntoSalida.SelectedValue = "0";
                    ddlMercadoRel.SelectedValue = "0";
                    TxtCantidadUsu.Text = "0";
                    TxtEquivaleKpcd.Text = "0"; // Campos nuevos Req. 009-17 Indicadores 20170321
                    CargarDatosUsu();
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnActualUsu_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_cont_usr", "@P_codigo_modif","@P_codigo_verif", "@P_no_identificacion_usr", "@P_codigo_tipo_doc",
                                        "@P_nombre_usuario", "@P_codigo_sector_consumo", "@P_codigo_punto_salida","@P_cantidad_contratada","@P_tipo_demanda",
                                        "@P_codigo_mercado_relevante","@P_equivalente_kpcd", "@P_accion","@P_destino_rueda", "@P_codigo_solicitud"
                                      };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,
                                         SqlDbType.VarChar, SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,
                                         SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int
                                      };
            string[] lValorParametros = { "0", "0", "0", "", "", "", "0", " 0", "0", "0", "0","0","2","", "0"
                                    };
            var lblMensaje = new StringBuilder();
            string[] lsUsuario;
            int liValor = 0;
            string lsNombre = "";
            string lsTipoDoc = "";
            try
            {
                lConexion.Abrir();
                lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
                if (dlTipoDemanda.SelectedValue == "0")
                    lblMensaje.Append("Debe Seleccionar el Tipo Demanda Atender. <br> ");
                else
                {
                    if (hdfTipoDemanda.Value == "2")
                    {
                        if (TxtUsuarioFinal.Text.Trim().Length <= 0)
                            lblMensaje.Append("Debe Ingresar el Usuario Final <br>");
                        else
                        {
                            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                            if (!lLector.HasRows)
                                lblMensaje.Append("El Usuario Ingresado NO existe en la Base de Datos.! <br>");
                            else
                            {
                                lLector.Read();
                                lsNombre = lLector["nombre"].ToString();
                                lsTipoDoc = lLector["codigo_tipo_doc"].ToString();
                            }
                            lLector.Close();
                            lLector.Dispose();
                        }
                    }
                    else
                    {
                        if (ddlMercadoRel.SelectedValue == "0")
                            lblMensaje.Append("Debe Seleccionar el mercado relevante. <br> ");
                    }

                }
                if (ddlSector.SelectedValue == "0")
                    lblMensaje.Append("Debe Seleccionar " + lblSector.Text + ". <br> ");
                if (ddlPuntoSalida.SelectedValue == "0")
                    lblMensaje.Append("Debe Seleccionar  Punto de Salida en SNT. <br> ");
                if (TxtCantidadUsu.Text.Trim().Length <= 0)
                    lblMensaje.Append("Debe Ingresar " + lblCantContra.Text + ". <br> ");
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidadUsu.Text.Trim());
                        if (liValor < 1)
                            lblMensaje.Append("Valor Inválido " + lblCantContra.Text + ". <br> ");
                        else
                        {
                            if (liValor > Convert.ToDecimal(TxtCantidad.Text))
                                lblMensaje.Append("La " + lblCantContra.Text + " No puede ser Mayor que " + TxtCantidad.Text + ". <br> ");
                            else
                            {
                                if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(TxtCantidad.Text))
                                    lblMensaje.Append("La Cantidad Acumulada de Usuarios No puede ser Mayor que " + TxtCantidad.Text + ". <br> ");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Append("Valor Inválido " + lblCantContra.Text + ". <br> ");
                    }
                }
                if (TxtEquivaleKpcd.Text.Trim().Length <= 0)
                    lblMensaje.Append("Debe Ingresar el Equivalente Kpcd. <br> ");
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtEquivaleKpcd.Text.Trim());
                        if (liValor < 0)
                            lblMensaje.Append("Valor Inválido en Equivalente Kpcd. <br> ");
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Append("Valor Inválido en " + lblCantContra.Text + ". <br> ");
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }
                if (hdfCodDatUsu.Value != "")
                    lValorParametros[0] = hdfCodDatUsu.Value;
                if (HdfCodModif != "")
                    lValorParametros[1] = HdfCodModif;
                if (HdfIdRegistro != "")
                    lValorParametros[2] = HdfIdRegistro;
                if (hdfTipoDemanda.Value == "2")
                {
                    lValorParametros[3] = lsUsuario[0].Trim();
                    lValorParametros[4] = lsTipoDoc;
                    lValorParametros[5] = lsNombre;
                }
                lValorParametros[6] = ddlSector.SelectedValue;
                lValorParametros[7] = ddlPuntoSalida.SelectedValue;
                lValorParametros[8] = TxtCantidadUsu.Text.Trim();
                lValorParametros[9] = dlTipoDemanda.SelectedValue;
                lValorParametros[10] = ddlMercadoRel.SelectedValue;
                lValorParametros[11] = TxtEquivaleKpcd.Text.Trim();
                lValorParametros[13] = DdlDestino.SelectedValue;
                lValorParametros[14] = HdfNoSolicitud;

                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetModContCesionRegUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    ToastrEvent?.Invoke("Se presentó un Problema en la Actualización de la Información del Usuario.! " + goInfo.mensaje_error, EnumTypeToastr.Error);
                    lConexion.Cerrar();
                }
                else
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                    ToastrEvent?.Invoke("Información de Usuarios Finales Actualizada Correctamente.!", EnumTypeToastr.Success);
                    TxtUsuarioFinal.Text = "";
                    ddlSector.SelectedValue = "0";
                    ddlPuntoSalida.SelectedValue = "0";
                    ddlMercadoRel.SelectedValue = "0";
                    TxtCantidadUsu.Text = "0";
                    TxtEquivaleKpcd.Text = "0"; // Campos nuevos Req. 009-17 Indicadores 20170321
                    CargarDatosUsu();
                    btnActualUsu.Visible = false;
                    btnCrearUsu.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dlTipoDemanda_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hdfTipoDemanda.Value = dlTipoDemanda.SelectedValue;
                if (hdfTipoDemanda.Value == "1")
                {
                    TxtUsuarioFinal.Enabled = false;
                    ddlMercadoRel.Enabled = true; //20160303
                    lblSector.Text = "Sector de Consumo Usuario Regulado";
                    lblCantContra.Text = "Cantidad a entregar";
                }
                else
                {
                    TxtUsuarioFinal.Enabled = true;
                    ddlMercadoRel.Enabled = false; //20160303
                    ddlMercadoRel.SelectedValue = "0"; //20160303
                    lblCantContra.Text = "Cantidad Contratada por usuario (MBTUD)";
                    lblSector.Text = "Sector Consumo Usuario No Regulado";
                }
                lConexion.Abrir();
                ddlSector.Items.Clear();
                ddlPuntoSalida.Items.Clear();
                LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo sec, m_demanda_sector dem", " sec.estado ='A' and sec.codigo_sector_consumo = dem.codigo_sector_consumo  and dem.estado ='A' and dem.codigo_tipo_demanda =" + dlTipoDemanda.SelectedValue + "  and dem.ind_uso ='T' order by descripcion", 0, 1); //20160706
                LlenarControles(lConexion.gObjConexion, ddlPuntoSalida, "m_punto_salida_snt", " estado = 'A'  order by descripcion", 0, 2);
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Nombre: CargarDatosUsu
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid de Usuarios.
        /// Modificacion:
        /// </summary>
        private void CargarDatosUsu()
        {
            string[] lsNombreParametros = { "@P_codigo_modif", "@P_codigo_verif", "@P_no_identificacion_usr", "@P_login" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "0", "", "" };
            try
            {
                if (HdfCodModif.Trim().Length > 0 && HdfCodModif.Trim() != "0")
                    lValorParametros[0] = HdfCodModif;
                if (HdfIdRegistro.Trim().Length > 0)
                    lValorParametros[1] = HdfIdRegistro;
                if (Session["tipoPerfil"].ToString() == "N")
                    lValorParametros[3] = goInfo.Usuario.ToString();
                lConexion.Abrir();
                dtgUsuarios.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContModCesionUsuFin", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgUsuarios.DataBind();
                lConexion.Cerrar();
                lblTotlCantidad.Text = "0";
                if (dtgUsuarios.Items.Count > 0)
                {
                    foreach (DataGridItem Grilla in dtgUsuarios.Items)
                    {
                        lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(Grilla.Cells[6].Text.Trim())).ToString();
                    }
                    dtgUsuarios.Columns[7].Visible = true;
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("No se Pudo Generar el Informe.! " + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: dtgUsuarios_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgUsuarios_EditCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals("Modificar"))
            {
                try
                {
                    ////////////////////////////////////////////////////////////////////////////////////////
                    /// OJO Cambios de los Indices por adicion de Campo Req. 009-17 Indicadores 20170321 ///
                    ////////////////////////////////////////////////////////////////////////////////////////
                    string lsTipoDoc = "";
                    if (dtgUsuarios.Items[e.Item.ItemIndex].Cells[14].Text == "1")  //rq009-17
                        lsTipoDoc = "Nit";
                    else
                        lsTipoDoc = "Cédula";
                    hdfCodDatUsu.Value = dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text; //rq009-17
                                                                                             //Ajuste para que no muestre infrmacion mala 20160621
                    if (dtgUsuarios.Items[e.Item.ItemIndex].Cells[1].Text == "&nbsp;")
                        TxtUsuarioFinal.Text = lsTipoDoc;
                    else
                        TxtUsuarioFinal.Text = dtgUsuarios.Items[e.Item.ItemIndex].Cells[1].Text + "-" + lsTipoDoc + "-" + dtgUsuarios.Items[e.Item.ItemIndex].Cells[3].Text;
                    TxtCantidadUsu.Text = dtgUsuarios.Items[e.Item.ItemIndex].Cells[6].Text;
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) - Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                    dlTipoDemanda.SelectedValue = dtgUsuarios.Items[e.Item.ItemIndex].Cells[15].Text; //rq009-17
                    dlTipoDemanda_SelectedIndexChanged(null, null);
                    ddlSector.SelectedValue = dtgUsuarios.Items[e.Item.ItemIndex].Cells[12].Text; //rq009-17
                    ddlMercadoRel.SelectedValue = dtgUsuarios.Items[e.Item.ItemIndex].Cells[16].Text; //rq009-17
                    ddlPuntoSalida.SelectedValue = dtgUsuarios.Items[e.Item.ItemIndex].Cells[13].Text; //rq009-17
                    btnCrearUsu.Visible = false;
                    btnActualUsu.Visible = true;
                    TxtEquivaleKpcd.Text = dtgUsuarios.Items[e.Item.ItemIndex].Cells[7].Text;
                    TdIndica.Visible = true;
                    ///TdIndica1.Visible = true;
                    btnActualUsu.Visible = true;
                    btnCrearUsu.Visible = false;
                    HdfCodigoVerUsr = dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text;
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Problemas al Recuperar el Registro.! " + ex.Message, EnumTypeToastr.Error);
                }

            }
            if (e.CommandName.Equals("Eliminar"))
            {
                string[] lsNombreParametros = { "@P_codigo_cont_usr", "@P_accion", "@P_codigo_solicitud" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { hdfCodDatUsu.Value, "3", "0" };

                try
                {
                    hdfCodDatUsu.Value = dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text;
                    lValorParametros[0] = hdfCodDatUsu.Value;
                    lValorParametros[2] = HdfNoSolicitud;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetModContCesionRegUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                    {
                        ToastrEvent?.Invoke("Se presentó un Problema en la Eliminación de la Información del Usuario.!" + goInfo.mensaje_error, EnumTypeToastr.Error);
                        lConexion.Cerrar();
                    }
                    else
                    {
                        TxtCantidadUsu.Text = dtgUsuarios.Items[e.Item.ItemIndex].Cells[6].Text; //20190506 ajsute
                        lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) - Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                        lConexion.Cerrar();
                        ToastrEvent?.Invoke("Registro Eliminado Correctamente.!", EnumTypeToastr.Success);
                        CargarDatosUsu();
                        TxtCantidadUsu.Text = ""; //20190506 ajsute
                    }
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("Problemas al Eliminar el Registro.! " + ex.Message, EnumTypeToastr.Error);
                }
            }
        }

        /// <summary>
        /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnGrabarDef_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_solicitud"
                                      };
            SqlDbType[] lTipoparametros = { SqlDbType.Int
                                        };
            string[] lValorParametros = { HdfNoSolicitud
                                    };
            var lblMensaje = new StringBuilder();
            try
            {
                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModContCesionDef", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Append(lLector["error"] + "<br>");
                }
                lConexion.Cerrar();

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                    return;
                }
                btnRegresar_Click(null, null);
                ToastrEvent?.Invoke("Solicitud grabada completa correctamente", EnumTypeToastr.Success);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='t_contrato_modificacion' and llave_registro='codigo_verif_contrato=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_verif_contrato=" + lscodigo_registro.ToString();
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_contrato_modificacion";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_contrato_modificacion", lsCondicion1);
            }
            return true;
        }

        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20170814 rq036-17
        protected void DdlBusMercado_SelectedIndexChanged(object sender, EventArgs e)
        {
            DdlBusProducto.Items.Clear();
            ListItem lItem = new ListItem();
            lItem.Value = "";
            lItem.Text = "Seleccione";
            DdlBusProducto.Items.Add(lItem);
            ListItem lItem1 = new ListItem();
            lItem1.Value = "G";
            lItem1.Text = "Suministro de gas";
            DdlBusProducto.Items.Add(lItem1);
            ListItem lItem2 = new ListItem();
            lItem2.Value = "T";
            lItem2.Text = "Capacidad de transporte";
            DdlBusProducto.Items.Add(lItem2);
            if (DdlBusMercado.SelectedValue == "" || DdlBusMercado.SelectedValue == "O")
            {
                ListItem lItem3 = new ListItem();
                lItem3.Value = "A";
                lItem3.Text = "Suministro y transporte";
                DdlBusProducto.Items.Add(lItem3);
            }
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected void trae_usr_fin(string lsModif, string lsVerif)
        {
            string[] lsNombreParametros = { "@P_codigo_modif","@P_codigo_verif"
                                      };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int
                                        };
            string[] lValorParametros = { lsModif, lsVerif
                                    };
            try
            {
                lConexion.Abrir();
                DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetUsrFinModCes", lsNombreParametros, lTipoparametros, lValorParametros);
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Se presentó un error al recuperar los usuarios finales. " + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected void revisa_usr_fin()
        {
            if (goInfo.cod_comisionista != DdlComprador.SelectedValue)
            {
                btnCrearUsu.Visible = false;
                // Se oculta la sección de demanda
                tblDemanda.Visible = false;
                TblDemanda = false;
            }
            else
            {
                if (DdlMercado.SelectedValue != "O")
                {
                    btnCrearUsu.Visible = true;
                    // Se muestra la sección de demanda
                    tblDemanda.Visible = true;
                    TblDemanda = true;
                    trae_usr_fin(HdfCodModif, HdfIdRegistro);
                }
                else
                {
                    btnCrearUsu.Visible = false;
                    // Se oculta la sección de demanda
                    tblDemanda.Visible = false;
                    TblDemanda = false;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DdlComprador_SelectedIndexChanged(object sender, EventArgs e)
        {
            revisa_usr_fin();
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        /// //20190425 rq022-19 ajsute modificaciones
        protected void color_label(string lsColor)
        {
            if (lsColor == "N")
            {
                LblOperadorC.ForeColor = System.Drawing.Color.Black;
                LblOperadorV.ForeColor = System.Drawing.Color.Black;
                LblFechaIni.ForeColor = System.Drawing.Color.Black;
                LblFechaFin.ForeColor = System.Drawing.Color.Black;
                LblCantidad.ForeColor = System.Drawing.Color.Black;
                trCambioUsrFin.Visible = false;
            }
            else
            {
                string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_codigo_modif" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { HdfIdRegistro, HdfCodModif };
                try
                {
                    lConexion.Abrir();
                    SqlDataReader lLector;
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetDatoModifContCes", lsNombreParametros, lTipoparametros, lValorParametros);
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        if (lLector["mod_operador_c"].ToString() == "S")
                            LblOperadorC.ForeColor = System.Drawing.Color.Red;
                        else
                            LblOperadorC.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_operador_v"].ToString() == "S")
                            LblOperadorV.ForeColor = System.Drawing.Color.Red;
                        else
                            LblOperadorV.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_fecha_ini"].ToString() == "S")
                            LblFechaIni.ForeColor = System.Drawing.Color.Red;
                        else
                            LblFechaIni.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_fecha_fin"].ToString() == "S")
                            LblFechaFin.ForeColor = System.Drawing.Color.Red;
                        else
                            LblFechaFin.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_cantidad"].ToString() == "S")
                            LblCantidad.ForeColor = System.Drawing.Color.Red;
                        else
                            LblCantidad.ForeColor = System.Drawing.Color.Black;
                        if (lLector["mod_usr_final"].ToString() == "S")
                            trCambioUsrFin.Visible = true;
                        else
                            trCambioUsrFin.Visible = false;
                    }
                    else
                    {
                        LblOperadorC.ForeColor = System.Drawing.Color.Black;
                        LblOperadorV.ForeColor = System.Drawing.Color.Black;
                        LblFechaIni.ForeColor = System.Drawing.Color.Black;
                        LblFechaFin.ForeColor = System.Drawing.Color.Black;
                        LblCantidad.ForeColor = System.Drawing.Color.Black;
                        trCambioUsrFin.Visible = false;
                    }
                    lLector.Close();
                    lConexion.Cerrar();
                }
                catch (Exception)
                {
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void lkbExcel_Click(object sender, EventArgs e)
        {
            try
            {
                string lsNombreArchivo = Session["login"] + "InfExcelReg" + DateTime.Now + ".xls";
                StringBuilder lsb = new StringBuilder();
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                dtgConsultaExcel.Visible = true;
                lpagina.EnableEventValidation = false;
                lpagina.Controls.Add(lform);
                dtgConsulta.EnableViewState = false;
                lform.Controls.Add(dtgConsultaExcel);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = Encoding.Default;

                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + "Consulta de contratos para modificar" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write(lsb.ToString());
                dtgConsultaExcel.Visible = false;
                Response.End();
                Response.Flush();
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("Problemas al Consultar los Registros. " + ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void SeleccionSeccionTr01(bool estado)
        {
            operadorContraparte.Visible = estado;
            busCausa.Visible = estado;
            busEstado.Visible = estado;
        }

        /// <summary>
        /// 
        /// </summary>
        public void SeleccionSeccionTr02(bool estado)
        {
            busCompra.Visible = estado;
            busVenta.Visible = estado;
        }

        /// <summary>
        /// 
        /// </summary>
        public void SeleccionSeccionTr03(bool estado)
        {
            busMercado.Visible = estado;
            busProducto.Visible = estado;
            busModalidad.Visible = estado;
            busCausa1.Visible = estado;
            busEstado1.Visible = estado;
        }
    }
}