﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucCargaModificaRegistroCession.ascx.cs" Inherits="UsersControls.Contratos.Registro_Contratos.Modificacion_Contratos_Cesion.WucCargaModificaRegistroCession" %>

<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label ID="Label2" Text="Archivo Contratos" AssociatedControlID="FuArchivo" runat="server" />
            <asp:FileUpload ID="FuArchivo" CssClass="form-control" runat="server" EnableTheming="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label ID="Label3" Text="Archivo Usuarios Finales por Contrato (Sólo Compradores)" AssociatedControlID="FuArchivoUsuarios" runat="server" />
            <asp:FileUpload ID="FuArchivoUsuarios" CssClass="form-control" runat="server" EnableTheming="true" />
        </div>
    </div>
</div>

<%--Modals--%>
<%--Solicitud--%>
<div class="modal fade" id="mdlSolicitud" tabindex="-1" role="dialog" aria-labelledby="mdlSolicitudLabel" aria-hidden="true" clientidmode="Static" runat="server">
    <div class="modal-dialog" id="mdlSolicitudInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="mdlSolicitudLabel">Modificación</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Id registro" AssociatedControlID="LblContrato01" runat="server" />
                                    <asp:Label ID="LblContrato01" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:Label ID="LblContrato02" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:Label ID="LblContrato03" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:Label ID="LblContrato04" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:Label ID="LblContrato05" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:Label ID="LblContrato06" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:Label ID="LblContrato07" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:Label ID="LblContrato08" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:Label ID="LblContrato09" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:Label ID="LblContrato10" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:Label ID="LblContrato11" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:Label ID="LblContrato12" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:Label ID="LblContrato13" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:Label ID="LblContrato14" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:Label ID="LblContrato15" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:Label ID="LblContrato16" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:Label ID="LblContrato17" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:Label ID="LblContrato18" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:Label ID="LblContrato19" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:Label ID="LblContrato20" CssClass="form-control" Visible="False" runat="server" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Contrato original" AssociatedControlID="FuArchivoOrg01" runat="server" />
                                    <asp:FileUpload ID="FuArchivoOrg01" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoOrg02" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoOrg03" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoOrg04" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoOrg05" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoOrg06" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoOrg07" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoOrg08" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoOrg09" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoOrg10" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoOrg11" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoOrg12" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoOrg13" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoOrg14" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoOrg15" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoOrg16" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoOrg17" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoOrg18" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoOrg19" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoOrg20" CssClass="form-control" Visible="False" runat="server" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Contrato Modificación" AssociatedControlID="FuArchivoMod01" runat="server" />
                                    <asp:FileUpload ID="FuArchivoMod01" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoMod02" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoMod03" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoMod04" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoMod05" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoMod06" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoMod07" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoMod08" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoMod09" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoMod10" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoMod11" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoMod12" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoMod13" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoMod14" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoMod15" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoMod16" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoMod17" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoMod18" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoMod19" CssClass="form-control" Visible="False" runat="server" />
                                    <asp:FileUpload ID="FuArchivoMod20" CssClass="form-control" Visible="False" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="BtnCancelar" Text="Cancelar" OnClick="BtnCancelar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" class="btn btn-secondary" runat="server" />
                        <asp:Button ID="BtnAceptar" Text="Cargar Archivos" OnClick="BtnAceptar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" CssClass="btn btn-primary" runat="server" />
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="BtnAceptar" runat="server" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
