﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucModificaRegistroCession.ascx.cs" Inherits="UsersControls.Contratos.Registro_Contratos.Modificacion_Contratos_Cesion.WucModificaRegistroCession" %>

<%--Contenido--%>
<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Número Contrato" AssociatedControlID="TxtBusContratoDef" runat="server" />
            <asp:TextBox ID="TxtBusContratoDef" runat="server" class="form-control" ValidationGroup="detalle" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Número Operación" AssociatedControlID="TxtBusOperacion" runat="server" />
            <asp:TextBox ID="TxtBusOperacion" class="form-control" runat="server" ValidationGroup="detalle" MaxLength="10"></asp:TextBox>
            <asp:CompareValidator ID="CvTxtBusOperacion" runat="server" ControlToValidate="TxtBusOperacion" Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Número Operación debe Ser numérico">*</asp:CompareValidator>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Id Registro" AssociatedControlID="TxtBusId" runat="server" />
            <asp:TextBox ID="TxtBusId" class="form-control" runat="server" ValidationGroup="detalle" MaxLength="10"></asp:TextBox>
            <asp:CompareValidator ID="CvTxtBusId" runat="server" ControlToValidate="TxtBusId" Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Número Id Registro debe Ser numérico">*</asp:CompareValidator>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Fec. Neg. Ini." AssociatedControlID="TxtBusFechaIni" runat="server" />
            <asp:TextBox ID="TxtBusFechaIni" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" Width="100%" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Fec. Neg. Fin" AssociatedControlID="TxtBusFechaIni" runat="server" />
            <asp:TextBox ID="TxtBusFechaFin" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" Width="100%" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
        </div>
    </div>
    <div id="operadorContraparte" class="col-sm-12 col-md-6 col-lg-4" runat="server">
        <div class="form-group">
            <asp:Label ID="lblBusContra" Text="Operador Contraparte" AssociatedControlID="DdlBusContra" runat="server" />
            <asp:DropDownList ID="DdlBusContra" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div id="busCausa" class="col-sm-12 col-md-6 col-lg-4" runat="server">
        <div class="form-group">
            <asp:Label Text="Causa" AssociatedControlID="DdlBusCausa" runat="server" />
            <asp:DropDownList ID="DdlBusCausa" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div id="busEstado" class="col-sm-12 col-md-6 col-lg-4" runat="server">
        <div class="form-group">
            <asp:Label Text="Estado" AssociatedControlID="DdlBusEstado" runat="server" />
            <asp:DropDownList ID="DdlBusEstado" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div id="busCompra" class="col-sm-12 col-md-6 col-lg-4" runat="server">
        <div class="form-group">
            <asp:Label Text="Comprador" AssociatedControlID="DdlBusCompra" runat="server" />
            <asp:DropDownList ID="DdlBusCompra" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div id="busVenta" class="col-sm-12 col-md-6 col-lg-4" runat="server">
        <div class="form-group">
            <asp:Label Text="Vendedor" AssociatedControlID="DdlBusVenta" runat="server" />
            <asp:DropDownList ID="DdlBusVenta" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div id="busMercado" class="col-sm-12 col-md-6 col-lg-4" runat="server">
        <div class="form-group">
            <asp:Label Text="Tipo Mercado" AssociatedControlID="DdlBusMercado" runat="server" />
            <asp:DropDownList ID="DdlBusMercado" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true" OnSelectedIndexChanged="DdlBusMercado_SelectedIndexChanged">
                <asp:ListItem Value="">Seleccione</asp:ListItem>
                <asp:ListItem Value="P">Primario</asp:ListItem>
                <asp:ListItem Value="S">Secundario</asp:ListItem>
                <asp:ListItem Value="O">Otras transacciones del mercado mayorista</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div id="busProducto" class="col-sm-12 col-md-6 col-lg-4" runat="server">
        <div class="form-group">
            <asp:Label Text="Producto" AssociatedControlID="DdlBusProducto" runat="server" />
            <asp:DropDownList ID="DdlBusProducto" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                <asp:ListItem Value="">Seleccione</asp:ListItem>
                <asp:ListItem Value="G">Suministro de gas</asp:ListItem>
                <asp:ListItem Value="T">Capacidad de transporte</asp:ListItem>
                <asp:ListItem Value="A">Suministro y transporte</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div id="busModalidad" class="col-sm-12 col-md-6 col-lg-4" runat="server">
        <div class="form-group">
            <asp:Label Text="Tipo de contrato" AssociatedControlID="DdlBusModalidad" runat="server" />
            <asp:DropDownList ID="DdlBusModalidad" runat="server" CssClass="form-control  selectpicker" data-live-search="true" />
        </div>
    </div>
    <div id="busCausa1" class="col-sm-12 col-md-6 col-lg-4" runat="server">
        <div class="form-group">
            <asp:Label Text="Causa" AssociatedControlID="DdlBusCausa1" runat="server" />
            <asp:DropDownList ID="DdlBusCausa1" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div id="busEstado1" class="col-sm-12 col-md-6 col-lg-4" runat="server">
        <div class="form-group">
            <asp:Label Text="Estado Modificación" AssociatedControlID="DdlBusEstado1" runat="server" />
            <asp:DropDownList ID="DdlBusEstado1" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
</div>
<%--Grilla--%>
<div class="table table-responsive">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                Width="100%" CssClass="table-bordered" OnItemCommand="dtgConsulta_EditCommand">
                <Columns>
                    <%--0--%>
                    <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" Visible="False"></asp:EditCommandColumn>
                    <%--1--%>
                    <asp:EditCommandColumn HeaderText="Consultar" EditText="Consultar" Visible="False"></asp:EditCommandColumn>
                    <%--2--%>
                    <asp:BoundColumn DataField="codigo_solicitud" Visible="false"></asp:BoundColumn>
                    <%--3--%>
                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--4--%>
                    <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--5--%>
                    <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <%--6--%>
                    <asp:BoundColumn DataField="contrato_definitivo" HeaderText="contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--7--%>
                    <asp:BoundColumn DataField="tipo_contrato" HeaderText="Tipo Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--8--%>
                    <asp:BoundColumn DataField="operador_compra" HeaderText="Cod. Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--9--%>
                    <asp:BoundColumn DataField="nombre_compra" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--10--%>
                    <asp:BoundColumn DataField="operador_venta" HeaderText="Cod. Venta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--11--%>
                    <asp:BoundColumn DataField="nombre_venta" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--12--%>
                    <asp:BoundColumn DataField="desc_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--13--%>
                    <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--14--%>
                    <asp:BoundColumn DataField="codigo_tipo_subasta" Visible="false"></asp:BoundColumn>
                    <%--15--%>
                    <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha inicial" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <%--16--%>
                    <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha final" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                    <%--17--%>
                    <asp:BoundColumn DataField="cantidad" HeaderText="cantidad" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
                    <%--18--%>
                    <asp:BoundColumn DataField="desc_estado" HeaderText="Estado Modificación"></asp:BoundColumn>
                    <%--19--%>
                    <asp:BoundColumn DataField="desc_causa" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--20--%>
                    <asp:BoundColumn DataField="desc_tipo_cesion" HeaderText="Tipo Cesión" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <%--21--%>
                    <asp:EditCommandColumn HeaderText="Contrato original" EditText="Original" Visible="False"></asp:EditCommandColumn>
                    <%--22--%>
                    <asp:EditCommandColumn HeaderText="Contrato Modificado" EditText="Modificado" Visible="False"></asp:EditCommandColumn>
                    <%--23--%>
                    <asp:BoundColumn DataField="contrato_original" Visible="false"></asp:BoundColumn>
                    <%--24--%>
                    <asp:BoundColumn DataField="contrato_nuevo" Visible="false"></asp:BoundColumn>
                    <%--25--%>
                    <asp:BoundColumn DataField="punta_solicitud" Visible="false"></asp:BoundColumn>
                    <%--26--%>
                    <asp:BoundColumn DataField="ind_accion" Visible="false"></asp:BoundColumn>
                    <%--27--%>
                    <asp:BoundColumn DataField="tipo_mercado" Visible="false"></asp:BoundColumn>
                    <%--28--%>
                    <asp:BoundColumn DataField="destino_rueda" Visible="false"></asp:BoundColumn>
                    <%--29--%>
                    <asp:BoundColumn DataField="punta" Visible="false"></asp:BoundColumn>
                    <%--30--%>
                    <asp:BoundColumn DataField="codigo_causa" Visible="false"></asp:BoundColumn>
                    <%--31--%>
                    <asp:BoundColumn DataField="codigo_modif" Visible="false"></asp:BoundColumn>
                    <%--32--%>
                    <asp:BoundColumn DataField="codigo_estado" Visible="false"></asp:BoundColumn>
                    <%--33--%>
                    <asp:BoundColumn DataField="tipo_cesion" Visible="false"></asp:BoundColumn>
                    <%--34--%>
                    <asp:BoundColumn DataField="tipo_causa" Visible="false"></asp:BoundColumn>
                    <%--35--%>
                    <asp:BoundColumn DataField="tipo_aprob" Visible="false"></asp:BoundColumn>
                    <%--36--%>
                    <asp:BoundColumn DataField="consec_enlace" HeaderText="consecutivo"></asp:BoundColumn>
                    <%--37--%>
                    <asp:BoundColumn DataField="ind_otras_mod" Visible="false"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                        <ItemTemplate>
                            <div class="dropdown dropdown-inline">
                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="flaticon-more-1"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                    <!--begin::Nav-->
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Modificar</span>
                                                    </asp:LinkButton>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <asp:LinkButton ID="lkbConsultar" CssClass="kt-nav__link" CommandName="Consultar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-expand"></i>
                                                            <span class="kt-nav__link-text">Consultar</span>
                                                    </asp:LinkButton>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <asp:LinkButton ID="lkbOriginal" CssClass="kt-nav__link" CommandName="Original" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-crisp-icons"></i>
                                                            <span class="kt-nav__link-text">Original</span>
                                                    </asp:LinkButton>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <asp:LinkButton ID="lkbModificado" CssClass="kt-nav__link" CommandName="Modificado" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-notepad"></i>
                                                            <span class="kt-nav__link-text">Modificado</span>
                                                    </asp:LinkButton>
                                                </li>
                                            </ul>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <!--end::Nav-->
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
            </asp:DataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<%--Excel--%>
<asp:DataGrid ID="dtgConsultaExcel" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
    Width="100%" CssClass="table-bordered" Visible="false">
    <Columns>
        <%--0--%>
        <%--2--%>
        <asp:BoundColumn DataField="codigo_solicitud" Visible="false"></asp:BoundColumn>
        <%--3--%>
        <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <%--4--%>
        <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <%--5--%>
        <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
        <%--6--%>
        <asp:BoundColumn DataField="contrato_definitivo" HeaderText="contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <%--7--%>
        <asp:BoundColumn DataField="tipo_contrato" HeaderText="Tipo Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <%--8--%>
        <asp:BoundColumn DataField="operador_compra" HeaderText="Cod. Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <%--9--%>
        <asp:BoundColumn DataField="nombre_compra" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <%--10--%>
        <asp:BoundColumn DataField="operador_venta" HeaderText="Cod. Venta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <%--11--%>
        <asp:BoundColumn DataField="nombre_venta" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <%--12--%>
        <asp:BoundColumn DataField="desc_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <%--13--%>
        <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <%--14--%>
        <asp:BoundColumn DataField="codigo_tipo_subasta" Visible="false"></asp:BoundColumn>
        <%--15--%>
        <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha inicial" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
        <%--16--%>
        <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha final" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
        <%--17--%>
        <asp:BoundColumn DataField="cantidad" HeaderText="cantidad" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
        <%--18--%>
        <asp:BoundColumn DataField="desc_estado" HeaderText="Estado Modificación"></asp:BoundColumn>
        <%--19--%>
        <asp:BoundColumn DataField="desc_causa" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <%--20--%>
        <asp:BoundColumn DataField="desc_tipo_cesion" HeaderText="Tipo Cesión" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
        <%--23--%>
        <asp:BoundColumn DataField="contrato_original" Visible="false"></asp:BoundColumn>
        <%--24--%>
        <asp:BoundColumn DataField="contrato_nuevo" Visible="false"></asp:BoundColumn>
        <%--25--%>
        <asp:BoundColumn DataField="punta_solicitud" Visible="false"></asp:BoundColumn>
        <%--26--%>
        <asp:BoundColumn DataField="ind_accion" Visible="false"></asp:BoundColumn>
        <%--27--%>
        <asp:BoundColumn DataField="tipo_mercado" Visible="false"></asp:BoundColumn>
        <%--28--%>
        <asp:BoundColumn DataField="destino_rueda" Visible="false"></asp:BoundColumn>
        <%--29--%>
        <asp:BoundColumn DataField="punta" Visible="false"></asp:BoundColumn>
        <%--30--%>
        <asp:BoundColumn DataField="codigo_causa" Visible="false"></asp:BoundColumn>
        <%--31--%>
        <asp:BoundColumn DataField="codigo_modif" Visible="false"></asp:BoundColumn>
        <%--32--%>
        <asp:BoundColumn DataField="codigo_estado" Visible="false"></asp:BoundColumn>
        <%--33--%>
        <asp:BoundColumn DataField="tipo_cesion" Visible="false"></asp:BoundColumn>
        <%--34--%>
        <asp:BoundColumn DataField="tipo_causa" Visible="false"></asp:BoundColumn>
        <%--35--%>
        <asp:BoundColumn DataField="tipo_aprob" Visible="false"></asp:BoundColumn>
        <%--36--%>
        <asp:BoundColumn DataField="consec_enlace" HeaderText="consecutivo"></asp:BoundColumn>
        <%--37--%>
        <asp:BoundColumn DataField="ind_otras_mod" Visible="false"></asp:BoundColumn>
    </Columns>
    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
</asp:DataGrid>

<%--Modals--%>

<%--Modal Causa Modifica--%>
<div class="modal fade" id="mdlCausaMod" tabindex="-1" role="dialog" aria-labelledby="mdlregistroContratoVerLabel" aria-hidden="true" clientidmode="Static" runat="server">
    <div class="modal-dialog" id="mdlCausaModInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="divSolicitudLabel">Causa de Modificación</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Causa de Modificación" AssociatedControlID="DdlIngCausa" runat="server" />
                                    <asp:DropDownList ID="DdlIngCausa" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Tipo Cesión" AssociatedControlID="ddlTipo" runat="server" />
                                    <asp:DropDownList ID="ddlTipo" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="T">Total</asp:ListItem>
                                        <asp:ListItem Value="P">Parcial</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Contrato original" AssociatedControlID="FuArchivoOrg" runat="server" />
                                    <ajaxToolkit:AsyncFileUpload ID="FuArchivoOrg" CssClass="form-control" OnUploadedComplete="FileUpload_UploadedComplete" Width="100%" runat="server" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Contrato Modificación" AssociatedControlID="FuArchivoMod" runat="server" />
                                    <ajaxToolkit:AsyncFileUpload ID="FuArchivoMod" CssClass="form-control" OnUploadedComplete="FileUpload_UploadedComplete" Width="100%" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="BtnCancelar" runat="server" class="btn btn-secondary" Text="Cancelar" OnClick="BtnCancelar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="BtnAceptar" runat="server" CssClass="btn btn-primary" Text="Aceptar" OnClick="BtnAceptar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>

<%--Modal Modificacion--%>
<div class="modal fade" id="mdlModificacion" tabindex="-1" role="dialog" aria-labelledby="mdlModificacionLabel" aria-hidden="true" clientidmode="Static" style="overflow-y: auto" runat="server">
    <div class="modal-dialog" id="mdlModificacionInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="mdlModificacionLabel">Modificación</h5>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <%--Infirmacion Basica--%>
                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5>Información Básica</h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Destino Rueda" AssociatedControlID="DdlDestino" runat="server" />
                                                    <asp:DropDownList ID="DdlDestino" runat="server" CssClass="form-control" Enabled="false">
                                                        <asp:ListItem Value="G" Text="Suministro de Gas"></asp:ListItem>
                                                        <asp:ListItem Value="T" Text="Capacidad de Transporte"></asp:ListItem>
                                                        <asp:ListItem Value="A" Text="Suministro y Transporte"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Tipo Mercado" AssociatedControlID="DdlMercado" runat="server" />
                                                    <asp:DropDownList ID="DdlMercado" runat="server" CssClass="form-control" Enabled="false">
                                                        <asp:ListItem Value="P" Text="Primario"></asp:ListItem>
                                                        <asp:ListItem Value="S" Text="Secundario"></asp:ListItem>
                                                        <asp:ListItem Value="O" Text="Otras Transacciones del Mercado Mayorista"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="LblOperadorC" Text="Operador Compra" AssociatedControlID="DdlComprador" runat="server" />
                                                    <asp:DropDownList ID="DdlComprador" runat="server" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="DdlComprador_SelectedIndexChanged" AutoPostBack="true" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="LblOperadorV" Text="Operador Venta" AssociatedControlID="DdlVendedor" runat="server" />
                                                    <asp:DropDownList ID="DdlVendedor" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Fecha Negociación" AssociatedControlID="TxtFechaNeg" runat="server" />
                                                    <asp:TextBox ID="TxtFechaNeg" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%" Enabled="false" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Hora Negociación" AssociatedControlID="TxtHoraNeg" runat="server" />
                                                    <asp:TextBox ID="TxtHoraNeg" runat="server" class="form-control" ValidationGroup="detalle" MaxLength="5" Enabled="false" />
                                                    <asp:RegularExpressionValidator ID="revTxtHoraNeg" ControlToValidate="TxtHoraNeg" ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ErrorMessage="Formato Incorrecto para la Hora de negociación"> * </asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Fecha Suscripción" AssociatedControlID="TxtFechaSus" runat="server" />
                                                    <asp:TextBox ID="TxtFechaSus" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%" Enabled="false" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Contrato Definitivo" AssociatedControlID="TxtContDef" runat="server" />
                                                    <asp:TextBox ID="TxtContDef" runat="server" class="form-control" ValidationGroup="detalle" MaxLength="30" Enabled="false" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Contrato Variable" AssociatedControlID="DdlVariable" runat="server" />
                                                    <asp:DropDownList ID="DdlVariable" runat="server" CssClass="form-control" Enabled="false">
                                                        <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                                        <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="LblFechaIni" Text="Fecha Entrega Inicial" AssociatedControlID="TxtFechaIni" runat="server" />
                                                    <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>
                                                    <asp:HiddenField ID="hdfErrorFecha" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="LblFechaFin" Text="Fecha Entrega Final" AssociatedControlID="TxtFechaFin" runat="server" />
                                                    <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="LblCantidad" Text="Cantidad de Energía Contratada (MBTUD)" AssociatedControlID="TxtCantidad" runat="server" />
                                                    <asp:TextBox ID="TxtCantidad" runat="server" CssClass="form-control" MaxLength="6"></asp:TextBox>
                                                    <asp:CompareValidator ID="CvTxtCantidad" runat="server" ControlToValidate="TxtCantidad" Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Cantidad de energía contratada (MBTUD) debe Ser numérico">*</asp:CompareValidator>
                                                    <asp:TextBox ID="TxtCapacOtm" runat="server" CssClass="form-control" MaxLength="6" Visible="false"></asp:TextBox>
                                                    <asp:CompareValidator ID="CvTxtCapacOtm" runat="server" ControlToValidate="TxtCapacOtm"
                                                        Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo >Cantidad de energía contratada (MBTUD) debe Ser numérico">*</asp:CompareValidator>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="LblPrecio" Text="Precio a la Fecha de Suscripción del Contrato (USD/MBTU)" AssociatedControlID="TxtPrecio" runat="server" />
                                                    <asp:TextBox ID="TxtPrecio" runat="server" CssClass="form-control" MaxLength="6" Enabled="false" />
                                                    <asp:CompareValidator ID="CvTxtPrecio" runat="server" ControlToValidate="TxtPrecio"
                                                        Type="Double" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Precio a la fecha de suscripción del contrato (USD/MBTU) debe Ser numérico">*</asp:CompareValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" id="trObs" runat="server" visible="false">
                                            <div class="col-sm-12 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <asp:Label Text="Observaciones" AssociatedControlID="TxtObs" runat="server" />
                                                    <asp:TextBox ID="TxtObs" runat="server" MaxLength="1000" TextMode="MultiLine" Width="100%" CssClass="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" runat="server" id="trCambioUsrFin" visible="false">
                                <%--20190425 rq022-19 --%>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label ID="LblModUsrFin" runat="server" ForeColor="Red" CssClass="form-control">LOS USUARIOS FINALES HAN SIDO MODIFICADOS</asp:Label>
                                    </div>
                                </div>
                            </div>

                            <%--Demanda--%>
                            <div id="tblDemanda" visible="false" runat="server">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5>Usuarios Finales</h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Tipo Demanda a Atender" AssociatedControlID="dlTipoDemanda" runat="server" />
                                                    <asp:DropDownList ID="dlTipoDemanda" runat="server" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="dlTipoDemanda_SelectedIndexChanged" AutoPostBack="true" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Usuario Final" AssociatedControlID="TxtUsuarioFinal" runat="server" />
                                                    <asp:TextBox ID="TxtUsuarioFinal" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <ajaxToolkit:AutoCompleteExtender runat="server" BehaviorID="AutoCompleteExUsuF" ID="AutoCompleteExtender1"
                                                        TargetControlID="TxtUsuarioFinal" ServicePath="~/WebService/AutoComplete.asmx"
                                                        ServiceMethod="GetCompletionListUsuarioFinal" MinimumPrefixLength="3" CompletionInterval="1000"
                                                        EnableCaching="true" CompletionSetCount="20" CompletionListCssClass="autocomplete_completionListElement"
                                                        CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                        DelimiterCharacters=";,:">
                                                        <Animations>
                        <OnShow>
                            <Sequence>
                                <OpacityAction Opacity="0" />
                                <HideAction Visible="true" />
                                <ScriptAction Script="
                                    // Cache the size and setup the initial size
                                    var behavior = $find('AutoCompleteExUsuF');
                                    if (!behavior._height) {
                                        var target = behavior.get_completionList();
                                        behavior._height = target.offsetHeight - 2;
                                        target.style.height = '0px';
                                    }" />
                                <Parallel Duration=".4">
                                    <FadeIn />
                                    <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteExUsuF')._height" />
                                </Parallel>
                            </Sequence>
                        </OnShow>
                        <OnHide>
                            <Parallel Duration=".4">
                                <FadeOut />
                                <Length PropertyKey="height" StartValueScript="$find('AutoCompleteExUsuF')._height" EndValue="0" />
                            </Parallel>
                        </OnHide>
                                                        </Animations>
                                                    </ajaxToolkit:AutoCompleteExtender>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="lblSector" Text="Sector Consumo Usuario Regulado" AssociatedControlID="ddlSector" runat="server" />
                                                    <asp:DropDownList ID="ddlSector" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Punto de Salida en SNT" AssociatedControlID="ddlPuntoSalida" runat="server" />
                                                    <asp:DropDownList ID="ddlPuntoSalida" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                    <asp:HiddenField ID="hdfTipoDemanda" runat="server" />
                                                    <asp:HiddenField ID="hdfCodDatUsu" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label Text="Mercado Relevante" AssociatedControlID="ddlMercadoRel" runat="server" />
                                                    <asp:DropDownList ID="ddlMercadoRel" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <asp:Label ID="lblCantContra" Text="Cantidad a Entregar" AssociatedControlID="TxtCantidadUsu" runat="server" />
                                                    <asp:TextBox ID="TxtCantidadUsu" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-4" runat="server" id="TdIndica">
                                                <div class="form-group">
                                                    <asp:Label Text="Equivalente Kpcd" AssociatedControlID="TxtEquivaleKpcd" runat="server" />
                                                    <asp:TextBox ID="TxtEquivaleKpcd" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <asp:Button ID="btnCrearUsu" runat="server" CssClass="btn btn-primary btn-block" Text="Agregar Usuario" OnClick="btnCrearUsu_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" Visible="false" />
                                        <asp:Button ID="btnActualUsu" runat="server" CssClass="btn btn-primary btn-lg btn-block" Text="Actualiza Usuario" OnClick="btnActualUsu_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" Visible="false" />

                                        <hr>

                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <b>TOTAL CANTIDAD:</b><asp:Label ID="lblTotlCantidad" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table table-responsive">
                                            <asp:DataGrid ID="dtgUsuarios" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                                Width="100%" CssClass="table-bordered" OnItemCommand="dtgUsuarios_EditCommand">
                                                <Columns>
                                                    <%--0--%>
                                                    <asp:BoundColumn DataField="tipo_demanda" HeaderText="Tipo Demanda Atender" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--1--%>
                                                    <asp:BoundColumn DataField="no_identificacion_usr" HeaderText="Identificación Usuario Final"
                                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--2--%>
                                                    <asp:BoundColumn DataField="tipo_documento" HeaderText="Tipo Identificación Usuario Final"
                                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                                                    <%--3--%>
                                                    <asp:BoundColumn DataField="nombre_usuario" HeaderText="Nombre Usuario Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--4--%>
                                                    <asp:BoundColumn DataField="sector_consumo" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"
                                                        ItemStyle-Width="80px"></asp:BoundColumn>
                                                    <%--5--%>
                                                    <asp:BoundColumn DataField="punto_salida" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"
                                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                                    <%--6--%>
                                                    <asp:BoundColumn DataField="cantidad_contratada" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Left"
                                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                                    <%--7--%>
                                                    <asp:BoundColumn DataField="equivalente_kpcd" HeaderText="Equivalente Kpcd" ItemStyle-HorizontalAlign="Left"
                                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                                    <%--8--%>
                                                    <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" Visible="False"></asp:EditCommandColumn>
                                                    <%--9--%>
                                                    <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar" Visible="False"></asp:EditCommandColumn>
                                                    <%--10--%>
                                                    <asp:BoundColumn DataField="codigo_cont_usr" Visible="false"></asp:BoundColumn>
                                                    <%--11--%>
                                                    <asp:BoundColumn DataField="codigo_modif_cesion" Visible="false"></asp:BoundColumn>
                                                    <%--12--%>
                                                    <asp:BoundColumn DataField="codigo_sector_consumo" Visible="false"></asp:BoundColumn>
                                                    <%--13--%>
                                                    <asp:BoundColumn DataField="codigo_punto_salida" Visible="false"></asp:BoundColumn>
                                                    <%--14--%>
                                                    <asp:BoundColumn DataField="codigo_tipo_doc" Visible="false"></asp:BoundColumn>
                                                    <%--15--%>
                                                    <asp:BoundColumn DataField="codigo_tipo_demanda" Visible="false"></asp:BoundColumn>
                                                    <%--17--%>
                                                    <asp:BoundColumn DataField="codigo_mercado_relevante" Visible="false"></asp:BoundColumn>
                                                    <%--18--%>
                                                    <asp:BoundColumn DataField="codigo_cont_usr" Visible="false"></asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                                        <ItemTemplate>
                                                            <div class="dropdown dropdown-inline">
                                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="flaticon-more-1"></i>
                                                                </button>
                                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                                    <!--begin::Nav-->
                                                                    <ul class="kt-nav">
                                                                        <li class="kt-nav__item">
                                                                            <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                             <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Modificar</span>
                                                                            </asp:LinkButton>
                                                                        </li>
                                                                        <li class="kt-nav__item">
                                                                            <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon-delete"></i>
                                                            <span class="kt-nav__link-text">Eliminar</span>
                                                                            </asp:LinkButton>
                                                                        </li>
                                                                    </ul>
                                                                    <!--end::Nav-->
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnRegresar" runat="server" class="btn btn-secondary" Text="Cancelar" OnClick="btnRegresar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnModificar" runat="server" CssClass="btn btn-primary" Text="Modificar" OnClick="btnModificar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnAprobar" runat="server" CssClass="btn btn-primary" Text="Aprobar" OnClick="btnAprobar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnRechazo" runat="server" CssClass="btn btn-primary" Text="Rechazar" OnClick="btnRechazar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnAprobarBmc" runat="server" CssClass="btn btn-primary" Text="Aprobar BMC" OnClick="btnAprobarBmc_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnRechazoBMC" runat="server" CssClass="btn btn-primary" Text="Rechazar" OnClick="btnRechazarBmc_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="btnGrabarDef" runat="server" CssClass="btn btn-primary" Text="Grabar Def" OnClick="btnGrabarDef_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
