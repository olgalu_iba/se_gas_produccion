﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace UsersControls.Contratos.Capacidad
{
    public partial class WucCapaMaxiMediPlazo : UserControl
    {
        #region EventHandler

        /// <summary>
        /// Delegado para el manejo de los modals
        /// </summary>
        public delegate void EventHandlerModal(string id, string insideId, EnumTypeModal typeModal);

        /// <summary>
        /// EventHandler para la selección de los los modals
        /// </summary>
        public event EventHandlerModal ModalEvent;

        /// <summary>
        /// Delegado para el manejo de los toastr
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        /// EventHandler para la selección de los toastr 
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        /// Delegado para la selección de los botones  
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController, string route = null);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get
            {
                return ViewState["NameController"] != null &&
                       !string.IsNullOrEmpty(ViewState["NameController"].ToString())
                    ? ViewState["NameController"].ToString()
                    : string.Empty;
            }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        #endregion Propiedades

        private InfoSessionVO goInfo;
        private static string lsTitulo = "Capacidad máxima mediano plazo (KPCD)";

        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        private clConexion lConexion;
        private SqlDataReader lLector;
        private SqlDataReader lLector1;
        private string gsTabla = "m_capacidad_max_trasp";

        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lConexion = new clConexion(goInfo);
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            try
            {
                // Carga informacion de combos
                lConexion.Abrir();
                //LlenarTramo(lConexion.gObjConexion, ddlBusTramo);
                LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and tipo_operador ='T' order by razon_social", 0, 4);
                LlenarControles(lConexion.gObjConexion, ddbBusOperador, "m_operador", " estado = 'A' and tipo_operador ='T'  order by razon_social", 0, 4);
                lConexion.Cerrar();
                EstablecerPermisosSistema();
                if (goInfo.cod_comisionista != "0")
                {

                    dtgMaestro.Columns[9].Visible = false;
                    dtgMaestro.Columns[10].Visible = false;
                    ddlOperador.SelectedValue = goInfo.cod_comisionista;
                    ddlOperador.Enabled = false;
                    ddlOperador_SelectedIndexChanged(null, null);
                    ddbBusOperador.SelectedValue = goInfo.cod_comisionista;
                    ddbBusOperador.Enabled = false;
                    ddbBusOperador_SelectedIndexChanged(null, null);
                }

                Listar();
                ButtonsEvent?.Invoke(null, null, NameController, gsTabla);
                //Se establece que el controlador ya se inicializo    
                Inicializado = true;
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            var permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, gsTabla);
            foreach (DataGridItem Grilla in dtgMaestro.Items)
            {
                var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                lkbModificar.Visible = (bool)permisos["UPDATE"]; //20190516 ajuste
                var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
                lkbEliminar.Visible = false;

                if (!lkbModificar.Visible && !lkbEliminar.Visible)
                    dtgMaestro.Columns[11].Visible = false;
            }
        }

        /// <summary>
        /// Nombre: Nuevo
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
        /// Modificacion:
        /// </summary>
        private void Nuevo()
        {
            imbCrear.Visible = true;
            imbActualiza.Visible = false;
            TxtCodigoCap.Visible = false;
            LblCodigoCap.Visible = true;
            LblCodigoCap.Text = "Automatico";
            ModalEvent?.Invoke(CrearRegistro.ID, CrearRegistroInside.ID, EnumTypeModal.Abrir);
        }

        /// <summary>
        /// Nombre: Listar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            CargarDatos();
            EstablecerPermisosSistema();
            if (goInfo.cod_comisionista == "0") return;
            foreach (DataGridItem Grilla in dtgMaestro.Items)
            {
                var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                lkbModificar.Visible = false;
                var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
                lkbEliminar.Visible = false;

                if (!lkbModificar.Visible && !lkbEliminar.Visible)
                    dtgMaestro.Columns[11].Visible = false;
            }
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            var lblMensaje = new StringBuilder();
            try
            {
                if (!string.IsNullOrEmpty(modificar))
                {
                    mdlCrearRegistroLabel.InnerHtml = "Modificar";
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia",
                            gsTabla, " codigo_capacidad= " + modificar + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            LblCodigoCap.Text = lLector["codigo_capacidad"].ToString();
                            TxtCodigoCap.Text = lLector["codigo_capacidad"].ToString();
                            TxtFecha.Text = lLector["fecha_vigencia"].ToString().Substring(6, 4) +
                                            lLector["fecha_vigencia"].ToString().Substring(2, 4) +
                                            lLector["fecha_vigencia"].ToString().Substring(0, 2);
                            try
                            {
                                ddlOperador.SelectedValue = lLector["codigo_trasportador"].ToString();
                                ddlOperador_SelectedIndexChanged(null, null);
                            }
                            catch (Exception)
                            {
                                lblMensaje.Append("El operador del registro no existe o esta inactivo<br>");
                            }

                            try
                            {
                                ddlTramo.SelectedValue = lLector["codigo_tramo"].ToString();
                            }
                            catch (Exception)
                            {
                                lblMensaje.Append("El tramo del registro no existe o esta inactivo<br>");
                            }

                            TxtCantidad.Text = lLector["capacidad_maxima_dia"].ToString();
                            ddlEstado.SelectedValue = lLector["estado"].ToString();
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                            TxtCodigoCap.Visible = false;
                            LblCodigoCap.Visible = true;
                        }

                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Bloquea el Registro a Modificar
                        manejo_bloqueo("A", modificar);
                    }
                    else
                    {
                        Listar();
                        lblMensaje.Append("No se Puede editar el Registro por que esta Bloqueado. Codigo Capacidad" + modificar);

                    }
                    if (lblMensaje.ToString() == "")
                        ModalEvent?.Invoke(CrearRegistro.ID, CrearRegistroInside.ID, EnumTypeModal.Abrir);
                    else
                        ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_codigo_capacidad", "@P_codigo_tramo", "@P_codigo_operador" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0" };

            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                if (ddlBusTramo.SelectedValue != "0" && ddlBusTramo.SelectedValue != "")
                    lValorParametros[1] = ddlBusTramo.SelectedValue;
                if (ddbBusOperador.SelectedValue != "0")
                    lValorParametros[2] = ddbBusOperador.SelectedValue;

                lConexion.Abrir();
                dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion,
                    "pa_GetCapacidadMax", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgMaestro.DataBind();
                lConexion.Cerrar();

                if (goInfo.cod_comisionista == "0") return;
                foreach (DataGridItem Grilla in dtgMaestro.Items)
                {
                    var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                    lkbModificar.Visible = false;
                    var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
                    lkbEliminar.Visible = false;
                }
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.Message, EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            string lCodigoRegistro = "";
            if (e.CommandName.Equals("Modificar"))
            {
                lCodigoRegistro = dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                TxtFecha.Enabled = false;
                ddlTramo.Enabled = false;
                ddlOperador.Enabled = false;
                Modificar(lCodigoRegistro);
            }
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia("m_capacidad_max_trasp", lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarTramo(SqlConnection lConn, DropDownList lDdl)
        {
            lDdl.Items.Clear();
            string[] lsNombreParametros = { "@P_codigo_trasportador" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int };
            string[] lValorParametros = { "0" };

            if (ddlOperador.SelectedValue != "")
                lValorParametros[0] = ddlOperador.SelectedValue;

            SqlDataReader lLector;
            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConn, "pa_GetTramo",
                lsNombreParametros, lTipoparametros, lValorParametros);
            if (lLector.HasRows)
            {
                while (lLector.Read())
                {

                    ListItem lItem1 = new ListItem();
                    lItem1.Value = lLector.GetValue(0).ToString();
                    lItem1.Text = lLector["desc_tramo"].ToString();
                    lDdl.Items.Add(lItem1);
                }
            }

            lLector.Close();
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='m_capacidad_max_trasp' and llave_registro='codigo_capacidad=" +
                                 lscodigo_registro + "'";
            string lsCondicion1 = "codigo_capacidad=" + lscodigo_registro.ToString();
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }

            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "m_capacidad_max_trasp";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }

            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_capacidad_max_trasp", lsCondicion1);
            }

            return true;
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] lValorParametros = { "0", "0", "0" };
            string lsParametros = "";

            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    lsParametros += " Codigo Capacidad : " + TxtBusCodigo.Text;
                }

                if (ddlBusTramo.SelectedValue != "0" && ddlBusTramo.SelectedValue != "")
                {
                    lValorParametros[1] = ddlBusTramo.SelectedValue;
                    lsParametros += " Tramo: " + ddlBusTramo.SelectedItem;
                }

                if (ddbBusOperador.SelectedValue != "0")
                {
                    lValorParametros[2] = ddbBusOperador.SelectedValue;
                    lsParametros += " Tramo: " + ddbBusOperador.SelectedItem;
                }

                Server.Transfer(
                    "../../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetCapacidadMax&nombreParametros=@P_codigo_capacidad*@P_codigo_tramo*@P_codigo_operador&valorParametros=" +
                    lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] +
                    "&columnas=codigo_capacidad*codigo_tramo*desc_tramo*codigo_trasportador*nombre_trasportador*capacidad_maxima_dia*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado de capacidad maxima tramo&TituloParametros=" +
                    lsParametros);
            }
            catch (Exception)
            {
                ToastrEvent?.Invoke("No se Pude Generar el Informe.!", EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: ImgPdf_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ImgPdf_Click(object sender, EventArgs e)
        {
            try
            {
                var lsCondicion = " codigo_capacidad <> '0'";
                if (ddbBusOperador.SelectedValue != "0")
                    lsCondicion += " and codigo_trasportador =" + ddbBusOperador.SelectedValue;
                Server.Execute(
                    "../../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_capacidad_max_trasp&procedimiento=pa_ValidarExistencia&columnas=codigo_capacidad*fecha_vigencia*codigo_tramo*codigo_trasportador*capacidad_maxima_dia*estado*login_usuario*fecha_hora_actual&condicion=" +
                    lsCondicion);
            }
            catch (Exception)
            {
                ToastrEvent?.Invoke("No se Pude Generar el Informe.!", EnumTypeToastr.Error);
            }

        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, int liIndiceLlave, int liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }

            lLector.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlOperador_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOperador.SelectedValue != "0")
            {
                lConexion.Abrir();
                ddlTramo.Items.Clear();
                LlenarTramo(lConexion.gObjConexion, ddlTramo);
                lConexion.Cerrar();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddbBusOperador_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddbBusOperador.SelectedValue != "0")
            {
                lConexion.Abrir();
                ddlBusTramo.Items.Clear();
                LlenarTramo(lConexion.gObjConexion, ddlBusTramo);
                lConexion.Cerrar();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros =
            {
                "@P_codigo_capacidad", "@P_fecha_vigencia", "@P_codigo_tramo", "@P_codigo_operador",
                "@P_capacidad_maxima_dia", "@P_estado", "@P_accion"
            };
            SqlDbType[] lTipoparametros =
            {
                SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar,
                SqlDbType.Int
            };
            string[] lValorParametros = { "0", "", "0", "0", "0", "", "1" };
            var lblMensaje = new StringBuilder();

            try
            {
                if (TxtFecha.Text == "")
                    lblMensaje.Append("debe seleccionar la fecha de vigencia<br>");
                else
                {
                    try
                    {
                        if (DateTime.Compare(Convert.ToDateTime(TxtFecha.Text),
                            Convert.ToDateTime(DateTime.Now.ToShortDateString())) < 0)
                            lblMensaje.Append("La fecha de vigencia no puede ser menor a la actual<br>");
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Append("La capacidad máxima digitada no es válida<br>");
                    }
                }

                if (ddlTramo.SelectedValue == "0" || ddlTramo.SelectedValue == "")
                    lblMensaje.Append("debe seleccionar el tramo<br>");
                if (ddlOperador.SelectedValue == "0")
                    lblMensaje.Append("debe seleccionar el transportador<br>");

                //if (VerificarExistencia(" fecha_vigencia='" + TxtFecha.Text + "' and codigo_tramo= " + ddlTramo.SelectedValue + " and codigo_trasportador=" + ddlOperador.SelectedValue))
                //    lblMensaje.Append(" La capacidad maxima de mediano plazo ya está definida para el tramo seleccionado<br>";
                if (TxtCantidad.Text == "")
                    lblMensaje.Append(" Debe digitar la capacidad maxima de mediano plazo para el tramo<br>");
                else
                {
                    string sOferta = TxtCantidad.Text.Replace(",", "");
                    int iPos = sOferta.IndexOf(".");
                    if (iPos > 0)
                        if (sOferta.Length - iPos > 3)
                            lblMensaje.Append("Se permiten máximo 2 decimales en la capacidad máxima<br>");
                    try
                    {
                        Convert.ToDouble(sOferta);
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append("La capacidad máxima digitada no es válida<br>");
                    }
                }

                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[1] = TxtFecha.Text;
                    lValorParametros[2] = ddlTramo.SelectedValue;
                    lValorParametros[3] = ddlOperador.SelectedValue;
                    lValorParametros[4] = TxtCantidad.Text;
                    lValorParametros[5] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetCapacMaxDia", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        ToastrEvent?.Invoke("Se presentó un Problema en la Creación de la capacidad del tramo.!", EnumTypeToastr.Error);
                        lConexion.Cerrar();
                    }
                    else
                    {
                        ToastrEvent?.Invoke("Se realizo el registro de forma exitosa.!", EnumTypeToastr.Success);
                        //Cierra el modal de Agregar
                        ModalEvent?.Invoke(CrearRegistro.ID, CrearRegistroInside.ID, EnumTypeModal.Cerrar);
                        Listar();
                        ddlTramo.SelectedValue = "0";
                        TxtCantidad.Text = "";
                        TxtFecha.Text = "";
                    }
                }
                else
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke(ex.ToString(), EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbActualiza_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros =
            {
                "@P_codigo_capacidad", "@P_fecha_vigencia", "@P_codigo_tramo", "@P_capacidad_maxima_dia", "@P_estado",
                "@P_accion"
            };
            SqlDbType[] lTipoparametros =
                {SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar, SqlDbType.Int};
            string[] lValorParametros = { "0", TxtFecha.Text, ddlTramo.SelectedValue, "0", "", "2" };
            var lblMensaje = new StringBuilder();
            try
            {
                if (TxtCantidad.Text == "")
                    lblMensaje.Append(" Debe digitar la capacidad máxima para el tramo<br>");
                else
                {
                    string sOferta = TxtCantidad.Text.Replace(",", "");
                    int iPos = sOferta.IndexOf(".");
                    if (iPos > 0)
                        if (sOferta.Length - iPos > 3)
                            lblMensaje.Append("Se permiten máximo 2 decimales en la capacidad máxima<br>");
                    try
                    {
                        Convert.ToDouble(sOferta);
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append("La capacidad máxima digitada no es válida<br>");
                    }
                }

                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[0] = LblCodigoCap.Text;
                    lValorParametros[3] = TxtCantidad.Text;
                    lValorParametros[4] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetCapacMaxDia",
                        lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Append("Se presento un Problema en la Actualizacion de la capacidad del tramo.!");
                        lConexion.Cerrar();
                    }
                    else
                    {
                        ToastrEvent?.Invoke("Se actualizo el registro de forma exitosa.!", EnumTypeToastr.Success);
                        //Cierra el modal de Agregar
                        ModalEvent?.Invoke(CrearRegistro.ID, CrearRegistroInside.ID, EnumTypeModal.Cerrar);

                        manejo_bloqueo("E", LblCodigoCap.Text);
                        Listar();
                    }
                }
                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                manejo_bloqueo("E", LblCodigoCap.Text);
                ToastrEvent?.Invoke(ex.ToString(), EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbSalir_Click1(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (LblCodigoCap.Text != "")
                manejo_bloqueo("E", LblCodigoCap.Text);
            ModalEvent?.Invoke(CrearRegistro.ID, CrearRegistroInside.ID, EnumTypeModal.Cerrar);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnConsultar_Click(object sender, EventArgs e)
        {
            Listar();
        }

        /// <summary>
        /// Metodo del Link Nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnNuevo_Click(object sender, EventArgs e)
        {
            mdlCrearRegistroLabel.InnerHtml = "Agregar";
            Nuevo();
        }

        /// <summary>
        /// Metodo del Link Listar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnListar(object sender, EventArgs e)
        {
            Listar();
        }
    }
}