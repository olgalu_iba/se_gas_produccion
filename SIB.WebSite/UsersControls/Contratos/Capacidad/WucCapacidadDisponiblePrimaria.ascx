﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucCapacidadDisponiblePrimaria.ascx.cs" Inherits="UsersControls.Contratos.Capacidad.WucCapacidadDisponiblePrimaria" %>

<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label ID="lblFuArchivo" AssociatedControlID="FuArchivo" runat="server">Archivo</asp:Label>
            <asp:FileUpload ID="FuArchivo" runat="server" CssClass="form-control" EnableTheming="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label AssociatedControlID="display" runat="server">Tiempo Transcurrido</asp:Label>
            <input type="text" name="display" id="display" size="8" value="00:00:0" class="form-control" readonly="true" clientidmode="Static" runat="server" />
        </div>
    </div>
</div>
