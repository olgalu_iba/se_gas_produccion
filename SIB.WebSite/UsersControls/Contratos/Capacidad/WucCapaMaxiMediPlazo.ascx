﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucCapaMaxiMediPlazo.ascx.cs" Inherits="UsersControls.Contratos.Capacidad.WucCapaMaxiMediPlazo" %>

<%--Contenido--%>
<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Código Capacidad Máxima" AssociatedControlID="TxtBusCodigo" runat="server" />
            <asp:TextBox ID="TxtBusCodigo" runat="server" autocomplete="off" CssClass="form-control" />
            <ajaxToolkit:FilteredTextBoxExtender ID="FTBETxtBusCodigo" runat="server" TargetControlID="TxtBusCodigo" FilterType="Custom, Numbers" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Transportador" AssociatedControlID="ddbBusOperador" runat="server" />
            <asp:DropDownList ID="ddbBusOperador" runat="server" OnSelectedIndexChanged="ddbBusOperador_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control selectpicker" data-live-search="true" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Tramo" AssociatedControlID="ddlBusTramo" runat="server" />
            <asp:DropDownList ID="ddlBusTramo" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                <asp:ListItem Value="">Seleccione</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
</div>
<%--Grilla--%>
<div class="table table-responsive">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                Width="100%" CssClass="table-bordered" PageSize="10" OnItemCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged">
                <Columns>
                    <asp:BoundColumn DataField="codigo_capacidad" HeaderText="Código capacidad" ItemStyle-HorizontalAlign="Left" />
                    <%--20180815 BUG230--%>
                    <asp:BoundColumn DataField="fecha_vigencia" HeaderText="Fec Vig" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}" />
                    <asp:BoundColumn DataField="codigo_tramo" HeaderText="Código Tramo" ItemStyle-HorizontalAlign="Left" />
                    <%--20180815 BUG230--%>
                    <asp:BoundColumn DataField="desc_tramo" HeaderText="Descripción Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    <asp:BoundColumn DataField="codigo_trasportador" HeaderText="Cód Trans" ItemStyle-HorizontalAlign="Left" />
                    <%--20180815 BUG230--%>
                    <asp:BoundColumn DataField="nombre_trasportador" HeaderText="Nombre Transportador" ItemStyle-HorizontalAlign="Left" />
                    <%--20180815 BUG230--%>
                    <asp:BoundColumn DataField="capacidad_maxima_dia" HeaderText="Capac máx día" ItemStyle-HorizontalAlign="Left" />
                    <%--20180815 BUG230--%>
                    <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left" />
                    <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualización" ItemStyle-HorizontalAlign="Left" />
                    <%--20180815 BUG230--%>
                    <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass="" Visible="False" />
                    <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar" Visible="False" />
                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                        <ItemTemplate>
                            <div class="dropdown dropdown-inline">
                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="flaticon-more-1"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                    <!--begin::Nav-->
                                    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                             <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Modificar</span>
                                                                    </asp:LinkButton>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon-delete"></i>
                                                            <span class="kt-nav__link-text">Eliminar</span>
                                                                    </asp:LinkButton>
                                                </li>
                                            </ul>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <!--end::Nav-->
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
            </asp:DataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

<%--Modals--%>
<%--Registro--%>
<div class="modal fade" id="CrearRegistro" tabindex="-1" role="dialog" aria-labelledby="mdlCrearRegistroLabel" aria-hidden="true" clientidmode="Static" runat="server">
    <div class="modal-dialog" id="CrearRegistroInside" role="document" clientidmode="Static" runat="server">
        <div class="modal-content">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="modal-header">
                        <h5 class="modal-title" id="mdlCrearRegistroLabel" runat="server">Agregar</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Código Capacidad Máxima" AssociatedControlID="TxtCodigoCap" runat="server" />
                                    <asp:TextBox ID="TxtCodigoCap" runat="server" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    <asp:Label ID="LblCodigoCap" runat="server" Visible="False" CssClass="form-control"></asp:Label>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Fecha Vigencia" AssociatedControlID="TxtFecha" runat="server" />
                                    <asp:TextBox ID="TxtFecha" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RfvTxtFecha" runat="server" ErrorMessage="Debe Ingresar la Fecha" ControlToValidate="TxtFecha" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Transportador" AssociatedControlID="ddlOperador" runat="server" />
                                    <asp:DropDownList ID="ddlOperador" runat="server" OnSelectedIndexChanged="ddlOperador_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Tramo" AssociatedControlID="ddlTramo" runat="server" />
                                    <asp:DropDownList ID="ddlTramo" runat="server" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Capacidad máxima Día Mediano Plazo" AssociatedControlID="TxtCantidad" runat="server" />
                                    <asp:TextBox ID="TxtCantidad" runat="server" CssClass="form-control" />
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FTEBTxtCantidad" runat="server" TargetControlID="TxtCantidad" FilterType="Custom, Numbers" ValidChars="."></ajaxToolkit:FilteredTextBoxExtender>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Estado" AssociatedControlID="ddlEstado" runat="server" />
                                    <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="A">Activo</asp:ListItem>
                                        <asp:ListItem Value="I">Inactivo</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="imbSalir" Text="Cancelar" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" CssClass="btn btn-secondary" runat="server" CausesValidation="false" OnClick="imbSalir_Click1" />
                        <asp:Button ID="imbCrear" runat="server" CssClass="btn btn-primary" Text="Crear" OnClick="imbCrear_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        <asp:Button ID="imbActualiza" runat="server" CssClass="btn btn-primary" Visible="false" Text="Actualizar" OnClick="imbActualiza_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
