﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace UsersControls.Contratos.Capacidad
{
    public partial class WucInfoCapaDispoPrim : UserControl
    {
        #region EventHandler

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public delegate void EventHandlerToastr(string message, EnumTypeToastr typeToastr);

        /// <summary>
        /// EventHandler para la selección de los toastr 
        /// </summary>
        public event EventHandlerToastr ToastrEvent;

        /// <summary>
        /// Delegado para la selección de los botones 
        /// </summary>
        public delegate void EventHandlerButtons(EnumBotones[] buttons, DropDownList dropDownList, string nameController, string route = null);

        /// <summary>
        /// EventHandler para la selección de los botones  
        /// </summary>
        public event EventHandlerButtons ButtonsEvent;

        #endregion EventHandler

        #region Propiedades

        /// <summary>
        /// Propiedad que tiene el nombre del controlador 
        /// </summary>
        public string NameController
        {
            get { return ViewState["NameController"] != null && !string.IsNullOrEmpty(ViewState["NameController"].ToString()) ? ViewState["NameController"].ToString() : string.Empty; }
            set { ViewState["NameController"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si el controlador ya se inicializo  
        /// </summary>
        public bool Inicializado
        {
            get { return (bool?)ViewState["Inicializado "] ?? false; }
            set { ViewState["Inicializado "] = value; }
        }

        #endregion Propiedades

        private InfoSessionVO goInfo = null;
        private static string lsTitulo = "Consulta Capacidad Disponible Primaria";
        private clConexion lConexion = null;
        private SqlDataReader lLector;
        private DataSet lds = new DataSet();
        private SqlDataAdapter lsqldata = new SqlDataAdapter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lConexion = new clConexion(goInfo);
            //Se agrega el comportamiento del botón
            //buttons.ExportarExcelOnclick += ImgExcel_Click;
            //buttons.FiltrarOnclick += imbConsultar_Click;

            if (IsPostBack) return;
        }

        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        public void InicializarFormulario()
        {
            try
            {
                /// Llenar controles del Formulario
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' And codigo_operador != 0 and tipo_operador ='T' order by codigo_operador", 0, 4);
                LlenarTramo("0");
                lConexion.Cerrar();

                if (Session["tipoPerfil"].ToString() == "N")
                {

                    ddlOperador.SelectedValue = goInfo.cod_comisionista;
                    if (ddlOperador.SelectedValue == "0")
                        ButtonsEvent?.Invoke(new[] { EnumBotones.Ninguno }, null, NameController);
                    else
                        ButtonsEvent?.Invoke(new[] { EnumBotones.Buscar }, null, NameController);
                    ddlOperador.Enabled = false;

                }
                //Se establece que el controlador ya se inicializo    
                Inicializado = true;
            }
            catch (Exception ex)
            {
                ButtonsEvent?.Invoke(new[] { EnumBotones.Ninguno }, null, NameController);
            }
        }

        /// <summary>
        /// Nombre: LlenarTramo
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarTramo(string lsPuntoIni)
        {
            ddlTramo.Items.Clear();
            string[] lsNombreParametros = { "@P_codigo_trasportador", "@P_codigo_pozo_ini" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { "0", "0" };

            lConexion.Abrir();
            SqlDataReader lLector;
            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            ddlTramo.Items.Add(lItem);

            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetTramo", lsNombreParametros, lTipoparametros, lValorParametros);
            if (lLector.HasRows)
            {
                while (lLector.Read())
                {
                    ListItem lItem1 = new ListItem();
                    lItem1.Value = lLector.GetValue(0).ToString();
                    lItem1.Text = lLector["desc_tramo"].ToString();
                    ddlTramo.Items.Add(lItem1);
                }
            }
            lLector.Close();
            lConexion.Cerrar();
        }
        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                }
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnConsultar_Click(object sender, EventArgs e)
        {
            var lblMensaje = new StringBuilder();
            int liValor;
            if (TxtAnoInicial.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtAnoInicial.Text);
                    if (ddlMesInicial.SelectedValue == "0")
                        lblMensaje.Append("Debe Seleccionar el Mes Inicial YA que ingresó el Año Inicial. <br>"); //20180126 rq107-16
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo Año Inicial. <br>"); //20180126 rq107-16
                }
            }
            if (TxtAnoFinal.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtAnoFinal.Text);
                    if (ddlMesFinal.SelectedValue == "0")
                        lblMensaje.Append("Debe Seleccionar el Mes Final YA que ingresó el Año Final. <br>"); //20180126 rq107-16
                    else
                    {
                        if (Convert.ToInt32(TxtAnoFinal.Text) < Convert.ToInt32(TxtAnoInicial.Text))
                            lblMensaje.Append("El Año Final debe Ser mayor o Igual al Año Inicial. <br>");
                        if (Convert.ToInt32(TxtAnoFinal.Text) == Convert.ToInt32(TxtAnoInicial.Text))
                        {
                            if (Convert.ToInt32(ddlMesFinal.SelectedValue) < Convert.ToInt32(ddlMesInicial.SelectedValue))
                                lblMensaje.Append("El Mes Final debe Ser mayor o Igual al Mes Inicial. <br>");
                        }
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo Año Final. <br>"); //20180126 rq107-16
                }
            }

            if (ddlOperador.SelectedValue == "0" && ddlTramo.SelectedValue == "0" && TxtAnoInicial.Text.Trim().Length <= 0 && TxtAnoFinal.Text.Trim().Length <= 0)
                lblMensaje.Append("Debe Ingresar algún Parametro de búsqueda. <br>"); //20180126 rq107-16
            if (lblMensaje.ToString() == "")
            {
                try
                {
                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    SqlCommand lComando = new SqlCommand();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandTimeout = 3600;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_GetCapacidadDispPrimaria";
                    lComando.Parameters.Add("@P_operador", SqlDbType.Int).Value = ddlOperador.SelectedValue;
                    lComando.Parameters.Add("@P_tramo", SqlDbType.Int).Value = ddlTramo.SelectedValue;
                    if (TxtAnoInicial.Text.Trim().Length > 0)
                        lComando.Parameters.Add("@P_ano_ini", SqlDbType.Int).Value = TxtAnoInicial.Text;
                    else
                        lComando.Parameters.Add("@P_ano_ini", SqlDbType.VarChar).Value = "0";
                    lComando.Parameters.Add("@P_mes_ini", SqlDbType.VarChar).Value = ddlMesInicial.SelectedValue;
                    if (TxtAnoFinal.Text.Trim().Length > 0)
                    {
                        lComando.Parameters.Add("@P_ano_fin", SqlDbType.VarChar).Value = TxtAnoFinal.Text;
                        lComando.Parameters.Add("@P_mes_fin", SqlDbType.VarChar).Value = ddlMesFinal.SelectedValue;
                    }
                    else
                    {
                        if (TxtAnoInicial.Text.Trim().Length > 0)
                        {
                            lComando.Parameters.Add("@P_ano_fin", SqlDbType.VarChar).Value = TxtAnoInicial.Text;
                            lComando.Parameters.Add("@P_mes_fin", SqlDbType.VarChar).Value = ddlMesInicial.SelectedValue;
                        }
                        else
                        {
                            lComando.Parameters.Add("@P_ano_fin", SqlDbType.VarChar).Value = "0";
                            lComando.Parameters.Add("@P_mes_fin", SqlDbType.VarChar).Value = ddlMesFinal.SelectedValue;
                        }
                    }
                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgMaestro.DataSource = lds;
                    dtgMaestro.DataBind();
                    ButtonsEvent?.Invoke(new[] { EnumBotones.Buscar, EnumBotones.Excel }, null, NameController); //20200924 ajsute compornene

                    lConexion.Cerrar();
                }
                catch (Exception ex)
                {
                    ToastrEvent?.Invoke("No se Pudo Generar el Informe.! " + ex.Message, EnumTypeToastr.Error);
                }
            }
            else
            {
                ToastrEvent?.Invoke(lblMensaje.ToString(), EnumTypeToastr.Error);
            }
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnExcel_Click(object sender, EventArgs e) //20200924 ajsute componente Olga
        {
            string lsNombreArchivo = goInfo.Usuario.ToString() + "InfPosturasVenta" + DateTime.Now + ".xls";
            string lstitulo_informe = "Informe Capacidad Disponilbe Primaria";
            string lsTituloParametros = "";
            try
            {
                if (ddlOperador.SelectedValue != "0")
                    lsTituloParametros += "  - Operador: " + ddlOperador.SelectedItem.ToString();
                if (ddlTramo.SelectedValue != "0")
                    lsTituloParametros += " Tramo: " + ddlTramo.SelectedItem.ToString();
                if (TxtAnoInicial.Text.Trim().Length > 0)
                    lsTituloParametros += " - Año y Mes Inicial: " + TxtAnoInicial.Text.Trim() + " - " + ddlMesInicial.SelectedValue;
                if (TxtAnoFinal.Text.Trim().Length > 0)
                    lsTituloParametros += " - Año y Mes Final: " + TxtAnoFinal.Text.Trim() + " - " + ddlMesFinal.SelectedValue;
                decimal ldCapacidad = 0;
                StringBuilder lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                dtgMaestro.EnableViewState = false;
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);

                lform.Controls.Add(dtgMaestro);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = System.Text.Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());

                Response.End();
                lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
            }
            catch (Exception ex)
            {
                ToastrEvent?.Invoke("No se Pudo Generar el Excel.!" + ex.Message.ToString(), EnumTypeToastr.Error);
            }
        }
    }
}