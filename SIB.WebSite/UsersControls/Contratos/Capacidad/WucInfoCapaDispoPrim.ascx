﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WucInfoCapaDispoPrim.ascx.cs" Inherits="UsersControls.Contratos.Capacidad.WucInfoCapaDispoPrim" %>

<%--Contenido--%>
<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
            <asp:DropDownList ID="ddlOperador" runat="server" Width="100%" CssClass="form-control" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Tramo" AssociatedControlID="ddlTramo" runat="server" />
            <asp:DropDownList ID="ddlTramo" runat="server" Width="100%" CssClass="form-control" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Año Inicial" AssociatedControlID="TxtAnoInicial" runat="server" />
            <asp:TextBox ID="TxtAnoInicial" runat="server" Width="100%" ValidationGroup="detalle" CssClass="form-control" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Mes Inicial" AssociatedControlID="" runat="server" />
            <asp:DropDownList ID="ddlMesInicial" runat="server" Width="100%" CssClass="form-control">
                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                <asp:ListItem Value="01" Text="Enero"></asp:ListItem>
                <asp:ListItem Value="02" Text="Febrero"></asp:ListItem>
                <asp:ListItem Value="03" Text="Marzo"></asp:ListItem>
                <asp:ListItem Value="04" Text="Abril"></asp:ListItem>
                <asp:ListItem Value="05" Text="Mayo"></asp:ListItem>
                <asp:ListItem Value="06" Text="Junio"></asp:ListItem>
                <asp:ListItem Value="07" Text="Julio"></asp:ListItem>
                <asp:ListItem Value="08" Text="Agosto"></asp:ListItem>
                <asp:ListItem Value="09" Text="Septiembre"></asp:ListItem>
                <asp:ListItem Value="10" Text="Octubre"></asp:ListItem>
                <asp:ListItem Value="11" Text="Noviembre"></asp:ListItem>
                <asp:ListItem Value="12" Text="Diciembre"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Año Final" AssociatedControlID="TxtAnoFinal" runat="server" />
            <asp:TextBox ID="TxtAnoFinal" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control" />
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <div class="form-group">
            <asp:Label Text="Mes Final" AssociatedControlID="ddlMesFinal" runat="server" />
            <asp:DropDownList ID="ddlMesFinal" runat="server" Width="100%" CssClass="form-control">
                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                <asp:ListItem Value="01" Text="Enero"></asp:ListItem>
                <asp:ListItem Value="02" Text="Febrero"></asp:ListItem>
                <asp:ListItem Value="03" Text="Marzo"></asp:ListItem>
                <asp:ListItem Value="04" Text="Abril"></asp:ListItem>
                <asp:ListItem Value="05" Text="Mayo"></asp:ListItem>
                <asp:ListItem Value="06" Text="Junio"></asp:ListItem>
                <asp:ListItem Value="07" Text="Julio"></asp:ListItem>
                <asp:ListItem Value="08" Text="Agosto"></asp:ListItem>
                <asp:ListItem Value="09" Text="Septiembre"></asp:ListItem>
                <asp:ListItem Value="10" Text="Octubre"></asp:ListItem>
                <asp:ListItem Value="11" Text="Noviembre"></asp:ListItem>
                <asp:ListItem Value="12" Text="Diciembre"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
</div>
<%--Grilla--%>
<div class="table table-responsive">
    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered">
        <Columns>
            <asp:BoundColumn DataField="transportador" HeaderText="Transportador" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="300px"></asp:BoundColumn>
            <asp:BoundColumn DataField="tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="300px"></asp:BoundColumn>
            <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px"></asp:BoundColumn>
            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="80px"></asp:BoundColumn>
            <asp:BoundColumn DataField="capacidad" HeaderText="Capacidad Disp. Primaria" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="120px"></asp:BoundColumn>
        </Columns>
        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
    </asp:DataGrid>
</div>
