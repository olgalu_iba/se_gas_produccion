﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Collections.Generic;

public partial class Verificacion_frm_modificaRegistro : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Modificación de registro de contratos";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    string[] lsCarperta;
    string Carpeta;
    string sRutaArc = ConfigurationManager.AppSettings["rutaModif"].ToString();
    string lsCarpetaAnt = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        //Titulo
        Master.Titulo = "Modificación de registro de contratos";

        if (!IsPostBack)
        {

            /// Llenar controles del Formulario 
            lConexion.Abrir();
            LlenarControles2(lConexion.gObjConexion, DdlBusVenta, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20190425 rq022-19
            LlenarControles2(lConexion.gObjConexion, DdlBusCompra, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20190425 rq022-19
            LlenarControles2(lConexion.gObjConexion, DdlBusContra, "m_operador ope", " estado = 'A' and codigo_operador !=0 and exists (select 1 from t_contrato_verificacion ver where (ope.codigo_operador= ver.operador_compra or ope.codigo_operador= ver.operador_venta) and (ver.operador_compra = " + goInfo.cod_comisionista + " or ver.operador_venta = " + goInfo.cod_comisionista + ")) and ope.codigo_operador <> " + goInfo.cod_comisionista + " order by razon_social", 0, 4); //20190425 rq022-19
            LlenarControles(lConexion.gObjConexion, DdlBusCausa, "m_causa_modificacion", "  estado ='A' and tipo_causa<> 'C' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlBusEstado, "m_estado_gas", "  tipo_estado ='M'  and sigla_estado not in ('R','L')  order by descripcion_estado", 2, 3);
            LlenarControles(lConexion.gObjConexion, DdlBusModalidad, "m_modalidad_contractual", "  estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlBusCausa1, "m_causa_modificacion", "  estado ='A' and tipo_causa <> 'C' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlBusEstado1, "m_estado_gas", "  tipo_estado ='M'  and sigla_estado not in ('R','L') order by descripcion_estado", 2, 3);
            LlenarControles(lConexion.gObjConexion, DdlIngCausa, "m_causa_modificacion", "  estado ='A' and tipo_causa not in  ('C','R') order by descripcion", 0, 1);
            LlenarControles2(lConexion.gObjConexion, DdlVendedor, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20190425 rq022-19
            LlenarControles2(lConexion.gObjConexion, DdlComprador, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20190425 rq022-19
            LlenarControles(lConexion.gObjConexion, DdlFuente, "m_pozo", " ind_campo_pto='C' and estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlPunto, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles1(lConexion.gObjConexion, DdlCentro, "m_divipola", " codigo_centro <> '0' and estado ='A' order by nombre_centro", 5, 6);
            LlenarControles(lConexion.gObjConexion, DdlModalidad, "m_modalidad_contractual", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlPeriodo, "m_periodos_entrega", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, dlTipoDemanda, "m_tipo_demanda_atender", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo sec, m_demanda_sector dem", " sec.estado ='A' and sec.codigo_sector_consumo = dem.codigo_sector_consumo  and dem.estado ='A' and dem.codigo_tipo_demanda =1 and dem.ind_uso ='T' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMercadoRel, "m_mercado_relevante", " estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlDeptoPuntoSal, "m_divipola", " codigo_ciudad = '0' and estado ='A' order by nombre_departamento", 1, 2);
            LlenarControles(lConexion.gObjConexion, ddlMercadoRelevante, "m_mercado_relevante", " estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlSectorConsumo, "m_sector_consumo sec", " sec.estado ='A' and estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlRutaMay, "m_ruta_snt", " estado = 'A' order by descripcion", 0, 4);
            lConexion.Cerrar();
            hdfOperador.Value = goInfo.cod_comisionista;

            if (goInfo.cod_comisionista == "0")
            {
                tr01.Visible = false;
                tr02.Visible = true;
                tr03.Visible = true;
            }
            else
            {
                tr01.Visible = true;
                tr02.Visible = false;
                tr03.Visible = false;
            }
        }
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: LlenarControles2
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles2(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// Nombre: LlenarControles1
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    /// 20170814 rq036-17
    protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["nombre_centro"].ToString() + "-" + lLector["nombre_ciudad"].ToString() + "-" + lLector["nombre_departamento"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        CargarDatos();
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        lblMensaje.Text = "";
        DateTime ldFecha;
        string[] lsNombreParametros = { "@P_numero_contrato", "@P_codigo_verif", "@P_contrato_definitivo", "@P_fecha_inicial", "@P_fecha_final",
                "@P_codigo_operador", "@P_codigo_contraparte", "@P_operador_compra", "@P_operador_venta","@P_tipo_mercado","@P_destino_rueda",
                "@P_codigo_modalidad","@P_codigo_causa","@P_estado_modif" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar , SqlDbType.VarChar,
                SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar};
        string[] lValorParametros = { "0", "0", "", "", "", "0", "0", "0", "0", "", "", "0", "0", "" };

        if (TxtBusFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtBusFechaIni.Text);
            }
            catch (Exception)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial de Negociación. <br>";
            }
        }
        if (TxtBusFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtBusFechaFin.Text);
            }
            catch (Exception)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Final de Negociación. <br>";
            }
        }
        if (TxtBusFechaIni.Text.Trim().Length == 0 && TxtBusFechaFin.Text.Trim().Length > 0)
            lblMensaje.Text += "Debe digitar la Fecha Inicial de Negociación antes que la final. <br>";

        if (TxtBusFechaIni.Text.Trim().Length > 0 && TxtBusFechaFin.Text.Trim().Length > 0)
            try
            {
                if (Convert.ToDateTime(TxtFechaFin.Text) < Convert.ToDateTime(TxtFechaIni.Text))
                    lblMensaje.Text += "La Fecha inicial de Negociación debe ser menor o igual que la fecha final. <br>";
            }
            catch (Exception)
            {
            }

        if (lblMensaje.Text == "")
        {
            try
            {
                if (TxtBusOperacion.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtBusOperacion.Text.Trim();
                if (TxtBusId.Text.Trim().Length > 0)
                    lValorParametros[1] = TxtBusId.Text.Trim();
                lValorParametros[2] = TxtBusContratoDef.Text.Trim();
                if (TxtBusFechaIni.Text.Trim().Length > 0)
                {
                    lValorParametros[3] = TxtBusFechaIni.Text.Trim();
                    if (TxtBusFechaFin.Text.Trim().Length > 0)
                        lValorParametros[4] = TxtBusFechaFin.Text.Trim();
                    else
                        lValorParametros[4] = TxtBusFechaIni.Text.Trim();
                }
                lValorParametros[5] = goInfo.cod_comisionista;
                lValorParametros[6] = DdlBusContra.SelectedValue;
                lValorParametros[7] = DdlBusCompra.SelectedValue;
                lValorParametros[8] = DdlBusVenta.SelectedValue;
                lValorParametros[9] = DdlBusMercado.SelectedValue;
                lValorParametros[10] = DdlBusProducto.SelectedValue;
                lValorParametros[11] = DdlBusModalidad.SelectedValue;
                if (goInfo.cod_comisionista == "0")
                {
                    lValorParametros[12] = DdlBusCausa1.SelectedValue;
                    lValorParametros[13] = DdlBusEstado1.SelectedValue;
                }
                else
                {
                    lValorParametros[12] = DdlBusCausa.SelectedValue;
                    lValorParametros[13] = DdlBusEstado.SelectedValue;
                }
                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContSolMod", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                lConexion.Cerrar();
                if (dtgConsulta.Items.Count > 0)
                {
                    tblGrilla.Visible = true;
                    lkbExcel.Visible = true;
                }
                else
                {
                    tblGrilla.Visible = false;
                    lkbExcel.Visible = false;
                    lblMensaje.Text = "No se encontraron Registros.";
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo consultar la información.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        hdfNoSolicitud.Value = "0";
        divSolicitud.Visible = false;
        hdfTipoCausa.Value = e.Item.Cells[29].Text;
        trReestructura.Visible = false;
        hdfConsec.Value = "0";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            hdfIdRegistro.Value = e.Item.Cells[4].Text;
            if (goInfo.cod_comisionista == "0")
            {
                if (e.Item.Cells[28].Text != "P")
                    lblMensaje.Text = "No está pendiente de aprobación de la BMC<br>";
            }
            else
            {
                if (e.Item.Cells[28].Text != "I" && e.Item.Cells[28].Text != "E" && e.Item.Cells[28].Text != "S" && e.Item.Cells[28].Text != "M" && e.Item.Cells[28].Text != "C")
                    lblMensaje.Text = "No se puede modificar porque ya se aprobó o se rechazó<br>";
                else
                {
                    if (e.Item.Cells[28].Text == "C" && e.Item.Cells[22].Text == "A")
                        lblMensaje.Text = "No se puede aprobar porque no se han ingresado los datos de la modificación<br>";
                    if (e.Item.Cells[28].Text == "E" && e.Item.Cells[22].Text == "A")
                        lblMensaje.Text = "No se puede aprobar porque está pendiente de corrección<br>";
                }
                if (e.Item.Cells[29].Text == "R")
                    lblMensaje.Text = "La modificación por reestructuración no se puede manejar por pantalla<br>";
                if (e.Item.Cells[30].Text == "S")
                    lblMensaje.Text = "Hay una solicitud de modificación en curso para el contrato<br>";
            }
            if (e.Item.Cells[29].Text == "C")
                lblMensaje.Text = "La operación está en proceso de modificación por cesión<br>";
            if (manejo_bloqueo("V", hdfIdRegistro.Value))
                lblMensaje.Text = "Se está modificando la operación<br>";
            if (lblMensaje.Text == "")
            {
                manejo_bloqueo("A", hdfIdRegistro.Value);
                DdlDestino.SelectedValue = e.Item.Cells[24].Text;
                DdlMercado.SelectedValue = e.Item.Cells[23].Text;
                hdfPunta.Value = e.Item.Cells[25].Text;
                hdfNoSolicitud.Value = e.Item.Cells[2].Text;
                hdfCausa.Value = e.Item.Cells[26].Text;
                hdfCodModif.Value = e.Item.Cells[27].Text;

                trae_usr_fin(hdfCodModif.Value, hdfIdRegistro.Value);
                if (e.Item.Cells[22].Text == "C")
                {
                    color_label("N"); // 20190425 rq022-19 ajuste modificacion
                    divSolicitud.Visible = true;
                }
                if (e.Item.Cells[22].Text == "M")
                {
                    color_label("N"); // 20190425 rq022-19 ajuste modificacion
                    modificar();
                }
                if (e.Item.Cells[22].Text == "A")
                {
                    color_label("R"); // 20190425 rq022-19 ajuste modificacion
                    aprobar();
                }
                if (e.Item.Cells[22].Text == "B")
                {
                    color_label("R"); // 20190425 rq022-19 ajuste modificacion
                    consultar("A");
                }
                if (hdfTipoCausa.Value == "T" || hdfTipoCausa.Value == "A")
                {
                    dtgUsuarios.Columns[8].Visible = false;
                    dtgUsuarios.Columns[9].Visible = false;
                    btnCrearUsu.Visible = false;
                }
                else
                {
                    dtgUsuarios.Columns[8].Visible = true;
                    dtgUsuarios.Columns[9].Visible = true;
                    if (tblDemanda.Visible)
                        btnCrearUsu.Visible = true;
                }
            }
        }
        if (((LinkButton)e.CommandSource).Text == "Consultar")
        {
            DdlDestino.SelectedValue = e.Item.Cells[24].Text;
            DdlMercado.SelectedValue = e.Item.Cells[23].Text;
            if (e.Item.Cells[29].Text == "R")
            {
                string[] lsNombreParametros = { "@P_codigo_solicitud" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int };
                string[] lValorParametros = { e.Item.Cells[2].Text };

                lConexion.Abrir();
                dtgReestructura.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetDatosContModRes", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgReestructura.DataBind();
                lConexion.Cerrar();
                trReestructura.Visible = true;
            }

            hdfIdRegistro.Value = e.Item.Cells[4].Text;
            color_label("R"); // 20190425 rq022-19 ajuste modificacion
            consultar("C");
            btnCrearUsu.Visible = false;
            dtgUsuarios.Columns[8].Visible = false;
            dtgUsuarios.Columns[9].Visible = false;

        }
        if (((LinkButton)e.CommandSource).Text == "Original")
        {
            try
            {
                if (e.Item.Cells[19].Text.Trim() != "&nbsp;" && e.Item.Cells[19].Text.Trim() != "")
                {
                    lsCarperta = sRutaArc.Split('\\');
                    lsCarpetaAnt = lsCarperta[lsCarperta.Length - 2];
                    string lsRuta = "../" + lsCarpetaAnt + "/" + e.Item.Cells[19].Text.Replace(@"\", "/");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + lsRuta + "');", true);
                }
                else
                    lblMensaje.Text = "No hay archivo para visualizar";

            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al mostrar el archivo. " + ex.Message.ToString();
            }
        }
        if (((LinkButton)e.CommandSource).Text == "Modificado")
        {
            try
            {
                if (e.Item.Cells[20].Text.Trim() != "&nbsp;" && e.Item.Cells[20].Text.Trim() != "")
                {
                    lsCarperta = sRutaArc.Split('\\');
                    lsCarpetaAnt = lsCarperta[lsCarperta.Length - 2];
                    string lsRuta = "../" + lsCarpetaAnt + "/" + e.Item.Cells[20].Text.Replace(@"\", "/");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + lsRuta + "');", true);
                }
                else
                    lblMensaje.Text = "No hay archivo para visualizar";

            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al mostrar el archivo. " + ex.Message.ToString();
            }
        }
    }
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void crear()
    {
        btnModificar.Visible = true;
        btnAprobar.Visible = false;
        btnRechazar.Visible = false;
        btnAprobarBmc.Visible = false;
        btnRechazarBmc.Visible = false;
        trObs.Visible = false;
        mostrar_datos("C");
    }
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void mostrar_datos(string lsAccion)
    {

        DdlComprador.SelectedValue = "0";
        TxNitComprador.Text = "";
        DdlVendedor.SelectedValue = "0";
        TxtFechaNeg.Text = "";
        TxtHoraNeg.Text = "";
        TxtFechaSus.Text = "";
        TxtContDef.Text = "";
        DdlFuente.SelectedValue = "0";
        DdlVariable.SelectedValue = "N";
        DdlConectado.SelectedValue = "S";
        DdlBoca.SelectedValue = "N";
        DdlPunto.SelectedValue = "0";
        DdlCentro.SelectedValue = "0";
        DdlModalidad.SelectedValue = "0";
        DdlPeriodo.SelectedValue = "0";
        TxtNoAños.Text = "";
        TxtFechaIni.Text = "";
        TxtHoraIni.Text = "";
        TxtFechaFin.Text = "";
        TxtHoraFin.Text = "";
        TxtCantidad.Text = "";
        TxtPrecio.Text = "";
        DdlSentido.SelectedValue = "NORMAL";
        TxtPresion.Text = "";
        TxtGarantia.Text = "";
        TxtVlrGrtia.Text = "0";
        TxtFechaGrtia.Text = "";
        ddlDeptoPuntoSal.SelectedValue = "0";
        DdlMunPuntoSal_SelectedIndexChanged(null, null);
        ddlSectorConsumo.SelectedValue = "0";
        ddlConexion.SelectedValue = "S";
        ddlMercadoRelevante.SelectedValue = "0";
        TxtCapacOtm.Text = "";
        ddlRutaMay.SelectedValue = "0";

        DdlComprador.Enabled = true;
        TxNitComprador.Enabled = true;
        DdlVendedor.Enabled = true;
        TxtFechaNeg.Enabled = true;
        TxtHoraNeg.Enabled = true;
        TxtFechaSus.Enabled = true;
        TxtContDef.Enabled = true;
        DdlFuente.Enabled = true;
        DdlVariable.Enabled = true;
        DdlConectado.Enabled = true;
        DdlBoca.Enabled = true;
        DdlPunto.Enabled = true;
        DdlCentro.Enabled = true;
        DdlModalidad.Enabled = true;
        DdlPeriodo.Enabled = true;
        TxtNoAños.Enabled = true;
        TxtFechaIni.Enabled = true;
        TxtHoraIni.Enabled = true;
        TxtFechaFin.Enabled = true;
        TxtHoraFin.Enabled = true;
        TxtCantidad.Enabled = true;
        TxtPrecio.Enabled = true;
        DdlSentido.Enabled = true;
        TxtPresion.Enabled = true;
        TxtGarantia.Enabled = true;
        TxtVlrGrtia.Enabled = true;
        TxtFechaGrtia.Enabled = true;
        ddlSectorConsumo.Enabled = true;
        ddlConexion.Enabled = true;
        ddlDeptoPuntoSal.Enabled = true;
        DdlMunPuntoSal.Enabled = true;
        ddlCentroPuntoSal.Enabled = true;
        TxtCapacOtm.Enabled = true;
        ddlRutaMay.Enabled = false;

        try
        {
            tblDatos.Visible = false;
            tblGrilla.Visible = false;
            tblModifica.Visible = true;
            lkbExcel.Visible = false;

            if (hdfPunta.Value == "C")
            {
                tblDemanda.Visible = true;
                btnCrearUsu.Visible = true;
            }
            else
            {
                tblDemanda.Visible = false;
                btnCrearUsu.Visible = false;
            }

            if (DdlMercado.SelectedValue != "O")
            {
                if (DdlDestino.SelectedValue == "G")
                {
                    DdlPunto.Items.Clear();
                    lConexion.Abrir();
                    LlenarControles(lConexion.gObjConexion, DdlPunto, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
                    lConexion.Cerrar();
                }
                else
                {
                    DdlPunto.Items.Clear();
                    lConexion.Abrir();
                    LlenarControles(lConexion.gObjConexion, DdlPunto, "m_ruta_snt", " estado = 'A' order by descripcion", 0, 4);
                    lConexion.Cerrar();
                }
            }
            else
            {
                LblPunto.Text = "Punto de Salida SNT";
                DdlPunto.Items.Clear();
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, DdlPunto, "m_punto_salida_snt", " estado = 'A'  order by descripcion", 0, 2);
                lConexion.Cerrar();
            }

            if (DdlDestino.SelectedValue == "G" && DdlMercado.SelectedValue == "P")
            {
                LblFuente.Visible = true;
                DdlFuente.Visible = true;
                trSumSec.Visible = false;
                DdlConectado.SelectedValue = "S";
                DdlBoca.SelectedValue = "N";
                DdlPunto.Visible = true;
                DdlCentro.Visible = false;
                DdlCentro.SelectedValue = "0";
                trFlujo.Visible = false;
                TxtPresion.Text = "0";
                trPeriodo.Visible = true;
                trMayor1.Visible = false;
                trMayor2.Visible = false;
                trMayor3.Visible = false;
                trMayor4.Visible = false;
                TxtVlrGrtia.Text = "0";
                TxtCapacOtm.Visible = false;
            }
            if (DdlDestino.SelectedValue == "G" && DdlMercado.SelectedValue == "S")
            {
                LblFuente.Visible = false;
                DdlFuente.Visible = false;
                DdlFuente.SelectedValue = "0";
                trSumSec.Visible = true;
                DdlConectado.SelectedValue = "S";
                DdlBoca.SelectedValue = "N";
                DdlPunto.Visible = true;
                DdlCentro.Visible = false;
                DdlCentro.SelectedValue = "0";
                trFlujo.Visible = false;
                TxtPresion.Text = "0";
                trPeriodo.Visible = true;
                trMayor1.Visible = false;
                trMayor2.Visible = false;
                trMayor3.Visible = false;
                trMayor4.Visible = false;
                TxtVlrGrtia.Text = "0";
                TxtCapacOtm.Visible = false;
            }
            if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "P")
            {
                LblFuente.Visible = false;
                DdlFuente.Visible = false;
                DdlFuente.SelectedValue = "0";
                trSumSec.Visible = false;
                DdlConectado.SelectedValue = "S";
                DdlBoca.SelectedValue = "N";
                DdlPunto.Visible = true;
                DdlCentro.Visible = false;
                DdlCentro.SelectedValue = "0";
                trFlujo.Visible = true;
                TxtPresion.Text = "";
                trPeriodo.Visible = true;
                trMayor1.Visible = false;
                trMayor2.Visible = false;
                trMayor3.Visible = false;
                trMayor4.Visible = false;
                TxtVlrGrtia.Text = "0";
                TxtCapacOtm.Visible = false;
            }
            if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "S")
            {
                LblFuente.Visible = false;
                DdlFuente.Visible = false;
                DdlFuente.SelectedValue = "0";
                trSumSec.Visible = false;
                DdlConectado.SelectedValue = "S";
                DdlBoca.SelectedValue = "N";
                DdlPunto.Visible = true;
                DdlCentro.Visible = false;
                DdlCentro.SelectedValue = "0";
                trFlujo.Visible = false;
                TxtPresion.Text = "0";
                trPeriodo.Visible = true;
                trMayor1.Visible = false;
                trMayor2.Visible = false;
                trMayor3.Visible = false;
                trMayor4.Visible = false;
                TxtVlrGrtia.Text = "0";
                TxtCapacOtm.Visible = false;
            }
            if (DdlMercado.SelectedValue == "O")
            {
                LblFuente.Visible = false;
                DdlFuente.Visible = false;
                DdlFuente.SelectedValue = "0";
                trSumSec.Visible = false;
                DdlConectado.SelectedValue = "S";
                DdlBoca.SelectedValue = "N";
                DdlPunto.Visible = true;
                DdlCentro.Visible = false;
                DdlCentro.SelectedValue = "0";
                trFlujo.Visible = false;
                TxtPresion.Text = "0";
                trPeriodo.Visible = false;
                trMayor1.Visible = true;
                trMayor2.Visible = true;
                TxtVlrGrtia.Text = "";
                tblDemanda.Visible = false;
                btnCrearUsu.Visible = false;
                if (DdlDestino.SelectedValue == "A")
                    TxtCapacOtm.Visible = true;
                else
                    TxtCapacOtm.Visible = false;
                if (DdlDestino.SelectedValue != "G")
                {
                    tdMayT01.Visible = true;
                    //tdMayT02.Visible = true;
                }
                else
                {
                    tdMayT01.Visible = false;
                    //tdMayT02.Visible = false;
                }
            }
            if (DdlDestino.SelectedValue == "G")
                LblCantidad.Text = "Cantidad de energía contratada (MBTUD)";
            else
            {
                if (DdlDestino.SelectedValue == "T")
                    LblCantidad.Text = "Capacidad de transporte contratada (KPCD)";
                else
                    LblCantidad.Text = "Cantidad y Capacidad contratada";
            }

            //habilita los campos permitidos
            string[] lsNombreParametros = { "@P_codigo_campo", "@P_codigo_causa" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { "0", hdfCausa.Value };

            try
            {
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetCampoMod", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    DdlComprador.Enabled = true;
                    TxNitComprador.Enabled = true;
                    DdlVendedor.Enabled = true;
                    TxtFechaNeg.Enabled = true;
                    TxtHoraNeg.Enabled = true;
                    TxtFechaSus.Enabled = true;
                    TxtContDef.Enabled = true;
                    DdlFuente.Enabled = true;
                    DdlVariable.Enabled = true;
                    DdlConectado.Enabled = true;
                    DdlConectado_SelectedIndexChanged(null, null);
                    DdlBoca.Enabled = true;
                    DdlBoca_SelectedIndexChanged(null, null);
                    DdlPunto.Enabled = true;
                    DdlCentro.Enabled = true;
                    DdlModalidad.Enabled = true;
                    DdlPeriodo.Enabled = true;
                    TxtNoAños.Enabled = true;
                    TxtFechaIni.Enabled = true;
                    TxtHoraIni.Enabled = true;
                    TxtFechaFin.Enabled = true;
                    TxtHoraFin.Enabled = true;
                    TxtCantidad.Enabled = true;
                    TxtCapacOtm.Enabled = true;
                    TxtPrecio.Enabled = true;
                    DdlSentido.Enabled = true;
                    TxtPresion.Enabled = true;
                    TxtGarantia.Enabled = true;
                    TxtVlrGrtia.Enabled = true;
                    TxtFechaGrtia.Enabled = true;
                    ddlSectorConsumo.Enabled = true;
                    ddlConexion.Enabled = true;
                    ddlDeptoPuntoSal.Enabled = true;
                    DdlMunPuntoSal.Enabled = true;
                    ddlCentroPuntoSal.Enabled = true;
                    ddlRutaMay.Enabled = true;

                    if (lLector["operador_compra"].ToString() == "N")
                    {
                        DdlComprador.Enabled = false;
                        TxNitComprador.Enabled = false;
                    }
                    if (lLector["operador_venta"].ToString() == "N")
                        DdlVendedor.Enabled = false;
                    if (lLector["fecha_negociacion"].ToString() == "N")
                        TxtFechaNeg.Enabled = false;
                    if (lLector["hora_negociacion"].ToString() == "N")
                        TxtHoraNeg.Enabled = false;
                    if (lLector["fecha_suscripcion"].ToString() == "N")
                        TxtFechaSus.Enabled = false;
                    if (lLector["contrato_definitivo"].ToString() == "N")
                        TxtContDef.Enabled = false;
                    if (lLector["codigo_fuente"].ToString() == "N")
                        DdlFuente.Enabled = false;
                    if (lLector["conectado_snt"].ToString() == "N")
                        DdlConectado.Enabled = false;
                    if (lLector["ent_boca_pozo"].ToString() == "N")
                        DdlBoca.Enabled = false;
                    if (lLector["punto_ruta"].ToString() == "N")
                        DdlPunto.Enabled = false;
                    if (lLector["codigo_centro"].ToString() == "N")
                        DdlCentro.Enabled = false;
                    if (lLector["codigo_modalidad"].ToString() == "N")
                        DdlModalidad.Enabled = false;
                    if (lLector["periodo_entrega"].ToString() == "N")
                        DdlPeriodo.Enabled = false;
                    if (lLector["numero_años"].ToString() == "N")
                        TxtNoAños.Enabled = false;
                    if (lLector["fecha_inicial"].ToString() == "N")
                        TxtFechaIni.Enabled = false;
                    if (lLector["hora_inicial"].ToString() == "N")
                        TxtHoraIni.Enabled = false;
                    if (lLector["fecha_final"].ToString() == "N")
                        TxtFechaFin.Enabled = false;
                    if (lLector["hora_final"].ToString() == "N")
                        TxtHoraFin.Enabled = false;
                    if (lLector["cantidad"].ToString() == "N")
                    {
                        TxtCantidad.Enabled = false;
                        TxtCapacOtm.Enabled = false;
                    }
                    if (lLector["precio"].ToString() == "N")
                        TxtPrecio.Enabled = false;
                    if (lLector["sentido_flujo"].ToString() == "N")
                        DdlSentido.Enabled = false;
                    if (lLector["presion_punto_final"].ToString() == "N")
                        TxtPresion.Enabled = false;
                    if (lLector["tipo_garantia"].ToString() == "N")
                        TxtGarantia.Enabled = false;
                    if (lLector["valor_garantia"].ToString() == "N")
                        TxtVlrGrtia.Enabled = false;
                    if (lLector["fecha_pago_garantia"].ToString() == "N")
                        TxtFechaGrtia.Enabled = false;
                    if (lLector["codigo_sector_consumo"].ToString() == "N")
                        ddlSectorConsumo.Enabled = false;
                    if (lLector["usuario_conectado"].ToString() == "N")
                        ddlConexion.Enabled = false;
                    if (lLector["codigo_departamento"].ToString() == "N")
                        ddlDeptoPuntoSal.Enabled = false;
                    if (lLector["codigo_ciudad"].ToString() == "N")
                        DdlMunPuntoSal.Enabled = false;
                    if (lLector["codigo_centro"].ToString() == "N")
                        ddlCentroPuntoSal.Enabled = false;
                    if (lLector["codigo_ruta_may"].ToString() == "N")
                        ddlRutaMay.Enabled = false;
                    if (lLector["ind_contrato_var"].ToString() == "N")
                        DdlVariable.Enabled = false;
                    if (lLector["cod_mercado_relev"].ToString() == "N")
                        ddlMercadoRelevante.Enabled = false;
                }
                else
                {
                    DdlComprador.Enabled = false;
                    TxNitComprador.Enabled = false;
                    DdlVendedor.Enabled = false;
                    TxtFechaNeg.Enabled = false;
                    TxtHoraNeg.Enabled = false;
                    TxtFechaSus.Enabled = false;
                    TxtContDef.Enabled = false;
                    DdlFuente.Enabled = false;
                    DdlVariable.Enabled = false;
                    DdlConectado.Enabled = false;
                    DdlBoca.Enabled = false;
                    DdlPunto.Enabled = false;
                    DdlCentro.Enabled = false;
                    DdlModalidad.Enabled = false;
                    DdlPeriodo.Enabled = false;
                    TxtNoAños.Enabled = false;
                    TxtFechaIni.Enabled = false;
                    TxtHoraIni.Enabled = false;
                    TxtFechaFin.Enabled = false;
                    TxtHoraFin.Enabled = false;
                    TxtCantidad.Enabled = false;
                    TxtCapacOtm.Enabled = false;
                    TxtPrecio.Enabled = false;
                    DdlSentido.Enabled = false;
                    TxtPresion.Enabled = false;
                    TxtGarantia.Enabled = false;
                    TxtVlrGrtia.Enabled = false;
                    TxtFechaGrtia.Enabled = false;
                    ddlSectorConsumo.Enabled = false;
                    ddlConexion.Enabled = false;
                    ddlDeptoPuntoSal.Enabled = false;
                    DdlMunPuntoSal.Enabled = false;
                    ddlCentroPuntoSal.Enabled = false;
                    ddlRutaMay.Enabled = false;
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al traer la información.! " + ex.Message.ToString();

            }

            //trae el detalle de contrados
            CargarDatosOper();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas en la Recuperación de la Información. " + ex.Message.ToString();
        }
        if (hdfTipoCausa.Value == "T" || hdfTipoCausa.Value == "A")
        {
            dtgUsuarios.Columns[8].Visible = false;
            dtgUsuarios.Columns[9].Visible = false;
            btnCrearUsu.Visible = false;
        }
        else
        {
            dtgUsuarios.Columns[8].Visible = true;
            dtgUsuarios.Columns[9].Visible = true;
            if (tblDemanda.Visible)
                btnCrearUsu.Visible = true;
        }
    }
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void aprueba_datos()
    {

        DdlComprador.SelectedValue = "0";
        TxNitComprador.Text = "";
        DdlVendedor.SelectedValue = "0";
        TxtFechaNeg.Text = "";
        TxtHoraNeg.Text = "";
        TxtFechaSus.Text = "";
        TxtContDef.Text = "";
        DdlFuente.SelectedValue = "0";
        DdlVariable.SelectedValue = "N";
        DdlConectado.SelectedValue = "S";
        DdlBoca.SelectedValue = "N";
        DdlPunto.SelectedValue = "0";
        DdlCentro.SelectedValue = "0";
        DdlModalidad.SelectedValue = "0";
        DdlPeriodo.SelectedValue = "0";
        TxtNoAños.Text = "";
        TxtFechaIni.Text = "";
        TxtHoraIni.Text = "";
        TxtFechaFin.Text = "";
        TxtHoraFin.Text = "";
        TxtCantidad.Text = "";
        TxtPrecio.Text = "";
        DdlSentido.SelectedValue = "NORMAL";
        TxtPresion.Text = "";
        TxtGarantia.Text = "";
        TxtVlrGrtia.Text = "0";
        TxtFechaGrtia.Text = "";
        ddlDeptoPuntoSal.SelectedValue = "0";
        DdlMunPuntoSal_SelectedIndexChanged(null, null);
        ddlSectorConsumo.SelectedValue = "0";
        ddlConexion.SelectedValue = "S";
        ddlMercadoRelevante.SelectedValue = "0";
        TxtCapacOtm.Text = "";
        ddlRutaMay.SelectedValue = "0";

        DdlComprador.Enabled = true;
        TxNitComprador.Enabled = true;
        DdlVendedor.Enabled = true;
        TxtFechaNeg.Enabled = true;
        TxtHoraNeg.Enabled = true;
        TxtFechaSus.Enabled = true;
        TxtContDef.Enabled = true;
        DdlFuente.Enabled = true;
        DdlVariable.Enabled = true;
        DdlConectado.Enabled = true;
        DdlBoca.Enabled = true;
        DdlPunto.Enabled = true;
        DdlCentro.Enabled = true;
        DdlModalidad.Enabled = true;
        DdlPeriodo.Enabled = true;
        TxtNoAños.Enabled = true;
        TxtFechaIni.Enabled = true;
        TxtHoraIni.Enabled = true;
        TxtFechaFin.Enabled = true;
        TxtHoraFin.Enabled = true;
        TxtCantidad.Enabled = true;
        TxtPrecio.Enabled = true;
        DdlSentido.Enabled = true;
        TxtPresion.Enabled = true;
        TxtGarantia.Enabled = true;
        TxtVlrGrtia.Enabled = true;
        TxtFechaGrtia.Enabled = true;
        ddlSectorConsumo.Enabled = true;
        ddlConexion.Enabled = true;
        ddlDeptoPuntoSal.Enabled = true;
        DdlMunPuntoSal.Enabled = true;
        ddlCentroPuntoSal.Enabled = true;
        TxtCapacOtm.Enabled = true;
        ddlRutaMay.Enabled = false;

        try
        {
            tblDatos.Visible = false;
            tblGrilla.Visible = false;
            tblModifica.Visible = true;
            lkbExcel.Visible = false;

            if (hdfPunta.Value == "C")
            {
                tblDemanda.Visible = true;
                btnCrearUsu.Visible = true;
            }
            else
            {
                tblDemanda.Visible = false;
                btnCrearUsu.Visible = false;
            }

            if (DdlMercado.SelectedValue != "O")
            {
                if (DdlDestino.SelectedValue == "G")
                {
                    DdlPunto.Items.Clear();
                    lConexion.Abrir();
                    LlenarControles(lConexion.gObjConexion, DdlPunto, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
                    lConexion.Cerrar();
                }
                else
                {
                    DdlPunto.Items.Clear();
                    lConexion.Abrir();
                    LlenarControles(lConexion.gObjConexion, DdlPunto, "m_ruta_snt", " estado = 'A' order by descripcion", 0, 4);
                    lConexion.Cerrar();
                }
            }
            else
            {
                LblPunto.Text = "Punto de Salida SNT";
                DdlPunto.Items.Clear();
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, DdlPunto, "m_punto_salida_snt", " estado = 'A'  order by descripcion", 0, 2);
                lConexion.Cerrar();
            }

            if (DdlDestino.SelectedValue == "G" && DdlMercado.SelectedValue == "P")
            {
                LblFuente.Visible = true;
                DdlFuente.Visible = true;
                trSumSec.Visible = false;
                DdlConectado.SelectedValue = "S";
                DdlBoca.SelectedValue = "N";
                DdlPunto.Visible = true;
                DdlCentro.Visible = false;
                DdlCentro.SelectedValue = "0";
                trFlujo.Visible = false;
                TxtPresion.Text = "0";
                trPeriodo.Visible = true;
                trMayor1.Visible = false;
                trMayor2.Visible = false;
                trMayor3.Visible = false;
                trMayor4.Visible = false;
                TxtVlrGrtia.Text = "0";
                TxtCapacOtm.Visible = false;
            }
            if (DdlDestino.SelectedValue == "G" && DdlMercado.SelectedValue == "S")
            {
                LblFuente.Visible = false;
                DdlFuente.Visible = false;
                DdlFuente.SelectedValue = "0";
                trSumSec.Visible = true;
                DdlConectado.SelectedValue = "S";
                DdlBoca.SelectedValue = "N";
                DdlPunto.Visible = true;
                DdlCentro.Visible = false;
                DdlCentro.SelectedValue = "0";
                trFlujo.Visible = false;
                TxtPresion.Text = "0";
                trPeriodo.Visible = true;
                trMayor1.Visible = false;
                trMayor2.Visible = false;
                trMayor3.Visible = false;
                trMayor4.Visible = false;
                TxtVlrGrtia.Text = "0";
                TxtCapacOtm.Visible = false;
            }
            if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "P")
            {
                LblFuente.Visible = false;
                DdlFuente.Visible = false;
                DdlFuente.SelectedValue = "0";
                trSumSec.Visible = false;
                DdlConectado.SelectedValue = "S";
                DdlBoca.SelectedValue = "N";
                DdlPunto.Visible = true;
                DdlCentro.Visible = false;
                DdlCentro.SelectedValue = "0";
                trFlujo.Visible = true;
                TxtPresion.Text = "";
                trPeriodo.Visible = true;
                trMayor1.Visible = false;
                trMayor2.Visible = false;
                trMayor3.Visible = false;
                trMayor4.Visible = false;
                TxtVlrGrtia.Text = "0";
                TxtCapacOtm.Visible = false;
            }
            if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "S")
            {
                LblFuente.Visible = false;
                DdlFuente.Visible = false;
                DdlFuente.SelectedValue = "0";
                trSumSec.Visible = false;
                DdlConectado.SelectedValue = "S";
                DdlBoca.SelectedValue = "N";
                DdlPunto.Visible = true;
                DdlCentro.Visible = false;
                DdlCentro.SelectedValue = "0";
                trFlujo.Visible = false;
                TxtPresion.Text = "0";
                trPeriodo.Visible = true;
                trMayor1.Visible = false;
                trMayor2.Visible = false;
                trMayor3.Visible = false;
                trMayor4.Visible = false;
                TxtVlrGrtia.Text = "0";
                TxtCapacOtm.Visible = false;
            }
            if (DdlMercado.SelectedValue == "O")
            {
                LblFuente.Visible = false;
                DdlFuente.Visible = false;
                DdlFuente.SelectedValue = "0";
                trSumSec.Visible = false;
                DdlConectado.SelectedValue = "S";
                DdlBoca.SelectedValue = "N";
                DdlPunto.Visible = true;
                DdlCentro.Visible = false;
                DdlCentro.SelectedValue = "0";
                trFlujo.Visible = false;
                TxtPresion.Text = "0";
                trPeriodo.Visible = false;
                trMayor1.Visible = true;
                trMayor2.Visible = true;
                TxtVlrGrtia.Text = "";
                tblDemanda.Visible = false;
                btnCrearUsu.Visible = false;
                if (DdlDestino.SelectedValue == "A")
                    TxtCapacOtm.Visible = true;
                else
                    TxtCapacOtm.Visible = false;
                if (DdlDestino.SelectedValue != "G")
                {
                    tdMayT01.Visible = true;
                    //tdMayT02.Visible = true;
                }
                else
                {
                    tdMayT01.Visible = false;
                    //tdMayT02.Visible = false;
                }
            }
            if (DdlDestino.SelectedValue == "G")
                LblCantidad.Text = "Cantidad de energía contratada (MBTUD)";
            else
            {
                if (DdlDestino.SelectedValue == "T")
                    LblCantidad.Text = "Capacidad de transporte contratada (KPCD)";
                else
                    LblCantidad.Text = "Cantidad y Capacidad contratada";
            }

            //habilita los campos permitidos
            string[] lsNombreParametros = { "@P_codigo_campo", "@P_codigo_causa" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { "0", hdfCausa.Value };

            try
            {
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetCampoMod", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    DdlComprador.Enabled = false;
                    TxNitComprador.Enabled = false;
                    DdlVendedor.Enabled = false;
                    TxtFechaNeg.Enabled = false;
                    TxtHoraNeg.Enabled = false;
                    TxtFechaSus.Enabled = false;
                    TxtContDef.Enabled = false;
                    DdlFuente.Enabled = false;
                    DdlVariable.Enabled = false;
                    DdlConectado.Enabled = false;
                    DdlConectado_SelectedIndexChanged(null, null);
                    DdlBoca.Enabled = false;
                    DdlBoca_SelectedIndexChanged(null, null);
                    DdlPunto.Enabled = false;
                    DdlCentro.Enabled = false;
                    DdlModalidad.Enabled = false;
                    DdlPeriodo.Enabled = false;
                    TxtNoAños.Enabled = false;
                    TxtFechaIni.Enabled = false;
                    TxtHoraIni.Enabled = false;
                    TxtFechaFin.Enabled = false;
                    TxtHoraFin.Enabled = false;
                    TxtCantidad.Enabled = false;
                    TxtCapacOtm.Enabled = false;
                    TxtPrecio.Enabled = false;
                    DdlSentido.Enabled = false;
                    TxtPresion.Enabled = false;
                    TxtGarantia.Enabled = false;
                    TxtVlrGrtia.Enabled = false;
                    TxtFechaGrtia.Enabled = false;
                    ddlSectorConsumo.Enabled = false;
                    ddlConexion.Enabled = false;
                    ddlDeptoPuntoSal.Enabled = false;
                    DdlMunPuntoSal.Enabled = false;
                    ddlCentroPuntoSal.Enabled = false;
                    ddlRutaMay.Enabled = false;

                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al traer la información.! " + ex.Message.ToString();

            }

            //trae el detalle de contrados
            CargarDatosOper();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas en la Recuperación de la Información. " + ex.Message.ToString();
        }
    }
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void consulta_datos()
    {

        DdlComprador.SelectedValue = "0";
        TxNitComprador.Text = "";
        DdlVendedor.SelectedValue = "0";
        TxtFechaNeg.Text = "";
        TxtHoraNeg.Text = "";
        TxtFechaSus.Text = "";
        TxtContDef.Text = "";
        DdlFuente.SelectedValue = "0";
        DdlVariable.SelectedValue = "N";
        DdlConectado.SelectedValue = "S";
        DdlBoca.SelectedValue = "N";
        DdlPunto.SelectedValue = "0";
        DdlCentro.SelectedValue = "0";
        DdlModalidad.SelectedValue = "0";
        DdlPeriodo.SelectedValue = "0";
        TxtNoAños.Text = "";
        TxtFechaIni.Text = "";
        TxtHoraIni.Text = "";
        TxtFechaFin.Text = "";
        TxtHoraFin.Text = "";
        TxtCantidad.Text = "";
        TxtPrecio.Text = "";
        DdlSentido.SelectedValue = "NORMAL";
        TxtPresion.Text = "";
        TxtGarantia.Text = "";
        TxtVlrGrtia.Text = "0";
        TxtFechaGrtia.Text = "";
        ddlDeptoPuntoSal.SelectedValue = "0";
        DdlMunPuntoSal_SelectedIndexChanged(null, null);
        ddlSectorConsumo.SelectedValue = "0";
        ddlConexion.SelectedValue = "S";
        ddlMercadoRelevante.SelectedValue = "0";
        TxtCapacOtm.Text = "";
        ddlRutaMay.SelectedValue = "0";

        DdlComprador.Enabled = true;
        TxNitComprador.Enabled = true;
        DdlVendedor.Enabled = true;
        TxtFechaNeg.Enabled = true;
        TxtHoraNeg.Enabled = true;
        TxtFechaSus.Enabled = true;
        TxtContDef.Enabled = true;
        DdlFuente.Enabled = true;
        DdlVariable.Enabled = true;
        DdlConectado.Enabled = true;
        DdlBoca.Enabled = true;
        DdlPunto.Enabled = true;
        DdlCentro.Enabled = true;
        DdlModalidad.Enabled = true;
        DdlPeriodo.Enabled = true;
        TxtNoAños.Enabled = true;
        TxtFechaIni.Enabled = true;
        TxtHoraIni.Enabled = true;
        TxtFechaFin.Enabled = true;
        TxtHoraFin.Enabled = true;
        TxtCantidad.Enabled = true;
        TxtPrecio.Enabled = true;
        DdlSentido.Enabled = true;
        TxtPresion.Enabled = true;
        TxtGarantia.Enabled = true;
        TxtVlrGrtia.Enabled = true;
        TxtFechaGrtia.Enabled = true;
        ddlSectorConsumo.Enabled = true;
        ddlConexion.Enabled = true;
        ddlDeptoPuntoSal.Enabled = true;
        DdlMunPuntoSal.Enabled = true;
        ddlCentroPuntoSal.Enabled = true;
        TxtCapacOtm.Enabled = true;
        ddlRutaMay.Enabled = false;

        try
        {
            tblDatos.Visible = false;
            tblGrilla.Visible = false;
            tblModifica.Visible = true;
            lkbExcel.Visible = false;

            if (hdfPunta.Value == "C")
            {
                tblDemanda.Visible = true;
                btnCrearUsu.Visible = true;
            }
            else
            {
                tblDemanda.Visible = false;
                btnCrearUsu.Visible = false;
            }

            if (DdlMercado.SelectedValue != "O")
            {
                if (DdlDestino.SelectedValue == "G")
                {
                    DdlPunto.Items.Clear();
                    lConexion.Abrir();
                    LlenarControles(lConexion.gObjConexion, DdlPunto, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
                    lConexion.Cerrar();
                }
                else
                {
                    DdlPunto.Items.Clear();
                    lConexion.Abrir();
                    LlenarControles(lConexion.gObjConexion, DdlPunto, "m_ruta_snt", " estado = 'A' order by descripcion", 0, 4);
                    lConexion.Cerrar();
                }
            }
            else
            {
                LblPunto.Text = "Punto de Salida SNT";
                DdlPunto.Items.Clear();
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, DdlPunto, "m_punto_salida_snt", " estado = 'A'  order by descripcion", 0, 2);
                lConexion.Cerrar();
            }

            if (DdlDestino.SelectedValue == "G" && DdlMercado.SelectedValue == "P")
            {
                LblFuente.Visible = true;
                DdlFuente.Visible = true;
                trSumSec.Visible = false;
                DdlConectado.SelectedValue = "S";
                DdlBoca.SelectedValue = "N";
                DdlPunto.Visible = true;
                DdlCentro.Visible = false;
                DdlCentro.SelectedValue = "0";
                trFlujo.Visible = false;
                TxtPresion.Text = "0";
                trPeriodo.Visible = true;
                trMayor1.Visible = false;
                trMayor2.Visible = false;
                trMayor3.Visible = false;
                trMayor4.Visible = false;
                TxtVlrGrtia.Text = "0";
                TxtCapacOtm.Visible = false;
            }
            if (DdlDestino.SelectedValue == "G" && DdlMercado.SelectedValue == "S")
            {
                LblFuente.Visible = false;
                DdlFuente.Visible = false;
                DdlFuente.SelectedValue = "0";
                trSumSec.Visible = true;
                DdlConectado.SelectedValue = "S";
                DdlBoca.SelectedValue = "N";
                DdlPunto.Visible = true;
                DdlCentro.Visible = false;
                DdlCentro.SelectedValue = "0";
                trFlujo.Visible = false;
                TxtPresion.Text = "0";
                trPeriodo.Visible = true;
                trMayor1.Visible = false;
                trMayor2.Visible = false;
                trMayor3.Visible = false;
                trMayor4.Visible = false;
                TxtVlrGrtia.Text = "0";
                TxtCapacOtm.Visible = false;
            }
            if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "P")
            {
                LblFuente.Visible = false;
                DdlFuente.Visible = false;
                DdlFuente.SelectedValue = "0";
                trSumSec.Visible = false;
                DdlConectado.SelectedValue = "S";
                DdlBoca.SelectedValue = "N";
                DdlPunto.Visible = true;
                DdlCentro.Visible = false;
                DdlCentro.SelectedValue = "0";
                trFlujo.Visible = true;
                TxtPresion.Text = "";
                trPeriodo.Visible = true;
                trMayor1.Visible = false;
                trMayor2.Visible = false;
                trMayor3.Visible = false;
                trMayor4.Visible = false;
                TxtVlrGrtia.Text = "0";
                TxtCapacOtm.Visible = false;
            }
            if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "S")
            {
                LblFuente.Visible = false;
                DdlFuente.Visible = false;
                DdlFuente.SelectedValue = "0";
                trSumSec.Visible = false;
                DdlConectado.SelectedValue = "S";
                DdlBoca.SelectedValue = "N";
                DdlPunto.Visible = true;
                DdlCentro.Visible = false;
                DdlCentro.SelectedValue = "0";
                trFlujo.Visible = false;
                TxtPresion.Text = "0";
                trPeriodo.Visible = true;
                trMayor1.Visible = false;
                trMayor2.Visible = false;
                trMayor3.Visible = false;
                trMayor4.Visible = false;
                TxtVlrGrtia.Text = "0";
                TxtCapacOtm.Visible = false;
            }
            if (DdlMercado.SelectedValue == "O")
            {
                LblFuente.Visible = false;
                DdlFuente.Visible = false;
                DdlFuente.SelectedValue = "0";
                trSumSec.Visible = false;
                DdlConectado.SelectedValue = "S";
                DdlBoca.SelectedValue = "N";
                DdlPunto.Visible = true;
                DdlCentro.Visible = false;
                DdlCentro.SelectedValue = "0";
                trFlujo.Visible = false;
                TxtPresion.Text = "0";
                trPeriodo.Visible = false;
                trMayor1.Visible = true;
                trMayor2.Visible = true;
                TxtVlrGrtia.Text = "";
                tblDemanda.Visible = false;
                btnCrearUsu.Visible = false;
                if (DdlDestino.SelectedValue == "A")
                    TxtCapacOtm.Visible = true;
                else
                    TxtCapacOtm.Visible = false;
                if (DdlDestino.SelectedValue != "G")
                {
                    tdMayT01.Visible = true;
                    //tdMayT02.Visible = true;
                }
                else
                {
                    tdMayT01.Visible = false;
                    //tdMayT02.Visible = false;
                }
            }
            if (DdlDestino.SelectedValue == "G")
                LblCantidad.Text = "Cantidad de energía contratada (MBTUD)";
            else
            {
                if (DdlDestino.SelectedValue == "T")
                    LblCantidad.Text = "Capacidad de transporte contratada (KPCD)";
                else
                    LblCantidad.Text = "Cantidad y Capacidad contratada";
            }

            DdlComprador.Enabled = false;
            TxNitComprador.Enabled = false;
            DdlVendedor.Enabled = false;
            TxtFechaNeg.Enabled = false;
            TxtHoraNeg.Enabled = false;
            TxtFechaSus.Enabled = false;
            TxtContDef.Enabled = false;
            DdlFuente.Enabled = false;
            DdlVariable.Enabled = false;
            DdlConectado.Enabled = false;
            DdlConectado_SelectedIndexChanged(null, null);
            DdlBoca.Enabled = false;
            DdlBoca_SelectedIndexChanged(null, null);
            DdlPunto.Enabled = false;
            DdlCentro.Enabled = false;
            DdlModalidad.Enabled = false;
            DdlPeriodo.Enabled = false;
            DdlPeriodo_SelectedIndexChanged(null, null);
            TxtNoAños.Enabled = false;
            TxtFechaIni.Enabled = false;
            TxtHoraIni.Enabled = false;
            TxtFechaFin.Enabled = false;
            TxtHoraFin.Enabled = false;
            TxtCantidad.Enabled = false;
            TxtCapacOtm.Enabled = false;
            TxtPrecio.Enabled = false;
            DdlSentido.Enabled = false;
            TxtPresion.Enabled = false;
            TxtGarantia.Enabled = false;
            TxtVlrGrtia.Enabled = false;
            TxtFechaGrtia.Enabled = false;
            ddlSectorConsumo.Enabled = false;
            ddlConexion.Enabled = false;
            ddlDeptoPuntoSal.Enabled = false;
            DdlMunPuntoSal.Enabled = false;
            ddlCentroPuntoSal.Enabled = false;
            ddlRutaMay.Enabled = false;
            ddlMercadoRelevante.Enabled = false; //20190425 rq022-19 ajsute modoificacion


            CargarDatosOper();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas en la Recuperación de la Información. " + ex.Message.ToString();
        }
    }

    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void modificar()
    {
        btnModificar.Visible = true;
        btnAprobar.Visible = false;
        btnRechazar.Visible = false;
        btnAprobarBmc.Visible = false;
        btnRechazarBmc.Visible = false;
        trObs.Visible = false;
        crear();
        if (tblDemanda.Visible)
            CargarDatosUsu();
    }
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void aprobar()
    {
        btnModificar.Visible = false;
        btnAprobar.Visible = true;
        btnRechazar.Visible = true;
        btnAprobarBmc.Visible = false;
        btnRechazarBmc.Visible = false;
        trObs.Visible = true;
        aprueba_datos();
        if (tblDemanda.Visible)
            CargarDatosUsu();
    }
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void consultar(string lsAccion)
    {
        if (lsAccion == "A")
        {
            btnModificar.Visible = false;
            btnAprobar.Visible = false;
            btnRechazar.Visible = false;
            btnAprobarBmc.Visible = true;
            btnRechazarBmc.Visible = true;
            trObs.Visible = true;
        }
        else
        {
            btnModificar.Visible = false;
            btnAprobar.Visible = false;
            btnRechazar.Visible = false;
            btnAprobarBmc.Visible = false;
            btnRechazarBmc.Visible = false;
            trObs.Visible = false;
        }
        consulta_datos();
        if (tblDemanda.Visible)
            CargarDatosUsu();
    }
    /// <summary>
    /// Nombre: dtgOperacion_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgOperacion_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";

        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            if (lblMensaje.Text == "")
            {
            }
        }
        if (((LinkButton)e.CommandSource).Text == "Copiar")
        {
            if (lblMensaje.Text == "")
            {
                try
                {
                    hdfCodModif.Value = e.Item.Cells[4].Text;
                    btnModificar.Text = "Crear";

                    lConexion.Abrir();
                    string[] lsNombreParametros = { "@P_codigo_verif", "@P_codigo_modif" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                    string[] lValorParametros = { hdfIdRegistro.Value, hdfCodModif.Value };

                    try
                    {
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetDatosContMod", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                        if (goInfo.mensaje_error != "")
                        {
                            lblMensaje.Text = "Se presentó un Problema en la selección de la información.! " + goInfo.mensaje_error.ToString();
                            lConexion.Cerrar();
                        }
                        else
                        {
                            if (lLector.HasRows)
                            {
                                lLector.Read();

                                DdlDestino.SelectedValue = lLector["destino_rueda"].ToString();
                                DdlMercado.SelectedValue = lLector["tipo_mercado"].ToString();
                                try
                                {
                                    DdlVendedor.SelectedValue = lLector["operador_venta"].ToString();
                                }
                                catch (Exception)
                                { }
                                try
                                {
                                    DdlComprador.SelectedValue = lLector["operador_compra"].ToString();
                                    TxNitComprador.Text = lLector["nit_usuario_cont"].ToString();
                                    if (DdlComprador.SelectedValue != "0")
                                    {
                                        DdlComprador.Visible = true;
                                        TxNitComprador.Visible = false;
                                        TxNitComprador.Text = "";
                                    }
                                    else
                                    {
                                        DdlComprador.Visible = false;
                                        TxNitComprador.Visible = true;
                                    }
                                }
                                catch (Exception)
                                {
                                }
                                TxtFechaNeg.Text = lLector["fecha_neg"].ToString();
                                TxtHoraNeg.Text = lLector["hora_neg"].ToString();
                                TxtFechaSus.Text = lLector["fecha_suscripcion"].ToString();
                                TxtContDef.Text = lLector["contrato_definitivo"].ToString();
                                DdlVariable.SelectedValue = lLector["ind_contrato_var"].ToString();
                                try
                                {
                                    DdlFuente.SelectedValue = lLector["codigo_fuente"].ToString();
                                }
                                catch (Exception)
                                {
                                }
                                DdlConectado.SelectedValue = lLector["conectado_snt"].ToString();
                                DdlBoca.SelectedValue = lLector["ent_boca_pozo"].ToString();
                                try
                                {
                                    DdlPunto.SelectedValue = lLector["codigo_punto_entrega"].ToString();
                                }
                                catch (Exception)
                                { }
                                try
                                {
                                    DdlCentro.SelectedValue = lLector["codigo_centro_poblado"].ToString();
                                }
                                catch (Exception)
                                {
                                }
                                try
                                {
                                    DdlModalidad.SelectedValue = lLector["codigo_modalidad"].ToString();
                                }
                                catch (Exception)
                                {
                                }
                                try
                                {
                                    DdlPeriodo.SelectedValue = lLector["codigo_periodo"].ToString();
                                }
                                catch (Exception)
                                { }
                                TxtNoAños.Text = lLector["no_años"].ToString();
                                TxtFechaIni.Text = lLector["fecha_ini"].ToString();
                                TxtHoraIni.Text = lLector["hora_ini"].ToString();
                                TxtFechaFin.Text = lLector["fecha_fin"].ToString();
                                TxtHoraFin.Text = lLector["hora_fin"].ToString();
                                TxtCantidad.Text = lLector["cantidad"].ToString();
                                TxtCapacOtm.Text = lLector["capacidad"].ToString();
                                TxtPrecio.Text = lLector["precio"].ToString();
                                if (lLector["sentido_flujo"].ToString().Trim() != "")
                                    DdlSentido.SelectedValue = lLector["sentido_flujo"].ToString();
                                TxtPresion.Text = lLector["presion_punto_fin"].ToString();
                                TxtGarantia.Text = lLector["tipo_garantia"].ToString();
                                TxtVlrGrtia.Text = lLector["valor_garantia"].ToString();
                                TxtFechaGrtia.Text = lLector["fecha_pago"].ToString();
                                try
                                {
                                    ddlSectorConsumo.SelectedValue = lLector["codigo_sector_consumo"].ToString();
                                }
                                catch (Exception)
                                {
                                }
                                try
                                {
                                    ddlConexion.SelectedValue = lLector["usuario_no_reg_snt"].ToString();
                                }
                                catch (Exception)
                                {
                                }
                                try
                                {
                                    ddlDeptoPuntoSal.SelectedValue = lLector["codigo_depto"].ToString();
                                }
                                catch (Exception)
                                {
                                }
                                try
                                {
                                    DdlMunPuntoSal.SelectedValue = lLector["codigo_municipio"].ToString();
                                }
                                catch (Exception)
                                {
                                }
                                try
                                {
                                    ddlCentroPuntoSal.SelectedValue = lLector["codigo_centro_poblado"].ToString();
                                }
                                catch (Exception)
                                {
                                }
                                try
                                {
                                    ddlRutaMay.SelectedValue = lLector["codigo_ruta_may"].ToString();
                                }
                                catch (Exception)
                                { }
                            }
                        }
                        lLector.Close();
                        if (hdfPunta.Value == "C" && DdlMercado.SelectedValue != "M")
                            CargarDatosUsu();
                    }
                    catch (Exception ex)
                    {
                        lLector.Close();
                        lblMensaje.Text = "Error al recuperar información." + ex.Message.ToString();
                        lConexion.Cerrar();
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = "Problemas en la Recuperación de la Información. " + ex.Message.ToString();
                }
            }
        }
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            if (lblMensaje.Text == "")
            {
            }
        }
    }
    /// <summary>
    /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnAceptar_Click(object sender, EventArgs e)
    {
        lblMensajeMod.Text = "";
        List<string> liExtension = new List<string>();
        liExtension.Add(".PDF");
        liExtension.Add(".PDFA");
        liExtension.Add(".GIF");
        liExtension.Add(".JPG");
        liExtension.Add(".PNG");
        liExtension.Add(".TIFF");
        string lsExtValidas = ".pdf, .pdfa, .gif, .jpg, .png, .tiff";
        string lsExtension;
        if (DdlIngCausa.SelectedValue == "0")
            lblMensajeMod.Text += "Debe seleccionar la causa de modificación<br>";
        if (FuArchivoOrg.FileName == "")
        {
            lblMensajeMod.Text += "Debe seleccionar el archivo del contrato original<br>";
        }
        else
        {
            lsExtension = Path.GetExtension(FuArchivoOrg.FileName).ToUpper();
            if (!liExtension.Contains(lsExtension))
                lblMensajeMod.Text += "La extensión del archivo original no es válida: " + lsExtValidas + "<br>";
            if (File.Exists(sRutaArc + FuArchivoOrg.FileName))
                lblMensajeMod.Text += "El archivo original ya está cargado en el sistema<br>";
        }
        if (FuArchivoMod.FileName == "")
        {
            lblMensajeMod.Text += "Debe seleccionar el archivo del contrato modificado<br>";
        }
        else
        {
            lsExtension = Path.GetExtension(FuArchivoMod.FileName).ToUpper();
            if (!liExtension.Contains(lsExtension))
                lblMensajeMod.Text += "La extensión del archivo modificado no es válida: " + lsExtValidas + "<br>";
            if (File.Exists(sRutaArc + FuArchivoMod.FileName))
                lblMensajeMod.Text += "El archivo modificado ya está cargado en el sistema<br>";
        }
        if (FuArchivoOrg.FileName == FuArchivoMod.FileName && FuArchivoOrg.FileName != "")
            lblMensajeMod.Text += "Los nombre de los archivos de los contratos original y modificado deben ser diferentes<br>";

        if (lblMensajeMod.Text == "")
        {
            try
            {
                hdfCausa.Value = "0";
                string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_codigo_operador", "@P_codigo_verif", "@P_contrato_definitivo", "@P_codigo_causa", "@P_contrato_original", "@P_contrato_nuevo" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
                string[] lValorParametros = { "0", hdfOperador.Value, hdfIdRegistro.Value, hdfContratoDef.Value, DdlIngCausa.SelectedValue, FuArchivoOrg.FileName, FuArchivoMod.FileName };

                lConexion.Abrir();
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetSolModCont", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensajeMod.Text = "Se presentó un Problema en la Creación de la solicitud de modificación.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    hdfCausa.Value = DdlIngCausa.SelectedValue;
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                        {
                            if (lLector["Error"].ToString() != "NO")
                                lblMensajeMod.Text = lLector["Error"].ToString();
                            else
                            {
                                hdfNoSolicitud.Value = lLector["codigo_sol"].ToString();
                                hdfTipoCausa.Value = lLector["tipo_causa"].ToString();
                                try
                                {
                                    FuArchivoOrg.SaveAs(sRutaArc + FuArchivoOrg.FileName);
                                    FuArchivoMod.SaveAs(sRutaArc + FuArchivoMod.FileName);
                                }
                                catch (Exception ex)
                                {
                                    lblMensaje.Text += "Error al cargar los archivos." + ex.Message.ToString();
                                }

                            }
                        }
                        if (lblMensajeMod.Text == "")
                        {
                            tblGrilla.Visible = true;
                            tblDatos.Visible = true;
                            divSolicitud.Visible = false;
                            crear();
                        }
                    }
                    lLector.Close();
                    lLector.Dispose();
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensajeMod.Text = ex.Message;
            }
        }
    }
    /// <summary>
    /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnCancelar_Click(object sender, EventArgs e)
    {
        manejo_bloqueo("E", hdfIdRegistro.Value);
        divSolicitud.Visible = false;
    }
    /// <summary>
    /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnModificar_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_solicitud","@P_codigo_verif","@P_codigo_modif","@P_codigo_comprador","@P_codigo_vendedor","@P_punta_modifica", "@P_contrato_definitivo",
                "@P_fecha_negociacion","@P_hora_negociacion","@P_fecha_suscripcion", "@P_conectado_snt", "@P_ent_boca_pozo", "@P_ind_contrato_var", "@P_codigo_centro", "@P_codigo_punto_ent",
                "@P_codigo_modalidad","@P_codigo_periodo_ent","@P_no_anos","@P_fecha_inicial","@P_hora_inicial","@P_fecha_final", "@P_hora_final","@P_precio","@P_cantidad","@P_capacidad","@P_sentido_flujo",
                "@P_presion_punto_fin", "@P_codigo_fuente","@P_tipo_garantia","@P_valor_garantia","@P_fecha_pago","@P_codigo_depto","@P_codigo_municipio",
                "@P_sector_consumo","@P_conexion_snt","@P_mercado_relevante", "@P_codigo_ruta_may", "@P_nit_usuario_cont"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,SqlDbType.Int,
                                        SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.Decimal,SqlDbType.Decimal,SqlDbType.Decimal,SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.Int,SqlDbType.VarChar,SqlDbType.Decimal,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,
                                        SqlDbType.Int,SqlDbType.VarChar,SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar
                                        };
        string[] lValorParametros = { "0", "0","0", "0","0", "","", "","", "","", "","", "","0", "0","0", "0","", "","", "","0", "0","0","", "","0", "","0", "","", "", "0","S","0" , "0", ""
                                    };
        lblMensaje.Text = "";
        DateTime ldFecha;
        DateTime ldFechaI = DateTime.Now;
        int liValor = 0;
        decimal ldValor;
        string[] lsPrecio;

        try
        {
            if (DdlComprador.SelectedValue == "0" && DdlMercado.SelectedValue != "O")
                lblMensaje.Text += "Debe seleccionar el Comprador.<br>";
            if (DdlVendedor.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el vendedor.<br>";
            if (DdlMercado.SelectedValue == "O" && DdlComprador.SelectedValue == "0" && TxNitComprador.Text == "")
                lblMensaje.Text += "Debe seleccionar el Comprador.<br>";
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaNeg.Text.Trim());
            }
            catch (Exception)
            {
                lblMensaje.Text += "Fecha de negociación inválida.<br>";
            }
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaSus.Text.Trim());
            }
            catch (Exception)
            {
                lblMensaje.Text += "Fecha de suscripción inválida.<br>";
            }
            if (DdlModalidad.SelectedValue == "0")
                lblMensaje.Text += " Debe seleccionar la modalidad del contrato. <br>";
            if (hdfTipoCausa.Value != "T" && hdfTipoCausa.Value != "A")
            {
                if (DdlMercado.SelectedValue != "O" && DdlPeriodo.SelectedValue == "0" && hdfTipoCausa.Value != "T" && hdfTipoCausa.Value != "A")
                    lblMensaje.Text += " Debe seleccionar el periodo de entrega. <br>";
            }
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIni.Text.Trim());
            }
            catch (Exception)
            {
                lblMensaje.Text += "Fecha de entrega inicial inválida.<br>";
            }

            if (TxtCantidad.Text.Trim().Length <= 0)
                lblMensaje.Text += " Debe Ingresar la cantidad. <br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCantidad.Text);
                }
                catch (Exception)
                {
                    lblMensaje.Text += " Valor Inválido en la cantidad/capacidad. <br>";
                }
            }
            if (TxtCapacOtm.Text.Trim().Length <= 0)
                lblMensaje.Text += " Debe Ingresar la capacidad de transporte. <br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCapacOtm.Text);
                }
                catch (Exception)
                {
                    lblMensaje.Text += " Valor Inválido en la capacidad de transporte. <br>";
                }
            }
            if (TxtPrecio.Text.Trim().Length <= 0)
                lblMensaje.Text += " Debe Ingresar el precio. <br>";
            else
            {
                try
                {
                    ldValor = Convert.ToDecimal(TxtPrecio.Text.Trim());
                    if (ldValor < 0)
                        lblMensaje.Text += "Valor Inválido en el precio. <br>";
                    else
                    {
                        lsPrecio = TxtPrecio.Text.Trim().Split('.');
                        if (lsPrecio.Length > 1)
                        {
                            if (lsPrecio[1].Trim().Length > 2)
                                lblMensaje.Text += "Valor Inválido en el precio. <br>";
                        }
                    }
                }
                catch (Exception)
                {
                    lblMensaje.Text += "Valor Inválido en el precio. <br>";
                }
            }
            if (TxtContDef.Text.Trim().Length <= 0)
                lblMensaje.Text += " Debe Ingresar el número del contrato definitivo. <br>";
            if (DdlFuente.Visible && DdlFuente.SelectedValue == "0")
                lblMensaje.Text += " Debe seleccionar la fuente. <br>";
            if (DdlMercado.SelectedValue != "O")
            {
                if (DdlDestino.SelectedValue == "G")
                {
                    if (DdlPunto.Visible && DdlPunto.SelectedValue == "0")
                        lblMensaje.Text += " Debe seleccionar el punto de entrega. <br>";
                    if (DdlCentro.Visible && DdlCentro.SelectedValue == "0")
                        lblMensaje.Text += " Debe seleccionar el centro poblado. <br>";
                }
                else
                    if (DdlPunto.SelectedValue == "0")
                    lblMensaje.Text += " Debe seleccionar el tramo o grupo de gasoductos. <br>";
            }
            else
            {
                if (DdlPunto.SelectedValue == "0")
                    lblMensaje.Text += " Debe seleccionar el punto de salida del SNT. <br>";
                if (DdlDestino.SelectedValue != "G")
                {
                    if (ddlRutaMay.SelectedValue == "0")
                        lblMensaje.Text += " Debe seleccionar la ruta de transporte. <br>";
                }
            }
            if (TxtNoAños.Visible && TxtNoAños.Text == "")
                lblMensaje.Text += " Debe digitar el número de años. <br>";
            if (TxtHoraIni.Visible && TxtHoraIni.Text == "")
                lblMensaje.Text += " Debe digitar la hora inicial. <br>";
            if (TxtFechaFin.Enabled)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaFin.Text.Trim());
                    if (ldFechaI > ldFecha)
                        lblMensaje.Text += "la fecha de entrega inicial debe ser menor o igual que la final.<br>";
                }
                catch (Exception)
                {
                    lblMensaje.Text += "Fecha de entrega final inválida.<br>";
                }
            }
            if (TxtHoraFin.Visible && TxtHoraFin.Text == "")
                lblMensaje.Text += " Debe digitar la hora final. <br>";
            if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "P")
            {
                if (TxtPresion.Text == "")
                    lblMensaje.Text += " Debe digitar la presión del punto final. <br>";
                else
                {
                    string[] lsPresion;
                    try
                    {
                        if (TxtPresion.Text.Trim().Length > 500)
                            lblMensaje.Text += "Valor Inválido en la presión del punto final. <br>";
                        else
                        {
                            lsPresion = TxtPresion.Text.Trim().Split('-');
                            foreach (string Presion in lsPresion)
                            {
                                try
                                {
                                    ldValor = Convert.ToDecimal(Presion.Trim());
                                }
                                catch (Exception)
                                {
                                    lblMensaje.Text += "Valor Inválido en la presión del punto final. <br>";
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += "Valor Inválido en la presión del punto final. <br>";
                    }

                }
            }
            calcula_fecha();

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = hdfNoSolicitud.Value;
                lValorParametros[1] = hdfIdRegistro.Value;
                lValorParametros[2] = "0";
                lValorParametros[3] = DdlComprador.SelectedValue;
                lValorParametros[4] = DdlVendedor.SelectedValue;
                lValorParametros[5] = hdfPunta.Value;
                lValorParametros[6] = TxtContDef.Text.Trim();
                lValorParametros[7] = TxtFechaNeg.Text;
                lValorParametros[8] = TxtHoraNeg.Text;
                lValorParametros[9] = TxtFechaSus.Text.Trim();
                lValorParametros[10] = DdlConectado.SelectedValue;
                lValorParametros[11] = DdlBoca.SelectedValue;
                lValorParametros[12] = DdlVariable.SelectedValue;
                if (DdlMercado.SelectedValue != "O")
                    lValorParametros[13] = DdlCentro.SelectedValue;
                else
                    lValorParametros[13] = ddlCentroPuntoSal.SelectedValue;
                lValorParametros[14] = DdlPunto.SelectedValue;
                lValorParametros[15] = DdlModalidad.SelectedValue;
                lValorParametros[16] = DdlPeriodo.SelectedValue;
                lValorParametros[17] = TxtNoAños.Text.Trim();
                lValorParametros[18] = TxtFechaIni.Text.Trim();
                if (TxtHoraIni.Visible)
                    lValorParametros[19] = TxtHoraIni.Text.Trim();
                else
                    lValorParametros[19] = "";
                lValorParametros[20] = TxtFechaFin.Text.Trim();
                if (TxtHoraFin.Visible)
                    lValorParametros[21] = TxtHoraFin.Text.Trim();
                else
                    lValorParametros[21] = "";
                lValorParametros[22] = TxtPrecio.Text.Trim();
                lValorParametros[23] = TxtCantidad.Text.Trim();
                lValorParametros[24] = TxtCapacOtm.Text.Trim();
                if (DdlDestino.SelectedValue == "T" && DdlMercado.SelectedValue == "P")
                    lValorParametros[25] = DdlSentido.SelectedValue;
                else
                    lValorParametros[25] = "";
                lValorParametros[26] = TxtPresion.Text.Trim();
                lValorParametros[27] = DdlFuente.SelectedValue;
                lValorParametros[28] = TxtGarantia.Text;
                lValorParametros[29] = TxtVlrGrtia.Text;
                lValorParametros[30] = TxtFechaGrtia.Text;
                lValorParametros[31] = ddlDeptoPuntoSal.SelectedValue;
                lValorParametros[32] = DdlMunPuntoSal.SelectedValue;
                lValorParametros[33] = ddlSectorConsumo.SelectedValue;
                lValorParametros[34] = ddlConexion.SelectedValue;
                lValorParametros[35] = ddlMercadoRelevante.SelectedValue;
                lValorParametros[36] = ddlRutaMay.SelectedValue;
                lValorParametros[37] = TxNitComprador.Text;

                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModContReg", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                    {
                        if (lLector["ind_error"].ToString() == "S")
                            lblMensaje.Text += lLector["error"].ToString() + "<br>";
                        else
                            hdfCodModif.Value = lLector["CodModif"].ToString();
                    }
                }
                lLector.Close();
                lConexion.Cerrar();

                if (lblMensaje.Text == "")
                {
                    CargarDatosOper();
                    lblMensaje.Text = "Operación modificada correctamente";
                }

            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAprobar_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_codigo_verif", "@P_ingresa_usr_fin", "@P_observaciones", "@P_estado" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "0", "N", "", "A" };
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;

        if (TxtObs.Text == "")
            lblMensaje.Text = "Debe ingresar las observaciones para hacer la aprobación";
        try
        {

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = hdfNoSolicitud.Value;
                lValorParametros[1] = hdfIdRegistro.Value;
                if (DdlMercado.SelectedValue != "O")
                    lValorParametros[2] = "S";
                lValorParametros[3] = TxtObs.Text;

                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModContRegAprobCont", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Text += lLector["error"].ToString() + "<br>";
                }
                lLector.Close();
                lConexion.Cerrar();

                if (lblMensaje.Text == "")
                {
                    manejo_bloqueo("E", hdfIdRegistro.Value);
                    CargarDatosOper();
                    lblMensaje.Text = "Modificación aprobada correctamente";
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRechazar_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_codigo_verif", "@P_ingresa_usr_fin", "@P_observaciones", "@P_estado" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "0", "N", "", "R" };
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;

        if (TxtObs.Text == "")
            lblMensaje.Text = "Debe ingresar las observaciones para hacer el rechazo";
        try
        {
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = hdfNoSolicitud.Value;
                lValorParametros[1] = hdfIdRegistro.Value;
                if (DdlMercado.SelectedValue != "O")
                    lValorParametros[2] = "S";
                lValorParametros[3] = TxtObs.Text;

                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModContRegAprobCont", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Text += lLector["error"].ToString() + "<br>";
                }
                lLector.Close();
                lConexion.Cerrar();

                if (lblMensaje.Text == "")
                {
                    manejo_bloqueo("E", hdfIdRegistro.Value);
                    CargarDatosOper();
                    lblMensaje.Text = "Modificación rechazada correctamente";
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAprobarBmc_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_solicitud","@P_codigo_verif","@P_observaciones", "@P_estado"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar
                                        };
        string[] lValorParametros = { "0","0", "", "A"
                                    };
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;

        if (TxtObs.Text == "")
            lblMensaje.Text = "Debe ingresar las observaciones para hacer la aprobación";
        try
        {

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = hdfNoSolicitud.Value;
                lValorParametros[1] = hdfIdRegistro.Value;
                lValorParametros[2] = TxtObs.Text;
                lConexion.Abrir();
                SqlDataReader lLector;

                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModContRegAprobBmc", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Text += lLector["error"].ToString() + "<br>";
                }
                lLector.Close();
                lConexion.Cerrar();

                if (lblMensaje.Text == "")
                {
                    manejo_bloqueo("E", hdfIdRegistro.Value);
                    CargarDatosOper();
                    lblMensaje.Text = "Modificación aprobada correctamente";
                }

            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Error al aprobar la solicitud. " + ex.Message;
        }
    }
    /// <summary>
    /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRechazarBmc_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_solicitud","@P_codigo_verif","@P_observaciones", "@P_estado"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar
                                        };
        string[] lValorParametros = { "0","0", "", "R"
                                    };
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;

        if (TxtObs.Text == "")
            lblMensaje.Text = "Debe ingresar las observaciones para hacer el rechazo";
        try
        {

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = hdfNoSolicitud.Value;
                lValorParametros[1] = hdfIdRegistro.Value;
                lValorParametros[2] = TxtObs.Text;
                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModContRegAprobBmc", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Text += lLector["error"].ToString() + "<br>";
                }
                lLector.Close();
                lConexion.Cerrar();

                if (lblMensaje.Text == "")
                {
                    manejo_bloqueo("E", hdfIdRegistro.Value);
                    CargarDatosOper();
                    lblMensaje.Text = "Modificación rechazada correctamente";
                }

            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Error al rechazar la solicitud. " + ex.Message;
        }
    }
    /// <summary>
    /// Nombre: btnRegresar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    protected void btnRegresar_Click(object sender, EventArgs e)
    {
        manejo_bloqueo("E", hdfIdRegistro.Value);
        tblDatos.Visible = true;
        tblGrilla.Visible = true;
        tblModifica.Visible = false;
        tblDemanda.Visible = false;
        lblMensaje.Text = "";
        CargarDatos();
    }
    /// <summary>
    /// Nombre: CargarDatosOper
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatosOper()
    {
        lConexion.Abrir();
        string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_consecutivo" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { hdfIdRegistro.Value, hdfConsec.Value };
        try
        {
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetDatosContMod", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
            if (goInfo.mensaje_error != "")
            {
                lblMensaje.Text = "Se presentó un Problema en la selección de la información.! " + goInfo.mensaje_error.ToString();
                lConexion.Cerrar();
            }
            else
            {
                if (lLector.HasRows)
                {
                    lLector.Read();

                    DdlDestino.SelectedValue = lLector["destino_rueda"].ToString();
                    DdlMercado.SelectedValue = lLector["tipo_mercado"].ToString();
                    try
                    {
                        DdlVendedor.SelectedValue = lLector["operador_venta"].ToString();
                    }
                    catch (Exception)
                    {
                    }
                    try
                    {
                        DdlComprador.SelectedValue = lLector["operador_compra"].ToString();
                        TxNitComprador.Text = lLector["nit_usuario_cont"].ToString();
                        if (DdlComprador.SelectedValue != "0")
                        {
                            DdlComprador.Visible = true;
                            TxNitComprador.Visible = false;
                            TxNitComprador.Text = "0";
                        }
                        else
                        {
                            DdlComprador.Visible = false;
                            TxNitComprador.Visible = true;
                        }
                    }
                    catch (Exception)
                    {
                    }
                    TxtFechaNeg.Text = lLector["fecha_neg"].ToString();
                    TxtHoraNeg.Text = lLector["hora_neg"].ToString();
                    TxtFechaSus.Text = lLector["fecha_suscripcion"].ToString();
                    TxtContDef.Text = lLector["contrato_definitivo"].ToString();
                    DdlVariable.SelectedValue = lLector["ind_contrato_var"].ToString();
                    try
                    {
                        DdlFuente.SelectedValue = lLector["codigo_fuente"].ToString();
                    }
                    catch (Exception)
                    { }
                    DdlConectado.SelectedValue = lLector["conectado_snt"].ToString();
                    DdlBoca.SelectedValue = lLector["ent_boca_pozo"].ToString();
                    try
                    {
                        DdlPunto.SelectedValue = lLector["codigo_punto_entrega"].ToString();
                    }
                    catch (Exception)
                    { }
                    try
                    {
                        DdlCentro.SelectedValue = lLector["codigo_centro_poblado"].ToString();
                    }
                    catch (Exception)
                    {
                    }
                    try
                    {
                        DdlModalidad.SelectedValue = lLector["codigo_modalidad"].ToString();
                    }
                    catch (Exception)
                    {
                    }
                    try
                    {
                        DdlPeriodo.SelectedValue = lLector["codigo_periodo"].ToString();
                    }
                    catch (Exception)
                    { }
                    TxtNoAños.Text = lLector["no_años"].ToString();
                    DdlPeriodo_SelectedIndexChanged(null, null);
                    TxtFechaIni.Text = lLector["fecha_ini"].ToString();
                    TxtHoraIni.Text = lLector["hora_ini"].ToString();
                    TxtFechaFin.Text = lLector["fecha_fin"].ToString();
                    TxtHoraFin.Text = lLector["hora_fin"].ToString();
                    TxtCantidad.Text = lLector["cantidad"].ToString();
                    TxtCapacOtm.Text = lLector["capacidad"].ToString();
                    TxtPrecio.Text = lLector["precio"].ToString();
                    if (lLector["sentido_flujo"].ToString().Trim() != "")
                        DdlSentido.SelectedValue = lLector["sentido_flujo"].ToString();
                    TxtPresion.Text = lLector["presion_punto_fin"].ToString();
                    TxtGarantia.Text = lLector["tipo_garantia"].ToString();
                    TxtVlrGrtia.Text = lLector["valor_garantia"].ToString();
                    TxtFechaGrtia.Text = lLector["fecha_pago"].ToString();
                    try
                    {
                        ddlDeptoPuntoSal.SelectedValue = lLector["codigo_depto"].ToString();
                        ddlDeptoPuntoSal_SelectedIndexChanged(null, null);
                    }
                    catch (Exception)
                    {
                    }
                    try
                    {
                        DdlMunPuntoSal.SelectedValue = lLector["codigo_municipio"].ToString();
                        DdlMunPuntoSal_SelectedIndexChanged(null, null);
                    }
                    catch (Exception)
                    {
                    }
                    try
                    {
                        ddlCentroPuntoSal.SelectedValue = lLector["codigo_centro_poblado"].ToString();
                    }
                    catch (Exception)
                    {
                    }
                    try
                    {
                        ddlConexion.SelectedValue = lLector["conexion_snt"].ToString();
                        ddlConexion_SelectedIndexChanged(null, null);
                    }
                    catch (Exception)
                    {
                    }
                    try
                    {
                        ddlSectorConsumo.SelectedValue = lLector["sector_consumo"].ToString();
                    }
                    catch (Exception)
                    {
                    }
                    try
                    {
                        ddlMercadoRelevante.SelectedValue = lLector["mercado_relevante"].ToString();
                    }
                    catch (Exception)
                    {
                    }
                    try
                    {
                        ddlRutaMay.SelectedValue = lLector["codigo_ruta_may"].ToString();
                    }
                    catch (Exception)
                    {
                    }

                    if (hdfPunta.Value == "C" && DdlMercado.SelectedValue != "M")
                        CargarDatosUsu();
                }
            }
            lLector.Close();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Error al recuperar información." + ex.Message.ToString();
            lConexion.Cerrar();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20170814 rq036-17
    protected void DdlConectado_SelectedIndexChanged(object sender, EventArgs e)
    {
        valida_conec_boca();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20170814 rq036-17
    protected void DdlBoca_SelectedIndexChanged(object sender, EventArgs e)
    {
        valida_conec_boca();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20170814 rq036-17
    protected void DdlPeriodo_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DdlMercado.SelectedValue != "O" && hdfTipoCausa.Value != "T") //20181206 ajuste  //20190109 ajuste
        {
            TxtHoraIni.Text = "";
            TxtHoraIni.Visible = false;
            TxtHoraFin.Text = "";
            TxtHoraFin.Visible = false;
            TxtFechaFin.Text = "";
            TxtFechaFin.Enabled = false;
            LblNoAños.Visible = false;
            TxtNoAños.Visible = false;
            SqlDataReader lLector;
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_periodos_entrega", "codigo_periodo=" + DdlPeriodo.SelectedValue);
            if (lLector.HasRows)
            {
                lLector.Read();
                HndTiempo.Value = lLector["medida_tiempo"].ToString();
                hdfMesInicialPeriodo.Value = lLector["mes_inicial_periodo"].ToString();
                if (HndTiempo.Value == "U")
                {
                    TxtFechaFin.Enabled = true;
                }
                if (HndTiempo.Value == "I")
                {
                    TxtHoraIni.Visible = true;
                    TxtHoraFin.Visible = true;
                }
                if (HndTiempo.Value == "L")
                {
                    LblNoAños.Visible = true;
                    TxtNoAños.Visible = true;
                }
                else
                    TxtNoAños.Text = "0";
            }
            else
            {
                HndTiempo.Value = "";
                hdfMesInicialPeriodo.Value = "0";
            }
            lLector.Close();
            lConexion.Cerrar();
            lblMensaje.Text = ""; //20190401 ajuste
            calcula_fecha();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20170814 rq036-17
    protected void valida_conec_boca()
    {
        if (DdlMercado.SelectedValue == "S" && DdlDestino.SelectedValue == "G")
        {
            if (DdlConectado.SelectedValue == "S")
            {
                lConexion.Abrir();
                DdlPunto.Items.Clear();
                LlenarControles(lConexion.gObjConexion, DdlPunto, "m_pozo", " ind_estandar='S' and  estado = 'A'  order by descripcion", 0, 1);
                lConexion.Cerrar();
                DdlCentro.Visible = false;
                DdlCentro.SelectedValue = "0";
                DdlBoca.SelectedValue = "N";
                DdlPunto.Visible = true;
                LblPunto.Text = "Punto de entrega de la energía al comprador";
            }
            else
            {
                if (DdlBoca.SelectedValue == "S")
                {
                    lConexion.Abrir();
                    DdlPunto.Items.Clear();
                    LlenarControles(lConexion.gObjConexion, DdlPunto, "m_pozo", " codigo_tipo_campo = 6 and  estado = 'A'  order by descripcion", 0, 1);
                    lConexion.Cerrar();
                    LblPunto.Text = "Punto de entrega de la energía al comprador";
                    DdlPunto.Visible = true;
                    DdlCentro.Visible = false;
                    DdlCentro.SelectedValue = "0";

                }
                else
                {
                    LblPunto.Text = "Centro Poblado";
                    DdlCentro.Visible = true;
                    DdlCentro.SelectedValue = "0";
                    DdlPunto.SelectedValue = "0";
                    DdlPunto.Visible = false;
                }
            }
        }
    }
    protected void TxtFechaIni_TextChanged(object sender, EventArgs e)
    {
        calcula_fecha();
    }
    protected void calcula_fecha()
    {
        DateTime ldFecha;
        string lsDisSemana = "0";
        string lsFecha = "";
        string lsFecha1 = "";
        int liAnos = 0;
        int liMes2Tr = 0;
        int liMes3Tr = 0;
        int liMes4Tr = 0;
        hdfErrorFecha.Value = "";
        //lblMensaje.Text = ""; 20190401 ajuste
        if (TxtFechaIni.Text.Trim().Length > 0 && DdlPeriodo.SelectedValue != "0")
        {
            try
            {

                ldFecha = Convert.ToDateTime(TxtFechaIni.Text.Trim());
                if (HndTiempo.Value != "I")
                {
                    //// Valculo Fecha Final cuando es Intradiario
                    if (HndTiempo.Value == "D")
                        TxtFechaFin.Text = TxtFechaIni.Text.Trim();
                    if (HndTiempo.Value == "S")
                    {
                        lsDisSemana = Convert.ToDateTime(TxtFechaIni.Text.Trim()).DayOfWeek.ToString();
                        if (lsDisSemana != "Monday")
                            hdfErrorFecha.Value = "La Fecha de Inicio debe ser el primer día de la semana, ya que escogió periodo semanal";
                        else
                        {
                            lsFecha = Convert.ToDateTime(TxtFechaIni.Text.Trim()).AddDays(6).ToString();
                        }
                    }
                    if (HndTiempo.Value == "M")
                    {
                        if (Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(8, 2)) != 1)
                            hdfErrorFecha.Value = "La Fecha de Inicio debe ser el primer día del mes, ya que escogió periodo mensual";
                        else
                        {
                            lsFecha = Convert.ToDateTime(TxtFechaIni.Text.Trim()).AddMonths(1).AddDays(-1).ToString();
                        }
                    }
                    if (HndTiempo.Value == "T")
                    {
                        lsFecha1 = DateTime.Now.Year.ToString() + "/" + hdfMesInicialPeriodo.Value + "/01";
                        liMes2Tr = Convert.ToDateTime(lsFecha1).AddMonths(3).Month;
                        liMes3Tr = Convert.ToDateTime(lsFecha1).AddMonths(6).Month;
                        liMes4Tr = Convert.ToDateTime(lsFecha1).AddMonths(9).Month;
                        if (Convert.ToInt32(hdfMesInicialPeriodo.Value) != Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(5, 2)) && Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(5, 2)) != liMes2Tr && Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(5, 2)) != liMes3Tr && Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(5, 2)) != liMes4Tr)
                            hdfErrorFecha.Value = "El Mes de la Fecha de Inicio es diferente al mes parametrizado de inicio {" + hdfMesInicialPeriodo.Value + "-" + liMes2Tr.ToString() + "-" + liMes3Tr.ToString() + "-" + liMes4Tr.ToString() + "}, ya que escogió periodo trimestral.";
                        else
                        {
                            if (Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(8, 2)) != 1)
                                hdfErrorFecha.Value = "La Fecha de Inicio debe ser el primer día del mes, ya que escogió periodo trimestral.";
                            else
                            {
                                lsFecha = Convert.ToDateTime(TxtFechaIni.Text.Trim()).AddMonths(3).AddDays(-1).ToString();
                            }

                        }
                    }
                    if (HndTiempo.Value == "A")
                    {
                        if (Convert.ToInt32(hdfMesInicialPeriodo.Value) != Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(5, 2)))
                            hdfErrorFecha.Value = "El Mes de la Fecha de inicio es diferente al mes parametrizado de inicio {" + hdfMesInicialPeriodo.Value + "}, ya que escogió periodo anual.";
                        else
                        {
                            if (Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(8, 2)) != 1)
                                hdfErrorFecha.Value = "La Fecha de inicio debe ser el primer día del mes, ya que escogió periodo anual.";
                            else
                            {
                                lsFecha = Convert.ToDateTime(TxtFechaIni.Text.Trim()).AddMonths(12).AddDays(-1).ToString();
                            }

                        }
                    }
                    if (HndTiempo.Value == "L")
                    {
                        if (TxtNoAños.Text.Trim().Length <= 0)
                            hdfErrorFecha.Value = "Debe ingresar el número de años, ya que escogió periodo multiAnual.";
                        if (TxtNoAños.Text.Trim().Length > 0)
                        {
                            try
                            {
                                liAnos = Convert.ToInt32(TxtNoAños.Text.Trim());
                                if (Convert.ToInt32(hdfMesInicialPeriodo.Value) != Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(5, 2)))
                                    hdfErrorFecha.Value = "El mes de la fecha de inicio es diferente al mes parametrizado de inicio {" + hdfMesInicialPeriodo.Value + "}, ya que escogió periodo multi-anual.";
                                else
                                {
                                    if (Convert.ToInt32(TxtFechaIni.Text.Trim().Substring(8, 2)) != 1)
                                        hdfErrorFecha.Value = "La Fecha de inicio debe ser el primer día del mes, ya que escogió periodo multi-anual.";
                                    else
                                    {
                                        lsFecha = Convert.ToDateTime(TxtFechaIni.Text.Trim()).AddYears(Convert.ToInt32(TxtNoAños.Text.Trim())).AddDays(-1).ToString();
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                hdfErrorFecha.Value = "Valor inválido en el campo de número de años del periodo multi-anual.";
                            }
                        }
                    }
                }
                else
                {
                    TxtFechaFin.Text = TxtFechaIni.Text.Trim();
                }
                //else
                //{
                //    TxtFechaFin.Text = TxtFechaIni.Text.Trim();
                //    if (ldFecha < Convert.ToDateTime(TxtFechaNeg.Text))
                //        hdfErrorFecha.Value += "La Fecha de Inicio debe ser mayor o igual que la fecha de negociación.<br>";
                //    if (TxtHoraIni.Text != "")
                //        if (Convert.ToDateTime(TxtFechaIni.Text + ' ' + TxtHoraIni.Text) <= Convert.ToDateTime(TxtFechaNeg.Text + ' ' + TxtHoraNeg.Text))
                //            hdfErrorFecha.Value += "La Fecha-hora de Inicio debe ser mayor que la fecha-hora de negociación.<br>";
                //}

                if (hdfErrorFecha.Value != "")
                    lblMensaje.Text += hdfErrorFecha.Value; //20190401 ajustes
                else
                    if (lsFecha != "")
                    TxtFechaFin.Text = lsFecha.Substring(6, 4) + "/" + lsFecha.Substring(3, 2) + "/" + lsFecha.Substring(0, 2);

            }
            catch (Exception)
            {
                lblMensaje.Text = "Valor Inválido en la fecha inicial";
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlDeptoPuntoSal_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlDeptoPuntoSal.SelectedValue != "0")  //20170530 divipola
        //{  //20170530 divipola
        DdlMunPuntoSal.Items.Clear();
        lConexion.Abrir();
        LlenarControles(lConexion.gObjConexion, DdlMunPuntoSal, "m_divipola", " estado = 'A'  And codigo_ciudad != '0' And codigo_departamento = '" + ddlDeptoPuntoSal.SelectedValue + "' and codigo_centro ='0' order by nombre_ciudad", 3, 4); //20170530 divipola
        lConexion.Cerrar();
        DdlMunPuntoSal_SelectedIndexChanged(null, null); //20170530 divipola
                                                         //}  //20170530 divipola
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20170530 divipola
    protected void DdlMunPuntoSal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlCentroPuntoSal.Items.Clear();
        lConexion.Abrir();
        LlenarControles(lConexion.gObjConexion, ddlCentroPuntoSal, "m_divipola", " estado = 'A' And codigo_ciudad = '" + DdlMunPuntoSal.SelectedValue + "' and codigo_centro <> '0' order by nombre_centro", 5, 6);
        lConexion.Cerrar();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCrearUsu_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_cont_usr", "@P_codigo_modif","@P_codigo_verif", "@P_no_identificacion_usr", "@P_codigo_tipo_doc",
                                        "@P_nombre_usuario", "@P_codigo_sector_consumo", "@P_codigo_punto_salida","@P_cantidad_contratada","@P_tipo_demanda",
                                        "@P_codigo_mercado_relevante","@P_equivalente_kpcd", "@P_accion","@P_destino_rueda", "@P_codigo_solicitud"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,
                                         SqlDbType.VarChar, SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,
                                         SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int
                                      };
        string[] lValorParametros = { "0", "0", "0", "", "", "", "0", " 0", "0", "0", "0","0","1","","0"
                                    };

        lblMensaje.Text = "";
        string[] lsUsuario;
        string lsNombre = "";
        string lsTipoDoc = "";
        int liValor = 0;
        try
        {
            lConexion.Abrir();
            lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
            if (dlTipoDemanda.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar el Tipo de Demanda a Atender. <br> ";
            else
            {
                if (dlTipoDemanda.SelectedValue == "2")
                {
                    if (TxtUsuarioFinal.Text.Trim().Length <= 0)
                        lblMensaje.Text = "Debe Ingresar el Usuario Final <br>";
                    else
                    {
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                        if (!lLector.HasRows)
                            lblMensaje.Text += "El Usuario Ingresado NO existe en la Base de Datos.! <br>";
                        else
                        {
                            lLector.Read();
                            lsNombre = lLector["nombre"].ToString();
                            lsTipoDoc = lLector["codigo_tipo_doc"].ToString();
                        }
                        lLector.Close();
                        lLector.Dispose();
                    }
                }
                else
                {
                    if (ddlMercadoRel.SelectedValue == "0")
                        lblMensaje.Text += "Debe Seleccionar el mercado relevante. <br> ";
                }
            }
            if (ddlSector.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar " + lblSector.Text + ". <br> ";
            if (ddlPuntoSalida.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar  Punto de Salida en SNT. <br> ";
            if (TxtCantidadUsu.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar " + lblCantContra.Text + ". <br> ";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCantidadUsu.Text.Trim());
                    if (liValor < 1)
                        lblMensaje.Text += "Valor Inválido " + lblCantContra.Text + ". <br> ";
                    else
                    {
                        if (liValor > Convert.ToDecimal(TxtCantidad.Text))
                            lblMensaje.Text += "La " + lblCantContra.Text + " No puede ser Mayor que " + TxtCantidad.Text + ". <br> ";
                        else
                        {
                            if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(TxtCantidad.Text))
                                lblMensaje.Text += "La cantidad Acumulada de Usuarios No puede ser Mayor que " + TxtCantidad.Text + ". <br> ";
                        }
                    }
                }
                catch (Exception)
                {
                    lblMensaje.Text += "Valor Inválido " + lblCantContra.Text + ". <br> ";
                }
            }
            if (TxtEquivaleKpcd.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar Equivalente Kpcd. <br> ";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtEquivaleKpcd.Text.Trim());
                    if (liValor < 0)
                        lblMensaje.Text += "Valor Inválido Equivalente Kpcd. <br> ";
                }
                catch (Exception)
                {
                    lblMensaje.Text += "Valor Inválido " + lblCantContra.Text + ". <br> ";
                }
            }
            if (lblMensaje.Text == "")
            {

                lValorParametros[0] = "0";
                if (hdfCodModif.Value != "")
                    lValorParametros[1] = hdfCodModif.Value;
                if (hdfIdRegistro.Value != "")
                    lValorParametros[2] = hdfIdRegistro.Value;
                if (hdfTipoDemanda.Value == "2")
                {
                    lValorParametros[3] = lsUsuario[0].Trim();
                    lValorParametros[4] = lsTipoDoc;
                    lValorParametros[5] = lsNombre;
                }
                lValorParametros[6] = ddlSector.SelectedValue;
                lValorParametros[7] = ddlPuntoSalida.SelectedValue;
                lValorParametros[8] = TxtCantidadUsu.Text.Trim();
                lValorParametros[9] = dlTipoDemanda.SelectedValue;
                lValorParametros[10] = ddlMercadoRel.SelectedValue;
                lValorParametros[11] = TxtEquivaleKpcd.Text.Trim();
                lValorParametros[13] = DdlDestino.SelectedValue;
                lValorParametros[14] = hdfNoSolicitud.Value;

                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetModContRegUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación de la Información del Usuario.!" + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(TxtCantidadUsu.Text.Trim())).ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Información de Usuarios Finales Ingresada Correctamente.!" + "');", true);
                    TxtUsuarioFinal.Text = "";
                    ddlSector.SelectedValue = "0";
                    ddlPuntoSalida.SelectedValue = "0";
                    ddlMercadoRel.SelectedValue = "0";
                    TxtCantidadUsu.Text = "0";
                    TxtEquivaleKpcd.Text = "0"; // Campos nuevos Req. 009-17 Indicadores 20170321
                    CargarDatosUsu();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnActualUsu_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_cont_usr", "@P_codigo_modif","@P_codigo_verif", "@P_no_identificacion_usr", "@P_codigo_tipo_doc",
                                        "@P_nombre_usuario", "@P_codigo_sector_consumo", "@P_codigo_punto_salida","@P_cantidad_contratada","@P_tipo_demanda",
                                        "@P_codigo_mercado_relevante","@P_equivalente_kpcd", "@P_accion","@P_destino_rueda", "@P_codigo_solicitud"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,
                                         SqlDbType.VarChar, SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,
                                         SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int
                                      };
        string[] lValorParametros = { "0", "0", "0", "", "", "", "0", " 0", "0", "0", "0","0","2","", "0"
                                    };
        lblMensaje.Text = "";
        string[] lsUsuario;
        int liValor = 0;
        string lsNombre = "";
        string lsTipoDoc = "";
        try
        {
            lConexion.Abrir();
            lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
            if (dlTipoDemanda.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar el Tipo Demanda Atender. <br> ";
            else
            {
                if (hdfTipoDemanda.Value == "2")
                {
                    if (TxtUsuarioFinal.Text.Trim().Length <= 0)
                        lblMensaje.Text = "Debe Ingresar el Usuario Final <br>";
                    else
                    {
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                        if (!lLector.HasRows)
                            lblMensaje.Text += "El Usuario Ingresado NO existe en la Base de Datos.! <br>";
                        else
                        {
                            lLector.Read();
                            lsNombre = lLector["nombre"].ToString();
                            lsTipoDoc = lLector["codigo_tipo_doc"].ToString();
                        }
                        lLector.Close();
                        lLector.Dispose();
                    }
                }
                else
                {
                    if (ddlMercadoRel.SelectedValue == "0")
                        lblMensaje.Text += "Debe Seleccionar el mercado relevante. <br> ";
                }

            }
            if (ddlSector.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar " + lblSector.Text + ". <br> ";
            if (ddlPuntoSalida.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar  Punto de Salida en SNT. <br> ";
            if (TxtCantidadUsu.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar " + lblCantContra.Text + ". <br> ";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCantidadUsu.Text.Trim());
                    if (liValor < 1)
                        lblMensaje.Text += "Valor Inválido " + lblCantContra.Text + ". <br> ";
                    else
                    {
                        if (liValor > Convert.ToDecimal(TxtCantidad.Text))
                            lblMensaje.Text += "La " + lblCantContra.Text + " No puede ser Mayor que " + TxtCantidad.Text + ". <br> ";
                        else
                        {
                            if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(TxtCantidad.Text))
                                lblMensaje.Text += "La Cantidad Acumulada de Usuarios No puede ser Mayor que " + TxtCantidad.Text + ". <br> ";
                        }
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido " + lblCantContra.Text + ". <br> ";
                }
            }
            if (TxtEquivaleKpcd.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar el Equivalente Kpcd. <br> ";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtEquivaleKpcd.Text.Trim());
                    if (liValor < 0)
                        lblMensaje.Text += "Valor Inválido en Equivalente Kpcd. <br> ";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en " + lblCantContra.Text + ". <br> ";
                }
            }
            if (lblMensaje.Text == "")
            {
                if (hdfCodDatUsu.Value != "")
                    lValorParametros[0] = hdfCodDatUsu.Value;
                if (hdfCodModif.Value != "")
                    lValorParametros[1] = hdfCodModif.Value;
                if (hdfIdRegistro.Value != "")
                    lValorParametros[1] = hdfIdRegistro.Value;
                if (hdfTipoDemanda.Value == "2")
                {
                    lValorParametros[3] = lsUsuario[0].Trim();
                    lValorParametros[4] = lsTipoDoc;
                    lValorParametros[5] = lsNombre;
                }
                lValorParametros[6] = ddlSector.SelectedValue;
                lValorParametros[7] = ddlPuntoSalida.SelectedValue;
                lValorParametros[8] = TxtCantidadUsu.Text.Trim();
                lValorParametros[9] = dlTipoDemanda.SelectedValue;
                lValorParametros[10] = ddlMercadoRel.SelectedValue;
                lValorParametros[11] = TxtEquivaleKpcd.Text.Trim();
                lValorParametros[13] = DdlDestino.SelectedValue;
                lValorParametros[14] = hdfNoSolicitud.Value;

                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetModContRegUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización de la Información del Usuario.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Información de Usuarios Finales Actualizada Correctamente.!" + "');", true);
                    TxtUsuarioFinal.Text = "";
                    ddlSector.SelectedValue = "0";
                    ddlPuntoSalida.SelectedValue = "0";
                    ddlMercadoRel.SelectedValue = "0";
                    TxtCantidadUsu.Text = "0";
                    TxtEquivaleKpcd.Text = "0"; // Campos nuevos Req. 009-17 Indicadores 20170321
                    btnCrearUsu.Visible = true;
                    btnActualUsu.Visible = false;
                    CargarDatosUsu();
                    btnActualUsu.Visible = false;
                    btnCrearUsu.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dlTipoDemanda_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            hdfTipoDemanda.Value = dlTipoDemanda.SelectedValue;
            if (hdfTipoDemanda.Value == "1")
            {
                TxtUsuarioFinal.Enabled = false;
                ddlMercadoRel.Enabled = true; //20160303
                lblSector.Text = "Sector de Consumo Usuario Regulado";
                lblCantContra.Text = "Cantidad a entregar";
            }
            else
            {
                TxtUsuarioFinal.Enabled = true;
                ddlMercadoRel.Enabled = false; //20160303
                ddlMercadoRel.SelectedValue = "0"; //20160303
                lblCantContra.Text = "Cantidad Contratada por usuario (MBTUD)";
                lblSector.Text = "Sector Consumo Usuario No Regulado";
            }
            lConexion.Abrir();
            ddlSector.Items.Clear();
            ddlPuntoSalida.Items.Clear();
            LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo sec, m_demanda_sector dem", " sec.estado ='A' and sec.codigo_sector_consumo = dem.codigo_sector_consumo  and dem.estado ='A' and dem.codigo_tipo_demanda =" + dlTipoDemanda.SelectedValue + "  and dem.ind_uso ='T' order by descripcion", 0, 1); //20160706
            LlenarControles(lConexion.gObjConexion, ddlPuntoSalida, "m_punto_salida_snt", " estado = 'A'  order by descripcion", 0, 2);
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {

        }
    }
    /// <summary>
    /// Nombre: CargarDatosUsu
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid de Usuarios.
    /// Modificacion:
    /// </summary>
    private void CargarDatosUsu()
    {
        string[] lsNombreParametros = { "@P_codigo_modif", "@P_codigo_verif", "@P_no_identificacion_usr", "@P_login" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "0", "", "" };
        try
        {
            if (hdfCodModif.Value.Trim().Length > 0 && hdfCodModif.Value.Trim() != "0")
                lValorParametros[0] = hdfCodModif.Value;
            if (hdfIdRegistro.Value.Trim().Length > 0)
                lValorParametros[1] = hdfIdRegistro.Value;
            if (Session["tipoPerfil"].ToString() == "N")
                lValorParametros[3] = goInfo.Usuario.ToString();
            lConexion.Abrir();
            dtgUsuarios.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContModUsuFin", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgUsuarios.DataBind();
            lConexion.Cerrar();
            lblTotlCantidad.Text = "0";
            if (dtgUsuarios.Items.Count > 0)
            {
                foreach (DataGridItem Grilla in this.dtgUsuarios.Items)
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(Grilla.Cells[6].Text.Trim())).ToString();
                }
                dtgUsuarios.Columns[7].Visible = true;
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// Nombre: dtgUsuarios_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgUsuarios_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";

        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            try
            {
                ////////////////////////////////////////////////////////////////////////////////////////
                /// OJO Cambios de los Indices por adicion de Campo Req. 009-17 Indicadores 20170321 ///
                ////////////////////////////////////////////////////////////////////////////////////////
                string lsTipoDoc = "";
                if (this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[14].Text == "1")  //rq009-17
                    lsTipoDoc = "Nit";
                else
                    lsTipoDoc = "Cédula";
                hdfCodDatUsu.Value = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text; //rq009-17
                                                                                              //Ajuste para que no muestre infrmacion mala 20160621
                if (this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[1].Text == "&nbsp;")
                    TxtUsuarioFinal.Text = lsTipoDoc;
                else
                    TxtUsuarioFinal.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[1].Text + "-" + lsTipoDoc + "-" + this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[3].Text;
                TxtCantidadUsu.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[6].Text;
                lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) - Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                dlTipoDemanda.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[15].Text; //rq009-17
                dlTipoDemanda_SelectedIndexChanged(null, null);
                ddlSector.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[12].Text; //rq009-17
                ddlMercadoRel.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[16].Text; //rq009-17
                ddlPuntoSalida.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[13].Text; //rq009-17
                btnCrearUsu.Visible = false;
                btnActualUsu.Visible = true;
                TxtEquivaleKpcd.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[7].Text;
                TdIndica.Visible = true;
                ///TdIndica1.Visible = true;
                btnActualUsu.Visible = true;
                btnCrearUsu.Visible = false;
                hdfCodigoVerUsr.Value = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text;
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al Recuperar el Registro.! " + ex.Message.ToString();
            }

        }
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            string[] lsNombreParametros = { "@P_codigo_cont_usr", "@P_accion", "@P_codigo_solicitud" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { hdfCodDatUsu.Value, "3", "0" };

            try
            {
                hdfCodDatUsu.Value = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text;
                lValorParametros[0] = hdfCodDatUsu.Value;
                lValorParametros[2] = hdfNoSolicitud.Value;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetModContRegUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Eliminación de la Información del Usuario.!" + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) - Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                    lConexion.Cerrar();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Eliminado Correctamente.!" + "');", true);
                    CargarDatosUsu();
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al Eliminar el Registro.! " + ex.Message.ToString();

            }
        }

    }


    /// <summary>
    /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGrabarDef_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_solicitud","@P_ind_reestructuracion","@P_punta"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar
                                        };
        string[] lValorParametros = { hdfNoSolicitud.Value , "N",hdfPunta.Value
                                    };
        lblMensaje.Text = "";
        try
        {
            if (lblMensaje.Text == "")
            {
                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModContRegDef", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Text += lLector["error"].ToString() + "<br>";
                }
                lLector.Close();
                lConexion.Cerrar();

                if (lblMensaje.Text == "")
                {
                    btnRegresar_Click(null, null);
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlConexion_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DdlMercado.SelectedValue == "O")
        {
            if (ddlConexion.SelectedValue == "S")
            {
                trMayor3.Visible = true;
                trMayor4.Visible = false;
                ddlMercadoRelevante.SelectedValue = "0";
            }
            else
            {
                trMayor3.Visible = false;
                trMayor4.Visible = true;
                ddlDeptoPuntoSal.SelectedValue = "0";
                ddlDeptoPuntoSal_SelectedIndexChanged(null, null);
            }
        }
    }
    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='t_contrato_modificacion' and llave_registro='codigo_verif_contrato=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_verif_contrato=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "t_contrato_modificacion";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_contrato_modificacion", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20170814 rq036-17
    protected void DdlBusMercado_SelectedIndexChanged(object sender, EventArgs e)
    {
        DdlBusProducto.Items.Clear();
        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "";
        lItem.Text = "Seleccione";
        DdlBusProducto.Items.Add(lItem);
        System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
        lItem1.Value = "G";
        lItem1.Text = "Suministro de gas";
        DdlBusProducto.Items.Add(lItem1);
        System.Web.UI.WebControls.ListItem lItem2 = new System.Web.UI.WebControls.ListItem();
        lItem2.Value = "T";
        lItem2.Text = "Capacidad de transporte";
        DdlBusProducto.Items.Add(lItem2);
        if (DdlBusMercado.SelectedValue == "" || DdlBusMercado.SelectedValue == "O")
        {
            System.Web.UI.WebControls.ListItem lItem3 = new System.Web.UI.WebControls.ListItem();
            lItem3.Value = "A";
            lItem3.Text = "Suministro y transporte";
            DdlBusProducto.Items.Add(lItem3);
        }
    }
    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected void trae_usr_fin(string lsModif, string lsVerif)
    {
        string[] lsNombreParametros = { "@P_codigo_modif","@P_codigo_verif"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int
                                        };
        string[] lValorParametros = { lsModif, lsVerif
                                    };
        try
        {
            lConexion.Abrir();
            DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetUsrFinMod", lsNombreParametros, lTipoparametros, lValorParametros);
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Se presentó un error al recuperar los usuarios finales. " + ex.Message;
        }

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgReestructura_EditCommand(object source, DataGridCommandEventArgs e)
    {
        hdfIdRegistro.Value = e.Item.Cells[2].Text;
        hdfConsec.Value = e.Item.Cells[3].Text;
        color_label("R"); //20190425 rq022-19 ajsute modificacion
        consultar("C");
    }
    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    /// //20190425 rq022-19 ajsute modificaciones
    protected void color_label(string lsColor)
    {
        if (lsColor == "N")
        {
            LblOperadorC.ForeColor = System.Drawing.Color.Black;
            LblOperadorV.ForeColor = System.Drawing.Color.Black;
            LblFechaNeg.ForeColor = System.Drawing.Color.Black;
            LblFechaSus.ForeColor = System.Drawing.Color.Black;
            LblContratoDef.ForeColor = System.Drawing.Color.Black;
            LblContratoVar.ForeColor = System.Drawing.Color.Black;
            LblFuente.ForeColor = System.Drawing.Color.Black;
            LblConectadoSnt.ForeColor = System.Drawing.Color.Black;
            LblEntregaBoca.ForeColor = System.Drawing.Color.Black;
            LblPunto.ForeColor = System.Drawing.Color.Black;
            LblModalidad.ForeColor = System.Drawing.Color.Black;
            LblPeriodo.ForeColor = System.Drawing.Color.Black;
            LblNoAños.ForeColor = System.Drawing.Color.Black;
            LblFechaIni.ForeColor = System.Drawing.Color.Black;
            LblFechaFin.ForeColor = System.Drawing.Color.Black;
            LblCantidad.ForeColor = System.Drawing.Color.Black;
            LblPrecio.ForeColor = System.Drawing.Color.Black;
            LblSentido.ForeColor = System.Drawing.Color.Black;
            LblPresion.ForeColor = System.Drawing.Color.Black;
            LblGarantia.ForeColor = System.Drawing.Color.Black;
            LblValorGrtia.ForeColor = System.Drawing.Color.Black;
            LblFechaPagoGrtia.ForeColor = System.Drawing.Color.Black;
            LblSectorCon.ForeColor = System.Drawing.Color.Black;
            LblUsuaroNoReg.ForeColor = System.Drawing.Color.Black;
            LblRutaMay.ForeColor = System.Drawing.Color.Black;
            LblDepto.ForeColor = System.Drawing.Color.Black;
            LblMunicipio.ForeColor = System.Drawing.Color.Black;
            LblCentro.ForeColor = System.Drawing.Color.Black;
            LblMercadoRel.ForeColor = System.Drawing.Color.Black;
            trCambioUsrFin.Visible = false;
        }
        else
        {
            string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_consecutivo" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { hdfIdRegistro.Value, hdfConsec.Value };
            try
            {
                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetDatoModifCont", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    if (lLector["mod_operador_c"].ToString() == "S")
                        LblOperadorC.ForeColor = System.Drawing.Color.Red;
                    else
                        LblOperadorC.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_operador_v"].ToString() == "S")
                        LblOperadorV.ForeColor = System.Drawing.Color.Red;
                    else
                        LblOperadorV.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_fecha_neg"].ToString() == "S")
                        LblFechaNeg.ForeColor = System.Drawing.Color.Red;
                    else
                        LblFechaNeg.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_fecha_suscripcion"].ToString() == "S")
                        LblFechaSus.ForeColor = System.Drawing.Color.Red;
                    else
                        LblFechaSus.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_contrato_definitivo"].ToString() == "S")
                        LblContratoDef.ForeColor = System.Drawing.Color.Red;
                    else
                        LblContratoDef.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_ind_contrato_var"].ToString() == "S")
                        LblContratoVar.ForeColor = System.Drawing.Color.Red;
                    else
                        LblContratoVar.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_codigo_fuente"].ToString() == "S")
                        LblFuente.ForeColor = System.Drawing.Color.Red;
                    else
                        LblFuente.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_conectado_snt"].ToString() == "S")
                        LblConectadoSnt.ForeColor = System.Drawing.Color.Red;
                    else
                        LblConectadoSnt.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_ent_boca_pozo"].ToString() == "S")
                        LblEntregaBoca.ForeColor = System.Drawing.Color.Red;
                    else
                        LblEntregaBoca.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_codigo_punto_entrega"].ToString() == "S")
                        LblPunto.ForeColor = System.Drawing.Color.Red;
                    else
                        LblPunto.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_codigo_modalidad"].ToString() == "S")
                        LblModalidad.ForeColor = System.Drawing.Color.Red;
                    else
                        LblModalidad.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_codigo_periodo"].ToString() == "S")
                        LblPeriodo.ForeColor = System.Drawing.Color.Red;
                    else
                        LblPeriodo.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_no_años"].ToString() == "S")
                        LblNoAños.ForeColor = System.Drawing.Color.Red;
                    else
                        LblNoAños.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_fecha_ini"].ToString() == "S")
                        LblFechaIni.ForeColor = System.Drawing.Color.Red;
                    else
                        LblFechaIni.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_fecha_fin"].ToString() == "S")
                        LblFechaFin.ForeColor = System.Drawing.Color.Red;
                    else
                        LblFechaFin.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_cantidad"].ToString() == "S")
                        LblCantidad.ForeColor = System.Drawing.Color.Red;
                    else
                        LblCantidad.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_precio"].ToString() == "S")
                        LblPrecio.ForeColor = System.Drawing.Color.Red;
                    else
                        LblPrecio.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_sentido_flujo"].ToString() == "S")
                        LblSentido.ForeColor = System.Drawing.Color.Red;
                    else
                        LblSentido.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_presion_punto_fin"].ToString() == "S")
                        LblPresion.ForeColor = System.Drawing.Color.Red;
                    else
                        LblPresion.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_tipo_garantia"].ToString() == "S")
                        LblGarantia.ForeColor = System.Drawing.Color.Red;
                    else
                        LblGarantia.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_valor_garantia"].ToString() == "S")
                        LblValorGrtia.ForeColor = System.Drawing.Color.Red;
                    else
                        LblValorGrtia.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_fecha_pago"].ToString() == "S")
                        LblFechaPagoGrtia.ForeColor = System.Drawing.Color.Red;
                    else
                        LblFechaPagoGrtia.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_sector_consumo"].ToString() == "S")
                        LblSectorCon.ForeColor = System.Drawing.Color.Red;
                    else
                        LblSectorCon.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_nit_usuario_cont"].ToString() == "S")
                        LblUsuaroNoReg.ForeColor = System.Drawing.Color.Red;
                    else
                        LblUsuaroNoReg.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_codigo_ruta_may"].ToString() == "S")
                        LblRutaMay.ForeColor = System.Drawing.Color.Red;
                    else
                        LblRutaMay.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_codigo_depto"].ToString() == "S")
                        LblDepto.ForeColor = System.Drawing.Color.Red;
                    else
                        LblDepto.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_codigo_municipio"].ToString() == "S")
                        LblMunicipio.ForeColor = System.Drawing.Color.Red;
                    else
                        LblMunicipio.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_codigo_centro_poblado"].ToString() == "S")
                        LblCentro.ForeColor = System.Drawing.Color.Red;
                    else
                        LblCentro.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_mercado_relevante"].ToString() == "S")
                        LblMercadoRel.ForeColor = System.Drawing.Color.Red;
                    else
                        LblMercadoRel.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_usr_final"].ToString() == "S")
                        trCambioUsrFin.Visible = true;
                    else
                        trCambioUsrFin.Visible = false;
                }
                else
                {
                    LblOperadorC.ForeColor = System.Drawing.Color.Black;
                    LblOperadorV.ForeColor = System.Drawing.Color.Black;
                    LblFechaNeg.ForeColor = System.Drawing.Color.Black;
                    LblFechaSus.ForeColor = System.Drawing.Color.Black;
                    LblContratoDef.ForeColor = System.Drawing.Color.Black;
                    LblContratoVar.ForeColor = System.Drawing.Color.Black;
                    LblFuente.ForeColor = System.Drawing.Color.Black;
                    LblConectadoSnt.ForeColor = System.Drawing.Color.Black;
                    LblEntregaBoca.ForeColor = System.Drawing.Color.Black;
                    LblPunto.ForeColor = System.Drawing.Color.Black;
                    LblModalidad.ForeColor = System.Drawing.Color.Black;
                    LblPeriodo.ForeColor = System.Drawing.Color.Black;
                    LblNoAños.ForeColor = System.Drawing.Color.Black;
                    LblFechaIni.ForeColor = System.Drawing.Color.Black;
                    LblFechaFin.ForeColor = System.Drawing.Color.Black;
                    LblCantidad.ForeColor = System.Drawing.Color.Black;
                    LblPrecio.ForeColor = System.Drawing.Color.Black;
                    LblSentido.ForeColor = System.Drawing.Color.Black;
                    LblPresion.ForeColor = System.Drawing.Color.Black;
                    LblGarantia.ForeColor = System.Drawing.Color.Black;
                    LblValorGrtia.ForeColor = System.Drawing.Color.Black;
                    LblFechaPagoGrtia.ForeColor = System.Drawing.Color.Black;
                    LblSectorCon.ForeColor = System.Drawing.Color.Black;
                    LblUsuaroNoReg.ForeColor = System.Drawing.Color.Black;
                    LblRutaMay.ForeColor = System.Drawing.Color.Black;
                    LblDepto.ForeColor = System.Drawing.Color.Black;
                    LblMunicipio.ForeColor = System.Drawing.Color.Black;
                    LblCentro.ForeColor = System.Drawing.Color.Black;
                    LblMercadoRel.ForeColor = System.Drawing.Color.Black;
                    trCambioUsrFin.Visible = false;
                }
                lLector.Close();
                lConexion.Cerrar();
            }
            catch (Exception)
            {
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lkbExcel_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        try
        {

            string lsNombreArchivo = Session["login"] + "InfExcelReg" + DateTime.Now + ".xls";
            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            lpagina.EnableEventValidation = false;
            lpagina.Controls.Add(lform);
            dtgConsulta.EnableViewState = false;
            dtgConsulta.Columns[0].Visible = false;
            dtgConsulta.Columns[1].Visible = false;
            dtgConsulta.Columns[17].Visible = false;
            dtgConsulta.Columns[18].Visible = false;
            lform.Controls.Add(dtgConsulta);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;

            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + "Consulta de contratos para modificar" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write(lsb.ToString());
            Response.End();
            Response.Flush();
            dtgConsulta.Columns[0].Visible = true;
            dtgConsulta.Columns[1].Visible = true;
            dtgConsulta.Columns[17].Visible = true;
            dtgConsulta.Columns[18].Visible = true;

        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas al Consultar los Registros. " + ex.Message.ToString();
        }
    }
}