﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_contratoSinDemanda.aspx.cs" Inherits="Verificacion.frm_contratoSinDemanda" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body" runat="server">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Código" AssociatedControlID="TxtBusCodigo" runat="server" />
                            <asp:TextBox ID="TxtBusCodigo" runat="server" autocomplete="off" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtBusCodigo" runat="server" TargetControlID="TxtBusCodigo"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operación" AssociatedControlID="TxtBusOperacion" runat="server" />
                            <asp:TextBox ID="TxtBusOperacion" runat="server" autocomplete="off" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtBusOperacion" runat="server" TargetControlID="TxtBusOperacion"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Inicial" AssociatedControlID="TxtBusFechaIni" runat="server" />
                            <asp:TextBox ID="TxtBusFechaIni" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Final" AssociatedControlID="TxtBusFechaFin" runat="server" />
                            <asp:TextBox ID="TxtBusFechaFin" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador" AssociatedControlID="ddlBusOperador" runat="server" />
                            <asp:DropDownList ID="ddlBusOperador" runat="server" CssClass="form-control selectpicker" />
                        </div>
                    </div>
                </div>
                <%--Mensaje--%>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:ValidationSummary ID="VsConsulta" runat="server" ValidationGroup="detalle1" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
                <%--Grilla--%>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" PageSize="10"
                                 OnPageIndexChanged="dtgMaestro_PageIndexChanged">
                                <Columns>
                                    <asp:BoundColumn DataField="codigo_contrato_sin" HeaderText="Código" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_comprador" HeaderText="Código Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_comprador" HeaderText="Nombre Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="Fecha-hora" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd HH:mm}"></asp:BoundColumn>
                                </Columns>
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%--Modals--%>
    <div class="modal fade" id="registroAdicion" tabindex="-1" role="dialog" aria-labelledby="mdlregistroAdicion" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="registroAdicionInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="lblRegistroAdicion" runat="server">Agregar</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Código" AssociatedControlID="TxtCodigoCap" runat="server" />
                                        <asp:TextBox ID="TxtCodigoCap" runat="server" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                        <asp:Label ID="LblCodigoCap" runat="server" Visible="False" CssClass="form-control"></asp:Label>
                                        <asp:HiddenField ID="hdnELiminado" runat="server" />
                                        <asp:HiddenField ID="hdfModificacion" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Operador Compra" AssociatedControlID="ddlOperador" runat="server" />
                                        <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Operación" AssociatedControlID="TxtOperacion" runat="server" />
                                        <asp:TextBox ID="TxtOperacion" runat="server" CssClass="form-control"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtOperacion" runat="server" TargetControlID="TxtOperacion"
                                            FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="detalle" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button Text="Salir" CssClass="btn btn-secondary" OnClick="Cancel_OnClick" runat="server" />
                            <asp:Button ID="imbCrear" runat="server" CssClass="btn btn-primary" Text="Crear" OnClick="imbCrear_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
