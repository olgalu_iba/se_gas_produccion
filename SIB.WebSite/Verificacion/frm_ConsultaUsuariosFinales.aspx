﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ConsultaUsuariosFinales.aspx.cs" Inherits="Verificacion_frm_ConsultaUsuariosFinales" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Número Operación</label>
                            <asp:TextBox ID="TxtNoContrato" type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Subasta</label>
                            <asp:DropDownList ID="ddlSubasta" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fecha Inicial Negociación</label>
                            <asp:TextBox ID="TxtFechaIni" runat="server" CssClass="form-control datepicker" Width="100%" ClientIDMode="Static" MaxLength="10" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fecha Final Negociación</label>
                            <asp:TextBox ID="TxtFechaFin" runat="server" CssClass="form-control datepicker" Width="100%" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Mercado</label>
                            <asp:DropDownList ID="ddlMercado" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                <asp:ListItem Value="" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="P" Text="Primario"></asp:ListItem>
                                <asp:ListItem Value="S" Text="Secundario"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Producto</label>
                            <asp:DropDownList ID="ddlProducto" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="T" Text="Capacidad de Transporte"></asp:ListItem>
                                <%--20180126 rq107-16--%>
                                <asp:ListItem Value="G" Text="Suministro de Gas"></asp:ListItem>
                                <%--20180126 rq107-16--%>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Comprador</label>
                            <asp:DropDownList ID="ddlComprador" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Vendedor</label>
                            <asp:DropDownList ID="ddlVendedor" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Estado</label>
                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Causa Modificación</label>
                            <asp:DropDownList ID="ddlCausa" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Id Registro</label>
                            <asp:TextBox ID="TxtNoId" type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" ValidationGroup="detalle" CssClass="form-control" />
                        </div>
                    </div>
                </div>
            </div>
            <%--Mensajes de Error--%>
            <div class="kt-portlet__body" runat="server" id="tblMensaje">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <%--Grilla--%>
            <div class="kt-portlet__body" runat="server" id="tblGrilla" visible="false">
                <div class="row">
                    <div class="table table-responsive">
                        <div style="overflow: scroll; height: 450px;">
                            <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                Width="100%" CssClass="table-bordered">
                                <Columns>
                                    <%--0--%>
                                    <asp:BoundColumn DataField="tipo_subasta" HeaderText="Tipo Subasta"></asp:BoundColumn>
                                    <%--1--%>
                                    <asp:BoundColumn DataField="tipo_rueda" HeaderText="Tipo Rueda"></asp:BoundColumn>
                                    <%--2--%>
                                    <%--20180126 rq107 - 16--%>
                                    <asp:BoundColumn DataField="numero_operacion" HeaderText="No. Operación"></asp:BoundColumn>
                                    <%--3--%>
                                    <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro"></asp:BoundColumn>
                                    <%--4--%>
                                    <asp:BoundColumn DataField="vendedor" HeaderText="Nombre Vendedor"></asp:BoundColumn>
                                    <%--5--%>
                                    <asp:BoundColumn DataField="codigo_vendedor" HeaderText="Código Vendedor" Visible="false"></asp:BoundColumn>
                                    <%--6--%>
                                    <asp:BoundColumn DataField="rol_vendedor" HeaderText="Rol Vendedor" Visible="false"></asp:BoundColumn>
                                    <%--7--%>
                                    <asp:BoundColumn DataField="comprador" HeaderText="Nombre Comprador"></asp:BoundColumn>
                                    <%--8--%>
                                    <asp:BoundColumn DataField="codigo_comprador" HeaderText="Código Comprador" Visible="false"></asp:BoundColumn>
                                    <%--9--%>
                                    <asp:BoundColumn DataField="rol_comprador" HeaderText="Rol Comprador" Visible="false"></asp:BoundColumn>
                                    <%--20180126 rq107-16--%>
                                    <%--10--%>
                                    <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato"></asp:BoundColumn>
                                    <%--11--%>
                                    <asp:BoundColumn DataField="codigo_sector_consumo" HeaderText="Código Sector Consumo"
                                        Visible="false"></asp:BoundColumn>
                                    <%--12--%>
                                    <asp:BoundColumn DataField="desc_sector_consumo" HeaderText="Descripción Sector Consumo"></asp:BoundColumn>
                                    <%--13--%>
                                    <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Código Punto Salida"
                                        Visible="false"></asp:BoundColumn>
                                    <%--14--%>
                                    <asp:BoundColumn DataField="desc_punto_salida" HeaderText="Descripción Punto Salida"></asp:BoundColumn>
                                    <%--15--%>
                                    <asp:BoundColumn DataField="cantidad_capacidad" HeaderText="Cantidad / Capacidad"></asp:BoundColumn>
                                    <%--16--%>
                                    <asp:BoundColumn DataField="desc_tipo_demanda" HeaderText="Descripción Tipo Demanda"></asp:BoundColumn>
                                    <%--17--%>
                                    <asp:BoundColumn DataField="codigo_mercado_relevante" HeaderText="Código Mercado Relevante"
                                        Visible="false"></asp:BoundColumn>
                                    <%--18--%>
                                    <asp:BoundColumn DataField="desc_mercado_relevante" HeaderText="Descripción Mercado Relevante"></asp:BoundColumn>
                                    <%--19--%>
                                    <asp:BoundColumn DataField="nit_usuario_final" HeaderText="Nit Usuario Final"></asp:BoundColumn>
                                    <%--20--%>
                                    <asp:BoundColumn DataField="nombre_usuario_final" HeaderText="Nombre Usuario Final"></asp:BoundColumn>
                                    <%--21--%>
                                    <asp:BoundColumn DataField="estado" HeaderText="Estado"></asp:BoundColumn>
                                    <%--22--%>
                                    <%--20180126 rq107 - 16--%>
                                    <asp:BoundColumn DataField="carga_extemporanea" HeaderText="Actualización/Extemporánea" Visible="false"></asp:BoundColumn>
                                    <%--Campo Nuevo Req. 009-17 Indicadores 20170321--%>
                                    <%--23--%>
                                    <asp:BoundColumn DataField="equivalente_kpcd" HeaderText="Cnt. Equiv. KPCD"></asp:BoundColumn>
                                    <%--24 20171130 rq026-17--%>
                                    <asp:BoundColumn DataField="desc_causa" HeaderText="Causa Modificación"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
