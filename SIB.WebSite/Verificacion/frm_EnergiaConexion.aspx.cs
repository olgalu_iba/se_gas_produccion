﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Verificacion
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frm_EnergiaConexion : Page
    {
        private InfoSessionVO goInfo;
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        private clConexion lConexion;
        private SqlDataReader lLector;
        private DataSet lds = new DataSet();
        private SqlDataAdapter lsqldata = new SqlDataAdapter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = "Cantidad de Energía Gasoducto de Conexión";

            lConexion = new clConexion(goInfo);
            // Activacion de los Botones
            buttons.Inicializar(ruta: "t_energia_conexion"); //20200727
            buttons.CrearOnclick += btnNuevo;
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.Inicializar(botones: new[] { EnumBotones.Crear, EnumBotones.Buscar, EnumBotones.Excel });

            if (IsPostBack) return;
            //Titulo
            Master.Titulo = "Registros Operativos";
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles1(lConexion.gObjConexion, ddlOperador, "m_operador ope, m_operador_conexion con", " ope.estado = 'A' and ope.tipo_operador= con.tipo_operador and con.estado ='A' order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlDemanda, "m_tipo_demanda_atender", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles1(lConexion.gObjConexion, ddlBusOperador, "m_operador ope, m_operador_conexion con", " ope.estado = 'A' and ope.tipo_operador= con.tipo_operador and con.estado ='A' order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlPunto, "m_pozo", " estado = 'A' and ind_entrada ='S' order by descripcion", 0, 1);

            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetFechaConexion", null, null, null, goInfo);
            lLector.Read();
            hdfPermitido.Value = lLector["ingresa"].ToString();
            hdfFechaIni.Value = lLector["fecha_ini"].ToString();
            hdfFechaFin.Value = lLector["fecha_fin"].ToString();
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
            //20200727
            if (hdfPermitido.Value == "N")
                buttons.Inicializar(botones: new[] { EnumBotones.Buscar, EnumBotones.Excel });

            var mensaje = new StringBuilder();
            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlBusOperador.SelectedValue = goInfo.cod_comisionista;
                ddlBusOperador_SelectedIndexChanged(null, null);
                ddlBusOperador.Enabled = false;
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
                ddlOperador_SelectedIndexChanged(null, null);
                hdnELiminado.Value = "N";
                if (ddlOperador.SelectedValue == "0")
                    mensaje.Append("El operador no está autoriazado para el registro de la información");
            }
            else
                hdnELiminado.Value = "S";

            //Establese los permisos del sistema
            //EstablecerPermisosSistema(); //20220621 ajuste

            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (!string.IsNullOrEmpty(mensaje.ToString()))
                Modal.Cerrar(this, registroEnergiaConexion.ID);
        }

        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "t_energia_conexion");
            //20220621 ajsute
            if ((bool)permisos["UPDATE"] && (bool)permisos["DELETE"])
                dtgMaestro.Columns[20].Visible = true;
            else
            {
                if (!(bool)permisos["UPDATE"] && !(bool)permisos["DELETE"])
                    dtgMaestro.Columns[20].Visible = false;
                else
                {
                    if (!(bool)permisos["UPDATE"])
                    {
                        foreach (DataGridItem Grilla in dtgMaestro.Items)
                        {
                            var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                            lkbModificar.Visible = false;
                        }
                    }
                    if (!(bool)permisos["DELETE"])
                    {
                        foreach (DataGridItem Grilla in dtgMaestro.Items)
                        {
                            var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
                            lkbEliminar.Visible = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            var mensaje = new StringBuilder();
            if (string.IsNullOrEmpty(modificar)) return;

            try
            {
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_energia_conexion", " codigo_energia_conexion= " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoCap.Text = lLector["codigo_energia_conexion"].ToString();
                        TxtCodigoCap.Text = lLector["codigo_energia_conexion"].ToString();
                        TxtFecha.Text = Convert.ToDateTime(lLector["fecha"].ToString()).ToString("yyyy/MM/dd");
                        try
                        {
                            ddlOperador.SelectedValue = lLector["codigo_Operador"].ToString();
                            ddlOperador_SelectedIndexChanged(null, null);
                        }
                        catch (Exception)
                        {
                            mensaje.Append("El operador del registro no existe o no está habilitado<br>");
                        }
                        try
                        {
                            ddlGasoducto.SelectedValue = lLector["codigo_gasoducto"].ToString();
                        }
                        catch (Exception)
                        {
                            mensaje.Append("El gasoducto de conexión no existe o está inactivo<br>");
                        }
                        try
                        {
                            ddlTipo.SelectedValue = lLector["tipo_entrega"].ToString();
                            ddlTipo_SelectedIndexChanged(null, null);
                        }
                        catch (Exception ex)
                        {
                        }
                        try
                        {
                            ddlPunto.SelectedValue = lLector["codigo_punto_entrega"].ToString();
                        }
                        catch (Exception)
                        {
                            mensaje.Append("El punto de entrega no existe o está inactivo<br>");
                        }
                        try
                        {
                            ddlDemanda.SelectedValue = lLector["codigo_tipo_demanda"].ToString();
                        }
                        catch (Exception)
                        {
                            mensaje.Append("El tipo de demanda no existe o está inactivo<br>");
                        }

                        txtOperacion.Text = lLector["numero_operacion"].ToString();
                        TxtCantidad.Text = lLector["cantidad"].ToString();
                        TxtEquiv.Text = lLector["equivalente_kpcd"].ToString();
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        lLector.Close();
                        lLector.Dispose();
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoCap.Visible = false;
                        LblCodigoCap.Visible = true;
                        TxtFecha.Enabled = false;
                        ddlOperador.Enabled = false;
                        ddlGasoducto.Enabled = false;
                        txtOperacion.Enabled = false;
                        ddlTipo.Enabled = false;
                        ddlPunto.Enabled = false;
                        ddlDemanda.Enabled = false;
                        ddlEstado.Enabled = true;
                        mdlRegistroEnergiaConexionLabel.InnerText = "Modificar";
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    if (string.IsNullOrEmpty(mensaje.ToString()))
                    {
                        manejo_bloqueo("A", modificar);
                        //Abre el modal de Agregar
                        Modal.Abrir(this, registroEnergiaConexion.ID, registroEnergiaConexionInside.ID);
                    }
                }
                else
                {
                    CargarDatos();
                    mensaje.Append("No se Puede editar el Registro por que está Bloqueado. Código energía conexión " + modificar);
                }

                if (!mensaje.ToString().Equals(""))
                {
                    Toastr.Warning(this, mensaje.ToString());
                    return;
                }

                //Abre el modal de Agregar
                mdlRegistroEnergiaConexionLabel.InnerHtml = "Modificar ";
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_codigo_energia_conexion", "@P_fecha_ini", "@P_fecha_fin", "@P_codigo_operador", "@P_codigo_gasoducto", "@P_mostrar_eliminado" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "", "", "0", "0", hdnELiminado.Value };
            DateTime ldFecha;
            var mensaje = new StringBuilder();

            try
            {
                if (TxtBusFecha.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtBusFecha.Text.Trim());
                    }
                    catch (Exception ex)
                    {
                        mensaje.Append("Valor Inválido en Fecha Inicial. <br>");
                    }
                }
                if (TxtBusFechaF.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtBusFechaF.Text.Trim());
                        if (ldFecha < Convert.ToDateTime(TxtBusFecha.Text.Trim()))
                            mensaje.Append("La Fecha final NO puede ser menor que la Fecha Inicial.!<br>");
                    }
                    catch (Exception)
                    {
                        mensaje.Append("Valor Inválido en Fecha Final. <br>");
                    }
                }
                if (TxtBusCodigo.Text.Trim().Length <= 0 && TxtBusFecha.Text.Trim().Length <= 0 && ddlBusOperador.SelectedValue == "0" && ddlBusGasoducto.SelectedValue == "0")
                {
                    mensaje.Append("Debe digitar un criterio de búsqueda. <br>");
                }
                if (TxtBusFecha.Text == "" && (ddlBusGasoducto.SelectedValue == "0" || ddlBusGasoducto.SelectedValue == "") && TxtBusCodigo.Text == "")
                {
                    TxtBusFecha.Text = hdfFechaIni.Value;
                    TxtBusFechaF.Text = hdfFechaFin.Value;
                }

                if (!string.IsNullOrEmpty(mensaje.ToString()))
                {
                    Toastr.Error(this, mensaje.ToString());
                }
                else
                {
                    if (TxtBusCodigo.Text.Trim().Length > 0)
                        lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    if (TxtBusFecha.Text.Trim().Length > 0)
                        lValorParametros[1] = TxtBusFecha.Text.Trim();
                    if (TxtBusFechaF.Text.Trim().Length > 0)
                        lValorParametros[2] = TxtBusFechaF.Text.Trim();
                    else
                    {
                        if (TxtBusFecha.Text.Trim().Length > 0)
                            lValorParametros[2] = TxtBusFecha.Text.Trim();
                    }

                    if (ddlBusOperador.SelectedValue != "0")
                        lValorParametros[3] = ddlBusOperador.SelectedValue;
                    if (ddlBusGasoducto.SelectedValue != "0" && ddlBusGasoducto.SelectedValue != "")
                        lValorParametros[4] = ddlBusGasoducto.SelectedValue;

                    lConexion.Abrir();
                    dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion,
                        "pa_GetEnergiaConexion", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgMaestro.DataBind();
                    dtgExcel.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion,
                        "pa_GetEnergiaConexion", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgExcel.DataBind();
                    lConexion.Cerrar();

                    EstablecerPermisosSistema();//20220621 ajsute
                }

                if (hdfPermitido.Value != "N") return;
                //hlkNuevo.Enabled = false;
                dtgMaestro.Columns[20].Visible = false;
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Page")) return;

                hdfModificacion.Value = "N";
                var mensaje = new StringBuilder();
                var lCodigoRegistro = dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                var lsFecha = dtgMaestro.Items[e.Item.ItemIndex].Cells[1].Text;
                var blPuede = true;

                if (!ddlOperador.Enabled)
                {
                    if (Convert.ToDateTime(lsFecha) < Convert.ToDateTime(hdfFechaIni.Value) || Convert.ToDateTime(lsFecha) > Convert.ToDateTime(hdfFechaFin.Value))
                    {
                        mensaje.Append("Está intentando modificar o eliminar información de fechas no permitidas.");
                    }
                }

                if (!string.IsNullOrEmpty(mensaje.ToString()))
                {
                    Toastr.Error(this, mensaje.ToString());
                    return;
                }
                if (e.CommandName.Equals("Modificar"))
                {
                    Modificar(lCodigoRegistro);
                }

                if (e.CommandName.Equals("Eliminar"))
                {
                    string[] lsNombreParametros = { "@P_codigo_energia_conexion", "@P_accion" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                    string[] lValorParametros = { lCodigoRegistro, "3" };


                    if (!string.IsNullOrEmpty(mensaje.ToString()))
                    {
                        Toastr.Error(this, mensaje.ToString());
                        return;
                    }
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaConexion", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                    {
                        mensaje.Append("Se presentó un Problema en la Eliminación del Registro.! " + goInfo.mensaje_error);
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Success(this, "Registro Eliminado Correctamente");
                        CargarDatos();
                    }
                }

                if (!string.IsNullOrEmpty(mensaje.ToString()))
                {
                    Toastr.Warning(this, mensaje.ToString());
                }
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia("t_energia_conexion", lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// // 20180126 rq107-16
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector["codigo_operador"].ToString();
                lItem1.Text = lLector["codigo_operador"] + "-" + lLector["razon_social"];
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='t_energia_conexion' and llave_registro='codigo_energia_conexion=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_energia_conexion=" + lscodigo_registro;
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_energia_conexion";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_energia_conexion", lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] lValorParametros = { "0", "", "", "0", "0", hdnELiminado.Value };
            string lsParametros = "";
            if (TxtBusFecha.Text == "" && (ddlBusGasoducto.SelectedValue == "0" || ddlBusGasoducto.SelectedValue == "") && TxtBusCodigo.Text == "")
            {
                TxtBusFecha.Text = hdfFechaIni.Value;
                TxtBusFechaF.Text = hdfFechaFin.Value;
            }

            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    lsParametros += " Código energía : " + TxtBusCodigo.Text;
                }
                if (TxtBusFecha.Text.Trim().Length > 0)
                {
                    lValorParametros[1] = TxtBusFecha.Text.Trim();
                    lsParametros += " Fecha Inicial : " + TxtBusFecha.Text;
                }
                if (TxtBusFechaF.Text.Trim().Length > 0)
                {
                    lValorParametros[2] = TxtBusFechaF.Text.Trim();
                    lsParametros += " Fecha Final : " + TxtBusFechaF.Text;
                }
                else
                {
                    if (TxtBusFecha.Text.Trim().Length > 0)
                    {
                        lValorParametros[2] = TxtBusFecha.Text.Trim();
                        lsParametros += " Fecha Final : " + TxtBusFecha.Text;
                    }
                }
                if (ddlBusOperador.SelectedValue != "0")
                {
                    lValorParametros[3] = ddlBusOperador.SelectedValue;
                    lsParametros += " Operador: " + ddlBusOperador.SelectedItem;
                }
                if (ddlBusGasoducto.SelectedValue != "0" && ddlBusGasoducto.SelectedValue != "")
                {
                    lValorParametros[4] = ddlBusGasoducto.SelectedValue;
                    lsParametros += " gasoducto Conexión: " + ddlBusGasoducto.SelectedItem;
                }
                string lsNombreArchivo = goInfo.Usuario + "EnergiaControl" + DateTime.Now + ".xls";
                string lstitulo_informe = "Energía Gasoducto Conexión";
                dtgExcel.Visible = true;
                StringBuilder lsb = new StringBuilder();
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                dtgExcel.EnableViewState = false;
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                lform.Controls.Add(dtgExcel);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parámetros: " + lsParametros + "</font></th><td></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());
                Response.End();
                lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
                dtgExcel.Visible = false;

            }
            catch (Exception)
            {
                Toastr.Error(this, "No se Pudo Generar el Informe.!");
            }
        }

        /// <summary>
        /// Nombre: ImgPdf_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        string lsCondicion = " codigo_energia_conexion<> '0'";
        //        Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=t_energia_coenxion&procedimiento=pa_ValidarExistencia&columnas=codigo_energia_conexion*fecha*codigo_operador*codigo_gasoducto*numero_operacion*tipo_entrega*codigo_tipo_entrega*codigo_tipo_demanda*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        //    }
        //    catch (Exception ex)
        //    {
        //        lblMensaje.Text = "No se Pudo Generar el Informe.!";
        //    }

        //}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPunto.Items.Clear();
            lConexion.Abrir();
            if (ddlTipo.SelectedValue == "E")
            {
                LlenarControles(lConexion.gObjConexion, ddlPunto, "m_pozo", " estado = 'A' and ind_entrada ='S' order by descripcion", 0, 1);
                lblPunto.Text = "Punto de entrega";
            }
            if (ddlTipo.SelectedValue == "S")
            {
                LlenarControles(lConexion.gObjConexion, ddlPunto, "m_punto_salida_snt", " estado = 'A' order by descripcion", 0, 2);
                lblPunto.Text = "Punto de salida";
            }
            if (ddlTipo.SelectedValue == "T")
            {
                LlenarControles(lConexion.gObjConexion, ddlPunto, "m_pozo", " estado = 'A' and ind_trasferencia='S' order by descripcion", 0, 1);
                lblPunto.Text = "Punto de transferencia";
            }
            if (ddlTipo.SelectedValue == "R")
            {
                LlenarControles(lConexion.gObjConexion, ddlPunto, "m_mercado_relevante", " estado = 'A' order by descripcion", 0, 1);
                lblPunto.Text = "Mercado relevante";
            }
            if (ddlTipo.SelectedValue == "G")
            {
                LlenarControles(lConexion.gObjConexion, ddlPunto, "m_gasoducto_dedicado", " estado = 'A'  order by descripcion", 0, 1);
                lblPunto.Text = "Gasoducto dedicado";
            }
            lConexion.Cerrar();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_energia_conexion", "@P_fecha", "@P_codigo_operador", "@P_codigo_gasoducto", "@P_numero_operacion", "@P_tipo_entrega", "@P_codigo_punto_entrega", "@P_codigo_tipo_demanda", "@P_cantidad", "@P_equivalente_kpcd", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "", "0", "0", "0", "", "0", "0", "0", "0", "", "1" };
            var mensaje = new StringBuilder();

            try
            {
                if (TxtFecha.Text == "")
                    mensaje.Append(" Debe digitar la fecha<br>");
                else
                {
                    try
                    {
                        if (Convert.ToDateTime(TxtFecha.Text) < Convert.ToDateTime(hdfFechaIni.Value) || Convert.ToDateTime(TxtFecha.Text) > Convert.ToDateTime(hdfFechaFin.Value))
                            mensaje.Append("La fecha no está dentro del rango permitido<br>");
                    }
                    catch (Exception)
                    {
                        mensaje.Append("La fecha no es válida<br>");
                    }
                }
                if (ddlOperador.SelectedValue == "0")
                    mensaje.Append("Debe seleccionar el operador<br>");
                if (ddlGasoducto.SelectedValue == "0" || ddlGasoducto.SelectedValue == "")
                    mensaje.Append("Debe seleccionar el Gasoducto de conexión<br>");
                if (ddlPunto.SelectedValue == "0" && ddlPunto.SelectedValue == "")
                    mensaje.Append("Debe seleccionar el punto de entrada<br>");
                if (ddlDemanda.SelectedValue == "0")
                    mensaje.Append("Debe seleccionar el tipo de demanda<br>");
                if (TxtCantidad.Text == "")
                    mensaje.Append("Debe digitar la cantidad<br>");
                if (TxtEquiv.Text == "")
                    mensaje.Append("Debe digitar la cantidad equivalente<br>");
                if (!string.IsNullOrEmpty(mensaje.ToString()))
                {
                    Toastr.Warning(this, mensaje.ToString());
                    return;
                }
                lValorParametros[1] = TxtFecha.Text;
                lValorParametros[2] = ddlOperador.SelectedValue;
                lValorParametros[3] = ddlGasoducto.SelectedValue;
                //20200727
                if (txtOperacion.Text != "")
                    lValorParametros[4] = txtOperacion.Text;
                else
                    lValorParametros[4] = "0";
                lValorParametros[5] = ddlTipo.SelectedValue;
                lValorParametros[6] = ddlPunto.SelectedValue;
                lValorParametros[7] = ddlDemanda.SelectedValue;
                lValorParametros[8] = TxtCantidad.Text;
                lValorParametros[9] = TxtEquiv.Text;
                lValorParametros[10] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetEnergiaConexion", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        mensaje.Append(lLector["error"] + "<br>");
                }
                lLector.Close();
                lConexion.Cerrar();
                if (string.IsNullOrEmpty(mensaje.ToString()))
                {
                    //Se notifica a el usuario que el registro fue realizado de manera exitosa
                    Toastr.Success(this, "Se realizo el registro de forma exitosa.!");
                    //Cierra el modal de Agregar
                    Modal.Cerrar(this, registroEnergiaConexion.ID);
                    CargarDatos();
                }
                else
                    Toastr.Warning(this, mensaje.ToString());
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbActualiza_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_energia_conexion", "@P_fecha", "@P_codigo_operador", "@P_codigo_gasoducto", "@P_numero_operacion", "@P_tipo_entrega", "@P_codigo_punto_entrega", "@P_codigo_tipo_demanda", "@P_cantidad", "@P_equivalente_kpcd", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "", "0", "0", "0", "", "0", "0", "0", "0", "", "2" };
            var mensaje = new StringBuilder();

            try
            {
                if (TxtCantidad.Text == "")
                    mensaje.Append(" Debe digitar la cantidad<br>");
                if (TxtEquiv.Text == "")
                    mensaje.Append(" Debe digitar la cantidad equivalente<br>");
                if (!string.IsNullOrEmpty(mensaje.ToString()))
                {
                    Toastr.Error(this, mensaje.ToString());
                    return;
                }
                lValorParametros[0] = LblCodigoCap.Text;
                lValorParametros[1] = TxtFecha.Text;
                lValorParametros[2] = ddlOperador.SelectedValue;
                lValorParametros[3] = ddlGasoducto.SelectedValue;
                //20200727
                if (txtOperacion.Text != "")
                    lValorParametros[4] = txtOperacion.Text;
                else
                    lValorParametros[4] = "0";
                lValorParametros[5] = ddlTipo.SelectedValue;
                lValorParametros[6] = ddlPunto.SelectedValue;
                lValorParametros[7] = ddlDemanda.SelectedValue;
                lValorParametros[8] = TxtCantidad.Text;
                lValorParametros[9] = TxtEquiv.Text;
                lValorParametros[10] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetEnergiaConexion", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        mensaje.Append(lLector["error"] + "<br>");
                }
                lLector.Close();
                lConexion.Cerrar();
                if (!string.IsNullOrEmpty(mensaje.ToString()))
                {
                    Toastr.Error(this, mensaje.ToString());
                    return;
                }

                manejo_bloqueo("E", LblCodigoCap.Text);
                //Se notifica a el usuario que el registro fue actualizado de manera exitosa
                Toastr.Success(this, "Se realizo la actualización de forma exitosa.!");
                //Cierra el modal de Agregar
                Modal.Cerrar(this, registroEnergiaConexion.ID);
                CargarDatos();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20160721 carga ptdv
        protected void ddlOperador_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlGasoducto.Items.Clear();
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlGasoducto, "m_gasoducto_conexion gas, m_gasoducto_conexion_ope ope", "  estado = 'A' and gas.codigo_gasoducto = ope.codigo_gasoducto and ope.codigo_operador ='" + ddlOperador.SelectedValue + "'  order by descripcion", 0, 1); //20190822 rq054-19      //20200727              
            lConexion.Cerrar();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20160721 carga ptdv
        protected void ddlBusOperador_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlBusGasoducto.Items.Clear();
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlBusGasoducto, "m_gasoducto_conexion gas, m_gasoducto_conexion_ope ope", "  estado = 'A' and gas.codigo_gasoducto = ope.codigo_gasoducto and ope.codigo_operador ='" + ddlBusOperador.SelectedValue + "'  order by descripcion", 0, 1); //20190822 rq054-19      //20200727
            lConexion.Cerrar();
        }

        ///// Eventos Nuevos para la Implementracion del UserControl
        /// <summary>
        /// Metodo del Link Nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNuevo(object sender, EventArgs e)
        {
            var liHoraAct = 0;
            mdlRegistroEnergiaConexionLabel.InnerText = "Agregar";
            LblCodigoCap.Text = "Automatico";
            ddlEstado.SelectedValue = "A";
            TxtCantidad.Text = string.Empty;
            TxtEquiv.Text = string.Empty;
            ddlEstado.SelectedIndex = 0;
            TxtFecha.Text = string.Empty;
            ddlGasoducto.SelectedIndex = 0;
            txtOperacion.Text = string.Empty;
            ddlTipo.SelectedIndex = 0;
            ddlPunto.SelectedIndex = 0;
            ddlDemanda.SelectedIndex = 0;

            imbCrear.Visible = true;
            imbActualiza.Visible = false;
            TxtCodigoCap.Visible = false;
            LblCodigoCap.Visible = true;
            ddlEstado.Enabled = false;
            TxtFecha.Enabled = true;
            ddlGasoducto.Enabled = true;
            txtOperacion.Enabled = true;
            ddlTipo.Enabled = true;
            ddlPunto.Enabled = true;
            ddlDemanda.Enabled = true;

            //Abre el modal de Agregar
            Modal.Abrir(this, registroEnergiaConexion.ID, registroEnergiaConexionInside.ID);
        }

        /// <summary>
        /// Metodo del Link Consultar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            CargarDatos();
        }

        /// <summary>
        /// 
        /// </summary>
        protected void Cancel_OnClick(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (mdlRegistroEnergiaConexionLabel.InnerText.Equals("Modificar") && LblCodigoCap.Text != "")
                manejo_bloqueo("E", LblCodigoCap.Text);

            //Cierra el modal de Agregar
            Modal.Cerrar(this, registroEnergiaConexion.ID);
        }
    }
}