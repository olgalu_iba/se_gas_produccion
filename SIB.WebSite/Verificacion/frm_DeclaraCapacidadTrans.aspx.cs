﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Verificacion_frm_DeclaraCapacidadTrans : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Declaración Perfil Capacidad Transporte"; //20180815 BUG230
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            // Carga informacion de combos
            LlenarTramo();
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlBusOperador, "m_operador", " estado = 'A' order by razon_social", 0, 4);
            lConexion.Cerrar();
            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlBusOperador.SelectedValue = goInfo.cod_comisionista;
                ddlBusOperador.Enabled = false;
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
            }
            TxtFecha.Text = DateTime.Now.Year.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Day.ToString();

            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == null || lsIndica == "" || lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "t_capacidad_transporte");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[13].Visible = (Boolean)permisos["UPDATE"];
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        /// Valido el parametro para la fecha maxima de registreo
        lblTitulo.Text = "Crear " + lsTitulo;
        int liDias = 0;
        string lsFechaMax = "";
        lConexion.Abrir();
        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
        if (lLector.HasRows)
        {
            lLector.Read();
            liDias = Convert.ToInt32(lLector["dias_max_reg_capacidad_tra"].ToString());
            lsFechaMax = DateTime.Now.Year.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + liDias.ToString();
        }
        lLector.Close();
        lLector.Dispose();
        lConexion.Cerrar();
        if (Convert.ToDateTime(TxtFecha.Text.Trim()) > Convert.ToDateTime(lsFechaMax))
        {
            lblMensaje.Text = "Se Vencio el Plazo Establecido para el Reporte de la Informacion.!";
            imbActualiza.Visible = false;
            imbCrear.Visible = false;
            tblCaptura.Visible = false;
        }
        else
        {
            tblCaptura.Visible = true;
            TxtCodigoCap.Visible = false;
            LblCodigoCap.Visible = true;
            LblCodigoCap.Text = "Automatico";
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            imbCrear.Visible = true;
            imbActualiza.Visible = false;
            lblTitulo.Text = "Crear " + lsTitulo;
            TxtFecha.Enabled = false;
            ddlEstado.Enabled = false;
        }
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_capacidad_transporte", " codigo_capacidad_trans = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoCap.Text = lLector["codigo_capacidad_trans"].ToString();
                        TxtCodigoCap.Text = lLector["codigo_capacidad_trans"].ToString();
                        TxtFecha.Text = lLector["fecha_registro"].ToString().Substring(6, 4) + "/" + lLector["fecha_registro"].ToString().Substring(3, 2) + "/" + lLector["fecha_registro"].ToString().Substring(0, 2);
                        try
                        {
                            ddlOperador.SelectedValue = lLector["codigo_Operador"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El operador del registro no existe o esta inactivo<br>";
                        }
                        try
                        {
                            ddlTramo.SelectedValue = lLector["codigo_tramo"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El tramo del registro no existe o esta inactivo<br>";
                        }
                        TxtCapacidadFirme.Text = lLector["capacidad_firme"].ToString();
                        TxtCapacidadDisp.Text = lLector["capacidad_disp_primaria"].ToString();
                        TxtFechaInio.Text = lLector["fecha_inicio"].ToString().Substring(6, 4) + "/" + lLector["fecha_inicio"].ToString().Substring(3, 2) + "/" + lLector["fecha_inicio"].ToString().Substring(0, 2);
                        TxtFechaFin.Text = lLector["fecha_fin"].ToString().Substring(6, 4) + "/" + lLector["fecha_fin"].ToString().Substring(3, 2) + "/" + lLector["fecha_fin"].ToString().Substring(0, 2);
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoCap.Visible = false;
                        LblCodigoCap.Visible = true;
                        TxtFecha.Enabled = false;
                        ddlTramo.Enabled = false;
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado. Codigo Capacidad" + modificar.ToString();

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {

        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_capacidad_trans", "@P_fecha_registro", "@P_codigo_operador", "@P_fecha_registro_fin" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "", "0", "" };
        DateTime ldFecha;
        lblMensaje.Text = "";

        try
        {
            if (TxtBusFecha.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtBusFecha.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Invalido en Fecha Inicial. <br>";
                }
            }
            if (TxtBusFechaF.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtBusFechaF.Text.Trim());
                    if (ldFecha < Convert.ToDateTime(TxtBusFecha.Text.Trim()))
                        lblMensaje.Text += "La Fecha final NO puede ser menor que la Fecha Inicial.!<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Invalido en Fecha Final. <br>";
                }
            }
            if (lblMensaje.Text == "")
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                if (TxtBusFecha.Text.Trim().Length > 0)
                    lValorParametros[1] = TxtBusFecha.Text.Trim();
                else
                    lValorParametros[1] = TxtFecha.Text.Trim();
                if (ddlBusOperador.SelectedValue != "0")
                    lValorParametros[2] = ddlBusOperador.SelectedValue;
                if (TxtBusFechaF.Text.Trim().Length > 0)
                    lValorParametros[3] = TxtBusFechaF.Text.Trim();
                else
                {
                    if (TxtBusFecha.Text.Trim().Length > 0)
                        lValorParametros[3] = TxtBusFecha.Text.Trim();
                    else
                        lValorParametros[3] = TxtFecha.Text.Trim();
                }
                lConexion.Abrir();
                dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetCapacidadTrans", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgMaestro.DataBind();
                lConexion.Cerrar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        if (TxtBusCodigo.Text.Trim().Length <= 0 && TxtBusFecha.Text.Trim().Length <= 0 && ddlBusOperador.SelectedValue == "0")
            lblMensaje.Text = "Debe Seleccionar al Menos un parametros para realizar la Busqueda!.";
        else
        {
            Listar();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    protected string Validaciones()
    {
        string lsError = "";
        int liValor = 0;
        DateTime ldFecha;

        if (ddlOperador.SelectedValue == "0")
            lsError += "debe seleccionar el operador<br>";
        if (ddlTramo.SelectedValue == "0")
            lsError += "debe seleccionar el Tramo<br>";
        if (TxtCapacidadFirme.Text.Trim() == "")
            lsError += " Debe digitar la Capacidad Firme<br>";
        else
        {
            try
            {
                liValor = Convert.ToInt32(TxtCapacidadFirme.Text.Trim());
                if (liValor <= 0)
                    lsError += " Valor Invalido en la Capacidad Firme<br>";
            }
            catch (Exception ex)
            {
                lsError += " Valor Invalido en la Capacidad Firme<br>";
            }
        }
        if (TxtCapacidadDisp.Text.Trim() == "")
            lsError += " Debe digitar la Capacidad Disponible Primaria<br>";
        else
        {
            try
            {
                liValor = Convert.ToInt32(TxtCapacidadDisp.Text.Trim());
                if (liValor <= 0)
                    lsError += " Valor Invalido en la Capacidad Disponible Primaria<br>";
            }
            catch (Exception ex)
            {
                lsError += " Valor Invalido en la Capacidad Disponible Primaria<br>";
            }
        }
        if (TxtFechaInio.Text.Trim() == "")
            lsError += " Debe digitar la Fecha de Inicio<br>";
        else
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaInio.Text.Trim());
            }
            catch (Exception ex)
            {
                lsError += " Valor Invalido en la Fecha de Inicio<br>";
            }
        }
        if (TxtFechaFin.Text.Trim() == "")
            lsError += " Debe digitar la Fecha de Finalizacion<br>";
        else
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaFin.Text.Trim());
            }
            catch (Exception ex)
            {
                lsError += " Valor Invalido en la Fecha de Finalizacion<br>";
            }
        }
        if (Convert.ToDateTime(TxtFechaInio.Text.Trim()) > Convert.ToDateTime(TxtFechaFin.Text.Trim()))
            lsError += " La Fecha de Inicio no puede ser Mayor que la Fecha de Finalizacion<br>";
        if (Convert.ToDateTime(TxtFechaInio.Text.Trim()) < Convert.ToDateTime(TxtFecha.Text.Trim()))
            lsError += " La Fecha de Inicio no puede ser Menor que la Fecha de Registro<br>";

        return lsError;
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            ddlTramo.Enabled = false;
            Modificar(lCodigoRegistro);
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("t_capacidad_transporte", lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }



    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='t_capacidad_transporte' and llave_registro='codigo_capacidad_trans=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_capacidad_trans=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "t_capacidad_transporte";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_capacidad_transporte", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "", "0", "" };
        string lsParametros = "";

        try
        {
            if (TxtBusCodigo.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusCodigo.Text.Trim();
                lsParametros += " Codigo Capacidad : " + TxtBusCodigo.Text;
            }
            if (TxtBusFecha.Text.Trim().Length > 0)
            {
                lValorParametros[1] = TxtBusFecha.Text.Trim();
                lsParametros += " Fecha Inicial : " + TxtBusFecha.Text;
            }
            else
            {
                lValorParametros[1] = TxtFecha.Text.Trim();
                lsParametros += " Fecha Inicial : " + TxtBusFecha.Text;
            }
            if (ddlBusOperador.SelectedValue != "0")
            {
                lValorParametros[2] = ddlBusOperador.SelectedValue;
                lsParametros += " Operador: " + ddlBusOperador.SelectedItem;
            }
            if (TxtBusFechaF.Text.Trim().Length > 0)
            {
                lValorParametros[3] = TxtBusFechaF.Text.Trim();
                lsParametros += " Fecha Final : " + TxtBusFechaF.Text;
            }
            else
            {
                if (TxtBusFecha.Text.Trim().Length > 0)
                {
                    lValorParametros[3] = TxtBusFecha.Text.Trim();
                    lsParametros += " Fecha Final : " + TxtBusFecha.Text;
                }
                else
                {
                    lValorParametros[3] = TxtFecha.Text.Trim();
                    lsParametros += " Fecha Final : " + TxtBusFecha.Text;
                }
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetCapacidadTrans&nombreParametros=@P_codigo_capacidad_trans*@P_fecha_registro*@P_codigo_operador*@P_fecha_registro_fin&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "*" + lValorParametros[3] + "&columnas=codigo_energia_inyectada*fecha*codigo_operador*nombre_operador*codigo_Pozo*desc_Pozo*codigo_modalidad*desc_modalidad*cantidad_inyectada*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado de Capacidad de Transporte&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_capacidad_trans <> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=t_capacidad_transporte&procedimiento=pa_ValidarExistencia&columnas=codigo_capacidad_trans*mes_reportado*fecha_registro*codigo_operador*codigo_tramo*capacidad_firme*capacidad_disp_primaria*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: LlenarTramo
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarTramo()
    {
        ddlTramo.Items.Clear();
        string[] lsNombreParametros = { "@P_codigo_tramo" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        string[] lValorParametros = { "0" };

        lConexion.Abrir();
        SqlDataReader lLector;
        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        ddlTramo.Items.Add(lItem);

        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetAyudaTramo", lsNombreParametros, lTipoparametros, lValorParametros);
        if (lLector.HasRows)
        {
            while (lLector.Read())
            {

                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(0).ToString();
                lItem1.Text = lLector.GetValue(1).ToString();
                ddlTramo.Items.Add(lItem1);
            }
        }
        lLector.Close();
        lConexion.Cerrar();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click1(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_capacidad_trans", "@P_fecha_registro", "@P_codigo_operador", "@P_codigo_tramo", "@P_capacidad_firme", "@P_capacidad_disp_primaria", "@P_fecha_inicio", "@P_fecha_fin", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "", "0", "0", "0", "0", "", "", "", "1" };
        lblMensaje.Text = "";

        try
        {
            lblMensaje.Text = Validaciones();
            if (lblMensaje.Text == "")
            {
                lValorParametros[1] = TxtFecha.Text;
                lValorParametros[2] = ddlOperador.SelectedValue;
                lValorParametros[3] = ddlTramo.SelectedValue;
                lValorParametros[4] = TxtCapacidadFirme.Text.Trim();
                lValorParametros[5] = TxtCapacidadDisp.Text.Trim();
                lValorParametros[6] = TxtFechaInio.Text.Trim();
                lValorParametros[7] = TxtFechaFin.Text.Trim();
                lValorParametros[8] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetCapacidadTrans", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Creación de la capacidad de Transporte.!";
                    lConexion.Cerrar();
                }
                else
                    Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click1(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_capacidad_trans", "@P_fecha_registro", "@P_codigo_operador", "@P_codigo_tramo", "@P_capacidad_firme", "@P_capacidad_disp_primaria", "@P_fecha_inicio", "@P_fecha_fin", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "", "0", "0", "0", "0", "", "", "", "2" };
        lblMensaje.Text = "";
        try
        {
            lblMensaje.Text = Validaciones();
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = LblCodigoCap.Text;
                lValorParametros[1] = TxtFecha.Text;
                lValorParametros[2] = ddlOperador.SelectedValue;
                lValorParametros[3] = ddlTramo.SelectedValue;
                lValorParametros[4] = TxtCapacidadFirme.Text.Trim();
                lValorParametros[5] = TxtCapacidadDisp.Text.Trim();
                lValorParametros[6] = TxtFechaInio.Text.Trim();
                lValorParametros[7] = TxtFechaFin.Text.Trim();
                lValorParametros[8] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetCapacidadTrans", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Actualizacion de la Capacidad de Transporte.!";
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigoCap.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoCap.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click1(object sender, EventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        lblMensaje.Text = "";
        if (LblCodigoCap.Text != "")
            manejo_bloqueo("E", LblCodigoCap.Text);
        Listar();
    }
}