﻿

<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_DeclaraCapacidadTrans.aspx.cs"
    Inherits="Verificacion_frm_DeclaraCapacidadTrans" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" bgcolor="#85BF46">
                <table border="0" cellspacing="2" cellpadding="2" align="right">
                    <tr>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkNuevo" runat="server" NavigateUrl="~/Verificacion/frm_DeclaraCapacidadTrans.aspx?lsIndica=N">Nuevo</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_DeclaraCapacidadTrans.aspx?lsIndica=L">Listar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_DeclaraCapacidadTrans.aspx?lsIndica=B">Consultar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/WebForms/Home.aspx">Salir</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click" />
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgPdf" runat="server" ImageUrl="~/Images/Pdf.png" OnClick="ImgPdf_Click"
                                Width="70%" />
                        </td>
                        <td class="tv2">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor="White" ></asp:Label>
            </td>
        </tr>
    </table>
    <br /><br /><br /><br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Código Capacidad Transporte
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCodigoCap" runat="server" MaxLength="3"></asp:TextBox>
                <asp:Label ID="LblCodigoCap" runat="server" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Fecha
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFecha" runat="server" MaxLength="10" Width="200px" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Operador
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlOperador" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Tramo
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlTramo" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">
                Capacidad Firme (KPCD)
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtCapacidadFirme" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTEBTxtCantidad" runat="server" TargetControlID="TxtCapacidadFirme"
                    FilterType="Custom, Numbers">
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">
                Capacidad Disponible Primaria (KPCD)
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtCapacidadDisp" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TxtCapacidadDisp"
                    FilterType="Custom, Numbers">
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Fecha Inicio {YYYY/MM/DD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaInio" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFecha" runat="server" ErrorMessage="Debe Ingresar la Fecha Inicio"
                    ControlToValidate="TxtFechaInio" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Fecha Fin {YYYY/MM/DD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RefTxtFechaFin" runat="server" ErrorMessage="Debe Ingresar la Fecha Fin"
                    ControlToValidate="TxtFechaFin" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Estado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlEstado" runat="server">
                    <asp:ListItem Value="A">Activo</asp:ListItem>
                    <asp:ListItem Value="I">Inactivo</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:Button ID="imbCrear" runat="server" Text="Crear" OnClientClick="this.disabled = true;"
                    UseSubmitBehavior="false" OnClick="imbCrear_Click1" />
                <asp:Button ID="imbActualiza" runat="server" Text="Actualizar" OnClientClick="this.disabled = true;"
                    UseSubmitBehavior="false" OnClick="imbActualiza_Click1" />
                <asp:Button ID="imbSalir" runat="server" Text="Salir" OnClientClick="this.disabled = true;"
                    UseSubmitBehavior="false" OnClick="imbSalir_Click1" />
                <%--                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
--%>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblBuscar" visible="false">
        <tr>
            <td class="td1">
                Código Capacidad Transporte
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusCodigo" runat="server" autocomplete="off"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTBETxtBusCodigo" runat="server" TargetControlID="TxtBusCodigo"
                    FilterType="Custom, Numbers">
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Fecha Registro
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusFecha" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                -
                <asp:TextBox ID="TxtBusFechaF" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Operador
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusOperador" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
        width="80%">
        <tr>
            <td colspan="2">
                <div style="overflow: scroll; width: 1020px; height: 450px;">
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged"
                        HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="codigo_capacidad_trans" HeaderText="Código Capacidad Transporte" 
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn> <%--20180815 BUG230--%>
                            <asp:BoundColumn DataField="mes_reportado" HeaderText="Mes Reportado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_registro" HeaderText="Fecha Registro" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código operador" ItemStyle-HorizontalAlign="Left"> <%--20180815 BUG230--%>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_tramo" HeaderText="código Tramo" ItemStyle-HorizontalAlign="Left"> <%--20180815 BUG230--%>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tramo" HeaderText="Desc Tramo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="capacidad_firme" HeaderText="Capacidad Firme" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="capacidad_disp_primaria" HeaderText="Capacidad disp Primaria"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_inicio" HeaderText="Fecha Inicio" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_fin" HeaderText="Fecha Fin" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualización" ItemStyle-HorizontalAlign="Left"> <%--20180815 BUG230--%>
                            </asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass="">
                            </asp:EditCommandColumn>
                        </Columns>
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>