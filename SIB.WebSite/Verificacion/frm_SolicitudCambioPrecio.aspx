﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_SolicitudCambioPrecio.aspx.cs"
    Inherits="Verificacion_frm_SolicitudCambioPrecio" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>
            </div>
            <%--Contenido--%>
            <%--Captura--%>
            <div class="kt-portlet__body" runat="server" id="tblDatos">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Número Operación <%--20180302 rq004-18--%></label>
                            <asp:TextBox ID="TxtNoContrato" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Número Id Registro <%--20180302 rq004-18--%></label>
                            <asp:TextBox ID="TxtNoIdRegistro" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Número Contrato<%--20180302 rq004-18--%></label>
                            <asp:TextBox ID="TxtContratoDef" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Aprobado Comprador</label>
                            <asp:DropDownList ID="ddlAprobadoCompra" runat="server" CssClass="form-control selectpicker">
                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="N" Text="NO"></asp:ListItem>
                                <asp:ListItem Value="S" Text="SI"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Operador</label>
                            <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Operador Contraparte<%--20180302 rq004-18--%></label>
                            <asp:DropDownList ID="dldContraparte" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Producto<%--20180302 rq004-18--%></label>
                            <asp:DropDownList ID="ddlProducto" runat="server" CssClass="form-control selectpicker">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <%--20180302 rq004-18--%>
                                <asp:ListItem Value="T">Capacidad de transporte</asp:ListItem>
                                <%--20180302 rq004-18--%>
                                <asp:ListItem Value="G">Suministro de gas</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="btnConsultar" runat="server" CssClass="btn btn-success" Text="Consultar" OnClick="btnConsultar_Click" ValidationGroup="detalle" />
                            <asp:Button ID="bgtnAprobar" runat="server" CssClass="btn btn-success" Text="Aprobar" OnClick="Button1_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="btnRechazoMas" runat="server" CssClass="btn btn-success" Text="Rechazar" OnClick="btnRechazoMas_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:LinkButton ID="lkbExcel" runat="server" CssClass="btn btn-outline-brand btn-square" OnClick="lkbExcel_Click">Exportar Excel</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Observacion</label>
                            <asp:TextBox ID="TxtObseAprob" runat="server" ValidationGroup="detalle" TextMode="MultiLine" Rows="2" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>

            </div>
            <%--Mensaje--%>
            <div class="kt-portlet__body" runat="server" id="tblMensaje">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <%--Grilla--%>
            <div class="kt-portlet__body" runat="server" id="tblGrilla" visible="false">
                <div class="row">
                    <div class="table table-responsive">
                        <div style="overflow: scroll; height: 450px;">
                            <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                Width="100%" CssClass="table-bordered" OnEditCommand="dtgConsulta_EditCommand">
                                <Columns>
                                    <%--0--%>
                                    <asp:BoundColumn DataField="numero_operacion" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20180302 rq004-18--%>
                                    <%--1--%>
                                    <asp:BoundColumn DataField="numero_id_registro" HeaderText="No. Registro"></asp:BoundColumn>
                                    <%--2--%>
                                    <asp:BoundColumn DataField="tipo_subasta" HeaderText="Tipo Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--3--%>
                                    <asp:BoundColumn DataField="mercado" HeaderText="Mercado" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="80px"></asp:BoundColumn>
                                    <%--4--%>
                                    <asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--5--%>
                                    <asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--6--%><%--2018302 rq004-18--%>
                                    <asp:BoundColumn DataField="contrato_definitivo" HeaderText="contrato" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--7--%><%--2018302 rq004-18--%>
                                    <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left"
                                        DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                    <%--8--%><%--2018302 rq004-18--%>
                                    <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left"
                                        DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                    <%--9--%>
                                    <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--10--%>
                                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--11--%>
                                    <asp:BoundColumn DataField="precio_actual" HeaderText="Precio Actual" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--12--%>
                                    <asp:BoundColumn DataField="codigo_solicitud" HeaderText="No. Solicitud Cambio Precio"
                                        ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--13--%>
                                    <asp:BoundColumn DataField="aprobado" HeaderText="Aprobado" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <%--14--%>
                                    <asp:BoundColumn DataField="aprobado" HeaderText="Aprobado" ItemStyle-HorizontalAlign="Center"
                                        Visible="false"></asp:BoundColumn>
                                    <%--15--%>
                                    <asp:BoundColumn DataField="observacion_bmc" HeaderText="Observación BMC" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20180302 rq004-18--%>
                                    <%--16--%>
                                    <asp:BoundColumn DataField="operador_compra" Visible="false"></asp:BoundColumn>
                                    <%--17--%>
                                    <asp:BoundColumn DataField="operador_venta" Visible="false"></asp:BoundColumn>
                                    <%--18--%>
                                    <asp:BoundColumn DataField="codigo_tipo_subasta" Visible="false"></asp:BoundColumn>
                                    <%--19--%>
                                    <asp:BoundColumn DataField="destino_rueda" Visible="false"></asp:BoundColumn>
                                    <%--20--%>
                                    <asp:BoundColumn DataField="sigla_estado" Visible="false"></asp:BoundColumn>
                                    <%--21--%>
                                    <asp:BoundColumn DataField="tipo_mercado" Visible="false"></asp:BoundColumn>
                                    <%--22--%>
                                    <asp:EditCommandColumn HeaderText="Solicitar" EditText="Solicitud Cambio"></asp:EditCommandColumn>
                                    <%--23--%>
                                    <asp:EditCommandColumn HeaderText="Aprobar" EditText="Aprobación"></asp:EditCommandColumn>
                                    <%--20180302 rq004-18--%>
                                    <%--24--%>
                                    <asp:BoundColumn DataField="nuevo_precio" HeaderText="Nuevo Precio"></asp:BoundColumn>
                                    <%--25--%>
                                    <asp:BoundColumn DataField="ruta_archivo" Visible="false"></asp:BoundColumn>
                                    <%--26--%>
                                    <asp:BoundColumn DataField="puede_solicitar" Visible="false"></asp:BoundColumn>
                                    <%--27--%>
                                    <asp:BoundColumn DataField="puerde_aprobar" Visible="false"></asp:BoundColumn>
                                    <%--28--%><asp:TemplateColumn HeaderText="Sele.">
                                        <ItemTemplate>
                                            <label class="kt-checkbox">
                                                <asp:CheckBox ID="chkRecibir" runat="server" />
                                                <span></span>
                                            </label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--29--%> <%--20180302 rq004-18--%>
                                    <asp:BoundColumn DataField="fecha_cambio" HeaderText="Fecha Cambio Precio" Visible="false"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
            </div>
            <%--Solicitud--%>
            <div class="kt-portlet__body" runat="server" id="tblSolicitud" visible="false">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Numero Operación</label>
                            <asp:Label ID="lblOperacion" runat="server" ForeColor="Red" Font-Bold="true" CssClass="form-control"></asp:Label>
                            <asp:HiddenField ID="hdfIdVerif" runat="server" />
                            <asp:HiddenField ID="hdfNoSolicitud" runat="server" />
                            <asp:HiddenField ID="hdfNoContrato" runat="server" />
                            <asp:HiddenField ID="hdfOPeraC" runat="server" />
                            <asp:HiddenField ID="hdfOPeraV" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Subasta</label>
                            <asp:Label ID="lblSubasta" runat="server" ForeColor="Red" Font-Bold="true" CssClass="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Precio</label>
                            <asp:Label ID="lblPrecio" runat="server" ForeColor="Red" Font-Bold="true" CssClass="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Nuevo Precio</label>
                            <asp:TextBox ID="TxtPrecio" runat="server" ValidationGroup="detalle" CssClass="form-control" MaxLength="30"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Copia del Contrato</label>
                            <asp:FileUpload ID="FuArchivo" BackColor="#dddddd" runat="server" EnableTheming="true" />
                            <asp:HyperLink ID="HplArchivo" runat="server">Ver Archivo</asp:HyperLink>
                        </div>
                    </div>
                </div>
                <div class="row" runat="server" visible="false" id="TrBmc">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Observación</label>
                            <asp:TextBox ID="TxtObservacion" runat="server" ValidationGroup="detalle" CssClass="form-control" TextMode="MultiLine" Rows="2"></asp:TextBox>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="btnActualizar" runat="server" CssClass="btn btn-success" Text="Actualizar" OnClick="btnActualizar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="btnAprobar" runat="server" CssClass="btn btn-success" Text="Aprobar" OnClick="btnAprobar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="brnRechazar" runat="server" CssClass="btn btn-success" Text="Rechazar" OnClick="brnRechazar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="btnRegresar" runat="server" CssClass="btn btn-secondary" Text="Regresar" OnClick="btnRegresar_Click" Visible="false" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Bold="true" CssClass="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
