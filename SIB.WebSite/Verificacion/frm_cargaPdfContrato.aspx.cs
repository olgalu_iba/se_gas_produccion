﻿using System;
using System.Collections;
using System.Globalization;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.DataVisualization.Charting;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Verificacion_frm_cargaPdfContrato : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "carga PDF de contratos registrados";
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    String strRutaCarga;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);
        strRutaCarga = ConfigurationManager.AppSettings["RutaCargaContPdf"].ToString();
        
        if (!IsPostBack)
        {    
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlComprador, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlVendedor, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4);
            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlVendedor.SelectedValue = goInfo.cod_comisionista;
                ddlVendedor.Enabled = false;
            }
        }
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        CargarDatos();
    }
    /// <summary>
    /// Nombre: dtgConsulta_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgConsulta.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF = DateTime.Now;
        int liValor = 0;
        string[] lsNombreParametros = { "@P_fecha_ini", "@P_fecha_fin", "@P_operacion", "@P_contrato_def", "@P_codigo_vendedor", "@P_codigo_comprador", "@P_tipo_mercado" };
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
        string[] lValorParametros = { "", "", "0", "", "0", "0", "" };

        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo fecha inicial <br>";
            }

        }
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo fecha final<br>";
            }
        }
        if (TxtFechaFin.Text.Trim().Length > 0 && TxtFechaIni.Text.Trim().Length == 0)
            lblMensaje.Text += "Debe digitar la fecha inicial antes que la final<br>";

        if (TxtFechaFin.Text.Trim().Length > 0 && TxtFechaIni.Text.Trim().Length > 0)
            if (ldFechaI > ldFechaF)
                lblMensaje.Text += "La fecha inicial debe ser menor o igual  que la final<br>";

        if (lblMensaje.Text == "")
        {
            try
            {
                if (TxtFechaIni.Text != "")
                {
                    lValorParametros[0] = TxtFechaIni.Text.Trim();
                    if (TxtFechaFin.Text != "")
                        lValorParametros[1] = TxtFechaFin.Text.Trim();
                    else
                        lValorParametros[1] = TxtFechaIni.Text.Trim();
                }
                if (TxtNoOper.Text.Trim().Length > 0)
                    lValorParametros[2] = TxtNoOper.Text.Trim();
                lValorParametros[3] = TxtContratoDef.Text.Trim();
                lValorParametros[4] = ddlVendedor.SelectedValue;
                lValorParametros[5] = ddlComprador.SelectedValue;
                lValorParametros[6] = ddlMercado.SelectedValue;

                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContCargaPdf", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                //20190607 rq036-19
                dtgExcel.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContCargaPdf", lsNombreParametros, lTipoparametros, lValorParametros);
                //20190607 rq036-19
                dtgExcel.DataBind();
                lConexion.Cerrar();
                if (dtgConsulta.Items.Count > 0)
                {
                    tblGrilla.Visible = true;
                    imbExcel.Visible = true;
                }
                else
                {
                    tblGrilla.Visible = false;
                    imbExcel.Visible = false;
                    lblMensaje.Text = "No se encontraron Registros.";
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        try
        {
            lblOperacion.Text = e.Item.Cells[0].Text;
            lblFechaNeg.Text = e.Item.Cells[3].Text;
            lblIdRegistro.Text = e.Item.Cells[1].Text;
            lblContrato.Text = e.Item.Cells[2].Text;
            lblComprador.Text = e.Item.Cells[5].Text;
            lblVendedor.Text = e.Item.Cells[4].Text;
            lblCantidad.Text = e.Item.Cells[8].Text;
            lblPrecio.Text = e.Item.Cells[9].Text;
            lblFechaIni.Text = e.Item.Cells[6].Text;
            lblFechaFin.Text = e.Item.Cells[7].Text;
            hndVendedor.Value = e.Item.Cells[11].Text;
            CargarDet();

            tblPdf.Visible = true;
            tblDatos.Visible = false;
            tblGrilla.Visible = false;
            imbExcel.Visible = false;
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas en la Recuperación de la Información. " + ex.Message.ToString();

        }
    }


    protected void btnRegresar_Click(object sender, EventArgs e)
    {
        tblPdf.Visible = false;
        tblDatos.Visible = true;
        tblGrilla.Visible = true;
        imbExcel.Visible = true;
    }

    protected void btnCargar_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        try
        {
            if (FuArchivo.FileName.ToString() == "")
                lblMensaje.Text = "Debe seleccionar el archivo";
            //20200430 ajuste nombre archivos
            if (!(Directory.Exists(strRutaCarga + hndVendedor.Value)))
                Directory.CreateDirectory(strRutaCarga + hndVendedor.Value);
            int liNumero = dtgPdf.Items.Count +1 ;
            string lsNumero = "";
            string lsArchivo = "";
            int liCOnta = 0;
            while (lsArchivo == "" && liCOnta < 10)
            {
                lsNumero = DateTime.Now.Millisecond.ToString();
                lsArchivo = DateTime.Now.ToString("yyyy_MM_dd") + "_" + hndVendedor.Value  + "_" + lblIdRegistro.Text +"_"+ liNumero.ToString()+"_" + lsNumero + Path.GetExtension(FuArchivo.FileName).ToUpper();
                if (File.Exists(strRutaCarga + hndVendedor.Value + "\\" + lsArchivo) )
                {
                    lsArchivo = "";
                }
                liCOnta++;
            }
            if (lsArchivo == "")
                lblMensaje.Text += "No se pueden cargar los archivos. Intente de nuevo<br>";

            //20200430 FIN ajuste nombre archivos
            if (lblMensaje.Text == "")
            {
                string[] lsNombreParametros = { "@P_codigo_verif", "@P_nombre_archivo", "@P_nombre_archivo_org", "@P_indica" };//20200430 ajuste nombre archivos
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };//20200430 ajuste nombre archivos
                string[] lValorParametros = { lblIdRegistro.Text, lsArchivo, FuArchivo.FileName.ToString(), "C" }; //20200430 ajuste nombre archivos

                lConexion.Abrir();
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetContratoPdf", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensaje.Text = "Se presentó un Problema en la carga del contrato.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                            lblMensaje.Text += lLector["error"].ToString() + "<br>";
                    }
                    else
                    {
                        try
                        {
                            //20200430 ajuste nombre archivos
                            //if (!(Directory.Exists(strRutaCarga + hndVendedor.Value )))
                            //    Directory.CreateDirectory(strRutaCarga + hndVendedor.Value );
                            //FuArchivo.SaveAs(strRutaCarga + hndVendedor.Value  + "\\" + FuArchivo.FileName.ToString());//20200430 ajuste nombre archivos
                            FuArchivo.SaveAs(strRutaCarga + hndVendedor.Value + "\\" + lsArchivo );//20200430 ajuste nombre archivos
                            CargarDet();
                        }
                        catch (Exception ex)
                        {
                            string[] lsNombreParametrosA = { "@P_codigo_verif", "@P_nombre_archivo", "@P_indica" };
                            SqlDbType[] lTipoparametrosA = { SqlDbType.Int, SqlDbType.VarChar,  SqlDbType.VarChar };
                            string[] lValorParametrosA = { lblIdRegistro.Text, lsArchivo ,  "A" };//20200430 ajuste nombre archivos

                            DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContratoPdf", lsNombreParametrosA, lTipoparametrosA, lValorParametrosA, goInfo);
                        }
                    }
                    lConexion.Cerrar();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
            lConexion.Cerrar();
        }
    }
    
    
    /// <summary>
    /// Exportacion a Excel de la Información  de la Grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbExcel_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        //20190607 rq036-19
        try
        {
            string lsNombreArchivo = Session["login"] + "InfContPdf" + DateTime.Now + ".xls";
            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            lpagina.EnableEventValidation = false;
            lpagina.Controls.Add(lform);
            dtgExcel.EnableViewState = false;
            dtgExcel.Visible = true;
            lform.Controls.Add(dtgExcel);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;

            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + "Consulta PDF de contratos registrados" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write(lsb.ToString());
            Response.End();
            Response.Flush();
            dtgExcel.Visible = true;
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas al Consultar los Registros. " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected void CargarDet()
    {
        string[] lsNombreParametros = { "@P_codigo_verif" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        string[] lValorParametros = { lblIdRegistro.Text };
        lConexion.Abrir();
        dtgPdf.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContCargaPdfDet", lsNombreParametros, lTipoparametros, lValorParametros);
        dtgPdf.DataBind();

    }

    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgPdf_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Ver")
        {
            try
            {
                string lsCarpetaAnt = "";
                string[] lsCarperta;
                lsCarperta = strRutaCarga.Split('\\');
                lsCarpetaAnt = lsCarperta[lsCarperta.Length - 2];
                string lsRuta = "../" + lsCarpetaAnt + "/"+ hndVendedor.Value +'/' + e.Item.Cells[1].Text.Replace(@"\", "/");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + lsRuta + "');", true);
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Error al visualizar el archivo. " + ex.Message.ToString();
            }
        }
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            try
            {
                string[] lsNombreParametros = { "@P_codigo_contrato_pdf", "@P_indica" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int,  SqlDbType.VarChar };
                string[] lValorParametros = { e.Item.Cells[0].Text, "E" };

                File.Delete(strRutaCarga + hndVendedor.Value + '/' + e.Item.Cells[1].Text.Replace(@"\", "/"));
                lConexion.Abrir();
                DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContratoPdf", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                lConexion.Cerrar();
                CargarDet();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Error al eliminar el archivo. " + ex.Message.ToString();
            }
        }
    }

}