﻿using System;
using System.Data;
using System.Data.SqlClient;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Verificacion_frm_EntregaUsuarioFinalDet : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    SqlDataReader lLector;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);

        string[] lsNombreParametros = { "@P_codigo_ent_usuario_final" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        string[] lValorParametros = { "0" };
        try
        {
            lValorParametros[0] = this.Request.QueryString["codigo_ent"];

            lConexion.Abrir();
            dtgDetalle.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetEntregaUsuarioFinalDet", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgDetalle.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception)
        {
        }

    }
}