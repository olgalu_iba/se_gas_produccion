﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_cargaPdfContrato.aspx.cs"
    Inherits="Verificacion_frm_cargaPdfContrato" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link href="../css/estilo.css" rel="stylesheet" type="text/css" />
</head>
<body background="../Images/IMG_FondoGrisDeg.png">
    <form id="frmAuditoria" runat="server">
        <asp:ScriptManager ID="ScriptManagerMestro" runat="server" AsyncPostBackTimeout="1000">
        </asp:ScriptManager>
        <div>
            <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
                width="100%">
                <tr>
                    <td align="center" class="th1" style="width: 80%;">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </td>
                    <td align="center" class="th1" style="width: 20%;">
                        <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/exel 2007 3D.png"
                            Height="40" OnClick="imbExcel_Click" />
                    </td>
                </tr>
            </table>
            <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
                width="100%">
                <tr>
                    <td class="td1">Fecha Negociación
                        <asp:HiddenField ID="hndVendedor" runat="server" />
                    </td>
                    <td class="td2">
                        <asp:TextBox ID="TxtFechaIni" runat="server" ValidationGroup="detalle" Width="80px"></asp:TextBox>-
                    <cc1:CalendarExtender ID="ceTxtFechaIni" runat="server" TargetControlID="TxtFechaIni"
                        Format="yyyy/MM/dd">
                    </cc1:CalendarExtender>
                        <asp:TextBox ID="TxtFechaFin" runat="server" ValidationGroup="detalle" Width="80px"></asp:TextBox>
                        <cc1:CalendarExtender ID="CeTxtFechaFin" runat="server" TargetControlID="TxtFechaFin"
                            Format="yyyy/MM/dd">
                        </cc1:CalendarExtender>
                    </td>
                    <td class="td1">Operación
                    </td>
                    <td class="td2">
                        <asp:TextBox ID="TxtNoOper" runat="server" ValidationGroup="detalle" Width="80px"></asp:TextBox>-
                        <cc1:FilteredTextBoxExtender ID="FTEBTxtNoOper" runat="server" TargetControlID="TxtNoOper"
                            FilterType="Custom, Numbers">
                        </cc1:FilteredTextBoxExtender>
                    </td>
                    <td class="td1">Contrato
                    </td>
                    <td class="td2">
                        <asp:TextBox ID="TxtContratoDef" runat="server" ValidationGroup="detalle" Width="80px" MaxLength="30"></asp:TextBox>-
                    </td>
                    <td class="td1">Vendedor
                    </td>
                    <td class="td2">
                        <asp:DropDownList ID="ddlVendedor" runat="server" Width="180px">
                        </asp:DropDownList>
                    </td>
                    <td class="td1">Comprador
                    </td>
                    <td class="td2">
                        <asp:DropDownList ID="ddlComprador" runat="server" Width="180px">
                        </asp:DropDownList>
                    </td>
                    <td class="td1">Tipo Mercado
                    </td>
                    <td class="td2">
                        <asp:DropDownList ID="ddlMercado" runat="server" Width="180px">
                            <asp:ListItem Value="">Seleccione</asp:ListItem>
                            <asp:ListItem Value="P">Primario</asp:ListItem>
                            <asp:ListItem Value="S">Secundario</asp:ListItem>
                            <asp:ListItem Value="O">OTMM</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="th1" colspan="12" align="center">
                        <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                            OnClick="imbConsultar_Click" Height="40" />
                    </td>
                </tr>
            </table>
        </div>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblMensaje">
            <tr>
                <td colspan="3" align="center">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
            id="tblGrilla" visible="false">
            <tr>
                <td colspan="3" align="center">
                    <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                        AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1"
                        OnEditCommand="dtgConsulta_EditCommand" AllowPaging="true" PageSize="20" OnPageIndexChanged="dtgConsulta_PageIndexChanged">
                        <Columns>
                            <%--0--%>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--1--%>
                            <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--2--%>
                            <asp:BoundColumn DataField="contrato_definitivo" HeaderText="No. Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--3--%>
                            <asp:BoundColumn DataField="fecha_negociacion" HeaderText="fecha Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--4--%>
                            <asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--5--%>
                            <asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--6--%>
                            <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicio" ItemStyle-HorizontalAlign="Left"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <%--7--%>
                            <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Fin" ItemStyle-HorizontalAlign="Left"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <%--8--%>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <%--9--%>
                            <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            <%--10--%>
                            <asp:BoundColumn DataField="archivos" Visible="false"></asp:BoundColumn>
                            <%--11--%>
                            <asp:BoundColumn DataField="operador_venta" Visible="false"></asp:BoundColumn>
                            <%--12--%>
                            <asp:EditCommandColumn HeaderText="Cargar PDF" EditText="Cargar PDF"></asp:EditCommandColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                    </asp:DataGrid>
                    <%--20190607 rq036-19--%>
                    <asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False" Visible="false"
                        AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <Columns>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="contrato_definitivo" HeaderText="No. Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_negociacion" HeaderText="fecha Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicio" ItemStyle-HorizontalAlign="Left"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Fin" ItemStyle-HorizontalAlign="Left"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="archivos" HeaderText="Pdf Contrato"></asp:BoundColumn>

                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
        <table id="tblPdf" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
            width="100%" visible="false">
            <tr>
                <td class="td1">Operación
                </td>
                <td class="td2">
                    <asp:Label ID="lblOperacion" runat="server"></asp:Label>
                </td>
                <td class="td1">Fecha Negociación
                </td>
                <td class="td2">
                    <asp:Label ID="lblFechaNeg" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="td1">Id registro
                </td>
                <td class="td2">
                    <asp:Label ID="lblIdRegistro" runat="server"></asp:Label>
                </td>
                <td class="td1">Número Contrato
                </td>
                <td class="td2">
                    <asp:Label ID="lblContrato" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="td1">Comprador
                </td>
                <td class="td2">
                    <asp:Label ID="lblComprador" runat="server"></asp:Label>
                </td>
                <td class="td1">Vendedor
                </td>
                <td class="td2">
                    <asp:Label ID="lblVendedor" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="td1">Cantidad
                </td>
                <td class="td2">
                    <asp:Label ID="lblCantidad" runat="server"></asp:Label>
                </td>
                <td class="td1">Precio
                </td>
                <td class="td2">
                    <asp:Label ID="lblPrecio" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="td1">Fecha Inicial
                </td>
                <td class="td2">
                    <asp:Label ID="lblFechaIni" runat="server"></asp:Label>
                </td>
                <td class="td1">Fecha Inicial
                </td>
                <td class="td2">
                    <asp:Label ID="lblFechaFin" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="th1" colspan="4" align="center"></td>
            </tr>
            <tr>
                <td class="td1">Seleccione el Archivo:
                </td>
                <td class="td2">
                    <asp:FileUpload ID="FuArchivo" runat="server" EnableTheming="true" />
                </td>
            </tr>
            <tr>
                <td class="th1" colspan="4" align="center">
                    <asp:Button ID="btnCargar" runat="server" Text="Cargar" OnClick="btnCargar_Click"
                        OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                    <asp:Button ID="btnRegresar" runat="server" Text="Regresar" OnClick="btnRegresar_Click" />
                </td>
            </tr>
            <tr>
                <td class="th1" colspan="4" align="center">Archivos ya cargados
                </td>
            </tr>
            <tr>
                <td class="td1" colspan="4" align="center">
                    <asp:DataGrid ID="dtgPdf" runat="server" AutoGenerateColumns="False"
                        AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1" OnEditCommand="dtgPdf_EditCommand">
                        <Columns>
                            <asp:BoundColumn DataField="codigo_contrato_pdf" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_archivo" HeaderText="Archivo Sistema" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px"></asp:BoundColumn> <%--20200430--%>
                            <asp:BoundColumn DataField="nombre_archivo_org" HeaderText="Nombre Archivo Original" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px"></asp:BoundColumn> <%--20200430--%>
                            <asp:EditCommandColumn HeaderText="Ver" EditText="Ver" ItemStyle-Width="100px"></asp:EditCommandColumn>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar" ItemStyle-Width="100px"></asp:EditCommandColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
