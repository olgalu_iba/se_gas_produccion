﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Verificacion_frm_EnergiaConsumida : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Cantidad de Energía Consumida"; //20160810 modalidad por tipo campo
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        //EstablecerPermisosSistema(); //20220621 ajsute
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);
        //Titulo
        Master.Titulo = "Cantidad de Energía Consumida";
        /// Activacion de los Botones
        buttons.Inicializar(ruta: "t_energia_consumida");
        buttons.CrearOnclick += btnNuevo;
        
        buttons.FiltrarOnclick += btnConsulta;
        buttons.ExportarExcelOnclick += ImgExcel_Click;
        buttons.ExportarPdfOnclick += ImgPdf_Click;

        if (!IsPostBack)
        {
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlPozo, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles1(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and (tipo_operador in ('P','I') or codigo_operador = " + goInfo.cod_comisionista + ") and codigo_operador >0 order by razon_social", 0, 4); //20180122 rq003-18 // 20180126 rq107-16
            LlenarControles(lConexion.gObjConexion, ddlModalidad, "m_modalidad_contractual", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlBusPozo, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles1(lConexion.gObjConexion, ddlBusOperador, "m_operador", " estado = 'A' and ( tipo_operador in ('P','I') or codigo_operador = " + goInfo.cod_comisionista + ") and codigo_operador>0 order by razon_social", 0, 4); //20180122 rq003-18 // 20180126 rq107-16
            lConexion.Cerrar();
            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlBusOperador.SelectedValue = goInfo.cod_comisionista;
                ddlBusOperador.Enabled = false;
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
                hdnELiminado.Value = "N";  //20160721 modif inf opertiva y transaccional
            }
            else
                hdnELiminado.Value = "S";  //20160721 modif inf opertiva y transaccional

            DateTime ldfecha;
            ldfecha = DateTime.Now.AddDays(-1);
            TxtFecha.Text = ldfecha.Year.ToString() + "/" + ldfecha.Month.ToString() + "/" + ldfecha.Day.ToString();
            TxtFecha.Enabled = false;

            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            Listar();
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "t_energia_consumida");
        //hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        //hlkListar.Enabled = (Boolean)permisos["SELECT"];
        //hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        //20190516 ajuste
        //if (Session["tipoPerfil"].ToString() != "N")
        //    dtgMaestro.Columns[11].Visible = (Boolean)permisos["UPDATE"];
        //else
        dtgMaestro.Columns[11].Visible = false;
        dtgMaestro.Columns[12].Visible = false;
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        int liHoraAct = 0;
        string lsHoraMax = "0";
        int liValFin = 0;
        lConexion.Abrir();
        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
        if (lLector.HasRows)
        {
            lLector.Read();
            lsHoraMax = lLector["hora_max_declara_opera"].ToString();
        }
        lLector.Close();
        lLector.Dispose();
        lConexion.Cerrar();
        liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
        liValFin = ((Convert.ToDateTime(lsHoraMax).Hour * 60) + Convert.ToDateTime(lsHoraMax).Minute);
        if (liHoraAct > liValFin)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Está intentando Registrar Información Fuera del Horario." + "');", true);
            tblCaptura.Visible = false;
            tblgrilla.Visible = true;
        }
        else
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            imbCrear.Visible = true;
            imbActualiza.Visible = false;
            lblTitulo.Text = "Crear " + lsTitulo;
            TxtCodigoCap.Visible = false;
            LblCodigoCap.Visible = true;
            LblCodigoCap.Text = "Automático"; //20180126 rq107-16
            ddlOperador.Enabled = true;
            ddlModalidad.Enabled = true;
        }
        ddlEstado.SelectedValue = "A"; //20161124  modif inf ope
        ddlEstado.Enabled = false; //20161124 modif inf ope
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_energia_consumida", " codigo_energia_consumida= " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoCap.Text = lLector["codigo_energia_consumida"].ToString();
                        TxtCodigoCap.Text = lLector["codigo_energia_consumida"].ToString();
                        DateTime ldFecha; //20160711 ajuste msificaciones
                        ldFecha = Convert.ToDateTime(lLector["fecha"].ToString()); //20160711 ajuste msificaciones
                        TxtFecha.Text = ldFecha.Year.ToString() + "/" + ldFecha.Month.ToString() + "/" + ldFecha.Day.ToString(); //20160711 ajuste msificaciones

                        try
                        {
                            ddlOperador.SelectedValue = lLector["codigo_Operador"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El operador del registro no existe o está inactivo<br>";
                        }
                        try
                        {
                            ddlPozo.SelectedValue = lLector["codigo_Pozo"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El punto del SNT del registro no existe o está inactivo<br>";
                        }
                        try
                        {
                            ddlModalidad.SelectedValue = lLector["codigo_modalidad"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El tipo de contrato del registro no existe o está inactivo<br>";
                        }
                        TxtCantidad.Text = lLector["cantidad_consumida"].ToString();
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoCap.Visible = false;
                        LblCodigoCap.Visible = true;
                        TxtFecha.Enabled = false;
                        ddlOperador.Enabled = false;
                        //ddlModalidad.Enabled = false; //20161124 modif inf ope
                        ddlEstado.Enabled = true; //20161124 modif inf ope
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    if (lblMensaje.Text == "") //20160809 carga ptdv
                        manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que está Bloqueado. Código energía consumida" + modificar.ToString();

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {

        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_energia_consumida", "@P_fecha", "@P_codigo_operador", "@P_codigo_Pozo", "@P_fecha_fin", "@P_mostrar_eliminado" }; //20160721 modif inf opertiva y trans
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar }; //20160721 modif inf opertiva y trans
        string[] lValorParametros = { "0", "", "0", "0", "", hdnELiminado.Value }; //20160721 modif inf opertiva y trans
        DateTime ldFecha;
        lblMensaje.Text = "";

        try
        {
            if (TxtBusFecha.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtBusFecha.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Fecha Inicial. <br>";
                }
            }
            if (TxtBusFechaF.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtBusFechaF.Text.Trim());
                    if (ldFecha < Convert.ToDateTime(TxtBusFecha.Text.Trim()))
                        lblMensaje.Text += "La Fecha final NO puede ser menor que la Fecha Inicial.!<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Fecha Final. <br>";
                }
            }
            if (lblMensaje.Text == "")
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                if (TxtBusFecha.Text.Trim().Length > 0)
                    lValorParametros[1] = TxtBusFecha.Text.Trim();
                else
                    lValorParametros[1] = ""; // 20181113 ajuste
                if (ddlBusOperador.SelectedValue != "0")
                    lValorParametros[2] = ddlBusOperador.SelectedValue;
                if (ddlBusPozo.SelectedValue != "0")
                    lValorParametros[3] = ddlBusPozo.SelectedValue;
                if (TxtBusFechaF.Text.Trim().Length > 0)
                    lValorParametros[4] = TxtBusFechaF.Text.Trim();
                else
                {
                    if (TxtBusFecha.Text.Trim().Length > 0)
                        lValorParametros[4] = TxtBusFecha.Text.Trim();
                    else
                        lValorParametros[4] = ""; // 20181113 ajuste
                }
                lConexion.Abrir();
                dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetEnergiaRec", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgMaestro.DataBind();
                lConexion.Cerrar();
                EstablecerPermisosSistema(); //20220621 ajuste
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            ddlPozo.Enabled = false;
            Modificar(lCodigoRegistro);
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("t_energia_consumida", lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }

    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    /// 20180122 rq003-18
    /// /// // 20180126 rq107-16
    protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceLlave).ToString() + "-" + lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='t_energia_consumida' and llave_registro='codigo_energia_consumida=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_energia_consumida=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "t_energia_consumida";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_energia_consumida", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, EventArgs e)
    {
        string[] lValorParametros = { "0", "", "0", "0", "" };
        string lsParametros = "";

        try
        {
            if (TxtBusCodigo.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusCodigo.Text.Trim();
                lsParametros += " Código energía : " + TxtBusCodigo.Text;
            }
            if (TxtBusFecha.Text.Trim().Length > 0)
            {
                lValorParametros[1] = TxtBusFecha.Text.Trim();
                lsParametros += " Fecha Inicial : " + TxtBusFecha.Text;
            }
            else
            {
                lValorParametros[1] = ""; //2018113 ajuste
                //lsParametros += " Fecha Inicial : " + TxtBusFecha.Text;  //2018113 ajuste
            }
            if (ddlBusOperador.SelectedValue != "0")
            {
                lValorParametros[2] = ddlBusOperador.SelectedValue;
                lsParametros += " Operador: " + ddlBusOperador.SelectedItem;
            }
            if (ddlBusPozo.SelectedValue != "0")
            {
                lValorParametros[3] = ddlBusPozo.SelectedValue;
                lsParametros += " Pozo: " + ddlBusPozo.SelectedItem;
            }
            if (TxtBusFechaF.Text.Trim().Length > 0)
            {
                lValorParametros[4] = TxtBusFechaF.Text.Trim();
                lsParametros += " Fecha Final : " + TxtBusFechaF.Text;
            }
            else
            {
                if (TxtBusFecha.Text.Trim().Length > 0)
                {
                    lValorParametros[4] = TxtBusFecha.Text.Trim();
                    lsParametros += " Fecha Final : " + TxtBusFecha.Text;
                }
                else
                {
                    lValorParametros[4] = ""; //2018113 ajuste
                    //lsParametros += " Fecha Final : " + TxtBusFecha.Text;  //2018113 ajuste
                }
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_getCapacidadIny&nombreParametros=@P_codigo_energia_consumida*@P_fecha*@P_codigo_operador*@P_codigo_Pozo*@P_fecha_fin&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "*" + lValorParametros[3] + "*" + lValorParametros[4] + "&columnas=codigo_energia_consumida*fecha*codigo_operador*nombre_operador*codigo_Pozo*desc_Pozo*codigo_modalidad*desc_modalidad*cantidad_consumida*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado de capacidad maxima Pozo&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, EventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_energia_consumida <> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=t_energia_consumida&procedimiento=pa_ValidarExistencia&columnas=codigo_energia_consumida*fecha*codigo_operador*codigo_Pozo*codigo_modalidad*cantidad_consumida*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click1(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_energia_consumida", "@P_fecha", "@P_codigo_operador", "@P_codigo_Pozo", "@P_codigo_modalidad", "@P_cantidad_consumida", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "", "0", "0", "0", "0", "", "1" };
        lblMensaje.Text = "";
        int liValor = 0;
        try
        {
            if (TxtFecha.Text == "")
                lblMensaje.Text += " Debe digitar la fecha<br>";
            if (ddlOperador.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el operador<br>";
            if (ddlPozo.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el Pozo<br>";
            if (ddlModalidad.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el tipo de contrato<br>";
            if (TxtCantidad.Text == "")
                lblMensaje.Text += " Debe digitar la Cantidad Consumida<br>";
            //20161124 modif inf ope
            if (VerificarExistencia(" fecha ='" + TxtFecha.Text + "' and codigo_operador =" + ddlOperador.SelectedValue + " and codigo_pozo=" + ddlPozo.SelectedValue + " and estado <>'E'"))
                lblMensaje.Text += " La información de energía consumida nacional ya está registrada<br>"; //20180126 rq107-16
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCantidad.Text.Trim());
                    if (liValor <= 0)
                        lblMensaje.Text += " Valor Inválido en la Cantidad Consumida<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor Inválido en la Cantidad Consumida<br>";
                }
            }
            if (lblMensaje.Text == "")
            {
                lValorParametros[1] = TxtFecha.Text;
                lValorParametros[2] = ddlOperador.SelectedValue;
                lValorParametros[3] = ddlPozo.SelectedValue;
                lValorParametros[4] = ddlModalidad.SelectedValue;
                lValorParametros[5] = TxtCantidad.Text;
                lValorParametros[6] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetCapacidadIny", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación de la capacidad Inyectada.!";
                    lConexion.Cerrar();
                }
                else
                    Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click1(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_energia_consumida", "@P_cantidad_consumida", "@P_estado", "@P_accion", "@P_codigo_pozo" };  //20161124  modif inf ope
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int };//20161124  modif inf ope
        string[] lValorParametros = { "0", "", "", "2", "0" };//20161124  modif inf ope
        lblMensaje.Text = "";
        int liValor = 0;
        try
        {
            //20161124 modif inf ope
            if (ddlPozo.SelectedValue == "0")
                lblMensaje.Text += " Debe seleccionar el punto del SNT<br>";
            if (TxtCantidad.Text == "")
                lblMensaje.Text += " Debe digitar la Cantidad Consumida <br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCantidad.Text.Trim());
                    if (liValor <= 0)
                        lblMensaje.Text += " Valor Inválido en la Cantidad Consumida<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor Inválido en la Cantidad Consumida<br>";
                }
            }
            //20161124 modif inf ope
            if (VerificarExistencia(" fecha ='" + TxtFecha.Text + "' and codigo_operador =" + ddlOperador.SelectedValue + " and codigo_pozo=" + ddlPozo.SelectedValue + " and codigo_energia_cons_nal <>" + LblCodigoCap.Text + " and estado <>'E'"))
                lblMensaje.Text += " La información de energía consumida nacional ya está registrada<br>"; //20180126 rq107-16

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = LblCodigoCap.Text;
                lValorParametros[1] = TxtCantidad.Text;
                lValorParametros[2] = ddlEstado.SelectedValue;
                lValorParametros[4] = ddlPozo.SelectedValue; //20161124 modif inf ope
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetCapacidadIny", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización de la cantidad inyectada.!";
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigoCap.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoCap.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click1(object sender, EventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (LblCodigoCap.Text != "")
            manejo_bloqueo("E", LblCodigoCap.Text);
        Listar();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        if (TxtBusCodigo.Text.Trim().Length <= 0 && TxtBusFecha.Text.Trim().Length <= 0 && ddlBusOperador.SelectedValue == "0" && ddlBusPozo.SelectedValue == "0")
            lblMensaje.Text = "Debe Seleccionar al Menos un parámetro para realizar la Búsqueda!.";
        else
        {
            Listar();
        }
    }
    ///// Eventos Nuevos para la Implementracion del UserControl
    /// <summary>
    /// Metodo del Link Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo(object sender, EventArgs e)
    {
        Nuevo();
    }
    /// <summary>
    /// Metodo del Link Listar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnListar(object sender, EventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Metodo del Link Consultar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsulta(object sender, EventArgs e)
    {
        Buscar();
    }
}