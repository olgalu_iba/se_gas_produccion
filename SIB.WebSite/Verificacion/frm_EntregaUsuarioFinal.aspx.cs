﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Verificacion
{
    public partial class frm_EntregaUsuarioFinal : Page
    {
        private InfoSessionVO goInfo = null;
        private static string lsTitulo = "Entrega a Usuarios Finales";
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        private clConexion lConexion = null;
        private clConexion lConexion1 = null;
        private SqlDataReader lLector;
        private SqlDataReader lLector1;

        #region Propiedades

        /// <summary>
        /// 
        /// </summary>
        public string HdnELiminado
        {
            get { return ViewState["HdnELiminado"] != null && !string.IsNullOrEmpty(ViewState["HdnELiminado"].ToString()) ? ViewState["HdnELiminado"].ToString() : string.Empty; }
            set { ViewState["HdnELiminado"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdnNuevo
        {
            get { return ViewState["HdnNuevo"] != null && !string.IsNullOrEmpty(ViewState["HdnNuevo"].ToString()) ? ViewState["HdnNuevo"].ToString() : string.Empty; }
            set { ViewState["HdnNuevo"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfModificacion
        {
            get { return ViewState["HdfModificacion"] != null && !string.IsNullOrEmpty(ViewState["HdfModificacion"].ToString()) ? ViewState["HdfModificacion"].ToString() : string.Empty; }
            set { ViewState["HdfModificacion"] = value; }
        }

        #endregion Propiedades

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lConexion = new clConexion(goInfo);
            lConexion1 = new clConexion(goInfo);
            //Titulo
            Master.Titulo = "Registros Operativos";
            /// Activacion de los Botones
            buttons.Inicializar(ruta: "t_entrega_usuario_final");
            buttons.CrearOnclick += btnNuevo;
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.ExportarPdfOnclick += ImgPdf_Click;

            if (IsPostBack) return;
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles1(lConexion.gObjConexion, ddlTramo, "m_tramo", " estado = 'A' order by descripcion", 0, 10);
            LlenarControles2(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador >0 and (tipo_operador not in ('T','P','I') or codigo_operador = " + goInfo.cod_comisionista + ") order by razon_social", 0, 4);  //20180122 rq003-18 // 20180126 rq107-16
            LlenarControles2(lConexion.gObjConexion, ddlBusOperador, "m_operador", " estado = 'A' and codigo_operador>0 and (tipo_operador not in ('T','P','I') or codigo_operador = " + goInfo.cod_comisionista + ") order by razon_social", 0, 4); //20180122 rq003-18 // 20180126 rq107-16

            if (Session["tipoPerfil"].ToString() == "N")
            {
                //20180122 rq003-18
                try
                {
                    ddlBusOperador.SelectedValue = goInfo.cod_comisionista;
                    ddlOperador.SelectedValue = goInfo.cod_comisionista;
                }
                catch (Exception ex)
                {
                }
                ddlBusOperador.Enabled = false;
                ddlOperador.Enabled = false;

                HdnELiminado = "N";  //20160721 modif inf opertiva y transaccional
                dtgMaestro.Columns[14].Visible = false; //20161222 rq112 entrega usr final
                dtgMaestro.Columns[15].Visible = false;//20161222 rq112 entrega usr final
            }
            else
                HdnELiminado = "S";  //20160721 modif inf opertiva y transaccional


            LlenarControles(lConexion.gObjConexion, ddlPuntoEnt, "m_punto_salida_snt sal, m_punto_operador ope", " sal.estado = 'A' and sal.codigo_punto_salida= ope.codigo_punto and ope.tipo_punto ='S' and (ope.codigo_operador =0 or ope.codigo_operador =" + ddlOperador.SelectedValue + ") and ope.estado='A' order by descripcion", 0, 2); //20220705
            LlenarControles(lConexion.gObjConexion, ddlTipoDemanda, "m_tipo_demanda_atender", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo", " estado = 'A' order by descripcion", 0, 1); //20160706
            LlenarControles1(lConexion.gObjConexion, ddlBusTramo, "m_tramo", " estado = 'A' order by descripcion", 0, 10);
            LlenarControles(lConexion.gObjConexion, ddlBusTipoDemanda, "m_tipo_demanda_atender", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlBusSector, "m_sector_consumo", " estado = 'A' order by descripcion", 0, 1); //20160706

            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_no_ing_inf_ope", " codigo_reporte = 10 and codigo_operador =" + goInfo.cod_comisionista + " and estado ='A' ");
            if (lLector.HasRows)
            {
                lblMensaje.Text = "El Operador no está obligado a ingresar la información de entrega a usuarios finales.";
            }
            lLector.Close();
            lLector.Dispose();

            lConexion.Cerrar();
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            Buscar();

            if (lblMensaje.Text != "")
            {
                Toastr.Error(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }

        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            var permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "t_entrega_usuario_final");
            //20220621 ajsute
            if ((bool)permisos["UPDATE"] && (bool)permisos["DELETE"])
                dtgMaestro.Columns[20].Visible = true; //20220705
            else
            {
                if (!(bool)permisos["UPDATE"] && !(bool)permisos["DELETE"])
                    foreach (DataGridItem Grilla in dtgMaestro.Items)
                    {
                        var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                        lkbModificar.Visible = false;
                        var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
                        lkbEliminar.Visible = false;
                    }
                else
                {
                    if (!(bool)permisos["UPDATE"])
                    {
                        foreach (DataGridItem Grilla in dtgMaestro.Items)
                        {
                            var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                            lkbModificar.Visible = false;
                        }
                    }
                    if (!(bool)permisos["DELETE"])
                    {
                        foreach (DataGridItem Grilla in dtgMaestro.Items)
                        {
                            var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
                            lkbEliminar.Visible = false;
                        }
                    }
                }
            }

        }

        /// <summary>
        /// Nombre: Nuevo
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
        /// Modificacion:
        /// </summary>
        private void Nuevo()
        {
            HdnNuevo = "S";  //20161222 rq112 entrega usr final
            int liHoraAct = 0;
            string lsHoraMax = "0";
            int liValFin = 0;
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
            if (lLector.HasRows)
            {
                lLector.Read();
                lsHoraMax = lLector["hora_max_declara_opera"].ToString();
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();

            DateTime ldfecha = DateTime.Now.AddDays(-1);
            TxtFecha.Text = ldfecha.Year + "/" + ldfecha.Month + "/" + ldfecha.Day;

            liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
            liValFin = ((Convert.ToDateTime(lsHoraMax).Hour * 60) + Convert.ToDateTime(lsHoraMax).Minute);
            if (liHoraAct > liValFin)
            {
                Toastr.Error(this, "Está intentando Registrar Información Fuera del Horario.");
                //Cierra el modal de Agregar
                Modal.Cerrar(this, registroEntregaUsuFinal.ID);

            }
            else
            {
                //Abre el modal de Agregar
                Modal.Abrir(this, registroEntregaUsuFinal.ID, registroEntregaUsuFinalInside.ID);

                ddlTramo.Enabled = true;
                ddlTramo.SelectedIndex = 0;
                ddlPuntoEnt.Enabled = true;
                //ddlPuntoEnt.Items.Clear(); //20220705
                ddlTipoDemanda.Enabled = true;
                ddlTipoDemanda.SelectedIndex = 0;
                ddlSector.Enabled = true;
                ddlSector.SelectedIndex = 0;
                TxtCapacidadFirme.Enabled = true;
                TxtCapacidadFirme.Text = string.Empty;
                TxtNoContrato.Enabled = true;
                TxtNoContrato.Text = string.Empty;
                dtgDetalle.DataSource = null;
                dtgDetalle.DataBind();

                imbCrear.Visible = true;
                imbActualiza.Visible = false;
                lblTitulo.Text = lsTitulo;
                TxtCodigoEnt.Visible = false;
                LblCodigoEnt.Visible = true;
                LblCodigoEnt.Text = "Automatico";
                TxtCodigoEnt.Text = "0"; //20220705
                TxtFecha.Enabled = false;
                ddlEstado.Enabled = false;
                ddlEstado.SelectedValue = "A"; //20161124 modif inf ope
                imbCrearCont.Visible = false;
            }
            LblCodigoEnt.Text = "0"; //20161222 rq112 entrega ausuario final
        }

        /// <summary>
        /// Nombre: Listar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            CargarDatos();
            lblTitulo.Text = lsTitulo;
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            if (modificar != null && modificar != "")
            {
                try
                {
                    lblMensaje.Text = "";
                    string lsPunto = "0";
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_entrega_usuario_final", " codigo_ent_usuario_final = " + modificar + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            LblCodigoEnt.Text = lLector["codigo_ent_usuario_final"].ToString();
                            TxtCodigoEnt.Text = lLector["codigo_ent_usuario_final"].ToString();
                            DateTime ldFecha; //20160711 ajuste msificaciones
                            ldFecha = Convert.ToDateTime(lLector["fecha_registro"].ToString()); //20160711 ajuste msificaciones
                            TxtFecha.Text = ldFecha.Year + "/" + ldFecha.Month + "/" + ldFecha.Day; //20160711 ajuste msificaciones

                            try
                            {
                                ddlOperador.SelectedValue = lLector["codigo_operador"].ToString();
                            }
                            catch (Exception)
                            {
                                lblMensaje.Text += "El operador del registro no existe o está inactivo<br>";
                            }
                            try
                            {
                                ddlTramo.SelectedValue = lLector["codigo_tramo"].ToString();
                            }
                            catch (Exception)
                            {
                                lblMensaje.Text += "El Punto de Salida al Snt del registro no existe o está inactivo<br>";
                            }
                            try
                            {
                                ddlTipoDemanda.SelectedValue = lLector["codigo_tipo_demanda"].ToString();
                            }
                            catch (Exception)
                            {
                                lblMensaje.Text += "El tipo de demanda del registro no existe o está inactivo<br>";
                            }
                            try
                            {
                                ddlSector.SelectedValue = lLector["codigo_sector_consumo"].ToString(); //20160303
                            }
                            catch (Exception)
                            {
                                lblMensaje.Text += "El sector de consumo del registro no existe o está inactivo<br>";
                            }
                            lsPunto = lLector["codigo_punto_salida"].ToString();
                            TxtCapacidadFirme.Text = lLector["cantidad_entregada"].ToString();
                            TxtNoContrato.Text = ""; //20161222 rq112 entrega usr final
                            ddlEstado.SelectedValue = lLector["estado"].ToString();
                            lLector.Close();
                            lLector.Dispose();
                            ddlPuntoEnt.Items.Clear();
                            LlenarControles(lConexion.gObjConexion, ddlPuntoEnt, "m_punto_salida_snt sal, m_punto_operador ope", " sal.estado = 'A' And sal.codigo_tramo = " + ddlTramo.SelectedValue + " and sal.codigo_punto_salida= ope.codigo_punto and ope.tipo_punto ='S' and (ope.codigo_operador =0 or ope.codigo_operador =" + ddlOperador.SelectedValue + ") and ope.estado='A' order by descripcion", 0, 2); //20220705
                            try
                            {
                                ddlPuntoEnt.SelectedValue = lsPunto;
                            }
                            catch (Exception)
                            {
                                lblMensaje.Text += "El punto de salida del SNT del registro no existe o está inactivo<br>";
                            }
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                            imbCrearCont.Visible = true; //20161222 rq112 entrega usr final
                            TxtCodigoEnt.Visible = false;
                            LblCodigoEnt.Visible = true;
                            TxtFecha.Enabled = false;
                            ddlOperador.Enabled = false;
                            ddlEstado.Enabled = true; //20161124 modif inf ope
                            ddlTramo.Enabled = true;  //20161222 rq112 entrega usr final
                            ddlPuntoEnt.Enabled = true; //20161222 rq112 entrega usr final
                            ddlTipoDemanda.Enabled = true; //20161222 rq112 entrega usr final
                            ddlSector.Enabled = true; //20161222 rq112 entrega usr final
                            TxtCapacidadFirme.Enabled = true; //20161222 rq112 entrega usr final
                            TxtNoContrato.Text = "";

                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Bloquea el Registro a Modificar
                        if (lblMensaje.Text == "") //20160809 carga ptdv
                            manejo_bloqueo("A", modificar);
                    }
                    else
                    {
                        Listar();
                        Toastr.Error(this, "No se Puede editar el Registro porque está Bloqueado. Código Entrega Usuario" + modificar);
                    }
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, ex.Message);
                }
            }
            if (lblMensaje.Text == "")
            {
                //Abre el modal de Agregar
                Modal.Abrir(this, registroEntregaUsuFinal.ID, registroEntregaUsuFinalInside.ID);

                lblTitulo.Text = lsTitulo;
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Error(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }

        /// <summary>
        /// Nombre: Buscar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
        /// Modificacion:
        /// </summary>
        private void Buscar()
        {
            lblTitulo.Text = lsTitulo;
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_codigo_ent_usuario_final", "@P_fecha_registro", "@P_codigo_operador", "@P_fecha_registro_fin", "@P_tramo", "@P_tipo_demanda", "@P_sector_consumo", "@P_mostrar_eliminado" }; //20160721 modif inf opertiva y trans
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar }; //20160721 modif inf opertiva y trans
            string[] lValorParametros = { "0", "", "0", "", ddlBusTramo.SelectedValue, ddlBusTipoDemanda.SelectedValue, ddlBusSector.SelectedValue, HdnELiminado }; //20160721 modif inf opertiva y trans
            DateTime ldFecha;
            lblMensaje.Text = "";

            try
            {
                if (TxtBusFecha.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtBusFecha.Text.Trim());
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Valor Inválido en Fecha Inicial. <br>";
                    }
                }
                if (TxtBusFechaF.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtBusFechaF.Text.Trim());
                        if (ldFecha < Convert.ToDateTime(TxtBusFecha.Text.Trim()))
                            lblMensaje.Text += "La Fecha final NO puede ser menor que la Fecha Inicial.!<br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Valor Inválido en Fecha Final. <br>";
                    }
                }
                if (lblMensaje.Text == "")
                {
                    if (TxtBusCodigo.Text.Trim().Length > 0)
                        lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    if (TxtBusFecha.Text.Trim().Length > 0)
                        lValorParametros[1] = TxtBusFecha.Text.Trim();
                    if (ddlBusOperador.SelectedValue != "0")
                        lValorParametros[2] = ddlBusOperador.SelectedValue;
                    if (TxtBusFechaF.Text.Trim().Length > 0)
                        lValorParametros[3] = TxtBusFechaF.Text.Trim();
                    else
                    {
                        if (TxtBusFecha.Text.Trim().Length > 0)
                            lValorParametros[3] = TxtBusFecha.Text.Trim();
                    }
                    if (TxtBusCodigo.Text.Trim().Length <= 0 && TxtBusFecha.Text.Trim().Length <= 0 && ddlBusOperador.SelectedValue == "0" && ddlBusSector.SelectedValue == "0" && ddlBusTramo.SelectedValue == "0" && ddlBusTipoDemanda.SelectedValue == "0")
                    {
                        lValorParametros[1] = TxtFecha.Text.Trim();
                        lValorParametros[3] = TxtFecha.Text.Trim();
                    }

                    lConexion.Abrir();
                    dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetEntregaUsuarioFinal", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgMaestro.DataBind();
                    lConexion.Cerrar();
                    EstablecerPermisosSistema(); //20220621 ajuste
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Error(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected string Validaciones(string lsAccion) //20161222 rq112 entega usr final
        {
            string lsError = "";
            int liValor = 0;
            //DateTime ldFecha;  //20161222 rq112 entrega usr final

            if (ddlOperador.SelectedValue == "0")
                lsError += "Debe seleccionar el operador<br>";
            if (ddlTramo.SelectedValue == "0")
                lsError += "Debe seleccionar el Tramo<br>";
            if (ddlPuntoEnt.SelectedValue == "0")
                lsError += "Debe seleccionar el Punto de Salida<br>";
            if (ddlTipoDemanda.SelectedValue == "0")
                lsError += "Debe seleccionar el Tipo de Demanda<br>";
            if (ddlSector.SelectedValue == "0") //20160303
                lsError += "Debe seleccionar el Sector de Consumo<br>";
            if (TxtCapacidadFirme.Text.Trim() == "")
                lsError += " Debe digitar la Cantidad de Entrega<br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCapacidadFirme.Text.Trim());
                    if (liValor < 0)
                        lsError += " Valor Inválido en la Cantidad de Entrega<br>";
                }
                catch (Exception)
                {
                    lsError += " Valor Invalido en la Cantidad de Energía<br>"; //20180126 rq107 - 16
                }
            }
            if (lsAccion == "C") //20161222 rq112 entrega usr final
            {//20161222 rq112 entrega usr final 
                if (TxtNoContrato.Text.Trim() == "")
                    lsError += " Debe digitar el No. de Contrato<br>";
                else
                {
                    Regex oExpresion = new Regex(@"^([A-Za-z0-9]*$)");
                    if (!oExpresion.IsMatch(TxtNoContrato.Text))
                        lsError += "El No. de Contrato solo debe tener letras o números<br>";
                }
            } //20161222 rq112 entrega usr final
              //20220705
            if (!VerificarExistencia("m_punto_sector", " codigo_punto= " + ddlPuntoEnt.SelectedValue + " and codigo_sector=" + ddlSector.SelectedValue + " and estado='A'"))
                lsError += "El sector de consumo " + ddlSector.SelectedItem.ToString() + " no se encuentra dentro de tipo de sector parametrizado para el punto de salida " + ddlPuntoEnt.SelectedItem.ToString()  + ". En caso de querer adicionarlo, solicítelo al correo gestordegas@bolsamercantil.com.co";

            return lsError;
        }

        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            lblMensaje.Text = "";
            this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();
            if (lblMensaje.Text != "")
            {
                Toastr.Error(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }

        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;

            string lCodigoRegistro = "";
            lblMensaje.Text = "";
            string lsFecha = ""; // Cambio Req. 003-17 20170216
            HdfModificacion = "N"; // Cambio Req. 003-17 20170216
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text; // Cambio Req. 003-17 20170216
            lsFecha = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[1].Text; // Cambio Req. 003-17 20170216

            if (e.CommandName.Equals("Contratos"))
            {
                //Se abre el modal de contratos
                OpenContractRegistration(lCodigoRegistro);
                return;
            }

            ///////////////////////////////////////////////////////////////////////////////////
            ///// Control Nuevo para Modif Inf Operativa Participantes Req. 003-17 20170216 ///
            ///////////////////////////////////////////////////////////////////////////////////
            bool blPuede = true;
            int liHoraAct = 0;
            string lsHoraMax = "0";
            int liValFin = 0;
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
            if (lLector.HasRows)
            {
                lLector.Read();
                lsHoraMax = lLector["hora_max_declara_opera"].ToString();
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
            liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
            liValFin = ((Convert.ToDateTime(lsHoraMax).Hour * 60) + Convert.ToDateTime(lsHoraMax).Minute);
            if (liHoraAct > liValFin)
            {
                if (Session["tipoPerfil"].ToString() == "N")
                {
                    Toastr.Error(this, "Está intentando modificar o eliminar información fuera del horario.");
                    blPuede = false;
                }
                else
                    HdfModificacion = "S";
            }
            if (Convert.ToDateTime(lsFecha) != DateTime.Now.Date.AddDays(-1))
            {
                if (Session["tipoPerfil"].ToString() == "N")
                {
                    blPuede = false;
                    Toastr.Error(this, "Está intentando modificar o eliminar información que no es del día anterior a la fecha del sistema.");
                }
                else
                    HdfModificacion = "S";
            }
            if (blPuede)
            {
                if (e.CommandName.Equals("Modificar"))
                {
                    mdlRegistroEntregaUsuFinalLabel.InnerText = "Modificar";
                    Modificar(lCodigoRegistro);
                    CargarDatosDet(); //20161222 rq112 entega usr final
                }
                // Evento Eliminar para los Participantes Modif Inf Operativa Participantes Req. 003-17 20170216
                if (e.CommandName.Equals("Eliminar"))
                {
                    string[] lsNombreParametros = { "@P_codigo_ent_usuario_final", "@P_fecha_registro", "@P_codigo_operador", "@P_codigo_tramo", "@P_codigo_punto_salida", "@P_codigo_tipo_demanda", "@P_codigo_sector_consumo", "@P_cantidad_entregada", "@P_contrato_definitivo", "@P_estado", "@P_accion" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
                    string[] lValorParametros = { lCodigoRegistro, "", "0", "0", "0", "0", "0", "0", "0", "E", "3" };
                    lblMensaje.Text = "";
                    try
                    {
                        if (lblMensaje.Text == "")
                        {
                            lConexion.Abrir();
                            if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEntregaUsuarioFinal", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                            {
                                lblMensaje.Text = "Se presentó un Problema en la Eliminación del Registro.! " + goInfo.mensaje_error;
                            }
                            else
                            {
                                Toastr.Success(this, "Registro Eliminado Correctamente");
                                Listar();//20161222 Rq112 entrega usr final
                            }
                            lConexion.Cerrar();
                        }
                    }
                    catch (Exception ex)
                    {
                        /// Desbloquea el Registro Actualizado
                        lblMensaje.Text = ex.Message;
                    }
                }
                if (lblMensaje.Text != "")
                {
                    Toastr.Error(this, lblMensaje.Text);
                    lblMensaje.Text = "";
                }
            }
            ///////////////////////////////////////////////////////////////////////////////////
            //if (((LinkButton)e.CommandSource).Text == "Modificar")
            //{
            //    HdnNuevo = "N"; //20161222 rq112 entrega usr final
            //    lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            //    Modificar(lCodigoRegistro);
            //    CargarDatosDet(); //20161222 rq112 entega usr final
            //}
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lsTable, string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia(lsTable, lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
            //dtgComisionista.DataBind();

            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetTramo", null, null, null);
            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);
            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["desc_tramo"].ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles3(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
            //dtgComisionista.DataBind();

            // Proceso para Cargar el DDL de ciudad
            string[] lsNombreParametros = { "@P_codigo_punto_salida"};
            SqlDbType[] lTipoparametros = { SqlDbType.Int};
            string[] lValorParametros = { ddlPuntoEnt.SelectedValue };

            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetTramoPtoSal", lsNombreParametros, lTipoparametros, lValorParametros);
            ListItem lItem = new ListItem();
            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["desc_tramo"].ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// 20180122 rq003-18
        /// // 20180126 rq107-16
        protected void LlenarControles2(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceLlave) + "-" + lLector.GetValue(liIndiceDescripcion);
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='t_entrega_usuario_final' and llave_registro='codigo_ent_usuario_final=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_ent_usuario_final=" + lscodigo_registro;
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_entrega_usuario_final";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_entrega_usuario_final", lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] lValorParametros = { "0", "", "0", "", ddlBusTramo.SelectedValue, ddlBusTipoDemanda.SelectedValue, ddlBusSector.SelectedValue, "S" }; //20161222 rq112 entrega usr final
            string lsParametros = "";

            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    lsParametros += " Código Entrega Usuario Final : " + TxtBusCodigo.Text;
                }
                if (TxtBusFecha.Text.Trim().Length > 0)
                {
                    lValorParametros[1] = TxtBusFecha.Text.Trim();
                    lsParametros += " Fecha Inicial : " + TxtBusFecha.Text;
                }
                if (ddlBusOperador.SelectedValue != "0")
                {
                    lValorParametros[2] = ddlBusOperador.SelectedValue;
                    lsParametros += " Operador: " + ddlBusOperador.SelectedItem;
                }
                if (TxtBusFechaF.Text.Trim().Length > 0)
                {
                    lValorParametros[3] = TxtBusFechaF.Text.Trim();
                    lsParametros += " Fecha Final : " + TxtBusFechaF.Text;
                }
                else
                {
                    if (TxtBusFecha.Text.Trim().Length > 0)
                    {
                        lValorParametros[3] = TxtBusFecha.Text.Trim();
                        lsParametros += " Fecha Final : " + TxtBusFecha.Text;
                    }
                }
                if (ddlBusTramo.SelectedValue != "0")
                {
                    lValorParametros[4] = ddlBusTramo.SelectedValue;
                    lsParametros += " Tramo: " + ddlBusTramo.SelectedItem;
                }
                if (ddlBusSector.SelectedValue != "0") //20160303
                {
                    lValorParametros[6] = ddlBusSector.SelectedValue;
                    lsParametros += " Sector De Consumo: " + ddlBusSector.SelectedItem; //20160303
                }

                if (ddlBusTipoDemanda.SelectedValue != "0")
                {
                    lValorParametros[5] = ddlBusTipoDemanda.SelectedValue;
                    lsParametros += " Tipo Demanda: " + ddlBusTipoDemanda.SelectedItem;
                }
                if (TxtBusCodigo.Text.Trim().Length <= 0 && TxtBusFecha.Text.Trim().Length <= 0 && ddlBusOperador.SelectedValue == "0" && ddlBusSector.SelectedValue == "0" && ddlBusTramo.SelectedValue == "0" && ddlBusTipoDemanda.SelectedValue == "0")
                {
                    lValorParametros[1] = TxtFecha.Text.Trim();
                    lValorParametros[3] = TxtFecha.Text.Trim();
                }
                //20161222 rq112 entrega usuario final 
                if (goInfo.cod_comisionista != "0")
                    lValorParametros[7] = "N";
                //20161222 rq112 entrega usuario final 
                Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetEntregaUsuarioFinalExc&nombreParametros=@P_codigo_ent_usuario_final*@P_fecha_registro*@P_codigo_operador*@P_fecha_registro_fin*@P_tramo*@P_tipo_demanda*@P_sector_consumo*@P_ind_ruta&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "*" + lValorParametros[3] + "*" + lValorParametros[4] + "*" + lValorParametros[5] + "*" + lValorParametros[6] + "*" + lValorParametros[7] + "&columnas=codigo_energia_inyectada*fecha*codigo_operador*nombre_operador*codigo_Pozo*desc_Pozo*codigo_modalidad*desc_modalidad*cantidad_inyectada*coincidencia*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado de Entrega a Usuario Final&TituloParametros=" + lsParametros);  //20161222 rq112 entrega usr final  //20220202
            }
            catch (Exception)
            {
                Toastr.Error(this, "No se Pudo Generar el Informe.!");
            }
        }

        /// <summary>
        /// Nombre: ImgPdf_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgPdf_Click(object sender, EventArgs e)
        {
            try
            {
                string lsCondicion = " codigo_ent_usuario_final <> '0'";
                Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=t_entrega_usuario_final&procedimiento=pa_ValidarExistencia&columnas=codigo_ent_usuario_final*fecha_registro*codigo_operador*codigo_punto_salida*codigo_tipo_demanda*codigo_sector_consumo*cantidad_entregada*contrato_definitivo*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
            }
            catch (Exception)
            {
                Toastr.Error(this, "No se Pudo Generar el Informe.!");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlTramo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTramo.SelectedValue != "0")
            {
                lConexion.Abrir();
                ddlPuntoEnt.Items.Clear();
                LlenarControles(lConexion.gObjConexion, ddlPuntoEnt, "m_punto_salida_snt sal, m_punto_operador ope", " sal.estado = 'A' And sal.codigo_tramo = " + ddlTramo.SelectedValue + " and sal.codigo_punto_salida= ope.codigo_punto and ope.tipo_punto ='S' and (ope.codigo_operador =0 or ope.codigo_operador =" + ddlOperador.SelectedValue + ") and ope.estado='A' order by descripcion", 0, 2); //20220705
                lConexion.Cerrar();
            }
            else
            {
                lConexion.Abrir();
                ddlPuntoEnt.Items.Clear();
                LlenarControles(lConexion.gObjConexion, ddlPuntoEnt, "m_punto_salida_snt sal, m_punto_operador ope", " sal.estado = 'A' and sal.codigo_punto_salida= ope.codigo_punto and ope.tipo_punto ='S' and (ope.codigo_operador =0 or ope.codigo_operador =" + ddlOperador.SelectedValue + ") and ope.estado='A' order by descripcion", 0, 2); //20220705
                lConexion.Cerrar();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPuntoEnt_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPuntoEnt.SelectedValue != "0")
            {
                lConexion.Abrir();
                ddlTramo.Items.Clear();
                LlenarControles3(lConexion.gObjConexion, ddlTramo, "m_tramo tra","", 0, 10);
                lConexion.Cerrar();
            }
            else
            {
                lConexion.Abrir();
                ddlTramo.Items.Clear();
                LlenarControles1(lConexion.gObjConexion, ddlTramo, "m_tramo tra", "  tra.estado = 'A'", 0, 10);
                lConexion.Cerrar();
                ddlTramo_SelectedIndexChanged(null, null);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlTipoDemanda_SelectedIndexChanged(object sender, EventArgs e)
        {
            lConexion.Abrir();
            ddlSector.Items.Clear();//20160303
            //20160706 se cambia para incuir la tabla de demanda sector para informacion operativa
            LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo sec, m_demanda_sector dem", " sec.estado ='A' and sec.codigo_sector_consumo = dem.codigo_sector_consumo and dem.codigo_tipo_demanda= " + ddlTipoDemanda.SelectedValue + " and dem.estado ='A' and dem.ind_uso='O' order by descripcion", 0, 1); //20160706
            lConexion.Cerrar();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_ent_usuario_final", "@P_fecha_registro", "@P_codigo_operador", "@P_codigo_tramo", "@P_codigo_punto_salida", "@P_codigo_tipo_demanda", "@P_codigo_sector_consumo", "@P_cantidad_entregada", "@P_contrato_definitivo", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "", "0", "0", "0", "0", "0", "0", "", "", "1" };
            lblMensaje.Text = "";

            try
            {
                lblMensaje.Text = Validaciones("C"); //20161222 rq112 entrega usr final
                //20161124 modif inf ope
                if (VerificarExistencia("t_entrega_usuario_final", " fecha_registro= '" + TxtFecha.Text + "' and codigo_operador=" + ddlOperador.SelectedValue + " and codigo_tramo=" + ddlTramo.SelectedValue + " and codigo_punto_salida=" + ddlPuntoEnt.SelectedValue + " and codigo_tipo_demanda=" + ddlTipoDemanda.SelectedValue + " and codigo_sector_consumo=" + ddlSector.SelectedValue + " and contrato_definitivo='" + TxtNoContrato.Text + "'" + " and estado <>'E'"))
                    lblMensaje.Text += "La información de entrega a usuarios finales ya está registrada<br>"; //20180126 rq107 - 16
                //20220705
                string lsError = "N";
                if (lblMensaje.Text == "")
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_punto_reporte_cnt", " codigo_reporte = 10 and codigo_punto =" + ddlPuntoEnt.SelectedValue + " and estado ='A'", goInfo))
                        lblMensaje.Text += "El punto de salida no tiene definida la cantidad máxima<br>";
                    else
                    {
                        if (validaCantidad())
                        {
                            lsError = "S";
                        }
                    }
                }
                if (lsError == "S" && lblMensaje.Text == "")
                {
                    Modal.Abrir(this, mdlConfirma.ID, mdlConfirmaInside.ID);
                }
                //20220705 fin

                if (lblMensaje.Text == "" && lsError == "N")//20220705 
                {
                    lValorParametros[1] = TxtFecha.Text;
                    lValorParametros[2] = ddlOperador.SelectedValue;
                    lValorParametros[3] = ddlTramo.SelectedValue;
                    lValorParametros[4] = ddlPuntoEnt.SelectedValue;
                    lValorParametros[5] = ddlTipoDemanda.SelectedValue;
                    lValorParametros[6] = ddlSector.SelectedValue;
                    lValorParametros[7] = TxtCapacidadFirme.Text.Trim();
                    lValorParametros[8] = TxtNoContrato.Text.Trim();
                    lValorParametros[9] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    //20161222 rq112 entega usr final
                    SqlDataReader lLector;
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetEntregaUsuarioFinal", lsNombreParametros, lTipoparametros, lValorParametros);
                    if (!lLector.HasRows)
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Creación de la Entrega a Usuario Final.!";
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lLector.Read();
                        LblCodigoEnt.Text = lLector["codigo"].ToString();
                        lLector.Close();
                        TxtBusFecha.Text = TxtFecha.Text; //20160711 ajuste msificaciones
                        imbCrearCont.Visible = true; //20161222 Rq112 entrega usr final
                        imbCrear.Visible = false; //20161222 Rq112 entrega usr final
                        Listar();//20161222 Rq112 entrega usr final
                        inactivaCampos(); //20161222 Rq112 entrega usr final
                        lConexion.Cerrar(); //20161222 rq112 entrega usr final
                        CargarDatosDet();//20161222 rq112 entrega usr final
                    }
                    //20161222 rq112 fin entega usr final
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Error(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbActualiza_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_ent_usuario_final", "@P_fecha_registro", "@P_codigo_operador", "@P_codigo_tramo", "@P_codigo_punto_salida", "@P_codigo_tipo_demanda", "@P_codigo_sector_consumo", "@P_cantidad_entregada", "@P_contrato_definitivo", "@P_estado", "@P_accion",
                "@P_modificacion"  }; // Campo Nuevo Req. 003-17  20170216
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                SqlDbType.VarChar  }; // Campo Nuevo Req. 003-17  20170216
            string[] lValorParametros = { "0", "", "0", "0", "0", "0", "0", "0", "", "", "2",
                HdfModificacion  }; // Campo Nuevo Req. 003-17  20170216
            lblMensaje.Text = "";
            try
            {

                lblMensaje.Text = Validaciones("M");  //20161222 rq112 entrega usr final
                //20161124 modif inf ope
                if (VerificarExistencia("t_entrega_usuario_final", " fecha_registro= '" + TxtFecha.Text + "' and codigo_operador=" + ddlOperador.SelectedValue + " and codigo_tramo=" + ddlTramo.SelectedValue + " and codigo_punto_salida=" + ddlPuntoEnt.SelectedValue + " and codigo_tipo_demanda=" + ddlTipoDemanda.SelectedValue + " and codigo_sector_consumo=" + ddlSector.SelectedValue + " and contrato_definitivo='" + TxtNoContrato.Text + "' and codigo_ent_usuario_final<>" + LblCodigoEnt.Text + " and estado <>'E'"))
                    lblMensaje.Text += "La información de entrega a usuarios finales ya está registrada<br>"; //20161222 rq112 entrega usr final mesnaje
                                                                                                              //20220705
                string lsError = "N";
                if (lblMensaje.Text == "")
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_punto_reporte_cnt", " codigo_reporte = 10 and codigo_punto =" + ddlPuntoEnt.SelectedValue + " and estado ='A'", goInfo))
                        lblMensaje.Text += "El punto de salida no tiene definida la cantidad máxima<br>";
                    else
                        if (validaCantidad())
                    {
                        lsError = "S";
                    }
                }
                if (lsError == "S" && lblMensaje.Text == "")
                {
                    Modal.Abrir(this, mdlConfirma.ID, mdlConfirmaInside.ID);
                }
                //20220705 fin
                if (lblMensaje.Text == "" && lsError == "N")//20220705 
                {
                    lValorParametros[0] = LblCodigoEnt.Text;
                    lValorParametros[1] = TxtFecha.Text;
                    lValorParametros[2] = ddlOperador.SelectedValue;
                    lValorParametros[3] = ddlTramo.SelectedValue;
                    lValorParametros[4] = ddlPuntoEnt.SelectedValue;
                    lValorParametros[5] = ddlTipoDemanda.SelectedValue;
                    lValorParametros[6] = ddlSector.SelectedValue;
                    lValorParametros[7] = TxtCapacidadFirme.Text.Trim();
                    lValorParametros[8] = ""; //20161222 rq112 entrega usr final
                    lValorParametros[9] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEntregaUsuarioFinal", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Actualización de la Entrega a Usuario Final.!";
                        lConexion.Cerrar();
                    }
                    else
                    {
                        if (LblCodigoEnt.Text != "")
                            manejo_bloqueo("E", LblCodigoEnt.Text);
                        TxtBusFecha.Text = TxtFecha.Text;
                        //Cierra el modal de Agregar
                        Modal.Cerrar(this, registroEntregaUsuFinal.ID);
                        Listar(); //20161222 rq112 entrega usr final
                        imbActualiza.Visible = false; //20161222 rq112 entrega usr final
                        inactivaCampos(); //20161222 rq112 entrega usr final
                        lConexion.Cerrar(); //20161222 rq112 entrega usr final
                    }
                }
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                manejo_bloqueo("E", LblCodigoEnt.Text);
                lblMensaje.Text = ex.Message;
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Error(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }

        /// <summary>
        /// Nombre: CargarDatosDet
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        // 20161222 rq112 entrega usuario final
        private void CargarDatosDet()
        {
            string[] lsNombreParametros = { "@P_codigo_ent_usuario_final" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int };
            string[] lValorParametros = { "0" };
            lblMensaje.Text = "";
            try
            {
                if (LblCodigoEnt.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = LblCodigoEnt.Text;
                }
                lConexion.Abrir();
                dtgDetalle.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetEntregaUsuarioFinalDet", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgDetalle.DataBind();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Error(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        // 20161222 rq112 entega usuario final
        protected void dtgDetalle_EditCommand(object source, DataGridCommandEventArgs e)
        {
            lblMensaje.Text = "";
            if (e.CommandName.Equals("Eliminar"))
            {
                string[] lsNombreParametros = { "@P_codigo_detalle", "@P_codigo_ent_usuario_final", "@P_contrato_definitivo", "@P_accion", "@P_ind_nuevo" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
                string[] lValorParametros = { dtgDetalle.Items[e.Item.ItemIndex].Cells[0].Text, LblCodigoEnt.Text, "", "3", HdnNuevo };
                lblMensaje.Text = "";
                try
                {
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEntregaUsuarioFinalDet", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Eliminación del contrato de la Entrega a Usuario Final.!";
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lConexion.Cerrar();
                        CargarDatosDet();
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = ex.Message;
                }
                if (lblMensaje.Text != "")
                {
                    Toastr.Error(this, lblMensaje.Text);
                    lblMensaje.Text = "";
                }
            }
        }

        //20161222 Rq112 entrega usr final
        protected void imbCrearCont_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_detalle", "@P_codigo_ent_usuario_final", "@P_contrato_definitivo", "@P_accion", "@P_ind_nuevo" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "0", LblCodigoEnt.Text, TxtNoContrato.Text, "1", HdnNuevo };
            lblMensaje.Text = "";
            if (TxtNoContrato.Text.Trim() == "")
                lblMensaje.Text += " Debe digitar el No. de Contrato<br>";
            else
            {
                Regex oExpresion = new Regex(@"^([A-Za-z0-9]*$)");
                if (!oExpresion.IsMatch(TxtNoContrato.Text))
                    lblMensaje.Text += "El No. de Contrato solo debe tener letras o números<br>";
                else
                {
                    if (VerificarExistencia("t_entrega_usuario_final_det", " codigo_ent_usuario_final= " + LblCodigoEnt.Text + " and contrato_definitivo='" + TxtNoContrato.Text + "'"))
                        lblMensaje.Text += "Ya se registró el contrato<br>";
                }
            }

            try
            {
                if (lblMensaje.Text == "")
                {
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEntregaUsuarioFinalDet", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Creación del contrato de la Entrega a Usuario Final.!";
                        lConexion.Cerrar();
                    }
                    else
                    {
                        TxtNoContrato.Text = "";
                        lConexion.Cerrar();
                        CargarDatosDet();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
                lConexion.Cerrar();
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Error(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }

        //20161222 Rq112 entrega usr final
        protected void inactivaCampos()
        {

            ddlOperador.Enabled = false;
            ddlTramo.Enabled = false;
            ddlPuntoEnt.Enabled = false;
            ddlTipoDemanda.Enabled = false;
            ddlSector.Enabled = false;
            TxtCapacidadFirme.Enabled = false;
            ddlEstado.Enabled = false;
            TxtNoContrato.Text = "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            lblMensaje.Text = "";
            if (TxtBusCodigo.Text.Trim().Length <= 0 && TxtBusFecha.Text.Trim().Length <= 0 && ddlBusOperador.SelectedValue == "0" && ddlBusSector.SelectedValue == "0" && ddlBusTramo.SelectedValue == "0" && ddlBusTipoDemanda.SelectedValue == "0")
                Toastr.Error(this, "Debe Seleccionar al Menos un parámetro para realizar la Búsqueda!.");
            else
            {
                Listar();
                //Establese los permisos del sistema
                //EstablecerPermisosSistema(); //20220621 ajsute
            }
        }

        /// <summary>
        /// Metodo del Link Nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNuevo(object sender, EventArgs e)
        {
            mdlRegistroEntregaUsuFinalLabel.InnerText = "Agregar";
            Nuevo();
        }

        /// <summary>
        /// Metodo del Link Listar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnListar(object sender, EventArgs e)
        {
            Listar();
        }

        /// 
        /// </summary>
        protected void Cancel_OnClick(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (mdlRegistroEntregaUsuFinalLabel.InnerText.Equals("Modificar") && LblCodigoEnt.Text != "")
                manejo_bloqueo("E", LblCodigoEnt.Text);

            //Cierra el modal de Agregar
            Modal.Cerrar(this, registroEntregaUsuFinal.ID);
            //Se limpia la fecha actual
            TxtFecha.Text = string.Empty;
        }
        protected void BtnAceptarConf_Click(object sender, EventArgs e)
        {
            Modal.Cerrar(this, mdlConfirma.ID);
            string[] lsNombreParametros = { "@P_codigo_ent_usuario_final", "@P_fecha_registro", "@P_codigo_operador", "@P_codigo_tramo", "@P_codigo_punto_salida", "@P_codigo_tipo_demanda", "@P_codigo_sector_consumo", "@P_cantidad_entregada", "@P_contrato_definitivo", "@P_estado", "@P_accion", "@P_dato_atipico" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "", "0", "0", "0", "0", "0", "0", "", "", "1", "S" };

            if (TxtCodigoEnt.Text != "0")
            {
                lValorParametros[0] = TxtCodigoEnt.Text;
                lValorParametros[10] = "2";
            }
            lValorParametros[1] = TxtFecha.Text;
            lValorParametros[2] = ddlOperador.SelectedValue;
            lValorParametros[3] = ddlTramo.SelectedValue;
            lValorParametros[4] = ddlPuntoEnt.SelectedValue;
            lValorParametros[5] = ddlTipoDemanda.SelectedValue;
            lValorParametros[6] = ddlSector.SelectedValue;
            lValorParametros[7] = TxtCapacidadFirme.Text.Trim();
            lValorParametros[8] = TxtNoContrato.Text.Trim();
            lValorParametros[9] = ddlEstado.SelectedValue;
            lConexion.Abrir();
            //20161222 rq112 entega usr final
            SqlDataReader lLector;
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetEntregaUsuarioFinal", lsNombreParametros, lTipoparametros, lValorParametros);
            if (lLector.HasRows)
            {
                lLector.Read();
                LblCodigoEnt.Text = lLector["codigo"].ToString();
                lLector.Close();
                TxtBusFecha.Text = TxtFecha.Text; //20160711 ajuste msificaciones
                imbCrearCont.Visible = true; //20161222 Rq112 entrega usr final
                imbCrear.Visible = false; //20161222 Rq112 entrega usr final
                Listar();//20161222 Rq112 entrega usr final
                inactivaCampos(); //20161222 Rq112 entrega usr final
                lConexion.Cerrar(); //20161222 rq112 entrega usr final
                CargarDatosDet();//20161222 rq112 entrega usr final
            }
            if (TxtCodigoEnt.Text != "0")
            {
                Modal.Cerrar(this, registroEntregaUsuFinal.ID);
                Listar();
                manejo_bloqueo("E", TxtCodigoEnt.Text);
            }
            lLector.Close();
            lConexion.Cerrar();
        }
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool validaCantidad()
        {
            bool lbValida = false;
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_punto_reporte_cnt", " codigo_reporte = 10 and codigo_punto =" + ddlPuntoEnt.SelectedValue + " and cantidad_maxima < " + TxtCapacidadFirme.Text + "  and estado ='A'");
            if (lLector.HasRows)
            {
                lLector.Read();
                lblMensajeCOnf.Text = "La cantidad de energía a registrar de " + Convert.ToInt32(TxtCapacidadFirme.Text).ToString("###,###,###,###") + " MBTU supera el valor máximo histórico parametrizado para el punto de salida " + ddlPuntoEnt.SelectedItem.ToString() + " que es de " + Convert.ToInt32(lLector["cantidad_maxima"].ToString()).ToString("###,###,###,###") + " MBTU";
                lbValida = true;
            }
            lLector.Close();
            lConexion.Cerrar();
            return lbValida;
        }

        #region Contratos Registrados

        /// <summary>
        /// Abre el modal de los contratos registrados
        /// </summary>
        protected void OpenContractRegistration(string codigo)
        {
            try
            {
                string[] lsNombreParametros = { "@P_codigo_ent_usuario_final" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int };
                string[] lValorParametros = { "0" };
                lValorParametros[0] = codigo;

                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                DataGrid1.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetEntregaUsuarioFinalDet", lsNombreParametros, lTipoparametros, lValorParametros);
                DataGrid1.DataBind();
                lConexion.Cerrar();

                //Abre el modal de contratos registrados
                Modal.Abrir(this, mdlContratosRegistrados.ID, mdlContratosRegistradosInside.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Cierra el modal de los contratos registrados
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseContractRegistration_OnClick(object sender, EventArgs e)
        {
            //Cierra el modal de contratos registrados
            Modal.Cerrar(this, mdlContratosRegistrados.ID);
        }

        #endregion Contratos Registrados
    }
}