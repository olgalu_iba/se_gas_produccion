﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_EntregaUsuarioFinal.aspx.cs" Inherits="Verificacion.frm_EntregaUsuarioFinal" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>ç
           
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="kt-portlet__body">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Código Entrega Usuario Final" AssociatedControlID="TxtBusCodigo" runat="server" />
                                    <asp:TextBox ID="TxtBusCodigo" type="number" runat="server" CssClass="form-control" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Fecha Registro Inicial" AssociatedControlID="TxtBusFecha" runat="server" />
                                    <asp:TextBox ID="TxtBusFecha" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Fecha Registro Final" AssociatedControlID="TxtBusFechaF" runat="server" />
                                    <asp:TextBox ID="TxtBusFechaF" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Operador" AssociatedControlID="ddlBusOperador" runat="server" />
                                    <asp:DropDownList ID="ddlBusOperador" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Tramo" AssociatedControlID="ddlBusTramo" runat="server" />
                                    <asp:DropDownList ID="ddlBusTramo" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Sector de Consumo" AssociatedControlID="ddlBusSector" runat="server" />
                                    <asp:DropDownList ID="ddlBusSector" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:Label Text="Tipo de Demanda" AssociatedControlID="ddlBusTipoDemanda" runat="server" />
                                    <asp:DropDownList ID="ddlBusTipoDemanda" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                </div>
                            </div>
                        </div>
                        <%--Mensaje--%>
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <asp:ValidationSummary ID="Vsconsulta" runat="server" ValidationGroup="detalle1" />
                                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <%--Grilla--%>
                        <div class="table table-responsive">
                            <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="true"
                                PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" PageSize="10"
                                OnItemCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged">
                                <Columns>
                                    <%--20180126 rq107-16--%>
                                    <asp:BoundColumn DataField="codigo_ent_usuario_final" HeaderText="Código Ent Usuario Final"
                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_registro" HeaderText="Fecha Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20180126 rq107-16--%>
                                    <asp:BoundColumn DataField="codigo_operador" HeaderText="Código operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_tramo" HeaderText="Desc Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Punto Salida Snt" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_punto_sal" HeaderText="Desc Punto Salida Snt" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_tipo_demanda" HeaderText="Tipo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_sector_consumo" HeaderText="Sector de consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="cantidad_entregada" HeaderText="Cantidad Entregada" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20161222 rq112--%>
                                    <asp:BoundColumn DataField="contrato_definitivo" HeaderText="No. Contrato" ItemStyle-HorizontalAlign="Left"
                                        Visible="false"></asp:BoundColumn>
                                    <%--20160803--%><%--20161222 rq112--%>
                                    <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad de Contrato" ItemStyle-HorizontalAlign="Left"
                                        Visible="false"></asp:BoundColumn>
                                    <%--20160803--%><%--20161222 rq112--%>
                                    <asp:BoundColumn DataField="ind_registrado" HeaderText="contrato registrado" ItemStyle-HorizontalAlign="Center"
                                        Visible="false"></asp:BoundColumn>
                                    <%--20160711--%>
                                    <asp:BoundColumn DataField="carga_normal" HeaderText="Estado de Carga" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20161222 rq112--%>
                                    <asp:BoundColumn DataField="codigo_ruta" HeaderText="Cod Ruta" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <%--20161222 rq112--%>
                                    <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20220202--%>
                                    <asp:BoundColumn DataField="coincidencia" HeaderText="Coincidencia" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <%--20220705--%>
                                    <asp:BoundColumn DataField="dato_atipico" HeaderText="Dato Atipico" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20180126 rq107-16--%>
                                    <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualización" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more-1"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                    <!--begin::Nav-->
                                                    <ul class="kt-nav">
                                                        <li class="kt-nav__item">
                                                            <asp:LinkButton ID="lkbContratos" CssClass="kt-nav__link" CommandName="Contratos" runat="server">
                                                             <i class="kt-nav__link-icon flaticon2-writing"></i>
                                                            <span class="kt-nav__link-text">Contratos</span>
                                                            </asp:LinkButton>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                             <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Modificar</span>
                                                            </asp:LinkButton>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon-delete"></i>
                                                            <span class="kt-nav__link-text">Eliminar</span>
                                                            </asp:LinkButton>
                                                        </li>
                                                    </ul>
                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <%--Modals--%>
    <%--Reg/Mod--%>
    <div class="modal fade" id="registroEntregaUsuFinal" tabindex="-1" role="dialog" aria-labelledby="mdlRegistroEntregaUsuFinalLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="registroEntregaUsuFinalInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlRegistroEntregaUsuFinalLabel" runat="server">Agregar</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Código Entrega Usuario Final</label>
                                        <asp:TextBox ID="TxtCodigoEnt" runat="server" MaxLength="3" CssClass="form-control" />
                                        <asp:Label ID="LblCodigoEnt" runat="server" Visible="False" CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Fecha</label>
                                        <asp:TextBox ID="TxtFecha" placeholder="yyyy/mm/dd" runat="server" Enabled="false" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Operador</label>
                                        <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Tramo</label>
                                        <asp:DropDownList ID="ddlTramo" runat="server" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="ddlTramo_SelectedIndexChanged" AutoPostBack="true" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Punto de Salida al Snt</label>
                                        <asp:DropDownList ID="ddlPuntoEnt" runat="server" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="ddlPuntoEnt_SelectedIndexChanged" AutoPostBack="true" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Tipo de Demanda</label>
                                        <asp:DropDownList ID="ddlTipoDemanda" runat="server" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="ddlTipoDemanda_SelectedIndexChanged" AutoPostBack="true" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Sector de Consumo<%--20160303--%></label>
                                        <asp:DropDownList ID="ddlSector" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Cantidad Energía Tomada para Entregar a Usuario Final (MBTU)</label>
                                        <asp:TextBox ID="TxtCapacidadFirme" runat="server" CssClass="form-control" />
                                        <asp:CompareValidator ID="CvTxtCapacidadFirme" runat="server" ControlToValidate="TxtCapacidadFirme"
                                            Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Cantidad Energía Tomada para Entregar a Usuario Final (MBTU) debe Ser numérico">*</asp:CompareValidator>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Número de Contrato</label>
                                        <asp:TextBox ID="TxtNoContrato" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Estado</label>
                                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control selectpicker" data-live-search="true" >
                                            <asp:ListItem Value="A">Activo</asp:ListItem>
                                            <asp:ListItem Value="I">Inactivo</asp:ListItem>
                                            <asp:ListItem Value="E">Eliminado</asp:ListItem>
                                            <%--20161124--%>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <asp:Button ID="imbCrearCont" runat="server" CssClass="btn btn-primary btn-block" Text="Agregar Contrato" OnClick="imbCrearCont_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />

                            <hr>

                            <div class="table table-responsive">
                                <asp:DataGrid ID="dtgDetalle" runat="server" AutoGenerateColumns="False" OnItemCommand="dtgDetalle_EditCommand" Width="100%" CssClass="table-bordered">
                                    <Columns>
                                        <asp:BoundColumn DataField="codigo_detalle" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="codigo_ent_usuario_final" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Cod Mod" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ind_registrado" HeaderText="Registrado" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                            <ItemTemplate>
                                                <div class="dropdown dropdown-inline">
                                                    <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="flaticon-more-1"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                        <!--begin::Nav-->
                                                        <ul class="kt-nav">
                                                            <li class="kt-nav__item">
                                                                <asp:LinkButton ID="lkbEliminarReg" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                                    <i class="kt-nav__link-icon flaticon-delete"></i>
                                                                    <span class="kt-nav__link-text">Eliminar</span>
                                                                </asp:LinkButton>
                                                            </li>
                                                        </ul>
                                                        <!--end::Nav-->
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                    <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                                    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                </asp:DataGrid>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button Text="Salir" CssClass="btn btn-secondary" OnClick="Cancel_OnClick" runat="server" /> <%--20201020--%>
                            <asp:Button ID="imbCrear" runat="server" CssClass="btn btn-primary" Text="Crear" OnClick="imbCrear_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="imbActualiza" runat="server" CssClass="btn btn-primary" Text="Actualizar" OnClick="imbActualiza_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <%--Contratos Registrados--%>
    <div class="modal fade" id="mdlContratosRegistrados" tabindex="-1" role="dialog" aria-labelledby="mdlContratosRegistradosLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlContratosRegistradosInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlContratosRegistradosLabel" runat="server">Contratos Registrados</h5>
                        </div>
                        <div class="modal-body">
                            <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered">
                                <Columns>
                                    <asp:BoundColumn DataField="codigo_detalle" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_ent_usuario_final" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Cod Mod" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="ind_registrado" HeaderText="Registrado" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                </Columns>
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </div>
                        <div class="modal-footer">
                            <asp:Button Text="Salir" ID="btnCancelar" CssClass="btn btn-secondary" OnClick="CloseContractRegistration_OnClick" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" runat="server" /> <%--20201020--%>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
            <%--20220705 pasa validaciones--%>
    <div class="modal fade" id="mdlConfirma" tabindex="-1" role="dialog" aria-labelledby="mdlConfirmaLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlConfirmaInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <div class="modal-header" style="background-color: #3E5F8A;">
                            <h5 class="modal-title" id="mdlConfirmaLabel" runat="server" style="color: white;">Confirmación</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff; opacity: 1;">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" style="background-color: #D3D3D3">
                            <asp:Label ID="lblMensajeCOnf" runat="server" Font-Size="Large"></asp:Label>
                        </div>
                        <div class="modal-body" style="background-color: #D3D3D3">
                            <asp:Label ID="lblConforma" runat="server" Font-Size="Large">¿Desea ingresar el dato como un valor atípico?<br /><br />
                                Nota: este reporte se rige por lo establecido en la Resolución CREG 080 de 2019 “Por la cual se establecen reglas generales de comportamiento de mercado para los agentes que desarrollen las actividades de los servicios públicos domiciliarios de energía eléctrica y gas combustible
                            </asp:Label>
                        </div>
                        <div class="modal-footer" style="background-color: #3E5F8A">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <asp:Button ID="btnAceptarConf" CssClass="btn btn-primary" Text="Aceptar" OnClick="BtnAceptarConf_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
