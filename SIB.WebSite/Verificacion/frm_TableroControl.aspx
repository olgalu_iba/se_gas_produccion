﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_TableroControl.aspx.cs" Inherits="Verificacion.frm_TableroControl" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Tipo de Información</label>
                            <asp:DropDownList ID="ddlTipoRep" runat="server" Width="100%" CssClass="form-control">
                                <asp:ListItem Value="Z" Text="Operativa"></asp:ListItem>
                                <asp:ListItem Value="T" Text="Transaccional"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="ddlOperador">Operador</label>
                            <asp:DropDownList ID="ddlOperador" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true" />
                            <asp:HiddenField ID="HiddenField1" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgInfOperativa" OnItemCommand="dtgInfOperativa_EditCommand" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                Visible="false" ViewStateMode="Enabled">
                                <Columns>
                                    <%--0--%>
                                    <asp:BoundColumn DataField="codigo_reporte" HeaderText="Código Reporte" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Font-Size="14px">
                                        <ItemStyle HorizontalAlign="Center" Font-Size="14px"></ItemStyle>
                                    </asp:BoundColumn>
                                    <%--1--%>
                                    <asp:BoundColumn DataField="tipo_reporte" HeaderText="Tipo Reporte"
                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="14px">
                                        <ItemStyle HorizontalAlign="Left" Font-Size="14px"></ItemStyle>
                                    </asp:BoundColumn>
                                    <%--2--%>
                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more-1"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                    <!--begin::Nav-->
                                                    <ul class="kt-nav">
                                                        <li class="kt-nav__item">
                                                            <asp:LinkButton ID="lkbRegistrosPendientes" CssClass="kt-nav__link" CommandName="RegistrosPendientes" runat="server">
                                                             <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Registros Pendientes</span>
                                                            </asp:LinkButton>
                                                        </li>
                                                    </ul>
                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--3--%>
                                    <asp:BoundColumn DataField="pendiente" Visible="false"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                <PagerStyle HorizontalAlign="Center"></PagerStyle>
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgInfTransaccional" Width="100%" runat="server" CssClass="table-bordered" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                Visible="false" OnItemCommand="dtgInfTransaccional_EditCommand">
                                <Columns>
                                    <%--0--%>
                                    <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Font-Size="14px"></asp:BoundColumn>
                                    <%--1--%>
                                    <asp:BoundColumn DataField="estado_contrato" HeaderText="Estado Contrato"
                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="14px"></asp:BoundColumn>
                                    <%--2--%>
                                    <%--20170601 RQ020-17--%>
                                    <asp:BoundColumn DataField="contratos" HeaderText="No. Registros"
                                        ItemStyle-HorizontalAlign="Right" ItemStyle-Font-Size="14px"></asp:BoundColumn>
                                    <%--3--%>
                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more-1"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                    <!--begin::Nav-->
                                                    <ul class="kt-nav">
                                                        <li class="kt-nav__item">
                                                            <asp:LinkButton ID="lkbRegistrosPendientes" CssClass="kt-nav__link" CommandName="RegistrosPendientes" runat="server">
                                                                <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                                <span class="kt-nav__link-text">Registros Pendientes</span>
                                                            </asp:LinkButton>
                                                        </li>
                                                    </ul>
                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="table">
                            <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server" id="tblMensaje">
                                <tr>
                                    <td align="center">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--Modal--%>
    <%--Registros Pendientes--%>
    <div class="modal fade" id="mdlRegistrosPendientes" tabindex="-1" role="dialog" aria-labelledby="mdlRegistrosPendientesLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlRegistrosPendientesInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlRegistrosPendientesLabel">Registros Pendientes</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <div class="container-fluid">
                                            <section class="row">
                                                <div class="col-md-8">
                                                    <asp:Label ID="lblInformacion" CssClass="mt-5" style="margin-top: auto; margin-bottom: auto;" runat="server" />
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="btn-group float-right" role="group">
                                                        <asp:LinkButton ID="lbtExcel" CssClass="btn btn-outline-brand btn-square float-md-right" OnClick="imbExcel_Click" runat="server">
                                                            <i class="flaticon-graphic"></i>
                                                            Exportar a Excel
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table table-responsive">
                                <asp:DataGrid ID="dtgInfOperativaDet" Width="100%" runat="server" CssClass="table-bordered" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                    Visible="false">
                                    <Columns>
                                        <%--0--%>
                                        <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-Width="150px"
                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="14px"></asp:BoundColumn>
                                        <%--1--%>
                                        <asp:BoundColumn DataField="operador" HeaderText="Operador" ItemStyle-Width="500px"
                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="14px"></asp:BoundColumn>
                                        <%--2--%>
                                        <asp:BoundColumn DataField="rol_operador" HeaderText="Rol" ItemStyle-Width="250px"
                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="14px"></asp:BoundColumn>
                                        <%--3--%>
                                        <asp:BoundColumn DataField="fecha" HeaderText="Fecha Pendiente Reporte" ItemStyle-HorizontalAlign="Center"
                                            ItemStyle-Font-Size="14px" ItemStyle-Width="150px"></asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                </asp:DataGrid>
                            </div>
                            <div class="table table-responsive">
                                <asp:DataGrid ID="dtgInfTransaccionalDet" runat="server" AutoGenerateColumns="False"
                                    Width="100%" CssClass="table table-bordered" PagerStyle-HorizontalAlign="Center"
                                    Visible="false">
                                    <Columns>
                                        <%--0--%>
                                        <asp:BoundColumn DataField="numero_operacion" HeaderText="No. Operacion"></asp:BoundColumn>
                                        <%--1--%>
                                        <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación"></asp:BoundColumn>
                                        <%--2--%>
                                        <asp:BoundColumn DataField="fecha_suscripcion" HeaderText="Fecha Suscripción"></asp:BoundColumn>
                                        <%--3--%>
                                        <asp:BoundColumn DataField="tipo_subasta" HeaderText="Tipo Subasta"></asp:BoundColumn>
                                        <%--4--%>
                                        <asp:BoundColumn DataField="tipo_rueda" HeaderText="Tipo Rueda"></asp:BoundColumn>
                                        <%--5--%>
                                        <asp:BoundColumn DataField="mercado" HeaderText="Mercado"></asp:BoundColumn>
                                        <%--6--%>
                                        <asp:BoundColumn DataField="producto" HeaderText="Producto"></asp:BoundColumn>
                                        <%--7--%>
                                        <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Contrato"></asp:BoundColumn>
                                        <%--8--%>
                                        <asp:BoundColumn DataField="comprador" HeaderText="Nombre Comprador"></asp:BoundColumn>
                                        <%--9--%>
                                        <asp:BoundColumn DataField="codigo_comprador" HeaderText="Código Comprador"></asp:BoundColumn>
                                        <%--10--%>
                                        <asp:BoundColumn DataField="rol_comprador" HeaderText="Rol Comprador"></asp:BoundColumn>
                                        <%--11--%>
                                        <asp:BoundColumn DataField="vendedor" HeaderText="Nombre Vendedor"></asp:BoundColumn>
                                        <%--12--%>
                                        <asp:BoundColumn DataField="codigo_vendedor" HeaderText="Código Vendedor"></asp:BoundColumn>
                                        <%--13--%>
                                        <asp:BoundColumn DataField="rol_vendedor" HeaderText="Rol Vendedor"></asp:BoundColumn>
                                        <%--14--%>
                                        <asp:BoundColumn DataField="hora_negociacion" HeaderText="Hora Negociacion"></asp:BoundColumn>
                                        <%--15--%>
                                        <asp:BoundColumn DataField="punto_ent_ruta" HeaderText="Punto Entrega/Ruta"></asp:BoundColumn>
                                        <%--16--%>
                                        <asp:BoundColumn DataField="modalidad" HeaderText="Modalidad"></asp:BoundColumn>
                                        <%--17--%>
                                        <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial"></asp:BoundColumn>
                                        <%--18--%>
                                        <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final"></asp:BoundColumn>
                                        <%--19--%>
                                        <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad"></asp:BoundColumn>
                                        <%--20--%>
                                        <asp:BoundColumn DataField="precio" HeaderText="Precio"></asp:BoundColumn>
                                        <%--21--%>
                                        <asp:BoundColumn DataField="fecha_limite_registro" HeaderText="Fecha Limite Registro"></asp:BoundColumn>
                                        <%--22--%>
                                        <asp:BoundColumn DataField="estado" HeaderText="Estado"></asp:BoundColumn>
                                        <%--23--%>
                                        <asp:BoundColumn DataField="numero_registro" HeaderText="No. Registro"></asp:BoundColumn>
                                        <%--24--%>
                                        <asp:BoundColumn DataField="carga_extemporanea" HeaderText="Carga Extemporanea"></asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                </asp:DataGrid>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="BtnCancelar" runat="server" class="btn btn-secondary" data-dismiss="modal" Text="Cancelar" OnClick="BtnCancelar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="lbtExcel" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
