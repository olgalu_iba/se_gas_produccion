﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Verificacion
{
    public partial class frm_EnergiaTrasportar : Page
    {
        private InfoSessionVO _goInfo;
        private static readonly string lsTitulo = "Cantidad de Energía Autorizada a Transportar";
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        private clConexion lConexion;
        private SqlDataReader lLector;
        private SqlDataReader lLector1;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            _goInfo = (InfoSessionVO)Session["infoSession"];
            if (_goInfo == null) Response.Redirect("../index.aspx");
            _goInfo.Programa = lsTitulo;
            //Establese los permisos del sistema
            //EstablecerPermisosSistema(); //20220621 ajsute
            lConexion = new clConexion(_goInfo);
            //Titulo
            Master.Titulo = "Registros Operativos";
            /// Activacion de los Botones
            buttons.Inicializar(ruta: "t_energia_trasportar");
            buttons.CrearOnclick += btnNuevo;
            
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.ExportarPdfOnclick += ImgPdf_Click;

            if (IsPostBack) return;
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador > 0 and (tipo_operador = 'T' or codigo_operador = " + _goInfo.cod_comisionista + ") order by razon_social", 0, 4);  //20180122 rq003-18
            LlenarControles(lConexion.gObjConexion, ddlBusOperador, "m_operador", " estado = 'A' and codigo_operador > 0 and (tipo_operador = 'T' or codigo_operador = " + _goInfo.cod_comisionista + ") order by razon_social", 0, 4);  //20180122 rq003-18
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_no_ing_inf_ope", " codigo_reporte = 9 and codigo_operador =" + _goInfo.cod_comisionista + " and estado ='A' ");
            if (lLector.HasRows)
            {
                Toastr.Error(this, "El Operador no está obligado a ingresar la información operativa de energía autorizada a transportar.");
            }
            lLector.Close();
            lLector.Dispose();

            lConexion.Cerrar();
            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlBusOperador.SelectedValue = _goInfo.cod_comisionista;
                ddlBusOperador.Enabled = false;
                ddlOperador.SelectedValue = _goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
                hdnELiminado.Value = "N";  //20160721 modif inf opertiva y transaccional
            }
            else
                hdnELiminado.Value = "S";  //20160721 modif inf opertiva y transaccional

            DateTime ldfecha;
            ldfecha = DateTime.Now.AddDays(-1);
            TxtFecha.Text = ldfecha.Year + "/" + ldfecha.Month + "/" + ldfecha.Day;
            TxtFecha.Enabled = false;

            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            Buscar();
        }

        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(_goInfo, "t_energia_trasportar");
            //hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
            //hlkListar.Enabled = (Boolean)permisos["SELECT"];
            //hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
            //if (Session["tipoPerfil"].ToString() != "N")
            //    dtgMaestro.Columns[8].Visible = (Boolean)permisos["UPDATE"]; //20160711 ajuste modificaciones
            //else
            //    dtgMaestro.Columns[8].Visible = false;
            //dtgMaestro.Columns[9].Visible = false; //20160711 ajuste modificaciones
            //20220621 ajsute
            if ((bool)permisos["UPDATE"] && (bool)permisos["DELETE"])
                dtgMaestro.Columns[9].Visible = true; //20220705
            else
            {
                if (!(bool)permisos["UPDATE"] && !(bool)permisos["DELETE"])
                    dtgMaestro.Columns[9].Visible = false; //20220705
                else
                {
                    if (!(bool)permisos["UPDATE"])
                    {
                        foreach (DataGridItem Grilla in dtgMaestro.Items)
                        {
                            var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                            lkbModificar.Visible = false;
                        }
                    }
                    if (!(bool)permisos["DELETE"])
                    {
                        foreach (DataGridItem Grilla in dtgMaestro.Items)
                        {
                            var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
                            lkbEliminar.Visible = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Nombre: Nuevo
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
        /// Modificacion:
        /// </summary>
        private void Nuevo()
        {
            int liHoraAct = 0;
            string lsHoraMax = "0";
            int liValFin = 0;
            lConexion.Abrir();
            imbActualiza.Visible = false;
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
            if (lLector.HasRows)
            {
                lLector.Read();
                lsHoraMax = lLector["hora_max_declara_opera"].ToString();
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
            liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
            liValFin = ((Convert.ToDateTime(lsHoraMax).Hour * 60) + Convert.ToDateTime(lsHoraMax).Minute);
            if (liHoraAct > liValFin)
            {
                Toastr.Warning(this, "Está intentando Registrar Información Fuera del Horario.");
                //Cierra el modal de Agregar
                Modal.Cerrar(this, registroEnergiaTransportar.ID);

            }
            else
            {
                //Abre el modal de Agregar
                Modal.Abrir(this, registroEnergiaTransportar.ID, registroEnergiaTransportarInside.ID);
                TxtCantidad.Text = "";
                TxtCodigoCap.Text = "0"; //20220705
                imbCrear.Visible = true;
                imbActualiza.Visible = false;
                lblTitulo.Text = lsTitulo;
                TxtCodigoCap.Visible = false;
                LblCodigoCap.Visible = true;
                LblCodigoCap.Text = "Automatico";
            }
            ddlEstado.SelectedValue = "A"; //20161123 modif inf ope
            ddlEstado.Enabled = false; //20161123 modif inf ope
        }

        /// <summary>
        /// Nombre: Listar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            CargarDatos();
            lblTitulo.Text = lsTitulo;
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            var lblMensaje = new StringBuilder();
            if (!string.IsNullOrEmpty(modificar))
            {
                try
                {
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_energia_trasportar", " codigo_energia_trasportar= " + modificar + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            LblCodigoCap.Text = lLector["codigo_energia_trasportar"].ToString();
                            TxtCodigoCap.Text = lLector["codigo_energia_trasportar"].ToString();
                            DateTime ldFecha; //20160711 ajuste msificaciones
                            ldFecha = Convert.ToDateTime(lLector["fecha"].ToString()); //20160711 ajuste msificaciones
                            TxtFecha.Text = ldFecha.Year + "/" + ldFecha.Month + "/" + ldFecha.Day; //20160711 ajuste msificaciones

                            try
                            {
                                ddlOperador.SelectedValue = lLector["codigo_Operador"].ToString();
                            }
                            catch (Exception)
                            {
                                lblMensaje.Append("El operador del registro no existe o está inactivo<br>");
                            }
                            TxtCantidad.Text = lLector["energia_trasportar"].ToString();
                            ddlEstado.SelectedValue = lLector["estado"].ToString();
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                            TxtCodigoCap.Visible = false;
                            LblCodigoCap.Visible = true;
                            TxtFecha.Enabled = false;
                            ddlOperador.Enabled = false;
                            ddlEstado.Enabled = true; //20161123  modif inf ope
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Bloquea el Registro a Modificar
                        if (lblMensaje.ToString() == "") //20160809 carga ptdv
                            manejo_bloqueo("A", modificar);
                    }
                    else
                    {
                        Listar();
                        lblMensaje.Append("No se Puede editar el Registro porque está Bloqueado. Código energía a transportar" + modificar); //20180126 rq107 - 16
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append(ex.Message);
                }
            }

            if (lblMensaje.ToString() != "")
            {
                Toastr.Error(this, lblMensaje.ToString());
                return;
            }
            //Abre el modal de Agregar
            Modal.Abrir(this, registroEnergiaTransportar.ID, registroEnergiaTransportarInside.ID);
            lblTitulo.Text = lsTitulo;
        }

        /// <summary>
        /// Nombre: Buscar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
        /// Modificacion:
        /// </summary>
        private void Buscar()
        {
            lblTitulo.Text = lsTitulo;
        }
        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_codigo_energia_trasportar", "@P_fecha", "@P_codigo_operador", "@P_fecha_fin", "@P_mostrar_eliminado" }; //20160721 modif inf opertiva y trans
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar }; //20160721 modif inf opertiva y trans
            string[] lValorParametros = { "0", "", "0", "", hdnELiminado.Value }; //20160721 modif inf opertiva y trans
            DateTime ldFecha;
            var lblMensaje = new StringBuilder();

            try
            {
                if (TxtBusFecha.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtBusFecha.Text.Trim());
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append("Valor Inválido en Fecha Inicial. <br>");
                    }
                }
                if (TxtBusFechaF.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtBusFechaF.Text.Trim());
                        if (ldFecha < Convert.ToDateTime(TxtBusFecha.Text.Trim()))
                            lblMensaje.Append("La Fecha final NO puede ser menor que la Fecha Inicial.!<br>");
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append("Valor Inválido en Fecha Final. <br>");
                    }
                }
                if (lblMensaje.ToString() == "")
                {
                    if (TxtBusCodigo.Text.Trim().Length > 0)
                        lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    if (TxtBusFecha.Text.Trim().Length > 0)
                        lValorParametros[1] = TxtBusFecha.Text.Trim();
                    else
                        lValorParametros[1] = ""; // 20181113 ajuste
                    if (ddlBusOperador.SelectedValue != "0")
                        lValorParametros[2] = ddlBusOperador.SelectedValue;
                    if (TxtBusFechaF.Text.Trim().Length > 0)
                        lValorParametros[3] = TxtBusFechaF.Text.Trim();
                    else
                    {
                        if (TxtBusFecha.Text.Trim().Length > 0)
                            lValorParametros[3] = TxtBusFecha.Text.Trim();
                        else
                            lValorParametros[3] = ""; // 20181113 ajuste
                    }
                    //20220621 ajuste
                    if (lValorParametros[0] == "0" && lValorParametros[1] == "")
                    {
                        lValorParametros[1] = TxtFecha.Text;
                        lValorParametros[3] = TxtFecha.Text;
                    }
                    lConexion.Abrir();
                    dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetEnergiaTrasporte", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgMaestro.DataBind();
                    lConexion.Cerrar();
                    EstablecerPermisosSistema(); //20220621 ajuste
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Append(ex.Message);
            }
            if (lblMensaje.ToString() != "")
            {
                Toastr.Error(this, lblMensaje.ToString());
            }
        }
        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            string lCodigoRegistro = "";
            var lblMensaje = new StringBuilder();
            string lsFecha = ""; // Cambio Req. 003-17 20170216
            hdfModificacion.Value = "N"; // Cambio Req. 003-17 20170216
            lCodigoRegistro = dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text; // Cambio Req. 003-17 20170216
            lsFecha = dtgMaestro.Items[e.Item.ItemIndex].Cells[1].Text; // Cambio Req. 003-17 20170216
            ///////////////////////////////////////////////////////////////////////////////////
            ///// Control Nuevo para Modif Inf Operativa Participantes Req. 003-17 20170216 ///
            ///////////////////////////////////////////////////////////////////////////////////
            bool blPuede = true;
            int liHoraAct = 0;
            string lsHoraMax = "0";
            int liValFin = 0;
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
            if (lLector.HasRows)
            {
                lLector.Read();
                lsHoraMax = lLector["hora_max_declara_opera"].ToString();
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
            liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
            liValFin = ((Convert.ToDateTime(lsHoraMax).Hour * 60) + Convert.ToDateTime(lsHoraMax).Minute);
            if (liHoraAct > liValFin)
            {
                if (Session["tipoPerfil"].ToString() == "N")
                {
                    lblMensaje.Append("Está intentando modificar o eliminar información fuera del horario.");
                    blPuede = false;
                }
                else
                    hdfModificacion.Value = "S";
            }
            if (Convert.ToDateTime(lsFecha) != DateTime.Now.Date.AddDays(-1))
            {
                if (Session["tipoPerfil"].ToString() == "N")
                {
                    blPuede = false;
                    lblMensaje.Append("Está intentando modificar o eliminar información que no es del día anterior a la fecha del sistema.");
                }
                else
                    hdfModificacion.Value = "S";
            }
            if (blPuede)
            {
                if (e.CommandName.Equals("Modificar"))
                {
                    mdlRegistroEnergiaTransportarLabel.InnerText = "Modificar";
                    Modificar(lCodigoRegistro);
                }
                // Evento Eliminar para los Participantes Modif Inf Operativa Participantes Req. 003-17 20170216
                if (e.CommandName.Equals("Eliminar"))
                {
                    string[] lsNombreParametros = { "@P_codigo_energia_trasportar", "@P_fecha", "@P_codigo_operador", "@P_energia_trasportar", "@P_estado", "@P_accion" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
                    string[] lValorParametros = { lCodigoRegistro, "", "0", "0", "E", "3" };
                    try
                    {
                        lConexion.Abrir();
                        if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaTrasporte", lsNombreParametros, lTipoparametros, lValorParametros, _goInfo))
                        {
                            lblMensaje.Append("Se presentó un Problema en la Eliminación del Registro.! " + _goInfo.mensaje_error);
                            lConexion.Cerrar();
                        }
                        else
                        {
                            Toastr.Success(this, "Registro Eliminado Correctamente");
                            Listar();
                        }
                    }
                    catch (Exception ex)
                    {
                        /// Desbloquea el Registro Actualizado
                        lblMensaje.Append(ex.Message);
                    }
                }
            }
            if (lblMensaje.ToString() != "")
            {
                Toastr.Error(this, lblMensaje.ToString());
            }

            ///////////////////////////////////////////////////////////////////////////////////
            //if (((LinkButton)e.CommandSource).Text == "Modificar")
            //{
            //    lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            //    Modificar(lCodigoRegistro);
            //}
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia("t_energia_trasportar", lswhere, _goInfo);
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, int liIndiceLlave, int liIndiceDescripcion)
        {
            // Proceso para Cargar el DDL de ciudad
            SqlDataReader lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            var lItem = new ListItem { Value = "0", Text = "Seleccione" };
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                var lItem1 = new ListItem
                {
                    Value = lLector.GetValue(liIndiceLlave).ToString(),
                    Text = lLector.GetValue(liIndiceLlave) + "-" + lLector.GetValue(liIndiceDescripcion)
                };
                //20180122 rq003-18  // 20180126 rq107-16
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='t_energia_trasportar' and llave_registro='codigo_energia_trasportar=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_energia_trasportar=" + lscodigo_registro;
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, _goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_energia_trasportar";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(_goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(_goInfo, "t_energia_trasportar", lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] lValorParametros = { "0", "", "0", "" };
            string lsParametros = "";

            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    lsParametros += " Código energía : " + TxtBusCodigo.Text;
                }
                if (TxtBusFecha.Text.Trim().Length > 0)
                {
                    lValorParametros[1] = TxtBusFecha.Text.Trim();
                    lsParametros += " Fecha Inicial : " + TxtBusFecha.Text;
                }
                else
                {
                    lValorParametros[1] = ""; //2018113 ajuste
                    //lsParametros += " Fecha Inicial : " + TxtBusFecha.Text;  //2018113 ajuste
                }
                if (ddlBusOperador.SelectedValue != "0")
                {
                    lValorParametros[2] = ddlBusOperador.SelectedValue;
                    lsParametros += " Operador: " + ddlBusOperador.SelectedItem;
                }
                if (TxtBusFechaF.Text.Trim().Length > 0)
                {
                    lValorParametros[3] = TxtBusFechaF.Text.Trim();
                    lsParametros += " Fecha Final : " + TxtBusFechaF.Text;
                }
                else
                {
                    if (TxtBusFecha.Text.Trim().Length > 0)
                    {
                        lValorParametros[3] = TxtBusFecha.Text.Trim();
                        lsParametros += " Fecha Final : " + TxtBusFecha.Text;
                    }
                    else
                    {
                        lValorParametros[3] = ""; //2018113 ajuste
                        //lsParametros += " Fecha Final : " + TxtBusFecha.Text;  //2018113 ajuste
                    }
                }
                //20220705
                if (lValorParametros[0] == "0" && lValorParametros[1] == "")
                {
                    lValorParametros[1] = DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd");
                    lValorParametros[3] = lValorParametros[1];
                }

                Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_getEnergiaTrasporte&nombreParametros=@P_codigo_energia_trasportar*@P_fecha*@P_codigo_operador*@P_fecha_fin&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "*" + lValorParametros[3] + "&columnas=codigo_energia_trasportar*fecha*codigo_operador*nombre_operador*energia_trasportar*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado de energia autorizada a trasportar&TituloParametros=" + lsParametros);
            }
            catch (Exception)
            {
                Toastr.Error(this, "No se Pudo Generar el Informe.!");
            }
        }

        /// <summary>
        /// Nombre: ImgPdf_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgPdf_Click(object sender, EventArgs e)
        {
            try
            {
                string lsCondicion = " codigo_energia_trasportar <> '0'";
                Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=t_energia_trasportar&procedimiento=pa_ValidarExistencia&columnas=codigo_energia_trasportar*fecha*codigo_operador*energia_trasportar*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
            }
            catch (Exception)
            {
                Toastr.Error(this, "No se Pudo Generar el Informe.!");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_energia_trasportar", "@P_fecha", "@P_codigo_operador", "@P_energia_trasportar", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "", "0", "0", "", "1" };
            var lblMensaje = new StringBuilder();
            int liValor = 0;
            try
            {
                if (TxtFecha.Text == "")
                    lblMensaje.Append(" Debe digitar la fecha<br>");
                if (ddlOperador.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el operador<br>");
                //20161123 modif inf ope
                if (VerificarExistencia(" fecha='" + TxtFecha.Text + "' and codigo_operador =" + ddlOperador.SelectedValue + " and estado <>'E'"))
                    lblMensaje.Append(" La información de energía a transportar ya está registrada<br>"); //20180126 rq107 - 16
                if (TxtCantidad.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad autorizada a transportar<br>");
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidad.Text.Trim());
                        if (liValor < 0)
                            lblMensaje.Append(" Valor Inválido en la cantidad autorizada a transportar<br>");
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append(" Valor Inválido en la cantidad autorizada a transportar<br>");
                    }
                }
                //20220705
                string lsError = "N";
                if (lblMensaje.ToString() == "")
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_punto_reporte_cnt", " codigo_reporte = 9 and codigo_punto =" + ddlOperador.SelectedValue + " and estado ='A'", _goInfo))
                        lblMensaje.Append(" El transportador no tiene definida la cantidad máxima<br>");
                    else
                    {
                        if (validaCantidad())
                        {
                            lsError = "S";
                        }
                    }
                }
                if (lsError == "S" && lblMensaje.ToString() == "")
                {
                    Modal.Abrir(this, mdlConfirma.ID, mdlConfirmaInside.ID);
                }
                //20220705 fin

                if (lblMensaje.ToString() == "" && lsError == "N")//20220705
                {
                    lValorParametros[1] = TxtFecha.Text;
                    lValorParametros[2] = ddlOperador.SelectedValue;
                    lValorParametros[3] = TxtCantidad.Text;
                    lValorParametros[4] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaTrasporte", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Append("Se presentó un Problema en la Creación de la energía autorizada a transportar.!");
                        lConexion.Cerrar();
                    }
                    else
                    {
                        //TxtBusFecha.Text = TxtFecha.Text; //20160711 ajuste msificaciones //20220621
                        //Cierra el modal de Agregar
                        Modal.Cerrar(this, registroEnergiaTransportar.ID);
                        Listar();
                        TxtCantidad.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Append(ex.Message);
            }
            if (lblMensaje.ToString() != "")
            {
                Toastr.Error(this, lblMensaje.ToString());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbActualiza_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_energia_trasportar", "@P_energia_trasportar", "@P_estado", "@P_accion",
                "@P_modificacion"  }; // Campo Nuevo Req. 003-17  20170216
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int,
                SqlDbType.VarChar  }; // Campo Nuevo Req. 003-17  20170216
            string[] lValorParametros = { "0", "", "", "2",
                hdfModificacion.Value  }; // Campo Nuevo Req. 003-17  20170216
            var lblMensaje = new StringBuilder();
            int liValor = 0;
            try
            {
                //20161123 modif inf ope
                if (VerificarExistencia(" fecha='" + TxtFecha.Text + "' and codigo_operador =" + ddlOperador.SelectedValue + " and codigo_energia_trasportar<>" + LblCodigoCap.Text + " and estado <>'E'"))
                    lblMensaje.Append(" La información de energía a transportar ya está registrada<br>"); //20180126 rq107 - 16
                if (TxtCantidad.Text == "")
                    lblMensaje.Append(" Debe digitar la cantidad autorizada a transportar<br>");
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidad.Text.Trim());
                        if (liValor < 0)
                            lblMensaje.Append(" Valor Inválido en la cantidad autorizada a transportar<br>");
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append(" Valor Inválido en la cantidad autorizada a transportar<br>");
                    }
                }
                //20220705
                string lsError = "N";
                if (lblMensaje.ToString()== "")
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_punto_reporte_cnt", " codigo_reporte = 9 and codigo_punto =" + ddlOperador.SelectedValue + " and estado ='A'", _goInfo))
                        lblMensaje.Append("El trnasportador no tiene definida la cantidad máxima<br>");
                    else
                    {
                        if (validaCantidad())
                        {
                            lsError = "S";
                        }
                    }
                }
                if (lsError == "S" && lblMensaje.ToString() == "")
                {
                    Modal.Abrir(this, mdlConfirma.ID, mdlConfirmaInside.ID);
                }
                //20220705 fin

                if (lblMensaje.ToString() == "" && lsError == "N")//20220705 
                {
                    lValorParametros[0] = LblCodigoCap.Text;
                    lValorParametros[1] = TxtCantidad.Text;
                    lValorParametros[2] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaTrasporte", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Append("Se presentó un Problema en la Actualización de la cantidad autorizada.!");
                        lConexion.Cerrar();
                    }
                    else
                    {
                        manejo_bloqueo("E", LblCodigoCap.Text);
                        //Cierra el modal de Agregar
                        Modal.Cerrar(this, registroEnergiaTransportar.ID);
                        Listar();
                    }
                }
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                manejo_bloqueo("E", LblCodigoCap.Text);
                lblMensaje.Append(ex.Message);
            }
            if (lblMensaje.ToString() != "")
            {
                Toastr.Error(this, lblMensaje.ToString());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            if (TxtBusCodigo.Text.Trim().Length <= 0 && TxtBusFecha.Text.Trim().Length <= 0 && ddlBusOperador.SelectedValue == "0")
                Toastr.Error(this, "Debe Seleccionar al Menos un parámetro para realizar la Búsqueda!.");
            else
            {
                Listar();
            }
        }

        ///// Eventos Nuevos para la Implementracion del UserControl
        /// <summary>
        /// Metodo del Link Nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNuevo(object sender, EventArgs e)
        {
            mdlRegistroEnergiaTransportarLabel.InnerText = "Agregar";
            Nuevo();
        }

        /// <summary>
        /// Metodo del Link Listar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnListar(object sender, EventArgs e)
        {
            Listar();
        }

        /// <summary>
        /// 
        /// </summary>
        protected void Cancel_OnClick(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (mdlRegistroEnergiaTransportarLabel.InnerText.Equals("Modificar") && LblCodigoCap.Text != "")
                manejo_bloqueo("E", LblCodigoCap.Text);

            //Cierra el modal de Agregar
            Modal.Cerrar(this, registroEnergiaTransportar.ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20220705
        protected void BtnAceptarConf_Click(object sender, EventArgs e)
        {
            Modal.Cerrar(this, mdlConfirma.ID);
            string[] lsNombreParametros = { "@P_codigo_energia_trasportar", "@P_fecha", "@P_codigo_operador", "@P_energia_trasportar", "@P_estado", "@P_accion", "@P_dato_atipico" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "", "0", "0", "", "1", "S" };

            if (TxtCodigoCap.Text != "0")
            {
                lValorParametros[0] = TxtCodigoCap.Text;
                lValorParametros[5] = "2";
            }
            lValorParametros[1] = TxtFecha.Text;
            lValorParametros[2] = ddlOperador.SelectedValue;
            lValorParametros[3] = TxtCantidad.Text;
            lValorParametros[4] = ddlEstado.SelectedValue;

            lConexion.Abrir();
            if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaTrasporte", lsNombreParametros, lTipoparametros, lValorParametros))
            {
                Toastr.Error(this, "Se presentó un Problema en la Creación de la energía autorizada a transportar.!");
                lConexion.Cerrar();
            }
            else
            {
                Modal.Cerrar(this, registroEnergiaTransportar.ID);
                Listar();
                TxtCantidad.Text = "";
                if (LblCodigoCap.Text != "0")
                    manejo_bloqueo("E", LblCodigoCap.Text);
            }
        }
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool validaCantidad()
        {
            bool lbValida = false;
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_punto_reporte_cnt", " codigo_reporte = 9 and codigo_punto =" + ddlOperador.SelectedValue + " and cantidad_maxima < " + TxtCantidad.Text + "  and estado ='A'");
            if (lLector.HasRows)
            {
                lLector.Read();
                lblMensajeCOnf.Text = "La cantidad de energía a registrar de " + Convert.ToInt32(TxtCantidad.Text).ToString("###,###,###,###") + " MBTU supera el valor máximo histórico parametrizado para el operador " + ddlOperador.SelectedItem.ToString() + " que es de " + Convert.ToInt32(lLector["cantidad_maxima"].ToString()).ToString("###,###,###,###") + " MBTU";
                lbValida = true;
            }
            lLector.Close();
            lConexion.Cerrar();
            return lbValida;
        }

    }
}