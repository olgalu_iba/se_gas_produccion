﻿using System;
using System.Globalization;
using System.Configuration;
using System.Data;
using System.Web.UI;    
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Verificacion_frm_RegistroContratos : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Registro de Contratos";
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    string sRutaArc = ConfigurationManager.AppSettings["rutaCarga"].ToString();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);
        //Titulo
        Master.Titulo = "Registro de Contratos";

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlProducto, "m_producto", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlComprador, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4);  //20180126 rq107-16
            LlenarControles(lConexion.gObjConexion, ddlVendedor, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20180126 rq107-16
            LlenarControles(lConexion.gObjConexion, ddlEstado, "m_estado_gas", " tipo_estado = 'V' order by descripcion_estado", 2, 3); //20180126 rq107-16
            LlenarControles(lConexion.gObjConexion, ddlMercadoRel, "m_mercado_relevante", " estado ='A' order by descripcion", 0, 1); //Mercado relevante sector 20160125
            LlenarControles(lConexion.gObjConexion, ddlFuente, "m_pozo", " ind_campo_pto='C' and estado ='A' order by descripcion", 0, 1); //Campo o fuente MP 20160601
            LlenarControles1(lConexion.gObjConexion, ddlCentro, "m_divipola", " codigo_centro <> '0' order by nombre_centro", 5, 6); //20170814 rq036-17
            LlenarControles(lConexion.gObjConexion, ddlPeriodo, "m_periodos_entrega", " estado ='A' order by descripcion", 0, 1); //20171130 rq026-17
            LlenarControles2(lConexion.gObjConexion, ddlCausa, "", "", 0, 1); //20171130 rq026-17
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
            if (lLector.HasRows)
            {
                lLector.Read();
                hdfNoHorasPrim.Value = lLector["no_horas_correcion_primario"].ToString();
                hdfNoDiasSecun.Value = lLector["no_dias_cal_correc_secundario"].ToString();

            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
            //CargarDatos();
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            if (lsTabla != "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            lDdl.Items.Add(lItem1);

        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// Nombre: LlenarControles1
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    /// 20170814 rq036-17
    protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["nombre_centro"].ToString() + "-" + lLector["nombre_ciudad"].ToString() + "-" + lLector["nombre_departamento"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles2(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetCausaModAdc", null, null, null);
        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);
        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["descripcion"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// <summary>
    /// Nombre: dtgConsulta_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgConsulta.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        //this.dtgConsulta.CurrentPageIndex = 0;//20171130 rq026-17  //ajuste 20190130
        lblMensaje.Text = "";
        DateTime ldFecha;
        int liValor = 0;
        string[] lsNombreParametros = { "@P_tipo_subasta", "@P_numero_contrato", "@P_fecha_contrato", "@P_mercado", "@P_codigo_producto",
                                        "@P_operador_compra", "@P_operador_venta","@P_tipo_perfil","@P_codigo_operador", "@P_estado" ,
                                        "@P_fecha_contrato_fin", "@P_numero_contrato_fin" ,//20161207 rq102 conformacion de rutas
                                        "@P_codigo_causa", "@P_codigo_verif"}; //20171130 rq026-17
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int,
                                        SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.Int, //20161207 rq102 conformacion de rutas
                                        SqlDbType.VarChar, SqlDbType.Int};//20171130 rq026-17
        string[] lValorParametros = { "0", "0", "", "", "0", "0", "0", Session["tipoPerfil"].ToString(), goInfo.cod_comisionista, "0", "", "0", "0", "0" };//20161207 rq102 conformacion de rutas //20171130 rq026-17

        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial de Negociación. <br>"; //20161207 rq102 conformacion de rutas
            }
        }
        //20161207 rq102 conformacion de rutas
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaFin.Text);
            }
            catch (Exception)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Final de Negociación. <br>";
            }
        }
        //20161207 rq102 conformacion de rutas
        if (TxtFechaIni.Text.Trim().Length == 0 && TxtFechaFin.Text.Trim().Length > 0)
            lblMensaje.Text += "Debe digitar la Fecha Inicial de Negociación. <br>";
        //20161207 rq102 conformacion de rutas
        if (TxtFechaIni.Text.Trim().Length > 0 && TxtFechaFin.Text.Trim().Length > 0)
            try
            {
                if (Convert.ToDateTime(TxtFechaFin.Text) < Convert.ToDateTime(TxtFechaIni.Text))
                    lblMensaje.Text += "La Fecha inicial de Negociación debe ser menor o igual que la fecha final. <br>";
            }
            catch (Exception)
            { }
        if (TxtNoContrato.Text.Trim().Length > 0)
        {
            try
            {
                liValor = Convert.ToInt32(TxtNoContrato.Text);
            }
            catch (Exception)
            {
                lblMensaje.Text += "Formato Inválido en el Campo No Operación. <br>"; // 20161207 rq102 conformación de rutas
            }

        }
        // 20161207 rq102 conformación de rutas
        if (TxtNoContratoFin.Text.Trim().Length > 0)
        {
            try
            {
                liValor = Convert.ToInt32(TxtNoContratoFin.Text);
            }
            catch (Exception)
            {
                lblMensaje.Text += "Formato Inválido en el Campo No Operación final. <br>";
            }

        }
        // 20161207 rq102 conformación de rutas
        if (TxtNoContrato.Text.Trim().Length == 0 && TxtNoContratoFin.Text.Trim().Length > 0)
        {
            lblMensaje.Text += "Debe digitar el No de operación inicial antes que el final. <br>";
        }
        // 20161207 rq102 conformación de rutas
        if (TxtNoContrato.Text.Trim().Length > 0 && TxtNoContratoFin.Text.Trim().Length > 0)
        {
            try
            {
                if (Convert.ToInt32(TxtNoContratoFin.Text) < Convert.ToInt32(TxtNoContrato.Text))
                    lblMensaje.Text += "El No Operación inicial debe ser menor o igual que el final. <br>";
            }
            catch (Exception)
            {

            }
        }
        if (lblMensaje.Text == "")
        {
            try
            {
                if (TxtNoContrato.Text.Trim().Length > 0)
                    lValorParametros[1] = TxtNoContrato.Text.Trim();
                if (ddlSubasta.SelectedValue != "0")
                    lValorParametros[0] = ddlSubasta.SelectedValue;
                if (TxtFechaIni.Text.Trim().Length > 0)
                    lValorParametros[2] = TxtFechaIni.Text.Trim();
                if (ddlMercado.SelectedValue != "")
                    lValorParametros[3] = ddlMercado.SelectedValue;
                if (ddlProducto.SelectedValue != "0")
                    lValorParametros[4] = ddlProducto.SelectedValue;
                if (ddlComprador.SelectedValue != "0")
                    lValorParametros[5] = ddlComprador.SelectedValue;
                if (ddlVendedor.SelectedValue != "0")
                    lValorParametros[6] = ddlVendedor.SelectedValue;
                if (ddlEstado.SelectedValue != "0")
                    lValorParametros[9] = ddlEstado.SelectedValue;
                //20161207 rq102 conformacion de rutas
                if (TxtFechaFin.Text.Trim().Length > 0)
                    lValorParametros[10] = TxtFechaFin.Text.Trim();
                else
                    lValorParametros[10] = lValorParametros[2];
                //20161207 rq102 conformacion de rutas
                if (TxtNoContratoFin.Text.Trim().Length > 0)
                    lValorParametros[11] = TxtNoContratoFin.Text.Trim();
                else
                    lValorParametros[11] = lValorParametros[1];
                lValorParametros[12] = ddlCausa.SelectedValue; //20171130 rq026-17
                //20171130 rq026-17
                if (TxtNoId.Text.Trim().Length > 0)
                    lValorParametros[13] = TxtNoId.Text.Trim();
                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRegistroContratos", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                lConexion.Cerrar();
                if (dtgConsulta.Items.Count > 0)
                {
                    tblGrilla.Visible = true;
                    lkbExcel.Visible = true;
                    ldFecha = DateTime.Now.Date;
                    foreach (DataGridItem Grilla in this.dtgConsulta.Items)
                    {
                        if (Session["tipoPerfil"].ToString() == "B")
                        {
                            if (Grilla.Cells[14].Text == "En verificacion") //20170814 rq036-17
                                Grilla.Cells[30].Enabled = true; //20160603 fuente o campo  //20170814 rq036-17
                            else
                                Grilla.Cells[30].Enabled = false; //20160603 fuente o campo //20170814 rq036-17
                        }
                        else
                        {
                            if (Grilla.Cells[14].Text == "En verificacion") //20170814 rq036-17
                                Grilla.Cells[29].Enabled = false; //20160603 fuente o campo //20170814 rq036-17
                            else
                                Grilla.Cells[29].Enabled = true; //20160603 fuente o campo //20170814 rq036-17
                        }
                        if (Grilla.Cells[14].Text != "Contrato Registrado")  //20170814 rq036-17
                            Grilla.Cells[23].Text = "0";  //20170814 rq036-17
                    }
                }
                else
                {
                    tblGrilla.Visible = false;
                    lkbExcel.Visible = false;
                    lblMensaje.Text = "No se encontraron Registros.";
                }
                if (Session["tipoPerfil"].ToString() == "B")
                    dtgConsulta.Columns[30].Visible = true; //20160603 fuente o campo  //20170814 rq036-17
                else
                    dtgConsulta.Columns[30].Visible = false;//20160603 fuente o campo  //20170814 rq036-17
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        string lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "/" + DateTime.Now.ToShortDateString().Substring(3, 2) + "/" + DateTime.Now.ToShortDateString().Substring(0, 2);
        //string lsFecha = "2015/12/01";
        int liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
        string lsPuedeEntrar = "S";
        string lsFechaMaxCor = "";
        string lsFechaMaxModif = "";
        string lsIndVerif = "N"; // Campo nuevo algoritmo y verificacion 20150529
        string lsIndModif = "N"; // Campo nuevo algoritmo y verificacion 20150529
        // Campos nuevos control cambios modificacion Informacion transaccional 20150812
        string lsFechaIni = "";
        string lsFechaFin = "";
        string lsCodModa = "0";
        string lsCodPunto = "0";
        string lsCodRuta = "0";
        string lsCantidad = "0";
        string lsPrescio = "0";
        string lsCodTipoSubasta = "0";
        string lsConecSnt = "S"; //20171130 rq026-17
        string lsBocaPoz = "N"; //20171130 rq026-17
        string lsCentroPob = "0"; //20171130 rq026-17
        string lsPeriodo = "0"; //20171130 rq026-17
        string lsIndVar = "N"; //20171130 rq026-17
        string lsFecSus = ""; //20171130 rq026-17
        string lsContDef = ""; //20171130 rq026-17
        string lsFuente = ""; //20171130 rq026-17


        if (((LinkButton)e.CommandSource).Text == "Seleccionar")
        {
            try
            {
                TxtNumContrato.Enabled = true;
                TxtFechaSus.Enabled = true;
                ddlModalidad.Enabled = true;
                ddlPuntoEntr.Enabled = true;
                TxtCantidad.Enabled = true;
                TxtPrecio.Enabled = true;
                TxtFechaInicial.Enabled = true;
                TxtHoraInicial.Enabled = true;
                TxtFechaFinal.Enabled = true;
                TxtHoraFinal.Enabled = true;
                ddlSentidoFlujo.Enabled = true;
                TxtPresion.Enabled = true;
                ddlConecSnt.Enabled = true; //20171114 rq036-17
                ddlEntBocPozo.Enabled = true; //20171114 rq036-17
                ddlCentro.Enabled = true; //20171114 rq036-17
                ddlPeriodo.Enabled = true; //20171130 rq026-17
                ddlIndVar.Enabled = true; //20171130 rq026-17
                /////////////////////////////////////////////////////////////////////////
                /// Inhabilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///
                /////////////////////////////////////////////////////////////////////////
                TdIndica.Visible = false;
                //TdIndica1.Visible = false;
                /////////////////////////////////////////////////////////////////////////

                hdfCodVerifCont.Value = "0";
                lblTotlCantidad.Text = "0";
                lblOperacion.Text = e.Item.Cells[0].Text;
                lblSubasta.Text = e.Item.Cells[2].Text + " - Mercado: " + e.Item.Cells[3].Text;
                lblProducto.Text = e.Item.Cells[4].Text;
                hdfIdVerif.Value = e.Item.Cells[15].Text;  //20170814 rq036-17
                hdfCodTipoSub.Value = e.Item.Cells[18].Text; //20170814 rq036-17
                hdfDestinoRueda.Value = e.Item.Cells[24].Text;  //20170814 rq036-17
                hdfEstadoAct.Value = e.Item.Cells[25].Text; //20170814 rq036-17
                hdfTipoMerc.Value = e.Item.Cells[26].Text; //20170814 rq036-17
                TxtHoraInicial.Text = "00:00";
                TxtHoraFinal.Text = "00:00"; //20171130 rq026-17
                lsIndVerif = e.Item.Cells[32].Text.Trim().Replace("&nbsp;", "N"); // Campo nuevo algoritmo y verificacion 20150529 //20160603 fuente o campo  //20170814 rq036-17
                hdfOPeraC.Value = e.Item.Cells[16].Text; // Campo nuevo algoritmo y verificacion 20150529 //20170814 rq036-17
                hdfOPeraV.Value = e.Item.Cells[17].Text; // Campo nuevo algoritmo y verificacion 20150529 //20170814 rq036-17
                lsFechaMaxCor = e.Item.Cells[34].Text; // Campo nuevo algoritmo y verificacion 20150529 //20160603 fuente o campo //20170814 rq036-17
                lsIndModif = e.Item.Cells[33].Text; // Campo nuevo Modificacion Contratos 20150617 //20160603 fuente o campo //20170814 rq036-17
                lsFechaMaxModif = e.Item.Cells[35].Text; // Campo nuevo Modificacion Contratos 20150617 //20160603 fuente o campo //20170814 rq036-17

                // Campos nuevos requerimiento modificacoin inf treansaccoinal 20150812
                lsFechaIni = e.Item.Cells[37].Text; //20160603 fuente o campo  //20170814 rq036-17
                lsFechaFin = e.Item.Cells[38].Text; //20160603 fuente o campo  //20170814 rq036-17
                lsCodModa = e.Item.Cells[39].Text; //20160603 fuente o campo  //20170814 rq036-17
                lsCodPunto = e.Item.Cells[40].Text; //20160603 fuente o campo  //20170814 rq036-17
                lsCodRuta = e.Item.Cells[43].Text; //20160603 fuente o campo  //20170814 rq036-17
                lsCantidad = e.Item.Cells[41].Text; //20160603 fuente o campo  //20170814 rq036-17
                hdfCantCnt.Value = e.Item.Cells[41].Text; //20160603 fuente o campo  //20170814 rq036-17
                lsPrescio = e.Item.Cells[42].Text; //20160603 fuente o campo  //20170814 rq036-17
                lsCodTipoSubasta = e.Item.Cells[18].Text; //20170814 rq036-17

                lsConecSnt = e.Item.Cells[45].Text; //20171130 rq026-17
                lsBocaPoz = e.Item.Cells[46].Text; //20171130 rq026-17
                lsCentroPob = e.Item.Cells[47].Text; //20171130 rq026-17
                lsPeriodo = e.Item.Cells[48].Text; //20171130 rq026-17
                lsIndVar = e.Item.Cells[49].Text; //20171130 rq026-17
                //20171130 rq026-17
                if (e.Item.Cells[50].Text == "&nbsp;")
                    lsFecSus = "";
                else
                    lsFecSus = e.Item.Cells[50].Text;
                //20171130 rq026-17
                if (e.Item.Cells[51].Text == "&nbsp;")
                    lsContDef = "";
                else
                    lsContDef = e.Item.Cells[51].Text;
                lsFuente = e.Item.Cells[27].Text; //20171130 rq026-17

                if (goInfo.cod_comisionista == e.Item.Cells[16].Text) //20170814 rq036-17
                {
                    hdfPunta.Value = "C";
                    lblPunta.Text = "COMPRADOR";
                    lnlNomOpera.Text = "Nombre del Comprador";
                    lblTipoDocOpera.Text = "Tipo Documento Comprador";
                    lblNoDocOPera.Text = "No. Documento Comprador";
                    lblNomOperador.Text = e.Item.Cells[5].Text;
                    lblTipoDocOperador.Text = e.Item.Cells[19].Text;  //20170814 rq036-17
                    lblNoDocumentoOperaqdor.Text = e.Item.Cells[21].Text;  //20170814 rq036-17
                    tblDemanda.Visible = true;
                    /////////////////////////////////////////////////////////////////////////
                    /// Habilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///
                    /////////////////////////////////////////////////////////////////////////
                    if (hdfDestinoRueda.Value == "G" && hdfTipoMerc.Value == "P")
                    {
                        TdIndica.Visible = true;
                        //TdIndica1.Visible = true;
                    }
                    /////////////////////////////////////////////////////////////////////////
                }
                else
                {
                    hdfPunta.Value = "V";
                    lblPunta.Text = "VENDEDOR";
                    lblTipoDocOpera.Text = "Tipo Documento Vendedor";
                    lnlNomOpera.Text = "Nombre del Vendedor";
                    lblNoDocOPera.Text = "No. Documento Vendedor";
                    lblNomOperador.Text = e.Item.Cells[6].Text;
                    lblTipoDocOperador.Text = e.Item.Cells[20].Text;  //20170814 rq036-17
                    lblNoDocumentoOperaqdor.Text = e.Item.Cells[22].Text;  //20170814 rq036-17
                }
                if (hdfEstadoAct.Value == "R" || hdfEstadoAct.Value == "D" || hdfEstadoAct.Value == "B")
                {
                    lsPuedeEntrar = "N";
                    tblDemanda.Visible = false;
                    lblMensaje.Text = "No se puede Ingresar o Actualizar la Información YA que el Contrato está en un estado NO válido para Modificación.!";
                }
                else
                {
                    if (lsIndVerif == "N") // Campo nuevo algoritmo y verificacion 20150529
                    {
                        if (lsIndModif == "S")
                        {
                            if (DateTime.Now > Convert.ToDateTime(lsFechaMaxModif))
                            {
                                lsPuedeEntrar = "N";
                                lblMensaje.Text = "No se puede Ingresar a Modificar la Información YA que se venció el plazo para hacerlo. {" + lsFechaMaxModif + "}!"; //20161207 rq102 conformacion de rutas
                                tblDemanda.Visible = false;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(lsFecha) > Convert.ToDateTime(e.Item.Cells[13].Text))  //20170814 rq036-17
                            {
                                lsPuedeEntrar = "N";
                                lblMensaje.Text = "No se puede Ingresar o Actualizar la Información YA que se venció el plazo para hacerlo. {" + e.Item.Cells[13].Text + "}!"; //20161207 rq102 conformacion de rutas //20170814 rq036-17
                                tblDemanda.Visible = false;
                            }
                            //if (lsFecha == e.Item.Cells[13].Text)  //20170814 rq036-17
                            //{
                            //    lConexion.Abrir();
                            //    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_verificacion", " codigo_tipo_subasta = " + hdfCodTipoSub.Value + " And tipo_mercado = '" + e.Item.Cells[25].Text.Trim() + "' And '" + e.Item.Cells[28].Text + "' >= hora_inicial_negociacion And '" + e.Item.Cells[28].Text + "' <= hora_final_negociacion ");  //20170814 rq036-17
                            //    if (lLector.HasRows)
                            //    {
                            //        lLector.Read();
                            //        lsHoraFin = lLector["hora_maxima_registro"].ToString();
                            //        liValFin = ((Convert.ToDateTime(lsHoraFin).Hour * 60) + Convert.ToDateTime(lsHoraFin).Minute);
                            //    }
                            //    lLector.Close();
                            //    lLector.Dispose();
                            //    lConexion.Cerrar();

                            //    if (liHoraAct > liValFin)
                            //    {
                            //        lsPuedeEntrar = "N";
                            //        tblDemanda.Visible = false;
                            //        lblMensaje.Text = "No se puede Ingresar o Actualizar la Información YA que se vencio el plazo para hacerlo. {" + e.Item.Cells[12].Text + "-" + lsHoraFin + "}!"; //20161207 rq102 conformacion de rutas //20170814 rq036-17
                            //    }
                            //}
                        }
                    }
                    else
                    {
                        if (DateTime.Now <= Convert.ToDateTime(lsFechaMaxCor))
                        {
                            lsPuedeEntrar = "S";
                        }
                        else
                        {
                            lsPuedeEntrar = "N";
                            lblMensaje.Text = "No se puede Ingresar o Corregir la Información YA que se venció el plazo para hacerlo. {" + lsFechaMaxCor + "}!"; //20161207 rq102 conformacion de rutas
                        }
                    }
                }
                if (lsPuedeEntrar == "S")
                {
                    if (!manejo_bloqueo("V", hdfIdVerif.Value))
                    {
                        /// Validaciones Cuando el Que entra en la pantalla es negociador
                        //if (hdfEstadoAct.Value != "R" && hdfEstadoAct.Value != "S")
                        //{
                        if (hdfDestinoRueda.Value == "G")
                        {
                            lblPuntoE.Text = "Punto de entrega de la energía al comprador";
                            lblCantidad.Text = "Cantidad de energía contratada (MBTUD)";
                            lblPrecio.Text = "Precio a la fecha de suscripción del contrato (USD/MBTU)";
                            lblFechaInc.Text = "Fecha Hora de inicio de la obligación de entrega";
                            lblFechaFin.Text = "Fecha Hora de terminación de la obligación de entrega";
                            TrTrans.Visible = false;
                        }
                        else
                        {

                            lblPuntoE.Text = "Tramo o grupos de gasoductos";
                            lblCantidad.Text = "Capacidad Contratada  (KPCD)";
                            lblPrecio.Text = "Tarifa a la fecha de suscripción del contrato (USD/KPC)";
                            lblFechaInc.Text = "Fecha Hora de inicio de la prestación del servicio";
                            lblFechaFin.Text = "Fecha Hora de terminación prestación del servicio";
                            if (hdfTipoMerc.Value == "P")
                                TrTrans.Visible = true;
                            else
                                TrTrans.Visible = false;

                        }
                        ddlModalidad.Items.Clear();
                        ddlPuntoEntr.Items.Clear();
                        dlTipoDemanda.Items.Clear();
                        ddlMotivoModifi.Items.Clear();
                        ddlCesionario.Items.Clear();

                        lConexion.Abrir();

                        if (hdfPunta.Value == "C")
                        {
                            string[] lsNombreParametrosC = { "@P_cadena" };
                            SqlDbType[] lTipoparametrosC = { SqlDbType.VarChar };
                            string[] lValorParametrosC = { " Delete from t_contrato_datos_usuarios where codigo_cont_datos = 0 And login_usuario = '" + goInfo.Usuario.ToString() + "' " };
                            /// Se obtiene los Datos de la Punta Compradora
                            DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_EjecutarCadena", lsNombreParametrosC, lTipoparametrosC, lValorParametrosC);
                        }
                        /// LLeno los Controles
                        LlenarControles(lConexion.gObjConexion, ddlModalidad, "m_modalidad_contractual", " estado = 'A'  order by descripcion", 0, 1);
                        if (hdfDestinoRueda.Value == "G")
                            LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_pozo", " estado = 'A'  order by descripcion", 0, 1);
                        else
                        {
                            if (hdfCodTipoSub.Value == "3")  //Subasta UVLP 20161005
                                LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_ruta_snt", " estado in ('A','E') order by descripcion", 0, 4);
                            else
                                LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_ruta_snt", " estado = 'A' order by descripcion", 0, 4);
                        }
                        LlenarControles(lConexion.gObjConexion, dlTipoDemanda, "m_tipo_demanda_atender", " estado = 'A' order by descripcion", 0, 1);
                        LlenarControles(lConexion.gObjConexion, ddlMotivoModifi, "m_motivos_modificacion", " estado = 'A' order by descripcion", 0, 1);
                        LlenarControles(lConexion.gObjConexion, ddlCesionario, "m_operador", " estado = 'A' And codigo_operador !=  " + goInfo.cod_comisionista + " order by razon_social", 0, 4);
                        //20190607 rq036-19
                        if (lsCodTipoSubasta == "5")
                        {
                            ddlPeriodo.Items.Clear();
                            //lConexion.Abrir();
                            LlenarControles(lConexion.gObjConexion, ddlPeriodo, "m_periodos_entrega per, m_caracteristica_sub scar", " per.estado = 'A'  and scar.destino_rueda = '" + hdfDestinoRueda.Value + "'  and scar.tipo_mercado= '" + hdfTipoMerc.Value + "' and scar.codigo_tipo_subasta = 5 and scar.estado ='A' and scar.tipo_caracteristica ='E' and scar.codigo_caracteristica = per.codigo_periodo  order by per.descripcion", 0, 1);
                            //lConexion.Cerrar();
                        }
                        else
                        {
                            ddlPeriodo.Items.Clear();
                            LlenarControles(lConexion.gObjConexion, ddlPeriodo, "m_periodos_entrega per", " per.estado = 'A'  order by per.descripcion", 0, 1);
                        }
                        //20190607  fin rq036-19
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_contrato_datos_verif", " codigo_verif_contrato = " + hdfIdVerif.Value + " And punta_contrato = '" + hdfPunta.Value + "' ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            /// LLamo metodo que bloquea los campos de la pantalla cuando las partes entran a corregir la informacion 
                            /// Requerimiento ALgoritmo y verificacion 20150529
                            if (lsIndVerif == "S")
                                inactivarCampos();
                            /////////////////////
                            // Inactiva campos que vienen del contrato 20150812
                            /////////////////////
                            if (lsCodTipoSubasta != "5")
                            {
                                ddlModalidad.Enabled = false;
                                ddlPuntoEntr.Enabled = false;
                                //TxtCantidad.Enabled = false;
                                TxtPrecio.Enabled = false;
                                TxtFechaInicial.Enabled = false;
                                TxtFechaFinal.Enabled = false;
                                ddlPeriodo.Enabled = false; //20171130 rq026
                                TxtHoraInicial.Visible = false;//20171130 rq026
                                TxtHoraFinal.Visible = false;//20171130 rq026
                            }
                            //Cambio de restrcciio de campos 20160125
                            if (lsCodTipoSubasta == "5")
                            {
                                TxtFechaInicial.Enabled = false;
                                TxtFechaFinal.Enabled = false;
                            }
                            hdfAccion.Value = "M";
                            TrModifi.Visible = true;
                            if (Session["tipoPerfil"].ToString() == "N")
                            {
                                btnCrear.Visible = false;
                                btnActualizar.Visible = true;
                            }
                            else
                            {
                                btnCrear.Visible = false;
                                btnActualizar.Visible = false;
                            }
                            /// Recuperacion Datos
                            DateTimeFormatInfo fmt = (new CultureInfo("hr-HR")).DateTimeFormat;
                            DateTime ldFecha;
                            string lsFechaIniTmp = "";
                            string lsFechaFinTmp = "";
                            hdfCodVerifCont.Value = lLector["codigo_cont_datos"].ToString();
                            TxtNumContrato.Text = lLector["contrato_definitivo"].ToString();
                            TxtFechaSus.Text = lLector["fecha_suscripcion_cont"].ToString().Substring(6, 4) + "/" + lLector["fecha_suscripcion_cont"].ToString().Substring(3, 2) + "/" + lLector["fecha_suscripcion_cont"].ToString().Substring(0, 2);
                            try
                            {
                                ddlModalidad.SelectedValue = lLector["codigo_modalidad"].ToString();
                            }
                            catch (Exception)
                            {

                            }
                            //20190718 ajuste
                            try
                            {
                                ddlPuntoEntr.SelectedValue = lLector["codigo_punto_entrega"].ToString();
                            }
                            catch (Exception)
                            {

                            }
                            // campo o fuente MP 20160601
                            try
                            {
                                ddlFuente.SelectedValue = lLector["codigo_fuente"].ToString();
                            }
                            catch (Exception)
                            {
                            }
                            TxtCantidad.Text = lLector["cantidad"].ToString();
                            TxtPrecio.Text = lLector["precio"].ToString();
                            ldFecha = Convert.ToDateTime(lLector["fecha_inicial"].ToString());
                            lsFechaIniTmp = ldFecha.ToString("t", fmt);
                            ldFecha = Convert.ToDateTime(lLector["fecha_final"].ToString());
                            lsFechaFinTmp = ldFecha.ToString("t", fmt);

                            TxtFechaInicial.Text = lLector["fecha_inicial"].ToString().Substring(6, 4) + "/" + lLector["fecha_inicial"].ToString().Substring(3, 2) + "/" + lLector["fecha_inicial"].ToString().Substring(0, 2);
                            TxtFechaFinal.Text = lLector["fecha_final"].ToString().Substring(6, 4) + "/" + lLector["fecha_final"].ToString().Substring(3, 2) + "/" + lLector["fecha_final"].ToString().Substring(0, 2);
                            TxtHoraInicial.Text = lsFechaIniTmp;
                            TxtHoraFinal.Text = lsFechaFinTmp;
                            hdfRutaArchivo.Value = lLector["ruta_contrato"].ToString();
                            if (hdfPunta.Value == "C")
                            {
                                //try
                                //{
                                //    dlTipoDemanda.SelectedValue = lLector["codigo_tipo_demanda"].ToString();
                                //}
                                //catch (Exception ex)
                                //{

                                //}
                                //hdfTipoDemanda.Value = dlTipoDemanda.SelectedValue;
                                tblDemanda.Visible = true;
                                btnActualUsu.Visible = false;
                                btnCrearUsu.Visible = true;
                                CargarDatosUsu();
                                //if (dlTipoDemanda.SelectedValue == "1")
                                //    TxtUsuarioFinal.Enabled = false;
                                //if (hdfTipoDemanda.Value == "1")
                                //{
                                //    TxtUsuarioFinal.Enabled = false;
                                //    lblMercadoRelev.Text = "Mercado Relevante Usuario Regulado";
                                //    if (hdfDestinoRueda.Value == "G")
                                //        lblCantContra.Text = "Cantidad a entregar";
                                //    else
                                //        lblCantContra.Text = "Capacidad Contratada";
                                //}
                                //else
                                //{
                                //    if (hdfDestinoRueda.Value == "G")
                                //    {
                                //        lblCantContra.Text = "Cantidad Contratada por usuario (MBTUD)";
                                //        lblMercadoRelev.Text = "Mercado Relevante Usuario No Regulado";
                                //    }
                                //    else
                                //    {
                                //        lblMercadoRelev.Text = "Mercado Relevante Transporte Usuario No Regulado";
                                //        lblCantContra.Text = "Capacidad Contratada por usuario (KPCD)";
                                //    }
                                //}

                            }
                            if (hdfDestinoRueda.Value == "T" && hdfTipoMerc.Value == "P")
                            {
                                try
                                {
                                    ddlSentidoFlujo.SelectedValue = lLector["sentido_flujo"].ToString().Replace("&nbsp;", "");
                                }
                                catch (Exception)
                                {

                                }
                                TxtPresion.Text = lLector["presion_punto_fin"].ToString();
                            }
                        }
                        else
                        {
                            lLector.Close();
                            lLector.Dispose();
                            /// Lleno los campos que vienen del contrato y deshabilito los campos 20150812
                            // Se bloquean los campos para las subastas UVCP, SSCI y BIMESTRAL
                            if (lsCodTipoSubasta != "5")
                            {
                                try
                                {
                                    ddlModalidad.SelectedValue = lsCodModa;
                                    if (hdfDestinoRueda.Value == "G")
                                        ddlPuntoEntr.SelectedValue = lsCodPunto;
                                    else
                                        ddlPuntoEntr.SelectedValue = lsCodRuta;
                                    TxtCantidad.Text = lsCantidad;
                                    TxtPrecio.Text = lsPrescio;
                                    TxtFechaInicial.Text = lsFechaIni;
                                    if (lsCodTipoSubasta == "2")
                                        TxtFechaFinal.Text = lsFechaIni;
                                    else
                                        TxtFechaFinal.Text = lsFechaFin;
                                    ddlModalidad.Enabled = false;
                                    ddlPuntoEntr.Enabled = false;
                                    //TxtCantidad.Enabled = false;
                                    TxtPrecio.Enabled = false;
                                    TxtFechaInicial.Enabled = false;
                                    TxtFechaFinal.Enabled = false;
                                }
                                catch (Exception)
                                {

                                }
                            }
                            if (lsCodTipoSubasta == "5") //Crea el else para traer las fechas 20151228                                                            
                            {
                                ddlModalidad.SelectedValue = lsCodModa;
                                if (hdfDestinoRueda.Value == "G")
                                    ddlPuntoEntr.SelectedValue = lsCodPunto;
                                else
                                    ddlPuntoEntr.SelectedValue = lsCodRuta;
                                TxtCantidad.Text = lsCantidad;
                                TxtPrecio.Text = lsPrescio;
                                TxtFechaInicial.Text = lsFechaIni;
                                TxtFechaFinal.Text = lsFechaFin;
                                TxtFechaInicial.Enabled = false;
                                TxtFechaFinal.Enabled = false;
                            }
                            ddlConecSnt.SelectedValue = lsConecSnt; //20171130 rq026-17
                            ddlEntBocPozo.SelectedValue = lsBocaPoz; //20171130 rq026-17
                            //20171130 rq026-17
                            try
                            {
                                ddlCentro.SelectedValue = lsCentroPob;
                            }
                            catch (Exception)
                            { }
                            //20190607 rq036-19
                            try
                            {
                                ddlPeriodo.SelectedValue = lsPeriodo; //20171130 rq026-17
                            }
                            catch (Exception)
                            { }
                            ddlIndVar.SelectedValue = lsIndVar; //20171130 rq026-17
                            TxtFechaSus.Text = lsFecSus; //20171130 rq026-17
                            TxtNumContrato.Text = lsContDef; //20171130 rq026-17
                            ddlFuente.SelectedValue = lsFuente; //20171130 rq026-17
                            ddlPeriodo_SelectedIndexChanged(null, null);

                            if (hdfPunta.Value == "C")
                            {
                                tblDemanda.Visible = true;
                                btnActualUsu.Visible = false;
                                btnCrearUsu.Visible = true;
                                CargarDatosUsu();
                                //if (dlTipoDemanda.SelectedValue == "1")
                                //    TxtUsuarioFinal.Enabled = false;
                                //if (hdfTipoDemanda.Value == "1")
                                //{
                                //    TxtUsuarioFinal.Enabled = false;
                                //    lblMercadoRelev.Text = "Mercado Relevante Usuario Regulado";
                                //    if (hdfDestinoRueda.Value == "G")
                                //        lblCantContra.Text = "Cantidad a entregar";
                                //    else
                                //        lblCantContra.Text = "Capacidad Contratada";
                                //}
                                //else
                                //{
                                //    if (hdfDestinoRueda.Value == "G")
                                //    {
                                //        lblCantContra.Text = "Cantidad Contratada por usuario (MBTUD)";
                                //        lblMercadoRelev.Text = "Mercado Relevante Usuario No Regulado";
                                //    }
                                //    else
                                //    {
                                //        lblMercadoRelev.Text = "Mercado Relevante Transporte Usuario No Regulado";
                                //        lblCantContra.Text = "Capacidad Contratada por usuario (KPCD)";
                                //    }
                                //}
                            }
                            TrModifi.Visible = false;
                            hdfAccion.Value = "C";
                            if (Session["tipoPerfil"].ToString() == "N")
                            {
                                btnCrear.Visible = true;
                                btnActualizar.Visible = false;
                            }
                            else
                            {
                                btnCrear.Visible = false;
                                btnActualizar.Visible = false;
                            }
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        //dlTipoDemanda_SelectedIndexChanged(null, null);

                        tblGrilla.Visible = false;
                        tblRegCnt.Visible = true;
                        tblDatos.Visible = false;
                        //}
                        //else
                        //    lblMensaje.Text = "El Registro No Puede ser Modificado, Ya que esta en Un estado NO Válido para Modificacion.!";
                    }
                    else
                        lblMensaje.Text = "No se puede Modificar el Registro YA que se encuentra Bloqueado por el Administrador.!";
                }
                //20170814 rq036-17
                if (hdfDestinoRueda.Value == "G" && hdfTipoMerc.Value == "S" && e.Item.Cells[44].Text == "S") //20170814 rq036-17
                {
                    trSum.Visible = true;
                    ddlConecSnt.SelectedValue = e.Item.Cells[45].Text; //20170814 rq036-17
                    ddlEntBocPozo.SelectedValue = e.Item.Cells[46].Text; //20170814 rq036-17
                    ddlConecSnt_SelectedIndexChanged(null, null);
                    //lblCentro.Visible = false;
                    //ddlCentro.Visible = false;
                    ddlCentro.SelectedValue = e.Item.Cells[47].Text; //20170814 rq036-17
                }
                else
                {
                    trSum.Visible = false;
                    ddlConecSnt.SelectedValue = "N";
                    ddlEntBocPozo.SelectedValue = "N";
                    ddlCentro.SelectedValue = "0";
                    lblPuntoE.Visible = true;
                    ddlPuntoEntr.Visible = true;
                }
                //20190607 rq036-19
                try
                {
                    ddlPeriodo.SelectedValue = e.Item.Cells[48].Text; //20171130 rq026-17
                }
                catch (Exception)
                { }
                ddlIndVar.SelectedValue = e.Item.Cells[49].Text; //20171130 rq026-17
                ddlPeriodo_SelectedIndexChanged(null, null);//20171130 rq026-17
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas en la Recuperación de la Información. " + ex.Message.ToString(); //20180126 rq107-16

            }
            //Fuente o campo MP 20160601
            if (hdfDestinoRueda.Value == "G" && hdfTipoMerc.Value == "P")
            {
                TrFuente.Visible = true;
            }
            else
            {
                TrFuente.Visible = false;
                ddlFuente.SelectedValue = "0";
            }
            //20171130 rq026-17
            if (hdfCodTipoSub.Value != "5")
            {
                ddlPeriodo.Enabled = false;
                TxtHoraInicial.Enabled = false;
                TxtHoraFinal.Enabled = false;
            }
        }
        /// Realiza la visualizacion de la Pantalla de Verificacion
        if (((LinkButton)e.CommandSource).Text == "Verificar")
        {
            string[] lsNombreParametros = { "@P_codigo_cont_datos", "@P_codigo_verif_contrato", "@P_punta_contrato" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
            Object[] lValorParametros = { "0", "0", "" };
            string lsVerifica = "1"; // 1=Correcta 0=Incorrecta

            try
            {
                lblOperacion.Text = e.Item.Cells[0].Text;
                lblSubasta.Text = e.Item.Cells[2].Text + " - Mercado: " + e.Item.Cells[3].Text;
                lblProducto.Text = e.Item.Cells[4].Text;
                hdfIdVerif.Value = e.Item.Cells[15].Text;  //20170814 rq036-17
                hdfCodTipoSub.Value = e.Item.Cells[18].Text;  //20170814 rq036-17
                hdfDestinoRueda.Value = e.Item.Cells[24].Text;  //20170814 rq036-17
                hdfEstadoAct.Value = e.Item.Cells[25].Text;  //20170814 rq036-17
                hdfTipoMerc.Value = e.Item.Cells[26].Text;  //20170814 rq036-17
                hdfOPeraC.Value = e.Item.Cells[16].Text;  //20170814 rq036-17
                hdfOPeraV.Value = e.Item.Cells[17].Text;  //20170814 rq036-17

                //// Obtengo la Informacion del Comprador
                lConexion.Abrir();
                lValorParametros[1] = hdfIdVerif.Value;
                lValorParametros[2] = "C";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetRegistroContratosDatos", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error == "")
                {
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lblDat1C.Text = lLector["contrato_definitivo"].ToString().Trim().ToUpper();
                        lblDat2C.Text = lLector["fecha_suscripcion_cont"].ToString().Trim();
                        lblDat3C.Text = lLector["codigo_modalidad"].ToString() + " - " + lLector["desc_modalidad"].ToString().Trim();
                        lblDat4C.Text = lLector["codigo_punto_entrega"].ToString() + " - " + lLector["desc_punto_ent"].ToString().Trim();
                        lblDat5C.Text = lLector["cantidad"].ToString().Trim();
                        lblDat6C.Text = lLector["precio"].ToString().Trim();
                        lblDat7C.Text = lLector["fecha_inicial"].ToString().Trim();
                        lblDat8C.Text = lLector["fecha_final"].ToString().Trim();
                        lblDat9C.Text = lLector["sentido_flujo"].ToString().Trim();
                        lblDat10C.Text = lLector["presion_punto_fin"].ToString().Trim();
                        if (lLector["ruta_contrato"].ToString().Trim().Length > 0)
                            LinkC.HRef = "../archivos/" + lLector["ruta_contrato"].ToString().Trim();
                        else
                            LinkC.Visible = false;
                        lblDat4Op.Text = lLector["entrega_cont"].ToString();
                        lblDat3Op.Text = lLector["modalidad_cont"].ToString();
                        lblDat5Op.Text = lLector["cantidad_cont"].ToString().Trim();
                        lblDat6Op.Text = lLector["precio_cont"].ToString().Trim();
                        lblDat7Op.Text = lLector["fecha_inicial_cont"].ToString().Trim();
                        lblDat8Op.Text = lLector["fecha_final_cont"].ToString().Trim();
                    }
                }
                else
                {
                    lblMensaje.Text = "Problemas al Obtener los Datos de la Compra.! " + goInfo.mensaje_error.ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + lblMensaje.Text + "');", true);
                }
                lLector.Close();
                lLector.Dispose();
                //// Obtengo los Datos de la Venta
                lValorParametros[2] = "V";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetRegistroContratosDatos", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error == "")
                {
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lblDat1V.Text = lLector["contrato_definitivo"].ToString().Trim().ToUpper();
                        lblDat2V.Text = lLector["fecha_suscripcion_cont"].ToString().Trim();
                        lblDat3V.Text = lLector["codigo_modalidad"].ToString().Trim() + " - " + lLector["desc_modalidad"].ToString().Trim();
                        lblDat4V.Text = lLector["codigo_punto_entrega"].ToString().Trim() + " - " + lLector["desc_punto_ent"].ToString().Trim();
                        lblDat5V.Text = lLector["cantidad"].ToString().Trim();
                        lblDat6V.Text = lLector["precio"].ToString().Trim();
                        lblDat7V.Text = lLector["fecha_inicial"].ToString().Trim();
                        lblDat8V.Text = lLector["fecha_final"].ToString().Trim();
                        lblDat9V.Text = lLector["sentido_flujo"].ToString().Trim();
                        lblDat10V.Text = lLector["presion_punto_fin"].ToString().Trim();
                        if (lLector["ruta_contrato"].ToString().Trim().Length > 0)
                            LinkV.HRef = "../archivos/" + lLector["ruta_contrato"].ToString().Trim();
                        else
                            LinkV.Visible = false;
                    }
                }
                else
                {
                    lblMensaje.Text = "Problemas al Obtener los Datos de la Compra.! " + goInfo.mensaje_error.ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + lblMensaje.Text + "');", true);
                }
                lLector.Close();
                lLector.Dispose();
                lblDat1Ver.Text = "Correcta";
                lblDat2Ver.Text = "Correcta";
                lblDat3Ver.Text = "Correcta";
                lblDat4Ver.Text = "Correcta";
                lblDat5Ver.Text = "Correcta";
                lblDat6Ver.Text = "Correcta";
                lblDat7Ver.Text = "Correcta";
                lblDat8Ver.Text = "Correcta";
                lblDat9Ver.Text = "Correcta";
                lblDat10Ver.Text = "Correcta";
                /// Realizo la Verificacion entre los Datos de la Compra y la Venta
                if (hdfDestinoRueda.Value == "G")
                {
                    TrTra1.Visible = false;
                    TrTra2.Visible = false;
                    lblTit1.Text = "Número del contrato";
                    lblTit2.Text = "Fecha de suscripción del contrato";
                    lblTit3.Text = "Modalidad de Contrato";
                    lblTit4.Text = "Punto de entrega de la energía al comprador";
                    lblTit5.Text = "Cantidad de energía contratada (MBTUD)";
                    lblTit6.Text = "Precio a la fecha de suscripción del contrato (USD/MBTU)";
                    lblTit7.Text = "Fecha de inicio de la obligación de entrega";
                    lblTit8.Text = "Fecha de terminación de la obligación de entrega";
                    lblTit11.Text = "Copia Contrato";
                    if (lblDat1V.Text != lblDat1C.Text)
                    {
                        lsVerifica = "0";
                        lblDat1Ver.Text = "Incorrecta";
                    }
                    if (lblDat2V.Text != lblDat2C.Text)
                    {
                        lsVerifica = "0";
                        lblDat2Ver.Text = "Incorrecta";
                    }
                    if (lblDat3V.Text != lblDat3C.Text)
                    {
                        lsVerifica = "0";
                        lblDat3Ver.Text = "Incorrecta";
                    }
                    if (lblDat4V.Text != lblDat4C.Text)
                    {
                        lsVerifica = "0";
                        lblDat4Ver.Text = "Incorrecta";
                    }
                    if (lblDat5V.Text != lblDat5C.Text)
                    {
                        lsVerifica = "0";
                        lblDat5Ver.Text = "Incorrecta";
                    }
                    if (lblDat6V.Text != lblDat6C.Text)
                    {
                        lsVerifica = "0";
                        lblDat6Ver.Text = "Incorrecta";
                    }
                    if (lblDat7V.Text != lblDat7C.Text)
                    {
                        lsVerifica = "0";
                        lblDat7Ver.Text = "Incorrecta";
                    }
                    if (lblDat8V.Text != lblDat8C.Text)
                    {
                        lsVerifica = "0";
                        lblDat8Ver.Text = "Incorrecta";
                    }
                }
                else
                {
                    lblTit1.Text = "Número del contrato";
                    lblTit2.Text = "Fecha de suscripción del contrato";
                    lblTit3.Text = "Modalidad de Contrato";
                    lblTit4.Text = "Tramo o grupos de gasoductos";
                    lblTit5.Text = "Capacidad Contratada  (KPCD)";
                    lblTit6.Text = "Tarifa a la fecha de suscripción del contrato (USD/KPC)";
                    lblTit7.Text = "Fecha de inicio de la prestación del servicio";
                    lblTit8.Text = "Fecha de terminación prestación del servicio";
                    lblTit9.Text = "Sentido Contratado para el flujo de Gas Natural";
                    lblTit10.Text = "Presión Punto Final";
                    lblTit11.Text = "Copia Contrato";
                    if (lblDat1V.Text != lblDat1C.Text)
                    {
                        lsVerifica = "0";
                        lblDat1Ver.Text = "Incorrecta";
                    }
                    if (lblDat2V.Text != lblDat2C.Text)
                    {
                        lsVerifica = "0";
                        lblDat2Ver.Text = "Incorrecta";
                    }
                    if (lblDat3V.Text != lblDat3C.Text)
                    {
                        lsVerifica = "0";
                        lblDat3Ver.Text = "Incorrecta";
                    }
                    if (lblDat4V.Text != lblDat4C.Text)
                    {
                        lsVerifica = "0";
                        lblDat4Ver.Text = "Incorrecta";
                    }
                    if (lblDat5V.Text != lblDat5C.Text)
                    {
                        lsVerifica = "0";
                        lblDat5Ver.Text = "Incorrecta";
                    }
                    if (lblDat6V.Text != lblDat6C.Text)
                    {
                        lsVerifica = "0";
                        lblDat6Ver.Text = "Incorrecta";
                    }
                    if (lblDat7V.Text != lblDat7C.Text)
                    {
                        lsVerifica = "0";
                        lblDat7Ver.Text = "Incorrecta";
                    }
                    if (lblDat8V.Text != lblDat8C.Text)
                    {
                        lsVerifica = "0";
                        lblDat8Ver.Text = "Incorrecta";
                    }
                    if (hdfTipoMerc.Value == "P")
                    {
                        TrTra1.Visible = true;
                        TrTra2.Visible = true;
                        if (lblDat9V.Text != lblDat9C.Text)
                        {
                            lsVerifica = "0";
                            lblDat9Ver.Text = "Incorrecta";
                        }
                        if (lblDat10V.Text != lblDat10C.Text)
                        {
                            lsVerifica = "0";
                            lblDat10Ver.Text = "Incorrecta";
                        }
                    }
                    else
                    {
                        TrTra1.Visible = false;
                        TrTra2.Visible = false;
                    }
                }
                if (lsVerifica == "0")
                {
                    btnRegCont.Visible = false;
                }
                else
                    btnRegCont.Visible = true;
                tblGrilla.Visible = false;
                tblVerifica.Visible = true;
                tblDatos.Visible = false;
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas en la Recuperación de la Información. " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Metodo que inactiva los campos de la pantalla cuando se esta corrigiendo el registro despues
    /// de la verificacion.
    /// Requerimiento ALgoritmo y verificacion 20150529
    /// </summary>
    protected void inactivarCampos()
    {
        string[] lsNombreParametros = { "@P_codigo_cont_datos", "@P_codigo_verif_contrato", "@P_punta_contrato" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
        Object[] lValorParametros = { "0", "0", "" };
        SqlDataReader oLector;

        string lsContratoC = "";
        string lsfechaSusC = "";
        string lsCodModaC = "";
        string lsCodPtoC = "";
        string lsCantC = "";
        string lsPrecC = "";
        string lsFecIniC = "";
        string lsFecFinC = "";
        string lsSentiC = "";
        string lsPresionC = "";
        string lsConectadoSntC = "";  //20171114 rq036-17
        string lsEntBocaPozoC = ""; //20171114 rq036-17
        string lsCentroC = ""; //20171114 rq036-17
        string lsPeriodoC = "";//20171130 rq026-17
        string lsIndVarC = "";//20171130 rq026-17


        string lsContratoV = "";
        string lsfechaSusV = "";
        string lsCodModaV = "";
        string lsCodPtoV = "";
        string lsCantV = "";
        string lsPrecV = "";
        string lsFecIniV = "";
        string lsFecFinV = "";
        string lsSentiV = "";
        string lsPresionV = "";
        string lsConectadoSntV = "";  //20171114 rq036-17
        string lsEntBocaPozoV = ""; //20171114 rq036-17
        string lsCentroV = ""; //20171114 rq036-17
        string lsPeriodoV = ""; //20171130 rq026-17
        string lsIndVarV = "";//20171130 rq026-17

        try
        {
            lConexion1.Abrir();
            // Obtengo los Datos de la Punta Compradora
            lValorParametros[1] = hdfIdVerif.Value;
            lValorParametros[2] = "C";
            oLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion1.gObjConexion, "pa_GetRegistroContratosDatos", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
            if (goInfo.mensaje_error == "")
            {
                if (oLector.HasRows)
                {
                    oLector.Read();
                    lsContratoC = oLector["contrato_definitivo"].ToString().Trim().ToUpper();
                    lsfechaSusC = oLector["fecha_suscripcion_cont"].ToString().Trim();
                    lsCodModaC = oLector["codigo_modalidad"].ToString() + " - " + oLector["desc_modalidad"].ToString().Trim();
                    lsCodPtoC = oLector["codigo_punto_entrega"].ToString() + " - " + oLector["desc_punto_ent"].ToString().Trim();
                    lsCantC = oLector["cantidad"].ToString().Trim();
                    lsPrecC = oLector["precio"].ToString().Trim();
                    lsFecIniC = oLector["fecha_inicial"].ToString().Trim();
                    lsFecFinC = oLector["fecha_final"].ToString().Trim();
                    lsSentiC = oLector["sentido_flujo"].ToString().Trim();
                    lsPresionC = oLector["presion_punto_fin"].ToString().Trim();
                    lsConectadoSntC = oLector["conectado_snt"].ToString().Trim();  //20171114 rq036-17
                    lsEntBocaPozoC = oLector["ent_boca_pozo"].ToString().Trim(); //20171114 rq036-17
                    lsCentroC = oLector["codigo_centro_pob"].ToString().Trim(); //20171114 rq036-17
                    lsPeriodoC = oLector["codigo_periodo"].ToString().Trim(); //20171130 rq026-17
                    lsIndVarC = oLector["ind_contrato_var"].ToString().Trim(); //20171130 rq026-17
                }
            }
            oLector.Close();
            oLector.Dispose();
            // Obtengo los Datos de la Punta Vendedora
            lValorParametros[1] = hdfIdVerif.Value;
            lValorParametros[2] = "V";
            oLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion1.gObjConexion, "pa_GetRegistroContratosDatos", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
            if (goInfo.mensaje_error == "")
            {
                if (oLector.HasRows)
                {
                    oLector.Read();
                    lsContratoV = oLector["contrato_definitivo"].ToString().Trim().ToUpper();
                    lsfechaSusV = oLector["fecha_suscripcion_cont"].ToString().Trim();
                    lsCodModaV = oLector["codigo_modalidad"].ToString() + " - " + oLector["desc_modalidad"].ToString().Trim();
                    lsCodPtoV = oLector["codigo_punto_entrega"].ToString() + " - " + oLector["desc_punto_ent"].ToString().Trim();
                    lsCantV = oLector["cantidad"].ToString().Trim();
                    lsPrecV = oLector["precio"].ToString().Trim();
                    lsFecIniV = oLector["fecha_inicial"].ToString().Trim();
                    lsFecFinV = oLector["fecha_final"].ToString().Trim();
                    lsSentiV = oLector["sentido_flujo"].ToString().Trim();
                    lsPresionV = oLector["presion_punto_fin"].ToString().Trim();
                    lsConectadoSntV = oLector["conectado_snt"].ToString().Trim();  //20171114 rq036-17
                    lsEntBocaPozoV = oLector["ent_boca_pozo"].ToString().Trim(); //20171114 rq036-17
                    lsCentroV = oLector["codigo_centro_pob"].ToString().Trim(); //20171114 rq036-17
                    lsPeriodoV = oLector["codigo_periodo"].ToString().Trim(); //20171130 rq026-17
                    lsIndVarV = oLector["ind_contrato_var"].ToString().Trim(); //20171130 rq026-17
                }
            }
            oLector.Close();
            oLector.Dispose();
            // Compraro los datos para inhabilitar las cajas de texto
            if (lsContratoV == lsContratoC)
                TxtNumContrato.Enabled = false;
            if (lsfechaSusV == lsfechaSusC)
                TxtFechaSus.Enabled = false;
            if (lsCodModaV == lsCodModaC)
                ddlModalidad.Enabled = false;
            if (lsCodPtoV == lsCodPtoC)
                ddlPuntoEntr.Enabled = false;
            if (lsCantV == lsCantC)
                TxtCantidad.Enabled = false;
            if (lsPrecV == lsPrecC)
                TxtPrecio.Enabled = false;
            if (lsFecIniV == lsFecIniC)
            {
                TxtFechaInicial.Enabled = false;
                TxtHoraInicial.Enabled = false;
            }
            if (lsFecFinV == lsFecFinC)
            {
                TxtFechaFinal.Enabled = false;
                TxtHoraFinal.Enabled = false;
            }
            if (lsSentiV == lsSentiC)
                ddlSentidoFlujo.Enabled = false;
            if (lsPresionV == lsPresionC)
                TxtPresion.Enabled = false;
            //20171114 rq036-17
            if (lsConectadoSntV == lsConectadoSntC)
                ddlConecSnt.Enabled = false;
            //20171114 rq036-17
            if (lsEntBocaPozoV == lsEntBocaPozoC)
                ddlEntBocPozo.Enabled = false;
            //20171114 rq036-17
            if (lsCentroV == lsCentroC)
                ddlCentro.Enabled = false;
            //20171130 rq026-17
            if (lsPeriodoV == lsPeriodoC)
                ddlPeriodo.Enabled = false;
            //20171130 rq026-17
            if (lsIndVarV == lsIndVarC)
                ddlIndVar.Enabled = false;

            lConexion1.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Se presentó un Error en el bloqueo de los Campos de la Corrección. " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_operador)
    {
        string lsCondicion = "nombre_tabla='t_contrato_verificacion' and llave_registro='codigo_verif_contrato=" + lscodigo_operador + "'";
        string lsCondicion1 = "codigo_verif_contrato=" + lscodigo_operador.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "t_contrato_verificacion";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_contrato_verificacion", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Realiza las Validaciones de los Tipos de Datos y Campos Obligatorios de la Pantalla
    /// </summary>
    /// <returns></returns>
    protected string validaciones()
    {
        decimal ldValor = 0;
        int liValor = 0;
        DateTime lFecha;
        string lsError = "";
        string[] lsPrecio;
        string[] lsFecha;

        try
        {
            if (TxtNumContrato.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar el No. de Contrato. <br>";
            if (TxtFechaSus.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar la Fecha de Suscripción. <br>";
            else
            {
                try
                {
                    lFecha = Convert.ToDateTime(TxtFechaSus.Text.Trim());
                    lsFecha = TxtFechaSus.Text.Trim().Split('/');
                    if (lsFecha[0].Trim().Length != 4)
                        lsError += "Formato Inválido en Fecha de Suscripción. <br>";
                }
                catch (Exception)
                {
                    lsError += "Valor Inválido en Fecha de Suscripción. <br>";
                }
            }
            if (ddlModalidad.SelectedValue == "0" && TxtOtrModalidad.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar la Modalidad de Contrato. <br>";
            //20170814 RQ036-17
            if (!trSum.Visible || !ddlCentro.Visible)
            {
                if (ddlPuntoEntr.SelectedValue == "0" && TxtOtrPuntoE.Text.Trim().Length <= 0)
                    lsError += "Debe Ingresar el Punto de Entrega. <br>";
            }
            if (TxtCantidad.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar " + lblCantidad.Text + " . <br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCantidad.Text.Trim());
                    if (liValor <= 0)
                        lsError += "La " + lblCantidad.Text + " debe ser mayor que cero. <br>";
                    // Validacion nueva de cantidad requerimiento varios verificacion 20151117
                    if (hdfCodTipoSub.Value != "5")
                    {
                        if (liValor > Convert.ToInt32(hdfCantCnt.Value))
                            lsError += "La " + lblCantidad.Text + " No puede ser Mayor a la Cantidad adjudicada. <br>";
                    }
                }
                catch (Exception)
                {
                    lsError += "Valor Inválido en " + lblCantidad.Text + ". <br>";
                }
            }
            if (TxtPrecio.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar " + lblPrecio.Text + " . <br>";
            else
            {
                try
                {
                    ldValor = Convert.ToDecimal(TxtPrecio.Text.Trim());
                    if (ldValor < 0)
                        lsError += "Valor Inválido en " + lblPrecio.Text + ". <br>";
                    else
                    {
                        lsPrecio = TxtPrecio.Text.Trim().Split('.');
                        if (lsPrecio.Length > 1)
                        {
                            if (lsPrecio[1].Trim().Length > 2)
                                lsError += "Valor Inválido en " + lblPrecio.Text + ". <br>";
                        }
                    }
                }
                catch (Exception)
                {
                    lsError += "Valor Inválido en " + lblPrecio.Text + ". <br>";
                }
            }
            if (TxtFechaInicial.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar " + lblFechaInc.Text + ". <br>";
            else
            {
                try
                {
                    lFecha = Convert.ToDateTime(TxtFechaInicial.Text.Trim());
                    lsFecha = TxtFechaInicial.Text.Trim().Split('/');
                    if (lsFecha[0].Trim().Length != 4)
                        lsError += "Formato Inválido en " + lblFechaInc.Text + ". <br>";
                    //if (Convert.ToDateTime(TxtFechaInicial.Text.Trim()) < Convert.ToDateTime(TxtFechaSus.Text.Trim()))
                    //    lsError += "La " + lblFechaInc.Text + " NO puede ser menor que la Fecha de Suscripcion. <br>";
                }
                catch (Exception)
                {
                    lsError += "Valor Inválido en " + lblFechaInc.Text + ". <br>";
                }
            }
            if (TxtFechaFinal.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar " + lblFechaFin.Text + ". <br>";
            else
            {
                try
                {
                    lFecha = Convert.ToDateTime(TxtFechaFinal.Text.Trim());
                    if (Convert.ToDateTime(TxtFechaFinal.Text.Trim()) < Convert.ToDateTime(TxtFechaInicial.Text.Trim()))
                        lsError += "La " + lblFechaFin.Text + " No puede ser menor que " + lblFechaInc.Text + " . <br>";
                    lsFecha = TxtFechaFinal.Text.Trim().Split('/');
                    if (lsFecha[0].Trim().Length != 4)
                        lsError += "Formato Inválido en " + lblFechaFin.Text + ". <br>";
                }
                catch (Exception)
                {
                    lsError += "Valor Inválido en " + lblFechaFin.Text + ". <br>";
                }
            }
            if (hdfAccion.Value == "C")
            {
                //if (FuCopiaCont.FileName.Trim().Length <= 0)
                //    lsError += "Debe Seleccionar el Archivo del Contrato. <br>";
            }
            else
            {
                if (ddlMotivoModifi.SelectedValue == "0")
                    lsError += "Debe Seleccionar el Motivo de Modificación. <br>"; // 20161207 rq102 conformacion de rutas
                //20171130 rq026-17
                //else
                //{
                //    if (DelegadaBase.Servicios.ValidarExistencia("m_motivos_modificacion", " codigo_motivo_modifica = " + ddlMotivoModifi.SelectedValue + " And cesion = 'S' ", goInfo))
                //    {
                //        if (ddlCesionario.SelectedValue == "0")
                //            lsError += "Debe Seleccionar el Cesionario YA que el Motivo de Modificación es Cesión. <br>"; // 20161207 rq102 conformacion de rutas
                //    }
                //}
            }
            if (hdfPunta.Value == "C")
            {
                //if (dlTipoDemanda.SelectedValue == "0")
                //    lsError += "Debe Seleccionar El Tipo de Demanda a Atender. <br>";
                //else
                //{
                string lsCont = "0";
                if (hdfCodVerifCont.Value.Trim().Length > 0)
                    lsCont = hdfCodVerifCont.Value;
                if (lsCont == "0")
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("t_contrato_datos_usuarios", " codigo_cont_datos = " + lsCont + " And login_usuario = '" + goInfo.Usuario + "'", goInfo))
                        lsError += "No ha Ingresado Información de Usuarios Finales. <br>"; //20161207 rq102 conformacion de rutas
                    else
                    {
                        //if (hdfDestinoRueda.Value == "G")
                        //{
                            if (Convert.ToDecimal(TxtCantidad.Text) != Convert.ToDecimal(lblTotlCantidad.Text))
                                lsError += "Hay Diferencias entre a Cantidad del Contrato y la Cantidad de los usuarios Finales. <br>";
                        //}
                        //else // Valicacion agregada para mercado relevante secto 20160125
                        //{
                        //    if (Convert.ToDecimal(TxtCantidad.Text) < Convert.ToDecimal(lblTotlCantidad.Text))
                        //        lsError += "La Cantidad del Contrato no puede ser menor que la Cantidad de los usuarios Finales. <br>";
                        //}
                    }
                }
                else
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("t_contrato_datos_usuarios", " codigo_cont_datos = " + lsCont + " ", goInfo))
                        lsError += "No ha Ingresado Información de Usuarios Finales. <br>"; //20161207 rq102 conformacion de rutas
                    else
                    {
                        if (Convert.ToDecimal(TxtCantidad.Text) != Convert.ToDecimal(lblTotlCantidad.Text))
                            lsError += "Hay Diferencias entre la Cantidad del Contrato y la Cantidad de los usuarios Finales. <br>"; //20161207 rq102 conformacion de rutas //20180126 rq107-16
                    }
                }
                //}
            }
            //20160810 valida fecha
            //20190520
            //if (DelegadaBase.Servicios.ValidarExistencia("t_contrato con, t_id_rueda idr, m_periodos_entrega per ", " con.numero_contrato =" + lblOperacion.Text + " and con.numero_id = idr.numero_id  and idr.codigo_periodo = per.codigo_periodo and case when per.medida_tiempo ='I' then con.fecha_creacion else convert(varchar(10),con.fecha_creacion,112) end > case when per.medida_tiempo ='I' then '" + TxtFechaInicial.Text + " " + TxtHoraInicial.Text + "' else '" + TxtFechaInicial.Text + "' end ", goInfo)) //20181016 ajsute
            //    lsError += "La fecha Inicial debe ser mayor que la fecha de negociación. <br>";

            if (hdfDestinoRueda.Value == "T" && hdfTipoMerc.Value == "P")
            {
                if (ddlSentidoFlujo.SelectedValue == "")
                    lsError += "Debe Ingresar Sentido Contratado para el flujo de Gas Natural. <br>";
                if (TxtPresion.Text.Trim().Length <= 0)
                    lsError += "Debe Ingresar Presión para el punto de terminación del servicio (psig). <br>";
                else
                {
                    ////////////////////////////////////////////////////////////////////////////////////////////////
                    //// Validacion Nueva sentido del flujo Requerimiento Ajuste presion transporte 20151006 ///////
                    ////////////////////////////////////////////////////////////////////////////////////////////////
                    string[] lsPresion;
                    try
                    {
                        if (TxtPresion.Text.Trim().Length > 500)
                            lsError += "Valor Inválido en Presión para el punto de terminación del servicio (psig). <br>"; //20161207 rq102 conformacion de rutas
                        else
                        {
                            lsPresion = TxtPresion.Text.Trim().Split('-');
                            foreach (string Presion in lsPresion)
                            {
                                try
                                {
                                    ldValor = Convert.ToDecimal(Presion.Trim());
                                }
                                catch (Exception)
                                {
                                    lsError += "Valor Inválido en Presión para el punto de terminación del servicio (psig). <br>";
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        lsError += "Valor Inválido en Presión para el punto de terminación del servicio (psig). <br>";
                    }

                }
            }
            //Se agrega valiadación fuente-campo MP 20160601
            if (hdfDestinoRueda.Value == "G" && ddlFuente.SelectedValue == "0" && hdfTipoMerc.Value == "P")
            {
                lsError += "Debe seleccionar la fuente. <br>";
            }

            //validacion para rueda neg directa 20151228 
            if (hdfCodTipoSub.Value == "5")
            {
                if (ddlModalidad.SelectedValue != "0")
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_caracteristica_sub", " codigo_tipo_subasta = 5 and tipo_caracteristica ='M' and destino_rueda ='" + hdfDestinoRueda.Value + "' and tipo_mercado='" + hdfTipoMerc.Value + "' and estado ='A' and codigo_caracteristica =" + ddlModalidad.SelectedValue, goInfo))
                        lsError += "La modalidad de contrto no está parametrizada para el tipo de mercado y producto. <br>";
                }
                // se cambia la validacion para MS por fuente campo MP 20160601
                if (hdfDestinoRueda.Value == "G" && ddlPuntoEntr.SelectedValue != "0" && hdfTipoMerc.Value == "S")
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_caracteristica_sub", " codigo_tipo_subasta = 5 and tipo_caracteristica ='T' and destino_rueda ='G' and tipo_mercado='S' and estado ='A' and codigo_caracteristica =" + ddlPuntoEntr.SelectedValue, goInfo))
                        lsError += "El punto de entrega no está parametrizado para el tipo de mercado y producto. <br>";
                    //Valida restricciones de subasta 20160127
                    //20180802 ajuste
                    //if (DelegadaBase.Servicios.ValidarExistencia("m_pozo pozo, m_tipos_rueda tpo, t_contrato con, t_id_rueda idr, m_restriccion_subasta rest", " pozo.codigo_pozo = " + ddlPuntoEntr.SelectedValue + " And rest.codigo_tipo_subasta = 5 and tpo.codigo_tipo_subasta = 5 and tpo.destino_rueda ='" + hdfDestinoRueda.Value + "' and tpo.tipo_mercado = '" + hdfTipoMerc.Value + "' and con.numero_contrato = " + lblOperacion.Text + " and con.numero_id = idr.numero_id  and tpo.codigo_tipo_rueda = rest.codigo_tipo_rueda and rest.codigo_periodo = idr.codigo_periodo and rest.codigo_modalidad =" + ddlModalidad.SelectedValue + " and (pozo.codigo_tipo_campo = rest.codigo_tipo_campo or rest.codigo_tipo_campo=0) And rest.estado ='A'", goInfo))
                    //    lsError += "La Modalidad, Periodo, Tipo de Mercado y Tipo de Campo atado al Punto de Entrega está en los parametros de restricción de subasta para Bilateral. <br>";
                }
                // se agrega validacion para MP fuente campo MP 20160601
                if (hdfDestinoRueda.Value == "G" && hdfTipoMerc.Value == "P")
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_pozo poz, m_caracteristica_sub carC", " poz.codigo_pozo = " + ddlFuente.SelectedValue + "  and carC.codigo_tipo_subasta = 5 and carC.tipo_caracteristica ='C' and carC.destino_rueda ='G' and carC.tipo_mercado='" + hdfTipoMerc.Value + "' and carC.estado ='A' and codigo_caracteristica = poz.codigo_tipo_campo", goInfo))
                        lsError += "El tipo de campo no está parametrizado para el tipo de mercado y producto. <br>";
                    //Valida restricciones de subasta 20160127
                    //20180802 ajuste
                    //if (DelegadaBase.Servicios.ValidarExistencia("m_pozo pozo, m_tipos_rueda tpo, t_contrato con, t_id_rueda idr, m_restriccion_subasta rest", " pozo.codigo_pozo = " + ddlFuente.SelectedValue + " And rest.codigo_tipo_subasta = 5 and tpo.codigo_tipo_subasta = 5 and tpo.destino_rueda ='" + hdfDestinoRueda.Value + "' and tpo.tipo_mercado = '" + hdfTipoMerc.Value + "' and con.numero_contrato = " + lblOperacion.Text + " and con.numero_id = idr.numero_id  and tpo.codigo_tipo_rueda = rest.codigo_tipo_rueda and rest.codigo_periodo = idr.codigo_periodo and rest.codigo_modalidad =" + ddlModalidad.SelectedValue + " and (pozo.codigo_tipo_campo = rest.codigo_tipo_campo or rest.codigo_tipo_campo=0) And rest.estado ='A'", goInfo))
                    //    lsError += "La Modalidad, Periodo, Tipo de Mercado y Tipo de Campo atado a la fuente está en los parametros de restricción de subasta para Bilateral. <br>";
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_pozo poz, m_campo_modalidad cam", " poz.codigo_pozo = " + ddlFuente.SelectedValue + "  and poz.codigo_tipo_campo= cam.codigo_tipo_campo and cam.codigo_modalidad =" + ddlModalidad.SelectedValue + " and cam.tipo_mercado='P' and cam.estado='A'", goInfo))
                        lsError += "El tipo de contrato no está parametrizado para el tipo de campo. <br>";
                    if (DelegadaBase.Servicios.ValidarExistencia("m_pozo poz, m_campo_periodo cam, t_contrato con", " poz.codigo_pozo = " + ddlFuente.SelectedValue + "  and poz.codigo_tipo_campo= cam.codigo_tipo_campo and cam.codigo_tipo_subasta = 5 and cam.destino_rueda='G' and  cam.tipo_mercado='P' and cam.estado ='A' and con.numero_contrato =" + lblOperacion.Text + " and (con.fecha_creacion < cam.fecha_inicial or convert(varchar(10),con.fecha_creacion, 111 ) > convert(varchar(10),cam.fecha_final,111))", goInfo))
                        lsError += "La fecha de negociación no está dentro del periodo para registro de negociaciones para el tipo de campo. <br>";
                    //20160808 modalidad campo
                    //20190404 rq018-19 fase II    
                    //lConexion.Abrir();
                    //lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_pozo poz, m_tipo_campo tpo, m_modalidad_contractual mod, m_campo_modalidad cam", " poz.codigo_pozo =" + ddlFuente.SelectedValue + " and poz.codigo_tipo_campo = tpo.codigo_tipo_campo and tpo.estado ='A' and mod.codigo_modalidad =" + ddlModalidad.SelectedValue + " and mod.ind_contingencia ='N' and poz.codigo_tipo_campo = cam.codigo_tipo_campo and mod.codigo_modalidad = cam.codigo_modalidad and cam.tipo_mercado = 'P' and cam.estado='A' and cam.ind_restriccion ='S'");  //fuente campo MP 20160601
                    //if (lLector.HasRows)
                    //{
                    //    lLector.Read();
                    //    if (lLector["mes_inicio"].ToString() != "0")
                    //        if (Convert.ToInt32(lLector["mes_inicio"].ToString()) != Convert.ToDateTime(TxtFechaInicial.Text).Month || Convert.ToInt32(lLector["dia_inicio"].ToString()) != Convert.ToDateTime(TxtFechaInicial.Text).Day)
                    //            lsError += "La fecha de inicio (" + TxtFechaInicial.Text + ") no corresponde con el mes y día parametrizado para el tipo de campo " + lLector["mes_inicio"].ToString() + "-" + lLector["dia_inicio"].ToString() + "<br>";
                    //    if (lLector["mes_fin"].ToString() != "0")
                    //        if (Convert.ToInt32(lLector["mes_fin"].ToString()) != Convert.ToDateTime(TxtFechaFinal.Text).Month || Convert.ToInt32(lLector["dia_fin"].ToString()) != Convert.ToDateTime(TxtFechaFinal.Text).Day)
                    //            lsError += "La fecha de finalización (" + TxtFechaFinal.Text + ") no corresponde con el mes y día parametrizado para el tipo de campo " + lLector["mes_fin"].ToString() + "-" + lLector["dia_fin"].ToString() + "<br>";
                    //    if (lLector["mes_inicio"].ToString() != "0" && lLector["mes_fin"].ToString() != "0")
                    //    {
                    //        int ldAnos = Convert.ToDateTime(TxtFechaFinal.Text).Year - Convert.ToDateTime(TxtFechaInicial.Text).Year;
                    //        if (ldAnos <= 0 || ldAnos > 1 && ldAnos < 5)
                    //            lsError += "La duración para este campo debe ser de 1 o mayor o igual que 5 años <br>";
                    //    }
                    //}
                    //lLector.Close();
                    //lLector.Dispose();
                    //lConexion.Cerrar();
                    //20190404 fin rq018-19 fase II

                    ////20190301 ajuste
                    /////20190404 rq018-19 fase II
                    //if (ddlModalidad.SelectedValue == "2" || ddlModalidad.SelectedValue == "3")
                    //{
                    //    if (Convert.ToDateTime(TxtFechaFinal.Text) > Convert.ToDateTime("2019/11/30"))
                    //        if (DelegadaBase.Servicios.ValidarExistencia("t_contrato ", " numero_contrato = " + lblOperacion.Text + "  and fecha_creacion < '2019/07/01'", goInfo))
                    //            lsError += "La fecha de finalización debe ser máximo hasta el 2019/11/30. <br>";
                    //}
                }
                //20180802 ajuste
                if (DelegadaBase.Servicios.ValidarExistencia("m_tipos_rueda tpo, t_contrato con, t_id_rueda idr, m_restriccion_subasta rest left join m_pozo pozo on (pozo.codigo_pozo =" + ddlPuntoEntr.SelectedValue + ")", " rest.codigo_tipo_subasta = 5 and tpo.codigo_tipo_subasta = 5 and tpo.destino_rueda ='" + hdfDestinoRueda.Value + "' and tpo.tipo_mercado = '" + hdfTipoMerc.Value + "' and con.numero_contrato = " + lblOperacion.Text + " and con.numero_id = idr.numero_id  and tpo.codigo_tipo_rueda = rest.codigo_tipo_rueda and rest.codigo_periodo = idr.codigo_periodo and rest.codigo_modalidad =" + ddlModalidad.SelectedValue + " and (pozo.codigo_tipo_campo = rest.codigo_tipo_campo or rest.codigo_tipo_campo=0) And rest.estado ='A'", goInfo))
                    lsError += "La Modalidad, Periodo de entrega y tipo de mercado se encuentra dentro de los parámetros de restricción por subasta. <br>";
            }
            //20190301 ajuste
            //20190404 rq018-19 fase II
            //if (hdfTipoMerc.Value == "S" && Convert.ToDateTime(TxtFechaInicial.Text) > Convert.ToDateTime("2019/11/30"))
            //{
            //    if (DelegadaBase.Servicios.ValidarExistencia("m_periodos_entrega ", " codigo_periodo = " + ddlPeriodo.SelectedValue + "  and medida_tiempo ='U'", goInfo))
            //        lsError += "La fecha de inicio debe ser máximo hasta el 2019/11/30. <br>";
            //}
            //20160823 validacion de la PTDVF
            if (hdfDestinoRueda.Value == "G" && hdfTipoMerc.Value == "P" && (hdfCodTipoSub.Value == "5" || hdfCodTipoSub.Value == "6"))
            {
                if (ddlPuntoEntr.SelectedValue != "0" && TxtFechaInicial.Text != "" && TxtFechaFinal.Text != "" && TxtCantidad.Text != "")
                {
                    if (ddlModalidad.SelectedValue == "1" || ddlModalidad.SelectedValue == "2" || ddlModalidad.SelectedValue == "3" || ddlModalidad.SelectedValue == "4")
                    {
                        try
                        {
                            string[] lsNombreParametros = { "@P_codigo_operador", "@P_codigo_punto", "@P_fecha_ini", "@P_fecha_fin", "@P_cantidad", "@P_codigo_modalidad", "@P_numero_contrato" };
                            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                            string[] lValorParametros = { hdfOPeraV.Value, ddlPuntoEntr.SelectedValue, TxtFechaInicial.Text, TxtFechaFinal.Text, TxtCantidad.Text, ddlModalidad.SelectedValue, lblOperacion.Text };

                            lConexion.Abrir();
                            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetValidaPtdvf", lsNombreParametros, lTipoparametros, lValorParametros);
                            if (lLector.HasRows)
                            {
                                while (lLector.Read())
                                    lsError += lLector["error"].ToString() + "<br>";
                            }
                            lLector.Close();
                            lConexion.Cerrar();
                        }
                        catch (Exception)
                        {
                            lLector.Close();
                            lConexion.Cerrar();
                        }
                    }
                }
            }
            //20170425 rq011-17
            //if (hdfDestinoRueda.Value == "G" && ddlPuntoEntr.SelectedValue != "0")  //20170814 rq036-17
            //{
            //    //vaida que el punto de entrega este parametizado para a subasta
            //    if (!DelegadaBase.Servicios.ValidarExistencia("m_caracteristica_sub ", " codigo_tipo_subasta =" + hdfCodTipoSub.Value + " and tipo_caracteristica ='T' and destino_rueda ='G' and estado ='A' and tipo_mercado ='" + hdfTipoMerc.Value + "' and codigo_caracteristica = " + ddlPuntoEntr.SelectedValue, goInfo))
            //        lsError += "El punto de entrega no está habilitado para la subasta-mercado. <br>";
            //}
            //fin 20170425 rq011-17
            //20170814 rq036-17
            if (trSum.Visible)
            {
                if (ddlConecSnt.SelectedValue == "N" && ddlEntBocPozo.SelectedValue == "N" && ddlCentro.SelectedValue == "0")
                    lsError += "Debe seleccionar el centro poblado. <br>";
            }
            //validaciones rq026-15 20171130
            if (ddlPeriodo.SelectedValue == "0")
                lsError += "Debe seleccionar el periodo de entrega. <br>";
            string[] lsNombreParametrosVal = { "@P_numero_contrato", "@P_contrato_definitivo", "@P_fecha_inicial", "@P_fecha_final", "@P_ind_contrato_var", "@P_codigo_periodo", "@P_fecha_suscripcion", "@P_codigo_modalidad", "@P_codigo_fuente" }; //20190404 rq018-19 fase II
            SqlDbType[] lTipoparametrosVal = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int }; //20190404 rq018-19 fase II
            string[] lValorParametrosVal = { lblOperacion.Text, TxtNumContrato.Text, TxtFechaInicial.Text, TxtFechaFinal.Text, ddlIndVar.SelectedValue, ddlPeriodo.SelectedValue, TxtFechaSus.Text, ddlModalidad.SelectedValue, ddlFuente.SelectedValue }; //20190404 rq018-19 fase II
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_ValidaRegSist", lsNombreParametrosVal, lTipoparametrosVal, lValorParametrosVal, goInfo);
            if (lLector.HasRows)
            {
                while (lLector.Read())
                    lsError += lLector["error"].ToString() + "<br>";
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
            //fin validaciones rq026-15 20171130
            return lsError;
        }
        catch (Exception ex)
        {
            if (lsError == "")
                return "Problemas en la Validación de la Información. " + ex.Message.ToString();
            else
                return lsError;
        }
    }
    /// <summary>
    /// Realiza la Limpieza de los Campo de Captura en la Pantalla luego de la Creación o Actualizacion
    /// </summary>
    protected void LimpiarCampos()
    {
        TxtNumContrato.Text = "";
        TxtFechaSus.Text = "";
        ddlModalidad.SelectedValue = "0";
        ChkOtrModa.Checked = false;
        TxtOtrModalidad.Text = "";
        ddlPuntoEntr.SelectedValue = "0";
        ChkOtrPuntoE.Checked = false;
        TxtOtrPuntoE.Text = "";
        TxtCantidad.Text = "";
        TxtPrecio.Text = "";
        TxtFechaInicial.Text = "";
        TxtFechaFinal.Text = "";
        //dlTipoDemanda.SelectedValue = "0";
        ddlSentidoFlujo.SelectedValue = "";
        TxtPresion.Text = "";
        ddlFuente.SelectedValue = "0"; // campo o fuente MP 20160601
    }
    /// <summary>
    /// Nombre: dtgUsuarios_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgUsuarios_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";

        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            try
            {
                ////////////////////////////////////////////////////////////////////////////////////////
                /// OJO Cambios de los Indices por adicion de Campo Req. 009-17 Indicadores 20170321 ///
                ////////////////////////////////////////////////////////////////////////////////////////
                string lsTipoDoc = "";
                if (this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[14].Text == "1")  //rq009-17
                    lsTipoDoc = "Nit";
                else
                    lsTipoDoc = "Cedula";
                hdfCodDatUsu.Value = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text; //rq009-17
                //Ajuste para que no muestre infrmacion mala 20160621
                if (this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[1].Text == "&nbsp;")
                    TxtUsuarioFinal.Text = lsTipoDoc;
                else
                    TxtUsuarioFinal.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[1].Text + "-" + lsTipoDoc + "-" + this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[3].Text;
                TxtCantidadUsu.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[6].Text;
                lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) - Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                dlTipoDemanda.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[15].Text; //rq009-17
                dlTipoDemanda_SelectedIndexChanged(null, null);
                ddlSector.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[12].Text; //rq009-17
                ddlMercadoRel.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[16].Text; //rq009-17
                ddlPuntoSalida.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[13].Text; //rq009-17
                btnCrearUsu.Visible = false;
                btnActualUsu.Visible = true;
                /////////////////////////////////////////////////////////////////////////
                /// Habilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///  
                /////////////////////////////////////////////////////////////////////////
                if (hdfDestinoRueda.Value == "G" && hdfTipoMerc.Value == "P")
                {
                    TxtEquivaleKpcd.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[7].Text;
                    TdIndica.Visible = true;
                    //TdIndica1.Visible = true;
                }
                else
                {
                    TdIndica.Visible = false;
                    //TdIndica1.Visible = false;
                }
                /////////////////////////////////////////////////////////////////////////
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al Recuperar el Registro.! " + ex.Message.ToString();
            }

        }
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            string[] lsNombreParametros = { "@P_codigo_datos_usuarios", "@P_codigo_cont_datos", "@P_no_identificacion_usr", "@P_codigo_tipo_doc",
                                        "@P_nombre_usuario", "@P_codigo_sector_consumo", "@P_codigo_punto_salida","@P_cantidad_contratada","@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                                        SqlDbType.Int,SqlDbType.Int,SqlDbType.Int};
            string[] lValorParametros = { hdfCodDatUsu.Value, "0", "", "", "", "0", "0", " 0", "3" };

            try
            {
                ////////////////////////////////////////////////////////////////////////////////////////
                /// OJO Cambios de los Indices por adicion de Campo Req. 009-17 Indicadores 20170321 ///
                ////////////////////////////////////////////////////////////////////////////////////////
                hdfCodDatUsu.Value = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text;
                lValorParametros[0] = hdfCodDatUsu.Value;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetUsuFinalCont", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Eliminación de la Información del Usuario.!" + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) - Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                    lConexion.Cerrar();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Eliminado Correctamente.!" + "');", true);
                    CargarDatosUsu();
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al Eliminar el Registro.! " + ex.Message.ToString();

            }
        }

    }
    /// <summary>
    /// Nombre: CargarDatosUsu
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid de Usuarios.
    /// Modificacion:
    /// </summary>
    private void CargarDatosUsu()
    {
        lblMensaje.Text = "";
        string[] lsNombreParametros = { "@P_codigo_cont_datos", "@P_no_identificacion_usr", "@P_login" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
        string[] lValorParametros = { "-1", "", "" };
        try
        {
            if (hdfCodVerifCont.Value.Trim().Length > 0)
                lValorParametros[0] = hdfCodVerifCont.Value;
            if (Session["tipoPerfil"].ToString() == "N")
                lValorParametros[2] = goInfo.Usuario.ToString();
            lConexion.Abrir();
            dtgUsuarios.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRegistroContratosUsu", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgUsuarios.DataBind();
            lConexion.Cerrar();
            lblTotlCantidad.Text = "0";
            if (dtgUsuarios.Items.Count > 0)
            {
                foreach (DataGridItem Grilla in this.dtgUsuarios.Items)
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(Grilla.Cells[6].Text.Trim())).ToString();
                }
                /////////////////////////////////////////////////////////////////////////
                /// Habilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///
                /////////////////////////////////////////////////////////////////////////
                if (hdfDestinoRueda.Value == "G" && hdfTipoMerc.Value == "P")
                    dtgUsuarios.Columns[7].Visible = true;
                else
                    dtgUsuarios.Columns[7].Visible = false;
                /////////////////////////////////////////////////////////////////////////
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// Realiza la actualizacion del estado y envia la Alerta para solicitar correccion de los datos del Registro.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSolCorr_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_estado", "@P_observacion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char, SqlDbType.VarChar };
        String[] lValorParametros = { hdfIdVerif.Value, "E", TxtObservacion.Text };

        try
        {
            lConexion.Abrir();
            if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetActualVerifDatosReg", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
            {
                lblMensaje.Text = "Se presentó un Problema en la Actualización de la Información del Usuario.!" + goInfo.mensaje_error.ToString();
                lConexion.Cerrar();
            }
            else
            {
                tblDemanda.Visible = false;
                tblVerifica.Visible = false;
                tblGrilla.Visible = true;
                tblDatos.Visible = true;
                tblVerifica.Visible = false;
                string lsAsunto = "";
                string lsMensaje = "";
                string lsMensajeC = "";
                string lsMailC = "";
                string lsNomOperadorC = "";
                string lsMailV = "";
                string lsNomOperadorV = "";
                /// Obtengo el mail del Operador Compra
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraC.Value + " ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsMailC = lLector["e_mail"].ToString() + ";" + lLector["e_mail2"].ToString() + ";" + lLector["e_mail3"].ToString();  //20180228 rq032-17
                    if (lLector["tipo_persona"].ToString() == "J")
                        lsNomOperadorC = lLector["razon_social"].ToString();
                    else
                        lsNomOperadorC = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                }
                lLector.Close();
                lLector.Dispose();
                /// Obtengo el mail del Operador Venta
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraV.Value + " ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsMailV = lLector["e_mail"].ToString() + ";" + lLector["e_mail2"].ToString() + ";" + lLector["e_mail2"].ToString(); // 20180228 rq032-17
                    if (lLector["tipo_persona"].ToString() == "J")
                        lsNomOperadorV = lLector["razon_social"].ToString();
                    else
                        lsNomOperadorV = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                ///// Envio del Mail de la Solicitud de la Correcion
                lsAsunto = "Notificación Solicitud Corrección Registro y Verificación";
                lsMensaje += "Nos permitimos  informarle que el Administrador del SEGAS acaba de Solicitar la Corrección de la Información transaccional de la Operación No: " + lblOperacion.Text + ". Por favor revisar los siguientes campos: <br><br>";
                if (lblDat1Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit1.Text + "<br>";
                if (lblDat2Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit2.Text + "<br>";
                if (lblDat3Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit3.Text + "<br>";
                if (lblDat4Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit4.Text + "<br>";
                if (lblDat6Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit5.Text + "<br>";
                if (lblDat6Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit6.Text + "<br>";
                if (lblDat7Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit7.Text + "<br>";
                if (lblDat8Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit8.Text + "<br>";
                if (hdfDestinoRueda.Value == "T" && hdfTipoMerc.Value == "P")
                {
                    if (lblDat9Ver.Text != "Correcta")
                        lsMensaje += " - " + lblTit9.Text + "<br>";
                    if (lblDat10Ver.Text != "Correcta")
                        lsMensaje += " - " + lblTit10.Text + "<br>";
                }
                lsMensaje += "<br><br>Observaciones: " + TxtObservacion.Text + "<br><br>";
                lsMensaje += "Cordialmente, <br><br><br>";
                lsMensaje += "Administrador SEGAS <br>";
                lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                clEmail mailC = new clEmail(lsMailC, lsAsunto, lsMensaje, "");
                lblMensaje.Text = mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                ////
                lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensaje, "");
                lblMensaje.Text = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");

                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Actualizado Correctamente.!" + "');", true);
                CargarDatos();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas en la Actualización de la Información. " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRegCont_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_estado", "@P_observacion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char, SqlDbType.VarChar };
        String[] lValorParametros = { hdfIdVerif.Value, "R", TxtObservacion.Text };

        try
        {
            lConexion.Abrir();
            if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetActualVerifDatosReg", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
            {
                lblMensaje.Text = "Se presentó un Problema en la Actualización de la Información del Usuario.!" + goInfo.mensaje_error.ToString();
                lConexion.Cerrar();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Actualizado Correctamente.!" + "');", true);
                tblDemanda.Visible = false;
                tblVerifica.Visible = false;
                tblGrilla.Visible = true;
                tblDatos.Visible = true;

                string lsAsunto = "";
                string lsMensaje = "";
                string lsMensajeC = "";
                string lsMailC = "";
                string lsNomOperadorC = "";
                string lsMailV = "";
                string lsNomOperadorV = "";
                /// Obtengo el mail del Operador Compra
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraC.Value + " ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsMailC = lLector["e_mail"].ToString() + ";" + lLector["e_mail2"].ToString() + ";" + lLector["e_mail2"].ToString(); // 20180228 rq032-17
                    if (lLector["tipo_persona"].ToString() == "J")
                        lsNomOperadorC = lLector["razon_social"].ToString();
                    else
                        lsNomOperadorC = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                }
                lLector.Close();
                lLector.Dispose();
                /// Obtengo el mail del Operador Venta
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraV.Value + " ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsMailV = lLector["e_mail"].ToString() + ";" + lLector["e_mail2"].ToString() + ";" + lLector["e_mail2"].ToString(); // 20180228 rq032-17
                    if (lLector["tipo_persona"].ToString() == "J")
                        lsNomOperadorV = lLector["razon_social"].ToString();
                    else
                        lsNomOperadorV = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                ///// Envio del Mail de la Solicitud de la Correcion
                lsAsunto = "Notificación Registro Contrato";
                lsMensaje += "Nos permitimos informarle que el Administrador de la plataforma SEGAS acaba de realizar exitosamente el Registro del Contrato de la Operación No: " + lblOperacion.Text + " con No. de Registro: " + hdfIdVerif.Value + ".<br><br>";
                lsMensaje += "<br><br>Observaciones: " + TxtObservacion.Text + "<br><br>";
                lsMensaje += "Cordialmente, <br><br><br>";
                lsMensaje += "Administrador SEGAS <br>";
                lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                clEmail mailC = new clEmail(lsMailC, lsAsunto, lsMensaje, "");
                lblMensaje.Text = mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                ////
                lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensaje, "");
                lblMensaje.Text = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                CargarDatos();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas en la Actualización de la Información. " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImmSalirVer_Click(object sender, ImageClickEventArgs e)
    {
        tblDemanda.Visible = false;
        tblGrilla.Visible = true;
        tblDatos.Visible = true;
        CargarDatos();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dlTipoDemanda_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            hdfTipoDemanda.Value = dlTipoDemanda.SelectedValue;
            if (hdfTipoDemanda.Value == "1")
            {
                TxtUsuarioFinal.Enabled = false;
                ddlMercadoRel.Enabled = true; //20160303
                lblSector.Text = "Sector de Consumo Usuario Regulado";
                if (hdfDestinoRueda.Value == "G")
                    lblCantContra.Text = "Cantidad a entregar";
                else
                    lblCantContra.Text = "Capacidad Contratada";
            }
            else
            {
                TxtUsuarioFinal.Enabled = true;
                ddlMercadoRel.Enabled = false; //20160303
                ddlMercadoRel.SelectedValue = "0"; //20160303
                if (hdfDestinoRueda.Value == "G")
                {
                    lblCantContra.Text = "Cantidad Contratada por usuario (MBTUD)";
                    lblSector.Text = "Sector Consumo Usuario No Regulado";
                }
                else
                {
                    lblSector.Text = "Sector de Consumo Transporte Usuario No Regulado";
                    lblCantContra.Text = "Capacidad Contratada por usuario (KPCD)";
                }
            }
            lConexion.Abrir();
            ddlSector.Items.Clear();
            ddlPuntoSalida.Items.Clear();
            LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo sec, m_demanda_sector dem", " sec.estado ='A' and sec.codigo_sector_consumo = dem.codigo_sector_consumo  and dem.estado ='A' and dem.codigo_tipo_demanda =" + dlTipoDemanda.SelectedValue + "  and dem.ind_uso ='T' order by descripcion", 0, 1); //20160706
            LlenarControles(lConexion.gObjConexion, ddlPuntoSalida, "m_punto_salida_snt", " estado = 'A'  order by descripcion", 0, 2);
            lConexion.Cerrar();
        }
        catch (Exception)
        {

        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCrearUsu_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_datos_usuarios", "@P_codigo_cont_datos", "@P_no_identificacion_usr", "@P_codigo_tipo_doc",
                                        "@P_nombre_usuario", "@P_codigo_sector_consumo", "@P_codigo_punto_salida","@P_cantidad_contratada","@P_accion","@P_destino_rueda", "@P_tipo_demanda" ,
                                        "@P_codigo_mercado_relevante",  // Se agrrega campo para mercado relevante-sector consumo 20160126
                                        "@P_equivalente_kpcd", // Campo nuevo Req. 009-17 Indicasdores 20170321
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                                        SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int,
                                        SqlDbType.Int, // Campo nuevo Req. 009-17 Indicasdores 20170321
                                      };
        string[] lValorParametros = { "0", "0", "", "", "", "0", "0", " 0", "1", hdfDestinoRueda.Value, dlTipoDemanda.SelectedValue, ddlMercadoRel.SelectedValue,
                                        "0",  // Campo nuevo Req. 009-17 Indicasdores 20170321
                                    };

        lblMensaje.Text = "";
        string[] lsUsuario;
        string lsNombre = "";
        string lsTipoDoc = "";
        int liValor = 0;
        try
        {
            lConexion.Abrir();
            lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
            if (dlTipoDemanda.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar Tipo Demanda Atender. <br> ";
            else
            {
                if (dlTipoDemanda.SelectedValue == "2")
                {
                    if (TxtUsuarioFinal.Text.Trim().Length <= 0)
                        lblMensaje.Text = "Debe Ingresar el Usuario Final <br>";
                    else
                    {
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                        if (!lLector.HasRows)
                            lblMensaje.Text += "El Usuario Ingresado NO existe en la Base de Datos.! <br>";
                        else
                        {
                            lLector.Read();
                            lsNombre = lLector["nombre"].ToString();
                            lsTipoDoc = lLector["codigo_tipo_doc"].ToString();
                        }
                        lLector.Close();
                        lLector.Dispose();
                    }
                }
                else //20160303
                {
                    if (ddlMercadoRel.SelectedValue == "0")
                        lblMensaje.Text += "Debe Seleccionar el mercado relevante. <br> ";
                }
            }
            if (ddlSector.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar " + lblSector.Text + ". <br> ";
            if (ddlPuntoSalida.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar  Punto de Salida en SNT. <br> ";
            if (TxtCantidadUsu.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar " + lblCantContra.Text + ". <br> ";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCantidadUsu.Text.Trim());
                    if (liValor <= 0)
                        lblMensaje.Text += lblCantContra.Text + " debe ser mayor que cero. <br> ";
                    else
                    {
                        if (liValor > Convert.ToDecimal(TxtCantidad.Text.Trim()))
                            lblMensaje.Text += "La " + lblCantContra.Text + " No puede ser Mayor que " + lblCantidad.Text + ". <br> ";
                        else
                        {
                            if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(TxtCantidad.Text))
                                lblMensaje.Text += "El acumulado de Usuarios No puede ser Mayor que " + lblCantidad.Text + ". <br> "; //20180126 rq107-16
                        }
                    }
                }
                catch (Exception)
                {
                    lblMensaje.Text += "Valor Inválido " + lblCantContra.Text + ". <br> ";
                }
            }
            /////////////////////////////////////////////////////////////////////////
            /// Habilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///
            /////////////////////////////////////////////////////////////////////////
            if (hdfDestinoRueda.Value == "G" && hdfTipoMerc.Value == "P")
            {
                if (TxtEquivaleKpcd.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Equivalente Kpcd. <br> ";
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtEquivaleKpcd.Text.Trim());
                        if (liValor < 0)
                            lblMensaje.Text += "Valor Inválido Equivalente Kpcd. <br> ";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Valor Inválido " + lblCantContra.Text + ". <br> ";
                    }
                }
            }
            //20170814 rq036-17
            //20170814 rq036-17
            //20170814 rq036-17
            //20170814 rq036-17
            //20170814 rq036-17

            if (lblMensaje.Text == "")
            {
                if (hdfCodVerifCont.Value != "")
                    lValorParametros[1] = hdfCodVerifCont.Value;
                if (hdfTipoDemanda.Value == "2")
                {
                    lValorParametros[2] = lsUsuario[0].Trim();
                    lValorParametros[3] = lsTipoDoc;
                    lValorParametros[4] = lsNombre;
                }
                lValorParametros[5] = ddlSector.SelectedValue;
                lValorParametros[6] = ddlPuntoSalida.SelectedValue;
                lValorParametros[7] = TxtCantidadUsu.Text.Trim();
                /////////////////////////////////////////////////////////////////////////
                /// Habilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///
                /////////////////////////////////////////////////////////////////////////
                if (hdfDestinoRueda.Value == "G" && hdfTipoMerc.Value == "P")
                    lValorParametros[12] = TxtEquivaleKpcd.Text.Trim();
                /////////////////////////////////////////////////////////////////////////
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetUsuFinalCont", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación de la Información del Usuario.!" + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(TxtCantidadUsu.Text.Trim())).ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Información de Usuarios Finales Ingresada Correctamente.!" + "');", true);
                    TxtUsuarioFinal.Text = "";
                    ddlSector.SelectedValue = "0";
                    ddlPuntoSalida.SelectedValue = "0";
                    ddlMercadoRel.SelectedValue = "0";
                    TxtCantidadUsu.Text = "0";
                    TxtEquivaleKpcd.Text = "0"; // Campos nuevos Req. 009-17 Indicadores 20170321
                    CargarDatosUsu();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnActualUsu_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_datos_usuarios", "@P_codigo_cont_datos", "@P_no_identificacion_usr", "@P_codigo_tipo_doc",
                                        "@P_nombre_usuario", "@P_codigo_sector_consumo", "@P_codigo_punto_salida","@P_cantidad_contratada","@P_accion","@P_destino_rueda", "@P_tipo_demanda" ,//20171130 rq026-17
                                        "@P_codigo_mercado_relevante", // Se agrrega campo para mercado relevante-sector consumo 20160126
                                        "@P_equivalente_kpcd" // Campo nuevo Req. 009-17 Indicasdores 20170321
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                                        SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int,
                                        SqlDbType.Int // Campo nuevo Req. 009-17 Indicasdores 20170321
                                      };
        string[] lValorParametros = { hdfCodDatUsu.Value, "0", "", "", "", "0", "0", " 0", "2", hdfDestinoRueda.Value, dlTipoDemanda.SelectedValue, ddlMercadoRel.SelectedValue ,
                                        "0" // Campo nuevo Req. 009-17 Indicasdores 20170321
                                    };
        lblMensaje.Text = "";
        string[] lsUsuario;
        int liValor = 0;
        string lsNombre = "";
        string lsTipoDoc = "";
        try
        {
            lConexion.Abrir();
            lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
            if (dlTipoDemanda.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar  Tipo Demanda Atender. <br> ";
            else
            {
                if (hdfTipoDemanda.Value == "2")
                {
                    if (TxtUsuarioFinal.Text.Trim().Length <= 0)
                        lblMensaje.Text = "Debe Ingresar el Usuario Final <br>";
                    else
                    {
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                        if (!lLector.HasRows)
                            lblMensaje.Text += "El Usuario Ingresado NO existe en la Base de Datos.! <br>";
                        else
                        {
                            lLector.Read();
                            lsNombre = lLector["nombre"].ToString();
                            lsTipoDoc = lLector["codigo_tipo_doc"].ToString();
                        }
                        lLector.Close();
                        lLector.Dispose();
                    }
                }
                else //20160303
                {
                    if (ddlMercadoRel.SelectedValue == "0")
                        lblMensaje.Text += "Debe Seleccionar el mercado relevante. <br> ";
                }

            }
            if (ddlSector.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar " + lblSector.Text + ". <br> ";
            if (ddlPuntoSalida.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar  Punto de Salida en SNT. <br> ";
            if (TxtCantidadUsu.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar " + lblCantContra.Text + ". <br> ";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCantidadUsu.Text.Trim());
                    if (liValor <= 0)
                        lblMensaje.Text += lblCantContra.Text + " debe ser mayor que cero. <br> ";
                    else
                    {
                        if (liValor > Convert.ToDecimal(TxtCantidad.Text.Trim()))
                            lblMensaje.Text += "La " + lblCantContra.Text + " No puede ser Mayor que " + lblCantidad.Text + ". <br> ";
                        else
                        {
                            if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(TxtCantidad.Text))
                                lblMensaje.Text += "El Acumulado de Usuarios No puede ser Mayor que " + lblCantidad.Text + ". <br> "; //20180126 rq107-16
                        }
                    }
                }
                catch (Exception)
                {
                    lblMensaje.Text += "Valor Inválido " + lblCantContra.Text + ". <br> ";
                }
            }
            /////////////////////////////////////////////////////////////////////////
            /// Habilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///
            /////////////////////////////////////////////////////////////////////////
            if (hdfDestinoRueda.Value == "G" && hdfTipoMerc.Value == "P")
            {
                if (TxtEquivaleKpcd.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Equivalente Kpcd. <br> ";
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtEquivaleKpcd.Text.Trim());
                        if (liValor < 0)
                            lblMensaje.Text += "Valor Inválido Equivalente Kpcd. <br> ";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Valor Inválido " + lblCantContra.Text + ". <br> ";
                    }
                }
            }
            //20170814 rq036-17
            //20170814 rq036-17
            //20170814 rq036-17
            //20170814 rq036-17
            //20170814 rq036-17

            if (lblMensaje.Text == "")
            {
                if (hdfCodVerifCont.Value.Trim().Length > 0)
                    lValorParametros[1] = hdfCodVerifCont.Value;
                if (hdfTipoDemanda.Value == "2")
                {
                    lValorParametros[2] = lsUsuario[0].Trim();
                    lValorParametros[3] = lsTipoDoc;
                    lValorParametros[4] = lsNombre;
                }
                lValorParametros[5] = ddlSector.SelectedValue;
                lValorParametros[6] = ddlPuntoSalida.SelectedValue;
                lValorParametros[7] = TxtCantidadUsu.Text.Trim();
                /////////////////////////////////////////////////////////////////////////
                /// Habilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///
                /////////////////////////////////////////////////////////////////////////
                if (hdfDestinoRueda.Value == "G" && hdfTipoMerc.Value == "P")
                    lValorParametros[12] = TxtEquivaleKpcd.Text.Trim();
                /////////////////////////////////////////////////////////////////////////
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetUsuFinalCont", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización de la Información del Usuario.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Información de Usuarios Finales Actualizada Correctamente.!" + "');", true);
                    TxtUsuarioFinal.Text = "";
                    ddlSector.SelectedValue = "0";
                    ddlPuntoSalida.SelectedValue = "0";
                    ddlMercadoRel.SelectedValue = "0";
                    TxtCantidadUsu.Text = "0";
                    TxtEquivaleKpcd.Text = "0"; // Campos nuevos Req. 009-17 Indicadores 20170321
                    btnCrearUsu.Visible = true;
                    btnActualUsu.Visible = false;
                    CargarDatosUsu();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlPuntoEntr_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (hdfPunta.Value == "C")
        {
            lConexion.Abrir();
            //ddlMercadoRelev.Items.Clear();
            ddlPuntoSalida.Items.Clear();
            //LlenarControles(lConexion.gObjConexion, ddlMercadoRelev, "m_mercado_relevante", " estado = 'A' And destino_rueda = '" + hdfDestinoRueda.Value + "' order by descripcion", 0, 2);
            //if (hdfDestinoRueda.Value == "G")
            //    LlenarControles(lConexion.gObjConexion, ddlPuntoSalida, "m_pozo", " estado = 'A'  order by descripcion", 0, 1);
            //else
            LlenarControles(lConexion.gObjConexion, ddlPuntoSalida, "m_punto_salida_snt", " estado = 'A' order by descripcion", 0, 2);
            lConexion.Cerrar();
        }
    }
    protected void btnCrear_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_ind_contrato", "@P_numero_contrato", "@P_contrato_definitivo", "@P_punta_contrato",
                                        "@P_fecha_suscripcion_cont", "@P_nombre_operador", "@P_codigo_tipo_doc", "@P_no_identificacion_operador", "@P_codigo_modalidad",
                                        "@P_desc_otro_moda", "@P_codigo_punto_entrega", "@P_desc_otro_punto", "@P_cantidad", "@P_precio", "@P_fecha_inicial",
                                        "@P_fecha_final", "@P_ruta_contrato", "@P_codigo_tipo_demanda", "@P_sentido_flujo", "@P_presion_punto_fin", "@P_codigo_motivo_modificacion",
                                        "@P_codigo_cesionario","@P_estado_act","@P_codigo_tipo_subasta","@P_destino_rueda","@P_accion" ,
                                        "@P_codigo_fuente", // campo o fuente 20160601
                                        "@P_conectado_snt", "@P_ent_boca_pozo", "@P_codigo_centro_pob", //20170814 rq036-17
                                        "@P_codigo_periodo", "@P_ind_contrato_var"};  //20171130 rq026-17
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int,SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int, SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, // Cambio del tipo de datos de la presion del punto 20151006
                                        SqlDbType.Int,SqlDbType.VarChar,SqlDbType.Int,
                                        SqlDbType.Int, // campo o fuente 20160601
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,//20170814 rq036-17
                                        SqlDbType.Int,SqlDbType.VarChar};//20171130 rq026-17
        string[] lValorParametros = { "0", "S", "0", "0", "", "", "", "", "", "0", "", "0", "", "0", "0", "", "", "", "0", "", "0", "0", "0", "V", "0", "0", "1", "0" , // campo o fuente 20160601
                                        "S","N","0",//20170814 rq036-17
                                        "0",""}; //20171130 rq026-17 
        lblMensaje.Text = "";
        string lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "-" + DateTime.Now.ToShortDateString().Substring(3, 2) + "-" + DateTime.Now.ToShortDateString().Substring(0, 2);
        string sRuta = sRutaArc + lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\";

        string lsMensajeMail = ""; // Campo nuevo requerimiento algoritmo y verificacion 20150529
        string lsEnviaMail = ""; // Campo nuevo requerimiento algoritmo y verificacion 20150529


        try
        {
            lblMensaje.Text = validaciones();
            if (FuCopiaCont.FileName != "")
            {

                try
                {
                    System.IO.Directory.CreateDirectory(sRuta);
                    FuCopiaCont.SaveAs(sRuta + FuCopiaCont.FileName);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Problemas en la Carga del Archivo. " + ex.Message.ToString();
                    lConexion.Cerrar();
                }

            }
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = hdfIdVerif.Value;
                lValorParametros[2] = lblOperacion.Text.Trim();
                lValorParametros[3] = TxtNumContrato.Text.Trim().ToUpper();
                lValorParametros[4] = hdfPunta.Value;
                lValorParametros[5] = TxtFechaSus.Text.Trim();
                lValorParametros[6] = lblNomOperador.Text.Trim();
                if (lblTipoDocOperador.Text.Trim() == "Nit")
                    lValorParametros[7] = "1";
                else
                    lValorParametros[7] = "2";
                lValorParametros[8] = lblNoDocumentoOperaqdor.Text.Trim();
                lValorParametros[9] = ddlModalidad.SelectedValue;
                lValorParametros[10] = TxtOtrModalidad.Text.Trim().ToUpper();
                lValorParametros[11] = ddlPuntoEntr.SelectedValue;
                lValorParametros[12] = TxtOtrPuntoE.Text.Trim().ToUpper();
                lValorParametros[13] = TxtCantidad.Text.Trim();
                lValorParametros[14] = TxtPrecio.Text.Trim();
                lValorParametros[15] = TxtFechaInicial.Text.Trim() + " " + TxtHoraInicial.Text;
                lValorParametros[16] = TxtFechaFinal.Text.Trim() + " " + TxtHoraFinal.Text;
                if (FuCopiaCont.FileName != "")
                    lValorParametros[17] = lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\" + FuCopiaCont.FileName;
                else
                    lValorParametros[17] = "";
                lValorParametros[18] = "0"; // Ajuste requerimiento ajuste registro de contratos 148 - 20150915
                if (hdfDestinoRueda.Value == "T" && hdfTipoMerc.Value == "P")
                {
                    lValorParametros[19] = ddlSentidoFlujo.SelectedValue;
                    lValorParametros[20] = TxtPresion.Text.Trim();
                }
                lValorParametros[21] = "0"; // Motivo de Modificacion
                lValorParametros[22] = "0"; // Codigo Cesionario
                lValorParametros[23] = hdfEstadoAct.Value;
                lValorParametros[24] = hdfCodTipoSub.Value;
                lValorParametros[25] = hdfDestinoRueda.Value;
                lValorParametros[27] = ddlFuente.SelectedValue; // campo o fuente MP 20160601
                //hdfTipoDemanda.Value = dlTipoDemanda.SelectedValue;
                //20170814 rq036-17
                if (trSum.Visible)
                {
                    lValorParametros[28] = ddlConecSnt.SelectedValue;
                    lValorParametros[29] = ddlEntBocPozo.SelectedValue;
                    lValorParametros[30] = ddlCentro.SelectedValue;
                }
                lValorParametros[31] = ddlPeriodo.SelectedValue; //20171130 rq026-17
                lValorParametros[32] = ddlIndVar.SelectedValue; //20171130 rq026-17
                SqlTransaction oTransaccion;
                lConexion.Abrir();
                oTransaccion = lConexion.gObjConexion.BeginTransaction(System.Data.IsolationLevel.Serializable);
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetRegistroContrato", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación del Registro del Contrato.! " + goInfo.mensaje_error.ToString();
                    oTransaccion.Rollback();
                    lConexion.Cerrar();
                }
                else
                {
                    string lsOperacion = "";
                    string lsIds = "";
                    while (lLector.Read())
                    {
                        hdfCodVerifCont.Value = lLector["codigo_cont_dato"].ToString();
                        lsMensajeMail = lLector["diferencia"].ToString(); // Campo nuevo requerimiento algoritmo y verificacion 20150529
                        lsEnviaMail = lLector["enviaMail"].ToString(); // Campo nuevo requerimiento algoritmo y verificacion 20150529
                        if (lsOperacion == "")
                        {
                            lsOperacion = lLector["operacion"].ToString();
                            lsIds = lLector["codigo_verif"].ToString();
                        }
                        else
                        {
                            lsOperacion += ", " + lLector["operacion"].ToString();
                            lsIds += ", " + lLector["codigo_verif"].ToString();
                        }
                    }
                    lLector.Close();
                    lLector.Dispose();
                    oTransaccion.Commit();

                    ///// Envio de la alerta al ingresar la informacion por ambas puntas 20150529
                    ///// Requerimiento Algoritmo y Verificacion
                    string lsAsunto = "";
                    string lsMensaje = "";
                    string lsMensajeC = "";
                    string lsMailC = "";
                    string lsNomOperadorC = "";
                    string lsMailV = "";
                    string lsNomOperadorV = "";
                    /// Obtengo el mail del Operador Compra
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraC.Value + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsMailC = lLector["e_mail"].ToString() + ";" + lLector["e_mail2"].ToString() + ";" + lLector["e_mail2"].ToString(); // 20180228 rq032-17
                        if (lLector["tipo_persona"].ToString() == "J")
                            lsNomOperadorC = lLector["razon_social"].ToString();
                        else
                            lsNomOperadorC = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                    }
                    lLector.Close();
                    lLector.Dispose();
                    /// Obtengo el mail del Operador Venta
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraV.Value + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsMailV = lLector["e_mail"].ToString() + ";" + lLector["e_mail2"].ToString() + ";" + lLector["e_mail2"].ToString(); // 20180228 rq032-17
                        if (lLector["tipo_persona"].ToString() == "J")
                            lsNomOperadorV = lLector["razon_social"].ToString();
                        else
                            lsNomOperadorV = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Envio de la alerta del ingreso de la información
                    lsAsunto = "Notificación Ingreso Información Contrato";
                    if (hdfPunta.Value == "C")
                        lsMensaje = "Nos permitimos informarle que el operador " + lsNomOperadorC + " acaba de Ingresar satisfactoriamente la información de la Operación No: " + lblOperacion.Text + ".<br><br>";
                    else
                        lsMensaje = "Nos permitimos  informarle que el operador " + lsNomOperadorV + " acaba de Ingresar satisfactoriamente la información de la Operación No: " + lblOperacion.Text + ".<br><br>";
                    lsMensaje += "\n\nObservaciones: Por favor proceda a ingresar la información transaccional e informar a su contraparte para culminar el proceso de registro. <br><br>";
                    lsMensaje += "Cordialmente, <br><br><br>";
                    lsMensaje += "Administrador SEGAS <br>";

                    lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                    clEmail mailC = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                    lblMensaje.Text = mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                    ////
                    lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                    clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                    lblMensaje.Text = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                    if (lsEnviaMail == "S")
                    {
                        ///// Envio del Mail de la Solicitud de la Correcion o Registro del Contrato
                        if (lsMensajeMail != "")
                        {
                            lsAsunto = "Notificación Solicitud Corrección Registro y Verificación";
                            lsMensaje = "Nos permitimos informarle que el Administrador de SEGAS acaba de Solicitar la corrección de la información transaccional de la Operación No: " + lblOperacion.Text + ". Por favor revisar los siguientes campos: <br><br> " + lsMensajeMail + "<br><br>";
                            lsMensaje += "Observaciones: Por favor validar los campos objeto de corrección con su contraparte. <br><br>";
                        }
                        else
                        {
                            if (ddlIndVar.SelectedValue == "N")
                            {
                                lsAsunto = "Notificación Registro Contrato";
                                lsMensaje = "Nos permitimos informarle que el Administrador de la plataforma SEGAS acaba de realizar exitosamente el Registro del Contrato de la Operación No: " + lblOperacion.Text + " con No. de Registro: " + hdfIdVerif.Value + ".<br><br>";
                            }
                            else
                            {
                                if (lsIds != hdfIdVerif.Value)
                                {
                                    lsAsunto = "Notificación Registro Contrato";
                                    lsMensaje = "Nos permitimos informarle que el Administrador de la plataforma SEGAS acaba de realizar exitosamente el Registro del Contrato de la Operación No: " + lblOperacion.Text + " con No. de Registro: " + hdfIdVerif.Value + ".<br><br>";
                                    lsMensaje += "Debido a que es un contrato variable quedaron registrados los siguientes Ids de registro: " + lsIds + "<br><br>";
                                }
                                else
                                {
                                    lsAsunto = "Notificación Registro Contrato";
                                    lsMensaje = "Nos permitimos informarle que el Administrador de la plataforma SEGAS acaba de realizar exitosamente el Registro del Contrato de la Operación No: " + lblOperacion.Text + " con No. de Registro: " + hdfIdVerif.Value + ".<br><br>";
                                    lsMensaje += "Debido a que es un contrato variable sólo quedará en firme cuando se registren todos los contrato asociadosa este.<br><br>";
                                }
                            }
                        }


                        lsMensaje += "Cordialmente, <br><br>";
                        lsMensaje += "Administrador SEGAS <br>";
                        lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                        clEmail mailC1 = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                        lblMensaje.Text = mailC1.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                        ////
                        lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                        clEmail mailV1 = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                        lblMensaje.Text = mailV1.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                    }
                    lConexion.Cerrar();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Ingresado Correctamente.!" + "');", true);
                    tblRegCnt.Visible = false;
                    tblGrilla.Visible = true;
                    tblDatos.Visible = true;
                    tblDemanda.Visible = false;
                    LimpiarCampos();
                    CargarDatos();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    protected void btnActualizar_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_ind_contrato", "@P_numero_contrato", "@P_contrato_definitivo", "@P_punta_contrato",
                                        "@P_fecha_suscripcion_cont", "@P_nombre_operador", "@P_codigo_tipo_doc", "@P_no_identificacion_operador", "@P_codigo_modalidad",
                                        "@P_desc_otro_moda", "@P_codigo_punto_entrega", "@P_desc_otro_punto", "@P_cantidad", "@P_precio", "@P_fecha_inicial",
                                        "@P_fecha_final", "@P_ruta_contrato", "@P_codigo_tipo_demanda", "@P_sentido_flujo", "@P_presion_punto_fin", "@P_codigo_motivo_modificacion",
                                        "@P_codigo_cesionario","@P_estado_act","@P_codigo_tipo_subasta","@P_destino_rueda","@P_accion" ,
                                        "@P_codigo_fuente", //campo o fuente MP 20160601
                                        "@P_conectado_snt", "@P_ent_boca_pozo", "@P_codigo_centro_pob", //20170814 rq036-17
                                        "@P_codigo_periodo", "@P_ind_contrato_var"};  //20171130 rq026-17
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int,SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int, SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, // Cambio del tipo de datos de la presion del punto 20151006
                                        SqlDbType.Int,SqlDbType.VarChar,SqlDbType.Int,
                                        SqlDbType.Int, // campo o fuente MP 20160601
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,//20170814 rq036-17
                                        SqlDbType.Int,SqlDbType.VarChar};//20171130 rq026-17
        string[] lValorParametros = { "0", "S", "0", "0", "", "", "", "", "", "0", "", "0", "", "0", "0", "", "", "", "0", "", "0", "0", "0", "V", "0", "0", "2", "0", //campo o fuente MP 20160601
                                        "S","N","0",//20170814 rq036-17
                                        "0","" };//20171130 rq026-17
        lblMensaje.Text = "";
        string lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "-" + DateTime.Now.ToShortDateString().Substring(3, 2) + "-" + DateTime.Now.ToShortDateString().Substring(0, 2);
        string sRuta = sRutaArc + lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\";

        string lsMensajeMail = ""; // Campo nuevo requerimiento algoritmo y verificacion 20150529
        string lsEnviaMail = ""; // Campo nuevo requerimiento algoritmo y verificacion 20150529

        try
        {
            lblMensaje.Text = validaciones();
            if (FuCopiaCont.FileName != "")
            {
                try
                {
                    System.IO.Directory.CreateDirectory(sRuta);
                    FuCopiaCont.SaveAs(sRuta + FuCopiaCont.FileName);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Problemas en la Carga del Archivo. " + ex.Message.ToString();
                    lConexion.Cerrar();
                }
            }
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = hdfIdVerif.Value;
                lValorParametros[2] = lblOperacion.Text.Trim();
                lValorParametros[3] = TxtNumContrato.Text.Trim().ToUpper();
                lValorParametros[4] = hdfPunta.Value;
                lValorParametros[5] = TxtFechaSus.Text.Trim();
                lValorParametros[6] = lblNomOperador.Text.Trim();
                if (lblTipoDocOperador.Text.Trim() == "Nit")
                    lValorParametros[7] = "1";
                else
                    lValorParametros[7] = "2";
                lValorParametros[8] = lblNoDocumentoOperaqdor.Text.Trim();
                lValorParametros[9] = ddlModalidad.SelectedValue;
                lValorParametros[10] = TxtOtrModalidad.Text.Trim().ToUpper();
                lValorParametros[11] = ddlPuntoEntr.SelectedValue;
                lValorParametros[12] = TxtOtrPuntoE.Text.Trim().ToUpper();
                lValorParametros[13] = TxtCantidad.Text.Trim();
                lValorParametros[14] = TxtPrecio.Text.Trim();
                lValorParametros[15] = TxtFechaInicial.Text.Trim() + " " + TxtHoraInicial.Text;
                lValorParametros[16] = TxtFechaFinal.Text.Trim() + " " + TxtHoraFinal.Text;
                if (FuCopiaCont.FileName != "")
                    lValorParametros[17] = lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\" + FuCopiaCont.FileName;
                else
                    lValorParametros[17] = hdfRutaArchivo.Value;
                lValorParametros[18] = "0"; // Ajuste requerimiento ajuste registro de contratos 148 - 20150915
                if (hdfDestinoRueda.Value == "T" && hdfTipoMerc.Value == "P")
                {
                    lValorParametros[19] = ddlSentidoFlujo.SelectedValue;
                    lValorParametros[20] = TxtPresion.Text.Trim();
                }
                lValorParametros[21] = ddlMotivoModifi.SelectedValue; // Motivo de Modificacion
                lValorParametros[22] = ddlCesionario.SelectedValue; // Codigo Cesionario
                lValorParametros[23] = hdfEstadoAct.Value;
                lValorParametros[24] = hdfCodTipoSub.Value;
                lValorParametros[25] = hdfDestinoRueda.Value;
                lValorParametros[27] = ddlFuente.SelectedValue; //campo o fuente MP 20160601
                //hdfTipoDemanda.Value = dlTipoDemanda.SelectedValue;
                //20170814 rq036-17
                if (trSum.Visible)
                {
                    lValorParametros[28] = ddlConecSnt.SelectedValue;
                    lValorParametros[29] = ddlEntBocPozo.SelectedValue;
                    lValorParametros[30] = ddlCentro.SelectedValue;
                }
                lValorParametros[31] = ddlPeriodo.SelectedValue;//20171130 rq026-17
                lValorParametros[32] = ddlIndVar.SelectedValue;//20171130 rq026-17
                SqlTransaction oTransaccion;
                lConexion.Abrir();
                oTransaccion = lConexion.gObjConexion.BeginTransaction(System.Data.IsolationLevel.Serializable);
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetRegistroContrato", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación del Registro del Contrato.! " + goInfo.mensaje_error.ToString(); //20180126 rq107-16
                    oTransaccion.Rollback();
                    lConexion.Cerrar();
                }
                else
                {
                    string lsOperacion = "";
                    string lsIds = "";

                    while (lLector.Read())
                    {
                        hdfCodVerifCont.Value = lLector["codigo_cont_dato"].ToString();
                        lsMensajeMail = lLector["diferencia"].ToString(); // Campo nuevo requerimiento algoritmo y verificacion 20150529
                        lsEnviaMail = lLector["enviaMail"].ToString(); // Campo nuevo requerimiento algoritmo y verificacion 20150529
                        if (lsOperacion == "")
                        {
                            lsOperacion = lLector["operacion"].ToString();
                            lsIds = lLector["codigo_verif"].ToString();
                        }
                        else
                        {
                            lsOperacion += ", " + lLector["operacion"].ToString();
                            lsIds += ", " + lLector["codigo_verif"].ToString();
                        }
                    }

                    lLector.Close();
                    lLector.Dispose();
                    oTransaccion.Commit();
                    ///// Envio de la alerta al ingresar la informacion por ambas puntas 20150529
                    ///// Requerimiento Algoritmo y Verificacion
                    string lsAsunto = "";
                    string lsMensaje = "";
                    string lsMensajeC = "";
                    string lsMailC = "";
                    string lsNomOperadorC = "";
                    string lsMailV = "";
                    string lsNomOperadorV = "";
                    /// Obtengo el mail del Operador Compra
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraC.Value + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsMailC = lLector["e_mail"].ToString() + ";" + lLector["e_mail2"].ToString() + ";" + lLector["e_mail2"].ToString(); // 20180228 rq032-17
                        if (lLector["tipo_persona"].ToString() == "J")
                            lsNomOperadorC = lLector["razon_social"].ToString();
                        else
                            lsNomOperadorC = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                    }
                    lLector.Close();
                    lLector.Dispose();
                    /// Obtengo el mail del Operador Venta
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraV.Value + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsMailV = lLector["e_mail"].ToString() + ";" + lLector["e_mail2"].ToString() + ";" + lLector["e_mail2"].ToString(); // 20180228 rq032-17
                        if (lLector["tipo_persona"].ToString() == "J")
                            lsNomOperadorV = lLector["razon_social"].ToString();
                        else
                            lsNomOperadorV = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Envio de la alerta del ingreso de la información
                    lsAsunto = "Notificación Actualización Información Contrato";
                    if (hdfPunta.Value == "C")
                        lsMensaje = "Nos permitimos  informarle que el operador " + lsNomOperadorC + " acaba de Actualizar la información transaccional del Registro del Contrato de la Operación No: " + lblOperacion.Text + ".<br><br>";
                    else
                        lsMensaje = "Nos permitimos  informarle que el operador " + lsNomOperadorV + " acaba de Actualizar la información transaccional del Registro del Contrato de la Operación No: " + lblOperacion.Text + ".<br><br>";
                    lsMensaje += "Observaciones: Por favor validar la información en el Módulo Registro de Contratos. <br><br>";
                    lsMensaje += "Cordialmente, <br><br><br>";
                    lsMensaje += "Administrador SEGAS <br>";
                    lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                    clEmail mailC = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                    lblMensaje.Text = mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                    ////
                    lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                    clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                    lblMensaje.Text = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                    if (lsEnviaMail == "S")
                    {
                        ///// Envio del Mail de la Solicitud de la Correcion o Registro del Contrato
                        if (lsMensajeMail != "")
                        {
                            lsAsunto = "Notificación Solicitud Corrección Registro y Verificación";
                            lsMensaje = "Nos permitimos  informarle que el Administrador del SEGAS acaba de Solicitar la Correción de la Información transaccional de la Operación No: " + lblOperacion.Text + ". Por favor revisar los siguientes campos: <br><br> " + lsMensajeMail + "<br><br>";
                            lsMensaje += "Observaciones: Por favor validar los campos objeto de corrección con su contraparte. <br><br>";
                        }
                        else
                        {
                            if (ddlIndVar.SelectedValue == "N")
                            {
                                lsAsunto = "Notificación Registro Contrato";
                                lsMensaje = "Nos permitimos informarle que el Administrador de la plataforma SEGAS acaba de realizar exitosamente el Registro del Contrato de la Operación No: " + lblOperacion.Text + " con No. de Registro: " + hdfIdVerif.Value + ".<br><br>";
                            }
                            else
                            {
                                if (lsIds != hdfIdVerif.Value)
                                {
                                    lsAsunto = "Notificación Registro Contrato";
                                    lsMensaje = "Nos permitimos informarle que el Administrador de la plataforma SEGAS acaba de realizar exitosamente el Registro del Contrato de la Operación No: " + lblOperacion.Text + " con No. de Registro: " + hdfIdVerif.Value + ".<br><br>";
                                    lsMensaje += "Debido a que es un contrato variable quedaron registrados los siguientes Ids de registro: " + lsIds + "<br><br>";
                                }
                                else
                                {
                                    lsAsunto = "Notificación Registro Contrato";
                                    lsMensaje = "Nos permitimos informarle que el Administrador de la plataforma SEGAS acaba de realizar exitosamente el Registro del Contrato de la Operación No: " + lblOperacion.Text + " con No. de Registro: " + hdfIdVerif.Value + ".<br><br>";
                                    lsMensaje += "Debido a que es un contrato variable sólo quedará en firme cuando se registren todos los contratos asociados a este.<br><br>";
                                }
                            }
                        }
                        lsMensaje += "Cordialmente, <br><br><br>";
                        lsMensaje += "Administrador SEGAS <br>";
                        lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                        clEmail mailC1 = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                        lblMensaje.Text = mailC1.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                        ////
                        lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                        clEmail mailV1 = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                        lblMensaje.Text = mailV1.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                    }
                    else
                    {
                        lConexion.Cerrar();
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Ingresado Correctamente.!" + "');", true);
                    tblRegCnt.Visible = false;
                    tblGrilla.Visible = true;
                    tblDatos.Visible = true;
                    tblDemanda.Visible = false;
                    LimpiarCampos();
                    CargarDatos();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    protected void btnRegresar_Click(object sender, EventArgs e)
    {
        tblGrilla.Visible = true;
        tblRegCnt.Visible = false;
        tblDatos.Visible = true;
        tblDemanda.Visible = false;
        TxtNumContrato.Text = "";
        TxtFechaSus.Text = "";
        ddlModalidad.SelectedValue = "0";
        ddlPuntoEntr.SelectedValue = "0";
        TxtCantidad.Text = "";
        TxtPrecio.Text = "";
        TxtFechaInicial.Text = "";
        TxtFechaFinal.Text = "";
        //dlTipoDemanda.SelectedValue = "0";
        ddlSentidoFlujo.SelectedValue = "";
        TxtPresion.Text = "";
        ddlFuente.SelectedValue = "0"; //campo o fuente MP 20160601
        lblMensaje.Text = "";
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20170814 rq036-17
    protected void ddlConecSnt_SelectedIndexChanged(object sender, EventArgs e)
    {
        string lsPunto = ddlPuntoEntr.SelectedValue;//20171114 rq036-17
        if (ddlConecSnt.SelectedValue == "S")
        {
            lConexion.Abrir();
            ddlPuntoEntr.Items.Clear();
            LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_pozo", " ind_estandar='S' and  estado = 'A'  order by descripcion", 0, 1);
            lConexion.Cerrar();
            //20171114 rq036-17
            try
            {
                ddlPuntoEntr.SelectedValue = lsPunto;
            }
            catch (Exception)
            {
                ddlPuntoEntr.SelectedValue = "0";
            }
            //20171114 rq036-17
            if (ddlPuntoEntr.SelectedValue == "0")
                ddlPuntoEntr.Enabled = true;
            lblCentro.Visible = false;
            ddlCentro.Visible = false;
            ddlCentro.SelectedValue = "0";
            lblPuntoE.Visible = true;
            ddlPuntoEntr.Visible = true;
        }
        else
        {
            if (ddlEntBocPozo.SelectedValue == "S")
            {
                lConexion.Abrir();
                ddlPuntoEntr.Items.Clear();
                LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_pozo", " codigo_tipo_campo = 6 and  estado = 'A'  order by descripcion", 0, 1);
                lConexion.Cerrar();
                //20171114 rq036-17
                try
                {
                    ddlPuntoEntr.SelectedValue = lsPunto;
                }
                catch (Exception)
                {
                    ddlPuntoEntr.SelectedValue = "0";
                }
                //20171114 rq036-17
                if (ddlPuntoEntr.SelectedValue == "0")
                    ddlPuntoEntr.Enabled = true;

                lblCentro.Visible = false;
                ddlCentro.Visible = false;
                //ddlCentro.SelectedValue = "0"; //20171114 rq036-17
                lblPuntoE.Visible = true;
                ddlPuntoEntr.Visible = true;
            }
            else
            {
                lblCentro.Visible = true;
                ddlCentro.Visible = true;
                ddlCentro.SelectedValue = "0";
                lblPuntoE.Visible = false;
                ddlPuntoEntr.Visible = false;
                ddlPuntoEntr.SelectedValue = "0";
                //20171114 rq036-17
                if (ddlCentro.SelectedValue == "0")
                    ddlCentro.Enabled = true;
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20170814 rq036-17
    protected void ddlEntBocPozo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string lsPunto = ddlPuntoEntr.SelectedValue;//20171114 rq036-17
        if (ddlConecSnt.SelectedValue == "S")
        {
            lConexion.Abrir();
            ddlPuntoEntr.Items.Clear();
            LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_pozo", " ind_estandar='S' and  estado = 'A'  order by descripcion", 0, 1);
            lConexion.Cerrar();
            //20171114 rq036-17
            try
            {
                ddlPuntoEntr.SelectedValue = lsPunto;
            }
            catch (Exception)
            {
                ddlPuntoEntr.SelectedValue = "0";
            }
            //20171114 rq036-17
            if (ddlPuntoEntr.SelectedValue == "0")
                ddlPuntoEntr.Enabled = true;

            lblCentro.Visible = false;
            ddlCentro.Visible = false;
            ddlCentro.SelectedValue = "0";
            lblPuntoE.Visible = true;
            ddlPuntoEntr.Visible = true;
        }
        else
        {
            if (ddlEntBocPozo.SelectedValue == "S")
            {
                lConexion.Abrir();
                ddlPuntoEntr.Items.Clear();
                LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_pozo", " codigo_tipo_campo = 6 and  estado = 'A'  order by descripcion", 0, 1);
                lConexion.Cerrar();
                //20171114 rq036-17
                try
                {
                    ddlPuntoEntr.SelectedValue = lsPunto;
                }
                catch (Exception)
                {
                    ddlPuntoEntr.SelectedValue = "0";
                }
                //20171114 rq036-17
                if (ddlPuntoEntr.SelectedValue == "0")
                    ddlPuntoEntr.Enabled = true;

                lblCentro.Visible = false;
                ddlCentro.Visible = false;
                ddlCentro.SelectedValue = "0";
                lblPuntoE.Visible = true;
                ddlPuntoEntr.Visible = true;
            }
            else
            {
                lblCentro.Visible = true;
                ddlCentro.Visible = true;
                //ddlCentro.SelectedValue = "0"; //20171114 rq036-17
                lblPuntoE.Visible = false;
                ddlPuntoEntr.Visible = false;
                ddlPuntoEntr.SelectedValue = "0";
                //20171114 rq036-17
                if (ddlCentro.SelectedValue == "0")
                    ddlCentro.Enabled = true;
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20170814 rq036-17
    protected void ddlPeriodo_SelectedIndexChanged(object sender, EventArgs e)
    {
        lConexion.Abrir();
        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_periodos_entrega", " codigo_periodo= " + ddlPeriodo.SelectedValue);
        if (lLector.HasRows)
        {
            lLector.Read();
            if (lLector["medida_tiempo"].ToString() != "I")
            {
                TxtHoraInicial.Visible = false;
                TxtHoraFinal.Visible = false;
                TxtHoraInicial.Text = "00:00";
                TxtHoraFinal.Text = "00:00";
            }
            else
            {
                TxtHoraInicial.Visible = true;
                TxtHoraFinal.Visible = true;
            }
        }
        lLector.Close();
        lLector.Dispose();
        lConexion.Cerrar();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        this.dtgConsulta.CurrentPageIndex = 0;//20190130 ajuste
        CargarDatos();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lkbExcel_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        DateTime ldFecha;
        int liValor = 0;
        string[] lsNombreParametros = { "@P_tipo_subasta", "@P_numero_contrato", "@P_fecha_contrato", "@P_mercado", "@P_codigo_producto",
                                        "@P_operador_compra", "@P_operador_venta","@P_tipo_perfil","@P_codigo_operador", "@P_estado",
                                        "@P_fecha_contrato_fin", "@P_numero_contrato_fin", //20161207 rq102 conformacion de rutas
                                        "@P_codigo_causa", "@P_codigo_verif"}; //20171130 rq026-17
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int,
                                        SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar ,
                                        SqlDbType.VarChar, SqlDbType.Int,  //20161207 rq102 conformacion de rutas
                                        SqlDbType.VarChar,SqlDbType.Int};//20171130 rq026-17
        string[] lValorParametros = { "0", "0", "", "", "0", "0", "0", Session["tipoPerfil"].ToString(), goInfo.cod_comisionista, "0", "", "0", "0", "0" }; //20161207 rq102 conformacion de rutas //20171130 rq026-17

        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial de Negociación. <br>"; //20161207 rq102 conformacion de rutas
            }
        }
        //20161207 rq102 conformacion de rutas
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaFin.Text);
            }
            catch (Exception)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Final de Negociación. <br>";
            }
        }
        //20161205 rq102 conformacion de rutas
        if (TxtFechaIni.Text.Trim().Length == 0 && TxtFechaFin.Text.Trim().Length > 0)
            lblMensaje.Text += "Debe digitar la Fecha inicial de Negociación. <br>";
        //20161207 rq102 conformacion de rutas
        if (TxtFechaIni.Text.Trim().Length > 0 && TxtFechaFin.Text.Trim().Length > 0)
            try
            {
                if (Convert.ToDateTime(TxtFechaFin.Text) < Convert.ToDateTime(TxtFechaIni.Text))
                    lblMensaje.Text += "La Fecha inicial de Negociación debe ser menor o igual que la fecha final. <br>";
            }
            catch (Exception)
            { }

        if (TxtNoContrato.Text.Trim().Length > 0)
        {
            try
            {
                liValor = Convert.ToInt32(TxtNoContrato.Text);
            }
            catch (Exception)
            {
                lblMensaje.Text += "Formato Inválido en el Campo No Operación. <br>";  //20161205 rq102 conformacion de rutas
            }

        }
        // 20161207 rq102 conformación de rutas
        if (TxtNoContratoFin.Text.Trim().Length > 0)
        {
            try
            {
                liValor = Convert.ToInt32(TxtNoContratoFin.Text);
            }
            catch (Exception)
            {
                lblMensaje.Text += "Formato Inválido en el Campo No Operación final. <br>";
            }

        }
        // 20161207 rq102 conformación de rutas
        if (TxtNoContrato.Text.Trim().Length == 0 && TxtNoContratoFin.Text.Trim().Length > 0)
        {
            lblMensaje.Text += "Debe digitar el No de operación inicial antes que el final. <br>";
        }
        // 20161207 rq102 conformación de rutas
        if (TxtNoContrato.Text.Trim().Length > 0 && TxtNoContratoFin.Text.Trim().Length > 0)
        {
            try
            {
                if (Convert.ToInt32(TxtNoContratoFin.Text) < Convert.ToInt32(TxtNoContrato.Text))
                    lblMensaje.Text += "El No Operación inicial debe ser menor o igual que el final. <br>";
            }
            catch (Exception)
            {

            }
        }

        if (lblMensaje.Text == "")
        {
            try
            {
                if (TxtNoContrato.Text.Trim().Length > 0)
                    lValorParametros[1] = TxtNoContrato.Text.Trim();
                if (ddlSubasta.SelectedValue != "0")
                    lValorParametros[0] = ddlSubasta.SelectedValue;
                if (TxtFechaIni.Text.Trim().Length > 0)
                    lValorParametros[2] = TxtFechaIni.Text.Trim();
                if (ddlMercado.SelectedValue != "")
                    lValorParametros[3] = ddlMercado.SelectedValue;
                if (ddlProducto.SelectedValue != "0")
                    lValorParametros[4] = ddlProducto.SelectedValue;
                if (ddlComprador.SelectedValue != "0")
                    lValorParametros[5] = ddlComprador.SelectedValue;
                if (ddlVendedor.SelectedValue != "0")
                    lValorParametros[6] = ddlVendedor.SelectedValue;
                if (ddlEstado.SelectedValue != "0")
                    lValorParametros[9] = ddlEstado.SelectedValue;
                //20161207 rq 102 conformacion de rutas
                if (TxtFechaFin.Text.Trim().Length > 0)
                    lValorParametros[10] = TxtFechaFin.Text.Trim();
                else
                    lValorParametros[10] = lValorParametros[2];
                //20161207 rq 102 conformacion de rutas
                if (TxtNoContratoFin.Text.Trim().Length > 0)
                    lValorParametros[11] = TxtNoContratoFin.Text.Trim();
                else
                    lValorParametros[11] = lValorParametros[1];

                lValorParametros[12] = ddlCausa.SelectedValue;
                //20171130 rq026-17
                if (TxtNoId.Text.Trim().Length > 0)
                    lValorParametros[13] = TxtNoId.Text.Trim();

                lConexion.Abrir();
                dtgConsultaExcel.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRegistroContratos", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsultaExcel.DataBind();
                lConexion.Cerrar();

                string lsNombreArchivo = Session["login"] + "InfExcelReg" + DateTime.Now + ".xls";
                StringBuilder lsb = new StringBuilder();
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                dtgConsultaExcel.Visible = true;
                lpagina.EnableEventValidation = false;
                lpagina.Controls.Add(lform);
                dtgConsultaExcel.EnableViewState = false;
                lform.Controls.Add(dtgConsultaExcel);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = System.Text.Encoding.Default;

                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + "Consulta Registro de Contratos" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write(lsb.ToString());
                dtgConsultaExcel.Visible = false;
                Response.End();
                Response.Flush();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al Consultar los Contratos. " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSalir_Click(object sender, EventArgs e)
    {
        tblDemanda.Visible = false;
        tblGrilla.Visible = true;
        tblDatos.Visible = true;
        CargarDatos();
    }
}