﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ActFuenteContReg.aspx.cs"
    Inherits="Verificacion_frm_ActFuenteContReg" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
            width="100%">
            <tr>
                <td align="center" class="th1" style="width: 80%;">
                    <asp:Label ID="lblTitulo" runat="server" ForeColor ="White" ></asp:Label>
                </td>
            </tr>
        </table>
        <br /><br /><br /><br /><br /><br />
        <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
            width="90%">
            <tr>
                <td class="td1">
                    Fecha Negociación
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtFechaNeg" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                </td>
                <td class="td1">
                    Número Id Registro
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtNoIdRegistro" runat="server" ValidationGroup="detalle" Width="120px"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="fteTxtNoIdRegistro" runat="server" Enabled="True"
                        FilterType="Custom, Numbers" TargetControlID="TxtNoIdRegistro">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td class="td1">
                    Número Operación
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtNoContrato" runat="server" ValidationGroup="detalle" Width="120px"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="fteTxtNoContrato" runat="server" Enabled="True"
                        FilterType="Custom, Numbers" TargetControlID="TxtNoContrato">
                    </cc1:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td class="th1" colspan="6" align="center">
                    <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                        OnClick="imbConsultar_Click" Height="40" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
                </td>
            </tr>
        </table>
    </div>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla" visible="false">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 1150px; height: 450px;">
                    <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                        AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1"
                        OnEditCommand="dtgConsulta_EditCommand">
                        <Columns>
                            <%--0--%>
                            <asp:BoundColumn DataField="numero_operacion" HeaderText="No. Operacion" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--1--%>
                            <asp:BoundColumn DataField="numero_id_registro" HeaderText="No. Registro"></asp:BoundColumn>
                            <%--2--%>
                            <asp:BoundColumn DataField="fecha_negociacion" HeaderText="fecha Negociacion"></asp:BoundColumn>
                            <%--3--%><asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--4--%>
                            <asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--5--%>
                            <asp:BoundColumn DataField="Punto_entrega" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--6--%>
                            <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente" ItemStyle-HorizontalAlign="Right">
                            </asp:BoundColumn>
                            <%--7--%>
                            <asp:BoundColumn DataField="fuente" HeaderText="fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--8--%>
                            <asp:EditCommandColumn HeaderText="Actualizar" EditText="Actualizar"></asp:EditCommandColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
    <table id="tblCambio" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
        width="80%" visible="false">
        <tr>
            <td class="td1">
                Número Operación
            </td>
            <td class="td2">
                <asp:Label ID="lblOperacion" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
            </td>
            <td class="td1">
                Id registro
            </td>
            <td class="td2">
                <asp:Label ID="lblId" runat="server" ForeColor="Red" Font-Bold="true" Width="200px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Fuente
            </td>
            <td class="td2" colspan="3">
                <asp:DropDownList ID="ddlFuente" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="6" align="center">
                <asp:Button ID="btnActualizar" runat="server" Text="Actualizar" OnClick="btnActualizar_Click"
                    OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnRegresar" runat="server" Text="Regresar" OnClick="btnRegresar_Click" />
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="6" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="14px"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>