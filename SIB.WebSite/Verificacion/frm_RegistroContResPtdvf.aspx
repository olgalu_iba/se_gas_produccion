﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_RegistroContResPtdvf.aspx.cs" Inherits="Verificacion.frm_RegistroContResPtdvf" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>
            </div>
            <%--Contenido--%>
            <%--Consulta--%>
            <div class="kt-portlet__body" runat="server" id="tblDatos">
                <div class="row">
                    <%--20161207 rq102--%>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>ID Inicial</label>
                            <asp:TextBox ID="TxtNoContrato" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>ID Final</label>
                            <asp:TextBox ID="TxtNoContratoFin" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Operador</label>
                            <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fuente</label>
                            <asp:DropDownList ID="ddlFuente" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Estado</label>
                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="btnConsultar" runat="server" CssClass="btn btn-success" Text="Consultar" OnClick="btnConsultar_Click" ValidationGroup="detalle" />
                            <asp:LinkButton ID="lkbExcel" runat="server" CssClass="btn btn-outline-brand btn-square" OnClick="lkbExcel_Click">Exportar Excel</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <%--Mensaje--%>
            <div class="kt-portlet__body" runat="server" id="tblMensaje">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <%--Grilla--%>
            <div class="kt-portlet__body" runat="server" id="tblGrilla" visible="false">
                <div class="row">
                    <div class="table table-responsive">
                        <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                            Width="100%" CssClass="table-bordered"
                            OnEditCommand="dtgConsulta_EditCommand" AllowPaging="true" PageSize="20" OnPageIndexChanged="dtgConsulta_PageIndexChanged">
                            <Columns>
                                <%--0--%>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="ID" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--1--%>
                                <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <%--2--%>
                                <asp:BoundColumn DataField="operador_venta" HeaderText="Operador Venta" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--3--%>
                                <asp:BoundColumn DataField="nombre_venta" HeaderText="vendedor" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--4--%>
                                <asp:BoundColumn DataField="operador_compra" HeaderText="Operador Compra" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--5--%>
                                <asp:BoundColumn DataField="nombre_compra" HeaderText="comprador" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--6--%>
                                <asp:BoundColumn DataField="codigo_fuente" HeaderText="Codigo Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--7--%>
                                <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--8--%>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <%--9--%>
                                <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                                <%--10--%>
                                <asp:BoundColumn DataField="fecha_max_registro" HeaderText="Fecha Limite Registro"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--11--%>
                                <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--12--%>
                                <asp:BoundColumn DataField="estado" Visible="false"></asp:BoundColumn>
                                <%--13--%>
                                <asp:BoundColumn DataField="punta" Visible="false"></asp:BoundColumn>
                                <%--14--%>
                                <asp:BoundColumn DataField="cantidad" Visible="false"></asp:BoundColumn>
                                <%--15--%>
                                <asp:BoundColumn DataField="contrato_definitivo" Visible="false"></asp:BoundColumn>
                                <%--16--%>
                                <asp:BoundColumn DataField="fecha_suscripcion" Visible="false"></asp:BoundColumn>
                                <%--17--%>
                                <asp:BoundColumn DataField="ind_reg" Visible="false"></asp:BoundColumn>
                                <%--18--%>
                                <asp:BoundColumn DataField="codigo_verif" Visible="false"></asp:BoundColumn>
                                <%--19--%>
                                <asp:EditCommandColumn HeaderText="Accion" EditText="Seleccionar"></asp:EditCommandColumn>
                            </Columns>
                            <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                        <%--20190607 rq036-19--%>
                        <asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False" Visible="false"
                            Width="100%" CssClass="table-bordered">
                            <Columns>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="ID" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_venta" HeaderText="Operador Venta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_venta" HeaderText="vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_compra" HeaderText="Operador Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_compra" HeaderText="comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_fuente" HeaderText="Codigo Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_max_registro" HeaderText="Fecha Limite Registro"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                </div>
            </div>

            <%--Modificacion--%>
            <div class="kt-portlet__body" runat="server" id="tblRegCnt" visible="false">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>ID</label>
                            <asp:Label ID="lblcontrato" runat="server" CssClass="form-control"></asp:Label>-<asp:Label
                                ID="lblPunta" runat="server" CssClass="form-control"></asp:Label>
                            <asp:HiddenField ID="hdfPunta" runat="server" />
                            <asp:HiddenField ID="hdfCodigoVer" runat="server" />
                            <asp:HiddenField ID="hdfCodigoVerUsr" runat="server" />
                            <asp:HiddenField ID="hdfCantidad" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fecha Negociación</label>
                            <asp:Label ID="lblFechaNeg" runat="server" CssClass="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Comprador</label>
                            <asp:Label ID="lblComprador" runat="server" CssClass="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Vendedor</label>
                            <asp:Label ID="lblVendedor" runat="server" CssClass="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fuente</label>
                            <asp:Label ID="lblFuente" runat="server" CssClass="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Cantidad</label>
                            <asp:Label ID="lblCantidad" runat="server" CssClass="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Número Contrato</label>
                            <asp:TextBox ID="TxtNumContrato" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fecha Suscripción {YYYY/MM/DD}</label>
                            <asp:TextBox ID="TxtFechaSus" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="btnCrear" runat="server" CssClass="btn btn-success" Text="Registrar" OnClick="btnCrear_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="btnActualizar" runat="server" CssClass="btn btn-success" Text="Actualizar" OnClick="btnActualizar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="btnCrearUsu" runat="server" CssClass="btn btn-success" Text="Crear Usuario" OnClick="btnCrearUsu_Click" Visible="false" />
                            <asp:Button ID="btnActualUsu" runat="server" CssClass="btn btn-success" Text="Actualiza Usuario" OnClick="btnActualUsu_Click" Visible="false" />
                            <asp:Button ID="btnRegresar" runat="server" CssClass="btn btn-secondary" Text="Regresar" OnClick="btnRegresar_Click" Visible="false" />
                        </div>
                    </div>
                </div>
            </div>

            <%--Demanda--%>
            <div class="kt-portlet__body" runat="server" id="tblDemanda" visible="false">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Tipo Demanda a Atender</label>
                            <asp:DropDownList ID="dlTipoDemanda" runat="server" CssClass="form-control selectpicker" OnSelectedIndexChanged="dlTipoDemanda_SelectedIndexChanged"
                                AutoPostBack="true">
                                <asp:ListItem Value="1">Regulado</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblSector" runat="server" Text="Sector Consumo Usuario Regulado" CssClass="form-control"></asp:Label>
                            <asp:DropDownList ID="ddlSector" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" runat="server" visible="false">
                        <div class="form-group">
                            <label>Usuario Final</label>
                            <asp:TextBox ID="TxtUsuarioFinal" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:AutoCompleteExtender runat="server" BehaviorID="AutoCompleteExUsuF" ID="AutoCompleteExtender1"
                                TargetControlID="TxtUsuarioFinal" ServicePath="~/WebService/AutoComplete.asmx"
                                ServiceMethod="GetCompletionListUsuarioFinal" MinimumPrefixLength="3" CompletionInterval="1000"
                                EnableCaching="true" CompletionSetCount="20" CompletionListCssClass="autocomplete_completionListElement"
                                CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                DelimiterCharacters=";,:">
                                <Animations>
                        <OnShow>
                            <Sequence>
                                <OpacityAction Opacity="0" />
                                <HideAction Visible="true" />
                                <ScriptAction Script="
                                    // Cache the size and setup the initial size
                                    var behavior = $find('AutoCompleteExUsuF');
                                    if (!behavior._height) {
                                        var target = behavior.get_completionList();
                                        behavior._height = target.offsetHeight - 2;
                                        target.style.height = '0px';
                                    }" />
                                <Parallel Duration=".4">
                                    <FadeIn />
                                    <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteExUsuF')._height" />
                                </Parallel>
                            </Sequence>
                        </OnShow>
                        <OnHide>
                            <Parallel Duration=".4">
                                <FadeOut />
                                <Length PropertyKey="height" StartValueScript="$find('AutoCompleteExUsuF')._height" EndValue="0" />
                            </Parallel>
                        </OnHide>
                                </Animations>
                            </ajaxToolkit:AutoCompleteExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Punto de Salida en SNT</label>
                            <asp:DropDownList ID="ddlPuntoSalida" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                            <asp:HiddenField ID="hdfTipoDemanda" runat="server" />
                            <asp:HiddenField ID="hdfCodDatUsu" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Mercado Relevante</label>
                            <asp:DropDownList ID="ddlMercadoRel" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblCantContra" runat="server" Width="100px" Text="Cantidad a entregar" CssClass="form-control"></asp:Label>
                            <asp:TextBox ID="TxtCantidadUsu" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" runat="server" id="TdIndica">
                        <div class="form-group">
                            <label>Equivalente Kpcd</label>
                            <asp:TextBox ID="TxtEquivaleKpcd" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label><b>LISTADO DE USUARIOS FINALES</b></label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <b>TOTAL CANTIDAD:</b><asp:Label ID="lblTotlCantidad" runat="server" ForeColor="Red"
                                Font-Bold="true"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="table table-responsive">
                        <div style="overflow: scroll; height: 450px;">
                            <asp:DataGrid ID="dtgUsuarios" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                Width="100%" CssClass="table-bordered" OnEditCommand="dtgUsuarios_EditCommand">
                                <Columns>
                                    <%--0--%>
                                    <asp:BoundColumn DataField="tipo_demanda" HeaderText="Tipo Demanda Atender" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--1--%>
                                    <asp:BoundColumn DataField="no_identificacion_usr" HeaderText="Identificacion Usuario Final"
                                        ItemStyle-HorizontalAlign="Left" Visible="false"></asp:BoundColumn>
                                    <%--2--%>
                                    <asp:BoundColumn DataField="tipo_documento" HeaderText="Tipo Identificacion Usuario Final"
                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px" Visible="false"></asp:BoundColumn>
                                    <%--3--%>
                                    <asp:BoundColumn DataField="nombre_usuario" HeaderText="Nombre Usuario Final" ItemStyle-HorizontalAlign="Left" Visible="false"></asp:BoundColumn>
                                    <%--4--%>
                                    <asp:BoundColumn DataField="sector_consumo" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="80px"></asp:BoundColumn>
                                    <%--5--%>
                                    <asp:BoundColumn DataField="punto_salida" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--6--%>
                                    <asp:BoundColumn DataField="cantidad_contratada" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--7--%>
                                    <asp:BoundColumn DataField="equivalente_kpcd" HeaderText="Equivalente Kpcd" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--8--%>
                                    <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar"></asp:EditCommandColumn>
                                    <%--9--%>
                                    <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                                    <%--10--%>
                                    <asp:BoundColumn DataField="codigo_verif_usr" Visible="false"></asp:BoundColumn>
                                    <%--11--%>
                                    <asp:BoundColumn DataField="codigo_verif" Visible="false"></asp:BoundColumn>
                                    <%--12--%>
                                    <asp:BoundColumn DataField="codigo_sector_consumo" Visible="false"></asp:BoundColumn>
                                    <%--13--%>
                                    <asp:BoundColumn DataField="codigo_punto_salida" Visible="false"></asp:BoundColumn>
                                    <%--14--%>
                                    <asp:BoundColumn DataField="codigo_tipo_doc" Visible="false"></asp:BoundColumn>
                                    <%--15--%>
                                    <asp:BoundColumn DataField="codigo_tipo_demanda" Visible="false"></asp:BoundColumn>
                                    <%--17--%>
                                    <asp:BoundColumn DataField="codigo_mercado_relevante" Visible="false"></asp:BoundColumn>
                                    <%--18--%>
                                    <asp:BoundColumn DataField="codigo_verif_usr" Visible="false"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
