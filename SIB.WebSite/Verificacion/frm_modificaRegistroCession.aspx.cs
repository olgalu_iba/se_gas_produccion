﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Collections.Generic;

public partial class Verificacion_frm_modificaRegistroCession : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Modificación de Contratos por Cesión";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    string[] lsCarperta;
    string Carpeta;
    string sRutaArc = ConfigurationManager.AppSettings["rutaModif"].ToString();
    string lsCarpetaAnt = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        //Titulo
        Master.Titulo = "Modificación de Contratos por Cesión";

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles2(lConexion.gObjConexion, DdlBusVenta, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20190425 rq022-19
            LlenarControles2(lConexion.gObjConexion, DdlBusCompra, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20190425 rq022-19
            LlenarControles2(lConexion.gObjConexion, DdlBusContra, "m_operador ope", " estado = 'A' and codigo_operador !=0 and exists (select 1 from t_contrato_verificacion ver where (ope.codigo_operador= ver.operador_compra or ope.codigo_operador= ver.operador_venta) and (ver.operador_compra = " + goInfo.cod_comisionista + " or ver.operador_venta = " + goInfo.cod_comisionista + ")) and ope.codigo_operador <> " + goInfo.cod_comisionista + " order by razon_social", 0, 4); //20190425 rq022-19
            LlenarControles(lConexion.gObjConexion, DdlBusCausa, "m_causa_modificacion", "  estado ='A' and tipo_causa ='C' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlBusEstado, "m_estado_gas", "  tipo_estado ='M'  and sigla_estado not in ('R','L') order by descripcion_estado", 2, 3);
            LlenarControles(lConexion.gObjConexion, DdlBusModalidad, "m_modalidad_contractual", "  estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlBusCausa1, "m_causa_modificacion", "  estado ='A' and tipo_causa ='C' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlBusEstado1, "m_estado_gas", "  tipo_estado ='M'  and sigla_estado not in ('R','L')  order by descripcion_estado", 2, 3);
            LlenarControles(lConexion.gObjConexion, DdlIngCausa, "m_causa_modificacion", "  estado ='A' and tipo_causa ='C' order by descripcion", 0, 1);
            LlenarControles2(lConexion.gObjConexion, DdlVendedor, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20190425 rq022-19
            LlenarControles2(lConexion.gObjConexion, DdlComprador, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20190425 rq022-19
            LlenarControles(lConexion.gObjConexion, dlTipoDemanda, "m_tipo_demanda_atender", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo sec, m_demanda_sector dem", " sec.estado ='A' and sec.codigo_sector_consumo = dem.codigo_sector_consumo  and dem.estado ='A' and dem.codigo_tipo_demanda =1 and dem.ind_uso ='T' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMercadoRel, "m_mercado_relevante", " estado ='A' order by descripcion", 0, 1);
            lConexion.Cerrar();
            hdfOperador.Value = goInfo.cod_comisionista;

            if (goInfo.cod_comisionista == "0")
            {
                tr01.Visible = false;
                tr02.Visible = true;
                tr03.Visible = true;
            }
            else
            {
                tr01.Visible = true;
                tr02.Visible = false;
                tr03.Visible = false;
            }
        }
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: LlenarControles2
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles2(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        CargarDatos();
    }

    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        lblMensaje.Text = "";
        DateTime ldFecha;
        string[] lsNombreParametros = { "@P_numero_contrato", "@P_codigo_verif", "@P_contrato_definitivo", "@P_fecha_inicial", "@P_fecha_final",
                "@P_codigo_operador", "@P_codigo_contraparte", "@P_operador_compra", "@P_operador_venta","@P_tipo_mercado","@P_destino_rueda",
                "@P_codigo_modalidad","@P_codigo_causa","@P_estado_modif" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar , SqlDbType.VarChar,
                SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar};
        string[] lValorParametros = { "0", "0", "", "", "", "0", "0", "0", "0", "", "", "0", "0", "" };

        if (TxtBusFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtBusFechaIni.Text);
            }
            catch (Exception)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial de Negociación. <br>";
            }
        }
        if (TxtBusFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtBusFechaFin.Text);
            }
            catch (Exception)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Final de Negociación. <br>";
            }
        }
        if (TxtBusFechaIni.Text.Trim().Length == 0 && TxtBusFechaFin.Text.Trim().Length > 0)
            lblMensaje.Text += "Debe digitar la Fecha Inicial de Negociación antes que la final. <br>";

        if (TxtBusFechaIni.Text.Trim().Length > 0 && TxtBusFechaFin.Text.Trim().Length > 0)
            try
            {
                if (Convert.ToDateTime(TxtFechaFin.Text) < Convert.ToDateTime(TxtFechaIni.Text))
                    lblMensaje.Text += "La Fecha inicial de Negociación debe ser menor o igual que la fecha final. <br>";
            }
            catch (Exception)
            {
            }

        if (lblMensaje.Text == "")
        {
            try
            {
                if (TxtBusOperacion.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtBusOperacion.Text.Trim();
                if (TxtBusId.Text.Trim().Length > 0)
                    lValorParametros[1] = TxtBusId.Text.Trim();
                lValorParametros[2] = TxtBusContratoDef.Text.Trim();
                if (TxtBusFechaIni.Text.Trim().Length > 0)
                {
                    lValorParametros[3] = TxtBusFechaIni.Text.Trim();
                    if (TxtBusFechaFin.Text.Trim().Length > 0)
                        lValorParametros[4] = TxtBusFechaFin.Text.Trim();
                    else
                        lValorParametros[4] = TxtBusFechaIni.Text.Trim();
                }
                lValorParametros[5] = goInfo.cod_comisionista;
                lValorParametros[6] = DdlBusContra.SelectedValue;
                lValorParametros[7] = DdlBusCompra.SelectedValue;
                lValorParametros[8] = DdlBusVenta.SelectedValue;
                lValorParametros[9] = DdlBusMercado.SelectedValue;
                lValorParametros[10] = DdlBusProducto.SelectedValue;
                lValorParametros[11] = DdlBusModalidad.SelectedValue;
                if (goInfo.cod_comisionista == "0")
                {
                    lValorParametros[12] = DdlBusCausa1.SelectedValue;
                    lValorParametros[13] = DdlBusEstado1.SelectedValue;
                }
                else
                {
                    lValorParametros[12] = DdlBusCausa.SelectedValue;
                    lValorParametros[13] = DdlBusEstado.SelectedValue;
                }
                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContSolModCes", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                lConexion.Cerrar();
                if (dtgConsulta.Items.Count > 0)
                {
                    tblGrilla.Visible = true;
                    lkbExcel.Visible = true;
                }
                else
                {
                    tblGrilla.Visible = false;
                    lkbExcel.Visible = false;
                    lblMensaje.Text = "No se encontraron Registros.";
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo consultar la información.! " + ex.Message.ToString();
            }
        }
    }

    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        hdfNoSolicitud.Value = "0";
        divSolicitud.Visible = false;

        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            hdfIdRegistro.Value = e.Item.Cells[4].Text;
            hdfTipoCesion.Value = e.Item.Cells[33].Text;
            if (goInfo.cod_comisionista == "0")
            {
                if (e.Item.Cells[32].Text != "P")
                    lblMensaje.Text = "No está pendiente de aprobación de la BMC";
                if (e.Item.Cells[7].Text != "Original")
                    lblMensaje.Text = "la BMC sólo puede aprobar el contrato original";
            }
            else
            {
                if (e.Item.Cells[7].Text == "Original")
                {
                    if (e.Item.Cells[32].Text != "I" && e.Item.Cells[32].Text != "E" && e.Item.Cells[32].Text != "S" && e.Item.Cells[32].Text != "M" && e.Item.Cells[32].Text != "C")
                        lblMensaje.Text = "No se puede modificar porque ya se aprobó o se rechazó";
                    if (e.Item.Cells[2].Text != "0" && e.Item.Cells[25].Text != e.Item.Cells[29].Text)
                        lblMensaje.Text = "No puede aprobar el contrato original";
                    if (e.Item.Cells[37].Text == "S")
                        lblMensaje.Text = "Hay una solicitud de modificación en curso para el contrato<br>";
                }
                else
                {
                    if (e.Item.Cells[25].Text != e.Item.Cells[29].Text && e.Item.Cells[32].Text != "A" && e.Item.Cells[32].Text != "S")
                        lblMensaje.Text = "No se puede aprobar el registro porque no ha sido autorizado totalmente o ya está aprobado";
                    if (e.Item.Cells[25].Text == e.Item.Cells[29].Text && (e.Item.Cells[32].Text == "B" || e.Item.Cells[32].Text == "O"))
                        lblMensaje.Text = "No puede modificar porque ya se aprobó por la contraparte";
                }
            }
            if (e.Item.Cells[34].Text.Length == 1 && e.Item.Cells[34].Text != "C")
                lblMensaje.Text = "La operación tiene en proceso una modificación que no es por cesión ";
            if (manejo_bloqueo("V", hdfIdRegistro.Value))
                lblMensaje.Text = "Se está modificando la operación";
            if (lblMensaje.Text == "")
            {
                manejo_bloqueo("A", hdfIdRegistro.Value);
                DdlDestino.SelectedValue = e.Item.Cells[28].Text;
                DdlMercado.SelectedValue = e.Item.Cells[27].Text;
                hdfPunta.Value = e.Item.Cells[29].Text;
                hdfNoSolicitud.Value = e.Item.Cells[2].Text;
                hdfCausa.Value = e.Item.Cells[30].Text;
                hdfCodModif.Value = e.Item.Cells[31].Text;

                if (e.Item.Cells[35].Text == "N")
                {
                    //dtgUsuarios.Columns[8].Visible = false;
                    //dtgUsuarios.Columns[9].Visible = false;
                    btnCrearUsu.Visible = false;
                    tblDemanda.Visible = false;
                }
                else
                {
                    //dtgUsuarios.Columns[8].Visible = true;
                    //dtgUsuarios.Columns[9].Visible = true;
                    if (e.Item.Cells[27].Text != "O")
                    {
                        btnCrearUsu.Visible = true;
                        tblDemanda.Visible = true;
                        trae_usr_fin(hdfCodModif.Value, hdfIdRegistro.Value);
                    }
                    else
                    {
                        btnCrearUsu.Visible = false;
                        tblDemanda.Visible = false;
                    }
                }

                if (e.Item.Cells[26].Text == "C")
                {
                    color_label("N"); // 20190425 rq022-19 ajuste modificacion
                    divSolicitud.Visible = true;
                    //20190425 rq022-19 
                    if (tblDemanda.Visible)
                    { 
                        hdfMostrar.Value = "S";
                        tblDemanda.Visible = false;
                    }
                    else
                        hdfMostrar.Value = "N";
                }
                if (e.Item.Cells[26].Text == "M")
                {
                    color_label("N"); // 20190425 rq022-19 ajuste modificacion
                    modificar();
                }
                if (e.Item.Cells[26].Text == "A")
                {
                    color_label("R"); // 20190425 rq022-19 ajuste modificacion
                    aprobar();
                }
                if (e.Item.Cells[26].Text == "B")
                {
                    color_label("R"); // 20190425 rq022-19 ajuste modificacion
                    consultar("A");
                }
                //if (hdfTipoCesion.Value == "T" || e.Item.Cells[35].Text=="N")

                

            }
        }
        if (((LinkButton)e.CommandSource).Text == "Consultar")
        {
            hdfIdRegistro.Value = e.Item.Cells[4].Text;
            hdfCodModif.Value = e.Item.Cells[31].Text;
            color_label("R"); // 20190425 rq022-19 ajuste modificacion
            consultar("C");
        }
        if (((LinkButton)e.CommandSource).Text == "Original")
        {
            try
            {
                if (e.Item.Cells[23].Text.Trim() != "&nbsp;" && e.Item.Cells[23].Text.Trim() != "")
                {
                    lsCarperta = sRutaArc.Split('\\');
                    lsCarpetaAnt = lsCarperta[lsCarperta.Length - 2];
                    string lsRuta = "../" + lsCarpetaAnt + "/" + e.Item.Cells[23].Text.Replace(@"\", "/");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + lsRuta + "');", true);
                }
                else
                    lblMensaje.Text = "No hay archivo para visualizar.!";

            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al mostrar el archivo. " + ex.Message.ToString();
            }
        }
        if (((LinkButton)e.CommandSource).Text == "Modificado")
        {
            try
            {
                if (e.Item.Cells[24].Text.Trim() != "&nbsp;" && e.Item.Cells[24].Text.Trim() != "")
                {
                    lsCarperta = sRutaArc.Split('\\');
                    lsCarpetaAnt = lsCarperta[lsCarperta.Length - 2];
                    string lsRuta = "../" + lsCarpetaAnt + "/" + e.Item.Cells[24].Text.Replace(@"\", "/");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + lsRuta + "');", true);
                }
                else
                    lblMensaje.Text = "No hay archivo para visualizar.!";

            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al mostrar el archivo. " + ex.Message.ToString();
            }
        }
    }
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void crear()
    {
        btnModificar.Visible = true;
        btnGrabarDef.Visible = true;
        btnAprobar.Visible = false;
        btnAprobarBmc.Visible = false;
        btnRechazo.Visible = false;
        btnRechazoBMC.Visible = false;
        trObs.Visible = true;
        mostrar_datos("C");
    }/// Modificacion:
     /// </summary>
     /// <param name="source"></param>
     /// <param name="e"></param>
    protected void mostrar_datos(string lsAccion)
    {

        //DdlComprador.SelectedValue = "0";
        //DdlVendedor.SelectedValue = "0";
        //TxtFechaNeg.Text = "";
        //TxtHoraNeg.Text = "";
        //TxtFechaSus.Text = "";
        //TxtContDef.Text = "";
        //DdlVariable.SelectedValue = "N";
        //TxtFechaIni.Text = "";
        //TxtFechaFin.Text = "";
        //TxtCantidad.Text = "";
        //TxtPrecio.Text = "";
        //TxtCapacOtm.Text = "";

        //DdlComprador.Enabled = true;
        //DdlVendedor.Enabled = true;
        //TxtFechaNeg.Enabled = false;
        //TxtHoraNeg.Enabled = false;
        //TxtFechaSus.Enabled = false;
        //TxtContDef.Enabled = false;
        //DdlVariable.Enabled = false;
        //TxtFechaIni.Enabled = true;
        //TxtFechaFin.Enabled = true;
        //TxtCantidad.Enabled = true;
        //TxtPrecio.Enabled = false;
        //TxtCapacOtm.Enabled = true;

        try
        {
            tblDatos.Visible = false;
            tblGrilla.Visible = false;
            tblModifica.Visible = true;
            lkbExcel.Visible = false;

            //if (hdfTipoCesion.Value == "P" && hdfPunta.Value == "C" && DdlMercado.SelectedValue != "O")
            //{
            //    tblDemanda.Visible = true;
            //    btnCrearUsu.Visible = true;
            //}
            //else
            //{
            //    tblDemanda.Visible = false;
            //    btnCrearUsu.Visible = false;
            //}

            if (DdlDestino.SelectedValue == "A")
                TxtCapacOtm.Visible = true;

            if (DdlDestino.SelectedValue == "G")
                LblCantidad.Text = "Cantidad de energía contratada (MBTUD)";
            else
            {
                if (DdlDestino.SelectedValue == "T")
                    LblCantidad.Text = "Capacidad de transporte contratada (KPCD)";
                else
                    LblCantidad.Text = "Cantidad y Capacidad contratada";
            }

            //habilita los campos permitidos
            string[] lsNombreParametros = { "@P_codigo_campo", "@P_codigo_causa" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { "0", hdfCausa.Value };

            try
            {
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetCampoMod", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    DdlComprador.Enabled = true;
                    DdlVendedor.Enabled = true;
                    if (hdfTipoCesion.Value == "P")
                    {
                        TxtFechaIni.Enabled = true;
                        TxtFechaFin.Enabled = true;
                        TxtCantidad.Enabled = true;
                        TxtCapacOtm.Enabled = true;
                    }
                    else
                    {
                        TxtFechaIni.Enabled = false;
                        TxtFechaFin.Enabled = false;
                        TxtCantidad.Enabled = false;
                        TxtCapacOtm.Enabled = false;
                    }


                    if (lLector["operador_compra"].ToString() == "N")
                        DdlComprador.Enabled = false;
                    if (lLector["operador_venta"].ToString() == "N")
                        DdlVendedor.Enabled = false;
                    if (lLector["fecha_inicial"].ToString() == "N")
                        TxtFechaIni.Enabled = false;
                    if (lLector["fecha_final"].ToString() == "N")
                        TxtFechaFin.Enabled = false;
                    if (lLector["cantidad"].ToString() == "N")
                    {
                        TxtCantidad.Enabled = false;
                        TxtCapacOtm.Enabled = false;
                    }
                }
                else
                {
                    DdlComprador.Enabled = false;
                    DdlVendedor.Enabled = false;
                    TxtFechaIni.Enabled = false;
                    TxtFechaFin.Enabled = false;
                    TxtCantidad.Enabled = false;
                    TxtCapacOtm.Enabled = false;

                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al traer la información.! " + ex.Message.ToString();

            }

            //trae el detalle de contrados
            CargarDatosOper();
            revisa_usr_fin();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas en la Recuperación de la Información. " + ex.Message.ToString();
        }
    }
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void consulta_datos()
    {

        DdlComprador.SelectedValue = "0";
        DdlVendedor.SelectedValue = "0";
        TxtFechaNeg.Text = "";
        TxtHoraNeg.Text = "";
        TxtFechaSus.Text = "";
        TxtContDef.Text = "";
        DdlVariable.SelectedValue = "N";
        TxtFechaIni.Text = "";
        TxtFechaFin.Text = "";
        TxtCantidad.Text = "";
        TxtPrecio.Text = "";
        TxtCapacOtm.Text = "";

        try
        {
            tblDatos.Visible = false;
            tblGrilla.Visible = false;
            tblModifica.Visible = true;
            lkbExcel.Visible = false;

            //if (hdfPunta.Value == "C" && DdlMercado.SelectedValue != "O")
            //{
            //    tblDemanda.Visible = true;
            //    btnCrearUsu.Visible = true;
            //}
            //else
            //{
            //    tblDemanda.Visible = false;
            //    btnCrearUsu.Visible = false;
            //}
            if (DdlDestino.SelectedValue == "A")
                TxtCapacOtm.Visible = true;
            else
                TxtCapacOtm.Visible = false;
            if (DdlDestino.SelectedValue == "G")
                LblCantidad.Text = "Cantidad de energía contratada (MBTUD)";
            else
            {
                if (DdlDestino.SelectedValue == "T")
                    LblCantidad.Text = "Capacidad de transporte contratada (KPCD)";
                else
                    LblCantidad.Text = "Cantidad y Capacidad contratada";
            }

            DdlComprador.Enabled = false;
            DdlVendedor.Enabled = false;
            TxtFechaNeg.Enabled = false;
            TxtHoraNeg.Enabled = false;
            TxtFechaSus.Enabled = false;
            TxtContDef.Enabled = false;
            DdlVariable.Enabled = false;
            TxtFechaIni.Enabled = false;
            TxtFechaFin.Enabled = false;
            TxtCantidad.Enabled = false;
            TxtCapacOtm.Enabled = false;
            TxtPrecio.Enabled = false;

            CargarDatosOper();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas en la Recuperación de la Información. " + ex.Message.ToString();
        }
    }

    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void modificar()
    {
        btnModificar.Visible = true;
        btnGrabarDef.Visible = true;
        btnAprobar.Visible = false;
        btnAprobarBmc.Visible = false;
        btnRechazo.Visible = false;
        btnRechazoBMC.Visible = false;
        trObs.Visible = false;
        crear();
        if (tblDemanda.Visible)
            CargarDatosUsu();
    }
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void aprobar()
    {
        tblDatos.Visible = false;
        tblGrilla.Visible = false;
        tblModifica.Visible = true;

        btnModificar.Visible = false;
        btnAprobar.Visible = true;
        btnGrabarDef.Visible = false;
        btnAprobarBmc.Visible = false;
        btnRechazo.Visible = true;
        btnRechazoBMC.Visible = false;
        //btnCrearUsu.Visible = false;

        DdlComprador.Enabled = false;
        DdlVendedor.Enabled = false;
        TxtFechaIni.Enabled = false;
        TxtFechaFin.Enabled = false;
        TxtCantidad.Enabled = false;
        TxtCapacOtm.Enabled = false;
        trObs.Visible = true;
        //if (hdfTipoCesion.Value == "P" && hdfPunta.Value == "C" && DdlMercado.SelectedValue != "O")
        //{
        //    tblDemanda.Visible = true;
        //    btnCrearUsu.Visible = true;
        //}
        //else
        //{
        //    tblDemanda.Visible = false;
        //    btnCrearUsu.Visible = false;
        //}

        CargarDatosOper();
        if (tblDemanda.Visible)
            CargarDatosUsu();
    }
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void consultar(string lsAccion)
    {
        consulta_datos();
        if (lsAccion == "A")
        {
            btnModificar.Visible = false;
            btnAprobar.Visible = false;
            btnAprobarBmc.Visible = true;
            btnRechazo.Visible = false;
            btnRechazoBMC.Visible = true;
            btnGrabarDef.Visible = false;
            btnCrearUsu.Visible = false;
            trObs.Visible = true;
        }
        else
        {
            btnModificar.Visible = false;
            btnAprobar.Visible = false;
            btnAprobarBmc.Visible = false;
            btnGrabarDef.Visible = false;
            btnCrearUsu.Visible = false;
            btnRechazo.Visible = false;
            btnRechazoBMC.Visible = false;
            trObs.Visible = false;
            tblDemanda.Visible = false;
            btnCrearUsu.Visible = false;
        }
        if (tblDemanda.Visible)
            CargarDatosUsu();
    }
    /// <summary>
    /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnAceptar_Click(object sender, EventArgs e)
    {
        lblMensajeMod.Text = "";
        List<string> liExtension = new List<string>();
        liExtension.Add(".PDF");
        liExtension.Add(".PDFA");
        liExtension.Add(".GIF");
        liExtension.Add(".JPG");
        liExtension.Add(".PNG");
        liExtension.Add(".TIFF");
        string lsExtValidas = ".pdf, .pdfa, .gif, .jpg, .png, .tiff";
        string lsExtension;

        if (DdlIngCausa.SelectedValue == "0")
            lblMensajeMod.Text += "Debe seleccionar la causa de modificación<br>";
        if (FuArchivoOrg.FileName == "")
        {
            lblMensajeMod.Text += "Debe seleccionar el archivo del contrato original<br>";
        }
        else
        {
            lsExtension = Path.GetExtension(FuArchivoOrg.FileName).ToUpper();
            if (!liExtension.Contains(lsExtension))
                lblMensajeMod.Text += "La extensión del archivo original no es válida: " + lsExtValidas + "<br>";
            if (File.Exists(sRutaArc + FuArchivoOrg.FileName))
                lblMensajeMod.Text += "El archivo original ya está cargado en el sistema<br>";
        }
        if (FuArchivoMod.FileName == "")
        {
            lblMensajeMod.Text += "Debe seleccionar el archivo del contrato modificado<br>";
        }
        else
        {
            lsExtension = Path.GetExtension(FuArchivoMod.FileName).ToUpper();
            if (!liExtension.Contains(lsExtension))
                lblMensajeMod.Text += "La extensión del archivo modificado no es válida: " + lsExtValidas + "<br>";
            if (File.Exists(sRutaArc + FuArchivoMod.FileName))
                lblMensajeMod.Text += "El archivo modificado ya está cargado en el sistema<br>";
        }
        if (FuArchivoOrg.FileName == FuArchivoMod.FileName && FuArchivoOrg.FileName != "")
            lblMensajeMod.Text += "Los nombres de los archivos de los contratos original y modificado deben ser diferentes<br>";

        if (lblMensajeMod.Text == "")
        {
            try
            {
                hdfCausa.Value = "0";
                hdfTipoCesion.Value = ddlTipo.SelectedValue;
                string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_codigo_operador", "@P_codigo_verif", "@P_contrato_definitivo", "@P_codigo_causa", "@P_contrato_original", "@P_contrato_nuevo", "@P_tipo_cesion", "@P_estado" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
                string[] lValorParametros = { "0", hdfOperador.Value, hdfIdRegistro.Value, hdfContratoDef.Value, DdlIngCausa.SelectedValue, FuArchivoOrg.FileName, FuArchivoMod.FileName, ddlTipo.SelectedValue, "C" };

                lConexion.Abrir();
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetSolModCont", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensajeMod.Text = "Se presentó un Problema en la Creación de la solicitud de modificación.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                        {
                            if (lLector["Error"].ToString() != "NO")
                                lblMensajeMod.Text = lLector["Error"].ToString();
                            else
                            {
                                hdfNoSolicitud.Value = lLector["codigo_sol"].ToString();
                                try
                                {
                                    FuArchivoOrg.SaveAs(sRutaArc + FuArchivoOrg.FileName);
                                    FuArchivoMod.SaveAs(sRutaArc + FuArchivoMod.FileName);
                                }
                                catch (Exception ex)
                                {
                                    lblMensaje.Text += "Error al cargar los archivos." + ex.Message.ToString();
                                }
                                hdfCausa.Value = DdlIngCausa.SelectedValue;
                                tblGrilla.Visible = true;
                                tblDatos.Visible = true;
                                divSolicitud.Visible = false;
                                crear();
                            }
                        }

                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();

                }
                lConexion.Cerrar();
                hdfCausa.Value = DdlIngCausa.SelectedValue;
            }
            catch (Exception ex)
            {
                lblMensajeMod.Text = ex.Message;
            }
            //20190425 rq022-19 
            if (hdfMostrar.Value == "S")
                tblDemanda.Visible = true;
        }
        
    }
    /// <summary>
    /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnCancelar_Click(object sender, EventArgs e)
    {
        manejo_bloqueo("E", hdfIdRegistro.Value);
        divSolicitud.Visible = false;
    }
    /// <summary>
    /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnModificar_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_solicitud","@P_codigo_verif","@P_codigo_modif","@P_codigo_comprador","@P_codigo_vendedor","@P_punta_modifica",
                "@P_fecha_inicial","@P_fecha_final", "@P_cantidad","@P_capacidad_otm"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int
                                        };
        string[] lValorParametros = { "0", "0","0", "0","0", "","","", "0","0"
                                    };
        lblMensaje.Text = "";
        DateTime ldFecha;
        DateTime ldFechaI = DateTime.Now;
        int liValor = 0;

        try
        {
            if (DdlComprador.SelectedValue == "0" && DdlMercado.SelectedValue != "O")
                lblMensaje.Text += "Debe seleccionar el Comprador.<br>";
            if (DdlVendedor.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el vendedor.<br>";
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIni.Text.Trim());
            }
            catch (Exception)
            {
                lblMensaje.Text += "Fecha de entrega inicial inválida.<br>";
            }
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaFin.Text.Trim());
                if (ldFechaI > ldFecha)
                    lblMensaje.Text += "la fecha de entrega inicial debe ser menor o igual que la final.<br>";
            }
            catch (Exception)
            {
                lblMensaje.Text += "Fecha de entrega final inválida.<br>";
            }
            if (TxtCantidad.Text.Trim().Length <= 0)
                lblMensaje.Text += " Debe Ingresar la cantidad. <br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCantidad.Text);
                }
                catch (Exception)
                {
                    lblMensaje.Text += " Valor Inválido en la cantidad/capacidad. <br>";
                }
            }
            if (TxtCapacOtm.Text.Trim().Length <= 0)
                lblMensaje.Text += " Debe Ingresar la capacidad de transporte. <br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCapacOtm.Text);
                }
                catch (Exception)
                {
                    lblMensaje.Text += " Valor Inválido en la capacidad de transporte. <br>";
                }
            }

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = hdfNoSolicitud.Value;
                lValorParametros[1] = hdfIdRegistro.Value;
                lValorParametros[2] = hdfCodModif.Value; ;
                lValorParametros[3] = DdlComprador.SelectedValue;
                lValorParametros[4] = DdlVendedor.SelectedValue;
                lValorParametros[5] = hdfPunta.Value;
                lValorParametros[6] = TxtFechaIni.Text.Trim();
                lValorParametros[7] = TxtFechaFin.Text.Trim();
                lValorParametros[8] = TxtCantidad.Text.Trim();
                lValorParametros[9] = TxtCapacOtm.Text.Trim();

                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModContCesion", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Text += lLector["error"].ToString() + "<br>";
                }
                lConexion.Cerrar();

                if (lblMensaje.Text == "")
                {
                    CargarDatosOper();
                    lblMensaje.Text = "Operación modificada correctamente";
                }

            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAprobar_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_solicitud","@P_codigo_modif","@P_codigo_operador","@P_estado" ,"@P_observaciones"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar
                                        };
        string[] lValorParametros = { "0", "0", "0", "A",""
                                    };
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;

        if (TxtObs.Text == "")
            lblMensaje.Text = "Debe ingresar las observaciones para hacer la aprobación";
        try
        {

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = hdfNoSolicitud.Value;
                lValorParametros[1] = hdfCodModif.Value;
                lValorParametros[2] = goInfo.cod_comisionista;
                lValorParametros[4] = TxtObs.Text;

                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModCesionApr", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Text += lLector["error"].ToString() + "<br>";
                }
                lConexion.Cerrar();

                if (lblMensaje.Text == "")
                {
                    manejo_bloqueo("E", hdfIdRegistro.Value);
                    CargarDatosOper();
                    lblMensaje.Text = "Modificación aprobada correctamente";
                }

            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRechazar_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_solicitud","@P_codigo_modif","@P_estado" ,"@P_observaciones"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar
                                        };
        string[] lValorParametros = { "0", "0", "R",""
                                    };
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;

        if (TxtObs.Text == "")
            lblMensaje.Text = "Debe ingresar las observaciones para hacer el rechazo";
        try
        {

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = hdfNoSolicitud.Value;
                lValorParametros[1] = hdfCodModif.Value;
                lValorParametros[3] = TxtObs.Text;

                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModCesionApr", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Text += lLector["error"].ToString() + "<br>";
                }
                lConexion.Cerrar();

                if (lblMensaje.Text == "")
                {
                    manejo_bloqueo("E", hdfIdRegistro.Value);
                    CargarDatosOper();
                    lblMensaje.Text = "Modificación rechazada correctamente";
                }

            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAprobarBmc_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_estado","@P_observaciones"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar
                                        };
        string[] lValorParametros = { "0","A",""
                                    };
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;

        if (TxtObs.Text == "")
            lblMensaje.Text = "Debe ingresar las observaciones para hacer la aprobación";
        try
        {

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = hdfNoSolicitud.Value;
                lValorParametros[2] = TxtObs.Text;
                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModCesionAprBmc", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Text += lLector["error"].ToString() + "<br>";
                }
                lConexion.Cerrar();

                if (lblMensaje.Text == "")
                {
                    manejo_bloqueo("E", hdfIdRegistro.Value);
                    CargarDatosOper();
                    lblMensaje.Text = "Modificación aprobada correctamente";
                }

            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Error al aprobar la solicitud. " + ex.Message;
        }
    }
    /// <summary>
    /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRechazarBmc_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_solicitud", "@P_estado","@P_observaciones"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar
                                        };
        string[] lValorParametros = { "0", "R", ""
                                    };
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;

        if (TxtObs.Text == "")
            lblMensaje.Text = "Debe ingresar las observaciones para hacer el rechazo";
        try
        {

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = hdfNoSolicitud.Value;
                lValorParametros[2] = TxtObs.Text;
                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModCesionAprBmc", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Text += lLector["error"].ToString() + "<br>";
                }
                lConexion.Cerrar();

                if (lblMensaje.Text == "")
                {
                    manejo_bloqueo("E", hdfIdRegistro.Value);
                    CargarDatosOper();
                    lblMensaje.Text = "Modificación rechazada correctamente";
                }

            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Error al rechazar la solicitud. " + ex.Message;
        }
    }
    /// <summary>
    /// Nombre: btnRegresar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    protected void btnRegresar_Click(object sender, EventArgs e)
    {
        manejo_bloqueo("E", hdfIdRegistro.Value);
        tblDatos.Visible = true;
        tblGrilla.Visible = true;
        tblModifica.Visible = false;
        tblDemanda.Visible = false;
        lblMensaje.Text = "";
        CargarDatos();
    }
    /// <summary>
    /// Nombre: CargarDatosOper
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatosOper()
    {
        lConexion.Abrir();
        string[] lsNombreParametros = { "@P_codigo_verif", "@P_codigo_modif" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { hdfIdRegistro.Value, hdfCodModif.Value };
        try
        {
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetDatosContModCesion", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
            if (goInfo.mensaje_error != "")
            {
                lblMensaje.Text = "Se presentó un Problema en la selección de la información.! " + goInfo.mensaje_error.ToString();
                lConexion.Cerrar();
            }
            else
            {
                if (lLector.HasRows)
                {
                    lLector.Read();

                    DdlDestino.SelectedValue = lLector["destino_rueda"].ToString();
                    DdlMercado.SelectedValue = lLector["tipo_mercado"].ToString();
                    DdlVendedor.SelectedValue = lLector["operador_venta"].ToString();
                    DdlComprador.SelectedValue = lLector["operador_compra"].ToString();
                    TxtFechaNeg.Text = lLector["fecha_neg"].ToString();
                    TxtHoraNeg.Text = lLector["hora_neg"].ToString();
                    TxtFechaSus.Text = lLector["fecha_suscripcion"].ToString();
                    TxtContDef.Text = lLector["contrato_definitivo"].ToString();
                    DdlVariable.SelectedValue = lLector["ind_contrato_var"].ToString();
                    TxtFechaIni.Text = lLector["fecha_ini"].ToString();
                    TxtFechaFin.Text = lLector["fecha_fin"].ToString();
                    TxtCantidad.Text = lLector["cantidad"].ToString();
                    TxtCapacOtm.Text = lLector["capacidad_otm"].ToString();
                    TxtPrecio.Text = lLector["precio"].ToString();
                }
            }
            if (hdfPunta.Value == "C" && DdlMercado.SelectedValue != "M")
                CargarDatosUsu();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Error al recuperar información." + ex.Message.ToString();
            lConexion.Cerrar();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCrearUsu_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_cont_usr", "@P_codigo_modif","@P_codigo_verif", "@P_no_identificacion_usr", "@P_codigo_tipo_doc",
                                        "@P_nombre_usuario", "@P_codigo_sector_consumo", "@P_codigo_punto_salida","@P_cantidad_contratada","@P_tipo_demanda",
                                        "@P_codigo_mercado_relevante","@P_equivalente_kpcd", "@P_accion","@P_destino_rueda", "@P_codigo_solicitud"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,
                                         SqlDbType.VarChar, SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,
                                         SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int
                                      };
        string[] lValorParametros = { "0", "0", "0", "", "", "", "0", " 0", "0", "0", "0","0","1","","0"
                                    };

        lblMensaje.Text = "";
        string[] lsUsuario;
        string lsNombre = "";
        string lsTipoDoc = "";
        int liValor = 0;
        try
        {
            lConexion.Abrir();
            lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
            if (dlTipoDemanda.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar el Tipo de Demanda a Atender. <br> ";
            else
            {
                if (dlTipoDemanda.SelectedValue == "2")
                {
                    if (TxtUsuarioFinal.Text.Trim().Length <= 0)
                        lblMensaje.Text = "Debe Ingresar el Usuario Final <br>";
                    else
                    {
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                        if (!lLector.HasRows)
                            lblMensaje.Text += "El Usuario Ingresado NO existe en la Base de Datos.! <br>";
                        else
                        {
                            lLector.Read();
                            lsNombre = lLector["nombre"].ToString();
                            lsTipoDoc = lLector["codigo_tipo_doc"].ToString();
                        }
                        lLector.Close();
                        lLector.Dispose();
                    }
                }
                else
                {
                    if (ddlMercadoRel.SelectedValue == "0")
                        lblMensaje.Text += "Debe Seleccionar el mercado relevante. <br> ";
                }
            }
            if (ddlSector.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar " + lblSector.Text + ". <br> ";
            if (ddlPuntoSalida.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar  Punto de Salida en SNT. <br> ";
            if (TxtCantidadUsu.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar " + lblCantContra.Text + ". <br> ";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCantidadUsu.Text.Trim());
                    if (liValor < 1)
                        lblMensaje.Text += "Valor Inválido " + lblCantContra.Text + ". <br> ";
                    else
                    {
                        if (liValor > Convert.ToDecimal(TxtCantidad.Text))
                            lblMensaje.Text += "La " + lblCantContra.Text + " No puede ser Mayor que " + TxtCantidad.Text + ". <br> ";
                        else
                        {
                            if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(TxtCantidad.Text))
                                lblMensaje.Text += "La cantidad Acumulada de Usuarios No puede ser Mayor que " + TxtCantidad.Text + ". <br> ";
                        }
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido " + lblCantContra.Text + ". <br> ";
                }
            }
            if (TxtEquivaleKpcd.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar Equivalente Kpcd. <br> ";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtEquivaleKpcd.Text.Trim());
                    if (liValor < 0)
                        lblMensaje.Text += "Valor Inválido Equivalente Kpcd. <br> ";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido " + lblCantContra.Text + ". <br> ";
                }
            }
            if (lblMensaje.Text == "")
            {

                lValorParametros[0] = "0";
                if (hdfCodModif.Value != "")
                    lValorParametros[1] = hdfCodModif.Value;
                if (hdfIdRegistro.Value != "")
                    lValorParametros[2] = hdfIdRegistro.Value;
                if (hdfTipoDemanda.Value == "2")
                {
                    lValorParametros[3] = lsUsuario[0].Trim();
                    lValorParametros[4] = lsTipoDoc;
                    lValorParametros[5] = lsNombre;
                }
                lValorParametros[6] = ddlSector.SelectedValue;
                lValorParametros[7] = ddlPuntoSalida.SelectedValue;
                lValorParametros[8] = TxtCantidadUsu.Text.Trim();
                lValorParametros[9] = dlTipoDemanda.SelectedValue;
                lValorParametros[10] = ddlMercadoRel.SelectedValue;
                lValorParametros[11] = TxtEquivaleKpcd.Text.Trim();
                lValorParametros[13] = DdlDestino.SelectedValue;
                lValorParametros[14] = hdfNoSolicitud.Value;

                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetModContCesionRegUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación de la Información del Usuario.!" + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(TxtCantidadUsu.Text.Trim())).ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Información de Usuarios Finales Ingresada Correctamente.!" + "');", true);
                    TxtUsuarioFinal.Text = "";
                    ddlSector.SelectedValue = "0";
                    ddlPuntoSalida.SelectedValue = "0";
                    ddlMercadoRel.SelectedValue = "0";
                    TxtCantidadUsu.Text = "0";
                    TxtEquivaleKpcd.Text = "0"; // Campos nuevos Req. 009-17 Indicadores 20170321
                    CargarDatosUsu();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnActualUsu_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_cont_usr", "@P_codigo_modif","@P_codigo_verif", "@P_no_identificacion_usr", "@P_codigo_tipo_doc",
                                        "@P_nombre_usuario", "@P_codigo_sector_consumo", "@P_codigo_punto_salida","@P_cantidad_contratada","@P_tipo_demanda",
                                        "@P_codigo_mercado_relevante","@P_equivalente_kpcd", "@P_accion","@P_destino_rueda", "@P_codigo_solicitud"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,
                                         SqlDbType.VarChar, SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,
                                         SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int
                                      };
        string[] lValorParametros = { "0", "0", "0", "", "", "", "0", " 0", "0", "0", "0","0","2","", "0"
                                    };
        lblMensaje.Text = "";
        string[] lsUsuario;
        int liValor = 0;
        string lsNombre = "";
        string lsTipoDoc = "";
        try
        {
            lConexion.Abrir();
            lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
            if (dlTipoDemanda.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar el Tipo Demanda Atender. <br> ";
            else
            {
                if (hdfTipoDemanda.Value == "2")
                {
                    if (TxtUsuarioFinal.Text.Trim().Length <= 0)
                        lblMensaje.Text = "Debe Ingresar el Usuario Final <br>";
                    else
                    {
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                        if (!lLector.HasRows)
                            lblMensaje.Text += "El Usuario Ingresado NO existe en la Base de Datos.! <br>";
                        else
                        {
                            lLector.Read();
                            lsNombre = lLector["nombre"].ToString();
                            lsTipoDoc = lLector["codigo_tipo_doc"].ToString();
                        }
                        lLector.Close();
                        lLector.Dispose();
                    }
                }
                else
                {
                    if (ddlMercadoRel.SelectedValue == "0")
                        lblMensaje.Text += "Debe Seleccionar el mercado relevante. <br> ";
                }

            }
            if (ddlSector.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar " + lblSector.Text + ". <br> ";
            if (ddlPuntoSalida.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar  Punto de Salida en SNT. <br> ";
            if (TxtCantidadUsu.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar " + lblCantContra.Text + ". <br> ";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCantidadUsu.Text.Trim());
                    if (liValor < 1)
                        lblMensaje.Text += "Valor Inválido " + lblCantContra.Text + ". <br> ";
                    else
                    {
                        if (liValor > Convert.ToDecimal(TxtCantidad.Text))
                            lblMensaje.Text += "La " + lblCantContra.Text + " No puede ser Mayor que " + TxtCantidad.Text + ". <br> ";
                        else
                        {
                            if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(TxtCantidad.Text))
                                lblMensaje.Text += "La Cantidad Acumulada de Usuarios No puede ser Mayor que " + TxtCantidad.Text + ". <br> ";
                        }
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido " + lblCantContra.Text + ". <br> ";
                }
            }
            if (TxtEquivaleKpcd.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar el Equivalente Kpcd. <br> ";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtEquivaleKpcd.Text.Trim());
                    if (liValor < 0)
                        lblMensaje.Text += "Valor Inválido en Equivalente Kpcd. <br> ";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en " + lblCantContra.Text + ". <br> ";
                }
            }
            if (lblMensaje.Text == "")
            {
                if (hdfCodDatUsu.Value != "")
                    lValorParametros[0] = hdfCodDatUsu.Value;
                if (hdfCodModif.Value != "")
                    lValorParametros[1] = hdfCodModif.Value;
                if (hdfIdRegistro.Value != "")
                    lValorParametros[2] = hdfIdRegistro.Value;
                if (hdfTipoDemanda.Value == "2")
                {
                    lValorParametros[3] = lsUsuario[0].Trim();
                    lValorParametros[4] = lsTipoDoc;
                    lValorParametros[5] = lsNombre;
                }
                lValorParametros[6] = ddlSector.SelectedValue;
                lValorParametros[7] = ddlPuntoSalida.SelectedValue;
                lValorParametros[8] = TxtCantidadUsu.Text.Trim();
                lValorParametros[9] = dlTipoDemanda.SelectedValue;
                lValorParametros[10] = ddlMercadoRel.SelectedValue;
                lValorParametros[11] = TxtEquivaleKpcd.Text.Trim();
                lValorParametros[13] = DdlDestino.SelectedValue;
                lValorParametros[14] = hdfNoSolicitud.Value;

                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetModContCesionRegUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización de la Información del Usuario.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Información de Usuarios Finales Actualizada Correctamente.!" + "');", true);
                    TxtUsuarioFinal.Text = "";
                    ddlSector.SelectedValue = "0";
                    ddlPuntoSalida.SelectedValue = "0";
                    ddlMercadoRel.SelectedValue = "0";
                    TxtCantidadUsu.Text = "0";
                    TxtEquivaleKpcd.Text = "0"; // Campos nuevos Req. 009-17 Indicadores 20170321
                    CargarDatosUsu();
                    btnActualUsu.Visible = false;
                    btnCrearUsu.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dlTipoDemanda_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            hdfTipoDemanda.Value = dlTipoDemanda.SelectedValue;
            if (hdfTipoDemanda.Value == "1")
            {
                TxtUsuarioFinal.Enabled = false;
                ddlMercadoRel.Enabled = true; //20160303
                lblSector.Text = "Sector de Consumo Usuario Regulado";
                lblCantContra.Text = "Cantidad a entregar";
            }
            else
            {
                TxtUsuarioFinal.Enabled = true;
                ddlMercadoRel.Enabled = false; //20160303
                ddlMercadoRel.SelectedValue = "0"; //20160303
                lblCantContra.Text = "Cantidad Contratada por usuario (MBTUD)";
                lblSector.Text = "Sector Consumo Usuario No Regulado";
            }
            lConexion.Abrir();
            ddlSector.Items.Clear();
            ddlPuntoSalida.Items.Clear();
            LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo sec, m_demanda_sector dem", " sec.estado ='A' and sec.codigo_sector_consumo = dem.codigo_sector_consumo  and dem.estado ='A' and dem.codigo_tipo_demanda =" + dlTipoDemanda.SelectedValue + "  and dem.ind_uso ='T' order by descripcion", 0, 1); //20160706
            LlenarControles(lConexion.gObjConexion, ddlPuntoSalida, "m_punto_salida_snt", " estado = 'A'  order by descripcion", 0, 2);
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {

        }
    }
    /// <summary>
    /// Nombre: CargarDatosUsu
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid de Usuarios.
    /// Modificacion:
    /// </summary>
    private void CargarDatosUsu()
    {
        string[] lsNombreParametros = { "@P_codigo_modif", "@P_codigo_verif", "@P_no_identificacion_usr", "@P_login" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "0", "", "" };
        try
        {
            if (hdfCodModif.Value.Trim().Length > 0 && hdfCodModif.Value.Trim() != "0")
                lValorParametros[0] = hdfCodModif.Value;
            if (hdfIdRegistro.Value.Trim().Length > 0)
                lValorParametros[1] = hdfIdRegistro.Value;
            if (Session["tipoPerfil"].ToString() == "N")
                lValorParametros[3] = goInfo.Usuario.ToString();
            lConexion.Abrir();
            dtgUsuarios.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContModCesionUsuFin", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgUsuarios.DataBind();
            lConexion.Cerrar();
            lblTotlCantidad.Text = "0";
            if (dtgUsuarios.Items.Count > 0)
            {
                foreach (DataGridItem Grilla in this.dtgUsuarios.Items)
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(Grilla.Cells[6].Text.Trim())).ToString();
                }
                dtgUsuarios.Columns[7].Visible = true;
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// Nombre: dtgUsuarios_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgUsuarios_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";

        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            try
            {
                ////////////////////////////////////////////////////////////////////////////////////////
                /// OJO Cambios de los Indices por adicion de Campo Req. 009-17 Indicadores 20170321 ///
                ////////////////////////////////////////////////////////////////////////////////////////
                string lsTipoDoc = "";
                if (this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[14].Text == "1")  //rq009-17
                    lsTipoDoc = "Nit";
                else
                    lsTipoDoc = "Cédula";
                hdfCodDatUsu.Value = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text; //rq009-17
                                                                                              //Ajuste para que no muestre infrmacion mala 20160621
                if (this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[1].Text == "&nbsp;")
                    TxtUsuarioFinal.Text = lsTipoDoc;
                else
                    TxtUsuarioFinal.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[1].Text + "-" + lsTipoDoc + "-" + this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[3].Text;
                TxtCantidadUsu.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[6].Text;
                lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) - Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                dlTipoDemanda.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[15].Text; //rq009-17
                dlTipoDemanda_SelectedIndexChanged(null, null);
                ddlSector.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[12].Text; //rq009-17
                ddlMercadoRel.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[16].Text; //rq009-17
                ddlPuntoSalida.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[13].Text; //rq009-17
                btnCrearUsu.Visible = false;
                btnActualUsu.Visible = true;
                TxtEquivaleKpcd.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[7].Text;
                TdIndica.Visible = true;
                ///TdIndica1.Visible = true;
                btnActualUsu.Visible = true;
                btnCrearUsu.Visible = false;
                hdfCodigoVerUsr.Value = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text;
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al Recuperar el Registro.! " + ex.Message.ToString();
            }

        }
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            string[] lsNombreParametros = { "@P_codigo_cont_usr", "@P_accion", "@P_codigo_solicitud" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { hdfCodDatUsu.Value, "3", "0" };

            try
            {
                hdfCodDatUsu.Value = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text;
                lValorParametros[0] = hdfCodDatUsu.Value;
                lValorParametros[2] = hdfNoSolicitud.Value;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetModContCesionRegUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Eliminación de la Información del Usuario.!" + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    TxtCantidadUsu.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[6].Text; //20190506 ajsute
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) - Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                    lConexion.Cerrar();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Eliminado Correctamente.!" + "');", true);
                    CargarDatosUsu();
                    TxtCantidadUsu.Text = ""; //20190506 ajsute
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al Eliminar el Registro.! " + ex.Message.ToString();

            }
        }

    }


    /// <summary>
    /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGrabarDef_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_solicitud"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int
                                        };
        string[] lValorParametros = { hdfNoSolicitud.Value
                                    };
        lblMensaje.Text = "";
        try
        {
            if (lblMensaje.Text == "")
            {
                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModContCesionDef", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Text += lLector["error"].ToString() + "<br>";
                }
                lConexion.Cerrar();

                if (lblMensaje.Text == "")
                {
                    btnRegresar_Click(null, null);
                    lblMensaje.Text = "Solicitud grabada completa correctamente";
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='t_contrato_modificacion' and llave_registro='codigo_verif_contrato=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_verif_contrato=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "t_contrato_modificacion";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_contrato_modificacion", lsCondicion1);
        }
        return true;
    }
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20170814 rq036-17
    protected void DdlBusMercado_SelectedIndexChanged(object sender, EventArgs e)
    {
        DdlBusProducto.Items.Clear();
        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "";
        lItem.Text = "Seleccione";
        DdlBusProducto.Items.Add(lItem);
        System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
        lItem1.Value = "G";
        lItem1.Text = "Suministro de gas";
        DdlBusProducto.Items.Add(lItem1);
        System.Web.UI.WebControls.ListItem lItem2 = new System.Web.UI.WebControls.ListItem();
        lItem2.Value = "T";
        lItem2.Text = "Capacidad de transporte";
        DdlBusProducto.Items.Add(lItem2);
        if (DdlBusMercado.SelectedValue == "" || DdlBusMercado.SelectedValue == "O")
        {
            System.Web.UI.WebControls.ListItem lItem3 = new System.Web.UI.WebControls.ListItem();
            lItem3.Value = "A";
            lItem3.Text = "Suministro y transporte";
            DdlBusProducto.Items.Add(lItem3);
        }
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected void trae_usr_fin(string lsModif, string lsVerif)
    {
        string[] lsNombreParametros = { "@P_codigo_modif","@P_codigo_verif"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int
                                        };
        string[] lValorParametros = { lsModif, lsVerif
                                    };
        try
        {
            lConexion.Abrir();
            DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetUsrFinModCes", lsNombreParametros, lTipoparametros, lValorParametros);
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Se presentó un error al recuperar los usuarios finales. " + ex.Message;
        }

    }
    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected void revisa_usr_fin()
    {
        if (goInfo.cod_comisionista != DdlComprador.SelectedValue)
        {
            btnCrearUsu.Visible = false;
            tblDemanda.Visible = false;
        }
        else
        {
            if (DdlMercado.SelectedValue != "O")
            {
                btnCrearUsu.Visible = true;
                tblDemanda.Visible = true;
                trae_usr_fin(hdfCodModif.Value, hdfIdRegistro.Value);
            }
            else
            {
                btnCrearUsu.Visible = false;
                tblDemanda.Visible = false;
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlComprador_SelectedIndexChanged(object sender, EventArgs e)
    {
        revisa_usr_fin();
    }
    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    /// //20190425 rq022-19 ajsute modificaciones
    protected void color_label(string lsColor)
    {
        if (lsColor == "N")
        {
            LblOperadorC.ForeColor = System.Drawing.Color.Black;
            LblOperadorV.ForeColor = System.Drawing.Color.Black;
            LblFechaIni.ForeColor = System.Drawing.Color.Black;
            LblFechaFin.ForeColor = System.Drawing.Color.Black;
            LblCantidad.ForeColor = System.Drawing.Color.Black;
            trCambioUsrFin.Visible = false;
        }
        else
        {
            string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_codigo_modif" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { hdfIdRegistro.Value, hdfCodModif.Value };
            try
            {
                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetDatoModifContCes", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    if (lLector["mod_operador_c"].ToString() == "S")
                        LblOperadorC.ForeColor = System.Drawing.Color.Red;
                    else
                        LblOperadorC.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_operador_v"].ToString() == "S")
                        LblOperadorV.ForeColor = System.Drawing.Color.Red;
                    else
                        LblOperadorV.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_fecha_ini"].ToString() == "S")
                        LblFechaIni.ForeColor = System.Drawing.Color.Red;
                    else
                        LblFechaIni.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_fecha_fin"].ToString() == "S")
                        LblFechaFin.ForeColor = System.Drawing.Color.Red;
                    else
                        LblFechaFin.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_cantidad"].ToString() == "S")
                        LblCantidad.ForeColor = System.Drawing.Color.Red;
                    else
                        LblCantidad.ForeColor = System.Drawing.Color.Black;
                    if (lLector["mod_usr_final"].ToString() == "S")
                        trCambioUsrFin.Visible = true;
                    else
                        trCambioUsrFin.Visible = false;
                }
                else
                {
                    LblOperadorC.ForeColor = System.Drawing.Color.Black;
                    LblOperadorV.ForeColor = System.Drawing.Color.Black;
                    LblFechaIni.ForeColor = System.Drawing.Color.Black;
                    LblFechaFin.ForeColor = System.Drawing.Color.Black;
                    LblCantidad.ForeColor = System.Drawing.Color.Black;
                    trCambioUsrFin.Visible = false;
                }
                lLector.Close();
                lConexion.Cerrar();
            }
            catch (Exception)
            {
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lkbExcel_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        try
        {

            string lsNombreArchivo = Session["login"] + "InfExcelReg" + DateTime.Now + ".xls";
            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            lpagina.EnableEventValidation = false;
            lpagina.Controls.Add(lform);
            dtgConsulta.EnableViewState = false;
            dtgConsulta.Columns[0].Visible = false;
            dtgConsulta.Columns[1].Visible = false;
            dtgConsulta.Columns[21].Visible = false;
            dtgConsulta.Columns[22].Visible = false;
            lform.Controls.Add(dtgConsulta);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;

            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + "Consulta de contratos para modificar" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write(lsb.ToString());
            Response.End();
            Response.Flush();
            dtgConsulta.Columns[0].Visible = true;
            dtgConsulta.Columns[1].Visible = true;
            dtgConsulta.Columns[21].Visible = true;
            dtgConsulta.Columns[22].Visible = true;

        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas al Consultar los Registros. " + ex.Message.ToString();
        }
    }
}
