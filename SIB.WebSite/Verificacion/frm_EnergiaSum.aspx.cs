﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Verificacion
{
    public partial class frm_EnergiaSum : Page
    {
        private InfoSessionVO goInfo;
        private static string lsTitulo = "Cantidad de Energía a Suministrar"; //20160810 modalidad por tipo campo 
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        private clConexion lConexion;
        private SqlDataReader lLector;
        private SqlDataReader lLector1;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            //Establese los permisos del sistema
            //EstablecerPermisosSistema(); //20220621 ajsute
            lConexion = new clConexion(goInfo);
            new clConexion(goInfo);
            //Titulo
            Master.Titulo = "Registros Operativos";
            /// Activacion de los Botones
            buttons.Inicializar(ruta: "t_energia_a_suminsitrar");
            buttons.CrearOnclick += btnNuevo;
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.ExportarPdfOnclick += ImgPdf_Click;

            if (IsPostBack) return;
            // Carga informacion de combos
            lConexion.Abrir();
            //20220705
            if (goInfo.cod_comisionista != "0")
            {
                LlenarControles(lConexion.gObjConexion, ddlPozo, "m_pozo poz, m_punto_operador ope", " poz.estado ='A' and poz.codigo_pozo = ope.codigo_punto and ope.tipo_punto ='E' and (ope.codigo_operador =0 or ope.codigo_operador =" + goInfo.cod_comisionista + ") and ind_entrada ='S' and ope.estado='A'  order by poz.descripcion", 0, 1); //20161108 control inf ope //20220202 verifica operativa //20220705
                LlenarControles(lConexion.gObjConexion, ddlBusPozo, "m_pozo poz, m_punto_operador ope", " poz.estado ='A' and poz.codigo_pozo = ope.codigo_punto and ope.tipo_punto ='E' and (ope.codigo_operador =0 or ope.codigo_operador =" + goInfo.cod_comisionista + ") and ind_entrada ='S' and ope.estado='A'  order by poz.descripcion", 0, 1); //20161108 control inf ope //20220202 verifica operativa //20220705
            }
            else
            {
                LlenarControles(lConexion.gObjConexion, ddlPozo, "m_pozo", " estado ='A' and ind_entrada ='S'  order by descripcion", 0, 1); //20161108 control inf ope //20220202 verifica operativa //20220705
                LlenarControles(lConexion.gObjConexion, ddlBusPozo, "m_pozo", " estado ='A' and ind_entrada ='S'  order by descripcion", 0, 1); //20161108 control inf ope //20220202 verifica operativa //20220705
            }
            LlenarControles1(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador >0  and (tipo_operador in ('P','I') or codigo_operador = " + goInfo.cod_comisionista + ") order by razon_social", 0, 4);  //20180122 rq003-18 // 20180126 rq107-16
            LlenarControles1(lConexion.gObjConexion, ddlBusOperador, "m_operador", " estado = 'A' and codigo_operador >0 and (tipo_operador in ('P','I') or codigo_operador = " + goInfo.cod_comisionista + ") order by razon_social", 0, 4);  //20180122 rq003-18 // 20180126 rq107-16
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_no_ing_inf_ope", " codigo_reporte = 6 and codigo_operador =" + goInfo.cod_comisionista + " and estado ='A' ");
            if (lLector.HasRows)
            {
                Toastr.Warning(this, "El Operador no está obligado a ingresar la información operativa de energía a suministrar.");
            }
            lLector.Close();
            lLector.Dispose();

            lConexion.Cerrar();
            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlBusOperador.SelectedValue = goInfo.cod_comisionista;
                ddlBusOperador.Enabled = false;
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
                hdnELiminado.Value = "N";  //20160721 modif inf opertiva y transaccional
            }
            else
                hdnELiminado.Value = "S";  //20160721 modif inf opertiva y transaccional

            DateTime ldfecha;
            ldfecha = DateTime.Now.AddDays(-1);
            TxtFecha.Text = ldfecha.Year + "/" + ldfecha.Month + "/" + ldfecha.Day;
            TxtFecha.Enabled = false;
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            Buscar();
        }

        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "t_energia_a_suminsitrar");
            //hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
            //hlkListar.Enabled = (Boolean)permisos["SELECT"];
            //hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
            //if (Session["tipoPerfil"].ToString() != "N")
            //    dtgMaestro.Columns[10].Visible = (Boolean)permisos["UPDATE"]; //20160711 ajuste modificaciones
            //else
            //    dtgMaestro.Columns[10].Visible = false;
            //dtgMaestro.Columns[11].Visible = false; //20160711 ajuste modificaciones
            //20220621 ajsute
            if ((bool)permisos["UPDATE"] && (bool)permisos["DELETE"])
                dtgMaestro.Columns[12].Visible = true; //20220705
            else
            {
                if (!(bool)permisos["UPDATE"] && !(bool)permisos["DELETE"])
                    dtgMaestro.Columns[12].Visible = false;//20220705
                else
                {
                    if (!(bool)permisos["UPDATE"])
                    {
                        foreach (DataGridItem Grilla in dtgMaestro.Items)
                        {
                            var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                            lkbModificar.Visible = false;
                        }
                    }
                    if (!(bool)permisos["DELETE"])
                    {
                        foreach (DataGridItem Grilla in dtgMaestro.Items)
                        {
                            var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
                            lkbEliminar.Visible = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Nombre: Listar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            CargarDatos();
            lblTitulo.Text = "Consultar " + lsTitulo;
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            try
            {
                var lblMensaje = new StringBuilder();

                if (modificar != null && modificar != "")
                {
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_energia_a_suminsitrar", " codigo_energia_suministrar= " + modificar + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            LblCodigoCap.Text = lLector["codigo_energia_suministrar"].ToString();
                            TxtCodigoCap.Text = lLector["codigo_energia_suministrar"].ToString();
                            DateTime ldFecha; //20160711 ajuste msificaciones
                            ldFecha = Convert.ToDateTime(lLector["fecha"].ToString()); //20160711 ajuste msificaciones
                            TxtFecha.Text = ldFecha.Year + "/" + ldFecha.Month + "/" + ldFecha.Day; //20160711 ajuste msificaciones

                            try
                            {
                                ddlOperador.SelectedValue = lLector["codigo_Operador"].ToString();
                            }
                            catch (Exception)
                            {
                                lblMensaje.Append("EL Operador del registro no existe o está inactivo<br>");
                            }
                            try
                            {
                                ddlPozo.SelectedValue = lLector["codigo_Pozo"].ToString();
                            }
                            catch (Exception)
                            {
                                lblMensaje.Append("El punto del SNT del registro no existe o está inactivo<br>");
                            }
                            TxtCantidad.Text = lLector["cantidad_a_suministrar"].ToString();
                            ddlEstado.SelectedValue = lLector["estado"].ToString();
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                            TxtCodigoCap.Visible = false;
                            LblCodigoCap.Visible = true;
                            TxtFecha.Enabled = false;
                            ddlOperador.Enabled = false;
                            ddlEstado.Enabled = true; //20161121 modif inf ope

                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Bloquea el Registro a Modificar
                        if (lblMensaje.ToString() == "") //20160809 carga ptdv
                            manejo_bloqueo("A", modificar);
                    }
                    else
                    {
                        Listar();
                        lblMensaje.Append("No se puede editar el registro por que está bloqueado. Código energía " + modificar); //20180126 rq107-16
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    Toastr.Warning(this, lblMensaje.ToString());
                    return;
                }

                //Abre el modal de Agregar
                Modal.Abrir(this, registroEnergiaSum.ID, registroEnergiaSumInside.ID);
                lblTitulo.Text = "Modificar " + lsTitulo;

            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: Buscar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
        /// Modificacion:
        /// </summary>
        private void Buscar()
        {
            lblTitulo.Text = "Consultar " + lsTitulo;
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_codigo_energia_suministrar", "@P_fecha", "@P_codigo_operador", "@P_codigo_Pozo", "@P_fecha_fin", "@P_mostrar_eliminado" }; //20160721 modif inf opertiva y trans
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar }; //20160721 modif inf opertiva y trans
            string[] lValorParametros = { "0", "", "0", "0", "", hdnELiminado.Value }; //20160721 modif inf opertiva y trans
            DateTime ldFecha;
            var lblMensaje = new StringBuilder();

            try
            {
                if (TxtBusFecha.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtBusFecha.Text.Trim());
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append("Valor Inválido en Fecha Inicial. <br>");
                    }
                }
                if (TxtBusFechaF.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtBusFechaF.Text.Trim());
                        if (ldFecha < Convert.ToDateTime(TxtBusFecha.Text.Trim()))
                            lblMensaje.Append("La Fecha final NO puede ser menor que la Fecha Inicial.!<br>");
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append("Valor Inválido en Fecha Final. <br>");
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    Toastr.Warning(this, lblMensaje.ToString());
                }
                else
                {
                    if (TxtBusCodigo.Text.Trim().Length > 0)
                        lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    if (TxtBusFecha.Text.Trim().Length > 0)
                        lValorParametros[1] = TxtBusFecha.Text.Trim();
                    else
                        lValorParametros[1] = ""; // 20181113 ajuste
                    if (ddlBusOperador.SelectedValue != "0")
                        lValorParametros[2] = ddlBusOperador.SelectedValue;
                    if (ddlBusPozo.SelectedValue != "0")
                        lValorParametros[3] = ddlBusPozo.SelectedValue;
                    if (TxtBusFechaF.Text.Trim().Length > 0)
                        lValorParametros[4] = TxtBusFechaF.Text.Trim();
                    else
                    {
                        if (TxtBusFecha.Text.Trim().Length > 0)
                            lValorParametros[4] = TxtBusFecha.Text.Trim();
                        else
                            lValorParametros[4] = ""; // 20181113 ajuste
                    }
                    //20220621 ajuste
                    if (lValorParametros[0] == "0" && lValorParametros[1] == "" && lValorParametros[3] == "0")
                    {
                        lValorParametros[1] = TxtFecha.Text;
                        lValorParametros[4] = TxtFecha.Text;
                    }

                    lConexion.Abrir();
                    dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetEnergiaSum", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgMaestro.DataBind();
                    lConexion.Cerrar();
                    EstablecerPermisosSistema(); //20220621 ajsute
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Page")) return;

                string lCodigoRegistro = "";
                var lblMensaje = new StringBuilder();
                string lsFecha = ""; // Cambio Req. 003-17 20170216
                hdfModificacion.Value = "N"; // Cambio Req. 003-17 20170216
                lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text; // Cambio Req. 003-17 20170216
                lsFecha = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[1].Text; // Cambio Req. 003-17 20170216
                ///////////////////////////////////////////////////////////////////////////////////
                ///// Control Nuevo para Modif Inf Operativa Participantes Req. 003-17 20170216 ///
                ///////////////////////////////////////////////////////////////////////////////////
                bool blPuede = true;
                int liHoraAct = 0;
                string lsHoraMax = "0";
                int liValFin = 0;
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsHoraMax = lLector["hora_max_declara_opera"].ToString();
                }

                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
                liValFin = ((Convert.ToDateTime(lsHoraMax).Hour * 60) + Convert.ToDateTime(lsHoraMax).Minute);
                if (liHoraAct > liValFin)
                {
                    if (Session["tipoPerfil"].ToString() == "N")
                    {
                        lblMensaje.Append("Está intentando modificar o eliminar información fuera del horario.");
                        blPuede = false;
                    }
                    else
                        hdfModificacion.Value = "S";
                }

                if (Convert.ToDateTime(lsFecha) != DateTime.Now.Date.AddDays(-1))
                {
                    if (Session["tipoPerfil"].ToString() == "N")
                    {
                        blPuede = false;
                        lblMensaje.Append("Está intentando modificar o eliminar información que no es del día anterior a la fecha del sistema.");
                    }
                    else
                        hdfModificacion.Value = "S";
                }

                if (blPuede)
                {
                    if (e.CommandName.Equals("Modificar"))
                    {
                        mdlregistroEnergiaSumLabel.InnerText = "Modificar";
                        Modificar(lCodigoRegistro);
                    }

                    // Evento Eliminar para los Participantes Modif Inf Operativa Participantes Req. 003-17 20170202
                    if (e.CommandName.Equals("Eliminar"))
                    {
                        string[] lsNombreParametros =
                        {
                            "@P_codigo_energia_suministrar", "@P_codigo_pozo", "@P_cantidad_a_suministrar", "@P_estado", "@P_accion"
                        }; //201061121 modif inf ope
                        SqlDbType[] lTipoparametros =
                        {
                            SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int
                        }; //201061121 modif inf ope
                        string[] lValorParametros = { lCodigoRegistro, "0", "0", "E", "3" }; //201061121 modif inf 

                        lConexion.Abrir();
                        if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaSum", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                        {
                            lblMensaje.Append("Se presentó un problema en la eliminación del registro.! " + goInfo.mensaje_error);
                            lConexion.Cerrar();
                        }
                        else
                        {
                            Toastr.Success(this, "Registro eliminado correctamente");
                            Listar();
                        }
                    }
                }
                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    Toastr.Warning(this, lblMensaje.ToString());
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
            ///////////////////////////////////////////////////////////////////////////////////
            //if (((LinkButton)e.CommandSource).Text == "Modificar")
            //{
            //    lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            //    //ddlPozo.Enabled = false;  20161121  modif inf ope 
            //    Modificar(lCodigoRegistro);
            //}
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia("t_energia_a_suminsitrar", lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// 20180122 rq003-18
        /// // 20180126 rq107-16
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceLlave) + "-" + lLector.GetValue(liIndiceDescripcion);
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='t_energia_a_suminsitrar' and llave_registro='codigo_energia_suministrar=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_energia_suministrar=" + lscodigo_registro;
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_energia_a_suminsitrar";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_energia_a_suminsitrar", lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] lValorParametros = { "0", "", "0", "0", "" };
            string lsParametros = "";

            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    lsParametros += " Código energía : " + TxtBusCodigo.Text; //20180126 rq107-16
                }
                if (TxtBusFecha.Text.Trim().Length > 0)
                {
                    lValorParametros[1] = TxtBusFecha.Text.Trim();
                    lsParametros += " Fecha Inicial : " + TxtBusFecha.Text;
                }
                else
                {
                    lValorParametros[1] = ""; //2018113 ajuste
                                              //lsParametros += " Fecha Inicial : " + TxtBusFecha.Text;  //2018113 ajuste
                }
                if (ddlBusOperador.SelectedValue != "0")
                {
                    lValorParametros[2] = ddlBusOperador.SelectedValue;
                    lsParametros += " Operador: " + ddlBusOperador.SelectedItem;
                }
                if (ddlBusPozo.SelectedValue != "0")
                {
                    lValorParametros[3] = ddlBusPozo.SelectedValue;
                    lsParametros += " Punto SNT: " + ddlBusPozo.SelectedItem;
                }
                if (TxtBusFechaF.Text.Trim().Length > 0)
                {
                    lValorParametros[4] = TxtBusFechaF.Text.Trim();
                    lsParametros += " Fecha Final : " + TxtBusFechaF.Text;
                }
                else
                {
                    if (TxtBusFecha.Text.Trim().Length > 0)
                    {
                        lValorParametros[4] = TxtBusFecha.Text.Trim();
                        lsParametros += " Fecha Final : " + TxtBusFecha.Text;
                    }
                    else
                    {
                        lValorParametros[4] = ""; //2018113 ajuste
                                                  //lsParametros += " Fecha Final : " + TxtBusFecha.Text;  //2018113 ajuste
                    }
                }
                //20220705
                if (lValorParametros[0] == "0" && lValorParametros[1] == "" && lValorParametros[3] == "0")
                {
                    lValorParametros[1] = DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd");
                    lValorParametros[4] = lValorParametros[1];
                }
                Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_getEnergiaSum&nombreParametros=@P_codigo_energia_suministrar*@P_fecha*@P_codigo_operador*@P_codigo_Pozo*@P_fecha_fin&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "*" + lValorParametros[3] + "*" + lValorParametros[4] + "&columnas=codigo_energia_suministrar*fecha*codigo_operador*nombre_operador*codigo_Pozo*desc_Pozo*cantidad_a_suministrar*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado de Energía a Suministrar&TituloParametros=" + lsParametros);
            }
            catch (Exception)
            {
                Toastr.Error(this, "No se pudo generar el informe.!");
            }
        }

        /// <summary>
        /// Nombre: ImgPdf_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgPdf_Click(object sender, EventArgs e)
        {
            try
            {
                string lsCondicion = " codigo_energia_suministrar <> '0'";
                Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=t_energia_a_suminsitrar&procedimiento=pa_ValidarExistencia&columnas=codigo_energia_suministrar*fecha*codigo_operador*codigo_Pozo*cantidad_a_suministrar*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
            }
            catch (Exception)
            {
                Toastr.Error(this, "No se pudo generar el informe.!");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_energia_suministrar", "@P_fecha", "@P_codigo_operador", "@P_codigo_Pozo", "@P_cantidad_a_suministrar", "@P_estado", "@P_accion", "@P_ingresa_snt" }; //20190306 rq013-17
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar }; //20190306 rq013-17
            string[] lValorParametros = { "0", "", "0", "0", "0", "", "1", ddlIngresa.SelectedValue }; //20190306 rq013-17
            var lblMensaje = new StringBuilder();
            int liValor = 0;
            try
            {
                if (TxtFecha.Text == "")
                    lblMensaje.Append("Debe digitar la fecha<br>");
                if (ddlOperador.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el operador<br>");
                if (ddlPozo.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el Punto SNT<br>");
                else //20220705
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_punto_operador", " tipo_punto= 'E' and codigo_punto =" + ddlPozo.SelectedValue + " and ( codigo_operador =0 or  codigo_operador= " + ddlOperador.SelectedValue + ") and estado ='A'", goInfo))
                        lblMensaje.Append("El punto no está asociado al operador<br>");
                }
                // 20161121 modif inf ope
                if (VerificarExistencia("fecha = '" + TxtFecha.Text + "' and codigo_operador = " + ddlOperador.SelectedValue + " and codigo_pozo = " + ddlPozo.SelectedValue + " and estado <>'E'"))
                    lblMensaje.Append("Ya está declarada la energía a suministrar para la fecha, el operador y el punto<br>"); //20180126 rq107-16
                if (TxtCantidad.Text == "")
                    lblMensaje.Append("Debe digitar la cantidad a suministrar<br>");
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidad.Text.Trim());
                        if (liValor < 0)
                            lblMensaje.Append(" Valor Inválido en la cantidad a suministrar<br>");
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append(" Valor Inválido en la cantidad a suministrar<br>");
                    }
                }
                //20220705
                string lsError = "N";
                if (lblMensaje.ToString() == "")
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_punto_reporte_cnt", " codigo_reporte = 6 and codigo_punto =" + ddlPozo.SelectedValue + " and estado ='A'", goInfo))
                        lblMensaje.Append(" El punto no tiene definida la cantidad máxima<br>");
                    else
                    {
                        if (validaCantidad())
                        {
                            lsError = "S";
                        }
                    }
                }
                if (lsError == "S" && lblMensaje.ToString() == "")
                {
                    Modal.Abrir(this, mdlConfirma.ID, mdlConfirmaInside.ID);
                }
                //20220705 fin
                if (string.IsNullOrEmpty(lblMensaje.ToString()) && lsError == "N")//20220705
                {
                    lValorParametros[1] = TxtFecha.Text;
                    lValorParametros[2] = ddlOperador.SelectedValue;
                    lValorParametros[3] = ddlPozo.SelectedValue;
                    lValorParametros[4] = TxtCantidad.Text;
                    lValorParametros[5] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaSum", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        Toastr.Error(this, "Se presentó un problema en la creación de la energía a suministrar.!");
                        lConexion.Cerrar();
                    }
                    else
                    {
                        //TxtBusFecha.Text = TxtFecha.Text; //20160711 ajuste msificaciones //20220621
                                                          //Se notifica a el usuario que el registro fue realizado de manera exitosa
                        Toastr.Success(this, "Se realizo el registro de forma exitosa.!");
                        //Cierra el modal de Agregar
                        Modal.Cerrar(this, registroEnergiaSum.ID);
                        Listar();
                    }
                }
                else
                {
                    if (lblMensaje.ToString() != "") //20220705
                        Toastr.Warning(this, lblMensaje.ToString());
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void Cancel_OnClick(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (mdlregistroEnergiaSumLabel.InnerText.Equals("Modificar") && LblCodigoCap.Text != "")
                manejo_bloqueo("E", LblCodigoCap.Text);

            //Cierra el modal de Agregar
            Modal.Cerrar(this, registroEnergiaSum.ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbActualiza_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_energia_suministrar","@P_codigo_pozo", "@P_cantidad_a_suministrar", "@P_estado", "@P_accion",  //201061121 modif inf ope
                "@P_modificacion" , "@P_ingresa_snt" }; // Campo Nuevo Req. 003-17  20170216 //20190306 rq013-17
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int,  //201061121 modif inf ope
                SqlDbType.VarChar , SqlDbType.VarChar   }; // Campo Nuevo Req. 003-17  20170216 //20190306 rq013-17
            string[] lValorParametros = { "0", "0", "", "", "2",  //201061121 modif inf ope
                hdfModificacion.Value , ddlIngresa.SelectedValue }; // Campo Nuevo Req. 003-17  20170216 //20190306 rq013-17
            var lblMensaje = new StringBuilder();
            int liValor = 0;
            try
            {
                if (TxtCantidad.Text == "")
                    lblMensaje.Append(" Debe digitar la energía a suministrar en el pozo <br>");//20180126 rq107-16
                else
                {
                    // 20161121 modif inf ope
                    if (ddlPozo.SelectedValue == "0")
                        lblMensaje.Append("Debe seleccionar el Punto SNT<br>");
                    // 20161121 modif inf ope
                    if (VerificarExistencia("fecha = '" + TxtFecha.Text + "' and codigo_operador = " + ddlOperador.SelectedValue + " and codigo_pozo = " + ddlPozo.SelectedValue + " and codigo_energia_suministrar <>" + LblCodigoCap.Text + " and estado <>'E'"))
                        lblMensaje.Append(" Ya está declarada la enegía a suministrar para la fecha, el operador y el punto<br>");
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidad.Text.Trim());
                        if (liValor < 0)
                            lblMensaje.Append(" Valor inválido en la cantidad a suministrar<br>");
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append(" Valor Inválido en la cantidad a suministrar<br>");
                    }
                }
                //20220705
                string lsError = "N";
                if (lblMensaje.ToString() == "")
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_punto_reporte_cnt", " codigo_reporte = 6 and codigo_punto =" + ddlPozo.SelectedValue + " and estado ='A'", goInfo))
                        lblMensaje.Append(" El punto no tiene definida la cantidad máxima<br>");
                    else
                    {
                        if (validaCantidad())
                        {
                            lsError = "S";
                        }
                    }
                }
                if (lsError == "S" && lblMensaje.ToString() == "")
                {
                    Modal.Abrir(this, mdlConfirma.ID, mdlConfirmaInside.ID);
                }
                //20220705 fin

                if (string.IsNullOrEmpty(lblMensaje.ToString()) && lsError == "N")//20220705
                {

                    lValorParametros[0] = LblCodigoCap.Text;
                    lValorParametros[1] = ddlPozo.SelectedValue; //20161121 modif inf ope
                    lValorParametros[2] = TxtCantidad.Text; //20161121 modif inf ope
                    lValorParametros[3] = ddlEstado.SelectedValue; //20161121 modif inf ope
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaSum", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        Toastr.Error(this, "Se presentó un Problema en la actualización de la energía suministrada.!");
                        lConexion.Cerrar();
                    }
                    else
                    {
                        manejo_bloqueo("E", LblCodigoCap.Text);
                        //Se notifica a el usuario que el registro fue actualizado de manera exitosa
                        Toastr.Success(this, "Se realizo la actualización de forma exitosa.!");
                        //Cierra el modal de Agregar
                        Modal.Cerrar(this, registroEnergiaSum.ID);
                        Listar();
                    }
                }
                else
                {
                    if (lblMensaje.ToString() != "") //20220705
                        Toastr.Warning(this, lblMensaje.ToString());
                }
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                manejo_bloqueo("E", LblCodigoCap.Text);
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            if (TxtBusCodigo.Text.Trim().Length <= 0 && TxtBusFecha.Text.Trim().Length <= 0 && ddlBusOperador.SelectedValue == "0" && ddlBusPozo.SelectedValue == "0")
                Toastr.Warning(this, "Debe seleccionar al menos un parámetro para realizar la búsqueda!.");
            else
            {
                Listar();
            }
        }

        /// <summary>
        /// Evento que controla el boton nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNuevo(object sender, EventArgs e)
        {
            mdlregistroEnergiaSumLabel.InnerText = "Agregar";
            int liHoraAct = 0;
            string lsHoraMax = "0";
            int liValFin = 0;
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
            if (lLector.HasRows)
            {
                lLector.Read();
                lsHoraMax = lLector["hora_max_declara_opera"].ToString();
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
            liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
            liValFin = ((Convert.ToDateTime(lsHoraMax).Hour * 60) + Convert.ToDateTime(lsHoraMax).Minute);
            if (liHoraAct > liValFin)
            {
                Toastr.Warning(this, "Está intentando Registrar Información Fuera del Horario.");
                //Cierra el modal de Agregar
                Modal.Cerrar(this, registroEnergiaSum.ID);
            }
            else
            {
                LblCodigoCap.Text = "Automatico";
                ddlEstado.SelectedValue = "A";  //20161121 modif inf ope
                ddlPozo.SelectedIndex = 0;
                TxtCantidad.Text = string.Empty;
                ddlIngresa.SelectedIndex = 0;

                ddlPozo.Enabled = true;
                imbCrear.Visible = true;
                imbActualiza.Visible = false;
                TxtCodigoCap.Visible = false;
                TxtCodigoCap.Text = "0"; //20220705
                LblCodigoCap.Visible = true;
                ddlEstado.Enabled = false; //20161121 modif inf ope
                TxtCantidad.Enabled = true;
                ddlIngresa.Enabled = true;

                //Abre el modal de Agregar
                Modal.Abrir(this, registroEnergiaSum.ID, registroEnergiaSumInside.ID);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20220705
        protected void BtnAceptarConf_Click(object sender, EventArgs e)
        {
            Modal.Cerrar(this, mdlConfirma.ID);
            string[] lsNombreParametros = { "@P_codigo_energia_suministrar", "@P_fecha", "@P_codigo_operador", "@P_codigo_Pozo", "@P_cantidad_a_suministrar", "@P_estado", "@P_accion", "@P_ingresa_snt", "@P_dato_atipico" }; //20190306 rq013-17
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar }; //20190306 rq013-17
            string[] lValorParametros = { "0", "", "0", "0", "0", "", "1", ddlIngresa.SelectedValue, "S" }; 

            if (TxtCodigoCap.Text != "0")
            {
                lValorParametros[0] = TxtCodigoCap.Text;
                lValorParametros[6] = "2";
            }
            lValorParametros[1] = TxtFecha.Text;
            lValorParametros[2] = ddlOperador.SelectedValue;
            lValorParametros[3] = ddlPozo.SelectedValue;
            lValorParametros[4] = TxtCantidad.Text;
            lValorParametros[5] = ddlEstado.SelectedValue;
            lConexion.Abrir();
            if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaSum", lsNombreParametros, lTipoparametros, lValorParametros))
            {
                Toastr.Error(this, "Se presentó un problema en la creación de la energía a suministrar.!");
                lConexion.Cerrar();
            }
            else
            {
                //TxtBusFecha.Text = TxtFecha.Text; //20160711 ajuste msificaciones //20220621
                //Se notifica a el usuario que el registro fue realizado de manera exitosa
                Toastr.Success(this, "Se realizo el registro de forma exitosa.!");
                //Cierra el modal de Agregar
                Modal.Cerrar(this, registroEnergiaSum.ID);
                Listar();
                if (TxtCodigoCap.Text != "0")
                    manejo_bloqueo("E", TxtCodigoCap.Text);
            }
        }
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool validaCantidad()
        {
            bool lbValida = false;
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_punto_reporte_cnt", " codigo_reporte = 6 and codigo_punto =" + ddlPozo.SelectedValue + " and cantidad_maxima < " + TxtCantidad.Text + "  and estado ='A'");
            if (lLector.HasRows)
            {
                lLector.Read();
                lblMensajeCOnf.Text = "La cantidad de energía a registrar de " + Convert.ToInt32(TxtCantidad.Text).ToString("###,###,###,###") + " MBTU supera el valor máximo histórico parametrizado para el punto del SNT " + ddlPozo.SelectedItem.ToString() + " que es de " + Convert.ToInt32(lLector["cantidad_maxima"].ToString()).ToString("###,###,###,###") + " MBTU";
                lbValida = true;
            }
            lLector.Close();
            lConexion.Cerrar();
            return lbValida;
        }

    }
}