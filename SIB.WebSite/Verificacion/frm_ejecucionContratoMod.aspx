﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ejecucionContratoMod.aspx.cs"
    Inherits="Verificacion_frm_ejecucionContratoMod" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <div>
            <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
                width="100%">
                <tr>
                    <td align="center" class="th1" style="width: 80%;">
                        <asp:Label ID="lblTitulo" runat="server" ForeColor="White" ></asp:Label>
                    </td>
                    <td align="center" class="th1" style="width: 20%;">
                        <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/exel 2007 3D.png"
                            Height="40" OnClick="imbExcel_Click" />
                    </td>
                </tr>
            </table>
            <br /><br /><br /><br /><br /><br />
            <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
                width="100%">
                <tr>
                    <td class="td1">Número Contrato
                    </td>
                    <td class="td2">
                        <asp:TextBox ID="TxtContratoDef" runat="server" ValidationGroup="detalle" Width="200px" MaxLength="30"></asp:TextBox>-
                    </td>
                    <td class="td1">Número Operación
                    </td>
                    <td class="td2">
                        <asp:TextBox ID="TxtNoContrato" runat="server" ValidationGroup="detalle" Width="80px"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="ftebTxtNoContrato" runat="server" TargetControlID="TxtNoContrato"
                            FilterType="Custom, Numbers">
                        </cc1:FilteredTextBoxExtender>
                    </td>
                    <td class="td1">Código Ejecución
                    </td>
                    <td class="td2">
                        <asp:TextBox ID="TxtCodEjec" runat="server" ValidationGroup="detalle" Width="80px"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="ftebTxtCodEjec" runat="server" TargetControlID="TxtCodEjec"
                            FilterType="Custom, Numbers">
                        </cc1:FilteredTextBoxExtender>
                    </td>
                    <td class="td1">Id Registro
                    </td>
                    <td class="td2">
                        <asp:TextBox ID="TxtCodigoVerif" runat="server" ValidationGroup="detalle" Width="80px"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="ftebTxtCodigoVerif" runat="server" TargetControlID="TxtCodigoVerif"
                            FilterType="Custom, Numbers">
                        </cc1:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td class="td1">Fecha Registro Ejecución
                    </td>
                    <td class="td2">
                        <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        -
                        <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    </td>
                    <td class="td1">Operador
                    </td>
                    <td class="td2">
                        <asp:DropDownList ID="ddlOperador" runat="server" Width="180px">
                        </asp:DropDownList>
                    </td>
                    <td class="td1">Punta
                    </td>
                    <td class="td2">
                        <asp:DropDownList ID="ddlPunta" runat="server" Width="180px">
                            <asp:ListItem Value="0">Seleccione</asp:ListItem>
                            <asp:ListItem Value="C">Compra</asp:ListItem>
                            <asp:ListItem Value="V">Venta</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="td1">Estado
                    </td>
                    <td class="td2">
                        <asp:DropDownList ID="ddlEstado" runat="server" Width="180px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="th1" colspan="8" align="center">
                        <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                            OnClick="imbConsultar_Click" Height="40" />
                    </td>
                </tr>
            </table>
        </div>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblMensaje">
            <tr>
                <td colspan="3" align="center">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
            id="tblGrilla" visible="false">
            <tr>
                <td colspan="3" align="center">
                    <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False"
                        AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1"
                        OnEditCommand="dtgConsulta_EditCommand">
                        <Columns>
                            <%--0--%>
                            <asp:BoundColumn DataField="fecha_gas" HeaderText="Fecha Gas" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <%--1--%>
                            <asp:BoundColumn DataField="desc_subasta" HeaderText="Subasta"></asp:BoundColumn>
                            <%--2--%>
                            <asp:BoundColumn DataField="desc_mercado" HeaderText="Mercado"></asp:BoundColumn>
                            <%--3--%>
                            <asp:BoundColumn DataField="desc_producto" HeaderText="Producto"></asp:BoundColumn>
                            <%--4--%>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual"></asp:BoundColumn>
                            <%--5--%>
                            <asp:BoundColumn DataField="codigo_ejec_c" HeaderText="Código Ejecución Compra"></asp:BoundColumn>
                            <%--6--%>
                            <asp:BoundColumn DataField="codigo_ejec_v" HeaderText="Código Ejecución venta"></asp:BoundColumn>
                            <%--7--%>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--8--%>
                            <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato Definitivo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>

                            <%--9--%>
                            <asp:BoundColumn DataField="codigo_punto" HeaderText="Código Punto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--10--%>
                            <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--11--%>
                            <asp:BoundColumn DataField="operador_compra" HeaderText="Código Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--12--%>
                            <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Comprador" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--13--%>
                            <asp:BoundColumn DataField="operador_venta" HeaderText="Código Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--14--%>
                            <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--15--%>
                            <asp:BoundColumn DataField="cantidad_compra" HeaderText="Cantidad Compra" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <%--16--%>
                            <asp:BoundColumn DataField="cantidad_venta" HeaderText="Cantidad Venta" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <%--17--%>
                            <asp:BoundColumn DataField="Valor_compra" HeaderText="Valor Compra" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            <%--18--%>
                            <asp:BoundColumn DataField="Valor_venta" HeaderText="Valor Venta" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            <%--19--%>
                            <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20--%>
                            <asp:BoundColumn DataField="estado" Visible="false"></asp:BoundColumn>
                            <%--21--%>
                            <asp:BoundColumn DataField="cantidad" Visible="false"></asp:BoundColumn>
                            <%--22--%>
                            <asp:BoundColumn DataField="precio" Visible="false"></asp:BoundColumn>
                            <%--23--%>
                            <asp:BoundColumn DataField="codigo_verif" Visible="false"></asp:BoundColumn>
                            <%--24--%>
                            <asp:BoundColumn DataField="Ind_modificado" HeaderText="Modificado" Visible="false"></asp:BoundColumn>
                            <%--25--%>
                            <asp:BoundColumn DataField="fecha_modificacion" HeaderText="Fecha Modificación" Visible="false"></asp:BoundColumn>
                            <%--26--%>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar"></asp:EditCommandColumn>
                            <%--27--%>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
        <table id="tblRegCnt" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
            width="100%" visible="false">
            <tr>
                <td class="td1">Operación
                </td>
                <td class="td2">
                    <asp:Label ID="lblOperacion" runat="server"></asp:Label>
                    <asp:HiddenField ID="hdfCodigoEjecC" runat="server" />
                    <asp:HiddenField ID="hdfCodigoEjecV" runat="server" />
                    <asp:HiddenField ID="hdfCantidad" runat="server" />
                    <asp:HiddenField ID="hdfCodDatUsu" runat="server" />
                    <asp:HiddenField ID="hdfCodigoVerif" runat="server" />
                    <%--20190524 rq029-19--%>
                    <asp:HiddenField ID="hdfUltDia" runat="server" />
                    <asp:HiddenField ID="hdfHora" runat="server" />
                </td>
                <td class="td1">Número de contrato
                </td>
                <td class="td2">
                    <asp:Label ID="lblContratoDef" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="td1">Subasta
                </td>
                <td class="td2">
                    <asp:Label ID="lblSubasta" runat="server"></asp:Label>
                </td>
                <td class="td1">Tipo Mercado
                </td>
                <td class="td2">
                    <asp:Label ID="lblMercado" runat="server"></asp:Label>
                </td>

            </tr>
            <tr>
                <td class="td1">Producto
                </td>
                <td class="td2">
                    <asp:Label ID="lblProducto" runat="server"></asp:Label>
                </td>
                <td class="td1">Modalidad Contractual
                </td>
                <td class="td2">
                    <asp:Label ID="lblModalidad" runat="server"></asp:Label>
                </td>

            </tr>
            <tr>
                <td class="td1">Cantidad
                </td>
                <td class="td2">
                    <asp:Label ID="lblCantidad" runat="server"></asp:Label>
                </td>
                <td class="td1">Precio del Contrato
                </td>
                <td class="td2">
                    <asp:Label ID="lblPrecio" runat="server"></asp:Label>
                </td>

            </tr>
            <tr>
                <td class="td1">Fecha Gas
                </td>
                <td class="td2" colspan="3">
                    <asp:Label ID="lblFechaGas" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="td1">Punto de entrega
                </td>
                <td class="td2" colspan="3">
                    <asp:DropDownList ID="ddlPunto" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">Cantidad Autorizada
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtCantidad" runat="server" ValidationGroup="detalle" Width="100px" OnTextChanged="TxtCantidad_TextChanged"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="fteTxtCantidad" runat="server" TargetControlID="TxtCantidad"
                        FilterType="Custom, Numbers">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td class="td1">Valor Facturado
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtValor" runat="server" ValidationGroup="detalle" Width="100px"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="fteTxtValor" runat="server" TargetControlID="TxtValor"
                        FilterType="Custom, Numbers" ValidChars=".">
                    </cc1:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td class="td1">Observaciones 
                </td>
                <td class="td2" colspan="3">
                    <asp:TextBox ID="TxtObservacion" runat="server" ValidationGroup="detalle" Width="800px" MaxLength="1000" TextMode="MultiLine" Rows="3"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="th1" colspan="4" align="center">
                    <asp:Button ID="btnActualizar" runat="server" Text="Actualizar" OnClick="btnActualizar_Click"
                        OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                    <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" OnClick="btnEliminar_Click"
                        OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                    <asp:Button ID="btnRegresar" runat="server" Text="Regresar" OnClick="btnRegresar_Click" />
                    <asp:Button ID="btnCrearUsu" runat="server" Text="Crear Usuario" OnClick="btnCrearUsu_Click"
                        Visible="false" />
                    <asp:Button ID="btnActualUsu" runat="server" Text="Actualiza Usuario" OnClick="btnActualUsu_Click"
                        Visible="false" />
                </td>
            </tr>
        </table>
        <table id="tblDemanda" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
            width="90%" visible="false">
            <tr>
                <%--20190524 rq029-19--%>
                <td class="td1">Tipo Demanda
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlDemanda" runat="server" OnSelectedIndexChanged="ddlDemanda_SelectedIndexChanged"
                        AutoPostBack="true">
                    </asp:DropDownList>
                </td>
                <td class="td1">Sector de Consumo
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlSector" runat="server"></asp:DropDownList>
                </td>
                <td class="td1">Punto de Salida
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlSalida" runat="server" Width="120px"></asp:DropDownList>
                </td>
                <%--20190524  fin rq029-19--%>
                <td class="td1">Usuario Final
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtUsuarioFinal" runat="server" Width="120px"></asp:TextBox>
                    <cc1:AutoCompleteExtender runat="server" BehaviorID="AutoCompleteExUsuF" ID="AutoCompleteExtender1"
                        TargetControlID="TxtUsuarioFinal" ServicePath="~/WebService/AutoComplete.asmx"
                        ServiceMethod="GetCompletionListUsuarioFinal" MinimumPrefixLength="3" CompletionInterval="1000"
                        EnableCaching="true" CompletionSetCount="20" CompletionListCssClass="autocomplete_completionListElement"
                        CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                        DelimiterCharacters=";,:">
                        <Animations>
                        <OnShow>
                            <Sequence>
                                <%-- Make the completion list transparent and then show it --%>
                                <OpacityAction Opacity="0" />
                                <HideAction Visible="true" />
                                
                                <%--Cache the original size of the completion list the first time
                                    the animation is played and then set it to zero --%>
                                <ScriptAction Script="
                                    // Cache the size and setup the initial size
                                    var behavior = $find('AutoCompleteExUsuF');
                                    if (!behavior._height) {
                                        var target = behavior.get_completionList();
                                        behavior._height = target.offsetHeight - 2;
                                        target.style.height = '0px';
                                    }" />
                                
                                <%-- Expand from 0px to the appropriate size while fading in --%>
                                <Parallel Duration=".4">
                                    <FadeIn />
                                    <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteExUsuF')._height" />
                                </Parallel>
                            </Sequence>
                        </OnShow>
                        <OnHide>
                            <%-- Collapse down to 0px and fade out --%>
                            <Parallel Duration=".4">
                                <FadeOut />
                                <Length PropertyKey="height" StartValueScript="$find('AutoCompleteExUsuF')._height" EndValue="0" />
                            </Parallel>
                        </OnHide>
                        </Animations>
                    </cc1:AutoCompleteExtender>

                </td>
                <td class="td1">Mercado Relevante
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlMercado" runat="server" Width="130px">
                    </asp:DropDownList>
                </td>
                <%--20190524 rq029-19--%>
                <td class="td1">Cantidad Contratada
                </td>
                <%--20190524 rq029-19--%>
                <td class="td2">
                    <asp:TextBox ID="TxtCantidadUsr" runat="server" ValidationGroup="detalle" Width="100px"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="ftebTxtCantidadUsr" runat="server" TargetControlID="TxtCantidadUsr"
                        FilterType="Custom, Numbers">
                    </cc1:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td class="td2" colspan="10" align="center">
                    <b>LISTADO DE USUARIOS FINALES</b>
                </td>
            </tr>
            <tr>
                <%--20190524 rq029-19--%>
                <td class="td2" colspan="12" align="center">
                    <b>TOTAL CANTIDAD:</b><asp:Label ID="lblTotlCantidad" runat="server" ForeColor="Red"
                        Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="12" align="center">
                    <asp:DataGrid ID="dtgUsuarios" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                        AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1"
                        OnEditCommand="dtgUsuarios_EditCommand">
                        <Columns>
                            <%--0--%>
                            <asp:BoundColumn DataField="codigo_contrato_eje_usr" Visible="false"></asp:BoundColumn>
                            <%--20190524 rq029-19--%>
                            <%--1--%>
                            <asp:BoundColumn DataField="tipo_demanda" HeaderText="Código Tipo Demanda"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--2--%>
                            <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--3--%>
                            <asp:BoundColumn DataField="sector_consumo" HeaderText="Código Sector Consumo"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--4--%>
                            <asp:BoundColumn DataField="desc_sector" HeaderText="Sector consumo"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--5--%>
                            <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Código Punto Salida"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--6--%>
                            <asp:BoundColumn DataField="desc_salida" HeaderText="Punto de Salida"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20190524 fin  rq029-19--%>
                            <%--7--%>
                            <asp:BoundColumn DataField="nit_usuario_no_regulado" HeaderText="Identificacion Usuario Final"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--8--%>
                            <asp:BoundColumn DataField="nombre_usuario" HeaderText="Nombre Usuario"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--9--%>
                            <asp:BoundColumn DataField="codigo_mercado_relevante" HeaderText="Código Mercado Relevante" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="80px"></asp:BoundColumn>
                            <%--10--%>
                            <asp:BoundColumn DataField="desc_mercado" HeaderText="Mercado Relevante" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--20190524 rq029-19--%>
                            <%--11--%>
                            <asp:BoundColumn DataField="cantidad_contratada" HeaderText="Cantidad Contratada" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <%--12--%>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar"></asp:EditCommandColumn>
                            <%--13--%>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
        <br />
        <table id="tblDetalle" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
            width="90%" visible="false">
            <tr>
                <td class="td2" align="center">
                    <b>LISTADO PUNTOS INGRESADOS</b>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dtgDetalle" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                        AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <Columns>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Operación"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_gas" HeaderText="Fecha Gas" DataFormatString="{0:yyyy/MM/dd}"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="punta" HeaderText="Punta"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_Punto" HeaderText="Código Punto"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrtega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20190524 rq029-19--%>
                            <%--<asp:BoundColumn DataField="tipo_demanda" HeaderText="Código Tipo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="sector_consumo" HeaderText="Código Sector Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_sector" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>--%>
                            <%--20190524 fin rq029-19--%>
                            <asp:BoundColumn DataField="cantidad_autorizada" HeaderText="Cantidad Autorizada" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_facturado" HeaderText="Valor Facturado" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0.00}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
    </asp:Content>