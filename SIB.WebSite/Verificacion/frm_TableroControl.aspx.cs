﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Verificacion
{
    public partial class frm_TableroControl : Page
    {
        private InfoSessionVO goInfo = null;
        private static string lsTitulo = "Tablero de Control";
        private clConexion lConexion = null;
        private DataSet lds = new DataSet();
        private string sRutaArc = ConfigurationManager.AppSettings["rutaCarga"];

        /// <summary>
        /// 
        /// </summary>
        public string HdfExcel
        {
            get { return ViewState["HdfExcel"] != null && !string.IsNullOrEmpty(ViewState["HdfExcel"].ToString()) ? ViewState["HdfExcel"].ToString() : string.Empty; }
            set { ViewState["HdfExcel"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo;
            lConexion = new clConexion(goInfo);
            //20170601 rq020-17 tablero de control
            //Se agrega el comportamiento del botón
            buttons.ExportarExcelOnclick += imbExcel_Click;
            buttons.FiltrarOnclick += BtnBuscar_Click;
            if (!IsPostBack)
            {
                //Titulo
                Master.Titulo = "Informes";
                /// Llenar controles del Formulario
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' order by razon_social", 0, 1);
                lConexion.Cerrar();

                //Botones
                EnumBotones[] botones = { EnumBotones.Buscar };
                buttons.Inicializar(botones: botones);

                if (Session["tipoPerfil"].ToString() == "N")
                {
                    ddlOperador.SelectedValue = goInfo.cod_comisionista;
                    ddlOperador.Enabled = false;
                }
            }
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            HdfExcel = "0";
            string[] lsNombreParametros = { "@P_tipo_reporte", "@P_codigo_reporte", "@P_detalle", "@P_codigo_operador" };
            SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { ddlTipoRep.SelectedValue, "0", "R", ddlOperador.SelectedValue };  //20170601 rq020-17 tablero de control
            try
            {
                dtgInfOperativa.Visible = false;
                dtgInfTransaccional.Visible = false;
                lConexion.Abrir();
                if (ddlTipoRep.SelectedValue == "Z")
                {
                    dtgInfOperativa.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetConsTableroControl", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgInfOperativa.DataBind();
                    if (dtgInfOperativa.Items.Count > 0)
                    {
                        //tblGrilla.Visible = true;
                        dtgInfOperativa.Visible = true;
                        foreach (DataGridItem Grilla in this.dtgInfOperativa.Items)
                        {
                            if (Grilla.Cells[3].Text == "S")
                            {
                                Grilla.Cells[2].ForeColor = Color.Red;
                            }
                            //20170601 rq020-17
                            if (Grilla.Cells[3].Text == "N")
                            {
                                Grilla.Cells[2].ForeColor = Color.Green;
                                Grilla.Cells[2].Text = "Registros Completos";
                            }
                            //20170601 rq020-17
                            if (Grilla.Cells[3].Text == "X")
                            {
                                Grilla.Cells[2].ForeColor = Color.Green;
                                Grilla.Cells[2].Text = "Operador No Obligado a Declarar Información Operativa";
                            }

                        }
                    }
                    else
                    {
                        //tblGrilla.Visible = false;
                        Toastr.Info(this, "No se encontraron Registros.");
                    }
                }
                else
                {
                    dtgInfTransaccional.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetConsTableroControl", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgInfTransaccional.DataBind();
                    if (dtgInfTransaccional.Items.Count > 0)
                    {
                        //tblGrilla.Visible = true;
                        dtgInfTransaccional.Visible = true;
                        foreach (DataGridItem Grilla in this.dtgInfTransaccional.Items)
                        {
                            if (Grilla.Cells[2].Text != "0")
                            {
                                Grilla.Cells[3].ForeColor = Color.Red;
                            }
                            else
                            {
                                Grilla.Cells[3].ForeColor = Color.Green;
                                Grilla.Cells[3].Text = "Registros Completos";
                            }
                        }
                    }
                    else
                    {
                        //tblGrilla.Visible = false;
                        Toastr.Info(this, "No se encontraron Registros.");
                    }
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se Pude Generar el Informe.! " + ex.Message);
            }
        }

        /// <summary>
        /// Nombre: dtgInfOperativa_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgInfOperativa_EditCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals("RegistrosPendientes"))
            {
                try
                {
                    if (e.Item.Cells[3].Text == "S")
                    {
                        string[] lsNombreParametros = { "@P_tipo_reporte", "@P_codigo_reporte", "@P_detalle", "@P_codigo_operador" };
                        SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
                        string[] lValorParametros = { ddlTipoRep.SelectedValue, e.Item.Cells[0].Text, "D", ddlOperador.SelectedValue }; //20170601 rq020-17
                        lConexion.Abrir();
                        dtgInfOperativaDet.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetConsTableroControl", lsNombreParametros, lTipoparametros, lValorParametros);
                        dtgInfOperativaDet.DataBind();
                        lConexion.Cerrar();
                        Modal.Abrir(this, mdlRegistrosPendientes.ID, mdlRegistrosPendientesInside.ID);
                        dtgInfOperativaDet.Visible = true;
                        dtgInfTransaccionalDet.Visible = false;
                        lblInformacion.Text = "Tipo Información: " + ddlTipoRep.SelectedItem.ToString() + "  - Tipo Reporte: " + e.Item.Cells[1].Text;
                        HdfExcel = "1";
                    }
                    else
                        Toastr.Info(this, "No se permite el Detalle ya que no tiene Reportes Pendientes.");
                }
                catch (Exception ex)
                {
                }
            }
        }

        /// <summary>
        /// Nombre: dtgInfTransaccional_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgInfTransaccional_EditCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals("RegistrosPendientes"))
            {
                try
                {
                    string[] lsNombreParametros = { "@P_tipo_subasta", "@P_numero_contrato", "@P_fecha_contrato", "@P_mercado", "@P_codigo_producto",
                        "@P_operador_compra", "@P_operador_venta", "@P_estado", "@P_extemporaneo", "@P_tipo_perfil","@P_codigo_operador" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                        SqlDbType.Int,  SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
                    string[] lValorParametros = { "0", "0", "", "", "0", "0", "0", e.Item.Cells[0].Text, "0", Session["tipoPerfil"].ToString(), ddlOperador.SelectedValue };  //20170601 rq020-17
                    lConexion.Abrir();
                    dtgInfTransaccionalDet.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetConsRegistroContratos", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgInfTransaccionalDet.DataBind();
                    lConexion.Cerrar();
                    Modal.Abrir(this, mdlRegistrosPendientes.ID, mdlRegistrosPendientesInside.ID);
                    dtgInfOperativaDet.Visible = false;
                    dtgInfTransaccionalDet.Visible = true;
                    lblInformacion.Text = "Tipo Información: " + ddlTipoRep.SelectedItem.ToString() + "  -  Estado Registro: " + e.Item.Cells[1].Text;
                    HdfExcel = "2";
                }
                catch (Exception ex)
                {
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnBuscar_Click(object sender, EventArgs e)
        {
            dtgInfOperativa.Visible = false;
            dtgInfTransaccional.Visible = false;
            dtgInfOperativaDet.Visible = false;
            dtgInfTransaccionalDet.Visible = false;
            CargarDatos();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            //tblDatos.Visible = true;
            //tblGrilla.Visible = true;
            //TblDetalle.Visible = false;
            dtgInfOperativaDet.Visible = false;
            dtgInfTransaccionalDet.Visible = false;
            HdfExcel = "0";
            CargarDatos();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbExcel_Click(object sender, EventArgs e)
        {
            if (HdfExcel == "0")
                Toastr.Error(this, "No se puede Generar el Informe Ya que no esta visualizando Detalle de Información.");
            else
            {
                try
                {

                    string lsNombreArchivo = Session["login"] + "InfExcelReg" + DateTime.Now + ".xls";
                    StringBuilder lsb = new StringBuilder();
                    StringWriter lsw = new StringWriter(lsb);
                    HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                    Page lpagina = new Page();
                    HtmlForm lform = new HtmlForm();
                    lpagina.EnableEventValidation = false;
                    lpagina.Controls.Add(lform);
                    if (HdfExcel == "1")
                    {
                        dtgInfOperativaDet.EnableViewState = false;
                        dtgInfOperativaDet.Visible = true;
                        lform.Controls.Add(dtgInfOperativaDet);
                    }
                    if (HdfExcel == "2")
                    {
                        dtgInfTransaccionalDet.EnableViewState = false;
                        dtgInfTransaccionalDet.Visible = true;
                        lform.Controls.Add(dtgInfTransaccionalDet);
                    }
                    lpagina.RenderControl(lhtw);
                    Response.Clear();

                    Response.Buffer = true;
                    Response.ContentType = "aplication/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                    Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                    Response.ContentEncoding = System.Text.Encoding.Default;

                    Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                    Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
                    Response.Write("<table><tr><th colspan='5' align='left'><font face=Arial size=4>" + "Informe " + lblInformacion.Text + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                    Response.Write(lsb.ToString());
                    dtgInfOperativaDet.Visible = false;
                    Response.End();
                    Response.Flush();
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, "Problemas al Consultar los Contratos.");
                }
            }
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// 20170601 rq020-17 tablero de control
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();

                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"] + "-" + lLector["razon_social"];
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Evento que cierra el modal de Registros pendientes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCancelar_Click(object sender, EventArgs e)
        {
            Modal.Cerrar(this, mdlRegistrosPendientes.ID);
        }
    }
}