﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_AutEjecContExt.aspx.cs"
    Inherits="Verificacion_frm_AutEjecContExt" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
            width="90%">
            <tr>
                <td align="center" class="th1">
                    <asp:Label ID="lblTitulo" runat="server" ForeColor="White" ></asp:Label>
                </td>
            </tr>
        </table>
    <br /><br /><br /><br /><br /><br />
        <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
            width="90%">
            <tr>
                <td class="td1">Operador
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlOperador" runat="server" Width="200px">
                    </asp:DropDownList>
                </td>
                <td class="td1">Estado
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlEstado" runat="server">
                        <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                        <asp:ListItem Value="S" Text="Autorizado"></asp:ListItem>
                        <asp:ListItem Value="N" Text="No Autorizado"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>


        <table border="0" align="center" cellpadding="3" cellspacing="2" width="95%" runat="server"
            id="tblMensaje">
            <tr>
                <td class="th1" colspan="3" align="center" valign="top">

                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click"
                        valign="top" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnAprobar" runat="server" Text="Autorizar" OnClientClick="return confirmar();"
                    OnClick="btnAprobar_Click" Visible="false" valign="top" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnRechazar" runat="server" Text="Des-Autorizar" OnClientClick="return confirmar1();"
                    OnClick="btnRechazar_Click" Visible="false" valign="top" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" ImageAlign="Top" />
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblGrilla" visible="false">
            <tr>
                <td colspan="3" align="center">

                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Sele.">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ChkTodos" runat="server" OnCheckedChanged="ChkTodos_CheckedChanged"
                                        AutoPostBack="true" Text="Seleccionar Todos" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkRecibir" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Cd Ope" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ind_autorizado" HeaderText="Autorizado" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>

                </td>
            </tr>
        </table>
</asp:Content>