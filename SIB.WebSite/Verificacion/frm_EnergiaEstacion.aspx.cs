﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Verificacion_frm_EnergiaEstacion : Page
{
    private InfoSessionVO goInfo;
    private static string lsTitulo = "Energía Estación de Compresión"; //20160810 modalidad por tipo campo 
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    private clConexion lConexion;
    private clConexion lConexion1;
    private SqlDataReader lLector;
    private SqlDataReader lLector1;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        //EstablecerPermisosSistema(); //20210621 ajuste
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);
        //Titulo
        Master.Titulo = "Registros Operativos";
        /// Activacion de los Botones
        buttons.Inicializar("t_energia_estacion");
        buttons.CrearOnclick += btnNuevo;

        buttons.FiltrarOnclick += btnConsultar_Click;
        buttons.ExportarExcelOnclick += ImgExcel_Click;
        buttons.ExportarPdfOnclick += ImgPdf_Click;

        if (IsPostBack) return;

        // Carga informacion de combos
        lConexion.Abrir();
        //20220705
        if (goInfo.cod_comisionista != "0")
        {
            LlenarControles(lConexion.gObjConexion, ddlEstacion, "m_estacion_compresion poz, m_punto_operador ope", " poz.estado ='A' and poz.codigo_estacion = ope.codigo_punto and ope.tipo_punto ='C' and (ope.codigo_operador =0 or ope.codigo_operador =" + goInfo.cod_comisionista + ")  and ope.estado='A'  order by poz.descripcion", 0, 1); //20161108 control inf ope //20220202 verifica operativa //20220705
            LlenarControles(lConexion.gObjConexion, ddlBusEstacion, "m_estacion_compresion poz, m_punto_operador ope", " poz.estado ='A' and poz.codigo_estacion = ope.codigo_punto and ope.tipo_punto ='C' and (ope.codigo_operador =0 or ope.codigo_operador =" + goInfo.cod_comisionista + ")  and ope.estado='A'  order by poz.descripcion", 0, 1); //20161108 control inf ope //20220202 verifica operativa //20220705
        }
        else
        {
            LlenarControles(lConexion.gObjConexion, ddlEstacion, "m_estacion_compresion", " estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlBusEstacion, "m_estacion_compresion", " estado ='A' order by descripcion", 0, 1);
        }
        LlenarControles1(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' order by razon_social", 0, 4);
        LlenarControles1(lConexion.gObjConexion, ddlBusOperador, "m_operador", " estado = 'A' order by razon_social", 0, 4);
        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_no_ing_inf_ope", " codigo_reporte = 11 and codigo_operador =" + goInfo.cod_comisionista + " and estado ='A' ");
        if (lLector.HasRows)
        {
            lblMensaje.Text = "El Operador no está obligado a ingresar la información operativa de energía de estación de compresión.";
        }
        lLector.Close();
        lLector.Dispose();

        lConexion.Cerrar();
        if (Session["tipoPerfil"].ToString() == "N")
        {
            ddlBusOperador.SelectedValue = goInfo.cod_comisionista;
            ddlBusOperador.Enabled = false;
            ddlOperador.SelectedValue = goInfo.cod_comisionista;
            ddlOperador.Enabled = false;
            hdnELiminado.Value = "N";
        }
        else
            hdnELiminado.Value = "S";

        DateTime ldfecha;
        ldfecha = DateTime.Now.AddDays(-1);
        TxtFecha.Text = ldfecha.Year.ToString() + "/" + ldfecha.Month.ToString() + "/" + ldfecha.Day.ToString();
        TxtFecha.Enabled = false;
        //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
        Buscar();
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "t_energia_estacion");
        //hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        //hlkListar.Enabled = (Boolean)permisos["SELECT"];
        //hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        //20220621 ajsute
        if ((bool)permisos["UPDATE"] && (bool)permisos["DELETE"])
            dtgMaestro.Columns[12].Visible = true;//20220705
        else
        {
            if (!(bool)permisos["UPDATE"] && !(bool)permisos["DELETE"])
                dtgMaestro.Columns[12].Visible = false;//20220705
            else
            {
                if (!(bool)permisos["UPDATE"])
                {
                    foreach (DataGridItem Grilla in dtgMaestro.Items)
                    {
                        var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                        lkbModificar.Visible = false;
                    }
                }
                if (!(bool)permisos["DELETE"])
                {
                    foreach (DataGridItem Grilla in dtgMaestro.Items)
                    {
                        var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
                        lkbEliminar.Visible = false;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        int liHoraAct = 0;
        string lsHoraMax = "0";
        int liValFin = 0;
        lConexion.Abrir();
        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
        if (lLector.HasRows)
        {
            lLector.Read();
            lsHoraMax = lLector["hora_max_declara_opera"].ToString();
        }
        lLector.Close();
        lLector.Dispose();
        lConexion.Cerrar();
        liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
        liValFin = ((Convert.ToDateTime(lsHoraMax).Hour * 60) + Convert.ToDateTime(lsHoraMax).Minute);
        if (liHoraAct > liValFin)
        {
            Toastr.Warning(this, "Está intentando Registrar Información Fuera del Horario.");

            //Cierra el modal de Agregar
            Modal.Cerrar(this, registroEnergiaEstacion.ID);
        }
        else
        {
            //Abre el modal de Agregar
            Modal.Abrir(this, registroEnergiaEstacion.ID, registroEnergiaEstacionInside.ID);

            imbCrear.Visible = true;
            imbActualiza.Visible = false;
            lblTitulo.Text = "Crear " + lsTitulo;
            TxtCodigoCap.Visible = false;
            LblCodigoCap.Visible = true;
            TxtCodigoCap.Text = "0"; //20220705
            LblCodigoCap.Text = "Automático";
            ddlEstado.SelectedValue = "A";  //20161121 modif inf ope
            ddlEstado.Enabled = false; //20161121 modif inf ope
        }
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_energia_estacion", " codigo_energia_estacion= " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoCap.Text = lLector["codigo_energia_estacion"].ToString();
                        TxtCodigoCap.Text = lLector["codigo_energia_estacion"].ToString();
                        DateTime ldFecha; //20160711 ajuste msificaciones
                        ldFecha = Convert.ToDateTime(lLector["fecha"].ToString()); //20160711 ajuste msificaciones
                        TxtFecha.Text = ldFecha.Year.ToString() + "/" + ldFecha.Month.ToString() + "/" + ldFecha.Day.ToString(); //20160711 ajuste msificaciones

                        try
                        {
                            ddlOperador.SelectedValue = lLector["codigo_Operador"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El Operador del registro no existe o está inactivo<br>";
                        }
                        try
                        {
                            ddlEstacion.SelectedValue = lLector["codigo_estacion"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "La estación de compresión del registro no existe o está inactiva<br>";
                        }
                        ddlOperacion.SelectedValue = lLector["tipo_operacion"].ToString();
                        TxtCantidad.Text = lLector["cantidad"].ToString();
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoCap.Visible = false;
                        LblCodigoCap.Visible = true;
                        TxtFecha.Enabled = false;
                        ddlOperador.Enabled = false;
                        ddlEstado.Enabled = true; //20161121 modif inf ope

                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    if (lblMensaje.Text == "") //20160809 carga ptdv
                        manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que está Bloqueado. Código Energía " + modificar.ToString(); //20180126 rq107-16

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            //Abre el modal de Agregar
            Modal.Abrir(this, registroEnergiaEstacion.ID, registroEnergiaEstacionInside.ID);
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_energia_estacion", "@P_fecha_ini", "@P_fecha_fin", "@P_codigo_operador", "@P_codigo_estacion" }; //20160721 modif inf opertiva y trans
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int }; //20160721 modif inf opertiva y trans
        string[] lValorParametros = { "0", "", "", "0", "0" }; //20160721 modif inf opertiva y trans
        DateTime ldFecha;
        lblMensaje.Text = "";

        try
        {
            if (TxtBusFecha.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtBusFecha.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Fecha Inicial. <br>";
                }
            }
            if (TxtBusFechaF.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtBusFechaF.Text.Trim());
                    if (TxtBusFecha.Text.Trim().Length > 0)
                    {
                        if (ldFecha < Convert.ToDateTime(TxtBusFecha.Text.Trim()))
                            lblMensaje.Text += "La Fecha final NO puede ser menor que la Fecha Inicial.!<br>";
                    }
                    else
                        lblMensaje.Text += "Debe seleccionar la fecha inicial antes que la final.!<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Fecha Final. <br>";
                }
            }
            if (lblMensaje.Text == "")
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                if (TxtBusFecha.Text.Trim().Length > 0)
                    lValorParametros[1] = TxtBusFecha.Text.Trim();
                else
                    if (TxtBusCodigo.Text.Trim().Length == 0)
                    lValorParametros[1] = TxtFecha.Text.Trim();
                if (TxtBusFechaF.Text.Trim().Length > 0)
                    lValorParametros[2] = TxtBusFechaF.Text.Trim();
                else
                {
                    if (TxtBusFecha.Text.Trim().Length > 0)
                        lValorParametros[2] = TxtBusFecha.Text.Trim();
                    else
                        if (TxtBusCodigo.Text.Trim().Length == 0)
                        lValorParametros[2] = TxtFecha.Text.Trim();
                }
                if (ddlBusOperador.SelectedValue != "0")
                    lValorParametros[3] = ddlBusOperador.SelectedValue;
                if (ddlBusEstacion.SelectedValue != "0")
                    lValorParametros[4] = ddlBusEstacion.SelectedValue;
                lConexion.Abrir();
                dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetEnergiaEstacion", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgMaestro.DataBind();
                lConexion.Cerrar();
                EstablecerPermisosSistema();//20220621 ajuste
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
        if (lblMensaje.Text != "")
        {
            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        string lsFecha = ""; // Cambio Req. 003-17 20170216
        hdfModificacion.Value = "N"; // Cambio Req. 003-17 20170216
        lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text; // Cambio Req. 003-17 20170216
        lsFecha = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[1].Text; // Cambio Req. 003-17 20170216
        ///////////////////////////////////////////////////////////////////////////////////
        ///// Control Nuevo para Modif Inf Operativa Participantes Req. 003-17 20170216 ///
        ///////////////////////////////////////////////////////////////////////////////////
        bool blPuede = true;
        int liHoraAct = 0;
        string lsHoraMax = "0";
        int liValFin = 0;
        lConexion.Abrir();
        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
        if (lLector.HasRows)
        {
            lLector.Read();
            lsHoraMax = lLector["hora_max_declara_opera"].ToString();
        }
        lLector.Close();
        lLector.Dispose();
        lConexion.Cerrar();
        liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
        liValFin = ((Convert.ToDateTime(lsHoraMax).Hour * 60) + Convert.ToDateTime(lsHoraMax).Minute);
        if (liHoraAct > liValFin)
        {
            if (Session["tipoPerfil"].ToString() == "N")
            {

                Toastr.Warning(this, "Está intentando modificar o eliminar información fuera del horario." + "');");

                blPuede = false;
            }
            else
                hdfModificacion.Value = "S";
        }
        if (Convert.ToDateTime(lsFecha) != DateTime.Now.Date.AddDays(-1))
        {
            if (Session["tipoPerfil"].ToString() == "N")
            {
                blPuede = false;

                Toastr.Warning(this, "Está intentando modificar o eliminar información que no es del día anterior a la fecha del sistema." + "');");
            }
            else
                hdfModificacion.Value = "S";
        }
        if (blPuede)
        {
            if (e.CommandName.Equals("Modificar"))
            {
                mdlRegistroEnergiaEstacionLabel.InnerText = "Modificar";
                Modificar(lCodigoRegistro);
            }
            // Evento Eliminar para los Participantes Modif Inf Operativa Participantes Req. 003-17 20170202
            if (e.CommandName.Equals("Eliminar"))
            {
                string[] lsNombreParametros = { "@P_codigo_energia_estacion", "@P_estado", "@P_accion" }; //201061121 modif inf ope
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int }; //201061121 modif inf ope
                string[] lValorParametros = { lCodigoRegistro, "E", "3" }; //201061121 modif inf ope
                lblMensaje.Text = "";
                try
                {
                    if (lblMensaje.Text == "")
                    {
                        lConexion.Abrir();
                        if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaEstacion", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                        {
                            lblMensaje.Text = "Se presentó un Problema en la Eliminación del Registro.! " + goInfo.mensaje_error.ToString();
                            lConexion.Cerrar();
                        }
                        else
                        {
                            Toastr.Success(this, "Registro Eliminado Correctamente");

                            Listar();
                        }
                    }
                }
                catch (Exception ex)
                {
                    /// Desbloquea el Registro Actualizado
                    lblMensaje.Text = ex.Message;
                }
                if (lblMensaje.Text != "")
                {
                    Toastr.Warning(this, lblMensaje.Text);
                    lblMensaje.Text = "";
                }
            }
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("t_energia_estacion", lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }

    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    /// // 20180126 rq107-16
    protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector["codigo_operador"].ToString();
            lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }



    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='t_energia_estacion' and llave_registro='codigo_energia_estacion=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_energia_estacion=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "t_energia_estacion";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_energia_estacion", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, EventArgs e)
    {
        string[] lValorParametros = { "0", "", "", "0", "0", "" };
        string lsParametros = "";

        try
        {
            if (TxtBusCodigo.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusCodigo.Text.Trim();
                lsParametros += " Código energía : " + TxtBusCodigo.Text;
            }
            if (TxtBusFecha.Text.Trim().Length > 0)
            {
                lValorParametros[1] = TxtBusFecha.Text.Trim();
                lsParametros += " Fecha Inicial : " + TxtBusFecha.Text;
            }
            else
            {
                if (TxtBusCodigo.Text.Trim().Length == 0)
                {
                    lValorParametros[1] = TxtFecha.Text.Trim();
                    lsParametros += " Fecha Inicial : " + TxtFecha.Text;
                }
            }
            if (TxtBusFechaF.Text.Trim().Length > 0)
            {
                lValorParametros[2] = TxtBusFechaF.Text.Trim();
                lsParametros += " Fecha Final : " + TxtBusFechaF.Text;
            }
            else
            {
                if (TxtBusFecha.Text.Trim().Length > 0)
                {
                    lValorParametros[2] = TxtBusFecha.Text.Trim();
                    lsParametros += " Fecha Final : " + TxtBusFecha.Text;
                }
                else
                {
                    if (TxtBusCodigo.Text.Trim().Length == 0)
                    {
                        lValorParametros[2] = TxtFecha.Text.Trim();
                        lsParametros += " Fecha Final : " + TxtBusFecha.Text;
                    }
                }
            }

            if (ddlBusOperador.SelectedValue != "0")
            {
                lValorParametros[3] = ddlBusOperador.SelectedValue;
                lsParametros += " Operador: " + ddlBusOperador.SelectedItem;
            }
            if (ddlBusEstacion.SelectedValue != "0")
            {
                lValorParametros[4] = ddlBusEstacion.SelectedValue;
                lsParametros += " Estación compresión: " + ddlBusEstacion.SelectedItem;
            }
            //20220705
            if (lValorParametros[0] == "0" && lValorParametros[1] == "" && lValorParametros[4] == "0")
            {
                lValorParametros[1] = TxtFecha.Text;
                lValorParametros[2] = TxtFecha.Text;
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_getEnergiaEstacion&nombreParametros=@P_codigo_energia_estacion*@P_fecha_ini*@P_fecha_fin*@P_codigo_operador*@P_codigo_estacion&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "*" + lValorParametros[3] + "*" + lValorParametros[4] + "&columnas=codigo_energia_estacion*fecha*codigo_operador*nombre_operador*codigo_estacion*¨desc_estacion*cantidad*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado de Energía estación de compresión&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }
        if (lblMensaje.Text != "")
        {
            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, EventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_energia_estacion <> '0'";
            if (TxtBusCodigo.Text.Trim().Length > 0)
            {
                lsCondicion += " and codigo_energia_estacion= " + TxtBusCodigo.Text;
            }
            if (TxtBusFecha.Text.Trim().Length > 0)
            {
                lsCondicion += " and fecha between  '" + TxtBusFecha.Text + "' and '";
            }
            else
            {
                if (TxtBusCodigo.Text.Trim().Length == 0)
                {
                    lsCondicion += " and fecha =  '" + TxtFecha.Text + "'";
                }
            }
            if (TxtBusFechaF.Text.Trim().Length > 0)
            {
                lsCondicion += TxtBusFechaF.Text + "'";
            }
            else
            {
                if (TxtBusFecha.Text.Trim().Length > 0)
                {
                    lsCondicion += TxtBusFecha.Text + "'";
                }
            }
            if (ddlBusOperador.SelectedValue != "0")
            {
                lsCondicion += " and codigo_operador =" + ddlBusOperador.SelectedValue;
            }
            if (ddlBusEstacion.SelectedValue != "0")
            {
                lsCondicion += " and codigo_estacion=" + ddlBusEstacion.SelectedValue;
            }

            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=t_energia_estacion&procedimiento=pa_ValidarExistencia&columnas=codigo_energia_estacion*fecha*codigo_operador*codigo_estacion*codigo_fuente*tipo_operacion*cantidad*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }
        if (lblMensaje.Text != "")
        {
            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click1(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_energia_estacion", "@P_fecha", "@P_codigo_operador", "@P_codigo_estacion", "@P_tipo_operacion", "@P_cantidad", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "", "0", "0", "", "0", "", "1" };
        lblMensaje.Text = "";
        int liValor = 0;
        try
        {
            if (TxtFecha.Text == "")
                lblMensaje.Text += " Debe digitar la fecha<br>";
            if (ddlOperador.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el operador<br>";
            if (ddlEstacion.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar la estación de compresión<br>";
            else //20220705
            {
                if (!DelegadaBase.Servicios.ValidarExistencia("m_punto_operador", " tipo_punto= 'C' and codigo_punto =" + ddlEstacion.SelectedValue + " and ( codigo_operador =0 or  codigo_operador= " + ddlOperador.SelectedValue + ") and estado ='A'", goInfo))
                    lblMensaje.Text = "La estación no está asociada al operador<br>";
            }
            if (VerificarExistencia("fecha = '" + TxtFecha.Text + "' and codigo_operador = " + ddlOperador.SelectedValue + " and codigo_estacion= " + ddlEstacion.SelectedValue + "  and tipo_operacion = '" + ddlOperacion.SelectedValue + "' and estado <>'E'"))
                lblMensaje.Text += " Ya está declarada la energía de estación de compresión para la fecha, el operador, estación de compresión y tipo de operación<br>"; //20180126 rq107-16
            if (TxtCantidad.Text == "")
                lblMensaje.Text += " Debe digitar la cantidad de la estación de compresión<br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCantidad.Text.Trim());
                    if (liValor < 0)
                        lblMensaje.Text += " Valor Inválido en la cantidad<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor Inválido en la cantidad<br>";
                }
            }
            //20220705
            string lsError = "N";
            if (lblMensaje.Text == "")
            {
                if (!DelegadaBase.Servicios.ValidarExistencia("m_punto_reporte_cnt", " codigo_reporte = 13 and codigo_punto =" + ddlEstacion.SelectedValue + " and estado ='A'", goInfo))
                    lblMensaje.Text += "La estación de compresión no tiene definida la cantidad máxima<br>";
                else
                {
                    if (validaCantidad())
                    {
                        lsError = "S";
                    }
                }
            }
            if (lsError == "S" && lblMensaje.Text == "")
            {
                Modal.Abrir(this, mdlConfirma.ID, mdlConfirmaInside.ID);
            }
            //20220705 fin

            if (lblMensaje.Text == "" && lsError == "N") //20220705
            {
                lValorParametros[1] = TxtFecha.Text;
                lValorParametros[2] = ddlOperador.SelectedValue;
                lValorParametros[3] = ddlEstacion.SelectedValue;
                lValorParametros[4] = ddlOperacion.SelectedValue;
                lValorParametros[5] = TxtCantidad.Text;
                lValorParametros[6] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaEstacion", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación de la energía.!"; //20161121 modif inf ope
                    lConexion.Cerrar();
                }
                else
                {
                    TxtBusFecha.Text = TxtFecha.Text; //20160711 ajuste msificaciones
                    //Cierra el modal de Agregar
                    ddlEstacion.SelectedValue = "0";
                    TxtCantidad.Text = "";
                    Modal.Cerrar(this, registroEnergiaEstacion.ID);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
        if (lblMensaje.Text != "")
        {
            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click1(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_energia_estacion","@P_codigo_estacion","@P_tipo_operacion", "@P_cantidad", "@P_estado", "@P_accion",  //201061121 modif inf ope
                                        "@P_modificacion"  }; // Campo Nuevo Req. 003-17  20170216
        SqlDbType[] lTipoparametros = { SqlDbType.Int,  SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int,SqlDbType.VarChar,SqlDbType.VarChar,  //201061121 modif inf ope
                                        SqlDbType.VarChar  }; // Campo Nuevo Req. 003-17  20170216
        string[] lValorParametros = { "0", "0", "", "0","", "2",  //201061121 modif inf ope
                                      hdfModificacion.Value  }; // Campo Nuevo Req. 003-17  20170216
        lblMensaje.Text = "";
        int liValor = 0;
        try
        {
            if (TxtCantidad.Text == "")
                lblMensaje.Text += " Debe digitar la energía de la estación de compresión<br>";//20180126 rq107-16
            else
            {
                // 20161121 modif inf ope
                if (ddlEstacion.SelectedValue == "0")
                    lblMensaje.Text += "Debe seleccionar la estación de compresión<br>";
                else //20220705
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_punto_operador", " tipo_punto= 'C' and codigo_punto =" + ddlEstacion.SelectedValue + " and ( codigo_operador =0 or  codigo_operador= " + ddlOperador.SelectedValue + ") and estado ='A'", goInfo))
                        lblMensaje.Text = "La estación no está asociada al operador<br>";
                }
                // 20161121 modif inf ope
                if (VerificarExistencia("fecha = '" + TxtFecha.Text + "' and codigo_operador = " + ddlOperador.SelectedValue + " and codigo_estacion= " + ddlEstacion.SelectedValue + " and tipo_operacion= '" + ddlOperacion.SelectedValue + "' and codigo_energia_estacion <>" + LblCodigoCap.Text + " and estado <>'E'"))
                    lblMensaje.Text += " Ya está declarada la enegía de la estación de compresión para la fecha, el operador, estación de compresión y la tipo de operación<br>";
                try
                {
                    liValor = Convert.ToInt32(TxtCantidad.Text.Trim());
                    if (liValor < 0)
                        lblMensaje.Text += " Valor Inválido en la cantidad de la estación de compresión<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor Inválido en la cantidad de la estación de compresión<br>";
                }
            }
            //20220705
            string lsError = "N";
            if (lblMensaje.Text == "")
            {
                if (!DelegadaBase.Servicios.ValidarExistencia("m_punto_reporte_cnt", " codigo_reporte = 13 and codigo_punto =" + ddlEstacion.SelectedValue + " and estado ='A'", goInfo))
                    lblMensaje.Text += "La estación de compresión no tiene definida la cantidad máxima<br>";
                else
                {
                    if (validaCantidad())
                    {
                        lsError = "S";
                    }
                }
            }
            if (lsError == "S" && lblMensaje.Text == "")
            {
                Modal.Abrir(this, mdlConfirma.ID, mdlConfirmaInside.ID);
            }
            //20220705 fin

            if (lblMensaje.Text == "" && lsError == "N") //20220705
            {

                lValorParametros[0] = LblCodigoCap.Text;
                lValorParametros[1] = ddlEstacion.SelectedValue; //20161121 modif inf ope
                lValorParametros[2] = ddlOperacion.SelectedValue; //20161121 modif inf ope
                lValorParametros[3] = TxtCantidad.Text; //20161121 modif inf ope
                lValorParametros[4] = ddlEstado.SelectedValue; //20161121 modif inf ope
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaEstacion", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización de la energía de la estacón de compresión.!";
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigoCap.Text);
                    //Cierra el modal de Agregar
                    Modal.Cerrar(this, registroEnergiaEstacion.ID);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoCap.Text);
            lblMensaje.Text = ex.Message;
        }
        if (lblMensaje.Text != "")
        {
            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        if (TxtBusCodigo.Text.Trim().Length <= 0 && TxtBusFecha.Text.Trim().Length <= 0 && ddlBusOperador.SelectedValue == "0" && ddlBusEstacion.SelectedValue == "0")
            lblMensaje.Text = "Debe Seleccionar al Menos un parámetro para realizar la Búsqueda!.";
        else
        {
            Listar();
        }
    }
    ///// Eventos Nuevos para la Implementracion del UserControl
    /// <summary>
    /// Metodo del Link Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo(object sender, EventArgs e)
    {
        mdlRegistroEnergiaEstacionLabel.InnerText = "Agregar";
        Nuevo();
    }

    /// <summary>
    /// Metodo del Link Listar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnListar(object sender, EventArgs e)
    {
        Listar();
    }

    /// <summary>
    /// 
    /// </summary>
    protected void Cancel_OnClick(object sender, EventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (mdlRegistroEnergiaEstacionLabel.InnerText.Equals("Modificar") && LblCodigoCap.Text != "")
            manejo_bloqueo("E", LblCodigoCap.Text);

        //Cierra el modal de Agregar
        Modal.Cerrar(this, registroEnergiaEstacion.ID);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20220705
    protected void BtnAceptarConf_Click(object sender, EventArgs e)
    {
        Modal.Cerrar(this, mdlConfirma.ID);
        string[] lsNombreParametros = { "@P_codigo_energia_estacion", "@P_fecha", "@P_codigo_operador", "@P_codigo_estacion", "@P_tipo_operacion", "@P_cantidad", "@P_estado", "@P_accion", "@P_dato_atipico" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "", "0", "0", "", "0", "", "1", "S" };

        if (TxtCodigoCap.Text != "0")
        {
            lValorParametros[0] = TxtCodigoCap.Text;
            lValorParametros[7] = "2";
        }
        lValorParametros[1] = TxtFecha.Text;
        lValorParametros[2] = ddlOperador.SelectedValue;
        lValorParametros[3] = ddlEstacion.SelectedValue;
        lValorParametros[4] = ddlOperacion.SelectedValue;
        lValorParametros[5] = TxtCantidad.Text;
        lValorParametros[6] = ddlEstado.SelectedValue;
        lConexion.Abrir();
        if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaEstacion", lsNombreParametros, lTipoparametros, lValorParametros))
        {
            lblMensaje.Text = "Se presentó un Problema en la Creación de la energía.!"; //20161121 modif inf ope
            lConexion.Cerrar();
        }
        else
        {
            TxtBusFecha.Text = TxtFecha.Text; //20160711 ajuste msificaciones
                                              //Cierra el modal de Agregar
            ddlEstacion.SelectedValue = "0";
            TxtCantidad.Text = "";
            Modal.Cerrar(this, registroEnergiaEstacion.ID);
            Listar();
            if (LblCodigoCap.Text != "0")
                manejo_bloqueo("E", LblCodigoCap.Text);
        }
    }
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool validaCantidad()
    {
        bool lbValida = false;
        lConexion.Abrir();
        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_punto_reporte_cnt", " codigo_reporte = 13 and codigo_punto =" + ddlEstacion.SelectedValue + " and cantidad_maxima < " + TxtCantidad.Text + "  and estado ='A'");
        if (lLector.HasRows)
        {
            lLector.Read();
            lblMensajeCOnf.Text = "La cantidad de energía a registrar de " + Convert.ToInt32(TxtCantidad.Text).ToString("###,###,###,###") + " MBTU supera el valor máximo histórico parametrizado para la estación de compresión " + ddlEstacion.SelectedItem.ToString() + " que es de " + Convert.ToInt32(lLector["cantidad_maxima"].ToString()).ToString("###,###,###,###") + " MBTU";
            lbValida = true;
        }
        lLector.Close();
        lConexion.Cerrar();
        return lbValida;
    }

}