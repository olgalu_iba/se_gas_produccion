﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ConsultaRegistroContratos.aspx.cs" Inherits="Verificacion.frm_ConsultaRegistroContratos" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Número Operación Inicial" AssociatedControlID="TxtNoContrato" runat="server"></asp:Label>
                            <asp:TextBox ID="TxtNoContrato" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Número Operación Final" AssociatedControlID="TxtNoContratoFin" runat="server" />
                            <asp:TextBox ID="TxtNoContratoFin" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Subasta" AssociatedControlID="ddlSubasta" runat="server" />
                            <asp:DropDownList ID="ddlSubasta" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Negociación Inicial" AssociatedControlID="TxtFechaIni" runat="server" />
                            <asp:TextBox ID="TxtFechaIni" CssClass="form-control datepicker" placeholder="yyyy/mm/dd" ValidationGroup="detalle" Width="100%" runat="server" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Negociación Final" AssociatedControlID="TxtFechaFin" runat="server" />
                            <asp:TextBox ID="TxtFechaFin" CssClass="form-control datepicker" placeholder="yyyy/mm/dd" ValidationGroup="detalle" Width="100%" runat="server" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Mercado" AssociatedControlID="ddlMercado" runat="server" />
                            <asp:DropDownList ID="ddlMercado" runat="server" CssClass="form-control">
                                <asp:ListItem Value="" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="P" Text="Primario"></asp:ListItem>
                                <asp:ListItem Value="S" Text="Secundario"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Producto" AssociatedControlID="ddlProducto" runat="server" />
                            <asp:DropDownList ID="ddlProducto" runat="server" CssClass="form-control">
                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="T" Text="Capacidad de Transporte"></asp:ListItem>
                                <%--20180126 rq107-16--%>
                                <asp:ListItem Value="G" Text="Suministro de Gas"></asp:ListItem>
                                <%--20180126 rq107-16--%>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Comprador" AssociatedControlID="ddlComprador" runat="server" />
                            <asp:DropDownList ID="ddlComprador" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Vendedor" AssociatedControlID="ddlVendedor" runat="server" />
                            <asp:DropDownList ID="ddlVendedor" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Estado" AssociatedControlID="ddlEstado" runat="server" />
                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Carga Extemporánea" AssociatedControlID="ddlExtemporanea" runat="server" />
                            <asp:DropDownList ID="ddlExtemporanea" runat="server" Width="100%" CssClass="form-control">
                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Causa Modificación" AssociatedControlID="ddlCausa" runat="server" />
                            <asp:DropDownList ID="ddlCausa" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Id Registro" AssociatedControlID="TxtNoId" runat="server" />
                            <asp:TextBox ID="TxtNoId" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgConsulta" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                OnPageIndexChanged="dtgConsulta_PageIndexChanged"
                                AllowPaging="true" PageSize="10" PagerStyle-Mode="NumericPages" PagerStyle-Position="TopAndBottom">
                                <Columns>
                                    <%--0--%>
                                    <asp:BoundColumn DataField="numero_operacion" HeaderText="No. Operación"></asp:BoundColumn>
                                    <%--20171130 rq026-17--%>
                                    <%--1--%>
                                    <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro"></asp:BoundColumn>
                                    <%--20171130 rq026-17--%>
                                    <%--1--%>
                                    <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación"></asp:BoundColumn>
                                    <%--2--%>
                                    <asp:BoundColumn DataField="fecha_suscripcion" HeaderText="Fecha Suscripción" Visible="false"></asp:BoundColumn>
                                    <%--3--%>
                                    <asp:BoundColumn DataField="tipo_subasta" HeaderText="Tipo Subasta"></asp:BoundColumn>
                                    <%--4--%>
                                    <asp:BoundColumn DataField="tipo_rueda" HeaderText="Tipo Rueda" Visible="false"></asp:BoundColumn>
                                    <%--5--%>
                                    <asp:BoundColumn DataField="mercado" HeaderText="Mercado"></asp:BoundColumn>
                                    <%--6--%>
                                    <asp:BoundColumn DataField="producto" HeaderText="Producto"></asp:BoundColumn>
                                    <%--7--%>
                                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Contrato"></asp:BoundColumn>
                                    <%--8--%>
                                    <asp:BoundColumn DataField="comprador" HeaderText="Nombre Comprador"></asp:BoundColumn>
                                    <%--9--%>
                                    <asp:BoundColumn DataField="codigo_comprador" HeaderText="Código Comprador" Visible="false"></asp:BoundColumn>
                                    <%--10--%>
                                    <asp:BoundColumn DataField="rol_comprador" HeaderText="Rol Comprador" Visible="false"></asp:BoundColumn>
                                    <%--11--%>
                                    <asp:BoundColumn DataField="vendedor" HeaderText="Nombre Vendedor"></asp:BoundColumn>
                                    <%--12--%>
                                    <asp:BoundColumn DataField="codigo_vendedor" HeaderText="Código Vendedor" Visible="false"></asp:BoundColumn>
                                    <%--13--%>
                                    <asp:BoundColumn DataField="rol_vendedor" HeaderText="Rol Vendedor" Visible="false"></asp:BoundColumn>
                                    <%--14--%>
                                    <%--20180126 rq107 - 16--%>
                                    <asp:BoundColumn DataField="hora_negociacion" HeaderText="Hora Negociación" Visible="false"></asp:BoundColumn>
                                    <%--15--%>
                                    <asp:BoundColumn DataField="punto_ent_ruta" HeaderText="Punto Entrega/Ruta"></asp:BoundColumn>
                                    <%--16--%>
                                    <asp:BoundColumn DataField="modalidad" HeaderText="Modalidad"></asp:BoundColumn>
                                    <%--17--%>
                                    <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial"></asp:BoundColumn>
                                    <%--18--%>
                                    <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final"></asp:BoundColumn>
                                    <%--19--%>
                                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad"></asp:BoundColumn>
                                    <%--20--%>
                                    <asp:BoundColumn DataField="precio" HeaderText="Precio"></asp:BoundColumn>
                                    <%--21--%>
                                    <asp:BoundColumn DataField="fecha_limite_registro" HeaderText="Fecha Límite Registro"><%--20171130 rq026-17--%>
                                    </asp:BoundColumn>
                                    <%--22--%>
                                    <asp:BoundColumn DataField="estado" HeaderText="Estado"></asp:BoundColumn>
                                    <%--23--%>
                                    <asp:BoundColumn DataField="numero_registro" HeaderText="No. Registro"></asp:BoundColumn>
                                    <%--24--%>
                                    <%--20180126 rq107 - 16--%>
                                    <asp:BoundColumn DataField="carga_extemporanea" HeaderText="Carga Extemporánea" Visible="false"></asp:BoundColumn>
                                    <%--25--%><%--20160602--%>
                                    <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente"></asp:BoundColumn>
                                    <%--26--%><%--20160602--%>
                                    <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente"></asp:BoundColumn>
                                    <%--27--%><%--20170814 rq036-17--%>
                                    <asp:BoundColumn DataField="conectado_snt" HeaderText="Ind Conectado SNT"></asp:BoundColumn>
                                    <%--28--%><%--20170814 rq036-17--%>
                                    <asp:BoundColumn DataField="ent_boca_pozo" HeaderText="Ind Entrega Boca Pozo"></asp:BoundColumn>
                                    <%--29--%><%--20170814 rq036-17--%>
                                    <asp:BoundColumn DataField="codigo_centro_pob" HeaderText="Código Centro"></asp:BoundColumn>
                                    <%--20171130 rq026-17--%>
                                    <%--30--%><%--20170814 rq036-17--%>
                                    <asp:BoundColumn DataField="nombre_centro" HeaderText="Centro Poblado"></asp:BoundColumn>
                                    <%--30--%><%--20171130 rq026-17--%>
                                    <asp:BoundColumn DataField="desc_causa" HeaderText="Causa Modificación"></asp:BoundColumn>
                                    <%--20200727 ajsute fornt end--%>
                                    <asp:BoundColumn DataField="codigo_trm" HeaderText="Código Tipo TRM"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_trm" HeaderText="Tipo TRM"></asp:BoundColumn>
                                    <%--20210707 trm y moneda--%>
                                    <asp:BoundColumn DataField="tipo_moneda" HeaderText="Moneda"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="observacion_trm" HeaderText="Observación TRM"></asp:BoundColumn>
                                    <%--20200727 fin ajsute fornt end--%>
                                    <%--20201124--%>
                                    <asp:BoundColumn DataField="clasificacion_transporte" HeaderText="Clasificación Transporte"></asp:BoundColumn>
                                    <%--20210305 broker--%>
                                    <asp:BoundColumn DataField="ind_broker" HeaderText="Contrato Broker"></asp:BoundColumn>
                                    <%--20210915--%>
                                    <asp:BoundColumn DataField="numero_id_tra" HeaderText="Id MP Contrato MS Transporte"></asp:BoundColumn>
                                    <%--20210915--%>
                                    <asp:BoundColumn DataField="ind_contrato_var" HeaderText="Contrato Variable"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                        AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1"
                        Visible="false">
                        <Columns>
                            <%--0--%>
                            <asp:BoundColumn DataField="numero_operacion" HeaderText="No. Operación"></asp:BoundColumn>
                            <%--1--%>
                            <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro"></asp:BoundColumn>
                            <%--20171130 rq026-17--%>
                            <%--1--%>
                            <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación"></asp:BoundColumn>
                            <%--2--%>
                            <asp:BoundColumn DataField="fecha_suscripcion" HeaderText="Fecha Suscripción"></asp:BoundColumn>
                            <%--3--%>
                            <asp:BoundColumn DataField="tipo_subasta" HeaderText="Tipo Subasta"></asp:BoundColumn>
                            <%--4--%>
                            <asp:BoundColumn DataField="tipo_rueda" HeaderText="Tipo Rueda"></asp:BoundColumn>
                            <%--5--%>
                            <asp:BoundColumn DataField="mercado" HeaderText="Mercado"></asp:BoundColumn>
                            <%--6--%>
                            <asp:BoundColumn DataField="producto" HeaderText="Producto"></asp:BoundColumn>
                            <%--7--%>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Contrato"></asp:BoundColumn>
                            <%--8--%>
                            <asp:BoundColumn DataField="comprador" HeaderText="Nombre Comprador"></asp:BoundColumn>
                            <%--9--%>
                            <asp:BoundColumn DataField="codigo_comprador" HeaderText="Código Comprador"></asp:BoundColumn>
                            <%--10--%>
                            <asp:BoundColumn DataField="rol_comprador" HeaderText="Rol Comprador"></asp:BoundColumn>
                            <%--11--%>
                            <asp:BoundColumn DataField="vendedor" HeaderText="Nombre Vendedor"></asp:BoundColumn>
                            <%--12--%>
                            <asp:BoundColumn DataField="codigo_vendedor" HeaderText="Código Vendedor"></asp:BoundColumn>
                            <%--13--%>
                            <asp:BoundColumn DataField="rol_vendedor" HeaderText="Rol Vendedor"></asp:BoundColumn>
                            <%--14--%>
                            <%--20180126 rq107 - 16--%>
                            <asp:BoundColumn DataField="hora_negociacion" HeaderText="Hora Negociación"></asp:BoundColumn>
                            <%--20170814 rq036-17--%>
                            <asp:BoundColumn DataField="codigo_punto_entrega" HeaderText="Código Entrega"></asp:BoundColumn>
                            <%--15--%>
                            <asp:BoundColumn DataField="punto_ent_ruta" HeaderText="Punto Entrega/Ruta"></asp:BoundColumn>
                            <%--16--%>
                            <asp:BoundColumn DataField="modalidad" HeaderText="Modalidad"></asp:BoundColumn>
                            <%--17--%>
                            <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial"></asp:BoundColumn>
                            <%--20161213 rq102--%>
                            <asp:BoundColumn DataField="hora_inicial" HeaderText="Hora Inicial"></asp:BoundColumn>
                            <%--18--%>
                            <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final"></asp:BoundColumn>
                            <%--20161213 rq102--%>
                            <asp:BoundColumn DataField="hora_final" HeaderText="Hora Final"></asp:BoundColumn>
                            <%--19--%>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad"></asp:BoundColumn>
                            <%--20--%>
                            <asp:BoundColumn DataField="precio" HeaderText="Precio"></asp:BoundColumn>
                            <%--21--%>
                            <asp:BoundColumn DataField="fecha_limite_registro" HeaderText="Fecha Límite Registro"><%--20171130 rq026-17--%>
                            </asp:BoundColumn>
                            <%--22--%>
                            <asp:BoundColumn DataField="estado" HeaderText="Estado"></asp:BoundColumn>
                            <%--23--%>
                            <asp:BoundColumn DataField="numero_registro" HeaderText="No. Registro"></asp:BoundColumn>
                            <%--24--%>
                            <%--20180126 rq107 - 16--%>
                            <asp:BoundColumn DataField="carga_extemporanea" HeaderText="Actualización/Extemporánea"></asp:BoundColumn>
                            <%--25--%><%--20160602--%>
                            <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente"></asp:BoundColumn>
                            <%--26--%><%--20160602--%>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente"></asp:BoundColumn>
                            <%--20161213 rq102--%>
                            <%--20180126 rq107 - 16--%>
                            <asp:BoundColumn DataField="presion" HeaderText="presión"></asp:BoundColumn>
                            <%--20161213 rq102--%>
                            <asp:BoundColumn DataField="sentido_flujo" HeaderText="Flujo"></asp:BoundColumn>
                            <%--20161213 rq102--%>
                            <asp:BoundColumn DataField="codigo_periodo" HeaderText="cod. periodo "></asp:BoundColumn>
                            <%--20161213 rq102--%>
                            <asp:BoundColumn DataField="desc_periodo" HeaderText="periodo entrega"></asp:BoundColumn>
                            <%--20170814 rq036-17--%>
                            <asp:BoundColumn DataField="conectado_snt" HeaderText="Ind Conectado SNT"></asp:BoundColumn>
                            <%--20170814 rq036-17--%>
                            <asp:BoundColumn DataField="ent_boca_pozo" HeaderText="Ind Entrega Boca Pozo"></asp:BoundColumn>
                            <%--20170814 rq036-17--%>
                            <asp:BoundColumn DataField="codigo_centro_pob" HeaderText="Código Centro"></asp:BoundColumn>
                            <%--20171130 rq026-17--%>
                            <%--20170814 rq036-17--%>
                            <asp:BoundColumn DataField="nombre_centro" HeaderText="Centro Poblado"></asp:BoundColumn>
                            <%--20171130 rq026-17--%>
                            <asp:BoundColumn DataField="desc_causa" HeaderText="Causa Modificación"></asp:BoundColumn>
                            <%--20200727 ajsute fornt end--%>
                            <asp:BoundColumn DataField="codigo_trm" HeaderText="Código Tipo TRM"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_trm" HeaderText="Tipo TRM"></asp:BoundColumn>
                            <%--20210707 trm y moneda--%>
                            <asp:BoundColumn DataField="tipo_moneda" HeaderText="Moneda"></asp:BoundColumn>
                            <%--20220505--%>
                            <asp:BoundColumn DataField="observacion_trm" HeaderText="Observaciones"></asp:BoundColumn>
                            <%--20200727 fin ajsute fornt end--%>
                            <%--20200803 res 138--%>
                            <asp:BoundColumn DataField="fecha_hora_verifica" HeaderText="Fecha Registro"></asp:BoundColumn>
                            <%--20201124--%>
                            <asp:BoundColumn DataField="clasificacion_transporte" HeaderText="Clasificación Transporte"></asp:BoundColumn>
                            <%--20210305 broker--%>
                            <asp:BoundColumn DataField="ind_broker" HeaderText="Contrato Broker"></asp:BoundColumn>
                            <%--20210915--%>
                            <asp:BoundColumn DataField="numero_id_tra" HeaderText="Id MP Contrato MS Transporte"></asp:BoundColumn>
                            <%--20210915--%>
                            <asp:BoundColumn DataField="ind_contrato_var" HeaderText="Contrato Variable"></asp:BoundColumn>
                            <%--20220505--%>
                            <asp:BoundColumn DataField="observacion_sol" HeaderText="Obs. Solicitud Desistimiento"></asp:BoundColumn>
                        </Columns>
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
