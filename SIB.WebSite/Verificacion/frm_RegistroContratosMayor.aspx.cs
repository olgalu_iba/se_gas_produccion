﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Verificacion_frm_RegistroContratosMayor : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Registro de Contratos de Mercado Mayorista";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    string sRutaArc = ConfigurationManager.AppSettings["rutaCarga"].ToString();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        //Titulo
        Master.Titulo = "Registro de Contratos de Mercado Mayorista";

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            //LlenarControles(lConexion.gObjConexion, ddlProducto, "m_producto a", " a.estado = 'A' order by descripcion", 0, 1);  //20161215 rq111 mayorista transporte  //20170926 rq027-17
            //LlenarControles(lConexion.gObjConexion, ddlCapProducto, "m_producto a", " a.estado = 'A' order by descripcion", 0, 1); //20161215 rq111 mayoristra transporte   //20170926 rq027-17
            LlenarControles(lConexion.gObjConexion, ddlVendedor, "m_operador a, m_operador_punta_mayorista b, m_tipos_operador c", " a.estado = 'A' And a.tipo_operador = c.sigla And b.codigo_tipo_operador = c.codigo_tipo_operador And b.punta_contrato = 'V' And b.estado = 'A'  order by razon_social", 0, 4); //20160912 AJUSTE COMBO
            LlenarControles(lConexion.gObjConexion, ddlComprador, "m_operador a, m_operador_punta_mayorista b, m_tipos_operador c", " a.estado = 'A' And a.tipo_operador = c.sigla And b.codigo_tipo_operador = c.codigo_tipo_operador And b.punta_contrato = 'C' And b.estado = 'A'  order by razon_social", 0, 4);//20160912 AJUSTE COMBO
            LlenarControles(lConexion.gObjConexion, ddlEstado, "m_estado_gas", " tipo_estado = 'V' and sigla_estado != 'S' order by descripcion_estado", 2, 3); //20180126 rq107-16
            LlenarControles(lConexion.gObjConexion, ddlBusSector, "m_sector_consumo", " estado='A' order by descripcion", 0, 1); //20160706
            LlenarControles(lConexion.gObjConexion, ddlBusMercado, "m_mercado_relevante", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlRuta, "m_ruta_snt", " estado in ('A','E')  order by descripcion", 0, 4); // 20161215 rq111 mayorista transporte
            LlenarControles2(lConexion.gObjConexion, ddlCausa, "", "", 0, 1); //20171130 rq026-17
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
            if (lLector.HasRows)
            {
                lLector.Read();
                hdfDiasMaxRegCont.Value = lLector["no_dias_hab_regi_cont_mayor"].ToString(); // Dias maximos para registro de contratos
                hdfDiasMaxCorCont.Value = lLector["no_dias_hab_corr_cont_mayor"].ToString(); // Dias maximos para correccion de contratos
                hdfVerifManual.Value = lLector["verifica_manual_cont_mayor"].ToString(); // Dias maximos para correccion de contratos
            }
            lLector.Close();
            lLector.Dispose();
            //20180228 rq011-18
            string sComi = "";
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador =" + goInfo.cod_comisionista);
            if (lLector.HasRows)
            {
                lLector.Read();
                sComi = lLector["codigo_operador"].ToString() + "-" + lLector["codigo_tipo_doc"].ToString() + "-" + lLector["no_documento"].ToString() + "-" + lLector["razon_social"].ToString();  //20180228 rq011-18
            }
            //20180228 fin rq011-18
            lConexion.Cerrar();
            //CargarDatos();
            if (Session["tipoPerfil"].ToString() == "B")
            {
                //ImbCrearReg.Visible = false;  20170601 rq020-17
                ddlVendedor.Enabled = true;
                ddlComprador.Enabled = true; // 20170601 rq020-17
            }
            else
            {
                //if (goInfo.codigo_grupo_usuario == 3)
                //{
                //    ddlVendedor.SelectedValue = Session["cod_comisionista"].ToString();
                //    ddlVendedor.Enabled = false;
                //}
                //2018228 rq011-18
                //if (goInfo.codigo_grupo_usuario == 32)
                //{
                //    ddlComprador.SelectedValue = Session["cod_comisionista"].ToString();
                //    ddlComprador.Enabled = false;
                //}
                //2018228 rq011-18
                try
                {
                    ddlComprador.SelectedValue = sComi;
                    if (ddlComprador.SelectedValue != "0")
                        ddlComprador.Enabled = false;
                }
                catch (Exception)
                {
                    ddlComprador.Enabled = true;
                }
                //2018228 rq011-18
                try
                {
                    ddlVendedor.SelectedValue = sComi;
                    if (ddlVendedor.SelectedValue != "0")
                        ddlVendedor.Enabled = false;
                }
                catch (Exception)
                {
                    ddlVendedor.Enabled = true;
                }

            }
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            if (lsTabla != "m_operador")
            {

                if (lsTabla != "m_producto a, m_caracteristica_sub b")
                {
                    if (lsTabla == "m_operador a")
                    {
                        lItem1.Value = lLector["codigo_operador"].ToString();
                        lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                    }
                    else
                    {
                        if (lsTabla == "m_operador a, m_operador_punta_mayorista b, m_tipos_operador c")
                        {
                            lItem1.Value = lLector["codigo_operador"].ToString() + "-" + lLector["codigo_tipo_doc"].ToString() + "-" + lLector["no_documento"].ToString() + "-" + lLector["razon_social"].ToString();  //20180228 rq011-18
                            lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                        }
                        else
                        {
                            if (lsTabla == "m_sector_consumo")
                            {
                                lItem1.Value = lLector["codigo_sector_consumo"].ToString();
                                lItem1.Text = lLector["descripcion"].ToString(); //20160706
                            }
                            else
                            {
                                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                            }
                        }
                    }
                }
                else
                {
                    lItem1.Value = lLector["codigo_producto"].ToString() + "-" + lLector["destino_rueda"].ToString();
                    lItem1.Text = lLector["descripcion"].ToString();
                }
            }
            else
            {
                lItem1.Value = lLector["codigo_operador"].ToString() + "-" + lLector["codigo_tipo_doc"].ToString() + "-" + lLector["no_documento"].ToString() + "-" + lLector["razon_social"].ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles2(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetCausaModAdc", null, null, null);
        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);
        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["descripcion"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        lblMensaje.Text = "";
        DateTime ldFecha;
        int liValor = 0;
        string[] lsOpera;  //20170130
        string[] lsNombreParametros = { "@P_tipo_subasta", "@P_numero_contrato", "@P_fecha_contrato", "@P_mercado", "@P_codigo_producto",
                                        "@P_operador_venta","@P_operador_compra","@P_tipo_perfil","@P_codigo_operador", "@P_estado",
                                        "@P_codigo_sector_consumo", "@P_codigo_mercado_relevante" ,
                                        "@P_fecha_fin_cont", "@P_numero_operacion", "@P_numero_operacion_fin" , //20161213 rq102 conformacion de rutas
                                        "@P_codigo_causa","@P_codigo_verif"}; //20171130 rq026-17
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int,
                                         SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int ,
                                         SqlDbType.VarChar , SqlDbType.Int, SqlDbType.Int ,   //20161213 rq102 conformacion rutas
                                         SqlDbType.VarChar, SqlDbType.Int};//20171130 rq026-17
        string[] lValorParametros = { "0", "0", "", "", "0", "0", "0", Session["tipoPerfil"].ToString(), goInfo.cod_comisionista, "0",
                                      ddlBusSector.SelectedValue, ddlBusMercado.SelectedValue,
                                      "","0", "0", "0", "0"}; //20161213 rq102 conformacion de rutas //20171130 rq026-17

        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial Negociación. <br>"; //20161213 rq102 conformacion de rutas
            }
        }

        //20161213 rq102 conformacion de rutas
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaFin.Text);
            }
            catch (Exception)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Final de Negociación. <br>";
            }
        }
        //20161213 rq102 conformacion de rutas
        if (TxtFechaIni.Text.Trim().Length == 0 && TxtFechaFin.Text.Trim().Length > 0)
            lblMensaje.Text += "Debe digitar la Fecha Inicial de Negociación. <br>";
        //20161213 rq102 conformacion de rutas
        if (TxtFechaIni.Text.Trim().Length > 0 && TxtFechaFin.Text.Trim().Length > 0)
            try
            {
                if (Convert.ToDateTime(TxtFechaFin.Text) < Convert.ToDateTime(TxtFechaIni.Text))
                    lblMensaje.Text += "La Fecha inicial de Negociación debe ser menor o igual que la fecha final. <br>";
            }
            catch (Exception)
            { }
        //20161213 rq102 conformacion de rutas
        if (txtOperacion.Text.Trim().Length > 0)
        {
            try
            {
                Convert.ToInt32(txtOperacion.Text);
            }
            catch (Exception)
            {
                lblMensaje.Text += "Valor Inválido en el campo número de operación inicial. <br>";
            }
        }
        // 20161207 rq102 conformación de rutas
        if (txtOperacionFin.Text.Trim().Length > 0)
        {
            try
            {
                liValor = Convert.ToInt32(txtOperacionFin.Text);
            }
            catch (Exception)
            {
                lblMensaje.Text += "Formato Inválido en el Campo No Operación final. <br>";
            }

        }
        // 20161207 rq102 conformación de rutas
        if (txtOperacion.Text.Trim().Length == 0 && txtOperacionFin.Text.Trim().Length > 0)
        {
            lblMensaje.Text += "Debe digitar el No de operación inicial antes que el final. <br>";
        }
        // 20161207 rq102 conformación de rutas
        if (txtOperacion.Text.Trim().Length > 0 && txtOperacionFin.Text.Trim().Length > 0)
        {
            try
            {
                if (Convert.ToInt32(txtOperacionFin.Text) < Convert.ToInt32(txtOperacion.Text))
                    lblMensaje.Text += "El No Operación inicial debe ser menor o igual que el final. <br>";
            }
            catch (Exception)
            {

            }
        }

        if (lblMensaje.Text == "")
        {
            try
            {
                if (TxtFechaIni.Text.Trim().Length > 0)
                    lValorParametros[2] = TxtFechaIni.Text.Trim();
                if (ddlMercado.SelectedValue != "")
                    lValorParametros[3] = ddlMercado.SelectedValue;
                if (ddlProducto.SelectedValue != "0")
                    lValorParametros[4] = ddlProducto.SelectedValue;
                if (ddlVendedor.SelectedValue != "0")
                {
                    lsOpera = ddlVendedor.SelectedValue.Split('-'); //20170130 ajuste por error
                    lValorParametros[5] = lsOpera[0]; //20170130 ajuste por error
                }
                if (ddlComprador.SelectedValue != "0")
                {
                    lsOpera = ddlComprador.SelectedValue.Split('-'); //20170130 ajuste por error
                    lValorParametros[6] = lsOpera[0]; //20170130 ajuste por error
                }
                if (ddlEstado.SelectedValue != "0")
                    lValorParametros[9] = ddlEstado.SelectedValue;
                //20161213 rq102 conformacion de rutas
                if (TxtFechaFin.Text.Trim().Length > 0)
                    lValorParametros[12] = TxtFechaFin.Text.Trim();
                else
                    lValorParametros[12] = lValorParametros[2];
                //20161213 rq102 conformacion de rutas
                if (txtOperacion.Text.Trim().Length > 0)
                    lValorParametros[13] = txtOperacion.Text.Trim();
                //20161213 rq102 conformacion de rutas
                if (txtOperacionFin.Text.Trim().Length > 0)
                    lValorParametros[14] = txtOperacionFin.Text.Trim();
                else
                    lValorParametros[14] = lValorParametros[13];
                lValorParametros[15] = ddlCausa.SelectedValue; //20171130 rq026-17
                //20171130 rq026-17
                if (TxtNoId.Text.Trim().Length > 0)
                    lValorParametros[16] = TxtNoId.Text.Trim();

                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRegistroContratosMayor", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                dtgConsultaExcel.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRegistroContratosMayor", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsultaExcel.DataBind();
                lConexion.Cerrar();
                if (dtgConsulta.Items.Count > 0)
                {
                    tblGrilla.Visible = true;
                    lkbExcel.Visible = true;
                    ldFecha = DateTime.Now.Date;
                    foreach (DataGridItem Grilla in this.dtgConsulta.Items)
                    {
                        if (Session["tipoPerfil"].ToString() == "B")
                        {
                            if (Grilla.Cells[8].Text == "Pendiente Verificacion" || Grilla.Cells[8].Text == "Pendiente Comprador")
                                Grilla.Cells[26].Enabled = true;
                            else
                                Grilla.Cells[26].Enabled = false;
                        }
                        else
                        {
                            if (Grilla.Cells[8].Text == "Pendiente Verificacion")
                                Grilla.Cells[28].Enabled = false;
                            else
                                Grilla.Cells[28].Enabled = true;
                        }
                        if (Grilla.Cells[8].Text != "Contrato Registrado")
                            Grilla.Cells[19].Text = "0";
                    }
                }
                else
                {
                    tblGrilla.Visible = false;
                    lkbExcel.Visible = false;
                    lblMensaje.Text = "No se encontraron Registros.";
                }
                if (Session["tipoPerfil"].ToString() == "B")
                    dtgConsulta.Columns[26].Visible = true;
                else
                    dtgConsulta.Columns[26].Visible = false;
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString(); //20180126 rq107-16
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        string lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "/" + DateTime.Now.ToShortDateString().Substring(3, 2) + "/" + DateTime.Now.ToShortDateString().Substring(0, 2);
        int liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
        string lsCodOperaC = "";
        string lsCodOperaV = "";
        string lsCodDocC = "";
        string lsCodDocV = "";
        string lsCodMpio = "";
        string lsCodCentro = "";  //20170530 divipola
        string lsCorreccion = "";
        string lsFechaMaxReg = "";
        string lsFechaMaxCor = "";
        string lsFechaMaxMod = "";
        string lsPuedeEntrar = "S";
        string lsIndModif = "N";
        string lsPuntaCont = "";

        if (((LinkButton)e.CommandSource).Text == "Seleccionar")
        {
            try
            {
                ActivarCampos(); //20180228 rq011-18
                ddlCapComprador.Enabled = false; //20180228 rq011-18
                ddlCapVendedor.Enabled = false; //20180228 rq011-18
                ddlCapPunta.Enabled = false;
                TxtCapFechaNeg.Text = "";
                TxtNumContrato.Text = "";
                TxtFechaSus.Text = "";
                TxtCantidad.Text = "";
                TxtCantidadT.Text = "";  //20170926 rq027-17
                TxtPrecio.Text = "";
                TxtFechaInicial.Text = "";
                TxtFechaFinal.Text = "";

                lblOperacion.Text = e.Item.Cells[0].Text;
                lblProducto.Text = e.Item.Cells[4].Text;
                hdfIdVerif.Value = e.Item.Cells[9].Text;
                hdfCodTipoSub.Value = e.Item.Cells[14].Text;
                hdfDestinoRueda.Value = e.Item.Cells[20].Text;
                hdfEstadoAct.Value = e.Item.Cells[21].Text;
                hdfTipoMerc.Value = e.Item.Cells[22].Text;
                hdfCodProd.Value = e.Item.Cells[27].Text; // Cambio indice ajustes reportes 20160405
                hdfOPeraC.Value = e.Item.Cells[10].Text;
                hdfOPeraV.Value = e.Item.Cells[11].Text;
                TxtCapFechaNeg.Text = e.Item.Cells[1].Text;

                lsCorreccion = e.Item.Cells[29].Text; // Cambio indice ajustes reportes 20160405
                lsFechaMaxReg = e.Item.Cells[30].Text; // Cambio indice ajustes reportes 20160405
                lsFechaMaxCor = e.Item.Cells[31].Text; // Cambio indice ajustes reportes 20160405
                lsFechaMaxMod = e.Item.Cells[32].Text; // Cambio indice ajustes reportes 20160405
                lsIndModif = e.Item.Cells[33].Text; // Cambio indice ajustes reportes 20160405
                ddlCapProducto.SelectedValue = e.Item.Cells[27].Text; // 20161215 rq111 mayorista transporte
                ddlCapProducto_SelectedIndexChanged(null, null); //200161215 rq111 mayorista transporte
                if (goInfo.cod_comisionista == e.Item.Cells[10].Text)
                {
                    hdfPunta.Value = "C";
                    lsPuntaCont = "V";
                    lblPunta.Text = "COMPRADOR";
                    lnlNomOpera.Text = "Nombre del Comprador";
                    lblTipoDocOpera.Text = "Tipo Documento Comprador";
                    lblNoDocOPera.Text = "No. Documento Comprador";
                    lblNomOperador.Text = e.Item.Cells[5].Text;
                    lblTipoDocOperador.Text = e.Item.Cells[15].Text;
                    lblNoDocumentoOperaqdor.Text = e.Item.Cells[17].Text;
                    ddlCapPunta.SelectedValue = "C";
                    //ddlCapComprador.Enabled = false; 20180228 rq011-18
                    //TxtCapComprador.Enabled = false;
                    //ddlCapVendedor.Enabled = false; 20180228 rq011-18
                }
                //else  //20170601 rq020-17
                if (goInfo.cod_comisionista == e.Item.Cells[11].Text)  //20170601 rq020-17
                {
                    hdfPunta.Value = "V";
                    lsPuntaCont = "C";
                    lblPunta.Text = "VENDEDOR";
                    lblTipoDocOpera.Text = "Tipo Documento Vendedor";
                    lnlNomOpera.Text = "Nombre del Vendedor";
                    lblNoDocOPera.Text = "No. Documento Vendedor";
                    lblNomOperador.Text = e.Item.Cells[6].Text;
                    lblTipoDocOperador.Text = e.Item.Cells[16].Text;
                    lblNoDocumentoOperaqdor.Text = e.Item.Cells[18].Text;
                    ddlCapPunta.SelectedValue = "V";
                    //ddlCapVendedor.Enabled = false;  20180228 rq011-18
                    //ddlCapComprador.Enabled = true; 20180228 rq011-18
                    //TxtCapComprador.Enabled = false;
                }

                if (hdfEstadoAct.Value == "R" || hdfEstadoAct.Value == "D" || hdfEstadoAct.Value == "B")
                {
                    lsPuedeEntrar = "N";
                    lblMensaje.Text = "No se puede Ingresar o Actualizar la Información YA que el Contrato está en un estado NO válido para Modificación.!";
                }
                else
                {
                    if (lsCorreccion == "N")
                    {
                        if (lsIndModif == "S")
                        {
                            hndFechaMax.Value = lsFechaMaxMod; //20190215 ajuste
                            if (DateTime.Now > Convert.ToDateTime(lsFechaMaxMod))
                            {
                                lsPuedeEntrar = "N";
                                lblMensaje.Text = "No se puede Ingresar a Modificar la Información YA que se venció el plazo para hacerlo. {" + lsFechaMaxMod + "}!"; //20161207 rq102 conoformacion de rutas
                            }
                        }
                        else
                        {
                            hndFechaMax.Value = lsFechaMaxReg + " 23:59"; //20190215 ajuste
                            if (Convert.ToDateTime(lsFecha) > Convert.ToDateTime(lsFechaMaxReg))
                            {
                                lsPuedeEntrar = "N";
                                lblMensaje.Text = "No se puede Ingresar o Actualizar la Información YA que se venció el plazo para hacerlo. {" + lsFechaMaxReg + "}!";
                            }
                        }
                    }
                    else
                    {
                        hndFechaMax.Value = lsFechaMaxCor; //20190215 ajuste
                        if (DateTime.Now <= Convert.ToDateTime(lsFechaMaxCor)) //20190215 ajuste
                        {
                            lsPuedeEntrar = "S";
                        }
                        else
                        {
                            lsPuedeEntrar = "N";
                            lblMensaje.Text = "No se puede Ingresar o Corregir la Información YA que se venció el plazo para hacerlo. {" + lsFechaMaxCor + "}!";
                        }
                    }
                }
                lConexion.Abrir();
                /// Obtengo la Punta Valida para el operador que esta hacienda la Captura
                /// // 20170601 rq020-17
                if (Session["tipoPerfil"].ToString() == "N")
                {
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_punta_mayorista a, m_tipos_operador b", " a.codigo_tipo_operador = b.codigo_tipo_operador And b.sigla = '" + Session["TipoOperador"].ToString() + "' And a.estado = 'A' ");
                    if (!lLector.HasRows)
                    {
                        lsPuedeEntrar = "N";
                        lblMensaje.Text = "El Tipo de Operador " + Session["DescTipoOperador"].ToString() + " NO está parametrizado para el Merecado Mayorista.!"; //20161215 rq111 mayorista transporte
                    }
                    lLector.Close();
                    lLector.Dispose();
                }
                if (lsPuedeEntrar == "S")
                {
                    if (!manejo_bloqueo("V", hdfIdVerif.Value))
                    {
                        /// Validaciones Cuando el Que entra en la pantalla es negociador
                        if (hdfEstadoAct.Value != "R" && hdfEstadoAct.Value != "S")
                        {
                            if (e.Item.Cells[15].Text == "Nit")
                                lsCodDocC = "1";
                            else
                                lsCodDocC = "3";
                            if (e.Item.Cells[16].Text == "Nit")
                                lsCodDocV = "1";
                            else
                                lsCodDocV = "3";
                            lsCodOperaC = e.Item.Cells[10].Text + "-" + lsCodDocC + "-" + e.Item.Cells[17].Text + "-" + e.Item.Cells[5].Text;
                            lsCodOperaV = e.Item.Cells[11].Text + "-" + lsCodDocV + "-" + e.Item.Cells[18].Text + "-" + e.Item.Cells[6].Text;

                            lblPuntoE.Text = "Punto de Salida SNT";
                            if (ddlCapProducto.SelectedValue == "1") //20161215 rq111 mayorista transporte
                            {
                                lblCantidad.Text = "Cantidad Contratada  (MBTUD)";
                                lblPrecio.Text = "Precio de la energía a entregar (USD/MBTU)";
                                TxtCantidadT.Visible = false; //20170926 rq027-17
                            }
                            if (ddlCapProducto.SelectedValue == "2") //20161215 rq111 mayorista transporte  //20170926 rq027-17
                            {
                                lblCantidad.Text = "Capacidad Contratada  (KPCD)"; //20161215 rq111 mayorista transporte
                                lblPrecio.Text = "Precio de la energía a entregar (USD/KPC)"; //20161215 rq111 mayorista transporte
                                TxtCantidadT.Visible = false; //20170926 rq027-17
                            }
                            if (ddlCapProducto.SelectedValue == "3") //20170926 rq027-17
                            {
                                lblCantidad.Text = "Cantidad-Capacidad Contratada  (MBTUD-KPCD)";
                                lblPrecio.Text = "Precio de la energía a entregar (USD/(MBTU))";
                                TxtCantidadT.Visible = true;
                            }
                            lblFechaInc.Text = "Fecha de inicio del contrato";
                            lblFechaFin.Text = "Fecha de terminación del contrato";
                            ddlModalidad.Items.Clear();

                            ddlPuntoEntr.Items.Clear();
                            ddlMotivoModifi.Items.Clear();
                            ddlCapVendedor.Items.Clear();
                            //ddlCapProducto.Items.Clear();  //20161215 rq111 mayorista transporte
                            ddlSector.Items.Clear();
                            ddlMercadoRel.Items.Clear();
                            ddlDeptoPuntoSal.Items.Clear();
                            DdlMunPuntoSal.Items.Clear();
                            DdlCentro.Items.Clear(); //20170530 divipola
                            lConexion.Abrir();
                            /// LLeno los Controles
                            //LlenarControles(lConexion.gObjConexion, ddlCapProducto, "m_producto a, m_caracteristica_sub b", " a.estado = 'A' And b.estado = 'A' And a.codigo_producto = b.codigo_caracteristica and b.tipo_caracteristica = 'P' and b.codigo_tipo_subasta = 5 And b.tipo_mercado = 'S' And b.destino_rueda = 'G' order by descripcion", 0, 1);  //20161215 rq111 mayorista transporte
                            //LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_pozo", " estado = 'A'  order by descripcion", 0, 1);
                            LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_punto_salida_snt", " estado = 'A'  order by descripcion", 0, 2); // Cambio informacion punto de entrega para punto salida SNT

                            LlenarControles(lConexion.gObjConexion, ddlCapVendedor, "m_operador a, m_operador_punta_mayorista b, m_tipos_operador c", " a.estado = 'A' And a.tipo_operador = c.sigla And b.codigo_tipo_operador = c.codigo_tipo_operador And b.punta_contrato = 'V' And b.estado = 'A'  order by razon_social", 0, 4);
                            LlenarControles(lConexion.gObjConexion, ddlCapComprador, "m_operador a, m_operador_punta_mayorista b, m_tipos_operador c", " a.estado = 'A' And a.tipo_operador = c.sigla And b.codigo_tipo_operador = c.codigo_tipo_operador And b.punta_contrato = 'C' And b.estado = 'A'  order by razon_social", 0, 4);

                            LlenarControles(lConexion.gObjConexion, ddlModalidad, "m_modalidad_contractual", " estado = 'A' order by descripcion", 0, 1);
                            LlenarControles(lConexion.gObjConexion, ddlMotivoModifi, "m_motivos_modificacion", " estado = 'A' order by descripcion", 0, 1);

                            LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo", " estado = 'A' order by descripcion", 0, 1); //20160706
                            LlenarControles(lConexion.gObjConexion, ddlMercadoRel, "m_mercado_relevante", " estado = 'A' order by descripcion", 0, 1);
                            LlenarControles(lConexion.gObjConexion, ddlDeptoPuntoSal, "m_divipola", " codigo_ciudad='0' and estado = 'A' order by nombre_departamento", 1, 2);  //20170530 divipola
                            try
                            {
                                ddlCapVendedor.SelectedValue = lsCodOperaV;
                                if (lsCodOperaC[0].ToString().Trim() != "0")
                                    ddlCapComprador.SelectedValue = lsCodOperaC;
                                ddlCapTipoMercado.SelectedValue = hdfTipoMerc.Value;
                                //ddlCapProducto.SelectedValue = hdfCodProd.Value + "-" + hdfDestinoRueda.Value; //20161215 rq111 mayorista trnasporte
                                TxtCapComprador.Text = e.Item.Cells[17].Text + "-" + lsCodDocC + "-" + e.Item.Cells[5].Text;
                                //ddlProducto.Enabled = false;  //20161215 rq111 mayorista transaporte
                                ddlCapProducto.Enabled = false;  //20161215 rq111 mayorista transaporte
                            }
                            catch (Exception)
                            {

                            }
                            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_contrato_datos_verif", " codigo_verif_contrato = " + hdfIdVerif.Value + " And punta_contrato = '" + hdfPunta.Value + "' ");
                            if (lLector.HasRows)
                            {
                                hdfAccion.Value = "M";
                                TrModifi.Visible = true;
                                if (Session["tipoPerfil"].ToString() == "N")
                                {
                                    btnCrear.Visible = false;
                                    btnActualizar.Visible = true;
                                }
                                else
                                {
                                    btnCrear.Visible = false;
                                    btnActualizar.Visible = true; //20170601 rq020-17
                                }
                                /// Recuperacion Datos
                                lLector.Read();
                                TxtCapComprador.Text = lLector["no_identifica_punta_contr"].ToString();
                                TxtNumContrato.Text = lLector["contrato_definitivo"].ToString();
                                TxtFechaSus.Text = lLector["fecha_suscripcion_cont"].ToString().Substring(6, 4) + "/" + lLector["fecha_suscripcion_cont"].ToString().Substring(3, 2) + "/" + lLector["fecha_suscripcion_cont"].ToString().Substring(0, 2);
                                try
                                {
                                    ddlModalidad.SelectedValue = lLector["codigo_modalidad"].ToString();
                                    ddlPuntoEntr.SelectedValue = lLector["codigo_punto_entrega"].ToString();
                                }
                                catch (Exception)
                                {

                                }
                                TxtCantidad.Text = lLector["cantidad"].ToString();
                                TxtCantidadT.Text = lLector["capacidad_transporte"].ToString();//20170926 rq027-17
                                TxtPrecio.Text = lLector["precio"].ToString();
                                TxtFechaInicial.Text = lLector["fecha_inicial"].ToString().Substring(6, 4) + "/" + lLector["fecha_inicial"].ToString().Substring(3, 2) + "/" + lLector["fecha_inicial"].ToString().Substring(0, 2);
                                TxtFechaFinal.Text = lLector["fecha_final"].ToString().Substring(6, 4) + "/" + lLector["fecha_final"].ToString().Substring(3, 2) + "/" + lLector["fecha_final"].ToString().Substring(0, 2);
                                hdfRutaArchivo.Value = lLector["ruta_contrato"].ToString();
                                TxtCapTipoGarantia.Text = lLector["tipo_garantia"].ToString();
                                TxtCapValorGarantia.Text = lLector["valor_garantia"].ToString();
                                // 20161215 rq111 mayorista transporte
                                try
                                {
                                    ddlRuta.SelectedValue = lLector["codigo_ruta_may"].ToString();
                                }
                                catch (Exception)
                                {
                                    ddlRuta.SelectedValue = "0";
                                }

                                TxtCapFecPlazo.Text = lLector["fecha_pago"].ToString().Substring(6, 4) + "/" + lLector["fecha_pago"].ToString().Substring(3, 2) + "/" + lLector["fecha_pago"].ToString().Substring(0, 2);
                                try
                                {
                                    ddlSector.SelectedValue = lLector["codigo_sector_consumo"].ToString();
                                    ddlUsuSnt.SelectedValue = lLector["usuario_no_reg_snt"].ToString();
                                    ddlDeptoPuntoSal.SelectedValue = lLector["codigo_depto_punto_sal"].ToString();
                                    lsCodMpio = lLector["codigo_municipio_pto_sal"].ToString();
                                    lsCodCentro = lLector["codigo_centro_pob"].ToString(); //20170530 divipola
                                    ddlMercadoRel.SelectedValue = lLector["codigo_mercado_relev_sistema"].ToString();
                                }
                                catch (Exception)
                                {

                                }
                                if (ddlUsuSnt.SelectedValue == "S")
                                {
                                    TrPuntaSal.Visible = true;
                                    TrSisDist.Visible = false;
                                }
                                else
                                {
                                    TrPuntaSal.Visible = false;
                                    TrSisDist.Visible = true;
                                }
                                hdfusuarioSnt.Value = ddlUsuSnt.SelectedValue;
                                lLector.Close();//2018028 rq011-18
                                lLector.Dispose();//2018028 rq011-18
                                //2018028 rq011-18
                                if (lsCorreccion == "S")
                                {
                                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_contrato_datos_verif", " codigo_verif_contrato = " + hdfIdVerif.Value + " And punta_contrato <> '" + hdfPunta.Value + "' ");
                                    if (lLector.HasRows)
                                    {
                                        lLector.Read();
                                        TxtCapFechaNeg.Enabled = false;
                                        if (TxtNumContrato.Text == lLector["contrato_definitivo"].ToString())
                                            TxtNumContrato.Enabled = false;
                                        if (TxtFechaSus.Text == lLector["fecha_suscripcion_cont"].ToString().Substring(6, 4) + "/" + lLector["fecha_suscripcion_cont"].ToString().Substring(3, 2) + "/" + lLector["fecha_suscripcion_cont"].ToString().Substring(0, 2))
                                            TxtFechaSus.Enabled = false;
                                        if (ddlModalidad.SelectedValue == lLector["codigo_modalidad"].ToString())
                                            ddlModalidad.Enabled = false;
                                        if (ddlPuntoEntr.SelectedValue == lLector["codigo_punto_entrega"].ToString())
                                            ddlPuntoEntr.Enabled = false;
                                        if (TxtCantidad.Text == lLector["cantidad"].ToString())
                                            TxtCantidad.Enabled = false;
                                        if (TxtCantidadT.Text == lLector["capacidad_transporte"].ToString())
                                            TxtCantidadT.Enabled = false;
                                        if (TxtPrecio.Text == lLector["precio"].ToString())
                                            TxtPrecio.Enabled = false;
                                        if (TxtFechaInicial.Text == lLector["fecha_inicial"].ToString().Substring(6, 4) + "/" + lLector["fecha_inicial"].ToString().Substring(3, 2) + "/" + lLector["fecha_inicial"].ToString().Substring(0, 2))
                                            TxtFechaInicial.Enabled = false;
                                        if (TxtFechaFinal.Text == lLector["fecha_final"].ToString().Substring(6, 4) + "/" + lLector["fecha_final"].ToString().Substring(3, 2) + "/" + lLector["fecha_final"].ToString().Substring(0, 2))
                                            TxtFechaFinal.Enabled = false;
                                        if (TxtCapTipoGarantia.Text == lLector["tipo_garantia"].ToString())
                                            TxtCapTipoGarantia.Enabled = false;
                                        if (TxtCapValorGarantia.Text == lLector["valor_garantia"].ToString())
                                            TxtCapValorGarantia.Enabled = false;
                                        if (ddlRuta.SelectedValue == lLector["codigo_ruta_may"].ToString())
                                            ddlRuta.Enabled = false;
                                        if (TxtCapFecPlazo.Text == lLector["fecha_pago"].ToString().Substring(6, 4) + "/" + lLector["fecha_pago"].ToString().Substring(3, 2) + "/" + lLector["fecha_pago"].ToString().Substring(0, 2))
                                            TxtCapFecPlazo.Enabled = false;
                                        if (ddlSector.SelectedValue == lLector["codigo_sector_consumo"].ToString())
                                            ddlSector.Enabled = false;
                                        if (ddlUsuSnt.SelectedValue == lLector["usuario_no_reg_snt"].ToString())
                                            ddlUsuSnt.Enabled = false;
                                        if (ddlDeptoPuntoSal.SelectedValue == lLector["codigo_depto_punto_sal"].ToString())
                                            ddlDeptoPuntoSal.Enabled = false;
                                        if (lsCodMpio == lLector["codigo_municipio_pto_sal"].ToString())
                                            DdlMunPuntoSal.Enabled = false;
                                        if (lsCodCentro == lLector["codigo_centro_pob"].ToString())
                                            DdlCentro.Enabled = false;
                                        if (ddlMercadoRel.SelectedValue == lLector["codigo_mercado_relev_sistema"].ToString())
                                            ddlMercadoRel.Enabled = false;
                                        lLector.Close();//2018028 rq011-18
                                        lLector.Dispose();//2018028 rq011-18
                                    }
                                }

                            }
                            else
                            {
                                lLector.Close();
                                lLector.Dispose();
                                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_contrato_datos_verif", " codigo_verif_contrato = " + hdfIdVerif.Value + " And punta_contrato = '" + lsPuntaCont + "' ");
                                if (lLector.HasRows)
                                {
                                    lLector.Read();
                                    TxtCapFechaNeg.Enabled = false;  //20180228 rq011-18
                                    TxtNumContrato.Text = lLector["contrato_definitivo"].ToString();
                                    TxtFechaSus.Text = lLector["fecha_suscripcion_cont"].ToString().Substring(6, 4) + "/" + lLector["fecha_suscripcion_cont"].ToString().Substring(3, 2) + "/" + lLector["fecha_suscripcion_cont"].ToString().Substring(0, 2);
                                    try
                                    {
                                        ddlModalidad.SelectedValue = lLector["codigo_modalidad"].ToString();
                                        ddlPuntoEntr.SelectedValue = lLector["codigo_punto_entrega"].ToString();
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    TxtCantidad.Text = lLector["cantidad"].ToString();
                                    TxtCantidadT.Text = lLector["capacidad_transporte"].ToString(); //20170926 rq027-17
                                    TxtPrecio.Text = lLector["precio"].ToString();
                                    TxtFechaInicial.Text = lLector["fecha_inicial"].ToString().Substring(6, 4) + "/" + lLector["fecha_inicial"].ToString().Substring(3, 2) + "/" + lLector["fecha_inicial"].ToString().Substring(0, 2);
                                    TxtFechaFinal.Text = lLector["fecha_final"].ToString().Substring(6, 4) + "/" + lLector["fecha_final"].ToString().Substring(3, 2) + "/" + lLector["fecha_final"].ToString().Substring(0, 2);
                                    TxtCapTipoGarantia.Text = lLector["tipo_garantia"].ToString();
                                    TxtCapValorGarantia.Text = lLector["valor_garantia"].ToString();
                                    // 20161215 rq111 mayorista transporte
                                    try
                                    {
                                        ddlRuta.SelectedValue = lLector["codigo_ruta_may"].ToString();
                                    }
                                    catch (Exception)
                                    {
                                        ddlRuta.SelectedValue = "0";
                                    }
                                    TxtCapFecPlazo.Text = lLector["fecha_pago"].ToString().Substring(6, 4) + "/" + lLector["fecha_pago"].ToString().Substring(3, 2) + "/" + lLector["fecha_pago"].ToString().Substring(0, 2);
                                    try
                                    {
                                        ddlSector.SelectedValue = lLector["codigo_sector_consumo"].ToString();
                                        ddlUsuSnt.SelectedValue = lLector["usuario_no_reg_snt"].ToString();
                                        ddlDeptoPuntoSal.SelectedValue = lLector["codigo_depto_punto_sal"].ToString();
                                        lsCodMpio = lLector["codigo_municipio_pto_sal"].ToString();
                                        lsCodCentro = lLector["codigo_centro_pob"].ToString(); //20170530 divipola
                                        ddlMercadoRel.SelectedValue = lLector["codigo_mercado_relev_sistema"].ToString();
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    if (ddlUsuSnt.SelectedValue == "S")
                                    {
                                        TrPuntaSal.Visible = true;
                                        TrSisDist.Visible = false;
                                    }
                                    else
                                    {
                                        TrPuntaSal.Visible = false;
                                        TrSisDist.Visible = true;
                                    }
                                    hdfusuarioSnt.Value = ddlUsuSnt.SelectedValue;
                                }
                                TrModifi.Visible = false;
                                hdfAccion.Value = "C";
                                if (Session["tipoPerfil"].ToString() == "N")
                                {
                                    btnCrear.Visible = true;
                                    btnActualizar.Visible = false;
                                }
                                else
                                {
                                    btnCrear.Visible = false;
                                    btnActualizar.Visible = true; //20170601 rq020-17
                                }
                                lLector.Close();//2018028 rq011-18
                                lLector.Dispose();//2018028 rq011-18
                            }
                            lLector.Close();
                            lLector.Dispose();
                            //20170530 divipola se baja para mantener la conexion
                            //LlenarControles(lConexion.gObjConexion, DdlMunPuntoSal, "m_divipola", " estado = 'A' And codigo_ciudad != '0' And codigo_departamento = " + ddlDeptoPuntoSal.SelectedValue + " and codigo_centro ='0'  order by nombre_ciudad", 3, 4); //20170530 divipola
                            //try
                            //{
                            //    DdlMunPuntoSal.SelectedValue = lsCodMpio;
                            //    DdlMunPuntoSal_SelectedIndexChanged(null, null);  //20170530 divipola
                            //    DdlCentro.SelectedValue = lsCodCentro; //20170530 divipola
                            //}
                            //catch (Exception ex)
                            //{

                            //}
                            /// Obtengo el usuario No Regulado
                            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + TxtCapComprador.Text + "'");
                            if (lLector.HasRows)
                            {
                                lLector.Read();
                                TxtCapComprador.Text = lLector["no_documento"].ToString() + " - " + lLector["codigo_tipo_doc"].ToString() + " - " + lLector["nombre"].ToString();
                            }
                            lLector.Close();
                            lLector.Dispose();
                            //20170530 divipola  se baja para mantenerla conexión
                            LlenarControles(lConexion.gObjConexion, DdlMunPuntoSal, "m_divipola", " estado = 'A' And codigo_ciudad != '0' And codigo_departamento = " + ddlDeptoPuntoSal.SelectedValue + " and codigo_centro ='0'  order by nombre_ciudad", 3, 4); //20170530 divipola
                            try
                            {
                                DdlMunPuntoSal.SelectedValue = lsCodMpio;
                                DdlMunPuntoSal_SelectedIndexChanged(null, null);  //20170530 divipola
                                DdlCentro.SelectedValue = lsCodCentro; //20170530 divipola
                            }
                            catch (Exception)
                            {

                            }
                            lConexion.Cerrar();
                            tblGrilla.Visible = false;
                            tblRegCnt.Visible = true;
                            tblDatos.Visible = false;
                        }
                        else
                            lblMensaje.Text = "El Registro No Puede ser Modificado, Ya que está en Un estado NO Válido para Modificación.!";
                    }
                    else
                        lblMensaje.Text = "No se puede Modificar el Registro YA que se encuentra Bloqueado por el Administrador.!";
                }
                //else
                //    lblMensaje.Text = "No se puede Ingresar o Actualizar la Información YA que se vencio el plazo para hacerlo.!";
                //ddlPuntoEntr_SelectedIndexChanged(null, null); //20161225 rq111 OTMM Mayorista //20180227 rq011-18
                //ddlRuta_SelectedIndexChanged(null, null); //20161225 rq111 OTMM Mayorista //20180227 rq011-18
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas en la Recuperación de la Información. " + ex.Message.ToString();

            }
        }
        /// Realiza la visualizacion de la Pantalla de Verificacion
        if (((LinkButton)e.CommandSource).Text == "Verificar")
        {
            string[] lsNombreParametros = { "@P_codigo_cont_datos", "@P_codigo_verif_contrato", "@P_punta_contrato" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
            Object[] lValorParametros = { "0", "0", "" };
            string lsVerifica = "1"; // 1=Correcta 0=Incorrecta
            string lsPuntaC = "N";
            try
            {
                TxtObservacion.Text = "";
                lblOperacion.Text = e.Item.Cells[0].Text;
                lblProducto.Text = e.Item.Cells[4].Text;
                hdfIdVerif.Value = e.Item.Cells[9].Text;
                hdfCodTipoSub.Value = e.Item.Cells[14].Text;
                hdfDestinoRueda.Value = e.Item.Cells[20].Text;
                hdfEstadoAct.Value = e.Item.Cells[21].Text;
                hdfTipoMerc.Value = e.Item.Cells[22].Text;
                hdfOPeraC.Value = e.Item.Cells[10].Text;
                hdfOPeraV.Value = e.Item.Cells[11].Text;

                //// Obtengo la Informacion del Comprador
                lConexion.Abrir();
                lValorParametros[1] = hdfIdVerif.Value;
                lValorParametros[2] = "C";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetRegistroContratosDatos", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error == "")
                {
                    if (lLector.HasRows)
                    {
                        lsPuntaC = "S";
                        lLector.Read();
                        lblDat1C.Text = lLector["contrato_definitivo"].ToString().Trim().ToUpper();
                        lblDat2C.Text = lLector["fecha_suscripcion_cont"].ToString().Trim();
                        lblDat3C.Text = lLector["codigo_modalidad"].ToString() + " - " + lLector["desc_modalidad"].ToString().Trim();
                        lblDat4C.Text = lLector["codigo_punto_entrega"].ToString() + " - " + lLector["desc_punto_ent"].ToString().Trim();
                        lblDat5C.Text = lLector["cantidad"].ToString().Trim();
                        lblDat5CX.Text = lLector["capacidad_transporte"].ToString().Trim();  //20170926 rq027-17
                        lblDat6C.Text = lLector["precio"].ToString().Trim();
                        lblDat7C.Text = lLector["fecha_inicial"].ToString().Trim();
                        lblDat8C.Text = lLector["fecha_final"].ToString().Trim();
                        lblDat9C.Text = lLector["sentido_flujo"].ToString().Trim();
                        lblDat10C.Text = lLector["presion_punto_fin"].ToString().Trim();
                        if (lLector["ruta_contrato"].ToString().Trim().Length > 0)
                            LinkC.HRef = "../archivos/" + lLector["ruta_contrato"].ToString().Trim();
                        else
                            LinkC.Visible = false;
                    }
                }
                else
                {
                    lblMensaje.Text = "Problemas al Obtener los Datos de la Compra.! " + goInfo.mensaje_error.ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + lblMensaje.Text + "');", true);
                }
                lLector.Close();
                lLector.Dispose();
                //// Obtengo los Datos de la Venta
                lValorParametros[2] = "V";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetRegistroContratosDatos", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error == "")
                {
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lblDat1V.Text = lLector["contrato_definitivo"].ToString().Trim().ToUpper();
                        lblDat2V.Text = lLector["fecha_suscripcion_cont"].ToString().Trim();
                        lblDat3V.Text = lLector["codigo_modalidad"].ToString().Trim() + " - " + lLector["desc_modalidad"].ToString().Trim();
                        lblDat4V.Text = lLector["codigo_punto_entrega"].ToString().Trim() + " - " + lLector["desc_punto_ent"].ToString().Trim();
                        lblDat5V.Text = lLector["cantidad"].ToString().Trim();
                        lblDat5VX.Text = lLector["capacidad_transporte"].ToString().Trim();  //20170926 rq027-17
                        lblDat6V.Text = lLector["precio"].ToString().Trim();
                        lblDat7V.Text = lLector["fecha_inicial"].ToString().Trim();
                        lblDat8V.Text = lLector["fecha_final"].ToString().Trim();
                        lblDat9V.Text = lLector["sentido_flujo"].ToString().Trim();
                        lblDat10V.Text = lLector["presion_punto_fin"].ToString().Trim();
                        if (lLector["ruta_contrato"].ToString().Trim().Length > 0)
                            LinkV.HRef = "../archivos/" + lLector["ruta_contrato"].ToString().Trim();
                        else
                            LinkV.Visible = false;
                    }
                }
                else
                {
                    lblMensaje.Text = "Problemas al Obtener los Datos de la Compra.! " + goInfo.mensaje_error.ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + lblMensaje.Text + "');", true);
                }
                lLector.Close();
                lLector.Dispose();
                lblDat1Ver.Text = "Correcta";
                lblDat2Ver.Text = "Correcta";
                lblDat3Ver.Text = "Correcta";
                lblDat4Ver.Text = "Correcta";
                lblDat5Ver.Text = "Correcta";
                lblDat5VerX.Text = "Correcta";  //20170926  rq027-17
                lblDat6Ver.Text = "Correcta";
                lblDat7Ver.Text = "Correcta";
                lblDat8Ver.Text = "Correcta";
                lblDat9Ver.Text = "Correcta";
                lblDat10Ver.Text = "Correcta";
                /// Realizo la Verificacion entre los Datos de la Compra y la Venta
                TrTra1.Visible = false;
                TrTra2.Visible = false;
                lblTit1.Text = "Número del contrato";
                lblTit2.Text = "Fecha de suscripción del contrato";
                lblTit3.Text = "Modalidad de Contrato";
                lblTit4.Text = "Punto de entrega de la energía al comprador";
                if (ddlCapProducto.SelectedValue == "1") //20161215 rq111 OTMM mayprista
                {
                    lblTit5.Text = "Cantidad de energía contratada (MBTUD)";
                    lblTit6.Text = "Precio a la fecha de suscripción del contrato (USD/MBTU)";
                    trSumTra.Visible = false; //20161215 rq111 OTMM mayprista
                }
                if (ddlCapProducto.SelectedValue == "2") //20161215 rq111 OTMM mayprista  //20170926 rq027-17
                {
                    lblTit5.Text = "Capacidad de energía contratada (KPCD)";  //20161215 rq111 OTMM mayprista
                    lblTit6.Text = "Precio a la fecha de suscripción del contrato (USD/KPC)";  //20161215 rq111 OTMM mayprista
                    trSumTra.Visible = false; //20161215 rq111 OTMM mayprista
                }
                //20170926 rq027-17
                if (ddlCapProducto.SelectedValue == "3")
                {
                    lblTit5.Text = "Cantidad de energía contratada (MBTUD)";
                    lblTit5X.Text = "Capacidad de energía contratada (KPCD)";
                    lblTit6.Text = "Precio a la fecha de suscripción del contrato (USD/(MBTU-KPC))";
                    trSumTra.Visible = true;
                }
                lblTit7.Text = "Fecha de inicio de la obligación de entrega";
                lblTit8.Text = "Fecha de terminación de la obligación de entrega";
                lblTit11.Text = "Copia Contrato";
                if (lblDat1V.Text != lblDat1C.Text)
                {
                    lsVerifica = "0";
                    lblDat1Ver.Text = "Incorrecta";
                }
                if (lblDat2V.Text != lblDat2C.Text)
                {
                    lsVerifica = "0";
                    lblDat2Ver.Text = "Incorrecta";
                }
                if (lblDat3V.Text != lblDat3C.Text)
                {
                    lsVerifica = "0";
                    lblDat3Ver.Text = "Incorrecta";
                }
                if (lblDat4V.Text != lblDat4C.Text)
                {
                    lsVerifica = "0";
                    lblDat4Ver.Text = "Incorrecta";
                }
                if (lblDat5V.Text != lblDat5C.Text)
                {
                    lsVerifica = "0";
                    lblDat5Ver.Text = "Incorrecta";
                }
                //20170926 rq027-17
                if (lblDat5VX.Text != lblDat5CX.Text)
                {
                    lsVerifica = "0";
                    lblDat5VerX.Text = "Incorrecta";
                }
                if (lblDat6V.Text != lblDat6C.Text)
                {
                    lsVerifica = "0";
                    lblDat6Ver.Text = "Incorrecta";
                }
                if (lblDat7V.Text != lblDat7C.Text)
                {
                    lsVerifica = "0";
                    lblDat7Ver.Text = "Incorrecta";
                }
                if (lblDat8V.Text != lblDat8C.Text)
                {
                    lsVerifica = "0";
                    lblDat8Ver.Text = "Incorrecta";
                }
                if (lsVerifica == "0")
                {
                    if (lsPuntaC == "S")
                        btnRegCont.Visible = false;
                }
                else
                    btnRegCont.Visible = true;
                tblGrilla.Visible = false;
                tblVerifica.Visible = true;
                tblDatos.Visible = false;
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas en la Recuperación de la Información. " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_operador)
    {
        string lsCondicion = "nombre_tabla='t_contrato_verificacion' and llave_registro='codigo_verif_contrato=" + lscodigo_operador + "'";
        string lsCondicion1 = "codigo_verif_contrato=" + lscodigo_operador.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "t_contrato_verificacion";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_contrato_verificacion", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Realiza las Validaciones de los Tipos de Datos y Campos Obligatorios de la Pantalla
    /// </summary>
    /// <returns></returns>
    protected string validaciones()
    {
        decimal ldValor = 0;
        int liValor = 0;
        DateTime lFecha;
        string lsError = "";
        string[] lsPrecio;
        string[] lsFechaV;
        string[] lsOperador; //20171130 rq026-17
        string lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "/" + DateTime.Now.ToShortDateString().Substring(3, 2) + "/" + DateTime.Now.ToShortDateString().Substring(0, 2);

        try
        {
            if (TxtCapFechaNeg.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar la Fecha de Negociación. <br>";
            else
            {
                try
                {
                    lFecha = Convert.ToDateTime(TxtCapFechaNeg.Text.Trim());
                    lsFechaV = TxtCapFechaNeg.Text.Trim().Split('/');
                    if (lsFechaV[0].Trim().Length != 4)
                        lsError += "Formato Inválido en Fecha de Negociación. <br>";
                    else
                    {
                        if (lFecha > Convert.ToDateTime(lsFecha))
                            lsError += "La Fecha de Negociación NO puede ser mayor a la fecha del día. <br>";
                        else
                        {
                            if (lFecha < Convert.ToDateTime("2015/05/01"))
                                lsError += "La Fecha de Negociación debe ser mayor o igual al 5 de Enero del 2015. <br>";
                        }
                    }

                }
                catch (Exception)
                {
                    lsError += "Valor Inválido en Fecha de Negociación. <br>";
                }
            }
            if (ddlCapPunta.SelectedValue == "V")
            {
                if (!ChekOtro.Checked)
                {
                    if (ddlCapComprador.SelectedValue == "0")
                        lsError += "Debe Ingresar el Operador Comprador <br>";
                }
                else
                {
                    if (ddlCapComprador.SelectedValue != "0")
                        lsError += "NO Debe Seleccionar el Operador Compra, YA que seleccionó OTRO.  <br>";

                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + TxtCapComprador.Text.Trim() + "' ");
                    if (!lLector.HasRows)
                        lsError += "El Usuario No Regulado Ingresado NO existe en la Base de Datos.! <br>";
                    else
                    {
                        lLector.Read();
                        hdfNomComp.Value = lLector["nombre"].ToString();
                        hdfTipoDocCom.Value = lLector["codigo_tipo_doc"].ToString();
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                }
            }
            else
            {
                if (ddlCapVendedor.SelectedValue == "0")
                    lsError += "Debe Ingresar el Vendedor (Comercializador)  <br>";
            }
            if (ddlCapProducto.SelectedValue == "0")
                lsError += "Debe Seleccionar el Producto. <br>";
            if (ddlCapTipoMercado.SelectedValue == "0")
                lsError += "Debe Seleccionar Tipo del Mercado. <br>";
            if (TxtNumContrato.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar el No. de Contrato. <br>";
            if (TxtFechaSus.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar la Fecha de Suscripción. <br>";
            else
            {
                try
                {
                    lFecha = Convert.ToDateTime(TxtFechaSus.Text.Trim());
                    lsFechaV = TxtFechaSus.Text.Trim().Split('/');
                    if (lsFechaV[0].Trim().Length != 4)
                        lsError += "Formato Inválido en Fecha de Suscripción. <br>";

                    else
                    {
                        if (lFecha < Convert.ToDateTime("2015/05/01"))
                            lsError += "La Fecha de Suscripción debe ser mayor o igual al 5 de Enero del 2015. <br>";
                        else
                        {
                            if (lFecha < Convert.ToDateTime(TxtCapFechaNeg.Text.Trim()))
                                lsError += "La Fecha de Suscripción debe ser mayor o igual a la Fecha de Negociación. <br>";
                        }
                        if (lblOperacion.Text == "") //20190215 ajuste
                        {
                            lConexion.Abrir();
                            string[] lsNombreParametrosC = { "@P_cadena" };
                            SqlDbType[] lTipoparametrosC = { SqlDbType.VarChar };
                            string[] lValorParametrosC = { " Select convert(varchar(10),dbo.FC_getFechaMasDias1(convert(datetime,'" + TxtFechaSus.Text.Trim() + "'),no_dias_hab_regi_cont_mayor,'S'),111) as FechaMax From m_parametros_generales " };
                            /// Se obtiene los Datos de la Punta Compradora
                            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_EjecutarCadena", lsNombreParametrosC, lTipoparametrosC, lValorParametrosC);
                            lLector.Read();
                            hndFechaMax.Value = lLector["FechaMax"].ToString() + " 23:59";
                            lLector.Close();
                            lLector.Dispose();
                            lConexion.Cerrar();
                        }
                        //if (Convert.ToDateTime(lsFecha) > Convert.ToDateTime(lsFechaMax)) //20190215 fin ajuste
                        if (DateTime.Now > Convert.ToDateTime(hndFechaMax.Value)) //20190215 fin ajuste
                            lsError += "La Fecha de Registro del Contrato está por fuera de los límites {" + hndFechaMax.Value + "}. <br>"; //20180126 rq107-16  //20190215 fin ajuste
                    }
                }
                catch (Exception)
                {
                    lsError += "Valor Inválido en Fecha de Suscripción. <br>";
                }
            }
            if (ddlModalidad.SelectedValue == "0" && TxtOtrModalidad.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar la Modalidad de Contrato. <br>";
            if (ddlPuntoEntr.SelectedValue == "0" && TxtOtrPuntoE.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar el Punto de Salida del SNT. <br>";
            if (TxtCantidad.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar " + lblCantidad.Text + " . <br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCantidad.Text.Trim());
                    //20170926 rq027-17
                    if (liValor <= 0)
                        lsError += lblCantidad.Text + " Debe ser mayor que cero. <br>";
                }
                catch (Exception)
                {
                    lsError += "Valor Inválido en " + lblCantidad.Text + ". <br>";
                }
            }
            //20170926 rq027-17
            if (ddlCapProducto.SelectedValue == "3")
            {
                if (TxtCantidadT.Text.Trim().Length <= 0)
                    lsError += "Debe Ingresar la capacidad de transporte. <br>";
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidadT.Text.Trim());
                        if (liValor <= 0)
                            lsError += "La capacidad de transporte debe ser mayor que cero. <br>";
                    }
                    catch (Exception)
                    {
                        lsError += "Valor Inválido en la capacidad de transporte. <br>";
                    }
                }
            }

            if (TxtPrecio.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar " + lblPrecio.Text + " . <br>";
            else
            {
                try
                {
                    ldValor = Convert.ToDecimal(TxtPrecio.Text.Trim());
                    if (ldValor < 0)
                        lsError += "Valor Inválido en " + lblPrecio.Text + ". <br>";
                    else
                    {
                        lsPrecio = TxtPrecio.Text.Trim().Split('.');
                        if (lsPrecio.Length > 1)
                        {
                            if (lsPrecio[1].Trim().Length > 2)
                                lsError += "Valor Inválido en " + lblPrecio.Text + ". <br>";
                        }
                    }
                }
                catch (Exception)
                {
                    lsError += "Valor Inválido en " + lblPrecio.Text + ". <br>";
                }
            }
            if (TxtFechaInicial.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar " + lblFechaInc.Text + ". <br>";
            else
            {
                try
                {
                    lFecha = Convert.ToDateTime(TxtFechaInicial.Text.Trim());
                    lsFechaV = TxtFechaInicial.Text.Trim().Split('/');
                    if (lsFechaV[0].Trim().Length != 4)
                        lsError += "Formato Inválido en " + lblFechaInc.Text + ". <br>";
                    else
                    {
                        if (lFecha < Convert.ToDateTime(TxtCapFechaNeg.Text.Trim()))
                            lsError += "La " + lblFechaInc.Text + " No puede ser menor a la fecha de Negociación. <br>";

                    }
                }
                catch (Exception)
                {
                    lsError += "Valor Inválido en " + lblFechaInc.Text + ". <br>";
                }
            }
            if (TxtFechaFinal.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar " + lblFechaFin.Text + ". <br>";
            else
            {
                try
                {
                    lFecha = Convert.ToDateTime(TxtFechaFinal.Text.Trim());
                    lsFechaV = TxtFechaInicial.Text.Trim().Split('/');
                    if (lsFechaV[0].Trim().Length != 4)
                        lsError += "Formato Inválido en " + lblFechaFin.Text + ". <br>";
                    else
                    {
                        if (lFecha < Convert.ToDateTime(TxtFechaInicial.Text.Trim()))
                            lsError += "La " + lblFechaInc.Text + " No puede ser menor a la " + lblFechaInc.Text + ". <br>";
                    }
                }
                catch (Exception)
                {
                    lsError += "Valor Inválido en " + lblFechaFin.Text + ". <br>";
                }
            }
            if (hdfAccion.Value == "C")
            {
                //if (FuCopiaCont.FileName.Trim().Length <= 0)
                //    lsError += "Debe Seleccionar el Archivo del Contrato. <br>";
            }
            else
            {
                if (ddlMotivoModifi.SelectedValue == "0")
                    lsError += "Debe Seleccionar el Motivo de Modificación. <br>";
            }

            if (TxtCapTipoGarantia.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar el Tipo de Garantía. <br>";
            if (TxtCapValorGarantia.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar el Valor de Garantía. <br>";
            else
            {
                try
                {
                    ldValor = Convert.ToDecimal(TxtCapValorGarantia.Text.Trim());
                    if (ldValor < 0)
                        lsError += "Valor Inválido en Valor de Garantía. <br>";
                }
                catch (Exception)
                {
                    lsError += "Valor Inválido en Valor de Garantía. <br>";
                }
            }
            if (TxtCapFecPlazo.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar Plazo Para realizar el Pago. <br>";
            else
            {
                try
                {
                    lFecha = Convert.ToDateTime(TxtCapFecPlazo.Text.Trim());
                    if (lFecha < Convert.ToDateTime(TxtCapFechaNeg.Text.Trim()))
                        lsError += "El Plazo Para realizar el Pago NO puede ser Menor a la Fecha de Negociación. <br>";
                }
                catch (Exception)
                {
                    lsError += "Valor Inválido en Plazo Para realizar el Pago. <br>";
                }
            }
            if (ddlSector.SelectedValue == "0")
                lsError += "Debe Seleccionar el sector de consumo. <br>";

            if (ddlUsuSnt.SelectedValue == "0")
                lsError += "Debe Seleccionar el Usuario no Regulado con conectado a SNT. <br>";
            else
            {
                if (hdfusuarioSnt.Value == "S")
                {
                    if (ddlDeptoPuntoSal.SelectedValue == "0")
                        lsError += "Debe Seleccionar el Departamento punto de salida. <br>";
                    if (DdlMunPuntoSal.SelectedValue == "0")
                        lsError += "Debe Seleccionar el Municipio punto de salida. <br>";
                    //20170530 divipola
                    if (DdlCentro.SelectedValue == "0")
                        lsError += "Debe Seleccionar el centro poblado. <br>";
                }
                else
                {
                    if (ddlMercadoRel.SelectedValue == "0")
                        lsError += "Debe Seleccionar Mercado Relevante. <br>";
                }
            }
            //20161215 rq111 mayorista transporte
            if ((ddlCapProducto.SelectedValue == "2" || ddlCapProducto.SelectedValue == "3") && ddlRuta.SelectedValue == "0")  //20170926 rq027-17
                lsError += "Debe seleccionar la ruta contratada. <br>";
            //20161215 rq111 mayorista transporte  
            //20180227 rq011-18 
            //if ((ddlCapProducto.SelectedValue == "2" || ddlCapProducto.SelectedValue == "3") && ddlRuta.SelectedValue != "0" && ddlPuntoEntr.SelectedValue != "0")  //20170926 rq027-17
            //{
            //    lConexion.Abrir();
            //    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_punto_salida_snt pto, m_ruta_snt_det rut", " pto.codigo_punto_salida = " + ddlPuntoEntr.SelectedValue + " and rut.codigo_ruta =" + ddlRuta.SelectedValue + " and pto.codigo_tramo = rut.codigo_tramo");
            //    if (!lLector.HasRows) 
            //        lsError += "El punto de salida no está dentro de la ruta contratada.<br>";
            //    lLector.Close();
            //    lLector.Dispose();
            //    lConexion.Cerrar();
            //}
            //20160810 valida fecha
            try
            {
                if (Convert.ToDateTime(TxtCapFechaNeg.Text) > Convert.ToDateTime(TxtFechaInicial.Text)) //20170530 divipola
                    lsError += "La fecha Inicial debe ser mayor o igual que la fecha de negociación. <br>"; //20170530 divipola
            }
            catch (Exception)
            {
            }
            //validaciones rq026-15 20171130
            string[] lsNombreParametrosVal = { "@P_contrato_definitivo", "@P_numero_contrato", "@P_operador_venta", "@P_operador_compra", "@P_destino_rueda", "@P_fecha_inicial", "@P_fecha_final", "@P_fecha_suscripcion" };
            SqlDbType[] lTipoparametrosVal = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametrosVal = { TxtNumContrato.Text, lblOperacion.Text, "0", "0", ddlCapProducto.SelectedValue, TxtFechaInicial.Text, TxtFechaFinal.Text, TxtFechaSus.Text };
            if (lValorParametrosVal[1] == "")
                lValorParametrosVal[1] = "0";
            lsOperador = ddlCapVendedor.SelectedValue.Split('-');
            lValorParametrosVal[2] = lsOperador[0];
            lsOperador = ddlCapComprador.SelectedValue.Split('-');
            lValorParametrosVal[3] = lsOperador[0];
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_ValidaRegMay", lsNombreParametrosVal, lTipoparametrosVal, lValorParametrosVal, goInfo);
            if (lLector.HasRows)
            {
                while (lLector.Read())
                    lsError += lLector["error"].ToString() + "<br>";
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
            //fin validaciones rq026-15 20171130

            return lsError;
        }
        catch (Exception ex)
        {
            return "Problemas en la Validación de la Información. " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// Realiza la Limpieza de los Campo de Captura en la Pantalla luego de la Creación o Actualizacion
    /// </summary>
    protected void LimpiarCampos()
    {
        TxtNumContrato.Text = "";
        TxtFechaSus.Text = "";
        ddlModalidad.SelectedValue = "0";
        ChkOtrModa.Checked = false;
        TxtOtrModalidad.Text = "";
        ddlPuntoEntr.SelectedValue = "0";
        //ddlPuntoEntr_SelectedIndexChanged(null, null); // 20161225 rq111 OTMM transporte  //20180227 rq011-18
        ChkOtrPuntoE.Checked = false;
        TxtOtrPuntoE.Text = "";
        TxtCantidad.Text = "";
        TxtCantidadT.Text = "";  //20170926 rq027-17
        TxtPrecio.Text = "";
        TxtFechaInicial.Text = "";
        TxtFechaFinal.Text = "";
        ddlRuta.SelectedValue = "0";// 20161225 rq111 OTMM transporte
    }
    /// <summary>
    /// Realiza la actualizacion del estado y envia la Alerta para solicitar correccion de los datos del Registro.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSolCorr_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_estado", "@P_observacion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char, SqlDbType.VarChar };
        String[] lValorParametros = { hdfIdVerif.Value, "E", TxtObservacion.Text };

        try
        {
            lConexion.Abrir();
            if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetActualVerifDatosReg", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
            {
                lblMensaje.Text = "Se presentó un Problema en la Actualización de la Información del Usuario.!" + goInfo.mensaje_error.ToString();
                lConexion.Cerrar();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Actualizado Correctamente.!" + "');", true);
                string lsAsunto = "";
                string lsMensaje = "";
                string lsMensajeC = "";
                string lsMailC = "";
                string lsNomOperadorC = "";
                string lsMailV = "";
                string lsNomOperadorV = "";
                /// Obtengo el mail del Operador Compra
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraC.Value + " ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsMailC = lLector["e_mail"].ToString() + ";" + lLector["e_mail2"].ToString() + ";" + lLector["e_mail2"].ToString(); // 20180228 rq032-17
                    if (lLector["tipo_persona"].ToString() == "J")
                        lsNomOperadorC = lLector["razon_social"].ToString();
                    else
                        lsNomOperadorC = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                }
                lLector.Close();
                lLector.Dispose();
                /// Obtengo el mail del Operador Venta
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraV.Value + " ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsMailV = lLector["e_mail"].ToString() + ";" + lLector["e_mail2"].ToString() + ";" + lLector["e_mail2"].ToString(); // 20180228 rq032-17
                    if (lLector["tipo_persona"].ToString() == "J")
                        lsNomOperadorV = lLector["razon_social"].ToString();
                    else
                        lsNomOperadorV = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                ///// Envio del Mail de la Solicitud de la Correcion
                lsAsunto = "Notificación Solicitud Corrección Registro y Verificación";
                lsMensaje += "Nos permitimos informarle que el Administrador de SEGAS acaba de Solicitar la corrección de la información transaccional del Registro del Contrato Mayorista con No. de Registro: " + lblOperacion.Text + ". Por favor revisar los siguientes campos: \n\n";
                if (lblDat1Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit1.Text + "\n";
                if (lblDat2Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit2.Text + "\n";
                if (lblDat3Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit3.Text + "\n";
                if (lblDat4Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit4.Text + "\n";
                if (lblDat6Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit5.Text + "\n";
                if (lblDat6Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit6.Text + "\n";
                if (lblDat7Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit7.Text + "\n";
                if (lblDat8Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit8.Text + "\n";
                if (hdfDestinoRueda.Value == "T" && hdfTipoMerc.Value == "P")
                {
                    if (lblDat9Ver.Text != "Correcta")
                        lsMensaje += " - " + lblTit9.Text + "\n";
                    if (lblDat10Ver.Text != "Correcta")
                        lsMensaje += " - " + lblTit10.Text + "\n";
                }
                lsMensaje += "\n\nObservaciones: " + TxtObservacion.Text + "\n\n";
                lsMensaje += "Cordialmente, \n\n\n";
                lsMensaje += "Administrador SEGAS \n";
                lsMensajeC = "Señores: " + lsNomOperadorC + " \n\n" + lsMensaje;
                clEmail mailC = new clEmail(lsMailC, lsAsunto, lsMensaje, "");
                lblMensaje.Text = mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                ////
                lsMensajeC = "Señores: " + lsNomOperadorV + " \n\n" + lsMensaje;
                clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensaje, "");
                lblMensaje.Text = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                tblGrilla.Visible = true;
                tblDatos.Visible = true;
                CargarDatos();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas en la Actualización de la Información. " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRegCont_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_estado", "@P_observacion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char, SqlDbType.VarChar };
        String[] lValorParametros = { hdfIdVerif.Value, "R", TxtObservacion.Text };

        try
        {
            lConexion.Abrir();
            if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetActualVerifDatosReg", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
            {
                lblMensaje.Text = "Se presentó un Problema en la Actualización de la Información del Usuario.!" + goInfo.mensaje_error.ToString();
                lConexion.Cerrar();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Actualizado Correctamente.!" + "');", true);
                string lsAsunto = "";
                string lsMensaje = "";
                string lsMensajeC = "";
                string lsMailC = "";
                string lsNomOperadorC = "";
                string lsMailV = "";
                string lsNomOperadorV = "";
                /// Obtengo el mail del Operador Compra
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraC.Value + " ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsMailC = lLector["e_mail"].ToString() + ";" + lLector["e_mail2"].ToString() + ";" + lLector["e_mail2"].ToString(); // 20180228 rq032-17
                    if (lLector["tipo_persona"].ToString() == "J")
                        lsNomOperadorC = lLector["razon_social"].ToString();
                    else
                        lsNomOperadorC = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                }
                lLector.Close();
                lLector.Dispose();
                /// Obtengo el mail del Operador Venta
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraV.Value + " ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsMailV = lLector["e_mail"].ToString() + ";" + lLector["e_mail2"].ToString() + ";" + lLector["e_mail2"].ToString(); // 20180228 rq032-17
                    if (lLector["tipo_persona"].ToString() == "J")
                        lsNomOperadorV = lLector["razon_social"].ToString();
                    else
                        lsNomOperadorV = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                ///// Envio del Mail de la Solicitud de la Correcion
                lsAsunto = "Notificación Registro Contrato";
                lsMensaje += "Nos permitimos informarle que el Administrador de la plataforma SEGAS acaba de realizar exitosamente el Registro del Contrato Mayorista con No. de Registro: " + lblOperacion.Text + ".\n\n";
                lsMensaje += "\n\nObservaciones: " + TxtObservacion.Text + "\n\n";
                lsMensaje += "Cordialmente, \n\n\n";
                lsMensaje += "Administrador SEGAS \n";
                lsMensajeC = "Señores: " + lsNomOperadorC + " \n\n" + lsMensaje;
                clEmail mailC = new clEmail(lsMailC, lsAsunto, lsMensaje, "");
                lblMensaje.Text = mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                ////
                lsMensajeC = "Señores: " + lsNomOperadorV + " \n\n" + lsMensaje;
                clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensaje, "");
                lblMensaje.Text = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                CargarDatos();
                tblGrilla.Visible = true;
                tblDatos.Visible = true;
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas en la Actualización de la Información. " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImmSalirVer_Click(object sender, ImageClickEventArgs e)
    {
        tblGrilla.Visible = true;
        tblDatos.Visible = true;
        tblVerifica.Visible = false;
        CargarDatos();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlCapTipoMercado_SelectedIndexChanged(object sender, EventArgs e)
    {
        //string[] lsProducto;  //20161215 rq111 mayorista transporte
        // lsProducto = ddlCapProducto.SelectedValue.Split('-'); //20161215 rq111 mayorista transporte
        //hdfDestinoRueda.Value = lsProducto[1].Trim();//20161215 rq111 mayorista transporte
        ddlPuntoEntr.Items.Clear();
        ddlMercadoRel.Items.Clear();
        ddlSector.Items.Clear();
        lConexion.Abrir();
        LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_punto_salida_snt", " estado = 'A'  order by descripcion", 0, 2);
        LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo", " estado = 'A' order by descripcion", 0, 1); //20160706
        LlenarControles(lConexion.gObjConexion, ddlMercadoRel, "m_mercado_relevante", " estado = 'A' order by descripcion", 0, 1);
        lConexion.Cerrar();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlUsuSnt_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlUsuSnt.SelectedValue != "0")
        {
            if (ddlUsuSnt.SelectedValue == "S")
            {
                TrPuntaSal.Visible = true;
                TrSisDist.Visible = false;
            }
            else
            {
                TrPuntaSal.Visible = false;
                TrSisDist.Visible = true;
            }
            hdfusuarioSnt.Value = ddlUsuSnt.SelectedValue;
        }
        else
            hdfusuarioSnt.Value = "N";
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlDeptoPuntoSal_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlDeptoPuntoSal.SelectedValue != "0")  //20170530 divipola
        //{  //20170530 divipola
        DdlMunPuntoSal.Items.Clear();
        lConexion.Abrir();
        LlenarControles(lConexion.gObjConexion, DdlMunPuntoSal, "m_divipola", " estado = 'A'  And codigo_ciudad != '0' And codigo_departamento = " + ddlDeptoPuntoSal.SelectedValue + " and codigo_centro ='0' order by nombre_ciudad", 3, 4); //20170530 divipola
        lConexion.Cerrar();
        DdlMunPuntoSal_SelectedIndexChanged(null, null); //20170530 divipola
        //}  //20170530 divipola
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20170530 divipola
    protected void DdlMunPuntoSal_SelectedIndexChanged(object sender, EventArgs e)
    {
        DdlCentro.Items.Clear();
        lConexion.Abrir();
        LlenarControles(lConexion.gObjConexion, DdlCentro, "m_divipola", " estado = 'A' And codigo_ciudad = " + DdlMunPuntoSal.SelectedValue + " and codigo_centro <> '0' order by nombre_centro", 5, 6);
        lConexion.Cerrar();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ChekOtro_CheckedChanged(object sender, EventArgs e)
    {
        if (ChekOtro.Checked)
        {
            TxtCapComprador.Visible = true;
            lblNIT.Visible = true;
        }
        else
        {
            TxtCapComprador.Visible = false;
            lblNIT.Visible = false;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCrear_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_ind_contrato", "@P_fecha_negociacion", "@P_contrato_definitivo", "@P_punta_contrato",
                                        "@P_fecha_suscripcion_cont", "@P_nombre_operador", "@P_codigo_tipo_doc", "@P_no_identificacion_operador", "@P_codigo_modalidad",
                                        "@P_desc_otro_moda", "@P_codigo_punto_entrega", "@P_desc_otro_punto", "@P_cantidad", "@P_precio", "@P_fecha_inicial",
                                        "@P_fecha_final", "@P_ruta_contrato", "@P_tipo_garantia","@P_valor_garantia","@P_fecha_pago", "@P_codigo_sector_consumo",
                                        "@P_usuario_no_reg_snt", "@P_codigo_depto_punto_sal", "@P_codigo_municipio_pto_sal","@P_codigo_mercado_relev_sistema","@P_codigo_motivo_modificacion",
                                        "@P_estado_act","@P_codigo_tipo_subasta","@P_destino_rueda","@P_codigo_producto","@P_tipo_mercado",
                                        "@P_nombre_contraparte","@P_tipo_doc_contraparte","@P_no_ident_contraparte","@P_operador_compra","@P_operador_venta","@P_accion",
                                        "@P_codigo_ruta_may" ,  //20161215 rq111 mayorista transporte
                                        "@P_codigo_centro_pob",  //20170530 divipola
                                        "@P_capacidad_transporte"  //20170926 rq027-17
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.VarChar,SqlDbType.Decimal,  SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, //20170530 divipola
                                        SqlDbType.Int,  SqlDbType.Int, SqlDbType.VarChar,SqlDbType.Int,SqlDbType.VarChar,SqlDbType.Int,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,
                                        SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,
                                        SqlDbType.Int, //20161215 rq111 mayorista transporte
                                        SqlDbType.VarChar,//20170530 divipola
                                        SqlDbType.Int   //20170926 rq027-17
                                      };
        string[] lValorParametros = { "0", "M", "", "0", "", "", "", "", "", "0", "", "0", "", "0", "0", "", "", "", "", "0", "", "0", "", "0", "0", "0", "0", "V", "0", "", "0", "", "", "", "", "0", "0", "1", "0", "0", "0" }; //20161215 rq111 mayorista transporte //20170530 divipola  //20170926 rq027-17
        lblMensaje.Text = "";
        string lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "-" + DateTime.Now.ToShortDateString().Substring(3, 2) + "-" + DateTime.Now.ToShortDateString().Substring(0, 2);
        string sRuta = sRutaArc + lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\";
        string[] lsOperaC;
        string[] lsOperaV;
        //string[] lsProducto;  //2016115 rq111 mayorista transporte

        try
        {

            lblMensaje.Text = validaciones();
            lsOperaV = ddlCapVendedor.SelectedValue.Split('-');
            lsOperaC = ddlCapComprador.SelectedValue.Split('-');
            //lsProducto = ddlCapProducto.SelectedValue.Split('-');  //20161215 rq111 mayorista transporte
            if (FuCopiaCont.FileName != "")
            {

                try
                {
                    System.IO.Directory.CreateDirectory(sRuta);
                    FuCopiaCont.SaveAs(sRuta + FuCopiaCont.FileName);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Problemas en la Carga del Archivo. " + ex.Message.ToString();
                }
            }
            SqlTransaction oTransaccion;
            lConexion.Abrir();
            if (lblMensaje.Text == "")
            {
                if (hdfIdVerif.Value.Trim().Length > 0)
                    lValorParametros[0] = hdfIdVerif.Value;
                lValorParametros[2] = TxtCapFechaNeg.Text.Trim();
                lValorParametros[3] = TxtNumContrato.Text.Trim().ToUpper();
                lValorParametros[4] = hdfPunta.Value;
                lValorParametros[5] = TxtFechaSus.Text.Trim();

                lValorParametros[6] = lsOperaV[3].Trim();
                lValorParametros[7] = lsOperaV[1].Trim();
                lValorParametros[8] = lsOperaV[2].Trim();
                if (!ChekOtro.Checked)
                {
                    lValorParametros[32] = lsOperaC[3].Trim();
                    lValorParametros[33] = lsOperaC[1].Trim();
                    lValorParametros[34] = lsOperaC[2].Trim();
                    lValorParametros[35] = lsOperaC[0].Trim();
                }
                else
                {
                    lValorParametros[32] = hdfNomComp.Value;
                    lValorParametros[33] = hdfTipoDocCom.Value;
                    lValorParametros[34] = TxtCapComprador.Text.Trim();
                    lValorParametros[35] = "0";
                }
                lValorParametros[36] = lsOperaV[0].Trim();

                lValorParametros[9] = ddlModalidad.SelectedValue;
                lValorParametros[10] = TxtOtrModalidad.Text.Trim().ToUpper();
                lValorParametros[11] = ddlPuntoEntr.SelectedValue;
                lValorParametros[12] = TxtOtrPuntoE.Text.Trim().ToUpper();
                lValorParametros[13] = TxtCantidad.Text.Trim();
                lValorParametros[14] = TxtPrecio.Text.Trim();
                lValorParametros[15] = TxtFechaInicial.Text.Trim();
                lValorParametros[16] = TxtFechaFinal.Text.Trim();
                if (FuCopiaCont.FileName != "")
                    lValorParametros[17] = lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\" + FuCopiaCont.FileName;
                else
                    lValorParametros[17] = "";
                lValorParametros[18] = TxtCapTipoGarantia.Text.Trim();
                lValorParametros[19] = TxtCapValorGarantia.Text.Trim();
                lValorParametros[20] = TxtCapFecPlazo.Text.Trim();
                lValorParametros[21] = ddlSector.SelectedValue;
                lValorParametros[22] = ddlUsuSnt.SelectedValue;
                lValorParametros[23] = ddlDeptoPuntoSal.SelectedValue;
                if (ddlDeptoPuntoSal.SelectedValue != "0")
                    lValorParametros[24] = DdlMunPuntoSal.SelectedValue;
                else
                    lValorParametros[24] = "0";
                lValorParametros[25] = ddlMercadoRel.SelectedValue;
                lValorParametros[26] = "0"; // Motivo de Modificacion
                lValorParametros[27] = "I";
                lValorParametros[28] = "0";
                lValorParametros[29] = hdfDestinoRueda.Value;
                lValorParametros[30] = ddlCapProducto.SelectedValue; //20161215 rq111 mayorista transporte
                lValorParametros[31] = ddlCapTipoMercado.SelectedValue;
                lValorParametros[38] = ddlRuta.SelectedValue; //20161215 rq111 mayorista transporte
                //20170530 divipola
                if (DdlCentro.SelectedValue != "" && DdlCentro.SelectedValue != "0")
                    lValorParametros[39] = DdlCentro.SelectedValue;
                else
                    lValorParametros[39] = "0";
                //20170530 divipola
                if (ddlUsuSnt.SelectedValue == "N")
                {
                    lValorParametros[23] = "0";
                    lValorParametros[24] = "0";
                    lValorParametros[39] = "0";
                }
                else
                    lValorParametros[25] = "0";
                //20170926 rq027-17
                if (ddlCapProducto.SelectedValue == "3" && TxtCantidadT.Text.Trim() != "")
                    lValorParametros[40] = TxtCantidadT.Text.Trim();

                oTransaccion = lConexion.gObjConexion.BeginTransaction(System.Data.IsolationLevel.Serializable);
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetRegistroContratoMay", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación del Registro del Contrato.! " + goInfo.mensaje_error.ToString();
                    oTransaccion.Rollback();
                    lConexion.Cerrar();
                }
                else
                {
                    string lsEnviarMail = "N";
                    string lsDiferencia = "";
                    string lsIdReg = "";
                    lblMensaje.Text = "";
                    lLector.Read();
                    hdfCodVerifCont.Value = lLector["codigo_cont_dato"].ToString();
                    lblOperacion.Text = lLector["Id"].ToString();
                    lblMensaje.Text = lLector["error"].ToString();
                    lsEnviarMail = lLector["enviaMail"].ToString();
                    lsDiferencia = lLector["diferencia"].ToString();
                    lsIdReg = lLector["idReg"].ToString();
                    if (lblMensaje.Text == "")
                    {
                        lLector.Close();
                        lLector.Dispose();
                        oTransaccion.Commit();
                        // Envio el correo del registro del contrato a la punta contraria
                        string lsAsunto = "";
                        string lsMensaje = "";
                        string lsMensajeC = "";
                        string lsMailC = "";
                        string lsNomOperadorC = "";
                        string lsMailV = "";
                        string lsNomOperadorV = "";
                        /// Obtengo el mail del Operador Venta
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + lsOperaV[0].Trim() + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lsMailV = lLector["e_mail"].ToString() + ";" + lLector["e_mail2"].ToString() + ";" + lLector["e_mail2"].ToString(); // 20180228 rq032-17
                            if (lLector["tipo_persona"].ToString() == "J")
                                lsNomOperadorV = lLector["razon_social"].ToString();
                            else
                                lsNomOperadorV = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                        }
                        lLector.Close();
                        lLector.Dispose();
                        if (!ChekOtro.Checked)
                        {
                            /// Obtengo el mail del Operador Compra
                            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + lsOperaC[0].Trim() + " ");
                            if (lLector.HasRows)
                            {
                                lLector.Read();
                                lsMailC = lLector["e_mail"].ToString() + ";" + lLector["e_mail2"].ToString() + ";" + lLector["e_mail2"].ToString(); // 20180228 rq032-17
                                if (lLector["tipo_persona"].ToString() == "J")
                                    lsNomOperadorC = lLector["razon_social"].ToString();
                                else
                                    lsNomOperadorC = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                            }
                            lLector.Close();
                            lLector.Dispose();
                        }
                        lConexion.Cerrar();
                        /// Envio de la alerta del ingreso de la información
                        lsAsunto = "Notificación Ingreso Información del Contrato Otras Transacciones del Mercado Mayorista";
                        if (hdfPunta.Value == "C")
                            lsMensaje = "Nos permitimos  informarle que el operador " + lsNomOperadorC + " acaba de Ingresar satisfactoriamente la información transaccional de la Operación No: " + lblOperacion.Text + ".<br><br>";
                        if (hdfPunta.Value == "V") //20170601 rq020-17
                            lsMensaje = "Nos permitimos  informarle que el operador " + lsNomOperadorV + " acaba de Ingresar satisfactoriamente la información transaccional de la Operación No: " + lblOperacion.Text + ".<br><br>";
                        //20170601 rq020-17
                        if (hdfPunta.Value == "")
                            lsMensaje = "Nos permitimos  informarle que el administrador del SE_GAS acaba de Ingresar satisfactoriamente la información transaccional de la Operación No: " + lblOperacion.Text + ".<br><br>";
                        lsMensaje += "Cordialmente, <br><br><br>";
                        lsMensaje += "Administrador SEGAS <br>";
                        if (lsMailV != "")
                        {
                            lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                            clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                            lblMensaje.Text = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                        }

                        if (lsMailC != "")
                        {
                            lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                            clEmail mailC = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                            lblMensaje.Text = mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                        }
                        if (lsEnviarMail == "S")
                        {
                            ///// Envio del Mail de la Solicitud de la Correcion o Registro del Contrato
                            if (lsDiferencia != "")
                            {
                                lsAsunto = "Notificación Solicitud corrección, registro y verificación Otras Transacciones del Mercado Mayorista";
                                lsMensaje = "Nos permitimos informarle que el Administrador de SEGAS acaba de Solicitar la corrección de la información transaccional de la Operación No: " + lblOperacion.Text + ". Por favor revisar los siguientes campos: <br><br> " + lsDiferencia + "<br><br>";
                                lsMensaje += "\n\nObservaciones: Por favor validar los campos objeto de corrección con su contraparte. <br><br>";
                            }
                            else
                            {
                                lsAsunto = "Notificación Registro Contrato Otras Transacciones del Mercado Mayorista";
                                lsMensaje = "Nos permitimos informarle que el Administrador de la plataforma SEGAS acaba de realizar exitosamente el Registro del Contrato de la Operación No: " + lblOperacion.Text + " con No. de Registro: " + lsIdReg + ".<br><br>";
                            }
                            lsMensaje += "Cordialmente, <br><br><br>";
                            lsMensaje += "Administrador SEGAS <br>";
                            if (lsMailV != "")
                            {
                                lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                                clEmail mailV1 = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                                lblMensaje.Text = mailV1.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                            }

                            if (lsMailC != "")
                            {
                                lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                                clEmail mailC1 = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                                lblMensaje.Text = mailC1.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                            }
                        }
                        lConexion.Cerrar();
                    }
                    if (lblMensaje.Text == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Ingresado Correctamente.!" + "');", true);

                        tblRegCnt.Visible = false;
                        tblGrilla.Visible = true;
                        tblDatos.Visible = true;
                        LimpiarCampos();
                        CargarDatos();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnActualizar_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_ind_contrato", "@P_fecha_negociacion", "@P_contrato_definitivo", "@P_punta_contrato",
                                        "@P_fecha_suscripcion_cont", "@P_nombre_operador", "@P_codigo_tipo_doc", "@P_no_identificacion_operador", "@P_codigo_modalidad",
                                        "@P_desc_otro_moda", "@P_codigo_punto_entrega", "@P_desc_otro_punto", "@P_cantidad", "@P_precio", "@P_fecha_inicial",
                                        "@P_fecha_final", "@P_ruta_contrato", "@P_tipo_garantia","@P_valor_garantia","@P_fecha_pago", "@P_codigo_sector_consumo",
                                        "@P_usuario_no_reg_snt", "@P_codigo_depto_punto_sal", "@P_codigo_municipio_pto_sal","@P_codigo_mercado_relev_sistema","@P_codigo_motivo_modificacion",
                                        "@P_estado_act","@P_codigo_tipo_subasta","@P_destino_rueda","@P_codigo_producto","@P_tipo_mercado",
                                        "@P_nombre_contraparte","@P_tipo_doc_contraparte","@P_no_ident_contraparte","@P_operador_compra","@P_operador_venta","@P_accion",
                                        "@P_codigo_ruta_may", //20161215 rq111 mayorista transporte
                                        "@P_codigo_centro_pob", //20170530 divipola
                                        "@P_capacidad_transporte" //20170926 rq027-17
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.VarChar,SqlDbType.Decimal,  SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,//20170530 divipola
                                        SqlDbType.Int,  SqlDbType.Int, SqlDbType.VarChar,SqlDbType.Int,SqlDbType.VarChar,SqlDbType.Int,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,
                                        SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,
                                        SqlDbType.Int,  //20161215 rq111 mayorista transporte 
                                        SqlDbType.VarChar, //20170530 divipola
                                        SqlDbType.Int //20170926 rq027-17
                                      };
        string[] lValorParametros = { "0", "M", "", "0", "", "", "", "", "", "0", "", "0", "", "0", "0", "", "", "", "", "0", "", "0", "", "0", "0", "0", "0", "V", "0", "", "0", "", "", "", "", "0", "0", "2", "0", "0", "0" }; //20161215 rq111 mayorista transporte  //20170530 divipola  //20170926 rq027-17
        lblMensaje.Text = "";
        string lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "-" + DateTime.Now.ToShortDateString().Substring(3, 2) + "-" + DateTime.Now.ToShortDateString().Substring(0, 2);
        string sRuta = sRutaArc + lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\";
        string[] lsOperaC;
        string[] lsOperaV;
        //string[] lsProducto; //20161215 rq111 mayorista transporte

        try
        {
            lblMensaje.Text = validaciones();
            lsOperaV = ddlCapVendedor.SelectedValue.Split('-');
            lsOperaC = ddlCapComprador.SelectedValue.Split('-');
            //lsProducto = ddlCapProducto.SelectedValue.Split('-');  //20161215  rq111 mayorista transporte
            if (FuCopiaCont.FileName != "")
            {

                try
                {
                    System.IO.Directory.CreateDirectory(sRuta);
                    FuCopiaCont.SaveAs(sRuta + FuCopiaCont.FileName);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Problemas en la Carga del Archivo. " + ex.Message.ToString();
                    lConexion.Cerrar();
                }
            }
            SqlTransaction oTransaccion;
            lConexion.Abrir();
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = hdfIdVerif.Value;
                lValorParametros[2] = TxtCapFechaNeg.Text.Trim();
                lValorParametros[3] = TxtNumContrato.Text.Trim().ToUpper();
                lValorParametros[4] = hdfPunta.Value;
                lValorParametros[5] = TxtFechaSus.Text.Trim();
                lValorParametros[6] = lsOperaV[3].Trim();
                lValorParametros[7] = lsOperaV[1].Trim();
                lValorParametros[8] = lsOperaV[2].Trim();
                if (!ChekOtro.Checked)
                {
                    lValorParametros[32] = lsOperaC[3].Trim();
                    lValorParametros[33] = lsOperaC[1].Trim();
                    lValorParametros[34] = lsOperaC[2].Trim();
                    lValorParametros[35] = lsOperaC[0].Trim();
                }
                else
                {
                    lValorParametros[32] = hdfNomComp.Value;
                    lValorParametros[33] = hdfTipoDocCom.Value;
                    lValorParametros[34] = TxtCapComprador.Text.Trim();
                    lValorParametros[35] = "0";
                }
                lValorParametros[36] = lsOperaV[0].Trim();
                lValorParametros[9] = ddlModalidad.SelectedValue;
                lValorParametros[10] = TxtOtrModalidad.Text.Trim().ToUpper();
                lValorParametros[11] = ddlPuntoEntr.SelectedValue;
                lValorParametros[12] = TxtOtrPuntoE.Text.Trim().ToUpper();
                lValorParametros[13] = TxtCantidad.Text.Trim();
                lValorParametros[14] = TxtPrecio.Text.Trim();
                lValorParametros[15] = TxtFechaInicial.Text.Trim();
                lValorParametros[16] = TxtFechaFinal.Text.Trim();
                if (FuCopiaCont.FileName != "")
                    lValorParametros[17] = lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\" + FuCopiaCont.FileName;
                else
                    lValorParametros[17] = hdfRutaArchivo.Value;
                lValorParametros[18] = TxtCapTipoGarantia.Text.Trim();
                lValorParametros[19] = TxtCapValorGarantia.Text.Trim();
                lValorParametros[20] = TxtCapFecPlazo.Text.Trim();
                lValorParametros[21] = ddlSector.SelectedValue;
                lValorParametros[22] = ddlUsuSnt.SelectedValue;
                lValorParametros[23] = ddlDeptoPuntoSal.SelectedValue;
                if (ddlDeptoPuntoSal.SelectedValue != "0")
                    lValorParametros[24] = DdlMunPuntoSal.SelectedValue;
                else
                    lValorParametros[24] = "0";
                lValorParametros[25] = ddlMercadoRel.SelectedValue;
                lValorParametros[26] = "0"; // Motivo de Modificacion
                lValorParametros[27] = "I";
                lValorParametros[28] = "0";
                lValorParametros[29] = hdfDestinoRueda.Value;  //20161215 rq111 mayorista tyransporte
                lValorParametros[30] = ddlCapProducto.SelectedValue; //20161215 rq111 mayorista transporte
                lValorParametros[31] = ddlCapTipoMercado.SelectedValue;
                lValorParametros[38] = ddlRuta.SelectedValue; //20161215 rq111 mayorista transporte
                //20170530 divipola
                if (DdlCentro.SelectedValue != "" && DdlCentro.SelectedValue != "0")
                    lValorParametros[39] = DdlCentro.SelectedValue;
                else
                    lValorParametros[39] = "0";
                //20170530 divipola
                if (ddlUsuSnt.SelectedValue == "N")
                {
                    lValorParametros[23] = "0";
                    lValorParametros[24] = "0";
                    lValorParametros[39] = "0";
                }
                else
                    lValorParametros[25] = "0";

                //20170926 rq027-17
                if (ddlCapProducto.SelectedValue == "3" && TxtCantidadT.Text.Trim() != "")
                    lValorParametros[40] = TxtCantidadT.Text.Trim();

                oTransaccion = lConexion.gObjConexion.BeginTransaction(System.Data.IsolationLevel.Serializable);
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetRegistroContratoMay", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización del Registro del Contrato.! " + goInfo.mensaje_error.ToString();
                    oTransaccion.Rollback();
                    lConexion.Cerrar();
                }
                else
                {
                    string lsEnviarMail = "N";
                    string lsDiferencia = "";
                    string lsIdReg = "";

                    lLector.Read();
                    hdfCodVerifCont.Value = lLector["codigo_cont_dato"].ToString();
                    lblOperacion.Text = lLector["Id"].ToString();
                    lblMensaje.Text = lLector["error"].ToString();
                    lsEnviarMail = lLector["enviaMail"].ToString();
                    lsDiferencia = lLector["diferencia"].ToString();
                    lsIdReg = lLector["idReg"].ToString();


                    lLector.Close();
                    lLector.Dispose();
                    oTransaccion.Commit();
                    // Envio el correo del registro del contrato a la punta contraria
                    string lsAsunto = "";
                    string lsMensaje = "";
                    string lsMensajeC = "";
                    string lsMailC = "";
                    string lsNomOperadorC = "";
                    string lsMailV = "";
                    string lsNomOperadorV = "";
                    /// Obtengo el mail del Operador Compra
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + lsOperaC[0].Trim() + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsMailC = lLector["e_mail"].ToString() + ";" + lLector["e_mail2"].ToString() + ";" + lLector["e_mail2"].ToString(); // 20180228 rq032-17
                        if (lLector["tipo_persona"].ToString() == "J")
                            lsNomOperadorC = lLector["razon_social"].ToString();
                        else
                            lsNomOperadorC = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                    }
                    lLector.Close();
                    lLector.Dispose();
                    /// Obtengo el mail del Operador Venta
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + lsOperaV[0].Trim() + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsMailV = lLector["e_mail"].ToString() + ";" + lLector["e_mail2"].ToString() + ";" + lLector["e_mail2"].ToString(); // 20180228 rq032-17
                        if (lLector["tipo_persona"].ToString() == "J")
                            lsNomOperadorV = lLector["razon_social"].ToString();
                        else
                            lsNomOperadorV = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Envio de la alerta del ingreso de la información
                    lsAsunto = "Notificación Actualización Información Contrato Otras Transacciones del Mercado Mayorista";
                    if (hdfPunta.Value == "C")
                        lsMensaje = "Nos permitimos informarle que el Operador " + lsNomOperadorC + " acaba de Actualizar la información transaccional de la Operación No: " + lblOperacion.Text + ".<br><br>";
                    else
                        lsMensaje = "Nos permitimos  informarle que el operador " + lsNomOperadorV + " acaba de Actualizar la información transaccional de la Operación No: " + lblOperacion.Text + ".<br><br>";
                    lsMensaje += "Cordialmente, <br><br><br>";
                    lsMensaje += "Administrador SEGAS <br>";
                    if (lsMailC != "")
                    {
                        lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                        clEmail mailC = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                        lblMensaje.Text = mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                    }
                    if (lsMailV != "")
                    {
                        lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                        clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                        lblMensaje.Text = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                    }
                    if (lsEnviarMail == "S")
                    {
                        ///// Envio del Mail de la Solicitud de la Correcion o Registro del Contrato
                        if (lsDiferencia != "")
                        {
                            lsAsunto = "Notificación Solicitud corrección, registro y verificación Otras Transacciones del Mercado Mayorista";
                            lsMensaje = "Nos permitimos informarle que el Administrador de SEGAS acaba de Solicitar la corrección de la información transaccional de la Operación No: " + lblOperacion.Text + ". Por favor revisar los siguientes campos: <br><br> " + lsDiferencia + "<br><br>";
                            lsMensaje += "\n\nObservaciones: Por favor validar los campos objeto de corrección con su contraparte. <br><br>";
                        }
                        else
                        {
                            lsAsunto = "Notificación Registro Contrato Otras Transacciones del Mercado Mayorista";
                            lsMensaje = "Nos permitimos informarle que el Administrador de la plataforma SEGAS acaba de realizar exitosamente el Registro del Contrato de la Operación No: " + lblOperacion.Text + " con No. de Registro: " + lsIdReg + ".<br><br>";
                        }
                        lsMensaje += "Cordialmente, <br><br><br>";
                        lsMensaje += "Administrador SEGAS <br>";
                        if (lsMailC != "")
                        {
                            lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                            clEmail mailC1 = new clEmail(lsMailC, lsAsunto, lsMensajeC, "");
                            lblMensaje.Text = mailC1.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                        }
                        ////
                        if (lsMailV != "")
                        {
                            lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                            clEmail mailV1 = new clEmail(lsMailV, lsAsunto, lsMensajeC, "");
                            lblMensaje.Text = mailV1.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                        }
                    }
                    lConexion.Cerrar();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Ingresado Correctamente.!" + "');", true);

                    tblRegCnt.Visible = false;
                    tblGrilla.Visible = true;
                    tblDatos.Visible = true;
                    LimpiarCampos();
                    CargarDatos();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRegresar_Click(object sender, EventArgs e)
    {
        tblGrilla.Visible = true;
        tblRegCnt.Visible = false;
        tblDatos.Visible = true;
        lblMensaje.Text = "";
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    // 20161215 rq111 mayorista transporte
    protected void ddlCapProducto_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCapProducto.SelectedValue == "2")
        {
            trRuta.Visible = true;
            //trRuta1.Visible = true;
            if (ddlCapProducto.SelectedValue == "2")
                hdfDestinoRueda.Value = "T";
            else
                hdfDestinoRueda.Value = "";
            lblCantidad.Text = "Capacidad Contratada (KPCD)";  //20161215 rq111 OTMM tramsporte
            lblPrecio.Text = "Precio de la energía a entregar (USD/KPC)";  //20161215 rq111 OTMM tramsporte
            TxtCantidadT.Visible = false; //20170926 rq027-17
        }
        if (ddlCapProducto.SelectedValue == "1") //20170926 rq027-17
        {
            trRuta.Visible = false;
            //trRuta1.Visible = false;
            ddlRuta.SelectedValue = "0";
            //ddlPuntoEntr_SelectedIndexChanged(null, null); //20161215 rq111 OTMM tramsporte //20180227 rq011-18
            hdfDestinoRueda.Value = "G";
            lblCantidad.Text = "Cantidad Contratada  (MBTUD)"; //20161215 rq111 OTMM tramsporte
            lblPrecio.Text = "Precio de la energía a entregar (USD/MBTU)"; //20161215 rq111 OTMM tramsporte
            TxtCantidadT.Visible = false; //20170926 rq027-17
        }
        //20170926 rq027-17
        if (ddlCapProducto.SelectedValue == "3")
        {
            trRuta.Visible = true;
            //trRuta1.Visible = true;
            hdfDestinoRueda.Value = "A";
            lblCantidad.Text = "Cantidad - Capacidad Contratada (MBTUD-KPCD)";
            lblPrecio.Text = "Precio de la energía a entregar (USD/MBTU)";
            TxtCantidadT.Visible = true;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    // 20161215 rq111 mayorista transporte
    //20180227 rq011-18
    //protected void ddlPuntoEntr_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    string sRuta = ddlRuta.SelectedValue;
    //    ddlRuta.Items.Clear();
    //    if (ddlPuntoEntr.SelectedValue == "0" || ddlPuntoEntr.SelectedValue == "")
    //    {
    //        lConexion.Abrir();
    //        LlenarControles(lConexion.gObjConexion, ddlRuta, "m_ruta_snt", " estado in ('A','E')  order by descripcion", 0, 4);
    //        lConexion.Cerrar();
    //    }
    //    else
    //    {
    //        lConexion.Abrir();
    //        LlenarControles(lConexion.gObjConexion, ddlRuta, "m_ruta_snt rut", " estado in ('A','E') and exists (select 1 from m_ruta_snt_det det, m_punto_salida_snt pto where det.codigo_ruta = rut.codigo_ruta and det.codigo_tramo = pto.codigo_tramo  and pto.codigo_punto_salida = " + ddlPuntoEntr.SelectedValue + ") order by descripcion", 0, 4);
    //        lConexion.Cerrar();
    //    }
    //    try
    //    {
    //        ddlRuta.SelectedValue = sRuta;
    //    }
    //    catch (Exception ex)
    //    {
    //        ddlRuta.SelectedValue = "0";
    //    }

    //}
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    // 20161215 rq111 mayorista transporte
    //20180227 rq011-18
    //protected void ddlRuta_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    string sPunto = ddlPuntoEntr.SelectedValue;
    //    ddlPuntoEntr.Items.Clear();
    //    if (ddlRuta.SelectedValue == "0" || ddlRuta.SelectedValue == "")
    //    {
    //        lConexion.Abrir();
    //        LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_punto_salida_snt", " estado = 'A'  order by descripcion", 0, 2);
    //        lConexion.Cerrar();
    //    }
    //    else
    //    {
    //        lConexion.Abrir();
    //        LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_punto_salida_snt pto", " estado = 'A' and exists (select 1 from m_ruta_snt_det det where pto.codigo_tramo = det.codigo_tramo and det.codigo_ruta = " + ddlRuta.SelectedValue + ") order by descripcion", 0, 2);
    //        lConexion.Cerrar();
    //    }
    //    try
    //    {
    //        ddlPuntoEntr.SelectedValue = sPunto;
    //    }
    //    catch (Exception ex)
    //    {
    //        ddlPuntoEntr.SelectedValue = "0";
    //    }
    //}
    //20180228 rq011-18
    protected void ActivarCampos()
    {
        TxtCapFechaNeg.Enabled = true;
        TxtNumContrato.Enabled = true;
        TxtFechaSus.Enabled = true;
        ddlModalidad.Enabled = true;
        ddlPuntoEntr.Enabled = true;
        TxtCantidad.Enabled = true;
        TxtCantidadT.Enabled = true;
        TxtPrecio.Enabled = true;
        TxtFechaInicial.Enabled = true;
        TxtFechaFinal.Enabled = true;
        TxtCapTipoGarantia.Enabled = true;
        TxtCapValorGarantia.Enabled = true;
        ddlRuta.Enabled = true;
        TxtCapFecPlazo.Enabled = true;
        ddlSector.Enabled = true;
        ddlUsuSnt.Enabled = true;
        ddlDeptoPuntoSal.Enabled = true;
        DdlMunPuntoSal.Enabled = true;
        DdlCentro.Enabled = true;
        ddlMercadoRel.Enabled = true;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        CargarDatos();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnInsertar_Click(object sender, EventArgs e)
    {
        try
        {
            ActivarCampos();  //20180228 rq011-18
            lblMensaje.Text = "";
            lblOperacion.Text = "";
            hdfCodVerifCont.Value = "0";
            hdfIdVerif.Value = "0";
            hdfAccion.Value = "C";
            TxtCapComprador.Text = "";
            ChekOtro.Checked = false;
            ChekOtro_CheckedChanged(null, null);
            lblPuntoE.Text = "Punto de Salida SNT";
            if (ddlCapProducto.SelectedValue == "1") //20161215 rq111 OTMM tramsporte
            {
                lblCantidad.Text = "Cantidad Contratada  (MBTUD)";
                lblPrecio.Text = "Precio de la energía a entregar (USD/MBTU)";
                TxtCantidadT.Visible = false; //20161215 rq111 OTMM tramsporte
            }
            if (ddlCapProducto.SelectedValue == "2") //20161215 rq111 OTMM tramsporte
            {
                lblCantidad.Text = "Capacidad Contratada (KPCD)";  //20161215 rq111 OTMM tramsporte
                lblPrecio.Text = "Precio de la energía a entregar (USD/KPC)";  //20161215 rq111 OTMM tramsporte
                TxtCantidadT.Visible = false; //20161215 rq111 OTMM tramsporte
            }
            //20170926 rq027-17
            if (ddlCapProducto.SelectedValue == "3")
            {
                lblCantidad.Text = "Cantidad-Capacidad Contratada (MBTUD-KPCD)";
                lblPrecio.Text = "Precio de la energía a entregar (USD/(MBTU))";
                TxtCantidadT.Visible = true;
            }
            lblFechaInc.Text = "Fecha de inicio del contrato";
            lblFechaFin.Text = "Fecha de terminación del contrato";
            ddlCapPunta.Enabled = false;
            lConexion.Abrir();
            ddlMotivoModifi.Items.Clear();
            ddlCapVendedor.Items.Clear();
            ddlCapComprador.Items.Clear();
            ddlModalidad.Items.Clear();
            ddlPuntoEntr.Items.Clear();
            //ddlCapProducto.Items.Clear();  //20161215 rq111 mayorista transporte
            ddlModalidad.Items.Clear();
            ddlPuntoEntr.Items.Clear();
            ddlDeptoPuntoSal.Items.Clear();
            DdlMunPuntoSal.Items.Clear();
            DdlCentro.Items.Clear(); //720170530 divipola
            TxtCapFechaNeg.Text = "";
            TxtNumContrato.Text = "";
            TxtFechaSus.Text = "";
            TxtCantidad.Text = "";
            TxtCantidadT.Text = "";  //20170926 rq027-17
            TxtPrecio.Text = "";
            TxtFechaInicial.Text = "";
            TxtFechaFinal.Text = "";
            TxtCapTipoGarantia.Text = "";
            TxtCapValorGarantia.Text = "";
            TxtCapFecPlazo.Text = "";
            ddlSector.SelectedValue = "0";
            ddlUsuSnt.SelectedValue = "0";
            ddlUsuSnt_SelectedIndexChanged(null, null);
            TrModifi.Visible = false;
            ddlCapProducto.Enabled = true; //20161219 rq111 mayorista transporte
            ddlCapProducto.SelectedValue = "0";//20161219 rq111 mayorista transporte
            /// LLeno los Controles
            //LlenarControles(lConexion.gObjConexion, ddlCapProducto, "m_producto a", " a.estado = 'A' order by descripcion", 0, 1); //20161215 rq111 mayoristra transporte
            LlenarControles(lConexion.gObjConexion, ddlModalidad, "m_modalidad_contractual", " estado = 'A'  order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMotivoModifi, "m_motivos_modificacion", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlCapVendedor, "m_operador a, m_operador_punta_mayorista b, m_tipos_operador c", " a.estado = 'A' And a.tipo_operador = c.sigla And b.codigo_tipo_operador = c.codigo_tipo_operador And b.punta_contrato = 'V' And b.estado = 'A'  order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlCapComprador, "m_operador a, m_operador_punta_mayorista b, m_tipos_operador c", " a.estado = 'A' And a.tipo_operador = c.sigla And b.codigo_tipo_operador = c.codigo_tipo_operador And b.punta_contrato = 'C' And b.estado = 'A'  order by razon_social", 0, 4);

            /// Obtengo la Punta Valida para el operador que esta hacienda la Captura
            /// /// 20170601 rq020-17
            if (Session["tipoPerfil"].ToString() == "N")
            {
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_punta_mayorista a, m_tipos_operador b", " a.codigo_tipo_operador = b.codigo_tipo_operador And b.sigla = '" + Session["TipoOperador"].ToString() + "' And a.estado = 'A' ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    hdfPunta.Value = lLector["punta_contrato"].ToString();
                    ddlCapPunta.SelectedValue = lLector["punta_contrato"].ToString();
                    if (hdfPunta.Value == "C")
                    {
                        ddlCapVendedor.Enabled = true;
                        TxtCapComprador.Enabled = false;
                        ddlCapComprador.Enabled = false;
                        TxtCapComprador.Text = Session["IdOperador"].ToString();
                        ddlCapComprador.SelectedValue = Session["IdOperador"].ToString();
                    }
                    else
                    {
                        ddlCapVendedor.Enabled = false;
                        TxtCapComprador.Enabled = true;
                        ddlCapComprador.Enabled = true;
                        ddlCapVendedor.SelectedValue = Session["IdOperador"].ToString();
                    }
                }
                else
                    lblMensaje.Text = "El Tipo de Operador " + Session["DescTipoOperador"].ToString() + " NO está parametrizado para el Mercado Mayorista.!"; //20161215 rq111 mayorista transporte
                lLector.Close();
                lLector.Dispose();
            }
            if (lblMensaje.Text == "")
            {
                LlenarControles(lConexion.gObjConexion, ddlDeptoPuntoSal, "m_divipola", " estado = 'A' And codigo_ciudad = '0' order by nombre_departamento", 1, 2); //20170530 divipola
                //ddlCapProducto.SelectedValue = "1-G";  //20161215 rq111 mayorista transporte
                lConexion.Cerrar();
                ddlCapTipoMercado_SelectedIndexChanged(null, null);
                tblDatos.Visible = false;
                tblGrilla.Visible = false;
                tblRegCnt.Visible = true;
                btnCrear.Visible = true;
                btnActualizar.Visible = false;
            }
            ddlCapProducto_SelectedIndexChanged(null, null);//20161219 rq111 mayorista transporte
            //ddlPuntoEntr_SelectedIndexChanged(null, null); //20161225 rq111 OTMM transporte //20180227 rq011-18
        }
        catch (Exception)
        {

        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lkbExcel_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        try
        {

            string lsNombreArchivo = Session["login"] + "InfExcelReg" + DateTime.Now + ".xls";
            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgConsultaExcel.Visible = true;
            lpagina.EnableEventValidation = false;
            lpagina.Controls.Add(lform);
            dtgConsulta.EnableViewState = false;
            lform.Controls.Add(dtgConsultaExcel);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;

            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + "Consulta Registro de Contratos Mayoristas" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write(lsb.ToString());
            dtgConsultaExcel.Visible = false;
            Response.End();
            Response.Flush();
        }

        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas al Consultar los Contratos. " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSalir_Click(object sender, EventArgs e)
    {
        tblGrilla.Visible = true;
        tblRegCnt.Visible = false;
        tblDatos.Visible = true;
        lblMensaje.Text = "";
    }
}