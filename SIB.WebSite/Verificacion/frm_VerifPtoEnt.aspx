﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_VerifPtoEnt.aspx.cs"
    Inherits="Verificacion_frm_VerifPtoEnt" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
        width="80%">
        <tr>
            <td align="center" class="th1">
                <asp:Label ID="lblTitulo" runat="server" ForeColor="White" ></asp:Label>
            </td>
        </tr>
    </table>
    <br /><br /><br /><br /><br /><br />
    <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
        width="80%">
        <tr>
            <td class="td1">
                Fecha
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFecha" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFecha" runat="server" ErrorMessage="Debe Ingresar la Fecha "
                    ControlToValidate="TxtFecha" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Tipo información
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlTipo" runat="server">
                    <asp:ListItem Value="1">Energía Inyectada Vs Recibida</asp:ListItem> <%--20180815 BUG230--%>
                    <asp:ListItem Value="2">Energía Remitentes Vs Usuario Final</asp:ListItem><%--20180815 BUG230--%>
                    <asp:ListItem Value="3">Contrato transportador Vs Distribuidores</asp:ListItem> <%--20180815 BUG230--%>
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla" visible="false">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 1050px; height: 350px;">
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="True" AllowPaging="false"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>