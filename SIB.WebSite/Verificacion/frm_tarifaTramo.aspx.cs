﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;
using System.Configuration;
using System.IO;
using System.Text;
using System.Web.UI.HtmlControls;

namespace Verificacion
{
    public partial class frm_tarifaTramo : Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Ingreso de cargos por tramo";
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        clConexion lConexion = null;

        SqlDataReader lLector;
        SqlDataReader lLector1;
        String strRutaCarga;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            strRutaCarga = ConfigurationManager.AppSettings["RutaSoporteTarifa"].ToString();
            //Controlador util = new Controlador();
            /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

            //Establese los permisos del sistema
            EstablecerPermisosSistema();
            lConexion = new clConexion(goInfo);
            new clConexion(goInfo);
            //Titulo
            Master.Titulo = "Cargos de Transporte";
            /// Activacion de los Botones
            //buttons.Inicializar(ruta: "t_tarifa_tramo");
            EnumBotones[] botones = { };
            botones = new[] { EnumBotones.Crear, EnumBotones.Buscar, EnumBotones.Excel, EnumBotones.Salir };
            // Activacion de los Botones
            buttons.Inicializar(botones: botones);
            buttons.CrearOnclick += btnNuevo;

            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;


            if (IsPostBack) return;
            // Carga informacion de combos

            hndAño.Value = DateTime.Now.Year.ToString();
            hndMes.Value = DateTime.Now.Month.ToString();
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlPareja, "m_pareja_cargo", " estado = 'A'", 0, 3);
            LlenarControles1(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador >0  and (tipo_operador in ('P','I') or codigo_operador = " + goInfo.cod_comisionista + ") order by razon_social", 0, 4);
            LlenarControles1(lConexion.gObjConexion, ddlBusOperador, "m_operador", " estado = 'A' and codigo_operador >0  and (tipo_operador in ('P','I') or codigo_operador = " + goInfo.cod_comisionista + ") order by razon_social", 0, 4);
            LlenarControles2(lConexion.gObjConexion, ddlBusTramo, "0");
            LlenarControles2(lConexion.gObjConexion, ddlTramo, "0");

            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_festivo, m_parametros_adc", " dbo.FC_getDiasPrevFinMes(" + hndAño.Value + "," + hndMes.Value + ",dia_carga_tarifa,'S') >= fecha and fecha = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' ");
            if (lLector.HasRows)
                hndPuede.Value = "S";
            else
                hndPuede.Value = "N";

            lLector.Close();
            lLector.Dispose();

            lConexion.Cerrar();
            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlBusOperador.SelectedValue = goInfo.cod_comisionista;
                ddlBusOperador.Enabled = false;
                ddlBusOperador_SelectedIndexChanged(null, null);
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
                ddlOperador_SelectedIndexChanged(null, null);
            }
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            Listar();
        }
        /// <summary>
        /// 
        /// </summary>
        private void Inicializar()
        {
            EnumBotones[] botones = { };
            botones = new[] { EnumBotones.Buscar, EnumBotones.Excel, EnumBotones.Salir };
            // Activacion de los Botones
            buttons.Inicializar(botones: botones);
        }
        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "t_tarifa_tramo");
            foreach (DataGridItem Grilla in dtgMaestro.Items)
            {
                var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                lkbModificar.Visible = (bool)permisos["UPDATE"];
                var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
                lkbEliminar.Visible = (bool)permisos["DELETE"];
            }
        }

        /// <summary>
        /// Nombre: Listar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            CargarDatos();
            lblTitulo.Text = lsTitulo;
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            if (modificar != null && modificar != "")
            {
                try
                {
                    lblMensaje.Text = "";
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        // Carga informacion de combos
                        lkbVer.Visible = true;
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_tarifa_tramo tar left join m_pareja_cargo par on  (tar.porc_cargo_fijo = par.porc_fijo and tar.porc_cargo_variable = par.porc_variable)", " codigo_tarifa= " + modificar);
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            LblCodigoCap.Text = lLector["codigo_tarifa"].ToString();
                            try
                            {
                                ddlOperador.SelectedValue = lLector["codigo_Operador"].ToString();
                                ddlOperador_SelectedIndexChanged(null, null);
                            }
                            catch (Exception ex)
                            {
                                lblMensaje.Text += "El operador del registro no existe o está inactivo<br>";
                            }
                            //20160810 mmodaldiad tipo de campo
                            try
                            {
                                ddlTramo.SelectedValue = lLector["codigo_tramo"].ToString();
                            }
                            catch (Exception ex)
                            {
                                lblMensaje.Text += "El campo de producción del registro no existe o está inactivo<br>";
                            }
                            TxtFechaIni.Text = Convert.ToDateTime(lLector["fecha_vig_inicial"].ToString()).ToString("yyyy/MM/dd");
                            TxtFechaFin.Text = Convert.ToDateTime(lLector["fecha_vig_final"].ToString()).ToString("yyyy/MM/dd");
                            TxtResolucion.Text = lLector["resolucion_creg"].ToString();
                            ddlPareja.SelectedValue = lLector["codigo_pareja"].ToString();
                            TxtCargoFijo.Text = lLector["cargo_fijo"].ToString();
                            TxtCargoVar.Text = lLector["cargo_variable"].ToString();
                            TxtCargoAdm.Text = lLector["cargo_adm"].ToString();
                            TxtDiario.Text = lLector["nombre_diario"].ToString();
                            TxtFechaPub.Text = Convert.ToDateTime(lLector["fecha_publicacion"].ToString()).ToString("yyyy/MM/dd");
                            hndArchivo.Value = lLector["nombre_archivo"].ToString();
                            ddlEstado.SelectedValue = lLector["estado"].ToString();
                            hndArchivo.Value = lLector["nombre_archivo"].ToString();
                            lLector.Close();
                            lLector.Dispose();
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                            ddlOperador.Enabled = false;
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Bloquea el Registro a Modificar
                        if (lblMensaje.Text == "")
                            manejo_bloqueo("A", modificar);
                    }
                    else
                    {
                        Listar();
                        lblMensaje.Text = "No se Puede editar el Registro porque está Bloqueado. Código cargo " + modificar.ToString();
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = ex.Message;
                }
            }
            if (lblMensaje.Text == "")
            {
                Modal.Abrir(this, registroTarifa.ID, registroTarifaInside.ID);
                lblTitulo.Text = "Modificar " + lsTitulo;
            }
        }

        /// <summary>
        /// Nombre: Buscar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
        /// Modificacion:
        /// </summary>
        private void Buscar()
        {
            lblTitulo.Text = "Consultar " + lsTitulo;
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_codigo_tarifa", "@P_año_datos", "@P_mes_datos", "@P_codigo_operador", "@P_codigo_tramo", "@P_fecha_vig_inicial", "@P_fecha_vig_final" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "0", "0", "0", "0", "", "" };
            DateTime ldFecha;
            lblMensaje.Text = "";

            try
            {
                if (TxtBusFechaI.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtBusFechaI.Text.Trim());
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += "Valor Inválido en Fecha de vigencia Inicial. <br>";
                    }
                }
                if (TxtBusFechaF.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtBusFechaF.Text.Trim());
                        if (ldFecha < Convert.ToDateTime(TxtBusFechaI.Text.Trim()))
                            lblMensaje.Text += "La Fecha de vigencia final NO puede ser menor que la Inicial.!<br>";
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += "Valor Inválido en Fecha de vigencia Final. <br>";
                    }
                }
                if (lblMensaje.Text == "")
                {
                    if (TxtBusCodigo.Text.Trim().Length > 0)
                        lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    lValorParametros[1] = hndAño.Value;
                    lValorParametros[2] = hndMes.Value;
                    lValorParametros[3] = ddlBusOperador.SelectedValue;
                    lValorParametros[4] = ddlBusTramo.SelectedValue;
                    if (TxtBusFechaI.Text.Trim().Length > 0)
                        lValorParametros[5] = TxtBusFechaI.Text.Trim();
                    if (TxtBusFechaF.Text.Trim().Length > 0)
                        lValorParametros[6] = TxtBusFechaF.Text.Trim();
                    else
                        lValorParametros[6] = lValorParametros[5];
                    lConexion.Abrir();
                    dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetTarifaTramo", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgMaestro.DataBind();
                    lConexion.Cerrar();
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }
        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            lblMensaje.Text = "";
            this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page")) return;

            string lCodigoRegistro = "";
            hdfModificacion.Value = "N";
            lblMensaje.Text = "";
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;

            if (hndPuede.Value == "N") return;
            if (e.CommandName.Equals("Modificar"))
            {
                lblRegistroTarifa.InnerText = "Modificar";
                Modificar(lCodigoRegistro);
            }
            // Evento Eliminar para los Participantes Modif Inf Operativa Participantes Req. 003-17 20170131
            if (!e.CommandName.Equals("Eliminar")) return;

            string[] lsNombreParametros = { "@P_codigo_tarifa", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { lCodigoRegistro, "3" };
            lblMensaje.Text = "";
            try
            {
                if (lblMensaje.Text == "")
                {
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetTarifaTramo", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Eliminación del Registro.! " + goInfo.mensaje_error;
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Success(this, "Registro Eliminado Correctamente");
                        this.dtgMaestro.CurrentPageIndex = 0;
                        Listar();
                    }
                }
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                lblMensaje.Text = ex.Message;
            }

            if (lblMensaje.Text == "") return;
            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia("t_tarifa_tramo", lswhere, goInfo);
        }
        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia1(string lsTabla, string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia(lsTabla, lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// 20180122 rq003-18
        ///  /// // 20180126 rq107-16
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceLlave).ToString() + "-" + lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }
        protected void LlenarControles2(SqlConnection lConn, DropDownList lDdl, string lsOperador)
        {
            SqlDataReader lLector;
            //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
            //dtgComisionista.DataBind();

            // ajuste a ptdv 20160721
            string[] lsNombreParametros = { "@P_codigo_trasportador" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int };
            string[] lValorParametros = { lsOperador };

            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetTramo", lsNombreParametros, lTipoparametros, lValorParametros);
            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);
            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector["codigo_tramo"].ToString();
                lItem1.Text = lLector["desc_tramo"].ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='t_tarifa_tramo' and llave_registro='codigo_tarifa=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_tarifa=" + lscodigo_registro.ToString();
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_tarifa_tramo";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_tarifa_tramo", lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_tarifa", "@P_año_datos", "@P_mes_datos", "@P_codigo_operador", "@P_codigo_tramo", "@P_fecha_vig_inicial", "@P_fecha_vig_final" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "0", hndAño.Value , hndMes.Value , "0", "0", "", "" };

            string lsParametros = "Año: " + hndAño.Value + " Mes: " + hndMes.Value;
            SqlDataAdapter lsqldata = new SqlDataAdapter();
            DataSet lds = new DataSet();
            string lsNombreArchivo = goInfo.Usuario + "TarifaTramo" + DateTime.Now + ".xls";
            dtgExcel.Visible = true;
            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    lsParametros += " Código Cargo : " + TxtBusCodigo.Text;
                }
                if (ddlBusOperador.SelectedValue != "0")
                {
                    lValorParametros[3] = ddlBusOperador.SelectedValue;
                    lsParametros += " Operador : " + ddlBusOperador.SelectedValue;
                }
                if (ddlBusTramo.SelectedValue != "0")
                {
                    lValorParametros[4] = ddlBusTramo.SelectedValue;
                    lsParametros += " Tramo : " + ddlBusTramo.SelectedValue;
                }
                if (TxtBusFechaI.Text.Trim().Length > 0)
                {
                    lValorParametros[5] = TxtBusFechaI.Text.Trim();
                    lsParametros += " Fecha Vigencia Inicial: " + TxtBusFechaI.Text;
                }
                if (TxtBusFechaF.Text.Trim().Length > 0)
                {
                    lValorParametros[6] = TxtBusFechaF.Text.Trim();
                    lsParametros += " Fecha Vigencia Final: " + TxtBusFechaF.Text;
                }
                else
                    lValorParametros[6] = lValorParametros[5];

                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                dtgExcel.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetTarifaTramo", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgExcel.DataBind();
                lConexion.Cerrar();

                decimal ldCapacidad = 0;
                StringBuilder lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                dtgExcel.EnableViewState = false;
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);

                lform.Controls.Add(dtgExcel);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>Cargos por Tramo</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsParametros + "</font></th><td></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());

                Response.End();
                lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
                dtgExcel.Visible = false;
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.!";
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_tarifa", "@P_año_datos", "@P_mes_datos", "@P_codigo_operador", "@P_codigo_tramo", "@P_fecha_vig_inicial", "@P_fecha_vig_final", "@P_resolucion_creg", "@P_codigo_pareja", "@P_cargo_fijo", "@P_cargo_variable", "@P_cargo_adm", "@P_nombre_diario", "@P_fecha_publicacion", "@P_nombre_archivo", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0", "0", "0", "", "", "", "0", "0", "0", "0", "", "", "", "", "1" };
            lblMensaje.Text = "";
            string lsArchivo = "";
            try
            {

                if (ddlOperador.SelectedValue == "0")
                    lblMensaje.Text += "debe seleccionar el operador<br>";
                if (ddlTramo.SelectedValue == "0")
                    lblMensaje.Text += "debe seleccionar el tramo<br>";
                if (TxtFechaIni.Text == "")
                    lblMensaje.Text += " Debe digitar la fecha de vigencia inicial<br>";
                else
                {
                    try
                    {
                        Convert.ToDateTime(TxtFechaIni.Text);
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += "La fecha de vigencia inicial no es válida<br>";
                    }
                }
                if (TxtFechaFin.Text == "")
                    lblMensaje.Text += " Debe digitar la fecha de vigencia final<br>";
                else
                {
                    try
                    {
                        Convert.ToDateTime(TxtFechaFin.Text);
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += "La fecha de vigencia final no es válida<br>";
                    }
                }
                if (lblMensaje.Text == "")
                {
                    //if (VerificarExistencia(" año_datos=" + hndAño.Value + " and mes_datos =" + hndMes.Value + " and codigo_operador =" + ddlOperador.SelectedValue + " and codigo_tramo=" + ddlTramo.SelectedValue))
                    if (VerificarExistencia1("t_tarifa_tramo tar, m_pareja_cargo par", " año_datos=" + hndAño.Value + " and mes_datos =" + hndMes.Value + " and codigo_operador =" + ddlOperador.SelectedValue + " and codigo_tramo=" + ddlTramo.SelectedValue + " and tar.porc_cargo_fijo = par.porc_fijo and tar.porc_cargo_variable= par.porc_variable and par.codigo_pareja =" + ddlPareja.SelectedValue))
                        lblMensaje.Text += " La información de cargos ya está registrada<br>";
                    try
                    {
                        if (Convert.ToDateTime(TxtFechaIni.Text) > Convert.ToDateTime(TxtFechaFin.Text))
                            lblMensaje.Text += "La fecha de vigencia final debe ser mayor o igual que la inicial<br>";
                    }
                    catch (Exception ex)
                    {
                    }
                }
                if (TxtResolucion.Text == "")
                    lblMensaje.Text += " Debe digitar la resolución de la CREG<br>";
                if (ddlPareja.SelectedValue == "0")
                    lblMensaje.Text += " Debe seleccionar la pareja de cargos<br>";
                decimal ldValor = 0;
                string[] lsValor;
                if (TxtCargoFijo.Text == "")
                    lblMensaje.Text += " Debe digitar el cargo fijo<br>";
                else
                {
                    try
                    {
                        ldValor = Convert.ToDecimal(TxtCargoFijo.Text.Trim());
                        lsValor = TxtCargoFijo.Text.Trim().Split('.');
                        if (lsValor.Length > 1)
                        {
                            if (lsValor[1].Trim().Length > 3)
                                lblMensaje.Text += "El cargo fijo debe tener máximo 3 decímales<br>";
                        }
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Valor Inválido en el cargo fijo <br>";
                    }
                }
                if (TxtCargoVar.Text == "")
                    lblMensaje.Text += " Debe digitar el cargo Variable<br>";
                else
                {
                    try
                    {
                        ldValor = Convert.ToDecimal(TxtCargoVar.Text.Trim());
                        lsValor = TxtCargoVar.Text.Trim().Split('.');
                        if (lsValor.Length > 1)
                        {
                            if (lsValor[1].Trim().Length > 3)
                                lblMensaje.Text += "El cargo variable debe tener máximo 3 decímales<br>";
                        }
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Valor Inválido en el cargo variable<br>";
                    }
                }
                if (TxtCargoAdm.Text == "")
                    lblMensaje.Text += " Debe digitar el cargo fijo de AOM<br>";
                else
                {
                    try
                    {
                        ldValor = Convert.ToDecimal(TxtCargoAdm.Text.Trim());
                        lsValor = TxtCargoAdm.Text.Trim().Split('.');
                        if (lsValor.Length > 1)
                        {
                            if (lsValor[1].Trim().Length > 3)
                                lblMensaje.Text += "El cargo fijo AOM debe tener máximo 3 decímales<br>";
                        }
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Valor Inválido en el cargo fijo AOM<br>";
                    }
                }
                if (TxtFechaPub.Text == "")
                    lblMensaje.Text += " Debe digitar la fecha de publicación<br>";
                else
                {
                    try
                    {
                        Convert.ToDateTime(TxtFechaPub.Text);
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += "La fecha de publicación no es válida<br>";
                    }
                }
                if (FuSoporte.FileName == "")
                    lblMensaje.Text += "Debe seleccionar el archivo de soporte<br>";
                else
                {
                    if (Path.GetExtension(FuSoporte.FileName).ToUpper() != ".PDF")
                        lblMensaje.Text += "El archivo de soporte debe ser en formato PDF<br>";
                    else
                    {
                        int liConta = 0;
                        string lsNumero = "";
                        while (lsArchivo == "" && liConta < 10)
                        {
                            lsNumero = DateTime.Now.Millisecond.ToString();
                            lsArchivo = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + lsNumero.ToString() + "_" + liConta.ToString() + Path.GetExtension(FuSoporte.FileName).ToUpper();
                            if (File.Exists(strRutaCarga + lsArchivo))
                            {
                                lsArchivo = "";
                            }
                            liConta++;
                        }
                        if (lsArchivo == "")
                            lblMensaje.Text += "No se puede cargar el archivo de soporte. Intente de nuevo<br>";
                    }
                }
                if (lblMensaje.Text == "")
                {
                    try
                    {
                        FuSoporte.SaveAs(strRutaCarga + lsArchivo);
                    }
                    catch (Exception ex)
                    { }
                    lValorParametros[1] = hndAño.Value;
                    lValorParametros[2] = hndMes.Value;
                    lValorParametros[3] = ddlOperador.SelectedValue;
                    lValorParametros[4] = ddlTramo.SelectedValue;
                    lValorParametros[5] = TxtFechaIni.Text;
                    lValorParametros[6] = TxtFechaFin.Text;
                    lValorParametros[7] = TxtResolucion.Text;
                    lValorParametros[8] = ddlPareja.SelectedValue;
                    lValorParametros[9] = TxtCargoFijo.Text;
                    lValorParametros[10] = TxtCargoVar.Text;
                    lValorParametros[11] = TxtCargoAdm.Text;
                    lValorParametros[12] = TxtDiario.Text;
                    lValorParametros[13] = TxtFechaPub.Text;
                    lValorParametros[14] = lsArchivo;
                    lValorParametros[15] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetTarifaTramo", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Creación del cargo para el tramo.!";
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Success(this, "Se realizo el registro de forma exitosa.!");
                        //Cierra el modal de Agregar
                        Modal.Cerrar(this, registroTarifa.ID);
                        Listar();
                        lblMensaje.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
                Modal.Abrir(this, registroTarifa.ID, registroTarifaInside.ID);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void Cancel_OnClick(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (lblRegistroTarifa.InnerText.Equals("Modificar") && LblCodigoCap.Text != "")
                manejo_bloqueo("E", LblCodigoCap.Text);

            //Cierra el modal de Agregar
            Modal.Cerrar(this, registroTarifa.ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbActualiza_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_tarifa", "@P_año_datos", "@P_mes_datos", "@P_codigo_operador", "@P_codigo_tramo", "@P_fecha_vig_inicial", "@P_fecha_vig_final", "@P_resolucion_creg", "@P_codigo_pareja", "@P_cargo_fijo", "@P_cargo_variable", "@P_cargo_adm", "@P_nombre_diario", "@P_fecha_publicacion", "@P_nombre_archivo", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0", "0", "0", "", "", "", "0", "0", "0", "0", "", "", "", "", "2" };
            lblMensaje.Text = "";

            try
            {

                if (ddlOperador.SelectedValue == "0")
                    lblMensaje.Text += "debe seleccionar el operador<br>";
                if (ddlTramo.SelectedValue == "0")
                    lblMensaje.Text += "debe seleccionar el tramo<br>";
                if (TxtFechaIni.Text == "")
                    lblMensaje.Text += " Debe digitar la fecha de vigencia inicial<br>";
                else
                {
                    try
                    {
                        Convert.ToDateTime(TxtFechaIni.Text);
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += "La fecha de vigencia inicial no es válida<br>";
                    }
                }
                if (TxtFechaFin.Text == "")
                    lblMensaje.Text += " Debe digitar la fecha de vigencia final<br>";
                else
                {
                    try
                    {
                        Convert.ToDateTime(TxtFechaFin.Text);
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += "La fecha de vigencia final no es válida<br>";
                    }
                }
                if (lblMensaje.Text == "")
                {
                    //                    if (VerificarExistencia1("t_tarifa_tramo tar, m_pareja_cargo par", " año_datos=" + hndAño.Value + " and mes_datos =" + hndMes.Value + " and codigo_operador =" + ddlOperador.SelectedValue + " and codigo_tramo=" + ddlTramo.SelectedValue + " and tar.porc_cargo_fijo = par.porc_fijo and tar.porc_cargo_variable= par.porc_variable and par.codigo_pareja =" + ddlPareja.SelectedValue))
                    //if (VerificarExistencia(" año_datos=" + hndAño.Value + " and mes_datos =" + hndMes.Value + " and codigo_operador =" + ddlOperador.SelectedValue + " and codigo_tramo=" + ddlTramo.SelectedValue + " and codigo_tarifa <>" + LblCodigoCap.Text))
                    if (VerificarExistencia1("t_tarifa_tramo tar, m_pareja_cargo par", " año_datos=" + hndAño.Value + " and mes_datos =" + hndMes.Value + " and codigo_operador =" + ddlOperador.SelectedValue + " and codigo_tramo=" + ddlTramo.SelectedValue + " and tar.porc_cargo_fijo = par.porc_fijo and tar.porc_cargo_variable= par.porc_variable and par.codigo_pareja =" + ddlPareja.SelectedValue + " and codigo_tarifa <>" + LblCodigoCap.Text))
                        lblMensaje.Text += " La información de cargos ya está registrada<br>";
                    try
                    {
                        if (Convert.ToDateTime(TxtFechaIni.Text) > Convert.ToDateTime(TxtFechaFin.Text))
                            lblMensaje.Text += "La fecha de vigencia final debe ser mayor o igual que la inicial<br>";
                    }
                    catch (Exception ex)
                    {
                    }
                }
                if (TxtResolucion.Text == "")
                    lblMensaje.Text += " Debe digitar la resolución de la CREG<br>";
                if (ddlPareja.SelectedValue == "0")
                    lblMensaje.Text += " Debe seleccionar la pareja de cargos<br>";
                decimal ldValor = 0;
                string[] lsValor;
                if (TxtCargoFijo.Text == "")
                    lblMensaje.Text += " Debe digitar el cargo fijo<br>";
                else
                {
                    try
                    {
                        ldValor = Convert.ToDecimal(TxtCargoFijo.Text.Trim());
                        lsValor = TxtCargoFijo.Text.Trim().Split('.');
                        if (lsValor.Length > 1)
                        {
                            if (lsValor[1].Trim().Length > 3)
                                lblMensaje.Text += "El cargo fijo debe tener máximo 3 decímales<br>";
                        }
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Valor Inválido en el cargo fijo <br>";
                    }
                }
                if (TxtCargoVar.Text == "")
                    lblMensaje.Text += " Debe digitar el cargo Variable<br>";
                else
                {
                    try
                    {
                        ldValor = Convert.ToDecimal(TxtCargoVar.Text.Trim());
                        lsValor = TxtCargoVar.Text.Trim().Split('.');
                        if (lsValor.Length > 1)
                        {
                            if (lsValor[1].Trim().Length > 3)
                                lblMensaje.Text += "El cargo variable debe tener máximo 3 decímales<br>";
                        }
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Valor Inválido en el cargo variable<br>";
                    }
                }
                if (TxtCargoAdm.Text == "")
                    lblMensaje.Text += " Debe digitar el cargo fijo de AOM<br>";
                else
                {
                    try
                    {
                        ldValor = Convert.ToDecimal(TxtCargoAdm.Text.Trim());
                        lsValor = TxtCargoAdm.Text.Trim().Split('.');
                        if (lsValor.Length > 1)
                        {
                            if (lsValor[1].Trim().Length > 3)
                                lblMensaje.Text += "El cargo fijo AOM debe tener máximo 3 decímales<br>";
                        }
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Valor Inválido en el cargo fijo AOM<br>";
                    }
                }
                string lsArchivo = "";
                if (FuSoporte.FileName != "")
                {
                    if (Path.GetExtension(FuSoporte.FileName).ToUpper() != ".PDF")
                        lblMensaje.Text += "El archivo de soporte debe ser en formato PDF<br>";
                    else
                    {
                        int liConta = 0;
                        string lsNumero = "";
                        while (lsArchivo == "" && liConta < 10)
                        {
                            lsNumero = DateTime.Now.Millisecond.ToString();
                            lsArchivo = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + lsNumero.ToString() + "_" + liConta.ToString() + Path.GetExtension(FuSoporte.FileName).ToUpper();
                            if (File.Exists(strRutaCarga + lsArchivo))
                            {
                                lsArchivo = "";
                            }
                            liConta++;
                        }
                        if (lsArchivo == "")
                            lblMensaje.Text += "No se puede cargar el archivo de soporte. Intente de nuevo<br>";
                    }
                }

                if (lblMensaje.Text == "")
                {
                    if (lsArchivo != "")
                    {
                        try
                        {
                            FuSoporte.SaveAs(strRutaCarga + lsArchivo);
                        }
                        catch (Exception ex)
                        { }
                    }
                    lValorParametros[0] = LblCodigoCap.Text;
                    lValorParametros[1] = hndAño.Value;
                    lValorParametros[2] = hndMes.Value;
                    lValorParametros[3] = ddlOperador.SelectedValue;
                    lValorParametros[4] = ddlTramo.SelectedValue;
                    lValorParametros[5] = TxtFechaIni.Text;
                    lValorParametros[6] = TxtFechaFin.Text;
                    lValorParametros[7] = TxtResolucion.Text;
                    lValorParametros[8] = ddlPareja.SelectedValue;
                    lValorParametros[9] = TxtCargoFijo.Text;
                    lValorParametros[10] = TxtCargoVar.Text;
                    lValorParametros[11] = TxtCargoAdm.Text;
                    lValorParametros[12] = TxtDiario.Text;
                    lValorParametros[13] = TxtFechaPub.Text;
                    lValorParametros[14] = lsArchivo;
                    lValorParametros[15] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetTarifaTramo", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Actualización de cargos del tramo.!"; //20190306 rq013-19
                        lConexion.Cerrar();
                    }
                    else
                    {
                        manejo_bloqueo("E", LblCodigoCap.Text);
                        //Se notifica a el usuario que el registro fue actualizado de manera exitosa
                        Toastr.Success(this, "Se realizo la actualización de forma exitosa.!");
                        //Cierra el modal de Agregar
                        Modal.Cerrar(this, registroTarifa.ID);
                        Listar();
                    }
                }
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                manejo_bloqueo("E", LblCodigoCap.Text);
                lblMensaje.Text = ex.Message;
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                Modal.Abrir(this, registroTarifa.ID, registroTarifaInside.ID);
                lblMensaje.Text = "";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            lblMensaje.Text = "";
            Listar();

            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }
        ///// Eventos Nuevos para la Implementracion del UserControl

        /// <summary>
        /// Metodo del Link Nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNuevo(object sender, EventArgs e)
        {
            lblRegistroTarifa.InnerText = "Agregar";
            if (hndPuede.Value == "N")
            {
                Toastr.Warning(this, "Se venció el plazo para el registro de los cargos");
                //Cierra el modal de Agregar
                Modal.Cerrar(this, registroTarifa.ID);
            }
            else
            {
                LblCodigoCap.Text = "Automático";
                lkbVer.Visible = false;
                imbCrear.Visible = true;
                imbActualiza.Visible = false;

                //Abre el modal de Agregar
                Modal.Abrir(this, registroTarifa.ID, registroTarifaInside.ID);
            }
            ddlEstado.SelectedValue = "A";
        }

        /// <summary>
        /// Metodo del Link Listar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnListar(object sender, EventArgs e)
        {
            Listar();
        }

        /// <summary>
        /// Metodo del Link Consultar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsulta(object sender, EventArgs e)
        {
            Buscar();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20160721 carga ptdv
        protected void ddlBusOperador_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlBusTramo.Items.Clear();
            lConexion.Abrir();
            LlenarControles2(lConexion.gObjConexion, ddlBusTramo, ddlBusOperador.SelectedValue);
            lConexion.Cerrar();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20160721 carga ptdv
        protected void ddlOperador_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlTramo.Items.Clear();
            lConexion.Abrir();
            LlenarControles2(lConexion.gObjConexion, ddlTramo, ddlOperador.SelectedValue);
            lConexion.Cerrar();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20160721 carga ptdv
        protected void lkbVer_Click(object sender, EventArgs e)
        {
            try
            {
                if (hndArchivo.Value == "&nbsp;" || hndArchivo.Value == "")
                    Toastr.Error(this, "No hay archivo para mostrar. ");
                else
                {
                    string lsCarpetaAnt = "";
                    string[] lsCarperta;
                    lsCarperta = strRutaCarga.Split('\\');
                    lsCarpetaAnt = lsCarperta[lsCarperta.Length - 2];
                    var lsRuta = "../" + lsCarpetaAnt + "/" + hndArchivo.Value.Replace(@"\", "/");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + lsRuta + "');", true);
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "Error al visualizar el archivo. " + ex.Message.ToString());
            }
        }
    }
}