﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Verificacion_frm_RegistroContratosExt : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Registro de Contratos celabrados antes del 05 de enero de 2015";
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    string sRutaArc = ConfigurationManager.AppSettings["rutaCarga"].ToString();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlProducto, "m_producto", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlComprador, "m_operador a", " a.estado = 'A' and a.codigo_operador !=0 order by a.codigo_operador", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlVendedor, "m_operador a", " a.estado = 'A' and a.codigo_operador !=0 order by a.codigo_operador", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlEstado, "m_estado_gas", " tipo_estado = 'V' and sigla_estado != 'S' order by sigla_estado", 2, 3);
            LlenarControles(lConexion.gObjConexion, ddlMercadoRel, "m_mercado_relevante", " estado ='A' order by descripcion ", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlFuente, "m_pozo", " ind_campo_pto ='C' and estado ='A' order by descripcion ", 0, 1); //fuente o campo 20160602
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
            if (lLector.HasRows)
            {
                lLector.Read();
                hdfPermiteIng.Value = lLector["permite_ing_cont_ext"].ToString();
                hdfNoHorasPrim.Value = lLector["no_horas_correcion_primario"].ToString();
                hdfNoDiasSecun.Value = lLector["no_dias_cal_correc_secundario"].ToString();

            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
            //CargarDatos();
            if (Session["tipoPerfil"].ToString() == "B")
            {
                ImbCrearReg.Visible = false;
                btnVincular.Visible = true;
                TblElimina.Visible = true;
            }
            else
            {
                if (hdfPermiteIng.Value == "S")
                    ImbCrearReg.Visible = true;
                else
                    ImbCrearReg.Visible = false;
                btnVincular.Visible = false;
                TblElimina.Visible = false;
            }
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            if (lsTabla != "m_operador")
            {

                if (lsTabla != "m_producto a, m_caracteristica_sub b")
                {
                    if (lsTabla == "m_operador a")
                    {
                        lItem1.Value = lLector["codigo_operador"].ToString();
                        lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                    }
                    else
                    {
                        lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                        lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                    }
                }
                else
                {
                    lItem1.Value = lLector["codigo_producto"].ToString() + "-" + lLector["destino_rueda"].ToString();
                    lItem1.Text = lLector["descripcion"].ToString();
                }
            }
            else
            {
                lItem1.Value = lLector["codigo_operador"].ToString() + "-" + lLector["codigo_tipo_doc"].ToString() + "-" + lLector["no_documento"].ToString() + "-" + lLector["razon_social"].ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        this.dtgConsulta.CurrentPageIndex = 0;
        CargarDatos();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgConsulta.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        lblMensaje.Text = "";
        DateTime ldFecha;
        int liValor = 0;
        string[] lsNombreParametros = { "@P_id_inicial","@P_id_final", "@P_fecha_contrato", "@P_mercado", "@P_codigo_producto", 
                                        "@P_operador_compra", "@P_operador_venta","@P_tipo_perfil","@P_codigo_operador", "@P_estado",
                                        "@P_numero_ids","@P_numero_contrato",
                                        "@P_fecha_fin_cont"};  //20161213 rq102 conformación de rutas
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, 
                                        SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.VarChar,
                                        SqlDbType.VarChar};  //20161213 rq102 conformacion de rutas
        string[] lValorParametros = { "0", "0", "", "", "0", "0", "0", Session["tipoPerfil"].ToString(), goInfo.cod_comisionista, "0", "0", "0", "" };  //20161213 rq102 conformacion de rutas

        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial Negociación. <br>";  //20161213 rq102 conformacion de rutas //20161207 rq102 conoformacion de rutas
            }
        }
        //20161213 rq102 conformacion de rutas
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaFin.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Final de Negociación. <br>";
            }
        }
        //20161213 rq102 conformacion de rutas
        if (TxtFechaIni.Text.Trim().Length == 0 && TxtFechaFin.Text.Trim().Length > 0)
            lblMensaje.Text += "Debe digitar la Fecha Inicial de Negociación. <br>";
        //20161213 rq102 conformacion de rutas
        if (TxtFechaIni.Text.Trim().Length > 0 && TxtFechaFin.Text.Trim().Length > 0)
            try
            {
                if (Convert.ToDateTime(TxtFechaFin.Text) < Convert.ToDateTime(TxtFechaIni.Text))
                    lblMensaje.Text += "La Fecha inicial de Negociación debe ser menor o igual que la fecha final. <br>";
            }
            catch (Exception ex)
            { }

        if (TxtIdRegini.Text.Trim().Length > 0)
        {
            try
            {
                liValor = Convert.ToInt32(TxtIdRegini.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo No. Operación Inicial. <br>";  //20161213 rq102 comformaciom de rutas
            }
        }
        if (TxtIdRegFin.Text.Trim().Length > 0)
        {
            try
            {
                liValor = Convert.ToInt32(TxtIdRegFin.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo No. operación Final. <br>";  //20161213 rq102 cofmormacomde rutas
            }
        }
        if (TxtIdRegini.Text.Trim().Length > 0 && TxtIdsReg.Text.Trim().Length > 0)
            lblMensaje.Text += "Debe Hacer Busqueda por rango de Número de operaion o por Número de operaciones, no por los dos al Tiempo. <br>";  //20161213 rq102 conformacioón de rutas

        if (lblMensaje.Text == "")
        {
            try
            {
                if (TxtIdRegini.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtIdRegini.Text.Trim();
                if (TxtIdRegFin.Text.Trim().Length > 0)
                    lValorParametros[1] = TxtIdRegFin.Text.Trim();
                else
                {
                    if (TxtIdRegini.Text.Trim().Length > 0)
                        lValorParametros[1] = TxtIdRegini.Text.Trim();
                }
                if (TxtFechaIni.Text.Trim().Length > 0)
                    lValorParametros[2] = TxtFechaIni.Text.Trim();
                if (ddlMercado.SelectedValue != "")
                    lValorParametros[3] = ddlMercado.SelectedValue;
                if (ddlProducto.SelectedValue != "0")
                    lValorParametros[4] = ddlProducto.SelectedValue;
                if (ddlComprador.SelectedValue != "0")
                    lValorParametros[5] = ddlComprador.SelectedValue;
                if (ddlVendedor.SelectedValue != "0")
                    lValorParametros[6] = ddlVendedor.SelectedValue;
                if (ddlEstado.SelectedValue != "0")
                    lValorParametros[9] = ddlEstado.SelectedValue;
                if (TxtIdsReg.Text.Trim().Length > 0)
                    lValorParametros[10] = TxtIdsReg.Text.Trim();
                if (TxtNoContrato.Text.Trim().Length > 0)
                    lValorParametros[11] = TxtNoContrato.Text.Trim();
                //20161207 rq102 conformacion de rutas
                if (TxtFechaFin.Text.Trim().Length > 0)
                    lValorParametros[12] = TxtFechaFin.Text.Trim();
                else
                    lValorParametros[12] = lValorParametros[2];

                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRegistroContratosExt", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                lConexion.Cerrar();
                if (dtgConsulta.Items.Count > 0)
                {
                    tblGrilla.Visible = true;
                    imbExcel.Visible = true;
                    ldFecha = DateTime.Now.Date;
                    foreach (DataGridItem Grilla in this.dtgConsulta.Items)
                    {
                        if (Session["tipoPerfil"].ToString() == "B")
                        {
                            if (Grilla.Cells[8].Text == "En verificacion")
                                Grilla.Cells[25].Enabled = true; // 20160603 fuente o campo
                            else
                                Grilla.Cells[25].Enabled = false; // 20160603 fuente o campo
                            if (Grilla.Cells[8].Text == "Pendiente Registro" || Grilla.Cells[8].Text == "Pendiente Vendedor" || Grilla.Cells[8].Text == "Pendiente Comprador" || Grilla.Cells[8].Text == "Pendiente Correcion")
                                Grilla.Cells[29].Enabled = true; // 20160603 fuente o campo
                            else
                                Grilla.Cells[29].Enabled = false; // 20160603 fuente o campo
                        }
                        else
                        {
                            Grilla.Cells[29].Enabled = false; // 20160603 fuente o campo
                            if (Grilla.Cells[8].Text == "En verificacion")
                                Grilla.Cells[28].Enabled = false; // 20160603 fuente o campo
                            else
                                Grilla.Cells[28].Enabled = true; // 20160603 fuente o campo
                        }
                        if (Grilla.Cells[8].Text != "Contrato Registrado")
                            Grilla.Cells[19].Text = "0";
                    }
                }
                else
                {
                    tblGrilla.Visible = false;
                    imbExcel.Visible = false;
                    lblMensaje.Text = "No se encontraron Registros.";
                }
                if (Session["tipoPerfil"].ToString() == "B")
                    dtgConsulta.Columns[25].Visible = true;  // 20160603 fuente o campo
                else
                    dtgConsulta.Columns[25].Visible = false; // 20160603 fuente o campo
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pude Generar el Informe.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        string lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "/" + DateTime.Now.ToShortDateString().Substring(3, 2) + "/" + DateTime.Now.ToShortDateString().Substring(0, 2);
        int liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
        int liValFin = 0;
        string lsHoraFin = "";
        string lsCodOperaC = "";
        string lsCodOperaV = "";
        string lsCodDocC = "";
        string lsCodDocV = "";
        string lsPuedeCorregir = "S";
        string lsCadena = "";
        string lsFechaMaxCor = "";
        string lsFechaMaxMod = "";
        string lsFechaMaxReg = "";
        string lsIndVerif = "N";
        string lsIndModif = "N";
        string lsPuedeEntrar = "S";

        if (((LinkButton)e.CommandSource).Text == "Seleccionar")
        {
            try
            {
                TxtNumContrato.Enabled = true;
                TxtFechaSus.Enabled = true;
                ddlModalidad.Enabled = true;
                ddlPuntoEntr.Enabled = true;
                TxtCantidad.Enabled = true;
                TxtPrecio.Enabled = true;
                TxtFechaInicial.Enabled = true;
                TxtFechaFinal.Enabled = true;
                ddlSentidoFlujo.Enabled = true;
                TxtPresion.Enabled = true;

                TxtCapFechaNeg.Text = "";
                TxtNumContrato.Text = "";
                TxtFechaSus.Text = "";
                TxtCantidad.Text = "";
                TxtPrecio.Text = "";
                TxtFechaInicial.Text = "";
                TxtFechaFinal.Text = "";
                ddlSentidoFlujo.SelectedValue = "";
                TxtPresion.Text = "";

                lblOperacion.Text = e.Item.Cells[0].Text;
                lblProducto.Text = e.Item.Cells[4].Text;
                hdfIdVerif.Value = e.Item.Cells[9].Text;
                hdfCodTipoSub.Value = e.Item.Cells[14].Text;
                hdfDestinoRueda.Value = e.Item.Cells[20].Text;
                hdfEstadoAct.Value = e.Item.Cells[21].Text;
                hdfTipoMerc.Value = e.Item.Cells[22].Text;
                hdfCodProd.Value = e.Item.Cells[26].Text; // 20160603 fuente o campo
                hdfOPeraC.Value = e.Item.Cells[10].Text;
                hdfOPeraV.Value = e.Item.Cells[11].Text;
                TxtCapFechaNeg.Text = e.Item.Cells[1].Text;
                hdfOPeraC.Value = e.Item.Cells[10].Text; // Campo nuevo algoritmo y verificacion 20150529
                hdfOPeraV.Value = e.Item.Cells[11].Text; // Campo nuevo algoritmo y verificacion 20150529
                lsIndVerif = e.Item.Cells[31].Text; // Modificacion Contratos 20150617 // 20160603 fuente o campo
                lsIndModif = e.Item.Cells[32].Text; // Modificacion Contratos 20150617 // 20160603 fuente o campo
                lsFechaMaxCor = e.Item.Cells[33].Text; // Modificacion Contratos 20150617 // 20160603 fuente o campo
                lsFechaMaxMod = e.Item.Cells[34].Text; // Modificacion Contratos 20150617 // 20160603 fuente o campo
                lsFechaMaxReg = e.Item.Cells[35].Text; // Modificacion Contratos 20150617 // 20160603 fuente o campo

                if (goInfo.cod_comisionista == e.Item.Cells[10].Text)
                {
                    hdfPunta.Value = "C";
                    lblPunta.Text = "COMPRADOR";
                    lnlNomOpera.Text = "Nombre del Comprador";
                    lblTipoDocOpera.Text = "Tipo Documento Comprador";
                    lblNoDocOPera.Text = "No. Documento Comprador";
                    lblNomOperador.Text = e.Item.Cells[5].Text;
                    lblTipoDocOperador.Text = e.Item.Cells[15].Text;
                    lblNoDocumentoOperaqdor.Text = e.Item.Cells[17].Text;
                    ddlCapPunta.SelectedValue = "C";
                    ddlCapComprador.Enabled = false;
                    ddlCapVendedor.Enabled = false;
                }
                else
                {
                    hdfPunta.Value = "V";
                    lblPunta.Text = "VENDEDOR";
                    lblTipoDocOpera.Text = "Tipo Documento Vendedor";
                    lnlNomOpera.Text = "Nombre del Vendedor";
                    lblNoDocOPera.Text = "No. Documento Vendedor";
                    lblNomOperador.Text = e.Item.Cells[6].Text;
                    lblTipoDocOperador.Text = e.Item.Cells[16].Text;
                    lblNoDocumentoOperaqdor.Text = e.Item.Cells[18].Text;
                    ddlCapPunta.SelectedValue = "V";
                    ddlCapVendedor.Enabled = false;
                    ddlCapComprador.Enabled = false;
                }

                if (hdfEstadoAct.Value == "R" || hdfEstadoAct.Value == "D" || hdfEstadoAct.Value == "B")
                {
                    lsPuedeEntrar = "N";
                    tblDemanda.Visible = false;
                    lblMensaje.Text = "No se puede Ingresar o Actualizar la Información YA que el Contrato esta en un estado NO valido para Modificación.!";
                }
                else
                {
                    if (lsIndVerif == "N")
                    {
                        if (lsIndModif == "S")
                        {
                            if (DateTime.Now > Convert.ToDateTime(lsFechaMaxMod))
                            {
                                lsPuedeEntrar = "N";
                                lblMensaje.Text = "No se puede Ingresar a Modificar la Información YA que se venció el plazo para hacerlo. {" + lsFechaMaxMod + "}!"; //20161207 rq102 conoformacion de rutas
                            }
                        }
                        //else
                        //{
                        //    if (Convert.ToDateTime(lsFecha) > Convert.ToDateTime(lsFechaMaxReg))
                        //    {
                        //        lsPuedeEntrar = "N";
                        //        lblMensaje.Text = "No se puede Ingresar o Actualizar la Información YA que se vencio el plazo para hacerlo. {" + lsFechaMaxReg + "}!";
                        //    }
                        //}
                    }
                    else
                    {
                        if (DateTime.Now <= Convert.ToDateTime(lsFechaMaxCor))
                        {
                            lsPuedeEntrar = "S";
                        }
                        else
                        {
                            lsPuedeEntrar = "N";
                            lblMensaje.Text = "No se puede Ingresar o Corregir la Información YA que se venció el plazo para hacerlo. {" + lsFechaMaxCor + "}!"; //20161207 rq102 conoformacion de rutas
                        }
                    }
                }

                if (lsFecha == e.Item.Cells[7].Text)
                {
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_verificacion", " codigo_tipo_subasta = " + hdfCodTipoSub.Value + " And tipo_mercado = '" + e.Item.Cells[22].Text.Trim() + "' ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsHoraFin = lLector["hora_maxima_registro"].ToString();
                        liValFin = ((Convert.ToDateTime(lsHoraFin).Hour * 60) + Convert.ToDateTime(lsHoraFin).Minute);
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                }
                else
                    liValFin = liHoraAct + 1;

                ///// Verifico los parametros para la correcion del contrato
                //if (hdfEstadoAct.Value == "E")
                //{
                //    lConexion.Abrir();
                //    string[] lsNombreParametrosC = { "@P_cadena" };
                //    SqlDbType[] lTipoparametrosC = { SqlDbType.VarChar };
                //    if (hdfTipoMerc.Value == "P")
                //        lsCadena = "Select  DATEDIFF(hour, GETDATE(),dateadd(HOUR," + hdfNoHorasPrim.Value + ",fecha_hora_actual)) as dif, dateadd(HOUR," + hdfNoHorasPrim.Value + ",fecha_hora_actual) as fecha_max_cor  from t_contrato_verificacion Where codigo_verif_contrato = " + hdfIdVerif.Value;
                //    else
                //        lsCadena = "Select fecha_final,DATEDIFF(DAY, GETDATE(),dateadd(DAY," + hdfNoDiasSecun.Value + ",fecha_final)) as dif, dateadd(DAY," + hdfNoDiasSecun.Value + ",fecha_final) as fecha_max_cor  From t_contrato_datos_verif Where codigo_verif_contrato = " + hdfIdVerif.Value + " And punta_contrato = '" + hdfPunta.Value + "'  ";
                //    Object[] lValorParametrosC = { lsCadena };
                //    /// Se obtiene los Datos de la Punta Compradora
                //    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_EjecutarCadena", lsNombreParametrosC, lTipoparametrosC, lValorParametrosC);
                //    lLector.Read();
                //    lsFechaMaxCor = lLector["fecha_max_cor"].ToString();
                //    if (Convert.ToDecimal(lLector["dif"].ToString()) >= 0)
                //        lsPuedeCorregir = "S";
                //    else
                //        lsPuedeCorregir = "N";
                //    lLector.Close();
                //    lLector.Dispose();
                //    lConexion.Cerrar();
                //}
                if (lsPuedeEntrar == "S")
                {
                    if (liHoraAct < liValFin)
                    {
                        if (!manejo_bloqueo("V", hdfIdVerif.Value))
                        {
                            /// Validaciones Cuando el Que entra en la pantalla es negociador
                            if (hdfEstadoAct.Value != "R" && hdfEstadoAct.Value != "S" && hdfEstadoAct.Value != "D" && hdfEstadoAct.Value != "B")
                            {

                                if (e.Item.Cells[15].Text == "Nit")
                                    lsCodDocC = "1";
                                else
                                    lsCodDocC = "3";
                                if (e.Item.Cells[16].Text == "Nit")
                                    lsCodDocV = "1";
                                else
                                    lsCodDocV = "3";
                                lsCodOperaC = e.Item.Cells[10].Text + "-" + lsCodDocC + "-" + e.Item.Cells[17].Text + "-" + e.Item.Cells[5].Text;
                                lsCodOperaV = e.Item.Cells[11].Text + "-" + lsCodDocV + "-" + e.Item.Cells[18].Text + "-" + e.Item.Cells[6].Text;
                                if (hdfDestinoRueda.Value == "G")
                                {
                                    lblPuntoE.Text = "Punto de entrega de la energía al comprador";
                                    lblCantidad.Text = "Cantidad de energía contratada (MBTUD)";
                                    lblPrecio.Text = "Precio a la fecha de suscripción del contrato (USD/MBTU)";
                                    lblFechaInc.Text = "Fecha de inicio de la obligación de entrega";
                                    lblFechaFin.Text = "Fecha de terminación de la obligación de entrega";
                                    TrTrans.Visible = false;
                                }
                                else
                                {

                                    lblPuntoE.Text = "Tramo o grupos de gasoductos";
                                    lblCantidad.Text = "Capacidad Contratada  (KPCD)";
                                    lblPrecio.Text = "Tarifa a la fecha de suscripción del contrato (USD/KPC)";
                                    lblFechaInc.Text = "Fecha de inicio de la prestación del servicio";
                                    lblFechaFin.Text = "Fecha de terminación prestación del servicio";
                                    if (hdfTipoMerc.Value == "P")
                                        TrTrans.Visible = true;
                                    else
                                        TrTrans.Visible = false;

                                }
                                ddlModalidad.Items.Clear();
                                ddlPuntoEntr.Items.Clear();
                                dlTipoDemanda.Items.Clear();
                                ddlMotivoModifi.Items.Clear();
                                ddlCesionario.Items.Clear();
                                ddlCapVendedor.Items.Clear();
                                ddlCapComprador.Items.Clear();
                                ddlCapProducto.Items.Clear();
                                lConexion.Abrir();
                                /// LLeno los Controles
                                if (hdfPunta.Value == "C")
                                {
                                    string[] lsNombreParametrosC = { "@P_cadena" };
                                    SqlDbType[] lTipoparametrosC = { SqlDbType.VarChar };
                                    string[] lValorParametrosC = { " Delete from t_contrato_datos_usuarios where codigo_cont_datos = 0 And login_usuario = '" + goInfo.Usuario.ToString() + "' " };
                                    /// Se obtiene los Datos de la Punta Compradora
                                    DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_EjecutarCadena", lsNombreParametrosC, lTipoparametrosC, lValorParametrosC);
                                }

                                LlenarControles(lConexion.gObjConexion, ddlCapProducto, "m_producto a, m_caracteristica_sub b", " a.estado = 'A' And b.estado = 'A' And a.codigo_producto = b.codigo_caracteristica and b.tipo_caracteristica = 'P' and b.codigo_tipo_subasta = 5 And b.tipo_mercado = 'S' order by descripcion", 0, 1);
                                if (hdfDestinoRueda.Value == "G")
                                    LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_pozo", " estado = 'A'  order by descripcion", 0, 1);
                                else
                                    LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_ruta_snt", " estado = 'A' order by descripcion", 0, 4);
                                LlenarControles(lConexion.gObjConexion, ddlCapComprador, "m_operador", " estado = 'A'  and codigo_operador !=0 order by codigo_operador", 0, 4);
                                LlenarControles(lConexion.gObjConexion, ddlCapVendedor, "m_operador", " estado = 'A'  and codigo_operador !=0 order by codigo_operador", 0, 4);
                                LlenarControles(lConexion.gObjConexion, ddlModalidad, "m_modalidad_contractual", " estado = 'A' order by descripcion", 0, 1);
                                LlenarControles(lConexion.gObjConexion, dlTipoDemanda, "m_tipo_demanda_atender", " estado = 'A' order by descripcion", 0, 1);
                                LlenarControles(lConexion.gObjConexion, ddlMotivoModifi, "m_motivos_modificacion", " estado = 'A' order by descripcion", 0, 1);
                                LlenarControles(lConexion.gObjConexion, ddlCesionario, "m_operador", " estado = 'A' And codigo_operador !=  " + goInfo.cod_comisionista + " order by razon_social", 0, 4);
                                try
                                {
                                    ddlCapVendedor.SelectedValue = lsCodOperaV;
                                    ddlCapComprador.SelectedValue = lsCodOperaC;
                                    ddlCapProducto.SelectedValue = hdfCodProd.Value + "-" + hdfDestinoRueda.Value;
                                    ddlCapTipoMercado.SelectedValue = hdfTipoMerc.Value;
                                    ddlCapProducto.Enabled = false;
                                }
                                catch (Exception ex)
                                {

                                }
                                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_contrato_datos_verif", " codigo_verif_contrato = " + hdfIdVerif.Value + " And punta_contrato = '" + hdfPunta.Value + "' ");
                                if (lLector.HasRows)
                                {
                                    lLector.Read();
                                    /// LLamo metodo que bloquea los campos de la pantalla cuando las partes entran a corregir la informacion 
                                    /// Requerimiento ALgoritmo y verificacion 20150529
                                    if (lsIndVerif == "S")
                                        inactivarCampos();

                                    hdfAccion.Value = "M";
                                    TrModifi.Visible = true;
                                    if (Session["tipoPerfil"].ToString() == "N")
                                    {
                                        btnCrear.Visible = false;
                                        btnActualizar.Visible = true;
                                    }
                                    else
                                    {
                                        btnCrear.Visible = false;
                                        btnActualizar.Visible = false;
                                    }
                                    /// Recuperacion Datos
                                    hdfCodVerifCont.Value = lLector["codigo_cont_datos"].ToString();
                                    TxtNumContrato.Text = lLector["contrato_definitivo"].ToString();
                                    TxtFechaSus.Text = lLector["fecha_suscripcion_cont"].ToString().Substring(6, 4) + "/" + lLector["fecha_suscripcion_cont"].ToString().Substring(3, 2) + "/" + lLector["fecha_suscripcion_cont"].ToString().Substring(0, 2);
                                    try
                                    {
                                        ddlModalidad.SelectedValue = lLector["codigo_modalidad"].ToString();
                                        ddlPuntoEntr.SelectedValue = lLector["codigo_punto_entrega"].ToString();
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                    //fuente o campo 20160602
                                    try
                                    {
                                        ddlFuente.SelectedValue = lLector["codigo_fuente"].ToString();
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                    TxtCantidad.Text = lLector["cantidad"].ToString();
                                    TxtPrecio.Text = lLector["precio"].ToString();
                                    TxtFechaInicial.Text = lLector["fecha_inicial"].ToString().Substring(6, 4) + "/" + lLector["fecha_inicial"].ToString().Substring(3, 2) + "/" + lLector["fecha_inicial"].ToString().Substring(0, 2);
                                    TxtFechaFinal.Text = lLector["fecha_final"].ToString().Substring(6, 4) + "/" + lLector["fecha_final"].ToString().Substring(3, 2) + "/" + lLector["fecha_final"].ToString().Substring(0, 2);
                                    hdfRutaArchivo.Value = lLector["ruta_contrato"].ToString();
                                    if (hdfDestinoRueda.Value == "T" && hdfTipoMerc.Value == "P")
                                    {
                                        try
                                        {
                                            ddlSentidoFlujo.SelectedValue = lLector["sentido_flujo"].ToString().Replace("&nbsp;", "");
                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                        TxtPresion.Text = lLector["presion_punto_fin"].ToString();
                                    }
                                    if (hdfPunta.Value == "C")
                                    {
                                        //try
                                        //{
                                        //    dlTipoDemanda.SelectedValue = lLector["codigo_tipo_demanda"].ToString();
                                        //}
                                        //catch (Exception ex)
                                        //{

                                        //}
                                        //hdfTipoDemanda.Value = dlTipoDemanda.SelectedValue;
                                        tblDemanda.Visible = true;
                                        btnActualUsu.Visible = false;
                                        btnCrearUsu.Visible = true;
                                        CargarDatosUsu();
                                        //if (dlTipoDemanda.SelectedValue == "1")
                                        //    TxtUsuarioFinal.Enabled = false;
                                        //if (hdfTipoDemanda.Value == "1")
                                        //{
                                        //    TxtUsuarioFinal.Enabled = false;
                                        //    lblMercadoRelev.Text = "Mercado Relevante Usuario Regulado";
                                        //    if (hdfDestinoRueda.Value == "G")
                                        //        lblCantContra.Text = "Cantidad a entregar";
                                        //    else
                                        //        lblCantContra.Text = "Capacidad Contratada";
                                        //}
                                        //else
                                        //{
                                        //    if (hdfDestinoRueda.Value == "G")
                                        //    {
                                        //        lblCantContra.Text = "Cantidad Contratada por usuario (MBTUD)";
                                        //        lblMercadoRelev.Text = "Mercado Relevante Usuario No Regulado";
                                        //    }
                                        //    else
                                        //    {
                                        //        lblMercadoRelev.Text = "Mercado Relevante Transporte Usuario No Regulado";
                                        //        lblCantContra.Text = "Capacidad Contratada por usuario (KPCD)";
                                        //    }
                                        //}
                                    }
                                }
                                else
                                {
                                    if (hdfPunta.Value == "C")
                                    {
                                        tblDemanda.Visible = true;
                                        btnActualUsu.Visible = false;
                                        btnCrearUsu.Visible = true;
                                        CargarDatosUsu();
                                        //if (dlTipoDemanda.SelectedValue == "1")
                                        //    TxtUsuarioFinal.Enabled = false;
                                        //if (hdfTipoDemanda.Value == "1")
                                        //{
                                        //    TxtUsuarioFinal.Enabled = false;
                                        //    lblMercadoRelev.Text = "Mercado Relevante Usuario Regulado";
                                        //    if (hdfDestinoRueda.Value == "G")
                                        //        lblCantContra.Text = "Cantidad a entregar";
                                        //    else
                                        //        lblCantContra.Text = "Capacidad Contratada";
                                        //}
                                        //else
                                        //{
                                        //    if (hdfDestinoRueda.Value == "G")
                                        //    {
                                        //        lblCantContra.Text = "Cantidad Contratada por usuario (MBTUD)";
                                        //        lblMercadoRelev.Text = "Mercado Relevante Usuario No Regulado";
                                        //    }
                                        //    else
                                        //    {
                                        //        lblMercadoRelev.Text = "Mercado Relevante Transporte Usuario No Regulado";
                                        //        lblCantContra.Text = "Capacidad Contratada por usuario (KPCD)";
                                        //    }
                                        //}
                                    }
                                    TrModifi.Visible = false;
                                    hdfAccion.Value = "C";
                                    if (Session["tipoPerfil"].ToString() == "N")
                                    {
                                        btnCrear.Visible = true;
                                        btnActualizar.Visible = false;
                                    }
                                    else
                                    {
                                        btnCrear.Visible = false;
                                        btnActualizar.Visible = false;
                                    }
                                }
                                lLector.Close();
                                lLector.Dispose();
                                lConexion.Cerrar();
                                //dlTipoDemanda_SelectedIndexChanged(null, null);
                                tblGrilla.Visible = false;
                                tblRegCnt.Visible = true;
                                tblDatos.Visible = false;
                                //FUente o campo MP 20160602 
                                if (ddlCapProducto.SelectedValue == "1-G" && ddlCapTipoMercado.SelectedValue == "P")
                                {
                                    TrFuente.Visible = true;
                                    // Acitvo el campo de captura Equivalente Kpcd Req. 009-17 20170321
                                    TdIndica.Visible = true;
                                    TdIndica1.Visible = true;
                                }
                                else
                                {
                                    TrFuente.Visible = false;
                                    ddlFuente.SelectedValue = "0";
                                    // Acitvo el campo de captura Equivalente Kpcd Req. 009-17 20170321
                                    TdIndica.Visible = false;
                                    TdIndica1.Visible = false;
                                }
                            }
                            else
                                lblMensaje.Text = "El Registro No Puede ser Modificado, Ya que esta en Un estyado NO Válido para Modificación.!"; //20161207 rq102 conoformacion de rutas
                        }
                        else
                            lblMensaje.Text = "No se puede Modificar el Registro YA que se encuentra Bloqueado por el Administrador.!";
                    }
                    else
                        lblMensaje.Text = "No se puede Ingresar o Actualizar la Información YA que se venció el plazo para hacerlo.!"; //20161207 rq102 conoformacion de rutas
                }
                else
                    lblMensaje.Text = "No se puede Ingresar o Corregir la Información YA que se venció el plazo para hacerlo. {" + lsFechaMaxCor + "}!"; //20161207 rq102 conoformacion de rutas
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas en la Recuperacion de la Información. " + ex.Message.ToString();

            }
        }
        /// Realiza la visualizacion de la Pantalla de Verificacion
        if (((LinkButton)e.CommandSource).Text == "Verificar")
        {
            string[] lsNombreParametros = { "@P_codigo_cont_datos", "@P_codigo_verif_contrato", "@P_punta_contrato" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
            Object[] lValorParametros = { "0", "0", "" };
            string lsVerifica = "1"; // 1=Correcta 0=Incorrecta

            try
            {
                TxtObservacion.Text = "";
                lblOperacion.Text = e.Item.Cells[0].Text;
                lblProducto.Text = e.Item.Cells[4].Text;
                hdfIdVerif.Value = e.Item.Cells[9].Text;
                hdfCodTipoSub.Value = e.Item.Cells[14].Text;
                hdfDestinoRueda.Value = e.Item.Cells[20].Text;
                hdfEstadoAct.Value = e.Item.Cells[21].Text;
                hdfTipoMerc.Value = e.Item.Cells[22].Text;
                hdfOPeraC.Value = e.Item.Cells[10].Text;
                hdfOPeraV.Value = e.Item.Cells[11].Text;

                //// Obtengo la Informacion del Comprador
                lConexion.Abrir();
                lValorParametros[1] = hdfIdVerif.Value;
                lValorParametros[2] = "C";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetRegistroContratosDatos", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error == "")
                {
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lblDat1C.Text = lLector["contrato_definitivo"].ToString().Trim().ToUpper();
                        lblDat2C.Text = lLector["fecha_suscripcion_cont"].ToString().Trim();
                        lblDat3C.Text = lLector["codigo_modalidad"].ToString() + " - " + lLector["desc_modalidad"].ToString().Trim();
                        lblDat4C.Text = lLector["codigo_punto_entrega"].ToString() + " - " + lLector["desc_punto_ent"].ToString().Trim();
                        lblDat5C.Text = lLector["cantidad"].ToString().Trim();
                        lblDat6C.Text = lLector["precio"].ToString().Trim();
                        lblDat7C.Text = lLector["fecha_inicial"].ToString().Trim();
                        lblDat8C.Text = lLector["fecha_final"].ToString().Trim();
                        lblDat9C.Text = lLector["sentido_flujo"].ToString().Trim();
                        lblDat10C.Text = lLector["presion_punto_fin"].ToString().Trim();
                        if (lLector["ruta_contrato"].ToString().Trim().Length > 0)
                            LinkC.HRef = "../archivos/" + lLector["ruta_contrato"].ToString().Trim();
                        else
                            LinkC.Visible = false;
                    }
                }
                else
                {
                    lblMensaje.Text = "Problemas al Obtener los Datos de la Compra.! " + goInfo.mensaje_error.ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + lblMensaje.Text + "');", true);
                }
                lLector.Close();
                lLector.Dispose();
                //// Obtengo los Datos de la Venta
                lValorParametros[2] = "V";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetRegistroContratosDatos", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error == "")
                {
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lblDat1V.Text = lLector["contrato_definitivo"].ToString().Trim().ToUpper();
                        lblDat2V.Text = lLector["fecha_suscripcion_cont"].ToString().Trim();
                        lblDat3V.Text = lLector["codigo_modalidad"].ToString().Trim() + " - " + lLector["desc_modalidad"].ToString().Trim();
                        lblDat4V.Text = lLector["codigo_punto_entrega"].ToString().Trim() + " - " + lLector["desc_punto_ent"].ToString().Trim();
                        lblDat5V.Text = lLector["cantidad"].ToString().Trim();
                        lblDat6V.Text = lLector["precio"].ToString().Trim();
                        lblDat7V.Text = lLector["fecha_inicial"].ToString().Trim();
                        lblDat8V.Text = lLector["fecha_final"].ToString().Trim();
                        lblDat9V.Text = lLector["sentido_flujo"].ToString().Trim();
                        lblDat10V.Text = lLector["presion_punto_fin"].ToString().Trim();
                        if (lLector["ruta_contrato"].ToString().Trim().Length > 0)
                            LinkV.HRef = "../archivos/" + lLector["ruta_contrato"].ToString().Trim();
                        else
                            LinkV.Visible = false;
                    }
                }
                else
                {
                    lblMensaje.Text = "Problemas al Obtener los Datos de la Compra.! " + goInfo.mensaje_error.ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + lblMensaje.Text + "');", true);
                }
                lLector.Close();
                lLector.Dispose();
                lblDat1Ver.Text = "Correcta";
                lblDat2Ver.Text = "Correcta";
                lblDat3Ver.Text = "Correcta";
                lblDat4Ver.Text = "Correcta";
                lblDat5Ver.Text = "Correcta";
                lblDat6Ver.Text = "Correcta";
                lblDat7Ver.Text = "Correcta";
                lblDat8Ver.Text = "Correcta";
                lblDat9Ver.Text = "Correcta";
                lblDat10Ver.Text = "Correcta";
                /// Realizo la Verificacion entre los Datos de la Compra y la Venta
                if (hdfDestinoRueda.Value == "G")
                {
                    TrTra1.Visible = false;
                    TrTra2.Visible = false;
                    lblTit1.Text = "Número del contrato";
                    lblTit2.Text = "Fecha de suscripción del contrato";
                    lblTit3.Text = "Modalidad de Contrato";
                    lblTit4.Text = "Punto de entrega de la energía al comprador";
                    lblTit5.Text = "Cantidad de energía contratada (MBTUD)";
                    lblTit6.Text = "Precio a la fecha de suscripción del contrato (USD/MBTU)";
                    lblTit7.Text = "Fecha de inicio de la obligación de entrega";
                    lblTit8.Text = "Fecha de terminación de la obligación de entrega";
                    lblTit11.Text = "Copia Contrato";
                    if (lblDat1V.Text != lblDat1C.Text)
                    {
                        lsVerifica = "0";
                        lblDat1Ver.Text = "Incorrecta";
                    }
                    if (lblDat2V.Text != lblDat2C.Text)
                    {
                        lsVerifica = "0";
                        lblDat2Ver.Text = "Incorrecta";
                    }
                    if (lblDat3V.Text != lblDat3C.Text)
                    {
                        lsVerifica = "0";
                        lblDat3Ver.Text = "Incorrecta";
                    }
                    if (lblDat4V.Text != lblDat4C.Text)
                    {
                        lsVerifica = "0";
                        lblDat4Ver.Text = "Incorrecta";
                    }
                    if (lblDat5V.Text != lblDat5C.Text)
                    {
                        lsVerifica = "0";
                        lblDat5Ver.Text = "Incorrecta";
                    }
                    if (lblDat6V.Text != lblDat6C.Text)
                    {
                        lsVerifica = "0";
                        lblDat6Ver.Text = "Incorrecta";
                    }
                    if (lblDat7V.Text != lblDat7C.Text)
                    {
                        lsVerifica = "0";
                        lblDat7Ver.Text = "Incorrecta";
                    }
                    if (lblDat8V.Text != lblDat8C.Text)
                    {
                        lsVerifica = "0";
                        lblDat8Ver.Text = "Incorrecta";
                    }
                }
                else
                {
                    lblTit1.Text = "Número del contrato";
                    lblTit2.Text = "Fecha de suscripción del contrato";
                    lblTit3.Text = "Modalidad de Contrato";
                    lblTit4.Text = "Tramo o grupos de gasoductos";
                    lblTit5.Text = "Capacidad Contratada  (KPCD)";
                    lblTit6.Text = "Tarifa a la fecha de suscripción del contrato (USD/KPC)";
                    lblTit7.Text = "Fecha de inicio de la prestación del servicio";
                    lblTit8.Text = "Fecha de terminación prestación del servicio";
                    lblTit9.Text = "Sentido Contratado para el flujo de Gas Natural";
                    lblTit10.Text = "Presión Punto Final";

                    lblTit11.Text = "Copia Contrato";
                    if (lblDat1V.Text != lblDat1C.Text)
                    {
                        lsVerifica = "0";
                        lblDat1Ver.Text = "Incorrecta";
                    }
                    if (lblDat2V.Text != lblDat2C.Text)
                    {
                        lsVerifica = "0";
                        lblDat2Ver.Text = "Incorrecta";
                    }
                    if (lblDat3V.Text != lblDat3C.Text)
                    {
                        lsVerifica = "0";
                        lblDat3Ver.Text = "Incorrecta";
                    }
                    if (lblDat4V.Text != lblDat4C.Text)
                    {
                        lsVerifica = "0";
                        lblDat4Ver.Text = "Incorrecta";
                    }
                    if (lblDat5V.Text != lblDat5C.Text)
                    {
                        lsVerifica = "0";
                        lblDat5Ver.Text = "Incorrecta";
                    }
                    if (lblDat6V.Text != lblDat6C.Text)
                    {
                        lsVerifica = "0";
                        lblDat6Ver.Text = "Incorrecta";
                    }
                    if (lblDat7V.Text != lblDat7C.Text)
                    {
                        lsVerifica = "0";
                        lblDat7Ver.Text = "Incorrecta";
                    }
                    if (lblDat8V.Text != lblDat8C.Text)
                    {
                        lsVerifica = "0";
                        lblDat8Ver.Text = "Incorrecta";
                    }
                    if (hdfTipoMerc.Value == "P")
                    {
                        TrTra1.Visible = true;
                        TrTra2.Visible = true;
                        if (lblDat9V.Text != lblDat9C.Text)
                        {
                            lsVerifica = "0";
                            lblDat9Ver.Text = "Incorrecta";
                        }
                        if (lblDat10V.Text != lblDat10C.Text)
                        {
                            lsVerifica = "0";
                            lblDat10Ver.Text = "Incorrecta";
                        }
                    }
                    else
                    {
                        TrTra1.Visible = false;
                        TrTra2.Visible = false;
                    }
                }
                if (lsVerifica == "0")
                {
                    btnRegCont.Visible = false;
                }
                else
                    btnRegCont.Visible = true;
                tblGrilla.Visible = false;
                tblVerifica.Visible = true;
                tblDatos.Visible = false;
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas en la Recuperacion de la Información. " + ex.Message.ToString();
            }
        }
        /// Realiza la Eliminacion del Registro
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_contrato_definitivo", "@P_observacion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
            Object[] lValorParametros = { "0", "", "" };
            string lsNoCont = "";
            if (TxtObservacionElim.Text.Trim().Length > 0)
            {
                SqlTransaction oTransaccion;
                lConexion.Abrir();
                oTransaccion = lConexion.gObjConexion.BeginTransaction(System.Data.IsolationLevel.Serializable);
                try
                {
                    hdfIdVerif.Value = e.Item.Cells[9].Text;
                    lsNoCont = e.Item.Cells[27].Text; // 20160603 fuente o campo
                    //// Obtengo la Informacion del Comprador
                    lValorParametros[0] = hdfIdVerif.Value;
                    lValorParametros[1] = lsNoCont;
                    lValorParametros[2] = TxtObservacionElim.Text.Trim();
                    goInfo.mensaje_error = "";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_EliminaRegistroContratosExt", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                    if (goInfo.mensaje_error == "")
                    {
                        lLector.Close();
                        lLector.Dispose();
                        oTransaccion.Commit();
                        lConexion.Cerrar();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Eliminado Correctamente.!" + "');", true);
                        TxtObservacionElim.Text = "";
                        CargarDatos();
                    }
                    else
                    {
                        lLector.Close();
                        lLector.Dispose();
                        oTransaccion.Rollback();
                        lConexion.Cerrar();
                        lblMensaje.Text = "Se Presente un Error en la Eliminación del Registro .! " + goInfo.mensaje_error;
                    }

                }
                catch (Exception ex)
                {
                    oTransaccion.Rollback();
                    lConexion.Cerrar();
                    lblMensaje.Text = "Se Presente un Error en la Eliminación del Registro .! " + ex.Message.ToString();
                }
            }
            else
            {
                lblMensaje.Text = "Debe Ingresar la Observación de la Eliminacion.!";
            }
        }
    }
    /// <summary>
    /// Metodo que inactiva los campos de la pantalla cuando se esta corrigiendo el registro despues
    /// de la verificacion.
    /// Requerimiento ALgoritmo y verificacion 20150529
    /// </summary>
    protected void inactivarCampos()
    {
        string[] lsNombreParametros = { "@P_codigo_cont_datos", "@P_codigo_verif_contrato", "@P_punta_contrato" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
        Object[] lValorParametros = { "0", "0", "" };
        SqlDataReader oLector;

        string lsContratoC = "";
        string lsfechaSusC = "";
        string lsCodModaC = "";
        string lsCodPtoC = "";
        string lsCantC = "";
        string lsPrecC = "";
        string lsFecIniC = "";
        string lsFecFinC = "";
        string lsSentiC = "";
        string lsPresionC = "";

        string lsContratoV = "";
        string lsfechaSusV = "";
        string lsCodModaV = "";
        string lsCodPtoV = "";
        string lsCantV = "";
        string lsPrecV = "";
        string lsFecIniV = "";
        string lsFecFinV = "";
        string lsSentiV = "";
        string lsPresionV = "";

        try
        {
            lConexion1.Abrir();
            // Obtengo los Datos de la Punta Compradora
            lValorParametros[1] = hdfIdVerif.Value;
            lValorParametros[2] = "C";
            oLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion1.gObjConexion, "pa_GetRegistroContratosDatos", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
            if (goInfo.mensaje_error == "")
            {
                if (oLector.HasRows)
                {
                    oLector.Read();
                    lsContratoC = oLector["contrato_definitivo"].ToString().Trim().ToUpper();
                    lsfechaSusC = oLector["fecha_suscripcion_cont"].ToString().Trim();
                    lsCodModaC = oLector["codigo_modalidad"].ToString() + " - " + oLector["desc_modalidad"].ToString().Trim();
                    lsCodPtoC = oLector["codigo_punto_entrega"].ToString() + " - " + oLector["desc_punto_ent"].ToString().Trim();
                    lsCantC = oLector["cantidad"].ToString().Trim();
                    lsPrecC = oLector["precio"].ToString().Trim();
                    lsFecIniC = oLector["fecha_inicial"].ToString().Trim();
                    lsFecFinC = oLector["fecha_final"].ToString().Trim();
                    lsSentiC = oLector["sentido_flujo"].ToString().Trim();
                    lsPresionC = oLector["presion_punto_fin"].ToString().Trim();
                }
            }
            oLector.Close();
            oLector.Dispose();
            // Obtengo los Datos de la Punta Vendedora
            lValorParametros[1] = hdfIdVerif.Value;
            lValorParametros[2] = "V";
            oLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion1.gObjConexion, "pa_GetRegistroContratosDatos", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
            if (goInfo.mensaje_error == "")
            {
                if (oLector.HasRows)
                {
                    oLector.Read();
                    lsContratoV = oLector["contrato_definitivo"].ToString().Trim().ToUpper();
                    lsfechaSusV = oLector["fecha_suscripcion_cont"].ToString().Trim();
                    lsCodModaV = oLector["codigo_modalidad"].ToString() + " - " + oLector["desc_modalidad"].ToString().Trim();
                    lsCodPtoV = oLector["codigo_punto_entrega"].ToString() + " - " + oLector["desc_punto_ent"].ToString().Trim();
                    lsCantV = oLector["cantidad"].ToString().Trim();
                    lsPrecV = oLector["precio"].ToString().Trim();
                    lsFecIniV = oLector["fecha_inicial"].ToString().Trim();
                    lsFecFinV = oLector["fecha_final"].ToString().Trim();
                    lsSentiV = oLector["sentido_flujo"].ToString().Trim();
                    lsPresionV = oLector["presion_punto_fin"].ToString().Trim();
                }
            }
            oLector.Close();
            oLector.Dispose();
            // Compraro los datos para inhabilitar las cajas de texto
            if (lsContratoV == lsContratoC)
                TxtNumContrato.Enabled = false;
            if (lsfechaSusV == lsfechaSusC)
                TxtFechaSus.Enabled = false;
            if (lsCodModaV == lsCodModaC)
                ddlModalidad.Enabled = false;
            if (lsCodPtoV == lsCodPtoC)
                ddlPuntoEntr.Enabled = false;
            if (lsCantV == lsCantC)
                TxtCantidad.Enabled = false;
            if (lsPrecV == lsPrecC)
                TxtPrecio.Enabled = false;
            if (lsFecIniV == lsFecIniC)
            {
                TxtFechaInicial.Enabled = false;
            }
            if (lsFecFinV == lsFecFinC)
            {
                TxtFechaFinal.Enabled = false;
            }
            if (lsSentiV == lsSentiC)
                ddlSentidoFlujo.Enabled = false;
            if (lsPresionV == lsPresionC)
                TxtPresion.Enabled = false;
            lConexion1.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Se presento un Error en el BLoqueo de los Campos de la Corrección. " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// Exportacion a Excel de la Información  de la Grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbExcel_Click(object sender, ImageClickEventArgs e)
    {
        string lsParametros = "";
        DateTime ldFecha;
        int liValor = 0;
        string[] lValorParametros = { "0", "0", "", "", "0", "0", "0", Session["tipoPerfil"].ToString(), goInfo.cod_comisionista, "0", "0", "0", "" };  //20161213 rq102 comformacion de rutas

        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Negociación. <br>"; //20161207 rq102 conoformacion de rutas
            }
        }
        //20161213 rq102 conformacion de rutas
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaFin.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Final de Negociación. <br>";
            }
        }
        //20161213 rq102 conformacion de rutas
        if (TxtFechaIni.Text.Trim().Length == 0 && TxtFechaFin.Text.Trim().Length > 0)
            lblMensaje.Text += "Debe digitar la Fecha Inicialde Negociación. <br>";
        //20161213 rq102 conformacion de rutas
        if (TxtFechaIni.Text.Trim().Length > 0 && TxtFechaFin.Text.Trim().Length > 0)
            try
            {
                if (Convert.ToDateTime(TxtFechaFin.Text) < Convert.ToDateTime(TxtFechaIni.Text))
                    lblMensaje.Text += "La Fecha inicial de Negociación debe ser menor o igual que la fecha final. <br>";
            }
            catch (Exception ex)
            { }

        if (TxtIdRegini.Text.Trim().Length > 0)
        {
            try
            {
                liValor = Convert.ToInt32(TxtIdRegini.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo No. Operación Inicial. <br>";  //20161213 rq102 conformacion de rutas
            }
        }
        if (TxtIdRegFin.Text.Trim().Length > 0)
        {
            try
            {
                liValor = Convert.ToInt32(TxtIdRegFin.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo No. Operación Final. <br>";  //20161213 rq102 conformación de rutas
            }
        }
        if (TxtIdRegini.Text.Trim().Length > 0 && TxtIdsReg.Text.Trim().Length > 0)
            lblMensaje.Text += "Debe Hacer Busqueda por Rango de operaciones o por Números de operaciones, no por los dos al Tiempo. <br>"; //20161213 rq102 coformacion de rutas

        if (lblMensaje.Text == "")
        {

            try
            {
                if (TxtIdRegini.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtIdRegini.Text.Trim();
                if (TxtIdRegFin.Text.Trim().Length > 0)
                    lValorParametros[1] = TxtIdRegFin.Text.Trim();
                else
                {
                    if (TxtIdRegini.Text.Trim().Length > 0)
                        lValorParametros[1] = TxtIdRegini.Text.Trim();
                }
                if (TxtFechaIni.Text.Trim().Length > 0)
                    lValorParametros[2] = TxtFechaIni.Text.Trim();
                if (ddlMercado.SelectedValue != "")
                    lValorParametros[3] = ddlMercado.SelectedValue;
                if (ddlProducto.SelectedValue != "0")
                    lValorParametros[4] = ddlProducto.SelectedValue;
                if (ddlComprador.SelectedValue != "0")
                    lValorParametros[5] = ddlComprador.SelectedValue;
                if (ddlVendedor.SelectedValue != "0")
                    lValorParametros[6] = ddlVendedor.SelectedValue;
                if (ddlEstado.SelectedValue != "0")
                    lValorParametros[9] = ddlEstado.SelectedValue;
                if (TxtIdsReg.Text.Trim().Length > 0)
                    lValorParametros[10] = TxtIdsReg.Text.Trim();
                if (TxtNoContrato.Text.Trim().Length > 0)
                    lValorParametros[11] = TxtNoContrato.Text.Trim();
                //20161213 rq 102 conformacion de rutas
                if (TxtFechaFin.Text.Trim().Length > 0)
                    lValorParametros[12] = TxtFechaFin.Text.Trim();
                else
                    lValorParametros[12] = lValorParametros[2];

                Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetRegistroContratosExtExcel&nombreParametros=@P_id_inicial*@P_id_final*@P_fecha_contrato*@P_mercado*@P_codigo_producto*@P_operador_compra*@P_operador_venta*@P_tipo_perfil*@P_codigo_operador*@P_estado*@P_numero_ids*@P_numero_contrato*@P_fecha_fin_cont&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "*" + lValorParametros[3] + "*" + lValorParametros[4] + "*" + lValorParametros[5] + "*" + lValorParametros[6] + "*" + lValorParametros[7] + "*" + lValorParametros[8] + "*" + lValorParametros[9] + "*" + lValorParametros[10] + "*" + lValorParametros[11] + "*" + lValorParametros[12] + "&columnas=Producto*valor_negocio*dias_plazo*valor_tasa_venta*valor_tasa_compra*valor_tasa_cierre&titulo_informe=Listado Contratos Antes del GestorParametros=" + lsParametros);  //20161213 rq102 conformacion de rutas
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pude Generar el Informe.!";
            }
        }
    }
    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_operador)
    {
        string lsCondicion = "nombre_tabla='t_contrato_verificacion' and llave_registro='codigo_verif_contrato=" + lscodigo_operador + "'";
        string lsCondicion1 = "codigo_verif_contrato=" + lscodigo_operador.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "t_contrato_verificacion";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_contrato_verificacion", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Realiza las Validaciones de los Tipos de Datos y Campos Obligatorios de la Pantalla
    /// </summary>
    /// <returns></returns>
    protected string validaciones()
    {
        string lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "/" + DateTime.Now.ToShortDateString().Substring(3, 2) + "/" + DateTime.Now.ToShortDateString().Substring(0, 2);
        int liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
        int liValFin = 0;
        string lsHoraFin = "";
        decimal ldValor = 0;
        int liValor = 0;
        DateTime lFecha;
        string lsError = "";
        string[] lsPrecio;
        string[] lsFechaV;
        string[] lsOperaC;
        string[] lsOperaV;
        string lsTipoC = "";
        string lsTipoV = "";
        lsOperaC = ddlCapComprador.SelectedValue.Split('-');
        lsOperaV = ddlCapVendedor.SelectedValue.Split('-');

        try
        {
            if (ddlCapComprador.SelectedValue == ddlCapVendedor.SelectedValue)
                lsError += "El Operador Vendedor no puede ser igual al operador Comprador. <br>";

            lConexion.Abrir();
            if (ddlCapTipoMercado.SelectedValue == "P")
            {
                if (ddlCapPunta.SelectedValue == "C")
                {
                    /// Obtengo el mail del Operador Compra
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + lsOperaV[0].Trim() + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsTipoV = lLector["tipo_operador"].ToString();
                    }
                    lLector.Close();
                    lLector.Dispose();
                    if (lsTipoV != "P" && lsTipoV != "T" && lsTipoV != "I")
                        lsError += "El Operador Vendedor Solo puede ser Productor, Transportador o Comercializador de Gas Importado. <br>";
                }
                else
                {
                    /// Obtengo el mail del Operador Venta
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + lsOperaV[0].Trim() + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsTipoV = lLector["tipo_operador"].ToString();
                    }
                    lLector.Close();
                    lLector.Dispose();
                    if (lsTipoV != "P" && lsTipoV != "T" && lsTipoV != "I")
                        lsError += "El Operador Vendedor Solo puede ser Productor, Trasportador o Comercializador de Gas Importado. <br>";
                }
            }
            else
            {
                // COntroles para el mercado secundario de los contratos EXT 20151021
                // Valido el Operador Compra
                if (ddlCapComprador.SelectedValue == "0")
                    lsError += "Debe Seleccionar el Operador Compra. <br>";
                else
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_operador_subasta tipoOp, m_operador opera, m_tipos_operador tipo, m_tipos_rueda rueda", "tipoOp.codigo_tipo_subasta = 5  And opera.codigo_operador = " + lsOperaC[0].Trim() + "  And opera.tipo_operador = tipo.sigla And tipoOp.punta = 'C' And tipo.codigo_tipo_operador = tipoOp.codigo_tipo_operador And tipoOp.codigo_tipo_rueda = rueda.codigo_tipo_rueda And rueda.destino_rueda = '" + hdfDestinoRueda.Value + "' And rueda.tipo_mercado = 'S'  ", goInfo))
                        lsError += "El Tipo de Operador de la punta Compradora NO esta parametrizado en los parámetros de Operador Subasta para Comprar. <br>";
                }
                // Valido el Operador Vendedora
                if (ddlCapVendedor.SelectedValue == "0")
                    lsError += "Debe Seleccionar el Operador Venta. <br>";
                else
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_operador_subasta tipoOp, m_operador opera, m_tipos_operador tipo, m_tipos_rueda rueda", "tipoOp.codigo_tipo_subasta = 5 And opera.codigo_operador = " + lsOperaV[0].Trim() + "  And opera.tipo_operador = tipo.sigla And tipoOp.punta = 'C' And tipo.codigo_tipo_operador = tipoOp.codigo_tipo_operador And tipoOp.codigo_tipo_rueda = rueda.codigo_tipo_rueda And rueda.destino_rueda = '" + hdfDestinoRueda.Value + "' And rueda.tipo_mercado = 'S'  ", goInfo))
                        lsError += "El Tipo de Operador de la punta Vendedora NO esta parametrizado en los parámetros de Operador Subasta para Vender. <br>";
                }
            }
            lConexion.Cerrar();
            if (TxtCapFechaNeg.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar la Fecha de Negociación. <br>";
            else
            {
                try
                {
                    lFecha = Convert.ToDateTime(TxtCapFechaNeg.Text.Trim());
                    lsFechaV = TxtCapFechaNeg.Text.Trim().Split('/');
                    if (lsFechaV[0].Trim().Length != 4)
                        lsError += "Formato Invalido en Fecha de Negociación. <br>"; //20161207 rq102 conoformacion de rutas
                    else
                    {
                        if (lFecha > Convert.ToDateTime("2015/01/05"))
                            lsError += "La Fecha de Negociación NO puede ser mayor que 2015/01/05. <br>";
                    }
                }
                catch (Exception ex)
                {
                    lsError += "Valor Invalido en Fecha de Negociación. <br>";
                }
            }
            if (ddlCapProducto.SelectedValue == "0")
                lsError += "Debe Seleccionar el Producto. <br>";
            if (ddlCapPunta.SelectedValue == "0")
                lsError += "Debe Seleccionar Punta que Registra. <br>";
            if (ddlCapTipoMercado.SelectedValue == "0")
                lsError += "Debe Seleccionar Tipo del Mercado. <br>";
            if (ddlCapPunta.SelectedValue == "C")
            {
                if (ddlCapVendedor.SelectedValue == "0")
                    lsError += "Debe Seleccionar el Vendedor del Contrato. <br>";
            }
            else
            {
                if (ddlCapComprador.SelectedValue == "0")
                    lsError += "Debe Seleccionar el Comprador del Contrato. <br>";
            }
            if (TxtNumContrato.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar el No. de Contrato. <br>";
            if (TxtFechaSus.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar la Fecha de Suscripción. <br>";
            else
            {
                try
                {
                    lFecha = Convert.ToDateTime(TxtFechaSus.Text.Trim());
                    lsFechaV = TxtFechaSus.Text.Trim().Split('/');
                    if (lsFechaV[0].Trim().Length != 4)
                        lsError += "Formato Invalido en Fecha de Suscripcion. <br>";

                }
                catch (Exception ex)
                {
                    lsError += "Valor Invalido en Fecha de Sescripción. <br>";
                }
            }
            if (ddlModalidad.SelectedValue == "0" && TxtOtrModalidad.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar la Modalidad de Contrato. <br>";
            if (ddlPuntoEntr.SelectedValue == "0" && TxtOtrPuntoE.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar el Punto de Entrega. <br>";
            if (TxtCantidad.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar " + lblCantidad.Text + " . <br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCantidad.Text.Trim());
                    if (liValor < 0)
                        lsError += "Valor Invalido en " + lblCantidad.Text + ". <br>";
                }
                catch (Exception ex)
                {
                    lsError += "Valor Invalido en " + lblCantidad.Text + ". <br>";
                }
            }
            if (TxtPrecio.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar " + lblPrecio.Text + " . <br>";
            else
            {
                try
                {
                    ldValor = Convert.ToDecimal(TxtPrecio.Text.Trim());
                    if (ldValor < 0)
                        lsError += "Valor Invalido en " + lblPrecio.Text + ". <br>";
                    else
                    {
                        lsPrecio = TxtPrecio.Text.Trim().Split('.');
                        if (lsPrecio.Length > 1)
                        {
                            if (lsPrecio[1].Trim().Length > 2)
                                lsError += "Valor Invalido en " + lblPrecio.Text + ". <br>";
                        }
                    }
                }
                catch (Exception ex)
                {
                    lsError += "Valor Invalido en " + lblPrecio.Text + ". <br>";
                }
            }
            if (TxtFechaInicial.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar " + lblFechaInc.Text + ". <br>";
            else
            {
                try
                {
                    lFecha = Convert.ToDateTime(TxtFechaInicial.Text.Trim());
                    lsFechaV = TxtFechaInicial.Text.Trim().Split('/');
                    if (lsFechaV[0].Trim().Length != 4)
                        lsError += "Formato Invalido en " + lblFechaInc.Text + ". <br>";

                }
                catch (Exception ex)
                {
                    lsError += "Valor Invalido en " + lblFechaInc.Text + ". <br>";
                }
            }
            if (TxtFechaFinal.Text.Trim().Length <= 0)
                lsError += "Debe Ingresar " + lblFechaFin.Text + ". <br>";
            else
            {
                try
                {
                    lFecha = Convert.ToDateTime(TxtFechaFinal.Text.Trim());
                    if (Convert.ToDateTime(TxtFechaFinal.Text.Trim()) < Convert.ToDateTime(TxtFechaInicial.Text.Trim()))
                        lsError += "La " + lblFechaFin.Text + " No puede ser menor que " + lblFechaInc.Text + " . <br>";
                    lsFechaV = TxtFechaInicial.Text.Trim().Split('/');
                    if (lsFechaV[0].Trim().Length != 4)
                        lsError += "Formato Invalido en " + lblFechaFin.Text + ". <br>";

                }
                catch (Exception ex)
                {
                    lsError += "Valor Invalido en " + lblFechaFin.Text + ". <br>";
                }
            }
            if (hdfAccion.Value == "C")
            {
                //if (FuCopiaCont.FileName.Trim().Length <= 0)
                //    lsError += "Debe Seleccionar el Archivo del Contrato. <br>";
            }
            else
            {
                if (ddlMotivoModifi.SelectedValue == "0")
                    lsError += "Debe Seleccionar el Motivo de Modificación. <br>";//20161207 rq102 conoformacion de rutas
                else
                {
                    if (DelegadaBase.Servicios.ValidarExistencia("m_motivos_modificacion", " codigo_motivo_modifica = " + ddlMotivoModifi.SelectedValue + " And cesion = 'S' ", goInfo))
                    {
                        if (ddlCesionario.SelectedValue == "0")
                            lsError += "Debe Seleccionar el Cesionario YA que el Motivo de Modificación es Cesion. <br>"; //20161207 rq102 conoformacion de rutas
                    }
                }
            }
            if (hdfPunta.Value == "C")
            {
                //if (dlTipoDemanda.SelectedValue == "0")
                //    lsError += "Debe Seleccionar El Tipo de Demanda a Atender. <br>";
                //else
                //{
                string lsCont = "0";
                if (hdfCodVerifCont.Value.Trim().Length > 0)
                    lsCont = hdfCodVerifCont.Value;
                if (!DelegadaBase.Servicios.ValidarExistencia("t_contrato_datos_usuarios", " codigo_cont_datos = " + lsCont + " And login_usuario = '" + goInfo.Usuario + "'", goInfo))
                    lsError += "No ha Ingresado Información de Usuarios Finales. <br>"; //20161207 rq102 conoformacion de rutas
                else
                {
                    if (hdfDestinoRueda.Value == "G")
                    {
                        if (Convert.ToDecimal(TxtCantidad.Text) != Convert.ToDecimal(lblTotlCantidad.Text))
                            lsError += "Hay Diferencias entre a Cantidad del Contrato y la Cantidad de los usuarios Finales. <br>";
                    }
                    else // Valicacion agregada para mercado relevante secto 20160125
                    {
                        if (Convert.ToDecimal(TxtCantidad.Text) < Convert.ToDecimal(lblTotlCantidad.Text))
                            lsError += "La Cantidad del Contrato no puede ser menor que la Cantidad de los usuarios Finales. <br>";
                    }
                }
                //  }
            }
            if (hdfDestinoRueda.Value == "T" && hdfTipoMerc.Value == "P")
            {
                if (ddlSentidoFlujo.SelectedValue == "")
                    lsError += "Debe Ingresar Sentido Contratado para el flujo de Gas Natural. <br>";
                if (TxtPresion.Text.Trim().Length <= 0)
                    lsError += "Debe Ingresar Presión para el punto de terminación del servicio (psig). <br>";
                else
                {
                    try
                    {
                        ldValor = Convert.ToDecimal(TxtPresion.Text.Trim());
                        if (ldValor < 0)
                            lsError += "Valor Invalido en Presión para el punto de terminación del servicio (psig). <br>";
                    }
                    catch (Exception ex)
                    {
                        lsError += "Valor Invalido en Presión para el punto de terminación del servicio (psig). <br>";
                    }
                }
            }
            //20160808 modalidad por titpo de campo
            //Fuente o campo MP 20160602
            if (ddlCapProducto.SelectedValue == "1-G" && ddlCapTipoMercado.SelectedValue == "P")
            {
                //20160808 modalidad campo
                if (ddlFuente.SelectedValue == "0")
                    lsError += "Debe seleccionar la Fuente. <br>";
                //rq012-17
                //else
                //{
                //    if (!DelegadaBase.Servicios.ValidarExistencia("m_pozo poz, m_caracteristica_sub carC", " poz.codigo_pozo = " + ddlFuente.SelectedValue + "  and carC.codigo_tipo_subasta = 5 and carC.tipo_caracteristica ='C' and carC.destino_rueda ='G' and carC.tipo_mercado='P' and carC.estado ='A' and codigo_caracteristica = poz.codigo_tipo_campo", goInfo))
                //        lsError += "El tipo de campo no está parametrizado para el tipo de mercado y producto. <br>";

                //    //No se puede colocar porque no tenemos el periodo e entrega
                //    //if (DelegadaBase.Servicios.ValidarExistencia("m_pozo pozo, m_tipos_rueda tpo, t_contrato con, t_id_rueda idr, m_restriccion_subasta rest", " pozo.codigo_pozo = " + ddlFuente.SelectedValue + " And rest.codigo_tipo_subasta = 5 and tpo.codigo_tipo_subasta = 5 and tpo.destino_rueda ='G' and tpo.tipo_mercado = 'P' and con.numero_contrato = " + lblOperacion.Text + " and con.numero_id = idr.numero_id  and tpo.codigo_tipo_rueda = rest.codigo_tipo_rueda and rest.codigo_periodo = idr.codigo_periodo and rest.codigo_modalidad =" + ddlModalidad.SelectedValue + " and (pozo.codigo_tipo_campo = rest.codigo_tipo_campo or rest.codigo_tipo_campo=0) And rest.estado ='A'", goInfo))
                //    //    lsError += "La Modalidad, Periodo, Tipo de Mercado y Tipo de Campo atado a la fuente está en los parametros de restricción de subasta para Bilateral. <br>";
                //    if (!DelegadaBase.Servicios.ValidarExistencia("m_pozo poz, m_campo_modalidad cam", " poz.codigo_pozo = " + ddlFuente.SelectedValue + "  and poz.codigo_tipo_campo= cam.codigo_tipo_campo and cam.codigo_modalidad =" + ddlModalidad.SelectedValue + " and cam.tipo_mercado='P' and cam.estado='A'", goInfo))
                //        lsError += "El tipo de contrato no está parametrizado para el tipo de campo. <br>";
                //    if (DelegadaBase.Servicios.ValidarExistencia("m_pozo poz, m_campo_periodo cam", " poz.codigo_pozo = " + ddlFuente.SelectedValue + "  and poz.codigo_tipo_campo= cam.codigo_tipo_campo and cam.codigo_tipo_subasta = 5 and cam.destino_rueda='G' and  cam.tipo_mercado='P' and cam.estado ='A' and ('" + TxtCapFechaNeg.Text + "'< cam.fecha_inicial or '" + TxtCapFechaNeg.Text + "' > cam.fecha_final)", goInfo))
                //        lsError += "La fecha de negociación no está dentro del periodo para registro de negociaciones para el tipo de campo. <br>";
                //    //20160808 modalidad campo
                //    lConexion.Abrir();
                //    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_pozo poz, m_tipo_campo tpo, m_modalidad_contractual mod, m_campo_modalidad cam", " poz.codigo_pozo =" + ddlFuente.SelectedValue + " and poz.codigo_tipo_campo = tpo.codigo_tipo_campo and tpo.estado ='A' and mod.codigo_modalidad =" + ddlModalidad.SelectedValue + " and mod.ind_contingencia ='N' and poz.codigo_tipo_campo = cam.codigo_tipo_campo and mod.codigo_modalidad = cam.codigo_modalidad and cam.tipo_mercado = 'P' and cam.estado='A' and cam.ind_restriccion ='S'");
                //    if (lLector.HasRows)
                //    {
                //        lLector.Read();
                //        if (lLector["mes_inicio"].ToString() != "0")
                //            if (Convert.ToInt32(lLector["mes_inicio"].ToString()) != Convert.ToDateTime(TxtFechaInicial.Text).Month || Convert.ToInt32(lLector["dia_inicio"].ToString()) != Convert.ToDateTime(TxtFechaInicial.Text).Day)
                //                lsError += "La fecha de inicio (" + TxtFechaInicial.Text + ") no corresponde con el mes y día parametrizado para el tipo de campo " + lLector["mes_inicio"].ToString() + "-" + lLector["dia_inicio"].ToString() + "<br>";
                //        if (lLector["mes_fin"].ToString() != "0")
                //            if (Convert.ToInt32(lLector["mes_fin"].ToString()) != Convert.ToDateTime(TxtFechaFinal.Text).Month || Convert.ToInt32(lLector["dia_fin"].ToString()) != Convert.ToDateTime(TxtFechaFinal.Text).Day)
                //                lsError += "La fecha de finalización (" + TxtFechaFinal.Text + ") no corresponde con el mes y día parametrizado para el tipo de campo " + lLector["mes_fin"].ToString() + "-" + lLector["dia_fin"].ToString() + "<br>";
                //        if (lLector["mes_inicio"].ToString() != "0" && lLector["mes_fin"].ToString() != "0")
                //        {
                //            int ldAnos = Convert.ToDateTime(TxtFechaFinal.Text).Year - Convert.ToDateTime(TxtFechaInicial.Text).Year;
                //            if (ldAnos <= 0 || ldAnos > 1 && ldAnos < 5)
                //                lsError += "La duración para este campo sebe ser de 1 o mayor o igual que 5 años <br>";
                //        }
                //    }
                //    lLector.Close();
                //    lLector.Dispose();
                //    lConexion.Cerrar();
                //}
            }

            //20160810 valida fecha
            try
            {
                if (Convert.ToDateTime(TxtCapFechaNeg.Text) >= Convert.ToDateTime(TxtFechaInicial.Text))
                    lsError += "La fecha Inicial debe ser mayor que la fecha de negociación. <br>";
            }
            catch (Exception ex)
            {
            }
            return lsError;
        }
        catch (Exception ex)
        {
            return "Problemas en la Validacion de la Información. " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// Realiza la Limpieza de los Campo de Captura en la Pantalla luego de la Creación o Actualizacion
    /// </summary>
    protected void LimpiarCampos()
    {
        TxtNumContrato.Text = "";
        TxtFechaSus.Text = "";
        ddlModalidad.SelectedValue = "0";
        ChkOtrModa.Checked = false;
        TxtOtrModalidad.Text = "";
        ddlPuntoEntr.SelectedValue = "0";
        ChkOtrPuntoE.Checked = false;
        TxtOtrPuntoE.Text = "";
        TxtCantidad.Text = "";
        TxtPrecio.Text = "";
        TxtFechaInicial.Text = "";
        TxtFechaFinal.Text = "";
        dlTipoDemanda.SelectedValue = "0";
        ddlSentidoFlujo.SelectedValue = "";
        TxtPresion.Text = "";
        ddlFuente.SelectedValue = "0"; //Fuente o campo MP 20160602
    }
    /// <summary>
    /// Nombre: dtgUsuarios_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgUsuarios_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";

        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            try
            {
                ////////////////////////////////////////////////////////////////////////////////////////
                /// OJO Cambios de los Indices por adicion de Campo Req. 009-17 Indicadores 20170321 ///
                ////////////////////////////////////////////////////////////////////////////////////////
                string lsTipoDoc = "";
                if (this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[14].Text == "1")
                    lsTipoDoc = "Nit";
                else
                    lsTipoDoc = "Cedula";
                hdfCodDatUsu.Value = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text;
                // Cambio Req. Fuente o Campo 20160906
                if (this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[1].Text == "&nbsp;")
                    TxtUsuarioFinal.Text = lsTipoDoc;
                else
                    TxtUsuarioFinal.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[1].Text + "-" + lsTipoDoc + "-" + this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[3].Text;
                //TxtUsuarioFinal.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[1].Text + "-" + lsTipoDoc + "-" + this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[3].Text;
                TxtCantidadUsu.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[6].Text;
                lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) - Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                dlTipoDemanda.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[15].Text;  //rq009-17
                dlTipoDemanda_SelectedIndexChanged(null, null);
                ddlSector.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[12].Text; //rq009-17
                ddlPuntoSalida.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[13].Text;  //rq009-17
                ddlMercadoRel.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[16].Text; //rq009-17
                btnCrearUsu.Visible = false;
                btnActualUsu.Visible = true;
                /////////////////////////////////////////////////////////////////////////
                /// Habilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///
                /////////////////////////////////////////////////////////////////////////
                if (ddlCapProducto.SelectedValue == "1-G" && ddlCapTipoMercado.SelectedValue == "P")
                {
                    TxtEquivaleKpcd.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[7].Text;
                    TdIndica.Visible = true;
                    TdIndica1.Visible = true;
                }
                else
                {
                    TdIndica.Visible = false;
                    TdIndica1.Visible = false;
                }
                /////////////////////////////////////////////////////////////////////////
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al Recuperar el Registro.! " + ex.Message.ToString();
            }

        }
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            string[] lsNombreParametros = { "@P_codigo_datos_usuarios", "@P_codigo_cont_datos", "@P_no_identificacion_usr", "@P_codigo_tipo_doc", 
                                        "@P_nombre_usuario", "@P_codigo_sector_consumo", "@P_codigo_punto_salida","@P_cantidad_contratada","@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, 
                                        SqlDbType.Int,SqlDbType.Int,SqlDbType.Int};
            string[] lValorParametros = { hdfCodDatUsu.Value, "0", "", "", "", "0", "0", " 0", "3" };

            try
            {
                ////////////////////////////////////////////////////////////////////////////////////////
                /// OJO Cambios de los Indices por adicion de Campo Req. 009-17 Indicadores 20170321 ///
                ////////////////////////////////////////////////////////////////////////////////////////
                hdfCodDatUsu.Value = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text;
                lValorParametros[0] = hdfCodDatUsu.Value;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetUsuFinalCont", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presento un Problema en la Eliminación de la Información del Usuario.!" + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) - Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                    lConexion.Cerrar();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Eliminado Correctamente.!" + "');", true);
                    CargarDatosUsu();
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al Eliminar el Registro.! " + ex.Message.ToString();

            }
        }

    }
    /// <summary>
    /// Nombre: CargarDatosUsu
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid de Usuarios.
    /// Modificacion:
    /// </summary>
    private void CargarDatosUsu()
    {
        lblMensaje.Text = "";
        string[] lsNombreParametros = { "@P_codigo_cont_datos", "@P_no_identificacion_usr", "@P_login" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
        string[] lValorParametros = { "-1", "", "" };
        try
        {
            if (hdfCodVerifCont.Value.Trim().Length > 0)
                lValorParametros[0] = hdfCodVerifCont.Value;
            if (Session["tipoPerfil"].ToString() == "N")
                lValorParametros[2] = goInfo.Usuario.ToString();

            lConexion.Abrir();
            dtgUsuarios.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRegistroContratosUsu", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgUsuarios.DataBind();
            lConexion.Cerrar();
            lblTotlCantidad.Text = "0";
            if (dtgUsuarios.Items.Count > 0)
            {
                foreach (DataGridItem Grilla in this.dtgUsuarios.Items)
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(Grilla.Cells[6].Text.Trim())).ToString();
                }
                /////////////////////////////////////////////////////////////////////////
                /// Habilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///
                /////////////////////////////////////////////////////////////////////////
                if (ddlCapProducto.SelectedValue == "1-G" && ddlCapTipoMercado.SelectedValue == "P")
                    dtgUsuarios.Columns[7].Visible = true;
                else
                    dtgUsuarios.Columns[7].Visible = false;
                /////////////////////////////////////////////////////////////////////////

            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.! " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImbSalir_Click(object sender, ImageClickEventArgs e)
    {
        tblDemanda.Visible = false;
        tblGrilla.Visible = true;
        tblDatos.Visible = true;
        tblVerifica.Visible = false;
        CargarDatos();
    }
    /// <summary>
    /// Realiza la actualizacion del estado y envia la Alerta para solicitar correccion de los datos del Registro.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSolCorr_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_estado", "@P_observacion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char, SqlDbType.VarChar };
        String[] lValorParametros = { hdfIdVerif.Value, "E", TxtObservacion.Text };

        try
        {
            lConexion.Abrir();
            if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetActualVerifDatosReg", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
            {
                lblMensaje.Text = "Se presento un Problema en la Actualizacion de la Información del Usuario.!" + goInfo.mensaje_error.ToString();
                lConexion.Cerrar();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Actualizado Correctamente.!" + "');", true);
                string lsAsunto = "";
                string lsMensaje = "";
                string lsMensajeC = "";
                string lsMailC = "";
                string lsNomOperadorC = "";
                string lsMailV = "";
                string lsNomOperadorV = "";
                /// Obtengo el mail del Operador Compra
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraC.Value + " ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsMailC = lLector["e_mail"].ToString();
                    if (lLector["tipo_persona"].ToString() == "J")
                        lsNomOperadorC = lLector["razon_social"].ToString();
                    else
                        lsNomOperadorC = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                }
                lLector.Close();
                lLector.Dispose();
                /// Obtengo el mail del Operador Venta
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraV.Value + " ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsMailV = lLector["e_mail"].ToString();
                    if (lLector["tipo_persona"].ToString() == "J")
                        lsNomOperadorV = lLector["razon_social"].ToString();
                    else
                        lsNomOperadorV = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                ///// Envio del Mail de la Solicitud de la Correcion
                lsAsunto = "Notificación Solicitud Corrección Registro y Verificación";
                lsMensaje += "Nos permitimos informarle que el Administrador de SEGAS acaba de Solicitar la corrección de la información transaccional " + lblOperacion.Text + ", Por favor revisar los siguientes campos: <br><br>";
                if (lblDat1Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit1.Text + "<br>";
                if (lblDat2Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit2.Text + "<br>";
                if (lblDat3Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit3.Text + "<br>";
                if (lblDat4Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit4.Text + "<br>";
                if (lblDat6Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit5.Text + "<br>";
                if (lblDat6Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit6.Text + "<br>";
                if (lblDat7Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit7.Text + "<br>";
                if (lblDat8Ver.Text != "Correcta")
                    lsMensaje += " - " + lblTit8.Text + "<br>";
                if (hdfDestinoRueda.Value == "T" && hdfTipoMerc.Value == "P")
                {
                    if (lblDat9Ver.Text != "Correcta")
                        lsMensaje += " - " + lblTit9.Text + "<br>";
                    if (lblDat10Ver.Text != "Correcta")
                        lsMensaje += " - " + lblTit10.Text + "<br>";
                }
                lsMensaje += "\n\nObservaciones: " + TxtObservacion.Text + "<br><br>";
                lsMensaje += "Cordialmente, <br><br><br>";
                lsMensaje += "Administrador SEGAS <br>";
                lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                clEmail mailC = new clEmail(lsMailC, lsAsunto, lsMensaje, "");
                lblMensaje.Text = mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                ////
                lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensaje, "");
                lblMensaje.Text = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                tblDemanda.Visible = false;
                tblGrilla.Visible = true;
                tblDatos.Visible = true;
                tblVerifica.Visible = false;
                CargarDatos();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas en la Actualizacion de la Información. " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRegCont_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_estado", "@P_observacion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char, SqlDbType.VarChar };
        String[] lValorParametros = { hdfIdVerif.Value, "R", TxtObservacion.Text };

        try
        {
            lConexion.Abrir();
            if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetActualVerifDatosReg", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
            {
                lblMensaje.Text = "Se presento un Problema en la Actualizacion de la Información del Usuario.!" + goInfo.mensaje_error.ToString();
                lConexion.Cerrar();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Actualizado Correctamente.!" + "');", true);
                string lsAsunto = "";
                string lsMensaje = "";
                string lsMensajeC = "";
                string lsMailC = "";
                string lsNomOperadorC = "";
                string lsMailV = "";
                string lsNomOperadorV = "";
                /// Obtengo el mail del Operador Compra
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraC.Value + " ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsMailC = lLector["e_mail"].ToString();
                    if (lLector["tipo_persona"].ToString() == "J")
                        lsNomOperadorC = lLector["razon_social"].ToString();
                    else
                        lsNomOperadorC = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                }
                lLector.Close();
                lLector.Dispose();
                /// Obtengo el mail del Operador Venta
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraV.Value + " ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsMailV = lLector["e_mail"].ToString();
                    if (lLector["tipo_persona"].ToString() == "J")
                        lsNomOperadorV = lLector["razon_social"].ToString();
                    else
                        lsNomOperadorV = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                ///// Envio del Mail de la Solicitud de la Correcion
                lsAsunto = "Notificación Registro Contrato";
                lsMensaje += "Nos permitimos informarle que el Administrador de la plataforma SEGAS acaba de realizar exitosamente el Registro del Contrato de la Operación No: " + lblOperacion.Text + " con No. de Registro: " + hdfIdVerif.Value + ".<br><br>";
                lsMensaje += "\n\nObservaciones: " + TxtObservacion.Text + "<br><br>";
                lsMensaje += "Cordialmente, <br><br><br>";
                lsMensaje += "Administrador SEGAS <br>";
                lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                clEmail mailC = new clEmail(lsMailC, lsAsunto, lsMensaje, "");
                lblMensaje.Text = mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                ////
                lsMensajeC = "Señores: " + lsNomOperadorV + " <br>" + lsMensaje;
                clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensaje, "");
                lblMensaje.Text = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                CargarDatos();
                tblDemanda.Visible = false;
                tblGrilla.Visible = true;
                tblDatos.Visible = true;
                tblVerifica.Visible = false;
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas en la Actualizacion de la Información. " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImmSalirVer_Click(object sender, ImageClickEventArgs e)
    {
        tblDemanda.Visible = false;
        tblGrilla.Visible = true;
        tblDatos.Visible = true;
        tblVerifica.Visible = false;
        CargarDatos();
    }
    /// <summary>
    /// Activar pantalla para la Creacion del Registro
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImbCrearReg_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            ddlCapProducto.Enabled = true; // fuente o campo MP 20160602
            hdfCodVerifCont.Value = "0";
            hdfIdVerif.Value = "0";
            lblMensaje.Text = "";
            hdfAccion.Value = "C";
            lConexion.Abrir();
            dlTipoDemanda.Items.Clear();
            ddlMotivoModifi.Items.Clear();
            ddlCesionario.Items.Clear();
            ddlCapComprador.Items.Clear();
            ddlCapVendedor.Items.Clear();
            ddlCapComprador.Enabled = true;
            ddlCapVendedor.Enabled = true;
            ddlModalidad.Items.Clear();
            ddlPuntoEntr.Items.Clear();
            ddlCapProducto.Items.Clear();
            ddlModalidad.Items.Clear();
            ddlPuntoEntr.Items.Clear();
            TxtCapFechaNeg.Text = "";
            TxtNumContrato.Text = "";
            TxtFechaSus.Text = "";
            TxtCantidad.Text = "";
            TxtPrecio.Text = "";
            TxtFechaInicial.Text = "";
            TxtFechaFinal.Text = "";
            ddlSentidoFlujo.SelectedValue = "";
            TxtPresion.Text = "";
            TrModifi.Visible = false;
            ddlCapTipoMercado.SelectedValue = "0";
            ddlModalidad.Items.Clear();
            ddlPuntoEntr.Items.Clear();
            ddlCapProducto.Items.Clear();
            /// LLeno los Controles
            LlenarControles(lConexion.gObjConexion, ddlCapProducto, "m_producto a, m_caracteristica_sub b", " a.estado = 'A' And b.estado = 'A' And a.codigo_producto = b.codigo_caracteristica and b.tipo_caracteristica = 'P' and b.codigo_tipo_subasta = 5 And b.tipo_mercado = 'S' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, dlTipoDemanda, "m_tipo_demanda_atender", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMotivoModifi, "m_motivos_modificacion", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlCesionario, "m_operador", " estado = 'A' And codigo_operador !=  " + goInfo.cod_comisionista + " order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlCapComprador, "m_operador", " estado = 'A'  order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlCapVendedor, "m_operador", " estado = 'A'  order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlModalidad, "m_modalidad_contractual", " estado = 'A'  order by descripcion", 0, 1);

            string[] lsNombreParametrosC = { "@P_cadena" };
            SqlDbType[] lTipoparametrosC = { SqlDbType.VarChar };
            string[] lValorParametrosC = { " Delete from t_contrato_datos_usuarios where codigo_cont_datos = 0 And login_usuario = '" + goInfo.Usuario.ToString() + "' " };
            /// Se obtiene los Datos de la Punta Compradora
            DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_EjecutarCadena", lsNombreParametrosC, lTipoparametrosC, lValorParametrosC);

            lConexion.Cerrar();
            ///// Ajustes Pantalla de acuerdo a control de cambios. 20150408
            //if (Session["TipoOperador"].ToString() == "C" || Session["TipoOperador"].ToString() == "U")
            //{
            //    ddlCapPunta.SelectedValue = "C";
            //    ddlCapPunta.Enabled = false;
            //}
            //else
            //{
            //    if (Session["TipoOperador"].ToString() == "T")
            //    {
            //        ddlCapPunta.SelectedValue = "V";
            //        ddlCapPunta.Enabled = false;
            //    }
            //}
            tblDatos.Visible = false;
            tblGrilla.Visible = false;
            tblRegCnt.Visible = true;
            btnActualizar.Visible = false;
            btnCrear.Visible = true;
        }
        catch (Exception ex)
        {

        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlCapPunta_SelectedIndexChanged(object sender, EventArgs e)
    {
        string[] lsProducto;
        lsProducto = ddlCapProducto.SelectedValue.Split('-');
        hdfDestinoRueda.Value = lsProducto[1].Trim();
        lConexion.Abrir();
        if (hdfDestinoRueda.Value == "G")
            LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_pozo", " estado = 'A'  order by descripcion", 0, 1);
        else
            LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_ruta_snt", " estado = 'A' order by descripcion", 0, 4);
        lConexion.Cerrar();
        if (ddlCapPunta.SelectedValue != "0")
        {
            hdfPunta.Value = ddlCapPunta.SelectedValue;
            if (ddlCapPunta.SelectedValue == "C")
            {
                ddlCapComprador.SelectedValue = Session["IdOperador"].ToString();
                ddlCapVendedor.SelectedValue = "0";
                ddlCapComprador.Enabled = false;
                ddlCapVendedor.Enabled = true;

                tblDemanda.Visible = true;
                btnActualUsu.Visible = false;
                btnCrearUsu.Visible = true;
                CargarDatosUsu();
            }
            else
            {
                ddlCapComprador.Enabled = true;
                ddlCapVendedor.Enabled = false;
                ddlCapComprador.SelectedValue = "0";
                ddlCapVendedor.SelectedValue = Session["IdOperador"].ToString();

                //20161108 control para no mostra usuarios finales por venta
                tblDemanda.Visible = false;
                btnActualUsu.Visible = false;
                btnCrearUsu.Visible = false;
            }

        }
        else
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Debe Seleccionar la Puntal de Contrato.!" + "');", true);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlCapTipoMercado_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (hdfDestinoRueda.Value == "G")
        {
            lblPuntoE.Text = "Punto de entrega de la energía al comprador";
            lblCantidad.Text = "Cantidad de energía contratada (MBTUD)";
            lblPrecio.Text = "Precio a la fecha de suscripción del contrato (USD/MBTU)";
            lblFechaInc.Text = "Fecha de inicio de la obligación de entrega";
            lblFechaFin.Text = "Fecha de terminación de la obligación de entrega";
            TrTrans.Visible = false;
        }
        else
        {
            lblPuntoE.Text = "Tramo o grupos de gasoductos";
            lblCantidad.Text = "Capacidad Contratada  (KPCD)";
            lblPrecio.Text = "Tarifa a la fecha de suscripción del contrato (USD/KPC)";
            lblFechaInc.Text = "Fecha de inicio de la prestación del servicio";
            lblFechaFin.Text = "Fecha de terminación prestación del servicio";
            if (ddlCapTipoMercado.SelectedValue == "P")
                TrTrans.Visible = true;
            else
                TrTrans.Visible = false;
        }
        //Fuente o campo MP 20160602
        if (ddlCapProducto.SelectedValue == "1-G" && ddlCapTipoMercado.SelectedValue == "P")
        {
            TrFuente.Visible = true;
            // Acitvo el campo de captura Equivalente Kpcd Req. 009-17 20170321
            TdIndica.Visible = true;
            TdIndica1.Visible = true;
        }
        else
        {
            TrFuente.Visible = false;
            ddlFuente.SelectedValue = "0";
            // Acitvo el campo de captura Equivalente Kpcd Req. 009-17 20170321
            TdIndica.Visible = false;
            TdIndica1.Visible = false;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCrearUsu_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_datos_usuarios", "@P_codigo_cont_datos", "@P_no_identificacion_usr", "@P_codigo_tipo_doc", 
                                        "@P_nombre_usuario", "@P_codigo_sector_consumo", "@P_codigo_punto_salida","@P_cantidad_contratada","@P_accion","@P_destino_rueda", "@P_tipo_demanda" ,
                                        "@P_codigo_mercado_relevante", // se agrega campo para mercado relevante-sector conuso 20160126
                                        "@P_equivalente_kpcd", // Campo nuevo Req. 009-17 Indicasdores 20170321
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, 
                                        SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int,
                                      SqlDbType.Int, // Campo nuevo Req. 009-17 Indicasdores 20170321
                                      };
        string[] lValorParametros = { "0", "0", "", "", "", "0", "0", " 0", "1", hdfDestinoRueda.Value, dlTipoDemanda.SelectedValue, ddlMercadoRel.SelectedValue,
                                    "0",  // Campo nuevo Req. 009-17 Indicasdores 20170321
                                    };

        lblMensaje.Text = "";
        string[] lsUsuario;
        string lsNombre = "";
        string lsTipoDoc = "";
        int liValor = 0;
        try
        {
            lConexion.Abrir();
            lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
            if (dlTipoDemanda.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar  Tipo Demanda Atender. <br> ";
            else
            {
                if (dlTipoDemanda.SelectedValue == "2")
                {
                    if (TxtUsuarioFinal.Text.Trim().Length <= 0)
                        lblMensaje.Text = "Debe Ingresar el Usuario Final <br>";
                    else
                    {
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                        if (!lLector.HasRows)
                            lblMensaje.Text += "El Usuario Ingresado NO existe en la Base de Datos.! <br>";
                        else
                        {
                            lLector.Read();
                            lsNombre = lLector["nombre"].ToString();
                            lsTipoDoc = lLector["codigo_tipo_doc"].ToString();
                        }
                        lLector.Close();
                        lLector.Dispose();
                    }
                }
                else //20160303
                {
                    if (ddlMercadoRel.SelectedValue == "0")
                        lblMensaje.Text += "Debe Seleccionar el mercado relevante. <br> ";
                }
            }
            if (dlTipoDemanda.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar  Tipo Demanda Atender. <br> ";
            if (ddlSector.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar " + lblSector.Text + ". <br> ";
            if (ddlPuntoSalida.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar  Punto de Salida en SNT. <br> ";
            if (TxtCantidadUsu.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar " + lblCantContra.Text + ". <br> ";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCantidadUsu.Text.Trim());
                    if (liValor < 1)
                        lblMensaje.Text += "Valor Invalido " + lblCantContra.Text + ". <br> ";
                    else
                    {
                        if (liValor > Convert.ToDecimal(TxtCantidad.Text.Trim()))
                            lblMensaje.Text += "La " + lblCantContra.Text + " No puede ser Mayor que " + lblCantidad.Text + ". <br> ";
                        else
                        {
                            if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(TxtCantidad.Text))
                                lblMensaje.Text += "La Acumulada de Usuarios No puede ser Mayor que " + lblCantidad.Text + ". <br> ";
                        }
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Invalido " + lblCantContra.Text + ". <br> ";
                }
            }
            /////////////////////////////////////////////////////////////////////////
            /// Habilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///
            /////////////////////////////////////////////////////////////////////////
            if (ddlCapProducto.SelectedValue == "1-G" && ddlCapTipoMercado.SelectedValue == "P")
            {
                if (TxtEquivaleKpcd.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Equivalente Kpcd. <br> ";
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtEquivaleKpcd.Text.Trim());
                        if (liValor < 0)
                            lblMensaje.Text += "Valor Invalido Equivalente Kpcd. <br> ";
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += "Valor Invalido " + lblCantContra.Text + ". <br> ";
                    }
                }
            }

            if (lblMensaje.Text == "")
            {
                if (hdfCodVerifCont.Value != "")
                    lValorParametros[1] = hdfCodVerifCont.Value;
                if (hdfTipoDemanda.Value == "2")
                {
                    lValorParametros[2] = lsUsuario[0].Trim();
                    lValorParametros[3] = lsTipoDoc;
                    lValorParametros[4] = lsNombre;
                }
                lValorParametros[5] = ddlSector.SelectedValue;
                lValorParametros[6] = ddlPuntoSalida.SelectedValue;
                lValorParametros[7] = TxtCantidadUsu.Text.Trim();
                /////////////////////////////////////////////////////////////////////////
                /// Habilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///
                /////////////////////////////////////////////////////////////////////////
                if (ddlCapProducto.SelectedValue == "1-G" && ddlCapTipoMercado.SelectedValue == "P")
                    lValorParametros[12] = TxtEquivaleKpcd.Text.Trim();

                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetUsuFinalCont", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presento un Problema en la Creación de la Información del Usuario.!" + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(TxtCantidadUsu.Text.Trim())).ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Ingresado Correctamente.!" + "');", true);
                    TxtUsuarioFinal.Text = "";
                    ddlSector.SelectedValue = "0";
                    ddlPuntoSalida.SelectedValue = "0";
                    ddlMercadoRel.SelectedValue = "0";
                    TxtCantidadUsu.Text = "0";
                    TxtEquivaleKpcd.Text = "0"; // Campos nuevos Req. 009-17 Indicadores 20170321
                    CargarDatosUsu();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnActualUsu_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_datos_usuarios", "@P_codigo_cont_datos", "@P_no_identificacion_usr", "@P_codigo_tipo_doc", 
                                        "@P_nombre_usuario", "@P_codigo_sector_consumo", "@P_codigo_punto_salida","@P_cantidad_contratada","@P_accion","@P_destino_rueda", "@P_tipo_demanda" ,
                                        "@P_codigo_mercado_relevante", // se agrega campo para mercado relevante-sector conuso 20160126
                                        "@P_equivalente_kpcd" // Campo nuevo Req. 009-17 Indicasdores 20170321
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, 
                                        SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int,
                                      SqlDbType.Int // Campo nuevo Req. 009-17 Indicasdores 20170321
                                      };
        string[] lValorParametros = { hdfCodDatUsu.Value, "0", "", "", "", "0", "0", " 0", "2", hdfDestinoRueda.Value, dlTipoDemanda.SelectedValue, ddlMercadoRel.SelectedValue,
                                    "0"  // Campo nuevo Req. 009-17 Indicasdores 20170321
                                    };
        lblMensaje.Text = "";
        string[] lsUsuario;
        string lsNombre = "";
        string lsTipoDoc = "";
        int liValor = 0;
        try
        {
            lConexion.Abrir();
            lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
            if (dlTipoDemanda.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar  Tipo Demanda Atender. <br> ";
            else
            {
                if (dlTipoDemanda.SelectedValue == "2")
                {
                    if (TxtUsuarioFinal.Text.Trim().Length <= 0)
                        lblMensaje.Text = "Debe Ingresar el Usuario Final <br>";
                    else
                    {
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                        if (!lLector.HasRows)
                            lblMensaje.Text += "El Usuario Ingresado NO existe en la Base de Datos.! <br>";
                        else
                        {
                            lLector.Read();
                            lsNombre = lLector["nombre"].ToString();
                            lsTipoDoc = lLector["codigo_tipo_doc"].ToString();
                        }
                        lLector.Close();
                        lLector.Dispose();
                    }
                }
                else //20160303
                {
                    if (ddlMercadoRel.SelectedValue == "0")
                        lblMensaje.Text += "Debe Seleccionar el mercado relevante. <br> ";
                }
            }
            if (ddlSector.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar " + lblSector.Text + ". <br> ";
            if (ddlPuntoSalida.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar  Punto de Salida en SNT. <br> ";
            if (TxtCantidadUsu.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar " + lblCantContra.Text + ". <br> ";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCantidadUsu.Text.Trim());
                    if (liValor < 1)
                        lblMensaje.Text += "Valor Invalido " + lblCantContra.Text + ". <br> ";
                    else
                    {
                        if (liValor > Convert.ToDecimal(TxtCantidad.Text.Trim()))
                            lblMensaje.Text += "La " + lblCantContra.Text + " No puede ser Mayor que " + lblCantidad.Text + ". <br> ";
                        else
                        {
                            if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(TxtCantidad.Text))
                                lblMensaje.Text += "La Acumulada de Usuarios No puede ser Mayor que " + lblCantidad.Text + ". <br> ";
                        }
                    }

                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Invalido " + lblCantContra.Text + ". <br> ";
                }
            }
            /////////////////////////////////////////////////////////////////////////
            /// Habilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///
            /////////////////////////////////////////////////////////////////////////
            if (ddlCapProducto.SelectedValue == "1-G" && ddlCapTipoMercado.SelectedValue == "P")
            {
                if (TxtEquivaleKpcd.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Equivalente Kpcd. <br> ";
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtEquivaleKpcd.Text.Trim());
                        if (liValor < 0)
                            lblMensaje.Text += "Valor Invalido Equivalente Kpcd. <br> ";
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += "Valor Invalido " + lblCantContra.Text + ". <br> ";
                    }
                }
            }

            if (lblMensaje.Text == "")
            {
                if (hdfCodVerifCont.Value.Trim().Length > 0)
                    lValorParametros[1] = hdfCodVerifCont.Value;
                if (hdfTipoDemanda.Value == "2")
                {
                    lValorParametros[2] = lsUsuario[0].Trim();
                    lValorParametros[3] = lsTipoDoc;
                    lValorParametros[4] = lsNombre;
                }
                lValorParametros[5] = ddlSector.SelectedValue;
                lValorParametros[6] = ddlPuntoSalida.SelectedValue;
                lValorParametros[7] = TxtCantidadUsu.Text.Trim();
                /////////////////////////////////////////////////////////////////////////
                /// Habilita Captura del Nuevo Campo Req. 009-17 Indicadores 20170321 ///
                /////////////////////////////////////////////////////////////////////////
                if (ddlCapProducto.SelectedValue == "1-G" && ddlCapTipoMercado.SelectedValue == "P")
                    lValorParametros[12] = TxtEquivaleKpcd.Text.Trim();

                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetUsuFinalCont", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presento un Problema en la Actualizacion de la Información del Usuario.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Ingresado Correctamente.!" + "');", true);
                    TxtUsuarioFinal.Text = "";
                    ddlSector.SelectedValue = "0";
                    ddlPuntoSalida.SelectedValue = "0";
                    ddlMercadoRel.SelectedValue = "0";
                    TxtCantidadUsu.Text = "0";
                    TxtEquivaleKpcd.Text = "0"; // Campos nuevos Req. 009-17 Indicadores 20170321
                    btnCrearUsu.Visible = true;
                    btnActualUsu.Visible = false;
                    CargarDatosUsu();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dlTipoDemanda_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            hdfTipoDemanda.Value = dlTipoDemanda.SelectedValue;
            if (hdfTipoDemanda.Value == "1")
            {
                TxtUsuarioFinal.Enabled = false;
                ddlMercadoRel.Enabled = true; //20160303
                lblSector.Text = "Sector de Consumo Usuario Regulado";
                if (hdfDestinoRueda.Value == "G")
                    lblCantContra.Text = "Cantidad a entregar";
                else
                    lblCantContra.Text = "Capacidad Contratada";
            }
            else
            {
                TxtUsuarioFinal.Enabled = true;
                ddlMercadoRel.Enabled = false; //20160303
                ddlMercadoRel.SelectedValue = "0"; //20160303
                if (hdfDestinoRueda.Value == "G")
                {
                    lblCantContra.Text = "Cantidad Contratada por usuario (MBTUD)";
                    lblSector.Text = "Sector de Consumo Usuario No Regulado";
                }
                else
                {
                    lblSector.Text = "Sector de consumo Usuario No Regulado";
                    lblCantContra.Text = "Capacidad Contratada por usuario (KPCD)";
                }
            }
            lConexion.Abrir();
            ddlSector.Items.Clear();
            ddlPuntoSalida.Items.Clear();
            LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo sec, m_demanda_sector dem", " sec.estado ='A' and sec.codigo_sector_consumo = dem.codigo_sector_consumo  and dem.estado ='A' and dem.codigo_tipo_demanda =" + dlTipoDemanda.SelectedValue + " and dem.ind_uso ='T' order by descripcion", 0, 1); // 20160706
            LlenarControles(lConexion.gObjConexion, ddlPuntoSalida, "m_punto_salida_snt", " estado = 'A' order by descripcion", 0, 2);
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {

        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlPuntoEntr_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (hdfPunta.Value == "C")
        {
            lConexion.Abrir();
            //ddlMercadoRelev.Items.Clear();
            ddlPuntoSalida.Items.Clear();
            //if (hdfTipoDemanda.Value == "1")
            //    LlenarControles(lConexion.gObjConexion, ddlMercadoRelev, "m_mercado_relevante", " estado = 'A' And destino_rueda = '" + hdfDestinoRueda.Value + "' order by descripcion", 0, 2);
            //else
            //    LlenarControles(lConexion.gObjConexion, ddlMercadoRelev, "m_mercado_relevante", " estado = 'A' And destino_rueda = '" + hdfDestinoRueda.Value + "' And codigo_mercado_relevante not in (3,6) order by descripcion", 0, 2);
            //if (hdfDestinoRueda.Value == "G")
            //    LlenarControles(lConexion.gObjConexion, ddlPuntoSalida, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            //else
            LlenarControles(lConexion.gObjConexion, ddlPuntoSalida, "m_punto_salida_snt", " estado = 'A'  order by descripcion", 0, 2);
            lConexion.Cerrar();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnVincular_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_verif_cont_uno", "@P_codigo_verif_cont_dos" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { "0", "0" };
        int liReg = 0;
        int liEnc = 0;
        string lsPunta1 = "";
        string lsPunta2 = "";
        string lsComp1 = "";
        string lsComp2 = "";
        string lsVend1 = "";
        string lsVend2 = "";
        string lsNoid1 = "0";
        string lsNoid2 = "0";
        lblMensaje.Text = "";
        try
        {
            foreach (DataGridItem Grilla in this.dtgConsulta.Items)
            {
                CheckBox Checkbox = null;
                Checkbox = (CheckBox)Grilla.Cells[30].Controls[1]; // 20160603 fuente o campo
                if (Checkbox.Checked == true)
                {
                    liReg++;
                    liEnc++;
                    if (liEnc == 1)
                    {
                        lsNoid1 = Grilla.Cells[9].Text;
                        lsComp1 = Grilla.Cells[10].Text;
                        lsVend1 = Grilla.Cells[11].Text;
                    }
                    if (liEnc == 2)
                    {
                        lsNoid2 = Grilla.Cells[9].Text;
                        lsComp2 = Grilla.Cells[10].Text;
                        lsVend2 = Grilla.Cells[11].Text;
                    }
                }
            }
            if (liReg == 0)
                lblMensaje.Text += "Debe Seleccinar Registros para Ejecutar el Proceso. <br>";
            else
            {
                if (liReg != 2)
                    lblMensaje.Text += "Se deben Seleccionar Dos Registros para poder Vincular. <br>";
                else
                {
                    //if (lsPunta1 == lsPunta2)
                    //    lblMensaje.Text += "Las puntas de los Registros Seleccionados no puden ser Iguales.<br>";
                    if (lsComp1 != lsComp2)
                        lblMensaje.Text += "La Punta Compradora del Primer Registro no Coincide con la Compradora del Segundo Registro.<br>";
                    if (lsVend1 != lsVend2)
                        lblMensaje.Text += "La Punta Vendedora del Primer Registro no Coincide con la Vededora del Segundo Registro.<br>";
                }
            }
            if (lblMensaje.Text == "")
            {
                lConexion.Abrir();
                SqlTransaction oTransaccion;
                oTransaccion = lConexion.gObjConexion.BeginTransaction(System.Data.IsolationLevel.Serializable);

                lValorParametros[0] = lsNoid1;
                lValorParametros[1] = lsNoid2;
                if (!DelegadaBase.Servicios.EjecutarProcedimientoConTransaccion(lConexion.gObjConexion, "pa_SetVinculaContrato", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo))
                {
                    lblMensaje.Text = "Se presento un Problema en la Vinculación de la Información.! " + goInfo.mensaje_error.ToString();
                    oTransaccion.Rollback();
                    lConexion.Cerrar();

                }
                else
                {
                    oTransaccion.Commit();
                    lConexion.Cerrar();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Vinculado Correctamente.!" + "');", true);
                    CargarDatos();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Se presentó un problema en la vinculación de la Información. " + ex.Message.ToString(); //20161207 rq102 conoformacion de rutas
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnLimpiar_Click(object sender, EventArgs e)
    {
        TxtFechaIni.Text = "";
        TxtFechaFin.Text = "";  //20161213 rq102 conformacion de rutas
        ddlMercado.SelectedValue = "";
        TxtNoContrato.Text = "";
        ddlProducto.SelectedValue = "0";
        ddlComprador.SelectedValue = "0";
        ddlVendedor.SelectedValue = "0";
        ddlEstado.SelectedValue = "0";
        TxtIdRegini.Text = "";
        TxtIdRegFin.Text = "";
        TxtIdsReg.Text = "";
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlCapProducto_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlCapPunta_SelectedIndexChanged(null, null);
        //Fuente o campo MP 20160602
        if (ddlCapProducto.SelectedValue == "1-G" && ddlCapTipoMercado.SelectedValue == "P")
        {
            TrFuente.Visible = true;
            // Acitvo el campo de captura Equivalente Kpcd Req. 009-17 20170321
            TdIndica.Visible = true;
            TdIndica1.Visible = true;
        }
        else
        {
            TrFuente.Visible = false;
            ddlFuente.SelectedValue = "0";
            // Acitvo el campo de captura Equivalente Kpcd Req. 009-17 20170321
            TdIndica.Visible = false;
            TdIndica1.Visible = false;
        }
        // Codigo nuevo Pruebas Modificacion Inf Operativa y Transaccional 20160906
        if (ddlCapProducto.SelectedValue == "2-T")
        {
            TrTra1.Visible = true;
            TrTra2.Visible = true;
            if (ddlCapTipoMercado.SelectedValue == "P")
                TrTrans.Visible = true;
            else
                TrTrans.Visible = false;
            ddlPuntoEntr.Items.Clear();
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_ruta_snt", " estado = 'A' order by descripcion", 0, 4);
            lConexion.Cerrar();
            lblPuntoE.Text = "Tramo o grupos de gasoductos";
            lblCantidad.Text = "Capacidad Contratada (KPCD)";
            lblPrecio.Text = "Tarifa a la fecha de suscripción del contrato (USD/KPC)";
            lblFechaInc.Text = "Fecha de inicio de la prestación del servicio";
            lblFechaFin.Text = "Fecha de terminación prestación del servicio";
            hdfDestinoRueda.Value = "T"; // Campo nuevo Req. 009-17 Indicadores 20170321
        }
        else
        {
            TrTra1.Visible = false;
            TrTra2.Visible = false;
            TrTrans.Visible = false;
            ddlPuntoEntr.Items.Clear();
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlPuntoEntr, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            lConexion.Cerrar();
            lblPuntoE.Text = "Punto de entrega de la energía al comprador";
            lblCantidad.Text = "Cantidad de energía contratada (MBTUD)";
            lblPrecio.Text = "Precio a la fecha de suscripción del contrato (USD/MBTU)";
            lblFechaInc.Text = "Fecha de inicio de la obligación de entrega";
            lblFechaFin.Text = "Fecha de terminación de la obligación de entrega";
            hdfDestinoRueda.Value = "G"; // Campo nuevo Req. 009-17 Indicadores 20170321
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCrear_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_ind_contrato", "@P_fecha_negociacion", "@P_contrato_definitivo", "@P_punta_contrato", 
                                        "@P_fecha_suscripcion_cont", "@P_nombre_operador", "@P_codigo_tipo_doc", "@P_no_identificacion_operador", "@P_codigo_modalidad",
                                        "@P_desc_otro_moda", "@P_codigo_punto_entrega", "@P_desc_otro_punto", "@P_cantidad", "@P_precio", "@P_fecha_inicial", 
                                        "@P_fecha_final", "@P_ruta_contrato", "@P_codigo_tipo_demanda", "@P_sentido_flujo", "@P_presion_punto_fin", "@P_codigo_motivo_modificacion", 
                                        "@P_codigo_cesionario","@P_estado_act","@P_codigo_tipo_subasta","@P_destino_rueda","@P_codigo_producto","@P_tipo_mercado",
                                        "@P_nombre_contraparte","@P_tipo_doc_contraparte","@P_no_ident_contraparte","@P_operador_compra","@P_operador_venta","@P_accion",
                                        "@P_codigo_fuente"}; // Fuente o campo MP 20160602
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, 
                                        SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int, SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, // Cambio del tipo de datos de la presion del punto 20151006
                                        SqlDbType.Int,SqlDbType.VarChar,SqlDbType.Int,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,
                                        SqlDbType.Int};// Fuente o Campo MP 20160602
        string[] lValorParametros = { "0", "E", "", "0", "", "", "", "", "", "0", "", "0", "", "0", "0", "", "", "", "0", "", "0", "0", "0", "V", "0", "0", "0", "", "", "", "", "0", "0", "1", "0" }; //Fuente o Camop MP 20160602
        lblMensaje.Text = "";
        string lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "-" + DateTime.Now.ToShortDateString().Substring(3, 2) + "-" + DateTime.Now.ToShortDateString().Substring(0, 2);
        string sRuta = sRutaArc + lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\";
        string[] lsOperaC;
        string[] lsOperaV;
        string[] lsProducto;
        string lsNoid = "0";
        string lsMensajeMail = ""; // Campo nuevo requerimiento algoritmo y verificacion 20150529
        string lsEnviaMail = ""; // Campo nuevo requerimiento algoritmo y verificacion 20150529

        try
        {
            //hdfDestinoRueda.Value = "";
            lblMensaje.Text = validaciones();
            lsOperaC = ddlCapComprador.SelectedValue.Split('-');
            lsOperaV = ddlCapVendedor.SelectedValue.Split('-');
            lsProducto = ddlCapProducto.SelectedValue.Split('-');
            if (FuCopiaCont.FileName != "")
            {

                try
                {
                    System.IO.Directory.CreateDirectory(sRuta);
                    FuCopiaCont.SaveAs(sRuta + FuCopiaCont.FileName);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Problemas en la Carga del Archivo. " + ex.Message.ToString();
                    lConexion.Cerrar();
                }
            }
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = hdfIdVerif.Value;
                lValorParametros[2] = TxtCapFechaNeg.Text.Trim();
                lValorParametros[3] = TxtNumContrato.Text.Trim().ToUpper();
                lValorParametros[4] = hdfPunta.Value;
                lValorParametros[5] = TxtFechaSus.Text.Trim();
                if (hdfPunta.Value == "C")
                {
                    lValorParametros[6] = lsOperaC[3].Trim();
                    lValorParametros[7] = lsOperaC[1].Trim();
                    lValorParametros[8] = lsOperaC[2].Trim();
                    lValorParametros[31] = lsOperaC[0].Trim();

                    lValorParametros[28] = lsOperaV[3].Trim();
                    lValorParametros[29] = lsOperaV[1].Trim();
                    lValorParametros[30] = lsOperaV[2].Trim();
                    lValorParametros[32] = lsOperaV[0].Trim();
                }
                else
                {
                    lValorParametros[6] = lsOperaV[3].Trim();
                    lValorParametros[7] = lsOperaV[1].Trim();
                    lValorParametros[8] = lsOperaV[2].Trim();

                    lValorParametros[28] = lsOperaC[3].Trim();
                    lValorParametros[29] = lsOperaC[1].Trim();
                    lValorParametros[30] = lsOperaC[2].Trim();

                    lValorParametros[31] = lsOperaC[0].Trim();
                    lValorParametros[32] = lsOperaV[0].Trim();
                }
                lValorParametros[9] = ddlModalidad.SelectedValue;
                lValorParametros[10] = TxtOtrModalidad.Text.Trim().ToUpper();
                lValorParametros[11] = ddlPuntoEntr.SelectedValue;
                lValorParametros[12] = TxtOtrPuntoE.Text.Trim().ToUpper();
                lValorParametros[13] = TxtCantidad.Text.Trim();
                lValorParametros[14] = TxtPrecio.Text.Trim();
                lValorParametros[15] = TxtFechaInicial.Text.Trim();
                lValorParametros[16] = TxtFechaFinal.Text.Trim();
                if (FuCopiaCont.FileName.Trim() != "")
                    lValorParametros[17] = lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\" + FuCopiaCont.FileName;
                else
                    lValorParametros[17] = "";
                lValorParametros[18] = "0";
                if (hdfDestinoRueda.Value == "T" && hdfTipoMerc.Value == "P")
                {
                    lValorParametros[19] = ddlSentidoFlujo.SelectedValue;
                    lValorParametros[20] = TxtPresion.Text.Trim();
                }
                lValorParametros[21] = "0"; // Motivo de Modificacion
                lValorParametros[22] = "0"; // Codigo Cesionario
                lValorParametros[23] = "I";
                lValorParametros[24] = "0";
                lValorParametros[25] = hdfDestinoRueda.Value;
                lValorParametros[26] = lsProducto[0].Trim();
                lValorParametros[27] = ddlCapTipoMercado.SelectedValue;
                lValorParametros[34] = ddlFuente.SelectedValue; // Fuente o Campo MP 20160602
                hdfTipoDemanda.Value = dlTipoDemanda.SelectedValue;
                SqlTransaction oTransaccion;
                lConexion.Abrir();
                oTransaccion = lConexion.gObjConexion.BeginTransaction(System.Data.IsolationLevel.Serializable);
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetRegistroContratoExt", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensaje.Text = "Se presento un Problema en la Creacion del Registro del Contrato.! " + goInfo.mensaje_error.ToString();
                    oTransaccion.Rollback();
                    lConexion.Cerrar();
                }
                else
                {
                    lblMensaje.Text = "";
                    lLector.Read();
                    hdfCodVerifCont.Value = lLector["codigo_cont_dato"].ToString();
                    lblMensaje.Text = lLector["error"].ToString();
                    lsNoid = lLector["Id"].ToString();
                    lsMensajeMail = lLector["diferencia"].ToString(); // Campo nuevo requerimiento algoritmo y verificacion 20150529
                    lsEnviaMail = lLector["enviaMail"].ToString(); // Campo nuevo requerimiento algoritmo y verificacion 20150529

                    lLector.Close();
                    lLector.Dispose();
                    oTransaccion.Commit();
                    lConexion.Cerrar();
                    if (lblMensaje.Text == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Ingresado Correctamente.!" + "');", true);

                        lConexion.Abrir();
                        //// Envio la Alerta a la Punta Contraria cuando se crea el registro
                        string lsAsunto = "";
                        string lsMensaje = "";
                        string lsMensajeC = "";
                        string lsMailC = "";
                        string lsNomOperadorC = "";
                        string lsMailV = "";
                        string lsNomOperadorV = "";
                        /// Obtengo el mail del Operador Compra
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + lsOperaC[0].Trim() + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lsMailC = lLector["e_mail"].ToString();
                            if (lLector["tipo_persona"].ToString() == "J")
                                lsNomOperadorC = lLector["razon_social"].ToString();
                            else
                                lsNomOperadorC = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                        }
                        lLector.Close();
                        lLector.Dispose();
                        /// Obtengo el mail del Operador Venta
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + lsOperaV[0].Trim() + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lsMailV = lLector["e_mail"].ToString();
                            if (lLector["tipo_persona"].ToString() == "J")
                                lsNomOperadorV = lLector["razon_social"].ToString();
                            else
                                lsNomOperadorV = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        lsAsunto = "Notificación Ingreso Información Contrato";
                        if (hdfPunta.Value == "C")
                            lsMensaje = "Nos permitimos  informarle que el Operador " + lsNomOperadorC + " acaba de Ingresarsatisfactoriamente la información del Contrato Externo No. " + TxtNumContrato.Text.Trim() + ", con No. de Registro: " + lsNoid + ".<br><br>";
                        else
                            lsMensaje = "Nos permitimos  informarle que el Operador " + lsNomOperadorV + " acaba de Ingresar la Información Transaccional del Registro del Contrato Externo No. " + TxtNumContrato.Text.Trim() + ", con No. de Registro: " + lsNoid + ".<br><br>";
                        lsMensaje += "Por favor Ingresar al sistema SE_GAS de Registro y Verificacion y Buscar el No. del Id y Complementar la Información. <br><br>";
                        lsMensaje += "\n\nObservaciones: Por favor proceda a ingresar la información transaccional e informar a su contraparte para culminar el proceso de registro. <br><br>";
                        lsMensaje += "Cordialmente, <br><br><br>";
                        lsMensaje += "Administrador SEGAS <br>";
                        lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                        clEmail mailC = new clEmail(lsMailC, lsAsunto, lsMensaje, "");
                        lblMensaje.Text = mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                        ////
                        lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                        clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensaje, "");
                        lblMensaje.Text = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                        if (lsEnviaMail == "S")
                        {
                            ///// Envio del Mail de la Solicitud de la Correcion o Registro del Contrato
                            if (lsMensajeMail != "")
                            {
                                lsAsunto = "Notificación Solicitud Corrección Registro y Verificación";
                                lsMensaje = "Nos permitimos informarle que el Administrador de SEGAS acaba de Solicitar la corrección de la información transaccional del Contrato Externo No. " + TxtNumContrato.Text.Trim() + ", con No. de Registro: " + lsNoid + ". Por favor revisar los siguientes campos: <br><br> " + lsMensajeMail + "<br><br>";
                                lsMensaje += "\n\nObservaciones: Por favor validar los campos objeto de corrección con su contraparte. <br><br>";
                            }
                            else
                            {
                                lsAsunto = "Notificación Registro Contrato";
                                lsMensaje = "Nos permitimos informarle que el Administrador de la plataforma SEGAS acaba de realizar exitosamente el Registro del Contrato Externo No. " + TxtNumContrato.Text.Trim() + ", con No. de Registro: " + lsNoid + ".<br><br>";
                            }
                            lsMensaje += "Cordialmente, <br><br><br>";
                            lsMensaje += "Administrador SEGAS <br>";
                            lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                            clEmail mailC1 = new clEmail(lsMailC, lsAsunto, lsMensaje, "");
                            lblMensaje.Text = mailC1.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                            ////
                            lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                            clEmail mailV1 = new clEmail(lsMailV, lsAsunto, lsMensaje, "");
                            lblMensaje.Text = mailV1.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                        }
                        lConexion.Cerrar();
                        tblRegCnt.Visible = false;
                        tblGrilla.Visible = true;
                        tblDatos.Visible = true;
                        tblDemanda.Visible = false;
                        LimpiarCampos();
                        CargarDatos();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnActualizar_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_ind_contrato", "@P_fecha_negociacion", "@P_contrato_definitivo", "@P_punta_contrato", 
                                        "@P_fecha_suscripcion_cont", "@P_nombre_operador", "@P_codigo_tipo_doc", "@P_no_identificacion_operador", "@P_codigo_modalidad",
                                        "@P_desc_otro_moda", "@P_codigo_punto_entrega", "@P_desc_otro_punto", "@P_cantidad", "@P_precio", "@P_fecha_inicial", 
                                        "@P_fecha_final", "@P_ruta_contrato", "@P_codigo_tipo_demanda", "@P_sentido_flujo", "@P_presion_punto_fin", "@P_codigo_motivo_modificacion", 
                                        "@P_codigo_cesionario","@P_estado_act","@P_codigo_tipo_subasta","@P_destino_rueda","@P_codigo_producto","@P_tipo_mercado",
                                        "@P_nombre_contraparte","@P_tipo_doc_contraparte","@P_no_ident_contraparte","@P_operador_compra","@P_operador_venta","@P_accion",
                                        "@P_codigo_fuente"}; //Fuente o campo MP 20160602
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, 
                                        SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int, SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, // Cambio del tipo de datos de la presion del punto 20151006
                                        SqlDbType.Int,SqlDbType.VarChar,SqlDbType.Int,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,
                                        SqlDbType.Int}; //Fuente o campo MP 20160602
        string[] lValorParametros = { "0", "E", "", "0", "", "", "", "", "", "0", "", "0", "", "0", "0", "", "", "", "0", "", "0", "0", "0", "V", "0", "0", "0", "", "", "", "", "0", "0", "2", "0" }; //Fuente o campo MP 20160602
        lblMensaje.Text = "";
        string lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "-" + DateTime.Now.ToShortDateString().Substring(3, 2) + "-" + DateTime.Now.ToShortDateString().Substring(0, 2);
        string sRuta = sRutaArc + lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\";
        string[] lsOperaC;
        string[] lsOperaV;
        string[] lsProducto;
        string lsNoid = "0";
        string lsMensajeMail = ""; // Campo nuevo requerimiento algoritmo y verificacion 20150529
        string lsEnviaMail = ""; // Campo nuevo requerimiento algoritmo y verificacion 20150529

        try
        {
            lblMensaje.Text = validaciones();
            lsOperaC = ddlCapComprador.SelectedValue.Split('-');
            lsOperaV = ddlCapVendedor.SelectedValue.Split('-');
            lsProducto = ddlCapProducto.SelectedValue.Split('-');
            if (FuCopiaCont.FileName != "")
            {

                try
                {
                    System.IO.Directory.CreateDirectory(sRuta);
                    FuCopiaCont.SaveAs(sRuta + FuCopiaCont.FileName);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Problemas en la Carga del Archivo. " + ex.Message.ToString();
                    lConexion.Cerrar();
                }
            }
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = hdfIdVerif.Value;
                lValorParametros[2] = TxtCapFechaNeg.Text.Trim();
                lValorParametros[3] = TxtNumContrato.Text.Trim().ToUpper();
                lValorParametros[4] = hdfPunta.Value;
                lValorParametros[5] = TxtFechaSus.Text.Trim();
                if (hdfPunta.Value == "C")
                {
                    lValorParametros[6] = lsOperaC[3].Trim();
                    lValorParametros[7] = lsOperaC[1].Trim();
                    lValorParametros[8] = lsOperaC[2].Trim();
                    lValorParametros[31] = lsOperaC[0].Trim();

                    lValorParametros[28] = lsOperaV[3].Trim();
                    lValorParametros[29] = lsOperaV[1].Trim();
                    lValorParametros[30] = lsOperaV[2].Trim();
                    lValorParametros[32] = lsOperaV[0].Trim();
                }
                else
                {
                    lValorParametros[6] = lsOperaV[3].Trim();
                    lValorParametros[7] = lsOperaV[1].Trim();
                    lValorParametros[8] = lsOperaV[2].Trim();

                    lValorParametros[28] = lsOperaC[3].Trim();
                    lValorParametros[29] = lsOperaC[1].Trim();
                    lValorParametros[30] = lsOperaC[2].Trim();

                    lValorParametros[31] = lsOperaC[0].Trim();
                    lValorParametros[32] = lsOperaV[0].Trim();
                }
                lValorParametros[9] = ddlModalidad.SelectedValue;
                lValorParametros[10] = TxtOtrModalidad.Text.Trim().ToUpper();
                lValorParametros[11] = ddlPuntoEntr.SelectedValue;
                lValorParametros[12] = TxtOtrPuntoE.Text.Trim().ToUpper();
                lValorParametros[13] = TxtCantidad.Text.Trim();
                lValorParametros[14] = TxtPrecio.Text.Trim();
                lValorParametros[15] = TxtFechaInicial.Text.Trim();
                lValorParametros[16] = TxtFechaFinal.Text.Trim();
                if (FuCopiaCont.FileName != "")
                    lValorParametros[17] = lsFecha + "\\" + "operador_" + goInfo.cod_comisionista + "\\" + FuCopiaCont.FileName;
                else
                    lValorParametros[17] = hdfRutaArchivo.Value;
                lValorParametros[18] = "0";
                if (hdfDestinoRueda.Value == "T" && hdfTipoMerc.Value == "P")
                {
                    lValorParametros[19] = ddlSentidoFlujo.SelectedValue;
                    lValorParametros[20] = TxtPresion.Text.Trim();
                }
                lValorParametros[21] = "0"; // Motivo de Modificacion
                lValorParametros[22] = "0"; // Codigo Cesionario
                lValorParametros[23] = hdfEstadoAct.Value;
                lValorParametros[24] = "0";
                lValorParametros[25] = lsProducto[1].Trim();
                lValorParametros[26] = lsProducto[0].Trim();
                lValorParametros[27] = ddlCapTipoMercado.SelectedValue;
                lValorParametros[34] = ddlFuente.SelectedValue; //Fuente o campo MP 20160602
                hdfTipoDemanda.Value = dlTipoDemanda.SelectedValue;
                hdfDestinoRueda.Value = "";
                SqlTransaction oTransaccion;
                lConexion.Abrir();
                oTransaccion = lConexion.gObjConexion.BeginTransaction(System.Data.IsolationLevel.Serializable);
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetRegistroContratoExt", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensaje.Text = "Se presento un Problema en la Creacion del Registro del Contrato.! " + goInfo.mensaje_error.ToString();
                    oTransaccion.Rollback();
                    lConexion.Cerrar();
                }
                else
                {
                    lLector.Read();
                    hdfCodVerifCont.Value = lLector["codigo_cont_dato"].ToString();
                    lsNoid = lLector["Id"].ToString();
                    lsMensajeMail = lLector["diferencia"].ToString(); // Campo nuevo requerimiento algoritmo y verificacion 20150529
                    lsEnviaMail = lLector["enviaMail"].ToString(); // Campo nuevo requerimiento algoritmo y verificacion 20150529
                    lLector.Close();
                    lLector.Dispose();
                    oTransaccion.Commit();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Ingresado Correctamente.!" + "');", true);

                    ///// Envio de la alerta al ingresar la informacion por ambas puntas 20150529
                    ///// Requerimiento Algoritmo y Verificacion
                    string lsAsunto = "";
                    string lsMensaje = "";
                    string lsMensajeC = "";
                    string lsMailC = "";
                    string lsNomOperadorC = "";
                    string lsMailV = "";
                    string lsNomOperadorV = "";
                    /// Obtengo el mail del Operador Compra
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraC.Value + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsMailC = lLector["e_mail"].ToString();
                        if (lLector["tipo_persona"].ToString() == "J")
                            lsNomOperadorC = lLector["razon_social"].ToString();
                        else
                            lsNomOperadorC = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                    }
                    lLector.Close();
                    lLector.Dispose();
                    /// Obtengo el mail del Operador Venta
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfOPeraV.Value + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsMailV = lLector["e_mail"].ToString();
                        if (lLector["tipo_persona"].ToString() == "J")
                            lsNomOperadorV = lLector["razon_social"].ToString();
                        else
                            lsNomOperadorV = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Envio de la alerta del ingreso de la información
                    lsAsunto = "Notificación Actualización Información Contrato";
                    if (hdfPunta.Value == "C")
                        lsMensaje = "Nos permitimos  informarle que el operador " + lsNomOperadorC + " acaba de Actualizar la información transaccional del Registro del Contrato Externo No. " + TxtNumContrato.Text.Trim() + ", con No. de Registro: " + lsNoid + ".<br><br>";
                    else
                        lsMensaje = "Nos permitimos  informarle que el operador " + lsNomOperadorV + " acaba de Actualizar la información transaccional del Registro del Contrato Externo No. " + TxtNumContrato.Text.Trim() + ", con No. de Registro: " + lsNoid + ".<br><br>";
                    lsMensaje += "\n\nObservaciones: Verificación Automática. <br><br>";
                    lsMensaje += "Cordialmente, <br><br><br>";
                    lsMensaje += "Administrador SEGAS <br>";
                    lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                    clEmail mailC = new clEmail(lsMailC, lsAsunto, lsMensaje, "");
                    lblMensaje.Text = mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                    ////
                    lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                    clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensaje, "");
                    lblMensaje.Text = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                    if (lsEnviaMail == "S")
                    {
                        ///// Envio del Mail de la Solicitud de la Correcion o Registro del Contrato
                        if (lsMensajeMail != "")
                        {
                            lsAsunto = "Notificación Solicitud Corrección Registro y Verificación";
                            lsMensaje = "Nos permitimos informarle que el Administrador de SEGAS acaba de Solicitar la corrección de la información transaccional del Contrato Externo No. " + TxtNumContrato.Text.Trim() + ". Por favor revisar los siguientes campos: <br><br> " + lsMensajeMail + "<br><br>";
                            lsMensaje += "\n\nObservaciones: Por favor validar los campos objeto de corrección con su contraparte. <br><br>";
                        }
                        else
                        {
                            lsAsunto = "Notificación Registro Contrato";
                            lsMensaje = "Nos permitimos informarle que el Administrador de la plataforma SEGAS acaba de realizar exitosamente el Registro del Contrato Externo No. " + TxtNumContrato.Text.Trim() + ", con No. de Registro: " + lsNoid + ".<br><br>";
                        }
                        lsMensaje += "Cordialmente, <br><br><br>";
                        lsMensaje += "Administrador SEGAS <br>";
                        lsMensajeC = "Señores: " + lsNomOperadorC + " <br><br>" + lsMensaje;
                        clEmail mailC1 = new clEmail(lsMailC, lsAsunto, lsMensaje, "");
                        lblMensaje.Text = mailC1.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                        ////
                        lsMensajeC = "Señores: " + lsNomOperadorV + " <br><br>" + lsMensaje;
                        clEmail mailV1 = new clEmail(lsMailV, lsAsunto, lsMensaje, "");
                        lblMensaje.Text = mailV1.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                    }
                    tblRegCnt.Visible = false;
                    tblGrilla.Visible = true;
                    tblDatos.Visible = true;
                    tblDemanda.Visible = false;
                    LimpiarCampos();
                    CargarDatos();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRegresar_Click(object sender, EventArgs e)
    {
        tblGrilla.Visible = true;
        tblRegCnt.Visible = false;
        tblDatos.Visible = true;
        tblDemanda.Visible = false;
        lblMensaje.Text = "";
        TxtCapFechaNeg.Text = "";
        ddlCapProducto.SelectedValue = "0";
        ddlCapPunta.SelectedValue = "0";
        ddlCapTipoMercado.SelectedValue = "0";
        ddlCapComprador.SelectedValue = "0";
        ddlCapVendedor.SelectedValue = "0";
        TxtNumContrato.Text = "";
        TxtFechaSus.Text = "";
        ddlModalidad.SelectedValue = "0";
        ddlPuntoEntr.SelectedValue = "0";
        TxtCantidad.Text = "";
        TxtPrecio.Text = "";
        TxtFechaInicial.Text = "";
        TxtFechaFinal.Text = "";
        //dlTipoDemanda.SelectedValue = "0";
        ddlSentidoFlujo.SelectedValue = "";
        TxtPresion.Text = "";
        ddlFuente.SelectedValue = "0"; //Fuente o campo 20160602
    }
}