﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_RegistroContratosExt.aspx.cs"
    Inherits="Verificacion_frm_RegistroContratosExt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
        width="90%">
        <tr>
            <td align="center" class="th1" style="width: 80%;">
                <asp:Label ID="lblTitulo" runat="server"></asp:Label>
            </td>
            <td align="center" class="th1" style="width: 20%;">
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/exel 2007 3D.png"
                    Height="40" OnClick="imbExcel_Click" Visible="false" />
            </td>
        </tr>
    </table>
    <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
        width="100%">
        <%--20161207 rq102--%>
        <tr>
            <td class="td1">Fecha Negociación
                <%--20161207 rq102--%>
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaIni" runat="server" ValidationGroup="detalle" Width="100px"></asp:TextBox>
              <%--  <ajaxToolkit:CalendarExtender ID="CeTxtFechaIni" runat="server" TargetControlID="TxtFechaIni"
                    Format="yyyy/MM/dd">
                </ajaxToolkit:CalendarExtender>--%>
                <%--20161213 rq102--%>
                &nbsp;&nbsp;- &nbsp;&nbsp;
                <%--20161213 rq102--%>
                <asp:TextBox ID="TxtFechaFin" runat="server" ValidationGroup="detalle" Width="100px"></asp:TextBox>
                <%--20161213 rq102--%>
               <%-- <ajaxToolkit:CalendarExtender ID="ceTxtFechaFin" runat="server" TargetControlID="TxtFechaFin"
                    Format="yyyy/MM/dd">
                </ajaxToolkit:CalendarExtender>--%>
                <asp:HiddenField ID="hdfPermiteIng" runat="server" />
                <asp:HiddenField ID="hdfNoHorasPrim" runat="server" />
                <asp:HiddenField ID="hdfNoDiasSecun" runat="server" />
            </td>
            <td class="td1">Mercado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlMercado" runat="server">
                    <asp:ListItem Value="" Text="Seleccione"></asp:ListItem>
                    <asp:ListItem Value="P" Text="Primario"></asp:ListItem>
                    <asp:ListItem Value="S" Text="Secundario"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td1">No. Contrato
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNoContrato" runat="server" ValidationGroup="detalle" Width="100px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">Producto
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlProducto" runat="server">
                </asp:DropDownList>
            </td>
            <td class="td1">Comprador
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlComprador" runat="server" Width="250px">
                </asp:DropDownList>
            </td>
            <td class="td1">Vendedor
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlVendedor" runat="server" Width="250px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">Estado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlEstado" runat="server">
                </asp:DropDownList>
            </td>
            <td class="td1">No. Operación
                <%--20160811--%>
                <%--20161213 Rq102--%>
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtIdRegini" runat="server" ValidationGroup="detalle" Width="100px"></asp:TextBox>
                -
                <asp:TextBox ID="TxtIdRegFin" runat="server" ValidationGroup="detalle" Width="100px"></asp:TextBox>
            </td>
            <td class="td1">Números de Operaciones
                <%--20160811--%>
                <%--20161213 rq102--%>
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtIdsReg" runat="server" ValidationGroup="detalle" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="8" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
                <asp:ImageButton ID="ImbCrearReg" runat="server" ImageUrl="~/Images/CrearOtro.png"
                    Height="40" OnClick="ImbCrearReg_Click" />
                <asp:Button ID="btnVincular" runat="server" Text="Vincular" OnClick="btnVincular_Click"
                    ToolTip="Permite vincular dos Registros Ingresados por Operadores, para Unir la Información en Un solo Registro." />
                <asp:Button ID="btnLimpiar" runat="server" Text="Limpiar Campos" OnClick="btnLimpiar_Click" />
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="TblElimina" visible="false">
        <tr>
            <td align="center" class="td1">Observacion Eliminación
            </td>
            <td align="center" colspan="3" class="td1">
                <asp:TextBox ID="TxtObservacionElim" runat="server" MaxLength="500" Width="300px"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="90%" runat="server"
        id="tblGrilla" visible="false">
        <tr>
            <td colspan="3" align="center">
                <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                    AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1"
                    OnEditCommand="dtgConsulta_EditCommand" AllowPaging="true" PageSize="30" OnPageIndexChanged="dtgConsulta_PageIndexChanged">
                    <Columns>
                        <%--20160811--%>
                        <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="No. Operacion" ItemStyle-HorizontalAlign="Left">
                            <%--20161213 rq102--%>
                        </asp:BoundColumn>
                        <%--1--%><asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociacion"
                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                        <%--2--%><asp:BoundColumn DataField="tipo_subasta" HeaderText="Tipo Subasta" ItemStyle-HorizontalAlign="Left"
                            Visible="false"></asp:BoundColumn>
                        <%--3--%><asp:BoundColumn DataField="mercado" HeaderText="Mercado" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="80px"></asp:BoundColumn>
                        <%--4--%><asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="100px"></asp:BoundColumn>
                        <%--5--%><asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="100px"></asp:BoundColumn>
                        <%--6--%><asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="100px"></asp:BoundColumn>
                        <%--7--%><asp:BoundColumn DataField="fecha_limite_registro" HeaderText="Fecha Limite Registro"
                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                        <%--8--%><asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--9--%><asp:BoundColumn DataField="id" Visible="false"></asp:BoundColumn>
                        <%--10--%><asp:BoundColumn DataField="operador_compra" Visible="false"></asp:BoundColumn>
                        <%--11--%><asp:BoundColumn DataField="operador_venta" Visible="false"></asp:BoundColumn>
                        <%--12--%><asp:BoundColumn DataField="cantidad" Visible="false"></asp:BoundColumn>
                        <%--13--%><asp:BoundColumn DataField="precio" Visible="false"></asp:BoundColumn>
                        <%--14--%><asp:BoundColumn DataField="codigo_tipo_subasta" Visible="false"></asp:BoundColumn>
                        <%--15--%><asp:BoundColumn DataField="tipo_doc_comprador" Visible="false"></asp:BoundColumn>
                        <%--16--%><asp:BoundColumn DataField="tipo_doc_vendedor" Visible="false"></asp:BoundColumn>
                        <%--17--%><asp:BoundColumn DataField="documento_c" Visible="false"></asp:BoundColumn>
                        <%--18--%><asp:BoundColumn DataField="documento_v" Visible="false"></asp:BoundColumn>
                        <%--19--%><asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="No. Registro"></asp:BoundColumn>
                        <%--20--%><asp:BoundColumn DataField="destino_rueda" Visible="false"></asp:BoundColumn>
                        <%--21--%><asp:BoundColumn DataField="sigla_estado" Visible="false"></asp:BoundColumn>
                        <%--22--%><asp:BoundColumn DataField="tipo_mercado" Visible="false"></asp:BoundColumn>
                        <%-- 23 20160603-cambio fuente--%>
                        <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente"></asp:BoundColumn>
                        <%-- 24 20160603-cambio fuente--%>
                        <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente"></asp:BoundColumn>
                        <%--25--%><asp:EditCommandColumn HeaderText="Verificar" EditText="Verificar"></asp:EditCommandColumn>
                        <%--26--%><asp:BoundColumn DataField="codigo_producto" Visible="false"></asp:BoundColumn>
                        <%--27--%><asp:BoundColumn DataField="no_contrato" HeaderText="No. Contrato"></asp:BoundColumn>
                        <%--28--%><asp:EditCommandColumn HeaderText="Accion" EditText="Seleccionar"></asp:EditCommandColumn>
                        <%--29--%><asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                        <%--30--%><asp:TemplateColumn HeaderText="Sele.">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkRecibir" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%--31--%><asp:BoundColumn DataField="ind_verifica" Visible="false"></asp:BoundColumn>
                        <%--32--%><asp:BoundColumn DataField="ind_modifica" Visible="false"></asp:BoundColumn>
                        <%--33--%><asp:BoundColumn DataField="fecha_maxima_correccion" Visible="false"></asp:BoundColumn>
                        <%--34--%><asp:BoundColumn DataField="fecha_maxima_modificacion" Visible="false"></asp:BoundColumn>
                        <%--35--%><asp:BoundColumn DataField="fecha_max_registro" Visible="false"></asp:BoundColumn>
                    </Columns>
                    <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <table id="tblRegCnt" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
        width="100%" visible="false">
        <tr>
            <td class="td1">Fecha de Negociación {YYYY/MM/DD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCapFechaNeg" runat="server"></asp:TextBox>
               <%-- <ajaxToolkit:CalendarExtender ID="CeTxtCapFechaNeg" runat="server" TargetControlID="TxtCapFechaNeg"
                    Format="yyyy/MM/dd">
                </ajaxToolkit:CalendarExtender>--%>
                <asp:Label ID="lblOperacion" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>-<asp:Label
                    ID="lblPunta" runat="server" ForeColor="Red" Font-Bold="true" Visible="false"></asp:Label>
                <asp:HiddenField ID="hdfPunta" runat="server" />
                <asp:HiddenField ID="hdfIdVerif" runat="server" />
                <asp:HiddenField ID="hdfCodTipoSub" runat="server" />
                <asp:HiddenField ID="hdfDestinoRueda" runat="server" />
                <asp:HiddenField ID="hdfRutaArchivo" runat="server" />
                <asp:HiddenField ID="hdfEstadoAct" runat="server" />
                <asp:HiddenField ID="hdfAccion" runat="server" />
                <asp:HiddenField ID="hdfCodVerifCont" runat="server" />
                <asp:HiddenField ID="hdfTipoMerc" runat="server" />
                <asp:HiddenField ID="hdfOPeraC" runat="server" />
                <asp:HiddenField ID="hdfOPeraV" runat="server" />
                <asp:HiddenField ID="hdfCodProd" runat="server" />
            </td>
            <td class="td1">Producto
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlCapProducto" runat="server" OnSelectedIndexChanged="ddlCapProducto_SelectedIndexChanged"
                    AutoPostBack="true">
                </asp:DropDownList>
                <asp:Label ID="lblProducto" runat="server" ForeColor="Red" Font-Bold="true" Visible="false"></asp:Label>
            </td>
            <td class="td1">Punta que Registra
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlCapPunta" runat="server" OnSelectedIndexChanged="ddlCapPunta_SelectedIndexChanged"
                    AutoPostBack="true">
                    <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                    <asp:ListItem Value="C" Text="Comprador"></asp:ListItem>
                    <asp:ListItem Value="V" Text="Vendedor"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">Tipo Mercado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlCapTipoMercado" runat="server" OnSelectedIndexChanged="ddlCapTipoMercado_SelectedIndexChanged"
                    AutoPostBack="true">
                    <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                    <asp:ListItem Value="P" Text="Primario"></asp:ListItem>
                    <asp:ListItem Value="S" Text="Secundario"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">Comprador
            </td>
            <td class="td2" colspan="2">
                <asp:DropDownList ID="ddlCapComprador" runat="server" Width="250px">
                </asp:DropDownList>
            </td>
            <td class="td1">Vendedor
            </td>
            <td class="td2" colspan="2">
                <asp:DropDownList ID="ddlCapVendedor" runat="server" Width="250px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr runat="server" id="TrInfOP" visible="false">
            <td class="td1">
                <asp:Label ID="lnlNomOpera" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblNomOperador" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
            </td>
            <td class="td1">
                <asp:Label ID="lblTipoDocOpera" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblTipoDocOperador" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
            </td>
            <td class="td1">
                <asp:Label ID="lblNoDocOPera" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblNoDocumentoOperaqdor" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">Número Contrato
                <%--20161207 rq102--%>
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNumContrato" runat="server" ValidationGroup="detalle" Width="100px"
                    MaxLength="30"></asp:TextBox>
            </td>
            <td class="td1">Fecha Suscripción {YYYY/MM/DD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaSus" runat="server" ValidationGroup="detalle" Width="100px"></asp:TextBox>
               <%-- <ajaxToolkit:CalendarExtender ID="CeTxtFechaSus" runat="server" TargetControlID="TxtFechaSus"
                    Format="yyyy/MM/dd">
                </ajaxToolkit:CalendarExtender>--%>
            </td>
            <td class="td1">Modalidad de Contrato
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlModalidad" runat="server">
                </asp:DropDownList>
                Otro
                <asp:CheckBox ID="ChkOtrModa" runat="server" />
                <asp:TextBox ID="TxtOtrModalidad" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                <asp:Label ID="lblPuntoE" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlPuntoEntr" runat="server" OnSelectedIndexChanged="ddlPuntoEntr_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:CheckBox ID="ChkOtrPuntoE" runat="server" Visible="false" />
                <asp:TextBox ID="TxtOtrPuntoE" runat="server" Visible="false"></asp:TextBox>
            </td>
            <td class="td1">
                <asp:Label ID="lblCantidad" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCantidad" runat="server" ValidationGroup="detalle" Width="100px"></asp:TextBox>
            </td>
            <td class="td1">
                <asp:Label ID="lblPrecio" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtPrecio" runat="server" ValidationGroup="detalle" Width="100px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                <asp:Label ID="lblFechaInc" runat="server"></asp:Label>
                {YYYY/MM/DD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaInicial" runat="server" ValidationGroup="detalle" Width="100px"></asp:TextBox>
               <%-- <ajaxToolkit:CalendarExtender ID="CeTxtFechaInicial" runat="server" TargetControlID="TxtFechaInicial"
                    Format="yyyy/MM/dd">
                </ajaxToolkit:CalendarExtender>--%>
            </td>
            <td class="td1">
                <asp:Label ID="lblFechaFin" runat="server"></asp:Label>
                {YYYY/MM/DD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaFinal" runat="server" ValidationGroup="detalle" Width="100px"></asp:TextBox>
             <%--   <ajaxToolkit:CalendarExtender ID="CeTxtFechaFinal" runat="server" TargetControlID="TxtFechaFinal"
                    Format="yyyy/MM/dd">
                </ajaxToolkit:CalendarExtender>--%>
            </td>
            <td class="td1">Copia del Contrato
            </td>
            <td class="td2">
                <asp:FileUpload ID="FuCopiaCont" runat="server" />
            </td>
        </tr>
        <tr runat="server" id="TrTrans" visible="false">
            <td class="td1">Sentido Contratado para el flujo de Gas Natural
            </td>
            <td class="td2" colspan="2">
                <asp:DropDownList ID="ddlSentidoFlujo" runat="server">
                    <asp:ListItem Value="" Text="Seleccione"></asp:ListItem>
                    <asp:ListItem Value="NORMAL" Text="NORMAL"></asp:ListItem>
                    <asp:ListItem Value="CONTRA FLUJO" Text="CONTRA FLUJO"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td1">Presión para el punto de terminación del servicio (psig)
            </td>
            <td class="td2" colspan="2">
                <asp:TextBox ID="TxtPresion" runat="server" ValidationGroup="detalle" Width="120px"></asp:TextBox>
            </td>
        </tr>
        <%--fuente o campo 20160602--%>
        <tr runat="server" id="TrFuente" visible="false">
            <td class="td1">Fuente
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlFuente" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr runat="server" id="TrModifi" visible="false">
            <td class="td1">Motivo Modificación
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlMotivoModifi" runat="server">
                </asp:DropDownList>
            </td>
            <td class="td1">Cesionario
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlCesionario" runat="server" Width="250px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="8" align="center">
                <asp:Button ID="btnCrear" runat="server" Text="Crear" OnClick="btnCrear_Click" OnClientClick="this.disabled = true;"
                    UseSubmitBehavior="false" />
                <asp:Button ID="btnActualizar" runat="server" Text="Actualizar" OnClick="btnActualizar_Click"
                    OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                <asp:Button ID="btnRegresar" runat="server" Text="Regresar" OnClick="btnRegresar_Click" />
                <asp:Button ID="btnCrearUsu" runat="server" Text="Crear Usuario" OnClick="btnCrearUsu_Click"
                    Visible="false" />
                <asp:Button ID="btnActualUsu" runat="server" Text="Actualiza Usuario" OnClick="btnActualUsu_Click"
                    Visible="false" />
            </td>
        </tr>
    </table>
    <table id="tblDemanda" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
        width="90%" visible="false">
        <tr>
            <td class="td1">Tipo Demanda a Atender
            </td>
            <td class="td1">Usuario Final
            </td>
            <td class="td1">
                <asp:Label ID="lblSector" runat="server"></asp:Label>
            </td>
            <td class="td1">Punto de Salida en SNT
            </td>
            <td class="td1">Mercado Relevante
            </td>
            <td class="td1">
                <asp:Label ID="lblCantContra" runat="server" Width="100px"></asp:Label>
            </td>
            <%--Campo Nuevo Req. 009-17 Indicadores 20170321--%>
            <td class="td1" runat="server" id="TdIndica" visible="false">Equivalente Kpcd
            </td>
        </tr>
        <tr>
            <td class="td2">
                <asp:DropDownList ID="dlTipoDemanda" runat="server" OnSelectedIndexChanged="dlTipoDemanda_SelectedIndexChanged"
                    AutoPostBack="true">
                </asp:DropDownList>
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtUsuarioFinal" runat="server" Width="200px"></asp:TextBox>
<%--                <ajaxToolkit:AutoCompleteExtender runat="server" BehaviorID="AutoCompleteExUsuF" ID="AutoCompleteExtender1"
                    TargetControlID="TxtUsuarioFinal" ServicePath="~/WebService/AutoComplete.asmx"
                    ServiceMethod="GetCompletionListUsuarioFinal" MinimumPrefixLength="3" CompletionInterval="1000"
                    EnableCaching="true" CompletionSetCount="20" CompletionListCssClass="autocomplete_completionListElement"
                    CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                    DelimiterCharacters=";,:">
                    <Animations>
                        <OnShow>
                            <Sequence>
                                <OpacityAction Opacity="0" />
                                <HideAction Visible="true" />
                                <ScriptAction Script="
                                    // Cache the size and setup the initial size
                                    var behavior = $find('AutoCompleteExUsuF');
                                    if (!behavior._height) {
                                        var target = behavior.get_completionList();
                                        behavior._height = target.offsetHeight - 2;
                                        target.style.height = '0px';
                                    }" />
                                <Parallel Duration=".4">
                                    <FadeIn />
                                    <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteExUsuF')._height" />
                                </Parallel>
                            </Sequence>
                        </OnShow>
                        <OnHide>
                            <Parallel Duration=".4">
                                <FadeOut />
                                <Length PropertyKey="height" StartValueScript="$find('AutoCompleteExUsuF')._height" EndValue="0" />
                            </Parallel>
                        </OnHide>
                    </Animations>
                </ajaxToolkit:AutoCompleteExtender>--%>
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlSector" runat="server" Width="150px">
                </asp:DropDownList>
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlPuntoSalida" runat="server" Width="190px">
                </asp:DropDownList>
                <asp:HiddenField ID="hdfTipoDemanda" runat="server" />
                <asp:HiddenField ID="hdfCodDatUsu" runat="server" />
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlMercadoRel" runat="server" Width="190px">
                </asp:DropDownList>
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCantidadUsu" runat="server" ValidationGroup="detalle" Width="120px"></asp:TextBox>
            </td>
            <%--Campo nuevo Req. 009-17 Indicadores 20170321--%>
            <td class="td2" runat="server" id="TdIndica1" visible="false">
                <asp:TextBox ID="TxtEquivaleKpcd" runat="server" ValidationGroup="detalle" Width="120px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td2" colspan="7" align="center">
                <%--rq011-17 20170425--%>
                <b>LISTADO DE USUARIOS FINALES</b>
            </td>
        </tr>
        <tr>
            <td class="td2" colspan="7" align="center">
                <%--rq011-17 20170425--%>
                <b>TOTAL CANTIDAD:</b><asp:Label ID="lblTotlCantidad" runat="server" ForeColor="Red"
                    Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="7" align="center">
                <%--rq011-17 20170425--%>
                <div style="overflow: scroll; width: 1020px; height: 450px;">
                    <asp:DataGrid ID="dtgUsuarios" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                        AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1"
                        OnEditCommand="dtgUsuarios_EditCommand">
                        <Columns>
                            <%--0--%>
                            <asp:BoundColumn DataField="tipo_demanda" HeaderText="Tipo Demanda Atender" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--1--%>
                            <asp:BoundColumn DataField="no_identificacion_usr" HeaderText="Identificacion Usuario Final"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--2--%>
                            <asp:BoundColumn DataField="tipo_documento" HeaderText="Tipo Identificacion Usuario Final"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--3--%>
                            <asp:BoundColumn DataField="nombre_usuario" HeaderText="Nombre Usuario Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--4--%>
                            <asp:BoundColumn DataField="sector_consumo" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="80px"></asp:BoundColumn>
                            <%--5--%>
                            <asp:BoundColumn DataField="punto_salida" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--6--%>
                            <asp:BoundColumn DataField="cantidad_contratada" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--7--%>
                            <%--Campo Nuevo Req. 009-17 Indsicadores 20170321--%>
                            <asp:BoundColumn DataField="equivalente_kpcd" HeaderText="Equivalente Kpcd" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px" Visible="false"></asp:BoundColumn>
                            <%--8--%>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar"></asp:EditCommandColumn>
                            <%--9--%>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                            <%--10--%>
                            <asp:BoundColumn DataField="codigo_datos_usuarios" Visible="false"></asp:BoundColumn>
                            <%--11--%>
                            <asp:BoundColumn DataField="codigo_cont_datos" Visible="false"></asp:BoundColumn>
                            <%--12--%>
                            <asp:BoundColumn DataField="codigo_sector_consumo" Visible="false"></asp:BoundColumn>
                            <%--13--%>
                            <asp:BoundColumn DataField="codigo_punto_salida" Visible="false"></asp:BoundColumn>
                            <%--14--%>
                            <asp:BoundColumn DataField="codigo_tipo_doc" Visible="false"></asp:BoundColumn>
                            <%--15--%>
                            <asp:BoundColumn DataField="codigo_tipo_demanda" Visible="false"></asp:BoundColumn>
                            <%--16--%>
                            <asp:BoundColumn DataField="codigo_mercado_relevante" Visible="false"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
    <table id="tblVerifica" runat="server" border="0" align="center" cellpadding="3"
        cellspacing="2" width="90%" visible="false">
        <tr>
            <td class="td2" colspan="4" align="center">
                <b>VERFICIACION REGISTRO</b>
            </td>
        </tr>
        <tr>
            <td class="td2" align="center">Dato
            </td>
            <td class="td2" align="center">Comprador
            </td>
            <td class="td2" align="center">Vendedor
            </td>
            <td class="td2" align="center">Verificacion
            </td>
        </tr>
        <tr>
            <td class="td1">
                <asp:Label ID="lblTit1" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblDat1C" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblDat1V" runat="server"></asp:Label>
            </td>
            <td class="td1">
                <asp:Label ID="lblDat1Ver" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                <asp:Label ID="lblTit2" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblDat2C" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblDat2V" runat="server"></asp:Label>
            </td>
            <td class="td1">
                <asp:Label ID="lblDat2Ver" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                <asp:Label ID="lblTit3" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblDat3C" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblDat3V" runat="server"></asp:Label>
            </td>
            <td class="td1">
                <asp:Label ID="lblDat3Ver" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                <asp:Label ID="lblTit4" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblDat4C" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblDat4V" runat="server"></asp:Label>
            </td>
            <td class="td1">
                <asp:Label ID="lblDat4Ver" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                <asp:Label ID="lblTit5" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblDat5C" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblDat5V" runat="server"></asp:Label>
            </td>
            <td class="td1">
                <asp:Label ID="lblDat5Ver" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                <asp:Label ID="lblTit6" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblDat6C" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblDat6V" runat="server"></asp:Label>
            </td>
            <td class="td1">
                <asp:Label ID="lblDat6Ver" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                <asp:Label ID="lblTit7" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblDat7C" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblDat7V" runat="server"></asp:Label>
            </td>
            <td class="td1">
                <asp:Label ID="lblDat7Ver" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                <asp:Label ID="lblTit8" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblDat8C" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblDat8V" runat="server"></asp:Label>
            </td>
            <td class="td1">
                <asp:Label ID="lblDat8Ver" runat="server"></asp:Label>
            </td>
        </tr>
        <tr runat="server" id="TrTra1" visible="false">
            <td class="td1">
                <asp:Label ID="lblTit9" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblDat9C" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblDat9V" runat="server"></asp:Label>
            </td>
            <td class="td1">
                <asp:Label ID="lblDat9Ver" runat="server"></asp:Label>
            </td>
        </tr>
        <tr runat="server" id="TrTra2" visible="false">
            <td class="td1">
                <asp:Label ID="lblTit10" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblDat10C" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <asp:Label ID="lblDat10V" runat="server"></asp:Label>
            </td>
            <td class="td1">
                <asp:Label ID="lblDat10Ver" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                <asp:Label ID="lblTit11" runat="server"></asp:Label>
            </td>
            <td class="td2">
                <a id="LinkC" runat="server">Ver Contrato</a>
            </td>
            <td class="td2">
                <a id="LinkV" runat="server">Ver Contrato</a>
            </td>
            <td class="td1"></td>
        </tr>
        <tr>
            <td class="td1">Observacion
            </td>
            <td class="td2" colspan="3">
                <asp:TextBox ID="TxtObservacion" runat="server" ValidationGroup="detalle" Width="700px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="4" align="center">
                <asp:Button ID="btnSolCorr" runat="server" Text="Solicitar Correccion" OnClick="btnSolCorr_Click" />
                <asp:Button ID="btnRegCont" runat="server" Text="Registro Contrato" OnClick="btnRegCont_Click" />
                <asp:ImageButton ID="ImmSalirVer" runat="server" ImageUrl="~/Images/Salir.png" Height="40"
                    OnClick="ImmSalirVer_Click" />
            </td>
        </tr>
    </table>
    <%--<table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="Table1">
        <tr>
            <td>
                <asp:ScriptManager ID="ScriptManagerMestro" runat="server" AsyncPostBackTimeout="1000">
                </asp:ScriptManager>
                <asp:UpdatePanel runat="server" ID="Panel">
                    <ContentTemplate>
                        <asp:UpdateProgress ID="PageUpdateProgress" runat="server">
                            <ProgressTemplate>
                                <div class="fuente">
                                    <img src="../Images/ajax-loader.gif" style="position: static; width: 32px;" alt="Procesando" />
                                    Procesando por Favor espere ...
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="imbConsultar" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>--%>
</asp:Content>
