﻿

<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_PubIndicadoresCregMP.aspx.cs"
    Inherits="Verificacion_frm_PubIndicadoresCregMP" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
            width="80%">
            <tr>
                <td align="center" class="th1">
                    <asp:Label ID="lblTitulo" runat="server" ForeColor="White" ></asp:Label>
                </td>
            </tr>
        </table>
        <br /><br /><br /><br /><br /><br />
        <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
            width="80%">
            <tr>
                <td class="td1">
                    Indicador
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlIndicador" runat="server" OnSelectedIndexChanged="ddlIndicador_SelectedIndexChanged"
                        AutoPostBack="true">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">
                    Vista
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlVista" runat="server" OnSelectedIndexChanged="ddlVista_SelectedIndexChanged"
                        AutoPostBack="true">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="trFechaIni" runat="server">
                <td class="td1">
                    Fecha Inicial
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                </td>
            </tr>
            <tr id="trFechaFin" runat="server">
                <td class="td1">
                    Fecha Final
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                </td>
            </tr>
            <tr id="trObs" runat="server" visible="false" >
                <td class="td1">
                    Observación de publicación:
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtObs" runat="server" ValidationGroup="detalle" MaxLength="1000" TextMode="MultiLine" Rows="3" Width= "600"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblMensaje">
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:Button ID="btnPrevia" runat="server" Text="Consulta" OnClick="btnPrevia_Click" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnDefinitiva" runat="server" Text="Publicación" OnClick="btnDefinitiva_Click" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 1050px; height: 350px;">
                    <asp:DataGrid ID="dtgIndicador1_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="PTDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PP (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador1_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="código Productor" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="nombre Productor" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="PTDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PP (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador1_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_punto_snt" HeaderText="código Fuente" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_punto" HeaderText="fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="PTDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PP (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador2_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PTDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador2" HeaderText="CIDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador2_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_punto" HeaderText="Código Fuente" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PTDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador2" HeaderText="CIDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador2_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="no_documento" HeaderText="Nit Operador" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PTDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador2" HeaderText="CIDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador3_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PP (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador3_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Productor" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Productor" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PP (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador3_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_punto" HeaderText="Código Fuente" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PP (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador4_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Oferta Comprometida en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PTDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador2" HeaderText="CIDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador4_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_punto" HeaderText="Código Fuente" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Oferta Comprometida en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PTDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador2" HeaderText="CIDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador4_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="no_documento" HeaderText="Nit operador" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Oferta Comprometida en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PTDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador2" HeaderText="CIDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador5_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Oferta Comprometida en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador5_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_punto" HeaderText="Código Fuente" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Oferta Comprometida en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador5_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="no_documento" HeaderText="Nit Operador" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Oferta Comprometida en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador6_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Oferta Comprometida en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PP (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador6_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_punto" HeaderText="Código Fuente" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Oferta Comprometida en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PP (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador6_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Productor" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Productor" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Oferta Comprometida en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PP (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador7_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Demanda Regulada (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador7_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Demanda Regulada (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador8_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratacón en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador8_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_punto" HeaderText="Código fuente" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_punto" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador9_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Oferta Comprometida en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador9_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_fuente" HeaderText="Código fuente" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Oferta Comprometida en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador10_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador10_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_punto_entrega" HeaderText="Código fuente" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador10_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="no_documento" HeaderText="Nit Operador" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador10_S" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_sector_consumo" HeaderText="Código Sector de Consumo" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_sector" HeaderText="Sector de Consumo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador11_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Oferta Comprometida en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador11_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_fuente" HeaderText="Código fuente" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Oferta Comprometida en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador11_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Oferta Comprometida en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador11_S" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_sector_consumo" HeaderText="Código Sector de Consumo" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_sector" HeaderText="Sector de Consumo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Oferta Comprometida en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador12_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Contratación Total (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador12_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Contratación Total (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador13_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Contratación Total (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador13_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Contratación total (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador14" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_mercado_relevante" HeaderText="Código Mercado Relevante"
                                ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_mercado" HeaderText="Mercado Relevante" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Contratación de Capacidad de Transporte (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador15" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador"
                                ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Contratación de Capacidad de Transporte (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador16" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código operador" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_tramo" HeaderText="Código tramo" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación de Transporte  (Kpcd)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Capacidad Máxima de Mediano Plazo (Kpcd)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador17_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Código Modalidad" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación por Modalidad (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Contratación Total (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador17_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Código Modalidad" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación por Modalidad (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Contratación Total (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador18_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Código Modalidad" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación por Modalidad (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Contratación total (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador18_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Código Modalidad" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contración por Modalidad (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Contratación Total (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador19" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_tramo" HeaderText="Código Tramo" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Código Modalidad" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_mercado_relevante" HeaderText="Código Mercado" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_mercado" HeaderText="Mercado Relevante" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación de Transporte por Modalidad (Kpcd)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Capacidad Máxima de Mediano Plazo (Kpcd)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador20" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_tramo" HeaderText="Código Tramo" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Código Modalidad" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación de transporte por Modalidad (Kpcd)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Capacidad Máxima de Mediano Plazo (Kpcd)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador21_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador Comprador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_fuente" HeaderText="Código Fuente" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación por Comprador (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Oferta Comprometida en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador21_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_venta" HeaderText="Código Vendedor" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_venta" HeaderText="Operador Vendedor" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_compra" HeaderText="Código Comprador" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_compra" HeaderText="Operador Comprador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación de Suministro (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Oferta Comprometida en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador21_A" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador Comprador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación de Suministro (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Oferta Comprometida en Firme (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador22" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_tramo" HeaderText="Código Tramo" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Contratación Por Agente (Kpcd)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Capacidad Máxima de Mediano Plazo (Kpcd)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador23_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Cantidad * Precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador23_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_fuente" HeaderText="Código Fuente" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Cantidad * Precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador23_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Cantidad * Precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador23_C" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Código Modalidad" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Cantidad * Precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador23_D" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_tipo_demanda" HeaderText="Código Tipo de Demanda" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo de Demanda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Cantidad * Precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador23_S" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_sector_consumo" HeaderText="Código Sector Consumo" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_sector" HeaderText="Sector de Consumo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Cantidad * Precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIndicador23_T" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_fuente" HeaderText="Código Fuente" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_tipo_demanda" HeaderText="Código Tipo Demanda" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo de Demanda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_sector_consumo" HeaderText="Código Sector de Consumo" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_sector" HeaderText="Sector de Consumo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numerador1" HeaderText="Cantidad * Precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="denominador1" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>