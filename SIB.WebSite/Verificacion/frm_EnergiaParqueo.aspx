﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_EnergiaParqueo.aspx.cs" Inherits="Verificacion_frm_EnergiaParqueo" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Código Cantidad parqueo</label>
                            <asp:TextBox ID="TxtBusCodigo" runat="server" autocomplete="off" CssClass="form-control"></asp:TextBox>
                            <asp:CompareValidator ID="CvTxtBusCodigo" runat="server" ControlToValidate="TxtBusCodigo"
                                Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle1" ErrorMessage="El Campo Código Cantidad parqueo Registro debe Ser numérico">*</asp:CompareValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fecha Inicial</label>
                            <asp:TextBox ID="TxtBusFecha" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fecha Final</label>
                            <asp:TextBox ID="TxtBusFechaF" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Operador</label>
                            <asp:DropDownList ID="ddlBusOperador" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Tramo</label>
                            <asp:DropDownList ID="ddlBusTramo" runat="server" CssClass="form-control selectpicker" OnSelectedIndexChanged="ddlBusTramo_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Remitente</label>
                            <asp:DropDownList ID="ddlBusRemitente" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Código Punto Salida SNT</label>
                            <asp:DropDownList ID="ddlBusPozo" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <%--Mensaje--%>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:ValidationSummary ID="VsConsultas" runat="server" ValidationGroup="detalle1" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
                <%--Grilla--%>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" PageSize="10"
                                OnItemCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged">
                                <Columns>
                                    <%--20180126 rq107-16--%>
                                    <asp:BoundColumn DataField="codigo_energia_parqueo" HeaderText="Código capacidad"
                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha" HeaderText="fecha" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20180126 rq107-16--%>
                                    <asp:BoundColumn DataField="codigo_operador" HeaderText="Código operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_tramo" HeaderText="Desc Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20180126 rq107-16--%>
                                    <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="código Punto Salida"
                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_punto_Salida" HeaderText="desc Punto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20160725--%>
                                    <asp:BoundColumn DataField="punto_trasferencia" HeaderText="Cod. Pto. Tras." ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20160725--%>
                                    <%--20180126 rq107-16--%>
                                    <asp:BoundColumn DataField="desc_trasferencia" HeaderText="Punto Transferencia" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20180126 rq107-16--%>
                                    <asp:BoundColumn DataField="codigo_remitente" HeaderText="código remitente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_remitente" HeaderText="nombre remitente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="energia_parqueo" HeaderText="cnt parqueo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20160711--%>
                                    <asp:BoundColumn DataField="carga_normal" HeaderText="Estado de Carga" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20220705--%>
                                    <asp:BoundColumn DataField="dato_atipico" HeaderText="Dato Atipico" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20180126 rq107-16--%>
                                    <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualización" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more-1"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                    <!--begin::Nav-->
                                                    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <ul class="kt-nav">
                                                                <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                             <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Modificar</span>
                                                                    </asp:LinkButton>
                                                                </li>
                                                                <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon-delete"></i>
                                                            <span class="kt-nav__link-text">Eliminar</span>
                                                                    </asp:LinkButton>
                                                                </li>
                                                            </ul>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="lkbModificar" />
                                                            <asp:AsyncPostBackTrigger ControlID="lkbEliminar" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%--Modals--%>
    <div class="modal fade" id="registroEnergiaParqueo" tabindex="-1" role="dialog" aria-labelledby="mdlregistroEnergiaParqueoLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="registroEnergiaParqueoInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <Triggers>
                        <asp:PostBackTrigger ControlID="imbCrear" />
                    </Triggers>
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlregistroEnergiaParqueoLabel" runat="server">Agregar</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h5>
                                <asp:Label ID="lblRegistroEnergiaParqueo" runat="server"></asp:Label>
                            </h5>

                            <hr>
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Código Energía de Parqueo</label>
                                        <asp:TextBox ID="TxtCodigoCap" runat="server" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                        <asp:Label ID="LblCodigoCap" runat="server" Visible="False" CssClass="form-control"></asp:Label>
                                        <asp:HiddenField ID="hdnELiminado" runat="server" />
                                        <%--20160721--%>
                                        <asp:HiddenField ID="hdfModificacion" runat="server" />
                                        <%--20170131 Req. 003-17 --%>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Fecha</label>
                                        <asp:TextBox ID="TxtFecha" placeholder="yyyy/mm/dd" runat="server" Enabled="false" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RfvTxtFecha" runat="server" ErrorMessage="Debe Ingresar la Fecha"
                                            ControlToValidate="TxtFecha" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Operador</label>
                                        <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlOperador_SelectedIndexChanged"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Tramo</label>
                                        <asp:DropDownList ID="ddlTramo" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlTramo_SelectedIndexChanged"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Punto Salida SNT</label>
                                        <asp:DropDownList ID="ddlPozo" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Punto de Transferencia</label>
                                        <asp:DropDownList ID="ddlTrasferencia" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlTrasferencia_SelectedIndexChanged"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Remitente</label>
                                        <asp:DropDownList ID="ddlRemitente" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Cantidad de parqueo</label>
                                        <asp:TextBox ID="TxtCantidad" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:CompareValidator ID="CvTxtCantidad" runat="server" ControlToValidate="TxtCantidad"
                                            Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Cantidad de parqueo debe Ser numérico">*</asp:CompareValidator>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Estado</label>
                                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="A">Activo</asp:ListItem>
                                            <asp:ListItem Value="I">Inactivo</asp:ListItem>
                                            <asp:ListItem Value="E">Eliminado</asp:ListItem>
                                            <%--20161124--%>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="detalle" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button Text="Salir" CssClass="btn btn-secondary" OnClick="Cancel_OnClick" runat="server" /> <%--20201020--%>
                            <asp:Button ID="imbCrear" runat="server" CssClass="btn btn-primary" Text="Crear" OnClick="imbCrear_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="imbActualiza" runat="server" CssClass="btn btn-primary" Text="Actualizar" OnClick="imbActualiza_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <%--20220705 pasa validaciones--%>
    <div class="modal fade" id="mdlConfirma" tabindex="-1" role="dialog" aria-labelledby="mdlConfirmaLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlConfirmaInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <div class="modal-header" style="background-color: #3E5F8A;">
                            <h5 class="modal-title" id="mdlConfirmaLabel" runat="server" style="color: white;">Confirmación</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff; opacity: 1;">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" style="background-color: #D3D3D3">
                            <asp:Label ID="lblMensajeCOnf" runat="server" Font-Size="Large"></asp:Label>
                        </div>
                        <div class="modal-body" style="background-color: #D3D3D3">
                            <asp:Label ID="lblConforma" runat="server" Font-Size="Large">¿Desea ingresar el dato como un valor atípico?<br /><br />
                                Nota: este reporte se rige por lo establecido en la Resolución CREG 080 de 2019 “Por la cual se establecen reglas generales de comportamiento de mercado para los agentes que desarrollen las actividades de los servicios públicos domiciliarios de energía eléctrica y gas combustible
                            </asp:Label>
                        </div>
                        <div class="modal-footer" style="background-color: #3E5F8A">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <asp:Button ID="btnAceptarConf" CssClass="btn btn-primary" Text="Aceptar" OnClick="BtnAceptarConf_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

</asp:Content>
