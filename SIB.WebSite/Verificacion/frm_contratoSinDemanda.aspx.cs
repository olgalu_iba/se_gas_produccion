﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;
using System.Text;

namespace Verificacion
{
    public partial class frm_contratoSinDemanda : Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Contratos sin demanda final";   //20160810 modalidad por tipo campo //20190306 rq013-19
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        clConexion lConexion = null;

        SqlDataReader lLector;
        SqlDataReader lLector1;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            //Controlador util = new Controlador();
            /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

            //Establese los permisos del sistema
            EstablecerPermisosSistema();
            lConexion = new clConexion(goInfo);
            new clConexion(goInfo);
            //Titulo
            Master.Titulo = "Contratos sin demanda final";
            /// Activacion de los Botones
            buttons.Inicializar(ruta: "t_contrato_sin_demand");
            buttons.CrearOnclick += btnNuevo;

            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.ExportarPdfOnclick += ImgPdf_Click;

            if (IsPostBack) return;
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles1(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A'  order by razon_social", 0, 4);
            LlenarControles1(lConexion.gObjConexion, ddlBusOperador, "m_operador", " estado = 'A'  order by razon_social", 0, 4);
            lConexion.Cerrar();
            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlBusOperador.SelectedValue = goInfo.cod_comisionista;
                ddlBusOperador.Enabled = false;
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
            }

            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            Buscar();
        }

        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "t_contrato_sin_demand");
            //hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
            //hlkListar.Enabled = (Boolean)permisos["SELECT"];
            //hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
            //if (Session["tipoPerfil"].ToString() != "N")
            //    dtgMaestro.Columns[10].Visible = (Boolean)permisos["UPDATE"]; // 20160711 ajuste modificaciones //20160810 modalidad tipo campo
            //else
            //    dtgMaestro.Columns[10].Visible = false;
            //dtgMaestro.Columns[11].Visible = false; // 20160711 ajuste modificaciones //20160810 modalidad tipo campo
            //foreach (DataGridItem Grilla in dtgMaestro.Items)
            //{
            //    var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
            //    lkbModificar.Visible = (bool)permisos["UPDATE"]; //20170131 Modif Inf Operativa Participantes Req.003-17 //20190306 rq013-19
            //    var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
            //    lkbEliminar.Visible = (bool)permisos["DELETE"];  //20170131 Modif Inf Operativa Participantes Req.003-17 //20190306 rq013-19
            //}
        }

        /// <summary>
        /// Nombre: Listar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            CargarDatos();
            lblTitulo.Text = "Consultar " + lsTitulo;
        }

       
        /// Nombre: Buscar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
        /// Modificacion:
        /// </summary>
        private void Buscar()
        {
            lblTitulo.Text = "Consultar " + lsTitulo;
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_codigo_contrato_sin", "@P_numero_contrato", "@P_fecha_inicial", "@P_fecha_final", "@P_codigo_operador" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "", "", "0" };
            DateTime ldFechaI = DateTime.Now;
            DateTime ldFechaF = DateTime.Now;
            lblMensaje.Text = "";

            try
            {
                if (TxtBusFechaIni.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFechaI = Convert.ToDateTime(TxtBusFechaIni.Text.Trim());
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += "Valor Inválido en Fecha Inicial. <br>";
                    }
                }
                if (TxtBusFechaFin.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFechaF = Convert.ToDateTime(TxtBusFechaFin.Text.Trim());
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += "Valor Inválido en Fecha Final. <br>";
                    }
                }
                if ( TxtBusFechaIni.Text.Trim().Length > 0 && TxtBusFechaFin.Text.Trim().Length > 0 && ldFechaI > ldFechaF)
                    lblMensaje.Text += "La fecha Inicial denbe ser menor o igual que la fecha final. <br>";

                if (lblMensaje.Text == "")
                {
                    if (TxtBusCodigo.Text.Trim().Length > 0)
                        lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    if (TxtBusOperacion.Text.Trim().Length > 0)
                        lValorParametros[1] = TxtBusOperacion.Text.Trim();
                    if (TxtBusFechaIni.Text.Trim().Length > 0)
                        lValorParametros[2] = TxtBusFechaIni.Text.Trim();
                    if (TxtBusFechaFin.Text.Trim().Length > 0)
                        lValorParametros[3] = TxtBusFechaFin.Text.Trim();
                    else
                        lValorParametros[3] = lValorParametros[2];
                    if (ddlBusOperador.SelectedValue != "0")
                        lValorParametros[4] = ddlBusOperador.SelectedValue;
                    lConexion.Abrir();
                    dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContratoSin", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgMaestro.DataBind();
                    lConexion.Cerrar();
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }

        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            lblMensaje.Text = "";
            this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();

        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
  


        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// 20180122 rq003-18
        ///  /// // 20180126 rq107-16
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceLlave).ToString() + "-" + lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='t_contrato_sin_demand' and llave_registro='codigo_congrato_sin=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_congrato_sin=" + lscodigo_registro.ToString();
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_contrato_sin_demand";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_contrato_sin_demand", lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] lValorParametros = { "0", "0", "", "", "0" };
            string lsParametros = "";

            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    lsParametros += " Código : " + TxtBusCodigo.Text;
                }
                if (TxtBusOperacion.Text.Trim().Length > 0)
                {
                    lValorParametros[1] = TxtBusCodigo.Text.Trim();
                    lsParametros += " Id Registro: " + TxtBusOperacion.Text;
                }
                if (TxtBusFechaIni.Text.Trim().Length > 0)
                {
                    lValorParametros[2] = TxtBusFechaIni.Text.Trim();
                    lsParametros += " Fecha inicial : " + TxtBusFechaIni.Text;
                }
                if (TxtBusFechaFin.Text.Trim().Length > 0)
                {
                    lValorParametros[3] = TxtBusFechaFin.Text.Trim();
                    lsParametros += " Fecha final: " + TxtBusFechaFin.Text;
                }
                else
                    lValorParametros[3] = lValorParametros[2];
                if (ddlBusOperador.SelectedValue != "0")
                {
                    lValorParametros[4] = ddlBusOperador.SelectedValue;
                    lsParametros += " Operador: " + ddlBusOperador.SelectedItem;
                }
                Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetContratoSin&nombreParametros=@P_codigo_contrato_sin*@P_numero_contrato*@P_fecha_inicial*@P_fecha_final*@P_codigo_operador&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "*" + lValorParametros[3] + "*" + lValorParametros[4] + "&columnas=codigo_contrato_sin*codigo_operador*nombre_compra*login_usuario*fecha_hora_actual*&titulo_informe=Listado de Adiciones&TituloParametros=" + lsParametros);
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.!";
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }

        }

        /// <summary>
        /// Nombre: ImgPdf_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgPdf_Click(object sender, EventArgs e)
        {
            try
            {
                string lsCondicion = " codigo_contrato_sin <> '0'";
                if (ddlBusOperador.SelectedValue != "0")
                    lsCondicion += " and codigo_comprador=" + ddlBusOperador.SelectedValue ;

                Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=t_contrato_sin_demand&procedimiento=pa_ValidarExistencia&columnas=codigo_contrato_sin*numero_contrato*codigo_comprador*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.!";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_numero_contrato", "@P_codigo_operador" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int};
            string[] lValorParametros = { "0", "0" };
            string lsMensaje = "";
            try
            {
                if (TxtOperacion.Text =="")
                    lsMensaje = "debe digbitar la operación<br>";
                if (lsMensaje=="")
                {
                    lValorParametros[0] = TxtOperacion.Text;
                    lValorParametros[1] = ddlOperador.SelectedValue;
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_setContratoSin", lsNombreParametros, lTipoparametros, lValorParametros);
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                            lsMensaje += lLector["error"].ToString() + "<br>";
                    }
                    else
                    {
                        Toastr.Success(this, "Registro creado correctamente");
                        Listar();
                    }
                    if (lsMensaje != "")
                        Toastr.Warning(this, lsMensaje);
                    else
                    {
                        Modal.Cerrar(this, registroAdicion.ID);
                        Listar();
                    }

                }
                else
                    Toastr.Warning(this, lsMensaje);

            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Error: " + ex.Message.ToString());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void Cancel_OnClick(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (lblRegistroAdicion.InnerText.Equals("Modificar") && LblCodigoCap.Text != "")
                manejo_bloqueo("E", LblCodigoCap.Text);

            //Cierra el modal de Agregar
            Modal.Cerrar(this, registroAdicion.ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            Listar();
        }
        ///// Eventos Nuevos para la Implementracion del UserControl

        /// <summary>
        /// Metodo del Link Nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNuevo(object sender, EventArgs e)
        {
            lblRegistroAdicion.InnerText = "Agregar";
            LblCodigoCap.Text = "Automático";
            TxtOperacion.Text = string.Empty;

            imbCrear.Visible = true;
            TxtCodigoCap.Visible = false;
            LblCodigoCap.Visible = true;
            TxtOperacion.Enabled = true;

            //Abre el modal de Agregar
            Modal.Abrir(this, registroAdicion.ID, registroAdicionInside.ID);
        }

        /// <summary>
        /// Metodo del Link Listar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnListar(object sender, EventArgs e)
        {
            Listar();
        }

        /// <summary>
        /// Metodo del Link Consultar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsulta(object sender, EventArgs e)
        {
            Buscar();
        }
    }
}