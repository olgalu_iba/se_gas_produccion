﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;

public partial class Verificacion_frm_PubIndicadoresCregMP : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Publicación de indicadores CREG";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlIndicador, "m_indicadores_creg", " tipo_mercado ='P' and destino_rueda ='G' order by codigo_indicador ", 0, 1);
            lConexion.Cerrar();
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnPrevia_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF;
        if (ddlIndicador.SelectedValue == "0")
            lblMensaje.Text += "Debe seleccionar el indicador. <br>";
        if (ddlVista.SelectedValue == "0")
            lblMensaje.Text += "Debe seleccionar la vista. <br>";
        if (ddlIndicador.SelectedValue != "7")
        {
            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial. <br>";
                }
            }
            else
                lblMensaje.Text += "Debe digitar la Fecha Inicial. <br>";
            if (TxtFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
                    if (TxtFechaIni.Text.Trim().Length > 0 && ldFechaF < ldFechaI)
                        lblMensaje.Text += "La Fecha Final NO puede ser Menor que la Fecha Inicial. <br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Formato Inválido en el Campo Fecha Final. <br>";
                }
            }
            else
                lblMensaje.Text += "Debe digitar la Fecha Final. <br>";
        }

        if (lblMensaje.Text == "")
        {
            try
            {
                InactivarGrilla();

                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_SetIndCregMP";
                lComando.Parameters.Add("@P_codigo_indicador", SqlDbType.Int).Value = ddlIndicador.SelectedValue;
                lComando.Parameters.Add("@P_tipo_desagregacion", SqlDbType.Char).Value = ddlVista.SelectedValue;
                lComando.Parameters.Add("@P_tipo_publicacion", SqlDbType.VarChar).Value = "P";
                lComando.Parameters.Add("@P_fecha_inicial", SqlDbType.VarChar).Value = TxtFechaIni.Text;
                lComando.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaFin.Text;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                if (ddlIndicador.SelectedValue == "1")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador1_N.DataSource = lds;
                            dtgIndicador1_N.DataBind();
                            dtgIndicador1_N.Visible = true;
                            break;
                        case "F": dtgIndicador1_F.DataSource = lds;
                            dtgIndicador1_F.DataBind();
                            dtgIndicador1_F.Visible = true;
                            break;
                        case "P": dtgIndicador1_P.DataSource = lds;
                            dtgIndicador1_P.DataBind();
                            dtgIndicador1_P.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "2")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador2_N.DataSource = lds;
                            dtgIndicador2_N.DataBind();
                            dtgIndicador2_N.Visible = true;
                            break;
                        case "P": dtgIndicador2_P.DataSource = lds;
                            dtgIndicador2_P.DataBind();
                            dtgIndicador2_P.Visible = true;
                            break;
                        case "F": dtgIndicador2_F.DataSource = lds;
                            dtgIndicador2_F.DataBind();
                            dtgIndicador2_F.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "3")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador3_N.DataSource = lds;
                            dtgIndicador3_N.DataBind();
                            dtgIndicador3_N.Visible = true;
                            break;
                        case "P": dtgIndicador3_P.DataSource = lds;
                            dtgIndicador3_P.DataBind();
                            dtgIndicador3_P.Visible = true;
                            break;
                        case "F": dtgIndicador3_F.DataSource = lds;
                            dtgIndicador3_F.DataBind();
                            dtgIndicador3_F.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "4")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador4_N.DataSource = lds;
                            dtgIndicador4_N.DataBind();
                            dtgIndicador4_N.Visible = true;
                            break;
                        case "P": dtgIndicador4_P.DataSource = lds;
                            dtgIndicador4_P.DataBind();
                            dtgIndicador4_P.Visible = true;
                            break;
                        case "F": dtgIndicador4_F.DataSource = lds;
                            dtgIndicador4_F.DataBind();
                            dtgIndicador4_F.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "5")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador5_N.DataSource = lds;
                            dtgIndicador5_N.DataBind();
                            dtgIndicador5_N.Visible = true;
                            break;
                        case "P": dtgIndicador5_P.DataSource = lds;
                            dtgIndicador5_P.DataBind();
                            dtgIndicador5_P.Visible = true;
                            break;
                        case "F": dtgIndicador5_F.DataSource = lds;
                            dtgIndicador5_F.DataBind();
                            dtgIndicador5_F.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "6")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador6_N.DataSource = lds;
                            dtgIndicador6_N.DataBind();
                            dtgIndicador6_N.Visible = true;
                            break;
                        case "P": dtgIndicador6_P.DataSource = lds;
                            dtgIndicador6_P.DataBind();
                            dtgIndicador6_P.Visible = true;
                            break;
                        case "F": dtgIndicador6_F.DataSource = lds;
                            dtgIndicador6_F.DataBind();
                            dtgIndicador6_F.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "7")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador7_N.DataSource = lds;
                            dtgIndicador7_N.DataBind();
                            dtgIndicador7_N.Visible = true;
                            break;
                        case "P": dtgIndicador7_P.DataSource = lds;
                            dtgIndicador7_P.DataBind();
                            dtgIndicador7_P.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "8")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador8_N.DataSource = lds;
                            dtgIndicador8_N.DataBind();
                            dtgIndicador8_N.Visible = true;
                            break;
                        case "F": dtgIndicador8_F.DataSource = lds;
                            dtgIndicador8_F.DataBind();
                            dtgIndicador8_F.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "9")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador9_N.DataSource = lds;
                            dtgIndicador9_N.DataBind();
                            dtgIndicador9_N.Visible = true;
                            break;
                        case "F": dtgIndicador9_F.DataSource = lds;
                            dtgIndicador9_F.DataBind();
                            dtgIndicador9_F.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "10")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador10_N.DataSource = lds;
                            dtgIndicador10_N.DataBind();
                            dtgIndicador10_N.Visible = true;
                            break;
                        case "F": dtgIndicador10_F.DataSource = lds;
                            dtgIndicador10_F.DataBind();
                            dtgIndicador10_F.Visible = true;
                            break;
                        case "P": dtgIndicador10_P.DataSource = lds;
                            dtgIndicador10_P.DataBind();
                            dtgIndicador10_P.Visible = true;
                            break;
                        case "S": dtgIndicador10_S.DataSource = lds;
                            dtgIndicador10_S.DataBind();
                            dtgIndicador10_S.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "11")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador11_N.DataSource = lds;
                            dtgIndicador11_N.DataBind();
                            dtgIndicador11_N.Visible = true;
                            break;
                        case "F": dtgIndicador11_F.DataSource = lds;
                            dtgIndicador11_F.DataBind();
                            dtgIndicador11_F.Visible = true;
                            break;
                        case "P": dtgIndicador11_P.DataSource = lds;
                            dtgIndicador11_P.DataBind();
                            dtgIndicador11_P.Visible = true;
                            break;
                        case "S": dtgIndicador11_S.DataSource = lds;
                            dtgIndicador11_S.DataBind();
                            dtgIndicador11_S.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "12")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador12_N.DataSource = lds;
                            dtgIndicador12_N.DataBind();
                            dtgIndicador12_N.Visible = true;
                            break;
                        case "P": dtgIndicador12_P.DataSource = lds;
                            dtgIndicador12_P.DataBind();
                            dtgIndicador12_P.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "13")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador13_N.DataSource = lds;
                            dtgIndicador13_N.DataBind();
                            dtgIndicador13_N.Visible = true;
                            break;
                        case "P": dtgIndicador13_P.DataSource = lds;
                            dtgIndicador13_P.DataBind();
                            dtgIndicador13_P.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "14")
                {
                    dtgIndicador14.DataSource = lds;
                    dtgIndicador14.DataBind();
                    dtgIndicador14.Visible = true;
                }
                if (ddlIndicador.SelectedValue == "15")
                {
                    dtgIndicador15.DataSource = lds;
                    dtgIndicador15.DataBind();
                    dtgIndicador15.Visible = true;
                }
                if (ddlIndicador.SelectedValue == "16")
                {
                    dtgIndicador16.DataSource = lds;
                    dtgIndicador16.DataBind();
                    dtgIndicador16.Visible = true;
                }
                if (ddlIndicador.SelectedValue == "17")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador17_N.DataSource = lds;
                            dtgIndicador17_N.DataBind();
                            dtgIndicador17_N.Visible = true;
                            break;
                        case "P": dtgIndicador17_P.DataSource = lds;
                            dtgIndicador17_P.DataBind();
                            dtgIndicador17_P.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "18")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador18_N.DataSource = lds;
                            dtgIndicador18_N.DataBind();
                            dtgIndicador18_N.Visible = true;
                            break;
                        case "P": dtgIndicador18_P.DataSource = lds;
                            dtgIndicador18_P.DataBind();
                            dtgIndicador18_P.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "19")
                {
                    dtgIndicador19.DataSource = lds;
                    dtgIndicador19.DataBind();
                    dtgIndicador19.Visible = true;
                }
                if (ddlIndicador.SelectedValue == "20")
                {
                    dtgIndicador20.DataSource = lds;
                    dtgIndicador20.DataBind();
                    dtgIndicador20.Visible = true;
                }
                if (ddlIndicador.SelectedValue == "21")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "F": dtgIndicador21_F.DataSource = lds;
                            dtgIndicador21_F.DataBind();
                            dtgIndicador21_F.Visible = true;
                            break;
                        case "P": dtgIndicador21_P.DataSource = lds;
                            dtgIndicador21_P.DataBind();
                            dtgIndicador21_P.Visible = true;
                            break;
                        case "A": dtgIndicador21_A.DataSource = lds;
                            dtgIndicador21_A.DataBind();
                            dtgIndicador21_A.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "22")
                {
                    dtgIndicador22.DataSource = lds;
                    dtgIndicador22.DataBind();
                    dtgIndicador22.Visible = true;
                }
                if (ddlIndicador.SelectedValue == "23")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador23_N.DataSource = lds;
                            dtgIndicador23_N.DataBind();
                            dtgIndicador23_N.Visible = true;
                            break;
                        case "P": dtgIndicador23_P.DataSource = lds;
                            dtgIndicador23_P.DataBind();
                            dtgIndicador23_P.Visible = true;
                            break;
                        case "F": dtgIndicador23_F.DataSource = lds;
                            dtgIndicador23_F.DataBind();
                            dtgIndicador23_F.Visible = true;
                            break;
                        case "C": dtgIndicador23_C.DataSource = lds;
                            dtgIndicador23_C.DataBind();
                            dtgIndicador23_C.Visible = true;
                            break;
                        case "D": dtgIndicador23_D.DataSource = lds;
                            dtgIndicador23_D.DataBind();
                            dtgIndicador23_D.Visible = true;
                            break;
                        case "S": dtgIndicador23_S.DataSource = lds;
                            dtgIndicador23_S.DataBind();
                            dtgIndicador23_S.Visible = true;
                            break;
                        case "T": dtgIndicador23_T.DataSource = lds;
                            dtgIndicador23_T.DataBind();
                            dtgIndicador23_T.Visible = true;
                            break;
                    }
                }
                imbExcel.Visible = true;
                lConexion.Cerrar();
                if (VerificarExistencia("m_indicadores_creg", "codigo_indicador = " + ddlIndicador.SelectedValue + " and fecha_publicacion_defi<getdate() and year(fecha_publicacion_defi) = year(getdate())"))
                {
                    btnDefinitiva.Visible = true;
                    trObs.Visible = true;
                    TxtObs.Text = "";
                }

            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo consultar el indicador.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: btnDefinitiva_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDefinitiva_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF;
        if (ddlIndicador.SelectedValue == "0")
            lblMensaje.Text += "Debe seleccionar el indicador. <br>";
        if (ddlVista.SelectedValue == "0")
            lblMensaje.Text += "Debe seleccionar la vista. <br>";
        if (ddlIndicador.SelectedValue != "7")
        {
            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial. <br>";
                }
            }
            else
                lblMensaje.Text += "Debe digitar la Fecha Inicial. <br>";
            if (TxtFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
                    if (TxtFechaIni.Text.Trim().Length > 0 && ldFechaF < ldFechaI)
                        lblMensaje.Text += "La Fecha Final NO puede ser Menor que la Fecha Inicial. <br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Formato Inválido en el Campo Fecha Final. <br>";
                }
            }
            else
                lblMensaje.Text += "Debe digitar la Fecha Final. <br>";
        }
        if (TxtObs.Text =="")
            lblMensaje.Text += "Debe digitar las observaciones de publicación. <br>";
        if (lblMensaje.Text == "")
        {
            try
            {
                InactivarGrilla();

                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_SetIndCregMP";
                lComando.Parameters.Add("@P_codigo_indicador", SqlDbType.Int).Value = ddlIndicador.SelectedValue;
                lComando.Parameters.Add("@P_tipo_desagregacion", SqlDbType.Char).Value = ddlVista.SelectedValue;
                lComando.Parameters.Add("@P_tipo_publicacion", SqlDbType.VarChar).Value = "D";
                lComando.Parameters.Add("@P_fecha_inicial", SqlDbType.VarChar).Value = TxtFechaIni.Text;
                lComando.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaFin.Text;
                lComando.Parameters.Add("@P_observacion", SqlDbType.VarChar).Value = TxtObs.Text; 
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                if (ddlIndicador.SelectedValue == "1")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador1_N.DataSource = lds;
                            dtgIndicador1_N.DataBind();
                            dtgIndicador1_N.Visible = true;
                            break;
                        case "F": dtgIndicador1_F.DataSource = lds;
                            dtgIndicador1_F.DataBind();
                            dtgIndicador1_F.Visible = true;
                            break;
                        case "P": dtgIndicador1_P.DataSource = lds;
                            dtgIndicador1_P.DataBind();
                            dtgIndicador1_P.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "2")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador2_N.DataSource = lds;
                            dtgIndicador2_N.DataBind();
                            dtgIndicador2_N.Visible = true;
                            break;
                        case "P": dtgIndicador2_P.DataSource = lds;
                            dtgIndicador2_P.DataBind();
                            dtgIndicador2_P.Visible = true;
                            break;
                        case "F": dtgIndicador2_F.DataSource = lds;
                            dtgIndicador2_F.DataBind();
                            dtgIndicador2_F.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "3")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador3_N.DataSource = lds;
                            dtgIndicador3_N.DataBind();
                            dtgIndicador3_N.Visible = true;
                            break;
                        case "P": dtgIndicador3_P.DataSource = lds;
                            dtgIndicador3_P.DataBind();
                            dtgIndicador3_P.Visible = true;
                            break;
                        case "F": dtgIndicador3_F.DataSource = lds;
                            dtgIndicador3_F.DataBind();
                            dtgIndicador3_F.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "4")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador4_N.DataSource = lds;
                            dtgIndicador4_N.DataBind();
                            dtgIndicador4_N.Visible = true;
                            break;
                        case "P": dtgIndicador4_P.DataSource = lds;
                            dtgIndicador4_P.DataBind();
                            dtgIndicador4_P.Visible = true;
                            break;
                        case "F": dtgIndicador4_F.DataSource = lds;
                            dtgIndicador4_F.DataBind();
                            dtgIndicador4_F.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "5")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador5_N.DataSource = lds;
                            dtgIndicador5_N.DataBind();
                            dtgIndicador5_N.Visible = true;
                            break;
                        case "P": dtgIndicador5_P.DataSource = lds;
                            dtgIndicador5_P.DataBind();
                            dtgIndicador5_P.Visible = true;
                            break;
                        case "F": dtgIndicador5_F.DataSource = lds;
                            dtgIndicador5_F.DataBind();
                            dtgIndicador5_F.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "6")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador6_N.DataSource = lds;
                            dtgIndicador6_N.DataBind();
                            dtgIndicador6_N.Visible = true;
                            break;
                        case "P": dtgIndicador6_P.DataSource = lds;
                            dtgIndicador6_P.DataBind();
                            dtgIndicador6_P.Visible = true;
                            break;
                        case "F": dtgIndicador6_F.DataSource = lds;
                            dtgIndicador6_F.DataBind();
                            dtgIndicador6_F.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "7")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador7_N.DataSource = lds;
                            dtgIndicador7_N.DataBind();
                            dtgIndicador7_N.Visible = true;
                            break;
                        case "P": dtgIndicador7_P.DataSource = lds;
                            dtgIndicador7_P.DataBind();
                            dtgIndicador7_P.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "8")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador8_N.DataSource = lds;
                            dtgIndicador8_N.DataBind();
                            dtgIndicador8_N.Visible = true;
                            break;
                        case "F": dtgIndicador8_F.DataSource = lds;
                            dtgIndicador8_F.DataBind();
                            dtgIndicador8_F.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "9")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador9_N.DataSource = lds;
                            dtgIndicador9_N.DataBind();
                            dtgIndicador9_N.Visible = true;
                            break;
                        case "F": dtgIndicador9_F.DataSource = lds;
                            dtgIndicador9_F.DataBind();
                            dtgIndicador9_F.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "10")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador10_N.DataSource = lds;
                            dtgIndicador10_N.DataBind();
                            dtgIndicador10_N.Visible = true;
                            break;
                        case "F": dtgIndicador10_F.DataSource = lds;
                            dtgIndicador10_F.DataBind();
                            dtgIndicador10_F.Visible = true;
                            break;
                        case "P": dtgIndicador10_P.DataSource = lds;
                            dtgIndicador10_P.DataBind();
                            dtgIndicador10_P.Visible = true;
                            break;
                        case "S": dtgIndicador10_S.DataSource = lds;
                            dtgIndicador10_S.DataBind();
                            dtgIndicador10_S.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "11")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador11_N.DataSource = lds;
                            dtgIndicador11_N.DataBind();
                            dtgIndicador11_N.Visible = true;
                            break;
                        case "F": dtgIndicador11_F.DataSource = lds;
                            dtgIndicador11_F.DataBind();
                            dtgIndicador11_F.Visible = true;
                            break;
                        case "P": dtgIndicador11_P.DataSource = lds;
                            dtgIndicador11_P.DataBind();
                            dtgIndicador11_P.Visible = true;
                            break;
                        case "S": dtgIndicador11_S.DataSource = lds;
                            dtgIndicador11_S.DataBind();
                            dtgIndicador11_S.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "12")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador12_N.DataSource = lds;
                            dtgIndicador12_N.DataBind();
                            dtgIndicador12_N.Visible = true;
                            break;
                        case "P": dtgIndicador12_P.DataSource = lds;
                            dtgIndicador12_P.DataBind();
                            dtgIndicador12_P.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "13")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador13_N.DataSource = lds;
                            dtgIndicador13_N.DataBind();
                            dtgIndicador13_N.Visible = true;
                            break;
                        case "P": dtgIndicador13_P.DataSource = lds;
                            dtgIndicador13_P.DataBind();
                            dtgIndicador13_P.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "14")
                {
                    dtgIndicador14.DataSource = lds;
                    dtgIndicador14.DataBind();
                    dtgIndicador14.Visible = true;
                }
                if (ddlIndicador.SelectedValue == "15")
                {
                    dtgIndicador15.DataSource = lds;
                    dtgIndicador15.DataBind();
                    dtgIndicador15.Visible = true;
                }
                if (ddlIndicador.SelectedValue == "16")
                {
                    dtgIndicador16.DataSource = lds;
                    dtgIndicador16.DataBind();
                    dtgIndicador16.Visible = true;
                }
                if (ddlIndicador.SelectedValue == "17")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador17_N.DataSource = lds;
                            dtgIndicador17_N.DataBind();
                            dtgIndicador17_N.Visible = true;
                            break;
                        case "P": dtgIndicador17_P.DataSource = lds;
                            dtgIndicador17_P.DataBind();
                            dtgIndicador17_P.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "18")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador18_N.DataSource = lds;
                            dtgIndicador18_N.DataBind();
                            dtgIndicador18_N.Visible = true;
                            break;
                        case "P": dtgIndicador18_P.DataSource = lds;
                            dtgIndicador18_P.DataBind();
                            dtgIndicador18_P.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "19")
                {
                    dtgIndicador19.DataSource = lds;
                    dtgIndicador19.DataBind();
                    dtgIndicador19.Visible = true;
                }
                if (ddlIndicador.SelectedValue == "20")
                {
                    dtgIndicador20.DataSource = lds;
                    dtgIndicador20.DataBind();
                    dtgIndicador20.Visible = true;
                }
                if (ddlIndicador.SelectedValue == "21")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "F": dtgIndicador21_F.DataSource = lds;
                            dtgIndicador21_F.DataBind();
                            dtgIndicador21_F.Visible = true;
                            break;
                        case "P": dtgIndicador21_P.DataSource = lds;
                            dtgIndicador21_P.DataBind();
                            dtgIndicador21_P.Visible = true;
                            break;
                        case "A": dtgIndicador21_A.DataSource = lds;
                            dtgIndicador21_A.DataBind();
                            dtgIndicador21_A.Visible = true;
                            break;
                    }
                }
                if (ddlIndicador.SelectedValue == "22")
                {
                    dtgIndicador22.DataSource = lds;
                    dtgIndicador22.DataBind();
                    dtgIndicador22.Visible = true;
                }
                if (ddlIndicador.SelectedValue == "23")
                {
                    switch (ddlVista.SelectedValue)
                    {
                        case "N": dtgIndicador23_N.DataSource = lds;
                            dtgIndicador23_N.DataBind();
                            dtgIndicador23_N.Visible = true;
                            break;
                        case "P": dtgIndicador23_P.DataSource = lds;
                            dtgIndicador23_P.DataBind();
                            dtgIndicador23_P.Visible = true;
                            break;
                        case "F": dtgIndicador23_F.DataSource = lds;
                            dtgIndicador23_F.DataBind();
                            dtgIndicador23_F.Visible = true;
                            break;
                        case "C": dtgIndicador23_C.DataSource = lds;
                            dtgIndicador23_C.DataBind();
                            dtgIndicador23_C.Visible = true;
                            break;
                        case "D": dtgIndicador23_D.DataSource = lds;
                            dtgIndicador23_D.DataBind();
                            dtgIndicador23_D.Visible = true;
                            break;
                        case "S": dtgIndicador23_S.DataSource = lds;
                            dtgIndicador23_S.DataBind();
                            dtgIndicador23_S.Visible = true;
                            break;
                        case "T": dtgIndicador23_T.DataSource = lds;
                            dtgIndicador23_T.DataBind();
                            dtgIndicador23_T.Visible = true;
                            break;
                    }
                } imbExcel.Visible = true;
                lConexion.Cerrar();
                lblMensaje.Text = "Indicador publicado correctamente";
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo publicar el indicador.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfIndicador" + DateTime.Now + ".xls";
        string lstitulo_informe = "Consulta de " + ddlIndicador.SelectedItem.ToString() + " - Vista: " + ddlVista.SelectedItem.ToString();
        string lsTituloParametros = "";
        try
        {
            if (TxtFechaIni.Text.Trim().Length > 0)
                lsTituloParametros += " - Fecha Inicial: " + TxtFechaIni.Text.Trim();
            if (TxtFechaFin.Text.Trim().Length > 0)
                lsTituloParametros += " - Fecha Final: " + TxtFechaFin.Text.Trim();

            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            if (ddlIndicador.SelectedValue == "1")
            {
                switch (ddlVista.SelectedValue)
                {
                    case "N": dtgIndicador1_N.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador1_N);
                        break;
                    case "F": dtgIndicador1_F.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador1_F);
                        break;
                    case "P": dtgIndicador1_P.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador1_P);
                        break;
                }
            }
            if (ddlIndicador.SelectedValue == "2")
            {
                switch (ddlVista.SelectedValue)
                {
                    case "N": dtgIndicador2_N.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador2_N);
                        break;
                    case "F": dtgIndicador2_F.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador2_F);
                        break;
                    case "P": dtgIndicador2_P.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador2_P);
                        break;
                }
            }
            if (ddlIndicador.SelectedValue == "3")
            {
                switch (ddlVista.SelectedValue)
                {
                    case "N": dtgIndicador3_N.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador3_N);
                        break;
                    case "P": dtgIndicador3_P.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador3_P);
                        break;
                    case "F": dtgIndicador3_F.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador3_F);
                        break;
                }
            }
            if (ddlIndicador.SelectedValue == "4")
            {
                switch (ddlVista.SelectedValue)
                {
                    case "N": dtgIndicador4_N.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador4_N);
                        break;
                    case "P": dtgIndicador4_P.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador4_P);
                        break;
                    case "F": dtgIndicador4_F.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador4_F);
                        break;
                }
            }
            if (ddlIndicador.SelectedValue == "5")
            {
                switch (ddlVista.SelectedValue)
                {
                    case "N": dtgIndicador5_N.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador5_N);
                        break;
                    case "P": dtgIndicador5_P.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador5_P);
                        break;
                    case "F": dtgIndicador5_F.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador5_F);
                        break;
                }
            }
            if (ddlIndicador.SelectedValue == "6")
            {
                switch (ddlVista.SelectedValue)
                {
                    case "N": dtgIndicador6_N.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador6_N);
                        break;
                    case "P": dtgIndicador6_P.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador6_P);
                        break;
                    case "F": dtgIndicador6_F.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador6_F);
                        break;
                }
            }
            if (ddlIndicador.SelectedValue == "7")
            {
                switch (ddlVista.SelectedValue)
                {
                    case "N": dtgIndicador7_N.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador7_N);
                        break;
                    case "P": dtgIndicador7_P.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador7_P);
                        break;
                }
            }
            if (ddlIndicador.SelectedValue == "8")
            {
                switch (ddlVista.SelectedValue)
                {
                    case "N": dtgIndicador8_N.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador8_N);
                        break;
                    case "F": dtgIndicador8_F.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador8_F);
                        break;
                }
            }
            if (ddlIndicador.SelectedValue == "9")
            {
                switch (ddlVista.SelectedValue)
                {
                    case "N": dtgIndicador9_N.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador9_N);
                        break;
                    case "F": dtgIndicador9_F.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador9_F);
                        break;
                }
            }
            if (ddlIndicador.SelectedValue == "10")
            {
                switch (ddlVista.SelectedValue)
                {
                    case "N": dtgIndicador10_N.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador10_N);
                        break;
                    case "F": dtgIndicador10_F.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador10_F);
                        break;
                    case "P": dtgIndicador10_P.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador10_P);
                        break;
                    case "S": dtgIndicador10_S.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador10_S);
                        break;
                }
            }
            if (ddlIndicador.SelectedValue == "11")
            {
                switch (ddlVista.SelectedValue)
                {
                    case "N": dtgIndicador11_N.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador11_N);
                        break;
                    case "F": dtgIndicador11_F.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador11_F);
                        break;
                    case "P": dtgIndicador11_P.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador11_P);
                        break;
                    case "S": dtgIndicador11_S.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador11_S);
                        break;
                }
            }
            if (ddlIndicador.SelectedValue == "12")
            {
                switch (ddlVista.SelectedValue)
                {
                    case "N": dtgIndicador12_N.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador12_N);
                        break;
                    case "P": dtgIndicador12_P.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador12_P);
                        break;
                }
            }
            if (ddlIndicador.SelectedValue == "13")
            {
                switch (ddlVista.SelectedValue)
                {
                    case "N": dtgIndicador13_N.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador13_N);
                        break;
                    case "P": dtgIndicador13_P.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador13_P);
                        break;
                }
            }
            if (ddlIndicador.SelectedValue == "14")
            {
                dtgIndicador14.EnableViewState = false;
                lform.Controls.Add(dtgIndicador14);
            }
            if (ddlIndicador.SelectedValue == "15")
            {
                dtgIndicador15.EnableViewState = false;
                lform.Controls.Add(dtgIndicador15);
            }
            if (ddlIndicador.SelectedValue == "16")
            {
                dtgIndicador16.EnableViewState = false;
                lform.Controls.Add(dtgIndicador16);
            }
            if (ddlIndicador.SelectedValue == "17")
            {
                switch (ddlVista.SelectedValue)
                {
                    case "N": dtgIndicador17_N.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador17_N);
                        break;
                    case "P": dtgIndicador17_P.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador17_P);
                        break;
                }
            }
            if (ddlIndicador.SelectedValue == "18")
            {
                switch (ddlVista.SelectedValue)
                {
                    case "N": dtgIndicador18_N.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador18_N);
                        break;
                    case "P": dtgIndicador18_P.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador18_P);
                        break;
                }
            }
            if (ddlIndicador.SelectedValue == "19")
            {
                dtgIndicador19.EnableViewState = false;
                lform.Controls.Add(dtgIndicador19);
            }
            if (ddlIndicador.SelectedValue == "20")
            {
                dtgIndicador20.EnableViewState = false;
                lform.Controls.Add(dtgIndicador20);
            }
            if (ddlIndicador.SelectedValue == "21")
            {
                switch (ddlVista.SelectedValue)
                {
                    case "F": dtgIndicador21_F.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador21_F);
                        break;
                    case "P": dtgIndicador21_P.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador21_P);
                        break;
                    case "A": dtgIndicador21_A.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador21_A);
                        break;
                }
            }
            if (ddlIndicador.SelectedValue == "22")
            {
                dtgIndicador22.EnableViewState = false;
                lform.Controls.Add(dtgIndicador22);
            }
            if (ddlIndicador.SelectedValue == "23")
            {
                switch (ddlVista.SelectedValue)
                {
                    case "N": dtgIndicador23_N.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador23_N);
                        break;
                    case "F": dtgIndicador23_F.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador23_F);
                        break;
                    case "P": dtgIndicador23_P.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador23_P);
                        break;
                    case "C": dtgIndicador23_C.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador23_C);
                        break;
                    case "D": dtgIndicador23_D.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador23_D);
                        break;
                    case "S": dtgIndicador23_S.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador23_S);
                        break;
                    case "T": dtgIndicador23_T.EnableViewState = false;
                        lform.Controls.Add(dtgIndicador23_T);
                        break;
                }
            }
            lpagina.RenderControl(lhtw);
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parámetros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> El cálculo del indicador se realiza con la siguiente fórmula: </font></th><td></td></tr></table><br>");
            switch (ddlIndicador.SelectedValue)
            {
                case "1":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> PTDV </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> PP </font></th><td></td></tr></table>");
                    break;
                case "2":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> PTDVF + CIDVF </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> PTDV + CIDV </font></th><td></td></tr></table>");
                    break;
                case "3":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> PTDVF </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> PP </font></th><td></td></tr></table>");
                    break;
                case "4":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OCF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> PTDV + CIDV </font></th><td></td></tr></table>");
                    break;
                case "5":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OCF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> PTDVF + CIDVF </font></th><td></td></tr></table>");
                    break;
                case "6":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;OCF&nbsp;&nbsp;</font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> PP </font></th><td></td></tr></table>");
                    break;
                case "7":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;CF&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> C </font></th><td></td></tr></table>");
                    break;
                case "8":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> PTDVF + CIDVF </font></th><td></td></tr></table>");
                    break;
                case "9":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;CF&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> OCF </font></th><td></td></tr></table>");
                    break;
                case "10":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> PTDVF + CIDVF </font></th><td></td></tr></table>");
                    break;
                case "11":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;CF&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> OCF </font></th><td></td></tr></table>");
                    break;
                case "12":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;CF&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> C </font></th><td></td></tr></table>");
                    break;
                case "13":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;CF&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> C </font></th><td></td></tr></table>");
                    break;
                case "14":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;CF&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> CF Tot </font></th><td></td></tr></table>");
                    break;
                case "15":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;CF&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> CT </font></th><td></td></tr></table>");
                    break;
                case "16":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;CT&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> CMMP </font></th><td></td></tr></table>");
                    break;
                case "17":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;C&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> C tot </font></th><td></td></tr></table>");
                    break;
                case "18":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;C&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> C tot </font></th><td></td></tr></table>");
                    break;
                case "19":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;CT&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> CMMP </font></th><td></td></tr></table>");
                    break;
                case "20":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;CT&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> CMMP </font></th><td></td></tr></table>");
                    break;
                case "21":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;C&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> OCF </font></th><td></td></tr></table>");
                    break;
                case "22":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;CT&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> CMMP </font></th><td></td></tr></table>");
                    break;
                case "23":
                    Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;Cnt * Pre&nbsp; </font></u></th><td></td></tr></table>");
                    Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> Cnt </font></th><td></td></tr></table>");
                    break;
            }
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Excel.!" + ex.Message.ToString();
        }
    }
    /// <summary>
    /// Nombre: ddlIndicador_SelectedIndexChanged
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>

    protected void ddlIndicador_SelectedIndexChanged(object sender, EventArgs e)
    {
        InactivarGrilla();
        btnPrevia.Visible = false;
        btnDefinitiva.Visible = false;
        //20170705 rq025 indicar creg fase III
        ddlVista.Items.Clear();
        lConexion.Abrir();
        LlenarControles(lConexion.gObjConexion, ddlVista, "m_indicador_vista", " codigo_indicador = " + ddlIndicador.SelectedValue + " and estado ='A'", 2, 3);
        lConexion.Cerrar();

        if (VerificarExistencia("m_indicadores_creg", "codigo_indicador = " + ddlIndicador.SelectedValue + " and fecha_publicacion_prev<getdate() and year(fecha_publicacion_prev) = year(getdate())"))
            btnPrevia.Visible = true;
        trFechaIni.Visible = true;
        trFechaFin.Visible = true;
        trObs.Visible = false;
        TxtObs.Text = "";
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lsTable, string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lsTable, lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: InactivarGrilla
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected void InactivarGrilla()
    {
        dtgIndicador1_N.Visible = false;
        dtgIndicador1_P.Visible = false;
        dtgIndicador1_F.Visible = false;
        dtgIndicador2_N.Visible = false;
        dtgIndicador2_P.Visible = false;
        dtgIndicador2_F.Visible = false;
        dtgIndicador3_N.Visible = false;
        dtgIndicador3_P.Visible = false;
        dtgIndicador3_F.Visible = false;
        dtgIndicador4_N.Visible = false;
        dtgIndicador4_P.Visible = false;
        dtgIndicador4_F.Visible = false;
        dtgIndicador5_N.Visible = false;
        dtgIndicador5_P.Visible = false;
        dtgIndicador5_F.Visible = false;
        dtgIndicador6_N.Visible = false;
        dtgIndicador6_P.Visible = false;
        dtgIndicador6_F.Visible = false;
        dtgIndicador7_N.Visible = false;
        dtgIndicador7_P.Visible = false;
        dtgIndicador8_N.Visible = false;
        dtgIndicador8_F.Visible = false;
        dtgIndicador9_N.Visible = false;
        dtgIndicador9_F.Visible = false;
        dtgIndicador10_N.Visible = false;
        dtgIndicador10_F.Visible = false;
        dtgIndicador10_P.Visible = false;
        dtgIndicador10_S.Visible = false;
        dtgIndicador11_N.Visible = false;
        dtgIndicador11_F.Visible = false;
        dtgIndicador11_P.Visible = false;
        dtgIndicador11_S.Visible = false;
        dtgIndicador12_N.Visible = false;
        dtgIndicador12_P.Visible = false;
        dtgIndicador13_N.Visible = false;
        dtgIndicador13_P.Visible = false;
        dtgIndicador14.Visible = false;
        dtgIndicador15.Visible = false;
        dtgIndicador16.Visible = false;
        dtgIndicador17_N.Visible = false;
        dtgIndicador17_P.Visible = false;
        dtgIndicador18_N.Visible = false;
        dtgIndicador18_P.Visible = false;
        dtgIndicador19.Visible = false;
        dtgIndicador20.Visible = false;
        dtgIndicador21_F.Visible = false;
        dtgIndicador21_P.Visible = false;
        dtgIndicador21_A.Visible = false;
        dtgIndicador22.Visible = false;
        dtgIndicador23_N.Visible = false;
        dtgIndicador23_F.Visible = false;
        dtgIndicador23_P.Visible = false;
        dtgIndicador23_C.Visible = false;
        dtgIndicador23_D.Visible = false;
        dtgIndicador23_S.Visible = false;
        dtgIndicador23_T.Visible = false;
    }
    /// <summary>
    /// Nombre: ddlVista_SelectedIndexChanged
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected void ddlVista_SelectedIndexChanged(object sender, EventArgs e)
    {
        InactivarGrilla();
        btnDefinitiva.Visible = false;
        trObs.Visible = false;
        TxtObs.Text = "";
    }
}