﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ejecucionContratoExt.aspx.cs"
    Inherits="Verificacion_frm_ejecucionContratoExt" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">    
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>
            </div>
            <%--Contenido--%>
            <%--Captura--%>
            <div class="kt-portlet__body" runat="server" id="tblDatos">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Número Contrato</label>
                            <asp:TextBox ID="TxtContratoDef" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Número Operación</label>
                            <asp:TextBox ID="TxtNoContrato" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                            <asp:CompareValidator ID="CvTxtNoContrato" runat="server" ControlToValidate="TxtNoContrato"
                                Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Número Operación debe Ser numérico">*</asp:CompareValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Código Ejecución</label>
                            <asp:TextBox ID="TxtCodEjec" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                            <asp:CompareValidator ID="CvTxtCodEjec" runat="server" ControlToValidate="TxtCodEjec"
                                Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Código Ejecució debe Ser numérico">*</asp:CompareValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Id Registro</label>
                            <asp:TextBox ID="TxtCodigoVerif" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                            <asp:CompareValidator ID="CvTxtCodigoVerif" runat="server" ControlToValidate="TxtCodigoVerif"
                                Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Número Id Registro debe Ser numérico">*</asp:CompareValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fecha Registro Ejecución Inicial</label>
                            <asp:TextBox ID="TxtFechaIni" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fecha Registro Ejecución Final</label>
                            <asp:TextBox ID="TxtFechaFin" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Operador</label>
                            <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Estado</label>
                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="btnConsultar" runat="server" CssClass="btn btn-success" Text="Consultar" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="btnConsultar_Click" />
                            <asp:LinkButton ID="lkbExcel" runat="server" CssClass="btn btn-outline-brand btn-square" OnClick="lkbExcel_Click">Exportar Excel</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <%--Grilla--%>
            <div class="kt-portlet__body" runat="server" id="tblGrilla" visible="false">
                <div class="row">
                    <div class="table table-responsive">
                        <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                            Width="100%" CssClass="table-bordered"
                            OnEditCommand="dtgConsulta_EditCommand" AllowPaging="true" PageSize="20" OnPageIndexChanged="dtgConsulta_PageIndexChanged">
                            <Columns>
                                <%--0--%>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha Gas" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <%--1--%>
                                <asp:BoundColumn DataField="desc_subasta" HeaderText="Subasta"></asp:BoundColumn>
                                <%--2--%>
                                <asp:BoundColumn DataField="desc_mercado" HeaderText="Mercado"></asp:BoundColumn>
                                <%--3--%>
                                <asp:BoundColumn DataField="desc_producto" HeaderText="Producto"></asp:BoundColumn>
                                <%--4--%>
                                <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual"></asp:BoundColumn>
                                <%--5--%>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--6--%>
                                <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato Definitivo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--7--%>
                                <asp:BoundColumn DataField="codigo_punto" HeaderText="Código Punto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--8--%>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--9--%>
                                <asp:BoundColumn DataField="operador_compra" HeaderText="Código Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--10--%>
                                <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Comprador" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--11--%>
                                <asp:BoundColumn DataField="operador_venta" HeaderText="Código Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--12--%>
                                <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--13--%>
                                <asp:BoundColumn DataField="cantidad_compra" HeaderText="Cantidad Compra" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <%--14--%>
                                <asp:BoundColumn DataField="cantidad_venta" HeaderText="Cantidad Venta" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <%--15--%>
                                <asp:BoundColumn DataField="Valor_compra" HeaderText="Valor Compra" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                                <%--16--%>
                                <asp:BoundColumn DataField="Valor_venta" HeaderText="Valor Venta" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                                <%--17--%>
                                <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--18--%>
                                <asp:BoundColumn DataField="estado" Visible="false"></asp:BoundColumn>
                                <%--19--%>
                                <asp:BoundColumn DataField="ind_modifica" Visible="false"></asp:BoundColumn>
                                <%--20--%>
                                <asp:BoundColumn DataField="cantidad" Visible="false"></asp:BoundColumn>
                                <%--21--%>
                                <asp:BoundColumn DataField="precio" Visible="false"></asp:BoundColumn>
                                <%--22--%>
                                <asp:BoundColumn DataField="estado" Visible="false"></asp:BoundColumn>
                                <%--23--%>
                                <asp:BoundColumn DataField="punta" Visible="false"></asp:BoundColumn>
                                <%--24--%>
                                <asp:BoundColumn DataField="codigo_verif_contrato" Visible="false"></asp:BoundColumn>
                                <%--25--%>
                                <asp:BoundColumn DataField="codigo_contrato_eje" Visible="false"></asp:BoundColumn>
                                <%--26--%>
                                <asp:EditCommandColumn HeaderText="Agregar" EditText="Agregar"></asp:EditCommandColumn>
                                <%--27--%>
                                <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar"></asp:EditCommandColumn>
                            </Columns>
                            <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                        <%--20190524 rq029-19--%>
                        <asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False" Visible="false"
                            Width="100%" CssClass="table-bordered">
                            <Columns>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha Gas" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_subasta" HeaderText="Subasta"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_mercado" HeaderText="Mercado"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_producto" HeaderText="Producto"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato Definitivo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_punto" HeaderText="Código Punto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_compra" HeaderText="Código Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Comprador" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_venta" HeaderText="Código Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_compra" HeaderText="Cantidad Compra" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_venta" HeaderText="Cantidad Venta" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Valor_compra" HeaderText="Valor Compra" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Valor_venta" HeaderText="Valor Venta" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>
                            <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <%--Registro Contrato--%>
            <div class="kt-portlet__body" runat="server" id="tblRegCnt" visible="false">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Operación</label>
                            <asp:Label ID="lblOperacion" runat="server" CssClass="form-control"></asp:Label>-<asp:Label ID="lblPunta"
                                runat="server" CssClass="form-control"></asp:Label>
                            <asp:HiddenField ID="hdfPunta" runat="server" />
                            <asp:HiddenField ID="hdfCodigoEjec" runat="server" />
                            <asp:HiddenField ID="hdfCantidad" runat="server" />
                            <asp:HiddenField ID="hdfCodDatUsu" runat="server" />
                            <asp:HiddenField ID="hdfCodigoVerif" runat="server" />
                            <%--20190524 rq029-19--%>
                            <asp:HiddenField ID="hdfUltDia" runat="server" />
                            <asp:HiddenField ID="hdfHora" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Número de contrato</label>
                            <asp:Label ID="lblContratoDef" runat="server" CssClass="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Subasta</label>
                            <asp:Label ID="lblSubasta" runat="server" CssClass="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Tipo Mercado</label>
                            <asp:Label ID="lblMercado" runat="server" CssClass="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Producto</label>
                            <asp:Label ID="lblProducto" runat="server" CssClass="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Modalidad Contractual</label>
                            <asp:Label ID="lblModalidad" runat="server" CssClass="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Cantidad</label>
                            <asp:Label ID="lblCantidad" runat="server" CssClass="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Precio del Contrato</label>
                            <asp:Label ID="lblPrecio" runat="server" CssClass="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fecha Gas</label>
                            <asp:Label ID="lblFechaGas" runat="server" CssClass="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Punto de entrega</label>
                            <asp:DropDownList ID="ddlPunto" runat="server" CssClass="form-control selectpicker"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Cantidad Autorizada</label>
                            <asp:TextBox ID="TxtCantidad" runat="server" ValidationGroup="detalle" CssClass="form-control" OnTextChanged="TxtCantidad_TextChanged" AutoPostBack="true"></asp:TextBox>
                            <asp:CompareValidator ID="CvTxtCantidad" runat="server" ControlToValidate="TxtCantidad"
                                Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Cantidad Autorizada debe Ser numérico">El Campo Cantidad Autorizada debe Ser numérico</asp:CompareValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Valor Facturado</label>
                            <asp:TextBox ID="TxtValor" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                            <asp:CompareValidator ID="CvTxtValor" runat="server" ControlToValidate="TxtValor"
                                Type="Double" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo >Valor Facturado debe Ser numérico">El Campo >Valor Facturado debe Ser numérico</asp:CompareValidator>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="btnCrear" runat="server" Text="Ingresar" OnClick="btnCrear_Click"
                                OnClientClick="this.disabled = true;" UseSubmitBehavior="false" ValidationGroup="detalle" CssClass="btn btn-success" />
                            <asp:Button ID="btnActualizar" runat="server" Text="Actualizar" OnClick="btnActualizar_Click"
                                OnClientClick="this.disabled = true;" UseSubmitBehavior="false" ValidationGroup="detalle" CssClass="btn btn-success" />
                            <asp:Button ID="btnRegresar" runat="server" Text="Regresar" OnClick="btnRegresar_Click" class="btn btn-secondary" />
                            <asp:Button ID="btnCrearUsu" runat="server" Text="Crear Usuario" OnClick="btnCrearUsu_Click"
                                Visible="false" CssClass="btn btn-success" />
                            <asp:Button ID="btnActualUsu" runat="server" Text="Actualiza Usuario" OnClick="btnActualUsu_Click"
                                Visible="false" CssClass="btn btn-success" />
                        </div>
                    </div>
                </div>
            </div>
            <%--Detalle Usuarios Finales--%>
            <div class="kt-portlet__body" runat="server" id="tblDemanda" visible="false">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <%--20190524 rq029-19--%>
                            <label>Tipo Demanda</label>
                            <asp:DropDownList ID="ddlDemanda" runat="server" OnSelectedIndexChanged="ddlDemanda_SelectedIndexChanged"
                                AutoPostBack="true" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Sector de Consumo</label>
                            <asp:DropDownList ID="ddlSector" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Punto de Salida</label>
                            <asp:DropDownList ID="ddlSalida" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <%--20190524  fin rq029-19--%>
                            <label>Usuario Final</label>
                            <asp:TextBox ID="TxtUsuarioFinal" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:AutoCompleteExtender runat="server" BehaviorID="AutoCompleteExUsuF" ID="AutoCompleteExtender1"
                                TargetControlID="TxtUsuarioFinal" ServicePath="~/WebService/AutoComplete.asmx"
                                ServiceMethod="GetCompletionListUsuarioFinal" MinimumPrefixLength="3" CompletionInterval="1000"
                                EnableCaching="true" CompletionSetCount="20" CompletionListCssClass="autocomplete_completionListElement"
                                CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                DelimiterCharacters=";,:">
                                <Animations>
                        <OnShow>
                            <Sequence>
                                <OpacityAction Opacity="0" />
                                <HideAction Visible="true" />
                                                                <ScriptAction Script="
                                    // Cache the size and setup the initial size
                                    var behavior = $find('AutoCompleteExUsuF');
                                    if (!behavior._height) {
                                        var target = behavior.get_completionList();
                                        behavior._height = target.offsetHeight - 2;
                                        target.style.height = '0px';
                                    }" />
                                
                                <Parallel Duration=".4">
                                    <FadeIn />
                                    <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteExUsuF')._height" />
                                </Parallel>
                            </Sequence>
                        </OnShow>
                        <OnHide>
                            <Parallel Duration=".4">
                                <FadeOut />
                                <Length PropertyKey="height" StartValueScript="$find('AutoCompleteExUsuF')._height" EndValue="0" />
                            </Parallel>
                        </OnHide>
                                </Animations>
                            </ajaxToolkit:AutoCompleteExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Mercado Relevante</label>
                            <asp:DropDownList ID="ddlMercado" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <%--20190524 rq029-19--%>
                            <label>Cantidad Contratada</label>
                            <asp:TextBox ID="TxtCantidadUsr" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                            <asp:CompareValidator ID="CvTxtCantidadUsr" runat="server" ControlToValidate="TxtCantidadUsr"
                                Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Cantidad Contratada debe Ser numérico">El Campo Cantidad Contratada debe Ser numérico</asp:CompareValidator>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <h3 class="kt-portlet__head-title">
                                <label>LISTADO DE USUARIOS FINALES</label>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <b>TOTAL CANTIDAD:</b><asp:Label ID="lblTotlCantidad" runat="server" ForeColor="Red"
                                Font-Bold="true" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="table table-responsive">
                        <asp:DataGrid ID="dtgUsuarios" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                            Width="100%" CssClass="table-bordered" OnEditCommand="dtgUsuarios_EditCommand">
                            <Columns>
                                <%--0--%>
                                <asp:BoundColumn DataField="codigo_contrato_eje_usr" Visible="false"></asp:BoundColumn>
                                <%--20190524 rq029-19--%>
                                <%--1--%>
                                <asp:BoundColumn DataField="tipo_demanda" HeaderText="Código Tipo Demanda"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--2--%>
                                <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--3--%>
                                <asp:BoundColumn DataField="sector_consumo" HeaderText="Código Sector Consumo"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--4--%>
                                <asp:BoundColumn DataField="desc_sector" HeaderText="Sector consumo"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>

                                <%--5--%>
                                <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Código Punto Salida"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--6--%>
                                <asp:BoundColumn DataField="desc_salida" HeaderText="Punto de Salida"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190524 fin  rq029-19--%>
                                <%--7--%>
                                <asp:BoundColumn DataField="nit_usuario_no_regulado" HeaderText="Identificacion Usuario Final"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--8--%>
                                <asp:BoundColumn DataField="nombre_usuario" HeaderText="Nombre Usuario"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--9--%>
                                <asp:BoundColumn DataField="codigo_mercado_relevante" HeaderText="Código Mercado Relevante" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="80px"></asp:BoundColumn>
                                <%--10--%>
                                <asp:BoundColumn DataField="desc_mercado" HeaderText="Mercado Relevante" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--20190524 rq029-19--%>
                                <%--11--%>
                                <asp:BoundColumn DataField="cantidad_contratada" HeaderText="Cantidad Contratada" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--12--%>
                                <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar"></asp:EditCommandColumn>
                                <%--13--%>
                                <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                </div>
            </div>

            <%--Detalle Puntos--%>
            <div class="kt-portlet__body" runat="server" id="tblDetalle" visible="false">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <h3 class="kt-portlet__head-title">
                                <label>LISTADO PUNTOS INGRESADOS</label>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="table table-responsive">
                        <asp:DataGrid ID="dtgDetalle" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                            Width="100%" CssClass="table-bordered">
                            <Columns>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Operación"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_gas" HeaderText="Fecha Gas" DataFormatString="{0:yyyy/MM/dd}"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_Punto" HeaderText="Código Punto"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrtega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190524 rq029-19--%>
                                <%--<asp:BoundColumn DataField="tipo_demanda" HeaderText="Código Tipo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="sector_consumo" HeaderText="Código Sector Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_sector" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>--%>
                                <%--20190524  FIn rq029-19--%>
                                <asp:BoundColumn DataField="cantidad_autorizada" HeaderText="Cantidad Autorizada" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_facturado" HeaderText="Valor Facturado" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
