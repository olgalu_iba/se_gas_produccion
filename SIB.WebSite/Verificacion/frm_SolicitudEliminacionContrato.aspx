﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_SolicitudEliminacionContrato.aspx.cs"
    Inherits="Verificacion_frm_SolicitudEliminacionContrato" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>
            </div>
            <%--Contenido--%>
            <%--Captura--%>
            <div class="kt-portlet__body" runat="server" id="tblDatos">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Número Operación <%--20180302 rq004-18--%></label>
                            <asp:TextBox ID="TxtNoContrato" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Número Id Registro <%--20180302 rq004-18--%></label>
                            <asp:TextBox ID="TxtNoIdRegistro" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Estado</label>
                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control selectpicker">
                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="I" Text="Pendiente Aprobacion Operador"></asp:ListItem>
                                <asp:ListItem Value="P" Text="Pendiente Aprobacion Bmc"></asp:ListItem>
                                <%--<asp:ListItem Value="A" Text="Aprobados"></asp:ListItem>--%> <%--20170906 ajuste--%>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Operador</label>
                            <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="btnConsultar" runat="server" CssClass="btn btn-success" Text="Consultar" OnClick="btnConsultar_Click" ValidationGroup="detalle" />
                            <%--20170601 rq020-17--%>
                            <asp:Button ID="btnSolicitud" runat="server" CssClass="btn btn-success" Text="Solicitud Eliminación" OnClick="btnSolicitud_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="btnAprobRechaOpera" runat="server" CssClass="btn btn-success" Text="Aprobación Operador" OnClick="btnAprobRechaOpera_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="btnRFechazaOpera" runat="server" CssClass="btn btn-success" Text="Rechaza Operador" OnClick="btnRFechazaOpera_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <%--20170601 rq020-17--%>
                            <asp:Button ID="btnAprobRechaBmc" runat="server" CssClass="btn btn-success" Text="Aprobación BMC" OnClick="btnAprobRechaBmc_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="btnRechzoBmc" runat="server" CssClass="btn btn-success" Text="Rechazo BMC" OnClick="btnRechzoBmc_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:LinkButton ID="lkbExcel" runat="server" CssClass="btn btn-outline-brand btn-square" OnClick="lkbExcel_Click">Exportar Excel</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <%--Mensaje--%>
            <div class="kt-portlet__body" runat="server" id="tblMensaje">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <%--Grilla--%>
            <div class="kt-portlet__body" runat="server" id="tblGrilla" visible="false">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Archivo Pdf</label>
                            <asp:FileUpload ID="FuArchivo" BackColor="#dddddd" runat="server" EnableTheming="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Observacion</label>
                            <asp:TextBox ID="TxtObservacion" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="table table-responsive">
                        <div style="overflow: scroll; height: 450px;">
                            <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                Width="100%" CssClass="table-bordered" OnEditCommand="dtgConsulta_EditCommand">
                                <Columns>
                                    <%--0--%>
                                    <asp:TemplateColumn HeaderText="Sele.">
                                        <ItemTemplate>
                                            <label class="kt-checkbox">
                                                <asp:CheckBox ID="chkRecibir" runat="server" />
                                                <span></span>
                                            </label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--1--%>
                                    <asp:BoundColumn DataField="numero_operacion" HeaderText="No. Operacion" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--2--%>
                                    <asp:BoundColumn DataField="numero_id_registro" HeaderText="No. Registro"></asp:BoundColumn>
                                    <%--3--%>
                                    <asp:BoundColumn DataField="tipo_subasta" HeaderText="Tipo Subasta" ItemStyle-HorizontalAlign="Left"
                                        Visible="false"></asp:BoundColumn>
                                    <%--4--%>
                                    <asp:BoundColumn DataField="mercado" HeaderText="Mercado" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="80px"></asp:BoundColumn>
                                    <%--5--%>
                                    <asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--6--%>
                                    <asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--7--%>
                                    <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--8--%>
                                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--9--%>
                                    <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--10--%>
                                    <asp:BoundColumn DataField="codigo_solicitud" HeaderText="No. Solicitud" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--11--%>
                                    <asp:BoundColumn DataField="observacion_compra" HeaderText="Observacion Compra" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--12--%>
                                    <asp:BoundColumn DataField="observacion_venta" HeaderText="Observacion Venta" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--13--%>
                                    <asp:BoundColumn DataField="aprobado_bmc" HeaderText="Aprobado BMC" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--14--%>
                                    <asp:BoundColumn DataField="aprobado_operador" HeaderText="Aprobado Operador" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--15--%>
                                    <asp:BoundColumn DataField="observacion_bmc" HeaderText="Observacion BMC" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--16--%>
                                    <asp:BoundColumn DataField="operador_compra" Visible="false"></asp:BoundColumn>
                                    <%--17--%>
                                    <asp:BoundColumn DataField="operador_venta" Visible="false"></asp:BoundColumn>
                                    <%--18--%>
                                    <asp:BoundColumn DataField="operador_solicitud" Visible="false"></asp:BoundColumn>
                                    <%--19--%>
                                    <asp:BoundColumn DataField="pdf_compra" Visible="false"></asp:BoundColumn>
                                    <%--20--%>
                                    <asp:BoundColumn DataField="pdf_venta" Visible="false"></asp:BoundColumn>
                                    <%--21--%>
                                    <asp:EditCommandColumn HeaderText="Pdf Compra" EditText="Pdf_C"></asp:EditCommandColumn>
                                    <%--22--%>
                                    <asp:EditCommandColumn HeaderText="Pdf Venta" EditText="Pdf_V"></asp:EditCommandColumn>
                                    <%--23--%>
                                    <asp:BoundColumn DataField="sigla_estado" Visible="false"></asp:BoundColumn>
                                    <%--24--%><%--20170601 rq020-17--%>
                                    <asp:BoundColumn DataField="cnt_reg" Visible="false"></asp:BoundColumn>
                                    <%--20180126 rq107-16--%>
                                    <asp:BoundColumn DataField="desc_subasta" HeaderText="Tipo Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
