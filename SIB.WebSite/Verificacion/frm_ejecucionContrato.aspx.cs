﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Verificacion_frm_ejecucionContrato : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Registro de Ejecución de Contratos";
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);
        //Titulo
        Master.Titulo = "Registro de Ejecución de Contratos";

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles1(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador !=0 order by codigo_operador", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlPunto, "m_pozo", " estado = 'A'  order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMercado, "m_mercado_relevante", " estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlDemanda, "m_tipo_demanda_atender", " estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo sec ", " sec.estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlSubasta, "m_tipos_subasta tpo", " tpo.estado ='A' and exists (select 1 from m_ejecucion_modalidad eje where tpo.codigo_tipo_subasta = eje.codigo_tipo_subasta and eje.estado ='A') order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlTpoMercado, "m_tipo_mercado tpo", "  exists (select 1 from m_ejecucion_modalidad eje where tpo.codigo_tipo_mercado = eje.tipo_mercado and eje.estado ='A') order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlProducto, "m_producto prd", " prd.estado ='A' and exists (select 1 from m_ejecucion_modalidad eje where prd.sigla = eje.destino_rueda and eje.estado ='A') order by descripcion", 6, 1);
            LlenarControles(lConexion.gObjConexion, ddlModalidad, "m_modalidad_contractual mod", " mod.estado ='A' and exists (select 1 from m_ejecucion_modalidad eje where mod.codigo_modalidad= eje.codigo_modalidad and eje.estado ='A') order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlSalida, "m_punto_salida_snt pto", " pto.estado ='A' order by descripcion", 0, 2); //20190524 rq029-19

            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
            }
        }
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// Nombre: dtgConsulta_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgConsulta.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        lblMensaje.Text = "";
        DateTime ldFecha;
        int liValor = 0;
        string[] lsNombreParametros = { "@P_numero_contrato_ini", "@P_numero_contrato_fin", "@P_contrato_definitivo", "@P_codigo_operador", "@P_tipo_subasta", "@P_tipo_mercado", "@P_destino_rueda", "@P_codigo_modalidad", "@P_fecha_gas_ini", "@P_fecha_gas_fin" }; //20190524 rq029-19
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };//20190524 rq029-19
        string[] lValorParametros = { "0", "0", "", "0", "0", "0", "0", "0", "", "" };//20190524 rq029-19

        //20190524 rq029-19
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF = DateTime.Now;

        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial. <br>";
            }
        }
        else
            lblMensaje.Text += "Debe digitar la Fecha Inicial. <br>";
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                if (TxtFechaIni.Text.Trim().Length > 0)
                {
                    ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
                    if (ldFechaI > ldFechaF)
                        lblMensaje.Text += "La Fecha Final NO puede ser Menor que la Fecha de Inicial. <br>";
                }
                else
                    lblMensaje.Text += "Debe digitar la fecha inicial antes que la final. <br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Final. <br>";
            }
        }
        //20190524 fin rq029-19
        if (TxtNoContrato.Text.Trim().Length > 0)
        {
            try
            {
                liValor = Convert.ToInt32(TxtNoContrato.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo operación inicial. <br>";
            }

        }
        if (TxtNoContratoFin.Text.Trim().Length > 0)
        {
            try
            {
                liValor = Convert.ToInt32(TxtNoContratoFin.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo operación final. <br>";
            }

        }
        if (TxtNoContrato.Text.Trim().Length == 0 && TxtNoContratoFin.Text.Trim().Length > 0)
        {
            lblMensaje.Text += "Debe digitar el No. de contrato inicial antes que el final. <br>";
        }
        if (TxtNoContrato.Text.Trim().Length > 0 && TxtNoContratoFin.Text.Trim().Length > 0)
        {
            try
            {
                if (Convert.ToInt32(TxtNoContratoFin.Text) < Convert.ToInt32(TxtNoContrato.Text))
                    lblMensaje.Text += "El No. de contrato inicial debe ser menor o igual que el final. <br>";
            }
            catch (Exception ex)
            {

            }
        }
        if (lblMensaje.Text == "")
        {
            try
            {
                if (TxtNoContrato.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtNoContrato.Text.Trim();
                    lValorParametros[1] = TxtNoContrato.Text.Trim();
                }
                if (TxtNoContratoFin.Text.Trim().Length > 0)
                    lValorParametros[1] = TxtNoContratoFin.Text.Trim();
                if (TxtContratoDef.Text.Trim().Length > 0)
                    lValorParametros[2] = TxtContratoDef.Text.Trim();
                if (ddlOperador.SelectedValue != "0")
                    lValorParametros[3] = ddlOperador.SelectedValue;
                lValorParametros[4] = ddlSubasta.SelectedValue;
                lValorParametros[5] = ddlTpoMercado.SelectedValue;
                lValorParametros[6] = ddlProducto.SelectedValue;
                lValorParametros[7] = ddlModalidad.SelectedValue;
                //20190524 rq029-19
                lValorParametros[8] = TxtFechaIni.Text.Trim();
                if (TxtFechaFin.Text != "")
                    lValorParametros[9] = TxtFechaFin.Text.Trim();
                else
                    lValorParametros[9] = TxtFechaIni.Text.Trim();
                //20190524 fin rq029-19
                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContEjecucion", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                dtgExcel.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContEjecucion", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgExcel.DataBind();
                lConexion.Cerrar();
                if (dtgConsulta.Items.Count > 0)
                {
                    tblGrilla.Visible = true;
                    //imbExcel.Visible = true;
                }
                else
                {
                    tblGrilla.Visible = false;
                    //imbExcel.Visible = false;
                    lblMensaje.Text = "No se encontraron Registros.";
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        hdfUltDia.Value = e.Item.Cells[32].Text;
        hdfHora.Value = e.Item.Cells[24].Text;
        if (((LinkButton)e.CommandSource).Text == "Ingresar")
        {
            string lsError = "N";
            if (hdfUltDia.Value == "S" && Convert.ToDateTime(hdfHora.Value) < Convert.ToDateTime(DateTime.Now.ToString("HH:mm"))) //20190524 rq029-19
                lblMensaje.Text = "Está fuera del horario para registro de ejecución de contrato";
            if (lblMensaje.Text == "")
            {
                try
                {
                    limpiarDatosUsu();
                    limpiarDatos();
                    ddlPunto.Enabled = true;
                    lblFechaGas.Text = e.Item.Cells[0].Text; //20190524 rq029-19
                    //20190524 rq029-19 cambios de indices
                    lblContratoDef.Text = e.Item.Cells[2].Text;
                    lblOperacion.Text = e.Item.Cells[1].Text;
                    //hdfCantidad.Value = e.Item.Cells[8].Text; //20190524 rq029-19
                    lblCantidad.Text = e.Item.Cells[8].Text;
                    lblPrecio.Text = e.Item.Cells[9].Text;
                    hdfPunta.Value = e.Item.Cells[18].Text;
                    hdfCodigoVerif.Value = e.Item.Cells[19].Text;
                    lblSubasta.Text = e.Item.Cells[25].Text;
                    lblMercado.Text = e.Item.Cells[26].Text;
                    lblProducto.Text = e.Item.Cells[27].Text;
                    lblModalidad.Text = e.Item.Cells[28].Text;
                    hdfCantidad.Value = "0";
                    hdfCodigoEjec.Value = "0";
                    btnActualizar.Visible = false;
                    btnCrear.Visible = true;
                    CargarDatosDet();
                    lblTotlCantidad.Text = "0";//20190524 rq029-19

                    if (hdfPunta.Value == "C")
                    {

                        //                        hdfPunta.Value = "C";  //20190524 rq029-19
                        lblPunta.Text = "COMPRADOR";
                        //trCompra.Visible = true; //20190524 rq029-19
                        if (lsError == "N")
                            btnCrearUsu.Visible = true;

                        string[] lsNombreParametrosC = { "@P_cadena" };
                        SqlDbType[] lTipoparametrosC = { SqlDbType.VarChar };
                        string[] lValorParametrosC = { " Delete from t_contrato_ejecucion_usr where codigo_contrato_eje= 0 And login_usuario = '" + goInfo.Usuario.ToString() + "' " };
                        DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_EjecutarCadena", lsNombreParametrosC, lTipoparametrosC, lValorParametrosC);
                        tblDemanda.Visible = true;
                        CargarDatosUsu();
                    }
                    if (hdfPunta.Value == "V")
                    {
                        //hdfPunta.Value = "V"; //20190524 rq029-19
                        lblPunta.Text = "VENDEDOR";
                        //trCompra.Visible = false; //20190524 rq029-19
                        btnCrearUsu.Visible = false;
                        btnActualUsu.Visible = false;
                        ddlDemanda.SelectedValue = "0";
                        ddlSector.SelectedValue = "0";
                        tblDemanda.Visible = false;
                    }

                    tblGrilla.Visible = false;
                    tblRegCnt.Visible = true;
                    tblDetalle.Visible = true;
                    tblDatos.Visible = false;
                    tblDetalle.Visible = true;
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = "Problemas en la Recuperación de la Información. " + ex.Message.ToString();

                }
            }
        }
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            string lsError = "N";
            try
            {
                //valida que se pueda crear o modificar el registro
                if (e.Item.Cells[17].Text == "&nbsp;") //20190524 rq029-19
                    lblMensaje.Text += "No se ha ingresado la información para modificar<br>";
                if (e.Item.Cells[20].Text == "A") //20190524 rq029-19
                    lblMensaje.Text += "La información ya se ingresó por ambas puntas corectamente<br>";
                if (hdfUltDia.Value == "S" && Convert.ToDateTime(hdfHora.Value) < Convert.ToDateTime(DateTime.Now.ToString("HH:mm"))) //20190524 rq029-19
                    lblMensaje.Text += "Está fuera del horario para registro de ejecución de contrato";
                if (lblMensaje.Text == "")
                {
                    limpiarDatosUsu();
                    ddlPunto.Enabled = false;
                    lblFechaGas.Text = e.Item.Cells[0].Text; //20190524 rq029-19
                    //20190524 rq029-19 canbio de inidices
                    lblOperacion.Text = e.Item.Cells[1].Text;
                    lblContratoDef.Text = e.Item.Cells[2].Text;
                    //hdfCantidad.Value = e.Item.Cells[8].Text; //20190524 rq019-19
                    lblCantidad.Text = e.Item.Cells[8].Text;
                    lblPrecio.Text = e.Item.Cells[9].Text;
                    ddlPunto.SelectedValue = e.Item.Cells[12].Text;
                    TxtCantidad.Text = e.Item.Cells[15].Text;
                    hdfCantidad.Value = TxtCantidad.Text;
                    TxtValor.Text = e.Item.Cells[16].Text;
                    hdfCodigoEjec.Value = e.Item.Cells[17].Text;
                    hdfPunta.Value = e.Item.Cells[18].Text;
                    hdfCodigoVerif.Value = e.Item.Cells[19].Text;
                    ddlDemanda.SelectedValue = e.Item.Cells[21].Text;
                    //hdfActBorrUsr.Value = "N"; //20190507 rq023-19
                    //ddlDemanda_SelectedIndexChanged(null, null); //20190507 rq023-19
                    //hdfActBorrUsr.Value = "S"; //20190507 rq023-19
                    ddlSector.SelectedValue = e.Item.Cells[22].Text;
                    CargarDatosDet();
                    if (Session["tipoPerfil"].ToString() != "N")
                    {
                        lsError = "S";
                        CargarDatosUsu();
                    }
                    btnActualizar.Visible = true;
                    btnCrear.Visible = false;

                    if (hdfPunta.Value == "C")
                    {
                        lblPunta.Text = "COMPRADOR";
                        //trCompra.Visible = true; //20190524 rq029-19
                        if (lsError == "N")
                            btnCrearUsu.Visible = true;
                        CargarDatosUsu();
                        string[] lsNombreParametrosC = { "@P_cadena" };
                        SqlDbType[] lTipoparametrosC = { SqlDbType.VarChar };
                        string[] lValorParametrosC = { " Delete from t_contrato_ejecucion_usr where codigo_contrato_eje= 0 And login_usuario = '" + goInfo.Usuario.ToString() + "' " };
                        DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_EjecutarCadena", lsNombreParametrosC, lTipoparametrosC, lValorParametrosC);
                        btnCrearUsu.Visible = true;
                        btnActualUsu.Visible = false;
                        tblDemanda.Visible = true;
                    }
                    if (hdfPunta.Value == "V")
                    {
                        lblPunta.Text = "VENDEDOR";
                        //trCompra.Visible = false; //20190524 rq029-19
                        btnCrearUsu.Visible = false;
                        btnActualUsu.Visible = false;
                        ddlDemanda.SelectedValue = "0";
                        ddlSector.SelectedValue = "0";
                        tblDemanda.Visible = false;
                    }
                    tblGrilla.Visible = false;
                    tblRegCnt.Visible = true;
                    tblDetalle.Visible = true;
                    tblDatos.Visible = false;
                    tblDetalle.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas en la Recuperación de la Información. " + ex.Message.ToString();
            }
        }
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            hdfCodigoEjec.Value = e.Item.Cells[17].Text; //20190524 rq029-19
            if (hdfCodigoEjec.Value == "&nbsp;")
                lblMensaje.Text += "No se ha ingresado la información para modificar<br>";
            if (e.Item.Cells[20].Text == "A") //20190524 rq029-19
                lblMensaje.Text += "La información ya se ingresó por ambas puntas corectamente<br>";
            if (hdfUltDia.Value == "S" && Convert.ToDateTime(hdfHora.Value) < Convert.ToDateTime(DateTime.Now.ToString("HH:mm"))) //20190524 rq029-19
                lblMensaje.Text += "Está fuera del horario para registro de ejecución de contrato";
            if (lblMensaje.Text == "")
            {
                string[] lsNombreParametros = { "@P_codigo_contrato_eje", };
                SqlDbType[] lTipoparametros = { SqlDbType.Int };
                string[] lValorParametros = { hdfCodDatUsu.Value, "3" };

                try
                {
                    lValorParametros[0] = hdfCodigoEjec.Value;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_DelContEjec", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Eliminación de la Información.!" + goInfo.mensaje_error.ToString();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lConexion.Cerrar();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Eliminado Correctamente.!" + "');", true);
                        CargarDatos();
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = "Problemas al Eliminar el Registro.! " + ex.Message.ToString();

                }
            }
        }
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_operador)
    {
        string lsCondicion = "nombre_tabla='t_contrato_ejecucion' and llave_registro='codigo_contato_eje=" + hdfCodigoEjec.Value + "'";
        string lsCondicion1 = "codigo_contrato_eje=" + hdfCodigoEjec.Value;
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "t_contrato_ejecucion";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
        }
        return true;
    }
    /// <summary>
    /// Realiza las Validaciones de los Tipos de Datos y Campos Obligatorios de la Pantalla
    /// </summary>
    /// <returns></returns>
    protected string validaciones()
    {
        string lsError = "";

        try
        {
            if (ddlPunto.SelectedValue == "0")
                lsError += "Debe Seleccionar el punto de entrega. <br>";
            if (TxtCantidad.Text == "")
                lsError += "Debe Ingresar la cantidad autorizada. <br>";
            if (TxtValor.Text == "")
                lsError += "Debe Ingresar el valor facturado. <br>";
            else
            {
                string[] lsValor;
                decimal ldValor;
                try
                {
                    ldValor = Convert.ToDecimal(TxtValor.Text.Trim());
                    if (ldValor < 0)
                        lsError += "Valor Inválido en el valor facturado. <br>";
                    else
                    {
                        lsValor = TxtValor.Text.Trim().Split('.');
                        if (lsValor.Length > 1)
                        {
                            if (lsValor[1].Trim().Length > 2)
                                lsError += "Valor Inválido en el valor facturado. <br>";
                        }
                    }
                }
                catch (Exception ex)
                {
                    lsError += "Valor Inválido en el valor facturado. <br>";
                }
            }
            //20190524 rq029-19
            //if (hdfPunta.Value == "C")
            //{
            //    if (ddlDemanda.SelectedValue == "0")
            //        lsError += "Debe seleccionar el tipo de demanda. <br>";
            //    if (ddlSector.SelectedValue == "0")
            //        lsError += "Debe seleccionar el sector de consumo. <br>";
            //    if (dtgUsuarios.Items.Count == 0)
            //        lsError += "No ha ingresado el detalle de usuarios finales. <br>";
            //}
            //else
            //{
            //    ddlDemanda.SelectedValue = "0";
            //    ddlSector.SelectedValue = "0";
            //}
            //20190524 rq029-19
            if (hdfPunta.Value == "C")
            {
                if (lblTotlCantidad.Text != hdfCantidad.Value)
                    lsError += "La cantidad del detalle de usuarios finales no es igual a la cantidad total de la ejecución del contrato. <br>";
            }
            return lsError;
        }
        catch (Exception ex)
        {
            return "Problemas en la Validación de la Información. " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// Nombre: dtgUsuarios_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgUsuarios_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";

        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            try
            {
                hdfCodDatUsu.Value = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[0].Text;
                ddlDemanda.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[1].Text; //20190524 rq029-19
                ddlDemanda_SelectedIndexChanged(null, null);//20190524 rq029-19 
                ddlSector.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[3].Text; //20190524 rq029-19
                ddlSalida.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[5].Text; //20190524 rq029-19
                if (this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[7].Text != "&nbsp;") //20190524 rq029-19
                    TxtUsuarioFinal.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[7].Text + "-" + this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[8].Text; //20190524 rq029-19
                else
                    TxtUsuarioFinal.Text = "";
                ddlMercado.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[9].Text; //20190524 rq029-19
                TxtCantidadUsr.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[11].Text; //20190524 rq029-19
                btnCrearUsu.Visible = false;
                btnActualUsu.Visible = true;
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al Recuperar el Registro.! " + ex.Message.ToString();
            }

        }
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            lblMensaje.Text = "";
            string[] lsNombreParametros = { "@P_codigo_contrato_eje_usr", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { hdfCodDatUsu.Value, "3" };
            //20190524 rq029-19
            if (hdfUltDia.Value == "S" && Convert.ToDateTime(hdfHora.Value) < Convert.ToDateTime(DateTime.Now.ToString("HH:mm")))
                lblMensaje.Text = "Está fuera del horario para registro de ejecución de contrato";
            if (lblMensaje.Text == "")
            {
                try
                {
                    hdfCodDatUsu.Value = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[0].Text;
                    lValorParametros[0] = hdfCodDatUsu.Value;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContEjecUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Eliminación de la Información del Usuario.!" + goInfo.mensaje_error.ToString();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lConexion.Cerrar();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Eliminado Correctamente.!" + "');", true);
                        btnActualUsu.Visible = false;//20190524 rq029-19
                        btnCrearUsu.Visible = true;//20190524 rq029-19
                        limpiarDatosUsu();//20190524 rq029-19
                        CargarDatosUsu();
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = "Problemas al Eliminar el Registro.! " + ex.Message.ToString();

                }
            }
        }

    }
    /// <summary>
    /// Nombre: CargarDatosUsu
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid de Usuarios.
    /// Modificacion:
    /// </summary>
    private void CargarDatosUsu()
    {
        string[] lsNombreParametros = { "@P_codigo_contrato_eje", "@P_login" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
        string[] lValorParametros = { "-1", "" };
        lblTotlCantidad.Text = "0";
        try
        {
            if (hdfCodigoEjec.Value.Trim().Length > 0)
                lValorParametros[0] = hdfCodigoEjec.Value;
            lValorParametros[1] = goInfo.Usuario.ToString();
            lConexion.Abrir();
            dtgUsuarios.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContEjecUsr", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgUsuarios.DataBind();
            lConexion.Cerrar();
            //20190524 rq029-19
            if (dtgUsuarios.Items.Count > 0)
            {
                foreach (DataGridItem Grilla in this.dtgUsuarios.Items)
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(Grilla.Cells[11].Text.Trim().Replace(",", ""))).ToString();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo consultar la información de usuarios finales.! " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// Nombre: CargarDatosUsu
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid de Usuarios.
    /// Modificacion:
    /// </summary>
    private void CargarDatosDet()
    {
        string[] lsNombreParametros = { "@P_codigo_verif", "@P_fecha_gas", "@P_punta" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
        string[] lValorParametros = { hdfCodigoVerif.Value, lblFechaGas.Text, hdfPunta.Value };
        try
        {
            lConexion.Abrir();
            dtgDetalle.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContEjecucion1", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgDetalle.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImbSalir_Click(object sender, ImageClickEventArgs e)
    {
        tblDemanda.Visible = false;
        tblGrilla.Visible = true;
        tblDatos.Visible = true;
        CargarDatos();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImmSalirVer_Click(object sender, ImageClickEventArgs e)
    {
        tblDemanda.Visible = false;
        tblGrilla.Visible = true;
        tblDatos.Visible = true;
        CargarDatos();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCrearUsu_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_contrato_eje_usr", "@P_codigo_contrato_eje","@P_tipo_demanda","@P_sector_consumo", "@P_codigo_punto_salida", "@P_nit_usuario_no_regulado", "@P_codigo_mercado_relevante","@P_cantidad_contratada","@P_accion" //20190524 rq029-19
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int , SqlDbType.Int //20190524 rq029-19
                                      };
        string[] lValorParametros = { "0", "0", "0","0","0","", "0","0", "1" //20190524 rq029-19
                                    };

        lblMensaje.Text = "";
        string[] lsUsuario;
        lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
        try
        {
            //20190524 rq029-19
            if (ddlDemanda.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el tipo de demanda<br>";
            if (ddlSector.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el sector de consumo<br>";
            if (ddlSalida.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el punto de salida<br>";
            //20190524 fin rq029-19
            lConexion.Abrir();
            if (TxtUsuarioFinal.Enabled)
            {
                if (TxtUsuarioFinal.Text.Trim() == "")
                    lblMensaje.Text += "Debe Ingresar el Usuario no regulado<br>";
                else
                {

                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                    if (!lLector.HasRows)
                        lblMensaje.Text += "El Usuario Ingresado NO existe en la Base de Datos.! <br>";
                    lLector.Close();
                }
            }
            if (ddlMercado.Enabled)
            {
                if (ddlMercado.SelectedValue == "0")
                    lblMensaje.Text += "Debe Seleccionar el mercado relevante. <br> ";
            }
            //20190524 rq029-19
            if (TxtCantidadUsr.Text == "")
                lblMensaje.Text += "Debe ingresar la cantidad contratada. <br> ";
            else
            {
                int liValor = Convert.ToInt32(TxtCantidadUsr.Text.Trim());
                if (liValor <= 0)
                    lblMensaje.Text += "La cantidad contratada debe ser mayor que cero. <br> ";
                //else
                //    if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(hdfCantidad.Value))
                //    lblMensaje.Text += "La cantidad Acumulada de Usuarios No puede ser Mayor que " + hdfCantidad.Value + ". <br> ";
            }
            //20190524 rq029-19
            if (hdfUltDia.Value == "S" && Convert.ToDateTime(hdfHora.Value) < Convert.ToDateTime(DateTime.Now.ToString("HH:mm")))
                lblMensaje.Text += "Está fuera del horario para registro de ejecución de contrato";

            if (lblMensaje.Text == "")
            {
                if (hdfCodigoEjec.Value != "")
                    lValorParametros[1] = hdfCodigoEjec.Value;
                lValorParametros[2] = ddlDemanda.SelectedValue; //20190524 rq029-19
                lValorParametros[3] = ddlSector.SelectedValue; //20190524 rq029-19
                lValorParametros[4] = ddlSalida.SelectedValue; //20190524 rq029-19
                lValorParametros[5] = lsUsuario[0].Trim();
                lValorParametros[6] = ddlMercado.SelectedValue;
                lValorParametros[7] = TxtCantidadUsr.Text; //20190524 rq029-19

                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContEjecUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación de la Información del Usuario.!" + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Información de Usuarios Finales Ingresada Correctamente.!" + "');", true);
                    limpiarDatosUsu(); //20190524 rq029-19
                    CargarDatosUsu();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnActualUsu_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_contrato_eje_usr", "@P_codigo_contrato_eje","@P_tipo_demanda","@P_sector_consumo", "@P_codigo_punto_salida", "@P_nit_usuario_no_regulado", "@P_codigo_mercado_relevante","@P_cantidad_contratada","@P_accion" //20190524 rq029-19
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int //20190524 rq029-19
                                      };
        string[] lValorParametros = { "0", "0","0","0","0", "", "0","0", "2" //20190524 rq029-19
                                    };
        lblMensaje.Text = "";
        string[] lsUsuario;
        lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
        try
        {
            //20190524 rq029-19
            if (ddlDemanda.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el tipo de demanda<br>";
            if (ddlSector.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el sector de consumo<br>";
            if (ddlSalida.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el punto de salida<br>";
            //20190524 fin rq029-19
            lConexion.Abrir();
            if (TxtUsuarioFinal.Enabled)
            {
                if (TxtUsuarioFinal.Text.Trim() == "")
                    lblMensaje.Text += "Debe Ingresar el Usuario no regulado<br>";
                else
                {

                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                    if (!lLector.HasRows)
                        lblMensaje.Text += "El Usuario Ingresado NO existe en la Base de Datos.! <br>";
                    lLector.Close();
                }
            }
            if (ddlMercado.Enabled)
            {
                if (ddlMercado.SelectedValue == "0")
                    lblMensaje.Text += "Debe Seleccionar el mercado relevante. <br> ";
            }
            //20190524 rq029-19
            if (TxtCantidadUsr.Text == "")
                lblMensaje.Text += "Debe ingresar la cantidad contratada. <br> ";
            else
            {
                int liValor = Convert.ToInt32(TxtCantidadUsr.Text.Trim());
                if (liValor <= 0)
                    lblMensaje.Text += "La cantidad contratada debe ser mayor que cero. <br> ";
                //else
                //    if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(hdfCantidad.Value))
                //    lblMensaje.Text += "La cantidad Acumulada de Usuarios No puede ser Mayor que " + hdfCantidad.Value + ". <br> ";
            }
            //20190524 rq029-19
            if (hdfUltDia.Value == "S" && Convert.ToDateTime(hdfHora.Value) < Convert.ToDateTime(DateTime.Now.ToString("HH:mm")))
                lblMensaje.Text += "Está fuera del horario para registro de ejecución de contrato";
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = hdfCodDatUsu.Value;
                if (hdfCodigoEjec.Value != "")
                    lValorParametros[1] = hdfCodigoEjec.Value;
                lValorParametros[2] = ddlDemanda.SelectedValue; //20190524 rq029-19
                lValorParametros[3] = ddlSector.SelectedValue; //20190524 rq029-19
                lValorParametros[4] = ddlSalida.SelectedValue; //20190524 rq029-19
                lValorParametros[5] = lsUsuario[0].Trim();
                lValorParametros[6] = ddlMercado.SelectedValue;
                lValorParametros[7] = TxtCantidadUsr.Text; //20190524 rq029-19
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContEjecUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización de la Información del Usuario.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Información de Usuarios Finales Actualizada Correctamente.!" + "');", true);
                    limpiarDatosUsu();//20190524 rq029-19
                    btnCrearUsu.Visible = true;
                    btnActualUsu.Visible = false;
                    CargarDatosUsu();
                    btnActualUsu.Visible = false;
                    btnCrearUsu.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    protected void btnCrear_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_contrato_eje", "@P_codigo_operador", "@P_punta", "@P_numero_contrato", "@P_codigo_verif", "@P_contrato_definitivo", "@P_codigo_punto", "@P_cantidad_autorizada", "@P_valor_facturado", "@P_fecha_gas" }; //20190524 rq029-19
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar }; //20190524 rq029-19
        string[] lValorParametros = { "0", "0", "", "0", "0", "", "0", "0", "0", "" }; //20190524 rq029-19
        lblMensaje.Text = "";
        try
        {
            lblMensaje.Text = validaciones();
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = "0";
                lValorParametros[1] = ddlOperador.SelectedValue;
                lValorParametros[2] = hdfPunta.Value;
                lValorParametros[3] = lblOperacion.Text;
                lValorParametros[4] = hdfCodigoVerif.Value;
                lValorParametros[5] = lblContratoDef.Text;
                lValorParametros[6] = ddlPunto.SelectedValue;
                lValorParametros[7] = TxtCantidad.Text;
                lValorParametros[8] = TxtValor.Text;
                //lValorParametros[9] = ddlDemanda.SelectedValue; //20190524 rq029-19
                //lValorParametros[10] = ddlSector.SelectedValue; //20190524 rq029-19
                lValorParametros[9] = lblFechaGas.Text; //20190524 rq029-19
                lConexion.Abrir();
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetContEjec", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación del Registro del Contrato.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                        {
                            if (lLector["ind_error"].ToString() == "S")
                                lblMensaje.Text += lLector["error"].ToString() + "<br>";
                            else
                            {
                                hdfCodigoEjec.Value = lLector["codigo"].ToString();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Ingresado Correctamente.!" + "');", true);
                                hdfCodigoEjec.Value = "0";
                                if (hdfPunta.Value == "C")
                                    CargarDatosUsu();
                                limpiarDatos();
                                limpiarDatosUsu();
                                CargarDatosDet();
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Ingresado Correctamente.!" + "');", true);
                        limpiarDatos();
                    }
                    lConexion.Cerrar();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    protected void btnActualizar_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_contrato_eje", "@P_codigo_operador", "@P_punta", "@P_numero_contrato", "@P_codigo_verif", "@P_contrato_definitivo", "@P_codigo_punto", "@P_cantidad_autorizada", "@P_valor_facturado", "@P_fecha_gas" }; //20190524 rq029-19
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar }; //20190524 rq029-19
        string[] lValorParametros = { "0", "0", "", "0", "0", "", "0", "0", "0", "" }; //20190524 rq029-19
        try
        {
            lblMensaje.Text = validaciones();
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = hdfCodigoEjec.Value;
                lValorParametros[1] = ddlOperador.SelectedValue;
                lValorParametros[2] = hdfPunta.Value;
                lValorParametros[3] = lblOperacion.Text;
                lValorParametros[4] = hdfCodigoVerif.Value;
                lValorParametros[5] = lblContratoDef.Text;
                lValorParametros[6] = ddlPunto.SelectedValue;
                lValorParametros[7] = TxtCantidad.Text;
                lValorParametros[8] = TxtValor.Text;
                //lValorParametros[9] = ddlDemanda.SelectedValue; //20190524 rq029-19
                //lValorParametros[10] = ddlSector.SelectedValue;  //20190524 rq029-19
                lValorParametros[9] = lblFechaGas.Text; //20190524 rq029-19
                lConexion.Abrir();
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetContEjec", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensaje.Text = "Se presentó un Problema en la Modificación del Registro.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                            lblMensaje.Text += lLector["error"].ToString() + "<br>";
                        lConexion.Cerrar();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Modificado Correctamente.!" + "');", true);
                        limpiarDatosUsu();
                        tblRegCnt.Visible = false;
                        tblDetalle.Visible = false;
                        tblGrilla.Visible = true;
                        tblDatos.Visible = true;
                        tblDemanda.Visible = false;
                        lConexion.Cerrar();
                        CargarDatos();
                    }

                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    protected void btnRegresar_Click(object sender, EventArgs e)
    {
        tblGrilla.Visible = true;
        tblRegCnt.Visible = false;
        tblDetalle.Visible = false;
        tblDatos.Visible = true;
        tblDemanda.Visible = false;
        TxtCantidad.Text = "";
        TxtValor.Text = "";
        ddlPunto.SelectedValue = "0";
        ddlSector.SelectedValue = "0";
        ddlDemanda.SelectedValue = "0";
        lblMensaje.Text = "";
        CargarDatos();
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lsTable, string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lsTable, lswhere, goInfo);
    }
    /// <summary>
    /// Nombre: limpiarDatosUsu
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected void limpiarDatosUsu()
    {
        ddlDemanda.SelectedValue = "0"; //20190524 rq029-19
        ddlSector.SelectedValue = "0";//20190524 rq029-19
        ddlSalida.SelectedValue = "0";//20190524 rq029-19
        ddlMercado.SelectedValue = "0";
        TxtUsuarioFinal.Text = "";
        TxtCantidadUsr.Text = "";//20190524 rq029-19
    }
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected void limpiarDatos()
    {
        ddlPunto.SelectedValue = "0";
        TxtCantidad.Text = "";
        TxtValor.Text = "";
        //ddlDemanda.SelectedValue = "0"; //20190524 rq029-19
        //ddlSector.SelectedValue = "0"; //20190524 rq029-19
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlDemanda_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlDemanda.SelectedValue == "1")
        {
            TxtUsuarioFinal.Text = "";
            TxtUsuarioFinal.Enabled = false;
            ddlMercado.Enabled = true;
        }
        else
        {
            TxtUsuarioFinal.Enabled = true;
            ddlMercado.SelectedValue = "0";
            ddlMercado.Enabled = false;
        }
        //20190507 rq023-19
        //if (hdfActBorrUsr.Value == "S") //20190507 rq023-19
        //{ //20190507 rq023-19
        //    string[] lsNombreParametros = { "@P_codigo_contrato_eje", "@P_accion" };
        //    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
        //    string[] lValorParametros = { hdfCodigoEjec.Value, "4" };

        //    try
        //    {
        //        lConexion.Abrir();
        //        DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContEjecUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
        //        CargarDatosUsu();
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}//20190507 rq023-19 
    }

    //20190524 rq029-19
    protected void TxtCantidad_TextChanged(object sender, EventArgs e)
    {
        hdfCantidad.Value = TxtCantidad.Text;
        if (hdfCantidad.Value == "")
            hdfCantidad.Value = "0";
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        this.dtgConsulta.CurrentPageIndex = 0;
        CargarDatos();

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lkbExcel_Click(object sender, EventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfEjecucion" + DateTime.Now + ".xls";
        string lstitulo_informe = "";
        string lsTituloParametros = "";
        try
        {
            lstitulo_informe = "Consulta Ingreso Ejecución de contratos";
            lsTituloParametros = "";

            if (TxtNoContrato.Text.Trim().Length > 0)
                lsTituloParametros = "No. Operación Inicial: " + TxtNoContrato.Text.Trim();
            if (TxtNoContratoFin.Text.Trim().Length > 0)
                lsTituloParametros += " - No. Operación Final: " + TxtNoContratoFin.Text.Trim();
            if (TxtContratoDef.Text.Trim().Length > 0)
                lsTituloParametros += " - Contrato: " + TxtContratoDef.Text.Trim();
            if (ddlOperador.SelectedValue != "0")
                lsTituloParametros += " - Operador: " + ddlOperador.SelectedItem.ToString();
            if (ddlSubasta.SelectedValue != "0")
                lsTituloParametros += " - Subasta: " + ddlSubasta.SelectedItem.ToString();
            if (ddlMercado.SelectedValue != "0")
                lsTituloParametros += " - Tipo Mercado: " + ddlMercado.SelectedItem.ToString();
            if (ddlProducto.SelectedValue != "0")
                lsTituloParametros += " - Producto: " + ddlProducto.SelectedItem.ToString();
            if (ddlModalidad.SelectedValue != "0")
                lsTituloParametros += " - Modalidad Contractual: " + ddlModalidad.SelectedItem.ToString();
            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgExcel.Visible = true;
            dtgExcel.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            lform.Controls.Add(dtgExcel);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lConexion.CerrarInforme();

            dtgExcel.Visible = true;
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Excel.!" + ex.Message.ToString();
        }
    }
}