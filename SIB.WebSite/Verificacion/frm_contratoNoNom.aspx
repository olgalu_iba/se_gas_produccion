﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_contratoNoNom.aspx.cs" Inherits="Verificacion.frm_contratoNoNom" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body" runat="server">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Código Registro" AssociatedControlID="TxtBusCodigo" runat="server" />
                            <asp:TextBox ID="TxtBusCodigo" runat="server" autocomplete="off" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtBusCodigo" runat="server" TargetControlID="TxtBusCodigo"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operación" AssociatedControlID="TxtBusOperacion" runat="server" />
                            <asp:TextBox ID="TxtBusOperacion" runat="server" autocomplete="off" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtBusOperacion" runat="server" TargetControlID="TxtBusOperacion"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Transportador" AssociatedControlID="ddlBusOperador" runat="server" />
                            <asp:DropDownList ID="ddlBusOperador" runat="server" CssClass="form-control selectpicker" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Remitente" AssociatedControlID="ddlBusRemitente" runat="server" />
                            <asp:DropDownList ID="ddlBusRemitente" runat="server" CssClass="form-control selectpicker" />
                        </div>
                    </div>
                </div>
                <%--Mensaje--%>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:ValidationSummary ID="VsConsulta" runat="server" ValidationGroup="detalle1" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
                <%--Grilla--%>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" PageSize="10"
                                OnItemCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged">
                                <Columns>
                                    <asp:BoundColumn DataField="codigo_no_nom" HeaderText="Código" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="transportador" HeaderText="Código Transportador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_trans" HeaderText="Nombre Transportador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="remitente" HeaderText="Remitente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_remitente" HeaderText="Nombre Remitente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_tramo" HeaderText="Código Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn> <%--20210224--%>
                                    <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn> <%--20210224--%>
                                    <asp:BoundColumn DataField="cantidad_no_nominada" HeaderText="Capacidad Disponible (KPCD)" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more-1"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                    <!--begin::Nav-->
                                                    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <ul class="kt-nav">
                                                                <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                             <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Modificar</span>
                                                                    </asp:LinkButton>
                                                                </li>
                                                                <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon-delete"></i>
                                                            <span class="kt-nav__link-text">Eliminar</span>
                                                                    </asp:LinkButton>
                                                                </li>
                                                                
                                                            </ul>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%--Modals--%>
    <div class="modal fade" id="registroAdicion" tabindex="-1" role="dialog" aria-labelledby="mdlregistroAdicion" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="registroAdicionInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="lblRegistroAdicion" runat="server">Agregar</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Código Registro" AssociatedControlID="TxtCodigoCap" runat="server" />
                                        <asp:TextBox ID="TxtCodigoCap" runat="server" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                        <asp:Label ID="LblCodigoCap" runat="server" Visible="False" CssClass="form-control"></asp:Label>
                                        <asp:HiddenField ID="hdnELiminado" runat="server" />
                                        <asp:HiddenField ID="hdfModificacion" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Transportador" AssociatedControlID="ddlOperador" runat="server"  />
                                        <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control" OnSelectedIndexChanged ="ddlOperador_SelectedIndexChanged" AutoPostBack ="true" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Remitente" AssociatedControlID="ddlRemitente" runat="server" />
                                        <asp:DropDownList ID="ddlRemitente" runat="server" CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Operación" AssociatedControlID="TxtOperacion" runat="server" />
                                        <asp:TextBox ID="TxtOperacion" runat="server" CssClass="form-control"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtOperacion" runat="server" TargetControlID="TxtOperacion"
                                            FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Tramo" AssociatedControlID="ddlTramo" runat="server" /> <%--20210224--%>
                                        <asp:DropDownList ID="ddlTramo" runat="server" CssClass="form-control" /> <%--20210224--%>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <%--20210224--%>
                                        <asp:Label Text="Cantidad No Nominada (KPCD)" AssociatedControlID="TxtCantidad" runat="server" />
                                        <asp:TextBox ID="TxtCantidad" runat="server" CssClass="form-control"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtCantidad" runat="server" TargetControlID="TxtCantidad"
                                            FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="detalle" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button Text="Salir" CssClass="btn btn-secondary" OnClick="Cancel_OnClick" runat="server" />
                            <asp:Button ID="imbCrear" runat="server" CssClass="btn btn-primary" Text="Crear" OnClick="imbCrear_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="imbActualiza" runat="server" CssClass="btn btn-primary" Text="Actualizar" OnClick="imbActualiza_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
