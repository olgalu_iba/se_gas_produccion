﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;
using System.Text;

namespace Verificacion
{
    public partial class frm_AutContratoNoNomExt : Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Autorización ingreso extemporáneo contratos no nominados";   //20160810 modalidad por tipo campo //20190306 rq013-19
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        clConexion lConexion = null;

        SqlDataReader lLector;
        SqlDataReader lLector1;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            //Controlador util = new Controlador();
            /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

            //Establese los permisos del sistema
            EstablecerPermisosSistema();
            lConexion = new clConexion(goInfo);
            new clConexion(goInfo);
            //Titulo
            Master.Titulo = "Contratos ";
            /// Activacion de los Botones
            buttons.Inicializar(ruta: "m_operador_aut_no_nom");
            buttons.CrearOnclick += btnNuevo;

            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.ExportarPdfOnclick += ImgPdf_Click;

            if (IsPostBack) return;
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " tipo_operador ='T' and estado = 'A'  order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlBusOperador, "m_operador", "tipo_operador ='T' and estado = 'A'  order by razon_social", 0, 4);
            lConexion.Cerrar();
            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlBusOperador.SelectedValue = goInfo.cod_comisionista;
                ddlBusOperador.Enabled = false;
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
            }

            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            Buscar();
        }

        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_operador_aut_no_nom");
            foreach (DataGridItem Grilla in dtgMaestro.Items)
            {
                var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                lkbModificar.Visible = (bool)permisos["UPDATE"]; //20170131 Modif Inf Operativa Participantes Req.003-17 //20190306 rq013-19
                var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
                lkbEliminar.Visible = (bool)permisos["DELETE"];  //20170131 Modif Inf Operativa Participantes Req.003-17 //20190306 rq013-19
            }
        }


        /// <summary>
        /// Nombre: Listar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            CargarDatos();
            lblTitulo.Text = "Consultar " + lsTitulo;
        }


        /// Nombre: Buscar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
        /// Modificacion:
        /// </summary>
        private void Buscar()
        {
            lblTitulo.Text = "Consultar " + lsTitulo;
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_codigo_autoriza", "@P_codigo_operador" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { "0", "0" };
            DateTime ldFechaI = DateTime.Now;
            DateTime ldFechaF = DateTime.Now;
            lblMensaje.Text = "";

            try
            {
                if (lblMensaje.Text == "")
                {
                    if (TxtBusCodigo.Text.Trim().Length > 0)
                        lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    if (ddlBusOperador.SelectedValue != "0")
                        lValorParametros[1] = ddlBusOperador.SelectedValue;
                    lConexion.Abrir();
                    dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetOperAutNoNom", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgMaestro.DataBind();
                    lConexion.Cerrar();
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            if (modificar != null && modificar != "")
            {
                try
                {
                    lblMensaje.Text = "";
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_aut_no_nom", " codigo_autoriza= " + modificar + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            LblCodigoCap.Text = lLector["codigo_autoriza"].ToString();
                            TxtCodigoCap.Text = lLector["codigo_autoriza"].ToString();
                            ddlOperador.SelectedValue = lLector["codigo_operador"].ToString();
                            ddlOperador.Enabled = false;
                            imbCrear.Visible = false;
                            imbModificar.Visible = true;
                            TxtCodigoCap.Visible = false;
                            LblCodigoCap.Visible = true;
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Bloquea el Registro a Modificar
                        if (lblMensaje.Text == "") //20160809 carga ptdv
                            manejo_bloqueo("A", modificar);
                    }
                    else
                    {
                        Listar();
                        lblMensaje.Text = "No se Puede editar el Registro porque está Bloqueado. Código registro " + modificar.ToString(); //20190306 rq013-19

                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = ex.Message;
                }
            }
            if (lblMensaje.Text == "")
            {
                //Abre el modal de Agregar
                Modal.Abrir(this, registroAdicion.ID, registroAdicionInside.ID);


                lblTitulo.Text = "Modificar " + lsTitulo;
            }
        }


        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            lblMensaje.Text = "";
            this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();

        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page")) return;

            string lCodigoRegistro = "";
            hdfModificacion.Value = "N"; // Cambio Req. 003-17 20170131
            lblMensaje.Text = "";
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text; // Cambio Req. 003-17 20170131
            ///////////////////////////////////////////////////////////////////////////////////
            ///// Control Nuevo para Modif Inf Operativa Participantes Req. 003-17 20170131 ///
            ///////////////////////////////////////////////////////////////////////////////////
            if (e.CommandName.Equals("Modificar"))
            {
                lblRegistroAdicion.InnerText = "Modificar";
                Modificar(lCodigoRegistro);
            }
            // Evento Eliminar para los Participantes Modif Inf Operativa Participantes Req. 003-17 20170131
            if (e.CommandName.Equals("Eliminar"))
            {
                string[] lsNombreParametros = { "@P_codigo_operador", "@P_accion" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { this.dtgMaestro.Items[e.Item.ItemIndex].Cells[1].Text, "3" };
                string lsMensaje = "";
                try
                {
                    if (lblMensaje.Text == "")
                    {
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetAutOpeContNoNom", lsNombreParametros, lTipoparametros, lValorParametros);
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                                lsMensaje += lLector["error"].ToString() + "<br>";
                        }
                        else
                        {
                            Toastr.Success(this, "Registro elimnado correctamente");
                            Listar();
                        }
                    }
                    if (lsMensaje != "")
                        Toastr.Warning(this, lsMensaje);
                }
                catch (Exception ex)
                {
                    /// Desbloquea el Registro Actualizado
                    lblMensaje.Text = ex.Message;
                }
            }
            if (lblMensaje.Text == "") return;
            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";
        }



        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// 20180122 rq003-18
        ///  /// // 20180126 rq107-16
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceLlave).ToString() + "-" + lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='m_operador_aut_no_nom' and llave_registro='codigo_autoriza=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_autoriza=" + lscodigo_registro.ToString();
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "m_operador_aut_no_nom";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_operador_aut_no_nom", lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] lValorParametros = { "0", "0" };
            string lsParametros = "";

            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    lsParametros += " Código : " + TxtBusCodigo.Text;
                }
                if (ddlBusOperador.SelectedValue != "0")
                {
                    lValorParametros[1] = ddlBusOperador.SelectedValue;
                    lsParametros += " Operador: " + ddlBusOperador.SelectedItem;
                }
                Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetOperAutNoNom&nombreParametros=@P_codigo_autoriza*@P_codigo_operador&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "&columnas=codigo_autoriza*codigo_operador*nombre_operador*login_usuario*fecha_hora_actual*&titulo_informe=Listado de operadores autorizados para ingreso extemporáneo de contratos no nominados&TituloParametros=" + lsParametros);
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.!";
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }

        }

        /// <summary>
        /// Nombre: ImgPdf_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgPdf_Click(object sender, EventArgs e)
        {
            try
            {
                string lsCondicion = " codigo_autoriza <> '0'";
                if (ddlBusOperador.SelectedValue != "0")
                    lsCondicion += " and codigo_operador=" + ddlBusOperador.SelectedValue;

                Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_operador_aut_no_nom&procedimiento=pa_ValidarExistencia&columnas=codigo_autoriza*codigo_operador*autorizado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.!";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_operador", "@P_autorizado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "", "1" };
            string lsMensaje = "";
            try
            {
                if (ddlOperador.SelectedValue == "0")
                    lsMensaje = "debe seleccionar el operador<br>";
                if (lsMensaje == "")
                {
                    lValorParametros[0] = ddlOperador.SelectedValue;
                    lValorParametros[1] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetAutOpeContNoNom", lsNombreParametros, lTipoparametros, lValorParametros);
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                            lsMensaje += lLector["error"].ToString() + "<br>";
                    }
                    else
                    {
                        Toastr.Success(this, "Registro creado correctamente");
                        Listar();
                    }
                    if (lsMensaje != "")
                        Toastr.Warning(this, lsMensaje);
                    else
                    {
                        Modal.Cerrar(this, registroAdicion.ID);
                        Listar();
                    }

                }
                else
                    Toastr.Warning(this, lsMensaje);

            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Error: " + ex.Message.ToString());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbModificar_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_operador", "@P_autorizado", "@P_accion" }; 
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar};
            string[] lValorParametros = { "0", "", "2", };
            string lsMensaje = "";
            try
            {
                if (ddlOperador.SelectedValue == "0")
                    lsMensaje += "debe seleccionar el operador<br>";
                if (lsMensaje == "")
                {
                    lValorParametros[0] = ddlOperador.SelectedValue;
                    lValorParametros[1] = ddlEstado.SelectedValue;

                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetAutOpeContNoNom", lsNombreParametros, lTipoparametros, lValorParametros);
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                            lsMensaje += lLector["error"] + "<br>";
                    }
                    else
                    {
                        Toastr.Success(this, "Registro modificado correctamente");
                        Listar();
                    }
                    if (lsMensaje != "")
                        Toastr.Warning(this, lsMensaje);
                    else
                    {
                        manejo_bloqueo("E", TxtCodigoCap.Text);
                        Modal.Cerrar(this, registroAdicion.ID);
                        Listar();
                    }
                }
                else
                    Toastr.Warning(this, lsMensaje);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Error: " + ex.Message.ToString());
            }
        }


        /// <summary>
        /// 
        /// </summary>
        protected void Cancel_OnClick(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (lblRegistroAdicion.InnerText.Equals("Modificar") && LblCodigoCap.Text != "")
                manejo_bloqueo("E", LblCodigoCap.Text);

            //Cierra el modal de Agregar
            Modal.Cerrar(this, registroAdicion.ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            Listar();
        }
        ///// Eventos Nuevos para la Implementracion del UserControl

        /// <summary>
        /// Metodo del Link Nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNuevo(object sender, EventArgs e)
        {
            lblRegistroAdicion.InnerText = "Agregar";
            LblCodigoCap.Text = "Automático";


            imbCrear.Visible = true;
            imbModificar.Visible = false;
            TxtCodigoCap.Visible = false;
            LblCodigoCap.Visible = true;
            ddlOperador.Visible = true;
            ddlOperador.SelectedValue  = "0";
            ddlOperador.Enabled = true;

            //Abre el modal de Agregar
            Modal.Abrir(this, registroAdicion.ID, registroAdicionInside.ID);
        }

        /// <summary>
        /// Metodo del Link Listar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnListar(object sender, EventArgs e)
        {
            Listar();
        }

        /// <summary>
        /// Metodo del Link Consultar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsulta(object sender, EventArgs e)
        {
            Buscar();
        }
    }
}