﻿

<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_EliminaContSinRegistro.aspx.cs"
    Inherits="Verificacion_frm_EliminaContSinRegistro" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <div>
            <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
                width="100%">
                <tr>
                    <td align="center" class="th1" style="width: 80%;">
                        <asp:Label ID="lblTitulo" runat="server" ForeColor="White" ></asp:Label>
                    </td>
                    <td align="center" class="th1" style="width: 20%;">
                        <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/exel 2007 3D.png"
                            Height="40" OnClick="imbExcel_Click" Visible="false" />
                    </td>
                </tr>
            </table>
            <br /><br /><br /><br /><br /><br />
            <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
                width="90%">
                <tr>
                    <td class="td1">Fecha Negociación
                    </td>
                    <td class="td2">
                        <asp:TextBox ID="TxtFecha" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    </td>
                    <td class="td1">Número Operación 
                    </td>
                    <td class="td2">
                        <asp:TextBox ID="TxtNoContrato" runat="server" ValidationGroup="detalle" Width="120px"></asp:TextBox>
                    </td>
                    <td class="td1">Número Id Registro 
                    </td>
                    <td class="td2">
                        <asp:TextBox ID="TxtNoIdRegistro" runat="server" ValidationGroup="detalle" Width="120px"></asp:TextBox>
                    </td>
                    <td class="td1">Número Contrato
                    </td>
                    <td class="td2">
                        <asp:TextBox ID="TxtContDef" runat="server" ValidationGroup="detalle" Width="120px" MaxLength="30"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td class="th1" colspan="8" align="center">
                        <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                            OnClick="imbConsultar_Click" Height="40" />
                    </td>
                </tr>
            </table>
        </div>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblMensaje">
            <tr>
                <td colspan="3" align="center">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblGrilla" visible="false">
            <tr>
                <td colspan="6" align="center">
                    <%--20190607 rq036-19--%>
                    <%--<div style="overflow: scroll; width: 1080px; height: 450px;">--%>
                        <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                            AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1"
                            OnEditCommand="dtgConsulta_EditCommand">
                            <Columns>
                                <%--20190607 rq036-19--%>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="No. Registro"></asp:BoundColumn>
                                <asp:BoundColumn DataField="contrato_definitivo" HeaderText="No. Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_compra" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="80px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_venta" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_producto" HeaderText="producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_subasta" HeaderText="Tipo Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ing_venta" HeaderText="Información Venta" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ing_compra" HeaderText="Información Compra" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Fecha_max_registro" HeaderText="Fecha Máxima Registro" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Fecha_maxima_correccion" HeaderText="Fecha Máxima Modificación" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <%--20190607 rq036-19--%>
                                <asp:TemplateColumn HeaderText="Punta a Borrar">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlPunta" runat="server" Width="80px" >
                                            <asp:ListItem Value="" Text="Seleccione"></asp:ListItem>
                                            <asp:ListItem Value="A" Text="Ambas"></asp:ListItem>
                                            <asp:ListItem Value="C" Text="Compra"></asp:ListItem>
                                            <asp:ListItem Value="V" Text="Venta"></asp:ListItem>
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar" ></asp:EditCommandColumn>
                            </Columns>
                        </asp:DataGrid>
                    <%--</div>--%>
                </td>
            </tr>
        </table>
</asp:Content>