﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Verificacion
{
    public partial class frm_EnergiaTrasferida : Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Cantidad de Energía Transferida";
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        clConexion lConexion = null;
        clConexion lConexion1 = null;
        SqlDataReader lLector;
        SqlDataReader lLector1;

        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            //Establese los permisos del sistema
            //EstablecerPermisosSistema(); //20220621 ajsute
            lConexion = new clConexion(goInfo);
            lConexion1 = new clConexion(goInfo);
            //Titulo
            Master.Titulo = "Registros Operativos";
            /// Activacion de los Botones
            buttons.Inicializar(ruta: "t_energia_trasferida");
            buttons.CrearOnclick += btnNuevo;

            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.ExportarPdfOnclick += ImgPdf_Click;

            if (IsPostBack) return;
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles1(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador >0  and (tipo_operador = 'T' or codigo_operador = " + goInfo.cod_comisionista + ") order by razon_social", 0, 4);  //20180122 rq003-18 // 20180126 rq107-16
            LlenarControles1(lConexion.gObjConexion, ddlTrasportador, "m_operador ope", " tipo_operador ='T' and estado = 'A'  order by razon_social", 0, 4); // 20180126 rq107-16                                                                                                
            //20220705
            if (goInfo.cod_comisionista != "0")
                LlenarControles(lConexion.gObjConexion, ddlPunto, "m_pozo poz, m_punto_operador ope", " poz.ind_trasferencia ='S' and poz.estado ='A' and ope.estado ='A' and poz.codigo_pozo = ope.codigo_punto and ope.tipo_punto ='E' and (ope.codigo_operador =0 or ope.codigo_operador =" + goInfo.cod_comisionista + ") and ope.estado='A' order by poz.descripcion", 0, 1);
            else
                LlenarControles(lConexion.gObjConexion, ddlPunto, "m_pozo", " ind_trasferencia ='S' and estado ='A'  order by descripcion", 0, 1); //20161108 control inf ope //20220202 verifica operativa //20220705
            LlenarControles1(lConexion.gObjConexion, ddlBusOperador, "m_operador", " estado = 'A' and codigo_operador >0 and (tipo_operador = 'T' or codigo_operador = " + goInfo.cod_comisionista + ") order by razon_social", 0, 4);  //20180122 rq003-18 // 20180126 rq107-16
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_no_ing_inf_ope", " codigo_reporte = 8 and codigo_operador =" + goInfo.cod_comisionista + " and estado ='A' ");
            if (lLector.HasRows)
            {
                lblMensaje.Text = "El Operador no está obligado a ingresar la información operativa de energía transferida.";
            }
            lLector.Close();
            lLector.Dispose();

            lConexion.Cerrar();
            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlBusOperador.SelectedValue = goInfo.cod_comisionista;
                ddlBusOperador.Enabled = false;
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
                hdnELiminado.Value = "N";  //20160721 modif inf opertiva y transaccional
            }
            else
                hdnELiminado.Value = "S";  //20160721 modif inf opertiva y transaccional

            DateTime ldfecha;
            ldfecha = DateTime.Now.AddDays(-1);
            TxtFecha.Text = ldfecha.Year.ToString() + "/" + ldfecha.Month.ToString() + "/" + ldfecha.Day.ToString();
            TxtFecha.Enabled = false;

            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            Buscar();
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }
        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "t_energia_trasferida");
            //hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
            //hlkListar.Enabled = (Boolean)permisos["SELECT"];
            //hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
            //if (Session["tipoPerfil"].ToString() != "N")
            //    dtgMaestro.Columns[12].Visible = (Boolean)permisos["UPDATE"]; //20160711 ajuste modificaciones  //20160725 ajuste parametrizacion pozo 20160725
            //else
            //    dtgMaestro.Columns[12].Visible = false; //20160725 ajuste parametrizacion pozo 20160725
            //dtgMaestro.Columns[13].Visible = false;//20160711 ajuste modificaciones  //20160725 ajuste parametrizacion pozo 20160725
            //20220621 ajsute
            if ((bool)permisos["UPDATE"] && (bool)permisos["DELETE"])
                dtgMaestro.Columns[13].Visible = true; //20220705
            else
            {
                if (!(bool)permisos["UPDATE"] && !(bool)permisos["DELETE"])
                    dtgMaestro.Columns[13].Visible = false; //20220705
                else
                {
                    if (!(bool)permisos["UPDATE"])
                    {
                        foreach (DataGridItem Grilla in dtgMaestro.Items)
                        {
                            var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                            lkbModificar.Visible = false;
                        }
                    }
                    if (!(bool)permisos["DELETE"])
                    {
                        foreach (DataGridItem Grilla in dtgMaestro.Items)
                        {
                            var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
                            lkbEliminar.Visible = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Nombre: Nuevo
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
        /// Modificacion:
        /// </summary>
        private void Nuevo()
        {
            int liHoraAct = 0;
            string lsHoraMax = "0";
            int liValFin = 0;
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
            if (lLector.HasRows)
            {
                lLector.Read();
                lsHoraMax = lLector["hora_max_declara_opera"].ToString();
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
            liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
            liValFin = ((Convert.ToDateTime(lsHoraMax).Hour * 60) + Convert.ToDateTime(lsHoraMax).Minute);
            if (liHoraAct > liValFin)
            {
                Toastr.Warning(this, "Está intentando Registrar Información Fuera del Horario.");
                //Cierra el modal de Agregar
                Modal.Cerrar(this, registroEnergiaTransferida.ID);
            }
            else
            {
                //Abre el modal de Agregar
                Modal.Abrir(this, registroEnergiaTransferida.ID, registroEnergiaTransferidaInside.ID);

                imbCrear.Visible = true;
                imbActualiza.Visible = false;
                lblTitulo.Text = "Crear " + lsTitulo;
                TxtCodigoCap.Visible = false;
                LblCodigoCap.Visible = true;
                TxtCodigoCap.Text = "0"; //20220705
                LblCodigoCap.Text = "Automatico";
                //ddlOperador.Enabled = true;
                ddlTrasportador.Enabled = true;
                ddlPunto.Enabled = true; //20160725 ajuste parametrizacion pozo 20160725
            }
            ddlEstado.SelectedValue = "A"; //20161122 modif inf ope
            ddlEstado.Enabled = false; //20161122 modif inf ope
        }
        /// <summary>
        /// Nombre: Listar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            CargarDatos();
            lblTitulo.Text = lsTitulo;
        }
        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            if (modificar != null && modificar != "")
            {
                try
                {
                    lblMensaje.Text = "";
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_energia_trasferida", " codigo_energia_trasferida= " + modificar + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            LblCodigoCap.Text = lLector["codigo_energia_trasferida"].ToString();
                            TxtCodigoCap.Text = lLector["codigo_energia_trasferida"].ToString();
                            DateTime ldFecha; //20160711 ajuste msificaciones
                            ldFecha = Convert.ToDateTime(lLector["fecha"].ToString()); //20160711 ajuste msificaciones
                            TxtFecha.Text = ldFecha.Year.ToString() + "/" + ldFecha.Month.ToString() + "/" + ldFecha.Day.ToString(); //20160711 ajuste msificaciones

                            try
                            {
                                ddlOperador.SelectedValue = lLector["codigo_Operador"].ToString();
                            }
                            catch (Exception)
                            {
                                lblMensaje.Text += "El operador del registro no existe o está inactivo<br>";
                            }
                            try
                            {
                                ddlTrasportador.SelectedValue = lLector["codigo_trasportador"].ToString();
                            }
                            catch (Exception)
                            {
                                lblMensaje.Text += "El transportador del registro no existe o está inactivo<br>";
                            }
                            //20160725 ajuste parametrizacion pozo 20160725
                            try
                            {
                                ddlPunto.SelectedValue = lLector["punto_trasferencia"].ToString();
                            }
                            catch (Exception)
                            {
                                lblMensaje.Text += "El punto de transferencia del registro no existe o está inactivo<br>";
                            }
                            TxtCantidad.Text = lLector["energia_trasferida"].ToString();
                            ddlEstado.SelectedValue = lLector["estado"].ToString();
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                            TxtCodigoCap.Visible = false;
                            LblCodigoCap.Visible = true;
                            TxtFecha.Enabled = false;
                            ddlOperador.Enabled = false;
                            //ddlTrasportador.Enabled = false;  //20161122 modif inf ope
                            //ddlPunto.Enabled = false;  //20161122 modif inf ope
                            ddlEstado.Enabled = true; //20161122 modif inf ope
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Bloquea el Registro a Modificar
                        if (lblMensaje.Text == "") //20160809 carga ptdv
                            manejo_bloqueo("A", modificar);
                    }
                    else
                    {
                        Listar();
                        lblMensaje.Text = "No se Puede editar el Registro porque está Bloqueado. Código energía transferida" + modificar.ToString();

                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = ex.Message;
                }
            }
            if (lblMensaje.Text == "")
            {
                //Abre el modal de Agregar
                Modal.Abrir(this, registroEnergiaTransferida.ID, registroEnergiaTransferidaInside.ID);
                lblTitulo.Text = "Modificar " + lsTitulo;
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }
        /// <summary>
        /// Nombre: Buscar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
        /// Modificacion:
        /// </summary>
        private void Buscar()
        {
            lblTitulo.Text = lsTitulo;
        }
        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_codigo_energia_trasferida", "@P_fecha", "@P_codigo_operador", "@P_fecha_fin", "@P_mostrar_eliminado" }; //20160721 modif inf opertiva y trans
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar }; //20160721 modif inf opertiva y trans
            string[] lValorParametros = { "0", "", "0", "", hdnELiminado.Value }; //20160721 modif inf opertiva y trans
            DateTime ldFecha;
            lblMensaje.Text = "";
            try
            {
                if (TxtBusFecha.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtBusFecha.Text.Trim());
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Valor Inválido en Fecha Inicial. <br>";
                    }
                }
                if (TxtBusFechaF.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtBusFechaF.Text.Trim());
                        if (ldFecha < Convert.ToDateTime(TxtBusFecha.Text.Trim()))
                            lblMensaje.Text += "La Fecha final NO puede ser menor que la Fecha Inicial.!<br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Valor Inválido en Fecha Final. <br>";
                    }
                }
                if (lblMensaje.Text == "")
                {
                    if (TxtBusCodigo.Text.Trim().Length > 0)
                        lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    if (TxtBusFecha.Text.Trim().Length > 0)
                        lValorParametros[1] = TxtBusFecha.Text.Trim();
                    else
                        lValorParametros[1] = ""; // 20181113 ajuste
                    if (ddlBusOperador.SelectedValue != "0")
                        lValorParametros[2] = ddlBusOperador.SelectedValue;
                    if (TxtBusFechaF.Text.Trim().Length > 0)
                        lValorParametros[3] = TxtBusFechaF.Text.Trim();
                    else
                    {
                        if (TxtBusFecha.Text.Trim().Length > 0)
                            lValorParametros[3] = TxtBusFecha.Text.Trim();
                        else
                            lValorParametros[3] = ""; // 20181113 ajuste
                    }
                    //20220621 ajuste
                    if (lValorParametros[0] == "0" && lValorParametros[1] == "")
                    {
                        lValorParametros[1] = TxtFecha.Text;
                        lValorParametros[3] = TxtFecha.Text;
                    }
                    lConexion.Abrir();
                    dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetEnergiaTras", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgMaestro.DataBind();
                    lConexion.Cerrar();
                    EstablecerPermisosSistema(); //20220621 ajuste
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }
        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            lblMensaje.Text = "";
            this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();

        }
        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            string lCodigoRegistro = "";
            lblMensaje.Text = "";
            string lsFecha = ""; // Cambio Req. 003-17 20170216
            hdfModificacion.Value = "N"; // Cambio Req. 003-17 20170216
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text; // Cambio Req. 003-17 20170216
            lsFecha = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[1].Text; // Cambio Req. 003-17 20170216
            ///////////////////////////////////////////////////////////////////////////////////
            ///// Control Nuevo para Modif Inf Operativa Participantes Req. 003-17 20170216 ///
            ///////////////////////////////////////////////////////////////////////////////////
            bool blPuede = true;
            int liHoraAct = 0;
            string lsHoraMax = "0";
            int liValFin = 0;
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
            if (lLector.HasRows)
            {
                lLector.Read();
                lsHoraMax = lLector["hora_max_declara_opera"].ToString();
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
            liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
            liValFin = ((Convert.ToDateTime(lsHoraMax).Hour * 60) + Convert.ToDateTime(lsHoraMax).Minute);
            if (liHoraAct > liValFin)
            {
                if (Session["tipoPerfil"].ToString() == "N")
                {
                    Toastr.Warning(this, "Está intentando modificar o eliminar información fuera del horario.");
                    blPuede = false;
                }
                else
                    hdfModificacion.Value = "S";
            }
            if (Convert.ToDateTime(lsFecha) != DateTime.Now.Date.AddDays(-1))
            {
                if (Session["tipoPerfil"].ToString() == "N")
                {
                    blPuede = false;
                    Toastr.Warning(this, "Está intentando modificar o eliminar información que no es del día anterior a la fecha del sistema.");
                }
                else
                    hdfModificacion.Value = "S";
            }
            if (blPuede)
            {
                if (e.CommandName.Equals("Modificar"))
                {
                    mdlRegistroEnergiaTransferidaLabel.InnerText = "Modificar";
                    Modificar(lCodigoRegistro);
                }
                // Evento Eliminar para los Participantes Modif Inf Operativa Participantes Req. 003-17 20170216
                if (e.CommandName.Equals("Eliminar"))
                {
                    string[] lsNombreParametros = { "@P_codigo_energia_trasferida", "@P_fecha", "@P_codigo_operador", "@P_punto_trasferencia", "@P_codigo_trasportador", "@P_energia_trasferida", "@P_estado", "@P_accion" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int }; //20160725 ajuste parametrizacion pozo 20160725
                    string[] lValorParametros = { lCodigoRegistro, "", "0", "0", "0", "0", "E", "3" };//20160725 ajuste parametrizacion pozo 20160725
                    lblMensaje.Text = "";
                    try
                    {
                        if (lblMensaje.Text == "")
                        {
                            lConexion.Abrir();
                            if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaTras", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                            {
                                lblMensaje.Text = "Se presentó un Problema en la Eliminación del Registro.! " + goInfo.mensaje_error.ToString();
                                lConexion.Cerrar();
                            }
                            else
                            {
                                Toastr.Success(this, "Registro Eliminado Correctamente");
                                Listar();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        /// Desbloquea el Registro Actualizado
                        lblMensaje.Text = ex.Message;
                    }
                    if (lblMensaje.Text != "")
                    {
                        Toastr.Warning(this, lblMensaje.Text);
                        lblMensaje.Text = "";
                    }
                }

            }
            ///////////////////////////////////////////////////////////////////////////////////
            //if (((LinkButton)e.CommandSource).Text == "Modificar")
            //{
            //    lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            //    Modificar(lCodigoRegistro);
            //}
        }
        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia("t_energia_trasferida", lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// 20180122 rq003-18
        /// // 20180126 rq107-16
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceLlave).ToString() + "-" + lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }


        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='t_energia_trasferida' and llave_registro='codigo_energia_trasferida=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_energia_trasferida=" + lscodigo_registro.ToString();
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_energia_trasferida";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_energia_trasferida", lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] lValorParametros = { "0", "", "0", "" };
            string lsParametros = "";

            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    lsParametros += " Código energía : " + TxtBusCodigo.Text;
                }
                if (TxtBusFecha.Text.Trim().Length > 0)
                {
                    lValorParametros[1] = TxtBusFecha.Text.Trim();
                    lsParametros += " Fecha Inicial : " + TxtBusFecha.Text;
                }
                else
                {
                    lValorParametros[1] = ""; //2018113 ajuste
                    //lsParametros += " Fecha Inicial : " + TxtBusFecha.Text;  //2018113 ajuste
                }
                if (ddlBusOperador.SelectedValue != "0")
                {
                    lValorParametros[2] = ddlBusOperador.SelectedValue;
                    lsParametros += " Operador: " + ddlBusOperador.SelectedItem;
                }
                if (TxtBusFechaF.Text.Trim().Length > 0)
                {
                    lValorParametros[3] = TxtBusFechaF.Text.Trim();
                    lsParametros += " Fecha Final : " + TxtBusFechaF.Text;
                }
                else
                {
                    if (TxtBusFecha.Text.Trim().Length > 0)
                    {
                        lValorParametros[3] = TxtBusFecha.Text.Trim();
                        lsParametros += " Fecha Final : " + TxtBusFecha.Text;
                    }
                    else
                    {
                        lValorParametros[3] = ""; //2018113 ajuste
                        //lsParametros += " Fecha Final : " + TxtBusFecha.Text;  //2018113 ajuste
                    }
                }
                //20220705
                if (lValorParametros[0] == "0" && lValorParametros[1] == "")
                {
                    lValorParametros[1] = TxtFecha.Text;
                    lValorParametros[2] = TxtFecha.Text;
                }
                Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_getEnergiaTras&nombreParametros=@P_codigo_energia_trasferida*@P_fecha*@P_codigo_operador*@P_fecha_fin&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "*" + lValorParametros[3] + "&columnas=codigo_energia_trasferida*fecha*codigo_operador*nombre_operador*codigo_tasportador*nombre_trsportador*punto_trasferencia*energia_trasferida*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado de energia transferida&TituloParametros=" + lsParametros);
            }
            catch (Exception)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.!";
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }

        /// <summary>
        /// Nombre: ImgPdf_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgPdf_Click(object sender, EventArgs e)
        {
            try
            {
                string lsCondicion = " codigo_energia_trasferida <> '0'";
                Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=t_energia_trasferida&procedimiento=pa_ValidarExistencia&columnas=codigo_energia_trasferida*fecha*codigo_operador*codigo_trasportador*punto_trasferencia*energia_trasferida*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
            }
            catch (Exception)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.!";
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_energia_trasferida", "@P_fecha", "@P_codigo_operador", "@P_punto_trasferencia", "@P_codigo_trasportador", "@P_energia_trasferida", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int }; //20160725 ajuste parametrizacion pozo 20160725
            string[] lValorParametros = { "0", "", "0", "0", "0", "0", "", "1" };//20160725 ajuste parametrizacion pozo 20160725
            lblMensaje.Text = "";
            int liValor = 0;
            try
            {
                if (TxtFecha.Text == "")
                    lblMensaje.Text += " Debe digitar la fecha<br>";
                if (ddlOperador.SelectedValue == "0")
                    lblMensaje.Text += "Debe seleccionar el operador<br>";
                if (ddlTrasportador.SelectedValue == "0")
                    lblMensaje.Text += "Debe seleccionar el transportador<br>";
                //20160725 ajuste parametrizacion pozo 20160725
                if (ddlPunto.SelectedValue == "0")
                    lblMensaje.Text += " Debe seleccionar el punto de trasferencia<br>";

                //20161122 modif inf ope
                if (VerificarExistencia("fecha ='" + TxtFecha.Text + "' and codigo_operador =" + ddlOperador.SelectedValue + " and punto_trasferencia =" + ddlPunto.SelectedValue + " and codigo_trasportador=" + ddlTrasportador.SelectedValue + " and estado <>'E'"))
                    lblMensaje.Text += " Ya está registrada la información de energía transferida<br>";
                if (TxtCantidad.Text == "")
                    lblMensaje.Text += " Debe digitar la cantidad transferida<br>";
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidad.Text.Trim());
                        if (liValor <= 0)
                            lblMensaje.Text += " Valor Inválido en la Cantidad transferida<br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += " Valor Inválido en la Cantidad transferida<br>";
                    }
                }
                //20220705
                string lsError = "N";
                if (lblMensaje.Text == "")
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_punto_reporte_cnt", " codigo_reporte = 8 and codigo_punto =" + ddlPunto.SelectedValue + " and estado ='A'", goInfo))
                        lblMensaje.Text += "El punto de trasferencia no tiene definida la cantidad máxima<br>";
                    else
                    {
                        if (validaCantidad())
                        {
                            lsError = "S";
                        }
                    }
                }
                if (lsError == "S" && lblMensaje.Text == "")
                {
                    Modal.Abrir(this, mdlConfirma.ID, mdlConfirmaInside.ID);
                }
                //20220705 fin

                if (lblMensaje.Text == "" && lsError == "N")//20220705
                {
                    lValorParametros[1] = TxtFecha.Text;
                    lValorParametros[2] = ddlOperador.SelectedValue;
                    lValorParametros[3] = ddlPunto.SelectedValue; //20160725 ajuste parametrizacion pozo 20160725
                    lValorParametros[4] = ddlTrasportador.SelectedValue;
                    lValorParametros[5] = TxtCantidad.Text;
                    lValorParametros[6] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaTras", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Creación de la energía transferida.!"; //20180126 rq107 - 16
                        lConexion.Cerrar();
                    }
                    else
                    {
                        ddlPunto.SelectedValue = "0";
                        ddlTrasportador.SelectedValue = "0";
                        TxtCantidad.Text = "";
                        //TxtBusFecha.Text = TxtFecha.Text; //20160711 ajuste msificaciones //20220621s
                        //Cierra el modal de Agregar
                        Modal.Cerrar(this, registroEnergiaTransferida.ID);
                        Listar();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbActualiza_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_energia_trasferida", "@P_energia_trasferida", "@P_estado", "@P_accion", "@P_punto_trasferencia", "@P_codigo_trasportador", //20161122 modif inf ope
                "@P_modificacion"  }; // Campo Nuevo Req. 003-17  20170216
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int,  //20161122 modif inf ope
                SqlDbType.VarChar  }; // Campo Nuevo Req. 003-17  20170216
            string[] lValorParametros = { "0", "", "", "2", "0", "0",  //20161122 modif inf ope
                hdfModificacion.Value  }; // Campo Nuevo Req. 003-17  20170216
            lblMensaje.Text = "";
            int liValor = 0;
            try
            {
                //20161122 modif inf ope
                if (ddlTrasportador.SelectedValue == "0")
                    lblMensaje.Text += "Debe seleccionar el transportador<br>";
                //20161122 modif inf ope
                if (ddlPunto.SelectedValue == "0")
                    lblMensaje.Text += " Debe seleccionar el punto de transferencia<br>";  //20180126 rq107 - 16
                //20161122 modif inf ope
                if (VerificarExistencia("fecha ='" + TxtFecha.Text + "' and codigo_operador =" + ddlOperador.SelectedValue + " and punto_trasferencia =" + ddlPunto.SelectedValue + " and codigo_trasportador=" + ddlTrasportador.SelectedValue + " and codigo_energia_trasferida <>" + LblCodigoCap.Text + " and estado <>'E'"))
                    lblMensaje.Text += " Ya está registrada la información de energía transferida<br>";

                if (TxtCantidad.Text == "")
                    lblMensaje.Text += " Debe digitar la cantidad transferida<br>";
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidad.Text.Trim());
                        if (liValor <= 0)
                            lblMensaje.Text += " Valor Inválido en la Cantidad transferida<br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += " Valor Inválido en la Cantidad transferida<br>";
                    }
                }
                //20220705
                string lsError = "N";
                if (lblMensaje.Text == "")
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_punto_reporte_cnt", " codigo_reporte = 8 and codigo_punto =" + ddlPunto.SelectedValue + " and estado ='A'", goInfo))
                        lblMensaje.Text += "El punto de trasferencia no tiene definida la cantidad máxima<br>";
                    else
                    {
                        if (validaCantidad())
                        {
                            lsError = "S";
                        }
                    }
                }
                if (lsError == "S" && lblMensaje.Text == "")
                {
                    Modal.Abrir(this, mdlConfirma.ID, mdlConfirmaInside.ID);
                }
                //20220705 fin

                if (lblMensaje.Text == "" && lsError == "N")//20220705
                {
                    lValorParametros[0] = LblCodigoCap.Text;
                    lValorParametros[1] = TxtCantidad.Text;
                    lValorParametros[2] = ddlEstado.SelectedValue;
                    lValorParametros[4] = ddlPunto.SelectedValue; //20161122 modif inf ope
                    lValorParametros[5] = ddlTrasportador.SelectedValue; //20161122 modif inf ope
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaTras", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Actualización de la cantidad transferida.!";
                        lConexion.Cerrar();
                    }
                    else
                    {
                        manejo_bloqueo("E", LblCodigoCap.Text);
                        //Cierra el modal de Agregar
                        Modal.Cerrar(this, registroEnergiaTransferida.ID);
                        Listar();
                    }
                }
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                manejo_bloqueo("E", LblCodigoCap.Text);
                lblMensaje.Text = ex.Message;
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            lblMensaje.Text = "";
            if (TxtBusCodigo.Text.Trim().Length <= 0 && TxtBusFecha.Text.Trim().Length <= 0 && ddlBusOperador.SelectedValue == "0")
                lblMensaje.Text = "Debe Seleccionar al Menos un parámetro para realizar la Búsqueda!.";
            else
            {
                Listar();
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }
        ///// Eventos Nuevos para la Implementracion del UserControl

        /// <summary>
        /// Metodo del Link Nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNuevo(object sender, EventArgs e)
        {
            mdlRegistroEnergiaTransferidaLabel.InnerText = "Agregar";
            Nuevo();
        }

        /// <summary>
        /// Metodo del Link Listar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnListar(object sender, EventArgs e)
        {
            Listar();
        }

        /// <summary>
        /// 
        /// </summary>
        protected void Cancel_OnClick(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (mdlRegistroEnergiaTransferidaLabel.InnerText.Equals("Modificar") && LblCodigoCap.Text != "")
                manejo_bloqueo("E", LblCodigoCap.Text);

            //Cierra el modal de Agregar
            Modal.Cerrar(this, registroEnergiaTransferida.ID);
        }

        protected void BtnAceptarConf_Click(object sender, EventArgs e)
        {
            Modal.Cerrar(this, mdlConfirma.ID);
            string[] lsNombreParametros = { "@P_codigo_energia_trasferida", "@P_fecha", "@P_codigo_operador", "@P_punto_trasferencia", "@P_codigo_trasportador", "@P_energia_trasferida", "@P_estado", "@P_accion", "@P_dato_atipico" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar }; //20160725 ajuste parametrizacion pozo 20160725
            string[] lValorParametros = { "0", "", "0", "0", "0", "0", "", "1", "S" };//20160725 ajuste parametrizacion pozo 20160725

            if (TxtCodigoCap.Text != "0")
            {
                lValorParametros[0] = TxtCodigoCap.Text;
                lValorParametros[7] = "2";
            }
            lValorParametros[1] = TxtFecha.Text;
            lValorParametros[2] = ddlOperador.SelectedValue;
            lValorParametros[3] = ddlPunto.SelectedValue; //20160725 ajuste parametrizacion pozo 20160725
            lValorParametros[4] = ddlTrasportador.SelectedValue;
            lValorParametros[5] = TxtCantidad.Text;
            lValorParametros[6] = ddlEstado.SelectedValue;
            lConexion.Abrir();
            if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaTras", lsNombreParametros, lTipoparametros, lValorParametros))
            {
                lblMensaje.Text = "Se presentó un Problema en la Creación de la energía transferida.!"; //20180126 rq107 - 16
                lConexion.Cerrar();
            }
            else
            {
                ddlPunto.SelectedValue = "0";
                ddlTrasportador.SelectedValue = "0";
                TxtCantidad.Text = "";
                //TxtBusFecha.Text = TxtFecha.Text; //20160711 ajuste msificaciones //20220621s
                //Cierra el modal de Agregar
                Modal.Cerrar(this, registroEnergiaTransferida.ID);
                Listar();
                if (LblCodigoCap.Text != "0")
                    manejo_bloqueo("E", LblCodigoCap.Text);
            }
        }
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool validaCantidad()
        {
            bool lbValida = false;
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_punto_reporte_cnt", " codigo_reporte = 8 and codigo_punto =" + ddlPunto.SelectedValue + " and cantidad_maxima < " + TxtCantidad.Text + "  and estado ='A'");
            if (lLector.HasRows)
            {
                lLector.Read();
                lblMensajeCOnf.Text = "La cantidad de energía a registrar de " + Convert.ToInt32(TxtCantidad.Text).ToString("###,###,###,###") + " MBTU supera el valor máximo histórico parametrizado para el punto de transferencia" + ddlPunto.SelectedItem.ToString() + " que es de " + Convert.ToInt32(lLector["cantidad_maxima"].ToString()).ToString("###,###,###,###") + " MBTU";
                lbValida = true;
            }
            lLector.Close();
            lConexion.Cerrar();
            return lbValida;
        }

    }
}