﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Verificacion
{
    public partial class frm_RegistroContResPtdvf : Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Registro de Contratos de reserva PTDVF/CIDVF";
        clConexion lConexion = null;
        clConexion lConexion1 = null;
        SqlDataReader lLector;
        DataSet lds = new DataSet();

        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo.ToString();
            lConexion = new clConexion(goInfo);
            lConexion1 = new clConexion(goInfo);
            //Titulo
            Master.Titulo = "Registro de Contratos de reserva PTDVF/CIDVF";

            if (!IsPostBack)
            {
                /// Llenar controles del Formulario
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador !=0 order by codigo_operador", 0, 4);
                LlenarControles(lConexion.gObjConexion, ddlEstado, "m_estado_gas", " tipo_estado = 'V' and sigla_estado <>'M' order by sigla_estado", 2, 3);  //20171130 rq026-17
                LlenarControles(lConexion.gObjConexion, ddlMercadoRel, "m_mercado_relevante", " estado ='A' order by descripcion", 0, 1);
                LlenarControles(lConexion.gObjConexion, ddlFuente, "m_pozo", " ind_campo_pto='C' and estado ='A' order by descripcion", 0, 1);
                //LlenarControles(lConexion.gObjConexion, dlTipoDemanda, "m_tipo_demanda_atender", " estado = 'A' order by descripcion", 0, 1);
                LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo sec, m_demanda_sector dem", " sec.estado ='A' and sec.codigo_sector_consumo = dem.codigo_sector_consumo  and dem.estado ='A' and dem.codigo_tipo_demanda =1 and dem.ind_uso ='T' order by descripcion", 0, 1); //20160706
                LlenarControles(lConexion.gObjConexion, ddlPuntoSalida, "m_punto_salida_snt", " estado = 'A'  order by descripcion", 0, 2);
                if (Session["tipoPerfil"].ToString() == "N")
                {
                    ddlOperador.SelectedValue = goInfo.cod_comisionista;
                    ddlOperador.Enabled = false;
                }
            }
        }
        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                }
                lDdl.Items.Add(lItem1);

            }
            lLector.Dispose();
            lLector.Close();
        }
        /// <summary>
        /// Nombre: dtgConsulta_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            lblMensaje.Text = "";
            this.dtgConsulta.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();

        }
        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            lblMensaje.Text = "";
            int liValor = 0;
            string[] lsNombreParametros = { "@P_numero_contrato_ini", "@P_numero_contrato_fin", "@P_codigo_operador", "@P_codigo_fuente", "@P_estado" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char };
            string[] lValorParametros = { "0", "0", "0", "0", "0" };

            if (TxtNoContrato.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoContrato.Text);
                }
                catch (Exception)
                {
                    lblMensaje.Text += "Formato Inválido en el Campo No. contrato inicial. <br>";
                }

            }
            if (TxtNoContratoFin.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoContratoFin.Text);
                }
                catch (Exception)
                {
                    lblMensaje.Text += "Formato Inválido en el Campo No. contrato final. <br>";
                }

            }
            if (TxtNoContrato.Text.Trim().Length == 0 && TxtNoContratoFin.Text.Trim().Length > 0)
            {
                lblMensaje.Text += "Debe digitar el No. de contrato inicial antes que el final. <br>";
            }
            if (TxtNoContrato.Text.Trim().Length > 0 && TxtNoContratoFin.Text.Trim().Length > 0)
            {
                try
                {
                    if (Convert.ToInt32(TxtNoContratoFin.Text) < Convert.ToInt32(TxtNoContrato.Text))
                        lblMensaje.Text += "El No. de contrato inicial debe ser menor o igual que el final. <br>";
                }
                catch (Exception)
                {

                }
            }
            if (lblMensaje.Text == "")
            {
                try
                {
                    if (TxtNoContrato.Text.Trim().Length > 0)
                    {
                        lValorParametros[0] = TxtNoContrato.Text.Trim();
                        lValorParametros[1] = TxtNoContrato.Text.Trim();
                    }
                    if (TxtNoContratoFin.Text.Trim().Length > 0)
                        lValorParametros[1] = TxtNoContratoFin.Text.Trim();
                    if (ddlOperador.SelectedValue != "0")
                        lValorParametros[2] = ddlOperador.SelectedValue;
                    if (ddlFuente.SelectedValue != "0")
                        lValorParametros[3] = ddlFuente.SelectedValue;
                    if (ddlEstado.SelectedValue != "0")
                        lValorParametros[4] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetReservaPtdvfAdjVer", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgConsulta.DataBind();
                    dtgExcel.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetReservaPtdvfAdjVer", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgExcel.DataBind();
                    lConexion.Cerrar();
                    if (dtgConsulta.Items.Count > 0)
                    {
                        tblGrilla.Visible = true;
                        lkbExcel.Visible = true;
                    }
                    else
                    {
                        tblGrilla.Visible = false;
                        lkbExcel.Visible = false;
                        lblMensaje.Text = "No se encontraron Registros.";
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString();
                }
            }
        }
        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
        {
            lblMensaje.Text = "";
            if (((LinkButton)e.CommandSource).Text == "Seleccionar")
            {
                string lsError = "N";
                try
                {
                    limpiarDatosUsu();
                    lblcontrato.Text = e.Item.Cells[0].Text;
                    hdfCodigoVer.Value = e.Item.Cells[18].Text;
                    //valida que se pueda crear o modificar el registro
                    if (VerificarExistencia("t_contrato_reserva_ptdvf", " numero_contrato = " + lblcontrato.Text + " and fecha_max_registro <= getdate()"))
                        lblMensaje.Text += "Ya se venció el plazo para el registro del contrato<br>";
                    if (VerificarExistencia("t_contrato_reserva_ptdvf", " numero_contrato = " + lblcontrato.Text + " and estado ='R'"))
                        lblMensaje.Text += "El contrato ya está registrado<br>";
                    if (Session["tipoPerfil"].ToString() != "N")
                    {
                        lsError = "S";
                        CargarDatosUsu();
                    }
                    if (e.Item.Cells[17].Text == "N")
                    {
                        btnActualizar.Visible = false;
                        btnCrear.Visible = true;
                    }
                    else
                    {
                        btnActualizar.Visible = true;
                        btnCrear.Visible = false;
                    }
                    TxtNumContrato.Enabled = true;
                    if (e.Item.Cells[15].Text != "&nbsp;")
                        TxtNumContrato.Text = e.Item.Cells[15].Text;
                    else
                        TxtNumContrato.Text = "";
                    TxtFechaSus.Enabled = true;
                    if (e.Item.Cells[16].Text != "&nbsp;")
                        TxtFechaSus.Text = e.Item.Cells[16].Text;
                    else
                        TxtFechaSus.Text = "";
                    lblTotlCantidad.Text = "0";

                    hdfPunta.Value = e.Item.Cells[13].Text;
                    lblFechaNeg.Text = e.Item.Cells[1].Text;
                    lblVendedor.Text = e.Item.Cells[2].Text + "-" + e.Item.Cells[3].Text;
                    lblComprador.Text = e.Item.Cells[4].Text + "-" + e.Item.Cells[5].Text;
                    lblFuente.Text = e.Item.Cells[6].Text + "-" + e.Item.Cells[7].Text;
                    lblCantidad.Text = e.Item.Cells[8].Text;
                    hdfCantidad.Value = e.Item.Cells[14].Text;
                    if (lblMensaje.Text != "")
                    {
                        btnCrear.Visible = false;
                        btnActualizar.Visible = false;
                        btnCrearUsu.Visible = false;
                        btnActualUsu.Visible = false;
                        dtgUsuarios.Columns[8].Visible = false;
                        dtgUsuarios.Columns[9].Visible = false;
                        lsError = "S";
                    }
                    else
                    {
                        dtgUsuarios.Columns[8].Visible = true;
                        dtgUsuarios.Columns[9].Visible = true;
                    }

                    if (hdfPunta.Value == "C")
                    {

                        hdfPunta.Value = "C";
                        lblPunta.Text = "COMPRADOR";
                        tblDemanda.Visible = true;
                        if (lsError == "N")
                            btnCrearUsu.Visible = true;
                        CargarDatosUsu();
                    }
                    if (hdfPunta.Value == "V")
                    {
                        hdfPunta.Value = "V";
                        lblPunta.Text = "VENDEDOR";
                        tblDemanda.Visible = false;
                        btnCrearUsu.Visible = false;
                        btnActualUsu.Visible = false;
                    }

                    if (hdfPunta.Value == "C")
                    {
                        string[] lsNombreParametrosC = { "@P_cadena" };
                        SqlDbType[] lTipoparametrosC = { SqlDbType.VarChar };
                        string[] lValorParametrosC = { " Delete from t_contrato_reserva_ptdvf_usr where codigo_verif = 0 And login_usuario = '" + goInfo.Usuario.ToString() + "' " };
                        /// Se obtiene los Datos de la Punta Compradora
                        DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_EjecutarCadena", lsNombreParametrosC, lTipoparametrosC, lValorParametrosC);
                    }
                    tblGrilla.Visible = false;
                    tblRegCnt.Visible = true;
                    tblDatos.Visible = false;
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = "Problemas en la Recuperación de la Información. " + ex.Message.ToString();

                }
            }
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_operador)
        {
            string lsCondicion = "nombre_tabla='t_contrato_reserva_ptdvf_ver' and llave_registro='codigo_verif=" + hdfCodigoVer.Value + "'";
            string lsCondicion1 = "codigo_verif =" + hdfCodigoVer.Value;
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_contrato_reserva_ptdvf_ver";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
            }
            return true;
        }
        /// <summary>
        /// Realiza las Validaciones de los Tipos de Datos y Campos Obligatorios de la Pantalla
        /// </summary>
        /// <returns></returns>
        protected string validaciones()
        {
            DateTime lFecha;
            string lsError = "";
            string[] lsFecha;

            try
            {
                if (TxtNumContrato.Text.Trim().Length <= 0)
                    lsError += "Debe Ingresar el No. de Contrato. <br>";
                if (TxtFechaSus.Text.Trim().Length <= 0)
                    lsError += "Debe Ingresar la Fecha de Suscripción. <br>";
                else
                {
                    try
                    {
                        lFecha = Convert.ToDateTime(TxtFechaSus.Text.Trim());
                        lsFecha = TxtFechaSus.Text.Trim().Split('/');
                        if (lsFecha[0].Trim().Length != 4)
                            lsError += "Formato Inválido en Fecha de Suscripción. <br>";
                        else
                        {
                            if (lFecha < Convert.ToDateTime(lblFechaNeg.Text))
                                lsError += "La fecha de suscripción no puede ser menor que la fecha de negociación. <br>";
                            if (lFecha > DateTime.Now)
                                lsError += "La fecha de suscripción no puede ser mayor que la fecha actual. <br>";
                        }
                    }
                    catch (Exception)
                    {
                        lsError += "Valor Inválido en Fecha de Suscripción. <br>";
                    }
                }
                //Valida detalle de usuarios finales
                if (hdfPunta.Value == "C")
                {
                    if (lblTotlCantidad.Text != hdfCantidad.Value)
                        lsError += "La cantidad del detalle de usuarios finales no es igual a la cantidad total del contrato. <br>";
                }
                return lsError;
            }
            catch (Exception ex)
            {
                return "Problemas en la Validación de la Información. " + ex.Message.ToString();
            }
        }
        /// <summary>
        /// Nombre: dtgUsuarios_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgUsuarios_EditCommand(object source, DataGridCommandEventArgs e)
        {
            lblMensaje.Text = "";

            if (((LinkButton)e.CommandSource).Text == "Modificar")
            {
                try
                {
                    ////////////////////////////////////////////////////////////////////////////////////////
                    /// OJO Cambios de los Indices por adicion de Campo Req. 009-17 Indicadores 20170321 ///
                    ////////////////////////////////////////////////////////////////////////////////////////
                    string lsTipoDoc = "";
                    if (this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[14].Text == "1")  //rq009-17
                        lsTipoDoc = "Nit";
                    else
                        lsTipoDoc = "Cédula";
                    hdfCodDatUsu.Value = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text; //rq009-17
                    //Ajuste para que no muestre infrmacion mala 20160621
                    if (this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[1].Text == "&nbsp;")
                        TxtUsuarioFinal.Text = lsTipoDoc;
                    else
                        TxtUsuarioFinal.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[1].Text + "-" + lsTipoDoc + "-" + this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[3].Text;
                    TxtCantidadUsu.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[6].Text;
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) - Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                    dlTipoDemanda.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[15].Text; //rq009-17
                    dlTipoDemanda_SelectedIndexChanged(null, null);
                    ddlSector.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[12].Text; //rq009-17
                    ddlMercadoRel.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[16].Text; //rq009-17
                    ddlPuntoSalida.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[13].Text; //rq009-17
                    btnCrearUsu.Visible = false;
                    btnActualUsu.Visible = true;
                    TxtEquivaleKpcd.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[7].Text;
                    TdIndica.Visible = true;
                    //TdIndica1.Visible = true;
                    btnActualUsu.Visible = true;
                    btnCrearUsu.Visible = false;
                    hdfCodigoVerUsr.Value = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text;
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = "Problemas al Recuperar el Registro.! " + ex.Message.ToString();
                }

            }
            if (((LinkButton)e.CommandSource).Text == "Eliminar")
            {
                string[] lsNombreParametros = { "@P_codigo_verif_usr", "@P_accion" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { hdfCodDatUsu.Value, "3" };

                try
                {
                    hdfCodDatUsu.Value = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[10].Text;
                    lValorParametros[0] = hdfCodDatUsu.Value;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContResUsuFin", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Eliminación de la Información del Usuario.!" + goInfo.mensaje_error.ToString();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) - Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                        lConexion.Cerrar();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Eliminado Correctamente.!" + "');", true);
                        CargarDatosUsu();
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = "Problemas al Eliminar el Registro.! " + ex.Message.ToString();

                }
            }

        }
        /// <summary>
        /// Nombre: CargarDatosUsu
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid de Usuarios.
        /// Modificacion:
        /// </summary>
        private void CargarDatosUsu()
        {
            string[] lsNombreParametros = { "@P_codigo_verif", "@P_no_identificacion_usr", "@P_login" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "-1", "", "" };
            try
            {
                if (hdfCodigoVer.Value.Trim().Length > 0)
                    lValorParametros[0] = hdfCodigoVer.Value;
                if (Session["tipoPerfil"].ToString() == "N")
                    lValorParametros[2] = goInfo.Usuario.ToString();
                lConexion.Abrir();
                dtgUsuarios.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContResUsuFin", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgUsuarios.DataBind();
                lConexion.Cerrar();
                lblTotlCantidad.Text = "0";
                if (dtgUsuarios.Items.Count > 0)
                {
                    foreach (DataGridItem Grilla in this.dtgUsuarios.Items)
                    {
                        lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(Grilla.Cells[6].Text.Trim())).ToString();
                    }
                    dtgUsuarios.Columns[7].Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dlTipoDemanda_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hdfTipoDemanda.Value = dlTipoDemanda.SelectedValue;
                if (hdfTipoDemanda.Value == "1")
                {
                    TxtUsuarioFinal.Enabled = false;
                    ddlMercadoRel.Enabled = true; //20160303
                    lblSector.Text = "Sector de Consumo Usuario Regulado";
                    lblCantContra.Text = "Cantidad a entregar";
                }
                else
                {
                    TxtUsuarioFinal.Enabled = true;
                    ddlMercadoRel.Enabled = false; //20160303
                    ddlMercadoRel.SelectedValue = "0"; //20160303
                    lblCantContra.Text = "Cantidad Contratada por usuario (MBTUD)";
                    lblSector.Text = "Sector Consumo Usuario No Regulado";
                }
                //lConexion.Abrir();
                //ddlSector.Items.Clear();
                //ddlPuntoSalida.Items.Clear();
                //LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo sec, m_demanda_sector dem", " sec.estado ='A' and sec.codigo_sector_consumo = dem.codigo_sector_consumo  and dem.estado ='A' and dem.codigo_tipo_demanda =" + dlTipoDemanda.SelectedValue + "  and dem.ind_uso ='T' order by descripcion", 0, 1); //20160706
                //LlenarControles(lConexion.gObjConexion, ddlPuntoSalida, "m_punto_salida_snt", " estado = 'A'  order by descripcion", 0, 2);
                //lConexion.Cerrar();
            }
            catch (Exception)
            {

            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCrearUsu_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_verif_usr", "@P_codigo_verif","@P_numero_contrato", "@P_no_identificacion_usr", "@P_codigo_tipo_doc",
                "@P_nombre_usuario", "@P_codigo_sector_consumo", "@P_codigo_punto_salida","@P_cantidad_contratada","@P_tipo_demanda",
                "@P_codigo_mercado_relevante","@P_equivalente_kpcd", "@P_accion"
            };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,
                SqlDbType.VarChar, SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,
                SqlDbType.Int, SqlDbType.Int, SqlDbType.Int,
            };
            string[] lValorParametros = { "0", "0", "0", "", "", "", "0", " 0", "0", "0", "0","0","1"
            };

            lblMensaje.Text = "";
            string[] lsUsuario;
            string lsNombre = "";
            string lsTipoDoc = "";
            int liValor = 0;
            try
            {
                lConexion.Abrir();
                lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
                if (dlTipoDemanda.SelectedValue == "0")
                    lblMensaje.Text += "Debe Seleccionar el Tipo de Demanda a Atender. <br> ";
                else
                {
                    if (dlTipoDemanda.SelectedValue == "2")
                    {
                        if (TxtUsuarioFinal.Text.Trim().Length <= 0)
                            lblMensaje.Text = "Debe Ingresar el Usuario Final <br>";
                        else
                        {
                            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                            if (!lLector.HasRows)
                                lblMensaje.Text += "El Usuario Ingresado NO existe en la Base de Datos.! <br>";
                            else
                            {
                                lLector.Read();
                                lsNombre = lLector["nombre"].ToString();
                                lsTipoDoc = lLector["codigo_tipo_doc"].ToString();
                            }
                            lLector.Close();
                            lLector.Dispose();
                        }
                    }
                    else
                    {
                        if (ddlMercadoRel.SelectedValue == "0")
                            lblMensaje.Text += "Debe Seleccionar el mercado relevante. <br> ";
                    }
                }
                if (ddlSector.SelectedValue == "0")
                    lblMensaje.Text += "Debe Seleccionar " + lblSector.Text + ". <br> ";
                if (ddlPuntoSalida.SelectedValue == "0")
                    lblMensaje.Text += "Debe Seleccionar  Punto de Salida en SNT. <br> ";
                if (TxtCantidadUsu.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar " + lblCantContra.Text + ". <br> ";
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidadUsu.Text.Trim());
                        if (liValor < 1)
                            lblMensaje.Text += "Valor Inválido " + lblCantContra.Text + ". <br> ";
                        else
                        {
                            if (liValor > Convert.ToDecimal(hdfCantidad.Value.Trim()))
                                lblMensaje.Text += "La " + lblCantContra.Text + " No puede ser Mayor que " + hdfCantidad.Value + ". <br> ";
                            else
                            {
                                if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(hdfCantidad.Value))
                                    lblMensaje.Text += "La cantidad Acumulada de Usuarios No puede ser Mayor que " + hdfCantidad.Value + ". <br> ";
                            }
                        }
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Valor Inválido " + lblCantContra.Text + ". <br> ";
                    }
                }
                if (TxtEquivaleKpcd.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Equivalente Kpcd. <br> ";
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtEquivaleKpcd.Text.Trim());
                        if (liValor < 0)
                            lblMensaje.Text += "Valor Inválido Equivalente Kpcd. <br> ";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Valor Inválido " + lblCantContra.Text + ". <br> ";
                    }
                }
                if (lblMensaje.Text == "")
                {
                    if (hdfCodigoVer.Value != "")
                        lValorParametros[1] = hdfCodigoVer.Value;
                    if (hdfTipoDemanda.Value == "2")
                    {
                        lValorParametros[3] = lsUsuario[0].Trim();
                        lValorParametros[4] = lsTipoDoc;
                        lValorParametros[5] = lsNombre;
                    }
                    lValorParametros[6] = ddlSector.SelectedValue;
                    lValorParametros[7] = ddlPuntoSalida.SelectedValue;
                    lValorParametros[8] = TxtCantidadUsu.Text.Trim();
                    lValorParametros[9] = dlTipoDemanda.SelectedValue;
                    lValorParametros[10] = ddlMercadoRel.SelectedValue;
                    lValorParametros[11] = TxtEquivaleKpcd.Text.Trim();

                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContResusuFin", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Creación de la Información del Usuario.!" + goInfo.mensaje_error.ToString();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(TxtCantidadUsu.Text.Trim())).ToString();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Información de Usuarios Finales Ingresada Correctamente.!" + "');", true);
                        TxtUsuarioFinal.Text = "";
                        ddlSector.SelectedValue = "0";
                        ddlPuntoSalida.SelectedValue = "0";
                        ddlMercadoRel.SelectedValue = "0";
                        TxtCantidadUsu.Text = "0";
                        TxtEquivaleKpcd.Text = "0"; // Campos nuevos Req. 009-17 Indicadores 20170321
                        CargarDatosUsu();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnActualUsu_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_verif_usr", "@P_codigo_verif","@P_numero_contrato", "@P_no_identificacion_usr", "@P_codigo_tipo_doc",
                "@P_nombre_usuario", "@P_codigo_sector_consumo", "@P_codigo_punto_salida","@P_cantidad_contratada","@P_tipo_demanda",
                "@P_codigo_mercado_relevante","@P_equivalente_kpcd", "@P_accion"
            };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,
                SqlDbType.VarChar, SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,
                SqlDbType.Int, SqlDbType.Int, SqlDbType.Int,
            };
            string[] lValorParametros = { "0", "0", "0", "", "", "", "0", " 0", "0", "0", "0","0","2"
            };
            lblMensaje.Text = "";
            string[] lsUsuario;
            int liValor = 0;
            string lsNombre = "";
            string lsTipoDoc = "";
            try
            {
                lConexion.Abrir();
                lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
                if (dlTipoDemanda.SelectedValue == "0")
                    lblMensaje.Text += "Debe Seleccionar el Tipo Demanda Atender. <br> ";
                else
                {
                    if (hdfTipoDemanda.Value == "2")
                    {
                        if (TxtUsuarioFinal.Text.Trim().Length <= 0)
                            lblMensaje.Text = "Debe Ingresar el Usuario Final <br>";
                        else
                        {
                            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                            if (!lLector.HasRows)
                                lblMensaje.Text += "El Usuario Ingresado NO existe en la Base de Datos.! <br>";
                            else
                            {
                                lLector.Read();
                                lsNombre = lLector["nombre"].ToString();
                                lsTipoDoc = lLector["codigo_tipo_doc"].ToString();
                            }
                            lLector.Close();
                            lLector.Dispose();
                        }
                    }
                    else
                    {
                        if (ddlMercadoRel.SelectedValue == "0")
                            lblMensaje.Text += "Debe Seleccionar el mercado relevante. <br> ";
                    }

                }
                if (ddlSector.SelectedValue == "0")
                    lblMensaje.Text += "Debe Seleccionar " + lblSector.Text + ". <br> ";
                if (ddlPuntoSalida.SelectedValue == "0")
                    lblMensaje.Text += "Debe Seleccionar  Punto de Salida en SNT. <br> ";
                if (TxtCantidadUsu.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar " + lblCantContra.Text + ". <br> ";
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidadUsu.Text.Trim());
                        if (liValor < 1)
                            lblMensaje.Text += "Valor Inválido " + lblCantContra.Text + ". <br> ";
                        else
                        {
                            if (liValor > Convert.ToDecimal(hdfCantidad.Value.Trim()))
                                lblMensaje.Text += "La " + lblCantContra.Text + " No puede ser Mayor que " + hdfCantidad.Value + ". <br> ";
                            else
                            {
                                if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(hdfCantidad.Value))
                                    lblMensaje.Text += "La Cantidad Acumulada de Usuarios No puede ser Mayor que " + hdfCantidad.Value + ". <br> ";
                            }
                        }
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Valor Inválido " + lblCantContra.Text + ". <br> ";
                    }
                }
                if (TxtEquivaleKpcd.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar el Equivalente Kpcd. <br> ";
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtEquivaleKpcd.Text.Trim());
                        if (liValor < 0)
                            lblMensaje.Text += "Valor Inválido en Equivalente Kpcd. <br> ";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Valor Inválido en " + lblCantContra.Text + ". <br> ";
                    }
                }
                if (lblMensaje.Text == "")
                {
                    if (hdfCodigoVer.Value != "")
                        lValorParametros[0] = hdfCodigoVerUsr.Value;
                    if (hdfTipoDemanda.Value == "2")
                    {
                        lValorParametros[3] = lsUsuario[0].Trim();
                        lValorParametros[4] = lsTipoDoc;
                        lValorParametros[5] = lsNombre;
                    }
                    lValorParametros[6] = ddlSector.SelectedValue;
                    lValorParametros[7] = ddlPuntoSalida.SelectedValue;
                    lValorParametros[8] = TxtCantidadUsu.Text.Trim();
                    lValorParametros[9] = dlTipoDemanda.SelectedValue;
                    lValorParametros[10] = ddlMercadoRel.SelectedValue;
                    lValorParametros[11] = TxtEquivaleKpcd.Text.Trim();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContResusuFin", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Actualización de la Información del Usuario.! " + goInfo.mensaje_error.ToString();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(TxtCantidadUsu.Text)).ToString();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Información de Usuarios Finales Actualizada Correctamente.!" + "');", true);
                        TxtUsuarioFinal.Text = "";
                        ddlSector.SelectedValue = "0";
                        ddlPuntoSalida.SelectedValue = "0";
                        ddlMercadoRel.SelectedValue = "0";
                        TxtCantidadUsu.Text = "0";
                        TxtEquivaleKpcd.Text = "0"; // Campos nuevos Req. 009-17 Indicadores 20170321
                        btnCrearUsu.Visible = true;
                        btnActualUsu.Visible = false;
                        CargarDatosUsu();
                        btnActualUsu.Visible = false;
                        btnCrearUsu.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPuntoEntr_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (hdfPunta.Value == "C")
            {
                lConexion.Abrir();
                ddlPuntoSalida.Items.Clear();
                LlenarControles(lConexion.gObjConexion, ddlPuntoSalida, "m_punto_salida_snt", " estado = 'A' order by descripcion", 0, 2);
                lConexion.Cerrar();
            }
        }
        protected void btnCrear_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_verif", "@P_numero_contrato", "@P_contrato_definitivo", "@P_punta", "@P_fecha_suscripcion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "0", "", "", "" };
            lblMensaje.Text = "";
            try
            {
                lblMensaje.Text = validaciones();
                if (lblMensaje.Text == "")
                {
                    lValorParametros[0] = "0";
                    lValorParametros[1] = lblcontrato.Text;
                    lValorParametros[2] = TxtNumContrato.Text.Trim().ToUpper();
                    lValorParametros[3] = hdfPunta.Value;
                    lValorParametros[4] = TxtFechaSus.Text.Trim();
                    lConexion.Abrir();
                    goInfo.mensaje_error = "";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetRegistroContResPtdvf", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Creación del Registro del Contrato.! " + goInfo.mensaje_error.ToString();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                                lblMensaje.Text += lLector["error"].ToString() + "<br>";

                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Ingresado Correctamente.!" + "');", true);
                            tblRegCnt.Visible = false;
                            tblGrilla.Visible = true;
                            tblDatos.Visible = true;
                            tblDemanda.Visible = false;
                            CargarDatos();
                        }
                        lConexion.Cerrar();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_verif", "@P_numero_contrato", "@P_contrato_definitivo", "@P_punta", "@P_fecha_suscripcion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "0", "", "", "" }; lblMensaje.Text = "";
            try
            {
                lblMensaje.Text = validaciones();
                if (lblMensaje.Text == "")
                {
                    lValorParametros[0] = hdfCodigoVer.Value;
                    lValorParametros[1] = lblcontrato.Text;
                    lValorParametros[2] = TxtNumContrato.Text.Trim().ToUpper();
                    lValorParametros[3] = hdfPunta.Value;
                    lValorParametros[4] = TxtFechaSus.Text.Trim();
                    lConexion.Abrir();
                    goInfo.mensaje_error = "";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetRegistroContResPtdvf", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Creación del Registro del Contrato.! " + goInfo.mensaje_error.ToString();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Ingresado Correctamente.!" + "');", true);
                        tblRegCnt.Visible = false;
                        tblGrilla.Visible = true;
                        tblDatos.Visible = true;
                        tblDemanda.Visible = false;
                        CargarDatos();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            tblGrilla.Visible = true;
            tblRegCnt.Visible = false;
            tblDatos.Visible = true;
            tblDemanda.Visible = false;
            TxtNumContrato.Text = "";
            TxtFechaSus.Text = "";
            ddlFuente.SelectedValue = "0";
        }
        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lsTable, string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia(lsTable, lswhere, goInfo);
        }
        /// <summary>
        /// Nombre: limpiarDatosUsu
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected void limpiarDatosUsu()
        {
            dlTipoDemanda.SelectedValue = "1";
            TxtUsuarioFinal.Text = "";
            ddlSector.SelectedValue = "0";
            ddlPuntoSalida.SelectedValue = "0";
            ddlMercadoRel.SelectedValue = "0";
            TxtCantidadUsu.Text = "";
            TxtEquivaleKpcd.Text = "";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            CargarDatos();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lkbExcel_Click(object sender, EventArgs e)
        {
            lblMensaje.Text = "";
            try
            {
                string lsNombreArchivo = Session["login"] + "InfResPtdvf" + DateTime.Now + ".xls";
                StringBuilder lsb = new StringBuilder();
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                lpagina.EnableEventValidation = false;
                lpagina.Controls.Add(lform);
                dtgExcel.EnableViewState = false;
                dtgExcel.Visible = true;
                lform.Controls.Add(dtgExcel);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = System.Text.Encoding.Default;

                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + "Consulta Registro de Contratos de reserva PTDVF/CIDVF" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write(lsb.ToString());
                Response.End();
                Response.Flush();
                dtgExcel.Visible = true;
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al Consultar los Registros. " + ex.Message.ToString();
            }
        }
    }
}