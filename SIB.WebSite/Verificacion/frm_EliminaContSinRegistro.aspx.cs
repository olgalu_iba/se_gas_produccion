﻿using System;
using System.Collections;
using System.Globalization;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.DataVisualization.Charting;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Verificacion_frm_EliminaContSinRegistro : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Eliminación de contratos sin registro"; //20190607 rq036-19
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    string sRutaArc = ConfigurationManager.AppSettings["rutaCarga"].ToString();
    string[] lsCarperta;
    string Carpeta;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            //lConexion.Abrir();
            //LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador !=0 order by codigo_operador", 0, 4);
            //lConexion.Cerrar();

        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        CargarDatos();
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        lblMensaje.Text = "";
        int liValor = 0;
        string[] lsNombreParametros = { "@P_fecha_negociacion", "@P_numero_contrato", "@P_codigo_verif_contrato", "@P_contrato_definitivo" };
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
        string[] lValorParametros = { "", "0", "0", "" };


        if (lblMensaje.Text == "")
        {
            try
            {
                if (TxtFecha.Text != "")
                    lValorParametros[0] = TxtFecha.Text;
                if (TxtNoContrato.Text.Trim().Length > 0)
                    lValorParametros[1] = TxtNoContrato.Text;
                if (TxtNoIdRegistro.Text.Trim().Length > 0)
                    lValorParametros[2] = TxtNoIdRegistro.Text.Trim();
                if (TxtContDef.Text != "")
                    lValorParametros[3] = TxtContDef.Text;

                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContSinRegistro", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                lConexion.Cerrar();
                if (dtgConsulta.Items.Count > 0)
                {
                    tblGrilla.Visible = true;
                    imbExcel.Visible = true;

                }
                else
                {
                    tblGrilla.Visible = false;
                    imbExcel.Visible = false;
                    lblMensaje.Text = "No se encontraron Registros.";
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Exportacion a Excel de la Información  de la Grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbExcel_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        string lsParametros = "";
        if (TxtFecha.Text != "")
            lsParametros = " - Fecha de Negociación = " + TxtFecha.Text;
        if (TxtNoContrato.Text.Trim().Length > 0)
            lsParametros = " - Número Operación = " + TxtNoContrato.Text; //20190607 rq036-19
        if (TxtNoIdRegistro.Text.Trim().Length > 0)
            lsParametros = " - Id registro = " + TxtNoIdRegistro.Text.Trim();
        if (TxtContDef.Text != "")
            lsParametros = " Contrato Definitivo = " + TxtContDef.Text;
        try
        {

            string lsNombreArchivo = Session["login"] + "InfExcelReg" + DateTime.Now + ".xls";
            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            lpagina.EnableEventValidation = false;
            lpagina.Controls.Add(lform);
            dtgConsulta.EnableViewState = false;
            dtgConsulta.Columns[15].Visible = false;
            dtgConsulta.Columns[16].Visible = false; //20190607 rq036-19
            lform.Controls.Add(dtgConsulta);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;

            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
            //20170601 rq020-17
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=4>" + "Consulta Contratos sin registro para eliminación" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=4> Parametros: " + lsParametros + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write(lsb.ToString());
            Response.End();
            Response.Flush();
            dtgConsulta.Columns[15].Visible = true;
            dtgConsulta.Columns[16].Visible = true; //20190607 rq036-19
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas al Consultar los Registros. " + ex.Message.ToString();
        }
    }


    /// <summary>
    /// Nombre: dtgReqHab_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos Visualizacion de Imagen de la Grilla de Requisitos
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
    {
        //20190607 rq036-19
        lblMensaje.Text = "";
        DropDownList oDrop;
        oDrop = (DropDownList)this.dtgConsulta.Items[e.Item.ItemIndex].Cells[15].Controls[1];
        if (oDrop.SelectedValue == "")
            lblMensaje.Text = "Debe seleccionar la punta a la que le elimina la información";
        if (lblMensaje.Text =="")
        {
            //20190607 fin rq036-19
            try
            {
                string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_punta" }; //20190607 rq036-19
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar  }; //20190607 rq036-19
                string[] lValorParametros = { this.dtgConsulta.Items[e.Item.ItemIndex].Cells[1].Text, oDrop.SelectedValue  }; // Aprobacion del Operador //20190607 rq036-19
                lblMensaje.Text = "";
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetElimContSinReg", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Text = lLector["error"].ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro eliminado correctamente." + "');", true);
                    CargarDatos();
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al eliminar el registro. " + ex.Message.ToString();
            }

        }//20190607 rq036-19
    }

}