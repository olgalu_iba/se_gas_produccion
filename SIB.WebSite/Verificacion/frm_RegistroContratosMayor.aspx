﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_RegistroContratosMayor.aspx.cs" Inherits="Verificacion_frm_RegistroContratosMayor" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .style1 {
            color: #000000;
            font-family: Tahoma;
            font-size: 11px;
            border-right: 1px solid #C1DAD7;
            border-bottom: 1px solid #C1DAD7;
            background: #fff;
            padding: 6px 6px 6px 12px;
            color: #4f6b72;
            height: 28px;
        }

        .style2 {
            color: #000000;
            font-family: Tahoma;
            font-size: 11px;
            border-right: 1px solid #C1DAD7;
            border-bottom: 1px solid #C1DAD7;
            background: #F5FAFA;
            color: #797268;
            padding: 6px 6px 6px 12px;
            height: 28px;
        }

        .style3 {
            color: #000000;
            font-family: Tahoma;
            font-size: 11px;
            border-right: 1px solid #C1DAD7;
            border-bottom: 1px solid #C1DAD7;
            background: #fff;
            padding: 6px 6px 6px 12px;
            color: #4f6b72;
            height: 37px;
        }

        .style4 {
            color: #000000;
            font-family: Tahoma;
            font-size: 11px;
            border-right: 1px solid #C1DAD7;
            border-bottom: 1px solid #C1DAD7;
            background: #F5FAFA;
            color: #797268;
            padding: 6px 6px 6px 12px;
            height: 37px;
        }
    </style>
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body" runat="server" id="tblDatos">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <%--20161213 rq102--%>
                        <div class="form-group">
                            <label>Fecha Negociación Ini.</label>
                            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fecha Negociación Fin</label>
                            <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Mercado</label>
                            <asp:DropDownList ID="ddlMercado" runat="server" CssClass="form-control selectpicker">
                                <asp:ListItem Value="" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="O" Text="Otras Transacciones del Mercado Mayorista"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Producto</label>
                            <asp:DropDownList ID="ddlProducto" runat="server" CssClass="form-control selectpicker">
                                <%--20170926 rq027-17--%>
                                <asp:ListItem Value="0">Seleccione</asp:ListItem>
                                <asp:ListItem Value="1">Suministro de Gas</asp:ListItem>
                                <asp:ListItem Value="2">Capacidad de Transporte</asp:ListItem>
                                <asp:ListItem Value="3">Suministro y Transporte</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Comprador</label>
                            <asp:DropDownList ID="ddlComprador" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Vendedor</label>
                            <asp:DropDownList ID="ddlVendedor" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Estado</label>
                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <%--20171130 rq026-27--%>
                            <label>Causa Modificación</label>
                            <asp:DropDownList ID="ddlCausa" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Sector de Consumo</label>
                            <asp:DropDownList ID="ddlBusSector" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Mercado Relevante</label>
                            <asp:DropDownList ID="ddlBusMercado" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <%--20161213 rq102--%>
                            <label>Número Operación Inicial</label>
                            <asp:TextBox ID="txtOperacion" type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <%--20161213 rq102--%>
                            <label>Número Operación Final</label>
                            <asp:TextBox ID="txtOperacionFin" type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <%--20171130 rq026-27--%>
                            <label>Id Registro</label>
                            <asp:TextBox ID="TxtNoId" type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="btnConsultar" runat="server" CssClass="btn btn-success" Text="Consultar" OnClick="btnConsultar_Click" ValidationGroup="detalle" />
                            <asp:Button ID="btnInsertar" runat="server" CssClass="btn btn-success" Text="Insertar" ValidationGroup="detalle" OnClick="btnInsertar_Click" />
                            <asp:LinkButton ID="lkbExcel" runat="server" CssClass="btn btn-outline-brand btn-square" OnClick="lkbExcel_Click">Exportar Excel</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <%--Mensaje--%>
            <div class="kt-portlet__body" runat="server" id="tblMensaje">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <%--Grilla--%>
            <%--20161207 rq102--%>
            <div class="kt-portlet__body" runat="server" id="tblGrilla" visible="false">
                <div class="row">
                    <div class="table table-responsive">
                        <div style="overflow: scroll; height: 450px;">
                            <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                Width="100%" CssClass="table-bordered" OnEditCommand="dtgConsulta_EditCommand">
                                <Columns>
                                    <%--20160811--%>
                                    <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20161213 rq102--%>
                                    <%--20180126 rq107-16--%>
                                    <%--1--%><asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación"
                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--2--%><asp:BoundColumn DataField="tipo_subasta" HeaderText="Tipo Subasta" ItemStyle-HorizontalAlign="Left"
                                        Visible="false"></asp:BoundColumn>
                                    <%--3--%><asp:BoundColumn DataField="mercado" HeaderText="Mercado" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="80px"></asp:BoundColumn>
                                    <%--4--%><asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--5--%><asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--6--%><asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--20180126 rq107-16--%>
                                    <%--7--%><asp:BoundColumn DataField="fecha_limite_registro" HeaderText="Fecha Límite Registro"
                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--8--%><asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--9--%><asp:BoundColumn DataField="id" Visible="false"></asp:BoundColumn>
                                    <%--10--%><asp:BoundColumn DataField="operador_compra" Visible="false"></asp:BoundColumn>
                                    <%--11--%><asp:BoundColumn DataField="operador_venta" Visible="false"></asp:BoundColumn>
                                    <%--12--%><asp:BoundColumn DataField="cantidad" HeaderText="Cantidad"></asp:BoundColumn>
                                    <%--13--%><asp:BoundColumn DataField="precio" HeaderText="Precio"></asp:BoundColumn>
                                    <%--14--%><asp:BoundColumn DataField="codigo_tipo_subasta" Visible="false"></asp:BoundColumn>
                                    <%--15--%><asp:BoundColumn DataField="tipo_doc_comprador" Visible="false"></asp:BoundColumn>
                                    <%--16--%><asp:BoundColumn DataField="tipo_doc_vendedor" Visible="false"></asp:BoundColumn>
                                    <%--17--%><asp:BoundColumn DataField="documento_c" Visible="false"></asp:BoundColumn>
                                    <%--18--%><asp:BoundColumn DataField="documento_v" Visible="false"></asp:BoundColumn>
                                    <%--19--%><asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="No. Registro"></asp:BoundColumn>
                                    <%--20--%><asp:BoundColumn DataField="destino_rueda" Visible="false"></asp:BoundColumn>
                                    <%--21--%><asp:BoundColumn DataField="sigla_estado" Visible="false"></asp:BoundColumn>
                                    <%--22--%><asp:BoundColumn DataField="tipo_mercado" Visible="false"></asp:BoundColumn>
                                    <%--23--%><asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial">
                                        <%-- Campo nuevo ajuste reportes 20160405 --%>
                                    </asp:BoundColumn>
                                    <%--24--%><asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final"></asp:BoundColumn>
                                    <%-- Campo nuevo ajuste reportes 20160405 --%>
                                    <%--25--%><asp:BoundColumn DataField="numero_contrato" HeaderText="Número Contrato"></asp:BoundColumn>
                                    <%-- Campo nuevo ajuste reportes 20160405 --%>
                                    <%--26--%><asp:EditCommandColumn HeaderText="Verificar" EditText="Verificar"></asp:EditCommandColumn>
                                    <%--27--%><asp:BoundColumn DataField="codigo_producto" Visible="false"></asp:BoundColumn>
                                    <%--28--%><asp:EditCommandColumn HeaderText="Accion" EditText="Seleccionar"></asp:EditCommandColumn>
                                    <%--29--%><asp:BoundColumn DataField="en_correccion" Visible="false"></asp:BoundColumn>
                                    <%--30--%><asp:BoundColumn DataField="fecha_max_registro" Visible="false"></asp:BoundColumn>
                                    <%--31--%><asp:BoundColumn DataField="fecha_maxima_correccion" Visible="false"></asp:BoundColumn>
                                    <%--32--%><asp:BoundColumn DataField="fecha_maxima_modificacion" Visible="false"></asp:BoundColumn>
                                    <%--33--%><asp:BoundColumn DataField="ind_modifica" Visible="false"></asp:BoundColumn>
                                    <%--201691215 rq111--%>
                                    <%--34--%><asp:BoundColumn DataField="codigo_ruta_may" Visible="false"></asp:BoundColumn>
                                    <%--20170530 divipola--%>
                                    <%--34--%><asp:BoundColumn DataField="codigo_depto_punto_sal" HeaderText="Cod Depto"></asp:BoundColumn>
                                    <%--20170530 divipola--%>
                                    <%--35--%><asp:BoundColumn DataField="departamento" HeaderText="Departamento"></asp:BoundColumn>
                                    <%--20170530 divipola--%>
                                    <%--36--%><asp:BoundColumn DataField="codigo_municipio_pto_sal" HeaderText="Cod Mcpio"></asp:BoundColumn>
                                    <%--20170530 divipola--%>
                                    <%--37--%><asp:BoundColumn DataField="municipio" HeaderText="Municipio"></asp:BoundColumn>
                                    <%--20170530 divipola--%>
                                    <%--20180126 rq107-16--%>
                                    <%--38--%><asp:BoundColumn DataField="codigo_centro_pob" HeaderText="Cod Centro Poblado"></asp:BoundColumn>
                                    <%--20170530 divipola--%>
                                    <%--40--%><asp:BoundColumn DataField="centro_poblado" HeaderText="centro poblado"></asp:BoundColumn>
                                    <%--20170926 rq027-17--%>
                                    <%--41--%>
                                    <asp:BoundColumn DataField="capacidad_transporte" HeaderText="Capacidad de Transporte"></asp:BoundColumn>
                                    <%--20171130 rq026-17--%>
                                    <%--41--%>
                                    <asp:BoundColumn DataField="desc_causa" HeaderText="Causa Modificación"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                            <asp:DataGrid ID="dtgConsultaExcel" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                Width="100%" CssClass="table-bordered" Visible="false">
                                <Columns>
                                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Contrato"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numero_id" HeaderText="No. Operación"></asp:BoundColumn>
                                    <%--20161213 rq102--%>
                                    <%--20180126 rq107-16--%>
                                    <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="vendedor" HeaderText="Nombre Vendedor"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="operador_venta" HeaderText="Código Vendedor"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="rol_vendedor" HeaderText="Rol Vendedor"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="comprador" HeaderText="Nombre Comprador"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_comprador" HeaderText="Código Comprador"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="rol_comprador" HeaderText="Rol Comprador"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nit_comprador" HeaderText="Nit Comprador"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="mercado" HeaderText="Tipo Mercado"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="producto" HeaderText="Producto"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad Contrato"></asp:BoundColumn>
                                    <%--20170926 rq027-17--%>
                                    <asp:BoundColumn DataField="capacidad_transporte" HeaderText="Capacidad de Transporte"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="precio" HeaderText="Precio"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="punto_salida" HeaderText="Punto Salida"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="modalidad" HeaderText="Modalidad"></asp:BoundColumn>
                                    <%--20170530 divipola--%>
                                    <asp:BoundColumn DataField="codigo_departamento" HeaderText="Cod Depto"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="departamento" HeaderText="Departamento"></asp:BoundColumn>
                                    <%--20170530 divipola--%>
                                    <asp:BoundColumn DataField="codigo_ciudad" HeaderText="Cod Mcpio"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="municipio" HeaderText="Municipio"></asp:BoundColumn>
                                    <%--20170530 divipola--%>
                                    <asp:BoundColumn DataField="codigo_centro_pob" HeaderText="Cod Centro Poblado"></asp:BoundColumn>
                                    <%--20170530 divipola--%>
                                    <asp:BoundColumn DataField="centro_poblado" HeaderText="Centro Poblado"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="sector_consumo" HeaderText="Sector Consumo"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="mercado_relevante" HeaderText="Mercado Relevante"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="tipo_garantia" HeaderText="Tipo Garantía"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_pago" HeaderText="Fecha Pago Garantía"></asp:BoundColumn>
                                    <%--20161213 rq102--%>
                                    <asp:BoundColumn DataField="estado" HeaderText="Estado"></asp:BoundColumn>
                                    <%--201691215 rq111--%>
                                    <asp:BoundColumn DataField="desc_ruta_may" HeaderText="Ruta"></asp:BoundColumn>
                                    <%--20171130 rq026-17--%>
                                    <%--41--%>
                                    <asp:BoundColumn DataField="desc_causa" HeaderText="Causa Modificación"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
            </div>
            <%--Modificacion--%>
            <div class="kt-portlet__body" runat="server" id="tblRegCnt" visible="false">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fecha de Negociación {YYYY/MM/DD}</label>
                            <asp:TextBox ID="TxtCapFechaNeg" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" style="display:none">
                        <div class="form-group" >
                            <label style="display:none">Numero Operacion</label>
                            <asp:Label ID="lblOperacion" runat="server" ForeColor="Red" style="display:none" Font-Bold="true" CssClass="form-control"></asp:Label>-<asp:Label
                                ID="lblPunta" runat="server" ForeColor="Red" Font-Bold="true" CssClass="form-control" Visible="false"></asp:Label>
                            <asp:HiddenField ID="hdfPunta" runat="server" />
                            <asp:HiddenField ID="hdfIdVerif" runat="server" />
                            <asp:HiddenField ID="hdfCodTipoSub" runat="server" />
                            <asp:HiddenField ID="hdfDestinoRueda" runat="server" />
                            <asp:HiddenField ID="hdfRutaArchivo" runat="server" />
                            <asp:HiddenField ID="hdfEstadoAct" runat="server" />
                            <asp:HiddenField ID="hdfAccion" runat="server" />
                            <asp:HiddenField ID="hdfCodVerifCont" runat="server" />
                            <asp:HiddenField ID="hdfTipoMerc" runat="server" />
                            <asp:HiddenField ID="hdfOPeraC" runat="server" />
                            <asp:HiddenField ID="hdfOPeraV" runat="server" />
                            <asp:HiddenField ID="hdfCodProd" runat="server" />
                            <asp:HiddenField ID="hdfDiasMaxRegCont" runat="server" />
                            <asp:HiddenField ID="hdfDiasMaxCorCont" runat="server" />
                            <asp:HiddenField ID="hdfVerifManual" runat="server" />
                            <%--20190215 ajuste--%>
                            <asp:HiddenField ID="hndFechaMax" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Producto</label>
                            <%--20161215 rq111--%>
                            <asp:DropDownList ID="ddlCapProducto" runat="server" CssClass="form-control selectpicker" AutoPostBack="true" OnSelectedIndexChanged="ddlCapProducto_SelectedIndexChanged">
                                <%--20170926 rq027-17--%>
                                <asp:ListItem Value="0">Seleccione</asp:ListItem>
                                <asp:ListItem Value="1">Suministro de Gas</asp:ListItem>
                                <asp:ListItem Value="2">Capacidad de Transporte</asp:ListItem>
                                <asp:ListItem Value="3">Suministro y Transporte</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="lblProducto" runat="server" ForeColor="Red" Font-Bold="true" Visible="false" CssClass="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Punta que Registra</label>
                            <asp:DropDownList ID="ddlCapPunta" runat="server" CssClass="form-control selectpicker">
                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="C" Text="Comprador"></asp:ListItem>
                                <asp:ListItem Value="V" Text="Vendedor"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Tipo Mercado</label>
                            <asp:DropDownList ID="ddlCapTipoMercado" runat="server" OnSelectedIndexChanged="ddlCapTipoMercado_SelectedIndexChanged"
                                AutoPostBack="true" CssClass="form-control selectpicker">
                                <asp:ListItem Value="O" Text="Otras Transaciones del Mercado Mayorista"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Comprador</label>
                            <asp:DropDownList ID="ddlCapComprador" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                            Otro
                           
                            <asp:CheckBox ID="ChekOtro" runat="server" OnCheckedChanged="ChekOtro_CheckedChanged"
                                AutoPostBack="true" />
                            <asp:Label ID="lblNIT" runat="server" Text="NIT:" Visible="false" CssClass="form-control"></asp:Label>
                            <asp:TextBox ID="TxtCapComprador" runat="server" Width="200px" Visible="false" CssClass="form-control"></asp:TextBox>
                            <asp:HiddenField ID="hdfNomComp" runat="server" />
                            <asp:HiddenField ID="hdfTipoDocCom" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Vendedor</label>
                            <asp:DropDownList ID="ddlCapVendedor" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row" runat="server" id="TrInfOP" visible="false">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lnlNomOpera" runat="server" CssClass="form-control"></asp:Label>
                            <asp:Label ID="lblNomOperador" runat="server" CssClass="form-control" ForeColor="Red" Font-Bold="true"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTipoDocOpera" runat="server" CssClass="form-control"></asp:Label>
                            <asp:Label ID="lblTipoDocOperador" runat="server" CssClass="form-control" ForeColor="Red" Font-Bold="true"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblNoDocOPera" runat="server" CssClass="form-control"></asp:Label>
                            <asp:Label ID="lblNoDocumentoOperaqdor" runat="server" CssClass="form-control" ForeColor="Red" Font-Bold="true"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <%--20161213 rq102--%>
                            <label>Número Contrato</label>
                            <asp:TextBox ID="TxtNumContrato" runat="server" ValidationGroup="detalle" CssClass="form-control" MaxLength="30"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fecha Suscripción {YYYY/MM/DD}</label>
                            <asp:TextBox ID="TxtFechaSus" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Modalidad de Contrato</label>
                            <asp:DropDownList ID="ddlModalidad" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                            Otro
                           
                            <asp:CheckBox ID="ChkOtrModa" runat="server" />
                            <asp:TextBox ID="TxtOtrModalidad" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblPuntoE" runat="server" CssClass="form-control"></asp:Label>
                            <%--20161215 rq111--%>
                            <asp:DropDownList ID="ddlPuntoEntr" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                            <asp:CheckBox ID="ChkOtrPuntoE" runat="server" Visible="false" />
                            <asp:TextBox ID="TxtOtrPuntoE" runat="server" Visible="false" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblCantidad" runat="server" CssClass="form-control"></asp:Label>
                            <asp:TextBox ID="TxtCantidad" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                            <%--20170926 rq027-17--%>
                            <asp:TextBox ID="TxtCantidadT" runat="server" ValidationGroup="detalle" class="form-control" Visible="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblPrecio" runat="server" CssClass="form-control"></asp:Label>
                            <asp:TextBox ID="TxtPrecio" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblFechaInc" runat="server" CssClass="form-control"></asp:Label>{YYYY/MM/DD}
                           
                            <asp:TextBox ID="TxtFechaInicial" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblFechaFin" runat="server" CssClass="form-control"></asp:Label>{YYYY/MM/DD}
                           
                            <asp:TextBox ID="TxtFechaFinal" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" id="trRuta" runat="server" visible="false">
                        <div class="form-group">
                            <label>Ruta Contratada:</label>
                            <asp:DropDownList ID="ddlRuta" runat="server" CssClass="form-control selectpicker">
                                <%--20180227 rq011-18--%>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Tipo de Garantía <%--20180126 rq107-16--%></label>
                            <asp:TextBox ID="TxtCapTipoGarantia" runat="server" ValidationGroup="detalle" CssClass="form-control" MaxLength="200"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Valor de Garantía <%--20180126 rq107-16--%></label>
                            <asp:TextBox ID="TxtCapValorGarantia" runat="server" ValidationGroup="detalle" CssClass="form-control" MaxLength="200"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Plazo Para realizar el Pago {YYYY/MM/DD}</label>
                            <asp:TextBox ID="TxtCapFecPlazo" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Sector de Consumo</label>
                            <asp:DropDownList ID="ddlSector" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Usuario no Regulado con Conexión a SNT <%--20180126 rq107-16--%></label>
                            <asp:DropDownList ID="ddlUsuSnt" runat="server" OnSelectedIndexChanged="ddlUsuSnt_SelectedIndexChanged"
                                AutoPostBack="true" CssClass="form-control selectpicker">
                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:HiddenField ID="hdfusuarioSnt" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Copia del Contrato</label>
                            <asp:FileUpload ID="FuCopiaCont" BackColor="#dddddd" runat="server" EnableTheming="true" />
                        </div>
                    </div>
                </div>
                <div class="row" runat="server" id="TrPuntaSal" visible="false">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Departamento punto de salida</label>
                            <asp:DropDownList ID="ddlDeptoPuntoSal" runat="server" OnSelectedIndexChanged="ddlDeptoPuntoSal_SelectedIndexChanged"
                                AutoPostBack="true" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <%--20170530 divipola--%>
                            <label>Municipio punto de salida</label>
                            <asp:DropDownList ID="DdlMunPuntoSal" runat="server" OnSelectedIndexChanged="DdlMunPuntoSal_SelectedIndexChanged"
                                AutoPostBack="true" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <%--20170530 divipola--%>
                            <label>Centro Poblado</label>
                            <asp:DropDownList ID="DdlCentro" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row" runat="server" id="TrSisDist" visible="false">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Mercado Relevante</label>
                            <asp:DropDownList ID="ddlMercadoRel" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row" runat="server" id="TrModifi" visible="false">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Motivo Modificación</label>
                            <asp:DropDownList ID="ddlMotivoModifi" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="btnCrear" runat="server" CssClass="btn btn-success" Text="Crear" OnClick="btnCrear_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="btnActualizar" runat="server" CssClass="btn btn-success" Text="Actualizar" OnClick="btnActualizar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="btnRegresar" runat="server" CssClass="btn btn-secondary" Text="Regresar" OnClick="btnRegresar_Click" Visible="false" />
                        </div>
                    </div>
                </div>
            </div>
            <%--Verificacion--%>
            <div class="kt-portlet__body" runat="server" id="tblVerifica" visible="false">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label><b>VERIFICACION REGISTRO</b></label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Dato</label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Comprador</label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Vendedor</label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Verificación</label>
                            <%--20180126 rq107-16--%>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit1" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat1C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat1V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat1Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit2" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat2C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat2V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat2Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit3" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat3C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat3V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat3Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit4" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat4C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat4V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat4Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit5" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat5C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat5V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat5Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row" id="trSumTra" runat="server">
                    <%--20170926 rq027-17--%>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit5X" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat5CX" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat5VX" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat5VerX" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit6" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat6C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat6V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat6Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit7" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat7C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat7V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat7Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit8" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat8C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat8V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat8Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row" runat="server" id="TrTra1" visible="false">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit9" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat9C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat9V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat9Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row" runat="server" id="TrTra2" visible="false">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit10" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat10C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat10V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat10Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit11" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <a id="LinkC" runat="server" class="btn btn-outline-brand btn-square">Ver Contrato</a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <a id="LinkV" runat="server" class="btn btn-outline-brand btn-square">Ver Contrato</a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="Label4" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="Observación" runat="server"></asp:Label><%--20180126 rq107-16--%>
                            <asp:TextBox ID="TxtObservacion" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="btnSolCorr" runat="server" CssClass="btn btn-success" Text="Solicitar Correccion" OnClick="btnSolCorr_Click" />
                            <asp:Button ID="btnRegCont" runat="server" CssClass="btn btn-success" Text="Registro Contrato" OnClick="btnRegCont_Click" />
                            <asp:Button ID="btnSalir" runat="server" class="btn btn-secondary" Text="Salir" OnClick="btnSalir_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
