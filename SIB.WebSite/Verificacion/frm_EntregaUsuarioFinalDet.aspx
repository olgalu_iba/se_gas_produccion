﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_EntregaUsuarioFinalDet.aspx.cs"
    Inherits="Verificacion_frm_EntregaUsuarioFinalDet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" Text="Contratos Registrados"></asp:Label>
                    </h3>
                </div>
            </div>
            <%--Grilla--%>
            <div class="kt-portlet__body" runat="server" id="Div1" visible="false">
                <div class="row">
                    <div class="table table-responsive">
                        <asp:DataGrid ID="dtgDetalle" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Width="100%" CssClass="table-bordered">
                            <Columns>
                                <asp:BoundColumn DataField="codigo_detalle" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_ent_usuario_final" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Cod Mod" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ind_registrado" HeaderText="Registrado" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
