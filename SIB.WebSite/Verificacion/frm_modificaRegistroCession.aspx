﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_modificaRegistroCession.aspx.cs"
    Inherits="Verificacion_frm_modificaRegistroCession" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>
            </div>
            <%--Contenido--%>
            <%--Captura--%>
            <div class="kt-portlet__body" runat="server" id="tblDatos">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Número Contrato</label>
                            <asp:TextBox ID="TxtBusContratoDef" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Número Operación</label>
                            <asp:TextBox ID="TxtBusOperacion" runat="server" ValidationGroup="detalle" Width="120px" MaxLength="10"></asp:TextBox>
                            <asp:CompareValidator ID="CvTxtBusOperacion" runat="server" ControlToValidate="TxtBusOperacion"
                                Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Número Operación debe Ser numérico">*</asp:CompareValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Id Registro</label>
                            <asp:TextBox ID="TxtBusId" runat="server" ValidationGroup="detalle" Width="120px" MaxLength="10"></asp:TextBox>
                            <asp:CompareValidator ID="CvTxtBusId" runat="server" ControlToValidate="TxtBusId"
                                Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Número Id Registro debe Ser numérico">*</asp:CompareValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fec. Neg. Ini.</label>
                            <asp:TextBox ID="TxtBusFechaIni" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fec. Neg. Fin</label>
                            <asp:TextBox ID="TxtBusFechaFin" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row" runat="server" id="tr01">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Operador Contraparte</label>
                            <asp:DropDownList ID="DdlBusContra" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Causa</label>
                            <asp:DropDownList ID="DdlBusCausa" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Estado</label>
                            <asp:DropDownList ID="DdlBusEstado" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row" runat="server" id="tr02">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Comprador</label>
                            <asp:DropDownList ID="DdlBusCompra" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Vendedor</label>
                            <asp:DropDownList ID="DdlBusVenta" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row" runat="server" id="tr03">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Tipo Mercado</label>
                            <asp:DropDownList ID="DdlBusMercado" runat="server" CssClass="form-control selectpicker" AutoPostBack="true" OnSelectedIndexChanged="DdlBusMercado_SelectedIndexChanged">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="P">Primario</asp:ListItem>
                                <asp:ListItem Value="S">Secundario</asp:ListItem>
                                <asp:ListItem Value="O">Otras transacciones del mercado mayorista</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Producto</label>
                            <asp:DropDownList ID="DdlBusProducto" runat="server" CssClass="form-control selectpicker">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="G">Suministro de gas</asp:ListItem>
                                <asp:ListItem Value="T">Capacidad de transporte</asp:ListItem>
                                <asp:ListItem Value="A">Suministro y transporte</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Tipo de contrato</label>
                            <asp:DropDownList ID="DdlBusModalidad" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Causa</label>
                            <asp:DropDownList ID="DdlBusCausa1" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Estado Modificación</label>
                            <asp:DropDownList ID="DdlBusEstado1" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="btnConsultar" runat="server" CssClass="btn btn-success" Text="Consultar" OnClick="btnConsultar_Click" ValidationGroup="detalle" />
                            <asp:LinkButton ID="lkbExcel" runat="server" CssClass="btn btn-outline-brand btn-square" OnClick="lkbExcel_Click">Exportar Excel</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <%--Mensaje--%>
            <div class="kt-portlet__body" runat="server" id="tblMensaje">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <%--Grilla--%>
            <div class="kt-portlet__body" runat="server" id="tblGrilla" visible="false">
                <div class="row">
                    <div class="table table-responsive">
                        <div style="overflow: scroll; height: 450px;">
                            <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                Width="100%" CssClass="table-bordered" OnEditCommand="dtgConsulta_EditCommand">
                                <Columns>
                                    <%--0--%>
                                    <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar"></asp:EditCommandColumn>
                                    <%--1--%>
                                    <asp:EditCommandColumn HeaderText="Consultar" EditText="Consultar"></asp:EditCommandColumn>
                                    <%--2--%>
                                    <asp:BoundColumn DataField="codigo_solicitud" Visible="false"></asp:BoundColumn>
                                    <%--3--%>
                                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--4--%>
                                    <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--5--%>
                                    <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                    <%--6--%>
                                    <asp:BoundColumn DataField="contrato_definitivo" HeaderText="contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--7--%>
                                    <asp:BoundColumn DataField="tipo_contrato" HeaderText="Tipo Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--8--%>
                                    <asp:BoundColumn DataField="operador_compra" HeaderText="Cod. Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--9--%>
                                    <asp:BoundColumn DataField="nombre_compra" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--10--%>
                                    <asp:BoundColumn DataField="operador_venta" HeaderText="Cod. Venta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--11--%>
                                    <asp:BoundColumn DataField="nombre_venta" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--12--%>
                                    <asp:BoundColumn DataField="desc_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--13--%>
                                    <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--14--%>
                                    <asp:BoundColumn DataField="codigo_tipo_subasta" Visible="false"></asp:BoundColumn>
                                    <%--15--%>
                                    <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha inicial" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                    <%--16--%>
                                    <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha final" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                    <%--17--%>
                                    <asp:BoundColumn DataField="cantidad" HeaderText="cantidad" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
                                    <%--18--%>
                                    <asp:BoundColumn DataField="desc_estado" HeaderText="Estado Modificación"></asp:BoundColumn>
                                    <%--19--%>
                                    <asp:BoundColumn DataField="desc_causa" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20--%>
                                    <asp:BoundColumn DataField="desc_tipo_cesion" HeaderText="Tipo Cesión" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--21--%>
                                    <asp:EditCommandColumn HeaderText="Contrato original" EditText="Original"></asp:EditCommandColumn>
                                    <%--22--%>
                                    <asp:EditCommandColumn HeaderText="Contrato Modificado" EditText="Modificado"></asp:EditCommandColumn>
                                    <%--23--%>
                                    <asp:BoundColumn DataField="contrato_original" Visible="false"></asp:BoundColumn>
                                    <%--24--%>
                                    <asp:BoundColumn DataField="contrato_nuevo" Visible="false"></asp:BoundColumn>
                                    <%--25--%>
                                    <asp:BoundColumn DataField="punta_solicitud" Visible="false"></asp:BoundColumn>
                                    <%--26--%>
                                    <asp:BoundColumn DataField="ind_accion" Visible="false"></asp:BoundColumn>
                                    <%--27--%>
                                    <asp:BoundColumn DataField="tipo_mercado" Visible="false"></asp:BoundColumn>
                                    <%--28--%>
                                    <asp:BoundColumn DataField="destino_rueda" Visible="false"></asp:BoundColumn>
                                    <%--29--%>
                                    <asp:BoundColumn DataField="punta" Visible="false"></asp:BoundColumn>
                                    <%--30--%>
                                    <asp:BoundColumn DataField="codigo_causa" Visible="false"></asp:BoundColumn>
                                    <%--31--%>
                                    <asp:BoundColumn DataField="codigo_modif" Visible="false"></asp:BoundColumn>
                                    <%--32--%>
                                    <asp:BoundColumn DataField="codigo_estado" Visible="false"></asp:BoundColumn>
                                    <%--33--%>
                                    <asp:BoundColumn DataField="tipo_cesion" Visible="false"></asp:BoundColumn>
                                    <%--34--%>
                                    <asp:BoundColumn DataField="tipo_causa" Visible="false"></asp:BoundColumn>
                                    <%--35--%>
                                    <asp:BoundColumn DataField="tipo_aprob" Visible="false"></asp:BoundColumn>
                                    <%--36--%>
                                    <asp:BoundColumn DataField="consec_enlace" HeaderText="consecutivo"></asp:BoundColumn>
                                    <%--37--%>
                                    <asp:BoundColumn DataField="ind_otras_mod" Visible="false"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
            </div>
            <%--Modificacion--%>
            <div class="kt-portlet__body" runat="server" id="tblModifica" visible="false">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Destino Rueda</label>
                            <asp:DropDownList ID="DdlDestino" runat="server" CssClass="form-control selectpicker" Enabled="false">
                                <asp:ListItem Value="G" Text="Suministro de Gas"></asp:ListItem>
                                <asp:ListItem Value="T" Text="Capacidad de Transporte"></asp:ListItem>
                                <asp:ListItem Value="A" Text="Suministro y Transporte"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:HiddenField ID="hdfNoSolicitud" runat="server" />
                            <asp:HiddenField ID="hdfCausa" runat="server" />
                            <asp:HiddenField ID="hdfOperador" runat="server" />
                            <asp:HiddenField ID="hdfCodModif" runat="server" />
                            <asp:HiddenField ID="hdfPunta" runat="server" />
                            <asp:HiddenField ID="hdfCodigoVerUsr" runat="server" />
                            <asp:HiddenField ID="hdfIdRegistro" runat="server" />
                            <asp:HiddenField ID="hdfContratoDef" runat="server" />
                            <asp:HiddenField ID="hdfTipoCesion" runat="server" />
                            <%--20190425 rq022-19--%>
                            <asp:HiddenField ID="hdfMostrar" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Tipo Mercado</label>
                            <asp:DropDownList ID="DdlMercado" runat="server" CssClass="form-control selectpicker" Enabled="false">
                                <asp:ListItem Value="P" Text="Primario"></asp:ListItem>
                                <asp:ListItem Value="S" Text="Secundario"></asp:ListItem>
                                <asp:ListItem Value="O" Text="Otras Transacciones del Mercado Mayorista"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label runat="server" ID="LblOperadorC">Operador Compra<%--20190425 rq022-19--%></asp:Label>
                            <asp:DropDownList ID="DdlComprador" runat="server" CssClass="form-control selectpicker" OnSelectedIndexChanged="DdlComprador_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label runat="server" ID="LblOperadorV">Operador Venta<%--20190425 rq022-19--%></asp:Label>
                            <asp:DropDownList ID="DdlVendedor" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label runat="server">Fecha-hora Negociación</asp:Label>
                            <asp:TextBox ID="TxtFechaNeg" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>-
                             <asp:TextBox ID="TxtHoraNeg" runat="server" class="form-control" ValidationGroup="detalle" MaxLength="5"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="revTxtHoraNeg" ControlToValidate="TxtHoraNeg"
                                ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                                ErrorMessage="Formato Incorrecto para la Hora de negociación"> * </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label runat="server">Fecha Suscripción</asp:Label>
                            <asp:TextBox ID="TxtFechaSus" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>-
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label runat="server">Contrato definitivo</asp:Label>
                            <asp:TextBox ID="TxtContDef" runat="server" class="form-control" ValidationGroup="detalle" MaxLength="30"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label runat="server" ID="LblFechaIni">Fecha Entrega Inicial<%--20190425 rq022-19--%></asp:Label>
                            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>
                            <asp:HiddenField ID="hdfErrorFecha" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label runat="server" ID="LblFechaFin">Fecha Entrega Final<%--20190425 rq022-19--%></asp:Label>
                            <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>-
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label runat="server" ID="LblCantidad">Cantidad de energía contratada (MBTUD)</asp:Label>
                            <asp:TextBox ID="TxtCantidad" runat="server" CssClass="form-control" MaxLength="6"></asp:TextBox>
                            <asp:CompareValidator ID="CvTxtCantidad" runat="server" ControlToValidate="TxtCantidad"
                                Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Cantidad de energía contratada (MBTUD) debe Ser numérico">*</asp:CompareValidator>
                            <asp:TextBox ID="TxtCapacOtm" runat="server" CssClass="form-control" MaxLength="6" Visible="false"></asp:TextBox>
                            <asp:CompareValidator ID="CvTxtCapacOtm" runat="server" ControlToValidate="TxtCapacOtm"
                                Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo >Cantidad de energía contratada (MBTUD) debe Ser numérico">*</asp:CompareValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label runat="server" ID="LblPrecio">Precio a la fecha de suscripción del contrato (USD/MBTU)</asp:Label>
                            <asp:TextBox ID="TxtPrecio" runat="server" CssClass="form-control" MaxLength="6"></asp:TextBox>
                            <asp:CompareValidator ID="CvTxtPrecio" runat="server" ControlToValidate="TxtPrecio"
                                Type="Double" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Precio a la fecha de suscripción del contrato (USD/MBTU) debe Ser numérico">*</asp:CompareValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label runat="server">Contrato Variable</asp:Label>
                            <asp:DropDownList ID="DdlVariable" runat="server" CssClass="form-control selectpicker">
                                <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row" id="trObs" runat="server" visible="false">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Observaciones</label>
                            <asp:TextBox ID="TxtObs" runat="server" CssClass="form-control" MaxLength="1000" TextMode="MultiLine" Rows="3"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row" runat="server" id="trCambioUsrFin" visible="false">
                    <%--20190425 rq022-19 --%>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="LblModUsrFin" runat="server" ForeColor="Red" CssClass="form-control">LOS USUARIOS FINALES HAN SIDO MODIFICADOS</asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="btnModificar" runat="server" CssClass="btn btn-success" Text="Modificar" OnClick="btnModificar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="btnAprobar" runat="server" CssClass="btn btn-success" Text="Aprobar" OnClick="btnAprobar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="btnRechazo" runat="server" CssClass="btn btn-success" Text="Rechazar" OnClick="btnRechazar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="btnAprobarBmc" runat="server" CssClass="btn btn-success" Text="Aprobar BMC" OnClick="btnAprobarBmc_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="btnRechazoBMC" runat="server" CssClass="btn btn-success" Text="Rechazar" OnClick="btnRechazarBmc_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="btnGrabarDef" runat="server" CssClass="btn btn-success" Text="Grabar Def" OnClick="btnGrabarDef_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="btnCrearUsu" runat="server" CssClass="btn btn-success" Text="Crear Usuario" OnClick="btnCrearUsu_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" Visible="false" />
                            <asp:Button ID="btnActualUsu" runat="server" CssClass="btn btn-success" Text="Actualiza Usuario" OnClick="btnActualUsu_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" Visible="false" />
                            <asp:Button ID="btnRegresar" runat="server" class="btn btn-secondary" Text="Regresar" OnClick="btnRegresar_Click" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="14px"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <%--Demanda--%>
            <div class="kt-portlet__body" runat="server" id="tblDemanda" visible="false">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Tipo Demanda a Atender</label>
                            <asp:DropDownList ID="dlTipoDemanda" runat="server" CssClass="form-control selectpicker" OnSelectedIndexChanged="dlTipoDemanda_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Usuario Final</label>
                            <asp:TextBox ID="TxtUsuarioFinal" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:AutoCompleteExtender runat="server" BehaviorID="AutoCompleteExUsuF" ID="AutoCompleteExtender1"
                                TargetControlID="TxtUsuarioFinal" ServicePath="~/WebService/AutoComplete.asmx"
                                ServiceMethod="GetCompletionListUsuarioFinal" MinimumPrefixLength="3" CompletionInterval="1000"
                                EnableCaching="true" CompletionSetCount="20" CompletionListCssClass="autocomplete_completionListElement"
                                CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                DelimiterCharacters=";,:">
                                <Animations>
                        <OnShow>
                            <Sequence>
                                <OpacityAction Opacity="0" />
                                <HideAction Visible="true" />
                                <ScriptAction Script="
                                    // Cache the size and setup the initial size
                                    var behavior = $find('AutoCompleteExUsuF');
                                    if (!behavior._height) {
                                        var target = behavior.get_completionList();
                                        behavior._height = target.offsetHeight - 2;
                                        target.style.height = '0px';
                                    }" />
                                <Parallel Duration=".4">
                                    <FadeIn />
                                    <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteExUsuF')._height" />
                                </Parallel>
                            </Sequence>
                        </OnShow>
                        <OnHide>
                            <Parallel Duration=".4">
                                <FadeOut />
                                <Length PropertyKey="height" StartValueScript="$find('AutoCompleteExUsuF')._height" EndValue="0" />
                            </Parallel>
                        </OnHide>
                                </Animations>
                            </ajaxToolkit:AutoCompleteExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label runat="server" ID="lblSector">Sector Consumo Usuario Regulado</asp:Label>
                            <asp:DropDownList ID="ddlSector" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Punto de Salida en SNT</label>
                            <asp:DropDownList ID="ddlPuntoSalida" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                            <asp:HiddenField ID="hdfTipoDemanda" runat="server" />
                            <asp:HiddenField ID="hdfCodDatUsu" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Mercado Relevante</label>
                            <asp:DropDownList ID="ddlMercadoRel" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label runat="server" ID="lblCantContra">Cantidad a entregar</asp:Label>
                            <asp:TextBox ID="TxtCantidadUsu" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" runat="server" id="TdIndica">
                        <div class="form-group">
                            <asp:Label runat="server">Equivalente Kpcd</asp:Label>
                            <asp:TextBox ID="TxtEquivaleKpcd" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label><b>LISTADO DE USUARIOS FINALES</b></label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <b>TOTAL CANTIDAD:</b><asp:Label ID="lblTotlCantidad" runat="server" ForeColor="Red"
                                Font-Bold="true"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="table table-responsive">
                        <div style="overflow: scroll; height: 450px;">
                            <asp:DataGrid ID="dtgUsuarios" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                Width="100%" CssClass="table-bordered" OnEditCommand="dtgUsuarios_EditCommand">
                                <Columns>
                                    <%--0--%>
                                    <asp:BoundColumn DataField="tipo_demanda" HeaderText="Tipo Demanda Atender" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--1--%>
                                    <asp:BoundColumn DataField="no_identificacion_usr" HeaderText="Identificación Usuario Final"
                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--2--%>
                                    <asp:BoundColumn DataField="tipo_documento" HeaderText="Tipo Identificación Usuario Final"
                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--3--%>
                                    <asp:BoundColumn DataField="nombre_usuario" HeaderText="Nombre Usuario Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--4--%>
                                    <asp:BoundColumn DataField="sector_consumo" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="80px"></asp:BoundColumn>
                                    <%--5--%>
                                    <asp:BoundColumn DataField="punto_salida" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--6--%>
                                    <asp:BoundColumn DataField="cantidad_contratada" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--7--%>
                                    <asp:BoundColumn DataField="equivalente_kpcd" HeaderText="Equivalente Kpcd" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--8--%>
                                    <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar"></asp:EditCommandColumn>
                                    <%--9--%>
                                    <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                                    <%--10--%>
                                    <asp:BoundColumn DataField="codigo_cont_usr" Visible="false"></asp:BoundColumn>
                                    <%--11--%>
                                    <asp:BoundColumn DataField="codigo_modif_cesion" Visible="false"></asp:BoundColumn>
                                    <%--12--%>
                                    <asp:BoundColumn DataField="codigo_sector_consumo" Visible="false"></asp:BoundColumn>
                                    <%--13--%>
                                    <asp:BoundColumn DataField="codigo_punto_salida" Visible="false"></asp:BoundColumn>
                                    <%--14--%>
                                    <asp:BoundColumn DataField="codigo_tipo_doc" Visible="false"></asp:BoundColumn>
                                    <%--15--%>
                                    <asp:BoundColumn DataField="codigo_tipo_demanda" Visible="false"></asp:BoundColumn>
                                    <%--17--%>
                                    <asp:BoundColumn DataField="codigo_mercado_relevante" Visible="false"></asp:BoundColumn>
                                    <%--18--%>
                                    <asp:BoundColumn DataField="codigo_cont_usr" Visible="false"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
            </div>
            <%--Popup Causa Modifica--%>
            <div class="kt-portlet__body" runat="server" id="divSolicitud" visible="false" style="width: 500PX; height: 250PX; position: absolute; left: 450px; top: 150px; border: solid; background-color: white;">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="Label2" runat="server" Text="Causa de modificación"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Causa Modificación</label>
                            <asp:DropDownList ID="DdlIngCausa" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Tipo Cesión</label>
                            <asp:DropDownList ID="ddlTipo" runat="server" CssClass="form-control selectpicker">
                                <asp:ListItem Value="T">Total</asp:ListItem>
                                <asp:ListItem Value="P">Parcial</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Contrato original</label>
                            <asp:FileUpload ID="FuArchivoOrg" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Contrato Modificación</label>
                            <asp:FileUpload ID="FuArchivoMod" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblMensajeMod" runat="server" CssClass="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="BtnAceptar" runat="server" CssClass="btn btn-success" Text="Aceptar" OnClick="BtnAceptar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="BtnCancelar" runat="server" class="btn btn-secondary" Text="Cancelar" OnClick="BtnCancelar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
