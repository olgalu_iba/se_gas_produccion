﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_EnergiaConexionExt.aspx.cs" Inherits="Verificacion.frm_EnergiaConexionExt" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" Text="Cantidad de Energía Gasoducto de Conexión Extemporánea" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body" runat="server" id ="tblDatos" runat="server"> <%--20200727--%>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Código Energía Gasoducto de conexión" AssociatedControlID="TxtBusCodigo" runat="server" />
                            <asp:TextBox ID="TxtBusCodigo" runat="server" autocomplete="off" CssClass="form-control" />
                            <ajaxToolkit:FilteredTextBoxExtender ID="FTBETxtBusCodigo" runat="server" TargetControlID="TxtBusCodigo" FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Inicial" AssociatedControlID="TxtBusFecha" runat="server" />
                            <asp:TextBox ID="TxtBusFecha" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Final" AssociatedControlID="TxtBusFechaF" runat="server" />
                            <asp:TextBox ID="TxtBusFechaF" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador" AssociatedControlID="ddlBusOperador" runat="server" />
                            <asp:DropDownList ID="ddlBusOperador" runat="server" CssClass="form-control selectpicker" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Gasoducto de conexión" AssociatedControlID="ddlBusGasoducto" runat="server" />
                            <asp:DropDownList ID="ddlBusGasoducto" runat="server" CssClass="form-control selectpicker" />
                        </div>
                    </div>
                </div>
                <%--Grilla--%>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True" PagerStyle-HorizontalAlign="Center" OnItemCommand="dtgMaestro_EditCommand"
                                OnPageIndexChanged="dtgMaestro_PageIndexChanged" Width="100%" CssClass="table-bordered" PageSize="10">
                                <Columns>
                                    <asp:BoundColumn DataField="codigo_energia_conexion" HeaderText="Código Energía" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_gasoducto" HeaderText="Código Gasoducto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_gasoducto" HeaderText="Gasoducto Conexión" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numero_operacion" HeaderText="Operación" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_tipo_punto" HeaderText="TIpo Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_punto_entrega" HeaderText="Código Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_punto_entrega" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_tipo_demanda" HeaderText="Códogo Tipo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="equivalente_kpcd" HeaderText="Equivalente (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="cantidad_cmmp" HeaderText="CMMP (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,###,##0}"></asp:BoundColumn> <%--20200727--%>
                                    <asp:BoundColumn DataField="carga_normal" HeaderText="Estado de Carga" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="ind_registrado" HeaderText="Operación Registrada" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="ind_vigente" HeaderText="Operación Vigente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualización" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more-1"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                    <!--begin::Nav-->
                                                    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <ul class="kt-nav">
                                                                <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                             <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Modificar</span>
                                                                    </asp:LinkButton>
                                                                </li>
                                                                <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon-delete"></i>
                                                            <span class="kt-nav__link-text">Eliminar</span>
                                                                    </asp:LinkButton>
                                                                </li>
                                                            </ul>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <%--Excel--%>
                <asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False"
                    Width="100%" CssClass="table-bordered" Visible="false">
                    <Columns>
                        <asp:BoundColumn DataField="codigo_energia_conexion" HeaderText="Código Energía"
                            ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_gasoducto" HeaderText="Código Gasoducto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_gasoducto" HeaderText="Gasoducto Conexión" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="numero_operacion" HeaderText="Operación"
                            ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_tipo_punto" HeaderText="TIpo Punto Entrega"
                            ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_punto_entrega" HeaderText="Código Punto Entrega"
                            ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_punto_entrega" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_tipo_demanda" HeaderText="Códogo Tipo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                        <asp:BoundColumn DataField="equivalente_kpcd" HeaderText="Equivalente (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                        <asp:BoundColumn DataField="cantidad_cmmp" HeaderText="CMMP (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,###,##0}"></asp:BoundColumn> <%--20200727--%>
                        <asp:BoundColumn DataField="carga_normal" HeaderText="Estado de Carga" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ind_registrado" HeaderText="Operación Registrada" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ind_vigente" HeaderText="Operación Vigente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualización" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                    </Columns>
                    <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                </asp:DataGrid>
            </div>
        </div>
    </div>

    <%--Modal--%>
    <div class="modal fade" id="registroEnergiaConexionEx" tabindex="-1" role="dialog" aria-labelledby="mdlRegistroEnergiaConexionExLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="registroEnergiaConexionExInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlRegistroEnergiaConexionExLabel" runat="server">Agregar</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Código Energía Gasoducto de conexión" AssociatedControlID="TxtCodigoCap" runat="server" />
                                        <asp:TextBox ID="TxtCodigoCap" runat="server" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                        <asp:Label ID="LblCodigoCap" runat="server" Visible="False" CssClass="form-control"></asp:Label>
                                        <asp:HiddenField ID="hdnELiminado" runat="server" />
                                        <asp:HiddenField ID="hdfModificacion" runat="server" />
                                        <asp:HiddenField ID="hdfPermitido" runat="server" />
                                        <asp:HiddenField ID="hdfFechaIni" runat="server" />
                                        <asp:HiddenField ID="hdfFechaFin" runat="server" />
                                        <asp:HiddenField ID="hdfExtemporaneo" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Fecha" AssociatedControlID="TxtCodigoCap" runat="server" />
                                        <asp:TextBox ID="TxtFecha" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RfvTxtFecha" runat="server" ErrorMessage="Debe Ingresar la Fecha" ControlToValidate="TxtFecha" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
                                        <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlOperador_SelectedIndexChanged" AutoPostBack="true" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <%--20190306 rq013-19--%>
                                        <asp:Label Text="Gasoducto de conexión" AssociatedControlID="ddlGasoducto" runat="server" />
                                        <asp:DropDownList ID="ddlGasoducto" runat="server" CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Número de Operación" AssociatedControlID="txtOperacion" runat="server" />
                                        <asp:TextBox ID="txtOperacion" runat="server" CssClass="form-control"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftebtxtOperacion" runat="server" TargetControlID="txtOperacion" FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Tipo Punto de entrega" AssociatedControlID="ddlTipo" runat="server" />
                                        <asp:DropDownList ID="ddlTipo" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlTipo_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="E">Punto de entrega</asp:ListItem>
                                            <asp:ListItem Value="S">Punto de salida</asp:ListItem>
                                            <asp:ListItem Value="T">Punto de transferencia</asp:ListItem>
                                            <asp:ListItem Value="R">Mercado relevante</asp:ListItem>
                                            <asp:ListItem Value="G">Gasoducto dedicado</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblPunto" Text="Punto de entrega" AssociatedControlID="ddlPunto" runat="server" />
                                        <asp:DropDownList ID="ddlPunto" runat="server" CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Tipo de demanda" AssociatedControlID="ddlDemanda" runat="server" />
                                        <asp:DropDownList ID="ddlDemanda" runat="server" CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Cantidad (MBTUD)" AssociatedControlID="TxtCantidad" runat="server" />
                                        <asp:TextBox ID="TxtCantidad" runat="server" CssClass="form-control" />
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTEBTxtCantidad" runat="server" TargetControlID="TxtCantidad" FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Equivalente Kpcd" AssociatedControlID="TxtEquiv" runat="server" />
                                        <asp:TextBox ID="TxtEquiv" runat="server" CssClass="form-control"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtEquiv" runat="server" TargetControlID="TxtEquiv" FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Estado" AssociatedControlID="ddlEstado" runat="server" />
                                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="A">Activo</asp:ListItem>
                                            <asp:ListItem Value="I">Inactivo</asp:ListItem>
                                            <asp:ListItem Value="E">Eliminado</asp:ListItem>
                                            <%--20161124--%>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button Text="Salir" CssClass="btn btn-secondary" OnClientClick="this.disabled = true;" OnClick="Cancel_OnClick" UseSubmitBehavior="false" runat="server" /> <%--20201020--%>
                            <asp:Button ID="imbCrear" runat="server" CssClass="btn btn-primary" Text="Crear" OnClick="imbCrear_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="imbActualiza" runat="server" CssClass="btn btn-primary" Text="Actualizar" OnClick="imbActualiza_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
