﻿using System;
using System.Collections;
using System.Globalization;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.DataVisualization.Charting;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Verificacion_frm_ejecucionContratoMod : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Modificación  Ejecución de Contratos";
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles1(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador !=0 order by codigo_operador", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlPunto, "m_pozo", " estado = 'A'  order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMercado, "m_mercado_relevante", " estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlDemanda, "m_tipo_demanda_atender", " estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo sec ", " sec.estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlEstado, "m_estado_gas ", " tipo_estado='E' and sigla_estado <>'S' order by descripcion_estado", 2, 3);
            LlenarControles(lConexion.gObjConexion, ddlSalida, "m_punto_salida_snt pto", " pto.estado ='A' order by descripcion", 0, 2); //20190524 rq029-19

            lConexion.Cerrar();
        }
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        this.dtgConsulta.CurrentPageIndex = 0;
        CargarDatos();
    }
    
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF = DateTime.Now;
        int liValor = 0;
        string[] lsNombreParametros = { "@P_numero_operacion", "@P_contrato_definitivo", "@P_codigo_ejecucion", "@P_codigo_verif", "@P_fecha_gas_ini", "@P_fecha_gas_fin", "@P_codigo_operador", "@P_punta", "@P_estado" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "", "0", "0", "", "", "0", "0", "0" };

        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial. <br>";
            }
        }
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                if (TxtFechaIni.Text.Trim().Length > 0)
                {
                    ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
                    if (ldFechaI > ldFechaF)
                        lblMensaje.Text += "La Fecha Final NO puede ser Menor que la Fecha de Inicial. <br>";
                }
                else
                    lblMensaje.Text += "Debe digitar la fecha inicial antes que la final. <br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Final. <br>";
            }
        }
        if (TxtFechaIni.Text == "" && TxtContratoDef.Text == "" && TxtNoContrato.Text == "" && TxtCodEjec.Text == "" && TxtCodigoVerif.Text == "")
            lblMensaje.Text += "Debe digitar la fecha, el contrrato, la operación , el Id de registro o el código de ejecución para la consulta. <br>";
        if (lblMensaje.Text == "")
        {
            try
            {
                if (TxtNoContrato.Text != "")
                    lValorParametros[0] = TxtNoContrato.Text.Trim();
                lValorParametros[1] = TxtContratoDef.Text.Trim();
                if (TxtCodEjec.Text != "")
                    lValorParametros[2] = TxtCodEjec.Text.Trim();
                if (TxtCodigoVerif.Text != "")
                    lValorParametros[3] = TxtCodigoVerif.Text.Trim();

                lValorParametros[4] = TxtFechaIni.Text.Trim();
                if (TxtFechaFin.Text != "")
                    lValorParametros[5] = TxtFechaFin.Text.Trim();
                else
                    lValorParametros[5] = TxtFechaIni.Text.Trim();
                lValorParametros[6] = ddlOperador.SelectedValue;
                lValorParametros[7] = ddlPunta.SelectedValue;
                lValorParametros[8] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContEjecucionMod", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                lConexion.Cerrar();
                if (dtgConsulta.Items.Count > 0)
                {
                    tblGrilla.Visible = true;
                    //imbExcel.Visible = true;
                }
                else
                {
                    tblGrilla.Visible = false;
                    //imbExcel.Visible = false;
                    lblMensaje.Text = "No se encontraron Registros.";
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        TxtObservacion.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            try
            {
                //valida que se pueda crear o modificar el registro
                if (e.Item.Cells[20].Text != "A")  //20190524 rq029-19
                    lblMensaje.Text += "No se puede modificar un registro no correcto<br>";
                if (lblMensaje.Text == "")
                {
                    limpiarDatosUsu();
                    ddlPunto.Enabled = false;
                    TxtCantidad.Enabled = true;
                    TxtValor.Enabled = true;
                    ddlDemanda.Visible = true;
                    ddlSector.Enabled = true;
                    btnActualizar.Visible = true;
                    btnCrearUsu.Visible = true;
                    dtgUsuarios.Columns[10].Visible = true;//20190524 rq029-19
                    dtgUsuarios.Columns[11].Visible = true;//20190524 rq029-19
                    tblDetalle.Visible = true;
                    btnEliminar.Visible = false;
                    lblContratoDef.Text = e.Item.Cells[8].Text;
                    lblOperacion.Text = e.Item.Cells[7].Text;
                    
                    lblCantidad.Text = e.Item.Cells[21].Text;//20190524 rq029-19
                    lblPrecio.Text = e.Item.Cells[22].Text;//20190524 rq029-19
                    hdfCodigoVerif.Value = e.Item.Cells[23].Text;//20190524 rq029-19
                    lblFechaGas.Text = e.Item.Cells[0].Text;
                    lblSubasta.Text = e.Item.Cells[1].Text;
                    lblMercado.Text = e.Item.Cells[2].Text;
                    lblProducto.Text = e.Item.Cells[3].Text;
                    lblModalidad.Text = e.Item.Cells[4].Text;
                    ddlPunto.SelectedValue = e.Item.Cells[9].Text;
                    TxtCantidad.Text = e.Item.Cells[15].Text; //20190524 rq029-19
                    hdfCantidad.Value = TxtCantidad.Text;
                    TxtValor.Text = e.Item.Cells[17].Text; //20190524 rq029-19
                    hdfCodigoEjecC.Value = e.Item.Cells[5].Text;
                    hdfCodigoEjecV.Value = e.Item.Cells[6].Text;

                    CargarDatosDet();
                    
                    btnActualizar.Visible = true;

                    btnCrearUsu.Visible = true;

                    string[] lsNombreParametrosC = { "@P_cadena" };
                    SqlDbType[] lTipoparametrosC = { SqlDbType.VarChar };
                    string[] lValorParametrosC = { " Delete from t_contrato_ejecucion_usr where codigo_contrato_eje= 0 And login_usuario = '" + goInfo.Usuario.ToString() + "' " };
                    DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_EjecutarCadena", lsNombreParametrosC, lTipoparametrosC, lValorParametrosC);
                    CargarDatosUsu();
                    btnCrearUsu.Visible = true;
                    btnActualUsu.Visible = false;
                    tblDemanda.Visible = true;

                    tblGrilla.Visible = false;
                    tblRegCnt.Visible = true;
                    tblDetalle.Visible = true;
                    tblDatos.Visible = false;
                    tblDetalle.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas en la Recuperación de la Información. " + ex.Message.ToString();
            }
        }
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            //valida que se pueda crear o modificar el registro
            try
            {
                if (lblMensaje.Text == "")
                {
                    limpiarDatosUsu();
                    ddlPunto.Enabled = false;
                    TxtCantidad.Enabled = false;
                    TxtValor.Enabled = false;
                    ddlDemanda.Visible = false;
                    ddlSector.Enabled = false;
                    btnActualizar.Visible = false;
                    btnCrearUsu.Visible = false;
                    dtgUsuarios.Columns[10].Visible = false; //20190524 rq029-19
                    dtgUsuarios.Columns[11].Visible = false; //20190524 rq029-19
                    tblDetalle.Visible = false;
                    btnEliminar.Visible = true;

                    lblContratoDef.Text = e.Item.Cells[8].Text;
                    lblOperacion.Text = e.Item.Cells[7].Text;
                    hdfCantidad.Value = e.Item.Cells[21].Text;//20190524 rq029-19
                    lblCantidad.Text = e.Item.Cells[21].Text;//20190524 rq029-19
                    lblPrecio.Text = e.Item.Cells[22].Text;//20190524 rq029-19
                    hdfCodigoVerif.Value = e.Item.Cells[23].Text;//20190524 rq029-19
                    lblFechaGas.Text = e.Item.Cells[0].Text;
                    lblSubasta.Text = e.Item.Cells[1].Text;
                    lblMercado.Text = e.Item.Cells[2].Text;
                    lblProducto.Text = e.Item.Cells[3].Text;
                    lblModalidad.Text = e.Item.Cells[4].Text;
                    ddlPunto.SelectedValue = e.Item.Cells[9].Text;
                    TxtCantidad.Text = e.Item.Cells[15].Text;//20190524 rq029-19
                    TxtValor.Text = e.Item.Cells[17].Text;//20190524 rq029-19
                    hdfCodigoEjecC.Value = e.Item.Cells[5].Text;
                    hdfCodigoEjecV.Value = e.Item.Cells[6].Text;

                    CargarDatosUsu();
                    tblDemanda.Visible = true;

                    tblGrilla.Visible = false;
                    tblRegCnt.Visible = true;
                    tblDatos.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas en la Recuperación de la Información. " + ex.Message.ToString();
            }
        }
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_operador)
    {
        string lsCondicion = "nombre_tabla='t_contrato_ejecucion' and llave_registro='codigo_contato_eje=" + hdfCodigoEjecC.Value + "'";
        string lsCondicion1 = "codigo_contrato_eje=" + hdfCodigoEjecC.Value;
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "t_contrato_ejecucion";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
        }
        return true;
    }
    /// <summary>
    /// Realiza las Validaciones de los Tipos de Datos y Campos Obligatorios de la Pantalla
    /// </summary>
    /// <returns></returns>
    protected string validaciones()
    {
        string lsError = "";

        try
        {
            if (ddlPunto.SelectedValue == "0")
                lsError += "Debe Seleccionar el punto de entrega. <br>";
            if (TxtCantidad.Text == "")
                lsError += "Debe Ingresar la cantidad autorizada. <br>";
            if (TxtValor.Text == "")
                lsError += "Debe Ingresar el valor facturado. <br>";
            else
            {
                string[] lsValor;
                decimal ldValor;
                try
                {
                    ldValor = Convert.ToDecimal(TxtValor.Text.Trim());
                    if (ldValor < 0)
                        lsError += "Valor Inválido en el valor facturado. <br>";
                    else
                    {
                        lsValor = TxtValor.Text.Trim().Split('.');
                        if (lsValor.Length > 1)
                        {
                            if (lsValor[1].Trim().Length > 2)
                                lsError += "Valor Inválido en el valor facturado. <br>";
                        }
                    }
                }
                catch (Exception ex)
                {
                    lsError += "Valor Inválido en el valor facturado. <br>";
                }
            }
            //20190524 rq029-19
            //if (ddlDemanda.SelectedValue == "0")
            //    lsError += "Debe seleccionar el tipo de demanda. <br>";
            //if (ddlSector.SelectedValue == "0")
            //    lsError += "Debe seleccionar el sector de consumo. <br>";
            //if (dtgUsuarios.Items.Count == 0)
            //    lsError += "No ha ingresado el detalle de usuarios finales. <br>";
            //20190524 rq029-19
            if (lblTotlCantidad.Text != hdfCantidad.Value)
                lsError += "La cantidad del detalle de usuarios finales no es igual a la cantidad total de la ejecución del contrato. <br>";
            if (TxtObservacion.Text.Trim() == "")
                lsError += "Debe ingresar las obervaciones de la modificación. <br>";
            return lsError;
        }
        catch (Exception ex)
        {
            return "Problemas en la Validación de la Información. " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// Nombre: dtgUsuarios_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgUsuarios_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";

        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            try
            {
                hdfCodDatUsu.Value = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[0].Text;
                ddlDemanda.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[1].Text; //20190524 rq029-19
                ddlDemanda_SelectedIndexChanged(null, null);//20190524 rq029-19 
                ddlSector.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[3].Text; //20190524 rq029-19
                ddlSalida.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[5].Text; //20190524 rq029-19
                if (this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[7].Text != "&nbsp;")
                    TxtUsuarioFinal.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[7].Text + "-" + this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[8].Text;
                else
                    TxtUsuarioFinal.Text = "";
                ddlMercado.SelectedValue = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[9].Text;
                TxtCantidadUsr.Text = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[11].Text; //20190524 rq029-19
                btnCrearUsu.Visible = false;
                btnActualUsu.Visible = true;
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al Recuperar el Registro.! " + ex.Message.ToString();
            }

        }
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            string[] lsNombreParametros = { "@P_codigo_contrato_eje_usr", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { hdfCodDatUsu.Value, "3" };

            try
            {
                hdfCodDatUsu.Value = this.dtgUsuarios.Items[e.Item.ItemIndex].Cells[0].Text;
                lValorParametros[0] = hdfCodDatUsu.Value;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContEjecUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Eliminación de la Información del Usuario.!" + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    lConexion.Cerrar();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Eliminado Correctamente.!" + "');", true);
                    CargarDatosUsu();
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al Eliminar el Registro.! " + ex.Message.ToString();

            }
        }

    }
    /// <summary>
    /// Nombre: CargarDatosUsu
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid de Usuarios.
    /// Modificacion:
    /// </summary>
    private void CargarDatosUsu()
    {
        string[] lsNombreParametros = { "@P_codigo_contrato_eje", "@P_login" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
        string[] lValorParametros = { "-1", "" };
        lblTotlCantidad.Text = "0"; //20190524 rq029-19
        try
        {
            if (hdfCodigoEjecC.Value.Trim().Length > 0)
                lValorParametros[0] = hdfCodigoEjecC.Value;
            lValorParametros[1] = goInfo.Usuario.ToString();
            lConexion.Abrir();
            dtgUsuarios.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContEjecUsr", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgUsuarios.DataBind();
            lConexion.Cerrar();
            //20190524 rq029-19
            if (dtgUsuarios.Items.Count > 0)
            {
                foreach (DataGridItem Grilla in this.dtgUsuarios.Items)
                {
                    lblTotlCantidad.Text = (Convert.ToDecimal(lblTotlCantidad.Text) + Convert.ToDecimal(Grilla.Cells[11].Text.Trim().Replace(",", ""))).ToString();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo consultar la información de usuarios finales.! " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// Nombre: CargarDatosUsu
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid de Usuarios.
    /// Modificacion:
    /// </summary>
    private void CargarDatosDet()
    {
        string[] lsNombreParametros = { "@P_codigo_verif", "@P_fecha_gas", "@P_punta" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
        string[] lValorParametros = { hdfCodigoVerif.Value, lblFechaGas.Text, "" };
        try
        {
            lConexion.Abrir();
            dtgDetalle.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContEjecucion1", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgDetalle.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImbSalir_Click(object sender, ImageClickEventArgs e)
    {
        tblDemanda.Visible = false;
        tblGrilla.Visible = true;
        tblDatos.Visible = true;
        CargarDatos();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImmSalirVer_Click(object sender, ImageClickEventArgs e)
    {
        tblDemanda.Visible = false;
        tblGrilla.Visible = true;
        tblDatos.Visible = true;
        CargarDatos();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCrearUsu_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_contrato_eje_usr", "@P_codigo_contrato_eje","@P_tipo_demanda","@P_sector_consumo","@P_codigo_punto_salida", "@P_nit_usuario_no_regulado", "@P_codigo_mercado_relevante","@P_cantidad_contratada","@P_accion" //20190524 rq029-19
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int , SqlDbType.Int //20190524 rq029-19
                                      };
        string[] lValorParametros = { "0", "0", "0","0","0","", "0","0", "1" //20190524 rq029-19
                                    };

        lblMensaje.Text = "";
        string[] lsUsuario;
        lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
        try
        {
            //20190524 rq029-19
            if (ddlDemanda.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el tipo de demanda<br>";
            if (ddlSector.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el sector de consumo<br>";
            if (ddlSalida.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el punto de salida<br>";
            //20190524 fin rq029-19
            lConexion.Abrir();
            if (TxtUsuarioFinal.Enabled)
            {
                if (TxtUsuarioFinal.Text.Trim() == "")
                    lblMensaje.Text += "Debe Ingresar el Usuario no regulado<br>";
                else
                {

                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                    if (!lLector.HasRows)
                        lblMensaje.Text += "El Usuario Ingresado NO existe en la Base de Datos.! <br>";
                    lLector.Close();
                }
            }
            if (ddlMercado.Enabled)
            {
                if (ddlMercado.SelectedValue == "0")
                    lblMensaje.Text += "Debe Seleccionar el mercado relevante. <br> ";
            }
            //20190524 rq029-19
            if (TxtCantidadUsr.Text == "")
                lblMensaje.Text += "Debe ingresar la cantidad contratada. <br> ";
            else
            {
                int liValor = Convert.ToInt32(TxtCantidadUsr.Text.Trim());
                if (liValor <= 0)
                    lblMensaje.Text += "La cantidad contratada debe ser mayor que cero. <br> ";
                //else
                //    if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(hdfCantidad.Value))
                //    lblMensaje.Text += "La cantidad Acumulada de Usuarios No puede ser Mayor que " + hdfCantidad.Value + ". <br> ";
            }
            if (lblMensaje.Text == "")
            {
                if (hdfCodigoEjecC.Value != "")
                    lValorParametros[1] = hdfCodigoEjecC.Value;
                lValorParametros[2] = ddlDemanda.SelectedValue; //20190524 rq029-19
                lValorParametros[3] = ddlSector.SelectedValue; //20190524 rq029-19
                lValorParametros[4] = ddlSalida.SelectedValue; //20190524 rq029-19
                lValorParametros[5] = lsUsuario[0].Trim();
                lValorParametros[6] = ddlMercado.SelectedValue;
                lValorParametros[7] = TxtCantidadUsr.Text; //20190524 rq029-19

                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContEjecUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación de la Información del Usuario.!" + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Información de Usuarios Finales Ingresada Correctamente.!" + "');", true);
                    limpiarDatosUsu(); //20190524 rq029-19
                    CargarDatosUsu();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnActualUsu_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_contrato_eje_usr", "@P_codigo_contrato_eje","@P_tipo_demanda","@P_sector_consumo","@P_codigo_punto_salida", "@P_nit_usuario_no_regulado", "@P_codigo_mercado_relevante","@P_cantidad_contratada","@P_accion" //20190524 rq029-19
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int //20190524 rq029-19
                                      };
        string[] lValorParametros = { "0", "0","0","0","0", "", "0","0", "2" //20190524 rq029-19
                                    };
        lblMensaje.Text = "";
        string[] lsUsuario;
        lsUsuario = TxtUsuarioFinal.Text.Trim().Split('-');
        try
        {
            //20190524 rq029-19
            if (ddlDemanda.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el tipo de demanda<br>";
            if (ddlSector.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el sector de consumo<br>";
            if (ddlSalida.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el punto de salida<br>";
            //20190524 fin rq029-19
            lConexion.Abrir();
            if (TxtUsuarioFinal.Enabled)
            {
                if (TxtUsuarioFinal.Text.Trim() == "")
                    lblMensaje.Text += "Debe Ingresar el Usuario no regulado<br>";
                else
                {

                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", " no_documento = '" + lsUsuario[0].Trim() + "' ");
                    if (!lLector.HasRows)
                        lblMensaje.Text += "El Usuario Ingresado NO existe en la Base de Datos.! <br>";
                    lLector.Close();
                }
            }
            if (ddlMercado.Enabled)
            {
                if (ddlMercado.SelectedValue == "0")
                    lblMensaje.Text += "Debe Seleccionar el mercado relevante. <br> ";
            }
            //20190524 rq029-19
            if (TxtCantidadUsr.Text == "")
                lblMensaje.Text += "Debe ingresar la cantidad contratada. <br> ";
            else
            {
                int liValor = Convert.ToInt32(TxtCantidadUsr.Text.Trim());
                if (liValor <= 0)
                    lblMensaje.Text += "La cantidad contratada debe ser mayor que cero. <br> ";
                //else
                //    if (liValor + Convert.ToDecimal(lblTotlCantidad.Text) > Convert.ToDecimal(hdfCantidad.Value))
                //    lblMensaje.Text += "La cantidad Acumulada de Usuarios No puede ser Mayor que " + hdfCantidad.Value + ". <br> ";
            }
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = hdfCodDatUsu.Value;
                if (hdfCodigoEjecC.Value != "")
                    lValorParametros[1] = hdfCodigoEjecC.Value;
                lValorParametros[2] = ddlDemanda.SelectedValue; //20190524 rq029-19
                lValorParametros[3] = ddlSector.SelectedValue; //20190524 rq029-19
                lValorParametros[4] = ddlSalida.SelectedValue; //20190524 rq029-19
                lValorParametros[5] = lsUsuario[0].Trim();
                lValorParametros[6] = ddlMercado.SelectedValue;
                lValorParametros[7] = TxtCantidadUsr.Text; //20190524 rq029-19
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContEjecUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización de la Información del Usuario.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Información de Usuarios Finales Actualizada Correctamente.!" + "');", true);
                    limpiarDatosUsu();//20190524 rq029-19
                    btnCrearUsu.Visible = true;
                    btnActualUsu.Visible = false;
                    CargarDatosUsu();
                    btnActualUsu.Visible = false;
                    btnCrearUsu.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }

    protected void btnActualizar_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_ejecucion_c", "@P_codigo_ejecucion_v", "@P_cantidad_autorizada", "@P_valor_facturado",  "@P_observacion_mod" };//20190524 rq029-19
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal,SqlDbType.VarChar };//20190524 rq029-19
        string[] lValorParametros = { "0", "0", "0", "0", "" };//20190524 rq029-19
        try
        {
            lblMensaje.Text = validaciones();
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = hdfCodigoEjecC.Value;
                lValorParametros[1] = hdfCodigoEjecV.Value;
                lValorParametros[2] = TxtCantidad.Text;
                lValorParametros[3] = TxtValor.Text;
                lValorParametros[4] = TxtObservacion.Text;

                lConexion.Abrir();
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetContEjecMod", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensaje.Text = "Se presentó un Problema en la Modificación del Registro.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                            lblMensaje.Text += lLector["error"].ToString() + "<br>";
                        lConexion.Cerrar();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Modificado Correctamente.!" + "');", true);
                        tblRegCnt.Visible = false;
                        tblDetalle.Visible = false;
                        tblGrilla.Visible = true;
                        tblDatos.Visible = true;
                        tblDemanda.Visible = false;
                        limpiarDatosUsu();//20190524 rq029-19
                        lConexion.Cerrar();
                        CargarDatos();
                    }

                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {

        string[] lsNombreParametros = { "@P_codigo_contrato_eje", "@P_ind_modifica", "@P_observacion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "S", "" };
        try
        {
            lblMensaje.Text = "";
            if (TxtObservacion.Text.Trim() == "")
                lblMensaje.Text = "Debe ingresar las observaciones de la eliminación";
            if (lblMensaje.Text == "")
            {
                lValorParametros[2] = TxtObservacion.Text;
                lConexion.Abrir();

                if (hdfCodigoEjecC.Value != "0")
                {
                    lValorParametros[0] = hdfCodigoEjecC.Value;

                    goInfo.mensaje_error = "";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_DelContEjec", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Modificación del Registro.! " + goInfo.mensaje_error.ToString();
                    }
                    lLector.Dispose();
                    lLector.Close();
                }
                if (hdfCodigoEjecV.Value != "0")
                {
                    lValorParametros[0] = hdfCodigoEjecV.Value;

                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_DelContEjec", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Modificación del Registro.! " + goInfo.mensaje_error.ToString();
                    }
                    lLector.Dispose();
                    lLector.Close();
                }
                lConexion.Cerrar();
                if (lblMensaje.Text == "")
                    btnRegresar_Click(null, null);
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    protected void btnRegresar_Click(object sender, EventArgs e)
    {
        tblGrilla.Visible = true;
        tblRegCnt.Visible = false;
        tblDetalle.Visible = false;
        tblDatos.Visible = true;
        tblDemanda.Visible = false;
        TxtCantidad.Text = "";
        TxtValor.Text = "";
        ddlPunto.SelectedValue = "0";
        ddlSector.SelectedValue = "0";
        ddlDemanda.SelectedValue = "0";
        lblMensaje.Text = "";
        CargarDatos();
    }
    /// <summary>
    /// Exportacion a Excel de la Información  de la Grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbExcel_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfEjecucion" + DateTime.Now + ".xls";
        string lstitulo_informe = "";
        string lsTituloParametros = "";
        try
        {
            lstitulo_informe = "Consulta Modificaión  ejecución de contratos ";
            lsTituloParametros = "";

            if (TxtContratoDef.Text.Trim().Length > 0)
                lsTituloParametros = "Contrato: " + TxtContratoDef.Text.Trim();
            if (TxtNoContrato.Text.Trim().Length > 0)
                lsTituloParametros = "No. Operación: " + TxtNoContrato.Text.Trim();
            if (TxtCodEjec.Text.Trim().Length > 0)
                lsTituloParametros += " - Código Ejecución: " + TxtCodEjec.Text.Trim();
            if (TxtCodigoVerif.Text.Trim().Length > 0)
                lsTituloParametros += " - Id registro: " + TxtCodigoVerif.Text.Trim();
            if (TxtFechaIni.Text.Trim().Length > 0)
                lsTituloParametros += " - Fecha Inicial: " + TxtFechaIni.Text.Trim();
            if (TxtFechaFin.Text.Trim().Length > 0)
                lsTituloParametros += " - Fecha Final: " + TxtFechaFin.Text.Trim();
            if (ddlOperador.SelectedValue != "0")
                lsTituloParametros += " - Operador: " + ddlOperador.SelectedItem.ToString();
            if (ddlPunta.SelectedValue != "0")
                lsTituloParametros += " - Punta: " + ddlPunta.SelectedItem.ToString();
            if (ddlEstado.SelectedValue != "0")
                lsTituloParametros += " - Estado: " + ddlEstado.SelectedItem.ToString();
            dtgConsulta.Columns[24].Visible = true;
            dtgConsulta.Columns[25].Visible = true;
            dtgConsulta.Columns[26].Visible = false;
            dtgConsulta.Columns[27].Visible = false;

            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgConsulta.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            lform.Controls.Add(dtgConsulta);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lConexion.CerrarInforme();

            dtgConsulta.Columns[24].Visible = false;
            dtgConsulta.Columns[25].Visible = false;
            dtgConsulta.Columns[26].Visible = true;
            dtgConsulta.Columns[27].Visible = true;

        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Excel.!" + ex.Message.ToString();
        }

    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lsTable, string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lsTable, lswhere, goInfo);
    }
    /// <summary>
    /// Nombre: limpiarDatosUsu
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected void limpiarDatosUsu()
    {
        ddlDemanda.SelectedValue = "0"; //20190524 rq029-19
        ddlSector.SelectedValue = "0";//20190524 rq029-19
        ddlSalida.SelectedValue = "0";//20190524 rq029-19
        ddlMercado.SelectedValue = "0";
        TxtUsuarioFinal.Text = "";
        TxtCantidadUsr.Text = "";//20190524 rq029-19
    }
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected void limpiarDatos()
    {
        ddlPunto.SelectedValue = "0";
        TxtCantidad.Text = "";
        TxtValor.Text = "";
        //ddlDemanda.SelectedValue = "0"; //20190524 rq029-19
        //ddlSector.SelectedValue = "0"; //20190524 rq029-19
        TxtObservacion.Text = "";
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlDemanda_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlDemanda.SelectedValue == "1")
        {
            TxtUsuarioFinal.Text = "";
            TxtUsuarioFinal.Enabled = false;
            ddlMercado.Enabled = true;
        }
        else
        {
            TxtUsuarioFinal.Enabled = true;
            ddlMercado.SelectedValue = "0";
            ddlMercado.Enabled = false;
        }
        //20190524 rq029-19
        //if (hdfActBorrUsr.Value == "S")
        //{
        //    string[] lsNombreParametros = { "@P_codigo_contrato_eje", "@P_accion" };
        //    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
        //    string[] lValorParametros = { hdfCodigoEjecC.Value, "4" };

        //    try
        //    {
        //        lConexion.Abrir();
        //        DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContEjecUsr", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
        //        CargarDatosUsu();
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}

    }
    protected void TxtCantidad_TextChanged(object sender, EventArgs e)
    {
        hdfCantidad.Value = TxtCantidad.Text;
        if (hdfCantidad.Value == "")
            hdfCantidad.Value = "0";
    }
}