﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Verificacion
{
    public partial class frm_EnergiaTomada : Page
    {
        private InfoSessionVO goInfo = null;
        private static string lsTitulo = "Cantidad de Energía Tomada"; //20160810 modalidad por tipo campo 
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        private clConexion lConexion = null;
        private clConexion lConexion1 = null;
        private SqlDataReader lLector;
        private SqlDataReader lLector1;

        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            //Establese los permisos del sistema
            //EstablecerPermisosSistema(); //20220621 ajuste
            lConexion = new clConexion(goInfo);
            lConexion1 = new clConexion(goInfo);
            //Titulo
            Master.Titulo = "Registros Operativos";
            /// Activacion de los Botones
            buttons.Inicializar(ruta: "t_energia_tomada");
            buttons.CrearOnclick += btnNuevo;

            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.ExportarPdfOnclick += ImgPdf_Click;

            if (IsPostBack) return;
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles1(lConexion.gObjConexion, ddlTramo, "m_tramo", " estado = 'A' order by descripcion", 0, 10);
            LlenarControles2(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador >0 and (tipo_operador = 'T' or codigo_operador = " + goInfo.cod_comisionista + ") order by razon_social", 0, 4);  //20180122 rq003-18 // 20180126 rq107-16
            LlenarControles2(lConexion.gObjConexion, ddlRemitente, "m_operador", " estado = 'A' order by razon_social", 0, 4);  //20180122 rq003-18 // 20180126 rq107-16
            if (goInfo.cod_comisionista != "0")
                LlenarControles(lConexion.gObjConexion, ddlBusPozo, "m_punto_salida_snt sal, m_punto_operador ope", " sal.estado = 'A' and sal.codigo_punto_salida= ope.codigo_punto and ope.tipo_punto ='S' and (ope.codigo_operador =0 or ope.codigo_operador =" + goInfo.cod_comisionista + ") and ope.estado='A' order by descripcion", 0, 2); //20220705
            else
                LlenarControles(lConexion.gObjConexion, ddlBusPozo, "m_punto_salida_snt", " estado = 'A' order by descripcion", 0, 2);
            LlenarControles2(lConexion.gObjConexion, ddlBusOperador, "m_operador", " estado = 'A' and codigo_operador >0 and (tipo_operador = 'T' or codigo_operador = " + goInfo.cod_comisionista + ") order by razon_social", 0, 4);  //20180122 rq003-18 // 20180126 rq107-16
            LlenarControles1(lConexion.gObjConexion, ddlBusTramo, "m_tramo", " estado = 'A' order by descripcion", 0, 10);
            LlenarControles2(lConexion.gObjConexion, ddlBusRemitente, "m_operador", " estado = 'A' order by razon_social", 0, 4); // 20180126 rq107-16
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_no_ing_inf_ope", " codigo_reporte = 7 and codigo_operador =" + goInfo.cod_comisionista + " and estado ='A' ");
            if (lLector.HasRows)
            {
                lblMensaje.Text = "El Operador no está obligado a ingresar la información operativa de energía tomada.";
            }
            lLector.Close();
            lLector.Dispose();

            lConexion.Cerrar();
            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlBusOperador.SelectedValue = goInfo.cod_comisionista;
                ddlBusOperador.Enabled = false;
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
                ddlOperador_SelectedIndexChanged(null, null); // 20160721 carg ptdv
                hdnELiminado.Value = "N";  //20160721 modif inf opertiva y transaccional
            }
            else
                hdnELiminado.Value = "S";  //20160721 modif inf opertiva y transaccional

            DateTime ldfecha;
            ldfecha = DateTime.Now.AddDays(-1);
            TxtFecha.Text = ldfecha.Year.ToString() + "/" + ldfecha.Month.ToString() + "/" + ldfecha.Day.ToString();
            TxtFecha.Enabled = false;

            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            Buscar();
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }

        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "t_energia_tomada");
            //hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
            //hlkListar.Enabled = (Boolean)permisos["SELECT"];
            //hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
            //if (Session["tipoPerfil"].ToString() != "N")
            //    dtgMaestro.Columns[15].Visible = (Boolean)permisos["UPDATE"]; //20160711 ajuste modififaciones  //20160803 energia tomada
            //else
            //    dtgMaestro.Columns[15].Visible = false;
            //dtgMaestro.Columns[16].Visible = false; //20160711 ajuste modififaciones //20160803 energia tomada
            //20220621 ajsute
            if ((bool)permisos["UPDATE"] && (bool)permisos["DELETE"])
                dtgMaestro.Columns[17].Visible = true; //20220705
            else
            {
                if (!(bool)permisos["UPDATE"] && !(bool)permisos["DELETE"])
                    dtgMaestro.Columns[17].Visible = false; //20220705
                else
                {
                    if (!(bool)permisos["UPDATE"])
                    {
                        foreach (DataGridItem Grilla in dtgMaestro.Items)
                        {
                            var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                            lkbModificar.Visible = false;
                        }
                    }
                    if (!(bool)permisos["DELETE"])
                    {
                        foreach (DataGridItem Grilla in dtgMaestro.Items)
                        {
                            var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
                            lkbEliminar.Visible = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Nombre: Nuevo
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
        /// Modificacion:
        /// </summary>
        private void Nuevo()
        {
            int liHoraAct = 0;
            string lsHoraMax = "0";
            int liValFin = 0;
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
            if (lLector.HasRows)
            {
                lLector.Read();
                lsHoraMax = lLector["hora_max_declara_opera"].ToString();
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
            liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
            liValFin = ((Convert.ToDateTime(lsHoraMax).Hour * 60) + Convert.ToDateTime(lsHoraMax).Minute);
            if (liHoraAct > liValFin)
            {
                Toastr.Warning(this, "Está intentando Registrar Información Fuera del Horario.");
                //Cierra el modal de Agregar
                Modal.Cerrar(this, registroEnergiaTomada.ID);

            }
            else
            {
                //Abre el modal de Agregar
                Modal.Abrir(this, registroEnergiaTomada.ID, registroEnergiaTomadaInside.ID);

                imbCrear.Visible = true;
                imbActualiza.Visible = false;
                lblTitulo.Text = "Crear " + lsTitulo;
                TxtCodigoCap.Visible = false;
                LblCodigoCap.Visible = true;
                LblCodigoCap.Text = "Automatico";
                TxtCodigoCap.Text = "0"; //20220705
                ddlRemitente.Enabled = true;
                ddlPozo.Enabled = true;
                ddlTramo.Enabled = true;
            }
            ddlEstado.SelectedValue = "A"; //20161122 modif inf ope
            ddlEstado.Enabled = false;  // 20161122 modif inf ope
        }

        /// <summary>
        /// Nombre: Listar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            CargarDatos();
            lblTitulo.Text = lsTitulo;
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            if (modificar != null && modificar != "")
            {
                try
                {
                    lblMensaje.Text = "";
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_energia_tomada", " codigo_energia_tomada= " + modificar + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            LblCodigoCap.Text = lLector["codigo_energia_tomada"].ToString();
                            TxtCodigoCap.Text = lLector["codigo_energia_tomada"].ToString();
                            DateTime ldFecha; //20160711 ajuste msificaciones
                            ldFecha = Convert.ToDateTime(lLector["fecha"].ToString()); //20160711 ajuste msificaciones
                            TxtFecha.Text = ldFecha.Year.ToString() + "/" + ldFecha.Month.ToString() + "/" + ldFecha.Day.ToString(); //20160711 ajuste msificaciones
                            try
                            {
                                ddlOperador.SelectedValue = lLector["codigo_Operador"].ToString();
                                ddlOperador_SelectedIndexChanged(null, null); //20190306 rq013-19
                            }
                            catch (Exception)
                            {
                                lblMensaje.Text += "El operador del registro no existe o está inactivo<br>";
                            }
                            try
                            {
                                ddlRemitente.SelectedValue = lLector["codigo_remitente"].ToString();  //20161122 modif inf ope
                            }
                            catch (Exception)
                            {
                                lblMensaje.Text += "El remitente del registro no existe o está inactivo<br>";
                            }
                            try
                            {
                                ddlTramo.SelectedValue = lLector["codigo_tramo"].ToString();
                                ddlTramo_SelectedIndexChanged(null, null); //20190306 rq013-19
                            }
                            catch (Exception)
                            {
                                lblMensaje.Text += "El Tramo no existe o está inactivo<br>";
                            }
                            ddlTramo_SelectedIndexChanged(null, null);
                            try
                            {
                                ddlPozo.SelectedValue = lLector["codigo_punto_salida"].ToString();
                            }
                            catch (Exception)
                            {
                                lblMensaje.Text += "El punto del SNT del registro no existe o está inactivo<br>";
                            }
                            TxtContrato.Text = lLector["numero_contrato"].ToString();
                            TxtCantidad.Text = lLector["energia_tomada"].ToString();
                            ddlEstado.SelectedValue = lLector["estado"].ToString();
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                            TxtCodigoCap.Visible = false;
                            LblCodigoCap.Visible = true;
                            TxtFecha.Enabled = false;
                            ddlOperador.Enabled = false;
                            //ddlRemitente.Enabled = false;   //20161122 modif inf ope
                            //ddlPozo.Enabled = false;  //20161122  modif inf ope
                            //ddlTramo.Enabled = false;  //20161122 modif inf ope
                            ddlEstado.Enabled = true; //20161122 modif inf ope
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Bloquea el Registro a Modificar
                        if (lblMensaje.Text == "") //20160809 carga ptdv
                            manejo_bloqueo("A", modificar);
                    }
                    else
                    {
                        Listar();
                        lblMensaje.Text = "No se Puede editar el Registro porque está Bloqueado. Código energía tomada" + modificar.ToString();

                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = ex.Message;
                }
            }
            if (lblMensaje.Text == "")
            {
                //Abre el modal de Agregar
                Modal.Abrir(this, registroEnergiaTomada.ID, registroEnergiaTomadaInside.ID);
                lblTitulo.Text = "Modificar " + lsTitulo;
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }

        /// <summary>
        /// Nombre: Buscar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
        /// Modificacion:
        /// </summary>
        private void Buscar()
        {
            lblTitulo.Text = lsTitulo;
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_codigo_energia_tomada", "@P_fecha", "@P_codigo_operador", "@P_codigo_Pozo", "@P_fecha_fin", "@P_codigo_remitente", "@P_tramo", "@P_mostrar_eliminado" }; //20160721 modif inf opertiva y trans
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar }; //20160721 modif inf opertiva y trans
            string[] lValorParametros = { "0", "", "0", "0", "", ddlBusRemitente.SelectedValue, ddlBusTramo.SelectedValue, hdnELiminado.Value }; //20160721 modif inf opertiva y trans
            DateTime ldFecha;
            lblMensaje.Text = "";

            try
            {
                if (TxtBusFecha.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtBusFecha.Text.Trim());
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Valor Inválido en Fecha Inicial. <br>";
                    }
                }
                if (TxtBusFechaF.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtBusFechaF.Text.Trim());
                        if (ldFecha < Convert.ToDateTime(TxtBusFecha.Text.Trim()))
                            lblMensaje.Text += "La Fecha final NO puede ser menor que la Fecha Inicial.!<br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Valor Inválido en Fecha Final. <br>";
                    }
                }
                if (lblMensaje.Text == "")
                {
                    if (TxtBusCodigo.Text.Trim().Length > 0)
                        lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    if (TxtBusFecha.Text.Trim().Length > 0)
                        lValorParametros[1] = TxtBusFecha.Text.Trim();
                    if (ddlBusOperador.SelectedValue != "0")
                        lValorParametros[2] = ddlBusOperador.SelectedValue;
                    if (ddlBusPozo.SelectedValue != "0")
                        lValorParametros[3] = ddlBusPozo.SelectedValue;
                    if (TxtBusFechaF.Text.Trim().Length > 0)
                        lValorParametros[4] = TxtBusFechaF.Text.Trim();
                    else
                    {
                        if (TxtBusFecha.Text.Trim().Length > 0)
                            lValorParametros[4] = TxtBusFecha.Text.Trim();
                    }
                    if (TxtBusCodigo.Text.Trim().Length <= 0 && TxtBusFecha.Text.Trim().Length <= 0 && ddlBusOperador.SelectedValue == "0" && ddlBusPozo.SelectedValue == "0" && ddlBusTramo.SelectedValue == "0" && ddlBusRemitente.SelectedValue == "0")
                    {
                        lValorParametros[1] = TxtFecha.Text.Trim();
                        lValorParametros[4] = TxtFecha.Text.Trim();
                    }
                    lConexion.Abrir();
                    dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetEnergiaTom", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgMaestro.DataBind();
                    lConexion.Cerrar();
                    EstablecerPermisosSistema(); //20220621 ajuste
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }

        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            lblMensaje.Text = "";
            this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();

        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            string lCodigoRegistro = "";
            lblMensaje.Text = "";
            string lsFecha = ""; // Cambio Req. 003-17 20170216
            hdfModificacion.Value = "N"; // Cambio Req. 003-17 2017021
            try
            {
                lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text; // Cambio Req. 003-17 20170216
                lsFecha = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[1].Text;
            }
            catch
            {

                return;
            }
            // Cambio Req. 003-17 20170216
            ///////////////////////////////////////////////////////////////////////////////////
            ///// Control Nuevo para Modif Inf Operativa Participantes Req. 003-17 20170216 ///
            ///////////////////////////////////////////////////////////////////////////////////
            bool blPuede = true;
            int liHoraAct = 0;
            string lsHoraMax = "0";
            int liValFin = 0;
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
            if (lLector.HasRows)
            {
                lLector.Read();
                lsHoraMax = lLector["hora_max_declara_opera"].ToString();
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
            liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
            liValFin = ((Convert.ToDateTime(lsHoraMax).Hour * 60) + Convert.ToDateTime(lsHoraMax).Minute);
            if (liHoraAct > liValFin)
            {
                if (Session["tipoPerfil"].ToString() == "N")
                {
                    Toastr.Warning(this, "Está intentando modificar o eliminar información fuera del horario.");
                    blPuede = false;
                }
                else
                    hdfModificacion.Value = "S";
            }
            if (Convert.ToDateTime(lsFecha) != DateTime.Now.Date.AddDays(-1))
            {
                if (Session["tipoPerfil"].ToString() == "N")
                {
                    blPuede = false;
                    Toastr.Warning(this, "Está intentando modificar o eliminar información que no es del día anterior a la fecha del sistema.");
                }
                else
                    hdfModificacion.Value = "S";
            }
            if (blPuede)
            {
                if (e.CommandName.Equals("Modificar"))
                {
                    mdlRegistroEnergiaTomadaLabel.InnerText = "Modificar";
                    Modificar(lCodigoRegistro);
                }
                // Evento Eliminar para los Participantes Modif Inf Operativa Participantes Req. 003-17 20170216
                if (e.CommandName.Equals("Eliminar"))
                {
                    string[] lsNombreParametros = { "@P_codigo_energia_tomada", "@P_fecha", "@P_codigo_operador", "@P_codigo_remitente", "@P_codigo_punto_salida", "@P_codigo_tramo", "@P_numero_contrato", "@P_energia_tomada", "@P_estado", "@P_accion" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
                    string[] lValorParametros = { lCodigoRegistro, "", "0", "0", "0", "0", "0", "0", "E", "3" };
                    lblMensaje.Text = "";
                    try
                    {
                        if (lblMensaje.Text == "")
                        {
                            lConexion.Abrir();
                            if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaTom", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                            {
                                lblMensaje.Text = "Se presentó un Problema en la Eliminación del Registro.! " + goInfo.mensaje_error.ToString();
                                lConexion.Cerrar();
                            }
                            else
                            {
                                Toastr.Success(this, "Registro Eliminado Correctamente");
                                Listar();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        /// Desbloquea el Registro Actualizado
                        lblMensaje.Text = ex.Message;
                    }
                    if (lblMensaje.Text != "")
                    {
                        Toastr.Warning(this, lblMensaje.Text);
                        lblMensaje.Text = "";
                    }
                }

            }
            ///////////////////////////////////////////////////////////////////////////////////
            //if (((LinkButton)e.CommandSource).Text == "Modificar")
            //{
            //    lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            //    Modificar(lCodigoRegistro);
            //}
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lsTabla, string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia(lsTabla, lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// 20180122 rq003-18
        /// // 20180126 rq107-16
        protected void LlenarControles2(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceLlave).ToString() + "-" + lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
            //dtgComisionista.DataBind();

            // ajuste a ptdv 20160721
            string[] lsNombreParametros = { "@P_codigo_trasportador" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int };
            string[] lValorParametros = { ddlOperador.SelectedValue };
            if (ddlOperador.SelectedValue == "")
                lValorParametros[0] = "0";

            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetTramo", lsNombreParametros, lTipoparametros, lValorParametros);
            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);
            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["desc_tramo"].ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }


        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='t_energia_tomada' and llave_registro='codigo_energia_tomada=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_energia_tomada=" + lscodigo_registro.ToString();
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_energia_tomada";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_energia_tomada", lsCondicion1);
            }
            return true;
        }
        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] lValorParametros = { "0", "", "0", "0", "", "0", "0" };
            string lsParametros = "";

            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    lsParametros += " Código energía : " + TxtBusCodigo.Text;
                }
                if (TxtBusFecha.Text.Trim().Length > 0)
                {
                    lValorParametros[1] = TxtBusFecha.Text.Trim();
                    lsParametros += " Fecha Inicial : " + TxtBusFecha.Text;
                }
                if (ddlBusOperador.SelectedValue != "0")
                {
                    lValorParametros[2] = ddlBusOperador.SelectedValue;
                    lsParametros += " Operador: " + ddlBusOperador.SelectedItem;
                }
                if (ddlBusPozo.SelectedValue != "0")
                {
                    lValorParametros[3] = ddlBusPozo.SelectedValue;
                    lsParametros += " Pozo: " + ddlBusPozo.SelectedItem;
                }
                if (TxtBusFechaF.Text.Trim().Length > 0)
                {
                    lValorParametros[4] = TxtBusFechaF.Text.Trim();
                    lsParametros += " Fecha Final : " + TxtBusFechaF.Text;
                }
                else
                {
                    if (TxtBusFecha.Text.Trim().Length > 0)
                    {
                        lValorParametros[4] = TxtBusFecha.Text.Trim();
                        lsParametros += " Fecha Final : " + TxtBusFecha.Text;
                    }
                }
                if (ddlBusRemitente.SelectedValue != "0")
                {
                    lValorParametros[5] = ddlBusRemitente.SelectedValue;
                    lsParametros += " Remitente: " + ddlBusRemitente.SelectedItem;
                }
                if (ddlBusTramo.SelectedValue != "0")
                {
                    lValorParametros[6] = ddlBusTramo.SelectedValue;
                    lsParametros += " Tramo: " + ddlBusTramo.SelectedItem;
                }

                if (TxtBusCodigo.Text.Trim().Length <= 0 && TxtBusFecha.Text.Trim().Length <= 0 && ddlBusOperador.SelectedValue == "0" && ddlBusPozo.SelectedValue == "0" && ddlBusTramo.SelectedValue == "0" && ddlBusRemitente.SelectedValue == "0")
                {
                    lValorParametros[1] = TxtFecha.Text.Trim();
                    lValorParametros[4] = TxtFecha.Text.Trim();
                }
                Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_getEnergiaTom&nombreParametros=@P_codigo_energia_tomada*@P_fecha*@P_codigo_operador*@P_codigo_Pozo*@P_fecha_fin*@P_codigo_remitente*@P_tramo&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "*" + lValorParametros[3] + "*" + lValorParametros[4] + "*" + lValorParametros[5] + "*" + lValorParametros[6] + "&columnas=codigo_energia_tomada*fecha*codigo_operador*nombre_operador*codigo_remitente*nombre_remitente*codigo_Pozo*desc_Pozo*codigo_modalidad*desc_modalidad*numero_contrato*energia_tomada*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado de energia tomada&TituloParametros=" + lsParametros);
            }
            catch (Exception)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.!";
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }

        }
        /// <summary>
        /// Nombre: ImgPdf_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgPdf_Click(object sender, EventArgs e)
        {
            try
            {
                string lsCondicion = " codigo_energia_tomada <> '0'";
                Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=t_energia_tomada&procedimiento=pa_ValidarExistencia&columnas=codigo_energia_tomada*fecha*codigo_operador*codigo_remitente*codigo_Pozo*codigo_modalidad*numero_contrato*energia_tomada*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
            }
            catch (Exception)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.!";
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlTramo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTramo.SelectedValue != "0")
            {
                lConexion.Abrir();
                ddlPozo.Items.Clear();
                LlenarControles(lConexion.gObjConexion, ddlPozo, "m_punto_salida_snt sal, m_punto_operador ope", " sal.estado = 'A' And sal.codigo_tramo = " + ddlTramo.SelectedValue + " and sal.codigo_punto_salida= ope.codigo_punto and ope.tipo_punto ='S' and (ope.codigo_operador =0 or ope.codigo_operador =" + ddlOperador.SelectedValue + ") and ope.estado='A' order by descripcion", 0, 2); //20220705
                lConexion.Cerrar();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_energia_tomada", "@P_fecha", "@P_codigo_operador", "@P_codigo_remitente", "@P_codigo_punto_salida", "@P_codigo_tramo", "@P_numero_contrato", "@P_energia_tomada", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "", "0", "0", "0", "0", "0", "", "", "1" };
            lblMensaje.Text = "";
            int liValor = 0;
            try
            {
                if (TxtFecha.Text == "")
                    lblMensaje.Text += " Debe digitar la fecha<br>";
                if (ddlOperador.SelectedValue == "0")
                    lblMensaje.Text += "Debe seleccionar el operador<br>";
                if (ddlRemitente.SelectedValue == "0")
                    lblMensaje.Text += "Debe seleccionar el remitente<br>";
                if (ddlTramo.SelectedValue == "0")
                    lblMensaje.Text += "Debe seleccionar el Tramo<br>";
                if (ddlPozo.SelectedValue == "0" || ddlPozo.SelectedValue == "") //20190306 rq013-19
                    lblMensaje.Text += "Debe seleccionar el Punto de salida del SNT<br>"; //20190306 rq013-19
                else //20220705
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_punto_operador", " tipo_punto= 'S' and codigo_punto =" + ddlPozo.SelectedValue + " and ( codigo_operador =0 or  codigo_operador= " + ddlOperador.SelectedValue + ") and estado ='A'", goInfo))
                        lblMensaje.Text = "El punto de salida no está asociado al operador<br>";
                }
                if (TxtContrato.Text == "")
                    lblMensaje.Text += " Debe digitar el contrato<br>";
                if (TxtContrato.Text.Trim() == "")
                    lblMensaje.Text += " Debe digitar el No. de Contrato<br>";
                else  //20160803 enegia tomada
                {
                    Regex oExpresion = new Regex(@"^([A-Za-z0-9]*$)");
                    if (!oExpresion.IsMatch(TxtContrato.Text))
                        lblMensaje.Text += "El No. de Contrato solo debe tener letras o números<br>";
                }

                //else
                //    if (!VerificarExistencia("t_contrato_verificacion", "contrato_definitivo ='" + TxtContrato.Text + "'"))
                //        lblMensaje.Text += " El contrato digitado no existe<br>";

                //20161122 modif inf ope
                if (ddlPozo.SelectedValue != "") //20190306 rq013-19
                    if (VerificarExistencia("t_energia_tomada", " fecha ='" + TxtFecha.Text + "' and codigo_operador =" + ddlOperador.SelectedValue + " and codigo_remitente=" + ddlRemitente.SelectedValue + " and codigo_punto_salida =" + ddlPozo.SelectedValue + " and codigo_tramo =" + ddlTramo.SelectedValue + "and numero_contrato ='" + TxtContrato.Text + "'" + " and estado <>'E'"))
                        lblMensaje.Text += " Ya está registrada la información de la energía tomada<br>";
                if (TxtCantidad.Text == "")
                    lblMensaje.Text += " Debe digitar la cantidad Tomada<br>";
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidad.Text.Trim());
                        if (liValor < 0)
                            lblMensaje.Text += " Valor Inválido en la Cantidad Tomada<br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += " Valor Inválido en la Cantidad Tomada<br>";
                    }
                }
                //20220705
                string lsError = "N";
                if (lblMensaje.Text == "")
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_punto_reporte_cnt", " codigo_reporte = 7 and codigo_punto =" + ddlPozo.SelectedValue + " and estado ='A'", goInfo))
                        lblMensaje.Text += " El punto de salida  no tiene definida la cantidad máxima<br>";
                    else
                    {
                        if (validaCantidad())
                        {
                            lsError = "S";
                        }
                    }
                }
                if (lsError == "S" && lblMensaje.Text == "")
                {
                    Modal.Abrir(this, mdlConfirma.ID, mdlConfirmaInside.ID);
                }
                //20220705 fin

                if (lblMensaje.Text == "" && lsError == "N")//20220705
                {
                    lValorParametros[1] = TxtFecha.Text;
                    lValorParametros[2] = ddlOperador.SelectedValue;
                    lValorParametros[3] = ddlRemitente.SelectedValue;
                    lValorParametros[4] = ddlPozo.SelectedValue;
                    lValorParametros[5] = ddlTramo.SelectedValue;
                    lValorParametros[6] = TxtContrato.Text;
                    lValorParametros[7] = TxtCantidad.Text;
                    lValorParametros[8] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaTom", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Creación de la energía tomada.! " + goInfo.mensaje_error;
                        lConexion.Cerrar();
                    }
                    else
                    {
                        TxtBusFecha.Text = TxtFecha.Text;
                        ddlRemitente.SelectedValue = "0";
                        ddlTramo.SelectedValue = "0";
                        ddlPozo.SelectedValue = "0";
                        TxtContrato.Text = "";
                        TxtCantidad.Text = "";
                        //Cierra el modal de Agregar
                        Modal.Cerrar(this, registroEnergiaTomada.ID);
                        Listar();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbActualiza_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_energia_tomada", "@P_fecha", "@P_codigo_operador", "@P_codigo_remitente", "@P_codigo_punto_salida", "@P_codigo_tramo", "@P_numero_contrato", "@P_energia_tomada", "@P_estado", "@P_accion",
                "@P_modificacion"  }; // Campo Nuevo Req. 003-17  20170216
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int,
                SqlDbType.VarChar  }; // Campo Nuevo Req. 003-17  20170216
            string[] lValorParametros = { "0", "", "0", "0", "0", "0", "0", "", "", "2",
                hdfModificacion.Value  }; // Campo Nuevo Req. 003-17  20170216

            lblMensaje.Text = "";
            int liValor = 0;
            try
            {
                //200161122 modif inf ope
                if (ddlRemitente.SelectedValue == "0")
                    lblMensaje.Text += "Debe seleccionar el remitente<br>";
                //20161122 modif inf ope
                if (ddlTramo.SelectedValue == "0")
                    lblMensaje.Text += "Debe seleccionar el Tramo<br>";
                //20161122 modif inf ope
                if (ddlPozo.SelectedValue == "0" || ddlPozo.SelectedValue == "") //20190306 rq013-19
                    lblMensaje.Text += "Debe seleccionar el Punto de salida del SNT<br>";//20190306 rq013-19

                if (TxtContrato.Text.Trim() == "")
                    lblMensaje.Text += " Debe digitar el No. de Contrato<br>";
                else //20160803 eenrgia tomada
                {
                    Regex oExpresion = new Regex(@"^([A-Za-z0-9]*$)");
                    if (!oExpresion.IsMatch(TxtContrato.Text))
                        lblMensaje.Text += "El No. de Contrato solo debe tener letras o números<br>";
                }
                //20161122 modif inf ope
                if (ddlPozo.SelectedValue != "") //20190306 rq013-19
                    if (VerificarExistencia("t_energia_tomada", " fecha ='" + TxtFecha.Text + "' and codigo_operador =" + ddlOperador.SelectedValue + " and codigo_remitente=" + ddlRemitente.SelectedValue + " and codigo_punto_salida =" + ddlPozo.SelectedValue + " and codigo_tramo =" + ddlTramo.SelectedValue + "and numero_contrato ='" + TxtContrato.Text + "' and codigo_energia_tomada <>" + LblCodigoCap.Text + " and estado <>'E'"))
                        lblMensaje.Text += " Ya está registrada la información de la energía tomada<br>";

                if (TxtCantidad.Text == "")
                    lblMensaje.Text += " Debe digitar la cantidad Tomada <br>";
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidad.Text.Trim());
                        if (liValor < 0)
                            lblMensaje.Text += " Valor Inválido en la Cantidad Tomada<br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += " Valor Inválido en la Cantidad Tomada<br>";
                    }
                }
                //20220705
                string lsError = "N";
                if (lblMensaje.Text == "")
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_punto_reporte_cnt", " codigo_reporte = 7 and codigo_punto =" + ddlPozo.SelectedValue + " and estado ='A'", goInfo))
                        lblMensaje.Text += " El punto de salida no tiene definida la cantidad máxima<br>";
                    else
                    {
                        if (validaCantidad())
                        {
                            lsError = "S";
                        }
                    }
                }
                if (lsError == "S" && lblMensaje.Text == "")
                {
                    Modal.Abrir(this, mdlConfirma.ID, mdlConfirmaInside.ID);
                }
                //20220705 fin

                if (lblMensaje.Text == "" && lsError == "N")//20220705
                {
                    lValorParametros[0] = LblCodigoCap.Text;
                    lValorParametros[1] = TxtFecha.Text;
                    lValorParametros[2] = ddlOperador.SelectedValue;
                    lValorParametros[3] = ddlRemitente.SelectedValue;  //20161122 modif inf ope
                    lValorParametros[4] = ddlPozo.SelectedValue;  //20161122 modif inf ope
                    lValorParametros[5] = ddlTramo.SelectedValue;  // 20161122 modif inf ope
                    lValorParametros[6] = TxtContrato.Text;
                    lValorParametros[7] = TxtCantidad.Text;
                    lValorParametros[8] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaTom", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Actualización de la cantidad tomada.!";
                        lConexion.Cerrar();
                    }
                    else
                    {
                        manejo_bloqueo("E", LblCodigoCap.Text);
                        //Cierra el modal de Agregar
                        Modal.Cerrar(this, registroEnergiaTomada.ID);
                        Listar();
                    }
                }
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                manejo_bloqueo("E", LblCodigoCap.Text);
                lblMensaje.Text = ex.Message;
            }

            if (lblMensaje.Text == "") return;
            Toastr.Error(this, lblMensaje.Text);
            lblMensaje.Text = "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20160721 carga ptdv
        protected void ddlOperador_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlTramo.Items.Clear();
            lConexion.Abrir();
            LlenarControles1(lConexion.gObjConexion, ddlTramo, "m_tramo", " estado = 'A' ", 0, 10);
            lConexion.Cerrar();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            lblMensaje.Text = "";
            if (TxtBusCodigo.Text.Trim().Length <= 0 && TxtBusFecha.Text.Trim().Length <= 0 && ddlBusOperador.SelectedValue == "0" && ddlBusPozo.SelectedValue == "0" && ddlBusTramo.SelectedValue == "0" && ddlBusRemitente.SelectedValue == "0")
                lblMensaje.Text = "Debe Seleccionar al Menos un parámetro para realizar la Búsqueda!.";
            else
            {
                Listar();
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }
        ///// Eventos Nuevos para la Implementracion del UserControl
        /// <summary>
        /// Metodo del Link Nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNuevo(object sender, EventArgs e)
        {
            mdlRegistroEnergiaTomadaLabel.InnerText = "Agregar";
            Nuevo();
        }
        /// <summary>
        /// Metodo del Link Listar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnListar(object sender, EventArgs e)
        {
            Listar();
        }

        /// <summary>
        /// 
        /// </summary>
        protected void Cancel_OnClick(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (mdlRegistroEnergiaTomadaLabel.InnerText.Equals("Modificar") && LblCodigoCap.Text != "")
                manejo_bloqueo("E", LblCodigoCap.Text);

            //Cierra el modal de Agregar
            Modal.Cerrar(this, registroEnergiaTomada.ID);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20220705
        protected void BtnAceptarConf_Click(object sender, EventArgs e)
        {
            Modal.Cerrar(this, mdlConfirma.ID);
            string[] lsNombreParametros = { "@P_codigo_energia_tomada", "@P_fecha", "@P_codigo_operador", "@P_codigo_remitente", "@P_codigo_punto_salida", "@P_codigo_tramo", "@P_numero_contrato", "@P_energia_tomada", "@P_estado", "@P_accion", "@P_dato_atipico" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "", "0", "0", "0", "0", "0", "", "", "1", "S" };

            if (TxtCodigoCap.Text != "0")
            {
                lValorParametros[0] = TxtCodigoCap.Text;
                lValorParametros[9] = "2";
            }
            lValorParametros[1] = TxtFecha.Text;
            lValorParametros[2] = ddlOperador.SelectedValue;
            lValorParametros[3] = ddlRemitente.SelectedValue;
            lValorParametros[4] = ddlPozo.SelectedValue;
            lValorParametros[5] = ddlTramo.SelectedValue;
            lValorParametros[6] = TxtContrato.Text;
            lValorParametros[7] = TxtCantidad.Text;
            lValorParametros[8] = ddlEstado.SelectedValue;
            lConexion.Abrir();
            if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEnergiaTom", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
            {
                lblMensaje.Text = "Se presentó un Problema en la Creación de la energía tomada.! " + goInfo.mensaje_error;
                lConexion.Cerrar();
            }
            else
            {
                TxtBusFecha.Text = TxtFecha.Text;
                ddlRemitente.SelectedValue = "0";
                ddlTramo.SelectedValue = "0";
                ddlPozo.SelectedValue = "0";
                TxtContrato.Text = "";
                TxtCantidad.Text = "";
                //Cierra el modal de Agregar
                Modal.Cerrar(this, registroEnergiaTomada.ID);
                Listar();
                if (LblCodigoCap.Text != "0")
                    manejo_bloqueo("E", LblCodigoCap.Text);
            }
        }
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool validaCantidad()
        {
            bool lbValida = false;
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_punto_reporte_cnt", " codigo_reporte = 7 and codigo_punto =" + ddlPozo.SelectedValue + " and cantidad_maxima < " + TxtCantidad.Text + "  and estado ='A'");
            if (lLector.HasRows)
            {
                lLector.Read();
                lblMensajeCOnf.Text = "La cantidad de energía a registrar de " + Convert.ToInt32(TxtCantidad.Text).ToString("###,###,###,###") + " MBTU supera el valor máximo histórico parametrizado para el punto de salida " + ddlPozo.SelectedItem.ToString() + " que es de " + Convert.ToInt32(lLector["cantidad_maxima"].ToString()).ToString("###,###,###,###") + " MBTU";
                lbValida = true;
            }
            lLector.Close();
            lConexion.Cerrar();
            return lbValida;
        }

    }
}