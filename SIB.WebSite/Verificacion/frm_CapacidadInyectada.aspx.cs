﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Verificacion
{
    public partial class frm_CapacidadInyectada : Page
    {
        private InfoSessionVO goInfo = null;
        private static string lsTitulo = "Cantidad de Energía Inyectada";
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        private clConexion lConexion = null;
        private clConexion lConexion1 = null;
        private SqlDataReader lLector;
        private SqlDataReader lLector1;

        /// <summary>
        /// 
        /// </summary>
        public string HdnELiminado
        {
            get { return ViewState["HdnELiminado"] != null && !string.IsNullOrEmpty(ViewState["HdnELiminado"].ToString()) ? ViewState["HdnELiminado"].ToString() : string.Empty; }
            set { ViewState["HdnELiminado"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;

            //Establese los permisos del sistema
            //EstablecerPermisosSistema(); //20220621 ajuste
            lConexion = new clConexion(goInfo);
            lConexion1 = new clConexion(goInfo);

            //Titulo
            Master.Titulo = "Registros Operativos";

            /// Activacion de los Botones
            buttons.Inicializar(ruta: "t_capacidad_inyectada");
            buttons.CrearOnclick += btnNuevo;
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.ExportarPdfOnclick += ImgPdf_Click;

            if (IsPostBack) return;

            // Carga informacion de combos
            lConexion.Abrir();
            //20220705
            if (goInfo.cod_comisionista != "0")
            {
                LlenarControles(lConexion.gObjConexion, ddlPozo, "m_pozo poz, m_punto_operador ope", " poz.estado ='A' and poz.codigo_pozo = ope.codigo_punto and ope.tipo_punto ='E' and (ope.codigo_operador =0 or ope.codigo_operador =" + goInfo.cod_comisionista + ") and ope.estado='A'  order by poz.descripcion", 0, 1); //20161108 control inf ope //20220202 verifica operativa //20220705
                LlenarControles(lConexion.gObjConexion, ddlBusPozo, "m_pozo poz, m_punto_operador ope", " poz.estado ='A' and poz.codigo_pozo = ope.codigo_punto and ope.tipo_punto ='E' and (ope.codigo_operador =0 or ope.codigo_operador =" + goInfo.cod_comisionista + ") and ope.estado='A'  order by poz.descripcion", 0, 1); //20161108 control inf ope //20220202 verifica operativa //20220705
                LlenarControles(lConexion.gObjConexion, ddlCampo, "m_pozo poz, m_punto_operador ope", " poz.estado = 'A' and poz.ind_campo_pto ='C'  and poz.codigo_pozo = ope.codigo_punto  and ope.tipo_punto ='F' and (ope.codigo_operador =0 or ope.codigo_operador =" + goInfo.cod_comisionista + ") and ope.estado='A' order by descripcion", 0, 1); //20160809 modaldiad por tipo campo
            }
            else
            {
                LlenarControles(lConexion.gObjConexion, ddlPozo, "m_pozo", " estado ='A' order by descripcion", 0, 1); //20161108 control inf ope //20220202 verifica operativa //20220705
                LlenarControles(lConexion.gObjConexion, ddlBusPozo, "m_pozo ", " estado ='A'  order by descripcion", 0, 1);//20220202 verifica operativa //20220705
                LlenarControles(lConexion.gObjConexion, ddlCampo, "m_pozo poz", " estado = 'A' and ind_campo_pto ='C' order by descripcion", 0, 1); //20160809 modaldiad por tipo campo
            }
            //LlenarControles(lConexion.gObjConexion, ddlModalidad, "m_tipo_contrato_verifica", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles1(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador>0 and (tipo_operador in ('P','I') or codigo_operador = " + goInfo.cod_comisionista + ") order by razon_social", 0, 4);  //20180122 rq003-18 // 20180126 rq107-16
            
            LlenarControles1(lConexion.gObjConexion, ddlBusOperador, "m_operador", " estado = 'A' and codigo_operador>0 and (tipo_operador in ('P','I') or codigo_operador = " + goInfo.cod_comisionista + ") order by razon_social", 0, 4);  //20180122 rq003-18 // 20180126 rq107-16
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_no_ing_inf_ope", " codigo_reporte = 1 and codigo_operador =" + goInfo.cod_comisionista + " and estado ='A' ");
            if (lLector.HasRows)
            {
                Toastr.Warning(this, "El Operador no está obligado a ingresar la información operativa de energía inyectada.");
            }
            lLector.Close();
            lLector.Dispose();

            lConexion.Cerrar();
            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlBusOperador.SelectedValue = goInfo.cod_comisionista;
                ddlBusOperador.Enabled = false;
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
                HdnELiminado = "N";  //20160721 modif inf opertiva y transaccional
            }
            else
                HdnELiminado = "S";  //20160721 modif inf opertiva y transaccional
            DateTime ldfecha;
            ldfecha = DateTime.Now.AddDays(-1);
            TxtFecha.Text = ldfecha.Year.ToString() + "/" + ldfecha.Month.ToString() + "/" + ldfecha.Day.ToString();
            TxtFecha.Enabled = false;

            Buscar();
        }

        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "t_capacidad_inyectada");
            //20220621 ajsute
            if ((bool)permisos["UPDATE"] && (bool)permisos["DELETE"])
                dtgMaestro.Columns[15].Visible = true;
            else
            {
                if (!(bool)permisos["UPDATE"] && !(bool)permisos["DELETE"])
                    dtgMaestro.Columns[15].Visible = false;
                else
                {
                    if (!(bool)permisos["UPDATE"])
                    {
                        foreach (DataGridItem Grilla in dtgMaestro.Items)
                        {
                            var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                            lkbModificar.Visible = false;
                        }
                    }
                    if (!(bool)permisos["DELETE"])
                    {
                        foreach (DataGridItem Grilla in dtgMaestro.Items)
                        {
                            var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
                            lkbEliminar.Visible = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Nombre: Listar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            CargarDatos();
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            if (modificar != null && modificar != "")
            {
                try
                {
                    var lblMensaje = "";
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_capacidad_inyectada", " codigo_energia_inyectada= " + modificar + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            LblCodigoCap.Text = lLector["codigo_energia_inyectada"].ToString();
                            TxtCodigoCap.Text = lLector["codigo_energia_inyectada"].ToString();
                            DateTime ldFecha; //20160711 ajuste msificaciones
                            ldFecha = Convert.ToDateTime(lLector["fecha"].ToString()); //20160711 ajuste msificaciones
                            TxtFecha.Text = ldFecha.Year.ToString() + "/" + ldFecha.Month.ToString() + "/" + ldFecha.Day.ToString(); //20160711 ajuste msificaciones
                            try
                            {
                                ddlOperador.SelectedValue = lLector["codigo_Operador"].ToString();
                            }
                            catch (Exception ex)
                            {
                                lblMensaje += "El operador del registro no existe o está inactivo<br>";
                            }
                            try
                            {
                                ddlPozo.SelectedValue = lLector["codigo_Pozo"].ToString();
                            }
                            catch (Exception ex)
                            {
                                lblMensaje += "El punto del SNT del registro no existe o está inactivo<br>";
                            }
                            //try
                            //{
                            //    ddlModalidad.SelectedValue = lLector["codigo_modalidad"].ToString();
                            //}
                            //catch (Exception ex)
                            //{
                            //    lblMensaje += "El tipo de contrato del registro no existe o está inactivo<br>";
                            //}
                            TxtCantidad.Text = lLector["cantidad_inyectada"].ToString();
                            //20160809 modaldiad por tipo campo
                            try
                            {
                                ddlProd.SelectedValue = lLector["tipo_prd"].ToString();
                            }
                            catch (Exception ex)
                            {
                            }
                            //20160809 modaldiad por tipo campo
                            try
                            {
                                ddlCampo.SelectedValue = lLector["campo_prod"].ToString();
                            }
                            catch (Exception ex)
                            {
                            }
                            ddlIngresa.SelectedValue = lLector["ingresa_snt"].ToString(); //20190306 rq013-19
                            ddlEstado.SelectedValue = lLector["estado"].ToString();
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                            TxtCodigoCap.Visible = false;
                            LblCodigoCap.Visible = true;
                            TxtFecha.Enabled = false;
                            ddlOperador.Enabled = false;
                            //ddlModalidad.Enabled = false;  //20161123 modif inf ope
                            ddlEstado.Enabled = true; //20161123 modif inf ope
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Bloquea el Registro a Modificar
                        if (lblMensaje == "") //20160809 carga ptdv
                            manejo_bloqueo("A", modificar);
                    }
                    else
                    {
                        Listar();
                        lblMensaje = "No se Puede editar el Registro porque está Bloqueado. Código Capacidad" + modificar; //20180126 rq107-16

                    }

                    if (!lblMensaje.Equals(""))
                    {
                        Toastr.Warning(this, lblMensaje);
                        return;
                    }

                    //Abre el modal de Agregar
                    Modal.Abrir(this, registroCapInye.ID, registroCapInyeInside.ID);
                    mdlregistroCapInyeLabel.InnerHtml = "Modificar " + lsTitulo;
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, ex.Message);
                }
            }
        }

        /// <summary>
        /// Nombre: Buscar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
        /// Modificacion:
        /// </summary>
        private void Buscar()
        {
            lblTitulo.Text = "Consultar " + lsTitulo;
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_codigo_energia_inyectada", "@P_fecha", "@P_codigo_operador", "@P_codigo_Pozo", "@P_fecha_fin", "@P_mostrar_eliminado" }; //20160721 modif inf opertiva y trans
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar }; //20160721 modif inf opertiva y trans
            string[] lValorParametros = { "0", "", "0", "0", "", HdnELiminado }; //20160721 modif inf opertiva y trans
            DateTime ldFecha;
            var lblMensaje = string.Empty;

            try
            {
                if (TxtBusFecha.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtBusFecha.Text.Trim());
                    }
                    catch (Exception ex)
                    {
                        lblMensaje += "Valor Inválido en Fecha Inicial. <br>";
                    }
                }
                if (TxtBusFechaF.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtBusFechaF.Text.Trim());
                        if (ldFecha < Convert.ToDateTime(TxtBusFecha.Text.Trim()))
                            lblMensaje += "La Fecha final NO puede ser menor que la Fecha Inicial.!<br>";
                    }
                    catch (Exception ex)
                    {
                        lblMensaje += "Valor Inválido en Fecha Final. <br>";
                    }
                }
                if (lblMensaje == "")
                {
                    if (TxtBusCodigo.Text.Trim().Length > 0)
                        lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    // Ajuste para Cargar la Informacion del Dia Anterior al Cargar la Pantalla
                    if (TxtBusFecha.Text.Trim().Length > 0)
                        lValorParametros[1] = TxtBusFecha.Text.Trim();
                    else
                        lValorParametros[1] = ""; //2018113 ajuste 
                    if (ddlBusOperador.SelectedValue != "0")
                        if (ddlBusPozo.SelectedValue != "0")
                            lValorParametros[3] = ddlBusPozo.SelectedValue;
                    lValorParametros[2] = ddlBusOperador.SelectedValue;
                    if (ddlBusPozo.SelectedValue != "0")
                        lValorParametros[3] = ddlBusPozo.SelectedValue;

                    if (TxtBusFechaF.Text.Trim().Length > 0)
                        lValorParametros[4] = TxtBusFechaF.Text.Trim();
                    else
                    {
                        if (TxtBusFecha.Text.Trim().Length > 0)
                            lValorParametros[4] = TxtBusFecha.Text.Trim();
                        else
                            lValorParametros[4] = ""; //2018113 ajuste 
                    }
                    //20220621 ajuste
                    if (lValorParametros[0] == "0" && lValorParametros[1] == "" && lValorParametros[3] == "0")
                    {
                        lValorParametros[1] = DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd");
                        lValorParametros[4] = lValorParametros[1];
                    }
                    lConexion.Abrir();
                    dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetCapacidadIny", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgMaestro.DataBind();
                    lConexion.Cerrar();
                    EstablecerPermisosSistema(); //20220621 ajuste
                }
                if (lblMensaje == "") return;
                Toastr.Error(this, lblMensaje);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Page")) return;

                string lCodigoRegistro = "";
                string lsFecha = ""; // Cambio Req. 003-17 20170131
                hdfModificacion.Value = "N"; // Cambio Req. 003-17 20170131
                var lblMensaje = new StringBuilder();
                lCodigoRegistro = dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text; // Cambio Req. 003-17 20170131
                lsFecha = dtgMaestro.Items[e.Item.ItemIndex].Cells[1].Text; // Cambio Req. 003-17 20170131
                                                                            ///////////////////////////////////////////////////////////////////////////////////
                                                                            ///// Control Nuevo para Modif Inf Operativa Participantes Req. 003-17 20170131 ///
                                                                            ///////////////////////////////////////////////////////////////////////////////////
                bool blPuede = true;
                int liHoraAct = 0;
                string lsHoraMax = "0";
                int liValFin = 0;
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsHoraMax = lLector["hora_max_declara_opera"].ToString();
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
                liValFin = ((Convert.ToDateTime(lsHoraMax).Hour * 60) + Convert.ToDateTime(lsHoraMax).Minute);
                if (liHoraAct > liValFin)
                {
                    if (Session["tipoPerfil"].ToString() == "N")
                    {
                        lblMensaje.Append("Está intentando modificar o eliminar información fuera del horario.");
                        blPuede = false;
                    }
                    else
                        hdfModificacion.Value = "S";
                }
                if (Convert.ToDateTime(lsFecha) != DateTime.Now.Date.AddDays(-1))
                {
                    if (Session["tipoPerfil"].ToString() == "N")
                    {
                        blPuede = false;
                        lblMensaje.Append("Está intentando modificar o eliminar información que no es del día anterior a la fecha del sistema.");
                    }
                    else
                        hdfModificacion.Value = "S";
                }
                /////////////////
                if (blPuede)
                {
                    if (e.CommandName.Equals("Modificar"))
                    {
                        mdlregistroCapInyeLabel.InnerText = "Modificar";
                        Modificar(lCodigoRegistro);
                    }
                    // Evento Eliminar para los Participantes Modif Inf Operativa Participantes Req. 003-17 20170131
                    if (e.CommandName.Equals("Eliminar"))
                    {
                        string[] lsNombreParametros = { "@P_codigo_energia_inyectada", "@P_cantidad_inyectada", "@P_estado", "@P_accion", "@P_tipo_prd", "@P_campo_prod", "@P_codigo_pozo" };
                        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int };
                        string[] lValorParametros = { lCodigoRegistro, "0", "E", "3", "", "0", "0" };


                        if (string.IsNullOrEmpty(lblMensaje.ToString()))
                        {
                            lConexion.Abrir();
                            if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetCapacidadIny", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                            {
                                Toastr.Error(this, "Se presentó un problema en la eliminación del registro.! " + goInfo.mensaje_error);
                                lConexion.Cerrar();
                            }
                            else
                            {
                                Toastr.Success(this, "Registro eliminado correctamente");
                                Listar();
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    Toastr.Warning(this, lblMensaje.ToString());
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia("t_capacidad_inyectada", lswhere, goInfo);
        }
        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia1(string lsTabla, string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia(lsTabla, lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// /// // 20180126 rq107-16
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceLlave).ToString() + "-" + lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='t_capacidad_inyectada' and llave_registro='codigo_energia_inyectada=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_energia_inyectada=" + lscodigo_registro.ToString();
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_capacidad_inyectada";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_capacidad_inyectada", lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] lValorParametros = { "0", "", "0", "0", "" };
            string lsParametros = "";

            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    lsParametros += " Código energía : " + TxtBusCodigo.Text;
                }
                if (TxtBusFecha.Text.Trim().Length > 0)
                {
                    lValorParametros[1] = TxtBusFecha.Text.Trim();
                    lsParametros += " Fecha Inicial : " + TxtBusFecha.Text;
                }
                else
                {
                    lValorParametros[1] = ""; //2018113 ajuste
                    //lsParametros += " Fecha Inicial : " + TxtBusFecha.Text; //20181113 ajuste
                }
                if (ddlBusOperador.SelectedValue != "0")
                {
                    lValorParametros[2] = ddlBusOperador.SelectedValue;
                    lsParametros += " Operador: " + ddlBusOperador.SelectedItem;
                }
                if (ddlBusPozo.SelectedValue != "0")
                {
                    lValorParametros[3] = ddlBusPozo.SelectedValue;
                    lsParametros += " Punto SNT: " + ddlBusPozo.SelectedItem;
                }
                if (TxtBusFechaF.Text.Trim().Length > 0)
                {
                    lValorParametros[4] = TxtBusFechaF.Text.Trim();
                    lsParametros += " Fecha Final : " + TxtBusFechaF.Text;
                }
                else
                {
                    if (TxtBusFecha.Text.Trim().Length > 0)
                    {
                        lValorParametros[4] = TxtBusFecha.Text.Trim();
                        lsParametros += " Fecha Final : " + TxtBusFecha.Text;
                    }
                    else
                    {
                        lValorParametros[4] = ""; //20181113 ajuste
                        //lsParametros += " Fecha Final : " + TxtBusFecha.Text; 20181113 ajuste
                    }
                }
                //20220705
                if (lValorParametros[0] == "0" && lValorParametros[1] == "" && lValorParametros[3] == "0")
                {
                    lValorParametros[1] = DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd");
                    lValorParametros[4] = lValorParametros[1];
                }
                Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_getCapacidadIny&nombreParametros=@P_codigo_energia_inyectada*@P_fecha*@P_codigo_operador*@P_codigo_Pozo*@P_fecha_fin&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "*" + lValorParametros[3] + "*" + lValorParametros[4] + "&columnas=codigo_energia_inyectada*fecha*codigo_operador*nombre_operador*codigo_Pozo*desc_Pozo*codigo_modalidad*desc_modalidad*cantidad_inyectada*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado de Capacidad Inyectada&TituloParametros=" + lsParametros);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se pudo generar el informe.!" + ex.Message);
            }
        }

        /// <summary>
        /// Nombre: ImgPdf_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgPdf_Click(object sender, EventArgs e)
        {
            try
            {
                var lsCondicion = " codigo_energia_inyectada <> '0'";
                Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=t_capacidad_inyectada&procedimiento=pa_ValidarExistencia&columnas=codigo_energia_inyectada*fecha*codigo_operador*codigo_Pozo*codigo_modalidad*cantidad_inyectada*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se pudo generar el informe.!" + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_energia_inyectada", "@P_fecha", "@P_codigo_operador", "@P_codigo_Pozo", "@P_codigo_modalidad", "@P_cantidad_inyectada", "@P_estado", "@P_accion", "@P_tipo_prd", "@P_campo_prod", "@P_ingresa_snt" };//20160809 modalidad tipo de campo //20190306 rq013-19
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar }; //20160809 modalidad tipo de campo //20190306 rq013-19
            string[] lValorParametros = { "0", "", "0", "0", "0", "0", "", "1", "", "0", ddlIngresa.SelectedValue }; //20160809 modalidad tipo de campo //20190306 rq013-19
            string lblMensaje = string.Empty;
            int liValor = 0;
            try
            {
                if (TxtFecha.Text == "")
                    lblMensaje += " Debe digitar la fecha<br>";
                if (ddlOperador.SelectedValue == "0")
                    lblMensaje += "Debe seleccionar el operador<br>";
                if (ddlPozo.SelectedValue == "0")
                    lblMensaje += "Debe seleccionar el Punto SNT<br>";
                else //20220705
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_punto_operador", " tipo_punto= 'E' and codigo_punto =" + ddlPozo.SelectedValue + " and ( codigo_operador =0 or  codigo_operador= " + ddlOperador.SelectedValue + ") and estado ='A'", goInfo))
                        lblMensaje += " El punto no está asociado al operador<br>";
                }
                //if (ddlModalidad.SelectedValue == "0" || ddlModalidad.SelectedValue == "")
                //    lblMensaje += "debe seleccionar el tipo de contrato<br>";
                //20161123 modif inf ope
                //if (VerificarExistencia("fecha = '" + TxtFecha.Text + "' and codigo_operador =" + ddlOperador.SelectedValue + " and codigo_pozo =" + ddlPozo.SelectedValue + " and codigo_modalidad = " + ddlModalidad.SelectedValue + " and tipo_prd ='" + ddlProd.SelectedValue + "' and campo_prod =" + ddlCampo.SelectedValue + " and estado <>'E'"))
                if (ddlCampo.SelectedValue != "0" && ddlPozo.SelectedValue != "0")
                {
                    if (!VerificarExistencia1("m_fuente_pozo_ope", "codigo_fuente = " + ddlCampo.SelectedValue + " and codigo_pozo =" + ddlPozo.SelectedValue + " and estado ='A'"))
                        lblMensaje += " La fuente y el punto del SNT no están relacionados<br>";
                }
                if (VerificarExistencia("fecha = '" + TxtFecha.Text + "' and codigo_operador =" + ddlOperador.SelectedValue + " and codigo_pozo =" + ddlPozo.SelectedValue + " and tipo_prd ='" + ddlProd.SelectedValue + "' and campo_prod =" + ddlCampo.SelectedValue + " and estado <>'E'"))
                    lblMensaje += " La información de energía inyectada ya está registrada<br>";
                if (TxtCantidad.Text == "")
                    lblMensaje += " Debe digitar la cantidad inyectada<br>";
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidad.Text.Trim());
                        if (liValor < 0)
                            lblMensaje += " Valor Inválido en la Cantidad Inyectada<br>";
                    }
                    catch (Exception ex)
                    {
                        lblMensaje += " Valor Inválido en la Cantidad Inyectada<br>";
                    }
                }
                //20220705
                string lsError = "N";
                if (lblMensaje == "")
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_punto_reporte_cnt", " codigo_reporte = 1 and codigo_punto =" + ddlPozo.SelectedValue + " and estado ='A'", goInfo))
                        lblMensaje += " El punto no tiene definida la cantidad máxima<br>";
                    else
                    {
                        if (validaCantidad())
                        {
                            lsError = "S";
                        }
                    }
                }
                //20220705 fin
                //20160809 modalidad tipo de campo
                if (ddlProd.SelectedValue == "")
                    lblMensaje += "Debe seleccionar el tipo de producto<br>"; //20220705
                //20160809 modalidad tipo de campo
                if (ddlCampo.SelectedValue == "0")
                    lblMensaje += "Debe seleccionar el campo de producción<br>";
                //20220705 
                if (lsError == "S" && lblMensaje == "")
                {
                    Modal.Abrir(this, mdlConfirma.ID, mdlConfirmaInside.ID);
                }
                if (lblMensaje == "" && lsError == "N")//20220705 
                {
                    lValorParametros[1] = TxtFecha.Text;
                    lValorParametros[2] = ddlOperador.SelectedValue;
                    lValorParametros[3] = ddlPozo.SelectedValue;
                    //lValorParametros[4] = ddlModalidad.SelectedValue;
                    lValorParametros[5] = TxtCantidad.Text;
                    lValorParametros[6] = ddlEstado.SelectedValue;
                    lValorParametros[8] = ddlProd.SelectedValue;  //20160809 modalidad tipo de campo
                    lValorParametros[9] = ddlCampo.SelectedValue;  //20160809 modalidad tipo de campo
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetCapacidadIny", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje = "Se presentó un problema en la creación de la capacidad inyectada!.";
                        lConexion.Cerrar();
                    }
                    else
                    {
                        //Se notifica a el usuario que el registro fue realizado de manera exitosa
                        Toastr.Success(this, "Se realizo el registro de forma exitosa.!");
                        //Cierra el modal de Agregar
                        Modal.Cerrar(this, registroCapInye.ID);
                        Listar();
                    }
                }
                if (lblMensaje != "")
                {
                    Toastr.Warning(this, lblMensaje);
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void Cancel_OnClick(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            //if (mdlregistroCapInyeLabel.InnerText.Equals("Modificar") && LblCodigoCap.Text != "") //20220621 ajuste
            if (LblCodigoCap.Text != "")
                manejo_bloqueo("E", LblCodigoCap.Text);

            //Cierra el modal de Agregar
            Modal.Cerrar(this, registroCapInye.ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbActualiza_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_energia_inyectada", "@P_cantidad_inyectada", "@P_estado", "@P_accion", "@P_tipo_prd", "@P_campo_prod", "@P_codigo_pozo",  //20160809 modalidad tipo de campo  //20161123 modif inf ope
                "@P_modificacion" , "@P_ingresa_snt" }; // Campo Nuevo Req. 003-17  20170131 //20190306 rq013-19
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, //20160809 modalidad tipo de campo  //20161123 modif inf ope
                SqlDbType.VarChar,SqlDbType.VarChar  }; // Campo Nuevo Req. 003-17  20170131 //20190306 rq013-19
            string[] lValorParametros = { "0", "", "", "2", "", "0", "0", //20160809 modalidad tipo de campo //20161123 modif inf ope
                hdfModificacion.Value ,ddlIngresa.SelectedValue   }; // Campo Nuevo Req. 003-17  20170131 //20190306 rq013-19
            var lblMensaje = string.Empty;
            int liValor = 0;
            try
            {
                if (TxtCantidad.Text == "")
                    lblMensaje += " Debe digitar la cantidad inyectada al pozo <br>";
                else
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtCantidad.Text.Trim());
                        if (liValor < 0)
                            lblMensaje += " Valor Inválido en la Cantidad Inyectada<br>";
                    }
                    catch (Exception ex)
                    {
                        lblMensaje += " Valor Inválido en la Cantidad Inyectada<br>";
                    }
                }
                //20220705
                string lsError = "N";
                if (lblMensaje == "")
                {
                    if (!DelegadaBase.Servicios.ValidarExistencia("m_punto_reporte_cnt", " codigo_reporte = 1 and codigo_punto =" + ddlPozo.SelectedValue + " and estado ='A'", goInfo))
                        lblMensaje += " El punto no tiene definida la cantidad máxima<br>";
                    else
                    {
                        if (validaCantidad())
                        {
                            lsError = "S";
                        }
                    }
                }
                //20220705 fin
                //20160809 modalidad tipo de campo
                if (ddlProd.SelectedValue == "")
                    lblMensaje += "Debe seleccionar el tipo de producto<br>"; //20220705
                //20160809 modalidad tipo de campo
                if (ddlCampo.SelectedValue == "0")
                    lblMensaje += "Debe seleccionar el campo de producción<br>";
                //20161123 modif inf ope
                if (ddlPozo.SelectedValue == "0")
                    lblMensaje += "Debe seleccionar el punto del SNT<br>";
                //20161123 modif inf ope
                //if (VerificarExistencia("fecha = '" + TxtFecha.Text + "' and codigo_operador =" + ddlOperador.SelectedValue + " and codigo_pozo =" + ddlPozo.SelectedValue + " and codigo_modalidad = " + ddlModalidad.SelectedValue + " and tipo_prd ='" + ddlProd.SelectedValue + "' and campo_prod =" + ddlCampo.SelectedValue + " and codigo_energia_inyectada <>" + LblCodigoCap.Text + " and estado <>'E'"))
                if (ddlCampo.SelectedValue != "0" && ddlPozo.SelectedValue != "0")
                {
                    if (!VerificarExistencia1("m_fuente_pozo_ope", "codigo_fuente = " + ddlCampo.SelectedValue + " and codigo_pozo =" + ddlPozo.SelectedValue + " and estado ='A'"))
                        lblMensaje += " La fuente y el punto del SNT no están relacionados<br>";
                }

                if (VerificarExistencia("fecha = '" + TxtFecha.Text + "' and codigo_operador =" + ddlOperador.SelectedValue + " and codigo_pozo =" + ddlPozo.SelectedValue + " and tipo_prd ='" + ddlProd.SelectedValue + "' and campo_prod =" + ddlCampo.SelectedValue + " and codigo_energia_inyectada <>" + LblCodigoCap.Text + " and estado <>'E'"))
                    lblMensaje += " La información de energía inyectada ya está registrada<br>";
                //20220705 
                if (lsError == "S" && lblMensaje == "")
                {
                    Modal.Abrir(this, mdlConfirma.ID, mdlConfirmaInside.ID);
                }
                if (lblMensaje == "" && lsError == "N")//20220705 
                {
                    lValorParametros[0] = LblCodigoCap.Text;
                    lValorParametros[1] = TxtCantidad.Text;
                    lValorParametros[2] = ddlEstado.SelectedValue;
                    lValorParametros[4] = ddlProd.SelectedValue; //20160809 modalidad tipo de campo
                    lValorParametros[5] = ddlCampo.SelectedValue; //20160809 modalidad tipo de campo
                    lValorParametros[6] = ddlPozo.SelectedValue; //20161123 modif inf ope
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetCapacidadIny", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        Toastr.Error(this, "Se presentó un Problema en la Actualización de la cantidad inyectada.!");
                        lConexion.Cerrar();
                    }
                    else
                    {
                        manejo_bloqueo("E", LblCodigoCap.Text);
                        //Se notifica a el usuario que el registro fue actualizado de manera exitosa
                        Toastr.Success(this, "Se realizo la actualización de forma exitosa.!");
                        //Cierra el modal de Agregar
                        Modal.Cerrar(this, registroCapInye.ID);
                        Listar();
                    }
                }
                else
                {
                    //20220705
                    if (lblMensaje != "")
                        Toastr.Warning(this, lblMensaje);
                }
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                manejo_bloqueo("E", LblCodigoCap.Text);
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// BOton Buscar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            if (TxtBusCodigo.Text.Trim().Length <= 0 && TxtBusFecha.Text.Trim().Length <= 0 && ddlBusOperador.SelectedValue == "0" && ddlBusPozo.SelectedValue == "0")
                Toastr.Warning(this, "Debe seleccionar al menos un parámetro para realizar la búsqueda.!");
            else
            {
                Listar();
            }
        }

        ///// Eventos Nuevos para la Implementracion del UserControl
        /// <summary>
        /// Metodo del Link Nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNuevo(object sender, EventArgs e)
        {
            mdlregistroCapInyeLabel.InnerText = "Agregar";
            var lsHoraMax = "0";
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
            if (lLector.HasRows)
            {
                lLector.Read();
                lsHoraMax = lLector["hora_max_declara_opera"].ToString();
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
            int liHoraAct = DateTime.Now.Hour * 60 + DateTime.Now.Minute;
            int liValFin = Convert.ToDateTime(lsHoraMax).Hour * 60 + Convert.ToDateTime(lsHoraMax).Minute;
            if (liHoraAct > liValFin)
            {
                Toastr.Warning(this, "Está intentando registrar información fuera del horario!.");
                //Cierra el modal de Agregar
                Modal.Cerrar(this, registroCapInye.ID);
            }
            else
            {
                LblCodigoCap.Text = "Automático";
                ddlPozo.SelectedIndex = 0;
                TxtCantidad.Text = string.Empty;
                ddlProd.SelectedIndex = 0;
                ddlIngresa.SelectedIndex = 0;
                ddlCampo.SelectedIndex = 0;

                imbCrear.Visible = true;
                imbActualiza.Visible = false;
                TxtCodigoCap.Visible = false;
                TxtCodigoCap.Text = "0"; //20220705
                LblCodigoCap.Visible = true;
                ddlPozo.Enabled = true;
                TxtCantidad.Enabled = true;
                //ddlProd.Enabled = true;
                ddlIngresa.Enabled = true;
                ddlCampo.Enabled = true;

                // Abre el modal de Agregar
                Modal.Abrir(this, registroCapInye.ID, registroCapInyeInside.ID);
            }
            ddlEstado.SelectedValue = "A"; //20161123 modif inf ope
            ddlEstado.Enabled = false; //20161123 modif inf ope
        }

        /// <summary>
        /// Metodo del Link Listar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnListar(object sender, EventArgs e)
        {
            Listar();
        }

        /// <summary>
        /// Metodo del Link Consultar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsulta(object sender, EventArgs e)
        {
            Buscar();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20220705
        protected void BtnAceptarConf_Click(object sender, EventArgs e)
        {
            Modal.Cerrar(this, mdlConfirma.ID);
            string[] lsNombreParametros = { "@P_codigo_energia_inyectada", "@P_fecha", "@P_codigo_operador", "@P_codigo_Pozo", "@P_codigo_modalidad", "@P_cantidad_inyectada", "@P_estado", "@P_accion", "@P_tipo_prd", "@P_campo_prod", "@P_ingresa_snt", "@P_dato_atipico" };//20160809 modalidad tipo de campo //20190306 rq013-19
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar }; //20160809 modalidad tipo de campo //20190306 rq013-19
            string[] lValorParametros = { "0", "", "0", "0", "0", "0", "", "1", "", "0", ddlIngresa.SelectedValue, "S" }; //20160809 modalidad tipo de campo //20190306 rq013-19

            if (TxtCodigoCap.Text != "0")
            {
                lValorParametros[0] = TxtCodigoCap.Text;
                lValorParametros[7] = "2";
            }
            lValorParametros[1] = TxtFecha.Text;
            lValorParametros[2] = ddlOperador.SelectedValue;
            lValorParametros[3] = ddlPozo.SelectedValue;
            //lValorParametros[4] = ddlModalidad.SelectedValue;
            lValorParametros[5] = TxtCantidad.Text;
            lValorParametros[6] = ddlEstado.SelectedValue;
            lValorParametros[8] = ddlProd.SelectedValue;  //20160809 modalidad tipo de campo
            lValorParametros[9] = ddlCampo.SelectedValue;  //20160809 modalidad tipo de campo
            lConexion.Abrir();
            if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetCapacidadIny", lsNombreParametros, lTipoparametros, lValorParametros))
            {
                Toastr.Error(this, "Se presentó un error en el proceso.!");
                lConexion.Cerrar();
            }
            else
            {
                //Se notifica a el usuario que el registro fue realizado de manera exitosa
                Toastr.Success(this, "Se realizo el registro de forma exitosa.!");
                //Cierra el modal de Agregar
                Modal.Cerrar(this, registroCapInye.ID);
                Listar();
                if (LblCodigoCap.Text != "0")
                    manejo_bloqueo("E", LblCodigoCap.Text);
            }
        }
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool validaCantidad()
        {
            bool lbValida = false;
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_punto_reporte_cnt", " codigo_reporte = 1 and codigo_punto =" + ddlPozo.SelectedValue + " and cantidad_maxima < " + TxtCantidad.Text + "  and estado ='A'");
            if (lLector.HasRows)
            {
                lLector.Read();
                lblMensajeCOnf.Text = "La cantidad de energía a registrar de "+ Convert.ToInt32(TxtCantidad.Text).ToString("###,###,###,###") + " MBTU supera el valor máximo histórico parametrizado para el punto del SNT " + ddlPozo.SelectedItem.ToString()  + " que es de " + Convert.ToInt32(lLector["cantidad_maxima"].ToString()).ToString("###,###,###,###") + " MBTU";
                lbValida = true;
            }
            lLector.Close();
            lConexion.Cerrar();
            return lbValida;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20160721 carga ptdv
        protected void ddlCampo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DelegadaBase.Servicios.ValidarExistencia("m_pozo", " codigo_pozo= " + ddlCampo.SelectedValue + " and ind_importacion='S'", goInfo))
                ddlProd.SelectedValue = "I";
            else
                ddlProd.SelectedValue = "N";
        }
    }
}