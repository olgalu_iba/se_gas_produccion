﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Verificacion
{
    public partial class frm_ConsultaRegistroContratos : Page
    {
        InfoSessionVO goInfo;
        static string lsTitulo = "Consulta Registro de Contratos";
        clConexion lConexion;
        clConexion lConexion1;
        SqlDataReader lLector;
        DataSet lds = new DataSet();
        string sRutaArc = ConfigurationManager.AppSettings["rutaCarga"];

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo.ToString();
            lConexion = new clConexion(goInfo);
            lConexion1 = new clConexion(goInfo);
            //Se agrega el comportamiento del botón
            buttons.ExportarExcelOnclick += imbExcel_Click;
            buttons.FiltrarOnclick += BtnBuscar_Click;

            if (IsPostBack) return;

            //Titulo
            Master.Titulo = "Informes";

            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlComprador, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //720180126 rq107-16
            LlenarControles(lConexion.gObjConexion, ddlVendedor, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //720180126 rq107-16
            LlenarControles(lConexion.gObjConexion, ddlEstado, "m_estado_gas", " tipo_estado = 'V' order by descripcion_estado", 2, 3); //720180126 rq107-16
            LlenarControles1(lConexion.gObjConexion, ddlCausa, "", "", 0, 1); //20171130 rq026-17
            lConexion.Cerrar();
            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Excel };

            buttons.Inicializar(botones: botones);
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                }
                lDdl.Items.Add(lItem1);

            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos(string lsIndica)
        {
            var lblMensaje = new StringBuilder();
            DateTime ldFecha;
            int liValor = 0;
            string[] lsNombreParametros = { "@P_tipo_subasta", "@P_numero_contrato", "@P_fecha_contrato", "@P_mercado", "@P_codigo_producto",
                "@P_operador_compra", "@P_operador_venta", "@P_estado", "@P_extemporaneo", "@P_tipo_perfil","@P_codigo_operador",
                "@P_fecha_fin_cont", "@P_numero_contrato_fin", "@P_codigo_causa", "@P_codigo_verif"}; //20161213 rq102 conformacion de rutas //20171120 rq026-17
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                SqlDbType.Int,  SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int }; //20161213 rq102 conformacion de rutas //20171120 rq026-17
            string[] lValorParametros = { "0", "0", "", "", "0", "0", "0", ddlEstado.SelectedValue, ddlExtemporanea.SelectedValue, Session["tipoPerfil"].ToString(), goInfo.cod_comisionista, "", "0", "0", "0" };  //20161213 rq102 conformacion de rutas //20171120 rq026-17

            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato inválido en el campo fecha negociación. <br>"); //20180126 rq107 - 16
                }
            }
            //20161213 rq102 conformacion de rutas
            if (TxtFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaFin.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato inválido en el campo fecha final de negociación. <br>");
                }
            }
            //20161213 rq102 conformacion de rutas
            if (TxtFechaIni.Text.Trim().Length == 0 && TxtFechaFin.Text.Trim().Length > 0)
                lblMensaje.Append("Debe digitar la fecha inicial de negociación. <br>");
            //20161213 rq102 conformacion de rutas
            if (TxtFechaIni.Text.Trim().Length > 0 && TxtFechaFin.Text.Trim().Length > 0)
                try
                {
                    if (Convert.ToDateTime(TxtFechaFin.Text) < Convert.ToDateTime(TxtFechaIni.Text))
                        lblMensaje.Append("La fecha inicial de negociación debe ser menor o igual que la fecha final. <br>");
                }
                catch (Exception ex)
                { }

            if (TxtNoContrato.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoContrato.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato inválido en el campo no operación inicial. <br>"); // 20161207 rq102 conformación de rutas
                }

            }
            // 20161207 rq102 conformación de rutas
            if (TxtNoContratoFin.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoContratoFin.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato inválido en el campo no operación final. <br>");
                }

            }
            // 20161207 rq102 conformación de rutas
            if (TxtNoContrato.Text.Trim().Length == 0 && TxtNoContratoFin.Text.Trim().Length > 0)
            {
                lblMensaje.Append("Debe digitar el no de operación inicial antes que el final. <br>");
            }
            // 20161207 rq102 conformación de rutas
            if (TxtNoContrato.Text.Trim().Length > 0 && TxtNoContratoFin.Text.Trim().Length > 0)
            {
                try
                {
                    if (Convert.ToInt32(TxtNoContratoFin.Text) < Convert.ToInt32(TxtNoContrato.Text))
                        lblMensaje.Append("El no operación inicial debe ser menor o igual que el final. <br>");
                }
                catch (Exception ex)
                {

                }
            }

            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                Toastr.Warning(this, lblMensaje.ToString());
                return;
            }

            try
            {
                if (ddlSubasta.SelectedValue != "0")
                    lValorParametros[0] = ddlSubasta.SelectedValue;
                if (TxtNoContrato.Text.Trim().Length > 0)
                    lValorParametros[1] = TxtNoContrato.Text.Trim();
                if (TxtFechaIni.Text.Trim().Length > 0)
                    lValorParametros[2] = TxtFechaIni.Text.Trim();
                if (ddlMercado.SelectedValue != "")
                    lValorParametros[3] = ddlMercado.SelectedValue;
                if (ddlProducto.SelectedValue != "0")
                    lValorParametros[4] = ddlProducto.SelectedValue;
                if (ddlComprador.SelectedValue != "0")
                    lValorParametros[5] = ddlComprador.SelectedValue;
                if (ddlVendedor.SelectedValue != "0")
                    lValorParametros[6] = ddlVendedor.SelectedValue;
                //20161207 rq 102 conformacion de rutas
                if (TxtFechaFin.Text.Trim().Length > 0)
                    lValorParametros[11] = TxtFechaFin.Text.Trim();
                else
                    lValorParametros[11] = lValorParametros[2];
                //20161207 rq 102 conformacion de rutas
                if (TxtNoContratoFin.Text.Trim().Length > 0)
                    lValorParametros[12] = TxtNoContratoFin.Text.Trim();
                else
                    lValorParametros[12] = lValorParametros[1];
                lValorParametros[13] = ddlCausa.SelectedValue; //20171130 rq026-17
                //20171130 rq026-17
                if (TxtNoId.Text.Trim().Length > 0)
                    lValorParametros[14] = TxtNoId.Text.Trim();
                lConexion.Abrir();
                if (lsIndica == "P")
                {
                    dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(
                        lConexion.gObjConexion, "pa_GetConsRegistroContratos", lsNombreParametros, lTipoparametros,
                        lValorParametros);
                    dtgConsulta.DataBind();
                    if (dtgConsulta.Items.Count > 0)
                        dtgConsulta.Visible = true;
                    else
                    {
                        dtgConsulta.Visible = false;
                        Toastr.Info(this, "No se encontraron registros.");
                    }
                }
                else
                {
                    dtgExcel.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetConsRegistroContratos", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgExcel.DataBind();
                }

                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se pudo generar el informe.! " + ex.Message); //20180126 rq107 - 16            
            }
        }

        /// <summary>
        /// Exportacion a Excel de la Información  de la Grilla
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbExcel_Click(object sender, EventArgs e)
        {
            try
            {
                CargarDatos("E");
                if (dtgExcel.Items.Count == 0)
                    Toastr.Info(this, "No se encontraron registros.");
                else
                {
                    string lsNombreArchivo = Session["login"] + "InfExcelReg" + DateTime.Now + ".xls";
                    StringBuilder lsb = new StringBuilder();
                    StringWriter lsw = new StringWriter(lsb);
                    HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                    Page lpagina = new Page();
                    HtmlForm lform = new HtmlForm();
                    lpagina.EnableEventValidation = false;
                    lpagina.Controls.Add(lform);
                    dtgConsulta.EnableViewState = false;
                    dtgExcel.Visible = true;
                    lform.Controls.Add(dtgExcel);
                    lpagina.RenderControl(lhtw);
                    Response.Clear();

                    Response.Buffer = true;
                    Response.ContentType = "aplication/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                    Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                    Response.ContentEncoding = System.Text.Encoding.Default;

                    Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                    Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
                    Response.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + "Consulta Registro de Contratos y Contratos EXT" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                    Response.Write(lsb.ToString());
                    dtgExcel.Visible = false;
                    Response.End();
                    Response.Flush();
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "Problemas al consultar los contratos. " + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnBuscar_Click(object sender, EventArgs e)
        {
            this.dtgConsulta.CurrentPageIndex = 0; //20171130 rq026-17
            CargarDatos("P");
        }

        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgConsulta.CurrentPageIndex = e.NewPageIndex;
            CargarDatos("P");

        }

        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
            //dtgComisionista.DataBind();

            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetCausaModAdc", null, null, null);
            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);
            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["descripcion"].ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }
    }
}
