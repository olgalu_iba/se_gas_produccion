﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using Segas.Web.Elements;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Verificacion_frm_ConsultaUsuariosFinales : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Consulta Registro de Usuarios Finales";
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    string sRutaArc = ConfigurationManager.AppSettings["rutaCarga"].ToString();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);
        //Titulo
        Master.Titulo = "Informes";
        //Se agrega el comportamiento del botón
        buttons.ExportarExcelOnclick += lkbExcel_Click;
        buttons.FiltrarOnclick += BtnBuscar_Click;

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlComprador, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4); //20180126 rq107-16
            LlenarControles(lConexion.gObjConexion, ddlVendedor, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4);//20180126 rq107-16
            LlenarControles(lConexion.gObjConexion, ddlEstado, "m_estado_gas", " tipo_estado = 'V' order by descripcion_estado", 2, 3);//20180126 rq107-16 //20180126 rq107-16
            LlenarControles1(lConexion.gObjConexion, ddlCausa, "", "", 0, 1); //20171130 rq026-17
            lConexion.Cerrar();
            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar };
            buttons.Inicializar(botones: botones);

            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlComprador.SelectedValue = goInfo.cod_comisionista;
                ddlComprador.Enabled = false;
            }
            //CargarDatos();
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            if (lsTabla != "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            lDdl.Items.Add(lItem1);

        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        lblMensaje.Text = "";
        DateTime ldFecha;
        int liValor = 0;
        string[] lsNombreParametros = { "@P_tipo_subasta", "@P_numero_contrato", "@P_fecha_ini_nego", "@P_fecha_fin_nego",
                                        "@P_mercado", "@P_codigo_producto", "@P_operador_compra", "@P_operador_venta", "@P_estado" ,
                                         "@P_codigo_causa", "@P_codigo_verif"}; //20171130 rq026-17
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int,  SqlDbType.VarChar ,
                                         SqlDbType.VarChar, SqlDbType.Int };//20171130 rq026-17
        string[] lValorParametros = { "0", "0", "", "", "", "0", "0", "0", ddlEstado.SelectedValue, "0", "0" }; //20171130 rq026-17

        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Negociación. <br>"; //20180126 rq107 - 16
            }
        }
        else
            lblMensaje.Text = "Debe Ingresar la Fecha Inicial de Negociación.";
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaFin.Text);
                if (ldFecha < Convert.ToDateTime(TxtFechaIni.Text))
                    lblMensaje.Text += "La Fecha Final de Negociación NO puede ser menor que la fecha inicial. <br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Negociación. <br>"; //20180126 rq107 - 16
            }
        }
        if (TxtNoContrato.Text.Trim().Length > 0)
        {
            try
            {
                liValor = Convert.ToInt32(TxtNoContrato.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo No Operacion. <br>";
            }

        }
        if (lblMensaje.Text == "")
        {
            try
            {
                if (ddlSubasta.SelectedValue != "0")
                    lValorParametros[0] = ddlSubasta.SelectedValue;
                if (TxtNoContrato.Text.Trim().Length > 0)
                    lValorParametros[1] = TxtNoContrato.Text.Trim();
                if (TxtFechaIni.Text.Trim().Length > 0)
                    lValorParametros[2] = TxtFechaIni.Text.Trim();
                if (TxtFechaFin.Text.Trim().Length > 0)
                    lValorParametros[3] = TxtFechaFin.Text.Trim();
                else
                    lValorParametros[3] = TxtFechaIni.Text.Trim();

                if (ddlMercado.SelectedValue != "")
                    lValorParametros[4] = ddlMercado.SelectedValue;
                if (ddlProducto.SelectedValue != "0")
                    lValorParametros[5] = ddlProducto.SelectedValue;
                if (ddlComprador.SelectedValue != "0")
                    lValorParametros[6] = ddlComprador.SelectedValue;
                if (ddlVendedor.SelectedValue != "0")
                    lValorParametros[7] = ddlVendedor.SelectedValue;
                lValorParametros[9] = ddlCausa.SelectedValue; //20171130 rq026-17
                //20171130 rq026-17
                if (TxtNoId.Text.Trim().Length > 0)
                    lValorParametros[10] = TxtNoId.Text.Trim();
                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetConsRegistroUsFInal", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                lConexion.Cerrar();
                if (dtgConsulta.Items.Count > 0)
                {
                    tblGrilla.Visible = true;

                    buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
                }
                else
                {
                    tblGrilla.Visible = false;

                    buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
                    lblMensaje.Text = "No se encontraron Registros.";
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString(); //20180126 rq107 - 16
            }
        }
        if (lblMensaje.Text != "")
        {
            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnBuscar_Click(object sender, EventArgs e)
    {
        CargarDatos();
    }
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetCausaModAdc", null, null, null);
        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);
        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["descripcion"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// <summary>
    /// Metodo Exportar a Excel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lkbExcel_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        try
        {

            string lsNombreArchivo = Session["login"] + "InfExcelReg" + DateTime.Now + ".xls";
            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            lpagina.EnableEventValidation = false;
            lpagina.Controls.Add(lform);
            dtgConsulta.EnableViewState = false;
            //20171130 rq026-17 cambio de indices
            dtgConsulta.Columns[5].Visible = true;
            dtgConsulta.Columns[6].Visible = true;
            dtgConsulta.Columns[8].Visible = true;
            dtgConsulta.Columns[9].Visible = true;
            dtgConsulta.Columns[11].Visible = true; //20180126 rq107-16
            dtgConsulta.Columns[13].Visible = true; //20180126 rq107-16
            dtgConsulta.Columns[17].Visible = true; //20180126 rq107-16
            dtgConsulta.Columns[22].Visible = true; //20180126 rq107-16
            lform.Controls.Add(dtgConsulta);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;

            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + "Consulta Registro de Usuarios Finales" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write(lsb.ToString());
            //20171130 rq026-17 cambio de indices
            dtgConsulta.Columns[5].Visible = false;
            dtgConsulta.Columns[6].Visible = false;
            dtgConsulta.Columns[8].Visible = false;
            dtgConsulta.Columns[9].Visible = false;
            dtgConsulta.Columns[11].Visible = false; //20180126 rq107-16
            dtgConsulta.Columns[13].Visible = false; //20180126 rq107-16
            dtgConsulta.Columns[17].Visible = false; //20180126 rq107-16
            dtgConsulta.Columns[22].Visible = false; //20180126 rq107-16
            Response.End();
            Response.Flush();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas al Consultar los Contratos. " + ex.Message.ToString();
        }
        if (lblMensaje.Text != "")
        {
            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";
        }
    }
}