﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;
using System.Text;

namespace Verificacion
{
    public partial class frm_retiroAmpliacion : Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Retiro de capacidad de ampliación";   //20160810 modalidad por tipo campo //20190306 rq013-19
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        clConexion lConexion = null;

        SqlDataReader lLector;
        SqlDataReader lLector1;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            //Controlador util = new Controlador();
            /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

            //Establese los permisos del sistema
            EstablecerPermisosSistema();
            lConexion = new clConexion(goInfo);
            new clConexion(goInfo);
            //Titulo
            Master.Titulo = "Retiro de capacidad de ampliación";
            /// Activacion de los Botones
            buttons.Inicializar(ruta: "t_retiro_ampliacion");
            buttons.CrearOnclick += btnNuevo;
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.ExportarPdfOnclick += ImgPdf_Click;

            if (IsPostBack) return;
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles1(lConexion.gObjConexion, ddlTransportador, "m_operador", " estado = 'A' and tipo_operador ='T' order by razon_social", 0, 4);
            LlenarControles1(lConexion.gObjConexion, ddlBusTransportador, "m_operador", " estado = 'A' and tipo_operador ='T' order by razon_social", 0, 4);
            LlenarControles1(lConexion.gObjConexion, ddlRemitente, "m_operador", " estado = 'A'  order by razon_social", 0, 4);
            LlenarControles1(lConexion.gObjConexion, ddlBusRemitente, "m_operador", " estado = 'A'  order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlBusMes, "m_mes", " 1=1  order by mes", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMes, "m_mes", " 1=1  order by mes", 0, 1);
            lConexion.Cerrar();
            if (Session["tipoPerfil"].ToString() == "N")
            {
                if (Session["tipoOperador"].ToString() == "T")
                {
                    ddlBusTransportador.SelectedValue = goInfo.cod_comisionista;
                    ddlBusTransportador.Enabled = false;
                    ddlTransportador.SelectedValue = goInfo.cod_comisionista;
                    ddlTransportador.Enabled = false;

                    /// Activacion de los Botones
                    //buttons.Inicializar(ruta: "t_retiro_ampliacion");
                    //buttons.CrearOnclick += btnNuevo;
                    //buttons.FiltrarOnclick += btnConsultar_Click;
                    //buttons.ExportarExcelOnclick += ImgExcel_Click;
                    //buttons.ExportarPdfOnclick += ImgPdf_Click;
                }
                else
                {

                    ddlBusRemitente.SelectedValue = goInfo.cod_comisionista;
                    ddlBusRemitente.Enabled = false;
                    ddlRemitente.SelectedValue = goInfo.cod_comisionista;
                    ddlRemitente.Enabled = false;

                    //EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Excel, EnumBotones.Pdf };

                    //buttons.FiltrarOnclick += btnConsultar_Click;
                    //buttons.ExportarExcelOnclick += ImgExcel_Click;
                    //buttons.ExportarPdfOnclick += ImgPdf_Click;

                }
            }
            //else
            //{
            //    EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Excel, EnumBotones.Pdf };

            //    buttons.FiltrarOnclick += btnConsultar_Click;
            //    buttons.ExportarExcelOnclick += ImgExcel_Click;
            //    buttons.ExportarPdfOnclick += ImgPdf_Click;
            //}

            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            Buscar();
        }

        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "t_retiro_ampliacion");
            //hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
            //hlkListar.Enabled = (Boolean)permisos["SELECT"];
            //hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
            //if (Session["tipoPerfil"].ToString() != "N")
            //    dtgMaestro.Columns[10].Visible = (Boolean)permisos["UPDATE"]; // 20160711 ajuste modificaciones //20160810 modalidad tipo campo
            //else
            //    dtgMaestro.Columns[10].Visible = false;
            //dtgMaestro.Columns[11].Visible = false; // 20160711 ajuste modificaciones //20160810 modalidad tipo campo
            foreach (DataGridItem Grilla in dtgMaestro.Items)
            {
                var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                lkbModificar.Visible = (bool)permisos["UPDATE"]; //20170131 Modif Inf Operativa Participantes Req.003-17 //20190306 rq013-19
                var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
                lkbEliminar.Visible = (bool)permisos["DELETE"];  //20170131 Modif Inf Operativa Participantes Req.003-17 //20190306 rq013-19
            }
        }

        /// <summary>
        /// Nombre: Listar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            CargarDatos();
            lblTitulo.Text = "Consultar " + lsTitulo;
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            if (modificar != null && modificar != "")
            {
                try
                {
                    lblMensaje.Text = "";
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_retiro_ampliacion", " codigo_retiro= " + modificar + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            LblCodigoCap.Text = lLector["codigo_retiro"].ToString();
                            TxtCodigoCap.Text = lLector["codigo_retiro"].ToString();
                            ddlTransportador.SelectedValue = lLector["codigo_transportador"].ToString();
                            ddlRemitente.SelectedValue = lLector["codigo_remitente"].ToString();
                            TxtAno.Text = lLector["año_datos"].ToString();
                            ddlMes.SelectedValue = lLector["mes_datos"].ToString();
                            TxtCantidad.Text = lLector["cantidad_retiro"].ToString();
                            lLector.Close();
                            lLector.Dispose();
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                            TxtCodigoCap.Visible = false;
                            LblCodigoCap.Visible = true;
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        ddlRemitente.Enabled = false;
                        ddlMes.Enabled = false;
                        TxtAno.Enabled = false;
                        /// Bloquea el Registro a Modificar
                        if (lblMensaje.Text == "") //20160809 carga ptdv
                            manejo_bloqueo("A", modificar);
                    }
                    else
                    {
                        Listar();
                        lblMensaje.Text = "No se Puede editar el Registro porque está Bloqueado. Código retiro" + modificar.ToString(); //20190306 rq013-19

                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = ex.Message;
                }
            }
            if (lblMensaje.Text == "")
            {
                //Abre el modal de Agregar
                Modal.Abrir(this, registroAdicion.ID, registroAdicionInside.ID);


                lblTitulo.Text = "Modificar " + lsTitulo;
            }
        }

        /// <summary>
        /// Nombre: Buscar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
        /// Modificacion:
        /// </summary>
        private void Buscar()
        {
            lblTitulo.Text = "Consultar " + lsTitulo;
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_codigo_retiro", "@P_codigo_transportador", "@P_codigo_remitente", "@P_año_datos", "@P_mes_datos" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0", "0", "0" };
            DateTime ldFecha;
            lblMensaje.Text = "";

            try
            {
                if (lblMensaje.Text == "")
                {
                    if (TxtBusCodigo.Text.Trim().Length > 0)
                        lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    if (ddlBusTransportador.SelectedValue != "0")
                        lValorParametros[1] = ddlBusTransportador.Text.Trim();
                    if (ddlBusRemitente.SelectedValue != "0")
                        lValorParametros[2] = ddlBusRemitente.Text.Trim();
                    if (TxtBusAno.Text.Trim().Length > 0)
                        lValorParametros[3] = TxtBusAno.Text.Trim();
                    if (ddlBusMes.SelectedValue != "0")
                        lValorParametros[4] = ddlBusMes.SelectedValue;
                    lConexion.Abrir();
                    dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRetiroAmpliacion", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgMaestro.DataBind();
                    lConexion.Cerrar();
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }

        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            lblMensaje.Text = "";
            this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();

        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page")) return;

            string lCodigoRegistro = "";
            hdfModificacion.Value = "N"; // Cambio Req. 003-17 20170131
            lblMensaje.Text = "";
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text; // Cambio Req. 003-17 20170131
            ///////////////////////////////////////////////////////////////////////////////////
            ///// Control Nuevo para Modif Inf Operativa Participantes Req. 003-17 20170131 ///
            ///////////////////////////////////////////////////////////////////////////////////
            if (e.CommandName.Equals("Modificar"))
            {
                if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[10].Text != "T")
                    lblMensaje.Text = "Sólo el transportador puede modificar el retiro";
                if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[9].Text != "C")
                    lblMensaje.Text = "El registro no se puede modificar";
                if (lblMensaje.Text == "")
                {
                    lblRegistroAdicion.InnerText = "Modificar";
                    Modificar(lCodigoRegistro);
                }
                else
                    Toastr.Warning(this, lblMensaje.Text);
            }
            if (e.CommandName.Equals("Aprobar"))
            {
                string[] lsNombreParametros = { "@P_codigo_retiro","@P_codigo_remitente", "@P_accion" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { lCodigoRegistro, this.dtgMaestro.Items[e.Item.ItemIndex].Cells[3].Text, "3" };
                string lsMensaje = "";

                try
                {
                    if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[10].Text == "T")
                        lblMensaje.Text = "El transportador no puede aprobar el retiro";
                    if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[10].Text == "R" && this.dtgMaestro.Items[e.Item.ItemIndex].Cells[9].Text != "C")
                        lblMensaje.Text = "El registro no está pendiente de aprobación";
                    if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[10].Text == "B" && this.dtgMaestro.Items[e.Item.ItemIndex].Cells[9].Text != "P")
                        lblMensaje.Text = "El registro no está pendiente de aprobación del gestor";
                    if (lblMensaje.Text == "")
                    {
                        if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[10].Text != "R")
                            lValorParametros[2] = "4";
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_setRetiroAmpliacion", lsNombreParametros, lTipoparametros, lValorParametros);
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                                lsMensaje += lLector["error"].ToString() + "<br>";
                        }
                        else
                        {
                            Toastr.Success(this, "Registro Aprobado correctamente");
                            Listar();
                        }
                    }
                    if (lsMensaje != "")
                        Toastr.Warning(this, lsMensaje);
                }
                catch (Exception ex)
                {
                    /// Desbloquea el Registro Actualizado
                    lblMensaje.Text = ex.Message;
                }
            }
            // Evento Eliminar para los Participantes Modif Inf Operativa Participantes Req. 003-17 20170131
            if (e.CommandName.Equals("Eliminar"))
            {
                string[] lsNombreParametros = { "@P_codigo_retiro", "@P_accion" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int,  SqlDbType.Int };
                string[] lValorParametros = { lCodigoRegistro, "5" };
                string lsMensaje = "";
                try
                {
                    if (lblMensaje.Text == "")
                    {
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_setRetiroAmpliacion", lsNombreParametros, lTipoparametros, lValorParametros);
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                                lsMensaje += lLector["error"].ToString() + "<br>";
                        }
                        else
                        {
                            Toastr.Success(this, "Registro eliminado correctamente");
                            Listar();
                        }
                    }
                    if (lsMensaje != "")
                        Toastr.Warning(this, lsMensaje);
                }
                catch (Exception ex)
                {
                    /// Desbloquea el Registro Actualizado
                    lblMensaje.Text = ex.Message;
                }
            }
            if (lblMensaje.Text == "") return;
            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia("t_retiro_ampliacion", lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// 20180122 rq003-18
        ///  /// // 20180126 rq107-16
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceLlave).ToString() + "-" + lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='t_retiro_ampliacion' and llave_registro='codigo_retiro=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_retiro=" + lscodigo_registro.ToString();
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_retiro_ampliacion";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_retiro_ampliacion", lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] lValorParametros = { "0", "0", "0", "0", "0" };
            string lsParametros = "";

            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    lsParametros += " Código retiro: " + TxtBusCodigo.Text;
                }
                if (ddlBusTransportador.SelectedValue != "0")
                {
                    lValorParametros[1] = ddlBusTransportador.SelectedValue;
                    lsParametros += " Trasnportador: " + ddlBusTransportador.SelectedItem;
                }
                if (ddlBusRemitente.SelectedValue != "0")
                {
                    lValorParametros[2] = ddlBusRemitente.SelectedValue;
                    lsParametros += " Remitente: " + ddlBusRemitente.SelectedItem;
                }
                if (TxtBusAno.Text.Trim().Length > 0)
                {
                    lValorParametros[3] = TxtBusAno.Text.Trim();
                    lsParametros += " Año: " + TxtBusAno.Text;
                }
                if (ddlBusMes.SelectedValue != "0")
                {
                    lValorParametros[4] = ddlBusMes.SelectedValue;
                    lsParametros += " Operador: " + ddlBusMes.SelectedItem;
                }
                Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetRetiroAmpliacion&nombreParametros=@P_codigo_retiro*@P_codigo_transportador*@P_codigo_remitente*@P_año_datos*@P_mes_datos&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "*" + lValorParametros[3] + "*" + lValorParametros[4] + "&columnas=codigo_retiro*codigo_transportador*codigo_remitente*año_datos*mes_datos*cantidad*estado*desc_estado*&titulo_informe=Listado de Retiros de ampliación&TituloParametros=" + lsParametros);
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.!";
            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }

        }

        /// <summary>
        /// Nombre: ImgPdf_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgPdf_Click(object sender, EventArgs e)
        {
            try
            {
                string lsCondicion = " codigo_retiro <> '0' and (codigo_transportador=" + ddlBusTransportador.SelectedValue + " or codigo_remitente= " + ddlBusRemitente.SelectedValue + ")";
                Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=t_retiro_ampliacion&procedimiento=pa_ValidarExistencia&columnas=codigo_retiro*codigo_transportador*codigo_remitente*cantidad_retiro&condicion=" + lsCondicion);
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.!";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_retiro", "@P_codigo_transportador", "@P_codigo_remitente", "@P_año_datos", "@P_mes_datos", "@P_cantidad", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "", "0", "0", "0", "1", };
            string lsMensaje = "";
            try
            {
                if (ddlTransportador.SelectedValue=="0")
                    lsMensaje = " Debe seleccionar el transportadorbr>";
                if (ddlRemitente.SelectedValue == "0")
                    lsMensaje = " Debe seleccionar el remitente>";
                if (TxtAno.Text == "")
                    lsMensaje = " Debe digitar el año <br>";
                if (ddlMes.SelectedValue == "0")
                    lsMensaje = "debe seleccionar el mes<br>";
                if (TxtCantidad.Text == "")
                    lsMensaje = " Debe digitar la cantidad<br>";
                if (lsMensaje == "")
                {
                    lValorParametros[1] = ddlTransportador.SelectedValue;
                    lValorParametros[2] = ddlRemitente.SelectedValue;
                    lValorParametros[3] = TxtAno.Text;
                    lValorParametros[4] = ddlMes.SelectedValue;
                    lValorParametros[5] = TxtCantidad.Text;
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_setRetiroAmpliacion", lsNombreParametros, lTipoparametros, lValorParametros);
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                            lsMensaje += lLector["error"].ToString() + "<br>";
                    }
                    else
                    {
                        Toastr.Success(this, "Registro creado correctamente");
                        Listar();
                    }
                    if (lsMensaje != "")
                        Toastr.Warning(this, lsMensaje);
                    else
                    {
                        Modal.Cerrar(this, registroAdicion.ID);
                        Listar();
                    }

                }
                else
                    Toastr.Warning(this, lsMensaje);

            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Error: " + ex.Message.ToString());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void Cancel_OnClick(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (lblRegistroAdicion.InnerText.Equals("Modificar") && LblCodigoCap.Text != "")
                manejo_bloqueo("E", LblCodigoCap.Text);

            //Cierra el modal de Agregar
            Modal.Cerrar(this, registroAdicion.ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbActualiza_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_retiro", "@P_codigo_transportador", "@P_codigo_remitente", "@P_año_datos", "@P_mes_datos", "@P_cantidad", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "", "0", "0", "0", "2", };
            string lsMensaje = "";
            try
            {
                if (ddlTransportador.SelectedValue == "0")
                    lsMensaje = " Debe seleccionar el transportadorbr>";
                if (ddlRemitente.SelectedValue == "0")
                    lsMensaje = " Debe seleccionar el remitente>";
                if (TxtAno.Text == "")
                    lsMensaje = " Debe digitar el año <br>";
                if (ddlMes.SelectedValue == "0")
                    lsMensaje = "debe seleccionar el mes<br>";
                if (TxtCantidad.Text == "")
                    lsMensaje = " Debe digitar la cantidad<br>";
                if (lsMensaje == "")
                {
                    lValorParametros[0] = LblCodigoCap.Text;
                    lValorParametros[1] = ddlTransportador.SelectedValue;
                    lValorParametros[2] = ddlRemitente.SelectedValue;
                    lValorParametros[3] = TxtAno.Text;
                    lValorParametros[4] = ddlMes.SelectedValue;
                    lValorParametros[5] = TxtCantidad.Text;
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_setRetiroAmpliacion", lsNombreParametros, lTipoparametros, lValorParametros);
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                            lsMensaje += lLector["error"] + "<br>";
                    }
                    else
                    {
                        Toastr.Success(this, "Registro modificado correctamente");
                        Listar();
                    }
                    if (lsMensaje != "")
                        Toastr.Warning(this, lsMensaje);
                    else
                    {
                        Modal.Cerrar(this, registroAdicion.ID);
                        Listar();
                    }
                }
                else
                    Toastr.Warning(this, lsMensaje);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Error: " + ex.Message.ToString());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            Listar();
        }
        ///// Eventos Nuevos para la Implementracion del UserControl

        /// <summary>
        /// Metodo del Link Nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNuevo(object sender, EventArgs e)
        {
            lblRegistroAdicion.InnerText = "Agregar";
            LblCodigoCap.Text = "Automático";
            TxtAno.Text = string.Empty;
            TxtCantidad.Text = string.Empty;

            imbCrear.Visible = true;
            imbActualiza.Visible = false;
            TxtCodigoCap.Visible = false;
            LblCodigoCap.Visible = true;
            TxtCantidad.Enabled = true;
            TxtAno.Enabled = true;
            ddlRemitente.Enabled = true;
            ddlMes.Enabled = true;
            TxtAno.Enabled = true;

            //Abre el modal de Agregar
            Modal.Abrir(this, registroAdicion.ID, registroAdicionInside.ID);
        }

        /// <summary>
        /// Metodo del Link Listar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnListar(object sender, EventArgs e)
        {
            Listar();
        }

        /// <summary>
        /// Metodo del Link Consultar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsulta(object sender, EventArgs e)
        {
            Buscar();
        }
    }
}