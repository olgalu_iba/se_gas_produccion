﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_RegistroContratos.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Verificacion_frm_RegistroContratos" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>
            </div>
            <%--Contenido--%>
            <%--Captura--%>
            <div class="kt-portlet__body" runat="server" id="tblDatos">
                <div class="row">
                    <%--20161207 rq102--%>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Número Operación Inicial</label>
                            <asp:TextBox ID="TxtNoContrato" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                            <asp:CompareValidator ID="CvTxtNoContrato" runat="server" ControlToValidate="TxtNoContrato"
                                Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Número Operación Inicial debe Ser numérico">*</asp:CompareValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Número Operación Final</label>
                            <asp:TextBox ID="TxtNoContratoFin" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                            <asp:CompareValidator ID="CvTxtNoContratoFin" runat="server" ControlToValidate="TxtNoContratoFin"
                                Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Número Operación Final debe Ser numérico">*</asp:CompareValidator>
                            <%--20161207 rq102--%>
                            <asp:HiddenField ID="hdfNoHorasPrim" runat="server" />
                            <asp:HiddenField ID="hdfNoDiasSecun" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Subasta</label>
                            <asp:DropDownList ID="ddlSubasta" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                            <%--20161207 rq102--%>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fecha Negociación Ini.</label>
                            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fecha Negociación Fin</label>
                            <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Mercado</label>
                            <asp:DropDownList ID="ddlMercado" runat="server" CssClass="form-control selectpicker">
                                <%--20161207 rq102--%>
                                <asp:ListItem Value="" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="P" Text="Primario"></asp:ListItem>
                                <asp:ListItem Value="S" Text="Secundario"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="row" runat="server" id="tr02">
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label>Comprador</label>
                                <asp:DropDownList ID="ddlComprador" runat="server" CssClass="form-control selectpicker">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label>Vendedor</label>
                                <asp:DropDownList ID="ddlVendedor" runat="server" CssClass="form-control selectpicker">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Producto</label>
                            <asp:DropDownList ID="ddlProducto" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Estado</label>
                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <%--20171130 rq026-27--%>
                            <label>Causa Modificación</label>
                            <asp:DropDownList ID="ddlCausa" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <%--20171130 rq026-27--%>
                            <label>Id Registro</label>
                            <asp:TextBox ID="TxtNoId" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                            <asp:CompareValidator ID="CvTxtNoId" runat="server" ControlToValidate="TxtNoId"
                                Type="Integer" Operator="DataTypeCheck" ValidationGroup="detalle" ErrorMessage="El Campo Número Id Registro debe Ser numérico">*</asp:CompareValidator>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="btnConsultar" runat="server" CssClass="btn btn-success" Text="Consultar" OnClick="btnConsultar_Click" ValidationGroup="detalle" />
                            <asp:LinkButton ID="lkbExcel" runat="server" CssClass="btn btn-outline-brand btn-square" OnClick="lkbExcel_Click">Exportar Excel</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <%--Mensaje--%>
            <div class="kt-portlet__body" runat="server" id="tblMensaje">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <%--Grilla--%>
            <%--20161207 rq102--%>
            <div class="kt-portlet__body" runat="server" id="tblGrilla" visible="false">
                <div class="row">
                    <div class="table table-responsive">
                        <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                            Width="100%" CssClass="table-bordered"
                            OnEditCommand="dtgConsulta_EditCommand" AllowPaging="true" PageSize="20" OnPageIndexChanged="dtgConsulta_PageIndexChanged">
                            <Columns>
                                <%--0--%>
                                <asp:BoundColumn DataField="numero_operacion" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--1--%>
                                <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--2--%>
                                <asp:BoundColumn DataField="tipo_subasta" HeaderText="Tipo Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--3--%>
                                <asp:BoundColumn DataField="mercado" HeaderText="Mercado" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="80px"></asp:BoundColumn>
                                <%--4--%>
                                <asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--5--%>
                                <asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--6--%>
                                <asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--7--%>
                                <asp:BoundColumn DataField="lugar_entrega" HeaderText="Lugar Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20170814 rq036-17--%> <%--cambio de indieces de aqui en adelante--%>
                                <%--8--%>
                                <asp:BoundColumn DataField="nombre_centro" HeaderText="centro poblado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--9--%>
                                <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--10--%>
                                <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--11--%>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <%--12--%>
                                <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <%--13--%>
                                <asp:BoundColumn DataField="fecha_limite_registro" HeaderText="Fecha Límite Registro"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--14--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--15--%>
                                <asp:BoundColumn DataField="id" Visible="false"></asp:BoundColumn>
                                <%--16--%>
                                <asp:BoundColumn DataField="operador_compra" Visible="false"></asp:BoundColumn>
                                <%--17--%>
                                <asp:BoundColumn DataField="operador_venta" Visible="false"></asp:BoundColumn>
                                <%--18--%>
                                <asp:BoundColumn DataField="codigo_tipo_subasta" Visible="false"></asp:BoundColumn>
                                <%--19--%>
                                <asp:BoundColumn DataField="tipo_doc_comprador" Visible="false"></asp:BoundColumn>
                                <%--20--%>
                                <asp:BoundColumn DataField="tipo_doc_vendedor" Visible="false"></asp:BoundColumn>
                                <%--21--%>
                                <asp:BoundColumn DataField="documento_c" Visible="false"></asp:BoundColumn>
                                <%--22--%>
                                <asp:BoundColumn DataField="documento_v" Visible="false"></asp:BoundColumn>
                                <%--23--%>
                                <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="No. Registro"></asp:BoundColumn>
                                <%--24--%>
                                <asp:BoundColumn DataField="destino_rueda" Visible="false"></asp:BoundColumn>
                                <%--25--%>
                                <asp:BoundColumn DataField="sigla_estado" Visible="false"></asp:BoundColumn>
                                <%--26--%>
                                <asp:BoundColumn DataField="tipo_mercado" Visible="false"></asp:BoundColumn>
                                <%--27 20160603-cambio fuente--%>
                                <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente" Visible="false"></asp:BoundColumn>
                                <%--28 20160603-cambio fuente--%>
                                <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente"></asp:BoundColumn>
                                <%--29--%>
                                <asp:EditCommandColumn HeaderText="Acción" EditText="Seleccionar"></asp:EditCommandColumn>
                                <%--30--%>
                                <asp:EditCommandColumn HeaderText="Verificar" EditText="Verificar"></asp:EditCommandColumn>
                                <%--31--%>
                                <asp:BoundColumn DataField="hora_negociacion" Visible="false"></asp:BoundColumn>
                                <%--32--%>
                                <asp:BoundColumn DataField="ind_verifica" Visible="false"></asp:BoundColumn>
                                <%--33--%>
                                <asp:BoundColumn DataField="ind_modifica" Visible="false"></asp:BoundColumn>
                                <%--34--%>
                                <asp:BoundColumn DataField="fecha_maxima_correccion" Visible="false"></asp:BoundColumn>
                                <%--35--%>
                                <asp:BoundColumn DataField="fecha_maxima_modificacion" Visible="false"></asp:BoundColumn>
                                <%--36--%>
                                <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad contractual" Visible="false"></asp:BoundColumn>
                                <%--37--%>
                                <asp:BoundColumn DataField="fecha_inicial" Visible="false"></asp:BoundColumn>
                                <%--38--%>
                                <asp:BoundColumn DataField="fecha_final" Visible="false"></asp:BoundColumn>
                                <%--39--%>
                                <asp:BoundColumn DataField="codigo_modalidad" Visible="false"></asp:BoundColumn>
                                <%--40--%>
                                <asp:BoundColumn DataField="codigo_punto_entrega" Visible="false"></asp:BoundColumn>
                                <%--41--%>
                                <asp:BoundColumn DataField="cantiad_cnt" Visible="false"></asp:BoundColumn>
                                <%--42--%>
                                <asp:BoundColumn DataField="precio_cnt" Visible="false"></asp:BoundColumn>
                                <%--43--%>
                                <asp:BoundColumn DataField="codigo_ruta" Visible="false"></asp:BoundColumn>
                                <%--20170814 rq036-17--%>
                                <%--44--%>
                                <asp:BoundColumn DataField="ind_bilateral" Visible="false"></asp:BoundColumn>
                                <%--20170814 rq036-17--%>
                                <%--45--%>
                                <asp:BoundColumn DataField="conectado_snt" Visible="false"></asp:BoundColumn>
                                <%--20170814 rq036-17--%>
                                <%--46--%>
                                <asp:BoundColumn DataField="ent_boca_pozo" Visible="false"></asp:BoundColumn>
                                <%--20170814 rq036-17--%>
                                <%--47--%>
                                <asp:BoundColumn DataField="codigo_centro_pob" Visible="false"></asp:BoundColumn>
                                <%--20171130 rq026-17--%>
                                <%--48--%>
                                <asp:BoundColumn DataField="codigo_periodo_reg" Visible="false"></asp:BoundColumn>
                                <%--20171130 rq026-17--%>
                                <%--49--%>
                                <asp:BoundColumn DataField="ind_contrato_var" Visible="false"></asp:BoundColumn>
                                <%--20171130 rq026-17--%>
                                <%--50--%>
                                <asp:BoundColumn DataField="fecha_suscripcion" Visible="false"></asp:BoundColumn>
                                <%--20171130 rq026-17--%>
                                <%--51--%>
                                <asp:BoundColumn DataField="contrato_definitivo" Visible="false"></asp:BoundColumn>
                                <%--20171130 rq026-17--%>
                                <%--52--%>
                                <asp:BoundColumn DataField="desc_causa" HeaderText="Causa Modificación"></asp:BoundColumn>
                            </Columns>
                            <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgConsultaExcel" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                            Width="100%" CssClass="table-bordered"
                            Visible="false">
                            <Columns>
                                <asp:BoundColumn DataField="numero_operacion" HeaderText="No. Operación"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación"></asp:BoundColumn>
                                <asp:BoundColumn DataField="hora_negociacion" HeaderText="Hora Negociación"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tipo_subasta" HeaderText="Tipo Subasta"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Contrato"></asp:BoundColumn>
                                <asp:BoundColumn DataField="mercado" HeaderText="Mercado"></asp:BoundColumn>
                                <asp:BoundColumn DataField="producto" HeaderText="Producto"></asp:BoundColumn>
                                <asp:BoundColumn DataField="comprador" HeaderText="Nombre Comprador"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_compra" HeaderText="Código Comprador"></asp:BoundColumn>
                                <asp:BoundColumn DataField="rol_comprador" HeaderText="Rol Comprador"></asp:BoundColumn>
                                <asp:BoundColumn DataField="vendedor" HeaderText="Nombre Vendedor"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_venta" HeaderText="Código Vendedor"></asp:BoundColumn>
                                <asp:BoundColumn DataField="rol_vendedor" HeaderText="Rol Vendedor"></asp:BoundColumn>
                                <%--20170814 rq036-17--%>
                                <asp:BoundColumn DataField="codigo_punto_entrega" HeaderText="Código Entrega"></asp:BoundColumn>
                                <asp:BoundColumn DataField="lugar_entrega" HeaderText="Lugar Entrega"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial"></asp:BoundColumn>
                                <%--20161207 rq102--%>
                                <asp:BoundColumn DataField="hora_ini" HeaderText="Hora Inicial"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final"></asp:BoundColumn>
                                <%--20161207 rq102--%>
                                <asp:BoundColumn DataField="hora_fin" HeaderText="Hora Final"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad"></asp:BoundColumn>
                                <asp:BoundColumn DataField="precio" HeaderText="Precio"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_limite_registro" HeaderText="Fecha Límite Registro"></asp:BoundColumn>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="No. Registro"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad contractual"></asp:BoundColumn>
                                <%--20180126 rq107-16--%>
                                <asp:BoundColumn DataField="ind_extemporaneo" HeaderText="Actualización/Extemporánea"></asp:BoundColumn>
                                <%--20160603-cambio fuente--%>
                                <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente"></asp:BoundColumn>
                                <%--20160603-cambio fuente--%>
                                <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente"></asp:BoundColumn>
                                <%--20160711--%>
                                <%--20161207 rq102--%>
                                <asp:BoundColumn DataField="presion_punto_fin" HeaderText="Presión"></asp:BoundColumn>
                                <%--20161207 rq102--%>
                                <asp:BoundColumn DataField="sentido_flujo" HeaderText="Sentido Flujo"></asp:BoundColumn>
                                <%--20161207 rq102--%>
                                <asp:BoundColumn DataField="codigo_periodo" HeaderText="Cd Periodo"></asp:BoundColumn>
                                <%--20161207 rq102--%>
                                <asp:BoundColumn DataField="desc_periodo" HeaderText="Periodo Entrega"></asp:BoundColumn>
                                <%--20170814 rq036-17--%>
                                <asp:BoundColumn DataField="conectado_snt" HeaderText="Ind Conectado SNT"></asp:BoundColumn>
                                <%--20170814 rq036-17--%>
                                <asp:BoundColumn DataField="ent_boca_pozo" HeaderText="Ind Entrega Boca Pozo"></asp:BoundColumn>
                                <%--20170814 rq036-17--%>
                                <%--20180126 rq107-16--%>
                                <asp:BoundColumn DataField="codigo_centro_pob" HeaderText="Código Centro"></asp:BoundColumn>
                                <%--20170814 rq036-17--%>
                                <asp:BoundColumn DataField="nombre_centro" HeaderText="Centro Poblado"></asp:BoundColumn>
                                <%--20171130 rq026-17--%>
                                <asp:BoundColumn DataField="desc_causa" HeaderText="Causa Modificación"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <%--Modificacion--%>
            <div class="kt-portlet__body" runat="server" id="tblRegCnt" visible="false">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Numero Operación</label>
                            <asp:Label ID="lblOperacion" runat="server" ForeColor="Red" Font-Bold="true" CssClass="form-control"></asp:Label>-<asp:Label
                                ID="lblPunta" runat="server" ForeColor="Red" Font-Bold="true" CssClass="form-control"></asp:Label>
                            <asp:HiddenField ID="hdfPunta" runat="server" />
                            <asp:HiddenField ID="hdfIdVerif" runat="server" />
                            <asp:HiddenField ID="hdfCodTipoSub" runat="server" />
                            <asp:HiddenField ID="hdfDestinoRueda" runat="server" />
                            <asp:HiddenField ID="hdfRutaArchivo" runat="server" />
                            <asp:HiddenField ID="hdfEstadoAct" runat="server" />
                            <asp:HiddenField ID="hdfAccion" runat="server" />
                            <asp:HiddenField ID="hdfCodVerifCont" runat="server" />
                            <asp:HiddenField ID="hdfTipoMerc" runat="server" />
                            <asp:HiddenField ID="hdfOPeraC" runat="server" />
                            <asp:HiddenField ID="hdfOPeraV" runat="server" />
                            <asp:HiddenField ID="hdfCantCnt" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Subasta</label>
                            <asp:Label ID="lblSubasta" runat="server" ForeColor="Red" Font-Bold="true" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Producto</label>
                            <asp:Label ID="lblProducto" runat="server" ForeColor="Red" Font-Bold="true" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lnlNomOpera" runat="server"></asp:Label>
                            <asp:Label ID="lblNomOperador" runat="server" ForeColor="Red" Font-Bold="true" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTipoDocOpera" runat="server"></asp:Label>
                            <asp:Label ID="lblTipoDocOperador" runat="server" ForeColor="Red" Font-Bold="true" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblNoDocOPera" runat="server"></asp:Label>
                            <asp:Label ID="lblNoDocumentoOperaqdor" runat="server" ForeColor="Red" Font-Bold="true" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Número Contrato <%--20180126 rq107-16--%></label>
                            <asp:TextBox ID="TxtNumContrato" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fecha Suscripción {YYYY/MM/DD}</label>
                            <asp:TextBox ID="TxtFechaSus" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Modalidad de Contrato</label>
                            <asp:DropDownList ID="ddlModalidad" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                            <label class="kt-checkbox">
                                <asp:CheckBox ID="ChkOtrModa" runat="server" Visible="false" />
                                <span></span>
                            </label>
                            <asp:TextBox ID="TxtOtrModalidad" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <%--20170814 rq036-17--%>
                <div class="row" id="trSum" visible="false" runat="server">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Punto entrega Conectado al SNT?</label>
                            <asp:DropDownList ID="ddlConecSnt" runat="server" CssClass="form-control selectpicker" OnSelectedIndexChanged="ddlConecSnt_SelectedIndexChanged"
                                AutoPostBack="true">
                                <asp:ListItem Value="S">Si</asp:ListItem>
                                <asp:ListItem Value="N">No</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Entrega en boca de pozo?</label>
                            <asp:DropDownList ID="ddlEntBocPozo" runat="server" CssClass="form-control selectpicker" OnSelectedIndexChanged="ddlEntBocPozo_SelectedIndexChanged"
                                AutoPostBack="true">
                                <asp:ListItem Value="S">Si</asp:ListItem>
                                <asp:ListItem Value="N">No</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblCentro" runat="server">Centro problado</asp:Label>
                            <asp:DropDownList ID="ddlCentro" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblPuntoE" runat="server"></asp:Label>
                            <asp:DropDownList ID="ddlPuntoEntr" runat="server" CssClass="form-control selectpicker" OnSelectedIndexChanged="ddlPuntoEntr_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                            <label class="kt-checkbox">
                                <asp:CheckBox ID="ChkOtrPuntoE" runat="server" Visible="false" />
                                <span></span>
                            </label>
                            <asp:TextBox ID="TxtOtrPuntoE" runat="server" Visible="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblCantidad" runat="server"></asp:Label>
                            <asp:TextBox ID="TxtCantidad" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblPrecio" runat="server"></asp:Label>
                            <asp:TextBox ID="TxtPrecio" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblFechaInc" runat="server"></asp:Label>
                            <asp:TextBox ID="TxtFechaInicial" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>-
                             <asp:TextBox ID="TxtHoraInicial" runat="server" class="form-control" ValidationGroup="detalle" MaxLength="5"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RevTxtHoraInicial" ControlToValidate="TxtHoraInicial"
                                runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                                ErrorMessage="Formato Incorrecto para la hora inicial "> * </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblFechaFin" runat="server"></asp:Label>
                            <asp:TextBox ID="TxtFechaFinal" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>-
                             <asp:TextBox ID="TxtHoraFinal" runat="server" class="form-control" ValidationGroup="detalle" MaxLength="5"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RevTxtHoraFinal" ControlToValidate="TxtHoraFinal"
                                runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                                ErrorMessage="Formato Incorrecto para la hora final "> * </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Copia del Contrato</label>
                            <asp:FileUpload ID="FuCopiaCont" BackColor="#dddddd" runat="server" EnableTheming="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Periodo de entrega</label>
                            <asp:DropDownList ID="ddlPeriodo" runat="server" CssClass="form-control selectpicker" OnSelectedIndexChanged="ddlPeriodo_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Indicador contrato variable</label>
                            <asp:DropDownList ID="ddlIndVar" runat="server" CssClass="form-control selectpicker">
                                <asp:ListItem Value="N" Text="NO"></asp:ListItem>
                                <asp:ListItem Value="S" Text="SI"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <%--20170814 rq036-17--%>
                <div class="row" id="TrTrans" visible="false" runat="server">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Sentido Contratado para el flujo de Gas Natural</label>
                            <asp:DropDownList ID="ddlSentidoFlujo" runat="server" CssClass="form-control selectpicker">
                                <asp:ListItem Value="" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="NORMAL" Text="NORMAL"></asp:ListItem>
                                <asp:ListItem Value="CONTRA FLUJO" Text="CONTRA FLUJO"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Presión para el punto de terminación del servicio (psig)</label>
                            <asp:TextBox ID="TxtPresion" runat="server" class="form-control" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <%--20160601 campo o fuente MP--%>
                <div class="row" id="TrFuente" visible="false" runat="server">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fuente</label>
                            <asp:DropDownList ID="ddlFuente" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row" id="TrModifi" visible="false" runat="server">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <%--20171130 rq026-27--%>
                            <label>Motivo Modificación</label>
                            <asp:DropDownList ID="ddlMotivoModifi" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <%--20171130 rq026-27--%>
                            <label>Cesionario</label>
                            <asp:DropDownList ID="ddlCesionario" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="btnCrear" runat="server" CssClass="btn btn-success" Text="Insertar" OnClick="btnCrear_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="btnActualizar" runat="server" CssClass="btn btn-success" Text="Actualizar" OnClick="btnActualizar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="btnCrearUsu" runat="server" CssClass="btn btn-success" Text="Crear Usuario" OnClick="btnCrearUsu_Click" Visible="false" />
                            <asp:Button ID="btnActualUsu" runat="server" CssClass="btn btn-success" Text="Actualiza Usuario" OnClick="btnActualUsu_Click" Visible="false" />
                            <asp:Button ID="btnRegresar" runat="server" CssClass="btn btn-secondary" Text="Regresar" OnClick="btnRegresar_Click" />
                        </div>
                    </div>
                </div>
            </div>
            <%--Demanda--%>
            <div class="kt-portlet__body" runat="server" id="tblDemanda" visible="false">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Tipo Demanda a Atender</label>
                            <asp:DropDownList ID="dlTipoDemanda" runat="server" CssClass="form-control selectpicker" OnSelectedIndexChanged="dlTipoDemanda_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Usuario Final</label>
                            <asp:TextBox ID="TxtUsuarioFinal" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:AutoCompleteExtender runat="server" BehaviorID="AutoCompleteExUsuF" ID="AutoCompleteExtender2"
                                TargetControlID="TxtUsuarioFinal" ServicePath="~/WebService/AutoComplete.asmx"
                                ServiceMethod="GetCompletionListUsuarioFinal" MinimumPrefixLength="3" CompletionInterval="1000"
                                EnableCaching="true" CompletionSetCount="20" CompletionListCssClass="autocomplete_completionListElement"
                                CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                DelimiterCharacters=";,:">
                                <Animations>
                        <OnShow>
                            <Sequence>
                                <OpacityAction Opacity="0" />
                                <HideAction Visible="true" />
                                <ScriptAction Script="
                                    // Cache the size and setup the initial size
                                    var behavior = $find('AutoCompleteExUsuF');
                                    if (!behavior._height) {
                                        var target = behavior.get_completionList();
                                        behavior._height = target.offsetHeight - 2;
                                        target.style.height = '0px';
                                    }" />
                                <Parallel Duration=".4">
                                    <FadeIn />
                                    <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteExUsuF')._height" />
                                </Parallel>
                            </Sequence>
                        </OnShow>
                        <OnHide>
                            <Parallel Duration=".4">
                                <FadeOut />
                                <Length PropertyKey="height" StartValueScript="$find('AutoCompleteExUsuF')._height" EndValue="0" />
                            </Parallel>
                        </OnHide>
                                </Animations>
                            </ajaxToolkit:AutoCompleteExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblSector" runat="server"></asp:Label>
                            <asp:DropDownList ID="ddlSector" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Punto de Salida en SNT</label>
                            <asp:DropDownList ID="ddlPuntoSalida" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                            <asp:HiddenField ID="hdfTipoDemanda" runat="server" />
                            <asp:HiddenField ID="hdfCodDatUsu" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Mercado Relevante</label>
                            <asp:DropDownList ID="ddlMercadoRel" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblCantContra" runat="server"></asp:Label>
                            <asp:TextBox ID="TxtCantidadUsu" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" runat="server" id="TdIndica">
                        <div class="form-group">
                            <asp:Label runat="server">Equivalente Kpcd</asp:Label>
                            <asp:TextBox ID="TxtEquivaleKpcd" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label><b>LISTADO DE USUARIOS FINALES</b></label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <b>TOTAL CANTIDAD:</b><asp:Label ID="lblTotlCantidad" runat="server" ForeColor="Red"
                                Font-Bold="true"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="table table-responsive">
                        <div style="overflow: scroll; height: 450px;">
                            <asp:DataGrid ID="dtgUsuarios" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                Width="100%" CssClass="table-bordered" OnEditCommand="dtgUsuarios_EditCommand">
                                <Columns>
                                    <%--0--%>
                                    <asp:BoundColumn DataField="tipo_demanda" HeaderText="Tipo Demanda Atender" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--1--%>
                                    <asp:BoundColumn DataField="no_identificacion_usr" HeaderText="Identificación Usuario Final"
                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--2--%>
                                    <asp:BoundColumn DataField="tipo_documento" HeaderText="Tipo Identificación Usuario Final"
                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--3--%>
                                    <asp:BoundColumn DataField="nombre_usuario" HeaderText="Nombre Usuario Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--4--%>
                                    <asp:BoundColumn DataField="sector_consumo" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="80px"></asp:BoundColumn>
                                    <%--5--%>
                                    <asp:BoundColumn DataField="punto_salida" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--6--%>
                                    <asp:BoundColumn DataField="cantidad_contratada" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--Campo Nuevo Req. 009-17 Indsicadores 20170321--%>
                                    <asp:BoundColumn DataField="equivalente_kpcd" HeaderText="Equivalente Kpcd" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px" Visible="false"></asp:BoundColumn>
                                    <%--7--%>
                                    <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar"></asp:EditCommandColumn>
                                    <%--8--%>
                                    <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                                    <%--9--%>
                                    <asp:BoundColumn DataField="codigo_datos_usuarios" Visible="false"></asp:BoundColumn>
                                    <%--10--%>
                                    <asp:BoundColumn DataField="codigo_cont_datos" Visible="false"></asp:BoundColumn>
                                    <%--11--%>
                                    <asp:BoundColumn DataField="codigo_sector_consumo" Visible="false"></asp:BoundColumn>
                                    <%--12--%>
                                    <asp:BoundColumn DataField="codigo_punto_salida" Visible="false"></asp:BoundColumn>
                                    <%--13--%>
                                    <asp:BoundColumn DataField="codigo_tipo_doc" Visible="false"></asp:BoundColumn>
                                    <%--14--%>
                                    <asp:BoundColumn DataField="codigo_tipo_demanda" Visible="false"></asp:BoundColumn>
                                    <%--15--%>
                                    <asp:BoundColumn DataField="codigo_mercado_relevante" Visible="false"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </div>

                    </div>
                </div>
            </div>
            <%--Verificacion--%>
            <div class="kt-portlet__body" runat="server" id="tblVerifica" visible="false">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label><b>VERIFICACION REGISTRO</b></label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Dato</label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Sistema</label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Comprador</label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Vendedor</label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Verificación</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label></label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit1" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat1C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat1V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat1Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label></label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit2" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat2C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat2V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat2Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit3" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat3Op" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat3C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat3V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat3Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit4" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat4Op" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat4C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat4V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat4Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit5" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat5Op" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat5C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat5V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat5Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit6" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat6Op" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat6C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat6V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat6Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit7" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat7Op" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat7C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat7V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat7Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit8" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat8Op" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat8C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat8V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat8Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row" runat="server" id="TrTra1" visible="false">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit9" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="Label2" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat9C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat9V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat9Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row" runat="server" id="TrTra2" visible="false">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit10" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="Label3" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat10C" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat10V" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDat10Ver" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTit11" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="Label4" runat="server" class="form-control"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <a id="LinkC" target="_blank" runat="server" class="btn btn-outline-brand btn-square">Ver Contrato</a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <a id="LinkV" target="_blank" runat="server" class="btn btn-outline-brand btn-square">Ver Contrato</a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="Label7" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="Observación" runat="server"></asp:Label>
                            <asp:TextBox ID="TxtObservacion" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="btnSolCorr" runat="server" CssClass="btn btn-success" Text="Solicitar Correccion" OnClick="btnSolCorr_Click" />
                            <asp:Button ID="btnRegCont" runat="server" CssClass="btn btn-success" Text="Registro Contrato" OnClick="btnRegCont_Click" />
                            <asp:Button ID="btnSalir" runat="server" class="btn btn-secondary" Text="Salir" OnClick="btnSalir_Click" />

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--<table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="Table1">
        <tr>
            <td>
                <asp:ScriptManager ID="ScriptManagerMestro" runat="server" AsyncPostBackTimeout="1000">
                </asp:ScriptManager>
                <asp:UpdatePanel runat="server" ID="Panel">
                    <ContentTemplate>
                        <asp:UpdateProgress ID="PageUpdateProgress" runat="server">
                            <ProgressTemplate>
                                <div class="fuente">
                                    <img src="../Images/ajax-loader.gif" style="position: static; width: 32px;" alt="Procesando" />
                                    Procesando por Favor espere ...
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="imbConsultar" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>--%>
</asp:Content>
