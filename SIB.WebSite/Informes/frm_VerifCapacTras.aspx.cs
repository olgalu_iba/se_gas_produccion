﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;

public partial class Informes_frm_VerifCapacTras : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Verificación de Capacidad de Transporte";  //20180815 BUG230
    clConexion lConexion = null;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        try
        {
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            SqlCommand lComando = new SqlCommand();
            lComando.Connection = lConexion.gObjConexion;
            lComando.CommandTimeout = 3600;
            lComando.CommandType = CommandType.StoredProcedure;
            lComando.CommandText = "pa_getVerifCapacTra";
            lComando.Parameters.Add("@P_indicador", SqlDbType.Char).Value = "P";
            lComando.Parameters.Add("@P_tipo", SqlDbType.Char).Value = ddlTipo.SelectedValue;
            lComando.ExecuteNonQuery();
            lsqldata.SelectCommand = lComando;
            lsqldata.Fill(lds);
            dtgMaestro.DataSource = lds;
            dtgMaestro.DataBind();
            tblGrilla.Visible = true;
            imbExcel.Visible = true;
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString(); //20180815 BUG230
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfVerifCapacTras" + DateTime.Now + ".xls";
        string lstitulo_informe = "Informe De verificación de capacidad de transporte"; //20180815 BUG230
        string lsTituloParametros = "";
        try
        {
            dtgExcel.Visible = true;
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            SqlCommand lComando = new SqlCommand();
            lComando.Connection = lConexion.gObjConexion;
            lComando.CommandTimeout = 3600;
            lComando.CommandType = CommandType.StoredProcedure;
            lComando.CommandText = "pa_getVerifCapacTra";
            lComando.Parameters.Add("@P_indicador", SqlDbType.Char).Value = "E";
            lComando.Parameters.Add("@P_tipo", SqlDbType.Char).Value = ddlTipo.SelectedValue;
            lComando.ExecuteNonQuery();
            lsqldata.SelectCommand = lComando;
            lsqldata.Fill(lds);
            dtgExcel.DataSource = lds;
            dtgExcel.DataBind();
            lConexion.Cerrar();

            //Recorre la grilla para cambiar colores
            Double lfCapac = 0;
            Double lfUso = 0;
            int iCampos = 0;
            int iContador= 0;
            foreach (DataGridItem Grilla in this.dtgExcel.Items)
            {
                lfCapac = Convert.ToDouble(Grilla.Cells[3].Text);
                iCampos = Grilla.Cells.Count;
                iContador=5;
                while (iContador < iCampos)
                {
                    if (Grilla.Cells[4].Text == "Error")
                    {
                        Grilla.Cells[4].BackColor = System.Drawing.Color.Pink;
                        Grilla.Cells[4].ForeColor = System.Drawing.Color.Red;
                    }
                    lfUso = Convert.ToDouble(Grilla.Cells[iContador].Text);
                    if (lfUso > lfCapac || lfUso <0)
                    {
                        Grilla.Cells[iContador].BackColor = System.Drawing.Color.Pink;
                        Grilla.Cells[iContador].ForeColor = System.Drawing.Color.Red;
                    }
                    iContador++;
                }
            }

            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgExcel.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            lform.Controls.Add(dtgExcel);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
            dtgExcel.Visible = false;
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Excel.!" + ex.Message.ToString(); //20180815 BUG230
        }
    }
}