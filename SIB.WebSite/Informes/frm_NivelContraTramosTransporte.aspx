﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_NivelContraTramosTransporte.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Informes.frm_NivelContraTramosTransporte" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body" runat="server">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Inicial" AssociatedControlID="TxtFechaIni" runat="server" />
                            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%" ValidationGroup="detalle" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Final" AssociatedControlID="TxtFechaFin" runat="server" />
                            <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%" ValidationGroup="detalle" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fuente" AssociatedControlID="ddlFuente" runat="server" />
                            <asp:DropDownList ID="ddlFuente" CssClass="form-control selectpicker" data-live-search="true" runat="server">
                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="S" Text="Contratos Sistema"></asp:ListItem>
                                <asp:ListItem Value="E" Text="Contratos EXT"></asp:ListItem>
                                <asp:ListItem Value="M" Text="Contratos Mayorista"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tipo Subasta" AssociatedControlID="ddlTippSubasta" runat="server" />
                            <asp:DropDownList ID="ddlTippSubasta" CssClass="form-control selectpicker" data-live-search="true" runat="server" OnSelectedIndexChanged="ddlTippSubasta_SelectedIndexChanged" AutoPostBack="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tipo Rueda" AssociatedControlID="ddlTipoRueda" runat="server" />
                            <asp:DropDownList ID="ddlTipoRueda" CssClass="form-control selectpicker" data-live-search="true" runat="server">
                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Modalidad" AssociatedControlID="ddlModalidad" runat="server" />
                            <asp:DropDownList ID="ddlModalidad" CssClass="form-control selectpicker" data-live-search="true" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tipo Mercado" AssociatedControlID="ddlTipoMercado" runat="server" />
                            <asp:DropDownList ID="ddlTipoMercado" CssClass="form-control selectpicker" data-live-search="true" runat="server">
                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="P" Text="Primario"></asp:ListItem>
                                <asp:ListItem Value="S" Text="Secundario"></asp:ListItem>
                                <asp:ListItem Value="O" Text="Otras Transacciones del Merecado Mayorista"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tramo" AssociatedControlID="ddlTramo" runat="server" />
                            <asp:DropDownList ID="ddlTramo" CssClass="form-control selectpicker" data-live-search="true" runat="server" />
                        </div>
                    </div>
                </div>
                <%--Grilla--%>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" Visible="False">
                                <Columns>
                                    <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="tipo_subasta" HeaderText="Tipo Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="tipo_rueda" HeaderText="Tipo Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="tipo_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="cantidad" HeaderText="Capacidad Negociada" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="capacidad_disp_primaria" HeaderText="Capacidad Disponible Primaria (CDP)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="capacidad_med_plazo" HeaderText="Capacidad MAx. Mediano Plazo (CMMP)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="precio_promedio" HeaderText="Precio Promedio" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}"></asp:BoundColumn>
                                </Columns>
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
