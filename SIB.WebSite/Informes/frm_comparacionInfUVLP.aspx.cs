﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;
using System.Text;
using Segas.Web.Elements;

public partial class Informes_frm_comparacionInfUVLP : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Consulta de comparación subasta UVLP";
    clConexion lConexion = null;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        //Se agrega el comportamiento del botón
        buttons.ExportarExcelOnclick += ImgExcel_Click;
        buttons.FiltrarOnclick += imbConsultar_Click;

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, DdlComprador, "m_operador", " estado = 'A' And codigo_operador != 0 order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, DdlVendedor, "m_operador", " estado = 'A' And codigo_operador != 0 order by razon_social", 0, 4);
            lConexion.Cerrar();
                 //Botones
            EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Excel };
            buttons.Inicializar(botones: botones);
        }
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        DateTime ldFecha = DateTime.Now;
        if (TxtAño.Text == "")
            lblMensaje.Text = "Debe seleccionar el año de la consulta";
        if (lblMensaje.Text == "")
        {
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetCompracionUVLP";
                lComando.Parameters.Add("@P_año_rueda", SqlDbType.Int).Value = TxtAño.Text;
                lComando.Parameters.Add("@P_destino_rueda", SqlDbType.VarChar).Value = ddlProducto.SelectedValue;
                lComando.Parameters.Add("@P_operador_compra", SqlDbType.Int).Value = DdlComprador.SelectedValue;
                lComando.Parameters.Add("@P_operador_venta", SqlDbType.Int).Value = DdlVendedor.SelectedValue;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                if (ddlProducto.SelectedValue == "G")
                {
                    tblSuministro.Visible = true;
                    tblTransporte.Visible = false;
                    llenarSum();
                }
                else
                {
                    tblSuministro.Visible = false;
                    tblTransporte.Visible = true;
                    llenarTra();
                }
      
                buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);

                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pudo Generar el Informe.!" + ex.Message.ToString());

            }
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, EventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfComparacionUVLP" + DateTime.Now + ".xls";
        string lstitulo_informe = "";
        string lsTituloParametros = "";
        if (TxtAño.Text == "")
            lblMensaje.Text = "Debe digitar el año";
        if (lblMensaje.Text == "")
            try
            {
                if (ddlProducto.SelectedValue == "G")
                {
                    lstitulo_informe = "Comparación de subasta UVLP - Suministro";
                    lsTituloParametros += " Año: " + TxtAño.Text.Trim();
                    if (DdlComprador.SelectedValue != "0")
                        lsTituloParametros += "  - Comprador: " + DdlComprador.SelectedItem.ToString();
                    if (DdlVendedor.SelectedValue != "0")
                        lsTituloParametros += "  - Vendedor: " + DdlVendedor.SelectedItem.ToString();

                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    SqlCommand lComando = new SqlCommand();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandTimeout = 3600;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_GetCompracionUVLP";
                    lComando.Parameters.Add("@P_año_rueda", SqlDbType.VarChar).Value = TxtAño.Text.Trim();
                    lComando.Parameters.Add("@P_destino_rueda", SqlDbType.VarChar).Value = ddlProducto.SelectedValue;
                    lComando.Parameters.Add("@P_operador_compra", SqlDbType.Int).Value = DdlComprador.SelectedValue;
                    lComando.Parameters.Add("@P_operador_venta", SqlDbType.Int).Value = DdlVendedor.SelectedValue;
                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgExcelSum.DataSource = lds;
                    dtgExcelSum.DataBind();
                    dtgExcelSum.Visible = true;
                    lConexion.Cerrar();

                    decimal ldCapacidad = 0;
                    StringBuilder lsb = new StringBuilder();
                    ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                    StringWriter lsw = new StringWriter(lsb);
                    HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                    Page lpagina = new Page();
                    HtmlForm lform = new HtmlForm();
                    lpagina.EnableEventValidation = false;
                    lpagina.DesignerInitialize();
                    lpagina.Controls.Add(lform);
                    dtgExcelSum.EnableViewState = false;
                    lform.Controls.Add(dtgExcelSum);

                    lpagina.RenderControl(lhtw);
                    Response.Clear();

                    Response.Buffer = true;
                    Response.ContentType = "aplication/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                    Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                    Response.ContentEncoding = System.Text.Encoding.Default;
                    Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                    Response.Charset = "UTF-8";
                    Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
                    Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                    Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                    Response.Write("<table border='1'><tr><th colspan='8' align='center' ><font face=Arial size=2> REGISTRO DE CONTRATOS</font></th><th colspan='13' align='center'><font face=Arial size=2> SUBASTA</font></th></tr></table>");
                    Response.Write(lsb.ToString());
                    Response.End();
                    lds.Dispose();
                    lsqldata.Dispose();
                    lConexion.CerrarInforme();
                    dtgExcelSum.Visible = false;
                }
                else
                {
                    lstitulo_informe = "Comparación de subasta UVLP - Transporte";
                    lsTituloParametros += " Año: " + TxtAño.Text.Trim();
                    if (DdlComprador.SelectedValue != "0")
                        lsTituloParametros += "  - Comprador: " + DdlComprador.SelectedItem.ToString();
                    if (DdlVendedor.SelectedValue != "0")
                        lsTituloParametros += "  - Vendedor: " + DdlVendedor.SelectedItem.ToString();

                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    SqlCommand lComando = new SqlCommand();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandTimeout = 3600;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_GetCompracionUVLP";
                    lComando.Parameters.Add("@P_año_rueda", SqlDbType.VarChar).Value = TxtAño.Text.Trim();
                    lComando.Parameters.Add("@P_destino_rueda", SqlDbType.VarChar).Value = ddlProducto.SelectedValue;
                    lComando.Parameters.Add("@P_operador_compra", SqlDbType.Int).Value = DdlComprador.SelectedValue;
                    lComando.Parameters.Add("@P_operador_venta", SqlDbType.Int).Value = DdlVendedor.SelectedValue;
                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgExcelTra.DataSource = lds;
                    dtgExcelTra.DataBind();
                    dtgExcelTra.Visible = true;
                    lConexion.Cerrar();

                    decimal ldCapacidad = 0;
                    StringBuilder lsb = new StringBuilder();
                    ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                    StringWriter lsw = new StringWriter(lsb);
                    HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                    Page lpagina = new Page();
                    HtmlForm lform = new HtmlForm();
                    lpagina.EnableEventValidation = false;
                    lpagina.DesignerInitialize();
                    lpagina.Controls.Add(lform);
                    dtgExcelTra.EnableViewState = false;
                    lform.Controls.Add(dtgExcelTra);
                    lpagina.RenderControl(lhtw);
                    Response.Clear();

                    Response.Buffer = true;
                    Response.ContentType = "aplication/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                    Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                    Response.ContentEncoding = System.Text.Encoding.Default;
                    Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                    Response.Charset = "UTF-8";
                    Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
                    Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                    Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                    Response.Write("<table border='1'><tr><th colspan='10' align='center' ><font face=Arial size=2> REGISTRO DE CONTRATOS</font></th><th colspan='15' align='center'><font face=Arial size=2> SUBASTA</font></th></tr></table>");
                    Response.Write(lsb.ToString());
                    Response.End();
                    lds.Dispose();
                    lsqldata.Dispose();
                    lConexion.CerrarInforme();
                    dtgExcelTra.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pudo Generar el Excel.!" + ex.Message.ToString());
     
            }
    }
    protected void llenarSum()
    {
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        SqlCommand lComando = new SqlCommand();
        lComando.Connection = lConexion.gObjConexion;
        lComando.Parameters.Clear();
        lComando.CommandTimeout = 3600;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_GetCompracionUVLP";
        lComando.Parameters.Add("@P_año_rueda", SqlDbType.Int).Value = TxtAño.Text;
        lComando.Parameters.Add("@P_destino_rueda", SqlDbType.VarChar).Value = "G";
        lComando.Parameters.Add("@P_operador_compra", SqlDbType.Int).Value = DdlComprador.SelectedValue;
        lComando.Parameters.Add("@P_operador_venta", SqlDbType.Int).Value = DdlVendedor.SelectedValue;
        SqlDataReader lLector;
        lLector = lComando.ExecuteReader();
        if (lLector.HasRows)
        {
            int liConta = 2;
            int liAux = 0;
            while (lLector.Read())
            {
                HtmlTableRow fila = new HtmlTableRow();
                HtmlTableCell celda1 = new HtmlTableCell();
                HtmlTableCell celda2 = new HtmlTableCell();
                HtmlTableCell celda3 = new HtmlTableCell();
                HtmlTableCell celda4 = new HtmlTableCell();
                HtmlTableCell celda5 = new HtmlTableCell();
                HtmlTableCell celda6 = new HtmlTableCell();
                HtmlTableCell celda7 = new HtmlTableCell();
                HtmlTableCell celda8 = new HtmlTableCell();
                HtmlTableCell celda9 = new HtmlTableCell();
                HtmlTableCell celda10 = new HtmlTableCell();
                HtmlTableCell celda11 = new HtmlTableCell();
                HtmlTableCell celda12 = new HtmlTableCell();
                HtmlTableCell celda13 = new HtmlTableCell();
                HtmlTableCell celda14 = new HtmlTableCell();
                HtmlTableCell celda15 = new HtmlTableCell();
                HtmlTableCell celda16 = new HtmlTableCell();
                HtmlTableCell celda17 = new HtmlTableCell();
                HtmlTableCell celda18 = new HtmlTableCell();
                HtmlTableCell celda19 = new HtmlTableCell();
                HtmlTableCell celda20 = new HtmlTableCell();
                HtmlTableCell celda21 = new HtmlTableCell();

                celda8.Align = "Right";
                celda18.Align = "Right";
                celda19.Align = "Right";
                celda20.Align = "Right";
                celda21.Align = "Right";
                celda2.Align = "Center";
                celda4.Align = "Center";
                celda6.Align = "Center";
                celda10.Align = "Center";
                celda12.Align = "Center";
                celda14.Align = "Center";
                celda16.Align = "Center";
                celda1.BorderColor = "#C1DAD7";
                celda2.BorderColor = "#C1DAD7";
                celda3.BorderColor = "#C1DAD7";
                celda4.BorderColor = "#C1DAD7";
                celda5.BorderColor = "#C1DAD7";
                celda6.BorderColor = "#C1DAD7";
                celda7.BorderColor = "#C1DAD7";
                celda8.BorderColor = "#C1DAD7";
                celda9.BorderColor = "#C1DAD7";
                celda10.BorderColor = "#C1DAD7";
                celda11.BorderColor = "#C1DAD7";
                celda12.BorderColor = "#C1DAD7";
                celda13.BorderColor = "#C1DAD7";
                celda14.BorderColor = "#C1DAD7";
                celda15.BorderColor = "#C1DAD7";
                celda16.BorderColor = "#C1DAD7";
                celda17.BorderColor = "#C1DAD7";
                celda18.BorderColor = "#C1DAD7";
                celda19.BorderColor = "#C1DAD7";
                celda20.BorderColor = "#C1DAD7";
                celda21.BorderColor = "#C1DAD7";
                fila.Cells.Add(celda1);
                fila.Cells.Add(celda2);
                fila.Cells.Add(celda3);
                fila.Cells.Add(celda4);
                fila.Cells.Add(celda5);
                fila.Cells.Add(celda6);
                fila.Cells.Add(celda7);
                fila.Cells.Add(celda8);
                fila.Cells.Add(celda9);
                fila.Cells.Add(celda10);
                fila.Cells.Add(celda11);
                fila.Cells.Add(celda12);
                fila.Cells.Add(celda13);
                fila.Cells.Add(celda14);
                fila.Cells.Add(celda15);
                fila.Cells.Add(celda16);
                fila.Cells.Add(celda17);
                fila.Cells.Add(celda18);
                fila.Cells.Add(celda19);
                fila.Cells.Add(celda20);
                fila.Cells.Add(celda21);
                tblSuministro.Rows.Add(fila);
                tblSuministro.Rows[liConta].Cells[0].InnerHtml = lLector["numero_contrato"].ToString();
                tblSuministro.Rows[liConta].Cells[1].InnerHtml = lLector["operador_compra"].ToString();
                tblSuministro.Rows[liConta].Cells[2].InnerHtml = lLector["nombre_compra"].ToString();
                tblSuministro.Rows[liConta].Cells[3].InnerHtml = lLector["operador_venta"].ToString();
                tblSuministro.Rows[liConta].Cells[4].InnerHtml = lLector["nombre_venta"].ToString();
                tblSuministro.Rows[liConta].Cells[5].InnerHtml = lLector["codigo_punto_salida"].ToString();
                tblSuministro.Rows[liConta].Cells[6].InnerHtml = lLector["desc_punto"].ToString();
                tblSuministro.Rows[liConta].Cells[7].InnerHtml = lLector["cantidad"].ToString();
                tblSuministro.Rows[liConta].Cells[8].InnerHtml = lLector["numero_contrato"].ToString();
                tblSuministro.Rows[liConta].Cells[9].InnerHtml = lLector["operador_compra"].ToString();
                tblSuministro.Rows[liConta].Cells[10].InnerHtml = lLector["nombre_compra"].ToString();
                tblSuministro.Rows[liConta].Cells[11].InnerHtml = lLector["operador_venta"].ToString();
                tblSuministro.Rows[liConta].Cells[12].InnerHtml = lLector["nombre_venta"].ToString();
                tblSuministro.Rows[liConta].Cells[13].InnerHtml = lLector["codigo_fuente"].ToString();
                tblSuministro.Rows[liConta].Cells[14].InnerHtml = lLector["desc_fuente"].ToString();
                tblSuministro.Rows[liConta].Cells[15].InnerHtml = lLector["codigo_punto_salida"].ToString();
                tblSuministro.Rows[liConta].Cells[16].InnerHtml = lLector["desc_punto"].ToString();
                tblSuministro.Rows[liConta].Cells[17].InnerHtml = lLector["cantidad_cont"].ToString();
                tblSuministro.Rows[liConta].Cells[18].InnerHtml = lLector["equiv_cont"].ToString();
                tblSuministro.Rows[liConta].Cells[19].InnerHtml = lLector["cantidad_dem"].ToString();
                tblSuministro.Rows[liConta].Cells[20].InnerHtml = lLector["equiv_dem"].ToString();
                liConta++;
                liAux--;
            }
        }
    }
    protected void llenarTra()
    {
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        SqlCommand lComando = new SqlCommand();
        lComando.Connection = lConexion.gObjConexion;
        lComando.Parameters.Clear();
        lComando.CommandTimeout = 3600;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_GetCompracionUVLP";
        lComando.Parameters.Add("@P_año_rueda", SqlDbType.Int).Value = TxtAño.Text;
        lComando.Parameters.Add("@P_destino_rueda", SqlDbType.VarChar).Value = "T";
        lComando.Parameters.Add("@P_operador_compra", SqlDbType.Int).Value = DdlComprador.SelectedValue;
        lComando.Parameters.Add("@P_operador_venta", SqlDbType.Int).Value = DdlVendedor.SelectedValue;
        SqlDataReader lLector;
        lLector = lComando.ExecuteReader();
        if (lLector.HasRows)
        {
            int liConta = 2;
            int liAux = 0;
            while (lLector.Read())
            {
                HtmlTableRow fila = new HtmlTableRow();
                HtmlTableCell celda1 = new HtmlTableCell();
                HtmlTableCell celda2 = new HtmlTableCell();
                HtmlTableCell celda3 = new HtmlTableCell();
                HtmlTableCell celda4 = new HtmlTableCell();
                HtmlTableCell celda5 = new HtmlTableCell();
                HtmlTableCell celda6 = new HtmlTableCell();
                HtmlTableCell celda7 = new HtmlTableCell();
                HtmlTableCell celda8 = new HtmlTableCell();
                HtmlTableCell celda9 = new HtmlTableCell();
                HtmlTableCell celda10 = new HtmlTableCell();
                HtmlTableCell celda11 = new HtmlTableCell();
                HtmlTableCell celda12 = new HtmlTableCell();
                HtmlTableCell celda13 = new HtmlTableCell();
                HtmlTableCell celda14 = new HtmlTableCell();
                HtmlTableCell celda15 = new HtmlTableCell();
                HtmlTableCell celda16 = new HtmlTableCell();
                HtmlTableCell celda17 = new HtmlTableCell();
                HtmlTableCell celda18 = new HtmlTableCell();
                HtmlTableCell celda19 = new HtmlTableCell();
                HtmlTableCell celda20 = new HtmlTableCell();
                HtmlTableCell celda21 = new HtmlTableCell();
                HtmlTableCell celda22 = new HtmlTableCell();
                HtmlTableCell celda23 = new HtmlTableCell();
                HtmlTableCell celda24 = new HtmlTableCell();
                HtmlTableCell celda25 = new HtmlTableCell();

                celda10.Align = "Right";
                celda20.Align = "Right";
                celda21.Align = "Right";
                celda22.Align = "Right";
                celda23.Align = "Right";
                celda24.Align = "Right";
                celda25.Align = "Right";
                celda2.Align = "Center";
                celda4.Align = "Center";
                celda6.Align = "Center";
                celda8.Align = "Center";
                celda12.Align = "Center";
                celda14.Align = "Center";
                celda16.Align = "Center";
                celda18.Align = "Center";
                celda1.BorderColor = "#C1DAD7";
                celda2.BorderColor = "#C1DAD7";
                celda3.BorderColor = "#C1DAD7";
                celda4.BorderColor = "#C1DAD7";
                celda5.BorderColor = "#C1DAD7";
                celda6.BorderColor = "#C1DAD7";
                celda7.BorderColor = "#C1DAD7";
                celda8.BorderColor = "#C1DAD7";
                celda9.BorderColor = "#C1DAD7";
                celda10.BorderColor = "#C1DAD7";
                celda11.BorderColor = "#C1DAD7";
                celda12.BorderColor = "#C1DAD7";
                celda13.BorderColor = "#C1DAD7";
                celda14.BorderColor = "#C1DAD7";
                celda15.BorderColor = "#C1DAD7";
                celda16.BorderColor = "#C1DAD7";
                celda17.BorderColor = "#C1DAD7";
                celda18.BorderColor = "#C1DAD7";
                celda19.BorderColor = "#C1DAD7";
                celda20.BorderColor = "#C1DAD7";
                celda21.BorderColor = "#C1DAD7";
                celda22.BorderColor = "#C1DAD7";
                celda23.BorderColor = "#C1DAD7";
                celda24.BorderColor = "#C1DAD7";
                celda25.BorderColor = "#C1DAD7";
                fila.Cells.Add(celda1);
                fila.Cells.Add(celda2);
                fila.Cells.Add(celda3);
                fila.Cells.Add(celda4);
                fila.Cells.Add(celda5);
                fila.Cells.Add(celda6);
                fila.Cells.Add(celda7);
                fila.Cells.Add(celda8);
                fila.Cells.Add(celda9);
                fila.Cells.Add(celda10);
                fila.Cells.Add(celda11);
                fila.Cells.Add(celda12);
                fila.Cells.Add(celda13);
                fila.Cells.Add(celda14);
                fila.Cells.Add(celda15);
                fila.Cells.Add(celda16);
                fila.Cells.Add(celda17);
                fila.Cells.Add(celda18);
                fila.Cells.Add(celda19);
                fila.Cells.Add(celda20);
                fila.Cells.Add(celda21);
                fila.Cells.Add(celda22);
                fila.Cells.Add(celda23);
                fila.Cells.Add(celda24);
                fila.Cells.Add(celda25);
                tblTransporte.Rows.Add(fila);
                tblTransporte.Rows[liConta].Cells[0].InnerHtml = lLector["numero_contrato"].ToString();
                tblTransporte.Rows[liConta].Cells[1].InnerHtml = lLector["operador_compra"].ToString();
                tblTransporte.Rows[liConta].Cells[2].InnerHtml = lLector["nombre_compra"].ToString();
                tblTransporte.Rows[liConta].Cells[3].InnerHtml = lLector["operador_venta"].ToString();
                tblTransporte.Rows[liConta].Cells[4].InnerHtml = lLector["nombre_venta"].ToString();
                tblTransporte.Rows[liConta].Cells[5].InnerHtml = lLector["cod_ruta_cont"].ToString();
                tblTransporte.Rows[liConta].Cells[6].InnerHtml = lLector["desc_ruta_cont"].ToString();
                tblTransporte.Rows[liConta].Cells[7].InnerHtml = lLector["cod_tramo_cont"].ToString();
                tblTransporte.Rows[liConta].Cells[8].InnerHtml = lLector["desc_tramo_cont"].ToString();
                tblTransporte.Rows[liConta].Cells[9].InnerHtml = lLector["cantidad_cont"].ToString();
                tblTransporte.Rows[liConta].Cells[10].InnerHtml = lLector["numero_contrato"].ToString();
                tblTransporte.Rows[liConta].Cells[11].InnerHtml = lLector["operador_compra"].ToString();
                tblTransporte.Rows[liConta].Cells[12].InnerHtml = lLector["nombre_compra"].ToString();
                tblTransporte.Rows[liConta].Cells[13].InnerHtml = lLector["operador_venta"].ToString();
                tblTransporte.Rows[liConta].Cells[14].InnerHtml = lLector["nombre_venta"].ToString();
                tblTransporte.Rows[liConta].Cells[15].InnerHtml = lLector["cod_ruta_sub"].ToString();
                tblTransporte.Rows[liConta].Cells[16].InnerHtml = lLector["desc_ruta_sub"].ToString();
                tblTransporte.Rows[liConta].Cells[17].InnerHtml = lLector["cod_tramo_sub"].ToString();
                tblTransporte.Rows[liConta].Cells[18].InnerHtml = lLector["desc_tramo_sub"].ToString();
                tblTransporte.Rows[liConta].Cells[19].InnerHtml = lLector["cargo_variable"].ToString();
                tblTransporte.Rows[liConta].Cells[20].InnerHtml = lLector["cargo_fijo_inv"].ToString();
                tblTransporte.Rows[liConta].Cells[21].InnerHtml = lLector["cargo_fijo_adm"].ToString();
                tblTransporte.Rows[liConta].Cells[22].InnerHtml = lLector["porc_cuota"].ToString();
                tblTransporte.Rows[liConta].Cells[23].InnerHtml = lLector["porc_imp_tra"].ToString();
                tblTransporte.Rows[liConta].Cells[24].InnerHtml = lLector["cantidad_sub"].ToString();

                liConta++;
                liAux--;
            }
        }
    }

}