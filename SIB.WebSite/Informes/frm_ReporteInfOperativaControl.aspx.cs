﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;
using System.Text;
using Segas.Web.Elements;

namespace Informes
{
    public partial class frm_ReporteInfOperativaControl : System.Web.UI.Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Reportes Información Operativa Control";
        clConexion lConexion = null;
        SqlDataReader lLector;
        DataSet lds = new DataSet();
        SqlDataAdapter lsqldata = new SqlDataAdapter();

        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo.ToString();
            lConexion = new clConexion(goInfo);

            buttons.FiltrarOnclick += imbConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;

            if (!IsPostBack)
            {
                /// Llenar controles
                lConexion.Abrir();
                // Campo nuevo tablero de control 20160712
                LlenarControles(lConexion.gObjConexion, ddlReporte, "m_reporte_inf_ope", " 1=1 order by codigo_reporte", 0, 1);
                lConexion.Cerrar();
                buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
            }
           
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// // 20180126 rq107-16
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                lItem1.Value = lLector["codigo_operador"].ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }
        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbConsultar_Click(object sender, EventArgs e)
        {
            bool Error = false;
            DateTime ldFecha = DateTime.Now;
            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, "Formato Inválido en el Campo Fecha Inicial. <br>");
                    Error = true;

                }
            }
            else
            {
                Toastr.Warning(this, "Debe seleccionar la fecha inicial.<br> ");
                Error = true;

            }
            if (TxtFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    if (Convert.ToDateTime(TxtFechaFin.Text) < ldFecha)
                    {
                        Toastr.Warning(this, "La fecha final debe ser mayor que la inicial. <br>");
                        Error = true;

                    }
                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, "Formato Inválido en el Campo Fecha Final. <br>");
                    Error = true;
                }
            }
            if (ddlReporte.SelectedValue == "0")
            {
                Toastr.Warning(this, "Debe seleccionar el Tipo de Reporte.<br> ");
                Error = true;

            }
            if (!Error)
            {
                try
                {
                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    SqlCommand lComando = new SqlCommand();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandTimeout = 3600;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_GetInfOpeCont";
                    lComando.Parameters.Add("@P_numero_reporte", SqlDbType.Int).Value = ddlReporte.SelectedValue;
                    lComando.Parameters.Add("@P_fecha_inicial", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                    if (TxtFechaFin.Text.Trim().Length <= 0)
                        lComando.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                    else
                        lComando.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaFin.Text.Trim();
                    lComando.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = ddlOperador.SelectedValue;
                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgMaestro.DataSource = lds;
                    dtgMaestro.DataBind();
                    dtgMaestro.Visible = true;
                    tblGrilla.Visible = true;
                 //   imbExcel.Visible = true;
                    lConexion.Cerrar();
                    int liCont = 1;
                    foreach (DataGridItem Grilla in this.dtgMaestro.Items)
                    {
                        while (liCont <= 100)
                        {
                            if (Grilla.Cells[liCont].Text != "&nbsp;")
                            {
                                this.dtgMaestro.Columns[liCont].HeaderText = Grilla.Cells[liCont].Text;
                                this.dtgMaestro.Columns[liCont].Visible = true;
                            }
                            else
                                this.dtgMaestro.Columns[liCont].Visible = false;
                            liCont++;
                        }
                        break;
                    }

                    dtgMaestro.DataBind();
                    liCont = 0;
                    foreach (DataGridItem Grilla in this.dtgMaestro.Items)
                    {
                        while (liCont <= 100)
                        {
                            Grilla.Cells[liCont].Visible = false;
                            //Grilla.Cells.RemoveAt(0);
                            liCont++;
                        }
                        break;
                    }
                }
                catch (Exception ex)
                {

                    Toastr.Warning(this, "No se Pudo Generar el Informe.! " + ex.Message.ToString());

                }
            }
        }
        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string lsNombreArchivo = goInfo.Usuario.ToString() + "InfControl" + DateTime.Now + ".xls";
            string lstitulo_informe = "Reporte  " + ddlReporte.SelectedItem.ToString();
            string lsTituloParametros = "";
            try
            {
                if (TxtFechaIni.Text.Trim().Length > 0)
                    lsTituloParametros += " - Fecha inicial: " + TxtFechaIni.Text.Trim();
                if (TxtFechaFin.Text.Trim().Length > 0)
                    lsTituloParametros += " - Fecha final : " + TxtFechaFin.Text.Trim();
                if (ddlOperador.SelectedValue != "0")
                    lsTituloParametros += "  - Operador: " + ddlOperador.SelectedItem.ToString();
                decimal ldCapacidad = 0;
                StringBuilder lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                dtgMaestro.EnableViewState = false;
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                lform.Controls.Add(dtgMaestro);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = System.Text.Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());

                Response.End();
                lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pudo Generar el Excel.! " + ex.Message.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlReporte_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlReporte.SelectedValue != "0")
            {
                ddlOperador.Items.Clear();
                /// Llenar controles del Formulario
                lConexion.Abrir();
                LlenarControles1(lConexion.gObjConexion, ddlOperador, "m_operador ope, m_reporte_operador rep", " ope.estado = 'A' and ope.tipo_operador = rep.sigla_tipo_operador and rep.codigo_reporte = " + ddlReporte.SelectedValue + " and rep.estado ='A' order by ope.razon_social", 0, 4);  // 20180126 rq107-16
                lConexion.Cerrar();
            }

        }
    }

}
