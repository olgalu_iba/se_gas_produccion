﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;


public partial class Informes_frm_AuditoriaMaestros : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Auditoría de Maestros";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlTabla, "", "", 1, 0);
            lConexion.Cerrar();
            TxtAño.Text = DateTime.Now.Date.Year.ToString();
            TxtFechaIni.Text = DateTime.Now.Date.ToString("yyyy/MM/dd");
            TxtFechaFin.Text = DateTime.Now.Date.ToString("yyyy/MM/dd");
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_RptAuditoriaMaestroTab", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        DateTime ldFecha;
        if (TxtAño.Text == "")
            lblMensaje.Text += "Debe digitar el año para la consulta de la auditoría. <br>";
        try
        {
            ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
            if (TxtAño.Text != "" && Convert.ToInt32(TxtAño.Text) != ldFecha.Year)
                lblMensaje.Text += "El año de la fecha inicial debe ser igual al año de consulta. <br>";
        }
        catch (Exception ex)
        {
            lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial. <br>";
        }
        try
        {
            ldFecha = Convert.ToDateTime(TxtFechaFin.Text);
            if (TxtAño.Text != "" && Convert.ToInt32(TxtAño.Text) != ldFecha.Year)
                lblMensaje.Text += "El año de la fecha final debe ser igual al año de consulta. <br>";
        }
        catch (Exception ex)
        {
            lblMensaje.Text += "Formato Inválido en el Campo Fecha Final. <br>";
        }
        
        if (ddlTabla.SelectedValue == "0")
            lblMensaje.Text += "Debe seleccionar la tabla a auditar. <br>";
        if (lblMensaje.Text == "")
        {
            tblGrilla.Visible = false;
            imbExcel.Visible = false;
            try
            {
                //valida que exista la auditoria
                SqlDataReader lLector;
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_RptAuditoriaMaestroVal";
                lComando.Parameters.Add("@P_año", SqlDbType.Int).Value = TxtAño.Text;
                lComando.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = ddlTabla.SelectedValue.ToString();
                lLector = lComando.ExecuteReader();
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lblMensaje.Text = lLector["error"].ToString();
                }
                lLector.Close();
                lLector.Dispose();
                //trae la información
                if (lblMensaje.Text == "")
                {
                    lComando.Parameters.Clear();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandTimeout = 3600;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_RptAuditoriaMaestro";
                    lComando.Parameters.Add("@P_año", SqlDbType.Int).Value = TxtAño.Text;
                    lComando.Parameters.Add("@P_fecha_ini", SqlDbType.VarChar).Value = TxtFechaIni.Text;
                    lComando.Parameters.Add("@P_fecha_fin", SqlDbType.VarChar).Value = TxtFechaFin.Text;
                    lComando.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = ddlTabla.SelectedValue.ToString();
                    lComando.Parameters.Add("@P_usuario_evento", SqlDbType.VarChar).Value = txtUsuario.Text;
                    if (ddlEvento.SelectedValue == "")
                        lComando.Parameters.Add("@P_evento", SqlDbType.VarChar).Value = "";
                    else
                        lComando.Parameters.Add("@P_evento", SqlDbType.VarChar).Value = ddlEvento.SelectedItem.ToString();
                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgReporte.DataSource = lds;
                    //EditCommandColumn columna = new EditCommandColumn();
                    //columna.EditText = "Detalle";
                    //gvReporte.Columns.Add(columna);
                    //gvReporte.AutoGenerateEditButton = true;

                    dtgReporte.DataBind();
                    tblGrilla.Visible = true;
                    imbExcel.Visible = true;
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.!";
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgReporte_EditCommand(object source, DataGridCommandEventArgs e)
    {
        tblDatos.Visible = false;
        tblMensaje.Visible = false;
        tblGrilla.Visible = false;
        tbDetalle.Visible = true;
        try
        {
            //fima la condicion con los campos

            string oCondicion = "";
            int licampo;
            string lsFecha = "";
            foreach (DataGridItem Grilla in this.dtgColumnas.Items)
            {
                licampo = Convert.ToInt32(Grilla.Cells[1].Text);
                if (Grilla.Cells[2].Text == "61" || Grilla.Cells[2].Text == "58")
                    {
                    lsFecha = Convert.ToDateTime(this.dtgReporte.Items[e.Item.ItemIndex].Cells[licampo].Text).Year.ToString() + "/" + Convert.ToDateTime(this.dtgReporte.Items[e.Item.ItemIndex].Cells[licampo].Text).Month.ToString() + "/" + Convert.ToDateTime(this.dtgReporte.Items[e.Item.ItemIndex].Cells[licampo].Text).Day.ToString();
                    oCondicion += " and " + Grilla.Cells[0].Text + "='" + lsFecha + "'";
                }
                else
                    oCondicion += " and " + Grilla.Cells[0].Text + "='" + this.dtgReporte.Items[e.Item.ItemIndex].Cells[licampo].Text + "'";
            }
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            SqlCommand lComando = new SqlCommand();
            lComando.Connection = lConexion.gObjConexion;
            lComando.CommandTimeout = 3600;
            lComando.CommandType = CommandType.StoredProcedure;
            lComando.CommandText = "pa_RptAuditoriaMaestroDet";
            lComando.Parameters.Add("@P_año", SqlDbType.Int).Value = TxtAño.Text;
            lComando.Parameters.Add("@P_fecha_ini", SqlDbType.VarChar).Value = TxtFechaIni.Text;
            lComando.Parameters.Add("@P_fecha_fin", SqlDbType.VarChar).Value = TxtFechaFin.Text;
            lComando.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = ddlTabla.SelectedValue.ToString();
            lComando.Parameters.Add("@P_usuario_evento", SqlDbType.VarChar).Value = txtUsuario.Text;
            if (ddlEvento.SelectedValue == "")
                lComando.Parameters.Add("@P_evento", SqlDbType.VarChar).Value = "";
            else
                lComando.Parameters.Add("@P_evento", SqlDbType.VarChar).Value = ddlEvento.SelectedItem.ToString();
            lComando.Parameters.Add("@P_condicion", SqlDbType.VarChar).Value = oCondicion;
            lComando.ExecuteNonQuery();
            lsqldata.SelectCommand = lComando;
            lsqldata.Fill(lds);
            dtgDetalle.DataSource = lds;
            dtgDetalle.DataBind();
            lConexion.Cerrar();
            int liConta = 0;
            int liColumnas = 0;
            String[] sValorAnt = { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };
            foreach (DataGridItem Grilla in this.dtgDetalle.Items)
            {
                liConta++;
                if (liConta == 1)
                {
                    int liDato = 0;
                    try
                    {
                        while (liDato <= 150)
                        {
                            sValorAnt[liDato] = Grilla.Cells[liDato + 3].Text;
                            liDato++;
                        }
                    }
                    catch (Exception ex)
                    {
                        liColumnas = liDato;
                        continue;
                    }

                }
                else
                {
                    int liDato = 0;
                    while (liDato < liColumnas)
                    {
                        if (sValorAnt[liDato] != Grilla.Cells[liDato + 3].Text)
                            Grilla.Cells[liDato + 3].Font.Bold = true;
                        sValorAnt[liDato] = Grilla.Cells[liDato + 3].Text;
                        liDato++;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlTabla_SelectedIndexChanged(object sender, EventArgs e)
    {
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        SqlCommand lComando = new SqlCommand();
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandTimeout = 3600;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_RptAuditoriaMaestroLlave";
        lComando.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = ddlTabla.SelectedValue.ToString();
        lComando.ExecuteNonQuery();
        lsqldata.SelectCommand = lComando;
        lsqldata.Fill(lds);
        dtgColumnas.DataSource = lds;
        dtgColumnas.DataBind();
        lConexion.Cerrar();
    }
    protected void BtnRegresar_Click(object sender, EventArgs e)
    {
        tblDatos.Visible = true;
        tblMensaje.Visible = true;
        tblGrilla.Visible = true;
        tbDetalle.Visible = false;
    }

    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "ConsAudmaestro" + DateTime.Now + ".xls";
        string lstitulo_informe = "Consulta de auditoría de maestro: " + ddlTabla.SelectedItem.Text;
        try
        {
            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgReporte.Columns[0].Visible = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            dtgReporte.EnableViewState = false;
            lform.Controls.Add(dtgReporte);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='7' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
            dtgReporte.Columns[0].Visible = true;
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Excel.!" + ex.Message.ToString();
        }
    }

    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel1_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "ConsAudmaestroDet" + DateTime.Now + ".xls";
        string lstitulo_informe = "Consulta de auditoría de maestro: " + ddlTabla.SelectedItem.Text;
        try
        {
            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            dtgDetalle.EnableViewState = false;
            lform.Controls.Add(dtgDetalle);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='7' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Excel.!" + ex.Message.ToString();
        }

    }

}
