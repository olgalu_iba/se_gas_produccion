﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_consultaPdfContrato.aspx.cs" Inherits="Informes_frm_consultaPdfContrato" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />

                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha negociación Inicial</label>
                            <asp:TextBox ID="TxtFechaIni" runat="server" Width="100%" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha negociación Final</label>
                            <asp:TextBox ID="TxtFechaFin" runat="server" Width="100%" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div id="tdMes01" class="form-group" runat="server">
                            <label for="Name">Número Operación</label>
                            <asp:TextBox ID="TxtNoOper" runat="server" Width="100%" CssClass="form-control" ValidationGroup="detalle"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FtebTxtNoOper" runat="server" TargetControlID="TxtNoOper"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div id="Div1" class="form-group" runat="server">
                            <label for="Name">Contrato</label>
                            <asp:TextBox ID="TxtContratoDef" runat="server" Width="100%" CssClass="form-control" ValidationGroup="detalle"></asp:TextBox>

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Operador</label>
                            <asp:DropDownList ID="ddlOperador" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Tipo de Mercado</label>
                            <asp:DropDownList ID="ddlMercado" runat="server" Width="100%" CssClass="form-control">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="P">Primario</asp:ListItem>
                                <asp:ListItem Value="S">Secundario</asp:ListItem>
                                <asp:ListItem Value="O">OTMM</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                    id="Div2">


                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="detalle" />
                    <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>

                </div>


                <div border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                    id="tblGrilla" visible="false">

                    <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                        <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            PagerStyle-HorizontalAlign="Center" OnItemCommand="dtgConsulta_EditCommand">

                            <Columns>
                                <%--0--%>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--1--%>
                                <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--2--%>
                                <asp:BoundColumn DataField="contrato_definitivo" HeaderText="No. Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--3--%>
                                <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--4--%>
                                <asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--5--%>
                                <asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--6--%>
                                <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <%--7--%>
                                <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <%--8--%>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <%--9--%>
                                <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                                <%--10--%> <%--20200430--%>
                                <asp:BoundColumn DataField="nombre_archivo" HeaderText="Archivo Sistema"></asp:BoundColumn>
                                <%--11--%><%--20200430--%>
                                <asp:BoundColumn DataField="nombre_archivo_org" HeaderText="Nombre Archivo Original"></asp:BoundColumn>
                                <%--12--%>
                                <asp:BoundColumn DataField="operador_venta" Visible="false"></asp:BoundColumn>
                                <%--13--%>
                                <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                    <ItemTemplate>
                                        <div class="dropdown dropdown-inline">
                                            <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="flaticon-more-1"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                <!--begin::Nav-->
                                                <ul class="kt-nav">
                                                    <li class="kt-nav__item">
                                                        <asp:LinkButton ID="lkbCargar" CssClass="kt-nav__link" CommandName="Ver PDF" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-crisp-icons"></i>
                                                            <span class="kt-nav__link-text">Ver PDF</span>
                                            </asp:LinkButton>
                                                    </li>
                                                </ul>
                                                <!--end::Nav-->
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>

