﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;
using System.Text;
using Segas.Web.Elements;

public partial class Informes_frm_ConsModRegCont : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Consulta de modificaciones de contratos registrados";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();
    string[] lsCarperta; //20171130 rq026-17
    string Carpeta; //20171130 rq026-17
    string sRutaArc = ConfigurationManager.AppSettings["rutaModif"].ToString();//20171130 rq026-17 
    string lsCarpetaAnt = ""; //20171130 rq026-17


    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        //Se agrega el comportamiento del botón
        buttons.ExportarExcelOnclick += ImgExcel_Click;
        buttons.FiltrarOnclick += imbConsultar_Click;

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlSubasta, "m_tipos_subasta", " estado='A' order by descripcion", 0, 1);
            LlenarControles2(lConexion.gObjConexion, ddlCausa, "", "", 0, 1); //20171130 rq026-17
            lConexion.Cerrar();
            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar };
            buttons.Inicializar(botones: botones);
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles2(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetCausaModAdc", null, null, null);
        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);
        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["descripcion"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, EventArgs e)
    {
        bool Error = false;
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF = DateTime.Now;
        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Formato Inválido en el Campo Fecha Inicial. <br>");
                Error = true;
            }
        }
        else
        {
            Toastr.Warning(this, "Debe digitar la Fecha Inicial. <br>");
            Error = true;
        }
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
                if (TxtFechaIni.Text.Trim().Length > 0 && ldFechaF < ldFechaI)
                {
                    Toastr.Warning(this, "La Fecha Final NO puede ser Menor que la Fecha Inicial. <br>");
                    Error = true;

                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Formato Inválido en el Campo Fecha Final. <br>");
                Error = true;

            }
        }
        else
        {
            Toastr.Warning(this, "Debe digitar la Fecha Final. <br>");
            Error = true;
        }

        //if (TxtOperacionIni.Text == "" && TxtOperacionFin.Text != "")
        //    lblMensaje.Text = "Debe digitar la operación inicial antes que la final<br>";
        if (!Error)
        {
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetConsModRegCont";
                lComando.Parameters.Add("@P_fecha_inicial", SqlDbType.VarChar).Value = TxtFechaIni.Text;
                lComando.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaFin.Text;
                if (TxtOperacion.Text != "")
                    lComando.Parameters.Add("@P_numero_contrato", SqlDbType.Int).Value = TxtOperacion.Text;
                lComando.Parameters.Add("@P_tipo_mercado", SqlDbType.Char).Value = ddlMercado.SelectedValue;
                lComando.Parameters.Add("@P_destino_rueda", SqlDbType.Char).Value = ddlProducto.SelectedValue;
                lComando.Parameters.Add("@P_codigo_tipo_subasta", SqlDbType.Int).Value = ddlSubasta.SelectedValue;
                lComando.Parameters.Add("@P_codigo_causa", SqlDbType.VarChar).Value = ddlCausa.SelectedValue;
                //20171130 rq026-17
                if (TxtIdRegistro.Text != "")
                    lComando.Parameters.Add("@P_codigo_verif", SqlDbType.Int).Value = TxtIdRegistro.Text;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgMaestro.DataSource = lds;
                dtgMaestro.DataBind();
                tblGrilla.Visible = true;
                buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pudo Generar el Informe.! " + ex.Message.ToString());

            }
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, EventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "ConsModContReg" + DateTime.Now + ".xls";
        string lstitulo_informe = "Informe Modificación de Operaciones registradas";
        string lsTituloParametros = "";
        try
        {
            lsTituloParametros = "Fecha Neg: " + TxtFechaIni.Text + " - " + TxtFechaFin.Text;
            if (TxtOperacion.Text != "")
                lsTituloParametros += " - Operación: " + TxtOperacion.Text;
            //if (TxtOperacionFin.Text != "")
            //    lsTituloParametros += "-" + TxtOperacionFin.Text;
            if (ddlMercado.SelectedValue != "")
                lsTituloParametros += "  - Tipo Mercado: " + ddlMercado.SelectedItem.ToString();
            if (ddlProducto.SelectedValue != "")
                lsTituloParametros += "  - Producto: " + ddlProducto.SelectedItem.ToString();
            if (ddlSubasta.SelectedValue != "0")
                lsTituloParametros += "  - Subasta: " + ddlSubasta.SelectedItem.ToString();
            //20171130 rq026-17
            if (ddlCausa.SelectedValue != "0")
                lsTituloParametros += "  - Causa Modificación: " + ddlCausa.SelectedItem.ToString();
            //20171130 rq026-17
            if (TxtIdRegistro.Text != "")
                lsTituloParametros += " - Id Registro: " + TxtIdRegistro.Text;
            dtgMaestro.Columns[22].Visible = false; //20160607 fuente MP //rq026-17 20171130
            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgMaestro.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            lform.Controls.Add(dtgMaestro);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='6' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
            dtgMaestro.Columns[22].Visible = true; //20160607 fuente MP //20171130 rq026-17
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "No se Pudo Generar el Excel.! " + ex.Message.ToString());
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensajeDet.Text = "";
        hdnContrato.Value = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        SqlCommand lComando = new SqlCommand();
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandTimeout = 3600;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_GetConsModRegContDet";
        lComando.Parameters.Add("@P_numero_contrato", SqlDbType.Int).Value = hdnContrato.Value;
        lComando.ExecuteNonQuery();
        lsqldata.SelectCommand = lComando;
        lsqldata.Fill(lds);
        dtgDetalle.DataSource = lds;
        dtgDetalle.DataBind();
        lConexion.Cerrar();
        tblGrilla.Visible = false;
        tbDetalle.Visible = true;
        tblMensaje.Visible = false;
        //tblDatos.Visible = false;
        //REcorro la grilla para mostrar los cambios en negrilla
        string sFechaNeg = "";
        string sPunto = "";
        string sModalidad = "";
        string sFechaIni = "";
        string sFechaFin = "";
        string sCantidad = "";
        string sPrecio = "";
        string sFuente = "";  //20160607 fuente MP
        string sCausa = "";  //20171130 rq026-17
        int liConta = 0;
        foreach (DataGridItem Grilla in this.dtgDetalle.Items)
        {
            liConta++;
            if (liConta == 1)
            {
                sFechaNeg = Grilla.Cells[1].Text;
                sPunto = Grilla.Cells[10].Text; //20171130 rq026-17
                sModalidad = Grilla.Cells[12].Text; //20171130 rq026-17
                sFechaIni = Grilla.Cells[14].Text; //20171130 rq026-17
                sFechaFin = Grilla.Cells[15].Text; //20171130 rq026-17
                sCantidad = Grilla.Cells[16].Text; //20171130 rq026-17 
                sPrecio = Grilla.Cells[17].Text; //20171130 rq026-17
                sFuente = Grilla.Cells[18].Text;  //20160607 fuente MP //20171130 rq026-17
                sCausa = Grilla.Cells[20].Text;  //20160607 fuente MP //20171130 rq026-17
            }
            else
            {
                if (sFechaNeg != Grilla.Cells[1].Text)
                    Grilla.Cells[1].Font.Bold = true;
                if (sPunto != Grilla.Cells[10].Text) //20171130 rq026-17
                {
                    Grilla.Cells[10].Font.Bold = true; //20171130 rq026-17
                    Grilla.Cells[11].Font.Bold = true; //20171130 rq026-17
                }
                if (sModalidad != Grilla.Cells[12].Text) //20171130 rq026-17
                {
                    Grilla.Cells[12].Font.Bold = true; //20171130 rq026-17
                    Grilla.Cells[13].Font.Bold = true; //20171130 rq026-17
                }
                if (sFechaIni != Grilla.Cells[14].Text) //20171130 rq026-17
                    Grilla.Cells[14].Font.Bold = true; //20171130 rq026-17
                if (sFechaFin != Grilla.Cells[15].Text) //20171130 rq026-17
                    Grilla.Cells[15].Font.Bold = true; //20171130 rq026-17
                if (sCantidad != Grilla.Cells[16].Text) //20171130 rq026-17
                    Grilla.Cells[16].Font.Bold = true; //20171130 rq026-17
                if (sPrecio != Grilla.Cells[17].Text) //20171130 rq026-17
                    Grilla.Cells[17].Font.Bold = true; //20171130 rq026-17
                //20160607 fuente MP //20171130 rq026-17
                if (sFuente != Grilla.Cells[18].Text)
                {
                    Grilla.Cells[18].Font.Bold = true;
                    Grilla.Cells[19].Font.Bold = true;
                }
                //20171130 rq026-17 //20171130 rq026-17
                if (sCausa != Grilla.Cells[20].Text)
                {
                    Grilla.Cells[20].Font.Bold = true;
                    Grilla.Cells[21].Font.Bold = true;
                }
                sFechaNeg = Grilla.Cells[1].Text;
                //20171130 rq026-17
                sPunto = Grilla.Cells[10].Text;
                sModalidad = Grilla.Cells[12].Text;
                sFechaIni = Grilla.Cells[14].Text;
                sFechaFin = Grilla.Cells[15].Text;
                sCantidad = Grilla.Cells[16].Text;
                sPrecio = Grilla.Cells[17].Text;
                sFuente = Grilla.Cells[18].Text; //20160607  fuente MP
                sCausa = Grilla.Cells[20].Text; //20171130 rq026-18
                //fin 20171130 rq026-17
            }

        }
    }
    protected void BtnRegresar_Click(object sender, EventArgs e)
    {
        tblGrilla.Visible = true;
        tbDetalle.Visible = false;
        tblMensaje.Visible = true;
        //tblDatos.Visible = true;
    }

    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel1_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "ConsModContRegDet" + DateTime.Now + ".xls";
        string lstitulo_informe = "Detalle de modificaciones de la operación " + hdnContrato.Value;
        try
        {
            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgDetalle.Columns[24].Visible = false; //20171130 rq026-17
            dtgDetalle.Columns[25].Visible = false; //20171130 rq026-17
            dtgDetalle.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            lform.Controls.Add(dtgDetalle);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
            dtgDetalle.Columns[24].Visible = true; //20171130 rq026-17
            dtgDetalle.Columns[25].Visible = true; //20171130 rq026-17
        }
        catch (Exception ex)
        {

            Toastr.Warning(this, "No se Pudo Generar el Excel.! " + ex.Message.ToString());
 
        }
    }
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    /// 20171130 rq026-17 
    protected void dtgDetalle_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensajeDet.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Original")
        {
            try
            {
                if (e.Item.Cells[26].Text.Trim() != "&nbsp;" && e.Item.Cells[26].Text.Trim() != "") //20171130 rq026-17
                {
                    lsCarperta = sRutaArc.Split('\\');
                    lsCarpetaAnt = lsCarperta[lsCarperta.Length - 2];
                    string lsRuta = "../" + lsCarpetaAnt + "/" + e.Item.Cells[26].Text.Replace(@"\", "/"); //20171130 rq026-17
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + lsRuta + "');", true);
                }
                else
                    Toastr.Warning(this, "No hay archivo para visualizar");
       

            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Problemas al mostrar el archivo. " + ex.Message.ToString());
            }
        }
        if (((LinkButton)e.CommandSource).Text == "Modificado")
        {
            try
            {
                if (e.Item.Cells[27].Text.Trim() != "&nbsp;" && e.Item.Cells[27].Text.Trim() != "") //20171130 rq026-17
                {
                    lsCarperta = sRutaArc.Split('\\');
                    lsCarpetaAnt = lsCarperta[lsCarperta.Length - 2];
                    string lsRuta = "../" + lsCarpetaAnt + "/" + e.Item.Cells[27].Text.Replace(@"\", "/"); //20171130 rq026-17
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + lsRuta + "');", true);
                }
                else
                    Toastr.Warning(this, "No hay archivo para visualizar");


            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Problemas al mostrar el archivo. " + ex.Message.ToString());
        
            }
        }
    }
}
