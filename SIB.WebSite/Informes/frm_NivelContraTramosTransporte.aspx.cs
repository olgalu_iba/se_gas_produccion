﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Informes
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frm_NivelContraTramosTransporte : Page
    {
        private InfoSessionVO goInfo;
        private static string lsTitulo = "Consulta Nivel de Contratación por Tramos";
        private clConexion lConexion;
        private SqlDataReader lLector;
        private DataSet lds = new DataSet();
        private SqlDataAdapter lsqldata = new SqlDataAdapter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo.ToString();
            lConexion = new clConexion(goInfo);
            //Titulo
            Master.Titulo = "Informes";
            //Activacion de los Botones
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;

            if (IsPostBack) return;
            //Activacion de los Botones
            buttons.Inicializar(botones: new[] { EnumBotones.Buscar });
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlTippSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlModalidad, "m_modalidad_contractual mod, m_modalidad_rep_auditoria rep", " mod.codigo_modalidad = rep.codigo_modalidad and rep.tipo_reporte = 2 and mod.estado = 'A'  and rep.estado ='A' order by descripcion", 0, 1);
            LlenarControles1(lConexion.gObjConexion, ddlTramo, "m_tramo", " estado = 'A' order by descripcion", 0, 1);
            lConexion.Cerrar();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
            //dtgComisionista.DataBind();

            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetTramo", null, null, null);
            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);
            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["desc_tramo"].ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string lsNombreArchivo = goInfo.Usuario.ToString() + "InfNivelContTra" + DateTime.Now + ".xls";
            string lstitulo_informe = "Informe Nivel de Contratacion por Tramos - Transporte";
            string lsTituloParametros = "";
            try
            {
                if (TxtFechaIni.Text.Trim().Length > 0)
                    lsTituloParametros += " - Fecha inicial: " + TxtFechaIni.Text.Trim();
                if (TxtFechaFin.Text.Trim().Length > 0)
                    lsTituloParametros += " - Fecha final : " + TxtFechaFin.Text.Trim();
                if (ddlFuente.SelectedValue != "0")
                    lsTituloParametros += " - Fuente : " + ddlFuente.SelectedItem.ToString();
                if (ddlTippSubasta.SelectedValue != "0")
                    lsTituloParametros += " - Tipo Subasta : " + ddlTippSubasta.SelectedItem.ToString();
                if (ddlTipoRueda.SelectedValue != "0" && ddlTipoRueda.SelectedValue != "")
                    lsTituloParametros += " - Tipo Rueda : " + ddlTipoRueda.SelectedItem.ToString();
                if (ddlModalidad.SelectedValue != "0")
                    lsTituloParametros += " - Modalidad : " + ddlModalidad.SelectedItem.ToString();
                if (ddlTipoMercado.SelectedValue != "0")
                    lsTituloParametros += " - Tipo Mercado : " + ddlTipoMercado.SelectedItem.ToString();
                if (ddlTramo.SelectedValue != "0")
                    lsTituloParametros += " - Tramo : " + ddlTramo.SelectedItem.ToString();

                decimal ldCapacidad = 0;
                StringBuilder lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                dtgMaestro.EnableViewState = false;
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                lform.Controls.Add(dtgMaestro);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = System.Text.Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());

                Response.End();
                lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pudo Generar el Excel.! " + ex.Message.ToString());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            bool Error = false;
            DateTime ldFecha = DateTime.Now;
            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, "Formato Inválido en el Campo Fecha Inicial. <br>");
                    Error = true;
                }
            }
            else
            {
                Toastr.Warning(this, "Debe seleccionar la fecha inicial.<br> ");
                Error = true;

            }
            if (TxtFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    if (Convert.ToDateTime(TxtFechaFin.Text) < ldFecha)
                    {
                        Toastr.Warning(this, "La fecha final debe ser mayor que la inicial. <br>");
                        Error = true;

                    }
                }
                catch (Exception ex)
                {

                    Toastr.Warning(this, "Formato Inválido en el Campo Fecha Final. <br>");
                    Error = true;
                }
            }
            if (!Error)
            {
                try
                {
                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    SqlCommand lComando = new SqlCommand();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandTimeout = 3600;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_GetInfNivelContTramos";
                    lComando.Parameters.Add("@P_fecha_inicial", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                    if (TxtFechaIni.Text.Trim().Length <= 0)
                        lComando.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                    else
                        lComando.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaFin.Text.Trim();
                    lComando.Parameters.Add("@P_fuente", SqlDbType.VarChar).Value = ddlFuente.SelectedValue;
                    lComando.Parameters.Add("@P_codigo_tipo_subasta", SqlDbType.Int).Value = ddlTippSubasta.SelectedValue;
                    if (ddlTipoRueda.SelectedValue != "")
                        lComando.Parameters.Add("@P_codigo_tipo_rueda", SqlDbType.Int).Value = ddlTipoRueda.SelectedValue;
                    else
                        lComando.Parameters.Add("@P_codigo_tipo_rueda", SqlDbType.Int).Value = "0";
                    lComando.Parameters.Add("@P_codigo_modalidad", SqlDbType.Int).Value = ddlModalidad.SelectedValue;

                    lComando.Parameters.Add("@P_tipo_mercado", SqlDbType.VarChar).Value = ddlTipoMercado.SelectedValue;
                    lComando.Parameters.Add("@P_codigo_tramo", SqlDbType.Int).Value = ddlTramo.SelectedValue;
                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgMaestro.DataSource = lds;
                    dtgMaestro.DataBind();
                    dtgMaestro.Visible = true;
                    dtgMaestro.Visible = true;
                    buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
                    lConexion.Cerrar();
                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, "No se Pude Generar el Informe.! " + ex.Message.ToString());
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlTippSubasta_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTippSubasta.SelectedValue != "0")
            {
                ddlTipoRueda.Items.Clear();
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlTipoRueda, "m_tipos_rueda", " estado = 'A' And codigo_tipo_subasta = " + ddlTippSubasta.SelectedValue + " order by descripcion", 0, 1);
                lConexion.Cerrar();
            }

        }
    }
}