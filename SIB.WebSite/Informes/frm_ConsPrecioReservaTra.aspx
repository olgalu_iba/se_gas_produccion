﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ConsPrecioReservaTra.aspx.cs" Inherits="Informes.Informes_frm_ConsPrecioReservaTra" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">

                <div runat="server" visible="false" id="tblGrilla">
                    <div class="table table-responsive">
                        <%--20180126 rq107-16--%>
                        <asp:DataGrid ID="dtgMaestro" AutoGenerateColumns="False" AllowPaging="true" PageSize="20" OnPageIndexChanged="dtgMaestro_PageIndexChanged" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" runat="server">
                            <Columns>
                                <asp:BoundColumn DataField="año_negociacion" HeaderText="Año Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="trimestre_negociacion" HeaderText="Trimestre Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_trimestre" HeaderText="Nombre Trimestre" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_ruta" HeaderText="Código Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20220828--%>
                                <asp:BoundColumn DataField="costo_ruta" HeaderText="Precio de Reserva Ruta (Moneda vigente/KPC)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                    <asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False" AllowPaging="false" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered">
                        <Columns>
                            <asp:BoundColumn DataField="año_negociacion" HeaderText="Año Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="trimestre_negociacion" HeaderText="Trimestre Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_trimestre" HeaderText="Nombre Trimestre" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_ruta" HeaderText="Código Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20220828--%>
                            <asp:BoundColumn DataField="costo_ruta" HeaderText="Precio de Reserva Ruta (Moneda vigente/KPC)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
