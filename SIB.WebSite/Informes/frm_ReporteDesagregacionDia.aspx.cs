﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;

public partial class Informes_frm_ReporteDesagregacionDia : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Reportes de desagregación diaria";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, DdlSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlRueda, "m_tipos_rueda", " codigo_tipo_subasta = 0 and estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlModalidad, "m_modalidad_contractual", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlPunto, "m_pozo", " estado = 'A'  order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlRuta, "m_ruta_snt", " estado = 'A'  order by descripcion", 0, 4);
            LlenarControles(lConexion.gObjConexion, DdlRolVenta, "m_tipos_operador", " estado = 'A'  order by descripcion", 2, 1);
            LlenarControles1(lConexion.gObjConexion, DdlOperadorV, "m_operador", " estado = 'A'  order by razon_social", 0, 1); // 20180307 fin rq010-11
            LlenarControles(lConexion.gObjConexion, DdlRolCompra, "m_tipos_operador", " estado = 'A'  order by descripcion", 2, 1);
            LlenarControles1(lConexion.gObjConexion, DdlOperadorC, "m_operador", " estado = 'A'  order by razon_social", 0, 1); // 20180307 fin rq010-11
            LlenarControles(lConexion.gObjConexion, DdlDemanda, "m_tipo_demanda_atender", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlPtoSal, "m_punto_salida_snt", " estado = 'A' order by descripcion", 0, 2);  //20180307 rq010-18
            lConexion.Cerrar();
            DdlDemanda_SelectedIndexChanged(null, null); // 20160706 sector de conusmo
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            // 20180307 fin rq010-11
            //if (lsTabla != "m_operador")
            //{
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            //}
            // 20180307 fin rq010-11
            //else
            //{
            //    lItem1.Value = lLector["codigo_operador"].ToString();
            //    lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            //}
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }

    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    /// 20180307 fin rq010-11
    protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector["codigo_operador"].ToString();
            lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF = DateTime.Now;
        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial. <br>";
            }
        }
        else
            lblMensaje.Text += "Debe digitar la Fecha Inicial. <br>";
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
                if (TxtFechaIni.Text.Trim().Length > 0 && ldFechaF < ldFechaI)
                    lblMensaje.Text += "La Fecha Final NO puede ser Menor que la Fecha Inicial. <br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Final. <br>";
            }
        }
        else
            lblMensaje.Text += "Debe digitar la Fecha Final. <br>";
        if (lblMensaje.Text == "")
        {
            TimeSpan DifFecha;
            DifFecha = ldFechaF - ldFechaI;
            if (ddlReporte.SelectedValue == "1" || ddlReporte.SelectedValue == "2" || ddlReporte.SelectedValue == "7" || ddlReporte.SelectedValue == "8" || ddlReporte.SelectedValue == "9")  //20180307 rq010-18
            {
                if (DifFecha.Days > 366) //20180307 rq010-18
                    lblMensaje.Text += "El rango de fechas debe ser máximo de un año. <br>";
            }
            if (ddlReporte.SelectedValue == "3" || ddlReporte.SelectedValue == "4")
            {
                if (DifFecha.Days > 92)
                    lblMensaje.Text += "El rango de fechas debe ser máximo de 3 meses. <br>";
            }
            if (ddlReporte.SelectedValue == "5" || ddlReporte.SelectedValue == "6")
            {
                if (DifFecha.Days > 31)
                    lblMensaje.Text += "El rango de fechas debe ser máximo de 1 mes. <br>";
            }
        }
        if (lblMensaje.Text == "")
        {
            try
            {
                dtgReporte1.Visible = false;
                dtgReporte2.Visible = false;
                dtgReporte3.Visible = false;
                dtgReporte4.Visible = false;
                dtgReporte5.Visible = false;
                dtgReporte6.Visible = false;
                dtgReporte7.Visible = false; // 20180307 rq010-18 
                dtgReporte8.Visible = false; // 20180307 rq010-18
                dtgReporte9.Visible = false; // 20180307 rq010-18

                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.Parameters.Clear();
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetContDesagreDia";
                lComando.Parameters.Add("@P_tipo_reporte", SqlDbType.Int).Value = ddlReporte.SelectedValue;
                lComando.Parameters.Add("@P_fecha_inicial", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                lComando.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaFin.Text.Trim();
                lComando.Parameters.Add("@P_fuente", SqlDbType.Char).Value = DdlFuente.SelectedValue;
                lComando.Parameters.Add("@P_codigo_tipo_rueda", SqlDbType.Char).Value = DdlRueda.SelectedValue;
                lComando.Parameters.Add("@P_codigo_tipo_subasta", SqlDbType.Char).Value = DdlSubasta.SelectedValue;
                lComando.Parameters.Add("@P_codigo_modalidad", SqlDbType.Char).Value = DdlModalidad.SelectedValue;
                if (ddlReporte.SelectedValue == "1" || ddlReporte.SelectedValue == "3" || ddlReporte.SelectedValue == "5")
                {
                    lComando.Parameters.Add("@P_codigo_punto_entrega", SqlDbType.Char).Value = DdlPunto.SelectedValue;
                }
                //20180307 rq010-18
                if (ddlReporte.SelectedValue == "2" || ddlReporte.SelectedValue == "4" || ddlReporte.SelectedValue == "6")
                {
                    lComando.Parameters.Add("@P_codigo_punto_entrega", SqlDbType.Char).Value = DdlRuta.SelectedValue;
                }
                //20180307 rq010-18
                if (ddlReporte.SelectedValue == "7" || ddlReporte.SelectedValue == "8" || ddlReporte.SelectedValue == "9")
                {
                    lComando.Parameters.Add("@P_codigo_punto_entrega", SqlDbType.Char).Value = DdlPtoSal.SelectedValue;
                    lComando.Parameters.Add("@P_codigo_ruta", SqlDbType.Char).Value = DdlRuta.SelectedValue;
                }
                lComando.Parameters.Add("@P_codigo_sector_consumo", SqlDbType.Int).Value = DdlSector.SelectedValue;
                lComando.Parameters.Add("@P_tipo_mercado", SqlDbType.Char).Value = DdlMercado.SelectedValue;
                lComando.Parameters.Add("@P_rol_venta", SqlDbType.Char).Value = DdlRolVenta.SelectedValue;
                lComando.Parameters.Add("@P_operador_venta", SqlDbType.Int).Value = DdlOperadorV.SelectedValue;
                lComando.Parameters.Add("@P_rol_compra", SqlDbType.Char).Value = DdlRolCompra.SelectedValue;
                lComando.Parameters.Add("@P_operador_compra", SqlDbType.Int).Value = DdlOperadorC.SelectedValue;
                lComando.Parameters.Add("@P_codigo_tipo_demanda", SqlDbType.Int).Value = DdlDemanda.SelectedValue;

                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                switch (ddlReporte.SelectedValue)
                {
                    case "1":
                        dtgReporte1.DataSource = lds;
                        dtgReporte1.DataBind();
                        dtgReporte1.Visible = true;
                        break;
                    case "2":
                        dtgReporte2.DataSource = lds;
                        dtgReporte2.DataBind();
                        dtgReporte2.Visible = true;
                        break;
                    case "3":
                        dtgReporte3.DataSource = lds;
                        dtgReporte3.DataBind();
                        dtgReporte3.Visible = true;
                        break;
                    case "4":
                        dtgReporte4.DataSource = lds;
                        dtgReporte4.DataBind();
                        dtgReporte4.Visible = true;
                        break;
                    case "5":
                        dtgReporte5.DataSource = lds;
                        dtgReporte5.DataBind();
                        dtgReporte5.Visible = true;
                        break;
                    case "6":
                        dtgReporte6.DataSource = lds;
                        dtgReporte6.DataBind();
                        dtgReporte6.Visible = true;
                        break;
                    //20180307 rq010-18
                    case "7":
                        dtgReporte7.DataSource = lds;
                        dtgReporte7.DataBind();
                        dtgReporte7.Visible = true;
                        break;
                    case "8":
                        dtgReporte8.DataSource = lds;
                        dtgReporte8.DataBind();
                        dtgReporte8.Visible = true;
                        break;
                    case "9":
                        dtgReporte9.DataSource = lds;
                        dtgReporte9.DataBind();
                        dtgReporte9.Visible = true;
                        break;
                        //20180307 fin rq010-18
                }
                imbExcel.Visible = true;
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString(); //20180126 rq107-16
            }
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfDesagregadoDia" + DateTime.Now + ".xls";
        string lstitulo_informe = ddlReporte.SelectedItem.ToString();
        string lsTituloParametros = "";
        try
        {
            if (TxtFechaIni.Text.Trim().Length > 0)
                lsTituloParametros += " - Fecha Inicial: " + TxtFechaIni.Text.Trim();
            if (TxtFechaFin.Text.Trim().Length > 0)
                lsTituloParametros += " - Fecha Final: " + TxtFechaFin.Text.Trim();
            if (DdlFuente.SelectedValue != "")
                lsTituloParametros += " - Fuente: " + DdlFuente.SelectedItem.ToString();
            if (DdlSubasta.SelectedValue != "0")
                lsTituloParametros += " - Tipo Subasta: " + DdlSubasta.SelectedItem.ToString();
            if (DdlRueda.SelectedValue != "0")
                lsTituloParametros += " - Tipo Rueda: " + DdlRueda.SelectedItem.ToString();
            if (DdlModalidad.SelectedValue != "0")
                lsTituloParametros += " - Modalidad: " + DdlModalidad.SelectedItem.ToString();
            if (DdlPunto.SelectedValue != "0")
                lsTituloParametros += " - Punto Entrega: " + DdlPunto.SelectedItem.ToString();
            //20180307 rq010-18
            if (DdlPtoSal.SelectedValue != "0")
                lsTituloParametros += " - Punto Salida: " + DdlPtoSal.SelectedItem.ToString();
            if (DdlRuta.SelectedValue != "0")
                lsTituloParametros += " - Ruta: " + DdlRuta.SelectedItem.ToString();
            if (DdlMercado.SelectedValue != "")
                lsTituloParametros += " - Tipo Mercado: " + DdlMercado.SelectedItem.ToString();
            if (DdlRolVenta.SelectedValue != "0")
                lsTituloParametros += " - Rol Venta: " + DdlRolVenta.SelectedItem.ToString();
            if (DdlOperadorV.SelectedValue != "0")
                lsTituloParametros += " - Operador Vta: " + DdlOperadorV.SelectedItem.ToString();
            if (DdlRolCompra.SelectedValue != "0")
                lsTituloParametros += " - Rol Compra: " + DdlRolCompra.SelectedItem.ToString();
            if (DdlOperadorC.SelectedValue != "0")
                lsTituloParametros += " - Operador Compra: " + DdlOperadorC.SelectedItem.ToString();
            if (DdlDemanda.SelectedValue != "0")
                lsTituloParametros += " - Tipo Demanda: " + DdlDemanda.SelectedItem.ToString();
            if (DdlSector.SelectedValue != "0")
                lsTituloParametros += " - Sector Consumo: " + DdlSector.SelectedItem.ToString();
            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            switch (ddlReporte.SelectedValue)
            {
                case "1":
                    dtgReporte1.EnableViewState = false;
                    lform.Controls.Add(dtgReporte1);
                    break;
                case "2":
                    dtgReporte2.EnableViewState = false;
                    lform.Controls.Add(dtgReporte2);
                    break;
                case "3":
                    dtgReporte3.EnableViewState = false;
                    lform.Controls.Add(dtgReporte3);
                    break;
                case "4":
                    dtgReporte4.EnableViewState = false;
                    lform.Controls.Add(dtgReporte4);
                    break;
                case "5":
                    dtgReporte5.EnableViewState = false;
                    lform.Controls.Add(dtgReporte5);
                    break;
                case "6":
                    dtgReporte6.EnableViewState = false;
                    lform.Controls.Add(dtgReporte6);
                    break;
                //20180307 rq010-18
                case "7":
                    dtgReporte7.EnableViewState = false;
                    lform.Controls.Add(dtgReporte7);
                    break;
                case "8":
                    dtgReporte8.EnableViewState = false;
                    lform.Controls.Add(dtgReporte8);
                    break;
                case "9":
                    dtgReporte9.EnableViewState = false;
                    lform.Controls.Add(dtgReporte9);
                    break;
                    //20180307 fin rq010-18
            }
            lpagina.RenderControl(lhtw);
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Excel.!" + ex.Message.ToString(); //20180126 rq107-16
        }
    }
    protected void ddlReporte_SelectedIndexChanged(object sender, EventArgs e)
    {
        DdlFuente.SelectedValue = "";
        DdlSubasta.SelectedValue = "0";
        DdlRueda.SelectedValue = "0";
        DdlModalidad.SelectedValue = "0";
        DdlPunto.SelectedValue = "0";
        DdlPtoSal.SelectedValue = "0"; //20180307 rq010-18
        DdlRuta.SelectedValue = "0";
        DdlMercado.SelectedValue = "";
        DdlRolVenta.SelectedValue = "0";
        DdlOperadorV.SelectedValue = "0";
        DdlRolCompra.SelectedValue = "0";
        DdlOperadorC.SelectedValue = "0";
        DdlDemanda.SelectedValue = "0";
        DdlSector.SelectedValue = "0";

        switch (ddlReporte.SelectedValue)
        {
            case "1":
                trFuente.Visible = true;
                trSubasta.Visible = true;
                trRueda.Visible = true;
                trModalidad.Visible = true;
                trPunto.Visible = true;
                trPtoSal.Visible = false; //20180307 rq010-18
                trRuta.Visible = false;
                trMercado.Visible = true;
                trRolV.Visible = false;
                trOperadorV.Visible = false;
                trRolC.Visible = false;
                trOperadorC.Visible = false;
                trDemanda.Visible = false;
                trSector.Visible = false;
                break;
            case "2":
                trFuente.Visible = true;
                trSubasta.Visible = true;
                trRueda.Visible = true;
                trModalidad.Visible = true;
                trPunto.Visible = false;
                trPtoSal.Visible = false; //20180307 rq010-18
                trRuta.Visible = true;
                trMercado.Visible = true;
                trRolV.Visible = false;
                trOperadorV.Visible = false;
                trRolC.Visible = false;
                trOperadorC.Visible = false;
                trDemanda.Visible = false;
                trSector.Visible = false;
                break;
            case "3":
                trFuente.Visible = false;
                trSubasta.Visible = false;
                trRueda.Visible = false;
                trModalidad.Visible = false;
                trPunto.Visible = false;
                trPtoSal.Visible = false; //20180307 rq010-18
                trRuta.Visible = false;
                trMercado.Visible = true;
                trRolV.Visible = true;
                trOperadorV.Visible = true;
                trRolC.Visible = true;
                trOperadorC.Visible = true;
                trDemanda.Visible = false;
                trSector.Visible = false;
                break;
            case "4":
                trFuente.Visible = false;
                trSubasta.Visible = false;
                trRueda.Visible = false;
                trModalidad.Visible = false;
                trPunto.Visible = false;
                trPtoSal.Visible = false; //20180307 rq010-18
                trRuta.Visible = false;
                trMercado.Visible = false;
                trRolV.Visible = true;
                trOperadorV.Visible = true;
                trRolC.Visible = true;
                trOperadorC.Visible = true;
                trDemanda.Visible = false;
                trSector.Visible = false;
                break;
            case "5":
                trFuente.Visible = false;
                trSubasta.Visible = false;
                trRueda.Visible = false;
                trModalidad.Visible = true;
                trPunto.Visible = false;
                trPtoSal.Visible = false; //20180307 rq010-18
                trRuta.Visible = false;
                trMercado.Visible = false;
                trRolV.Visible = true;
                trOperadorV.Visible = true;
                trRolC.Visible = true;
                trOperadorC.Visible = true;
                trDemanda.Visible = true;
                trSector.Visible = true;
                break;
            case "6":
                trFuente.Visible = false;
                trSubasta.Visible = false;
                trRueda.Visible = false;
                trModalidad.Visible = true;
                trPunto.Visible = false;
                trPtoSal.Visible = false; //20180307 rq010-18
                trRuta.Visible = false;
                trMercado.Visible = false;
                trRolV.Visible = true;
                trOperadorV.Visible = true;
                trRolC.Visible = true;
                trOperadorC.Visible = true;
                trDemanda.Visible = true;
                trSector.Visible = true;
                break;
            //20180307 rq010-11
            case "7":
                trFuente.Visible = false;
                trSubasta.Visible = false;
                trRueda.Visible = false;
                trModalidad.Visible = true;
                trPunto.Visible = false;
                trPtoSal.Visible = true;
                trRuta.Visible = true;
                trMercado.Visible = false;
                trRolV.Visible = false;
                trOperadorV.Visible = false;
                trRolC.Visible = false;
                trOperadorC.Visible = false;
                trDemanda.Visible = false;
                trSector.Visible = false;
                break;
            case "8":
                trFuente.Visible = false;
                trSubasta.Visible = false;
                trRueda.Visible = false;
                trModalidad.Visible = false;
                trPunto.Visible = false;
                trPtoSal.Visible = false;
                trRuta.Visible = false;
                trMercado.Visible = false;
                trRolV.Visible = true;
                trOperadorV.Visible = true;
                trRolC.Visible = true;
                trOperadorC.Visible = true;
                trDemanda.Visible = false;
                trSector.Visible = false;
                break;
            case "9":
                trFuente.Visible = false;
                trSubasta.Visible = false;
                trRueda.Visible = false;
                trModalidad.Visible = true;
                trPunto.Visible = false;
                trPtoSal.Visible = false;
                trRuta.Visible = false;
                trMercado.Visible = false;
                trRolV.Visible = true;
                trOperadorV.Visible = true;
                trRolC.Visible = true;
                trOperadorC.Visible = true;
                trDemanda.Visible = true;
                trSector.Visible = true;
                break;
                //20180307 fin rq010-11
        }
        //20180307 rq010-11
        DdlRolCompra.Items.Clear();
        DdlOperadorV.Items.Clear();
        DdlRolVenta.Items.Clear();
        DdlOperadorC.Items.Clear();
        lConexion.Abrir();
        if (ddlReporte.SelectedValue == "8" || ddlReporte.SelectedValue == "9")
        {
            LlenarControles(lConexion.gObjConexion, DdlRolVenta, "m_tipos_operador tpo, m_operador_punta_mayorista may", " tpo.estado = 'A'  and tpo.codigo_tipo_operador = may.codigo_tipo_operador and may.punta_contrato ='V' and may.estado ='A' order by descripcion", 2, 1);
            LlenarControles1(lConexion.gObjConexion, DdlOperadorV, "m_operador ope,m_tipos_operador tpo, m_operador_punta_mayorista may", " ope.estado = 'A'  and  ope.tipo_operador = tpo.sigla and tpo.codigo_tipo_operador = may.codigo_tipo_operador and may.punta_contrato ='V' and may.estado ='A' order by razon_social", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlRolCompra, "m_tipos_operador tpo, m_operador_punta_mayorista may", " tpo.estado = 'A'  and tpo.codigo_tipo_operador = may.codigo_tipo_operador and may.punta_contrato ='C' and may.estado ='A' order by descripcion", 2, 1);
            LlenarControles1(lConexion.gObjConexion, DdlOperadorC, "m_operador ope,m_tipos_operador tpo, m_operador_punta_mayorista may", " ope.estado = 'A'  and  ope.tipo_operador = tpo.sigla and tpo.codigo_tipo_operador = may.codigo_tipo_operador and may.punta_contrato ='C' and may.estado ='A' order by razon_social", 0, 1);
        }
        else
        {
            LlenarControles(lConexion.gObjConexion, DdlRolVenta, "m_tipos_operador tpo", " tpo.estado = 'A'   order by descripcion", 2, 1);
            LlenarControles1(lConexion.gObjConexion, DdlOperadorV, "m_operador ope", " ope.estado = 'A' order by razon_social", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlRolCompra, "m_tipos_operador tpo", " tpo.estado = 'A'  order by descripcion", 2, 1);
            LlenarControles1(lConexion.gObjConexion, DdlOperadorC, "m_operador ope", " ope.estado = 'A' order by razon_social", 0, 1);
        }
        lConexion.Cerrar();
        //20180307 fin rq010-11
    }
    protected void DdlSubasta_SelectedIndexChanged(object sender, EventArgs e)
    {
        DdlRueda.Items.Clear();
        lConexion.Abrir();
        LlenarControles(lConexion.gObjConexion, DdlRueda, "m_tipos_rueda", " codigo_tipo_subasta = " + DdlSubasta.SelectedValue + " and estado = 'A' order by descripcion", 0, 1);
        lConexion.Cerrar();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20160706 sector de conusmo
    protected void DdlDemanda_SelectedIndexChanged(object sender, EventArgs e)
    {
        lConexion.Abrir();
        DdlSector.Items.Clear();
        LlenarControles(lConexion.gObjConexion, DdlSector, "m_sector_consumo sec, m_demanda_sector dem ", " sec.codigo_sector_consumo = dem.codigo_sector_consumo and dem.codigo_tipo_demanda = " + DdlDemanda.SelectedValue + " and sec.estado = 'A'  and dem.estado ='A' and dem.ind_uso ='T' order by sec.descripcion", 0, 1); //20160706 sector de conusmo
        lConexion.Cerrar();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20180307 fin rq010-11
    protected void DdlRolVenta_SelectedIndexChanged(object sender, EventArgs e)
    {
        DdlOperadorV.Items.Clear();
        lConexion.Abrir();
        if (DdlRolVenta.SelectedValue != "0")
        {
            LlenarControles1(lConexion.gObjConexion, DdlOperadorV, "m_operador ope", " ope.estado = 'A'  and  ope.tipo_operador = '" + DdlRolVenta.SelectedValue + "'  order by razon_social", 0, 1);
        }
        else
        {
            if (ddlReporte.SelectedValue == "8" || ddlReporte.SelectedValue == "9")
            {
                LlenarControles1(lConexion.gObjConexion, DdlOperadorV, "m_operador ope,m_tipos_operador tpo, m_operador_punta_mayorista may", " ope.estado = 'A'  and  ope.tipo_operador = tpo.sigla and tpo.codigo_tipo_operador = may.codigo_tipo_operador and may.punta_contrato ='V' and may.estado ='A' order by razon_social", 0, 1);
            }
            else
            {
                LlenarControles1(lConexion.gObjConexion, DdlOperadorV, "m_operador ope", " ope.estado = 'A' order by razon_social", 0, 1);
            }
        }
        lConexion.Cerrar();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20180307 fin rq010-11
    protected void DdlRolCompra_SelectedIndexChanged(object sender, EventArgs e)
    {
        DdlOperadorC.Items.Clear();
        lConexion.Abrir();
        if (DdlRolCompra.SelectedValue != "0")
        {
            LlenarControles1(lConexion.gObjConexion, DdlOperadorC, "m_operador ope", " ope.estado = 'A'  and  ope.tipo_operador = '" + DdlRolCompra.SelectedValue + "'  order by razon_social", 0, 1);
        }
        else
        {
            if (ddlReporte.SelectedValue == "8" || ddlReporte.SelectedValue == "9")
            {
                LlenarControles1(lConexion.gObjConexion, DdlOperadorC, "m_operador ope,m_tipos_operador tpo, m_operador_punta_mayorista may", " ope.estado = 'A'  and  ope.tipo_operador = tpo.sigla and tpo.codigo_tipo_operador = may.codigo_tipo_operador and may.punta_contrato ='C' and may.estado ='A' order by razon_social", 0, 1);
            }
            else
            {
                LlenarControles1(lConexion.gObjConexion, DdlOperadorC, "m_operador ope", " ope.estado = 'A' order by razon_social", 0, 1);
            }
        }
        lConexion.Cerrar();
    }

}