﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Informes
{
    public partial class frm_ProgDefSum : Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Consulta de Programación Definitiva de Suministro";
        clConexion lConexion = null;
        DataSet lds = new DataSet();
        SqlDataAdapter lsqldata = new SqlDataAdapter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo;
            lConexion = new clConexion(goInfo);
            //Se agrega el comportamiento del botón
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.FiltrarOnclick += imbConsultar_Click;

            if (IsPostBack) return;
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, DdlPozo, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlComprador, "m_operador", " estado = 'A' And codigo_operador != 0 order by codigo_operador", 0, 4);
            LlenarControles(lConexion.gObjConexion, DdlVendedor, "m_operador", " estado = 'A' And codigo_operador != 0 order by codigo_operador", 0, 4);
            lConexion.Cerrar();

            //Titulo
            Master.Titulo = "Registros Operativos";

            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar };
            buttons.Inicializar(botones: botones);

            if (goInfo.cod_comisionista == "0") return;

            try
            {
                DdlVendedor.SelectedValue = goInfo.cod_comisionista;
                DdlVendedor.Enabled = false;
            }
            catch
            {
                buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
            }
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, int liIndiceLlave, int liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                var lItem1 = new ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"] + "-" + lLector["razon_social"];
                }
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                var lblMensaje = new StringBuilder();
                DateTime ldFecha = DateTime.Now;

                if (TxtFechaIni.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Append("Formato Inválido en el Campo Fecha Inicial. <br>");
                    }
                }
                if (TxtFechaFin.Text.Trim().Length > 0)
                {
                    try
                    {
                        if (TxtFechaIni.Text.Trim().Length > 0)
                        {
                            if (ldFecha > Convert.ToDateTime(TxtFechaFin.Text))
                            {
                                lblMensaje.Append("La Fecha Final NO puede ser Menor que la Fecha de Inicial. <br>");
                            }
                        }
                        else
                        {
                            lblMensaje.Append("Debe digitar la fecha inicial antes que la final. <br>");
                        }
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Append("Formato Inválido en el Campo Fecha Final. <br>");
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    Toastr.Error(this, lblMensaje.ToString());
                    return;
                }

                if (DdlReporte.SelectedValue == "A")
                    dtgMaestro.Columns[2].HeaderText = "Fecha de Gas D-1";
                //20190318 rq017-19
                if (DdlReporte.SelectedValue == "D")
                    dtgMaestro.Columns[2].HeaderText = "Fecha de Gas día de Gas";
                //20190318 rq017-19
                if (DdlReporte.SelectedValue == "P")
                    dtgMaestro.Columns[2].HeaderText = "Fecha de Gas posterior a re-nominaciones D+1";

                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetProgDefSum";
                lComando.Parameters.Add("@P_tipo_reporte", SqlDbType.VarChar).Value = DdlReporte.SelectedValue;
                lComando.Parameters.Add("@P_fecha_inicial", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                lComando.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaFin.Text.Trim() == "" ? TxtFechaIni.Text.Trim() : TxtFechaFin.Text.Trim();
                lComando.Parameters.Add("@P_operador_comprador", SqlDbType.Int).Value = DdlComprador.SelectedValue;
                lComando.Parameters.Add("@P_operador_carga", SqlDbType.Int).Value = DdlVendedor.SelectedValue;
                lComando.Parameters.Add("@P_codigo_pozo", SqlDbType.Int).Value = DdlPozo.SelectedValue;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgMaestro.DataSource = lds;
                dtgMaestro.DataBind();
                dtgMaestroExcel.DataSource = lds;
                dtgMaestroExcel.DataBind();
                buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se pudo Generar el informe.! " + ex.Message);
            }
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string lsNombreArchivo = goInfo.Usuario + "InfIdRueda" + DateTime.Now + ".xls";
            string lstitulo_informe = "";
            string lsTituloParametros = "";
            try
            {
                if (dtgMaestro.Columns[2].HeaderText == "Fecha de Gas D-1")
                    lstitulo_informe = "Programación Definitiva Suministro";
                //20190318 rq017-19
                if (dtgMaestro.Columns[2].HeaderText == "Fecha de Gas día de Gas")
                    lstitulo_informe = "Programación Definitiva Suministro día de gas";
                //20190318 rq017-19
                if (dtgMaestro.Columns[2].HeaderText == "Fecha de Gas posterior a re-nominaciones D+1")
                    lstitulo_informe = "Programación Definitiva Suministro Posterior a Re-Nominación";
                if (TxtFechaIni.Text.Trim().Length > 0)
                    lsTituloParametros += " - Fecha Inicial: " + TxtFechaIni.Text.Trim();
                if (TxtFechaFin.Text.Trim().Length > 0)
                    lsTituloParametros += " - Fecha Final: " + TxtFechaFin.Text.Trim();
                if (DdlComprador.SelectedValue != "0")
                    lsTituloParametros += "  - Comprador: " + DdlComprador.SelectedItem;
                if (DdlVendedor.SelectedValue != "0")
                    lsTituloParametros += "  - Vendedor: " + DdlVendedor.SelectedItem;
                if (DdlPozo.SelectedValue != "0")
                    lsTituloParametros += "  - Punto SNT: " + DdlPozo.SelectedItem;
                decimal ldCapacidad = 0;
                StringBuilder lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                dtgMaestroExcel.Visible = true;
                dtgMaestroExcel.EnableViewState = false;
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                lform.Controls.Add(dtgMaestroExcel);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());
                dtgMaestroExcel.Visible = false;
                Response.End();
                lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pudo Generar el Excel.!" + ex.Message);
            }
        }
    }
}