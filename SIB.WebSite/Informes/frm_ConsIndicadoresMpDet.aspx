﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ConsIndicadoresMpDet.aspx.cs"
    Inherits="Informes_frm_ConsIndicadoresMpDet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                        
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">  Variables:</label>
                         <asp:Label ID="lblVariable" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name"> Periodicidad</label>
                         <asp:Label ID="lblPeriodo" runat="server"></asp:Label>

                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name"> Observaciones de Publicación</label>
                           <%--<asp:Label ID="lblObs" runat="server" ></asp:Label>--%>
                            <asp:TextBox ID="lblObs" type="Text" runat="server"  ></asp:TextBox>
                        </div>
                    </div>

                
                </div>
            </div>
        </div>
    </div>
   
</asp:Content>
