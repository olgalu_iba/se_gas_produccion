﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_tokenPendiente.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Informes_frm_tokenPendiente" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />


            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Subasta</label>
                            <asp:DropDownList ID="ddlSubasta" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Tipo Rueda</label>
                            <asp:DropDownList ID="ddlTipoRueda" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Contrato</label>
                           
                            <asp:TextBox ID="TxtFechaIni" runat="server" Width="100%" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <label for="Name">Número Rueda</label>
                        <asp:TextBox ID="TxtNoRueda" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control"></asp:TextBox>

                    </div>
                
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="form-group">
                        <label for="Name">Operador</label>
                        <asp:DropDownList ID="ddlOperador" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="form-group">
                        <label for="Name">Id Registro</label>
                        <asp:TextBox ID="TxtId" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FtebTxtId" runat="server" TargetControlID="TxtId"
                            FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="form-group">
                        <label for="Name">Número Operación</label>
                        <asp:DropDownList ID="ddlPuntoSnt" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="form-group">
                        <label for="Name">Operación</label>
                        <asp:TextBox ID="TxtOperacion" runat="server" Width="100%" CssClass="form-control" ValidationGroup="detalle"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FtebTxtOperacion" runat="server" TargetControlID="TxtOperacion"
                            FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="form-group">
                        <label for="Name">Estado</label>
                        <asp:DropDownList ID="ddlEstado" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server" >
                        </asp:DropDownList>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TxtOperacion"
                            FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                    </div>
                </div>
            </div>
                    <table border="0" align="center" cellpadding="3" cellspacing="2" width="90%" runat="server"
            id="tblMensaje">

            <tr>
                <td align="center">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    <div  runat="server"
        id="tblGrilla" visible="false">
        
               <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                    <asp:DataGrid ID="dtgMaestro"  runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table-bordered" AllowPaging="false"
                        PagerStyle-HorizontalAlign="Center"  OnEditCommand="dtgMaestro_EditCommand">
                     
                        <Columns>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                            <asp:BoundColumn DataField="codigo_cont_firma" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_contrato" HeaderText="Fecha Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_venta" HeaderText="Operador Venta" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="200px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_compra" HeaderText="Operador Compra" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="200px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="tipo_rueda" HeaderText="Tipo  Rueda" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="200px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="descripcion_estado" HeaderText="estado certificado"></asp:BoundColumn>
                            <asp:BoundColumn DataField="estado_cert" Visible="false"></asp:BoundColumn>
                        </Columns>
                        
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </div>
         
    </div>
        </div>
    </div>
  

    
    
</asp:Content>
