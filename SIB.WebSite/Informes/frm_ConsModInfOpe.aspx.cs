﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;
using System.Text;
using Segas.Web.Elements;

public partial class Informes_frm_ConsModInfOpe : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Consulta de modificaciones de información operativa "; //20180126 rq107-16
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        //Se agrega el comportamiento del botón
        buttons.ExportarExcelOnclick += ImgExcel_Click;
        buttons.FiltrarOnclick += imbConsultar_Click;

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlReporte, "m_reporte_inf_ope", " 1=1 order by codigo_reporte", 0, 1);
            lConexion.Cerrar();
            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar };
            buttons.Inicializar(botones: botones);
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, EventArgs e)
    {
        bool Error = false; 
       
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF = DateTime.Now;
        if (ddlReporte.SelectedValue == "0")
        {
            Toastr.Warning(this, "Debe seleccionar el tipo de información. <br>");
            Error = true;

        }
        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Formato Inválido en el Campo Fecha Inicial. <br>");
                Error = true;

            }
        }
        else
        {
            Toastr.Warning(this, "Debe digitar la Fecha Inicial. <br>");
            Error = true;
        }
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
                if (TxtFechaIni.Text.Trim().Length > 0 && ldFechaF < ldFechaI)
                {
                    Toastr.Warning(this, "La Fecha Final NO puede ser Menor que la Fecha Inicial. <br>");
                    Error = true;

                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Formato Inválido en el Campo Fecha Final. <br>");
                Error = true;

            }
        }
        else
        {
            Toastr.Warning(this, "Debe digitar la Fecha Final. <br>");
            Error = true;

        }
        //if (TxtOperacionIni.Text == "" && TxtOperacionFin.Text != "")
        //    lblMensaje.Text = "Debe digitar la operación inicial antes que la final<br>";
        if (!Error)
        {
            lblTipoIinf.Text = ddlReporte.SelectedItem.Text;
            tblGrilla.Visible = true;
            dtgMaestro1.Visible = false;
            dtgMaestro2.Visible = false;
            dtgMaestro3.Visible = false;
            dtgMaestro4.Visible = false;
            dtgMaestro5.Visible = false;
            dtgMaestro6.Visible = false;
            dtgMaestro7.Visible = false;
            dtgMaestro8.Visible = false;
            dtgMaestro9.Visible = false;
            dtgMaestro10.Visible = false;
            //20190306 rq013-19
            dtgMaestro11.Visible = false;
            dtgMaestro12.Visible = false;
            dtgMaestro13.Visible = false;
            dtgMaestro14.Visible = false;
            //20190306 fin rq013-19
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetConsModInfOpe";
                lComando.Parameters.Add("@P_codigo_reporte", SqlDbType.Int).Value = ddlReporte.SelectedValue;
                lComando.Parameters.Add("@P_fecha_inicial", SqlDbType.VarChar).Value = TxtFechaIni.Text;
                lComando.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaFin.Text;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                switch (ddlReporte.SelectedValue)
                {
                    case "1":
                        dtgMaestro1.DataSource = lds;
                        dtgMaestro1.DataBind();
                        dtgMaestro1.Visible = true;
                        break;
                    case "2":
                        dtgMaestro2.DataSource = lds;
                        dtgMaestro2.DataBind();
                        dtgMaestro2.Visible = true;
                        break;
                    case "3":
                        dtgMaestro3.DataSource = lds;
                        dtgMaestro3.DataBind();
                        dtgMaestro3.Visible = true;
                        break;
                    case "4":
                        dtgMaestro4.DataSource = lds;
                        dtgMaestro4.DataBind();
                        dtgMaestro4.Visible = true;
                        break;
                    case "5":
                        dtgMaestro5.DataSource = lds;
                        dtgMaestro5.DataBind();
                        dtgMaestro5.Visible = true;
                        break;
                    case "6":
                        dtgMaestro6.DataSource = lds;
                        dtgMaestro6.DataBind();
                        dtgMaestro6.Visible = true;
                        break;
                    case "7":
                        dtgMaestro7.DataSource = lds;
                        dtgMaestro7.DataBind();
                        dtgMaestro7.Visible = true;
                        break;
                    case "8":
                        dtgMaestro8.DataSource = lds;
                        dtgMaestro8.DataBind();
                        dtgMaestro8.Visible = true;
                        break;
                    case "9":
                        dtgMaestro9.DataSource = lds;
                        dtgMaestro9.DataBind();
                        dtgMaestro9.Visible = true;
                        break;
                    case "10":
                        dtgMaestro10.DataSource = lds;
                        dtgMaestro10.DataBind();
                        dtgMaestro10.Visible = true;
                        break;
                    //20190306 rq013-19
                    case "11":
                        dtgMaestro11.DataSource = lds;
                        dtgMaestro11.DataBind();
                        dtgMaestro11.Visible = true;
                        break;
                    case "12":
                        dtgMaestro12.DataSource = lds;
                        dtgMaestro12.DataBind();
                        dtgMaestro12.Visible = true;
                        break;
                    case "13":
                        dtgMaestro13.DataSource = lds;
                        dtgMaestro13.DataBind();
                        dtgMaestro13.Visible = true;
                        break;
                    case "14":
                        dtgMaestro14.DataSource = lds;
                        dtgMaestro14.DataBind();
                        dtgMaestro14.Visible = true;
                        break;
                        //20190306 fin rq013-19
                }
                buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this,"No se Pudo Generar el Informe.! " + ex.Message.ToString());
                
            }
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, EventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "ConsModInfOpe" + DateTime.Now + ".xls";
        string lstitulo_informe = "Informe Modificación de información operativa: " + ddlReporte.SelectedItem.Text;
        string lsTituloParametros = "";
        try
        {
            lsTituloParametros = "Fecha: " + TxtFechaIni.Text + " - " + TxtFechaFin.Text;

            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            switch (ddlReporte.SelectedValue)
            {
                case "1":
                    dtgMaestro1.Columns[12].Visible = false; //20161124 modif inf ope //20190306 rq013-19
                    dtgMaestro1.EnableViewState = false;
                    lform.Controls.Add(dtgMaestro1);
                    break;
                case "2":
                    dtgMaestro2.Columns[9].Visible = false; //20161124 modif inf ope //20190306 rq013-19
                    dtgMaestro2.EnableViewState = false;
                    lform.Controls.Add(dtgMaestro2);
                    break;
                case "3":
                    dtgMaestro3.Columns[7].Visible = false; //20161124 modif inf ope //20190306 rq013-19
                    dtgMaestro3.EnableViewState = false;
                    lform.Controls.Add(dtgMaestro3);
                    break;
                case "4":
                    dtgMaestro4.Columns[16].Visible = false; //20161124 modif inf ope //20190306 rq013-19
                    dtgMaestro4.EnableViewState = false;
                    lform.Controls.Add(dtgMaestro4);
                    break;
                case "5":
                    dtgMaestro5.Columns[10].Visible = false; //20161124 modif inf ope //20190306 rq013-19
                    dtgMaestro5.EnableViewState = false;
                    lform.Controls.Add(dtgMaestro5);
                    break;
                case "6":
                    dtgMaestro6.Columns[9].Visible = false; //20161124 modif inf ope //20190306 rq013-19
                    dtgMaestro6.EnableViewState = false;
                    lform.Controls.Add(dtgMaestro6);
                    break;
                case "7":
                    dtgMaestro7.Columns[15].Visible = false; //20161124 modif inf ope //20190306 rq013-19
                    dtgMaestro7.EnableViewState = false;
                    lform.Controls.Add(dtgMaestro7);
                    break;
                case "8":
                    dtgMaestro8.Columns[12].Visible = false; //20161124 modif inf ope //20190306 rq013-19
                    dtgMaestro8.EnableViewState = false;
                    lform.Controls.Add(dtgMaestro8);
                    break;
                case "9":
                    dtgMaestro9.Columns[8].Visible = false; //20161124 modif inf ope //20190306 rq013-19
                    dtgMaestro9.EnableViewState = false;
                    lform.Controls.Add(dtgMaestro9);
                    break;
                case "10":
                    dtgMaestro10.Columns[15].Visible = false; //20161124 modif inf ope
                    dtgMaestro10.EnableViewState = false;
                    lform.Controls.Add(dtgMaestro10);
                    break;
                //20190306 rq013-19
                case "11":
                    dtgMaestro11.Columns[11].Visible = false;
                    dtgMaestro11.EnableViewState = false;
                    lform.Controls.Add(dtgMaestro11);
                    break;
                case "12":
                    dtgMaestro12.Columns[9].Visible = false;
                    dtgMaestro12.EnableViewState = false;
                    lform.Controls.Add(dtgMaestro12);
                    break;
                case "13":
                    dtgMaestro13.Columns[11].Visible = false;
                    dtgMaestro13.EnableViewState = false;
                    lform.Controls.Add(dtgMaestro13);
                    break;
                case "14":
                    dtgMaestro14.Columns[14].Visible = false;
                    dtgMaestro14.EnableViewState = false;
                    lform.Controls.Add(dtgMaestro14);
                    break;
                    //20190306 fin rq013-19
            }
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='7' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='7' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
            switch (ddlReporte.SelectedValue)
            {
                case "1":
                    dtgMaestro1.Columns[11].Visible = true; //20161124 modif inf ope
                    break;
                case "2":
                    dtgMaestro2.Columns[8].Visible = true; //20161124 modif inf ope
                    break;
                case "3":
                    dtgMaestro3.Columns[6].Visible = true; //20161124 modif inf ope
                    break;
                case "4":
                    dtgMaestro4.Columns[14].Visible = true; //20161124 modif inf ope
                    break;
                case "5":
                    dtgMaestro5.Columns[8].Visible = true; //20161124 modif inf ope
                    break;
                case "6":
                    dtgMaestro6.Columns[8].Visible = true; //20161124 modif inf ope
                    break;
                case "7":
                    dtgMaestro7.Columns[13].Visible = true; //20161124 modif inf ope
                    break;
                case "8":
                    dtgMaestro8.Columns[10].Visible = true; //20161124 modif inf ope
                    break;
                case "9":
                    dtgMaestro9.Columns[6].Visible = true; //20161124 modif inf ope
                    break;
                case "10":
                    dtgMaestro10.Columns[15].Visible = true; //20161124 modif inf ope
                    break;
                //20190306 rq013-19
                case "11":
                    dtgMaestro10.Columns[11].Visible = true;
                    break;
                case "12":
                    dtgMaestro10.Columns[9].Visible = true;
                    break;
                case "13":
                    dtgMaestro10.Columns[11].Visible = true;
                    break;
                case "14":
                    dtgMaestro10.Columns[14].Visible = true;
                    break;
                    //20190306 fin rq013-19
            }
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "No se Pudo Generar el Excel.! " + ex.Message.ToString());

        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro1_EditCommand(object source, DataGridCommandEventArgs e)
    {
        dtgDetalle1.Visible = true;
        dtgDetalle2.Visible = false;
        dtgDetalle3.Visible = false;
        dtgDetalle4.Visible = false;
        dtgDetalle5.Visible = false;
        dtgDetalle6.Visible = false;
        dtgDetalle7.Visible = false;
        dtgDetalle8.Visible = false;
        dtgDetalle9.Visible = false;
        dtgDetalle10.Visible = false;
        //20190306 rq013-19
        dtgDetalle11.Visible = false;
        dtgDetalle12.Visible = false;
        dtgDetalle13.Visible = false;
        dtgDetalle14.Visible = false;
        //20190306 fin rq013-19

        hdnRegistro.Value = this.dtgMaestro1.Items[e.Item.ItemIndex].Cells[0].Text;
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        SqlCommand lComando = new SqlCommand();
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandTimeout = 3600;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_GetConsModInfOpeDet";
        lComando.Parameters.Add("@P_codigo_reporte", SqlDbType.Int).Value = "1";
        lComando.Parameters.Add("@P_codigo_registro", SqlDbType.Int).Value = hdnRegistro.Value;
        lComando.ExecuteNonQuery();
        lsqldata.SelectCommand = lComando;
        lsqldata.Fill(lds);
        dtgDetalle1.DataSource = lds;
        dtgDetalle1.DataBind();
        lConexion.Cerrar();
        tblGrilla.Visible = false;
        tbDetalle.Visible = true;
        tblMensaje.Visible = false;
        //tblDatos.Visible = false;
        //REcorro la grilla para mostrar los cambios en negrilla
        string sFecha = "";
        string sOperador = "";
        string sPozo = "";
        string sCantidad = "";
        string sProducto = "";  //20161124 modif inf ope
        string sCampo = ""; //20161124 modif inf ope
        string sEstado = "";  //20161124 modif inf ope
        string sIngresa = "";  //20190306 rq013-19
        int liConta = 0;
        foreach (DataGridItem Grilla in this.dtgDetalle1.Items)
        {
            liConta++;
            if (liConta == 1)
            {
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sPozo = Grilla.Cells[4].Text;
                sProducto = Grilla.Cells[6].Text; //20161124 modif inf ope
                sCampo = Grilla.Cells[7].Text; //20161124 modif inf ope
                sCantidad = Grilla.Cells[9].Text; //20161124 modif inf ope
                sEstado = Grilla.Cells[10].Text; //20161124 modif inf ope
                sIngresa = Grilla.Cells[11].Text; //20190306 rq013-19
            }
            else
            {
                if (sFecha != Grilla.Cells[1].Text)
                    Grilla.Cells[1].Font.Bold = true;
                if (sOperador != Grilla.Cells[2].Text)
                {
                    Grilla.Cells[2].Font.Bold = true;
                    Grilla.Cells[3].Font.Bold = true;
                }
                if (sPozo != Grilla.Cells[4].Text)
                {
                    Grilla.Cells[4].Font.Bold = true;
                    Grilla.Cells[5].Font.Bold = true;
                }
                //20161124 modif inf ope
                if (sProducto != Grilla.Cells[6].Text)
                    Grilla.Cells[6].Font.Bold = true;
                //20161124 modif inf ope
                if (sCampo != Grilla.Cells[7].Text)
                {
                    Grilla.Cells[7].Font.Bold = true;
                    Grilla.Cells[8].Font.Bold = true;
                }
                if (sCantidad != Grilla.Cells[9].Text)  //20161124 modif inf ope
                    Grilla.Cells[9].Font.Bold = true;  //20161124 modif inf ope
                //20161124 modif inf ope
                if (sEstado != Grilla.Cells[10].Text)
                    Grilla.Cells[10].Font.Bold = true;
                //20190306 rq013-19
                if (sIngresa != Grilla.Cells[11].Text)
                    Grilla.Cells[11].Font.Bold = true;

                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sPozo = Grilla.Cells[4].Text;
                sProducto = Grilla.Cells[6].Text; //20161124 modif inf ope
                sCampo = Grilla.Cells[7].Text; //20161124 modif inf ope
                sCantidad = Grilla.Cells[9].Text; //20161124 modif inf ope
                sEstado = Grilla.Cells[10].Text; //20161124 modif inf ope
                sIngresa = Grilla.Cells[11].Text; //20190306 rq013-19
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro2_EditCommand(object source, DataGridCommandEventArgs e)
    {
        dtgDetalle1.Visible = false;
        dtgDetalle2.Visible = true;
        dtgDetalle3.Visible = false;
        dtgDetalle4.Visible = false;
        dtgDetalle5.Visible = false;
        dtgDetalle6.Visible = false;
        dtgDetalle7.Visible = false;
        dtgDetalle8.Visible = false;
        dtgDetalle9.Visible = false;
        dtgDetalle10.Visible = false;
        //20190306 rq013-19
        dtgDetalle11.Visible = false;
        dtgDetalle12.Visible = false;
        dtgDetalle13.Visible = false;
        dtgDetalle14.Visible = false;
        //20190306 fin rq013-19

        hdnRegistro.Value = this.dtgMaestro2.Items[e.Item.ItemIndex].Cells[0].Text;
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        SqlCommand lComando = new SqlCommand();
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandTimeout = 3600;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_GetConsModInfOpeDet";
        lComando.Parameters.Add("@P_codigo_reporte", SqlDbType.Int).Value = "2";
        lComando.Parameters.Add("@P_codigo_registro", SqlDbType.Int).Value = hdnRegistro.Value;
        lComando.ExecuteNonQuery();
        lsqldata.SelectCommand = lComando;
        lsqldata.Fill(lds);
        dtgDetalle2.DataSource = lds;
        dtgDetalle2.DataBind();
        lConexion.Cerrar();
        tblGrilla.Visible = false;
        tbDetalle.Visible = true;
        tblMensaje.Visible = false;
        //tblDatos.Visible = false;
        //REcorro la grilla para mostrar los cambios en negrilla
        string sFecha = "";
        string sOperador = "";
        string sCantidad = "";
        string sCampo = ""; //20161124 modif inf ope
        string sEstado = ""; //20161124 modif inf ope
        string sIngresa = ""; //20190306 rq013-19
        int liConta = 0;
        foreach (DataGridItem Grilla in this.dtgDetalle2.Items)
        {
            liConta++;
            if (liConta == 1)
            {
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sCampo = Grilla.Cells[4].Text;  //20161124 modif inf ope
                sCantidad = Grilla.Cells[6].Text;  //20161124 modif inf ope
                sEstado = Grilla.Cells[7].Text; //20161124 modif inf ope
                sEstado = Grilla.Cells[8].Text; //20190306 rq013-19
            }
            else
            {
                if (sFecha != Grilla.Cells[1].Text)
                    Grilla.Cells[1].Font.Bold = true;
                if (sOperador != Grilla.Cells[2].Text)
                {
                    Grilla.Cells[2].Font.Bold = true;
                    Grilla.Cells[3].Font.Bold = true;
                }
                //20161124 modif inf ope
                if (sCampo != Grilla.Cells[4].Text)
                {
                    Grilla.Cells[4].Font.Bold = true;
                    Grilla.Cells[5].Font.Bold = true;
                }
                if (sCantidad != Grilla.Cells[6].Text)  //20161124 modif inf ope
                    Grilla.Cells[6].Font.Bold = true; //20161124 modif inf ope
                //20161124 modif inf ope
                if (sEstado != Grilla.Cells[7].Text)
                    Grilla.Cells[7].Font.Bold = true;
                //20190306 rq013-19
                if (sIngresa != Grilla.Cells[8].Text)
                    Grilla.Cells[8].Font.Bold = true;
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sCampo = Grilla.Cells[4].Text;  //20161124 modif inf ope
                sCantidad = Grilla.Cells[6].Text;  //20161124 modif inf ope
                sEstado = Grilla.Cells[7].Text;  //20161124 modif inf ope
                sIngresa = Grilla.Cells[8].Text; //20190306 rq013-19
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro3_EditCommand(object source, DataGridCommandEventArgs e)
    {
        dtgDetalle1.Visible = false;
        dtgDetalle2.Visible = false;
        dtgDetalle3.Visible = true;
        dtgDetalle4.Visible = false;
        dtgDetalle5.Visible = false;
        dtgDetalle6.Visible = false;
        dtgDetalle7.Visible = false;
        dtgDetalle8.Visible = false;
        dtgDetalle9.Visible = false;
        dtgDetalle10.Visible = false;
        //20190306 rq013-19
        dtgDetalle11.Visible = false;
        dtgDetalle12.Visible = false;
        dtgDetalle13.Visible = false;
        dtgDetalle14.Visible = false;
        //20190306 fin rq013-19

        hdnRegistro.Value = this.dtgMaestro3.Items[e.Item.ItemIndex].Cells[0].Text;
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        SqlCommand lComando = new SqlCommand();
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandTimeout = 3600;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_GetConsModInfOpeDet";
        lComando.Parameters.Add("@P_codigo_reporte", SqlDbType.Int).Value = "3";
        lComando.Parameters.Add("@P_codigo_registro", SqlDbType.Int).Value = hdnRegistro.Value;
        lComando.ExecuteNonQuery();
        lsqldata.SelectCommand = lComando;
        lsqldata.Fill(lds);
        dtgDetalle3.DataSource = lds;
        dtgDetalle3.DataBind();
        lConexion.Cerrar();
        tblGrilla.Visible = false;
        tbDetalle.Visible = true;
        tblMensaje.Visible = false;
        //tblDatos.Visible = false;
        //REcorro la grilla para mostrar los cambios en negrilla
        string sFecha = "";
        string sOperador = "";
        string sCantidad = "";
        string sEstado = ""; //20161124 modif inf ope
        string sIngresa = ""; //20190306 rq013-19
        int liConta = 0;
        foreach (DataGridItem Grilla in this.dtgDetalle3.Items)
        {
            liConta++;
            if (liConta == 1)
            {
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sCantidad = Grilla.Cells[4].Text;
                sEstado = Grilla.Cells[5].Text; //2016112 modif inf ope
                sIngresa = Grilla.Cells[6].Text; //20190306 rq013-19
            }
            else
            {
                if (sFecha != Grilla.Cells[1].Text)
                    Grilla.Cells[1].Font.Bold = true;
                if (sOperador != Grilla.Cells[2].Text)
                {
                    Grilla.Cells[2].Font.Bold = true;
                    Grilla.Cells[3].Font.Bold = true;
                }
                if (sCantidad != Grilla.Cells[4].Text)
                {
                    Grilla.Cells[4].Font.Bold = true;
                }
                //20161124 modif inf ope
                if (sEstado != Grilla.Cells[5].Text)
                {
                    Grilla.Cells[5].Font.Bold = true;
                }
                //20190306 rq013-19
                if (sIngresa != Grilla.Cells[6].Text)
                {
                    Grilla.Cells[6].Font.Bold = true;
                }
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sCantidad = Grilla.Cells[4].Text;
                sEstado = Grilla.Cells[5].Text; //20161124 modif inf ope
                sIngresa = Grilla.Cells[6].Text; //20190306 rq013-19
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro4_EditCommand(object source, DataGridCommandEventArgs e)
    {
        dtgDetalle1.Visible = false;
        dtgDetalle2.Visible = false;
        dtgDetalle3.Visible = false;
        dtgDetalle4.Visible = true;
        dtgDetalle5.Visible = false;
        dtgDetalle6.Visible = false;
        dtgDetalle7.Visible = false;
        dtgDetalle8.Visible = false;
        dtgDetalle9.Visible = false;
        dtgDetalle10.Visible = false;
        //20190306 rq013-19
        dtgDetalle11.Visible = false;
        dtgDetalle12.Visible = false;
        dtgDetalle13.Visible = false;
        dtgDetalle14.Visible = false;
        //20190306 fin rq013-19

        hdnRegistro.Value = this.dtgMaestro4.Items[e.Item.ItemIndex].Cells[0].Text;
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        SqlCommand lComando = new SqlCommand();
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandTimeout = 3600;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_GetConsModInfOpeDet";
        lComando.Parameters.Add("@P_codigo_reporte", SqlDbType.Int).Value = "4";
        lComando.Parameters.Add("@P_codigo_registro", SqlDbType.Int).Value = hdnRegistro.Value;
        lComando.ExecuteNonQuery();
        lsqldata.SelectCommand = lComando;
        lsqldata.Fill(lds);
        dtgDetalle4.DataSource = lds;
        dtgDetalle4.DataBind();
        lConexion.Cerrar();
        tblGrilla.Visible = false;
        tbDetalle.Visible = true;
        tblMensaje.Visible = false;
        //tblDatos.Visible = false;
        //REcorro la grilla para mostrar los cambios en negrilla
        string sFecha = "";
        string sOperador = "";
        string sTramo = "";
        string sPunto = "";
        string sRemitente = "";
        string sCantidad = "";
        string sTransf = "";  //20161124 modif inf ope
        string sEstado = "";  //20161124 modif inf ope
        string sCausa = "";  //20190306 rq013-19
        int liConta = 0;
        foreach (DataGridItem Grilla in this.dtgDetalle4.Items)
        {
            liConta++;
            if (liConta == 1)
            {
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sTramo = Grilla.Cells[4].Text;
                sPunto = Grilla.Cells[6].Text;
                sRemitente = Grilla.Cells[8].Text;
                sTransf = Grilla.Cells[10].Text; //20161124 modif inf ope
                sCantidad = Grilla.Cells[12].Text; //20161124 modif inf ope
                sEstado = Grilla.Cells[13].Text; //20161124 modif inf ope
                sCausa = Grilla.Cells[14].Text; //20190306 rq013-19
            }
            else
            {
                if (sFecha != Grilla.Cells[1].Text)
                    Grilla.Cells[1].Font.Bold = true;
                if (sOperador != Grilla.Cells[2].Text)
                {
                    Grilla.Cells[2].Font.Bold = true;
                    Grilla.Cells[3].Font.Bold = true;
                }
                if (sTramo != Grilla.Cells[4].Text)
                {
                    Grilla.Cells[4].Font.Bold = true;
                    Grilla.Cells[5].Font.Bold = true;
                }
                if (sPunto != Grilla.Cells[6].Text)
                {
                    Grilla.Cells[6].Font.Bold = true;
                    Grilla.Cells[7].Font.Bold = true;
                }
                if (sRemitente != Grilla.Cells[8].Text)
                {
                    Grilla.Cells[8].Font.Bold = true;
                    Grilla.Cells[9].Font.Bold = true;
                }
                //20161124 modif inf ope
                if (sTransf != Grilla.Cells[10].Text)
                {
                    Grilla.Cells[10].Font.Bold = true;
                    Grilla.Cells[11].Font.Bold = true;
                }
                if (sCantidad != Grilla.Cells[12].Text) //20161124 modif inf ope
                    Grilla.Cells[12].Font.Bold = true; //20161124 modif inf ope
                //20161124 modif inf ope
                if (sEstado != Grilla.Cells[13].Text)
                    Grilla.Cells[13].Font.Bold = true;
                //20190306 rq013-19
                if (sCausa != Grilla.Cells[14].Text)
                {
                    Grilla.Cells[14].Font.Bold = true;
                    Grilla.Cells[15].Font.Bold = true;
                }
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sTramo = Grilla.Cells[4].Text;
                sPunto = Grilla.Cells[6].Text;
                sRemitente = Grilla.Cells[8].Text;
                sTransf = Grilla.Cells[10].Text; //20161124 modif inf ope
                sCantidad = Grilla.Cells[12].Text; //20161124 modif inf ope
                sEstado = Grilla.Cells[13].Text;  //20161124 modif inf ope
                sCausa = Grilla.Cells[14].Text;  //20190306 rq013-19
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro5_EditCommand(object source, DataGridCommandEventArgs e)
    {
        dtgDetalle1.Visible = false;
        dtgDetalle2.Visible = false;
        dtgDetalle3.Visible = false;
        dtgDetalle4.Visible = false;
        dtgDetalle5.Visible = true;
        dtgDetalle6.Visible = false;
        dtgDetalle7.Visible = false;
        dtgDetalle8.Visible = false;
        dtgDetalle9.Visible = false;
        dtgDetalle10.Visible = false;
        //20190306 rq013-19
        dtgDetalle11.Visible = false;
        dtgDetalle12.Visible = false;
        dtgDetalle13.Visible = false;
        dtgDetalle14.Visible = false;
        //20190306 fin rq013-19

        hdnRegistro.Value = this.dtgMaestro5.Items[e.Item.ItemIndex].Cells[0].Text;
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        SqlCommand lComando = new SqlCommand();
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandTimeout = 3600;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_GetConsModInfOpeDet";
        lComando.Parameters.Add("@P_codigo_reporte", SqlDbType.Int).Value = "5";
        lComando.Parameters.Add("@P_codigo_registro", SqlDbType.Int).Value = hdnRegistro.Value;
        lComando.ExecuteNonQuery();
        lsqldata.SelectCommand = lComando;
        lsqldata.Fill(lds);
        dtgDetalle5.DataSource = lds;
        dtgDetalle5.DataBind();
        lConexion.Cerrar();
        tblGrilla.Visible = false;
        tbDetalle.Visible = true;
        tblMensaje.Visible = false;
        //tblDatos.Visible = false;
        //REcorro la grilla para mostrar los cambios en negrilla
        string sFecha = "";
        string sOperador = "";
        string sPozo = "";
        string sCantidad = "";
        string sEstado = "";  //20161124 modif inf ope
        string sCausa = "";  //20190306 rq013-19
        int liConta = 0;
        foreach (DataGridItem Grilla in this.dtgDetalle5.Items)
        {
            liConta++;
            if (liConta == 1)
            {
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sPozo = Grilla.Cells[4].Text;
                sCantidad = Grilla.Cells[6].Text;
                sEstado = Grilla.Cells[7].Text;  //20161124 modif inf ope
                sCausa = Grilla.Cells[8].Text;  //20190306 rq013-19
            }
            else
            {
                if (sFecha != Grilla.Cells[1].Text)
                    Grilla.Cells[1].Font.Bold = true;
                if (sOperador != Grilla.Cells[2].Text)
                {
                    Grilla.Cells[2].Font.Bold = true;
                    Grilla.Cells[3].Font.Bold = true;
                }
                if (sPozo != Grilla.Cells[4].Text)
                {
                    Grilla.Cells[4].Font.Bold = true;
                    Grilla.Cells[5].Font.Bold = true;
                }
                if (sCantidad != Grilla.Cells[6].Text)
                    Grilla.Cells[6].Font.Bold = true;
                //20161124 modif inf ope
                if (sEstado != Grilla.Cells[7].Text)
                    Grilla.Cells[7].Font.Bold = true;
                //20190306 rq013-19
                if (sCausa != Grilla.Cells[8].Text)
                {
                    Grilla.Cells[8].Font.Bold = true;
                    Grilla.Cells[9].Font.Bold = true;
                }
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sPozo = Grilla.Cells[4].Text;
                sCantidad = Grilla.Cells[6].Text;
                sEstado = Grilla.Cells[7].Text; //20161124 modif inf ope
                sCausa = Grilla.Cells[8].Text; //20190306 rq013-19
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro6_EditCommand(object source, DataGridCommandEventArgs e)
    {
        dtgDetalle1.Visible = false;
        dtgDetalle2.Visible = false;
        dtgDetalle3.Visible = false;
        dtgDetalle4.Visible = false;
        dtgDetalle5.Visible = false;
        dtgDetalle6.Visible = true;
        dtgDetalle7.Visible = false;
        dtgDetalle8.Visible = false;
        dtgDetalle9.Visible = false;
        dtgDetalle10.Visible = false;
        //20190306 rq013-19
        dtgDetalle11.Visible = false;
        dtgDetalle12.Visible = false;
        dtgDetalle13.Visible = false;
        dtgDetalle14.Visible = false;
        //20190306 fin rq013-19

        hdnRegistro.Value = this.dtgMaestro6.Items[e.Item.ItemIndex].Cells[0].Text;
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        SqlCommand lComando = new SqlCommand();
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandTimeout = 3600;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_GetConsModInfOpeDet";
        lComando.Parameters.Add("@P_codigo_reporte", SqlDbType.Int).Value = "6";
        lComando.Parameters.Add("@P_codigo_registro", SqlDbType.Int).Value = hdnRegistro.Value;
        lComando.ExecuteNonQuery();
        lsqldata.SelectCommand = lComando;
        lsqldata.Fill(lds);
        dtgDetalle6.DataSource = lds;
        dtgDetalle6.DataBind();
        lConexion.Cerrar();
        tblGrilla.Visible = false;
        tbDetalle.Visible = true;
        tblMensaje.Visible = false;
        //tblDatos.Visible = false;
        //REcorro la grilla para mostrar los cambios en negrilla
        string sFecha = "";
        string sOperador = "";
        string sPozo = "";
        string sCantidad = "";
        string sEstado = ""; //20161124 modif inf ope
        string sIngresa = ""; //20190306 rq013-19
        int liConta = 0;
        foreach (DataGridItem Grilla in this.dtgDetalle6.Items)
        {
            liConta++;
            if (liConta == 1)
            {
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sPozo = Grilla.Cells[4].Text;
                sCantidad = Grilla.Cells[6].Text;
                sEstado = Grilla.Cells[7].Text;  //20161124 modif inf ope
                sIngresa = Grilla.Cells[8].Text;  //20190306 rq013-19
            }
            else
            {
                if (sFecha != Grilla.Cells[1].Text)
                    Grilla.Cells[1].Font.Bold = true;
                if (sOperador != Grilla.Cells[2].Text)
                {
                    Grilla.Cells[2].Font.Bold = true;
                    Grilla.Cells[3].Font.Bold = true;
                }
                if (sPozo != Grilla.Cells[4].Text)
                {
                    Grilla.Cells[4].Font.Bold = true;
                    Grilla.Cells[5].Font.Bold = true;
                }
                if (sCantidad != Grilla.Cells[6].Text)
                    Grilla.Cells[6].Font.Bold = true;
                //20161124 modif inf ope
                if (sEstado != Grilla.Cells[7].Text)
                    Grilla.Cells[7].Font.Bold = true;
                //20190306 rq013-19
                if (sIngresa != Grilla.Cells[8].Text)
                    Grilla.Cells[8].Font.Bold = true;
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sPozo = Grilla.Cells[4].Text;
                sCantidad = Grilla.Cells[6].Text;
                sEstado = Grilla.Cells[7].Text; //20161124 modif inf ope
                sIngresa = Grilla.Cells[8].Text; //20190306 rq013-19
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro7_EditCommand(object source, DataGridCommandEventArgs e)
    {
        dtgDetalle1.Visible = false;
        dtgDetalle2.Visible = false;
        dtgDetalle3.Visible = false;
        dtgDetalle4.Visible = false;
        dtgDetalle5.Visible = false;
        dtgDetalle6.Visible = false;
        dtgDetalle7.Visible = true;
        dtgDetalle8.Visible = false;
        dtgDetalle9.Visible = false;
        dtgDetalle10.Visible = false;
        //20190306 rq013-19
        dtgDetalle11.Visible = false;
        dtgDetalle12.Visible = false;
        dtgDetalle13.Visible = false;
        dtgDetalle14.Visible = false;
        //20190306 fin rq013-19

        hdnRegistro.Value = this.dtgMaestro7.Items[e.Item.ItemIndex].Cells[0].Text;
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        SqlCommand lComando = new SqlCommand();
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandTimeout = 3600;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_GetConsModInfOpeDet";
        lComando.Parameters.Add("@P_codigo_reporte", SqlDbType.Int).Value = "7";
        lComando.Parameters.Add("@P_codigo_registro", SqlDbType.Int).Value = hdnRegistro.Value;
        lComando.ExecuteNonQuery();
        lsqldata.SelectCommand = lComando;
        lsqldata.Fill(lds);
        dtgDetalle7.DataSource = lds;
        dtgDetalle7.DataBind();
        lConexion.Cerrar();
        tblGrilla.Visible = false;
        tbDetalle.Visible = true;
        tblMensaje.Visible = false;
        //tblDatos.Visible = false;
        //REcorro la grilla para mostrar los cambios en negrilla
        string sFecha = "";
        string sOperador = "";
        string sTramo = "";
        string sPunto = "";
        string sRemitente = "";
        string sContrato = "";
        string sCantidad = "";
        string sEstado = ""; //20161124 modif inf ope
        string sCausa = ""; //20190306 rq013-19
        int liConta = 0;
        foreach (DataGridItem Grilla in this.dtgDetalle7.Items)
        {
            liConta++;
            if (liConta == 1)
            {
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sTramo = Grilla.Cells[4].Text;
                sPunto = Grilla.Cells[6].Text;
                sRemitente = Grilla.Cells[8].Text;
                sContrato = Grilla.Cells[10].Text;
                sCantidad = Grilla.Cells[11].Text;
                sEstado = Grilla.Cells[12].Text; //20161124 modif inf ope
                sCausa = Grilla.Cells[13].Text;//20190306 rq013-19
            }
            else
            {
                if (sFecha != Grilla.Cells[1].Text)
                    Grilla.Cells[1].Font.Bold = true;
                if (sOperador != Grilla.Cells[2].Text)
                {
                    Grilla.Cells[2].Font.Bold = true;
                    Grilla.Cells[3].Font.Bold = true;
                }
                if (sTramo != Grilla.Cells[4].Text)
                {
                    Grilla.Cells[4].Font.Bold = true;
                    Grilla.Cells[5].Font.Bold = true;
                }
                if (sPunto != Grilla.Cells[6].Text)
                {
                    Grilla.Cells[6].Font.Bold = true;
                    Grilla.Cells[7].Font.Bold = true;
                }
                if (sRemitente != Grilla.Cells[8].Text)
                {
                    Grilla.Cells[8].Font.Bold = true;
                    Grilla.Cells[9].Font.Bold = true;
                }
                if (sContrato != Grilla.Cells[10].Text)
                {
                    Grilla.Cells[10].Font.Bold = true;
                }
                if (sCantidad != Grilla.Cells[11].Text)
                    Grilla.Cells[11].Font.Bold = true;
                //20161124 modif inf ope
                if (sEstado != Grilla.Cells[12].Text)
                    Grilla.Cells[12].Font.Bold = true;
                //20190306 rq013-19
                if (sCausa != Grilla.Cells[13].Text)
                {
                    Grilla.Cells[13].Font.Bold = true;
                    Grilla.Cells[14].Font.Bold = true;
                }
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sTramo = Grilla.Cells[4].Text;
                sPunto = Grilla.Cells[6].Text;
                sRemitente = Grilla.Cells[8].Text;
                sContrato = Grilla.Cells[10].Text;
                sCantidad = Grilla.Cells[11].Text;
                sEstado = Grilla.Cells[12].Text; //200161124 modif inf ope
                sCausa = Grilla.Cells[13].Text; //20190306 rq013-19
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro8_EditCommand(object source, DataGridCommandEventArgs e)
    {
        dtgDetalle1.Visible = false;
        dtgDetalle2.Visible = false;
        dtgDetalle3.Visible = false;
        dtgDetalle4.Visible = false;
        dtgDetalle5.Visible = false;
        dtgDetalle6.Visible = false;
        dtgDetalle7.Visible = false;
        dtgDetalle8.Visible = true;
        dtgDetalle9.Visible = false;
        dtgDetalle10.Visible = false;
        //20190306 rq013-19
        dtgDetalle11.Visible = false;
        dtgDetalle12.Visible = false;
        dtgDetalle13.Visible = false;
        dtgDetalle14.Visible = false;
        //20190306 fin rq013-19

        hdnRegistro.Value = this.dtgMaestro8.Items[e.Item.ItemIndex].Cells[0].Text;
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        SqlCommand lComando = new SqlCommand();
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandTimeout = 3600;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_GetConsModInfOpeDet";
        lComando.Parameters.Add("@P_codigo_reporte", SqlDbType.Int).Value = "8";
        lComando.Parameters.Add("@P_codigo_registro", SqlDbType.Int).Value = hdnRegistro.Value;
        lComando.ExecuteNonQuery();
        lsqldata.SelectCommand = lComando;
        lsqldata.Fill(lds);
        dtgDetalle8.DataSource = lds;
        dtgDetalle8.DataBind();
        lConexion.Cerrar();
        tblGrilla.Visible = false;
        tbDetalle.Visible = true;
        tblMensaje.Visible = false;
        //tblDatos.Visible = false;
        //REcorro la grilla para mostrar los cambios en negrilla
        string sFecha = "";
        string sOperador = "";
        string sPunto = "";
        string sTrans = "";
        string sCantidad = "";
        string sEstado = ""; //20161124 modif inf ope
        string sCausa = ""; //20190306 rq013-19
        int liConta = 0;
        foreach (DataGridItem Grilla in this.dtgDetalle8.Items)
        {
            liConta++;
            if (liConta == 1)
            {
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sPunto = Grilla.Cells[4].Text;
                sTrans = Grilla.Cells[6].Text;
                sCantidad = Grilla.Cells[8].Text;
                sEstado = Grilla.Cells[9].Text; //20161124 modif inf ope
                sCausa = Grilla.Cells[10].Text;//20190306 rq013-19
            }
            else
            {
                if (sFecha != Grilla.Cells[1].Text)
                    Grilla.Cells[1].Font.Bold = true;
                if (sOperador != Grilla.Cells[2].Text)
                {
                    Grilla.Cells[2].Font.Bold = true;
                    Grilla.Cells[3].Font.Bold = true;
                }
                if (sPunto != Grilla.Cells[4].Text)
                {
                    Grilla.Cells[4].Font.Bold = true;
                    Grilla.Cells[5].Font.Bold = true;
                }
                if (sTrans != Grilla.Cells[6].Text)
                {
                    Grilla.Cells[6].Font.Bold = true;
                    Grilla.Cells[7].Font.Bold = true;
                }
                if (sCantidad != Grilla.Cells[8].Text)
                    Grilla.Cells[8].Font.Bold = true;
                //20161124 modif inf ope
                if (sEstado != Grilla.Cells[9].Text)
                    Grilla.Cells[9].Font.Bold = true;
                //20190306 rq013-19
                if (sCausa != Grilla.Cells[10].Text)
                {
                    Grilla.Cells[10].Font.Bold = true;
                    Grilla.Cells[11].Font.Bold = true;
                }
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sPunto = Grilla.Cells[4].Text;
                sTrans = Grilla.Cells[6].Text;
                sCantidad = Grilla.Cells[8].Text;
                sEstado = Grilla.Cells[9].Text; //20161124 modif inf ope
                sCausa = Grilla.Cells[10].Text;//20190306 rq013-19
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro9_EditCommand(object source, DataGridCommandEventArgs e)
    {
        dtgDetalle1.Visible = false;
        dtgDetalle2.Visible = false;
        dtgDetalle3.Visible = false;
        dtgDetalle4.Visible = false;
        dtgDetalle5.Visible = false;
        dtgDetalle6.Visible = false;
        dtgDetalle7.Visible = false;
        dtgDetalle8.Visible = false;
        dtgDetalle9.Visible = true;
        dtgDetalle10.Visible = false;
        //20190306 rq013-19
        dtgDetalle11.Visible = false;
        dtgDetalle12.Visible = false;
        dtgDetalle13.Visible = false;
        dtgDetalle14.Visible = false;
        //20190306 fin rq013-19

        hdnRegistro.Value = this.dtgMaestro9.Items[e.Item.ItemIndex].Cells[0].Text;
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        SqlCommand lComando = new SqlCommand();
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandTimeout = 3600;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_GetConsModInfOpeDet";
        lComando.Parameters.Add("@P_codigo_reporte", SqlDbType.Int).Value = "9";
        lComando.Parameters.Add("@P_codigo_registro", SqlDbType.Int).Value = hdnRegistro.Value;
        lComando.ExecuteNonQuery();
        lsqldata.SelectCommand = lComando;
        lsqldata.Fill(lds);
        dtgDetalle9.DataSource = lds;
        dtgDetalle9.DataBind();
        lConexion.Cerrar();
        tblGrilla.Visible = false;
        tbDetalle.Visible = true;
        tblMensaje.Visible = false;
        //tblDatos.Visible = false;
        //REcorro la grilla para mostrar los cambios en negrilla
        string sFecha = "";
        string sOperador = "";
        string sCantidad = "";
        string sEstado = "";  //20161124 modif inf ope
        string sCausa = "";//20190306 rq013-19
        int liConta = 0;
        foreach (DataGridItem Grilla in this.dtgDetalle9.Items)
        {
            liConta++;
            if (liConta == 1)
            {
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sCantidad = Grilla.Cells[4].Text;
                sEstado = Grilla.Cells[5].Text; //20161124 modif inf ope
                sCausa = Grilla.Cells[6].Text;//20190306 rq013-19
            }
            else
            {
                if (sFecha != Grilla.Cells[1].Text)
                    Grilla.Cells[1].Font.Bold = true;
                if (sOperador != Grilla.Cells[2].Text)
                {
                    Grilla.Cells[2].Font.Bold = true;
                    Grilla.Cells[3].Font.Bold = true;
                }
                if (sCantidad != Grilla.Cells[4].Text)
                    Grilla.Cells[4].Font.Bold = true;
                //20161124 modif inf ope
                if (sEstado != Grilla.Cells[5].Text)
                    Grilla.Cells[5].Font.Bold = true;
                //20190306 rq013-19
                sFecha = Grilla.Cells[1].Text;
                if (sCausa != Grilla.Cells[6].Text)
                {
                    Grilla.Cells[6].Font.Bold = true;
                    Grilla.Cells[7].Font.Bold = true;
                }
                sOperador = Grilla.Cells[2].Text;
                sCantidad = Grilla.Cells[4].Text;
                sEstado = Grilla.Cells[5].Text; //20161124 modif inf ope
                sCausa = Grilla.Cells[6].Text; //20190306 rq013-19
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro10_EditCommand(object source, DataGridCommandEventArgs e)
    {
        dtgDetalle1.Visible = false;
        dtgDetalle2.Visible = false;
        dtgDetalle3.Visible = false;
        dtgDetalle4.Visible = false;
        dtgDetalle5.Visible = false;
        dtgDetalle6.Visible = false;
        dtgDetalle7.Visible = false;
        dtgDetalle8.Visible = false;
        dtgDetalle9.Visible = false;
        dtgDetalle10.Visible = true;
        //20190306 rq013-19
        dtgDetalle11.Visible = false;
        dtgDetalle12.Visible = false;
        dtgDetalle13.Visible = false;
        dtgDetalle14.Visible = false;
        //20190306 fin rq013-19

        hdnRegistro.Value = this.dtgMaestro10.Items[e.Item.ItemIndex].Cells[0].Text;
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        SqlCommand lComando = new SqlCommand();
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandTimeout = 3600;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_GetConsModInfOpeDet";
        lComando.Parameters.Add("@P_codigo_reporte", SqlDbType.Int).Value = "10";
        lComando.Parameters.Add("@P_codigo_registro", SqlDbType.Int).Value = hdnRegistro.Value;
        lComando.ExecuteNonQuery();
        lsqldata.SelectCommand = lComando;
        lsqldata.Fill(lds);
        dtgDetalle10.DataSource = lds;
        dtgDetalle10.DataBind();
        lConexion.Cerrar();
        tblGrilla.Visible = false;
        tbDetalle.Visible = true;
        tblMensaje.Visible = false;
        //tblDatos.Visible = false;
        //REcorro la grilla para mostrar los cambios en negrilla
        string sFecha = "";
        string sOperador = "";
        string sTramo = "";
        string sPunto = "";
        string sDemanda = "";
        string sSector = "";
        string sContrato = "";
        string sCantidad = "";
        string sEstado = ""; //20161124 modif inf ope
        int liConta = 0;
        foreach (DataGridItem Grilla in this.dtgDetalle10.Items)
        {
            liConta++;
            if (liConta == 1)
            {
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sTramo = Grilla.Cells[4].Text;
                sPunto = Grilla.Cells[6].Text;
                sDemanda = Grilla.Cells[8].Text;
                sSector = Grilla.Cells[10].Text;
                sContrato = Grilla.Cells[12].Text;
                sCantidad = Grilla.Cells[13].Text;
                sEstado = Grilla.Cells[14].Text; //20161124 modif inf ope
            }
            else
            {
                if (sFecha != Grilla.Cells[1].Text)
                    Grilla.Cells[1].Font.Bold = true;
                if (sOperador != Grilla.Cells[2].Text)
                {
                    Grilla.Cells[2].Font.Bold = true;
                    Grilla.Cells[3].Font.Bold = true;
                }
                if (sTramo != Grilla.Cells[4].Text)
                {
                    Grilla.Cells[4].Font.Bold = true;
                    Grilla.Cells[5].Font.Bold = true;
                }
                if (sPunto != Grilla.Cells[6].Text)
                {
                    Grilla.Cells[6].Font.Bold = true;
                    Grilla.Cells[7].Font.Bold = true;
                }
                if (sDemanda != Grilla.Cells[8].Text)
                {
                    Grilla.Cells[8].Font.Bold = true;
                    Grilla.Cells[9].Font.Bold = true;
                }
                if (sSector != Grilla.Cells[10].Text)
                {
                    Grilla.Cells[10].Font.Bold = true;
                    Grilla.Cells[11].Font.Bold = true;
                }
                if (sContrato != Grilla.Cells[12].Text)
                {
                    Grilla.Cells[12].Font.Bold = true;
                }
                if (sCantidad != Grilla.Cells[13].Text)
                    Grilla.Cells[13].Font.Bold = true;
                //20161124 modif inf ope
                if (sEstado != Grilla.Cells[14].Text)
                    Grilla.Cells[14].Font.Bold = true;
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sTramo = Grilla.Cells[4].Text;
                sPunto = Grilla.Cells[6].Text;
                sDemanda = Grilla.Cells[8].Text;
                sSector = Grilla.Cells[10].Text;
                sContrato = Grilla.Cells[12].Text;
                sCantidad = Grilla.Cells[13].Text;
                sEstado = Grilla.Cells[14].Text; //20161124 modif inf ope
            }
        }
    }

    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    /// 20190306 rq013-19
    protected void dtgMaestro11_EditCommand(object source, DataGridCommandEventArgs e)
    {
        dtgDetalle1.Visible = false;
        dtgDetalle2.Visible = false;
        dtgDetalle3.Visible = false;
        dtgDetalle4.Visible = false;
        dtgDetalle5.Visible = false;
        dtgDetalle6.Visible = false;
        dtgDetalle7.Visible = false;
        dtgDetalle8.Visible = false;
        dtgDetalle9.Visible = false;
        dtgDetalle10.Visible = false;
        dtgDetalle11.Visible = true;
        dtgDetalle12.Visible = false;
        dtgDetalle13.Visible = false;
        dtgDetalle14.Visible = false;

        hdnRegistro.Value = this.dtgMaestro11.Items[e.Item.ItemIndex].Cells[0].Text;
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        SqlCommand lComando = new SqlCommand();
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandTimeout = 3600;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_GetConsModInfOpeDet";
        lComando.Parameters.Add("@P_codigo_reporte", SqlDbType.Int).Value = "11";
        lComando.Parameters.Add("@P_codigo_registro", SqlDbType.Int).Value = hdnRegistro.Value;
        lComando.ExecuteNonQuery();
        lsqldata.SelectCommand = lComando;
        lsqldata.Fill(lds);
        dtgDetalle11.DataSource = lds;
        dtgDetalle11.DataBind();
        lConexion.Cerrar();
        tblGrilla.Visible = false;
        tbDetalle.Visible = true;
        tblMensaje.Visible = false;
        //tblDatos.Visible = false;
        //REcorro la grilla para mostrar los cambios en negrilla
        string sFecha = "";
        string sOperador = "";
        string sGas = "";
        string sFuente = "";
        string sCantidad = "";
        string sIngresa = "";
        string sEstado = "";
        int liConta = 0;
        foreach (DataGridItem Grilla in this.dtgDetalle11.Items)
        {
            liConta++;
            if (liConta == 1)
            {
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sGas = Grilla.Cells[4].Text;
                sFuente = Grilla.Cells[6].Text;
                sIngresa = Grilla.Cells[8].Text;
                sCantidad = Grilla.Cells[9].Text;
                sEstado = Grilla.Cells[10].Text;
            }
            else
            {
                if (sFecha != Grilla.Cells[1].Text)
                    Grilla.Cells[1].Font.Bold = true;
                if (sOperador != Grilla.Cells[2].Text)
                {
                    Grilla.Cells[2].Font.Bold = true;
                    Grilla.Cells[3].Font.Bold = true;
                }
                if (sGas != Grilla.Cells[4].Text)
                {
                    Grilla.Cells[4].Font.Bold = true;
                    Grilla.Cells[5].Font.Bold = true;
                }
                if (sFuente != Grilla.Cells[6].Text)
                {
                    Grilla.Cells[6].Font.Bold = true;
                    Grilla.Cells[7].Font.Bold = true;
                }
                if (sIngresa != Grilla.Cells[8].Text)
                    Grilla.Cells[8].Font.Bold = true;
                if (sCantidad != Grilla.Cells[9].Text)
                    Grilla.Cells[9].Font.Bold = true;
                if (sEstado != Grilla.Cells[10].Text)
                    Grilla.Cells[10].Font.Bold = true;

                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sGas = Grilla.Cells[4].Text;
                sFuente = Grilla.Cells[6].Text;
                sIngresa = Grilla.Cells[8].Text;
                sCantidad = Grilla.Cells[9].Text;
                sEstado = Grilla.Cells[10].Text;
            }
        }
    }

    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    /// 20190306 rq013-19
    protected void dtgMaestro12_EditCommand(object source, DataGridCommandEventArgs e)
    {
        dtgDetalle1.Visible = false;
        dtgDetalle2.Visible = false;
        dtgDetalle3.Visible = false;
        dtgDetalle4.Visible = false;
        dtgDetalle5.Visible = false;
        dtgDetalle6.Visible = false;
        dtgDetalle7.Visible = false;
        dtgDetalle8.Visible = false;
        dtgDetalle9.Visible = false;
        dtgDetalle10.Visible = false;
        dtgDetalle11.Visible = false;
        dtgDetalle12.Visible = true;
        dtgDetalle13.Visible = false;
        dtgDetalle14.Visible = false;

        hdnRegistro.Value = this.dtgMaestro12.Items[e.Item.ItemIndex].Cells[0].Text;
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        SqlCommand lComando = new SqlCommand();
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandTimeout = 3600;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_GetConsModInfOpeDet";
        lComando.Parameters.Add("@P_codigo_reporte", SqlDbType.Int).Value = "12";
        lComando.Parameters.Add("@P_codigo_registro", SqlDbType.Int).Value = hdnRegistro.Value;
        lComando.ExecuteNonQuery();
        lsqldata.SelectCommand = lComando;
        lsqldata.Fill(lds);
        dtgDetalle12.DataSource = lds;
        dtgDetalle12.DataBind();
        lConexion.Cerrar();
        tblGrilla.Visible = false;
        tbDetalle.Visible = true;
        tblMensaje.Visible = false;
        //tblDatos.Visible = false;
        //REcorro la grilla para mostrar los cambios en negrilla
        string sFecha = "";
        string sOperador = "";
        string sFuente = "";
        string sCantidad = "";
        string sIngresa = "";
        string sEstado = "";
        int liConta = 0;
        foreach (DataGridItem Grilla in this.dtgDetalle11.Items)
        {
            liConta++;
            if (liConta == 1)
            {
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sFuente = Grilla.Cells[4].Text;
                sIngresa = Grilla.Cells[6].Text;
                sCantidad = Grilla.Cells[7].Text;
                sEstado = Grilla.Cells[8].Text;
            }
            else
            {
                if (sFecha != Grilla.Cells[1].Text)
                    Grilla.Cells[1].Font.Bold = true;
                if (sOperador != Grilla.Cells[2].Text)
                {
                    Grilla.Cells[2].Font.Bold = true;
                    Grilla.Cells[3].Font.Bold = true;
                }
                if (sFuente != Grilla.Cells[4].Text)
                {
                    Grilla.Cells[4].Font.Bold = true;
                    Grilla.Cells[5].Font.Bold = true;
                }
                if (sIngresa != Grilla.Cells[6].Text)
                    Grilla.Cells[6].Font.Bold = true;
                if (sCantidad != Grilla.Cells[7].Text)
                    Grilla.Cells[7].Font.Bold = true;
                if (sEstado != Grilla.Cells[8].Text)
                    Grilla.Cells[8].Font.Bold = true;

                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sFuente = Grilla.Cells[4].Text;
                sIngresa = Grilla.Cells[6].Text;
                sCantidad = Grilla.Cells[7].Text;
                sEstado = Grilla.Cells[8].Text;
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    /// 20190306 rq013-19
    protected void dtgMaestro13_EditCommand(object source, DataGridCommandEventArgs e)
    {
        dtgDetalle1.Visible = false;
        dtgDetalle2.Visible = false;
        dtgDetalle3.Visible = false;
        dtgDetalle4.Visible = false;
        dtgDetalle5.Visible = false;
        dtgDetalle6.Visible = false;
        dtgDetalle7.Visible = false;
        dtgDetalle8.Visible = false;
        dtgDetalle9.Visible = false;
        dtgDetalle10.Visible = false;
        dtgDetalle11.Visible = false;
        dtgDetalle12.Visible = false;
        dtgDetalle13.Visible = true;
        dtgDetalle14.Visible = false;

        hdnRegistro.Value = this.dtgMaestro13.Items[e.Item.ItemIndex].Cells[0].Text;
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        SqlCommand lComando = new SqlCommand();
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandTimeout = 3600;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_GetConsModInfOpeDet";
        lComando.Parameters.Add("@P_codigo_reporte", SqlDbType.Int).Value = "13";
        lComando.Parameters.Add("@P_codigo_registro", SqlDbType.Int).Value = hdnRegistro.Value;
        lComando.ExecuteNonQuery();
        lsqldata.SelectCommand = lComando;
        lsqldata.Fill(lds);
        dtgDetalle13.DataSource = lds;
        dtgDetalle13.DataBind();
        lConexion.Cerrar();
        tblGrilla.Visible = false;
        tbDetalle.Visible = true;
        tblMensaje.Visible = false;
        ////tblDatos.Visible = false;
        //REcorro la grilla para mostrar los cambios en negrilla
        string sFecha = "";
        string sOperador = "";
        string sEstacion = "";
        string sTipo = "";
        string sCantidad = "";
        string sCausa = "";
        string sEstado = "";
        int liConta = 0;
        foreach (DataGridItem Grilla in this.dtgDetalle11.Items)
        {
            liConta++;
            if (liConta == 1)
            {
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sEstacion = Grilla.Cells[4].Text;
                sTipo = Grilla.Cells[6].Text;
                sCantidad = Grilla.Cells[7].Text;
                sEstado = Grilla.Cells[8].Text;
                sCausa = Grilla.Cells[9].Text;
            }
            else
            {
                if (sFecha != Grilla.Cells[1].Text)
                    Grilla.Cells[1].Font.Bold = true;
                if (sOperador != Grilla.Cells[2].Text)
                {
                    Grilla.Cells[2].Font.Bold = true;
                    Grilla.Cells[3].Font.Bold = true;
                }
                if (sEstacion != Grilla.Cells[4].Text)
                {
                    Grilla.Cells[4].Font.Bold = true;
                    Grilla.Cells[5].Font.Bold = true;
                }
                if (sTipo != Grilla.Cells[6].Text)
                    Grilla.Cells[6].Font.Bold = true;
                if (sCantidad != Grilla.Cells[7].Text)
                    Grilla.Cells[7].Font.Bold = true;
                if (sEstado != Grilla.Cells[8].Text)
                    Grilla.Cells[8].Font.Bold = true;
                if (sCausa != Grilla.Cells[9].Text)
                {
                    Grilla.Cells[9].Font.Bold = true;
                    Grilla.Cells[10].Font.Bold = true;
                }
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sEstacion = Grilla.Cells[4].Text;
                sTipo = Grilla.Cells[6].Text;
                sCantidad = Grilla.Cells[7].Text;
                sEstado = Grilla.Cells[8].Text;
                sCausa = Grilla.Cells[9].Text;
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    /// 20190306 rq013-19
    protected void dtgMaestro14_EditCommand(object source, DataGridCommandEventArgs e)
    {
        dtgDetalle1.Visible = false;
        dtgDetalle2.Visible = false;
        dtgDetalle3.Visible = false;
        dtgDetalle4.Visible = false;
        dtgDetalle5.Visible = false;
        dtgDetalle6.Visible = false;
        dtgDetalle7.Visible = false;
        dtgDetalle8.Visible = false;
        dtgDetalle9.Visible = false;
        dtgDetalle10.Visible = false;
        dtgDetalle11.Visible = false;
        dtgDetalle12.Visible = false;
        dtgDetalle13.Visible = false;
        dtgDetalle14.Visible = true;

        hdnRegistro.Value = this.dtgMaestro14.Items[e.Item.ItemIndex].Cells[0].Text;
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        SqlCommand lComando = new SqlCommand();
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandTimeout = 3600;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_GetConsModInfOpeDet";
        lComando.Parameters.Add("@P_codigo_reporte", SqlDbType.Int).Value = "14";
        lComando.Parameters.Add("@P_codigo_registro", SqlDbType.Int).Value = hdnRegistro.Value;
        lComando.ExecuteNonQuery();
        lsqldata.SelectCommand = lComando;
        lsqldata.Fill(lds);
        dtgDetalle14.DataSource = lds;
        dtgDetalle14.DataBind();
        lConexion.Cerrar();
        tblGrilla.Visible = false;
        tbDetalle.Visible = true;
        tblMensaje.Visible = false;
        //tblDatos.Visible = false;
        //REcorro la grilla para mostrar los cambios en negrilla
        string sFecha = "";
        string sOperador = "";
        string sRemitente = "";
        string sPunto = "";
        string sTramo = "";
        string sCantidad = "";
        string sCausa = "";
        string sEstado = "";
        int liConta = 0;
        foreach (DataGridItem Grilla in this.dtgDetalle11.Items)
        {
            liConta++;
            if (liConta == 1)
            {
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sRemitente = Grilla.Cells[4].Text;
                sPunto = Grilla.Cells[6].Text;
                sTramo = Grilla.Cells[8].Text;
                sCantidad = Grilla.Cells[10].Text;
                sEstado = Grilla.Cells[11].Text;
                sCausa = Grilla.Cells[12].Text;
            }
            else
            {
                if (sFecha != Grilla.Cells[1].Text)
                    Grilla.Cells[1].Font.Bold = true;
                if (sOperador != Grilla.Cells[2].Text)
                {
                    Grilla.Cells[2].Font.Bold = true;
                    Grilla.Cells[3].Font.Bold = true;
                }
                if (sRemitente != Grilla.Cells[4].Text)
                {
                    Grilla.Cells[4].Font.Bold = true;
                    Grilla.Cells[5].Font.Bold = true;
                }
                if (sPunto != Grilla.Cells[6].Text)
                {
                    Grilla.Cells[6].Font.Bold = true;
                    Grilla.Cells[7].Font.Bold = true;
                }
                if (sTramo != Grilla.Cells[8].Text)
                {
                    Grilla.Cells[8].Font.Bold = true;
                    Grilla.Cells[9].Font.Bold = true;
                }
                if (sCantidad != Grilla.Cells[10].Text)
                    Grilla.Cells[10].Font.Bold = true;
                if (sEstado != Grilla.Cells[11].Text)
                    Grilla.Cells[11].Font.Bold = true;
                if (sCausa != Grilla.Cells[12].Text)
                {
                    Grilla.Cells[12].Font.Bold = true;
                    Grilla.Cells[13].Font.Bold = true;
                }
                sFecha = Grilla.Cells[1].Text;
                sOperador = Grilla.Cells[2].Text;
                sRemitente = Grilla.Cells[4].Text;
                sPunto = Grilla.Cells[6].Text;
                sTramo = Grilla.Cells[8].Text;
                sCantidad = Grilla.Cells[10].Text;
                sEstado = Grilla.Cells[11].Text;
                sCausa = Grilla.Cells[12].Text;
            }
        }
    }

    protected void BtnRegresar_Click(object sender, EventArgs e)
    {
        tblGrilla.Visible = true;
        tbDetalle.Visible = false;
        tblMensaje.Visible = true;
        //tblDatos.Visible = true;
    }

    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel1_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "ConsModInfOpeDet" + DateTime.Now + ".xls";
        string lstitulo_informe = "Detalle de modificaciones de información operativa: " + ddlReporte.SelectedItem.Text + ", código: " + hdnRegistro.Value;
        try
        {
            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            switch (ddlReporte.SelectedValue)
            {
                case "1":
                    dtgDetalle1.EnableViewState = false;
                    lform.Controls.Add(dtgDetalle1);
                    break;
                case "2":
                    dtgDetalle2.EnableViewState = false;
                    lform.Controls.Add(dtgDetalle2);
                    break;
                case "3":
                    dtgDetalle3.EnableViewState = false;
                    lform.Controls.Add(dtgDetalle3);
                    break;
                case "4":
                    dtgDetalle4.EnableViewState = false;
                    lform.Controls.Add(dtgDetalle4);
                    break;
                case "5":
                    dtgDetalle5.EnableViewState = false;
                    lform.Controls.Add(dtgDetalle5);
                    break;
                case "6":
                    dtgDetalle6.EnableViewState = false;
                    lform.Controls.Add(dtgDetalle6);
                    break;
                case "7":
                    dtgDetalle7.EnableViewState = false;
                    lform.Controls.Add(dtgDetalle7);
                    break;
                case "8":
                    dtgDetalle8.EnableViewState = false;
                    lform.Controls.Add(dtgDetalle8);
                    break;
                case "9":
                    dtgDetalle9.EnableViewState = false;
                    lform.Controls.Add(dtgDetalle9);
                    break;
                case "10":
                    dtgDetalle10.EnableViewState = false;
                    lform.Controls.Add(dtgDetalle10);
                    break;
                //20190306 rq013-19
                case "11":
                    dtgDetalle11.EnableViewState = false;
                    lform.Controls.Add(dtgDetalle11);
                    break;
                case "12":
                    dtgDetalle12.EnableViewState = false;
                    lform.Controls.Add(dtgDetalle12);
                    break;
                case "13":
                    dtgDetalle13.EnableViewState = false;
                    lform.Controls.Add(dtgDetalle13);
                    break;
                case "14":
                    dtgDetalle14.EnableViewState = false;
                    lform.Controls.Add(dtgDetalle14);
                    break;
                    //20190306 fin rq013-19
            }
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='7' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "No se Pudo Generar el Excel.! " + ex.Message.ToString());
        }
    }

}
