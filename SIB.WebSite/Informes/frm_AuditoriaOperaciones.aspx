﻿<%@ Page Language="C#" MasterPageFile="~/PlantillaPrincipal.master" AutoEventWireup="true" CodeFile="frm_AuditoriaOperaciones.aspx.cs" Inherits="Informes_frm_AuditoriaOperaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">

                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Inicial</label>
                            <asp:TextBox ID="TxtFechaIni" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static" Width="100%"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RfvTxtFechaIni" runat="server" ErrorMessage="El Campo Fecha Inicial es obligatorio"
                                ControlToValidate="TxtFechaIni" ValidationGroup="detalle">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Final</label>
                            <asp:TextBox ID="TxtFechaFin" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static" Width="100%"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RfvTxtFechaFin" runat="server" ErrorMessage="El Campo Fechs Final es obligatorio"
                                ControlToValidate="TxtFechaFin" ValidationGroup="detalle">*</asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Número Operación</label>
                            <asp:TextBox ID="txtOperacion" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:CompareValidator ID="cvtxtOperacion" runat="server" ErrorMessage="EL número de operación debe ser numérico"
                                ControlToValidate="txtOperacion" Operator="DataTypeCheck" Type="Double" ValidationGroup="detalle"></asp:CompareValidator>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Consecutivo Interno</label>
                            <asp:TextBox ID="txtConsecutivo" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:CompareValidator ID="cvtxtConsecutivo" runat="server" ErrorMessage="EL consecutivo interno debe ser numérico"
                                ControlToValidate="txtOperacion" Operator="DataTypeCheck" Type="Double" ValidationGroup="detalle"></asp:CompareValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Usuario Evento</label>
                            <asp:TextBox ID="txtUsuario" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Evento</label>
                            <asp:DropDownList ID="ddlEvento" runat="server" CssClass="form-control">
                                <asp:ListItem Value="">Seleccione </asp:ListItem>
                                <asp:ListItem Value="1">Insertar</asp:ListItem>
                                <asp:ListItem Value="2">Actualizar</asp:ListItem>
                                <asp:ListItem Value="3">Eliminar</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>

                <div class="row" runat="server">
                    <div class="form-group">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                        <asp:Label ID="lblMensaje" Style="display: inline-block;" CssClass="invalid-feedback" runat="server" />
                    </div>
                </div>

            </div>

        </div>

    </div>

    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla" visible="false">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 850px; height: 350px;">
                    <asp:GridView ID="gvReporte" runat="server" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1"
                        HorizontalAlign="Left">
                    </asp:GridView>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
