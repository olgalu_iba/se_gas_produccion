﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;
using System.Text;
using Segas.Web.Elements;

public partial class Informes_frm_ContratosSectorConsumo : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Contratación por Sector de Consumo - Suministro";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        //Se agrega el comportamiento del botón
        buttons.ExportarExcelOnclick += ImgExcel_Click;
        buttons.FiltrarOnclick += imbConsultar_Click;

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, DdlRolV, "m_tipos_operador", " estado = 'A' order by descripcion", 2, 1);
            LlenarControles(lConexion.gObjConexion, DdlVendedor, "m_operador", " estado = 'A' order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, DdlRolC, "m_tipos_operador", " estado = 'A' order by descripcion", 2, 1);
            LlenarControles(lConexion.gObjConexion, DdlComprador, "m_operador", " estado = 'A' order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, DdlModalidad, "m_modalidad_contractual", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlDemanda, "m_tipo_demanda_atender", " estado = 'A' order by descripcion", 0, 1);
            lConexion.Cerrar();
            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar };
            buttons.Inicializar(botones: botones);
            DdlDemanda_SelectedIndexChanged(null, null); // 20160706 sector de conusmo
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            if (lsTabla != "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, EventArgs e)
    {
        bool Error = false;
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF = DateTime.Now;
        TimeSpan DifFecha;
        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Formato Inválido en el Campo Fecha Inicial. <br>");
                Error = true;
            }
        }
        else
        {
            Toastr.Warning(this, "Debe digitar la Fecha Inicial. <br>");
            Error = true;
        
        }
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                if (TxtFechaIni.Text.Trim().Length > 0)
                {
                    ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
                    if (ldFechaI > ldFechaF)
                    {
                        Toastr.Warning(this, "La Fecha Final NO puede ser Menor que la Fecha de Inicial. <br>");
                        Error = true;

                    }
                    else
                    {
                        DifFecha = ldFechaF - ldFechaI;
                        if (DifFecha.Days > 61)
                        {
                            Toastr.Warning(this, "El rango de fechas debe ser máximo de dos meses. <br> ");
                            Error = true;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Formato Inválido en el Campo Fecha Final. <br>");
                Error = true;

            }
        }
        else
        {
            Toastr.Warning(this, "Debe digitar la Fecha Final. <br>");
            Error = true;
      
        }
        if (!Error)
        {
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetContDesMenSector";
                lComando.Parameters.Add("@P_fecha_inicial", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                lComando.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaFin.Text.Trim();
                lComando.Parameters.Add("@P_tipo_operador_c", SqlDbType.VarChar).Value = DdlRolC.SelectedValue;
                lComando.Parameters.Add("@P_codigo_operador_c", SqlDbType.Int).Value = DdlComprador.SelectedValue;
                lComando.Parameters.Add("@P_tipo_operador_v", SqlDbType.VarChar).Value = DdlRolV.SelectedValue;
                lComando.Parameters.Add("@P_codigo_operador_v", SqlDbType.Int).Value = DdlVendedor.SelectedValue;
                lComando.Parameters.Add("@P_codigo_modalidad", SqlDbType.Int).Value = DdlModalidad.SelectedValue;
                lComando.Parameters.Add("@P_codigo_tipo_demanda", SqlDbType.Int).Value = DdlDemanda.SelectedValue;
                lComando.Parameters.Add("@P_sector_consumo", SqlDbType.Int).Value = DdlSector.SelectedValue;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgMaestro.DataSource = lds;
                dtgMaestro.DataBind();
                tblGrilla.Visible = true;
                buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pudo Generar el Informe.! " + ex.Message.ToString());
              
            }
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, EventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfIdRueda" + DateTime.Now + ".xls";
        string lstitulo_informe = "";
        string lsTituloParametros = "";
        try
        {
            lstitulo_informe = "Contratación por Sector de Consumo - Suministro";
                lsTituloParametros += " - Fecha Inicial: " + TxtFechaIni.Text.Trim() + " - Fecha Final: " + TxtFechaFin.Text.Trim();
            if (DdlRolV.SelectedValue != "")
                lsTituloParametros += "  - Rol Venta: " + DdlRolV.SelectedItem.ToString();
            if (DdlVendedor.SelectedValue != "0")
                lsTituloParametros += "  - Vendedor: " + DdlVendedor.SelectedItem.ToString();
            if (DdlRolC.SelectedValue != "0")
                lsTituloParametros += "  - Rol Compra: " + DdlRolC.SelectedItem.ToString();
            if (DdlComprador.SelectedValue != "0")
                lsTituloParametros += "  - Comprador: " + DdlComprador.SelectedItem.ToString();
            if (DdlModalidad.SelectedValue != "0")
                lsTituloParametros += "  - Modalidad: " + DdlModalidad.SelectedItem.ToString();
            if (DdlSector.SelectedValue != "0")
                lsTituloParametros += "  - Sector de Consumo: " + DdlSector.SelectedItem.ToString();
            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgMaestro.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            lform.Controls.Add(dtgMaestro);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "No se Pudo Generar el Excel.! " + ex.Message.ToString());
          
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20160706 sector de conusmo
    protected void DdlDemanda_SelectedIndexChanged(object sender, EventArgs e)
    {
        lConexion.Abrir();
        DdlSector.Items.Clear();
        LlenarControles(lConexion.gObjConexion, DdlSector, "m_sector_consumo sec, m_demanda_sector dem ", " sec.codigo_sector_consumo = dem.codigo_sector_consumo and dem.codigo_tipo_demanda = " + DdlDemanda.SelectedValue + " and sec.estado = 'A'  and dem.estado ='A' and dem.ind_uso ='T' order by sec.descripcion", 0, 1); //20160706 sector de conusmo
        lConexion.Cerrar();
    }

}