﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;
using System.Text;
using Segas.Web.Elements;

public partial class Informes_frm_IndicadorPublicacionIOP : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "INDICADOR DE OPORTUNIDAD DE PUBLICACION (IOP)";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    DataSet lds1 = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();
    SqlDataAdapter lsqldata1 = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);

        //Se agrega el comportamiento del botón
        buttons.ExportarExcelOnclick += ImgExcel_Click;
        buttons.FiltrarOnclick += imbConsultar_Click;


        //Botones
        EnumBotones[] botones = { EnumBotones.Buscar };
        buttons.Inicializar(botones: botones);

    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, EventArgs e)
    {
        bool Error = false;
        bool blEncontro = false;
        decimal liTotal = 0;
        decimal liCumple = 0;
        decimal ldIndicadorMs = 0;
        decimal ldIndicadorOp = 0;
        decimal ldIndicadorTotal = 0;

        DateTime ldFecha;
        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Formato Inválido en el Campo Fecha Inicial. <br>");
                Error = true;


            }
        }
        else
        {
            Toastr.Warning(this, "Debe Ingresar la Fecha Inicial. <br>");
            Error = true;

        }
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaFin.Text);
                if (ldFecha < Convert.ToDateTime(TxtFechaIni.Text))
                {
                    Toastr.Warning(this, "La Fecha Final NO puede ser menor que la fecha Inicial. <br>");
                    Error = true;

                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Formato Inválido en el Campo Fecha Final. <br>");
                Error = true;

            }
        }
        else
        {
            Toastr.Warning(this, "Debe Ingresar la Fecha Final. <br>");
            Error = true;
        }
        if (!Error)
        {
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();

                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetIndicadorMsIOP";
                lComando.Parameters.Add("@P_fecha_inicial", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                lComando.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaFin.Text.Trim();
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgTransaccion.DataSource = lds;
                dtgTransaccion.DataBind();
                if (dtgTransaccion.Items.Count > 0)
                {
                    blEncontro = true;
                    foreach (DataGridItem Grilla in this.dtgTransaccion.Items)
                    {
                        liTotal++;
                        if (Grilla.Cells[9].Text == "Cumple")
                            liCumple++;
                    }
                    if (liTotal > 0)
                    {
                        ldIndicadorMs = (liCumple / liTotal) * 100;
                        lblResInd1.Text = ldIndicadorMs.ToString("##0.00");
                    }
                }
                liCumple = 0;
                liTotal = 0;
                SqlCommand lComando1 = new SqlCommand();
                lComando1.Connection = lConexion.gObjConexion;
                lComando1.CommandTimeout = 3600;
                lComando1.CommandType = CommandType.StoredProcedure;
                lComando1.CommandText = "pa_GetIndicadorOperaIOP";
                lComando1.Parameters.Add("@P_fecha_inicial", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                lComando1.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaFin.Text.Trim();
                lComando1.ExecuteNonQuery();
                lsqldata1.SelectCommand = lComando1;
                lsqldata1.Fill(lds1);
                dtgOperativa.DataSource = lds1;
                dtgOperativa.DataBind();
                if (dtgOperativa.Items.Count > 0)
                {
                    blEncontro = true;
                    foreach (DataGridItem Grilla in this.dtgOperativa.Items)
                    {
                        liTotal++;
                        if (Grilla.Cells[4].Text == "Cumple")
                            liCumple++;
                    }
                    if (liTotal > 0)
                    {
                        ldIndicadorOp = (liCumple / liTotal) * 100;
                        lblResInd2.Text = ldIndicadorOp.ToString("##0.00");
                    }
                }
                lConexion.Cerrar();
                // Calculo el Indiciador Total
                ldIndicadorTotal = (ldIndicadorMs + ldIndicadorOp) / 2;
                lblResIndTotal.Text = ldIndicadorTotal.ToString("##0.00");
                if (blEncontro)
                {
                    tblGrilla.Visible = true;
                    buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pude Generar el Informe.! " + ex.Message.ToString());
                Error = true;
                
            }
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, EventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfContratos" + DateTime.Now + ".xls";
        string lstitulo_informe = "INDICADOR DE OPORTUNIDAD DE PUBLICACION (IOP)";
        string lsTituloParametros = "Fecha Inicial: " + TxtFechaIni.Text + " - Fecha Final: " + TxtFechaFin.Text;
        try
        {

            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgTransaccion.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            Label oText = new Label();
            oText.Text = "<br><table><tr><th colspan='4' align='left'><font face=Arial size=4>Información Transaccinal Mercado Secundario</font></th><td><font face=Arial size=5></font></td></tr></table>";
            lform.Controls.Add(oText);
            lform.Controls.Add(dtgTransaccion);
            Label oTextResulta = new Label();
            oTextResulta.Text = "<br><table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lblInd1.Text + " : " + lblResInd1.Text + "</font></th><td><font face=Arial size=5></font></td></tr></table>";
            lform.Controls.Add(oTextResulta);
            Label oText1 = new Label();
            oText1.Text = "<br><table><tr><th colspan='4' align='left'><font face=Arial size=4>Información Operativa</font></th><td><font face=Arial size=5></font></td></tr></table>";
            lform.Controls.Add(oText1);
            lform.Controls.Add(dtgOperativa);
            Label oTextResulta1 = new Label();
            oTextResulta1.Text = "<br><table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lblInd2.Text + " : " + lblResInd2.Text + "</font></th><td><font face=Arial size=5></font></td></tr></table>";
            lform.Controls.Add(oTextResulta1);
            Label oTextResultaTotal = new Label();
            oTextResultaTotal.Text = "<br><table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lblIndTotal.Text + " : " + lblResIndTotal.Text + "</font></th><td><font face=Arial size=5></font></td></tr></table>";
            lform.Controls.Add(oTextResultaTotal);
            lpagina.RenderControl(lhtw);
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            this.EnableViewState = false;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='6' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center></center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='6' align='left'><font face=Arial size=4>Parametros: " + lsTituloParametros + "</font></th><td><font face=Arial size=5><center></center></font></td></tr></table><br>");
            Response.Write(lsb.ToString());
            Response.End();
            Response.Flush();

        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "No se Pude Generar el Excel.! " + ex.Message.ToString());
         
        }
    }
}