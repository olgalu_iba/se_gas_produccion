﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_PrecioPromSumMp.aspx.cs"
    Inherits="Informes_frm_PrecioPromSumMp" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

        <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
            width="100%">
            <tr>
                <td align="center" class="th1" style="width: 80%;">
                    <asp:Label ID="lblTitulo" runat="server" ForeColor="White"></asp:Label>
                </td>
                <td align="center" class="th1" style="width: 20%;">
                    <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/exel 2007 3D.png"
                        Height="40" OnClick="imbExcel_Click" />
                </td>
            </tr>
        </table>
    <br /><br /><br /><br /><br /><br />
        <asp:UpdateProgress ID="panelUpdateProgress" DisplayAfter="0" runat="server" AssociatedUpdatePanelID="UptPnlTodo">
            <ProgressTemplate>
                <asp:Panel ID="PnlCarga" runat="server" Style="position: relative; top: 30%; text-align: center;"
                    CssClass="popup">
                    <div class="fuente">
                        <img src="../Images/ajax-loader.gif" style="vertical-align: middle" alt="Procesando" />
                        Procesando por Favor espere ...
                    </div>
                </asp:Panel>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="UptPnlTodo" runat="server">
            <ContentTemplate>
                <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
                    width="90%">
                    <tr>
                        <td class="td1">Fecha Negociación Inicial
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtFechaIni" runat="server" width="100%" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
                        </td>
                        <td class="td1">Fecha Negociación Final
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtFechaFin" runat="server" width="100%" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">Número Operación Inicial
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtNoContrato" runat="server" ValidationGroup="detalle" Width="120px"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="fteTxtNoContrato" runat="server" TargetControlID="TxtNoContrato"
                                FilterType="Custom, Numbers">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                        <td class="td1">Número Operación Final
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtNoContratoFin" runat="server" ValidationGroup="detalle" Width="120px"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="ftTxtNoContratoFin" runat="server" TargetControlID="TxtNoContratoFin"
                                FilterType="Custom, Numbers">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">Modalidad de contrato
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlModalidad" runat="server" Width="200px">
                            </asp:DropDownList>
                        </td>
                        <td class="td1">Punto de Entrega
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlPunto" runat="server" Width="250px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="th1" colspan="4" align="center">
                            <asp:Button ID="BtnBuscar" runat="server" Text="Consultar" OnClick="BtnBuscar_Click" />
                        </td>
                    </tr>
                </table>
                </div>
            <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                id="tblMensaje">
                <tr>
                    <td colspan="3" align="center">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
                <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                    id="tblGrilla" visible="false">
                    <tr>
                        <td colspan="3" align="center">
                            <div style="overflow: scroll; width: 1220px; height: 450px;">
                                <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                    AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                                    <Columns>
                                        <%--0--%>
                                        <asp:BoundColumn DataField="año" HeaderText="Año de negociación"></asp:BoundColumn>
                                        <%--1--%>
                                        <asp:BoundColumn DataField="mes" HeaderText="Mes"></asp:BoundColumn>
                                        <%--2--%>
                                        <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes de Negociación"></asp:BoundColumn>
                                        <%--3--%>
                                        <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Cod. Modalidad"></asp:BoundColumn>
                                        <%--4--%>
                                        <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad de contrato"></asp:BoundColumn>
                                        <%--5--%>
                                        <asp:BoundColumn DataField="codigo_punto_entrega" HeaderText="Cod. Pto. Entrega"></asp:BoundColumn>
                                        <%--6--%>
                                        <asp:BoundColumn DataField="desc_punto" HeaderText="Punto entrega"></asp:BoundColumn>
                                        <%--7--%>
                                        <asp:BoundColumn DataField="cantidad" HeaderText="cantidad" ItemStyle-HorizontalAlign="Right"
                                            DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                        <%--8--%>
                                        <asp:BoundColumn DataField="precio_prom" HeaderText="Precio Promedio" ItemStyle-HorizontalAlign="Right"
                                            DataFormatString="{0:###,###,###,##0.00}"></asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </div>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>