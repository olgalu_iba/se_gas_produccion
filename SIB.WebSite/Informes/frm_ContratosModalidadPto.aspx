﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ContratosModalidadPto.aspx.cs"
    Inherits="Informes_frm_ContratosModalidadPto" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

          <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                       
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Inicial de vigencia</label>
                            <asp:TextBox ID="TxtFechaIni" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Final de vigencia</label>
                            <asp:TextBox ID="TxtFechaFin" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fuente</label>
                          <asp:DropDownList ID="DdlFuente" runat="server" Width="100%" CssClass="form-control">
                                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                                    <asp:ListItem Value="S">Registro de contratos del sistema</asp:ListItem>
                                    <asp:ListItem Value="E">Registro de contratos externos</asp:ListItem>
                                    <asp:ListItem Value="M">Registro de contratos Mayoristas</asp:ListItem>
                                    <%--20180126 rq107-16--%>
                                </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div id="tdMes01" class="form-group" runat="server">
                            <label for="Name">Tipo Rueda</label>
                            <asp:DropDownList ID="DdlRueda" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                                </asp:DropDownList>
                        </div>
                    </div>
                    <div id="tdMes02" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                        <div class="form-group">
                            <label for="Name">Tipo Subasta</label>
                              <asp:DropDownList ID="DdlSubasta" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                                </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Modalidad Contrato</label>
                          <asp:DropDownList ID="DdlOperador" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                                </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Punta</label>
                          <asp:DropDownList ID="DdlPunta" runat="server" Width="100%" CssClass="form-control">
                                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                                    <asp:ListItem Value="C">Comprador</asp:ListItem>
                                    <asp:ListItem Value="V">Vendedor</asp:ListItem>
                                </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Modalidad Contractual</label>
                               <asp:DropDownList ID="DdlModalidad" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                                </asp:DropDownList>
                        </div>
                    </div>
                     <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Punto entrega</label>
                                <asp:DropDownList ID="DdlPozo" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                                </asp:DropDownList>
                        </div>
                    </div>
                </div>
                 <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblMensaje">
          
            <tr>
                <td colspan="3" align="center">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
        <div runat="server"
            id="tblGrilla" visible="false">
        
                    <div style="overflow: scroll; height: 450px;">
                        <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center">
                          
                            <Columns>
                                <asp:BoundColumn DataField="Año" HeaderText="año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="mes" HeaderText="mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fuente" HeaderText="fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tipo_rueda" HeaderText="tpo rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tipo_subasta" HeaderText="tpo subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="tpo mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20170926 rq027-17--%>
                                <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_modalidad" HeaderText="modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20180925 ajuste--%>
                                <asp:BoundColumn DataField="desc_salida" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20170926 rq027-17--%>
                                <asp:BoundColumn DataField="capacidad_transporte" HeaderText="capacidad transporte"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cnt_contratos" HeaderText="no. contratos" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
              
        </div>
            </div>
        </div>
    </div>
    
       
 </asp:Content>