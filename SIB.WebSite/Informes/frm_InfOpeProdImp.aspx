﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_InfOpeProdImp.aspx.cs"
    Inherits="Informes_frm_InfOpeProdImp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Inicial</label>
                            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Final</label>
                            <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                    <div id="tdMes02" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                        <div class="form-group">
                            <label for="Name">Operador</label>
                            <asp:DropDownList ID="ddlOperador" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="Div1" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                        <div class="form-group">
                            <label for="Name">Campo Producción o Importación</label>
                            <asp:DropDownList ID="ddlFuente" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="Div2" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                        <div class="form-group">
                            <label for="Name">Destino Energía</label>
                            <asp:DropDownList ID="ddlReporte" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                        id="tblMensaje">

                        <tr>
                            <td colspan="3" align="center">
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="detalle" />
                                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red" Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div runat="server" visible="false"
                    id="tblGrilla">

                    <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                        <%--20180126 rq107-16--%>
                        <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False"
                            Width="100%" CssClass="table-bordered">

                            <Columns>
                                <asp:BoundColumn DataField="destino" HeaderText="Destino Energía" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_capacidad" HeaderText="Código Capacidad" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_campo" HeaderText="Código Campo" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_campo" HeaderText="Campo Producción" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad Energía Oferta (MBTU)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_punto" HeaderText="Código Punto SNT" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Punto SNT" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_gasoducto" HeaderText="Código Gasoducto Dedicado" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_gasoducto" HeaderText="Gasoducto Dedicado" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tipo_prod" HeaderText="Tipo Producción" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="estado_carga" HeaderText="Estado Carga" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ingresa_snt" HeaderText="Ingresa SNT" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="login_usuario" HeaderText="usuario Actualización" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
