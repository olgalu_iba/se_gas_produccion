﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;

public partial class Informes_frm_SolCamPrecio : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Consulta de solicitudes de cambio de precio Aprobadas";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, DdlSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlOperador, "m_operador", " estado = 'A' And codigo_operador != 0 order by codigo_operador", 0, 4);
            lConexion.Cerrar();
            if (goInfo.cod_comisionista != "0")
            {
                DdlOperador.Enabled = false;
                DdlOperador.SelectedValue = goInfo.cod_comisionista;
            }
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            if (lsTabla != "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        DateTime ldFecha = DateTime.Now;
        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial. <br>";
            }
        }
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                if (TxtFechaIni.Text.Trim().Length > 0)
                {
                    if (ldFecha > Convert.ToDateTime(TxtFechaFin.Text))
                        lblMensaje.Text += "La Fecha Final NO puede ser Menor que la Fecha de Inicial. <br>";
                }
                else
                    lblMensaje.Text += "Debe digitar la fecha inicial antes que la final. <br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Final. <br>";
            }
        }
        if (lblMensaje.Text == "")
        {
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetSolicCamPre";
                lComando.Parameters.Add("@P_fecha_ini", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                if (TxtFechaFin.Text.Trim() == "")
                    lComando.Parameters.Add("@P_fecha_fin", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim() + " 23:59";
                else
                    lComando.Parameters.Add("@P_fecha_fin", SqlDbType.VarChar).Value = TxtFechaFin.Text.Trim() + " 23:59";
                lComando.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = DdlOperador.SelectedValue;
                lComando.Parameters.Add("@P_codigo_tipo_subasta", SqlDbType.Int).Value = DdlSubasta.SelectedValue;
                lComando.Parameters.Add("@P_tipo_mercado", SqlDbType.VarChar).Value = DdlMercado.SelectedValue;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgMaestro.DataSource = lds;
                dtgMaestro.DataBind();
                tblGrilla.Visible = true;
                imbExcel.Visible = true;
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pude Generar el Informe.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfIdRueda" + DateTime.Now + ".xls";
        string lstitulo_informe = "";
        string lsTituloParametros = "";
        try
        {
            lstitulo_informe = "Consulta de solicitudes de cambio de precio Aprobadas";
            if (TxtFechaIni.Text.Trim().Length > 0)
                lsTituloParametros += " - Fecha Inicial: " + TxtFechaIni.Text.Trim();
            if (TxtFechaFin.Text.Trim().Length > 0)
                lsTituloParametros += " - Fecha Final: " + TxtFechaFin.Text.Trim();
            if (DdlOperador.SelectedValue != "0")
                lsTituloParametros += " - Vendedor: " + DdlOperador.SelectedItem.ToString();
            if (DdlSubasta.SelectedValue != "0")
                lsTituloParametros += "  - Subasta: " + DdlSubasta.SelectedItem.ToString();
            if (DdlMercado.SelectedValue != "")
                lsTituloParametros += "  - Tpo Mercado: " + DdlMercado.SelectedItem.ToString();
            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgMaestro.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            lform.Controls.Add(dtgMaestro);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Excel.!" + ex.Message.ToString();
        }
    }
}