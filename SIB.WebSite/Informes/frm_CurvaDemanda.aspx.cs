﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Informes
{
    public partial class frm_CurvaDemanda : Page
    {
        private InfoSessionVO goInfo = null;
        private static string lsTitulo = "Curvas de Demanda"; // 20180126 rq107-16
        private clConexion lConexion = null;
        private SqlDataReader lLector;
        private DataSet lds = new DataSet();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo;
            lConexion = new clConexion(goInfo);
            //Se agrega el comportamiento del botón
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.FiltrarOnclick += imbConsultar_Click;

            if (IsPostBack) return;

            //Titulo
            Master.Titulo = "Curvas";
            //Descripcion
            //Master.DescripcionPagina = "D";

            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlSubasta, "m_tipos_subasta", " estado = 'A' and codigo_tipo_subasta not in (5,7,8) order by descripcion", 0, 1); // 20180126 rq107-16
            LlenarControles(lConexion.gObjConexion, ddlTipoRueda, "m_tipos_rueda", " codigo_tipo_rueda = -1 order by descripcion", 0, 1); // 20210224 ajsutes transporte
            LlenarControles(lConexion.gObjConexion, ddlPuntoEnt, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlComprador, "m_operador", " estado = 'A' order by razon_social", 0, 4);
            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlComprador.SelectedValue = goInfo.cod_comisionista;
                ddlComprador.Enabled = false;
            }

            lConexion.Cerrar();

            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar };
            buttons.Inicializar(botones: botones);
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            var lItem = new ListItem { Value = "0", Text = "Seleccione" };
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                var lItem1 = new ListItem();
                if (lsTabla != "m_operador") // 20180126 rq107-16
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else // 20180126 rq107-16
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"] + "-" + lLector["razon_social"];
                }
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCampos, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlCommand lComando = new SqlCommand();
            lComando.Connection = lConn;
            lComando.CommandTimeout = 3600;
            lComando.CommandType = CommandType.StoredProcedure;
            lComando.CommandText = "pa_ValidarExistencia1";
            lComando.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = lsTabla;
            lComando.Parameters.Add("@P_campos", SqlDbType.VarChar).Value = lsCampos;
            lComando.Parameters.Add("@P_filtro", SqlDbType.VarChar).Value = lsCondicion;
            SqlDataReader lLector;
            lLector = lComando.ExecuteReader();

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbConsultar_Click(object sender, EventArgs e)
        {
            var lblMensaje = "";
            if (ddlComprador.SelectedValue == "0")
                lblMensaje += "Debe seleccionar el comprador. <br>";
            if (TxtFechaIni.Text.Trim().Length <= 0 && TxtNoRueda.Text.Trim().Length <= 0 && TxtNoID.Text.Trim().Length <= 0)
                lblMensaje += "Debe selecionar la fecha, la rueda o el Id para la consulta. <br>";
            if (lblMensaje == "")
            {
                this.dtgCurva.CurrentPageIndex = 0; //20210707 ajuste
                CargarDatos();
            }
            else
                Toastr.Warning(this, lblMensaje);
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            var Error = false;
            DateTime ldFecha;
            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, "Formato Inválido en el Campo Fecha Inicial. <br>");
                    Error = true;
                }
            }
            if (!Error)
            {
                try
                {
                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    var lComando = new SqlCommand();
                    var lsqldata = new SqlDataAdapter();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandTimeout = 3600;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_GetCurvaDemanda";
                    lComando.Parameters.Add("@P_codigo_tipo_subasta", SqlDbType.Int).Value = ddlSubasta.SelectedValue;
                    if (ddlModalidad.SelectedValue == "")
                        lComando.Parameters.Add("@P_modalidad", SqlDbType.Int).Value = "0";
                    else
                        lComando.Parameters.Add("@P_modalidad", SqlDbType.Int).Value = ddlModalidad.SelectedValue;
                    if (TxtNoRueda.Text.Trim().Length <= 0)
                        lComando.Parameters.Add("@P_numero_rueda", SqlDbType.Int).Value = 0;
                    else
                        lComando.Parameters.Add("@P_numero_rueda", SqlDbType.Int).Value = TxtNoRueda.Text.Trim();
                    if (TxtNoID.Text.Trim().Length <= 0)
                        lComando.Parameters.Add("@P_numero_id", SqlDbType.Int).Value = 0;
                    else
                        lComando.Parameters.Add("@P_numero_id", SqlDbType.Int).Value = TxtNoID.Text.Trim();
                    lComando.Parameters.Add("@P_fecha_rueda", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                    lComando.Parameters.Add("@P_operador", SqlDbType.Int).Value = ddlComprador.SelectedValue;
                    if (ddlPuntoEnt.SelectedValue == "")
                        lComando.Parameters.Add("@P_codigo_punto_entrega", SqlDbType.Int).Value = "0";
                    else
                        lComando.Parameters.Add("@P_codigo_punto_entrega", SqlDbType.Int).Value = ddlPuntoEnt.SelectedValue;
                    lComando.Parameters.Add("@P_codigo_tipo_rueda", SqlDbType.Int).Value = ddlTipoRueda.SelectedValue; //20210224 
                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgCurva.DataSource = lds;
                    dtgCurva.DataBind();
                    dtgExcel.DataSource = lds; //20180126 rq107-16
                    dtgExcel.DataBind(); //20180126 rq107-16

                    buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
                    lConexion.Cerrar();
                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, "No se Pudo Generar el Informe.! " + ex.Message.ToString());


                }
            }
        }

        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgCurva_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            this.dtgCurva.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();

        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgCurva_EditCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Gráfica"))
                {
                    OpenCurvaOferDemAgre(e);
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlSubasta_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubasta.SelectedValue != "3")  //20161019 Subasta UVLP
            {
                lConexion.Abrir();
                ddlModalidad.Items.Clear();
                LlenarControles1(lConexion.gObjConexion, ddlModalidad, "m_modalidad_contractual mod, m_caracteristica_sub carM", "distinct mod.codigo_modalidad, mod.descripcion", " mod.codigo_modalidad = carM.codigo_caracteristica and carM.tipo_caracteristica ='M' and carM.codigo_tipo_subasta = " + ddlSubasta.SelectedValue + " and carM.estado ='A' and mod.estado ='A' order by descripcion", 0, 1);
                lConexion.Cerrar();
            }
            else //20161019 Subasta UVLP
            {
                lConexion.Abrir();
                ddlModalidad.Items.Clear();
                LlenarControles1(lConexion.gObjConexion, ddlModalidad, "m_modalidad_contractual mod", "mod.codigo_modalidad, mod.descripcion", " mod.codigo_modalidad = 1", 0, 1);
                lConexion.Cerrar();
            }
            //20210224 ajustes transporte
            lConexion.Abrir();
            ddlTipoRueda.Items.Clear();
            LlenarControles(lConexion.gObjConexion, ddlTipoRueda, "m_tipos_rueda", " codigo_tipo_subasta = "+ ddlSubasta .SelectedValue + " and estado ='A' order by descripcion", 0, 1); 
            lConexion.Cerrar();
            //20210224 fin ajustes transporte
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20180126 rq107-16
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            var lsqldata = new SqlDataAdapter();
            string lsNombreArchivo = goInfo.Usuario + "InfCurvaC" + DateTime.Now + ".xls";
            string lstitulo_informe = "Informe curva demandas";
            string lsTituloParametros = "";
            try
            {
                if (ddlSubasta.SelectedValue != "0")
                    lsTituloParametros += " Tipo Subasta: " + ddlSubasta.SelectedItem.ToString();
                //20210224
                if (ddlTipoRueda.SelectedValue != "0")
                    lsTituloParametros += " Tipo Rueda: " + ddlTipoRueda.SelectedItem.ToString();
                if (ddlModalidad.SelectedValue != "0" && ddlModalidad.SelectedValue != "")
                    lsTituloParametros += " Modalidad: " + ddlModalidad.SelectedItem.ToString();
                if (TxtNoRueda.Text.Trim().Length > 0)
                    lsTituloParametros += " - Número Rueda: " + TxtNoRueda.Text.Trim();
                if (TxtNoID.Text.Trim().Length > 0)
                    lsTituloParametros += " - Número Id: " + TxtNoID.Text.Trim();
                if (TxtFechaIni.Text.Trim().Length > 0)
                    lsTituloParametros += " - Fecha rueda: " + TxtFechaIni.Text.Trim();
                if (Session["tipoPerfil"].ToString() == "N")
                    lsTituloParametros += " - Operador: " + goInfo.cod_comisionista;
                if (ddlPuntoEnt.SelectedValue != "0" && ddlPuntoEnt.SelectedValue != "")
                    lsTituloParametros += "  - " + lblPunto.Text + ": " + ddlPuntoEnt.SelectedItem.ToString();
                decimal ldCapacidad = 0;
                var lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                var lsw = new StringWriter(lsb);
                var lhtw = new HtmlTextWriter(lsw);
                var lpagina = new Page();
                var lform = new HtmlForm();
                dtgExcel.EnableViewState = false;
                dtgExcel.Visible = true;
                lpagina.EnableEventValidation = false;
                lpagina.Controls.Add(lform);
                lpagina.DesignerInitialize();
                lform.Controls.Add(dtgExcel);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = System.Text.Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());

                Response.End();
                //lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
                dtgExcel.Visible = false;
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se Pudo Generar el Excel.! " + ex.Message);
            }
        }

        #region Curva de Oferta y Demanda Agregada

        /// <summary>
        /// Abre el modal de la Curva de Oferta y Demanda Agregada
        /// </summary>
        private void OpenCurvaOferDemAgre(DataGridCommandEventArgs e)
        {
            if (dtgCurva.Items[e.Item.ItemIndex].Cells[7].Text == " 0")
            {
                lblCantidadAdjudicada.Text = "No Adjudicado";
                lblPrecioAdjudicacion.Text = "No Adjudicado";
            }
            else
            {
                lblCantidadAdjudicada.Text = dtgCurva.Items[e.Item.ItemIndex].Cells[7].Text;
                lblPrecioAdjudicacion.Text = dtgCurva.Items[e.Item.ItemIndex].Cells[8].Text;
            }
            lblNoId.Text = dtgCurva.Items[e.Item.ItemIndex].Cells[0].Text;
            var lsDestino = dtgCurva.Items[e.Item.ItemIndex].Cells[10].Text;
            lblPunto.Text = dtgCurva.Items[e.Item.ItemIndex].Cells[5].Text; //cambio 20160128

            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            SqlCommand lComando1 = new SqlCommand();
            SqlDataAdapter lsqldata1 = new SqlDataAdapter();
            lComando1.Connection = lConexion.gObjConexion;
            lComando1.CommandTimeout = 3600;
            lComando1.CommandType = CommandType.StoredProcedure;
            lComando1.CommandText = "pa_GetDemandaCurva";
            lComando1.Parameters.Add("@P_numero_id", SqlDbType.Int).Value = lblNoId.Text;
            lComando1.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = ddlComprador.SelectedValue;
            lComando1.ExecuteNonQuery();
            lsqldata1.SelectCommand = lComando1;
            lsqldata1.Fill(lds);
            dtgInformacion.DataSource = lds;
            dtgInformacion.DataBind();
            lConexion.Cerrar();

            lblTipoPunto.Text = lsDestino == "G" ? "Punto de Entrega" : "Ruta";

            //Carga la grafica
            AuctionGraphics.GenerateGraphDemand(this, dtgInformacion, lblNoId.Text, lsDestino != "G");
            //Se abre el modal de Ingreso de Declaración de Información
            Modal.Abrir(this, mdlCurvaDem.ID, mdlmdlCurvaDemInside.ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseCurvaOferDemAgre_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Ingreso de Declaración de Información
                Modal.Cerrar(this, mdlCurvaDem.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Curva de Oferta y Demanda Agregada
    }
}