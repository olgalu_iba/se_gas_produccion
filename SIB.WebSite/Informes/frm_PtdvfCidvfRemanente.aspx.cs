﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;

public partial class Informes_frm_PtdvfCidvfRemanente : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Consulta PTDVF/CIDVF Remanente";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' And codigo_operador != 0 order by codigo_operador", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlFuente, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMesIni, "m_mes", " 1=1 order by mes", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMesFinal, "m_mes", " 1=1 order by mes", 0, 1);
            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
            }
            else
                if (VerificarExistencia("m_parametros_sslp", " fecha_pub_remanente <getdate() and year(fecha_pub_remanente) = year(getdate())"))
                    btnPublica.Visible = true;
            lConexion.Cerrar();
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            if (lsTabla != "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsulta_Click(object sender, EventArgs e)
    {
        consulta("C");
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnPublica_Click(object sender, EventArgs e)
    {
        consulta("P");
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfPTDVFRemanenete" + DateTime.Now + ".xls";
        string lstitulo_informe = "Consulta PTDVF/CIDVF Remanente";
        string lsTituloParametros = "";
        try
        {
            if (TxtAnoIni.Text != "")
            {
                lsTituloParametros = " Año Inicial: " + TxtAnoIni.Text + " - Mes Inicial: " + ddlMesIni.SelectedItem.ToString();
                lsTituloParametros += " - Año Final: " + TxtAnoFin.Text + " - Mes Final: " + ddlMesFinal.SelectedItem.ToString();
            }

            if (ddlOperador.SelectedValue != "0")
                lsTituloParametros += "  - Operador: " + ddlOperador.SelectedItem.ToString();
            if (ddlFuente.SelectedValue != "0")
                lsTituloParametros += "  - Fuente: " + ddlFuente.SelectedItem.ToString();


            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgMaestro.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            lform.Controls.Add(dtgMaestro);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Excel.!" + ex.Message.ToString();
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lsTable, string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lsTable, lswhere, goInfo);
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected void consulta(string lsIndica)
    {
        lblMensaje.Text = "";
        Int32 liValor;
        if (TxtAnoIni.Text.Trim().Length > 0)
        {
            try
            {
                liValor = Convert.ToInt32(TxtAnoIni.Text);
                if (liValor <= 0)
                    lblMensaje.Text += "Valor Inválido en el Campo Año Inicial. <br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Año Inicial. <br>";
            }
        }
        if (TxtAnoFin.Text.Trim().Length > 0)
        {
            try
            {
                liValor = Convert.ToInt32(TxtAnoFin.Text);
                if (liValor <= 0)
                    lblMensaje.Text += "Valor Inválido en el Campo Año Final. <br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Año Final. <br>";
            }
        }
        if (TxtAnoIni.Text.Trim().Length == 0 && TxtAnoFin.Text.Trim().Length > 0)
            lblMensaje.Text += "No puede seleccionar el año final sin seleccionar el año inicial. <br>";
        if (TxtAnoIni.Text.Trim().Length > 0 && TxtAnoFin.Text.Trim().Length == 0)
            lblMensaje.Text += "Si selecciona el año inicial debe seleccionar el final. <br>";
        if (ddlMesIni.SelectedValue == "0" && TxtAnoIni.Text.Trim().Length > 0)
            lblMensaje.Text += "Si selecciona el año inicial, debe seleccionar el mes inicial. <br>";
        if (ddlMesIni.SelectedValue != "0" && TxtAnoIni.Text.Trim().Length == 0)
            lblMensaje.Text += "No debe seleccionar el mes inicial sino selecciona el año inicial. <br>";
        if (ddlMesFinal.SelectedValue == "0" && TxtAnoFin.Text.Trim().Length > 0)
            lblMensaje.Text += "Si selecciona el año final, debe seleccionar el mes final. <br>";
        if (ddlMesFinal.SelectedValue != "0" && TxtAnoFin.Text.Trim().Length == 0)
            lblMensaje.Text += "No debe seleccionar el mes final sino selecciona el año final. <br>";
        if (lblMensaje.Text == "")
            if (TxtAnoIni.Text.Trim().Length > 0 && TxtAnoFin.Text.Trim().Length > 0)
                try
                {
                    if (Convert.ToInt32(TxtAnoIni.Text) * 100 + Convert.ToInt32(ddlMesIni.SelectedValue) > Convert.ToInt32(TxtAnoFin.Text) * 100 + Convert.ToInt32(ddlMesFinal.SelectedValue))
                        lblMensaje.Text += "EL periodo inicial debe ser menor o igual que el final. <br>";
                }
                catch (Exception ex)
                {
                }
        if (lblMensaje.Text == "")
        {
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_SetPtdvfRemanente";
                if (TxtAnoIni.Text =="")
                    lComando.Parameters.Add("@P_año_ini", SqlDbType.Int).Value = "0";
                else
                    lComando.Parameters.Add("@P_año_ini", SqlDbType.Int).Value = TxtAnoIni.Text.Trim();
                lComando.Parameters.Add("@P_mes_ini", SqlDbType.Int).Value = ddlMesIni.SelectedValue;
                if (TxtAnoFin.Text == "")
                    lComando.Parameters.Add("@P_año_fin", SqlDbType.Int).Value = "0";
                else
                    lComando.Parameters.Add("@P_año_fin", SqlDbType.Int).Value = TxtAnoFin.Text.Trim();
                lComando.Parameters.Add("@P_mes_fin", SqlDbType.Int).Value = ddlMesFinal.SelectedValue;
                lComando.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = ddlOperador.SelectedValue;
                lComando.Parameters.Add("@P_codigo_punto_snt", SqlDbType.Int).Value = ddlFuente.SelectedValue;
                lComando.Parameters.Add("@P_indicador", SqlDbType.VarChar).Value = lsIndica;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgMaestro.DataSource = lds;
                dtgMaestro.DataBind();
                tblGrilla.Visible = true;
                imbExcel.Visible = true;
                lConexion.Cerrar();
                if (lsIndica == "P")
                    lblMensaje.Text = "PTDVF/CIDVF publicada correctamente";
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString();
            }
        }

    }

}