﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_PrecioReservaC1C2.aspx.cs"
    Inherits="Informes_frm_PrecioReservaC1C2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Año Subasta</label>
                            <asp:TextBox ID="TxtAno" runat="server" MaxLength="5" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="fteBTxtAno" runat="server" TargetControlID="TxtAno"
                                FilterType="Custom, Numbers">
                            </ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div id="tdMes02" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                        <div class="form-group">
                            <label for="Name">Operador</label>
                            <asp:DropDownList ID="ddlOperador" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="Div1" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                        <div class="form-group">
                            <label for="Name">Fuente</label>
                            <asp:DropDownList ID="ddlFuente" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                        id="tblMensaje">

                        <tr>
                            <td colspan="3" align="center">
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="detalle" />
                                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red" Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div runat="server" visible="false"
                    id="tblGrilla">

                    <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                        <%--20180126 rq107-16--%>
                        <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False"
                            Width="100%" CssClass="table-bordered">

                            <Columns>
                                <asp:BoundColumn DataField="año_subasta" HeaderText="Año Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_fuente" HeaderText="Nombre Fuente" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="precio_reserva" HeaderText="Precio Reserva" ItemStyle-HorizontalAlign="right"
                                    DataFormatString="{0:###,###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
