﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_CurvaOfertayDemandaAgregada.aspx.cs" Inherits="Informes.frm_CurvaOfertayDemandaAgregada" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/dev/auctionChart.js")%>"></script>

    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Subasta" AssociatedControlID="ddlSubasta" runat="server" />
                            <asp:DropDownList ID="ddlSubasta" runat="server" OnSelectedIndexChanged="ddlSubasta_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <%--20210224 ajuste transporte--%>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tipo Rueda" AssociatedControlID="ddlTipoRueda" runat="server" />
                            <asp:DropDownList ID="ddlTipoRueda" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Modalidad Contractual" AssociatedControlID="ddlModalidad" runat="server" />
                            <asp:DropDownList ID="ddlModalidad" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Producto" AssociatedControlID="DdlProducto" runat="server" />
                            <asp:DropDownList ID="DdlProducto" runat="server" OnSelectedIndexChanged="DdlProducto_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="G">Suministro de gas</asp:ListItem>
                                <asp:ListItem Value="T">Capacidad de transporte</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Rueda" AssociatedControlID="TxtFechaIni" runat="server" />
                            <asp:TextBox ID="TxtFechaIni" Width="100%" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker" placeholder="yyyy/mm/dd" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Número Rueda" AssociatedControlID="TxtNoRueda" runat="server" />
                            <asp:TextBox ID="TxtNoRueda" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Número ID" AssociatedControlID="TxtNoID" runat="server" />
                            <asp:TextBox ID="TxtNoID" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblPuntoEnt" AssociatedControlID="ddlPuntoEnt" runat="server" />
                            <asp:DropDownList ID="ddlPuntoEnt" runat="server" Visible="false" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                </div>
                <%--Grillas--%>
                <div class="table table-responsive">
                    <asp:DataGrid ID="dtgCurva" runat="server" AutoGenerateColumns="False" AllowPaging="true" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered"
                        OnItemCommand="dtgCurva_EditCommand" OnPageIndexChanged="dtgCurva_PageIndexChanged" PageSize="10">
                        <Columns>
                            <asp:BoundColumn DataField="numero_id" HeaderText="No. Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_rueda" HeaderText="No. Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_contrato" HeaderText="Fecha Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="tipo_modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="pto_entrega" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_entrega" HeaderText="Fecha Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" DataFormatString="{0: ###,###,##0}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="Precio" DataFormatString="{0: ###,###,##0.00}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            <%--20180126 rq107-16--%>
                            <asp:EditCommandColumn HeaderText="Gráfica" EditText="Gráfica" Visible="False" />
                            <asp:BoundColumn DataField="destino_rueda" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                <ItemTemplate>
                                    <div class="dropdown dropdown-inline">
                                        <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="flaticon-more-1"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                            <!--begin::Nav-->
                                            <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <ul class="kt-nav">
                                                        <li class="kt-nav__item">
                                                            <asp:LinkButton ID="lkbComplementar" CssClass="kt-nav__link" CommandName="Gráfica" runat="server">
                                                                <i class="kt-nav__link-icon flaticon2-chart"></i>
                                                                <span class="kt-nav__link-text">Gráfica</span>
                                                            </asp:LinkButton>
                                                        </li>
                                                    </ul>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <!--end::Nav-->
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                    </asp:DataGrid>
                </div>
                <%--Excel--%>
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False" Visible="false">
                            <Columns>
                                <asp:BoundColumn DataField="numero_id" HeaderText="No. Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numero_rueda" HeaderText="No. Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_contrato" HeaderText="Fecha Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tipo_modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pto_entrega" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_entrega" HeaderText="Fecha Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" DataFormatString="{0: ###,###,##0}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="precio" HeaderText="Precio" DataFormatString="{0: ###,###,##0.00}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            </Columns>
                            <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                        </asp:DataGrid>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <%--Modal Curva de Oferta y Demanda Agregada--%>
    <div class="modal fade" id="mdlCurvaOfertaDemAgre" tabindex="-1" role="dialog" aria-labelledby="mdlCurvaOfertaDemAgreLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlCurvaOfertaDemAgreInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="mdlCurvaOfertaDemAgreLabel">Curva de Oferta y Demanda Agregada</h5>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:Label Text="No. Id" AssociatedControlID="lblNoId" runat="server" />
                            <asp:Label ID="lblNoId" Font-Bold="true" ForeColor="#dc3912" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <div id="chart_div" style="width: 700px; height: 500px; display: block; margin: 0 auto !important;"></div>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <asp:UpdatePanel runat="server">
                                            <ContentTemplate>
                                                <table class="table table-striped">
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Label Text="Precio de Adjudicación" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Label ID="lblPrecioAdjudicacion" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Label Text="Cantidad Adjudicada" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Label ID="lblCantidadAdjudicada" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Label ID="lblTipoPunto" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Label ID="lblPunto" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="table table-responsive">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <asp:DataGrid ID="dtgInformacion" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                            ViewStateMode="Enabled">
                                            <Columns>
                                                <asp:BoundColumn DataField="precio" HeaderText="Precio" DataFormatString="{0: ###,###,##0.00}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="cantidad_venta" HeaderText="Cantidad Venta" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,##0}"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="cantidad_compra" HeaderText="Cantidad Compra" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,##0}"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="cantidad_min" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="cantidad_max" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="precio_min" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="precio_max" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="precio_venta" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="precio_compra" Visible="false"></asp:BoundColumn>
                                                <%--20210224--%>
                                                <asp:BoundColumn DataField="grafica_compra" Visible="false"></asp:BoundColumn>
                                            </Columns>
                                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                                        </asp:DataGrid>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseCurvaOferDemAgre_Click" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
