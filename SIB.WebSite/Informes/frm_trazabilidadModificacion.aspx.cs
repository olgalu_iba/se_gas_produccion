﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Informes
{
    public partial class frm_trazabilidadModificacion : Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Consulta de Trazabilidad de Modificación de Operaciones";
        clConexion lConexion = null;
        SqlDataReader lLector;
        DataSet lds = new DataSet();
        SqlDataAdapter lsqldata = new SqlDataAdapter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo.ToString();
            lConexion = new clConexion(goInfo);
            //Se agrega el comportamiento del botón
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.FiltrarOnclick += btnConsultar_Click;

            if (IsPostBack) return;

            //Titulo
            Master.Titulo = "Informes";

            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles1(lConexion.gObjConexion, ddlSubasta, "", "", 0, 1);
            LlenarControles2(lConexion.gObjConexion, ddlCausa, "", "", 0, 1);
            if (goInfo.cod_comisionista == "0")
            {
                LlenarControles3(lConexion.gObjConexion, ddlContraparte, "m_operador ope", " estado = 'A' and codigo_operador !=0  order by codigo_operador", 0, 4);
                LlenarControles3(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador !=0 order by codigo_operador", 0, 4);
                tr01.Visible = false;
                tr02.Visible = true;
                tr03.Visible = true;
            }
            else
            {
                LlenarControles3(lConexion.gObjConexion, ddlContraparte, "m_operador ope", " estado = 'A' and codigo_operador !=0 and (exists (select 1 from t_contrato_verificacion ver where ver.estado ='R' and ver.operador_compra = " + goInfo.cod_comisionista + " and ver.operador_venta = ope.codigo_operador) or exists(select 1 from t_contrato_verificacion ver where ver.estado = 'R' and ver.operador_venta = " + goInfo.cod_comisionista + " and ver.operador_compra = ope.codigo_operador)) order by codigo_operador", 0, 4);
                LlenarControles3(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador !=0 order by codigo_operador", 0, 4);
                tr01.Visible = true;
                tr02.Visible = false;
                tr03.Visible = false;
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
            }
            lConexion.Cerrar();
            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar };
            buttons.Inicializar(botones: botones);
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string lsNombreArchivo = goInfo.Usuario.ToString() + "InfNivelContTra" + DateTime.Now + ".xls";
            string lstitulo_informe = "Informe de trazabilidad de operaciones";
            string lsTituloParametros = "";
            try
            {
                if (TxtOperIni.Text.Trim().Length > 0)
                    lsTituloParametros += " - Operación: " + TxtOperIni.Text.Trim();
                if (TxtNoId.Text.Trim().Length > 0)
                    lsTituloParametros += " - Id Registro: " + TxtNoId.Text.Trim();
                if (TxtFechaIni.Text.Trim().Length > 0)
                    lsTituloParametros += " - Fecha Operación inicial: " + TxtFechaIni.Text.Trim();
                if (TxtFechaFin.Text.Trim().Length > 0)
                    lsTituloParametros += " - Fecha Operación final : " + TxtFechaFin.Text.Trim();
                if (TxtFechaModIni.Text.Trim().Length > 0)
                    lsTituloParametros += " - Fecha modificación inicial: " + TxtFechaIni.Text.Trim();
                if (TxtFechaModFin.Text.Trim().Length > 0)
                    lsTituloParametros += " - Fecha modificación final : " + TxtFechaFin.Text.Trim();
                if (TxtContratoDef.Text.Trim().Length > 0)
                    lsTituloParametros += " - Contrato : " + TxtContratoDef.Text.Trim();
                if (ddlSubasta.SelectedValue != "0")
                    lsTituloParametros += " - Subasta : " + ddlSubasta.SelectedItem.ToString();
                if (ddlProducto.SelectedValue != "")
                    lsTituloParametros += " - Producto: " + ddlProducto.SelectedItem.ToString();
                if (ddlCausa.SelectedValue != "0")
                    lsTituloParametros += " - Causa Modificación: " + ddlCausa.SelectedItem.ToString();
                if (ddlContraparte.SelectedValue != "0")
                    lsTituloParametros += " - Operador Contraparte: " + ddlContraparte.SelectedItem.ToString();
                if (ddlPunta.SelectedValue != "")
                    lsTituloParametros += " - Punta: " + ddlPunta.SelectedItem.ToString();
                if (ddlOperador.SelectedValue != "0")
                    lsTituloParametros += " - Operador: " + ddlOperador.SelectedItem.ToString();

                dtgMaestro.Columns[48].Visible = false; //20210707 trm moneda
                decimal ldCapacidad = 0;
                StringBuilder lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                dtgMaestro.EnableViewState = false;
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                lform.Controls.Add(dtgMaestro);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = System.Text.Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());

                Response.End();
                lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
                dtgMaestro.Columns[48].Visible = true; //2021077 trm moneda
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pudo Generar la Excel.! " + ex.Message.ToString());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            var lblMensaje = new StringBuilder();
            DateTime ldFecha = DateTime.Now;
            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato inválido en el campo fecha de negociación inicial. <br>");
                }
            }
            if (TxtFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    if (TxtFechaIni.Text == "")
                    {
                        lblMensaje.Append("Debe seleccionar la fecha de negociación inicial antes que la final<br>");
                    }
                    else
                    {
                        if (Convert.ToDateTime(TxtFechaFin.Text) < ldFecha)
                        {
                            lblMensaje.Append("La fecha de negociación final debe ser mayor que la inicial. <br>");
                        }
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato inválido en el campo fecha de negociación inicial. <br>");
                }
            }
            if (TxtFechaModIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaModIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato inválido en el campo Fecha de modificación inicial. <br>");
                }
            }
            if (TxtFechaModFin.Text.Trim().Length > 0)
            {
                try
                {
                    if (TxtFechaModIni.Text == "")
                    {
                        lblMensaje.Append("Debe seleccionar la fecha de modificación inicial antes que la final<br>");
                    }
                    else
                    {
                        if (Convert.ToDateTime(TxtFechaModFin.Text) < ldFecha)
                        {
                            lblMensaje.Append("La fecha de modificación final debe ser mayor que la inicial. <br>");
                        }
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato inválido en el campo fecha de modificación inicial. <br>");
                }
            }

            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                Toastr.Warning(this, lblMensaje.ToString());
                return;
            }

            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                var lComando = new SqlCommand
                {
                    Connection = lConexion.gObjConexion,
                    CommandTimeout = 3600,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "pa_GetTrazaModifCont"
                };
                if (TxtFechaIni.Text.Trim().Length <= 0)
                {
                    lComando.Parameters.Add("@P_fecha_neg_ini", SqlDbType.VarChar).Value = "";
                    lComando.Parameters.Add("@P_fecha_neg_fin", SqlDbType.VarChar).Value = "";
                }
                else
                {
                    lComando.Parameters.Add("@P_fecha_neg_ini", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                    if (TxtFechaFin.Text.Trim().Length <= 0)
                        lComando.Parameters.Add("@P_fecha_neg_fin", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                    else
                        lComando.Parameters.Add("@P_fecha_neg_fin", SqlDbType.VarChar).Value = TxtFechaFin.Text.Trim();
                }

                if (TxtFechaModIni.Text.Trim().Length <= 0)
                {
                    lComando.Parameters.Add("@P_fecha_modif_ini", SqlDbType.VarChar).Value = "";
                    lComando.Parameters.Add("@P_fecha_modif_fin", SqlDbType.VarChar).Value = "";
                }
                else
                {
                    lComando.Parameters.Add("@P_fecha_modif_ini", SqlDbType.VarChar).Value = TxtFechaModIni.Text.Trim();
                    if (TxtFechaModFin.Text.Trim().Length <= 0)
                        lComando.Parameters.Add("@P_fecha_modif_fin", SqlDbType.VarChar).Value =
                            TxtFechaModIni.Text.Trim();
                    else
                        lComando.Parameters.Add("@P_fecha_modif_fin", SqlDbType.VarChar).Value =
                            TxtFechaModFin.Text.Trim();
                }

                if (TxtOperIni.Text.Trim().Length <= 0)
                    lComando.Parameters.Add("@P_operacion", SqlDbType.Int).Value = "0";
                else
                    lComando.Parameters.Add("@P_operacion", SqlDbType.Int).Value = TxtOperIni.Text.Trim();
                if (TxtNoId.Text.Trim().Length <= 0)
                    lComando.Parameters.Add("@P_codigo_verif", SqlDbType.Int).Value = "0";
                else
                    lComando.Parameters.Add("@P_codigo_verif", SqlDbType.Int).Value = TxtNoId.Text.Trim();
                lComando.Parameters.Add("@P_contrato_definitivo", SqlDbType.VarChar).Value = TxtContratoDef.Text.Trim();
                lComando.Parameters.Add("@P_codigo_tipo_subasta", SqlDbType.VarChar).Value = ddlSubasta.SelectedValue;
                lComando.Parameters.Add("@P_producto", SqlDbType.VarChar).Value = ddlProducto.SelectedValue;
                lComando.Parameters.Add("@P_codigo_causa", SqlDbType.VarChar).Value = ddlCausa.SelectedValue;
                lComando.Parameters.Add("@P_codigo_contraparte", SqlDbType.Int).Value = ddlContraparte.SelectedValue;
                lComando.Parameters.Add("@P_punta", SqlDbType.VarChar).Value = ddlPunta.SelectedValue;
                lComando.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = ddlOperador.SelectedValue;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgMaestro.DataSource = lds;
                dtgMaestro.DataBind();
                dtgMaestro.Visible = true;
                buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se pudo generar el informe.! ");
            }
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            SqlCommand lComando = new SqlCommand();
            lComando.Connection = lConexion.gObjConexion;
            lComando.CommandTimeout = 3600;
            lComando.CommandType = CommandType.StoredProcedure;
            lComando.CommandText = "pa_GetTrazaModifContUsr";
            lComando.Parameters.Add("@P_tipo", SqlDbType.VarChar).Value = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            lComando.Parameters.Add("@P_codigo", SqlDbType.Int).Value = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[1].Text;
            lComando.ExecuteNonQuery();
            lsqldata.SelectCommand = lComando;
            lsqldata.Fill(lds);
            dtgDetalle.DataSource = lds;
            dtgDetalle.DataBind();
            lConexion.Cerrar();
            //   tblDatos.Visible = false;
            dtgDetalle.Visible = true;
            dtgMaestro.Visible = false;
            tblDetalle.Visible = true;
            buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            dtgDetalle.Visible = false;
            dtgMaestro.Visible = true;
            tblDetalle.Visible = false;
            buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
        }

        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
            //dtgComisionista.DataBind();

            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetTiposSubastaAdc", null, null, null);
            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);
            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["descripcion"].ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles2(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
            //dtgComisionista.DataBind();

            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetCausaModAdc", null, null, null);
            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);
            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["descripcion"].ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles2
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles3(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20170814 rq036-17
        protected void ddlMercado_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlProducto.Items.Clear();
            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "";
            lItem.Text = "Seleccione";
            ddlProducto.Items.Add(lItem);
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = "G";
            lItem1.Text = "Suministro de gas";
            ddlProducto.Items.Add(lItem1);
            System.Web.UI.WebControls.ListItem lItem2 = new System.Web.UI.WebControls.ListItem();
            lItem2.Value = "T";
            lItem2.Text = "Capacidad de transporte";
            ddlProducto.Items.Add(lItem2);
            if (ddlMercado.SelectedValue == "" || ddlMercado.SelectedValue == "O")
            {
                System.Web.UI.WebControls.ListItem lItem3 = new System.Web.UI.WebControls.ListItem();
                lItem3.Value = "A";
                lItem3.Text = "Suministro y transporte";
                ddlProducto.Items.Add(lItem3);
            }
        }
    }
}