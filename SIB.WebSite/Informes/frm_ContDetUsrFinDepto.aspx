﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ContDetUsrFinDepto.aspx.cs"
    Inherits="Informes_frm_ContDetUsrFinDepto" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
            width="100%">
            <tr>
                <td align="center" class="th1" style="width: 80%;">
                    <asp:Label ID="lblTitulo" runat="server" ForeColor="White" ></asp:Label>
                </td>
                <td align="center" class="th1" style="width: 20%;">
                    <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/exel 2007 3D.png"
                        Height="40" OnClick="imbExcel_Click" />
                </td>
            </tr>
        </table>
    <br /><br /><br /><br /><br /><br />
        <asp:UpdateProgress ID="panelUpdateProgress" DisplayAfter="0" runat="server" AssociatedUpdatePanelID="UptPnlTodo">
            <ProgressTemplate>
                <asp:Panel ID="PnlCarga" runat="server" Style="position: relative; top: 30%; text-align: center;"
                    CssClass="popup">
                    <div class="fuente">
                        <img src="../Images/ajax-loader.gif" style="vertical-align: middle" alt="Procesando" />
                        Procesando por Favor espere ...
                    </div>
                </asp:Panel>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="UptPnlTodo" runat="server">
            <ContentTemplate>
                <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
                    width="90%">
                    <tr>
                        <td class="td1">Número Operación Inicial
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtOperIni" runat="server" ValidationGroup="detalle" Width="120px"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="fteTxtOperIni" runat="server" TargetControlID="TxtOperIni"
                                FilterType="Custom, Numbers">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                        <td class="td1">Número Operación Final
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtOperFin" runat="server" ValidationGroup="detalle" Width="120px"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="fteTxtOperFin" runat="server" TargetControlID="TxtOperFin"
                                FilterType="Custom, Numbers">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">Fecha Negociación Inicial
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </td>
                        <td class="td1">Fecha Negociación Final
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td class="td1">Operador Compra
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlOperadorC" runat="server" Width="200px">
                            </asp:DropDownList>
                        </td>
                        <td class="td1">Operador Venta
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlOperadorV" runat="server" Width="250px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">Tipo Subasta
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlTpoSub" runat="server" Width="200px">
                            </asp:DropDownList>
                        </td>
                        <td class="td1">Tipo mercado
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlMercado" runat="server" Width="250px">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="P">Primario</asp:ListItem>
                                <asp:ListItem Value="S">Secundario</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">Tipo Producto
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlProducto" runat="server" Width="200px">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="G">Suministro de gas</asp:ListItem>
                                <asp:ListItem Value="T">Capacidad de transporte</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="td1">Estado Operación
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlEstado" runat="server" Width="250px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">Departamento
                        </td>
                        <td class="td2" colspan="3">
                            <asp:DropDownList ID="DdlDepto" runat="server" Width="200px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="th1" colspan="4" align="center">
                            <asp:Button ID="BtnBuscar" runat="server" Text="Consultar" OnClick="BtnBuscar_Click" />
                        </td>
                    </tr>
                </table>
                </div>
            <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                id="tblMensaje">
                <tr>
                    <td colspan="3" align="center">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
                <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                    id="tblGrilla" visible="false">
                    <tr>
                        <td colspan="3" align="center">
                            <div style="overflow: scroll; width: 1220px; height: 450px;">
                                <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                    AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                                    <Columns>
                                        <asp:BoundColumn DataField="tipo_subasta" HeaderText="Tipo Subasta"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="tipo_rueda" HeaderText="Tipo Rueda"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="numero_operacion" HeaderText="Operación"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="vendedor" HeaderText="Vendedor"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="codigo_vendedor" HeaderText="Cod. Venta"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="rol_vendedor" HeaderText="Rol venta"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="comprador" HeaderText="Comprador"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="codigo_comprador" HeaderText="Cod. Compra"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="rol_comprador" HeaderText="Rol Compra"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="codigo_sector_consumo" HeaderText="Cod. Sector Consumo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_sector_consumo" HeaderText="Sector de Consumo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Cod. Punto Salida"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_punto_salida" HeaderText="Punto de salida"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="cantidad_capacidad" HeaderText="Cantidad/Capacidad" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_tipo_demanda" HeaderText="Descripción Tipo Demanda"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="codigo_mercado_relevante" HeaderText="Cod. Mercado Relev."></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_mercado_relevante" HeaderText="Mercado Relevante"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="nit_usuario_final" HeaderText="Nit Usr. Final"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="nombre_usuario_final" HeaderText="Usuario Final"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="estado" HeaderText="Estado"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="carga_extemporanea" HeaderText="Actualización/Extemporánea"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="equivalente_kpcd" HeaderText="Cantidad Equiv. KPCD" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="codigo_depto" HeaderText="Cod. Depto."></asp:BoundColumn>
                                        <asp:BoundColumn DataField="nombre_departamento" HeaderText="Departamento"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="codigo_ciudad" HeaderText="Cod. Mpio."></asp:BoundColumn>
                                        <asp:BoundColumn DataField="nombre_ciudad" HeaderText="Municipio"></asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </div>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>