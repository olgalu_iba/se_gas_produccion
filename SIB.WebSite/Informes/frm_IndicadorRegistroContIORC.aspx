﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_IndicadorRegistroContIORC.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Informes_frm_IndicadorRegistroContIORC" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">

                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Inicial</label>
                            <asp:TextBox ID="TxtFechaIni" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static" Width="100%"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RfvTxtFechaFin" runat="server" ErrorMessage="El Campo Fechs Final es obligatorio"
                                ControlToValidate="TxtFechaFin" ValidationGroup="detalle">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Final</label>
                            <asp:TextBox ID="TxtFechaFin" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static" Width="100%"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="El Campo Fechs Final es obligatorio"
                                ControlToValidate="TxtFechaFin" ValidationGroup="detalle">*</asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div runat="server"
                        id="tblGrilla" visible="false">
                        <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                            <asp:DataGrid ID="dtgSistema" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered">

                                <Columns>
                                    <asp:BoundColumn DataField="numero_operacion" HeaderText="No. Operacion" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_ult_modifica" HeaderText="Fecha Ult. Modifica Opera"
                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_verifica" HeaderText="Fecha Verif gestor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="cumplimiento" HeaderText="Cumplimiento"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </div>

                        <div>
                               <asp:Label ID="lblMensaje" runat="server" Font-Bold="true" Font-Size="14px" ForeColor="Red"></asp:Label>
                            <asp:Label ID="lblInd1" runat="server" Text="Indicador Oportunidad Publicacion" Font-Bold="true"
                                Font-Size="12px" ForeColor="Red"></asp:Label>

                            <asp:Label ID="lblResInd1" runat="server" Font-Bold="true" Font-Size="14px" ForeColor="Red"></asp:Label>

                           
                        </div>
                        <div>

                            <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                                <asp:DataGrid ID="dtgExterno" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                    PagerStyle-HorizontalAlign="Center">

                                    <Columns>
                                        <asp:BoundColumn DataField="numero_operacion" HeaderText="operacion" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="fecha_ult_modifica" HeaderText="Fecha Ult. Modifica Opera"
                                            ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="fecha_verifica" HeaderText="Fecha Verif gestor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="cumplimiento" HeaderText="Cumplimiento"></asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </div>

                        </div>
                        <div>

                            <asp:Label ID="lblInd2" runat="server" Text="Indicador Oportunidad Publicacion" Font-Bold="true"
                                Font-Size="12px" ForeColor="Red"></asp:Label>

                            <asp:Label ID="lblResInd2" runat="server" Font-Bold="true" Font-Size="14px" ForeColor="Red"></asp:Label>

                        </div>
                        <div>

                            <asp:Label ID="lblIndTotal" runat="server" Text="Indicador Oportunidad Publicacion IORC"
                                Font-Bold="true" Font-Size="12px" ForeColor="Red"></asp:Label>

                            <asp:Label ID="lblResIndTotal" runat="server" Font-Bold="true" Font-Size="14px" ForeColor="Red"></asp:Label>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
