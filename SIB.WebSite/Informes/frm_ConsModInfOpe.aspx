﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ConsModInfOpe.aspx.cs"
    Inherits="Informes_frm_ConsModInfOpe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Tipo de Información:</label>
                           <asp:DropDownList ID="ddlReporte" runat="server" Width="300px" CssClass="form-control selectpicker" data-live-search="true">
                        </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Inicial:</label>
                           <asp:TextBox ID="TxtFechaIni" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>

                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Final:</label>
                          <asp:TextBox ID="TxtFechaFin" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>

             
                </div>
                   <div border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblGrilla" visible="false">
       
                   <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                        <asp:DataGrid ID="dtgMaestro1" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" 
                            OnEditCommand="dtgMaestro1_EditCommand" >
                          
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_inyectada" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Cod Pto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="desc_tipo_prd" HeaderText="Tipo Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="campo_prod" HeaderText="Cod Campo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20161124--%>
                                <%--20180126 rq107-16--%>
                                <asp:BoundColumn DataField="desc_campo" HeaderText="Campo Producción" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="ingresa_snt" HeaderText="Ingresa SNT" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:EditCommandColumn HeaderText="Detalle" EditText="Detalle" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgMaestro2" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" 
                            OnEditCommand="dtgMaestro2_EditCommand">
                           
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_cons_nal" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Cod Campo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20161124--%>
                                <%--20180126 rq107-16--%>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Campo producción" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="ingresa_snt" HeaderText="Ingresa SNT" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:EditCommandColumn HeaderText="Detalle" EditText="Detalle" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgMaestro3" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" 
                            OnEditCommand="dtgMaestro3_EditCommand">
                         
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_exportar" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="ingresa_snt" HeaderText="Ingresa SNT" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:EditCommandColumn HeaderText="Detalle" EditText="Detalle" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgMaestro4" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" 
                            OnEditCommand="dtgMaestro4_EditCommand">
                           
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_parqueo" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_tramo" HeaderText="Cod Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Cod Pto Sal" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_remitente" HeaderText="Cod Remitente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_remitente" HeaderText="Remitente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="punto_trasferencia" HeaderText="Pto Transf" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="desc_transferencia" HeaderText="Punto Transferencia" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Capacidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="codigo_causa_mod" HeaderText="Cod Causa Mod" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="desc_causa_mod" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:EditCommandColumn HeaderText="Detalle" EditText="Detalle" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgMaestro5" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" 
                            OnEditCommand="dtgMaestro5_EditCommand">
                         
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_recibida" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Cod Punto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="codigo_causa_mod" HeaderText="Cod Causa Mod" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="desc_causa_mod" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>

                                <asp:EditCommandColumn HeaderText="Detalle" EditText="Detalle" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgMaestro6" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" 
                            OnEditCommand="dtgMaestro6_EditCommand">
                       
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_suministrar" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Cod Punto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="ingresa_snt" HeaderText="Ingresa SNT" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:EditCommandColumn HeaderText="Detalle" EditText="Detalle" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgMaestro7" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" 
                            OnEditCommand="dtgMaestro7_EditCommand" >
                       
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_tomada" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_tramo" HeaderText="Cod Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Cod Pto Sal" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_remitente" HeaderText="Cod Remitente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_remitente" HeaderText="Remitente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Capacidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="codigo_causa_mod" HeaderText="Cod Causa Mod" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="desc_causa_mod" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:EditCommandColumn HeaderText="Detalle" EditText="Detalle" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgMaestro8" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center"
                            OnEditCommand="dtgMaestro8_EditCommand">
                         
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_trasferida" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="punto_trasferencia" HeaderText="Cod Pto Transf" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="desc_trasferencia" HeaderText="Punto Transferencia" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_trasportador" HeaderText="Cod Transportador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_trasportador" HeaderText="Transportador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Capacidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="codigo_causa_mod" HeaderText="Cod Causa Mod" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="desc_causa_mod" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:EditCommandColumn HeaderText="Detalle" EditText="Detalle" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgMaestro9" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" 
                            OnEditCommand="dtgMaestro9_EditCommand" >
                       
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_trasportar" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Capacidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="codigo_causa_mod" HeaderText="Cod Causa Mod" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="desc_causa_mod" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:EditCommandColumn HeaderText="Detalle" EditText="Detalle" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgMaestro10" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" 
                            OnEditCommand="dtgMaestro10_EditCommand">
                           
                            <Columns>
                                <asp:BoundColumn DataField="codigo_ent_usuario_final" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_tramo" HeaderText="Cod tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Cod Pto Sal" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_tipo_demanda" HeaderText="Cod Tpo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanada" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_sector_consumo" HeaderText="Cod Sector Cons" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_sector" HeaderText="Sector de Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:EditCommandColumn HeaderText="Detalle" EditText="Detalle" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                        <%--20190306 rq013-19--%>
                        <asp:DataGrid ID="dtgMaestro11" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" 
                            OnEditCommand="dtgMaestro11_EditCommand">
                         
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_gasoducto" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_gasoducto" HeaderText="Cod Gasoducto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_gasoducto" HeaderText="Gasoducto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ingresa_snt" HeaderText="Ingresa SNT" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:EditCommandColumn HeaderText="Detalle" EditText="Detalle" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                        <%--20190306 rq013-19--%>
                        <asp:DataGrid ID="dtgMaestro12" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" 
                            OnEditCommand="dtgMaestro12_EditCommand">
                         
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_gnc" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ingresa_snt" HeaderText="Ingresa SNT" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:EditCommandColumn HeaderText="Detalle" EditText="Detalle" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
 
                        <%--20190306 rq013-19--%>
                        <asp:DataGrid ID="dtgMaestro13" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" 
                            OnEditCommand="dtgMaestro13_EditCommand">
                          
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_estacion" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_estacion" HeaderText="Cod Estación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_estacion" HeaderText="Estación Compresión" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tipo_operacion" HeaderText="Tipo Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_causa_mod" HeaderText="Cod Causa Mod" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_causa_mod" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:EditCommandColumn HeaderText="Detalle" EditText="Detalle" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                        <%--20190306 rq013-19--%>
                        <asp:DataGrid ID="dtgMaestro14" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" 
                            OnEditCommand="dtgMaestro14_EditCommand">
                          
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_asignacion" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_remitente" HeaderText="Cod Remitente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_remitente" HeaderText="Remitente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Cod Pto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto_sal" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_tramo" HeaderText="Cod Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_causa_mod" HeaderText="Cod Causa Mod" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_causa_mod" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:EditCommandColumn HeaderText="Detalle" EditText="Detalle" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>

                        </div>
           
        </div>
        <div border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tbDetalle" visible="false">
          
                    <asp:Label ID="lblTipoIinf" runat="server"></asp:Label>
             
                    <asp:Button ID="BtnRegresar" runat="server" Text="Regresar" OnClick="BtnRegresar_Click" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:ImageButton ID="imbExcel1" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel1_Click"
                    Height="35" />
                    <asp:HiddenField ID="hdnRegistro" runat="server" />
                    <br />
             
                    <div style="overflow: scroll; width: 1050px; height: 350px;">
                        <asp:DataGrid ID="dtgDetalle1" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center">
                       
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_inyectada" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Cod Pto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="desc_tipo_prd" HeaderText="Tipo Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="campo_prod" HeaderText="Cod Campo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20161124--%>
                                <%--20180126 rq107-16--%>
                                <asp:BoundColumn DataField="desc_campo" HeaderText="Campo Producción" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="ingresa_snt" HeaderText="Ingresa SNT" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="login_usuario" HeaderText="usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="fecha-hora" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgDetalle2" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" >
                           
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_cons_nal" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Cod Campo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20161124--%>
                                <%--20180126 rq107-16--%>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Campo producción" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="ingresa_snt" HeaderText="Ingresa SNT" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="login_usuario" HeaderText="usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="fecha-hora" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgDetalle3" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" >
                        
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_exportar" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="ingresa_snt" HeaderText="Ingresa SNT" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="login_usuario" HeaderText="usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="fecha-hora" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgDetalle4" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" >
                          
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_parqueo" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_tramo" HeaderText="Cod Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Cod Pto Sal" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_remitente" HeaderText="Cod Remitente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_remitente" HeaderText="Remitente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="punto_trasferencia" HeaderText="Pto Transf" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="desc_transf" HeaderText="Punto Transferencia" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Capacidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="codigo_causa_mod" HeaderText="Cod Causa Mod" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="desc_causa_mod" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="login_usuario" HeaderText="usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="fecha-hora" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgDetalle5" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center">
                           
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_recibida" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Cod Punto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>

                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="codigo_causa_mod" HeaderText="Cod Causa Mod" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="desc_causa_mod" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="login_usuario" HeaderText="usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="fecha-hora" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgDetalle6" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" >
                      
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_suministrar" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Cod Punto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="ingresa_snt" HeaderText="Ingresa SNT" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="login_usuario" HeaderText="usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="fecha-hora" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgDetalle7" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" >
                           
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_tomada" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_tramo" HeaderText="Cod Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Cod Pto Sal" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_remitente" HeaderText="Cod Remitente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_remitente" HeaderText="Remitente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Capacidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="codigo_causa_mod" HeaderText="Cod Causa Mod" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="desc_causa_mod" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>

                                <asp:BoundColumn DataField="login_usuario" HeaderText="usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="fecha-hora" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgDetalle8" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center">
                        
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_trasferida" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="punto_trasferencia" HeaderText="Cod Pto Trans" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_trasferencia" HeaderText="Punto Transferencia" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_trasportador" HeaderText="Cod Transportador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_trasportador" HeaderText="Transportador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Capacidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="codigo_causa_mod" HeaderText="Cod Causa Mod" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="desc_causa_mod" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>

                                <asp:BoundColumn DataField="login_usuario" HeaderText="usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="fecha-hora" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgDetalle9" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" >
                           
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_trasportar" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Capacidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="codigo_causa_mod" HeaderText="Cod Causa Mod" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="desc_causa_mod" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>

                                <asp:BoundColumn DataField="login_usuario" HeaderText="usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="fecha-hora" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgDetalle10" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center">
                          
                            <Columns>
                                <asp:BoundColumn DataField="codigo_ent_usuario_final" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_tramo" HeaderText="Cod tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Cod Pto Sal" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_tipo_demanda" HeaderText="Cod Tpo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanada" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_sector_consumo" HeaderText="Cod Sector Cons" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_sector" HeaderText="Sector de Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20161124--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="login_usuario" HeaderText="usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="fecha-hora" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>

                           <%--20190306 rq013-19--%>
                        <asp:DataGrid ID="dtgDetalle11" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center">
                          
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_gasoducto" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_gasoducto" HeaderText="Cod Gasoducto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_gasoducto" HeaderText="Gasoducto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ingresa_snt" HeaderText="Ingresa SNT" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="login_usuario" HeaderText="usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="fecha-hora" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>

                           <%--20190306 rq013-19--%>
                        <asp:DataGrid ID="dtgDetalle12" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center">
                          
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_gnc" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ingresa_snt" HeaderText="Ingresa SNT" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="login_usuario" HeaderText="usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="fecha-hora" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>

                           <%--20190306 rq013-19--%>
                        <asp:DataGrid ID="dtgDetalle13" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" >
                           
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_estacion" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_estacion" HeaderText="Cod Estación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_estacion" HeaderText="Estación Compresión" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tipo_operacion" HeaderText="Tipo Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_causa_mod" HeaderText="Cod Causa Mod" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_causa_mod" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="login_usuario" HeaderText="usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="fecha-hora" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>

                           <%--20190306 rq013-19--%>
                        <asp:DataGrid ID="dtgDetalle14" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                            PagerStyle-HorizontalAlign="Center" >
                        
                            <Columns>
                                <asp:BoundColumn DataField="codigo_energia_asignacion" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_remitente" HeaderText="Cod Remitente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_remitente" HeaderText="Remitente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Cod Pto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto_sal" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_tramo" HeaderText="Cod Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_causa_mod" HeaderText="Cod Causa Mod" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_causa_mod" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="login_usuario" HeaderText="usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="fecha-hora" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>

                    </div>
              
        </div>
            </div>
        </div>
    </div>   
  <div border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblMensaje">
          <%--  <tr>
                <td class="th1" colspan="3" align="center">
                    <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                        OnClick="imbConsultar_Click" Height="40" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
                </td>
            </tr>--%>
         
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
              
        </div>
     
</asp:Content>