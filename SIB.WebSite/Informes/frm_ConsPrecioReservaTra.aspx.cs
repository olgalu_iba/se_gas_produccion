﻿using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
namespace Informes
{
    public partial class Informes_frm_ConsPrecioReservaTra : System.Web.UI.Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Consulta precios de reserva subasta transporte ante congestión contractual"; //20220211 ajsute lables
        clConexion lConexion = null;
        SqlDataReader lLector;
        DataSet lds = new DataSet();
        SqlDataAdapter lsqldata = new SqlDataAdapter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo;
            lConexion = new clConexion(goInfo);
            //Se agrega el comportamiento del botón
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.FiltrarOnclick += btnConsultar_Click;

            if (IsPostBack) return;

            Master.Titulo = "Informes";

            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Excel };
            buttons.Inicializar(botones: botones);
            cargarDatos();

        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            dtgMaestro.CurrentPageIndex = 0; //20180126 rq107 - 16
            cargarDatos();
        }

        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// //20180126 rq107 - 16
        protected void cargarDatos()
        {
            var lblMensaje = new StringBuilder();

            string[] lsNombreParametros = { "@P_indicador" };
            SqlDbType[] lTipoparametros = { SqlDbType.VarChar };
            string[] lValorParametros = { "V" };

            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            SqlDataReader lLector;
            ListItem lItem = new ListItem();
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion , "pa_GetPrecioSubTra", lsNombreParametros, lTipoparametros, lValorParametros);
            if (lLector.HasRows)
            {
                while (lLector.Read())
                    lblMensaje.Append(lLector["mensaje"].ToString() +"<br>");
            }
            lLector.Close();

            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                Toastr.Warning(this, lblMensaje.ToString());
                lConexion.Cerrar();
                dtgMaestro.Visible = false;
                return;
            }
            try
            {
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetPrecioSubTra";
                lComando.Parameters.Add("@P_indicador", SqlDbType.VarChar).Value = "C";

                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgMaestro.DataSource = lds;
                dtgMaestro.DataBind();
                tblGrilla.Visible = true;
                lConexion.Cerrar();
                dtgMaestro.Visible = true;
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se pudo generar el informe.! " + ex.Message);
                lConexion.Cerrar();
            }
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string lsNombreArchivo = goInfo.Usuario + "InfPrecioSubTra" + DateTime.Now + ".xls";
            string lstitulo_informe = "Informe Precios Reserva Subasta Transporte";
            string lsTituloParametros = "";
            var Error = false;

            if (dtgMaestro.Visible )
            {
                try
                {
                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    SqlCommand lComando = new SqlCommand();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandTimeout = 3600;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_GetPrecioSubTra";
                    lComando.Parameters.Add("@P_indicador", SqlDbType.VarChar).Value = "C";

                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgExcel.DataSource = lds;
                    dtgExcel.DataBind();
                    lConexion.Cerrar();
                    decimal ldCapacidad = 0;
                    StringBuilder lsb = new StringBuilder();
                    ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                    StringWriter lsw = new StringWriter(lsb);
                    HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                    Page lpagina = new Page();
                    HtmlForm lform = new HtmlForm();
                    dtgExcel.EnableViewState = false;
                    lpagina.EnableEventValidation = false;
                    lpagina.DesignerInitialize();
                    lpagina.Controls.Add(lform);

                    lform.Controls.Add(dtgExcel);
                    lpagina.RenderControl(lhtw);
                    Response.Clear();

                    Response.Buffer = true;
                    Response.ContentType = "aplication/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                    Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                    Response.ContentEncoding = Encoding.Default;
                    Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                    Response.Charset = "UTF-8";
                    Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre + "</td></tr></table>");
                    Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                    Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                    Response.ContentEncoding = Encoding.Default;
                    Response.Write(lsb.ToString());

                    Response.End();
                    lds.Dispose();
                    lsqldata.Dispose();
                    lConexion.CerrarInforme();
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, "No se Pudo Generar el Excel.!" + ex.Message);

                }
            }
        }

        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        /// 20180126 rq107-16
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            cargarDatos();
        }
    }
}