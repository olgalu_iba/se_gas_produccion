﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_EntUsrFinalDepto.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Informes_frm_EntUsrFinalDepto" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

       <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">

                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Código Registro Inicial</label>
                             <asp:TextBox ID="TxtRegistroIni" runat="server" ValidationGroup="detalle" Width="120px"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtRegistroIni" runat="server" TargetControlID="TxtRegistroIni"
                                FilterType="Custom, Numbers">
                            </ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Código Registro Final</label>
                        <asp:TextBox ID="TxtRegistroFin" runat="server" ValidationGroup="detalle" Width="120px"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtRegistroFin" runat="server" TargetControlID="TxtRegistroIni"
                                FilterType="Custom, Numbers">
                            </ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                     <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Registro Inicial</label>
                                 <asp:TextBox ID="TxtFechaIni" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static" Width="100%"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RfvTxtFechaFin" runat="server" ErrorMessage="El Campo Fechs Final es obligatorio"
                                ControlToValidate="TxtFechaFin" ValidationGroup="detalle">*</asp:RequiredFieldValidator>
                        
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Registro Final</label>
                          <asp:TextBox ID="TxtFechaFin" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static" Width="100%"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="El Campo Fechs Final es obligatorio"
                                ControlToValidate="TxtFechaFin" ValidationGroup="detalle">*</asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Operador</label>
                            <asp:DropDownList ID="DdlOperador" runat="server" Width="200px">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Tramo</label>
                                <asp:DropDownList ID="DdlTramo" runat="server" Width="250px">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Sector de consumo</label>
                             <asp:DropDownList ID="DdlSector" runat="server" Width="200px">
                            </asp:DropDownList>
                        </div>
                    </div>
                     <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Tipo de demanda</label>
                          <asp:DropDownList ID="DdlDemanda" runat="server" Width="250px">
                            </asp:DropDownList>
                        </div>
                    </div>
                     <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Departamento</label>
                            <asp:DropDownList ID="DdlDepto" runat="server" Width="200px">
                            </asp:DropDownList>
                        </div>
                    </div>
                     <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Tipo de demanda</label>
                          <asp:DropDownList ID="DropDownList2" runat="server" Width="250px">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>

                     <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                id="tblMensaje">
                <tr>
                    <td colspan="3" align="center">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
                <div  runat="server"
                    id="tblGrilla" visible="false">
               
                           <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                                <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <asp:BoundColumn DataField="codigo_ent_usuario_final" HeaderText="Código Ent Usuario Final"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="fecha_registro" HeaderText="Fecha Registro"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="codigo_tramo" HeaderText="Código Tramo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Cod. Pto. Salida"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_punto_sal" HeaderText="Punto Salida"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="codigo_tipo_demanda" HeaderText="Cod. tipo demanda"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_tipo_demanda" HeaderText="Tipo Demanda"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="codigo_sector_consumo" HeaderText="Cod. Sector Consumo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_sector_consumo" HeaderText="Sector Consumo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="cantidad_entregada" HeaderText="Cantidad Entregada" ItemStyle-HorizontalAlign="Right"
                                            DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="contrato_definitivo" HeaderText="No. Contrato"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="estado" HeaderText="Estado"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="login_usuario" HeaderText="Login usuario"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="Fecha Hora Actual"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="carga_normal" HeaderText="Estado Carga"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ind_registrado" HeaderText="Contrato registrado"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="codigo_ruta" HeaderText="Cod. Ruta"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="codigo_depto" HeaderText="Cod. Depto."></asp:BoundColumn>
                                        <asp:BoundColumn DataField="nombre_departamento" HeaderText="Departamento"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="codigo_ciudad" HeaderText="Cod. Mpio."></asp:BoundColumn>
                                        <asp:BoundColumn DataField="nombre_ciudad" HeaderText="Municipio"></asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                </asp:DataGrid>
                            </div>
                 
                </div>

            </div>

        </div>

    </div>
       
</asp:Content>