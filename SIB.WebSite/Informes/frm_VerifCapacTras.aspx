﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_VerifCapacTras.aspx.cs"
    Inherits="Informes_frm_VerifCapacTras" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
        width="80%">
        <tr>
            <td align="center" class="th1">
                <asp:Label ID="lblTitulo" runat="server" ForeColor ="White"></asp:Label>
            </td>
        </tr>
    </table>
    <br /><br /><br /><br /><br /><br />
    <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
        width="80%">
        <tr>
            <td class="td1">
                Tipo información
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlTipo" runat="server">
                    <asp:ListItem Value="1">Capacidad Comprometida</asp:ListItem>  <%--20180815 BUG230--%>
                    <asp:ListItem Value="2">Capacidad en Firme</asp:ListItem>
                    <asp:ListItem Value="3">Capacidad Disponible</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla" visible="false">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 1050px; height: 350px;">
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="codigo_tramo" HeaderText="cód" ItemStyle-HorizontalAlign="Left">  <%--20180815 BUG230--%>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_tramo" HeaderText="desc tramo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="trasportador" HeaderText="transportador" ItemStyle-HorizontalAlign="Left">  <%--20180815 BUG230--%>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="capacidad_max" HeaderText="CMMP" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="validacion" HeaderText="validación" ItemStyle-HorizontalAlign="Left">  <%--20180815 BUG230--%>
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="True" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>