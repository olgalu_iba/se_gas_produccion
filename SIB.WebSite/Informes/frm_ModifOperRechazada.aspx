﻿

<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ModifOperRechazada.aspx.cs" Inherits="Informes_frm_ModifOperRechazada" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
            width="80%">
            <tr>
                <td align="center" class="th1">
                    <asp:Label ID="lblTitulo" runat="server" ForeColor ="White" ></asp:Label>
                </td>
            </tr>
        </table>
    <br /><br /><br /><br /><br /><br />
        <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
            width="80%">

            <tr>
                <td class="td1">Fecha Negociación Inicial
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                </td>
                <td class="td1">Fecha Negociación Final
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                </td>
            </tr>
            
            <tr>
                <td class="td1">Número Operación
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtOperIni" runat="server" ValidationGroup="detalle"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FTEBTxtOperIni" runat="server" TargetControlID="TxtOperIni"
                        FilterType="Custom, Numbers">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td class="td1">Número contrato
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtContratoDef" runat="server" ValidationGroup="detalle" MaxLength="30"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="td1">Número Id Registro
                </td>
                <td class="td2" colspan="3">
                    <asp:TextBox ID="TxtNoId" runat="server" ValidationGroup="detalle"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="ftebTxtNoId" runat="server" TargetControlID="TxtNoId"
                        FilterType="Custom, Numbers">
                    </cc1:FilteredTextBoxExtender>
                </td>
              
            </tr>
            <tr>
                <td class="td1">Tipo Subasta
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlSubasta" runat="server"  Width="200"></asp:DropDownList>
                </td>
                <td class="td1">Tipo Mercado
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlMercado" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlMercado_SelectedIndexChanged"  Width="200">
                        <asp:ListItem  Value="">Seleccione</asp:ListItem>
                        <asp:ListItem  Value="P">Primario</asp:ListItem>
                        <asp:ListItem  Value="S">Secundario</asp:ListItem>
                        <asp:ListItem  Value="O">Otras transacciones mercado mayorista</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">Producto
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlProducto" runat="server"  Width="200">
                        <asp:ListItem  Value="">Seleccione</asp:ListItem>
                        <asp:ListItem  Value="G">Suministro de gas</asp:ListItem>
                        <asp:ListItem  Value="T">Capacidad de transporte</asp:ListItem>
                        <asp:ListItem  Value="A">Suministro y transporte</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="td1">Causa Modificación
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlCausa" runat="server"  Width="200"></asp:DropDownList>
                </td>
            </tr>
            <tr id ="tr01" runat="server">
                <td class="td1">Operador contraparte
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlContraparte" runat="server"  Width="200"></asp:DropDownList>
                </td>
            </tr>
            <tr id ="tr02" runat="server">
                <td class="td1">Punta
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlPunta" runat="server">
                        <asp:ListItem value ="">Seleccione</asp:ListItem>
                        <asp:ListItem value ="C">Compra</asp:ListItem>
                        <asp:ListItem value ="V">Venta</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="td1">Operador
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlOperador" runat="server"  Width="200"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="th1" colspan="4" align="center">
                    <asp:Button ID="btnConsultar" runat="server" Text="Consultar" OnClick="btnConsultar_Click" />
                    <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                        Visible="false" Height="35" />
                </td>
            </tr>
        </table>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblMensaje">
            <tr>
                <td colspan="3" align="center">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
        <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblGrilla">
            <tr>
                <td align="center">
                    <div style="overflow: scroll; width: 1050px; height: 350px;">
                        <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                            PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                            HeaderStyle-CssClass="th1" >
                            <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                            <ItemStyle CssClass="td2"></ItemStyle>
                            <Columns>
                                <%--1--%>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Operación" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <%--1--%>
                                <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <%--5--%>
                                <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato definitivo"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--6--%>
                                <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--7--%>
                                <asp:BoundColumn DataField="hora_negociacion" HeaderText="Hora Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--8--%>
                                <asp:BoundColumn DataField="fecha_suscripcion_cont" HeaderText="Fecha Suscripción"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--9--%>
                                <asp:BoundColumn DataField="codigo_punto_entrega" HeaderText="Cod Pto/ Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--10--%>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Pto Entrega/ Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--11--%>
                                <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Cod Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--12--%>
                                <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--13--%>
                                <asp:BoundColumn DataField="codigo_periodo" HeaderText="Cod Periodo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--14--%>
                                <asp:BoundColumn DataField="desc_periodo" HeaderText="Periodo Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--15--%>
                                <asp:BoundColumn DataField="operador_compra" HeaderText="Ope Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--16--%>
                                <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--17--%>
                                <asp:BoundColumn DataField="operador_venta" HeaderText="Ope Venta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--18--%>
                                <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Venta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--19--%>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0: ###,###,###,##0}"></asp:BoundColumn>
                                <%--20--%>
                                <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0: ###,###,###,##0.00}"></asp:BoundColumn>
                                <%--21--%>
                                <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <%--22--%>
                                <asp:BoundColumn DataField="hora_inicial" HeaderText="Hora Inicial" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--23--%>
                                <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <%--24--%>
                                <asp:BoundColumn DataField="hora_final" HeaderText="Hora Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--25--%>
                                <asp:BoundColumn DataField="no_años" HeaderText="No Años" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--26--%>
                                <asp:BoundColumn DataField="sentido_flujo" HeaderText="Sentido FLujo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--27--%>
                                <asp:BoundColumn DataField="presion_punto_fin" HeaderText="Presión" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--28--%>
                                <asp:BoundColumn DataField="tipo_garantia" HeaderText="Tipo Garantía" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--29--%>
                                <asp:BoundColumn DataField="valor_garantia" HeaderText="Valor Garantía" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--30--%>
                                <asp:BoundColumn DataField="fecha_pago" HeaderText="Fecha pago Garantía" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--31--%>
                                <asp:BoundColumn DataField="codigo_depto_punto_sal" HeaderText="Código Depto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--32--%>
                                <asp:BoundColumn DataField="nombre_departamento" HeaderText="Nombre Depto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--33--%>
                                <asp:BoundColumn DataField="codigo_municipio_pto_sal" HeaderText="Código Mcpio" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--34--%>
                                <asp:BoundColumn DataField="nombre_ciudad" HeaderText="Nombre Municipio" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--35--%>
                                <asp:BoundColumn DataField="codigo_centro_pob" HeaderText="Código Centro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--36--%>
                                <asp:BoundColumn DataField="nombre_centro" HeaderText="Nombre Centro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--37--%>
                                <asp:BoundColumn DataField="codigo_fuente" HeaderText="Código Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--38--%>
                                <asp:BoundColumn DataField="desc_fuente" HeaderText="Nombre Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--39--%>
                                <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--40--%>
                                <asp:BoundColumn DataField="desc_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--41--%>
                                <asp:BoundColumn DataField="desc_subasta" HeaderText="Tipo Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--42--%>
                                <asp:BoundColumn DataField="desc_causa" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20210707 --%>
                                <asp:BoundColumn DataField="codigo_trm" HeaderText="Código Tasa Cambio" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_trm" HeaderText="Tasa Cambio" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="observacion_trm" HeaderText="Observación Tasa Cambio" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tipo_moneda" HeaderText="Tipo Moneda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--fin 20210707 --%>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </td>
            </tr>
        </table>
</asp:Content>