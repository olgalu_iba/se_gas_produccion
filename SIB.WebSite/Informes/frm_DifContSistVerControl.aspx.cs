﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Informes
{
    public partial class frm_DifContSistVerControl : Page
    {
        private InfoSessionVO goInfo;
        private clConexion lConexion;
        private SqlDataReader lLector;
        private DataSet lds = new DataSet();
        private SqlDataAdapter lsqldata = new SqlDataAdapter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = "Operaciones con Valor Diferente Entre la Subasta y el Registro Final";
            lConexion = new clConexion(goInfo);

            buttons.FiltrarOnclick += imbConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.Inicializar(botones: new[] { EnumBotones.Buscar });

            if (IsPostBack) return;
            //Titulo
            Master.Titulo = "Informes";
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOpeC, "m_operador", " estado='A' order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlOpeV, "m_operador", " estado='A' order by razon_social", 0, 4);
            lConexion.Cerrar();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"] + "-" + lLector["razon_social"];
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbConsultar_Click(object sender, EventArgs e)
        {
            bool Error = false;

            try
            {
                if (TxtFechaIni.Text == "" && TxtFechaFin.Text != "")
                {
                    Toastr.Warning(this, "Debe digitar la fecha inicial antes que la final<br>");
                    Error = true;

                }
                if (TxtFechaIni.Text != "")
                    Convert.ToDateTime(TxtFechaIni.Text);
                if (TxtFechaFin.Text != "")
                    Convert.ToDateTime(TxtFechaFin.Text);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Error en el formato de las fechas <br>");
                Error = true;

            }
            //if (TxtOperacionIni.Text == "" && TxtOperacionFin.Text != "")
            //    lblMensaje.Text = "Debe digitar la operación inicial antes que la final<br>";
            if (!Error)
            {
                try
                {
                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    SqlCommand lComando = new SqlCommand();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandTimeout = 3600;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_GetDifContControl";
                    lComando.Parameters.Add("@P_fecha_negociacion_ini", SqlDbType.VarChar).Value = TxtFechaIni.Text;
                    if (TxtFechaFin.Text == "")
                        lComando.Parameters.Add("@P_fecha_negociacion_fin", SqlDbType.VarChar).Value = TxtFechaIni.Text;
                    else
                        lComando.Parameters.Add("@P_fecha_negociacion_fin", SqlDbType.VarChar).Value = TxtFechaFin.Text;
                    if (TxtOperacionIni.Text != "")
                    {
                        lComando.Parameters.Add("@P_numero_contrato_ini", SqlDbType.Int).Value = TxtOperacionIni.Text;
                        //if (TxtOperacionFin.Text == "")
                        lComando.Parameters.Add("@P_numero_contrato_fin", SqlDbType.Int).Value = TxtOperacionIni.Text;
                        //else
                        //   lComando.Parameters.Add("@P_numero_contrato_fin", SqlDbType.Int).Value = TxtOperacionFin.Text;
                    }
                    lComando.Parameters.Add("@P_operador_compra", SqlDbType.Int).Value = ddlOpeC.SelectedValue;
                    lComando.Parameters.Add("@P_operador_venta", SqlDbType.Int).Value = ddlOpeV.SelectedValue;
                    //20171130 rq026-17
                    if (TxtNoId.Text != "")
                        lComando.Parameters.Add("@P_codigo_verif", SqlDbType.Int).Value = TxtNoId.Text;
                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgMaestro.DataSource = lds;
                    dtgMaestro.DataBind();
                    dtgMaestro.Visible = true;
                    buttons.SwitchOnButton(new[] { EnumBotones.Buscar, EnumBotones.Excel });
                    lConexion.Cerrar();
                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, "No se Pudo Generar el Informe.! " + ex.Message);

                }
            }
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string lsNombreArchivo = goInfo.Usuario + "InfDifSubVerif" + DateTime.Now + ".xls";
            string lstitulo_informe = "Informe Operaciones con valor deiferente entre la subasta y el registro final";
            string lsTituloParametros = "";
            try
            {
                if (TxtOperacionIni.Text != "")
                    lsTituloParametros += " - Operación: " + TxtOperacionIni.Text;
                //20171130 rq026 - 17
                if (TxtNoId.Text != "")
                    lsTituloParametros += " - Id Registro: " + TxtNoId.Text;
                //if (TxtOperacionFin.Text != "")
                //    lsTituloParametros += "-" + TxtOperacionFin.Text;
                if (TxtFechaIni.Text != "")
                    lsTituloParametros += " - Fecha Neg: " + TxtFechaIni.Text;
                if (TxtFechaFin.Text != "")
                    lsTituloParametros += "-" + TxtFechaFin.Text;
                if (ddlOpeC.SelectedValue != "0")
                    lsTituloParametros += "  - Operador Compra: " + ddlOpeC.SelectedItem;
                if (ddlOpeV.SelectedValue != "0")
                    lsTituloParametros += "  - Operador Venta: " + ddlOpeV.SelectedItem;
                dtgMaestro.Columns[5].Visible = false; //20171130 rq026 - 17
                StringBuilder lsb = new StringBuilder();
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                dtgMaestro.EnableViewState = false;
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                lform.Controls.Add(dtgMaestro);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = System.Text.Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='6' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());

                Response.End();
                lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
                dtgMaestro.Columns[5].Visible = true; //20171130 rq026-17
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pudo Generar el Excel.! " + ex.Message);
            }
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            var lComando = new SqlCommand
            {
                Connection = lConexion.gObjConexion,
                CommandTimeout = 3600,
                CommandType = CommandType.StoredProcedure,
                CommandText = "pa_GetDifContControlDet"
            };
            lComando.Parameters.Add("@P_numero_contrato", SqlDbType.Int).Value = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text; ;
            lComando.ExecuteNonQuery();
            lsqldata.SelectCommand = lComando;
            lsqldata.Fill(lds);
            dtgDetalle.DataSource = lds;
            dtgDetalle.DataBind();
            lConexion.Cerrar();
            //Se abre el modal de detalle  
            Modal.Abrir(this, mdlDetalle.ID, mdlDetalleInside.ID);
        }

        /// <summary>
        /// Evento que cierra el modal de detalle
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCancelar_Click(object sender, EventArgs e)
        {
            Modal.Cerrar(this, mdlDetalle.ID);
        }
    }
}