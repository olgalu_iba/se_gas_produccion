using System;
using System.Web.UI;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Informes_exportar_excel : System.Web.UI.Page
{
    //variables globales de la clase
    clConexion lConexion = null;
    InfoSessionVO goInfo = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            string lsdatos = "";
            string lstipo_export = "";
            string lsnombre_tabla = "";
            string lsnombre_campos = "";
            string lstitulo_informe = "";
            string lscondicion = "";
            string[] lscolumnas = null;
            string[] lsNombreParametros = null;
            string[] lValorParametros = null;
            string lscolumnas2 = "";
            string lsprocedimiento = "";
            string lsNombreArchivo = goInfo.Usuario.ToString() + "InformeExcel" + DateTime.Now + ".xls";

            //CAPTURA NOMBRE DE LA TABLA O TITULO PARA ENCABEZADO
            if (this.Request.QueryString["nombre_tabla"] != null)
            {
                lsnombre_tabla = this.Request.QueryString["nombre_tabla"];
            }
            if (this.Request.QueryString["titulo_informe"] != null)
            {
                lstitulo_informe = this.Request.QueryString["titulo_informe"];
            }
            else
                lstitulo_informe = "SIN TITULO";

            //CAPTURA NOMBRE DE LA TABLA O TITULO PARA ENCABEZADO
            if (this.Request.QueryString["condicion"] != null)
            {
                lscondicion = this.Request.QueryString["condicion"].Replace("#", "%");
            }

            //DESCOMPONE EL ARREGLO DE COLUMNAS A MOSTRAR
            if (this.Request.QueryString["columnas"] != null)
            {
                lscolumnas = this.Request.QueryString["columnas"].Split('*');
                lscolumnas2 = this.Request.QueryString["columnas"].Replace('*', ',');
                lsnombre_campos = "<tr>";
                foreach (string lsnc in lscolumnas)
                {
                    lsnombre_campos += "<td><b>" + lsnc.ToString() + "</b></td>";
                }
                lsnombre_campos += "</tr>";
            }
            //DESCOMPONE EL ARREGLO DE Nombres de Parametros
            if (this.Request.QueryString["nombreParametros"] != null)
            {
                lsNombreParametros = this.Request.QueryString["nombreParametros"].Split('*');
            }
            //Descompone el Arreglo de los Valores de los Parametros
            if (this.Request.QueryString["valorParametros"] != null)
            {
                lValorParametros = this.Request.QueryString["valorParametros"].Split('*');
            }
            //CAPTURA EL NOMBRE DEL PROCEDIMIENTO EN CASO DE QUE VENGA
            if (this.Request.QueryString["procedimiento"] != null)
            {
                lsprocedimiento = this.Request.QueryString["procedimiento"];
            }
            //SELECCIONA EL TIPO DE EXPORTACION SEGUN EL PARAMETRO 1-PROCEDIMIENTO 2-SP_SELECCIONARREGISTROS
            if (this.Request.QueryString["tipo_export"] != null)
            {
                lstipo_export = this.Request.QueryString["tipo_export"];
            }

            if (Convert.ToInt32(lstipo_export.ToString()) == 1)
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                lsdatos = DelegadaBase.Servicios.llenarHtmlTabla(lConexion.gObjConexion, lsprocedimiento, lscondicion, lscolumnas, lsnombre_tabla);
                lConexion.Cerrar();
            }
            if (Convert.ToInt32(lstipo_export.ToString()) == 2)
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                lsdatos = DelegadaBase.Servicios.llenarHtmlTabla(lConexion.gObjConexion, lsprocedimiento, lsNombreParametros, lValorParametros, lscolumnas, "2");
                lConexion.Cerrar();
            }

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            this.EnableViewState = false;
            System.IO.StringWriter w = new System.IO.StringWriter();
            HtmlTextWriter html = new HtmlTextWriter(w);
            html.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Array oTabla = lsnombre_tabla.Split(',');
            html.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            html.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + oTabla.GetValue(0).ToString() + "</center></font></td></tr></table><br>");
            html.Write("<table border = 1>" + lsnombre_campos + "</table>");
            if (lsdatos.Length > 0)
            {
                html.Write("<table border = 1>" + lsdatos.ToString() + "</table><br>");
            }
            Response.Write(w.ToString());
            Response.Flush();
        }
        catch (Exception ex)
        {
            lblError.Text = "Ha ocurrido un error: " + ex.Message;
            lblError.Visible = true;
        }
    }


}
