﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ConsultaOfertasNegDirecta.aspx.cs"
    Inherits="Informes_frm_ConsultaOfertasNegDirecta" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
                    width="80%">
                    <tr>
                        <td align="center" class="th1">
                            <asp:Label ID="lblTitulo" runat="server" ForeColor="White" ></asp:Label>
                        </td>
                    </tr>
                </table>
                <br /><br /><br /><br /><br /><br />
                <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
                    width="80%">
                    <tr>
                        <td class="td1">
                            Fecha Negociación Inicial  <%--20180815 BUG230--%>
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            Fecha Negociación Final  <%--20180815 BUG230--%>
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            Número Rueda  <%--20180815 BUG230--%>
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtNoRueda" runat="server" ValidationGroup="detalle"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            Número Id  <%--20180815 BUG230--%>
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtNoid" runat="server" ValidationGroup="detalle"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            Operador
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlOperador" runat="server" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla" visible="false">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 1150px; height: 350px;">
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="fecha_hora_oferta" HeaderText="Fecha Hora Oferta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_postura" HeaderText="No. Oferta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_participante" HeaderText="Código Participante">  <%--20180815 BUG230--%>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_participante" HeaderText="Nombre Participante">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_id" HeaderText="No. Id" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="130px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="punto_entrega" HeaderText="Punto de Entrega"></asp:BoundColumn>
                            <asp:BoundColumn DataField="modalidad" HeaderText="Modalidad de Entrega"></asp:BoundColumn>
                            <asp:BoundColumn DataField="periodo_entrega" HeaderText="Periodo de Entrega"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicio" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Fin" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left">  <%--20180815 BUG230--%>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="tipo_oferta" HeaderText="Tipo Oferta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Right" 
                                ItemStyle-Width="90px"></asp:BoundColumn>  <%--20180815 BUG230--%>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Right"
                                ItemStyle-Width="90px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="adjudicado" HeaderText="Adjudicado" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_operacion" HeaderText="Número Operación"></asp:BoundColumn>  <%--20180815 BUG230--%>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>