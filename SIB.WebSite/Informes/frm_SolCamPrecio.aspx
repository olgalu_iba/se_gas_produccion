﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_SolCamPrecio.aspx.cs"
    Inherits="Informes_frm_SolCamPrecio" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div>
                    <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
                        width="80%">
                        <tr>
                            <td align="center" class="th1">
                                <asp:Label ID="lblTitulo" runat="server" ForeColor ="White"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <br /><br /><br /><br /><br /><br />
                    <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
                        width="80%">
                        <tr>
                            <td class="td1">Fecha Inicial
                            </td>
                            <td class="td2">
                                <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                            </td>
                            <td class="td1">Fecha Final
                            </td>
                            <td class="td2">
                                <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1">Operador Venta
                            </td>
                            <td class="td2" colspan="3">
                                <asp:DropDownList ID="DdlOperador" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1">Tipo Subasta
                            </td>
                            <td class="td2">
                                <asp:DropDownList ID="DdlSubasta" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="td1">Tipo Mercado
                            </td>
                            <td class="td2">
                                <asp:DropDownList ID="DdlMercado" runat="server">
                                    <asp:ListItem Value="">Selecccione</asp:ListItem>
                                    <asp:ListItem Value="P">Primario</asp:ListItem>
                                    <asp:ListItem Value="S">Secundario</asp:ListItem>
                                    <asp:ListItem Value="O">Otras transacciones del mercado </asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblMensaje">
            <tr>
                <td class="th1" colspan="3" align="center">
                    <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                        OnClick="imbConsultar_Click" Height="40" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblGrilla" visible="false">
            <tr>
                <td colspan="3" align="center">
                    <div style="overflow: scroll; width: 100%; height: 450px;">
                        <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                            HeaderStyle-CssClass="th1">
                            <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                            <ItemStyle CssClass="td2"></ItemStyle>
                            <Columns>

                                <asp:BoundColumn DataField="numero_contrato" HeaderText="no. contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_subasta" HeaderText="Tpo Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_mercado" HeaderText="Tpo Mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_venta" HeaderText="Operador venta" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_compra" HeaderText="Operador compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Fecha_vigencia_ini" HeaderText="Fecha Vig Ini" ItemStyle-HorizontalAlign="Center"
                                    DataFormatString="{0:yyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Fecha_vigencia_fin" HeaderText="Fecha Vig Fin" ItemStyle-HorizontalAlign="Center"
                                    DataFormatString="{0:yyy/MM/dd}"></asp:BoundColumn>
                                <%--20180302 rq004-18--%>
                                <asp:BoundColumn DataField="precio_actual" HeaderText="Precio Actual" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,###,###,##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="precio_nuevo" HeaderText="Precio Nuevo" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </td>
            </tr>
        </table>
</asp:Content>