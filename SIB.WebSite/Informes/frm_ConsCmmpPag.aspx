﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ConsCmmpPag.aspx.cs" Inherits="Informes.Informes_frm_ConsCmmpPag" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Año Negociación" AssociatedControlID="TxtAño" runat="server" />
                            <asp:TextBox ID="TxtAño" runat="server" Width="100%" CssClass="form-control" ValidationGroup="detalle"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtAño" runat="server" TargetControlID="TxtAño"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Trimestre Negociación" AssociatedControlID="ddlTrimestre" runat="server" />
                            <asp:DropDownList ID="ddlTrimestre" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
                            <asp:DropDownList ID="ddlOperador" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server" OnSelectedIndexChanged="ddlOperador_SelectedIndexChanged" AutoPostBack="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tramo" AssociatedControlID="ddlTramo" runat="server" />
                            <asp:DropDownList ID="ddlTramo" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server" />
                        </div>
                    </div>
                </div>

                <div runat="server" visible="false" id="tblGrilla">
                    <div class="table table-responsive">
                        <%--20180126 rq107-16--%>
                        <asp:DataGrid ID="dtgMaestro" AutoGenerateColumns="False" AllowPaging="true" PageSize="20" OnPageIndexChanged="dtgMaestro_PageIndexChanged" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" runat="server">
                            <Columns>
                                <%--20210915-- visualizacion--%>
                                <asp:BoundColumn DataField="año_negociacion" HeaderText="Año Negociacion" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="trimestre_negociacion" HeaderText="Trimestre Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_trim_neg" HeaderText="Descripción Trimestre Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20210915 fin visualiacion--%>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Transportador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_transportador" HeaderText="Nombre Transportador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_tramo" HeaderText="Código Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="sentido" HeaderText="Sentido Flujo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="año_datos" HeaderText="Año" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="trimestre_datos" HeaderText="Número Trimestre" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_trimestre" HeaderText="Trimestre" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cap_cmmp" HeaderText="CMMP (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cap_comp_firme" HeaderText="Capacidad comprometida para contratos en firme (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cap_disp_prim" HeaderText="Capacidad Disponible Primaria (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cargos" HeaderText="Cargos(USD/KPC)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0.000}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                    <asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False" AllowPaging="false" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered">
                        <Columns>
                            <%--20210915-- visualizacion--%>
                            <asp:BoundColumn DataField="año_negociacion" HeaderText="Año Negociacion" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="trimestre_negociacion" HeaderText="Trimestre Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_trim_neg" HeaderText="Descripción Trimestre Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20210915 fin visualiacion--%>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Transportador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_transportador" HeaderText="Nombre Transportador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_tramo" HeaderText="Código Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="sentido" HeaderText="Sentido Flujo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="año_datos" HeaderText="Año" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="trimestre_datos" HeaderText="Número Trimestre" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_trimestre" HeaderText="Trimestre" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cap_cmmp" HeaderText="CMMP (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cap_comp_firme" HeaderText="Capacidad comprometida para contratos en firme (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cap_disp_prim" HeaderText="Capacidad Disponible Primaria (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cargos" HeaderText="Cargos(USD/KPC)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0.000}"></asp:BoundColumn>
                        </Columns>
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
