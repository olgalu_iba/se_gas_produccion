﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_IndicadorPublicacionIOP.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Informes_frm_IndicadorPublicacionIOP" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">

                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Inicial</label>
                            <asp:TextBox ID="TxtFechaIni" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static" Width="100%"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RfvTxtFechaFin" runat="server" ErrorMessage="El Campo Fechs Final es obligatorio"
                                ControlToValidate="TxtFechaFin" ValidationGroup="detalle">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Final</label>
                            <asp:TextBox ID="TxtFechaFin" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static" Width="100%"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="El Campo Fechs Final es obligatorio"
                                ControlToValidate="TxtFechaFin" ValidationGroup="detalle">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Tabla</label>
                            <asp:DropDownList ID="ddlTabla" runat="server" AutoPostBack="true" CssClass="form-control" Width="100%">
                               <%-- OnSelectedIndexChanged="ddlTabla_SelectedIndexChanged"--%>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Usuario Evento</label>
                            <asp:TextBox ID="txtOperacion" runat="server" CssClass="form-control"  Width="100%"></asp:TextBox>
                            <asp:CompareValidator ID="cvtxtConsecutivo" runat="server" ErrorMessage="EL consecutivo interno debe ser numérico"
                                ControlToValidate="txtOperacion" Operator="DataTypeCheck" Type="Double" ValidationGroup="detalle"></asp:CompareValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Usuario Evento</label>
                            <asp:TextBox ID="txtUsuario" runat="server" CssClass="form-control"  Width="100%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Evento</label>
                            <asp:DropDownList ID="ddlEvento" runat="server" CssClass="form-control"  Width="100%">
                                <asp:ListItem Value="">Seleccione </asp:ListItem>
                                <asp:ListItem Value="1">Insertar</asp:ListItem>
                                <asp:ListItem Value="2">Actualizar</asp:ListItem>
                                <asp:ListItem Value="3">Eliminar</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>

                <div class="row" runat="server">
                    <div class="form-group">
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="detalle" />
                        <asp:Label ID="Label2" Style="display: inline-block;" CssClass="invalid-feedback" runat="server" />
                    </div>
                </div>


                <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                    id="tblMensaje">

                    <tr>
                        <td align="center">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                </table>
                <div runat="server"
                    id="tblGrilla" visible="false">

                    <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                        <asp:DataGrid ID="dtgTransaccion" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">

                            <Columns>
                                <asp:BoundColumn DataField="numero_operacion" HeaderText="No. Operacion" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_venta" HeaderText="Operador Venta" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="200px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_compra" HeaderText="Operador Compra" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="200px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_publicacion" HeaderText="Fecha Publica Bec" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="130px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="hora_publicacion" HeaderText="Hora Publica Bec" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="producto" HeaderText="Prodeucto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="modalidad" HeaderText="Modalidad de Entrega"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tipo_rueda" HeaderText="Tipo Rueda" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cumplimiento" HeaderText="Cumplimiento"></asp:BoundColumn>
                            </Columns>

                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>

                  
                    <div>
                        <asp:Label ID="lblInd1" runat="server" Text="Indicador Oportunidad Publicacion" Font-Bold="true"
                            Font-Size="12px" ForeColor="Red"></asp:Label>
                    </div>
                    <div >
                        <asp:Label ID="lblResInd1" runat="server" Font-Bold="true" Font-Size="14px" ForeColor="Red"></asp:Label>
                    </div>
                 
             
                    <div>
                        <div style="overflow: scroll; width: 1050px; height: 350px;">
                            <asp:DataGrid ID="dtgOperativa" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered"
                                PagerStyle-HorizontalAlign="Center" >
                          
                                <Columns>
                                    <asp:BoundColumn DataField="proceso_operativo" HeaderText="Proceso Operativo" ItemStyle-HorizontalAlign="Left">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="400px"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_reporte" HeaderText="Fecha Reporte" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="150px"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_publicacion" HeaderText="Fecha Publica Bec" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="150px"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="cumplimiento" HeaderText="Cumplimiento"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </div>
                    </div>
               
              
                    <div>
                        <asp:Label ID="lblInd2" runat="server" Text="Indicador Oportunidad Publicacion" Font-Bold="true"
                            Font-Size="12px" ForeColor="Red"></asp:Label>
                    </div>
                    <div>
                        <asp:Label ID="lblResInd2" runat="server" Font-Bold="true" Font-Size="14px" ForeColor="Red"></asp:Label>
                    </div>
                    <div>
                        <asp:Label ID="lblIndTotal" runat="server" Text="Indicador Oportunidad Publicacion IOP"
                            Font-Bold="true" Font-Size="12px" ForeColor="Red"></asp:Label>
                    </div>
                    <div>
                        <asp:Label ID="lblResIndTotal" runat="server" Font-Bold="true" Font-Size="14px" ForeColor="Red"></asp:Label>
                    </div>
               
                </div>

            </div>

        </div>

    </div>


</asp:Content>
