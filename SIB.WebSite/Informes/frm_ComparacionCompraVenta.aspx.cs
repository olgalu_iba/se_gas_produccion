﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;

public partial class Informes_frm_ComparacionCompraVenta : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Comparación de Compras y Ventas";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlMercado, "m_tipo_mercado", " 1=1 order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlPunto, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlModalidadC, "m_modalidad_contractual", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlModalidadV, "m_modalidad_contractual", " estado = 'A' order by descripcion", 0, 1);
            lConexion.Cerrar();
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, CheckBoxList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        //lItem.Value = "0";
        //lItem.Text = "Seleccione";
        //lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            if (lsTabla != "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }


    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF = DateTime.Now;
        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial. <br>";
            }
        }
        else
            lblMensaje.Text += "Debe digitar la Fecha Inicial. <br>";
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                if (TxtFechaIni.Text.Trim().Length > 0)
                {
                    ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
                    if (ldFechaI > ldFechaF)
                        lblMensaje.Text += "La Fecha Final NO puede ser Menor que la Fecha de Inicial. <br>";
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Final. <br>";
            }
        }
        else
            lblMensaje.Text += "Debe digitar la Fecha Final. <br>";
        if (lblMensaje.Text == "")
        {
            try
            {
                int liConta = 0;
                string lsMercado = "'0'";
                string lsPunto = "0";
                string lsModalidadC = "0";
                string lsModalidadV = "0";
                while (liConta < ddlOperador.Items.Count)
                {
                    if (ddlOperador.Items[liConta].Selected)//&& ddlOperador.Items[liConta].Value != "0")
                    {
                        hdfOperador.Value += "," + ddlOperador.Items[liConta].Value;
                    }
                    liConta++;
                }
                liConta = 0;
                while (liConta < ddlMercado.Items.Count)
                {
                    if (ddlMercado.Items[liConta].Selected)// && ddlMercado.Items[liConta].Value != "0")
                    {
                        lsMercado += "," + "'" + ddlMercado.Items[liConta].Value + "'";
                    }
                    liConta++;
                }
                liConta = 0;
                while (liConta < ddlPunto.Items.Count)
                {
                    if (ddlPunto.Items[liConta].Selected)// && ddlPunto.Items[liConta].Value != "0")
                    {
                        lsPunto += "," + ddlPunto.Items[liConta].Value;
                    }
                    liConta++;
                }
                liConta = 0;
                while (liConta < ddlModalidadC.Items.Count)
                {
                    if (ddlModalidadC.Items[liConta].Selected)// && ddlModalidadC.Items[liConta].Value != "0")
                    {
                        lsModalidadC += "," + ddlModalidadC.Items[liConta].Value;
                    }
                    liConta++;
                }
                liConta = 0;
                while (liConta < ddlModalidadV.Items.Count)
                {
                    if (ddlModalidadV.Items[liConta].Selected)// && ddlModalidadV.Items[liConta].Value != "0")
                    {
                        lsModalidadV += "," + ddlModalidadV.Items[liConta].Value;
                    }
                    liConta++;
                }

                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetRepCompVent";
                lComando.Parameters.Add("@P_fecha_ini", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                lComando.Parameters.Add("@P_fecha_fin", SqlDbType.VarChar).Value = TxtFechaFin.Text.Trim();
                lComando.Parameters.Add("@P_destino_rueda", SqlDbType.VarChar).Value = ddlProducto.SelectedValue;
                lComando.Parameters.Add("@P_codigo_operador", SqlDbType.VarChar).Value = hdfOperador.Value;
                lComando.Parameters.Add("@P_tipo_mercado", SqlDbType.VarChar).Value = lsMercado;
                lComando.Parameters.Add("@P_codigo_punto", SqlDbType.VarChar).Value = lsPunto;
                lComando.Parameters.Add("@P_modalidad_c", SqlDbType.VarChar).Value = lsModalidadC;
                lComando.Parameters.Add("@P_modalidad_v", SqlDbType.VarChar).Value = lsModalidadV;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgMaestro.DataSource = lds;
                dtgMaestro.DataBind();
                tblGrilla.Visible = true;
                imbExcel.Visible = true;
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfIdRueda" + DateTime.Now + ".xls";
        string lstitulo_informe = "";
        string lsTituloParametros = "";
        try
        {
            lstitulo_informe = "Comparación de Compras y Ventas";
            if (ddlProducto.SelectedValue == "G")
                lstitulo_informe += " - Suministro";
            else
                lstitulo_informe += " - Transporte";

            lsTituloParametros += " - Fecha Inicial: " + TxtFechaIni.Text.Trim() + " - Fecha Final: " + TxtFechaFin.Text.Trim();

            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgMaestro.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            lform.Controls.Add(dtgMaestro);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Excel.!" + ex.Message.ToString();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20160706 sector de conusmo
    protected void ddlProducto_SelectedIndexChanged(object sender, EventArgs e)
    {
        lConexion.Abrir();
        ddlPunto.Items.Clear();
        if (ddlProducto.SelectedValue == "G")
            LlenarControles(lConexion.gObjConexion, ddlPunto, "m_pozo", " estado ='A' order by descripcion", 0, 1);
        else
            LlenarControles(lConexion.gObjConexion, ddlPunto, "m_ruta_snt", " estado ='A' order by descripcion", 0, 4);
        lConexion.Cerrar();
    }
    protected void BtnBusOpe_Click(object sender, EventArgs e)
    {
        int liConta = 0;
        while (liConta < ddlOperador.Items.Count)
        {
            if (ddlOperador.Items[liConta].Selected)// && ddlOperador.Items[liConta].Value != "0")
            {
                hdfOperador.Value += "," + ddlOperador.Items[liConta].Value;
            }
            liConta++;
        }
        ddlOperador.Items.Clear();
        lConexion.Abrir();
        LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A'  and razon_social like '%" + TxtOperador.Text + "%' order by razon_social", 0, 4);
        lConexion.Cerrar();
    }
    protected void btnBorraOpe_Click(object sender, EventArgs e)
    {
        hdfOperador.Value = "0";
        TxtOperador.Text = "";
        ddlOperador.Items.Clear();
        lConexion.Abrir();
        LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' order by razon_social", 0, 4);
        lConexion.Cerrar();
    }
    protected void BtnBusPunto_Click(object sender, EventArgs e)
    {
        int liConta = 0;
        while (liConta < ddlPunto.Items.Count)
        {
            if (ddlPunto.Items[liConta].Selected)// && ddlOperador.Items[liConta].Value != "0")
            {
                HdfPunto.Value += "," + ddlPunto.Items[liConta].Value;
            }
            liConta++;
        }
        ddlPunto.Items.Clear();
        lConexion.Abrir();
        if (ddlProducto.SelectedValue == "G")
            LlenarControles(lConexion.gObjConexion, ddlPunto, "m_pozo", " estado = 'A' and descripcion like '%" + TxtPunto.Text + "%' order by descripcion", 0, 1);
        else
            LlenarControles(lConexion.gObjConexion, ddlPunto, "m_ruta_snt", " estado ='A' and descripcion like '%" + TxtPunto.Text + "%' order by descripcion", 0, 4);
        lConexion.Cerrar();
    }
    protected void btnBorraPunto_Click(object sender, EventArgs e)
    {
        HdfPunto.Value = "0";
        TxtPunto.Text = "";
        ddlPunto.Items.Clear();
        lConexion.Abrir();
        if (ddlProducto.SelectedValue == "G")
            LlenarControles(lConexion.gObjConexion, ddlPunto, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
        else
            LlenarControles(lConexion.gObjConexion, ddlPunto, "m_ruta_snt", " estado ='A' order by descripcion", 0, 4);
        lConexion.Cerrar();
    }
}