﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_DifContSistVerControl.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Informes.frm_DifContSistVerControl" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" Text="Operaciones con Valor Diferente Entre la Subasta y el Registro Final" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Inicial de Vigencia" AssociatedControlID="TxtFechaIni" runat="server" />
                            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%" ValidationGroup="detalle" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Final de Vigencia" AssociatedControlID="TxtFechaFin" runat="server" />
                            <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%" ValidationGroup="detalle" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Número de Operación" AssociatedControlID="TxtOperacionIni" runat="server" />
                            <asp:TextBox ID="TxtOperacionIni" runat="server" ValidationGroup="detalle" CssClass="form-control" Width="100%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtOperacionIni" runat="server" TargetControlID="TxtOperacionIni" FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div id="tdMes01" class="form-group" runat="server">
                            <asp:Label Text="Id Registro" AssociatedControlID="TxtNoId" runat="server" />
                            <asp:TextBox ID="TxtNoId" runat="server" ValidationGroup="detalle" CssClass="form-control" Width="100%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtNoId" runat="server" TargetControlID="TxtNoId" FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div id="tdMes02" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                        <div class="form-group">
                            <asp:Label Text="Número ID" AssociatedControlID="TextBox1" runat="server" />
                            <asp:TextBox ID="TextBox1" runat="server" ValidationGroup="detalle" CssClass="form-control" Width="100%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador Compra" AssociatedControlID="ddlOpeC" runat="server" />
                            <asp:DropDownList ID="ddlOpeC" runat="server" CssClass="form-control selectpicker" data-live-search="true" Width="100%" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador Venta" AssociatedControlID="ddlOpeV" runat="server" />
                            <asp:DropDownList ID="ddlOpeV" runat="server" CssClass="form-control selectpicker" data-live-search="true" Width="100%" />
                        </div>
                    </div>
                </div>
                <%--Grilla--%>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center" OnItemCommand="dtgMaestro_EditCommand">
                                <Columns>
                                    <asp:BoundColumn DataField="numero_contrato" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--2   0171130 rq026-17--%>
                                    <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20180126 rq107-16--%>
                                    <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_compra" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_venta" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="observaciones" HeaderText="observaciones" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more-1"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                    <!--begin::Nav-->
                                                    <ul class="kt-nav">
                                                        <li class="kt-nav__item">
                                                            <asp:LinkButton ID="lkbDetalle" CssClass="kt-nav__link" CommandName="Detalle" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Detalle</span>
                                                            </asp:LinkButton>
                                                        </li>
                                                    </ul>
                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%--Modals--%>

    <%--Modal Detalle--%>
    <div class="modal fade" id="mdlDetalle" tabindex="-1" role="dialog" aria-labelledby="mdlDetalleLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlDetalleInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlDetalleLabel">Detalle</h5>
                        </div>
                        <div class="modal-body">
                            <div class="table table-responsive">
                                <asp:DataGrid ID="dtgDetalle" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <asp:BoundColumn DataField="campo" HeaderText="Ítem" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="valor_sub" HeaderText="Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="valor_verif" HeaderText="Verificación Automática" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="validacion" HeaderText="Validación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                </asp:DataGrid>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="BtnCancelar" runat="server" class="btn btn-secondary" data-dismiss="modal" Text="Cancelar" OnClick="BtnCancelar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

</asp:Content>
