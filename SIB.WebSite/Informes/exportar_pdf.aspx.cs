using System;
using System.Data;
using System.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data.SqlClient;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Diagnostics;
using System.Web.UI;
using Segas.Web.Elements;

/// <summary>
/// 
/// </summary>
public partial class Informes_exportar_pdf : Page
{
    //variables globales de la clase
    clConexion lConexion = null;
    InfoSessionVO goInfo = null;
    string LogErrores;

    //funciones para conversion de datos
    protected double puntosCm(double pix)
    {
        double res;
        res = ((pix / (2.54)) * 72);
        return res;
    }

    public float posVert(double cm)
    {
        double totVertCm = puntosCm(27.94);
        double valVertCm = puntosCm(cm);
        return (float)(totVertCm - valVertCm);
    }

    public float posHor(double cm)
    {
        double totHorizCm = puntosCm(21.6);
        double valHoriCm = puntosCm(cm);
        return (float)valHoriCm;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.EnableViewState = false;
        string lsdatos = "";
        string lstipo_export = "";
        string lsnombre_tabla = "";
        string lscondicion = "";
        string[] lscolumnas = null;
        string lscolumnas2 = "";
        string lsprocedimiento = "";
        string lsdirLogo;
        string lsdirPDF = "";

        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../Login.aspx");

        Document document = new Document(PageSize.LETTER.Rotate(), 5, 5, 5, 5);
        document.AddKeywords("");
        try
        {

            //CAPTURA NOMBRE DE LA TABLA O TITULO PARA ENCABEZADO
            if (this.Request.QueryString["nombre_tabla"] != null)
            {
                lsnombre_tabla = this.Request.QueryString["nombre_tabla"];
            }

            //CAPTURA NOMBRE DE LA TABLA O TITULO PARA ENCABEZADO
            if (this.Request.QueryString["condicion"] != null)
            {
                lscondicion = this.Request.QueryString["condicion"].Replace("#", "%");
            }

            //DESCOMPONE EL ARREGLO DE COLUMNAS A MOSTRAR
            if (this.Request.QueryString["columnas"] != null)
            {
                lscolumnas = this.Request.QueryString["columnas"].Split('*');
                lscolumnas2 = this.Request.QueryString["columnas"].Replace('*', ',');
            }
            //CAPTURA EL NOMBRE DEL PROCEDIMIENTO EN CASO DE QUE VENGA
            if (this.Request.QueryString["procedimiento"] != null)
            {
                lsprocedimiento = this.Request.QueryString["procedimiento"];
            }
            //SELECCIONA EL TIPO DE EXPORTACION SEGUN EL PARAMETRO 1-PROCEDIMIENTO 2-SP_SELECCIONARREGISTROS
            if (this.Request.QueryString["tipo_export"] != null)
            {
                lstipo_export = this.Request.QueryString["tipo_export"];
            }
            // SE CREA EL ARCHIVO
            //a_ruta rutaBuscar = new a_ruta();
            //rutaBuscar.cod_ruta = 7;
            //a_ruta ruta = DelegadaBase.Servicios.traer_a_ruta(goInfo, rutaBuscar);

            lsdirPDF = ConfigurationManager.AppSettings["RutaPDF"].ToString() + "\\" + lsnombre_tabla.ToString() + "_" + goInfo.Usuario.ToString() + ".pdf";
            PdfWriter.GetInstance(document, new FileStream(lsdirPDF, FileMode.Create));

            Table TableCabecera = new Table(2, 2);

            // AGREGA IMAGEN
            //rutaBuscar.cod_ruta = 8;
            //ruta = DelegadaBase.Servicios.traer_a_ruta(goInfo, rutaBuscar);
            //lsdirLogo = ruta.ruta.ToString();
            //Image png = Image.GetInstance(lsdirLogo + "\\logo_bna.gif");
            //png.Alignment = Image.RIGHT_ALIGN | Image.UNDERLYING;
            //png.ScalePercent(80);
            Array oTabla = lsnombre_tabla.Split(',');

            Paragraph lpTitulo1 = new Paragraph("INFORME : " + oTabla.GetValue(0).ToString().ToUpper(), FontFactory.GetFont(FontFactory.HELVETICA, 14, Font.BOLD));
            Paragraph lpTitulo2 = new Paragraph("Usuario : " + goInfo.Usuario.ToString());
            Paragraph lpTitulo3 = new Paragraph("Fecha : " + DateTime.Now); //20190306 rq013-19 ajsute informacion operativa

            Cell CellTitulo1 = new Cell();
            CellTitulo1.Border = 0;
            CellTitulo1.AddElement(lpTitulo1);
            CellTitulo1.VerticalAlignment = Element.ALIGN_MIDDLE;
            CellTitulo1.HorizontalAlignment = Element.ALIGN_LEFT;
            TableCabecera.AddCell(CellTitulo1);

            Cell CellTitulo = new Cell();
            CellTitulo.Border = 0;
            //CellTitulo.AddElement(png);
            CellTitulo.VerticalAlignment = Element.ALIGN_MIDDLE;
            CellTitulo.HorizontalAlignment = Element.ALIGN_LEFT;
            TableCabecera.AddCell(CellTitulo);

            Cell CellTitulo2 = new Cell();
            CellTitulo2.Border = 0;
            CellTitulo2.AddElement(lpTitulo2);
            CellTitulo2.VerticalAlignment = Element.ALIGN_MIDDLE;
            CellTitulo2.HorizontalAlignment = Element.ALIGN_LEFT;
            TableCabecera.AddCell(CellTitulo2);

            Cell CellTitulo3 = new Cell();
            CellTitulo3.Border = 0;
            CellTitulo3.AddElement(lpTitulo3);
            CellTitulo3.VerticalAlignment = Element.ALIGN_MIDDLE;
            CellTitulo3.HorizontalAlignment = Element.ALIGN_LEFT;
            TableCabecera.AddCell(CellTitulo3);

            Paragraph lpTitulo = new Paragraph();
            TableCabecera.Border = 0;
            lpTitulo.Add(TableCabecera);
            HeaderFooter lTitulo3 = new HeaderFooter(lpTitulo, false);
            lTitulo3.Border = 0;
            document.Header = lTitulo3;

            //Table TableCabecera1 = new Table(2, 1);

            //Cell CellTitulo2 = new Cell();
            //CellTitulo2.Border = 0;
            //CellTitulo2.AddElement(lpTitulo2);
            //CellTitulo2.VerticalAlignment = Element.ALIGN_MIDDLE;
            //CellTitulo2.HorizontalAlignment = Element.ALIGN_LEFT;
            //TableCabecera1.AddCell(CellTitulo2);

            //Cell CellTitulo3 = new Cell();
            //CellTitulo3.Border = 0;
            //CellTitulo3.AddElement(lpTitulo3);
            //CellTitulo3.VerticalAlignment = Element.ALIGN_MIDDLE;
            //CellTitulo3.HorizontalAlignment = Element.ALIGN_LEFT;
            //TableCabecera1.AddCell(CellTitulo3);
            HeaderFooter numeracion_pag = new HeaderFooter(new Phrase("Pag. "), true);
            numeracion_pag.Border = 0;
            document.Footer = numeracion_pag;

            //Paragraph lpTitulo5 = new Paragraph();
            //TableCabecera1.Border = 0;
            //lpTitulo5.Add(TableCabecera1);
            //HeaderFooter lTitulo5 = new HeaderFooter(lpTitulo5, true);
            //document.Footer = lTitulo5;
            //document.Add(png);

            // AGREGA TEXTO
            //document.Add(new Paragraph("INFORME : " + lsnombre_tabla.ToUpper(), FontFactory.GetFont(FontFactory.HELVETICA, 14, Font.BOLD)));
            //document.Add(new Paragraph("Usuario : " + goInfo.Usuario.ToString()));
            //document.Add(new Paragraph("Fecha : " + DateTime.Today.ToString()));
            //document.Add(new Paragraph(" "));

            document.Open();

            Table lotabla = null;
            if (Convert.ToInt32(lstipo_export.ToString()) == 1)
            {
                lotabla = llenarTablaPdf(lsprocedimiento, lscondicion, lscolumnas, lsnombre_tabla);
            }
            //else if (Convert.ToInt32(lstipo_export.ToString()) == 2)
            //{
            //    lotabla = llenarTablaPdf(lsnombre_tabla, lscondicion, lscolumnas);
            //}
            //else if (Convert.ToInt32(lstipo_export.ToString()) == 3)
            //{
            //    lotabla = llenarTablaPdf(lsnombre_tabla, lscondicion, lscolumnas, lscolumnas2);
            //}

            //COLOCAMOS ANCHOS A LAS COLUMNAS (OPCIONAL) - Verifica si viene el parametro.. si no lo obvia
            //DESCOMPONE EL ARREGLO DE COLUMNAS A MOSTRAR
            if (this.Request.QueryString["anchoColumnas"] != null)
            {
                string[] lsporcColumnas = null;
                lsporcColumnas = this.Request.QueryString["anchoColumnas"].Split('*');
                anchosColumnas(lotabla, lsporcColumnas);
            }

            document.Add(lotabla);
            lblUrlPdf.Text = "<script>window.open('../PDF/" + lsnombre_tabla.ToString() + "_" + goInfo.Usuario.ToString() + ".pdf');</script>";
            lblUrlPdf.Visible = true;
        }
        catch (Exception ex)
        {
            LogErrores = "SIBEventos";
            EventLog Log = new EventLog();
            Log.Source = this.LogErrores;
            string error = "Error Creando Archico PDF - " + ex.Message + " ::Traza - " + ex.StackTrace;
            Log.WriteEntry("Programa - " + "Generación de Informes en PDF" + " ::Usuario - " + goInfo.Usuario + " ::Mensaje - " + error, EventLogEntryType.Error);
            Toastr.Error(this, ex.Message);
        }
        document.Close();


    }

    private Table llenarTablaPdf(string lsprocedimiento, string lsCondicion, string[] lscolumnas, string lsTabla)
    {
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        SqlDataReader lLector;
        SqlCommand lComando = new SqlCommand();
        Table ltabla = new Table(lscolumnas.Length, 1);

        ltabla.Padding = 1;
        ltabla.Spacing = 1;

        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandText = lsprocedimiento;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.Parameters.Add(new SqlParameter("@P_tabla", lsTabla));
        lComando.Parameters.Add(new SqlParameter("@P_filtro", lsCondicion));
        lLector = lComando.ExecuteReader();
        if (lLector.HasRows == true)
        {
            foreach (string lshead in lscolumnas)
            {
                Cell lcell = new Cell(lshead.Replace('_', ' ').ToUpper());
                lcell.Header = true;
                lcell.BorderWidth = 1;
                ltabla.AddCell(lcell);
            }
            ltabla.EndHeaders();
            int y = 1;
            while (lLector.Read())
            {
                int x = 0;
                foreach (string lscol in lscolumnas)
                {
                    Cell lcell2 = new Cell(lLector[lscol].ToString());
                    ltabla.AddCell(lcell2, y, x);
                    x = x + 1;
                }
                y = y + 1;
            }
        }
        ltabla.WidthPercentage = 95;
        lConexion.Cerrar();

        return ltabla;
    }

    private float[] anchosColumnas(Table lotabla, string[] lsanchosColumnas)
    {
        // VERIFICAMOS QUE CONCUERDEN LOS ANCHOS CON EL NUMERO DE COLUMNAS
        float[] lfanchCol = new float[lsanchosColumnas.Length];
        if (lotabla.Columns == lsanchosColumnas.Length)
        {
            // CALCULAMOS EL PORCENTAJE PASADO CON RESPECTO A LOS ANCHOS
            //float[] anchCol = null;
            int liconta = 0;
            float lfanchoActual = lotabla.Width;
            foreach (string liporcCol in lsanchosColumnas)
            {
                //anchCol[conta]=((float)tabla.Width * (float)Convert.ToInt32(porcCol))/100;
                lfanchCol[liconta] = lfanchoActual;
                lfanchCol[liconta] = lfanchCol[liconta] * Convert.ToInt32(liporcCol);
                lfanchCol[liconta] = lfanchCol[liconta] / 100;
                liconta += 1;
            }
            lotabla.Widths = lfanchCol;
        }
        return lfanchCol;
    }

}
