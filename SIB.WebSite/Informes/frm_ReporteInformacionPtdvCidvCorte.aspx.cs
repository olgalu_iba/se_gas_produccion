﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Informes
{
    public partial class frm_ReporteInformacionPtdvCidvCorte : Page
    {
        private InfoSessionVO goInfo;
        private static string lsTitulo = "Reporte trazabilidad Información PTDV y CIDV a fecha de Corte";
        private clConexion lConexion;
        private SqlDataReader lLector;
        private DataSet lds = new DataSet();
        private SqlDataAdapter lsqldata = new SqlDataAdapter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo.ToString();
            lConexion = new clConexion(goInfo);

            buttons.FiltrarOnclick += imbConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;

            if (IsPostBack) return;
            //Titulo
            Master.Titulo = "Informes";
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' And codigo_operador != 0 order by codigo_operador", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlNoReg, "m_operador_no_reg", " estado = 'A' order by nombre_operador", 1, 2); 
            LlenarControles(lConexion.gObjConexion, ddlPuntoSnt, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMesIni, "m_mes", " 1=1 order by mes", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMesFinal, "m_mes", " 1=1 order by mes", 0, 1);
            lConexion.Cerrar();
            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
            }

            buttons.SwitchOnButton(EnumBotones.Buscar);
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, int liIndiceLlave, int liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            var lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                var lItem1 = new System.Web.UI.WebControls.ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                }
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbConsultar_Click(object sender, EventArgs e)
        {
            var Error = false;
            int liValor;
            var lblMensaje = new StringBuilder();
            if (TxtAnoCarga.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtAnoCarga.Text);
                    if (liValor <= 0)
                    {
                        lblMensaje.Append("Valor Inválido en el Campo Año Carga. <br>");
                        Error = true;
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Valor Inválido en el Campo Año Carga. <br>");
                    Error = true;
                }
            }
            else
            {
                lblMensaje.Append("Debe Ingresar el Año Carga. <br>");
                Error = true;
            }
            if (TxtAnoIni.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtAnoIni.Text);
                    if (liValor <= 0)
                    {
                        lblMensaje.Append("Valor Inválido en el Campo Año Final. <br>");
                        Error = true;
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo Año Inicial. <br>");
                    Error = true;
                }
            }
            else
            {
                lblMensaje.Append("Debe Ingresar el Año Inicial. <br>");
                Error = true;
            }
            if (TxtAnoFin.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtAnoFin.Text);
                    if (liValor <= 0)
                    {
                        lblMensaje.Append("Valor Inválido en el Campo Año Final. <br>");
                        Error = true;
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo Año Final. <br>");
                    Error = true;
                }
            }
            else
                lblMensaje.Append("Debe Ingresar el Año Final. <br>");
            if (ddlMesIni.SelectedValue == "0")
                lblMensaje.Append("Debe Ingresar el Mes Inicial. <br>");
            if (ddlMesFinal.SelectedValue == "0")
                lblMensaje.Append("Debe Ingresar el Mes Final. <br>");
            
            //20220118
            if (TxtFechaRes.Text != "")
            {
                try
                {
                    Convert.ToDateTime(TxtFechaRes.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("La fecha de resolución no es válida<br>");
                }
            }
            if (TxtCodigoReg.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCodigoReg.Text);
                    if (liValor <= 0)
                    {
                        lblMensaje.Append("Valor Inválido en el código del registro. <br>");
                        Error = true;
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo código de registro. <br>");
                    Error = true;
                }
            }
            if (!Error && string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                try
                {
                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    var lComando = new SqlCommand();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandTimeout = 3600;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_GetInformePtdvCidvCorte";
                    lComando.Parameters.Add("@P_fecha_corte", SqlDbType.VarChar ).Value = TxtFechaCorte.Text;
                    lComando.Parameters.Add("@P_ano_ini", SqlDbType.Int).Value = TxtAnoIni.Text.Trim();
                    lComando.Parameters.Add("@P_mes_ini", SqlDbType.Int).Value = ddlMesIni.SelectedValue;
                    lComando.Parameters.Add("@P_ano_fin", SqlDbType.Int).Value = TxtAnoFin.Text.Trim();
                    lComando.Parameters.Add("@P_mes_fin", SqlDbType.Int).Value = ddlMesFinal.SelectedValue;
                    lComando.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = ddlOperador.SelectedValue;
                    lComando.Parameters.Add("@P_codigo_punto_snt", SqlDbType.Int).Value = ddlPuntoSnt.SelectedValue;
                    lComando.Parameters.Add("@P_ano_carga", SqlDbType.Int).Value = TxtAnoCarga.Text.Trim();
                    lComando.Parameters.Add("@P_nit_operador", SqlDbType.VarChar).Value = ddlNoReg.SelectedValue;  //20170705 rq025-17 indicadores MP fase III
                    lComando.Parameters.Add("@P_tipo_informacion", SqlDbType.VarChar).Value = ddlTipo.SelectedValue; //20170705 rq025-17 indicadores MP fase III
                    lComando.Parameters.Add("@P_estado", SqlDbType.VarChar).Value = ddlEstado.SelectedValue; //20220118
                    lComando.Parameters.Add("@P_fecha_resolucion", SqlDbType.VarChar).Value = TxtFechaRes.Text ; //20220118
                    lComando.Parameters.Add("@P_codigo_registro", SqlDbType.VarChar).Value = TxtCodigoReg.Text; //20220118
                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgMaestro.DataSource = lds;
                    dtgMaestro.DataBind();

                    buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
                    lConexion.Cerrar();
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, "No se Pudo Generar el Informe.! " + ex.Message);
                }
            }

            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                Toastr.Error(this, lblMensaje.ToString());
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            var lsNombreArchivo = goInfo.Usuario.ToString() + "InfInformacionPTDV" + DateTime.Now + ".xls";
            var lstitulo_informe = "Informe de Información PTDV y CIDV";
            var lsTituloParametros = "";
            try
            {
                lsTituloParametros = " Fecha Corte: " + TxtFechaCorte.Text + " - Año Carga: " + TxtAnoCarga.Text ;
                lsTituloParametros += " - Año Inicial: " + TxtAnoIni.Text + " - Mes Inicial: " + ddlMesIni.SelectedItem.ToString();
                lsTituloParametros += " - Año Final: " + TxtAnoFin.Text + " - Mes Final: " + ddlMesFinal.SelectedItem.ToString();

                if (ddlOperador.SelectedValue != "0")
                    lsTituloParametros += "  - Operador: " + ddlOperador.SelectedItem.ToString();
                //20170705 rq025-17 indicadores MP fase III
                if (ddlPuntoSnt.SelectedValue != "0")
                    lsTituloParametros += "  - Campo o Punto SNT: " + ddlPuntoSnt.SelectedItem.ToString();
                //20220118
                if (ddlEstado.SelectedValue !="")
                    lsTituloParametros += "  - Estado: " + ddlEstado.SelectedItem.ToString();
                //20220118
                if (TxtFechaRes.Text != "")
                    lsTituloParametros += "  - Fecha Resolución: " + TxtFechaRes.Text;

                decimal ldCapacidad = 0;
                var lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                var lsw = new StringWriter(lsb);
                var lhtw = new HtmlTextWriter(lsw);
                var lpagina = new Page();
                var lform = new HtmlForm();
                dtgMaestro.EnableViewState = false;
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                lform.Controls.Add(dtgMaestro);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = System.Text.Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());

                Response.End();
                lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pudo Generar el Excel.!" + ex.Message.ToString());

            }
        }
    }
}