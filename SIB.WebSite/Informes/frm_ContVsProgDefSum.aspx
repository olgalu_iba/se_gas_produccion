﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ContVsProgDefSum.aspx.cs"
    Inherits="Informes_frm_ContVsProgDefSum" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                       
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Inicial de vigencia</label>
                            <asp:TextBox ID="TxtFechaIni" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Final de vigencia</label>
                            <asp:TextBox ID="TxtFechaFin" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>

                  

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div id="tdMes01" class="form-group" runat="server">
                            <label for="Name">Vendedor</label>
                            <asp:DropDownList ID="DdlVendedor" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true"> <%--20180126 rq107-16--%>
                            </asp:DropDownList>

                        </div>
                    </div>
                   
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Comprador</label>
                        <asp:DropDownList ID="DdlComprador" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true"> <%--20180126 rq107-16--%>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name"> Tipo Demanda</label>
                            <asp:DropDownList ID="DdlDemanda" runat="server" OnSelectedIndexChanged ="DdlDemanda_SelectedIndexChanged" AutoPostBack="true" Width="100%" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                   
                     <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name"> Sector Consumo</label>
                             <asp:DropDownList ID="DdlSector" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name"> Tipo Mercado</label>
                               <asp:DropDownList ID="DdlMercado" runat="server" Width="100%" CssClass="form-control">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="P">Primario</asp:ListItem>
                                <asp:ListItem Value="S">Secundario</asp:ListItem>
                                <asp:ListItem Value="O">Otras transacciones del Mercado Mayorista</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                     <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Modalidad de Contrato</label>
                                <asp:DropDownList ID="DdlModalidad" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
     
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png" Visible="false"
                    OnClick="imbConsultar_Click" Height="40" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
          
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
           
    </div>
    <div border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla" visible="false">
      
               <div class="table table-responsive" style=" height: 450px;">
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered"
                        PagerStyle-HorizontalAlign="Center">
                     
                        <Columns>
                            <asp:BoundColumn DataField="Fecha" HeaderText="fecha" ItemStyle-HorizontalAlign="Center"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_vendedor" HeaderText="operador vta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_operacion" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_rol_compra" HeaderText="rol compra" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_comprador" HeaderText="operador compra" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_demanda" HeaderText="tipo demanda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_sector" HeaderText="sector de consumo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_destino" HeaderText="destino" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_nom" HeaderText="cantidad nominada" ItemStyle-HorizontalAlign="Right">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_post" HeaderText="cnt post re-nominaciones"
                                ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            <asp:BoundColumn DataField="dif_nom_post" HeaderText="Cnt Nom Vs Cnt post" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_cont" HeaderText="cantidad contratada" ItemStyle-HorizontalAlign="Right">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="dif_cont_nom" HeaderText="Cnt cont Vs Cnt nom" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="dif_cont_post" HeaderText="cnt cont Vs cnt post" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="tipo mercado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="modalidad contractual" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="precio" ItemStyle-HorizontalAlign="Right">
                            </asp:BoundColumn>
                        </Columns>
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </div>
          
                 </div>
            </div>
        </div>
    </div>
   
    
 </asp:Content>