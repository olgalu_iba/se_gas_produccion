﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ConsModEjecucion.aspx.cs"
    Inherits="Informes_frm_ConsModEjecucion" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
            width="80%">
            <tr>
                <td align="center" class="th1">
                    <asp:Label ID="lblTitulo" runat="server" ForeColor="White" ></asp:Label>
                </td>
            </tr>
        </table>
    <br /><br /><br /><br /><br /><br />
        <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
            width="80%">
            <tr>
                <td class="td1">Reporte
                </td>
                <td class="td2" colspan="1">
                    <asp:DropDownList ID="DdlReporte" runat="server" Width="250px">
                        <asp:ListItem Value="C">Consulta Modificaciones Ejecución de Contratos </asp:ListItem>
                        <asp:ListItem Value="U">Consulta Ejecución de Contratos Usuarios Finales</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="td1">Número de Contrato:
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtContrato" runat="server" ValidationGroup="detalle" MaxLength="30"></asp:TextBox>
                </td>
                <td class="td1">Número de Operación:
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtOperacion" runat="server" ValidationGroup="detalle"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="ftebTxtOperacion" runat="server" TargetControlID="TxtOperacion"
                        FilterType="Custom, Numbers">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td class="td1">Número de Ejecución:
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtCodEjec" runat="server" ValidationGroup="detalle"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="ftebTxtCodEjec" runat="server" TargetControlID="TxtCodEjec"
                        FilterType="Custom, Numbers">
                    </cc1:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td class="td1">Fecha de Gas:
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    - 
                    <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                </td>
                <td classs="td1">Id registro:
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtId" runat="server" ValidationGroup="detalle"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="ftebTxtId" runat="server" TargetControlID="TxtId"
                        FilterType="Custom, Numbers">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td class="td1">Operador
                </td>
                <td class="td2" colspan="1">
                    <asp:DropDownList ID="DdlOperador" runat="server" Width="300px">
                    </asp:DropDownList>
                </td>
                <td class="td1">Punta:
                </td>
                <td class="td2" colspan="1">
                    <asp:DropDownList ID="DdlPunta" runat="server">
                        <asp:ListItem Value="0">Seleccione</asp:ListItem>
                        <asp:ListItem Value="C">Compra</asp:ListItem>
                        <asp:ListItem Value="V">Venta</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblMensaje">
            <tr>
                <td class="th1" colspan="3" align="center">
                    <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                        OnClick="imbConsultar_Click" Height="40" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblGrilla" visible="false">
            <tr>
                <td colspan="3" align="center">

                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgMaestro_EditCommand" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <%--0--%>
                            <asp:BoundColumn DataField="fecha_gas" HeaderText="Fecha Gas" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <%--1--%>
                            <asp:BoundColumn DataField="desc_subasta" HeaderText="Subasta"></asp:BoundColumn>
                            <%--2--%>
                            <asp:BoundColumn DataField="desc_mercado" HeaderText="Mercado"></asp:BoundColumn>
                            <%--3--%>
                            <asp:BoundColumn DataField="desc_producto" HeaderText="Producto"></asp:BoundColumn>
                            <%--4--%>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual"></asp:BoundColumn>
                            <%--5--%>
                            <asp:BoundColumn DataField="codigo_ejec_c" HeaderText="Código Ejecución Compra"></asp:BoundColumn>
                            <%--6--%>
                            <asp:BoundColumn DataField="codigo_ejec_v" HeaderText="Código Ejecución venta"></asp:BoundColumn>
                            <%--7--%>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--8--%>
                            <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato Definitivo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>

                            <%--9--%>
                            <asp:BoundColumn DataField="codigo_punto" HeaderText="Código Punto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--10--%>
                            <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20190524 rq029-19--%>
                            <%--11--%>
                            <%--<asp:BoundColumn DataField="tipo_demanda" HeaderText="Código Tipo Demanda"></asp:BoundColumn>--%>
                            <%--12--%>
                            <%--<asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda"></asp:BoundColumn>--%>
                            <%--13--%>
                            <%--<asp:BoundColumn DataField="sector_consumo" HeaderText="Código Sector Consumo"></asp:BoundColumn>--%>
                            <%--14--%>
                            <%--<asp:BoundColumn DataField="desc_sector" HeaderText="Sector Consumo"></asp:BoundColumn>--%>
                            <%--20190524 fin rq029-19--%>
                            <%--11--%>
                            <asp:BoundColumn DataField="operador_compra" HeaderText="Código Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--12--%>
                            <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Comprador" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--13--%>
                            <asp:BoundColumn DataField="operador_venta" HeaderText="Código Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--14--%>
                            <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--15--%>
                            <asp:BoundColumn DataField="cantidad_compra" HeaderText="Cantidad Compra" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <%--16--%>
                            <asp:BoundColumn DataField="cantidad_venta" HeaderText="Cantidad Venta" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <%--17--%>
                            <asp:BoundColumn DataField="Valor_compra" HeaderText="Valor Compra" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            <%--18--%>
                            <asp:BoundColumn DataField="Valor_venta" HeaderText="Valor Venta" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            <%--19--%>
                            <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20--%>
                            <asp:BoundColumn DataField="observacion_mod" HeaderText="Observaciones Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--21--%>
                            <asp:EditCommandColumn HeaderText="Detalle" EditText="Detalle"></asp:EditCommandColumn>
                        </Columns>
                    </asp:DataGrid>

                </td>
            </tr>
        </table>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tbDetalle" visible="false">
            <tr>
                <td align="center">
                    <asp:Button ID="BtnRegresar" runat="server" Text="Regresar" OnClick="BtnRegresar_Click" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel1" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel1_Click"
                    Height="35" />
                    <asp:HiddenField ID="hdnCodigoC" runat="server" />
                    <asp:HiddenField ID="hdnCodigoV" runat="server" />
                    <asp:HiddenField ID="hdnContrato" runat="server" />
                    <br />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblMensajeDet" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">

                    <asp:DataGrid ID="dtgDetalle" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <%--0--%>
                            <asp:BoundColumn DataField="fecha_hora_evento" HeaderText="Fecha Hora Evento" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--1--%>
                            <asp:BoundColumn DataField="usuario_evento" HeaderText="Usuario Evento" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--2--%>
                            <asp:BoundColumn DataField="evento" HeaderText="Evento" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--3--%>
                            <asp:BoundColumn DataField="fecha_gas" HeaderText="Fecha Gas" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <%--4--%>
                            <asp:BoundColumn DataField="desc_subasta" HeaderText="Subasta"></asp:BoundColumn>
                            <%--5--%>
                            <asp:BoundColumn DataField="desc_mercado" HeaderText="Mercado"></asp:BoundColumn>
                            <%--6--%>
                            <asp:BoundColumn DataField="desc_producto" HeaderText="Producto"></asp:BoundColumn>
                            <%--7--%>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual"></asp:BoundColumn>
                            <%--8--%>
                            <asp:BoundColumn DataField="Punta" HeaderText="Punta"></asp:BoundColumn>
                            <%--9--%>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--10--%>
                            <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato Definitivo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--11--%>
                            <asp:BoundColumn DataField="codigo_punto" HeaderText="Código Punto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--12--%>
                            <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20190524 rq029-19--%>
                            <%--13--%>
                            <%--<asp:BoundColumn DataField="tipo_demanda" HeaderText="Código Tipo Demanda"></asp:BoundColumn>--%>
                            <%--14--%>
                            <%--<asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda"></asp:BoundColumn>--%>
                            <%--15--%>
                            <%--<asp:BoundColumn DataField="sector_consumo" HeaderText="Código Sector Consumo"></asp:BoundColumn>--%>
                            <%--16--%>
                            <%--<asp:BoundColumn DataField="desc_sector" HeaderText="Sector Consumo"></asp:BoundColumn>--%>
                            <%--20190524 rq029-19--%>
                            <%--13--%>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--14--%>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--15--%>
                            <asp:BoundColumn DataField="cantidad_autorizada" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <%--16--%>
                            <asp:BoundColumn DataField="Valor_facturado" HeaderText="Valor facturado" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            <%--17--%>
                            <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--18--%>
                            <asp:BoundColumn DataField="observacion_mod" HeaderText="Observaciones Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgDetalleUsr" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <%--0--%>
                            <asp:BoundColumn DataField="fecha_hora_evento" HeaderText="Fecha Hora Evento" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--1--%>
                            <asp:BoundColumn DataField="usuario_evento" HeaderText="Usuario Evento" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--2--%>
                            <asp:BoundColumn DataField="evento" HeaderText="Evento" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--3--%>
                            <asp:BoundColumn DataField="fecha_gas" HeaderText="Fecha Gas" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <%--4--%>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--5--%>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--6--%>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--7--%>
                            <asp:BoundColumn DataField="codigo_punto" HeaderText="Código Punto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--8--%>
                            <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20190524 rq029-19--%>
                            <%--9--%>
                            <asp:BoundColumn DataField="tipo_demanda" HeaderText="Código Tipo Demanda"></asp:BoundColumn>
                            <%--10--%>
                            <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda"></asp:BoundColumn>
                            <%--11--%>
                            <asp:BoundColumn DataField="sector_consumo" HeaderText="Código Sector Consumo"></asp:BoundColumn>
                            <%--12--%>
                            <asp:BoundColumn DataField="desc_sector" HeaderText="Sector Consumo"></asp:BoundColumn>
                            <%--13--%>
                            <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Código Punto Salida"></asp:BoundColumn>
                            <%--14--%>
                            <asp:BoundColumn DataField="desc_salida" HeaderText="Punto Salida"></asp:BoundColumn>
                            <%--20190524 rq029-19--%>
                            <%--15--%>
                            <asp:BoundColumn DataField="nit_usuario_no_regulado" HeaderText="Nit Usuario"></asp:BoundColumn>
                            <%--16--%>
                            <asp:BoundColumn DataField="nombre_usuario" HeaderText="Nombre Usuario"></asp:BoundColumn>
                            <%--17--%>
                            <asp:BoundColumn DataField="codigo_mercado_relevante" HeaderText="Código Mercado relevante"></asp:BoundColumn>
                            <%--18--%>
                            <asp:BoundColumn DataField="desc_mercado" HeaderText="Mercado Relevante"></asp:BoundColumn>
                            <%--19--%>
                            <%--20190524 rq029-19--%>
                            <asp:BoundColumn DataField="cantidad_contratada" HeaderText="Cantidad contratada" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>

                </td>
            </tr>
        </table>
</asp:Content>