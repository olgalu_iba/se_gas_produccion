﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;

public partial class Informes_frm_ContratosAgenteTermico : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Contratación por Agente Térmico";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles1(lConexion.gObjConexion, DdlMercado, "m_tipo_mercado", " 1=1 order by descripcion", 0, 1);
            LlenarControles1(lConexion.gObjConexion, DdlSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles1(lConexion.gObjConexion, DdlRol, "m_tipos_operador", " sigla in ('G','C') order by descripcion", 2, 1);
            LlenarControles(lConexion.gObjConexion, DdlOperador, "m_operador ope", " (tipo_operador='G' or tipo_operador='C' and exists (select 1 from m_operador ope1 where ope.no_documento = ope1.no_documento  and ope1.tipo_operador ='G')) order by razon_social", 0, 4);
            LlenarControles1(lConexion.gObjConexion, DdlModalidad, "m_modalidad_contractual", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles1(lConexion.gObjConexion, DdlSector, "m_sector_consumo", " estado = 'A' order by descripcion", 0, 1);
            lConexion.Cerrar();
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();

            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF = DateTime.Now;
        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
                if (ldFechaI.Day != 1)
                    lblMensaje.Text += "La Fecha Inicial debe ser el primer día del mes. <br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial. <br>";
            }
        }
        else
            lblMensaje.Text += "Debe digitar la Fecha Inicial. <br>";
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                if (TxtFechaIni.Text.Trim().Length > 0)
                {
                    ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
                    if (ldFechaI > ldFechaF)
                        lblMensaje.Text += "La Fecha Final NO puede ser Menor que la Fecha de Inicial. <br>";
                    if (VerificarExistencia("m_fecha_reporte", "año=" + ldFechaF.Year.ToString() + " and mes = " + ldFechaF.Month.ToString() + " and fecha >'" + TxtFechaFin.Text + "'"))
                        lblMensaje.Text += "La Fecha Final debe ser el último día del mes. <br>";
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Final. <br>";
            }
        }
        else
            lblMensaje.Text += "Debe digitar la Fecha Final. <br>";

        if (lblMensaje.Text == "")
        {
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetContAgenteTermico";
                lComando.Parameters.Add("@P_fecha_inicial", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                lComando.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaFin.Text.Trim();
                lComando.Parameters.Add("@P_tipo_mercado", SqlDbType.VarChar).Value = DdlMercado.SelectedValue;
                lComando.Parameters.Add("@P_codigo_tipo_subasta", SqlDbType.Int).Value = DdlSubasta.SelectedValue;
                lComando.Parameters.Add("@P_rol_operador", SqlDbType.VarChar).Value = DdlRol.SelectedValue;
                lComando.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = DdlOperador.SelectedValue;
                lComando.Parameters.Add("@P_punta", SqlDbType.VarChar).Value = DdlPunta.SelectedValue;
                lComando.Parameters.Add("@P_codigo_modalidad", SqlDbType.Int).Value = DdlModalidad.SelectedValue;
                lComando.Parameters.Add("@P_codigo_sector", SqlDbType.Int).Value = DdlSector.SelectedValue;

                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgMaestro.DataSource = lds;
                dtgMaestro.DataBind();
                tblGrilla.Visible = true;
                imbExcel.Visible = true;
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfContratoAgenteTermico" + DateTime.Now + ".xls";
        string lstitulo_informe = "";
        string lsTituloParametros = "";
        try
        {
            dtgMaestro.Columns[8].Visible = true;
            dtgMaestro.Columns[10].Visible = true;
            dtgMaestro.Columns[12].Visible = true;
            dtgMaestro.Columns[14].Visible = true;
            lstitulo_informe = "Contratación de Agentes Térmicos";
            lsTituloParametros += " - Fecha Inicial: " + TxtFechaIni.Text.Trim() + " - Fecha Final: " + TxtFechaFin.Text.Trim();
            if (DdlMercado.SelectedValue != "0")
                lsTituloParametros += "  - Mercado: " + DdlMercado.SelectedItem.ToString();
            if (DdlSubasta.SelectedValue != "0")
                lsTituloParametros += "  - Subasta: " + DdlSubasta.SelectedItem.ToString();
            if (DdlRol.SelectedValue != "0")
                lsTituloParametros += "  - Rol Operador: " + DdlRol.SelectedItem.ToString();
            if (DdlOperador.SelectedValue != "0")
                lsTituloParametros += "  - Operador: " + DdlOperador.SelectedItem.ToString();
            if (DdlPunta.SelectedValue != "")
                lsTituloParametros += "  - Punta: " + DdlPunta.SelectedItem.ToString();
            if (DdlModalidad.SelectedValue != "0")
                lsTituloParametros += "  - Modalidad: " + DdlModalidad.SelectedItem.ToString();
            if (DdlSector.SelectedValue != "0")
                lsTituloParametros += "  - Sector Consumo: " + DdlSector.SelectedItem.ToString();
            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgMaestro.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            lform.Controls.Add(dtgMaestro);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
            dtgMaestro.Columns[8].Visible = false;
            dtgMaestro.Columns[10].Visible = false;
            dtgMaestro.Columns[12].Visible = false;
            dtgMaestro.Columns[14].Visible = false;

        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Excel.!" + ex.Message.ToString();
        }
    }
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lsTabla, string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lsTabla, lswhere, goInfo);
    }

    protected void DdlRol_SelectedIndexChanged(object sender, EventArgs e)
    {
        DdlOperador.Items.Clear();
        lConexion.Abrir();
        if (DdlRol.SelectedValue == "0")
            LlenarControles(lConexion.gObjConexion, DdlOperador, "m_operador ope", " (tipo_operador='G' or tipo_operador='C' and exists (select 1 from m_operador ope1 where ope.no_documento = ope1.no_documento  and ope1.tipo_operador ='G')) order by razon_social", 0, 4);
        else
            LlenarControles(lConexion.gObjConexion, DdlOperador, "m_operador ope", " tipo_operador ='" + DdlRol.SelectedValue + "' and (tipo_operador='G' or tipo_operador='C' and exists (select 1 from m_operador ope1 where ope.no_documento = ope1.no_documento  and ope1.tipo_operador ='G')) order by razon_social", 0, 4);
        lConexion.Cerrar();
    }
}