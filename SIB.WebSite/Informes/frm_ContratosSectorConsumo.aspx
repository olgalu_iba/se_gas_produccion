﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ContratosSectorConsumo.aspx.cs"
    Inherits="Informes_frm_ContratosSectorConsumo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />

                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Inicial de vigencia</label>
                            <asp:TextBox ID="TxtFechaIni" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Final de vigencia</label>
                            <asp:TextBox ID="TxtFechaFin" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Rol vendedor</label>
                            <asp:DropDownList ID="DdlRolV" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div id="tdMes01" class="form-group" runat="server">
                            <label for="Name">Vendedor</label>
                            <asp:DropDownList ID="DdlVendedor" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                                <%--20180126 rq107-16--%>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="tdMes02" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                        <div class="form-group">
                            <label for="Name">Rol Comprador</label>
                            <asp:DropDownList ID="DdlRolC" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Comprador</label>
                            <asp:DropDownList ID="DdlComprador" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                                <%--20180126 rq107-16--%>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Modalidad Contrato</label>
                            <asp:DropDownList ID="DdlModalidad" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Tipo Demanda</label>
                            <asp:DropDownList ID="DdlDemanda" runat="server" OnSelectedIndexChanged="DdlDemanda_SelectedIndexChanged"
                                AutoPostBack="true" Width="100%" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Sector de Consumo</label>
                            <asp:DropDownList ID="DdlSector" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>

                <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                    id="tblMensaje">

                    <tr>
                        <td colspan="3" align="center">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                </table>
                <div runat="server"
                    id="tblGrilla" visible="false">

                    <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                        <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered">

                            <Columns>
                                <asp:BoundColumn DataField="Año" HeaderText="año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="mes" HeaderText="mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tipo_rueda" HeaderText="tpo Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tipo_subasta" HeaderText="tpo Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="No. contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numero_operacion" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="id_registro" HeaderText="Id registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_v" HeaderText="operador V" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="rol_venta" HeaderText="Rol venta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_c" HeaderText="operador C" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="rol_compra" HeaderText="Rol compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="tipo mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20170926 rq027-17--%>
                                <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,##0}"></asp:BoundColumn>
                                <%--20170926 rq027-17--%>
                                <asp:BoundColumn DataField="capacidad_transporte" HeaderText="capacidad transporte"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_modalidad" HeaderText="modalidad contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="periodo_entrega" HeaderText="periodo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="no_identificacion_usr" HeaderText="nit usr final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_usuario" HeaderText="nombre" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_sector" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="punto_salida" HeaderText="punto salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="demanda_a_atender" HeaderText="tipo demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_contratada" HeaderText="cnt_usr_final" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,##0}"></asp:BoundColumn>
                                <%--20160602--%>
                                <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20160602--%>
                                <asp:BoundColumn DataField="desc_fuente" HeaderText="desc fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>

                </div>

            </div>
        </div>
    </div>


</asp:Content>
