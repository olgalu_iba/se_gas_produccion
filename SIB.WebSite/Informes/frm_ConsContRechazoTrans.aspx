﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ConsContRechazoTrans.aspx.cs" Inherits="Informes.Informes_frm_ConsContRechazoTrans" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:crudbutton id="buttons" runat="server" />
            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tipo Subasta" AssociatedControlID="ddlSubasta" runat="server" />
                            <asp:DropDownList ID="ddlSubasta" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server"  />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tipo Rueda" AssociatedControlID="ddlRueda" runat="server" />
                            <asp:DropDownList ID="ddlRueda" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server"  />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Año Negociación" AssociatedControlID="TxtAño" runat="server" />
                            <asp:TextBox ID="TxtAño" runat="server" Width="100%" CssClass="form-control" ValidationGroup="detalle"></asp:TextBox>
                            <ajaxtoolkit:filteredtextboxextender id="ftebTxtAño" runat="server" targetcontrolid="TxtAño"
                                filtertype="Custom, Numbers"></ajaxtoolkit:filteredtextboxextender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Trimestre Negociación" AssociatedControlID="ddlTrimestre" runat="server" />
                            <asp:DropDownList ID="ddlTrimestre" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador Venta" AssociatedControlID="ddlOperador" runat="server" />
                            <asp:DropDownList ID="ddlOperador" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server"/>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Número rueda" AssociatedControlID="TxtRueda" runat="server" />
                            <asp:TextBox ID="TxtRueda" runat="server" autocomplete="off" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtRueda" runat="server" TargetControlID="TxtRueda"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                </div>

                <div runat="server" visible="false" id="tblGrilla">
                    <div class="table table-responsive">
                        <asp:DataGrid ID="dtgMaestro" AutoGenerateColumns="False" AllowPaging="true" PageSize="20" OnPageIndexChanged="dtgMaestro_PageIndexChanged" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" runat="server">
                            <columns>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numero_rueda" HeaderText="Número Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_rueda" HeaderText="Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_venta" HeaderText="Código Venta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Venta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_compra" HeaderText="Código Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad (KPCD)" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:###,###,###,###}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="precio" HeaderText="Precio (USD/KPC)" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:###,##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_ruta" HeaderText="Código Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Right"  ></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_entrega_ini" HeaderText="Fecha Entrega Inicial" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:yyyy/MM/dd}" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_entrega_fin" HeaderText="Fecha Entrega Final" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:yyyy/MM/dd}" ></asp:BoundColumn>
                            </columns>
                            <headerstyle cssclass="theadColor" horizontalalign="Center" />
                            <pagerstyle mode="NumericPages" position="TopAndBottom" horizontalalign="Center" />
                        </asp:DataGrid>
                    </div>
                    <asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False" AllowPaging="false" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered">
                        <columns>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numero_rueda" HeaderText="Número Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_rueda" HeaderText="Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_venta" HeaderText="Código Venta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Venta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_compra" HeaderText="Código Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad (KPCD)" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:###,###,###,###}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="precio" HeaderText="Precio (USD/KPC)" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:###,##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_ruta" HeaderText="Código Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Right"  ></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_entrega_ini" HeaderText="Fecha Entrega Inicial" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:yyyy/MM/dd}" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_entrega_fin" HeaderText="Fecha Entrega Final" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:yyyy/MM/dd}" ></asp:BoundColumn>
                        </columns>
                        <headerstyle cssclass="theadColor" horizontalalign="Center" />
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
