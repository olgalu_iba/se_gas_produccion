﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ContratosAgenteTermico.aspx.cs"
    Inherits="Informes_frm_ContratosAgenteTermico" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div>
                    <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
                        width="80%">
                        <tr>
                            <td align="center" class="th1">
                                <asp:Label ID="lblTitulo" runat="server" ForeColor="White" ></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <br /><br /><br /><br /><br /><br />
                    <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
                        width="80%">
                        <tr>
                            <td class="td1">Fecha Inicial de vigencia
                            </td>
                            <td class="td2">
                                <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                            </td>
                            <td class="td1">Fecha Final de vigencia
                            </td>
                            <td class="td2">
                                <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1">Tipo Mercado
                            </td>
                            <td class="td2">
                                <asp:DropDownList ID="DdlMercado" runat="server" Width="250px">
                                </asp:DropDownList>
                            </td>
                            <td class="td1">Tipo Subasta
                            </td>
                            <td class="td2">
                                <asp:DropDownList ID="DdlSubasta" runat="server" Width="250px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1">Rol Operador
                            </td>
                            <td class="td2">
                                <asp:DropDownList ID="DdlRol" runat="server" OnSelectedIndexChanged="DdlRol_SelectedIndexChanged" AutoPostBack="true" >
                                </asp:DropDownList>
                            </td>
                            <td class="td1">Operador
                            </td>
                            <td class="td2">
                                <asp:DropDownList ID="DdlOperador" runat="server" Width="250px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1">Punta
                            </td>
                            <td class="td2">
                                <asp:DropDownList ID="DdlPunta" runat="server">
                                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                                    <asp:ListItem Value="C">Comprador</asp:ListItem>
                                    <asp:ListItem Value="V">Vendedor</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="td1">Modalidad Contractual
                            </td>
                            <td class="td2">
                                <asp:DropDownList ID="DdlModalidad" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1">Sector de Consumo
                            </td>
                            <td class="td2">
                                <asp:DropDownList ID="DdlSector" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="td1"></td>
                            <td class="td2"></td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblMensaje">
            <tr>
                <td class="th1" colspan="4" align="center">
                    <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                        OnClick="imbConsultar_Click" Height="40" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblGrilla" visible="false">
            <tr>
                <td colspan="3" align="center">
                    <div style="overflow: scroll; width: 100%; height: 450px;">
                        <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                            HeaderStyle-CssClass="th1">
                            <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                            <ItemStyle CssClass="td2"></ItemStyle>
                            <Columns>
                                <asp:BoundColumn DataField="Año" HeaderText="año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="mes" HeaderText="mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador"  ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_rol" HeaderText="Rol Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_subasta" HeaderText="Tipo Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="punta" HeaderText="punta" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Código Modalidad" Visible="false"  ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_entrega" HeaderText="Código punto entrega" Visible="false"  ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_entrega" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_salida" HeaderText="Código Punto Salida" Visible="false"  ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_salida" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_sector" HeaderText="Código Sector Consumo" Visible="false"  ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_sector" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>

                                <asp:BoundColumn DataField="cantidad" HeaderText="cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </td>
            </tr>
        </table>
</asp:Content>