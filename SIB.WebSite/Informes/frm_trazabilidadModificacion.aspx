﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_trazabilidadModificacion.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Informes.frm_trazabilidadModificacion" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Negociación Inicial" AssociatedControlID="TxtFechaIni" runat="server" />
                            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" Width="100%" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Negociación Final" AssociatedControlID="TxtFechaFin" runat="server" />
                            <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" Width="100%" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Modificación Inicial" AssociatedControlID="TxtFechaModIni" runat="server" />
                            <asp:TextBox ID="TxtFechaModIni" placeholder="yyyy/mm/dd" Width="100%" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group" runat="server">
                            <asp:Label Text="Fecha Modificación Final" AssociatedControlID="TxtFechaModFin" runat="server" />
                            <asp:TextBox ID="TxtFechaModFin" placeholder="yyyy/mm/dd" Width="100%" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" runat="server">
                        <div class="form-group">
                            <asp:Label Text="Número Operación" AssociatedControlID="TxtOperIni" runat="server" />
                            <asp:TextBox ID="TxtOperIni" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FTEBTxtOperIni" runat="server" TargetControlID="TxtOperIni" FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Número contrato" AssociatedControlID="TxtContratoDef" runat="server" />
                            <asp:TextBox ID="TxtContratoDef" runat="server" ValidationGroup="detalle" MaxLength="30" Width="100%" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tipo Subasta" AssociatedControlID="ddlSubasta" runat="server" />
                            <asp:DropDownList ID="ddlSubasta" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tipo Mercado" AssociatedControlID="ddlMercado" runat="server" />
                            <asp:DropDownList ID="ddlMercado" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlMercado_SelectedIndexChanged" Width="100%" CssClass="form-control">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="P">Primario</asp:ListItem>
                                <asp:ListItem Value="S">Secundario</asp:ListItem>
                                <asp:ListItem Value="O">Otras transacciones mercado mayorista</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Producto" AssociatedControlID="ddlProducto" runat="server" />
                            <asp:DropDownList ID="ddlProducto" runat="server" Width="100%" CssClass="form-control">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="G">Suministro de gas</asp:ListItem>
                                <asp:ListItem Value="T">Capacidad de transporte</asp:ListItem>
                                <asp:ListItem Value="A">Suministro y transporte</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Causa Modificación" AssociatedControlID="ddlCausa" runat="server" />
                            <asp:DropDownList ID="ddlCausa" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div id="tr01" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                        <div class="form-group">
                            <asp:Label Text="Operador contraparte" AssociatedControlID="ddlContraparte" runat="server" />
                            <asp:DropDownList ID="ddlContraparte" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div id="tr02" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                        <div class="form-group">
                            <asp:Label Text="Punta" AssociatedControlID="ddlPunta" runat="server" />
                            <asp:DropDownList ID="ddlPunta" runat="server" Width="100%" CssClass="form-control">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="C">Compra</asp:ListItem>
                                <asp:ListItem Value="V">Venta</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="tr03" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                        <div class="form-group">
                            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
                            <asp:DropDownList ID="ddlOperador" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Número Id" AssociatedControlID="TxtNoId" runat="server" />
                            <asp:TextBox ID="TxtNoId" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtNoId" runat="server" TargetControlID="TxtNoId" FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                </div>
                <div class="table table-responsive">
                    <asp:DataGrid ID="dtgMaestro" Visible="False" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="false" AllowPaging="false"
                        PagerStyle-HorizontalAlign="Center" OnEditCommand="dtgMaestro_EditCommand">
                        <Columns>
                            <%--0--%>
                            <asp:BoundColumn DataField="indicador" Visible="false"></asp:BoundColumn>
                            <%--1--%>
                            <asp:BoundColumn DataField="codigo" Visible="false"></asp:BoundColumn>
                            <%--2--%>
                            <asp:BoundColumn DataField="id_registro" HeaderText="Id Registro"></asp:BoundColumn>
                            <%--3--%>
                            <asp:BoundColumn DataField="fecha_hora" HeaderText="fecha hora modificación" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            <%--4--%>
                            <asp:BoundColumn DataField="codigo_solicitud" HeaderText="Solicitud Modif" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <%--5--%>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Operación" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <%--6--%>
                            <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato definitivo"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--7--%>
                            <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--8--%>
                            <asp:BoundColumn DataField="hora_negociacion" HeaderText="Hora Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--9--%>
                            <asp:BoundColumn DataField="fecha_suscripcion_cont" HeaderText="Fecha Suscripción"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--10--%>
                            <asp:BoundColumn DataField="codigo_punto_entrega" HeaderText="Cod Pto/ Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--11--%>
                            <asp:BoundColumn DataField="desc_punto" HeaderText="Pto Entrega/ Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--12--%>
                            <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Cod Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--13--%>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--14--%>
                            <asp:BoundColumn DataField="codigo_periodo" HeaderText="Cod Periodo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--15--%>
                            <asp:BoundColumn DataField="desc_periodo" HeaderText="Periodo Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--16--%>
                            <asp:BoundColumn DataField="operador_compra" HeaderText="Ope Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--17--%>
                            <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--18--%>
                            <asp:BoundColumn DataField="operador_venta" HeaderText="Ope Venta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--19--%>
                            <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Venta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20--%>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0: ###,###,###,##0}"></asp:BoundColumn>
                            <%--21--%>
                            <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0: ###,###,###,##0.00}"></asp:BoundColumn>
                            <%--22--%>
                            <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <%--23--%>
                            <asp:BoundColumn DataField="hora_inicial" HeaderText="Hora Inicial" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--24--%>
                            <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <%--25--%>
                            <asp:BoundColumn DataField="hora_final" HeaderText="Hora Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--26--%>
                            <asp:BoundColumn DataField="no_años" HeaderText="No Años" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--27--%>
                            <asp:BoundColumn DataField="sentido_flujo" HeaderText="Sentido FLujo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--28--%>
                            <asp:BoundColumn DataField="presion_punto_fin" HeaderText="Presión" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--29--%>
                            <asp:BoundColumn DataField="tipo_garantia" HeaderText="Tipo Garantía" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--30--%>
                            <asp:BoundColumn DataField="valor_garantia" HeaderText="Valor Garantía" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--31--%>
                            <asp:BoundColumn DataField="fecha_pago" HeaderText="Fecha pago Garantía" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--32--%>
                            <asp:BoundColumn DataField="codigo_depto_punto_sal" HeaderText="Código Depto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--33--%>
                            <asp:BoundColumn DataField="nombre_departamento" HeaderText="Nombre Depto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--34--%>
                            <asp:BoundColumn DataField="codigo_municipio_pto_sal" HeaderText="Código Mcpio" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--35--%>
                            <asp:BoundColumn DataField="nombre_ciudad" HeaderText="Nombre Municipio" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--36--%>
                            <asp:BoundColumn DataField="codigo_centro_pob" HeaderText="Código Centro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--37--%>
                            <asp:BoundColumn DataField="nombre_centro" HeaderText="Nombre Centro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--38--%>
                            <asp:BoundColumn DataField="codigo_fuente" HeaderText="Código Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--39--%>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="Nombre Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--40--%>
                            <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--41--%>
                            <asp:BoundColumn DataField="desc_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--42--%>
                            <asp:BoundColumn DataField="desc_subasta" HeaderText="Tipo Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--43--%>
                            <asp:BoundColumn DataField="desc_causa" HeaderText="Causa Modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--44 202010707--%>
                            <asp:BoundColumn DataField="codigo_trm" HeaderText="Código Tasa" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--45 202010707--%>
                            <asp:BoundColumn DataField="desc_trm" HeaderText="Tasa Cambio" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--46 202010707--%>
                            <asp:BoundColumn DataField="observacion_trm" HeaderText="Observación Tasa Cambio" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--47 202010707--%>
                            <asp:BoundColumn DataField="tipo_moneda" HeaderText="Tipo Moneda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--48--%>
                            <asp:EditCommandColumn HeaderText="Usr Final" EditText="Usr Final" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                        </Columns>
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </div>
                <div runat="server" visible="false" id="tblDetalle" width="100%">
                    <div class="table table-responsive">
                        <asp:DataGrid ID="dtgDetalle" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="false" AllowPaging="false"
                            PagerStyle-HorizontalAlign="Center">

                            <Columns>
                                <asp:BoundColumn DataField="codigo_tipo_demanda" HeaderText="Cod Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda Atender" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="no_identificacion_usr" HeaderText="Identificación Usuario Final"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tipo_doc" HeaderText="Tipo Identificación Usuario Final"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_usuario" HeaderText="Nombre Usuario Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_sector_consumo" HeaderText="Cod Sector Consumo"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="80px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_sector" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="80px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Cod Pto Salida" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_contratada" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="equivalente_kpcd" HeaderText="Equivalente Kpcd" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
