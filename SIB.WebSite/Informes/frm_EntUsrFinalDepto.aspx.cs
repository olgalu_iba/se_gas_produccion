﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using Segas.Web.Elements;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Informes_frm_EntUsrFinalDepto : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Entrega a usuario final por departamento";
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);
        //Se agrega el comportamiento del botón
        buttons.ExportarExcelOnclick += imbExcel_Click;
        buttons.FiltrarOnclick += BtnBuscar_Click;
        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles2(lConexion.gObjConexion, DdlOperador, "m_operador", " estado = 'A' order by razon_social", 0, 4);
            LlenarControles1(lConexion.gObjConexion, DdlTramo, "m_pozo", " estado = 'A' ", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlSector, "m_sector_consumo", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlDemanda, "m_tipo_demanda_atender", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlDepto, "m_divipola", " estado = 'A' and codigo_ciudad ='0' order by nombre_departamento ", 1, 2);
            lConexion.Cerrar();

            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar };
            buttons.Inicializar(botones: botones);
        }
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetTramo", null, null, null);
        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);
        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["desc_tramo"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    /// 20180122 rq003-18
    protected void LlenarControles2(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceLlave).ToString() + "-" + lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }

    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        bool Error = false;
        DateTime ldFecha;
        int liValor = 0;
        string[] lsNombreParametros = { "@P_codigo_ent_usr_ini", "@P_codigo_ent_usr_fin", "@P_fecha_registro_ini", "@P_fecha_registro_fin", "@P_codigo_operador", "@P_codigo_tramo", "@P_sector_consumo", "@P_tipo_demanda", "@P_codigo_depto" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "0", "", "", "0", "0", "0", "0", "" };

        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Formato Inválido en el Campo Fecha inicial de registro. <br>");
                Error = true;
        
            }
        }
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaFin.Text);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Formato Inválido en el Campo Fecha Final de registro. <br>");
                Error = true;              
            }
        }

        if (TxtFechaIni.Text.Trim().Length == 0 && TxtFechaFin.Text.Trim().Length > 0)
        {
            Toastr.Warning(this, "Debe digitar la Fecha Inicial de registro antes que la final. <br>");
            Error = true;
        }

        if (TxtFechaIni.Text.Trim().Length > 0 && TxtFechaFin.Text.Trim().Length > 0)
            try
            {
                if (Convert.ToDateTime(TxtFechaFin.Text) < Convert.ToDateTime(TxtFechaIni.Text))
                {
                    Toastr.Warning(this, "La Fecha inicial de registro debe ser menor o igual que la fecha final. <br>");
                    Error = true;
                    
                }
            }
            catch (Exception ex)
            { }

        if (TxtRegistroIni.Text.Trim().Length > 0)
        {
            try
            {
                liValor = Convert.ToInt32(TxtRegistroIni.Text);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Formato Inválido en el Campo No registro inicial. <br>");
                Error = true;
                
            }

        }
        if (TxtRegistroFin.Text.Trim().Length > 0)
        {
            try
            {
                liValor = Convert.ToInt32(TxtRegistroFin.Text);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Formato Inválido en el Campo No registro final. <br>");
                Error = true;
            }

        }
        if (TxtRegistroIni.Text.Trim().Length == 0 && TxtRegistroFin.Text.Trim().Length > 0)
        {
            Toastr.Warning(this, "Debe digitar el No de registro inicial antes que el final. <br>");
            Error = true;
          
        }
        if (TxtRegistroIni.Text.Trim().Length > 0 && TxtRegistroFin.Text.Trim().Length > 0)
        {
            try
            {
                if (Convert.ToInt32(TxtRegistroFin.Text) < Convert.ToInt32(TxtRegistroIni.Text))
                {
                    Toastr.Warning(this, "El No registro inicial debe ser menor o igual que el final. <br>");
                    Error = true;
                }
            }
            catch (Exception ex)
            {

            }
        }
        if (TxtRegistroIni.Text == "" && TxtRegistroFin.Text == "" && TxtFechaIni.Text == "" && TxtFechaFin.Text == "")
        {
            Toastr.Warning(this, "Debe seleccionar al menos un parámetro de No. de registro o de fecha. <br>");
            Error = true;
        }

        if (!Error)
        {
            try
            {
                if (TxtRegistroIni.Text != "")
                {
                    lValorParametros[0] = TxtRegistroIni.Text;
                    if (TxtRegistroFin.Text != "")
                        lValorParametros[1] = TxtRegistroFin.Text;
                    else
                        lValorParametros[1] = TxtRegistroIni.Text;
                }
                if (TxtFechaIni.Text != "")
                {
                    lValorParametros[2] = TxtFechaIni.Text;
                    if (TxtFechaFin.Text != "")
                        lValorParametros[3] = TxtFechaFin.Text;
                    else
                        lValorParametros[3] = TxtFechaIni.Text;
                }
                
                lValorParametros[4] = DdlOperador.SelectedValue;
                lValorParametros[5] = DdlTramo.SelectedValue;
                lValorParametros[6] = DdlSector.SelectedValue;
                lValorParametros[7] = DdlDemanda.SelectedValue;
                lValorParametros[8] = DdlDepto.SelectedValue;
                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetEntregaUsuarioFinDepto", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                if (dtgConsulta.Items.Count > 0)
                    tblGrilla.Visible = true;
                else
                {
                    tblGrilla.Visible = false;
                    Toastr.Warning(this, "No se encontraron Registros.");
                    
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pudo Generar el Informe.! " + ex.Message.ToString());
   
            }
        }
    }
    /// <summary>
    /// Exportacion a Excel de la Información  de la Grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbExcel_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        try
        {
            string lsNombreArchivo = Session["login"] + "InfExcelReg" + DateTime.Now + ".xls";
            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            lpagina.EnableEventValidation = false;
            lpagina.Controls.Add(lform);
            dtgConsulta.EnableViewState = false;
            dtgConsulta.Visible = true;
            lform.Controls.Add(dtgConsulta);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;

            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + "Consulta entrega a usuarios finales por departamento" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write(lsb.ToString());
            Response.End();
            Response.Flush();
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "Problemas al Consultar los Contratos. " + ex.Message.ToString());

        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnBuscar_Click(object sender, EventArgs e)
    {
        CargarDatos();
    }
}
