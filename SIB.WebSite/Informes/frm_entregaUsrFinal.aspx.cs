﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;

public partial class Informes_frm_entregaUsrFinal : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Consulta consolidada entrega usuarios finales";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles1(lConexion.gObjConexion, ddlBusTramo, "m_tramo", " estado = 'A' order by descripcion", 0, 10);
            LlenarControles(lConexion.gObjConexion, ddlBusTipoDemanda, "m_tipo_demanda_atender", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlBusSector, "m_sector_consumo", " estado = 'A' order by descripcion", 0, 1); 
            LlenarControles2(lConexion.gObjConexion, ddlBusOperador, "m_operador", " estado = 'A' order by razon_social", 0, 4); 
            lConexion.Cerrar();
            if (goInfo.cod_comisionista == "0")
            { 
                hdnELiminado.Value = "S";
                hdnRuta.Value = "S";
            }
            else
            { 
                hdnELiminado.Value = "N";
                hdnRuta.Value = "N";
                ddlBusOperador.SelectedValue = goInfo.cod_comisionista;
                ddlBusOperador.Enabled = false;
                dtgReporte1.Columns[14].Visible = false;
                dtgReporte1.Columns[15].Visible = false;
            }
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    /// // 20180126 rq107-16
    protected void LlenarControles2(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector["codigo_operador"].ToString();
            lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    /// 20180307 fin rq010-11
    protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetTramo", null, null, null);
        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);
        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["desc_tramo"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        string[] lsNombreParametros = { "@P_codigo_ent_usuario_final", "@P_fecha_registro", "@P_codigo_operador", "@P_fecha_registro_fin", "@P_tramo", "@P_tipo_demanda", "@P_sector_consumo", "@P_mostrar_eliminado" , "@P_ind_ruta" }; 
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar }; 
        string[] lValorParametros = { "0", "", "0", "", ddlBusTramo.SelectedValue, ddlBusTipoDemanda.SelectedValue, ddlBusSector.SelectedValue, hdnELiminado.Value, hdnRuta.Value }; 
        DateTime ldFecha;
        lblMensaje.Text = "";

        try
        {
            if (TxtBusFecha.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtBusFecha.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Fecha Inicial. <br>";
                }
            }
            if (TxtBusFechaF.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtBusFechaF.Text.Trim());
                    if (ldFecha < Convert.ToDateTime(TxtBusFecha.Text.Trim()))
                        lblMensaje.Text += "La Fecha final NO puede ser menor que la Fecha Inicial.!<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Fecha Final. <br>";
                }
            }
            if(TxtBusCodigo.Text =="" && TxtBusFecha.Text =="")
                lblMensaje.Text += "Debe seleccionar el código o el rango de fechas para realizar la búsqueda. <br>";
            if (lblMensaje.Text == "")
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                if (TxtBusFecha.Text.Trim().Length > 0)
                    lValorParametros[1] = TxtBusFecha.Text.Trim();
                if (ddlBusOperador.SelectedValue != "0")
                    lValorParametros[2] = ddlBusOperador.SelectedValue;
                if (TxtBusFechaF.Text.Trim().Length > 0)
                    lValorParametros[3] = TxtBusFechaF.Text.Trim();
                else
                {
                    if (TxtBusFecha.Text.Trim().Length > 0)
                        lValorParametros[3] = TxtBusFecha.Text.Trim();
                }

                lConexion.Abrir();
                dtgReporte1.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetEntregaUsuarioFinal", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgReporte1.DataBind();
                lConexion.Cerrar();
                dtgReporte1.Visible = true;
                imbExcel.Visible = true;
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfTransporte" + DateTime.Now + ".xls";
        string lstitulo_informe = "Consulta Consolidada Entrega a Usuarios Finales";
        string lsTituloParametros = "";
        try
        {
            if (TxtBusCodigo.Text.Trim().Length > 0)
            {
                lsTituloParametros += " Código Entrega Usuario Final : " + TxtBusCodigo.Text;
            }
            if (TxtBusFecha.Text.Trim().Length > 0)
            {
                lsTituloParametros += " Fecha Inicial : " + TxtBusFecha.Text;
            }
            if (ddlBusOperador.SelectedValue != "0")
            {
                lsTituloParametros += " Operador: " + ddlBusOperador.SelectedItem;
            }
            if (TxtBusFechaF.Text.Trim().Length > 0)
            {
                lsTituloParametros += " Fecha Final : " + TxtBusFechaF.Text;
            }
            else
            {
                if (TxtBusFecha.Text.Trim().Length > 0)
                {
                    lsTituloParametros += " Fecha Final : " + TxtBusFecha.Text;
                }
            }
            if (ddlBusTramo.SelectedValue != "0")
            {
                lsTituloParametros += " Tramo: " + ddlBusTramo.SelectedItem;
            }
            if (ddlBusSector.SelectedValue != "0") //20160303
            {
                lsTituloParametros += " Sector De Consumo: " + ddlBusSector.SelectedItem; //20160303
            }

            if (ddlBusTipoDemanda.SelectedValue != "0")
            {
                lsTituloParametros += " Tipo Demanda: " + ddlBusTipoDemanda.SelectedItem;
            }
            dtgReporte1.Columns[4].Visible = true;
            dtgReporte1.Columns[8].Visible = true;
            dtgReporte1.Columns[10].Visible = true;
            dtgReporte1.Columns[13].Visible = true;
            dtgReporte1.Columns[14].Visible = true;
            dtgReporte1.Columns[15].Visible = true;
            dtgReporte1.Columns[21].Visible = true;
            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            dtgReporte1.EnableViewState = false;
            lform.Controls.Add(dtgReporte1);
            lpagina.RenderControl(lhtw);
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
            dtgReporte1.Columns[4].Visible = false;
            dtgReporte1.Columns[8].Visible = false;
            dtgReporte1.Columns[10].Visible = false;
            dtgReporte1.Columns[13].Visible = false;
            dtgReporte1.Columns[14].Visible = false;
            dtgReporte1.Columns[15].Visible = false;
            dtgReporte1.Columns[21].Visible = false;

        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Excel.!" + ex.Message.ToString();
        }
    }

}