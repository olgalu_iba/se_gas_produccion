﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ReporteInformacionPTDVF.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Informes.frm_ReporteInformacionPTDVF" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="Label1" Text="Reporte Información PTDVF y CIDVF" runat="server" /> <%--20200727--%>
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Año Inicial" AssociatedControlID="TxtAnoIni" runat="server" />
                            <asp:TextBox ID="TxtAnoIni" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Mes Inicial" AssociatedControlID="ddlMesIni" runat="server" />
                            <asp:DropDownList ID="ddlMesIni" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Año Final" AssociatedControlID="TxtAnoFin" runat="server" />
                            <asp:TextBox ID="TxtAnoFin" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Mes Final" AssociatedControlID="ddlMesFinal" runat="server" />
                            <asp:DropDownList ID="ddlMesFinal" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
                            <asp:DropDownList ID="ddlOperador" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Punto del SNT" AssociatedControlID="ddlPuntoSnt" runat="server" />
                            <asp:DropDownList ID="ddlPuntoSnt" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Año Carga" AssociatedControlID="TxtAnoCarga" runat="server" />
                            <asp:TextBox ID="TxtAnoCarga" runat="server" Width="100%" CssClass="form-control" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div runat="server" id="tblGrilla" visible="false">
                <div class="table table-responsive">
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                        <Columns>
                            <asp:BoundColumn DataField="operador_del_campo" HeaderText="Operador Campo" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="campo_punto_snt" HeaderText="Campo o Punto SNT" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="socio_productor" HeaderText="Socio Productor incluye Estado" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ptdvf" HeaderText="PTDVF (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cidvf" HeaderText="CIDVF (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                        </Columns>
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
