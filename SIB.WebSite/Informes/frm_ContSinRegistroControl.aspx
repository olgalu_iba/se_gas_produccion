﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ContSinRegistroControl.aspx.cs"
    Inherits="Informes_frm_ContSinRegistroControl" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                       
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Número de Operación</label>
                              <asp:TextBox ID="TxtOperacion" runat="server" ValidationGroup="detalle" CssClass="form-control" Width="100%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtOperacion" runat="server" TargetControlID="TxtOperacion"
                                FilterType="Custom, Numbers">
                            </ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Negociación</label>
                            <asp:TextBox ID="TxtFecha" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Número Id Registro</label>
                          <asp:TextBox ID="TxtNoId" runat="server" ValidationGroup="detalle" CssClass="form-control" Width="100%"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtNoId" runat="server" TargetControlID="TxtNoId"
                                FilterType="Custom, Numbers">
                            </ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div id="tdMes01" class="form-group" runat="server">
                            <label for="Name"> Tipo Subasta</label>
                             <asp:DropDownList ID="ddlSubasta" runat="server" CssClass="form-control selectpicker" data-live-search="true" Width="100%">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="tdMes02" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                        <div class="form-group">
                            <label for="Name">  Tipo Mercado</label>
                            <asp:DropDownList ID="ddlMercado" runat="server" CssClass="form-control" Width="100%">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="P">Primario</asp:ListItem>
                                <asp:ListItem Value="S">Secundario</asp:ListItem>
                                <asp:ListItem Value="M">Otras Transacciones del Mercado Mayorista</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" style="display:none">
                        <div class="form-group">
                            <label for="Name">Comprador</label>
                         <asp:DropDownList ID="DdlComprador" runat="server" CssClass="form-control selectpicker" data-live-search="true" Width="100%"> <%--20180126 rq107-16--%>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Producto</label>
                          <asp:DropDownList ID="ddlProducto" runat="server" CssClass="form-control" Width="100%">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="G">Suministro de Gas</asp:ListItem>
                                <asp:ListItem Value="T">Capacidad Transporte</asp:ListItem>
                                <%--20170926 rq027-17--%>
                                <asp:ListItem Value="A">Suministro y Transporte</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Operador Compra</label>
                                <asp:DropDownList ID="ddlOpeC" runat="server" CssClass="form-control selectpicker" data-live-search="true" Width="100%">
                            </asp:DropDownList>
                        </div>
                    </div>
                     <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name"> Operador Venta</label>
                            <asp:DropDownList ID="ddlOpeV" runat="server" CssClass="form-control selectpicker" data-live-search="true" Width="100%">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                 <div border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                     id="tblMensaje">
      
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png" Visible="false"
                    OnClick="imbConsultar_Click" Height="40" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
        
          
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
           
    </div>
    <div border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla" visible="false">
       
                    <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered"
                        PagerStyle-HorizontalAlign="Center" >
                       
                        <Columns>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                             <%--20171130 rq026-17--%>
                            <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Neg" ItemStyle-HorizontalAlign="Left"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_subasta" HeaderText="Subasta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_comprador" HeaderText="Operador compra" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_vendedor" HeaderText="Operador venta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_punto" HeaderText="pto entrega/ Ruta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_inicial" HeaderText="fecha ini" DataFormatString="{0:yyyy/MM/dd}">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_final" HeaderText="fecha fin" DataFormatString="{0:yyyy/MM/dd}">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="cantidad" DataFormatString="{0:###,###,###,###,###,###,###,##0}">
                            </asp:BoundColumn>
                            <%--20170926 rq027-17--%>
                            <asp:BoundColumn DataField="capacidad_transporte" HeaderText="capacidad transporte"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="precio" DataFormatString="{0:###,###,###,###,###,###,###,##0.00}">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_max_registro" HeaderText="fec max reg"></asp:BoundColumn>
                        </Columns>
                        
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </div>
          
            </div>
         </div>
     </div>
 </div>
  
   
  </asp:Content>