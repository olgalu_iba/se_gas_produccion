﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_EnergiaTomadaDepto.aspx.cs"
    Inherits="Informes_frm_EnergiaTomadaDepto" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
        width="100%">
        <tr>
            <td align="center" class="th1" style="width: 80%;">
                <asp:Label ID="lblTitulo" runat="server" ForeColor="White"></asp:Label>
            </td>
            <td align="center" class="th1" style="width: 20%;">
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/exel 2007 3D.png"
                    Height="40" OnClick="imbExcel_Click" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <asp:UpdateProgress ID="panelUpdateProgress" DisplayAfter="0" runat="server" AssociatedUpdatePanelID="UptPnlTodo">
        <ProgressTemplate>
            <asp:Panel ID="PnlCarga" runat="server" Style="position: relative; top: 30%; text-align: center;"
                CssClass="popup">
                <div class="fuente">
                    <img src="../Images/ajax-loader.gif" style="vertical-align: middle" alt="Procesando" />
                    Procesando por Favor espere ...
                </div>
            </asp:Panel>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UptPnlTodo" runat="server">
        <ContentTemplate>
            <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
                width="90%">
                <tr>
                    <td class="td1">Código Registro Inicial
                    </td>
                    <td class="td2">
                        <asp:TextBox ID="TxtRegistroIni" runat="server" ValidationGroup="detalle" Width="120px"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="fteTxtRegistroIni" runat="server" TargetControlID="TxtRegistroIni"
                            FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>

                    </td>
                    <td class="td1">Código Registro Final
                    </td>
                    <td class="td2">
                        <asp:TextBox ID="TxtRegistroFin" runat="server" ValidationGroup="detalle" Width="120px"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="fteTxtRegistroFin" runat="server" TargetControlID="TxtRegistroFin"
                            FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>

                    </td>
                </tr>
                <tr>
                    <td class="td1">Fecha Registro Inicial
                    </td>
                    <td class="td2">
                        <asp:TextBox ID="TxtFechaIni" runat="server" Width="100%" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static"></asp:TextBox>
                    </td>
                    <td class="td1">Fecha Registro Final
                    </td>
                    <td class="td2">
                        <asp:TextBox ID="TxtFechaFin" runat="server" Width="100%" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static"></asp:TextBox>

                    </td>
                </tr>

                <tr>
                    <td class="td1">Operador
                    </td>
                    <td class="td2">
                        <asp:DropDownList ID="DdlOperador" runat="server" Width="200px">
                        </asp:DropDownList>
                    </td>
                    <td class="td1">Remitente
                    </td>
                    <td class="td2">
                        <asp:DropDownList ID="DdlRemitente" runat="server" Width="250px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="td1">Tramo
                    </td>
                    <td class="td2">
                        <asp:DropDownList ID="DdlTramo" runat="server" Width="200px">
                        </asp:DropDownList>
                    </td>
                    <td class="td1">Punto de salida
                    </td>
                    <td class="td2">
                        <asp:DropDownList ID="DdlPunto" runat="server" Width="250px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="td1">Departamento
                    </td>
                    <td class="td2" colspan="3">
                        <asp:DropDownList ID="DdlDepto" runat="server" Width="200px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="th1" colspan="4" align="center">
                        <asp:Button ID="BtnBuscar" runat="server" Text="Consultar" OnClick="BtnBuscar_Click" />
                    </td>
                </tr>
            </table>
            </div>
            <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                id="tblMensaje">
                <tr>
                    <td colspan="3" align="center">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
            <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                id="tblGrilla" visible="false">
                <tr>
                    <td colspan="3" align="center">
                        <div style="overflow: scroll; width: 1220px; height: 450px;">
                            <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                                <Columns>
                                    <asp:BoundColumn DataField="codigo_energia_tomada" HeaderText="Código Energía tomada"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha" HeaderText="Fecha Registro"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_remitente" HeaderText="Código Remitente"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_remitente" HeaderText="Nombre Remitente"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_tramo" HeaderText="Desc. Tramo"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Código Punto Salida"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_punto_sal" HeaderText="Des. Pto. Salida"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="energia_tomada" HeaderText="Energía Tomada" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Contrato"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="estado" HeaderText="Estado"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="login_usuario" HeaderText="Login usuario"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="Fecha-hora actual"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="carga_normal" HeaderText="Estado Carga"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="ind_registrado" HeaderText="Ind. Registrado"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_depto" HeaderText="Cod. Depto"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_departamento" HeaderText="Departamento"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_ciudad" HeaderText="Cod. Mpio."></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_ciudad" HeaderText="Municipio"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
