﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ProgDefTra.aspx.cs" Inherits="Informes_frm_ProgDefTra" MasterPageFile="~/PlantillaPrincipal.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>

            <%--Contenido--%>
            <%--Campos de Filtros--%>
            <div class="kt-portlet__body" runat="server" id="Div1">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Tipo Reporte</label>
                            <asp:DropDownList ID="DdlReporte" runat="server" Width="100%" CssClass="form-control">
                                <asp:ListItem Value="A">Programación Definitiva Transporte D-1</asp:ListItem>
                                <asp:ListItem Value="D">Programación Definitiva Transporte Día de Gas</asp:ListItem>
                                <%--20190318 rq017-19--%>
                                <asp:ListItem Value="P">Programación Definitiva Transporte posterior a renominación D+1</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fecha Inicial</label>
                            <asp:TextBox ID="TxtFechaIni" runat="server" CssClass="form-control datepicker" Width="100%" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Fecha Final</label>
                            <asp:TextBox ID="TxtFechaFin" runat="server" CssClass="form-control datepicker" Width="100%" ClientIDMode="Static" MaxLength="10"></asp:TextBox>

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Comprador</label>
                            <asp:DropDownList ID="DdlComprador" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Vendedor</label>
                            <asp:DropDownList ID="DdlVendedor" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </div>
                    </div>

                </div>
                <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                    id="tblMensaje">

                    <tr>
                        <td colspan="3" align="center">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                </table>
                <div runat="server" visible="false"
                    id="tblGrilla">

                    <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                        <%--20190318 rq017-19--%>
                        <asp:DataGrid ID="dtgMaestro" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" AllowPaging="false"
                            PagerStyle-HorizontalAlign="Center">

                            <Columns>
                                <%--20190318 rq017-19--%>
                                <asp:BoundColumn DataField="codigo_prog_tra" HeaderText="Código Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Fecha_reporte" HeaderText="Fecha reporte" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:yyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Fecha_gas" HeaderText="Fecha de gas" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:yyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numero_operacion" HeaderText="Número operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_vendedor" HeaderText="Cod. vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190211 rq005-19--%>
                                <asp:BoundColumn DataField="nombre_vendedor" HeaderText="Nombre vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_comprador" HeaderText="Cod. comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190211 rq005-19--%>
                                <asp:BoundColumn DataField="nombre_comprador" HeaderText="Nombre comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_punto_entrada" HeaderText="Cod. pto. Entrada" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <%--20190211 rq005-19--%>
                                <asp:BoundColumn DataField="desc_punto_entrada" HeaderText="Punto Entrada" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Cod. pto. Salida" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <%--20190211 rq005-19--%>
                                <asp:BoundColumn DataField="desc_punto_salida" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_tipo_demanda" HeaderText="Cod tpo demanda" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_sector_consumo" HeaderText="Cod Sector consumo" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_sector" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_destino" HeaderText="Cod Destino" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_destino" HeaderText="Destino" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="indice_desvio_firme" HeaderText="Ind. desvio/firme" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="equivalente_volumen" HeaderText="Equivalente (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ind_contrato_reg" HeaderText="Ind. Cont. Reg." ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_vigente" HeaderText="Ind. Cont. Vig." ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190318 rq017-19--%>
                                <asp:BoundColumn DataField="fecha_recibo" HeaderText="Fecha Recibo Nominación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="hora_recibo" HeaderText="Hora Recibo Nominación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190607 rq036-19--%>
                                <asp:BoundColumn DataField="estado_carga" HeaderText="Estado Carga" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190607 rq036-19--%>
                                <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190607 rq036-19--%>
                                <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="Fecha-Hora Carga" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyy/MM/dd HH:mm}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>

                </div>

            </div>



        </div>
    </div>



</asp:Content>
