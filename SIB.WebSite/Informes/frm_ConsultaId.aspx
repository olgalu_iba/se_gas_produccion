﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ConsultaId.aspx.cs"
    Inherits="Informes_frm_ConsultaId" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
                    width="80%">
                    <tr>
                        <td align="center" class="th1">
                            <asp:Label ID="lblTitulo" runat="server" ForeColor="White" ></asp:Label>
                        </td>
                    </tr>
                </table>
                <br /><br /><br /><br /><br /><br />
                <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
                    width="80%">
                    <tr>
                        <td class="td1">
                            Subasta
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlSubasta" runat="server" OnSelectedIndexChanged="ddlSubasta_SelectedIndexChanged"
                                AutoPostBack="true" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            Tipo Rueda
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlTipoRueda" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            Fecha Rueda
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            Numero Rueda
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtNoRueda" runat="server" ValidationGroup="detalle"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            Estado
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlEstado" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla" visible="false">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 1050px; height: 350px;">
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="numero_id" HeaderText="No. Id" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_rueda" HeaderText="No. Rueda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_rueda" HeaderText="Fecha Rueda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="tipo_rueda" HeaderText="Tipo  Rueda" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="200px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="unidad_medida" HeaderText="Unidad de Medida"></asp:BoundColumn>
                            <asp:BoundColumn DataField="punto_entrega" HeaderText="Punto de Entrega"></asp:BoundColumn>
                            <asp:BoundColumn DataField="modalidad" HeaderText="Modalidad de Entrega"></asp:BoundColumn>
                            <asp:BoundColumn DataField="periodo_entrega" HeaderText="Periodo de Entrega"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_total_venta" HeaderText="Cnt. Total Venta" ItemStyle-HorizontalAlign="Right">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_adjudicada" HeaderText="Cantidad Adjudicada"
                                Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio_adjudicado" HeaderText="Preci Adjudicado" Visible="false">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Postura" Visible="false">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="Fecha Hora Postura" Visible="false">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>