﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Informes
{
    public partial class frm_ReporteAsigProyPag : Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Reporte Asignación Proyectos Pag";
        clConexion lConexion = null;
        DataSet lds = new DataSet();
        SqlDataAdapter lsqldata = new SqlDataAdapter();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo;
            lConexion = new clConexion(goInfo);
            //Se agrega el comportamiento del botón
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.FiltrarOnclick += btnConsultar_Click;

            if (IsPostBack) return;

            Master.Titulo = "Informes";

            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlTrimestre, "m_trimestre", " 1=1 order by numero_trimestre", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado='A' And tipo_operador = 'T'  order by razon_social", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlRemitente, "m_operador", " estado='A' And tipo_operador != 'T'  order by razon_social", 0, 1);
            lConexion.Cerrar();
            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Excel };
            buttons.Inicializar(botones: botones);
        }
        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            var lItem = new ListItem { Value = "0", Text = "Seleccione" };
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                var lItem1 = new ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"] + "-" + lLector["razon_social"];
                }
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }
        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            dtgMaestro.CurrentPageIndex = 0; //20180126 rq107 - 16
            cargarDatos();
        }
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// //20180126 rq107 - 16
        protected void cargarDatos()
        {
            var lblMensaje = new StringBuilder();

            if (TxtAño.Text == "")
                lblMensaje.Append("Debe seleccionar el Año de Negociación. <br>");
            if (ddlTrimestre.SelectedValue == "0")
            {
                lblMensaje.Append("Debe seleccionar el trimestre de Negociación. <br>");
            }
            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                Toastr.Warning(this, lblMensaje.ToString());
                return;
            }
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetAsignaProyPag";
                lComando.Parameters.Add("@P_año_nego", SqlDbType.Int).Value = TxtAño.Text;
                lComando.Parameters.Add("@P_trimestre_nego", SqlDbType.Int).Value = ddlTrimestre.SelectedValue;
                lComando.Parameters.Add("@P_codigo_transportador", SqlDbType.Int).Value = ddlOperador.SelectedValue;
                lComando.Parameters.Add("@P_codigo_remitente", SqlDbType.Int).Value = ddlRemitente.SelectedValue;

                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgMaestro.DataSource = lds;
                dtgMaestro.DataBind();
                tblGrilla.Visible = true;
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se pudo generar el informe.! " + ex.Message);
            }
        }
        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string lsNombreArchivo = goInfo.Usuario + "InfAsigProyPag" + DateTime.Now + ".xls";
            string lstitulo_informe = "Reporte Asignación Proyectos Pag";
            string lsTituloParametros = "";
            var Error = false;
            if (TxtAño.Text == "")
            {
                Toastr.Error(this, "Debe seleccionar el Año de negociación.");
                Error = true;
            }
            if (ddlTrimestre.SelectedValue == "0")
            {
                Toastr.Error(this, "Debe seleccionar el trimestre de negociación.");
                Error = true;
            }

            if (!Error)
            {
                try
                {
                    lsTituloParametros += " Año: " + TxtAño.Text + " - Trimestre: " + ddlTrimestre.SelectedValue;
                    if (ddlOperador.SelectedValue != "0")
                        lsTituloParametros += "  - Transportador: " + ddlOperador.SelectedItem;
                    if (ddlRemitente.SelectedValue != "0")
                        lsTituloParametros += "  - Remitente: " + ddlOperador.SelectedItem;
                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    SqlCommand lComando = new SqlCommand();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandTimeout = 3600;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_GetAsignaProyPag";
                    lComando.Parameters.Add("@P_año_nego", SqlDbType.Int).Value = TxtAño.Text;
                    lComando.Parameters.Add("@P_trimestre_nego", SqlDbType.Int).Value = ddlTrimestre.SelectedValue;
                    lComando.Parameters.Add("@P_codigo_transportador", SqlDbType.Int).Value = ddlOperador.SelectedValue;
                    lComando.Parameters.Add("@P_codigo_remitente", SqlDbType.Int).Value = ddlRemitente.SelectedValue;

                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgExcel.DataSource = lds;
                    dtgExcel.DataBind();
                    lConexion.Cerrar();
                    decimal ldCapacidad = 0;
                    StringBuilder lsb = new StringBuilder();
                    ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                    StringWriter lsw = new StringWriter(lsb);
                    HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                    Page lpagina = new Page();
                    HtmlForm lform = new HtmlForm();
                    dtgExcel.EnableViewState = false;
                    lpagina.EnableEventValidation = false;
                    lpagina.DesignerInitialize();
                    lpagina.Controls.Add(lform);

                    lform.Controls.Add(dtgExcel);
                    lpagina.RenderControl(lhtw);
                    Response.Clear();

                    Response.Buffer = true;
                    Response.ContentType = "aplication/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                    Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                    Response.ContentEncoding = Encoding.Default;
                    Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                    Response.Charset = "UTF-8";
                    Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre + "</td></tr></table>");
                    Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                    Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parámetros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                    Response.ContentEncoding = Encoding.Default;
                    Response.Write(lsb.ToString());

                    Response.End();
                    lds.Dispose();
                    lsqldata.Dispose();
                    lConexion.CerrarInforme();
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, "No se Pudo Generar el Excel.!" + ex.Message);

                }
            }
        }
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        /// 20180126 rq107-16
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            cargarDatos();
        }
    }
}