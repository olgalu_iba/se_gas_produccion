﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ReservaPtdvf.aspx.cs"
    Inherits="Informes_frm_ReservaPtdvf" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
                    width="80%">
                    <tr>
                        <td align="center" class="th1">
                            <asp:Label ID="lblTitulo" runat="server" ForeColor ="White"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br /><br /><br /><br /><br /><br />
                <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
                    width="80%">
                    <tr>
                        <td class="td1">
                            Operador
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlOperador" runat="server" Width="350px">
                            </asp:DropDownList>
                        </td>
                        <td class="td1">
                            Fuente
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlFuente" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:Button ID="btnConsulta" runat="server" Text="Consulta" OnClick="btnConsulta_Click" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla" visible="false">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 1050px; height: 350px;">
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="Id" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_venta" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Vendedor" ItemStyle-HorizontalAlign="left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_compra" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Comprador" ItemStyle-HorizontalAlign="left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="Nombre Fuente" ItemStyle-HorizontalAlign="left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="center"
                                DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Precio" HeaderText="Precio" ItemStyle-HorizontalAlign="center"
                                DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_estado" HeaderText="Estado"
                                ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_max_registro" HeaderText="Fecha Máxima Registro"
                                ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>