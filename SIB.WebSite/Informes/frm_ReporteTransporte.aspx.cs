﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;

public partial class Informes_frm_ReporteTransporte : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Reportes de transporte";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, DdlRuta, "m_ruta_snt", " estado <> 'I'  order by descripcion", 0, 4);
            lConexion.Cerrar();
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }

    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    /// 20180307 fin rq010-11
    protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector["codigo_operador"].ToString();
            lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF = DateTime.Now;
        if (ddlReporte.SelectedValue == "")
            lblMensaje.Text += "Debe seleccionar el tipo de reporte. <br>";
        if (ddlReporte.SelectedValue == "2")
        {
            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial. <br>";
                }
            }
            else
                lblMensaje.Text += "Debe digitar la Fecha Inicial. <br>";
            if (TxtFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
                    if (TxtFechaIni.Text.Trim().Length > 0 && ldFechaF < ldFechaI)
                        lblMensaje.Text += "La Fecha Final NO puede ser Menor que la Fecha Inicial. <br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Formato Inválido en el Campo Fecha Final. <br>";
                }
            }
            else
                lblMensaje.Text += "Debe digitar la Fecha Final. <br>";
        }

        if (ddlReporte.SelectedValue == "1")
        {
            string[] lsPeriodo;
            if (TxtPerIni.Text.Trim().Length > 0)
            {
                lsPeriodo = TxtPerIni.Text.Split('/');
                try
                {
                    if (lsPeriodo.Length != 2)
                        lblMensaje.Text += "Formato Inválido en el Periodo Inicial. <br>";
                    else
                    {
                        Convert.ToInt32(lsPeriodo[0]);
                        if (Convert.ToInt32(lsPeriodo[1]) < 1 || Convert.ToInt32(lsPeriodo[1]) > 12)
                            lblMensaje.Text += "Formato Inválido en el Periodo Inicial. <br>";
                        if (lsPeriodo[0].Length != 4)
                            lblMensaje.Text += "Formato Inválido en el Periodo Inicial. <br>";
                        if (lsPeriodo[1].Length != 2)
                            lblMensaje.Text += "Formato Inválido en el Periodo Inicial. <br>";
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Formato Inválido en el Periodo Inicial. <br>";
                }
            }
            else
                lblMensaje.Text += "Debe ingresar el Periodo Inicial. <br>";

            if (TxtPerFin.Text.Trim().Length > 0)
            {
                lsPeriodo = TxtPerFin.Text.Split('/');
                try
                {
                    if (lsPeriodo.Length != 2)
                        lblMensaje.Text += "Formato Inválido en el Periodo Final. <br>";
                    else
                    {
                        Convert.ToInt32(lsPeriodo[0]);
                        if (Convert.ToInt32(lsPeriodo[1]) < 1 || Convert.ToInt32(lsPeriodo[1]) > 12)
                            lblMensaje.Text += "Formato Inválido en el Periodo Final. <br>";
                        if (lsPeriodo[0].Length != 4)
                            lblMensaje.Text += "Formato Inválido en el Periodo Final. <br>";
                        if (lsPeriodo[1].Length != 2)
                            lblMensaje.Text += "Formato Inválido en el Periodo Final. <br>";
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Formato Inválido en el Periodo Final. <br>";
                }
            }
            else
                lblMensaje.Text += "Debe ingresar el Periodo Final. <br>";
            if (lblMensaje.Text == "")
                if (Convert.ToDateTime(TxtPerIni.Text) > Convert.ToDateTime(TxtPerFin.Text))
                    lblMensaje.Text += "El periodo inicial debe ser mayor que el final. <br>";
        }

        if (ddlReporte.SelectedValue == "3")
        {
            if (TxtAñoIni.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe ingresar el Año Inicial. <br>";
            if (TxtAñoIni.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe ingresar el Año Final. <br>";
            if (lblMensaje.Text == "")
                if (Convert.ToInt32(TxtAñoIni.Text ) > Convert.ToInt32(TxtAñoFin.Text ))
                    lblMensaje.Text += "El año inicial debe ser mayor que el final. <br>";
        }

        if (lblMensaje.Text == "")
        {
            try
            {
                dtgReporte1.Visible = false;
                dtgReporte2.Visible = false;
                dtgReporte3.Visible = false;
                //dtgReporte4.Visible = false;

                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.Parameters.Clear();
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetRepAnulTrans";
                lComando.Parameters.Add("@P_tipo_reporte", SqlDbType.Int).Value = ddlReporte.SelectedValue;
                switch (ddlReporte.SelectedValue)

                {
                    case "1":
                        lComando.Parameters.Add("@P_periodo_ini", SqlDbType.VarChar).Value = TxtPerIni.Text.Trim();
                        lComando.Parameters.Add("@P_periodo_fin", SqlDbType.VarChar).Value = TxtPerFin.Text.Trim();
                        break;
                    case "2":
                        lComando.Parameters.Add("@P_fecha_ini", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                        lComando.Parameters.Add("@P_fecha_fin", SqlDbType.VarChar).Value = TxtFechaFin.Text.Trim();
                        break;
                    case "3":
                        lComando.Parameters.Add("@P_año_ini", SqlDbType.Int).Value = TxtAñoIni.Text.Trim();
                        lComando.Parameters.Add("@P_año_fin", SqlDbType.Int).Value = TxtAñoFin.Text.Trim();
                        break;
                }
                lComando.Parameters.Add("@P_tipo_mercado", SqlDbType.Char).Value = DdlMercado.SelectedValue;
                lComando.Parameters.Add("@P_codigo_ruta", SqlDbType.Int).Value = DdlRuta.SelectedValue;

                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                switch (ddlReporte.SelectedValue)
                {
                    case "1":
                        dtgReporte1.DataSource = lds;
                        dtgReporte1.DataBind();
                        dtgReporte1.Visible = true;
                        break;
                    case "2":
                        dtgReporte2.DataSource = lds;
                        dtgReporte2.DataBind();
                        dtgReporte2.Visible = true;
                        break;
                    case "3":
                        dtgReporte3.DataSource = lds;
                        dtgReporte3.DataBind();
                        dtgReporte3.Visible = true;
                        break;
                    //case "4":
                    //    dtgReporte4.DataSource = lds;
                    //    dtgReporte4.DataBind();
                    //    dtgReporte4.Visible = true;
                    //    break;
                }
                imbExcel.Visible = true;
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfTransporte" + DateTime.Now + ".xls";
        string lstitulo_informe = ddlReporte.SelectedItem.ToString();
        string lsTituloParametros = "";
        try
        {
            if (DdlMercado.SelectedValue != "")
                lsTituloParametros += " - Mercado: " + DdlMercado.SelectedItem.ToString();
            if (DdlRuta.SelectedValue != "0")
                lsTituloParametros += " - Ruta: " + DdlRuta.SelectedItem.ToString();
            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            switch (ddlReporte.SelectedValue)
            {
                case "1":
                    dtgReporte1.EnableViewState = false;
                    lform.Controls.Add(dtgReporte1);
                    if (TxtPerIni.Text.Trim().Length > 0)
                        lsTituloParametros += " - Periodo Inicial: " + TxtPerIni.Text.Trim();
                    if (TxtPerFin.Text.Trim().Length > 0)
                        lsTituloParametros += " - Periodo Final: " + TxtPerFin.Text.Trim();
                    break;
                case "2":
                    dtgReporte2.EnableViewState = false;
                    lform.Controls.Add(dtgReporte2);
                    if (TxtFechaIni.Text.Trim().Length > 0)
                        lsTituloParametros += " - Fecha Inicial: " + TxtFechaIni.Text.Trim();
                    if (TxtFechaFin.Text.Trim().Length > 0)
                        lsTituloParametros += " - Fecha Final: " + TxtFechaFin.Text.Trim();
                    break;
                case "3":
                    dtgReporte3.EnableViewState = false;
                    lform.Controls.Add(dtgReporte3);
                    if (TxtAñoIni.Text.Trim().Length > 0)
                        lsTituloParametros += " - Año Inicial: " + TxtAñoIni.Text.Trim();
                    if (TxtAñoFin.Text.Trim().Length > 0)
                        lsTituloParametros += " - Año Final: " + TxtAñoFin.Text.Trim();
                    break;
                //case "4":
                //    dtgReporte4.EnableViewState = false;
                //    lform.Controls.Add(dtgReporte4);
                //    break;
            }
            lpagina.RenderControl(lhtw);
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Excel.!" + ex.Message.ToString();
        }
    }

    protected void ddlReporte_SelectedIndexChanged(object sender, EventArgs e)
    {
        trfechaI.Visible = false;
        trfechaF.Visible = false;
        trPerI.Visible = false;
        trPerF.Visible = false;
        trAnoI.Visible = false;
        trAnoF.Visible = false;
        trMercado.Visible = false;
        trRuta.Visible = false;
        switch (ddlReporte.SelectedValue)
        {
            case "1":
                trfechaI.Visible = false;
                trfechaF.Visible = false;
                trPerI.Visible = true;
                trPerF.Visible = true;
                trAnoI.Visible = false;
                trAnoF.Visible = false;
                trMercado.Visible = true;
                trRuta.Visible = true;
                break;
            case "2":
                trfechaI.Visible = true;
                trfechaF.Visible = true;
                trPerI.Visible = false;
                trPerF.Visible = false;
                trAnoI.Visible = false;
                trAnoF.Visible = false;
                trMercado.Visible = true;
                trRuta.Visible = true;
                break;
            case "3":
                trfechaI.Visible = false;
                trfechaF.Visible = false;
                trPerI.Visible = false;
                trPerF.Visible = false;
                trAnoI.Visible = true;
                trAnoF.Visible = true;
                trMercado.Visible = true;
                trRuta.Visible = true;
                break;
        }
    }
}