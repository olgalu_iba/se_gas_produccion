﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ComparacionCompraVenta.aspx.cs" Inherits="Informes_frm_ComparacionCompraVenta" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
                    width="80%">
                    <tr>
                        <td align="center" class="th1">
                            <asp:Label ID="lblTitulo" runat="server" ForeColor ="White" ></asp:Label>
                        </td>
                    </tr>
                </table>
                <br /><br /><br /><br /><br /><br />
                <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
                    width="80%">
                    <tr>
                        <td class="td1">
                            Fecha Inicial
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </td>
                        <td class="td1">
                            Fecha Final
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            Participante
                            <br />
                            <asp:TextBox ID="TxtOperador" runat="server" ValidationGroup="detalle"></asp:TextBox>
                            <br />
                            <asp:Button ID="BtnBusOpe" runat="server" Text="Buscar" OnClick="BtnBusOpe_Click" />
                            <asp:Button ID="btnBorraOpe" runat="server" Text="Borrar" OnClick="btnBorraOpe_Click" />
                            <asp:HiddenField ID="hdfOperador" value = "0" runat="server" />
                        </td>
                        <td class="td2">
                            <div style="overflow-y: scroll; width: 300px; height: 100px">
                                <asp:CheckBoxList ID="ddlOperador" runat="server" SelectionMode="Multiple" Rows="4"
                                    Height="10">
                                </asp:CheckBoxList>
                            </div>
                        </td>
                        <td class="td1">
                            Producto
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlProducto" runat="server" OnSelectedIndexChanged="ddlProducto_SelectedIndexChanged"
                                AutoPostBack="true">
                                <asp:ListItem Value="G">Suministro</asp:ListItem>
                                <asp:ListItem Value="T">Transporte</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            Tipo de Mercado
                        </td>
                        <td class="td2">
                            <div style="overflow-y: scroll; width: 300px; height: 100px">
                                <asp:CheckBoxList ID="ddlMercado" runat="server" SelectionMode="Multiple" Rows="4">
                                </asp:CheckBoxList>
                            </div>
                        </td>
                        <td class="td1">
                            Punto/Ruta
                                <br />
                            <asp:TextBox ID="TxtPunto" runat="server" ValidationGroup="detalle" Width= "120"></asp:TextBox>
                            <br />
                            <asp:Button ID="BtnBusPunto" runat="server" Text="Buscar" OnClick="BtnBusPunto_Click" />
                            <asp:Button ID="BtnBorraPunto" runat="server" Text="Borrar" OnClick="btnBorraPunto_Click" />
                            <asp:HiddenField ID="HdfPunto" value = "0" runat="server" />
                        </td>
                        <td class="td2">
                            <div style="overflow-y: scroll; width: 300px; height: 100px">
                                <asp:CheckBoxList ID="ddlPunto" runat="server" SelectionMode="Multiple" Rows="4">
                                </asp:CheckBoxList>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            Modalidad Compra
                        </td>
                        <td class="td2">
                            <div style="overflow-y: scroll; width: 300px; height: 100px">
                                <asp:CheckBoxList ID="ddlModalidadC" runat="server" SelectionMode="Multiple" Rows="4">
                                </asp:CheckBoxList>
                            </div>
                        </td>
                        <td class="td1">
                            Modalidad Venta
                        </td>
                        <td class="td2">
                            <div style="overflow-y: scroll; width: 300px; height: 100px">
                                <asp:CheckBoxList ID="ddlModalidadV" runat="server" SelectionMode="Multiple" Rows="4">
                                </asp:CheckBoxList>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td class="th1" colspan="4" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
        id="tblGrilla" visible="false">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 100%; height: 450px;">
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="fecha" HeaderText="fecha" ItemStyle-HorizontalAlign="Center"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Participante" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_ope" HeaderText="Participante" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_Punto" HeaderText="Cod Punto/Ruta" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_punto" HeaderText="Punto/Ruta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="compras" HeaderText="Compras" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ventas" HeaderText="Ventas" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="modalidad_compra" HeaderText="Modalidades Compra" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="modalidad_venta" HeaderText="Modalidades Venta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="diferencia" HeaderText="Diferencia" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>