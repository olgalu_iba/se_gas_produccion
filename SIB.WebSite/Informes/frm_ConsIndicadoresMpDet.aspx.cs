﻿using System;
using System.Data.SqlClient;
using System.Web.UI;
using Segas.Web.Elements;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Informes_frm_ConsIndicadoresMpDet : Page
{
    private InfoSessionVO goInfo;
    private clConexion lConexion;
    private SqlDataReader lLector;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");

            lConexion = new clConexion(goInfo);

            if (IsPostBack) return;

            // Trae la informacón del indicador
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_indicadores_creg ind left join t_indicador_pub_obs obs on (obs.codigo_indicador = " + this.Request.QueryString["CodInd"] + " and obs.año_proceso = " + this.Request.QueryString["AñoInd"] + " and obs.mes_proceso= " + this.Request.QueryString["MesInd"] + " and obs.tipo_desagregacion= '" + this.Request.QueryString["TipoVista"] + "')", " ind.codigo_indicador = " + this.Request.QueryString["CodInd"]);
            if (lLector.HasRows)
            {
                lLector.Read();
                lblTitulo.Text = "Indicador: " + lLector["descripcion"];
                lblVariable.Text = "<ul>";
                if (lLector["numerador_1"].ToString() != "")
                    lblVariable.Text += "<li>" + lLector["numerador_1"] + "</li>";
                if (lLector["numerador_2"].ToString() != "")
                    lblVariable.Text += "<li>" + lLector["numerador_2"] + "</li>";
                if (lLector["denominador_1"].ToString() != "")
                    lblVariable.Text += "<li>" + lLector["denominador_1"] + "</li>";
                if (lLector["denominador_2"].ToString() != "")
                    lblVariable.Text += "<li>" + lLector["denominador_2"] + "</li>";
                lblVariable.Text += "</ul>";
                lblPeriodo.Text = lLector["periodicidad"].ToString();
                //imgIndicador.ImageUrl = "../Images/" + lLector["ruta_archivo_formula"].ToString();
                lblObs.Text = lLector["observacion"].ToString();
                lLector.Close();
                lLector.Dispose();
            }

            lConexion.Cerrar();
            //Botones
        }
        catch (Exception ex)
        {
            Toastr.Error(this, ex.Message);
        }
    }

}