﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ReporteVerifOpeTrans.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Informes.frm_ReporteVerifOpeTrans" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Reporte" AssociatedControlID="ddlReporte" runat="server" />
                            <asp:DropDownList ID="ddlReporte" CssClass="form-control selectpicker" data-live-search="true" runat="server" OnSelectedIndexChanged="ddlReporte_SelectedIndexChanged" AutoPostBack="true" />
                            <asp:HiddenField ID="hndRegPag" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Inicial" AssociatedControlID="TxtFechaIni" runat="server" />
                            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" Width="100%" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Final" AssociatedControlID="TxtFechaFin" runat="server" />
                            <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" Width="100%" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" id="trTramo" runat="server">
                        <div class="form-group">
                            <asp:Label Text="Tramo" AssociatedControlID="DdlTramo" runat="server" />
                            <asp:DropDownList ID="DdlTramo" CssClass="form-control selectpicker" data-live-search="true" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" id="trMercado" runat="server">
                        <div class="form-group">
                            <asp:Label Text="Tipo Mercado" AssociatedControlID="DdlMercado" runat="server" />
                            <asp:DropDownList ID="DdlMercado" CssClass="form-control" runat="server">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="P">Primario</asp:ListItem>
                                <asp:ListItem Value="S">Secundario</asp:ListItem>
                                <asp:ListItem Value="O">Otras transacciones mercado mayorista</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" id="trPozo" runat="server" visible="false">
                        <div class="form-group">
                            <%--20220202--%>
                            <asp:Label Text="Punto de Entrada" AssociatedControlID="DdlPozo" runat="server" />
                            <asp:DropDownList ID="DdlPozo" CssClass="form-control selectpicker" data-live-search="true" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" id="trPunto" runat="server" visible="false">
                        <div class="form-group">
                            <asp:Label Text="Punto Salida" AssociatedControlID="DdlPunto" runat="server" />
                            <asp:DropDownList ID="DdlPunto" CssClass="form-control selectpicker" data-live-search="true" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" id="trContrato" runat="server" visible="false">
                        <div class="form-group" runat="server">
                            <asp:Label Text="Contrato Definitivo" AssociatedControlID="TxtContrato" runat="server" />
                            <asp:TextBox ID="TxtContrato" runat="server" CssClass="form-control" ValidationGroup="detalle" MaxLength="30" />
                        </div>
                    </div>
                    <%--20220202--%>
                    <div class="col-sm-12 col-md-6 col-lg-4" runat="server" id="trCoincide" visible ="false" >
                        <div class="form-group">
                            <asp:Label Text="Verificación" AssociatedControlID="DdlVerifica" runat="server" />
                            <asp:DropDownList ID="DdlVerifica" CssClass="form-control" runat="server">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="S">Coincide</asp:ListItem>
                                <asp:ListItem Value="N">No Coincide</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="tdTotal" visible="false" runat="server">
                        <div class="form-group" runat="server">
                            <asp:Label Text="Total Registros:" runat="server" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lblTotal" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="tblGrilla" runat="server">
                        <div class="table table-responsive">
                            <%--20180126 rq107-16--%>
                            <asp:DataGrid ID="dtgReporte1" runat="server" AutoGenerateColumns="False" AllowPaging="true" Width="100%" CssClass="table-bordered"
                                PageSize="15" OnPageIndexChanged="dtgReporte1_PageIndexChanged" PagerStyle-HorizontalAlign="Center" Visible="false">
                                <Columns>
                                    <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <%--20161103 rq082-16--%>
                                    <%--<asp:BoundColumn DataField="dia" HeaderText="dia" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>--%>
                                    <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%><%--20200727--%>
                                    <asp:BoundColumn DataField="capacidad_cmmp" HeaderText="Capacidad CMMP (KPCD)" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%><%--20200727--%>
                                    <asp:BoundColumn DataField="capacidad_cdp" HeaderText="Capacidad CDP (KPCD)" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%><%--20200727--%>
                                    <asp:BoundColumn DataField="capacidad_trans" HeaderText="Capacidad Negociada (KPCD)" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn> 
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="validacion" HeaderText="Validación" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="verificacion" HeaderText="Verificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20161103 rq082-16--%>
                                    <asp:BoundColumn DataField="cuenta" Visible="false"></asp:BoundColumn>
                                    <%--20161103 rq082-16--%>
                                    <asp:BoundColumn DataField="codigo_tramo" Visible="false"></asp:BoundColumn>
                                </Columns>
                                <%--20180126 rq107-16--%>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                            </asp:DataGrid>

                            <asp:DataGrid ID="dtgReporteExc1" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table-bordered"
                                Visible="false">
                                <Columns>
                                    <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <%--20161103 rq082-16--%>
                                    <%--<asp:BoundColumn DataField="dia" HeaderText="dia" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>--%>
                                    <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%><%--20200727--%>
                                    <asp:BoundColumn DataField="capacidad_cmmp" HeaderText="Capacidad CMMP (KPCD)" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn> 
                                    <%--20190711 rq034-19--%><%--20200727--%>
                                    <asp:BoundColumn DataField="capacidad_cdp" HeaderText="Capacidad CDP (KPCD)" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn> 
                                    <%--20190711 rq034-19--%><%--20200727--%>
                                    <asp:BoundColumn DataField="capacidad_trans" HeaderText="Capacidad Negociada (KPCD)" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn> 
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="validacion" HeaderText="Validación" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="verificacion" HeaderText="Verificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20161103 rq082-16--%>
                                    <asp:BoundColumn DataField="cuenta" Visible="false"></asp:BoundColumn>
                                    <%--20161103 rq082-16--%>
                                    <asp:BoundColumn DataField="codigo_tramo" Visible="false"></asp:BoundColumn>
                                </Columns>
                                <%--20180126 rq107-16--%>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                            </asp:DataGrid>

                            <asp:DataGrid ID="dtgReporte2" runat="server" AutoGenerateColumns="False" AllowPaging="true" Width="100%" CssClass="table-bordered"
                                PageSize="15" OnPageIndexChanged="dtgReporte2_PageIndexChanged" PagerStyle-HorizontalAlign="Center" Visible="false">
                                <Columns>
                                    <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <%--20161103 rq082-16--%>
                                    <%--<asp:BoundColumn DataField="dia" HeaderText="dia" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>--%>
                                    <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%><%--20200727--%>
                                    <asp:BoundColumn DataField="capacidad_cmmp" HeaderText="Capacidad CMMP (KPCD)" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn> 
                                    <%--20190711 rq034-19--%><%--20200727--%>
                                    <asp:BoundColumn DataField="capacidad_trans" HeaderText="Capacidad Negociada (KPCD)" 
                                        ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="validacion" HeaderText="Validación" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="verificacion" HeaderText="Verificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20161103 rq082-16--%>
                                    <asp:BoundColumn DataField="cuenta" Visible="false"></asp:BoundColumn>
                                    <%--20161103 rq082-16--%>
                                    <asp:BoundColumn DataField="codigo_tramo" Visible="false"></asp:BoundColumn>
                                </Columns>
                                <%--20180126 rq107-16--%>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                            </asp:DataGrid>


                            <asp:DataGrid ID="dtgReporteExc2" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table-bordered"
                                Visible="false">
                                <Columns>
                                    <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <%--20161103 rq082-16--%>
                                    <%--<asp:BoundColumn DataField="dia" HeaderText="dia" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>--%>
                                    <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%><%--20200727--%>
                                    <asp:BoundColumn DataField="capacidad_cmmp" HeaderText="Capacidad CMMP (KPCD)" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>  
                                    <%--20190711 rq034-19--%><%--20200727--%>
                                    <asp:BoundColumn DataField="capacidad_trans" HeaderText="Capacidad Negociada (KPCD)" 
                                         ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="validacion" HeaderText="Validación" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="verificacion" HeaderText="Verificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20161103 rq082-16--%>
                                    <asp:BoundColumn DataField="cuenta" Visible="false"></asp:BoundColumn>
                                    <%--20161103 rq082-16--%>
                                    <asp:BoundColumn DataField="codigo_tramo" Visible="false"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>

                            <asp:DataGrid ID="dtgReporte3" runat="server" AutoGenerateColumns="False" AllowPaging="true" Width="100%" CssClass="table-bordered"
                                PageSize="15" OnPageIndexChanged="dtgReporte3_PageIndexChanged" PagerStyle-HorizontalAlign="Center" Visible="false">
                                <Columns>
                                    <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="dia" HeaderText="Día" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--20220202--%>
                                    <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrada" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="nombre_productor" HeaderText="Productor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%> <%--20200727--%>
                                    <asp:BoundColumn DataField="capacidad_iny" HeaderText="Cantidad Inyectada (MBTUD)" ItemStyle-HorizontalAlign="Right" 
                                        DataFormatString="{0:###,###,##0}"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%> <%--20200727--%>
                                    <asp:BoundColumn DataField="capacidad_rec" HeaderText="Cantidad Recibida (MBTUD)" ItemStyle-HorizontalAlign="Right" 
                                        DataFormatString="{0:###,###,##0}"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="nombre_transportador" HeaderText="Transportador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="coincidencia" HeaderText="Verificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>

                                </Columns>
                                <%--20180126 rq107-16--%>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                            </asp:DataGrid>


                            <asp:DataGrid ID="dtgReporteExc3" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table-bordered"
                                Visible="false">
                                <Columns>
                                    <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="dia" HeaderText="Día" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--20220202--%>
                                    <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrada" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="nombre_productor" HeaderText="Productor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%> <%--20200727--%>
                                    <asp:BoundColumn DataField="capacidad_iny" HeaderText="Cantidad Inyectada (MBTUD)" ItemStyle-HorizontalAlign="Right" 
                                        DataFormatString="{0:###,###,##0}"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%> <%--20200727--%>
                                    <asp:BoundColumn DataField="capacidad_rec" HeaderText="Cantidad Recibida (MBTUD)" ItemStyle-HorizontalAlign="Right" 
                                        DataFormatString="{0:###,###,##0}"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="nombre_transportador" HeaderText="Transportador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="coincidencia" HeaderText="Verificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>

                            <asp:DataGrid ID="dtgReporte4" runat="server" AutoGenerateColumns="False" AllowPaging="true" Width="100%" CssClass="table-bordered"
                                PageSize="15" OnPageIndexChanged="dtgReporte4_PageIndexChanged" PagerStyle-HorizontalAlign="Center" Visible="false">
                                <Columns>
                                    <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="dia" HeaderText="Día" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_punto_sal" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="nombre_transportador" HeaderText="Transportador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="nombre_remitente" HeaderText="Agente Al Que Le Entregó" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%> <%--20200727--%>
                                    <asp:BoundColumn DataField="cantidad_tom" HeaderText="Cantidad Tomada (MBTUD)" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,###,##0}"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%><%--20200727--%>
                                    <asp:BoundColumn DataField="cantidad_ent" HeaderText="Cantidad Entregada (MBTUD)" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,###,##0}"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="nombre_comercializador" HeaderText="Comercializador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="coincidencia" HeaderText="Verificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>

                                </Columns>
                                <%--20180126 rq107-16--%>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                            </asp:DataGrid>

                            <asp:DataGrid ID="dtgReporteExc4" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table-bordered"
                                Visible="false">
                                <Columns>
                                    <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="dia" HeaderText="Día" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_punto_sal" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="nombre_transportador" HeaderText="Transportador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="nombre_remitente" HeaderText="Agente Al Que Le Entregó" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%> <%--20200727--%>
                                    <asp:BoundColumn DataField="cantidad_tom" HeaderText="Cantidad Tomada (MBTUD)" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,###,##0}"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%> <%--20200727--%>
                                    <asp:BoundColumn DataField="cantidad_ent" HeaderText="Cantidad Entregada (MBTUD)" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,###,##0}"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="nombre_comercializador" HeaderText="Comercializador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="coincidencia" HeaderText="Verificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>



                            <asp:DataGrid ID="dtgReporte5" runat="server" AutoGenerateColumns="False" AllowPaging="true" Width="100%" CssClass="table-bordered"
                                PageSize="15" OnPageIndexChanged="dtgReporte5_PageIndexChanged" PagerStyle-HorizontalAlign="Center" Visible="false">
                                <Columns>
                                    <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="dia" HeaderText="Dia" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_punto_sal" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_trans" HeaderText="Nombre Transportador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="contrato_definitivo_tra" HeaderText="Contrato Transportador"
                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_usr_fin" HeaderText="Nombre Usuario Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="contrato_definitivo_usr" HeaderText="Contrato Usuario Final"
                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="coincidencia" HeaderText="Verificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="registrado" HeaderText="Contrato Registrado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                </Columns>
                                <%--20180126 rq107-16--%>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                            </asp:DataGrid>

                            <asp:DataGrid ID="dtgReporteExc5" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table-bordered"
                                Visible="false">
                                <Columns>
                                    <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="dia" HeaderText="Dia" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_punto_sal" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_trans" HeaderText="Nombre Transportador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="contrato_definitivo_tra" HeaderText="Contrato Transportador"
                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_usr_fin" HeaderText="Nombre Usuario Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="contrato_definitivo_usr" HeaderText="Contrato Usuario Final"
                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="coincidencia" HeaderText="Verificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190711 rq034-19--%>
                                    <asp:BoundColumn DataField="registrado" HeaderText="Contrato Registrado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
