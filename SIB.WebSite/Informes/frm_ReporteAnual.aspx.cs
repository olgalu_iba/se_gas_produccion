﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;
using System.Text;
using Segas.Web.Elements;

public partial class Informes_frm_ReporteAnual : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Reportes Anuales de Suministro";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        //Se agrega el comportamiento del botón
        buttons.ExportarExcelOnclick += ImgExcel_Click;
        buttons.FiltrarOnclick += imbConsultar_Click;

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlRueda, "m_tipos_rueda", " estado = 'A' And codigo_tipo_subasta = 2 and tipo_mercado ='S' order by descripcion", 0, 1);
            lConexion.Cerrar();
            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar };
            buttons.Inicializar(botones: botones);
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, EventArgs e)
    {
        bool Error = false;
        DateTime ldFechaI = DateTime.Now ;
        DateTime ldFechaF;
        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Formato Inválido en el Campo Fecha Inicial. <br>");
                Error = true;
            }
        }
        else
        {
            Toastr.Warning(this, "Debe digitar la Fecha Inicial. <br>");
            Error = true;
        }
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
                if (TxtFechaIni.Text.Trim().Length > 0 && ldFechaF < ldFechaI)
                {
                    Toastr.Warning(this, "La Fecha Final NO puede ser Menor que la Fecha Inicial. <br>");
                    Error = true;
                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Formato Inválido en el Campo Fecha Final. <br>");
                Error = true;
            }
        }
        else
        {
            Toastr.Warning(this, "Debe digitar la Fecha Final. <br>");
            Error = true;
        }

        if (!Error)
        {
            try
            {
                dtgReporte1.Visible = false;
                dtgReporte2.Visible = false;
                dtgReporte3.Visible = false;
                dtgReporte4.Visible = false;
                dtgReporte5.Visible = false;
                dtgReporte6.Visible = false;
                dtgReporte7.Visible = false;

                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetInfAnual";
                lComando.Parameters.Add("@P_codigo_reporte", SqlDbType.Int).Value = ddlReporte.SelectedValue;
                lComando.Parameters.Add("@P_fecha_inicial", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                lComando.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaFin.Text.Trim();
                lComando.Parameters.Add("@P_codigo_tipo_mercado", SqlDbType.Char).Value = ddlMercado.SelectedValue;
                lComando.Parameters.Add("@P_codigo_tipo_rueda", SqlDbType.Int).Value = ddlRueda.SelectedValue;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                switch (ddlReporte.SelectedValue)
                {
                    case "1": dtgReporte1.DataSource = lds;
                        dtgReporte1.DataBind();
                        dtgReporte1.Visible = true;
                        break;
                    case "2": dtgReporte2.DataSource = lds;
                        dtgReporte2.DataBind();
                        dtgReporte2.Visible = true;
                        break;
                    case "3": dtgReporte3.DataSource = lds;
                        dtgReporte3.DataBind();
                        dtgReporte3.Visible = true;
                        break;
                    case "4": dtgReporte4.DataSource = lds;
                        dtgReporte4.DataBind();
                        dtgReporte4.Visible = true;
                        break;
                    case "5": dtgReporte5.DataSource = lds;
                        dtgReporte5.DataBind();
                        dtgReporte5.Visible = true;
                        break;
                    case "6": dtgReporte6.DataSource = lds;
                        dtgReporte6.DataBind();
                        dtgReporte6.Visible = true;
                        break;
                    case "7": dtgReporte7.DataSource = lds;
                        dtgReporte7.DataBind();
                        dtgReporte7.Visible = true;
                        break;
                }
                buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
                lConexion.Cerrar();
                lsqldata.Dispose();  //20170926 rq027-17
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pudo Generar el Informe.! " + ex.Message.ToString());
                Error = true;
            }
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, EventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfMensual" + DateTime.Now + ".xls";
        string lstitulo_informe = ddlReporte.SelectedItem.ToString();
        string lsTituloParametros = "";
        try
        {
            if (TxtFechaIni.Text.Trim().Length > 0)
                lsTituloParametros += " - Fecha Inicial: " + TxtFechaIni.Text.Trim();
            if (TxtFechaFin.Text.Trim().Length > 0)
                lsTituloParametros += " - Fecha Final: " + TxtFechaFin.Text.Trim();
            if (ddlMercado.SelectedValue != "")
                lsTituloParametros += " - Tipo Mercado: " + ddlMercado.SelectedItem.ToString();
            if (ddlRueda.SelectedValue != "0")
                lsTituloParametros += " - Tipo Rueda: " + ddlRueda.SelectedItem.ToString();

            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            switch (ddlReporte.SelectedValue)
            {
                case "1": dtgReporte1.EnableViewState = false;
                    lform.Controls.Add(dtgReporte1);
                    break;
                case "2": dtgReporte2.EnableViewState = false;
                    lform.Controls.Add(dtgReporte2);
                    break;
                case "3": dtgReporte3.EnableViewState = false;
                    lform.Controls.Add(dtgReporte3);
                    break;
                case "4": dtgReporte4.EnableViewState = false;
                    lform.Controls.Add(dtgReporte4);
                    break;
                case "5": dtgReporte5.EnableViewState = false;
                    lform.Controls.Add(dtgReporte5);
                    break;
                case "6": dtgReporte6.EnableViewState = false;
                    lform.Controls.Add(dtgReporte6);
                    break;
                case "7": dtgReporte7.EnableViewState = false;
                    lform.Controls.Add(dtgReporte7);
                    break;
            }
            lpagina.RenderControl(lhtw);
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Excel.!" + ex.Message.ToString(); //20180126 rq107-16
        }
    }
    protected void ddlReporte_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlReporte.SelectedValue != "7")
        {
            //trMercado.Visible = true;
            //trRueda.Visible = false;
            ddlRueda.SelectedValue = "0";
        }
        else
        {
            //trMercado.Visible = false;
            //trRueda.Visible = true;
            ddlMercado.SelectedValue = "";
        }
    }
}