﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Informes
{
    public partial class frm_ConsultaPosturasCompra : Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Consulta Posturas de Compra";
        clConexion lConexion = null;
        DataSet lds = new DataSet();
        SqlDataAdapter lsqldata = new SqlDataAdapter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo.ToString();
            lConexion = new clConexion(goInfo);
            //Se agrega el comportamiento del botón
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.FiltrarOnclick += imbConsultar_Click;

            if (IsPostBack) return;

            //Titulo
            Master.Titulo = "Informes";

            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlSubasta, "m_tipos_subasta", " estado = 'A' and codigo_tipo_subasta not in (1,5,7,8) order by descripcion", 0, 1); //20180126 rq107-16
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' And codigo_operador != 0 order by codigo_operador", 0, 4);
            lConexion.Cerrar();
            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar };
            buttons.Inicializar(botones: botones);

            if (Session["tipoPerfil"].ToString() != "N") return;
            ddlOperador.SelectedValue = goInfo.cod_comisionista;
            ddlOperador.Enabled = false;
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                }
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbConsultar_Click(object sender, EventArgs e)
        {
            dtgMaestro.CurrentPageIndex = 0; //20180126 rq107 - 16
            cargarDatos();
        }

        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// //20180126 rq107 - 16
        protected void cargarDatos()
        {
            var lblMensaje = new StringBuilder();
            DateTime ldFecha;
            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato inválido en el campo fecha inicial. <br> ");
                }
            }
            if (TxtFechaIni.Text.Trim().Length <= 0 && ddlOperador.SelectedValue == "0" && (ddlTipoRueda.SelectedValue == "0" || ddlTipoRueda.SelectedValue == "") && TxtNoRueda.Text.Trim().Length <= 0) //20180126 rq107 - 16
            {
                lblMensaje.Append("Debe ingresar algún parámetro de búsqueda. <br>");
            }

            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                Toastr.Warning(this, lblMensaje.ToString());
                return;
            }

            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetPosturasCompra";
                lComando.Parameters.Add("@P_tipo_subasta", SqlDbType.Int).Value = ddlSubasta.SelectedValue; //20211108
                //20180126 rq107 - 16
                if (ddlTipoRueda.SelectedValue == "")
                    lComando.Parameters.Add("@P_tipo_rueda", SqlDbType.Int).Value = "0";
                else
                    lComando.Parameters.Add("@P_tipo_rueda", SqlDbType.Int).Value = ddlTipoRueda.SelectedValue;
                if (TxtNoRueda.Text.Trim().Length <= 0)
                    lComando.Parameters.Add("@P_numero_rueda", SqlDbType.Int).Value = 0;
                else
                    lComando.Parameters.Add("@P_numero_rueda", SqlDbType.Int).Value = TxtNoRueda.Text.Trim();
                lComando.Parameters.Add("@P_fecha_rueda", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                lComando.Parameters.Add("@P_codigo_operador", SqlDbType.VarChar).Value = ddlOperador.SelectedValue;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgMaestro.DataSource = lds;
                dtgMaestro.DataBind();
                tblGrilla.Visible = true;
                buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se pudo generar el informe.! " + ex.Message);
            }
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// ///  //20180126 rq107 - 16
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string lsNombreArchivo = goInfo.Usuario.ToString() + "InfPosturasCompra" + DateTime.Now + ".xls";
            string lstitulo_informe = "Informe Posturas de Compra";
            string lsTituloParametros = "";
            bool Error = false;
            DateTime ldFecha;
            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, "Formato Inválido en el Campo Fecha Inicial. <br>");
                    Error = true;
                }
            }
            if (TxtFechaIni.Text.Trim().Length <= 0 && ddlOperador.SelectedValue == "0" && (ddlTipoRueda.SelectedValue == "0" || ddlTipoRueda.SelectedValue == "") && TxtNoRueda.Text.Trim().Length <= 0) //20180126 rq107 - 16
            {
                Toastr.Warning(this, "Debe Ingresar algún Parámetro de búsqueda. <br>");
                Error = true;
            }
            if (!Error)
            {
                try
                {
                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    SqlCommand lComando = new SqlCommand();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandTimeout = 3600;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_GetPosturasCompra";
                    lComando.Parameters.Add("@P_tipo_subasta", SqlDbType.Int).Value = ddlSubasta.SelectedValue; //20211108
                    //20180126 rq107 - 16
                    if (ddlTipoRueda.SelectedValue == "")
                        lComando.Parameters.Add("@P_tipo_rueda", SqlDbType.Int).Value = "0";
                    else
                        lComando.Parameters.Add("@P_tipo_rueda", SqlDbType.Int).Value = ddlTipoRueda.SelectedValue;
                    if (TxtNoRueda.Text.Trim().Length <= 0)
                        lComando.Parameters.Add("@P_numero_rueda", SqlDbType.Int).Value = 0;
                    else
                        lComando.Parameters.Add("@P_numero_rueda", SqlDbType.Int).Value = TxtNoRueda.Text.Trim();
                    lComando.Parameters.Add("@P_fecha_rueda", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                    lComando.Parameters.Add("@P_codigo_operador", SqlDbType.VarChar).Value = ddlOperador.SelectedValue;
                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgExcel.DataSource = lds;
                    dtgExcel.DataBind();
                    dtgExcel.Visible = true;
                    lConexion.Cerrar();
                    //20211108
                    if (ddlSubasta.SelectedValue != "0" ) 
                        lsTituloParametros += " Tipo Subasta: " + ddlSubasta.SelectedItem.ToString();
                    if (ddlTipoRueda.SelectedValue != "0" && ddlTipoRueda.SelectedValue != "") //20180126 rq107 - 16
                        lsTituloParametros += " Tipo Rueda: " + ddlTipoRueda.SelectedItem.ToString();
                    if (TxtFechaIni.Text.Trim().Length > 0)
                        lsTituloParametros += " - Fecha Rueda: " + TxtFechaIni.Text.Trim();
                    if (TxtNoRueda.Text.Trim().Length > 0)
                        lsTituloParametros += " - Número Rueda: " + TxtNoRueda.Text.Trim();
                    if (ddlOperador.SelectedValue != "0")
                        lsTituloParametros += "  - Operador: " + ddlOperador.SelectedItem.ToString();
                    decimal ldCapacidad = 0;
                    StringBuilder lsb = new StringBuilder();
                    ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                    StringWriter lsw = new StringWriter(lsb);
                    HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                    Page lpagina = new Page();
                    HtmlForm lform = new HtmlForm();
                    dtgExcel.EnableViewState = false;
                    lpagina.EnableEventValidation = false;
                    lpagina.DesignerInitialize();
                    lpagina.Controls.Add(lform);

                    lform.Controls.Add(dtgExcel);
                    lpagina.RenderControl(lhtw);
                    Response.Clear();

                    Response.Buffer = true;
                    Response.ContentType = "aplication/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                    Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                    Response.ContentEncoding = Encoding.Default;
                    Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                    Response.Charset = "UTF-8";
                    Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
                    Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                    Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                    Response.ContentEncoding = Encoding.Default;
                    Response.Write(lsb.ToString());

                    Response.End();
                    lds.Dispose();
                    lsqldata.Dispose();
                    lConexion.CerrarInforme();
                    dtgExcel.Visible = false;

                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, "No se Pudo Generar el Excel.! " + ex.Message.ToString());
                    Error = true;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlSubasta_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubasta.SelectedValue != "0")
            {
                lConexion.Abrir();
                ddlTipoRueda.Items.Clear();
                LlenarControles(lConexion.gObjConexion, ddlTipoRueda, "m_tipos_rueda", " estado = 'A' And codigo_tipo_subasta = " + ddlSubasta.SelectedValue + " order by descripcion", 0, 1);
                lConexion.Cerrar();
            }

        }

        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        /// 20180126 rq107-16
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            cargarDatos();
        }
    }
}