﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;
using System.Configuration;

namespace Informes
{
    public partial class Informes_frm_desistimientoContrato : System.Web.UI.Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Consulta Desistimiento Contratos";
        clConexion lConexion = null;
        SqlDataReader lLector;
        DataSet lds = new DataSet();
        SqlDataAdapter lsqldata = new SqlDataAdapter();
        String strRutaCarga;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo;
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["RutaDesistimiento"].ToString();

            //Se agrega el comportamiento del botón
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.FiltrarOnclick += btnConsultar_Click;

            if (IsPostBack) return;

            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles1(lConexion.gObjConexion, ddlComprador, "m_operador", " estado = 'A' order by razon_social", 0, 4);
            LlenarControles1(lConexion.gObjConexion, ddlVendedor, "m_operador", " estado = 'A' order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlEstado, "m_estado_gas", " tipo_estado ='V' and sigla_estado in ('N', 'D', 'T', 'Z', 'I', 'V', 'C', 'Z') order by descripcion_estado", 2, 3);
            //LlenarControles(lConexion.gObjConexion, ddlObservacionApro, "m_observacion_sin_reg", " estado = 'A' order by observacion", 0, 1);
            lConexion.Cerrar();


            Master.Titulo = "Informes";

            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Excel };
            buttons.Inicializar(botones: botones);
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            dtgMaestro.CurrentPageIndex = 0; //20180126 rq107 - 16
            cargarDatos();
        }

        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// //20180126 rq107 - 16
        protected void cargarDatos()
        {
            bool Error = false;
            DateTime ldFechaI = DateTime.Now;
            DateTime ldFechaF = DateTime.Now;
            var lblMensaje = new StringBuilder();
            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo fecha inicial <br>");
                }
            }
            if (TxtFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo fecha final<br>");
                }
            }
            if (TxtFechaFin.Text.Trim().Length > 0 && TxtFechaIni.Text.Trim().Length == 0)
                lblMensaje.Append("Debe digitar la fecha inicial antes que la final<br>");

            if (TxtFechaFin.Text.Trim().Length > 0 && TxtFechaIni.Text.Trim().Length > 0)
                if (ldFechaI > ldFechaF)
                    lblMensaje.Append("La fecha inicial debe ser menor o igual  que la final<br>");

            if (lblMensaje.ToString() == "")
            {
                try
                {
                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    SqlCommand lComando = new SqlCommand();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandTimeout = 3600;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_GetContDesistimientoRpt";
                    lComando.Parameters.Add("@P_fecha_ini", SqlDbType.VarChar).Value = TxtFechaIni.Text;
                    if (TxtFechaFin.Text != "")
                        lComando.Parameters.Add("@P_fecha_fin", SqlDbType.VarChar).Value = TxtFechaFin.Text;
                    else
                        lComando.Parameters.Add("@P_fecha_fin", SqlDbType.VarChar).Value = TxtFechaIni.Text;
                    if (TxtNoOper.Text != "")
                        lComando.Parameters.Add("@P_operacion", SqlDbType.Int).Value = TxtNoOper.Text;
                    lComando.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = goInfo.cod_comisionista;
                    lComando.Parameters.Add("@P_codigo_comprador", SqlDbType.Int).Value = ddlComprador.SelectedValue;
                    lComando.Parameters.Add("@P_codigo_vendedor", SqlDbType.Int).Value = ddlVendedor.SelectedValue;
                    lComando.Parameters.Add("@P_codigo_tipo_subasta", SqlDbType.Int).Value = ddlSubasta.SelectedValue;
                    lComando.Parameters.Add("@P_estado_contrato", SqlDbType.VarChar).Value = ddlEstado.SelectedValue;
                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgMaestro.DataSource = lds;
                    dtgMaestro.DataBind();
                    tblGrilla.Visible = true;
                    lConexion.Cerrar();
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, "No se pudo generar el informe.! " + ex.Message);
                }
            }
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string lsNombreArchivo = goInfo.Usuario + "InfDesistimiento" + DateTime.Now + ".xls";
            string lstitulo_informe = "Informe Desistimiento Contratos";
            string lsTituloParametros = "";
            var Error = false;
            DateTime ldFechaI = DateTime.Now;
            DateTime ldFechaF = DateTime.Now;
            if (!Error)
            {
                try
                {
                    lsTituloParametros = "";
                    if (TxtFechaIni.Text != "")
                        lsTituloParametros += "  - Fecha Inicial: " + TxtFechaIni.Text;
                    if (TxtFechaFin.Text != "")
                        lsTituloParametros += "  - Fecha Final: " + TxtFechaFin.Text;
                    if (TxtNoOper.Text !="")
                        lsTituloParametros += "  - Operación: " + TxtNoOper.Text;
                    if (goInfo.cod_comisionista  != "0")
                        lsTituloParametros += "  - Comisionista: " + goInfo.cod_comisionista;
                    if (ddlComprador.SelectedValue  != "0")
                        lsTituloParametros += "  - Comprador: " + ddlComprador.SelectedItem;
                    if (ddlVendedor.SelectedValue != "0")
                        lsTituloParametros += "  - vendedor: " + ddlVendedor.SelectedItem;
                    if (ddlSubasta.SelectedValue != "0")
                        lsTituloParametros += "  - Tipo Subasta: " + ddlSubasta.SelectedItem;
                    if (ddlEstado.SelectedValue != "0")
                        lsTituloParametros += "  - Estado: " + ddlEstado.SelectedItem;

                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    SqlCommand lComando = new SqlCommand();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandTimeout = 3600;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_GetContDesistimientoRpt";
                    lComando.Parameters.Add("@P_fecha_ini", SqlDbType.VarChar).Value = TxtFechaIni.Text;
                    if (TxtFechaFin.Text != "")
                        lComando.Parameters.Add("@P_fecha_fin", SqlDbType.VarChar).Value = TxtFechaFin.Text;
                    else
                        lComando.Parameters.Add("@P_fecha_fin", SqlDbType.VarChar).Value = TxtFechaIni.Text;
                    if (TxtNoOper.Text != "")
                        lComando.Parameters.Add("@P_operacion", SqlDbType.Int).Value = TxtNoOper.Text;
                    lComando.Parameters.Add("@P_codigo_operador", SqlDbType.VarChar).Value = goInfo.cod_comisionista;
                    lComando.Parameters.Add("@P_codigo_comprador", SqlDbType.VarChar).Value = ddlComprador.SelectedValue;
                    lComando.Parameters.Add("@P_codigo_vendedor", SqlDbType.VarChar).Value = ddlVendedor.SelectedValue;
                    lComando.Parameters.Add("@P_codigo_tipo_subasta", SqlDbType.VarChar).Value = ddlSubasta.SelectedValue;
                    lComando.Parameters.Add("@P_estado_contrato", SqlDbType.VarChar).Value = ddlEstado.SelectedValue;

                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgExcel.DataSource = lds;
                    dtgExcel.DataBind();
                    lConexion.Cerrar();
                    decimal ldCapacidad = 0;
                    StringBuilder lsb = new StringBuilder();
                    ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                    StringWriter lsw = new StringWriter(lsb);
                    HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                    Page lpagina = new Page();
                    HtmlForm lform = new HtmlForm();
                    dtgExcel.EnableViewState = false;
                    lpagina.EnableEventValidation = false;
                    lpagina.DesignerInitialize();
                    lpagina.Controls.Add(lform);

                    lform.Controls.Add(dtgExcel);
                    lpagina.RenderControl(lhtw);
                    Response.Clear();

                    Response.Buffer = true;
                    Response.ContentType = "aplication/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                    Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                    Response.ContentEncoding = Encoding.Default;
                    Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                    Response.Charset = "UTF-8";
                    Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre + "</td></tr></table>");
                    Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                    Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                    Response.ContentEncoding = Encoding.Default;
                    Response.Write(lsb.ToString());

                    Response.End();
                    lds.Dispose();
                    lsqldata.Dispose();
                    lConexion.CerrarInforme();
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, "No se Pudo Generar el Excel.!" + ex.Message);

                }
            }
        }

        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        /// 20180126 rq107-16
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            cargarDatos();
        }
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            var lblMensaje = new StringBuilder();
            if (e.CommandName.Equals("Ver"))
            {
                try
                {
                    if (e.Item.Cells[8].Text == "&nbsp;" || e.Item.Cells[8].Text == "")
                        Toastr.Error(this, "No hay archivo para mostrar. ");
                    else
                    {
                        string lsCarpetaAnt = "";
                        string[] lsCarperta;
                        lsCarperta = strRutaCarga.Split('\\');
                        lsCarpetaAnt = lsCarperta[lsCarperta.Length - 2];
                        var lsRuta = "../" + lsCarpetaAnt + "/" + e.Item.Cells[8].Text.Replace(@"\", "/");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + lsRuta + "');", true);
                    }
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, "Error al visualizar el archivo. " + ex.Message.ToString());
                }
            }
        }
        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }
        /// <summary>
        /// Inicializa el formulario con la selección en el acordeón 
        /// </summary>
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceLlave).ToString() + "-"+lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

    }
}