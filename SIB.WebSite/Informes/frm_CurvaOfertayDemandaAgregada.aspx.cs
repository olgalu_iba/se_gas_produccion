﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Informes
{
    public partial class frm_CurvaOfertayDemandaAgregada : Page
    {
        InfoSessionVO goInfo;
        static string lsTitulo = "Curvas de Oferta y Demanda"; //20180126 rq107-16
        clConexion lConexion;
        SqlDataReader lLector;
        DataSet lds = new DataSet();

        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo;
            lConexion = new clConexion(goInfo);
            //Se agrega el comportamiento del botón
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.FiltrarOnclick += imbConsultar_Click;

            if (IsPostBack) return;

            //Titulo
            Master.Titulo = "Curvas";
            //Descripcion
            //Master.DescripcionPagina = "D";

            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlSubasta, "m_tipos_subasta", " estado = 'A' and codigo_tipo_subasta not in (5,7,8) order by descripcion", 0, 1); // 20180126 rq107-16
            LlenarControles(lConexion.gObjConexion, ddlTipoRueda, "m_tipos_rueda", " codigo_tipo_rueda = -1 ", 0, 1); // 20210224 ajsutes transporte
            LlenarControles(lConexion.gObjConexion, ddlPuntoEnt, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            lConexion.Cerrar();
            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar };
            buttons.Inicializar(botones: botones);

        }
        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            var lItem = new ListItem { Value = "0", Text = "Seleccione" };
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                var lItem1 = new ListItem
                {
                    Value = lLector.GetValue(liIndiceLlave).ToString(),
                    Text = lLector.GetValue(liIndiceDescripcion).ToString()
                };
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCampos, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlCommand lComando = new SqlCommand();
            lComando.Connection = lConn;
            lComando.CommandTimeout = 3600;
            lComando.CommandType = CommandType.StoredProcedure;
            lComando.CommandText = "pa_ValidarExistencia1";
            lComando.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = lsTabla;
            lComando.Parameters.Add("@P_campos", SqlDbType.VarChar).Value = lsCampos;
            lComando.Parameters.Add("@P_filtro", SqlDbType.VarChar).Value = lsCondicion;
            SqlDataReader lLector;
            lLector = lComando.ExecuteReader();

            var lItem = new ListItem { Value = "0", Text = "Seleccione" };
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                var lItem1 = new ListItem
                {
                    Value = lLector.GetValue(liIndiceLlave).ToString(),
                    Text = lLector.GetValue(liIndiceDescripcion).ToString()
                };
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbConsultar_Click(object sender, EventArgs e)
        {
            dtgCurva.CurrentPageIndex = 0; //20210707 ajuste
            CargarDatos();
        }
        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            bool Error = false;
            DateTime ldFecha;
            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, "Formato Inválido en el Campo Fecha Inicial. <br> ");
                    Error = true;
                }
            }
            if (TxtFechaIni.Text == "" && TxtNoRueda.Text == "" && TxtNoID.Text == "")
            {
                Toastr.Warning(this, "Debe seleccionar la fecha, la rueda o el Id para realizar la búsqueda. <br>");
                Error = true;

            }
            if (!Error)
            {
                try
                {
                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    SqlCommand lComando = new SqlCommand();
                    SqlDataAdapter lsqldata = new SqlDataAdapter();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandTimeout = 3600;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_GetConsCurvaOfertaDemanda";
                    lComando.Parameters.Add("@P_codigo_tipo_subasta", SqlDbType.Int).Value = ddlSubasta.SelectedValue;
                    if (ddlModalidad.SelectedValue == "")
                        lComando.Parameters.Add("@P_modalidad", SqlDbType.Int).Value = "0";
                    else
                        lComando.Parameters.Add("@P_modalidad", SqlDbType.Int).Value = ddlModalidad.SelectedValue;
                    if (TxtNoRueda.Text.Trim().Length <= 0)
                        lComando.Parameters.Add("@P_numero_rueda", SqlDbType.Int).Value = 0;
                    else
                        lComando.Parameters.Add("@P_numero_rueda", SqlDbType.Int).Value = TxtNoRueda.Text.Trim();
                    if (TxtNoID.Text.Trim().Length <= 0)
                        lComando.Parameters.Add("@P_numero_id", SqlDbType.Int).Value = 0;
                    else
                        lComando.Parameters.Add("@P_numero_id", SqlDbType.Int).Value = TxtNoID.Text.Trim();
                    lComando.Parameters.Add("@P_fecha_rueda", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                    if (Session["tipoPerfil"].ToString() == "N")
                        lComando.Parameters.Add("@P_operador", SqlDbType.Int).Value = goInfo.cod_comisionista;
                    else
                        lComando.Parameters.Add("@P_operador", SqlDbType.Int).Value = "0";
                    if (ddlPuntoEnt.SelectedValue == "")
                        lComando.Parameters.Add("@P_codigo_punto_entrega", SqlDbType.Int).Value = "0";
                    else
                        lComando.Parameters.Add("@P_codigo_punto_entrega", SqlDbType.Int).Value = ddlPuntoEnt.SelectedValue;
                    lComando.Parameters.Add("@P_codigo_tipo_rueda", SqlDbType.Int).Value = ddlTipoRueda.SelectedValue; //20210224
                    //20180126 rq107-16
                    lComando.Parameters.Add("@P_destino_rueda", SqlDbType.VarChar).Value = DdlProducto.SelectedValue;
                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgCurva.DataSource = lds;
                    dtgCurva.DataBind();
                    dtgExcel.DataSource = lds; //20180126 rq107-16
                    dtgExcel.DataBind(); //20180126 rq107-16
                    buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
                    lConexion.Cerrar();
                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, "No se Pudo Generar el Informe.! " + ex.Message);
                }
            }
        }
        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgCurva_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgCurva.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();

        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los Link del DataGrid.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgCurva_EditCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Gráfica"))
                {
                    OpenCurvaOferDemAgre(e);
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlSubasta_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubasta.SelectedValue != "3")  //20161019 Subasta UVLP
            {
                lConexion.Abrir();
                ddlModalidad.Items.Clear();
                LlenarControles1(lConexion.gObjConexion, ddlModalidad, "m_modalidad_contractual mod, m_caracteristica_sub carM", "distinct mod.codigo_modalidad, mod.descripcion", " mod.codigo_modalidad = carM.codigo_caracteristica and carM.tipo_caracteristica ='M' and carM.codigo_tipo_subasta = " + ddlSubasta.SelectedValue + " and carM.estado ='A' and mod.estado ='A' order by descripcion", 0, 1);
                lConexion.Cerrar();
            }
            else //20161019 Subasta UVLP
            {
                lConexion.Abrir();
                ddlModalidad.Items.Clear();
                LlenarControles1(lConexion.gObjConexion, ddlModalidad, "m_modalidad_contractual mod", "mod.codigo_modalidad, mod.descripcion", " mod.codigo_modalidad = 1", 0, 1);
                lConexion.Cerrar();
            }
            //20210224
            lConexion.Abrir();
            ddlTipoRueda.Items.Clear();
            LlenarControles(lConexion.gObjConexion, ddlTipoRueda, "m_tipos_rueda", " codigo_tipo_subasta = " + ddlSubasta.SelectedValue + "  and estado ='A'", 0, 1); // 20210224 ajsutes transporte
            lConexion.Cerrar();

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20180126 rq107-16
        protected void DdlProducto_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPuntoEnt.Items.Clear();
            if (DdlProducto.SelectedValue == "G")
            {
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlPuntoEnt, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
                lConexion.Cerrar();
                lblPuntoEnt.Text = "Punto Entrega";
                ddlPuntoEnt.Visible = true;
            }
            if (DdlProducto.SelectedValue == "T")
            {
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlPuntoEnt, "m_ruta_snt", " estado <> 'I' order by descripcion", 0, 4);
                lConexion.Cerrar();
                lblPuntoEnt.Text = "Ruta";
                ddlPuntoEnt.Visible = true;
            }
            if (DdlProducto.SelectedValue == "")
            {
                lblPuntoEnt.Text = "";
                ddlPuntoEnt.Visible = false;
            }
        }
        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20180126 rq107-16
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            SqlDataAdapter lsqldata = new SqlDataAdapter();
            string lsNombreArchivo = goInfo.Usuario + "InfCurva" + DateTime.Now + ".xls";
            string lstitulo_informe = "Informe curva de ofertas y demandas";
            string lsTituloParametros = "";
            try
            {
                if (ddlSubasta.SelectedValue != "0")
                    lsTituloParametros += " Tipo Subasta: " + ddlSubasta.SelectedItem;
                //20210224
                if (ddlTipoRueda.SelectedValue != "0")
                    lsTituloParametros += " Tipo Rueda: " + ddlTipoRueda.SelectedItem;
                if (ddlModalidad.SelectedValue != "0" && ddlModalidad.SelectedValue != "")
                    lsTituloParametros += " Modalidad: " + ddlModalidad.SelectedItem;
                if (TxtNoRueda.Text.Trim().Length > 0)
                    lsTituloParametros += " - Número Rueda: " + TxtNoRueda.Text.Trim();
                if (TxtNoID.Text.Trim().Length > 0)
                    lsTituloParametros += " - Número Id: " + TxtNoID.Text.Trim();
                if (TxtFechaIni.Text.Trim().Length > 0)
                    lsTituloParametros += " - Fecha rueda: " + TxtFechaIni.Text.Trim();
                if (Session["tipoPerfil"].ToString() == "N")
                    lsTituloParametros += " - Operador: " + goInfo.cod_comisionista;
                if (ddlPuntoEnt.SelectedValue != "0" && ddlPuntoEnt.SelectedValue != "")
                    lsTituloParametros += "  - " + lblPunto.Text + ": " + ddlPuntoEnt.SelectedItem;
                if (DdlProducto.SelectedValue != "")
                    lsTituloParametros += "  - Producto: " + DdlProducto.SelectedItem;
                decimal ldCapacidad = 0;
                var lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                var lsw = new StringWriter(lsb);
                var lhtw = new HtmlTextWriter(lsw);
                var lpagina = new Page();
                var lform = new HtmlForm();
                dtgExcel.EnableViewState = false;
                dtgExcel.Visible = true;
                lpagina.EnableEventValidation = false;
                lpagina.Controls.Add(lform);
                lpagina.DesignerInitialize();
                lform.Controls.Add(dtgExcel);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());

                Response.End();
                //lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
                dtgExcel.Visible = false;
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pudo Generar el Excel.! " + ex.Message);
            }
        }

        #region Curva de Oferta y Demanda Agregada

        /// <summary>
        /// Abre el modal de la Curva de Oferta y Demanda Agregada
        /// </summary>
        private void OpenCurvaOferDemAgre(DataGridCommandEventArgs e)
        {
            if (dtgCurva.Items[e.Item.ItemIndex].Cells[7].Text == " 0")
            {
                lblCantidadAdjudicada.Text = "No Adjudicado";
                lblPrecioAdjudicacion.Text = "No Adjudicado";
            }
            else
            {
                lblCantidadAdjudicada.Text = dtgCurva.Items[e.Item.ItemIndex].Cells[7].Text;
                lblPrecioAdjudicacion.Text = dtgCurva.Items[e.Item.ItemIndex].Cells[8].Text;
            }
            lblNoId.Text = dtgCurva.Items[e.Item.ItemIndex].Cells[0].Text;
            var lsDestino = dtgCurva.Items[e.Item.ItemIndex].Cells[10].Text;
            lblPunto.Text = dtgCurva.Items[e.Item.ItemIndex].Cells[5].Text; //cambio 20160128

            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            SqlCommand lComando1 = new SqlCommand();
            SqlDataAdapter lsqldata1 = new SqlDataAdapter();
            lComando1.Connection = lConexion.gObjConexion;
            lComando1.CommandTimeout = 3600;
            lComando1.CommandType = CommandType.StoredProcedure;
            lComando1.CommandText = "pa_GetPreciosCurva1";
            lComando1.Parameters.Add("@P_numero_id", SqlDbType.Int).Value = lblNoId.Text;
            lComando1.ExecuteNonQuery();
            lsqldata1.SelectCommand = lComando1;
            lsqldata1.Fill(lds);
            dtgInformacion.DataSource = lds;
            dtgInformacion.DataBind();
            lConexion.Cerrar();

            lblTipoPunto.Text = lsDestino == "G" ? "Punto de Entrega" : "Ruta";

            //Carga la grafica
            AuctionGraphics.GenerateGraph(this, dtgInformacion, lblNoId.Text, lsDestino != "G");
            //Se abre el modal de Ingreso de Declaración de Información
            Modal.Abrir(this, mdlCurvaOfertaDemAgre.ID, mdlCurvaOfertaDemAgreInside.ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseCurvaOferDemAgre_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Ingreso de Declaración de Información
                Modal.Cerrar(this, mdlCurvaOfertaDemAgre.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Curva de Oferta y Demanda Agregada

        public void GenerateGraph1(Control page, DataGrid dtgInformacion, string lblNoId, bool transporte = false)
        {
            var builder = new StringBuilder();

            var dt = new DataTable();
            dt.Columns.Add("ID");
            dt.Columns.Add("PRECIO");
            dt.Columns.Add("CANTIDAD_DE_VENTA");
            dt.Columns.Add("CANTIDAD_DE_COMPRA");

            var contador = 0;
            //Se agrega el punto inicial    
            DataGridItem grilla = dtgInformacion.Items[0];
            DataRow da = dt.NewRow();
            contador++;
            da[0] = contador;
            da[2] = Math.Round(decimal.Parse(grilla.Cells[2].Text), 2); //cantidad de compra
            da[1] = 0; //precio
            da[3] = 0; //cantidad de venta

            dt.Rows.Add(da);

            da = dt.NewRow();
            contador++;
            da[0] = contador;
            da[2] = Math.Round(decimal.Parse(grilla.Cells[2].Text), 2); //cantidad de compra
            da[1] = Math.Round(decimal.Parse(grilla.Cells[0].Text), 2); // precio
            da[3] = 0; //cantidad de venta

            dt.Rows.Add(da);
            var grafCompra = "S"; //20210224

            for (var index = 1; index < dtgInformacion.Items.Count; index++)
            {
                DataGridItem grilla2 = dtgInformacion.Items[index];

                if (index - 1 < 0) continue;
                DataGridItem grilla3 = dtgInformacion.Items[index - 1];

                DataRow da2 = dt.NewRow();
                contador++;
                da2[0] = contador;
                da2[2] = Math.Round(decimal.Parse(grilla2.Cells[2].Text), 2); //cantidad de compra
                da2[1] = Math.Round(decimal.Parse(grilla3.Cells[0].Text), 2);  //precio
                da2[3] = Math.Round(decimal.Parse(grilla3.Cells[1].Text), 2);  //cantidad de venta
                dt.Rows.Add(da2);

                DataRow da3 = dt.NewRow();
                contador++;
                da3[0] = contador;
                da3[2] = Math.Round(decimal.Parse(grilla2.Cells[2].Text), 2); //cantidad de compra
                da3[1] = Math.Round(decimal.Parse(grilla2.Cells[0].Text), 2);  //precio            
                da3[3] = Math.Round(decimal.Parse(grilla3.Cells[1].Text), 2);  //cantidad de venta
                dt.Rows.Add(da3);

                grafCompra = grilla2.Cells[9].Text; //20210224
            }

            //Se agrega el punto final
            DataGridItem grilla4 = dtgInformacion.Items[dtgInformacion.Items.Count - 1];
            DataRow da4 = dt.NewRow();
            contador++;
            da4[0] = contador;
            da4[2] = 0; //cantidad de compra
            da4[1] = Math.Round(decimal.Parse(grilla4.Cells[0].Text), 2); //precio
            da4[3] = Math.Round(decimal.Parse(grilla4.Cells[1].Text), 2); //cantidad de venta
            dt.Rows.Add(da4);

            builder.Append("[['Precio','Demanda', 'Puntos', 'Oferta']");
            for (var i = 0; i < dt.Rows.Count; i++)
            {
                //cantidad de venta
                var venta = dt.Rows[i]["CANTIDAD_DE_VENTA"].ToString();
                //cantidad de compra
                var compra = dt.Rows[i]["CANTIDAD_DE_COMPRA"].ToString();
                //precio
                var precio = dt.Rows[i]["PRECIO"].ToString();

                //puntos
                var punto = string.Empty;
                if (i + 1 < dt.Rows.Count && dt.Rows[i + 1]["PRECIO"].ToString().Equals(dt.Rows[i]["PRECIO"].ToString()))
                    punto = venta;

                var punto2 = string.Empty;
                if (i - 2 >= 0 && !dt.Rows[i - 2]["CANTIDAD_DE_COMPRA"].ToString().Equals(dt.Rows[i]["CANTIDAD_DE_COMPRA"].ToString()))
                    punto2 = compra;

                //Arreglo de datos para pintar en la grafica de Google
                var res = punto.Equals(string.Empty) ? punto2 : punto; 

                precio = precio.Replace(',', '.');
                venta = venta.Replace(',', '.');
                res = res.Replace(',', '.');
                compra = compra.Replace(',', '.');

                if (grafCompra == "S") //2021024
                {
                    if (i % 2 == 1 && i + 1 < dt.Rows.Count && venta.Equals(dt.Rows[i + 1]["CANTIDAD_DE_VENTA"].ToString()))
                        builder.Append($", [{precio}, {venta}, , {compra}]");
                    else
                        builder.Append($", [{precio}, {venta}, {res}, {compra}]");
                }
                else //20210024
                {
                    if (i % 2 == 1 && i + 1 < dt.Rows.Count && venta.Equals(dt.Rows[i + 1]["CANTIDAD_DE_VENTA"].ToString()))
                        builder.Append($", [{precio}, {compra}, , {compra}]");
                    else
                        builder.Append($", [{precio}, {compra}, {res}, {compra}]");
                }
            }
            builder.Append("]");
            //builder.Clear();
            //builder.Append("[['Precio','Demanda', 'Puntos', 'Oferta'], [0,  ,0 ,0 ], [0.00,  , 0,0], [0.00,, 5000, 5000], [0.51, 0,5000 ,5000 ], [0.51, , 10000, 10000]]");
            ///builder.Append("[['Precio','Demanda', 'Puntos', 'Oferta'], [0,  , ,0 ], [0.00,  , ,0], [0.00,, 5000, 5000], [0.51,0 , ,5000 ], [0.51, , 10000, 10000]]");
            //builder.Append("[['Precio','Demanda', 'Puntos', 'Oferta'], [0,  , ,0 ], [0.00,  , ,0], [0.00,, 5000, 5000], [0.51,5000 , ,5000 ], [0.51, , 10000, 10000], [0.7, 10000, , 10000], [0.7, , 12000, 12000]]");

            var str = $"drawChartRefresh({builder},'{transporte}', '{null}', '{null}');";
            ScriptManager.RegisterStartupScript(page, page.GetType(), Guid.NewGuid().ToString(), str, true);
        }
    }
}
