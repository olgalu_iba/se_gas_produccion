﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ReporteAnual.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Informes_frm_ReporteAnual" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                       
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Reporte</label>
                            <asp:DropDownList ID="ddlReporte" runat="server" OnSelectedIndexChanged="ddlReporte_SelectedIndexChanged"
                                AutoPostBack="true" CssClass="form-control selectpicker" data-live-search="true">
                                <asp:ListItem Value="1">Promedio de cantidades negociadas por mes</asp:ListItem>
                                <asp:ListItem Value="2">Promedio de cantidades negociadas diariamente</asp:ListItem>
                                <asp:ListItem Value="3">Cantidad total negociada por año</asp:ListItem>
                                <asp:ListItem Value="4">Cantidad total negociada por mes</asp:ListItem>
                                <asp:ListItem Value="5">Número de negociaciones del año</asp:ListItem>
                                <asp:ListItem Value="6">Número promedio de negociaciones diarias</asp:ListItem>
                                <asp:ListItem Value="7">Comportamiento de subasta UVCP</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Inicial</label>
                             
                            <asp:TextBox ID="TxtFechaIni" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>

                  

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div id="tdMes01" class="form-group" runat="server">
                            <label for="Name">Fecha Final</label>
                              <asp:TextBox ID="TxtFechaFin" runat="server" ValidationGroup="detalle" Width="100px" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                   
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Tipo Mercado</label>
                     <asp:DropDownList ID="ddlMercado" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="P">Primario</asp:ListItem>
                                <asp:ListItem Value="S">Secundario</asp:ListItem>
                                <asp:ListItem Value="O">Otras Transacciones del Mercado Mayorista</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Tipo Rueda</label>
                              <asp:DropDownList ID="ddlRueda" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                   
                    
            </div>
        </div>
    </div>
        </div>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblMensaje">
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 1050px; height: 350px;">
                    <asp:DataGrid ID="dtgReporte1" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="cantidad" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cnt_contratos" HeaderText="No. contratos" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio_prom" HeaderText="precio promedio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,##0.00}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_prom" HeaderText="cantidad promedio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgReporte2" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <%--20180126 rq107-16--%>
                            <asp:BoundColumn DataField="dia" HeaderText="día" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="cantidad" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cnt_contratos" HeaderText="No. contratos" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio_prom" HeaderText="precio promedio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,##0.00}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_prom" HeaderText="cantidad promedio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgReporte3" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="cantidad" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio_prom" HeaderText="precio promedio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgReporte4" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="cantidad" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio_prom" HeaderText="precio promedio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgReporte5" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_producto" HeaderText="Tipo Producto" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20180126 rq107-16--%>
                            <asp:BoundColumn DataField="cnt_contratos" HeaderText="No. Contratos" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgReporte6" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_producto" HeaderText="Tipo Producto" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20180126 rq107-16--%>
                            <asp:BoundColumn DataField="cont_dia" HeaderText="Prom contratos día" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgReporte7" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <%--20180126 rq107-16--%>
                            <asp:BoundColumn DataField="dia" HeaderText="Día" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_rueda" HeaderText="Tipo Rueda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="cantidad" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cnt_contratos" HeaderText="No. contratos" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cnt_prom" HeaderText="cantidad promedio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio_prom" HeaderText="precio promedio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
 </asp:Content>