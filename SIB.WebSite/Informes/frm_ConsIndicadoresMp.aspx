﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ConsIndicadoresMp.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Informes.frm_ConsIndicadoresMp" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Tipo de Indicador <b>(*)</b></label>
                            <asp:DropDownList ID="ddlTipoIndica" runat="server" Width="100%" OnSelectedIndexChanged="ddlTipoIndica_SelectedIndexChanged" AutoPostBack="true" TabIndex="0" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Tipo de Vista <b>(*)</b></label>
                            <asp:DropDownList ID="ddlTipoVista" runat="server" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoVista_SelectedIndexChanged" TabIndex="1" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Año Publicación Indicador <b>(*)</b></label>
                            <asp:TextBox ID="TxtAnoIndica" runat="server" ValidationGroup="detalle" TabIndex="2" Width="100%" OnTextChanged="TxtAnoIndica_TextChanged" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FteTxtAnoIndica" runat="server" Enabled="True" FilterType="Custom, Numbers" TargetControlID="TxtAnoIndica"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <div id="tdMes01" runat="server">
                                <label for="Name">Mes Publicación Indicador <b>(*)</b></label>
                            </div>
                            <div id="tdMes02" runat="server">
                                <asp:DropDownList ID="ddlMesIndica" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlMesIndica_SelectedIndexChanged" TabIndex="3" CssClass="form-control selectpicker" data-live-search="true" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Periodo Inicial <b>(*)</b></label>
                            Año:
                            <asp:TextBox ID="TxtAnoIni" runat="server" ValidationGroup="detalle" TabIndex="4" CssClass="form-control" OnTextChanged="TxtAnoIni_TextChanged" AutoPostBack="true" Width="100%"></asp:TextBox>-
                            Mes:
                            <asp:DropDownList ID="ddlMesIni" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlMesIni_SelectedIndexChanged" TabIndex="5" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Periodo Final <b>(*)</b></label>
                            Año:
                            <asp:TextBox ID="TxtAnoFin" runat="server" ValidationGroup="detalle" TabIndex="6" OnTextChanged="TxtAnoFin_TextChanged" AutoPostBack="true" Width="100%" CssClass="form-control" />-
                            Mes:
                            <asp:DropDownList ID="ddlMesFinal" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlMesFinal_SelectedIndexChanged" TabIndex="7" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" style="display: none">
                        <div class="form-group">
                            <label for="Name">Modalidad de contrato</label>
                            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" style="display: none">
                        <div class="form-group">
                            <label for="Name">Tipo de Mercado</label>
                            <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="P">Primario</asp:ListItem>
                                <asp:ListItem Value="S">Secundario</asp:ListItem>
                                <asp:ListItem Value="O">Otras transacciones de mercado mayorista</asp:ListItem>
                            </asp:DropDownList>
                            <%-- <asp:Button ID="btnConsultar" runat="server" Text="Consultar" OnClick="btnConsultar_Click"
                                  TabIndex="8" />--%>
                                      &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnDetalle" runat="server" Text="Detalle" TabIndex="9" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(*) Filtros Obligatorios
                        </div>
                    </div>
                </div>
                <div id="tblGrilla" runat="server">
                    <div class="table table-responsive">
                        <asp:DataGrid ID="dtgIndicador1_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="PTDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PP (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador1_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">

                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="código Productor" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="nombre Productor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="PTDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PP (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador1_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">

                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="código Fuente" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="PTDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PP (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador2_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">

                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PTDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador2" HeaderText="CIDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador2_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">

                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Código Fuente" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PTDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador2" HeaderText="CIDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador2_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">

                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nit_operador" HeaderText="Nit Operador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PTDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador2" HeaderText="CIDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador3_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">

                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PP (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador3_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">

                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Productor" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Productor" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PP (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador3_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">

                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Código Fuente" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Fuente" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PP (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador4_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Oferta Comprometida en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PTDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador2" HeaderText="CIDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador4_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Código Fuente" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Oferta Comprometida en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PTDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador2" HeaderText="CIDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador4_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nit_operador" HeaderText="Nit operador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Oferta Comprometida en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PTDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador2" HeaderText="CIDV (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador5_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Oferta Comprometida en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador5_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Código Fuente" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Oferta Comprometida en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador5_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nit_operador" HeaderText="Nit Operador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Oferta Comprometida en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador6_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Oferta Comprometida en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PP (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador6_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">

                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Código Fuente" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Oferta Comprometida en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PP (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador6_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Productor" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Productor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Oferta Comprometida en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PP (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador7_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Demanda Regulada (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador7_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Demanda Regulada (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador8_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratacón en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador8_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Código fuente" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador9_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Oferta Comprometida en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador9_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Código fuente" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Oferta Comprometida en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador10_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador10_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Código fuente" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador10_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nit_operador" HeaderText="Nit Operador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador10_S" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_sector" HeaderText="Código Sector de Consumo"
                                    ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_sector" HeaderText="Sector de Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="PTDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador2" HeaderText="CIDVF (Mbtud)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador11_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Oferta Comprometida en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador11_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Código fuente" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Oferta Comprometida en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador11_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Oferta Comprometida en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador11_S" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_sector" HeaderText="Código Sector de Consumo"
                                    ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_sector" HeaderText="Sector de Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Oferta Comprometida en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador12_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Contratación Total (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador12_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Contratación Total (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador13_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">

                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Contratación Total (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador13_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Contratación total (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador14" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_mercado" HeaderText="Código Mercado Relevante"
                                    ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_mercado" HeaderText="Mercado Relevante" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Contratación de Capacidad de Transporte (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador15" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación en Firme" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Contratación de Capacidad de Transporte (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador16" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código operador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Código tramo" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación de Transporte  (Kpcd)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Capacidad Máxima de Mediano Plazo (Kpcd)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador17_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Código Modalidad" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación por Modalidad (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Contratación Total (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador17_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Código Modalidad" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación por Modalidad (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Contratación Total (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador18_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Código Modalidad" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación por Modalidad (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Contratación total (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador18_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Código Modalidad" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contración por Modalidad (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Contratación Total (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador19" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Código Tramo" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Código Modalidad" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_mercado" HeaderText="Código Mercado" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_mercado" HeaderText="Mercado Relevante" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación de Transporte por Modalidad (Kpcd)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Capacidad Máxima de Mediano Plazo (Kpcd)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador20" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Código Tramo" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Código Modalidad" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación de transporte por Modalidad (Kpcd)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Capacidad Máxima de Mediano Plazo (Kpcd)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador21_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Código Fuente" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación por Comprador (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Oferta Comprometida en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador21_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador1" HeaderText="Código Vendedor" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador1" HeaderText="Operador Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Comprador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación de Suministro (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Oferta Comprometida en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador21_A" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación de Suministro (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Oferta Comprometida en Firme (Mbtud)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador22" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Código Tramo" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Contratación Por Agente (Kpcd)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Capacidad Máxima de Mediano Plazo (Kpcd)"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador23_N" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Cantidad * Precio" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador23_F" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Código Fuente" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Cantidad * Precio" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador23_P" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Cantidad * Precio" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador23_C" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Código Modalidad" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Cantidad * Precio" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador23_D" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_tipo_demanda" HeaderText="Código Tipo de Demanda"
                                    ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo de Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Cantidad * Precio" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador23_S" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_sector" HeaderText="Código Sector Consumo" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_sector" HeaderText="Sector de Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Cantidad * Precio" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgIndicador23_T" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_pozo" HeaderText="Códigod Fuente" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_demanda" HeaderText="Código Tipo Demanda" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo de Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_sector" HeaderText="Código Sector de Consumo"
                                    ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_sector" HeaderText="Sector de Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numerador1" HeaderText="Cantidad * Precio" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="denominador1" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_indicador" HeaderText="indicador" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="3" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>

    <asp:HiddenField ID="CodInd" runat="server" />
    <asp:HiddenField ID="AnoInd" runat="server" />
    <asp:HiddenField ID="MesInd" runat="server" />
    <asp:HiddenField ID="tipoVista" runat="server" />

    <%--Modals--%>
    <div class="modal fade" id="CrearRegistro" tabindex="-1" role="dialog" aria-labelledby="mdlCrearRegistroLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="CrearRegistroInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlCrearRegistroLabel" runat="server">Indicador: </h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <center>
                                            <asp:Image ID="imgIndicador" runat="server" />
                                        </center>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label Text="Variables" AssociatedControlID="lblVariable" runat="server" />
                                        <asp:Literal ID="lblVariable" Mode="Transform" runat="server"></asp:Literal>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label Text="Periodicidad" AssociatedControlID="lblPeriodo" runat="server" />
                                        <asp:TextBox ID="lblPeriodo" Enabled="False" CssClass="form-control" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label Text="Observaciones de Publicación" AssociatedControlID="lblObs" runat="server" />
                                        <%--<asp:TextBox ID="lblObs" Enabled="False" CssClass="form-control" runat="server" />--%>
                                        <%--20201027--%>
                                        <asp:TextBox ID="lblObs" type="Text"  CssClass="form-control" enabled="false"  runat="server" TextMode ="MultiLine"  rows="4"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="imbSalir" Text="Cancelar" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" CssClass="btn btn-secondary" runat="server" CausesValidation="false" OnClick="imbSalir_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
