﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ConsModRegCont.aspx.cs"
    Inherits="Informes_frm_ConsModRegCont" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />

                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Inicial Negociación:</label>
                            <asp:TextBox ID="TxtFechaIni" runat="server" Width="100%" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Final Negociación:</label>
                            <asp:TextBox ID="TxtFechaFin" runat="server" Width="100%" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>

                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Número de Operación:</label>
                            <asp:TextBox ID="TxtOperacion" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtOperacion" runat="server" TargetControlID="TxtOperacion"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div id="tdMes01" class="form-group" runat="server">
                            <label for="Name">Tipo Mercado</label>
                            <asp:DropDownList ID="ddlMercado" runat="server" Width="100%" CssClass="form-control">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="P">Primario</asp:ListItem>
                                <asp:ListItem Value="S">Secundario</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                    </div>
                    <div id="tdMes02" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                        <div class="form-group">
                            <label for="Name">Producto:</label>
                            <asp:DropDownList ID="ddlProducto" runat="server" Width="100%" CssClass="form-control">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="S">Suminsitro</asp:ListItem>
                                <asp:ListItem Value="T">Transporte</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Tipo Subasta:</label>
                            <asp:DropDownList ID="ddlSubasta" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Causa modificación:</label>
                            <asp:DropDownList ID="ddlCausa" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Id Registro:</label>
                            <asp:TextBox ID="TxtIdRegistro" runat="server" Width="100%" ValidationGroup="detalle" MaxLength="10" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtIdRegistro" runat="server" TargetControlID="TxtIdRegistro"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                </div>
                 <div border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <%--  <tr>
                <td class="th1" colspan="3" align="center">
                    <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                        OnClick="imbConsultar_Click" Height="40" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
                </td>
            </tr>--%>
      
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
       
    </div>
    <div border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla" visible="false">
       
                <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                        PagerStyle-HorizontalAlign="Center" 
                        OnEditCommand="dtgMaestro_EditCommand" >
                       
                        <Columns>
                            <asp:BoundColumn DataField="no_operacion" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_negociacion" HeaderText="F. Neg" ItemStyle-HorizontalAlign="Left"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="no_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_compra" HeaderText="Cod Comp" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Comp" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_venta" HeaderText="Cod Vend" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Vend" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="punto_ruta" HeaderText="Punto/Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_punto_ruta" HeaderText="desc Punto/Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Cod Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Desc Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fec Ini" ItemStyle-HorizontalAlign="Left"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_final" HeaderText="Fec Fin" ItemStyle-HorizontalAlign="Left"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad/Capacidad " ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                            <%--20160607--%>
                            <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20160607--%>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="Desc Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20171130--%>
                            <%--20180126 rq107-16--%>
                            <asp:BoundColumn DataField="codigo_causa" HeaderText="Código Causa" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20171130--%>
                            <asp:BoundColumn DataField="desc_causa" HeaderText="Causa modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Detalle" EditText="Detalle" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                        </Columns>
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </div>
         
    </div>
    <div border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tbDetalle" visible="false">
     
                <asp:Button ID="BtnRegresar" runat="server" Text="Regresar" OnClick="BtnRegresar_Click" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel1" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel1_Click"
                    Height="35" />
                <asp:HiddenField ID="hdnContrato" runat="server" />
             
                <asp:Label ID="lblMensajeDet" runat="server" ForeColor="Red"></asp:Label>
         
                <div style="overflow: scroll; width: 1050px; height: 350px;">
                    <asp:DataGrid ID="dtgDetalle" runat="server" AutoGenerateColumns="False" AllowPaging="false"  Width="100%" CssClass="table-bordered"
                        PagerStyle-HorizontalAlign="Center"  OnEditCommand="dtgDetalle_EditCommand">
                     
                        <Columns>
                            <asp:BoundColumn DataField="no_operacion" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_negociacion" HeaderText="F. Neg" ItemStyle-HorizontalAlign="Left"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="no_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_compra" HeaderText="Cod Comp" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Comp" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_venta" HeaderText="Cod Vend" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Vend" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="punto_ruta" HeaderText="Punto/Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_punto_ruta" HeaderText="desc Punto/Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Cod Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Desc Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fec Ini" ItemStyle-HorizontalAlign="Left"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_final" HeaderText="Fec Fin" ItemStyle-HorizontalAlign="Left"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad/Capacidad " ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                            <%--20160607--%>
                            <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20160607--%>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="Desc Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20171130--%>
                            <%--20180126 rq107-16--%>
                            <asp:BoundColumn DataField="codigo_causa" HeaderText="Código Causa" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20171130--%>
                            <asp:BoundColumn DataField="desc_causa" HeaderText="Causa modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="login_usuario" HeaderText="usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="fecha-hora" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20171130--%>
                            <asp:EditCommandColumn HeaderText="Contrato original" EditText="Original"></asp:EditCommandColumn>
                            <%--20171130--%>
                            <asp:EditCommandColumn HeaderText="Contrato Modificado" EditText="Modificado"></asp:EditCommandColumn>
                            <%--20171130--%>
                            <asp:BoundColumn DataField="contrato_original" Visible="false"></asp:BoundColumn>
                            <%--20171130--%>
                            <asp:BoundColumn DataField="contrato_nuevo" Visible="false"></asp:BoundColumn>
                        </Columns>
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </div>
           
    </div>
            </div>
        </div>
    </div>
   
</asp:Content>
