﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_InformeCreg.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Informes_frm_InformeCreg" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">

                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Inicial</label>
                            <asp:TextBox ID="TxtFechaIni" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static" Width="100%"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RfvTxtFechaFin" runat="server" ErrorMessage="El Campo Fechs Final es obligatorio"
                                ControlToValidate="TxtFechaFin" ValidationGroup="detalle">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Final</label>
                            <asp:TextBox ID="TxtFechaFin" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static" Width="100%"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="El Campo Fechs Final es obligatorio"
                                ControlToValidate="TxtFechaFin" ValidationGroup="detalle">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <asp:Label ID="lblMensaje" runat="server" />

                    <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                        <asp:DataGrid ID="dtgInformacion" runat="server" AutoGenerateColumns="False" AllowPaging="true"
                            PagerStyle-HorizontalAlign="Center" PageSize="30" Width="100%" CssClass="table-bordered" OnPageIndexChanged="dtgMaestro_PageIndexChanged">
                            <%--20160711--%>

                            <Columns>
                                <asp:BoundColumn DataField="tipo_rueda" HeaderText="Tipo Rueda" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="150px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tipo_subasta" HeaderText="Tipo Subasta" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="150px"></asp:BoundColumn>
                                <%--20181106--%>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="No. Contrato" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--20181106--%>
                                <asp:BoundColumn DataField="id_registro" HeaderText="Id. Registro" ItemStyle-HorizontalAlign="Right"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="no_operacion" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Right"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_vendedor" HeaderText="Nombre Vendedor" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="250px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_vendedor" HeaderText="Código Vendedor" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="rol_vendedor" HeaderText="Rol Vendedor" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_comprador" HeaderText="Nombre Comprador" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="250px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_comprador" HeaderText="Código Comprador" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="rol_comprador" HeaderText="Rol Comprador" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_contrato" HeaderText="Cantidad Contrato" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0: ###,###,##0}"></asp:BoundColumn>
                                <%--20170926 rq027-17--%>
                                <asp:BoundColumn DataField="capacidad_transporte" HeaderText="Capacidad Transporte"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0: ###,###,##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="lugar_entrega" HeaderText="Lugar de Entrega" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="modalidad" HeaderText="Modalidad Entrega" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="periodo_entrega" HeaderText="Periodo Entrega" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nit_usuario_final" HeaderText="Nit Usuario Final" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_usuario_final" HeaderText="Nombre Usuario Final"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="mercado_relevante" HeaderText="Mercado Relevante" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="punto_salida" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tipo_demanda" HeaderText="Tipo Demanda" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--20181106--%>
                                <asp:BoundColumn DataField="sector_consumo" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_usuario_final" HeaderText="Cantidad Usuario Final"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,##0}"></asp:BoundColumn>
                                <%--20160602--%>
                                <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20160602--%>
                                <asp:BoundColumn DataField="desc_fuente" HeaderText="desc fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20181106--%>
                                <asp:BoundColumn DataField="ind_extemporaneo" HeaderText="Contrato extemporáneo Bilateral" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>

                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />

                            <%--20160711--%>
                        </asp:DataGrid>


                    </div>
                    <div class="table table-responsive" style="display:none; overflow: scroll; height: 450px;">
                        <asp:DataGrid ID="dtgInformacion1" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            PagerStyle-HorizontalAlign="Center" CssClass="table-bordered" >

                      
                            <Columns>
                                <asp:BoundColumn DataField="tipo_rueda" HeaderText="Tipo Rueda" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="150px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tipo_subasta" HeaderText="Tipo Subasta" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="150px"></asp:BoundColumn>
                                <%--20181106--%>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="No. Contrato" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--20181106--%>
                                <asp:BoundColumn DataField="id_registro" HeaderText="Id. Registro" ItemStyle-HorizontalAlign="Right"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="no_operacion" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Right"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_vendedor" HeaderText="Nombre Vendedor" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="250px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_vendedor" HeaderText="Código Vendedor" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="rol_vendedor" HeaderText="Rol Vendedor" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_comprador" HeaderText="Nombre Comprador" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="250px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_comprador" HeaderText="Código Comprador" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="rol_comprador" HeaderText="Rol Comprador" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_contrato" HeaderText="Cantidad Contrato" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0: ###,###,##0}"></asp:BoundColumn>
                                <%--20170926 rq027-17--%>
                                <asp:BoundColumn DataField="capacidad_transporte" HeaderText="Capacidad Transporte"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0: ###,###,##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="lugar_entrega" HeaderText="Lugar de Entrega" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="modalidad" HeaderText="Modalidad Entrega" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="periodo_entrega" HeaderText="Periodo Entrega" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nit_usuario_final" HeaderText="Nit Usuario Final" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_usuario_final" HeaderText="Nombre Usuario Final"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="mercado_relevante" HeaderText="Mercado Relevante" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="punto_salida" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tipo_demanda" HeaderText="Tipo Demanda" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--20181106--%>
                                <asp:BoundColumn DataField="sector_consumo" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_usuario_final" HeaderText="Cantidad Usuario Final"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,##0}"></asp:BoundColumn>
                                <%--20160711--%>
                                <asp:BoundColumn DataField="carga_extemporanea" HeaderText="Modificación/Extemporánea"></asp:BoundColumn>
                                <%--20160602--%>
                                <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20160602--%>
                                <asp:BoundColumn DataField="desc_fuente" HeaderText="desc fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20181106--%>
                                <asp:BoundColumn DataField="ind_extemporaneo" HeaderText="Contrato extemporáneo Bilateral" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
