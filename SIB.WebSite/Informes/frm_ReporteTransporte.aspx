﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ReporteTransporte.aspx.cs"
    Inherits="Informes_frm_ReporteTransporte" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div>
                    <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
                        width="80%">
                        <tr>
                            <td align="center" class="th1">
                                <asp:Label ID="lblTitulo" runat="server" ForeColor="White"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <br /><br /><br /><br /><br /><br />
                    <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
                        width="80%">
                        <tr>
                            <td class="td1">Reporte
                            </td>
                            <td class="td2">
                                <asp:DropDownList ID="ddlReporte" runat="server" OnSelectedIndexChanged="ddlReporte_SelectedIndexChanged" AutoPostBack="True">
                                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                                    <asp:ListItem Value="2">Promedio de los volúmenes negociados diariamente</asp:ListItem>
                                    <asp:ListItem Value="1">Promedio de los volúmenes negociados durante cada mes</asp:ListItem>                                    
                                    <asp:ListItem Value="3">Promedio de los volúmenes negociados durante el año</asp:ListItem>
                                    <%--<asp:ListItem Value="4">volumen negociado durante cada mes del año</asp:ListItem>--%>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr id ="trfechaI" runat ="server" visible ="false" >
                            <td class="td1">Fecha Inicial
                            </td>
                            <td class="td2">
                                <asp:TextBox ID="TxtFechaIni" runat="server" width="150px" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id ="trfechaF" runat ="server" visible ="false" >
                            <td class="td1">Fecha Final
                            </td>
                            <td class="td2">
                                <asp:TextBox ID="TxtFechaFin" runat="server" width="150px" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id ="trPerI" runat ="server" visible ="false" >
                            <td class="td1">Mes-Año Inicial (aaaa/mm)
                            </td>
                            <td class="td2">
                                <asp:TextBox ID="TxtPerIni" runat="server" ValidationGroup="detalle" MaxLength ="7"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="ftebTxtPerIni" runat="server" TargetControlID="TxtPerIni"
                                    FilterType="Custom, Numbers" ValidChars="/">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr id ="trPerF" runat ="server" visible ="false" >
                            <td class="td1">Mes-Año Final (aaaa/mm)
                            </td>
                            <td class="td2">
                                <asp:TextBox ID="TxtPerFin" runat="server" ValidationGroup="detalle" MaxLength ="7"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="ftebTxtPerFin" runat="server" TargetControlID="TxtPerFin"
                                    FilterType="Custom, Numbers" ValidChars="/">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr id ="trAnoI" runat ="server" visible ="false" >
                            <td class="td1">Año Inicial
                            </td>
                            <td class="td2">
                                <asp:TextBox ID="TxtAñoIni" runat="server" ValidationGroup="detalle" MaxLength ="4"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTBETxtAñoIni" runat="server" TargetControlID="TxtAñoIni"
                                    FilterType="Custom, Numbers">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr id ="trAnoF" runat ="server" visible ="false" >
                            <td class="td1">Año Final
                            </td>
                            <td class="td2">
                                <asp:TextBox ID="TxtAñoFin" runat="server" ValidationGroup="detalle" MaxLength ="4"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="ftebTxtAñoFin" runat="server" TargetControlID="TxtAñoFin"
                                    FilterType="Custom, Numbers">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr id ="trMercado" runat ="server" visible ="false" >
                            <td class="td1">Tipo Mercado
                            </td>
                            <td class="td2">
                                <asp:DropDownList ID="DdlMercado" runat="server">
                                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                                    <asp:ListItem Value="P">Primario</asp:ListItem>
                                    <asp:ListItem Value="S">Secundario</asp:ListItem>
                                    <asp:ListItem Value="O">Otras Transacciones del Mercado Mayorista</asp:ListItem>
                                </asp:DropDownList>

                            </td>
                        </tr>
                        <tr id="trRuta" runat="server" visible ="false" >
                            <td class="td1">Ruta
                            </td>
                            <td class="td2">
                                <asp:DropDownList ID="DdlRuta" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
            id="tblMensaje">
            <tr>
                <td class="th1" colspan="3" align="center">
                    <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                        OnClick="imbConsultar_Click" Height="40" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server">
            <tr>
                <td colspan="3" align="center">
                    <div style="overflow: scroll; width: 1050px; height: 350px;">
                        <asp:DataGrid ID="dtgReporte1" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                            ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                            <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                            <ItemStyle CssClass="td2"></ItemStyle>
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_mercado" HeaderText="Tipo mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_ruta" HeaderText="Cod. Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="no_contratos" HeaderText="No. contratos" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="capacidad" HeaderText="Capacidad (KPCD)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_negociado" HeaderText="Valor Negociado(USD)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pre_prom" HeaderText="Precio Promedio (KPCD/USD)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="capac_prom" HeaderText="Capacidad Promedio (KPCD)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgReporte2" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                            ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                            <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                            <ItemStyle CssClass="td2"></ItemStyle>
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="dia" HeaderText="Día" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_mercado" HeaderText="Tipo mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_ruta" HeaderText="Cod. Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="no_contratos" HeaderText="No. contratos" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="capacidad" HeaderText="Capacidad (KPCD)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_negociado" HeaderText="Valor Negociado(USD)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pre_prom" HeaderText="Precio Promedio (KPCD/USD)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="capac_prom" HeaderText="Capacidad Promedio (KPCD)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgReporte3" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                            ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                            <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                            <ItemStyle CssClass="td2"></ItemStyle>
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_mercado" HeaderText="Tipo mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_ruta" HeaderText="Cod. Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="no_contratos" HeaderText="No. contratos" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="capacidad" HeaderText="Capacidad (KPCD)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_negociado" HeaderText="Valor Negociado(USD)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pre_prom" HeaderText="Precio Promedio (KPCD/USD)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="capac_prom" HeaderText="Capacidad Promedio (KPCD)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                        <%--<asp:DataGrid ID="dtgReporte4" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                            ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                            <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                            <ItemStyle CssClass="td2"></ItemStyle>
                            <Columns>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_mercado" HeaderText="Tipo mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_ruta" HeaderText="Cod. Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="capacidad" HeaderText="Capacidad (KPCD)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="pre_prom" HeaderText="Precio Promedio (KPCD/USD)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>--%>
                    </div>
                </td>
            </tr>
        </table>
</asp:Content>