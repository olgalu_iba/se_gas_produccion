﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;
using System.Text;
using Segas.Web.Elements;

public partial class Informes_frm_AuditoriaOperaciones : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Auditoria de Operaciones";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();

        //Se agrega el comportamiento del botón
        buttons.ExportarExcelOnclick += ImgExcel_Click;
        buttons.FiltrarOnclick += imbConsultar_Click;

        if (IsPostBack) return;
        TxtFechaIni.Text = DateTime.Now.Date.ToString("yyyy/MM/dd");
        TxtFechaFin.Text = DateTime.Now.Date.ToString("yyyy/MM/dd");

        //Botones
        EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Excel};
        buttons.Inicializar(botones: botones);
    }
    /// <summary>

    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, EventArgs e)
    {
        bool Error = false ;

        DateTime ldFecha;
        try
        {
            ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "Formato Inválido en el Campo Fecha Inicial. <br>");
            Error = true;
        }
        try
        {
            ldFecha = Convert.ToDateTime(TxtFechaFin.Text);
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "Formato Inválido en el Campo Fecha Final. <br>");
            Error = true;
     
        }
        if (!Error)
        {
            try
            {
                decimal ldOperacion = 0;
                decimal ldConsecutivo = 0;
                if (txtOperacion.Text != "")
                    ldOperacion = Convert.ToDecimal(txtOperacion.Text);
                if (txtConsecutivo.Text != "")
                    ldConsecutivo = Convert.ToDecimal(txtConsecutivo.Text);


                lConexion = new clConexion(goInfo);
                lConexion.AbrirInforme();
                SqlCommand lComando = new SqlCommand();
                DataSet lds = new DataSet();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_RptAuditoriaOperaciones";
                lComando.Parameters.Add("@P_fecha_ini", SqlDbType.VarChar).Value = TxtFechaIni.Text;
                lComando.Parameters.Add("@P_fecha_fin", SqlDbType.VarChar).Value = TxtFechaFin.Text;
                lComando.Parameters.Add("@P_usuario_evento", SqlDbType.VarChar).Value = txtUsuario.Text;
                if (ddlEvento.SelectedValue != "")
                    lComando.Parameters.Add("@P_evento", SqlDbType.VarChar).Value = ddlEvento.SelectedItem.ToString();
                else
                    lComando.Parameters.Add("@P_evento", SqlDbType.VarChar).Value = "";
                lComando.Parameters.Add("@P_numero_operacion", SqlDbType.Decimal).Value = ldOperacion;
                lComando.Parameters.Add("@P_consecutivo_interno", SqlDbType.Decimal).Value = ldConsecutivo;
                lComando.CommandTimeout = 3600;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                gvReporte.DataSource = lds;
                gvReporte.DataBind();
                tblGrilla.Visible = true;
                buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);

            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pude Generar el Informe.!");
            }
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, EventArgs e)
    {
        try
        {
            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            gvReporte.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);

            lform.Controls.Add(gvReporte);
            lpagina.RenderControl(lhtw);
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=data.xls");
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());
            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "No se Pude Generar el Excel.!" + ex.Message.ToString());
        }
    }

}
