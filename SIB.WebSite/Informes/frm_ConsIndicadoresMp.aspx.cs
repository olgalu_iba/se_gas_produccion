﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Informes
{
    public partial class frm_ConsIndicadoresMp : Page
    {
        static InfoSessionVO goInfo;
        static string lsTitulo = "Consulta Indicadores CREG";
        static clConexion lConexion;
        clConexion lConexion1;
        SqlDataReader lLector;
        DataSet lds = new DataSet();
        SqlDataAdapter lsqldata = new SqlDataAdapter();
        private static int PageSize = 25;
        static string lsTipoPerfil;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo.ToString();
            lConexion = new clConexion(goInfo);
            lConexion1 = new clConexion(goInfo);
            lsTipoPerfil = Session["tipoPerfil"].ToString();
            //Se agrega el comportamiento del botón
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.DetalleOnclick += btnDetalle_Click;
            if (IsPostBack) return;
            //Titulo
            Master.Titulo = "Informes";
            lConexion1.Abrir();
            LlenarControles(lConexion1.gObjConexion, ddlTipoIndica, "m_indicadores_creg ind", " exists (select 1 from m_indicador_rol rol where rol.estado = 'A' And rol.sistema='S' And rol.codigo_indicador = ind.codigo_indicador And ind.tipo_mercado='P' And rol.rol = " + goInfo.codigo_grupo_usuario + ") order by ind.codigo_indicador", 0, 1);
            LlenarControles(lConexion1.gObjConexion, ddlMesIni, "m_mes", " 1=1 order by mes", 0, 1);
            LlenarControles(lConexion1.gObjConexion, ddlMesFinal, "m_mes", " 1=1 order by mes", 0, 1);
            LlenarControles(lConexion1.gObjConexion, ddlMesIndica, "m_mes", " 1=1 order by mes", 0, 1);
            lConexion1.Cerrar();
            ddlTipoIndica.Focus();
            if (!IsPostBack)
            {
                ddlTipoVista.Items.Add("Seleccione");
            }
            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Detalle };
            buttons.Inicializar(botones: botones);
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            var lItem = new ListItem { Value = "0", Text = "Seleccione" };
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                }
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlTipoIndica_SelectedIndexChanged(object sender, EventArgs e)
        {
            string lsAño = "0";
            if (TxtAnoIndica.Text != "")
                lsAño = TxtAnoIndica.Text;
            CodInd.Value = ddlTipoIndica.SelectedValue;
            MesInd.Value = ddlMesIndica.SelectedValue;
            AnoInd.Value = lsAño;
            tipoVista.Value = ddlTipoVista.SelectedValue;

            btnDetalle.Attributes.Add("onClick", "javascript:Popup=window.open('frm_ConsIndicadoresMpDet.aspx?CodInd=" + ddlTipoIndica.SelectedValue + "&AñoInd=" + lsAño + "&MesInd=" + ddlMesIndica.SelectedValue + "&tipoVista=" + ddlTipoVista.SelectedValue + "','Detalle','width=450,height=450,left=350,top=150,status=no,location=0,menubar=no,toolbar=no,resizable=yes');");
            InactivarGrilla();
            //20170705 rq025 indicar creg fase III
            ddlTipoVista.Items.Clear();
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlTipoVista, "m_indicador_vista ind", " ind.codigo_indicador = " + ddlTipoIndica.SelectedValue + " and ind.estado ='A' and exists (select 1 from m_indicador_rol rol where rol.estado = 'A' And rol.sistema='S' And rol.codigo_indicador = ind.codigo_indicador and rol.vista_desagregacion = ind.vista_desagregacion And rol.rol = " + goInfo.codigo_grupo_usuario + ")", 2, 3);
            //seleccina si maneja mes de publicacion
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_indicadores_creg ind", " ind.codigo_indicador = " + ddlTipoIndica.SelectedValue);
            if (lLector.HasRows)
            {
                lLector.Read();
                if (lLector["tipo_publicacion"].ToString() == "M")
                {
                    tdMes01.Visible = true;
                    tdMes02.Visible = true;
                }
                else
                {
                    tdMes01.Visible = false;
                    tdMes02.Visible = false;
                }
            }
            else
            {
                tdMes01.Visible = false;
                tdMes02.Visible = false;
            }
            ddlMesIndica.SelectedValue = "0";

            lConexion.Cerrar();

        }

        /// <summary>
        /// Nombre: InactivarGrilla
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected void InactivarGrilla()
        {
            dtgIndicador1_N.Visible = false;
            dtgIndicador1_P.Visible = false;
            dtgIndicador1_F.Visible = false;
            dtgIndicador2_N.Visible = false;
            dtgIndicador2_P.Visible = false;
            dtgIndicador2_F.Visible = false;
            dtgIndicador3_N.Visible = false;
            dtgIndicador3_P.Visible = false;
            dtgIndicador3_F.Visible = false;
            dtgIndicador4_N.Visible = false;
            dtgIndicador4_P.Visible = false;
            dtgIndicador4_F.Visible = false;
            dtgIndicador5_N.Visible = false;
            dtgIndicador5_P.Visible = false;
            dtgIndicador5_F.Visible = false;
            dtgIndicador6_N.Visible = false;
            dtgIndicador6_P.Visible = false;
            dtgIndicador6_F.Visible = false;
            dtgIndicador7_N.Visible = false;
            dtgIndicador7_P.Visible = false;
            dtgIndicador8_N.Visible = false;
            dtgIndicador8_F.Visible = false;
            dtgIndicador9_N.Visible = false;
            dtgIndicador9_F.Visible = false;
            dtgIndicador10_N.Visible = false;
            dtgIndicador10_F.Visible = false;
            dtgIndicador10_P.Visible = false;
            dtgIndicador10_S.Visible = false;
            dtgIndicador11_N.Visible = false;
            dtgIndicador11_F.Visible = false;
            dtgIndicador11_P.Visible = false;
            dtgIndicador11_S.Visible = false;
            dtgIndicador12_N.Visible = false;
            dtgIndicador12_P.Visible = false;
            dtgIndicador13_N.Visible = false;
            dtgIndicador13_P.Visible = false;
            dtgIndicador14.Visible = false;
            dtgIndicador15.Visible = false;
            dtgIndicador16.Visible = false;
            dtgIndicador17_N.Visible = false;
            dtgIndicador17_P.Visible = false;
            dtgIndicador18_N.Visible = false;
            dtgIndicador18_P.Visible = false;
            dtgIndicador19.Visible = false;
            dtgIndicador20.Visible = false;
            dtgIndicador21_F.Visible = false;
            dtgIndicador21_P.Visible = false;
            dtgIndicador21_A.Visible = false;
            dtgIndicador22.Visible = false;
            dtgIndicador23_N.Visible = false;
            dtgIndicador23_F.Visible = false;
            dtgIndicador23_P.Visible = false;
            dtgIndicador23_C.Visible = false;
            dtgIndicador23_D.Visible = false;
            dtgIndicador23_S.Visible = false;
            dtgIndicador23_T.Visible = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDetalle_Click(object sender, EventArgs e)
        {
            mdlCrearRegistroLabel.InnerHtml = $"Indicador: {ddlTipoIndica.SelectedItem.Text}";
            var lsAño = "0";
            if (TxtAnoIndica.Text != "")
                lsAño = TxtAnoIndica.Text;
            CodInd.Value = ddlTipoIndica.SelectedValue;
            MesInd.Value = ddlMesIndica.SelectedValue;
            AnoInd.Value = lsAño;
            tipoVista.Value = ddlTipoVista.SelectedValue;

            try
            {
                goInfo = (InfoSessionVO)Session["infoSession"];
                if (goInfo == null) Response.Redirect("../index.aspx");

                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_indicadores_creg ind left join t_indicador_pub_obs obs on (obs.codigo_indicador = " + CodInd.Value + " and obs.año_proceso = " + AnoInd.Value + " and obs.mes_proceso= " + MesInd.Value + " and obs.tipo_desagregacion= '" + tipoVista.Value + "')", " ind.codigo_indicador = " + CodInd.Value);

                if (lLector.HasRows)
                {
                    lLector.Read();
                    lblTitulo.Text = "Indicador: " + lLector["descripcion"];
                    lblVariable.Text = "<ul>";
                    if (lLector["numerador_1"].ToString() != "")
                        lblVariable.Text += "<li>" + lLector["numerador_1"] + "</li>";
                    if (lLector["numerador_2"].ToString() != "")
                        lblVariable.Text += "<li>" + lLector["numerador_2"] + "</li>";
                    if (lLector["denominador_1"].ToString() != "")
                        lblVariable.Text += "<li>" + lLector["denominador_1"] + "</li>";
                    if (lLector["denominador_2"].ToString() != "")
                        lblVariable.Text += "<li>" + lLector["denominador_2"] + "</li>";
                    lblVariable.Text += "</ul>";
                    lblPeriodo.Text = lLector["periodicidad"].ToString();
                    imgIndicador.ImageUrl = "../Images/" + lLector["ruta_archivo_formula"].ToString();
                    lblObs.Text = lLector["observacion"].ToString();
                    lLector.Close();
                    lLector.Dispose();
                }

                lConexion.Cerrar();
                //Se abre el modal
                Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            lblMensaje.Text = "";
            InactivarGrilla();
            if (ddlTipoIndica.SelectedValue != "0" && ddlTipoVista.SelectedValue != "0" && TxtAnoIni.Text.Trim().Length > 0 && TxtAnoFin.Text.Trim().Length > 0 && TxtAnoIndica.Text.Trim().Length > 0 && (tdMes01.Visible == false || ddlMesIndica.SelectedValue != "0"))
            {
                if ((ddlMesIni.SelectedValue == "0" || ddlMesFinal.SelectedValue == "0"))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Debe Seleccionar los Filtros de búsqueda Obligatorios!" + "');", true);
                    return;
                }
                lblMensaje.Text = Validaciones();
                if (lblMensaje.Text == "")
                {
                    mostrar_datos();
                }
                if (lblMensaje.Text != "")
                {
                    Toastr.Warning(this, lblMensaje.Text);
                    lblMensaje.Text = "";
                }
            }
            else
                Toastr.Warning(this, "Debe Seleccionar los Filtros de búsqueda Obligatorios!");


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlTipoVista_SelectedIndexChanged(object sender, EventArgs e)
        {
            string lsAño = "0";
            if (TxtAnoIndica.Text != "")
                lsAño = TxtAnoIndica.Text;
            btnDetalle.Attributes.Add("onClick", "javascript:Popup=window.open('frm_ConsIndicadoresMpDet.aspx?CodInd=" + ddlTipoIndica.SelectedValue + "&AñoInd=" + lsAño + "&MesInd=" + ddlMesIndica.SelectedValue + "&tipoVista=" + ddlTipoVista.SelectedValue + "','Detalle','width=450,height=450,left=350,top=150,status=no,location=0,menubar=no,toolbar=no,resizable=yes');");

            InactivarGrilla();
            if (ddlTipoIndica.SelectedValue != "0" && ddlTipoVista.SelectedValue != "0" && TxtAnoIni.Text.Trim().Length > 0 && TxtAnoFin.Text.Trim().Length > 0 && TxtAnoIndica.Text.Trim().Length > 0 && (tdMes01.Visible == false || ddlMesIndica.SelectedValue != "0"))
            {
                lblMensaje.Text = Validaciones();
                if (lblMensaje.Text == "")
                {
                    mostrar_datos();
                }

            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
            TxtAnoIni.Focus();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlMesIni_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTipoIndica.SelectedValue != "0" && ddlTipoVista.SelectedValue != "0" && TxtAnoIni.Text.Trim().Length > 0 && TxtAnoFin.Text.Trim().Length > 0 && TxtAnoIndica.Text.Trim().Length > 0 && (tdMes01.Visible == false || ddlMesIndica.SelectedValue != "0"))
            {
                lblMensaje.Text = Validaciones();
                if (lblMensaje.Text == "")
                {
                    mostrar_datos();
                }

            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
            TxtAnoFin.Focus();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlMesFinal_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblMensaje.Text = Validaciones();
            if (ddlTipoIndica.SelectedValue != "0" && ddlTipoVista.SelectedValue != "0" && TxtAnoIni.Text.Trim().Length > 0 && TxtAnoFin.Text.Trim().Length > 0 && TxtAnoIndica.Text.Trim().Length > 0 && (tdMes01.Visible == false || ddlMesIndica.SelectedValue != "0"))
            {
                lblMensaje.Text = Validaciones();
                if (lblMensaje.Text == "")
                {
                    mostrar_datos();
                }

            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }

            ddlTipoIndica.Focus();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlMesIndica_SelectedIndexChanged(object sender, EventArgs e)
        {
            string lsAño = "0";
            if (TxtAnoIndica.Text != "")
                lsAño = TxtAnoIndica.Text;
            btnDetalle.Attributes.Add("onClick", "javascript:Popup=window.open('frm_ConsIndicadoresMpDet.aspx?CodInd=" + ddlTipoIndica.SelectedValue + "&AñoInd=" + lsAño + "&MesInd=" + ddlMesIndica.SelectedValue + "&tipoVista=" + ddlTipoVista.SelectedValue + "','Detalle','width=450,height=450,left=350,top=150,status=no,location=0,menubar=no,toolbar=no,resizable=yes');");

            if (ddlTipoIndica.SelectedValue != "0" && ddlTipoVista.SelectedValue != "0" && TxtAnoIni.Text.Trim().Length > 0 && TxtAnoFin.Text.Trim().Length > 0 && TxtAnoIndica.Text.Trim().Length > 0 && (tdMes01.Visible == false || ddlMesIndica.SelectedValue != "0"))
            {
                lblMensaje.Text = Validaciones();
                if (lblMensaje.Text == "")
                {
                    mostrar_datos();
                }

            }
            if (lblMensaje.Text != "")
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
            TxtAnoIni.Focus();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string lsNombreArchivo = goInfo.Usuario.ToString() + "InfIndicador" + DateTime.Now + ".xls";
            string lstitulo_informe = "Consulta de " + ddlTipoIndica.SelectedItem.ToString() + " - Vista: " + ddlTipoVista.SelectedItem.ToString();
            string lsTituloParametros = "";
            try
            {

                decimal ldCapacidad = 0;
                StringBuilder lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                if (ddlTipoIndica.SelectedValue == "1")
                {
                    switch (ddlTipoVista.SelectedValue)
                    {
                        case "N":
                            dtgIndicador1_N.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador1_N);
                            break;
                        case "F":
                            dtgIndicador1_F.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador1_F);
                            break;
                        case "P":
                            dtgIndicador1_P.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador1_P);
                            break;
                    }
                }
                if (ddlTipoIndica.SelectedValue == "2")
                {
                    switch (ddlTipoVista.SelectedValue)
                    {
                        case "N":
                            dtgIndicador2_N.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador2_N);
                            break;
                        case "F":
                            dtgIndicador2_F.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador2_F);
                            break;
                        case "P":
                            dtgIndicador2_P.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador2_P);
                            break;
                    }
                }
                if (ddlTipoIndica.SelectedValue == "3")
                {
                    switch (ddlTipoVista.SelectedValue)
                    {
                        case "N":
                            dtgIndicador3_N.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador3_N);
                            break;
                        case "P":
                            dtgIndicador3_P.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador3_P);
                            break;
                        case "F":
                            dtgIndicador3_F.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador3_F);
                            break;
                    }
                }
                if (ddlTipoIndica.SelectedValue == "4")
                {
                    switch (ddlTipoVista.SelectedValue)
                    {
                        case "N":
                            dtgIndicador4_N.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador4_N);
                            break;
                        case "P":
                            dtgIndicador4_P.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador4_P);
                            break;
                        case "F":
                            dtgIndicador4_F.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador4_F);
                            break;
                    }
                }
                if (ddlTipoIndica.SelectedValue == "5")
                {
                    switch (ddlTipoVista.SelectedValue)
                    {
                        case "N":
                            dtgIndicador5_N.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador5_N);
                            break;
                        case "P":
                            dtgIndicador5_P.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador5_P);
                            break;
                        case "F":
                            dtgIndicador5_F.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador5_F);
                            break;
                    }
                }
                if (ddlTipoIndica.SelectedValue == "6")
                {
                    switch (ddlTipoVista.SelectedValue)
                    {
                        case "N":
                            dtgIndicador6_N.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador6_N);
                            break;
                        case "P":
                            dtgIndicador6_P.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador6_P);
                            break;
                        case "F":
                            dtgIndicador6_F.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador6_F);
                            break;
                    }
                }
                if (ddlTipoIndica.SelectedValue == "7")
                {
                    switch (ddlTipoVista.SelectedValue)
                    {
                        case "N":
                            dtgIndicador7_N.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador7_N);
                            break;
                        case "P":
                            dtgIndicador7_P.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador7_P);
                            break;
                    }
                }
                if (ddlTipoIndica.SelectedValue == "8")
                {
                    switch (ddlTipoVista.SelectedValue)
                    {
                        case "N":
                            dtgIndicador8_N.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador8_N);
                            break;
                        case "F":
                            dtgIndicador8_F.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador8_F);
                            break;
                    }
                }
                if (ddlTipoIndica.SelectedValue == "9")
                {
                    switch (ddlTipoVista.SelectedValue)
                    {
                        case "N":
                            dtgIndicador9_N.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador9_N);
                            break;
                        case "F":
                            dtgIndicador9_F.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador9_F);
                            break;
                    }
                }
                if (ddlTipoIndica.SelectedValue == "10")
                {
                    switch (ddlTipoVista.SelectedValue)
                    {
                        case "N":
                            dtgIndicador10_N.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador10_N);
                            break;
                        case "F":
                            dtgIndicador10_F.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador10_F);
                            break;
                        case "P":
                            dtgIndicador10_P.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador10_P);
                            break;
                        case "S":
                            dtgIndicador10_S.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador10_S);
                            break;
                    }
                }
                if (ddlTipoIndica.SelectedValue == "11")
                {
                    switch (ddlTipoVista.SelectedValue)
                    {
                        case "N":
                            dtgIndicador11_N.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador11_N);
                            break;
                        case "F":
                            dtgIndicador11_F.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador11_F);
                            break;
                        case "P":
                            dtgIndicador11_P.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador11_P);
                            break;
                        case "S":
                            dtgIndicador11_S.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador11_S);
                            break;
                    }
                }
                if (ddlTipoIndica.SelectedValue == "12")
                {
                    switch (ddlTipoVista.SelectedValue)
                    {
                        case "N":
                            dtgIndicador12_N.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador12_N);
                            break;
                        case "P":
                            dtgIndicador12_P.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador12_P);
                            break;
                    }
                }
                if (ddlTipoIndica.SelectedValue == "13")
                {
                    switch (ddlTipoVista.SelectedValue)
                    {
                        case "N":
                            dtgIndicador13_N.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador13_N);
                            break;
                        case "P":
                            dtgIndicador13_P.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador13_P);
                            break;
                    }
                }
                if (ddlTipoIndica.SelectedValue == "14")
                {
                    dtgIndicador14.EnableViewState = false;
                    lform.Controls.Add(dtgIndicador14);
                }
                if (ddlTipoIndica.SelectedValue == "15")
                {
                    dtgIndicador15.EnableViewState = false;
                    lform.Controls.Add(dtgIndicador15);
                }
                if (ddlTipoIndica.SelectedValue == "16")
                {
                    dtgIndicador16.EnableViewState = false;
                    lform.Controls.Add(dtgIndicador16);
                }
                if (ddlTipoIndica.SelectedValue == "17")
                {
                    switch (ddlTipoVista.SelectedValue)
                    {
                        case "N":
                            dtgIndicador17_N.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador17_N);
                            break;
                        case "P":
                            dtgIndicador17_P.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador17_P);
                            break;
                    }
                }
                if (ddlTipoIndica.SelectedValue == "18")
                {
                    switch (ddlTipoVista.SelectedValue)
                    {
                        case "N":
                            dtgIndicador18_N.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador18_N);
                            break;
                        case "P":
                            dtgIndicador18_P.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador18_P);
                            break;
                    }
                }
                if (ddlTipoIndica.SelectedValue == "19")
                {
                    dtgIndicador19.EnableViewState = false;
                    lform.Controls.Add(dtgIndicador19);
                }
                if (ddlTipoIndica.SelectedValue == "20")
                {
                    dtgIndicador20.EnableViewState = false;
                    lform.Controls.Add(dtgIndicador20);
                }
                if (ddlTipoIndica.SelectedValue == "21")
                {
                    switch (ddlTipoVista.SelectedValue)
                    {
                        case "F":
                            dtgIndicador21_F.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador21_F);
                            break;
                        case "P":
                            dtgIndicador21_P.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador21_P);
                            break;
                        case "A":
                            dtgIndicador21_A.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador21_A);
                            break;
                    }
                }
                if (ddlTipoIndica.SelectedValue == "22")
                {
                    dtgIndicador22.EnableViewState = false;
                    lform.Controls.Add(dtgIndicador22);
                }
                if (ddlTipoIndica.SelectedValue == "23")
                {
                    switch (ddlTipoVista.SelectedValue)
                    {
                        case "N":
                            dtgIndicador23_N.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador23_N);
                            break;
                        case "F":
                            dtgIndicador23_F.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador23_F);
                            break;
                        case "P":
                            dtgIndicador23_P.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador23_P);
                            break;
                        case "C":
                            dtgIndicador23_C.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador23_C);
                            break;
                        case "D":
                            dtgIndicador23_D.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador23_D);
                            break;
                        case "S":
                            dtgIndicador23_S.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador23_S);
                            break;
                        case "T":
                            dtgIndicador23_T.EnableViewState = false;
                            lform.Controls.Add(dtgIndicador23_T);
                            break;
                    }
                }
                lpagina.RenderControl(lhtw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = System.Text.Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parámetros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> El cálculo del indicador se realiza con la siguiente fórmula: </font></th><td></td></tr></table><br>");
                switch (ddlTipoIndica.SelectedValue)
                {
                    case "1":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> PTDV </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> PP </font></th><td></td></tr></table>");
                        break;
                    case "2":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> PTDVF + CIDVF </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> PTDV + CIDV </font></th><td></td></tr></table>");
                        break;
                    case "3":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> PTDVF </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> PP </font></th><td></td></tr></table>");
                        break;
                    case "4":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OCF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> PTDV + CIDV </font></th><td></td></tr></table>");
                        break;
                    case "5":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OCF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> PTDVF + CIDVF </font></th><td></td></tr></table>");
                        break;
                    case "6":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;OCF&nbsp;&nbsp;</font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> PP </font></th><td></td></tr></table>");
                        break;
                    case "7":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;CF&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> C </font></th><td></td></tr></table>");
                        break;
                    case "8":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> PTDVF + CIDVF </font></th><td></td></tr></table>");
                        break;
                    case "9":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;CF&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> OCF </font></th><td></td></tr></table>");
                        break;
                    case "10":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> PTDVF + CIDVF </font></th><td></td></tr></table>");
                        break;
                    case "11":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;CF&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> OCF </font></th><td></td></tr></table>");
                        break;
                    case "12":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;CF&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> C </font></th><td></td></tr></table>");
                        break;
                    case "13":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;CF&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> C </font></th><td></td></tr></table>");
                        break;
                    case "14":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;CF&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> CF Tot </font></th><td></td></tr></table>");
                        break;
                    case "15":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;CF&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> CT </font></th><td></td></tr></table>");
                        break;
                    case "16":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;CT&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> CMMP </font></th><td></td></tr></table>");
                        break;
                    case "17":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;C&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> C tot </font></th><td></td></tr></table>");
                        break;
                    case "18":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;&nbsp;C&nbsp;&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> C tot </font></th><td></td></tr></table>");
                        break;
                    case "19":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;CT&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> CMMP </font></th><td></td></tr></table>");
                        break;
                    case "20":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;CT&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> CMMP </font></th><td></td></tr></table>");
                        break;
                    case "21":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;C&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> OCF </font></th><td></td></tr></table>");
                        break;
                    case "22":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;&nbsp;&nbsp;CT&nbsp;&nbsp;&nbsp; </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> CMMP </font></th><td></td></tr></table>");
                        break;
                    case "23":
                        Response.Write("<table><tr><th colspan='2' align='center'><u><font face=Arial size=2> &nbsp;Cnt * Pre&nbsp; </font></u></th><td></td></tr></table>");
                        Response.Write("<table><tr><th colspan='2' align='center'><font face=Arial size=2> Cnt </font></th><td></td></tr></table>");
                        break;
                }
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());

                Response.End();
                lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Excel.!" + ex.Message.ToString();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TxtAnoFin_TextChanged(object sender, EventArgs e)
        {
            if (ddlTipoIndica.SelectedValue != "0" && ddlTipoVista.SelectedValue != "0" && TxtAnoIni.Text.Trim().Length > 0 && TxtAnoFin.Text.Trim().Length > 0 && TxtAnoIndica.Text.Trim().Length > 0 && (tdMes01.Visible == false || ddlMesIndica.SelectedValue != "0"))
            {


                if (Validaciones() == "")
                {
                    mostrar_datos();
                }
                else
                { Toastr.Warning(this, Validaciones()); }
            }
            ddlMesFinal.Focus();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TxtAnoIni_TextChanged(object sender, EventArgs e)
        {
            if (ddlTipoIndica.SelectedValue != "0" && ddlTipoVista.SelectedValue != "0" && TxtAnoIni.Text.Trim().Length > 0 && TxtAnoFin.Text.Trim().Length > 0 && TxtAnoIndica.Text.Trim().Length > 0 && (tdMes01.Visible == false || ddlMesIndica.SelectedValue != "0"))
            {


                if (Validaciones() == "")
                {
                    mostrar_datos();
                }
                else
                {
                    Toastr.Warning(this, Validaciones());
                }
            }
            ddlMesIni.Focus();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected string Validaciones()
        {
            string lsError = "";
            int liValor = 0;
            int liPerI = 0;
            int liPerF = 0;
            try
            {
                if (TxtAnoIndica.Text.Trim().Length > 0)
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtAnoIndica.Text);
                    }
                    catch (Exception ex)
                    {
                        lsError += "Formato Inválido en el Campo Año Indicador. \n";
                    }
                }
                if (TxtAnoIni.Text.Trim().Length > 0)
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtAnoIni.Text);
                    }
                    catch (Exception ex)
                    {
                        lsError += "Formato Inválido en el Campo Año Inicial. \n";
                    }
                }
                if (TxtAnoFin.Text.Trim().Length > 0)
                {
                    try
                    {
                        liValor = Convert.ToInt32(TxtAnoFin.Text);
                    }
                    catch (Exception ex)
                    {
                        lsError += "Formato Inválido en el Campo Año Final. \n";
                    }
                }
                if (lsError == "")
                {
                    liPerI = Convert.ToInt32(TxtAnoIni.Text) * 100 + Convert.ToInt32(ddlMesIni.SelectedValue);
                    liPerF = Convert.ToInt32(TxtAnoFin.Text) * 100 + Convert.ToInt32(ddlMesFinal.SelectedValue);
                    if (liPerI > liPerF)
                        lsError += "El Periodo Inicial debe ser menor que el periodo Final. \n";
                }
                return lsError;
            }
            catch (Exception ex)
            {
                return "Error en las validaciones " + ex.Message.ToString();


            }


        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected void mostrar_datos()
        {
            try
            {
                InactivarGrilla();
                lds.Clear();
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetIndCregMP_Dat";
                lComando.Parameters.Add("@P_codigo_indicador", SqlDbType.Int).Value = ddlTipoIndica.SelectedValue;
                lComando.Parameters.Add("@P_año_consulta", SqlDbType.Char).Value = TxtAnoIndica.Text;
                lComando.Parameters.Add("@P_mes_consulta", SqlDbType.Char).Value = ddlMesIndica.SelectedValue;
                lComando.Parameters.Add("@P_tipo_desagregacion", SqlDbType.VarChar).Value = ddlTipoVista.SelectedValue;
                lComando.Parameters.Add("@P_año_ini", SqlDbType.VarChar).Value = TxtAnoIni.Text;
                lComando.Parameters.Add("@P_mes_ini", SqlDbType.VarChar).Value = ddlMesIni.SelectedValue;
                lComando.Parameters.Add("@P_año_fin", SqlDbType.VarChar).Value = TxtAnoFin.Text;
                lComando.Parameters.Add("@P_mes_fin", SqlDbType.VarChar).Value = ddlMesFinal.SelectedValue;
                lComando.Parameters.Add("@P_codigo_operador", SqlDbType.VarChar).Value = "0";
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);

                //else
                {
                    if (ddlTipoIndica.SelectedValue == "1")
                    {
                        switch (ddlTipoVista.SelectedValue)
                        {
                            case "N":
                                dtgIndicador1_N.DataSource = lds;
                                dtgIndicador1_N.DataBind();
                                dtgIndicador1_N.Visible = true;
                                if (dtgIndicador1_N.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");

                                }
                                break;
                            case "F":
                                dtgIndicador1_F.DataSource = lds;
                                dtgIndicador1_F.DataBind();
                                dtgIndicador1_F.Visible = true;
                                if (dtgIndicador1_F.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "P":
                                dtgIndicador1_P.DataSource = lds;
                                dtgIndicador1_P.DataBind();
                                dtgIndicador1_P.Visible = true;
                                if (dtgIndicador1_P.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "2")
                    {
                        switch (ddlTipoVista.SelectedValue)
                        {
                            case "N":
                                dtgIndicador2_N.DataSource = lds;
                                dtgIndicador2_N.DataBind();
                                dtgIndicador2_N.Visible = true;
                                if (dtgIndicador2_N.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "P":
                                dtgIndicador2_P.DataSource = lds;
                                dtgIndicador2_P.DataBind();
                                dtgIndicador2_P.Visible = true;
                                if (dtgIndicador2_P.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "F":
                                dtgIndicador2_F.DataSource = lds;
                                dtgIndicador2_F.DataBind();
                                dtgIndicador2_F.Visible = true;
                                if (dtgIndicador2_F.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "3")
                    {
                        switch (ddlTipoVista.SelectedValue)
                        {
                            case "N":
                                dtgIndicador3_N.DataSource = lds;
                                dtgIndicador3_N.DataBind();
                                dtgIndicador3_N.Visible = true;
                                if (dtgIndicador3_N.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "P":
                                dtgIndicador3_P.DataSource = lds;
                                dtgIndicador3_P.DataBind();
                                dtgIndicador3_P.Visible = true;
                                if (dtgIndicador3_P.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "F":
                                dtgIndicador3_F.DataSource = lds;
                                dtgIndicador3_F.DataBind();
                                dtgIndicador3_F.Visible = true;
                                if (dtgIndicador3_F.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "4")
                    {
                        switch (ddlTipoVista.SelectedValue)
                        {
                            case "N":
                                dtgIndicador4_N.DataSource = lds;
                                dtgIndicador4_N.DataBind();
                                dtgIndicador4_N.Visible = true;
                                if (dtgIndicador4_N.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "P":
                                dtgIndicador4_P.DataSource = lds;
                                dtgIndicador4_P.DataBind();
                                dtgIndicador4_P.Visible = true;
                                if (dtgIndicador4_P.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "F":
                                dtgIndicador4_F.DataSource = lds;
                                dtgIndicador4_F.DataBind();
                                dtgIndicador4_F.Visible = true;
                                if (dtgIndicador4_F.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "5")
                    {
                        switch (ddlTipoVista.SelectedValue)
                        {
                            case "N":
                                dtgIndicador5_N.DataSource = lds;
                                dtgIndicador5_N.DataBind();
                                dtgIndicador5_N.Visible = true;
                                if (dtgIndicador5_N.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "P":
                                dtgIndicador5_P.DataSource = lds;
                                dtgIndicador5_P.DataBind();
                                dtgIndicador5_P.Visible = true;
                                if (dtgIndicador5_P.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "F":
                                dtgIndicador5_F.DataSource = lds;
                                dtgIndicador5_F.DataBind();
                                dtgIndicador5_F.Visible = true;
                                if (dtgIndicador5_F.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "6")
                    {
                        switch (ddlTipoVista.SelectedValue)
                        {
                            case "N":
                                dtgIndicador6_N.DataSource = lds;
                                dtgIndicador6_N.DataBind();
                                dtgIndicador6_N.Visible = true;
                                if (dtgIndicador6_N.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "P":
                                dtgIndicador6_P.DataSource = lds;
                                dtgIndicador6_P.DataBind();
                                dtgIndicador6_P.Visible = true;
                                if (dtgIndicador6_P.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "F":
                                dtgIndicador6_F.DataSource = lds;
                                dtgIndicador6_F.DataBind();
                                dtgIndicador6_F.Visible = true;
                                if (dtgIndicador6_F.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "7")
                    {
                        switch (ddlTipoVista.SelectedValue)
                        {
                            case "N":
                                dtgIndicador7_N.DataSource = lds;
                                dtgIndicador7_N.DataBind();
                                dtgIndicador7_N.Visible = true;
                                if (dtgIndicador7_N.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "P":
                                dtgIndicador7_P.DataSource = lds;
                                dtgIndicador7_P.DataBind();
                                dtgIndicador7_P.Visible = true;
                                if (dtgIndicador7_P.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "8")
                    {
                        switch (ddlTipoVista.SelectedValue)
                        {
                            case "N":
                                dtgIndicador8_N.DataSource = lds;
                                dtgIndicador8_N.DataBind();
                                dtgIndicador8_N.Visible = true;
                                if (dtgIndicador8_N.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "F":
                                dtgIndicador8_F.DataSource = lds;
                                dtgIndicador8_F.DataBind();
                                dtgIndicador8_F.Visible = true;
                                if (dtgIndicador8_F.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "9")
                    {
                        switch (ddlTipoVista.SelectedValue)
                        {
                            case "N":
                                dtgIndicador9_N.DataSource = lds;
                                dtgIndicador9_N.DataBind();
                                dtgIndicador9_N.Visible = true;
                                if (dtgIndicador9_N.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "F":
                                dtgIndicador9_F.DataSource = lds;
                                dtgIndicador9_F.DataBind();
                                dtgIndicador9_F.Visible = true;
                                if (dtgIndicador9_F.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "10")
                    {
                        switch (ddlTipoVista.SelectedValue)
                        {
                            case "N":
                                dtgIndicador10_N.DataSource = lds;
                                dtgIndicador10_N.DataBind();
                                dtgIndicador10_N.Visible = true;
                                if (dtgIndicador10_N.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "F":
                                dtgIndicador10_F.DataSource = lds;
                                dtgIndicador10_F.DataBind();
                                dtgIndicador10_F.Visible = true;
                                if (dtgIndicador10_F.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "P":
                                dtgIndicador10_P.DataSource = lds;
                                dtgIndicador10_P.DataBind();
                                dtgIndicador10_P.Visible = true;
                                if (dtgIndicador10_P.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "S":
                                dtgIndicador10_S.DataSource = lds;
                                dtgIndicador10_S.DataBind();
                                dtgIndicador10_S.Visible = true;
                                if (dtgIndicador10_S.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "11")
                    {
                        switch (ddlTipoVista.SelectedValue)
                        {
                            case "N":
                                dtgIndicador11_N.DataSource = lds;
                                dtgIndicador11_N.DataBind();
                                dtgIndicador11_N.Visible = true;
                                if (dtgIndicador11_N.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "F":
                                dtgIndicador11_F.DataSource = lds;
                                dtgIndicador11_F.DataBind();
                                dtgIndicador11_F.Visible = true;
                                if (dtgIndicador11_F.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "P":
                                dtgIndicador11_P.DataSource = lds;
                                dtgIndicador11_P.DataBind();
                                dtgIndicador11_P.Visible = true;
                                if (dtgIndicador11_P.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "S":
                                dtgIndicador11_S.DataSource = lds;
                                dtgIndicador11_S.DataBind();
                                dtgIndicador11_S.Visible = true;
                                if (dtgIndicador11_S.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "12")
                    {
                        switch (ddlTipoVista.SelectedValue)
                        {
                            case "N":
                                dtgIndicador12_N.DataSource = lds;
                                dtgIndicador12_N.DataBind();
                                dtgIndicador12_N.Visible = true;
                                if (dtgIndicador12_N.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "P":
                                dtgIndicador12_P.DataSource = lds;
                                dtgIndicador12_P.DataBind();
                                dtgIndicador12_P.Visible = true;
                                if (dtgIndicador12_P.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "13")
                    {
                        switch (ddlTipoVista.SelectedValue)
                        {
                            case "N":
                                dtgIndicador13_N.DataSource = lds;
                                dtgIndicador13_N.DataBind();
                                dtgIndicador13_N.Visible = true;
                                if (dtgIndicador13_N.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "P":
                                dtgIndicador13_P.DataSource = lds;
                                dtgIndicador13_P.DataBind();
                                dtgIndicador13_P.Visible = true;
                                if (dtgIndicador13_P.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "14")
                    {
                        dtgIndicador14.DataSource = lds;
                        dtgIndicador14.DataBind();
                        dtgIndicador14.Visible = true;
                        if (dtgIndicador14.Items.Count == 0)
                        {
                            Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "15")
                    {
                        dtgIndicador15.DataSource = lds;
                        dtgIndicador15.DataBind();
                        dtgIndicador15.Visible = true;
                        if (dtgIndicador15.Items.Count == 0)
                        {
                            Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "16")
                    {
                        dtgIndicador16.DataSource = lds;
                        dtgIndicador16.DataBind();
                        dtgIndicador16.Visible = true;
                        if (dtgIndicador16.Items.Count == 0)
                        {
                            Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "17")
                    {
                        switch (ddlTipoVista.SelectedValue)
                        {
                            case "N":
                                dtgIndicador17_N.DataSource = lds;
                                dtgIndicador17_N.DataBind();
                                dtgIndicador17_N.Visible = true;
                                if (dtgIndicador17_N.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "P":
                                dtgIndicador17_P.DataSource = lds;
                                dtgIndicador17_P.DataBind();
                                dtgIndicador17_P.Visible = true;
                                if (dtgIndicador17_P.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "18")
                    {
                        switch (ddlTipoVista.SelectedValue)
                        {
                            case "N":
                                dtgIndicador18_N.DataSource = lds;
                                dtgIndicador18_N.DataBind();
                                dtgIndicador18_N.Visible = true;
                                if (dtgIndicador18_N.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "P":
                                dtgIndicador18_P.DataSource = lds;
                                dtgIndicador18_P.DataBind();
                                dtgIndicador18_P.Visible = true;
                                if (dtgIndicador18_P.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "19")
                    {
                        dtgIndicador19.DataSource = lds;
                        dtgIndicador19.DataBind();
                        dtgIndicador19.Visible = true;
                        if (dtgIndicador19.Items.Count == 0)
                        {
                            Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "20")
                    {
                        dtgIndicador20.DataSource = lds;
                        dtgIndicador20.DataBind();
                        dtgIndicador20.Visible = true;
                        if (dtgIndicador20.Items.Count == 0)
                        {
                            Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "21")
                    {
                        switch (ddlTipoVista.SelectedValue)
                        {
                            case "F":
                                dtgIndicador21_F.DataSource = lds;
                                dtgIndicador21_F.DataBind();
                                dtgIndicador21_F.Visible = true;
                                if (dtgIndicador21_F.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "P":
                                dtgIndicador21_P.DataSource = lds;
                                dtgIndicador21_P.DataBind();
                                dtgIndicador21_P.Visible = true;
                                if (dtgIndicador21_P.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "A":
                                dtgIndicador21_A.DataSource = lds;
                                dtgIndicador21_A.DataBind();
                                dtgIndicador21_A.Visible = true;
                                if (dtgIndicador21_A.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "22")
                    {
                        dtgIndicador22.DataSource = lds;
                        dtgIndicador22.DataBind();
                        dtgIndicador22.Visible = true;
                        if (dtgIndicador22.Items.Count == 0)
                        {
                            Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                        }
                    }
                    if (ddlTipoIndica.SelectedValue == "23")
                    {
                        switch (ddlTipoVista.SelectedValue)
                        {
                            case "N":
                                dtgIndicador23_N.DataSource = lds;
                                dtgIndicador23_N.DataBind();
                                dtgIndicador23_N.Visible = true;
                                if (dtgIndicador23_N.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "P":
                                dtgIndicador23_P.DataSource = lds;
                                dtgIndicador23_P.DataBind();
                                dtgIndicador23_P.Visible = true;
                                if (dtgIndicador23_P.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "F":
                                dtgIndicador23_F.DataSource = lds;
                                dtgIndicador23_F.DataBind();
                                dtgIndicador23_F.Visible = true;
                                if (dtgIndicador23_F.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "C":
                                dtgIndicador23_C.DataSource = lds;
                                dtgIndicador23_C.DataBind();
                                dtgIndicador23_C.Visible = true;
                                if (dtgIndicador23_C.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "D":
                                dtgIndicador23_D.DataSource = lds;
                                dtgIndicador23_D.DataBind();
                                dtgIndicador23_D.Visible = true;
                                if (dtgIndicador23_D.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "S":
                                dtgIndicador23_S.DataSource = lds;
                                dtgIndicador23_S.DataBind();
                                dtgIndicador23_S.Visible = true;
                                if (dtgIndicador23_S.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                            case "T":
                                dtgIndicador23_T.DataSource = lds;
                                dtgIndicador23_T.DataBind();
                                dtgIndicador23_T.Visible = true;
                                if (dtgIndicador23_T.Items.Count == 0)
                                {
                                    Toastr.Warning(this, "No hay información para los criterios de búsqueda");
                                }
                                break;
                        }
                    }
                }
                buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Detalle, EnumBotones.Excel);
                lConexion.Cerrar();
                if (lblMensaje.Text != "")
                    tblMensaje.Visible = true;
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pudo consultar el indicador.! " + ex.Message.ToString());

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void btnDetalle_Click(object sender, EventArgs e)
        //{
        //    if (ddlTipoIndica.SelectedValue != "0")
        //       //// ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "javascript:Popup=window.open('frm_ConsIndicadoresMpDet.aspx?CodInd=" + ddlTipoIndica.SelectedValue + "&AñoInd=" + TxtAnoIndica.Text + "&MesInd=" + ddlMesIndica.SelectedValue + "');", true);
        //        btnDetalle.Attributes.Add("onClick", "javascript:Popup=window.open('frm_ConsIndicadoresMpDet.aspx?CodInd=" + ddlTipoIndica.SelectedValue + "&AñoInd=" + TxtAnoIndica.Text + "&MesInd=" + ddlMesIndica.SelectedValue + "','Detalle','width=600,height=500,left=350,top=150,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
        //}


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TxtAnoIndica_TextChanged(object sender, EventArgs e)
        {
            string lsAño = "0";
            if (TxtAnoIndica.Text != "")
                lsAño = TxtAnoIndica.Text;
            btnDetalle.Attributes.Add("onClick", "javascript:Popup=window.open('frm_ConsIndicadoresMpDet.aspx?CodInd=" + ddlTipoIndica.SelectedValue + "&AñoInd=" + lsAño + "&MesInd=" + ddlMesIndica.SelectedValue + "&tipoVista=" + ddlTipoVista.SelectedValue + "','Detalle','width=450,height=450,left=350,top=150,status=no,location=0,menubar=no,toolbar=no,resizable=yes');");

            if (ddlTipoIndica.SelectedValue != "0" && ddlTipoVista.SelectedValue != "0" && TxtAnoIni.Text.Trim().Length > 0 && TxtAnoFin.Text.Trim().Length > 0 && TxtAnoIndica.Text.Trim().Length > 0 && (tdMes01.Visible == false || ddlMesIndica.SelectedValue != "0"))
            {

                if (Validaciones() == "")
                {
                    mostrar_datos();
                }
                else
                { Toastr.Warning(this, Validaciones()); }
            }
            TxtAnoIni.Focus();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbSalir_Click(object sender, EventArgs e)
        {
            Modal.Cerrar(this, CrearRegistro.ID);
        }
    }
}
