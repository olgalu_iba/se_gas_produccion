﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ConsultaPosturasVenta.aspx.cs" Inherits="Informes.Informes_frm_ConsultaPosturasVenta" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Subasta" AssociatedControlID="ddlSubasta" runat="server" />
                            <asp:DropDownList ID="ddlSubasta" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server" OnSelectedIndexChanged="ddlSubasta_SelectedIndexChanged" AutoPostBack="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tipo Rueda" AssociatedControlID="ddlTipoRueda" runat="server" />
                            <asp:DropDownList ID="ddlTipoRueda" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Rueda" AssociatedControlID="TxtFechaIni" runat="server" />
                            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" Width="100%" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group" runat="server">
                            <asp:Label Text="Número Rueda" AssociatedControlID="TxtNoRueda" runat="server" />
                            <asp:TextBox ID="TxtNoRueda" runat="server" Width="100%" CssClass="form-control" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" runat="server">
                        <div class="form-group">
                            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
                            <asp:DropDownList ID="ddlOperador" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server" />
                        </div>
                    </div>
                </div>

                <div runat="server" visible="false" id="tblGrilla">
                    <div class="table table-responsive">
                        <%--20180126 rq107-16--%>
                        <asp:DataGrid ID="dtgMaestro" AutoGenerateColumns="False" AllowPaging="true" PageSize="20" OnPageIndexChanged="dtgMaestro_PageIndexChanged" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" runat="server">
                            <Columns>
                                <asp:BoundColumn DataField="numero_postura" HeaderText="No. Postura" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numero_id" HeaderText="No. Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numero_rueda" HeaderText="No. Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_rueda" HeaderText="Fecha Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tipo_rueda" HeaderText="Tipo  Rueda" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="180px"></asp:BoundColumn>
                                <%--20180126 rq107-16--%>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="90px" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="90px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="punto_entrega" HeaderText="Punto de Entrega"></asp:BoundColumn>
                                <asp:BoundColumn DataField="modalidad" HeaderText="Modalidad de Entrega" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="periodo_entrega" HeaderText="Periodo de Entrega" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador" HeaderText="operador"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_no_nominada" HeaderText="Cnt. No Nominada" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_no_disponible" HeaderText="Cnt. No Disponible" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_postura" HeaderText="Cnt. Postura" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="precio_reserva" HeaderText="Precio Reserva" ItemStyle-HorizontalAlign="Right" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="delta_precio" HeaderText="Delta Precio" ItemStyle-HorizontalAlign="Right" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="precio" HeaderText="Precio Oferta" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_adjudicada" HeaderText="Cantidad Adjudicada" Visible="false"></asp:BoundColumn>
                                <%--20180126 rq107-16--%>
                                <asp:BoundColumn DataField="precio_adjudicado" HeaderText="Precio Adjudicado" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Postura" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="Fecha Hora Postura"></asp:BoundColumn>
                                <asp:BoundColumn DataField="transportador" HeaderText="Transportador" Visible="false"></asp:BoundColumn>
                            </Columns>
                            <%--20180126 rq107-16--%>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                    <asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False" AllowPaging="false" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered">
                        <Columns>
                            <asp:BoundColumn DataField="numero_postura" HeaderText="No. Postura" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_id" HeaderText="No. Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_rueda" HeaderText="No. Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_rueda" HeaderText="Fecha Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="tipo_rueda" HeaderText="Tipo  Rueda" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="180px"></asp:BoundColumn>
                            <%--20180126 rq107-16--%>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="90px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="90px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="punto_entrega" HeaderText="Punto de Entrega"></asp:BoundColumn>
                            <asp:BoundColumn DataField="modalidad" HeaderText="Modalidad de Entrega"></asp:BoundColumn>
                            <asp:BoundColumn DataField="periodo_entrega" HeaderText="Periodo de Entrega"></asp:BoundColumn>
                            <asp:BoundColumn DataField="operador" HeaderText="operador"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_no_nominada" HeaderText="Cnt. No Nominada" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_no_disponible" HeaderText="Cnt. No Disponible" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_postura" HeaderText="Cnt. Postura" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="Precio Oferta" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_adjudicada" HeaderText="Cantidad Adjudicada"></asp:BoundColumn>
                            <%--20180126 rq107-16--%>
                            <asp:BoundColumn DataField="precio_adjudicado" HeaderText="Precio Adjudicado"></asp:BoundColumn>
                            <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Postura"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="Fecha Hora Postura"></asp:BoundColumn>
                            <asp:BoundColumn DataField="transportador" HeaderText="Transportador"></asp:BoundColumn>
                        </Columns>
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
