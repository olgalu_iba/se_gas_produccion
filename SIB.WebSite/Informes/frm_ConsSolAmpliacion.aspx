﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ConsSolAmpliacion.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Informes.Informes_frm_ConsSolAmpliacion" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Año Negociación" AssociatedControlID="TxtAño" runat="server" />
                            <asp:TextBox ID="TxtAño" runat="server" Width="100%" CssClass="form-control" ValidationGroup="detalle"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtAño" runat="server" TargetControlID="TxtAño"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Trimestre Negociación" AssociatedControlID="ddlTrimestre" runat="server" />
                            <asp:DropDownList ID="ddlTrimestre" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Transportador" AssociatedControlID="ddlTransportador" runat="server" />
                            <asp:DropDownList ID="ddlTransportador" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server" OnSelectedIndexChanged="ddlTransportador_SelectedIndexChanged" AutoPostBack="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tramo" AssociatedControlID="ddlTramo" runat="server" />
                            <asp:DropDownList ID="ddlTramo" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Remitente" AssociatedControlID="DdlRemitente" runat="server" />
                            <asp:DropDownList ID="DdlRemitente" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server" />
                        </div>
                    </div>
                </div>
                <div runat="server" visible="false" id="tblGrilla">
                    <div class="table table-responsive">
                        <%--20180126 rq107-16--%>
                        <asp:DataGrid ID="dtgMaestro" AutoGenerateColumns="False" AllowPaging="true" PageSize="20" OnPageIndexChanged="dtgMaestro_PageIndexChanged" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" runat="server">
                            <Columns>
                                <%--20210915-- visualizacion--%>
                                <asp:BoundColumn DataField="año_negociacion" HeaderText="Año Negociacion" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="trimestre_negociacion" HeaderText="Trimestre Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_trim_neg" HeaderText="Descripción Trimestre Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20210915 fin visualiacion--%>
                                <asp:BoundColumn DataField="codigo_transportador" HeaderText="Código Transportador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_transportador" HeaderText="Nombre Transportador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="remitente" HeaderText="Remitente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_remitente" HeaderText="Nombre Remitente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_tramo" HeaderText="Código Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="año_datos" HeaderText="Año" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="mes_datos" HeaderText="Mes" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_solicitada" HeaderText="Cantidad Solicitada Inicial(KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_retiro" HeaderText="Cantidad retirada (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_ampliacion" HeaderText="Cantidad Solicitada Final(KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                    <asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False" AllowPaging="false" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered">
                        <Columns>
                            <%--20210915-- visualizacion--%>
                            <asp:BoundColumn DataField="año_negociacion" HeaderText="Año Negociacion" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="trimestre_negociacion" HeaderText="Trimestre Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_trim_neg" HeaderText="Descripción Trimestre Negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20210915 fin visualiacion--%>
                            <asp:BoundColumn DataField="codigo_transportador" HeaderText="Código Transportador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_transportador" HeaderText="Nombre Transportador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="remitente" HeaderText="Remitente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_remitente" HeaderText="Nombre Remitente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_tramo" HeaderText="Código Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="año_datos" HeaderText="Año" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="mes_datos" HeaderText="Mes" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_solicitada" HeaderText="Cantidad Solicitada Inicial(KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_retiro" HeaderText="Cantidad retirada (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_ampliacion" HeaderText="Cantidad Solicitada Final(KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                        </Columns>
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
