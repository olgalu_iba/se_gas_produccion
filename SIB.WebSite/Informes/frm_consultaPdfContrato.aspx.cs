﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;
using System.Text;
using System.Configuration;
using Segas.Web.Elements;
public partial class Informes_frm_consultaPdfContrato : System.Web.UI.Page
{

    InfoSessionVO goInfo = null;
    static string lsTitulo = "Consulta de PDF de contratos registrados";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();
    String strRutaCarga;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        strRutaCarga = ConfigurationManager.AppSettings["RutaCargaContPdf"].ToString();
        //Se agrega el comportamiento del botón
        buttons.ExportarExcelOnclick += ImgExcel_Click;
        buttons.FiltrarOnclick += imbConsultar_Click;

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' And codigo_operador != 0 order by razon_social", 0, 4);
            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
            }
            lConexion.Cerrar();
            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar };
            buttons.Inicializar(botones: botones);
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();

            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, EventArgs e)
    {
        consulta();
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, EventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfContratosPDF" + DateTime.Now + ".xls";
        string lstitulo_informe = "Consulta PDF de contratos registrados";

        try
        {
            dtgConsulta.Columns[13].Visible = false; //20200430 ajsute nombre archivo
            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgConsulta.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            lform.Controls.Add(dtgConsulta);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
            dtgConsulta.Columns[13].Visible = true; //20200430 ajsute nombre archivo
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "No se Pude Generar el Excel.!" + ex.Message.ToString());
        }
    }

    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected void consulta()
    {
        var lblMensaje = new StringBuilder();
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF = DateTime.Now;

        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Append("Formato Inválido en el Campo fecha inicial <br>");
            }

        }
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Append("Formato Inválido en el Campo fecha final<br>");
            }
        }
        if (TxtFechaFin.Text.Trim().Length > 0 && TxtFechaIni.Text.Trim().Length == 0)
            lblMensaje.Append("Debe digitar la fecha inicial antes que la final<br>");

        if (TxtFechaFin.Text.Trim().Length > 0 && TxtFechaIni.Text.Trim().Length > 0)
            if (ldFechaI > ldFechaF)
                lblMensaje.Append("La fecha inicial debe ser menor o igual  que la final<br>");


        if(string.IsNullOrEmpty(lblMensaje.ToString()))
        {
            try
            {
                string[] lsNombreParametros = { "@P_fecha_ini", "@P_fecha_fin", "@P_operacion", "@P_contrato_def", "@P_codigo_operador", "@P_tipo_mercado" };
                SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar };
                string[] lValorParametros = { "", "", "0", "", "0", "" };

                if (TxtFechaIni.Text != "")
                {
                    lValorParametros[0] = TxtFechaIni.Text.Trim();
                    if (TxtFechaFin.Text != "")
                        lValorParametros[1] = TxtFechaFin.Text.Trim();
                    else
                        lValorParametros[1] = TxtFechaIni.Text.Trim();
                }
                if (TxtNoOper.Text.Trim().Length > 0)
                    lValorParametros[2] = TxtNoOper.Text.Trim();
                lValorParametros[3] = TxtContratoDef.Text.Trim();
                lValorParametros[4] = ddlOperador.SelectedValue;
                lValorParametros[5] = ddlMercado.SelectedValue;

                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContCargaPdfRep", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();

                tblGrilla.Visible = true;
                buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pudo Generar el Informe.! " + ex.Message.ToString());
            }
        }
        else
            Toastr.Warning(this, lblMensaje.ToString() );
    }
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
    {
        try
        {
            string lsCarpetaAnt = "";
            string[] lsCarperta;
            lsCarperta = strRutaCarga.Split('\\');
            lsCarpetaAnt = lsCarperta[lsCarperta.Length - 2];
            string lsRuta = "../" + lsCarpetaAnt + "/" + e.Item.Cells[12].Text + '/' + e.Item.Cells[10].Text.Replace(@"\", "/"); //20200430
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + lsRuta + "');", true);

        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "Error al visualizar el archivo. " + ex.Message.ToString());
        }
    }
    
}