﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;

public partial class Informes_frm_IndicadorDisponibilidadBEC : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "INDICADOR DE DISPONIBILIDAD BEC";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    DataSet lds1 = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();
    SqlDataAdapter lsqldata1 = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        bool blEncontro = false;
        int liTotal = 0;
        int liCumple = 0;
        decimal ldIndicadorMs = 0;
        decimal ldIndicadorOp = 0;
        decimal ldIndicadorTotal = 0;

        DateTime ldFecha;
        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial. <br>";
            }
        }
        else
            lblMensaje.Text += "Debe Ingresar la Fecha Inicial. <br>";
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaFin.Text);
                if (ldFecha < Convert.ToDateTime(TxtFechaIni.Text))
                    lblMensaje.Text += "La Fecha Final NO puede ser menor que la fecha Inicial. <br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Final. <br>";
            }
        }
        else
            lblMensaje.Text += "Debe Ingresar la Fecha Final. <br>";
        if (lblMensaje.Text == "")
        {
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();

                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetIndicadorBec";
                lComando.Parameters.Add("@P_fecha_inicial", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                lComando.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaFin.Text.Trim();
                lComando.Parameters.Add("@P_indicador", SqlDbType.VarChar).Value = ddlIndicador.SelectedValue;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgTransaccion.DataSource = lds;
                dtgTransaccion.DataBind();
                if (dtgTransaccion.Items.Count <= 0)
                {
                    lblMensaje.Text = "No hay Datos para Visualizar.";
                    tblGrilla.Visible = false;
                    imbExcel.Visible = false;
                }
                else
                {
                    tblGrilla.Visible = true;
                    imbExcel.Visible = true;
                    foreach (DataGridItem Grilla in this.dtgTransaccion.Items)
                    {
                        liTotal++;
                        ldIndicadorMs = ldIndicadorMs + Convert.ToDecimal(Grilla.Cells[3].Text);
                    }
                    if (liTotal > 0)
                    {
                        ldIndicadorOp = (ldIndicadorMs / liTotal);
                        lblResInd.Text = ldIndicadorOp.ToString("##0.00");
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pude Generar el Informe.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfDispBec" + DateTime.Now + ".xls";
        string lstitulo_informe = "INDICADOR DE DISPONIBILIDAD DEL BEC";
        string lsTituloParametros = "Fecha Inicial: " + TxtFechaIni.Text + " - Fecha Final: " + TxtFechaFin.Text + " Indicador: " + ddlIndicador.SelectedItem.ToString();
        try
        {

            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgTransaccion.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            lform.Controls.Add(dtgTransaccion);
            lpagina.RenderControl(lhtw);
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            this.EnableViewState = false;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='6' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center></center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='6' align='left'><font face=Arial size=4>Parametros: " + lsTituloParametros + "</font></th><td><font face=Arial size=5><center></center></font></td></tr></table><br>");
            Response.Write(lsb.ToString());
            Response.End();
            Response.Flush();

        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Excel.!" + ex.Message.ToString();
        }
    }
}