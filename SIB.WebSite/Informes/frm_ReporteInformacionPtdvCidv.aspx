﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ReporteInformacionPtdvCidv.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Informes.frm_ReporteInformacionPtdvCidv" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <div class="kt-portlet__body" id="tblDatos" runat="server">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Año Carga" AssociatedControlID="TxtAnoCarga" runat="server" />
                            <asp:TextBox ID="TxtAnoCarga" runat="server" ValidationGroup="detalle" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tipo Información" AssociatedControlID="ddlTipo" runat="server" />
                            <asp:DropDownList ID="ddlTipo" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                <asp:ListItem Value="">Ambos</asp:ListItem>
                                <asp:ListItem Value="R">Operador Registrado</asp:ListItem>
                                <asp:ListItem Value="N">Operador No Registrado</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Año Inicial" AssociatedControlID="TxtAnoIni" runat="server" />
                            <asp:TextBox ID="TxtAnoIni" runat="server" ValidationGroup="detalle" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Mes Inicial" AssociatedControlID="ddlMesIni" runat="server" />
                            <asp:DropDownList ID="ddlMesIni" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Año Final" AssociatedControlID="TxtAnoFin" runat="server" />
                            <asp:TextBox ID="TxtAnoFin" runat="server" ValidationGroup="detalle" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Mes Final" AssociatedControlID="ddlMesFinal" runat="server" />
                            <asp:DropDownList ID="ddlMesFinal" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador Registrado" AssociatedControlID="ddlOperador" runat="server" />
                            <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador No Registrado" AssociatedControlID="ddlNoReg" runat="server" />
                            <asp:DropDownList ID="ddlNoReg" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Punto del SNT" AssociatedControlID="ddlPuntoSnt" runat="server" />
                            <asp:DropDownList ID="ddlPuntoSnt" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <%--20220118--%>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Estado" AssociatedControlID="ddlEstado" runat="server" />
                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                <asp:ListItem Value ="">Seleccione</asp:ListItem>
                                <asp:ListItem Value ="0">Operadro del campo</asp:ListItem>
                                <asp:ListItem Value ="1">Estado</asp:ListItem>
                                <asp:ListItem Value ="2">Asociado</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <%--20220118--%>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Resolucion" AssociatedControlID="TxtFechaRes" runat="server" />
                            <asp:TextBox ID="TxtFechaRes" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                    <%--20220118--%>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Código Registro" AssociatedControlID="TxtCodigoReg" runat="server" />
                            <asp:TextBox ID="TxtCodigoReg" runat="server" ValidationGroup="detalle" CssClass="form-control" />
                        </div>
                    </div>
                </div>
                <div class="table table-responsive">
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="False" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered">
                        <Columns>
                            <asp:BoundColumn DataField="codigo_prod_ptdv_cidv" HeaderText="Código Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20220118--%>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador Campo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_del_campo" HeaderText="Operador Campo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20220118--%>
                            <asp:BoundColumn DataField="codigo_punto_snt" HeaderText="Código Campo o Punto SNT" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="campo_punto_snt" HeaderText="Campo o Punto SNT" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20220118--%>
                            <asp:BoundColumn DataField="nit_socio_productor" HeaderText="Nit Socio Productor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="socio_productor" HeaderText="Socio Productor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20220118--%>
                            <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn DataField="mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ptdv" HeaderText="PTDV (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="pc_sum_cons_int" HeaderText="PC - contratos suministro consumo interno (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="pc_exportaciones" HeaderText="PC - exportaciones (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="pc_ref_barranca" HeaderText="PC - refineria de barrancabermeja (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="pc_ref_cartagena" HeaderText="PC - refineria de cartagena (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="pp_operador_campo" HeaderText="PP - (declarado por el operador del campo) (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="gas_operacion" HeaderText="Gas Operacion - (declarado por el operador del campo) (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cidv" HeaderText="CIDV (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="poder_calorifico" HeaderText="poder calorifico (BTU/FT3)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,##0.000}"></asp:BoundColumn>
                            <%--20220118--%>
                            <asp:BoundColumn DataField="fecha_resolucion" HeaderText="Fecha Resolucion" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
