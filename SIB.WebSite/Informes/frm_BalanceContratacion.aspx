﻿<%@ Page Language="C#" MasterPageFile="~/PlantillaPrincipal.master" AutoEventWireup="true" CodeFile="frm_BalanceContratacion.aspx.cs"
    Inherits="Informes_frm_BalanceContratacion" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>
           <%--   <asp:UpdateProgress ID="panelUpdateProgress" DisplayAfter="0" runat="server" AssociatedUpdatePanelID="UptPnlTodo">
            <ProgressTemplate>
                <asp:Panel ID="PnlCarga" runat="server" Style="position: relative; top: 30%; text-align: center;"
                    CssClass="popup">
                    <div class="fuente">
                        <img src="../Images/ajax-loader.gif" style="vertical-align: middle" alt="Procesando" />
                        Procesando por Favor espere ...
                    </div>
                </asp:Panel>
            </ProgressTemplate>
        </asp:UpdateProgress>--%>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Vigencia Inicial</label>
                            <asp:TextBox ID="TxtFechaIni" runat="server" width="100%" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RfvTxtFechaIni" runat="server" ErrorMessage="El Campo Fecha Inicial es obligatorio"
                                ControlToValidate="TxtFechaIni" ValidationGroup="detalle">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Vigencia Final</label>
                            <asp:TextBox ID="TxtFechaFin" runat="server" width="100%" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RfvTxtFechaFin" runat="server" ErrorMessage="El Campo Fechs Final es obligatorio"
                                ControlToValidate="TxtFechaFin" ValidationGroup="detalle">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
              
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="form-group">
                        <label for="Name">Tipo de Producto</label>
                         <asp:DropDownList ID="DdlProducto" runat="server"  CssClass="form-control">
                                <asp:ListItem Value="G">Suministro de gas</asp:ListItem>
                                <asp:ListItem Value="T">Capacidad de transporte</asp:ListItem>
                            </asp:DropDownList>
                    </div>
                </div>
         
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="form-group">
                    <label for="Name">Operador</label>
                        <asp:DropDownList ID="DdlOperador" runat="server"  CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="form-group">
                    <label for="Name">Modalidad de contrato</label>
                    <asp:DropDownList ID="DdlModalidad" runat="server"  CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="form-group">
                    <label for="Name">Tipo de Mercado</label>
                     <asp:DropDownList ID="DdlMercado" runat="server"  CssClass="form-control">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="P">Primario</asp:ListItem>
                                <asp:ListItem Value="S">Secundario</asp:ListItem>
                                <asp:ListItem Value="O">Otras transacciones de mercado mayorista</asp:ListItem>
                            </asp:DropDownList>
                </div>
            </div>
        </div>

                </div>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="Table1">
            <%--<tr>
                <td class="th1" colspan="3" align="center">
                   <asp:Button ID="Button1" runat="server" Text="Consultar" OnClick="BtnBuscar_Click" />
                </td>
            </tr>--%>
            <tr>
                <td colspan="3" align="center">
                      <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
       <div border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                    id="tblGrilla" visible="false">
                   
                            <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                                <asp:DataGrid ID="dtgConsultaG" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                   Width="100%" CssClass="table-bordered">
                                    <Columns>
                                        <asp:BoundColumn DataField="fecha" HeaderText="Fecha" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="cod_ope_compra" HeaderText="Cod Operador"></asp:BoundColumn> <%--20180622--%>
                                        <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre operador"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id registro"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_mercado" HeaderText="Tipo mercado"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_subasta" HeaderText="Tipo subasta"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_punto_ent" HeaderText="Punto entrada"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_punto_sal" HeaderText="Punto salida"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="cantidad" HeaderText="cantidad" ItemStyle-HorizontalAlign="Right"
                                            DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_sector" HeaderText="Sector Consumo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Número Contrato"></asp:BoundColumn>
                                        <%--20180622 ajuste--%>
                                        <asp:BoundColumn DataField="nit_usr" HeaderText="Nit Usr final"></asp:BoundColumn>
                                        <%--20180622 ajuste--%>
                                        <asp:BoundColumn DataField="nombre_usr" HeaderText="Nombre Usr final"></asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                </asp:DataGrid>
                                <asp:DataGrid ID="dtgConsultaT" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered"
                                    Visible="false">
                                    <Columns>
                                        <asp:BoundColumn DataField="fecha" HeaderText="Fecha" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="cod_ope_compra" HeaderText="Cod Operador"></asp:BoundColumn> <%--20180622--%>
                                        <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre operador"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id registro"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_mercado" HeaderText="Tipo mercado"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_subasta" HeaderText="Tipo subasta"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="cantidad" HeaderText="Capacidad contratada" ItemStyle-HorizontalAlign="Right"
                                            DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_sector" HeaderText="Sector Consumo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Número Contrato"></asp:BoundColumn>
                                        <%--20180622 ajuste--%>
                                        <asp:BoundColumn DataField="nit_usr" HeaderText="Nit Usr final"></asp:BoundColumn>
                                        <%--20180622 ajuste--%>
                                        <asp:BoundColumn DataField="nombre_usr" HeaderText="Nombre Usr final"></asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                </asp:DataGrid>
                            </div>
                       
                </div>
            </div>
        </div>
  
   
    </asp:Content>

