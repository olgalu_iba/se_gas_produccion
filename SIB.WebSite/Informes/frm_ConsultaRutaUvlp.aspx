﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ConsultaRutaUvlp.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Informes.frm_ConsultaRutaUvlp" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div id="tblGrilla" visible="false" runat="server">
                        <div class="table table-responsive">
                            <%--20180126 rq107-16--%>
                            <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered">
                                <Columns>
                                    <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Vendedor" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_ruta" HeaderText="Código Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                    <%--20210531--%>
                                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad (KPCD)" ItemStyle-HorizontalAlign="right" 
                                        DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="costo" HeaderText="COSTO (USD/KPC)" ItemStyle-HorizontalAlign="right"
                                        DataFormatString="{0:###,###,###,##0.00}"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
