﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_entregaUsrFinal.aspx.cs" Inherits="Informes_frm_entregaUsrFinal" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div>
                    <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
                        width="80%">
                        <tr>
                            <td align="center" class="th1">
                                <asp:Label ID="lblTitulo" runat="server" ForeColor ="White" ></asp:Label>
                                <asp:HiddenField ID="hdnELiminado" runat="server" />
                                <asp:HiddenField ID="hdnRuta" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <br /><br /><br /><br /><br /><br />
                    <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
                        width="80%">
                        <tr>
                            <td class="td1">Código Entrega Usuario Final
                            </td>
                            <td class="td2">
                                <asp:TextBox ID="TxtBusCodigo" runat="server" autocomplete="off"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTBETxtBusCodigo" runat="server" TargetControlID="TxtBusCodigo"
                                    FilterType="Custom, Numbers">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr >
                            <td class="td1">Fecha Inicial
                            </td>
                            <td class="td2">
                                <asp:TextBox ID="TxtBusFecha" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>

                            </td>
                        </tr>
                        <tr >
                            <td class="td1">Fecha Final
                            </td>
                            <td class="td2">
                                <asp:TextBox ID="TxtBusFechaF" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr >
                            <td class="td1">Operador
                            </td>
                            <td class="td2">
                                <asp:DropDownList ID="ddlBusOperador" runat="server" Width="400px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr >
                            <td class="td1">Tramo
                            </td>
                            <td class="td2">
                                <asp:DropDownList ID="ddlBusTramo" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr >
                            <td class="td1">Sector de consumo
                            </td>
                            <td class="td2">
                                <asp:DropDownList ID="ddlBusSector" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr >
                            <td class="td1">Tipo de Demanda
                            </td>
                            <td class="td2">
                                <asp:DropDownList ID="ddlBusTipoDemanda" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
            id="tblMensaje">
            <tr>
                <td class="th1" colspan="3" align="center">
                    <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                        OnClick="imbConsultar_Click" Height="40" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server">
            <tr>
                <td colspan="3" align="center">
                    <div style="overflow: scroll; width: 1050px; height: 350px;">
                        <asp:DataGrid ID="dtgReporte1" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                            ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                            <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                            <ItemStyle CssClass="td2"></ItemStyle>
                            <Columns>
                                <asp:BoundColumn DataField="codigo_ent_usuario_final" HeaderText="Código Ent Usuario Final"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_registro" HeaderText="Fecha Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_tramo" HeaderText="Código Tramo" ItemStyle-HorizontalAlign="Left" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tramo" HeaderText="Desc Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Código Punto Salida Snt" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto_sal" HeaderText="Desc Punto Salida Snt" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_tipo_demanda" HeaderText="Código Tipo Demanda" ItemStyle-HorizontalAlign="Left" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tipo_demanda" HeaderText="Tipo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_sector_consumo" HeaderText="Código Sector Consumo" ItemStyle-HorizontalAlign="Left" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_sector_consumo" HeaderText="Sector de consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_entregada" HeaderText="Cantidad Entregada" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato definitivo" ItemStyle-HorizontalAlign="Left"
                                    Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad de Contrato" ItemStyle-HorizontalAlign="Left"
                                    Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ind_registrado" HeaderText="contrato registrado" ItemStyle-HorizontalAlign="Center"
                                    Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="carga_normal" HeaderText="Estado de Carga" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_ruta" HeaderText="Cod Ruta" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualización" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="Fecha-hora actual" ItemStyle-HorizontalAlign="Left" Visible="false" ></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </td>
            </tr>
        </table>
  </asp:Content>