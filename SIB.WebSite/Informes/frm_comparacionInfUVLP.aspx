﻿<%@ Page Language="C#" MasterPageFile="~/PlantillaPrincipal.master" AutoEventWireup="true" CodeFile="frm_comparacionInfUVLP.aspx.cs"
    Inherits="Informes_frm_comparacionInfUVLP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

       <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                   
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>
           <%--   <asp:UpdateProgress ID="panelUpdateProgress" DisplayAfter="0" runat="server" AssociatedUpdatePanelID="UptPnlTodo">
            <ProgressTemplate>
                <asp:Panel ID="PnlCarga" runat="server" Style="position: relative; top: 30%; text-align: center;"
                    CssClass="popup">
                    <div class="fuente">
                        <img src="../Images/ajax-loader.gif" style="vertical-align: middle" alt="Procesando" />
                        Procesando por Favor espere ...
                    </div>
                </asp:Panel>
            </ProgressTemplate>
        </asp:UpdateProgress>--%>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Año</label>
                             <asp:TextBox ID="TxtAño" runat="server" ValidationGroup="detalle" MaxLength="4" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtAño" runat="server" Enabled="True" FilterType="Custom, Numbers"
                                TargetControlID="TxtAño">
                            </ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Producto</label>
                          <asp:DropDownList ID="ddlProducto" runat="server" CssClass="form-control">
                                <asp:ListItem Value="G">Suministro de Gas</asp:ListItem>
                                <asp:ListItem Value="T">Transporte</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
              
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="form-group">
                        <label for="Name"> Comprador</label>
                       <asp:DropDownList ID="DdlComprador" runat="server" CssClass="form-control selectpicker" data-live-search="true" >
                            </asp:DropDownList>
                    </div>
                </div>
         
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="form-group">
                    <label for="Name"> Vendedor</label>
                        <asp:DropDownList ID="DdlVendedor" runat="server"  CssClass="form-control selectpicker" data-live-search="true" >
                            </asp:DropDownList>
                       
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4" style="display:none">
                <div class="form-group">
                    <label for="Name">Modalidad de contrato</label>
                    <asp:DropDownList ID="DdlModalidad" runat="server"  CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4" style="display:none">
                <div class="form-group">
                    <label for="Name">Tipo de Mercado</label>
                     <asp:DropDownList ID="DdlMercado" runat="server"  CssClass="form-control">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="P">Primario</asp:ListItem>
                                <asp:ListItem Value="S">Secundario</asp:ListItem>
                                <asp:ListItem Value="O">Otras transacciones de mercado mayorista</asp:ListItem>
                            </asp:DropDownList>
                </div>
            </div>
        </div>

                </div>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
     <%--   <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
            </td>
        </tr>--%>
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
   
       
               <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                    <table class="table-bordered" runat="server" id="tblSuministro" 
                          visible="false">
                        <tr>
                            <td align="center" colspan="8" style="color: #595d6e;
                                    background-color: #ebedf2;
                                    border-color: #ebedf2;
                                    align-items: center;
                                    align-content: center;
                                    font-weight: bold;">
                                Registro de Contratos
                            </td>
                            <td  align="center" colspan="13" style="color: #595d6e;
                                    background-color: #ebedf2;
                                    border-color: #ebedf2;
                                    align-items: center;
                                    align-content: center;
                                    font-weight: bold;">
                                Subasta
                            </td>
                        </tr>
                        <tr>
                            <td  align="center" style="color: #595d6e;
                                    background-color: #ebedf2;
                                    border-color: #ebedf2;
                                    align-items: center;
                                    align-content: center;
                                    font-weight: bold;">
                                Operación
                            </td>
                            <td  align="center" style="color: #595d6e;
                                    background-color: #ebedf2;
                                    border-color: #ebedf2;
                                    align-items: center;
                                    align-content: center;
                                    font-weight: bold;">
                                Cod Compra
                            </td>
                            <td  align="center" style="color: #595d6e;
                                    background-color: #ebedf2;
                                    border-color: #ebedf2;
                                    align-items: center;
                                    align-content: center;
                                    font-weight: bold;">
                                Nombre Compra
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cod Venta
                            </td>
                            <td align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Nombre Venta
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cod Punto Salida
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Punto Salida
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cantidad (MBTUD)
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Operación
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cod Compra
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Nombre Compra
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cod Venta
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Nombre Venta
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cod Fuente
                            </td>
                            <td align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Fuente
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cod Punto Salida
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Punto Salida
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cantidad Contratada (MBTUD)
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cantidad Contratada (KPCD)
                            </td> 
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cantidad Demandada (MBTUD)
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cantidad Demandada (KPCD)
                            </td>
                        </tr>
                    </table>
                    <table  class="table-bordered" runat="server" id="tblTransporte" 
                         visible="false">
                        <tr>
                            <td  align="center" colspan="10" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Registro de Contratos
                            </td>
                            <td  align="center" colspan="15" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Subasta
                            </td>
                        </tr>
                        <tr>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Operación
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cod Compra
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Nombre Compra
                            </td>
                            <td align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cod Venta
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Nombre Venta
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cod Ruta
                            </td>
                            <td align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Ruta
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cod Tramo
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Tramo
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cantidad (KPCD)
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Operación
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cod Compra
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Nombre Compra
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cod Venta
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Nombre Venta
                            </td>
                            <td align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cod Ruta
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Ruta
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cod Tramo
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Tramo
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                CV Inversión
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                CF Inversión
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Costo AOM
                            </td>
                            <td  align="center" style="color: #595d6e;
                                background-color: #ebedf2;
                                border-color: #ebedf2;
                                align-items: center;
                                align-content: center;
                                font-weight: bold;">
                                Cuota Fomento
                            </td>
                            <td  align="center" style="color: #595d6e;
                            background-color: #ebedf2;
                            border-color: #ebedf2;
                            align-items: center;
                            align-content: center;
                            font-weight: bold;">
                                Impuesto
                            </td>
                            <td  align="center" style="color: #595d6e;
                            background-color: #ebedf2;
                            border-color: #ebedf2;
                            align-items: center;
                            align-content: center;
                            font-weight: bold;">
                                Cantidad (KPCD)
                            </td>
                        </tr>
                    </table>
                    <asp:DataGrid ID="dtgExcelSum" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered"
                        Visible="false">
                        <Columns>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="Operacion" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_compra" HeaderText="Cod Compra" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Compra" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_venta" HeaderText="Cod Venta" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Venta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Cod Pto Salida" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_punto" HeaderText="Pto Salida" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad (MBTUD)" ItemStyle-HorizontalAlign="Right">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="Operacion" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_compra" HeaderText="Cod Compra" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Compra" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_venta" HeaderText="Cod Venta" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Venta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Cod Pto Salida" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_punto" HeaderText="Pto Salida" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_cont" HeaderText="Cantidad contratada(MBTUD)"
                                ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            <asp:BoundColumn DataField="equiv_cont" HeaderText="Cantidad contratada(KPCD)" ItemStyle-HorizontalAlign="Right">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_dem" HeaderText="Cantidad demandada (MBTUD)"
                                ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            <asp:BoundColumn DataField="equiv_dem" HeaderText="Cantidad demandada (KPCD)" ItemStyle-HorizontalAlign="Right">
                            </asp:BoundColumn>
                        </Columns>
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgExcelTra" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered"
                        Visible="false">
                        <Columns>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="Operacion" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_compra" HeaderText="Cod Compra" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Compra" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_venta" HeaderText="Cod Venta" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Venta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cod_ruta_cont" HeaderText="Cod Ruta" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_ruta_cont" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cod_tramo_cont" HeaderText="Cod Tramo" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tramo_cont" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_sub" HeaderText="Cantidad (KPCD)" ItemStyle-HorizontalAlign="Right">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="Operacion" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_compra" HeaderText="Cod Compra" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Compra" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_venta" HeaderText="Cod Venta" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Venta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cod_ruta_cont" HeaderText="Cod Ruta" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_ruta_cont" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cod_tramo_cont" HeaderText="Cod Tramo" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tramo_cont" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cargo_variable" HeaderText="CV Inversion" ItemStyle-HorizontalAlign="Right">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cargo_fijo_inv" HeaderText="CF Inversion" ItemStyle-HorizontalAlign="Right">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cargo_fijo_adm" HeaderText="Costo AOM" ItemStyle-HorizontalAlign="Right">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="porc_cuota" HeaderText="Cuota Fomento" ItemStyle-HorizontalAlign="Right">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="porc_imp_tra" HeaderText="Impuesto" ItemStyle-HorizontalAlign="Right">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_cont" HeaderText="Cantidad (KPCD)" ItemStyle-HorizontalAlign="Right">
                            </asp:BoundColumn>
                        </Columns>
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </div>
           
             </div>
         
        </div>
  
   
</asp:Content>