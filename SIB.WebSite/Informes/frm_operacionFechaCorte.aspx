﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_operacionFechaCorte.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Informes.frm_operacionFechaCorte" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Corte" AssociatedControlID="TxtFechaCorte" runat="server"/>
                            <asp:TextBox ID="TxtFechaCorte" placeholder="yyyy/mm/dd" Width="100%" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Negociación Inicial" AssociatedControlID="TxtFechaIni" runat="server"></asp:Label>
                            <asp:TextBox ID="TxtFechaIni" Width="100%" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker" placeholder="yyyy/mm/dd" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Negociación Final" AssociatedControlID="TxtFechaFin" runat="server"></asp:Label>
                            <asp:TextBox ID="TxtFechaFin" Width="100%" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker" placeholder="yyyy/mm/dd" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group" runat="server">
                            <asp:Label Text="Fecha Modificación Inicial" AssociatedControlID="TxtFechaModIni" runat="server"></asp:Label>
                            <asp:TextBox ID="TxtFechaModIni" Width="100%" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker" placeholder="yyyy/mm/dd" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group" runat="server">
                            <asp:Label Text="Fecha Modificación Final" AssociatedControlID="TxtFechaModFin" runat="server"></asp:Label>
                            <asp:TextBox ID="TxtFechaModFin" Width="100%" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker" placeholder="yyyy/mm/dd" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" runat="server">
                        <div class="form-group">
                            <asp:Label Text="Número Operación" AssociatedControlID="TxtOperIni" runat="server"></asp:Label>
                            <asp:TextBox ID="TxtOperIni" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbeTxtOperIni" runat="server" TargetControlID="TxtOperIni" FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Número contrato" AssociatedControlID="TxtContratoDef" runat="server"></asp:Label>
                            <asp:TextBox ID="TxtContratoDef" runat="server" ValidationGroup="detalle" MaxLength="30" Width="100%" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tipo Subasta" AssociatedControlID="ddlSubasta" runat="server"></asp:Label>
                            <asp:DropDownList ID="ddlSubasta" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tipo Mercado" AssociatedControlID="ddlMercado" runat="server" />
                            <asp:DropDownList ID="ddlMercado" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlMercado_SelectedIndexChanged" Width="100%" CssClass="form-control">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="P">Primario</asp:ListItem>
                                <asp:ListItem Value="S">Secundario</asp:ListItem>
                                <asp:ListItem Value="O">Otras transacciones mercado mayorista</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Producto" AssociatedControlID="ddlProducto" runat="server" />
                            <asp:DropDownList ID="ddlProducto" runat="server" Width="100%" CssClass="form-control">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="G">Suministro de gas</asp:ListItem>
                                <asp:ListItem Value="T">Capacidad de transporte</asp:ListItem>
                                <asp:ListItem Value="A">Suministro y transporte</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Causa Modificación" AssociatedControlID="ddlCausa" runat="server" />
                            <asp:DropDownList ID="ddlCausa" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador contraparte" AssociatedControlID="ddlContraparte" runat="server" />
                            <asp:DropDownList ID="ddlContraparte" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Id Registro" AssociatedControlID="TxtNoId" runat="server" />
                            <asp:TextBox ID="TxtNoId" runat="server" ValidationGroup="detalle" CssClass="form-control" />
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtNoId" runat="server" TargetControlID="TxtNoId" FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Punta" AssociatedControlID="ddlPunta" runat="server" />
                            <asp:DropDownList ID="ddlPunta" runat="server" CssClass="form-control">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="C">Compra</asp:ListItem>
                                <asp:ListItem Value="V">Venta</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
                            <asp:DropDownList ID="ddlOperador" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                </div>
                <div class="table table-responsive">
                    <asp:DataGrid ID="dtgMaestro" Visible="False" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="false" AllowPaging="false" PagerStyle-HorizontalAlign="Center">
                        <Columns>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Operación" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociación" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_suscripcion_cont" HeaderText="Fecha Suscripción" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_tipo_subasta" HeaderText="Cod Tipo Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_subasta" HeaderText="Tipo Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="tipo_rueda" HeaderText="Cod Tipo Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_rueda" HeaderText="Tipo Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_mercado" HeaderText="Mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Número de contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_compra" HeaderText="Código Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="rol_compra" HeaderText="Rol Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_venta" HeaderText="Código vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="rol_venta" HeaderText="Rol vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="hora_negociacion" HeaderText="Hora negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_punto_entrega" HeaderText="Cod Punto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_punto" HeaderText="Punto / ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Cod Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="hora_inicial" HeaderText="Hora Inicial" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="hora_final" HeaderText="Hora Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="No registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="carga_extemporanea" HeaderText="Actualización/Extemporánea" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_fuente" HeaderText="Código fuene" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="presion_punto_fin" HeaderText="Presión" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="sentido_flujo" HeaderText="Flujo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_periodo" HeaderText="Cod Periodo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_periodo" HeaderText="Periodo Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_causa" HeaderText="Causa modificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20210707--%>
                            <asp:BoundColumn DataField="codigo_trm" HeaderText="Código Tasa Cambio" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_trm" HeaderText="Tasa Cambio" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="observacion_trm" HeaderText="Observación Tasa Cambio" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="tipo_moneda" HeaderText="Tipo Moneda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--fin 20210707--%>
                        </Columns>
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
