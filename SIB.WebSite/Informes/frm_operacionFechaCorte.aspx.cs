﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Informes
{
    public partial class frm_operacionFechaCorte : Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Consulta de Operaciones a Fecha de Corte";
        clConexion lConexion = null;
        SqlDataReader lLector;
        DataSet lds = new DataSet();
        SqlDataAdapter lsqldata = new SqlDataAdapter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo.ToString();
            lConexion = new clConexion(goInfo);
            //Se agrega el comportamiento del botón
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.FiltrarOnclick += btnConsultar_Click;

            if (IsPostBack) return;

            //Titulo
            Master.Titulo = "Informes";

            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles1(lConexion.gObjConexion, ddlSubasta, "", "", 0, 1);
            LlenarControles2(lConexion.gObjConexion, ddlCausa, "", "", 0, 1);
            if (goInfo.cod_comisionista == "0")
            {
                LlenarControles3(lConexion.gObjConexion, ddlContraparte, "m_operador ope", " estado = 'A' and codigo_operador !=0  order by codigo_operador", 0, 4);
                LlenarControles3(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador !=0 order by codigo_operador", 0, 4);
            }
            else
            {
                LlenarControles3(lConexion.gObjConexion, ddlContraparte, "m_operador ope", " estado = 'A' and codigo_operador !=0 and (exists (select 1 from t_contrato_verificacion ver where ver.estado ='R' and ver.operador_compra = " + goInfo.cod_comisionista + " and ver.operador_venta = ope.codigo_operador) or exists(select 1 from t_contrato_verificacion ver where ver.estado = 'R' and ver.operador_venta = " + goInfo.cod_comisionista + " and ver.operador_compra = ope.codigo_operador)) order by codigo_operador", 0, 4);
                LlenarControles3(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador = " + goInfo.cod_comisionista + " order by codigo_operador", 0, 4);
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
            }
            lConexion.Cerrar();
            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar };
            buttons.Inicializar(botones: botones);
            TxtFechaCorte.Text = DateTime.Now.Date.ToString("yyyy/MM/dd");

        }

        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
            //dtgComisionista.DataBind();

            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetTiposSubastaAdc", null, null, null);
            var lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);
            while (lLector.Read())
            {
                var lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["descripcion"].ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles2(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
            //dtgComisionista.DataBind();

            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetCausaModAdc", null, null, null);
            var lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);
            while (lLector.Read())
            {
                var lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["descripcion"].ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles2
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles3(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            var lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                var lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            var lsNombreArchivo = goInfo.Usuario.ToString() + "InfNivelContTra" + DateTime.Now + ".xls";
            var lstitulo_informe = "Informe operaciones a fecha de corte";
            var lsTituloParametros = "";
            try
            {
                lsTituloParametros += "Fecha Corte: " + TxtFechaCorte.Text.Trim();
                if (TxtOperIni.Text.Trim().Length > 0)
                    lsTituloParametros += " - Operación: " + TxtOperIni.Text.Trim();
                if (TxtNoId.Text.Trim().Length > 0)
                    lsTituloParametros += " - No Registro: " + TxtNoId.Text.Trim();
                if (TxtFechaIni.Text.Trim().Length > 0)
                    lsTituloParametros += " - Fecha Operación inicial: " + TxtFechaIni.Text.Trim();
                if (TxtFechaFin.Text.Trim().Length > 0)
                    lsTituloParametros += " - Fecha Operación final : " + TxtFechaFin.Text.Trim();
                if (TxtFechaModIni.Text.Trim().Length > 0)
                    lsTituloParametros += " - Fecha modificación inicial: " + TxtFechaIni.Text.Trim();
                if (TxtFechaModFin.Text.Trim().Length > 0)
                    lsTituloParametros += " - Fecha modificación final : " + TxtFechaFin.Text.Trim();
                if (TxtContratoDef.Text.Trim().Length > 0)
                    lsTituloParametros += " - Contrato : " + TxtContratoDef.Text.Trim();
                if (ddlSubasta.SelectedValue != "0")
                    lsTituloParametros += " - Subasta : " + ddlSubasta.SelectedItem.ToString();
                if (ddlProducto.SelectedValue != "")
                    lsTituloParametros += " - Producto: " + ddlProducto.SelectedItem.ToString();
                if (ddlCausa.SelectedValue != "0")
                    lsTituloParametros += " - Causa Modificación: " + ddlCausa.SelectedItem.ToString();
                if (ddlContraparte.SelectedValue != "0")
                    lsTituloParametros += " - Operador Contraparte: " + ddlContraparte.SelectedItem.ToString();
                if (ddlPunta.SelectedValue != "")
                    lsTituloParametros += " - Punta: " + ddlPunta.SelectedItem.ToString();
                if (ddlOperador.SelectedValue != "0")
                    lsTituloParametros += " - Operador: " + ddlOperador.SelectedItem.ToString();


                decimal ldCapacidad = 0;
                var lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                var lsw = new StringWriter(lsb);
                var lhtw = new HtmlTextWriter(lsw);
                var lpagina = new Page();
                var lform = new HtmlForm();
                dtgMaestro.EnableViewState = false;
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                lform.Controls.Add(dtgMaestro);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());

                Response.End();
                lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
            }
            catch (Exception ex)
            {

                Toastr.Warning(this, "No se Pudeo Generar el Excel.!" + ex.Message.ToString());

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            var lblMensaje = new StringBuilder();
            DateTime ldFecha = DateTime.Now;
            if (TxtFechaCorte.Text.Trim().Length == 0)
            {
                lblMensaje.Append("Debe ingresar la fecha de corte. <br>");
            }
            else
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaCorte.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato inválido en el campo fecha de corte. <br>");
                }
            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo Fecha de negociación inicial. <br>");

                }
            }
            if (TxtFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    if (TxtFechaIni.Text == "")
                    {
                        lblMensaje.Append("Debe seleccionar la fecha de negociación inicial antes que la final<br>");
                    }
                    else
                    {
                        if (Convert.ToDateTime(TxtFechaFin.Text) < ldFecha)
                        {
                            lblMensaje.Append("La fecha de negociación final debe ser mayor que la inicial. <br>");
                        }
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo Fecha de negociación inicial. <br>");
                }
            }
            if (TxtFechaModIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaModIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo Fecha de modificación inicial. <br>");
                }
            }
            if (TxtFechaModFin.Text.Trim().Length > 0)
            {
                try
                {
                    if (TxtFechaModIni.Text == "")
                    {
                        lblMensaje.Append("Debe seleccionar la fecha de modificación inicial antes que la final<br>");
                    }
                    else
                    {
                        if (Convert.ToDateTime(TxtFechaModFin.Text) < ldFecha)
                        {
                            lblMensaje.Append("La fecha de modificación final debe ser mayor que la inicial. <br>");
                        }
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo Fecha de modificación inicial. <br>");
                }
            }
            if (TxtFechaIni.Text.Trim() == "" && TxtFechaFin.Text.Trim() == "" && TxtFechaModIni.Text.Trim() == "" && TxtFechaModIni.Text.Trim() == ""
                && (TxtOperIni.Text.Trim() == "" || TxtOperIni.Text.Trim() == "0") && TxtContratoDef.Text.Trim() == "" && ddlSubasta.SelectedValue == "0"
                && ddlProducto.SelectedValue == "" && ddlCausa.SelectedValue == "0" &&
                ddlContraparte.SelectedValue == "0" && ddlPunta.SelectedValue == "" && ddlOperador.SelectedValue == "0")
            {
                lblMensaje.Append("Debe seleccionar la menos un criterio de búsqueda. <br>");
            }

            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                Toastr.Warning(this, lblMensaje.ToString());
                return;
            }

            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                var lComando = new SqlCommand
                {
                    Connection = lConexion.gObjConexion,
                    CommandTimeout = 3600,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "pa_GetOperFechaCorte"
                };
                lComando.Parameters.Add("@P_fecha_corte", SqlDbType.VarChar).Value = TxtFechaCorte.Text.Trim();
                if (TxtFechaIni.Text.Trim().Length <= 0)
                {
                    lComando.Parameters.Add("@P_fecha_neg_ini", SqlDbType.VarChar).Value = "";
                    lComando.Parameters.Add("@P_fecha_neg_fin", SqlDbType.VarChar).Value = "";
                }
                else
                {
                    lComando.Parameters.Add("@P_fecha_neg_ini", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                    lComando.Parameters.Add("@P_fecha_neg_fin", SqlDbType.VarChar).Value = TxtFechaFin.Text.Trim().Length <= 0 ? TxtFechaIni.Text.Trim() : TxtFechaFin.Text.Trim();
                }
                if (TxtFechaModIni.Text.Trim().Length <= 0)
                {
                    lComando.Parameters.Add("@P_fecha_modif_ini", SqlDbType.VarChar).Value = "";
                    lComando.Parameters.Add("@P_fecha_modif_fin", SqlDbType.VarChar).Value = "";
                }
                else
                {
                    lComando.Parameters.Add("@P_fecha_modif_ini", SqlDbType.VarChar).Value = TxtFechaModIni.Text.Trim();
                    if (TxtFechaModFin.Text.Trim().Length <= 0)
                        lComando.Parameters.Add("@P_fecha_modif_fin", SqlDbType.VarChar).Value = TxtFechaModIni.Text.Trim();
                    else
                        lComando.Parameters.Add("@P_fecha_modif_fin", SqlDbType.VarChar).Value = TxtFechaModFin.Text.Trim();
                }
                lComando.Parameters.Add("@P_operacion", SqlDbType.Int).Value = TxtOperIni.Text.Trim().Length <= 0 ? "0" : TxtOperIni.Text.Trim();
                lComando.Parameters.Add("@P_contrato_definitivo", SqlDbType.VarChar).Value = TxtContratoDef.Text.Trim();
                lComando.Parameters.Add("@P_codigo_tipo_subasta", SqlDbType.VarChar).Value = ddlSubasta.SelectedValue;
                lComando.Parameters.Add("@P_tipo_mercado", SqlDbType.VarChar).Value = ddlMercado.SelectedValue;
                lComando.Parameters.Add("@P_producto", SqlDbType.VarChar).Value = ddlProducto.SelectedValue;
                lComando.Parameters.Add("@P_codigo_causa", SqlDbType.VarChar).Value = ddlCausa.SelectedValue;
                lComando.Parameters.Add("@P_codigo_contraparte", SqlDbType.Int).Value = ddlContraparte.SelectedValue;
                lComando.Parameters.Add("@P_punta", SqlDbType.VarChar).Value = ddlPunta.SelectedValue;
                lComando.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = ddlOperador.SelectedValue;
                lComando.Parameters.Add("@P_codigo_verif", SqlDbType.Int).Value = TxtNoId.Text.Trim().Length <= 0 ? "0" : TxtNoId.Text.Trim();
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgMaestro.DataSource = lds;
                dtgMaestro.DataBind();
                dtgMaestro.Visible = true;
                buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se pudo generar el informe.! " + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20170814 rq036-17
        protected void ddlMercado_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlProducto.Items.Clear();
            var lItem = new ListItem { Value = "", Text = "Seleccione" };
            ddlProducto.Items.Add(lItem);
            var lItem1 = new ListItem { Value = "G", Text = "Suministro de gas" };
            ddlProducto.Items.Add(lItem1);
            var lItem2 = new ListItem { Value = "T", Text = "Capacidad de transporte" };
            ddlProducto.Items.Add(lItem2);
            if (ddlMercado.SelectedValue != "" && ddlMercado.SelectedValue != "O") return;
            var lItem3 = new ListItem { Value = "A", Text = "Suministro y transporte" };
            ddlProducto.Items.Add(lItem3);
        }
    }
}