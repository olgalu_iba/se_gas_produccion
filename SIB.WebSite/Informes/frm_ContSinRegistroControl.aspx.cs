﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;
using System.Text;
using Segas.Web.Elements;

public partial class Informes_frm_ContSinRegistroControl : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Consulta de operaciones sin registro";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        //Se agrega el comportamiento del botón
        buttons.ExportarExcelOnclick += ImgExcel_Click;
        buttons.FiltrarOnclick += imbConsultar_Click;

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlOpeC, "m_operador ope", " exists  (select 1 from t_contrato_verificacion con where con.estado ='S' and ope.codigo_operador = con.operador_compra) or exists (select 1 from t_contrato con where ope.codigo_operador = con.operador_compra  and not exists (select 1 from t_contrato_verificacion conV where con.numero_contrato = conV.numero_contrato and conV.ind_contrato ='S')) order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlOpeV, "m_operador ope", " exists  (select 1 from t_contrato_verificacion con where con.estado ='S' and ope.codigo_operador = con.operador_venta) or exists (select 1 from t_contrato con where ope.codigo_operador = con.operador_venta and not exists (select 1 from t_contrato_verificacion conV where con.numero_contrato = conV.numero_contrato and conV.ind_contrato ='S')) order by razon_social", 0, 4);
            lConexion.Cerrar();
            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar };
            buttons.Inicializar(botones: botones);
            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "E";
            lItem.Text = "SUBASTA NEGOCIACIÓN DIRECTA EXT";
            ddlSubasta.Items.Add(lItem);
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = "M";
            lItem1.Text = " OTRAS TRANSACCIONES DEL MERCADO MAYORISTA";
            ddlSubasta.Items.Add(lItem1);
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            if (lsTabla != "m_operador ope")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        if (lblMensaje.Text == "")
        {
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetContSinRegControl";
                if (TxtOperacion.Text != "")
                    lComando.Parameters.Add("@P_numero_contrato", SqlDbType.Int).Value = TxtOperacion.Text;
                lComando.Parameters.Add("@P_fecha_negociacion", SqlDbType.VarChar).Value = TxtFecha.Text;
                lComando.Parameters.Add("@P_codigo_tipo_subasta", SqlDbType.VarChar).Value = ddlSubasta.SelectedValue;
                lComando.Parameters.Add("@P_tipo_mercado", SqlDbType.Char).Value = ddlMercado.SelectedValue;
                lComando.Parameters.Add("@P_destino_rueda", SqlDbType.Char).Value = ddlProducto.SelectedValue;
                lComando.Parameters.Add("@P_operador_compra", SqlDbType.Int).Value = ddlOpeC.SelectedValue;
                lComando.Parameters.Add("@P_operador_venta", SqlDbType.Int).Value = ddlOpeV.SelectedValue;
                //20171130 rq026-17
                if (TxtNoId.Text != "")
                    lComando.Parameters.Add("@P_codigo_verif", SqlDbType.Int).Value = TxtNoId.Text;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgMaestro.DataSource = lds;
                dtgMaestro.DataBind();
                tblGrilla.Visible = true;
                buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pudo Generar el Informe.! " + ex.Message.ToString());
   
            }
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, EventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfContSInReg" + DateTime.Now + ".xls";
        string lstitulo_informe = "Informe Operaciones sin registro";
        string lsTituloParametros = "";
        try
        {
            if (TxtOperacion.Text !="")
                lsTituloParametros += " - Operación: " + TxtOperacion.Text;
            //20171130 rq026-17
            if (TxtNoId.Text != "")
                lsTituloParametros += " - Id Registro: " + TxtNoId.Text;
            if (TxtFecha.Text != "")
                lsTituloParametros += " - Fecha Neg: " + TxtFecha.Text;
            if (ddlSubasta.SelectedValue != "0")
                lsTituloParametros += " - Tipo Subasta: " + ddlSubasta.SelectedItem.ToString();
            if (ddlMercado.SelectedValue != "")
                lsTituloParametros += " - Tipo Mercado: " + ddlMercado.SelectedItem.ToString();
            if (ddlProducto.SelectedValue != "")
                lsTituloParametros += " - Producto: " + ddlProducto.SelectedItem.ToString();
            if (ddlOpeC.SelectedValue != "0")
                lsTituloParametros += "  - Operador Compra: " + ddlOpeC.SelectedItem.ToString();
            if (ddlOpeV.SelectedValue != "0")
                lsTituloParametros += "  - Operador Venta: " + ddlOpeV.SelectedItem.ToString();
            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgMaestro.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            lform.Controls.Add(dtgMaestro);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "No se Pudo Generar el Informe.! " + ex.Message.ToString());
  
        }
    }
   
}