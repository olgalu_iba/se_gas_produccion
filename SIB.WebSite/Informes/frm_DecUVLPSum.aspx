﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_DecUVLPSum.aspx.cs" Inherits="Informes_frm_DecUVLPSum" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
            width="80%">
            <tr>
                <td align="center" class="th1">
                    <asp:Label ID="lblTitulo" runat="server" ForeColor ="White" ></asp:Label>
                </td>
            </tr>
        </table>
    <br /><br /><br /><br /><br /><br />
        <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
            width="80%">
            <tr>
                <td class="td1">Año subasta
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtAño" runat="server" MaxLength="4"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FTEBTxtAño" runat="server" TargetControlID="TxtAño"
                        FilterType="Custom, Numbers">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td class="td1">Operador
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlOperador" runat="server" Width="200px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">Punto de salida
                </td>
                <td class="td2">
                    <asp:DropDownList ID="DdlPuntoSal" runat="server" Width="150px">
                    </asp:DropDownList>
                </td>
                <td class="td1">Estado declaración
                </td>
                <td class="td2">
                    <asp:DropDownList ID="DdlEstado" runat="server">
                        <asp:ListItem Value="">Seleccione</asp:ListItem>
                        <asp:ListItem Value="C">Creado</asp:ListItem>
                        <asp:ListItem Value="A">Aprobado</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblMensaje">
            <tr>
                <td class="th1" colspan="3" align="center">
                    <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                        OnClick="imbConsultar_Click" Height="40" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblGrilla" visible="false">
            <tr>
                <td colspan="3" align="center">
                    <div style="overflow: scroll; width: 1050px; height: 350px;">
                        <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                            HeaderStyle-CssClass="th1">
                            <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                            <ItemStyle CssClass="td2"></ItemStyle>
                            <Columns>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_compra" HeaderText="Cod. Titular" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_compra" HeaderText="Titular compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Número contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_modalidad" HeaderText="cod. modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_modalidad" HeaderText="modalidad contractual" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha inicial" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha final" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod. Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_punto" HeaderText="Cod. Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto_ent" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Cod. punto salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto_sal" HeaderText="Punto salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad contratada MBTUD" ItemStyle-HorizontalAlign="right" DataFormatString="{0: ###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="equiv_cont_real" HeaderText="Cantidad contratada KPCD" ItemStyle-HorizontalAlign="right" DataFormatString="{0: ###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_contratada" HeaderText="Cantidad contratada real MBTUD" ItemStyle-HorizontalAlign="right" DataFormatString="{0: ###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="equiv_cont_kpcd" HeaderText="Cantidad contratada real KPCD" ItemStyle-HorizontalAlign="right" DataFormatString="{0: ###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_demandada" HeaderText="Cantidad demanda MBTUD" ItemStyle-HorizontalAlign="right" DataFormatString="{0: ###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="equiv_demand_kpcd" HeaderText="Cantidad demandada KPCD" ItemStyle-HorizontalAlign="right" DataFormatString="{0: ###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_estado" HeaderText="Estado declaración" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="True" AllowPaging="false"
                            Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                            ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                            <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                            <ItemStyle CssClass="td2"></ItemStyle>
                            <Columns>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </td>
            </tr>
        </table>
</asp:Content>