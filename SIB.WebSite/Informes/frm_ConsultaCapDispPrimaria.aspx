﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ConsultaCapDispPrimaria.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Informes_frm_ConsultaCapDispPrimaria" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />

                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Operador</label>
                            <asp:DropDownList ID="ddlOperador" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                            <label for="Name">Tramo</label>
                            <asp:DropDownList ID="ddlTramo" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>

                        </div>
                    </div>


                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Año  inicial</label>
                            <asp:TextBox ID="TxtAnoInicial" runat="server" Width="100%" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox> 
                             
                             <label for="Name">Año Final</label>
                            <asp:TextBox ID="TxtAnoFinal" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div id="tdMes01" class="form-group" runat="server">
                              <label for="Name">Mes  inicial</label>                            
                             <asp:DropDownList ID="ddlMesInicial" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="01" Text="Enero"></asp:ListItem>
                                <asp:ListItem Value="02" Text="Febrero"></asp:ListItem>
                                <asp:ListItem Value="03" Text="Marzo"></asp:ListItem>
                                <asp:ListItem Value="04" Text="Abril"></asp:ListItem>
                                <asp:ListItem Value="05" Text="Mayo"></asp:ListItem>
                                <asp:ListItem Value="06" Text="Junio"></asp:ListItem>
                                <asp:ListItem Value="07" Text="Julio"></asp:ListItem>
                                <asp:ListItem Value="08" Text="Agosto"></asp:ListItem>
                                <asp:ListItem Value="09" Text="Septiembre"></asp:ListItem>
                                <asp:ListItem Value="10" Text="Octubre"></asp:ListItem>
                                <asp:ListItem Value="11" Text="Noviembre"></asp:ListItem>
                                <asp:ListItem Value="12" Text="Diciembre"></asp:ListItem>
                            </asp:DropDownList> 
                                  <label for="Name">Mes  Final</label>
                            <asp:DropDownList ID="ddlMesFinal" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="01" Text="Enero"></asp:ListItem>
                                <asp:ListItem Value="02" Text="Febrero"></asp:ListItem>
                                <asp:ListItem Value="03" Text="Marzo"></asp:ListItem>
                                <asp:ListItem Value="04" Text="Abril"></asp:ListItem>
                                <asp:ListItem Value="05" Text="Mayo"></asp:ListItem>
                                <asp:ListItem Value="06" Text="Junio"></asp:ListItem>
                                <asp:ListItem Value="07" Text="Julio"></asp:ListItem>
                                <asp:ListItem Value="08" Text="Agosto"></asp:ListItem>
                                <asp:ListItem Value="09" Text="Septiembre"></asp:ListItem>
                                <asp:ListItem Value="10" Text="Octubre"></asp:ListItem>
                                <asp:ListItem Value="11" Text="Noviembre"></asp:ListItem>
                                <asp:ListItem Value="12" Text="Diciembre"></asp:ListItem>
                            </asp:DropDownList>

                        </div>
                    </div>
                   
                </div>
            </div>
             <table border="0" align="center" cellpadding="3" cellspacing="2" cssclass="form-control" width="100%" runat="server"
                        id="tblMensaje">
                    
                        <tr>
                            <td colspan="3" align="center">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <div runat="server" 
                        id="tblGrilla" visible="false">

                        <div class="table table-responsive"  style="overflow: scroll; height: 450px;">
                            <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" 
                                PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered">

                                <Columns>
                                    <asp:BoundColumn DataField="transportador" HeaderText="Transportador" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="300px"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="300px"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Width="80px"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="80px"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="capacidad" HeaderText="Capacidad Disp. Primaria" ItemStyle-HorizontalAlign="Right"
                                        ItemStyle-Width="120px"></asp:BoundColumn>
                                </Columns>

                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </div>

                    </div>
        </div>
    </div>



</asp:Content>
