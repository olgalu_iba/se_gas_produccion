﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using Segas.Web.Elements;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Informes_frm_BalanceContratacion : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Balance de contratación por agente"; //20200727
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);
        //Se agrega el comportamiento del botón
        buttons.ExportarExcelOnclick += imbExcel_Click;
        buttons.FiltrarOnclick += BtnBuscar_Click;


        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, DdlModalidad, "m_modalidad_contractual", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlOperador, "m_operador", " estado = 'A' order by razon_social", 0, 1);
            lConexion.Cerrar();

            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Excel };
            buttons.Inicializar(botones: botones);
        }
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            if (lsTabla == "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            lDdl.Items.Add(lItem1);
        }

        lLector.Close();
    }


    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        bool Error = false;
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF = DateTime.Now;
        int liValor = 0;
        string[] lsNombreParametros = { "@P_fecha_ini", "@P_fecha_fin", "@P_destino_rueda", "@P_codigo_operador", "@P_codigo_modalidad", "@P_tipo_mercado" };
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
        string[] lValorParametros = { "", "", "", "0", "0", "" };

        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Formato Inválido en el Campo Fecha inicial. <br>");
                Error = true;
            }
        }
        else
        {
            Toastr.Warning(this, "Debe digitar la fecha inicial. <br>");
            Error = true;
        }
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Formato Inválido en el Campo Fecha Final. <br>");
                Error = true;
            }
        }
        else
        {
            Toastr.Warning(this, "Debe digitar la fecha final. <br>");
            Error = true;

        }

        if (TxtFechaIni.Text.Trim().Length > 0 && TxtFechaFin.Text.Trim().Length > 0)
            try
            {
                if (ldFechaF < ldFechaI)
                {
                    Toastr.Warning(this, "La Fecha inicial debe ser menor o igual que la fecha final. <br>");
                    Error = true;
                }
        
            }
            catch (Exception ex)
            { }
        if (lblMensaje.Text == "")
        {
            TimeSpan DifFecha;
            DifFecha = ldFechaF - ldFechaI;
            if (DifFecha.Days > 31)
            {
                Toastr.Warning(this, "El rango de fechas debe ser máximo de un mes. <br>");
                Error = true;
            }
        }
        if (!Error)
        {
            try
            {
                dtgConsultaG.Visible = false;
                dtgConsultaT.Visible = false;
                if (TxtFechaIni.Text != "")
                {
                    lValorParametros[0] = TxtFechaIni.Text;
                    if (TxtFechaFin.Text != "")
                        lValorParametros[1] = TxtFechaFin.Text;
                    else
                        lValorParametros[1] = TxtFechaIni.Text;
                }
                lValorParametros[2] = DdlProducto.SelectedValue;
                lValorParametros[3] = DdlOperador.SelectedValue;
                lValorParametros[4] = DdlModalidad.SelectedValue;
                lValorParametros[5] = DdlMercado.SelectedValue;
                lConexion.Abrir();
                if (DdlProducto.SelectedValue == "G")
                {
                    dtgConsultaG.Visible = true;
                    dtgConsultaG.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetBalanceCont", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgConsultaG.DataBind();
                    if (dtgConsultaG.Items.Count > 0)
                        tblGrilla.Visible = true;
                    else
                    {
                        tblGrilla.Visible = false;
                        Toastr.Warning(this, "No se encontraron Registros. <br>");
        
                    }
                }
                else
                {
                    dtgConsultaT.Visible = true;
                    dtgConsultaT.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetBalanceCont", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgConsultaT.DataBind();
                    if (dtgConsultaT.Items.Count > 0)
                        tblGrilla.Visible = true;
                    else
                    {
                        tblGrilla.Visible = false;
                        Toastr.Warning(this, "No se encontraron Registros. <br>");
                     
                    }
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pudo Generar el Informe.! " + ex.Message.ToString());
              
            }
        }
    }
    /// <summary>
    /// Exportacion a Excel de la Información  de la Grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbExcel_Click(object sender, EventArgs e)
    {
        bool Error = false;
        string lsParametros = "Fecha Inicial =" + TxtFechaIni.Text + " - Fecha Final = "+ TxtFechaFin.Text ;
        if (DdlOperador.SelectedValue != "0")
            lsParametros += " - Operador =" + DdlOperador.SelectedItem.ToString();
        if (DdlModalidad.SelectedValue != "0")
            lsParametros += " - Modalidad =" + DdlModalidad.SelectedItem.ToString();
        if (DdlMercado.SelectedValue != "")
            lsParametros += " - Mercado =" + DdlMercado.SelectedItem.ToString();
        try
        {
            string lsNombreArchivo = Session["login"] + "InfExcelReg" + DateTime.Now + ".xls";
            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            lpagina.EnableEventValidation = false;
            lpagina.Controls.Add(lform);
            if (DdlProducto.SelectedValue == "G")
            {
                dtgConsultaG.EnableViewState = false;
                dtgConsultaG.Visible = true;
                lform.Controls.Add(dtgConsultaG);
            }
            else
            {
                dtgConsultaT.EnableViewState = false;
                dtgConsultaT.Visible = true;
                lform.Controls.Add(dtgConsultaT);
            }
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;

            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=4>" + "Consulta Balance de contratación " + DdlProducto.SelectedItem.ToString() + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=4> Parametros: " + lsParametros + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write(lsb.ToString());
            Response.End();
            Response.Flush();
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "Problemas al Consultar los Contratos. " + ex.Message.ToString());

        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnBuscar_Click(object sender, EventArgs e)
    {
        CargarDatos();
    }
}
