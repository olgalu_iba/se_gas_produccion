﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_AuditoriaMaestros.aspx.cs" Inherits="Informes_frm_AuditoriaMaestros" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
                    width="80%">
                    <tr>
                        <td align="center" class="th1">
                            <asp:Label ID="lblTitulo" runat="server" ForeColor ="White" ></asp:Label>
                        </td>
                    </tr>
                </table>
                <br /><br /><br /><br /><br /><br />
                <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
                    width="80%">
                    <tr>
                        <td class="td1">
                            Año Auditoría
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtAño" runat="server" ValidationGroup="detalle"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FteTxtAño" runat="server" Enabled="True" FilterType="Custom, Numbers"
                                TargetControlID="TxtAño">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            Fecha Inicial
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RfvTxtFechaIni" runat="server" ErrorMessage="El Campo Fecha Inicial es obligatorio"
                                ControlToValidate="TxtFechaIni" ValidationGroup="detalle">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            Fecha Final
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RfvTxtFechaFin" runat="server" ErrorMessage="El Campo Fecha Final es obligatorio"
                                ControlToValidate="TxtFechaFin" ValidationGroup="detalle">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            Tabla
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlTabla" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTabla_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            Usuario Evento
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="txtUsuario" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            Evento
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlEvento" runat="server">
                                <asp:ListItem Value="">Seleccione </asp:ListItem>
                                <asp:ListItem Value="1">Insertar</asp:ListItem>
                                <asp:ListItem Value="2">Actualizar</asp:ListItem>
                                <asp:ListItem Value="3">Eliminar</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla" visible="false">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; height: 400px;">
                    <asp:DataGrid ID="dtgReporte" runat="server" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1"
                        HorizontalAlign="Left" OnEditCommand="dtgReporte_EditCommand">
                        <Columns>
                            <asp:EditCommandColumn HeaderText="Detalle" EditText="Detalle" HeaderStyle-CssClass="">
                            </asp:EditCommandColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <asp:DataGrid ID="dtgColumnas" runat="server" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1"
                    HorizontalAlign="Left" Visible="false">
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tbDetalle" visible="false">
        <tr>
            <td align="center" class="th1">
                <asp:Label ID="lblTipoIinf" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="BtnRegresar" runat="server" Text="Regresar" OnClick="BtnRegresar_Click" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel1" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel1_Click"
                    Height="35" />
                <br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <div style="overflow: scroll; height: 400px;">
                    <asp:DataGrid ID="dtgDetalle" runat="server" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1"
                        HorizontalAlign="Left">
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>