﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_IndicadorDisponibilidadBEC.aspx.cs" Inherits="Informes_frm_IndicadorDisponibilidadBEC" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
        width="80%">
        <tr>
            <td align="center" class="th1" style="width: 60%;">
                <asp:Label ID="lblTitulo" runat="server" ForeColor ="White" ></asp:Label>
            </td>
            <td align="center" class="th1" style="width: 60%;">
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
            </td>
        </tr>
    </table>
    <br /><br /><br /><br /><br /><br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
                width="80%">
                <tr>
                    <td class="style1">
                        Fecha Inicial
                    </td>
                    <td class="style2">
                        <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="td1">
                        Fecha Final
                    </td>
                    <td class="td2">
                        <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="td1">
                        Indicador
                    </td>
                    <td class="td2">
                        <asp:DropDownList ID="ddlIndicador" runat="server">
                            <asp:ListItem Value="D" Text="Dia"></asp:ListItem>
                            <asp:ListItem Value="Noche" Text="Noche"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            </div>
            <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                id="tblMensaje">
                <tr>
                    <td class="th1" align="center">
                        <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                            OnClick="imbConsultar_Click" Height="40" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
            <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                id="tblGrilla" visible="false">
                <tr>
                    <td colspan="3" align="center">
                        <div style="overflow: scroll; width: 1050px; height: 350px;">
                            <asp:DataGrid ID="dtgTransaccion" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                                HeaderStyle-CssClass="th1">
                                <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                                <ItemStyle CssClass="td2"></ItemStyle>
                                <Columns>
                                    <asp:BoundColumn DataField="fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Width="150px"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="tiempo_total" HeaderText="Total" ItemStyle-HorizontalAlign="Right">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="tiempo_disponible" HeaderText="Tiempo Disponible" ItemStyle-HorizontalAlign="Right">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="porcentaje" HeaderText="Porcentaje" ItemStyle-HorizontalAlign="Right">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="1" align="left">
                        <asp:Label ID="lblInd1" runat="server" Text="INDICADOR DE DISPONIBILIDAD BEC" Font-Bold="true"
                            Font-Size="12px" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="right">
                        <asp:Label ID="lblResInd" runat="server" Font-Bold="true" Font-Size="14px" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="right">
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>