﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ConsPecioMsMp.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Informes.Informes_frm_ConsPecioMsMp" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Inicial:</label>
                            <asp:TextBox ID="TxtFechaIni" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Final:</label>
                            <asp:TextBox ID="TxtFechaFin" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Vendedor MS" AssociatedControlID="ddlOperador" runat="server" />
                            <asp:DropDownList ID="ddlOperador" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server" />
                        </div>
                    </div>
                </div>

                <div runat="server" visible="false" id="tblGrilla">
                    <div class="table table-responsive">
                        <%--20180126 rq107-16--%>
                        <asp:DataGrid ID="dtgMaestro" AutoGenerateColumns="False" AllowPaging="true" PageSize="20" OnPageIndexChanged="dtgMaestro_PageIndexChanged" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" runat="server">
                            <Columns>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación Mercado Secundario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20210224--%>
                                <asp:BoundColumn DataField="operador_venta" HeaderText="Código Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20210224--%>
                                <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20220828--%>
                                <asp:BoundColumn DataField="precio_ms" HeaderText="Precio Mercado Secundario (Moneda vigente/KPC)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro Mercado Primario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20220828--%>
                                <asp:BoundColumn DataField="precio_mp" HeaderText="Precio Mercado Primario (Moneda vigente/KPC)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_ruta" HeaderText="Código Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="supera_precio" HeaderText="Indicador Precio MS mayor a Precio MP"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                    <asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False" AllowPaging="false" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered">
                        <Columns>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación Mercado Secundario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20210224--%>
                            <asp:BoundColumn DataField="operador_venta" HeaderText="Código Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20210224--%>
                            <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20220828--%>
                            <asp:BoundColumn DataField="precio_ms" HeaderText="Precio Mercado Secundario (Moneda vigente/KPC)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:##0.00}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro Mercado Primario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20220828--%>
                            <asp:BoundColumn DataField="precio_mp" HeaderText="Precio Mercado Primario (Moneda vigente/KPC)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:##0.00}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_ruta" HeaderText="Código Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            <asp:BoundColumn DataField="supera_precio" HeaderText="Indicador Precio MS mayor a Precio MP"></asp:BoundColumn>
                        </Columns>
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
