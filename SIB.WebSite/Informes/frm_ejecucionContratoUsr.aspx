﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ejecucionContratoUsr.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Informes_frm_ejecucionContratoUsr" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />

                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Inicial de vigencia</label>
                            <asp:TextBox ID="TxtFechaIni" runat="server" Width="100%" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Final de vigencia</label>
                            <asp:TextBox ID="TxtFechaFin" runat="server" Width="100%" ValidationGroup="detalle" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>



                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div id="tdMes01" class="form-group" runat="server">
                            <label for="Name">Número Operación</label>
                            <asp:TextBox ID="TxtOperacion" runat="server" ValidationGroup="detalle" MaxLength="10" Width="100%" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtOperacion" runat="server" TargetControlID="TxtOperacion"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Contrato Definitivo</label>
                            <asp:TextBox ID="TxtContratoDef" runat="server" ValidationGroup="detalle" MaxLength="30" Width="100%" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" style="display:none">
                        <div class="form-group">
                            <label for="Name">Punta</label>
                            <asp:DropDownList ID="DdlPunta" runat="server" Width="100%" CssClass="form-control">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="C">Comprador</asp:ListItem>
                                <asp:ListItem Value="V">Vendedor</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Operador</label>
                            <asp:DropDownList ID="DdlOperador" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" style="display:none">
                        <div class="form-group">
                            <label for="Name">Tipo Mercado</label>
                            <asp:DropDownList ID="DdlMercado" runat="server" Width="100%" CssClass="form-control">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="P">Primario</asp:ListItem>
                                <asp:ListItem Value="S">Secundario</asp:ListItem>
                                <asp:ListItem Value="O">Otras transacciones del Mercado Mayorista</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" style="display:none">
                        <div class="form-group">
                            <label for="Name">Punta</label>
                            <asp:DropDownList ID="DdlModalidad" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" style="display:none">
                        <div class="form-group">
                            <label for="Name">Operador</label>
                            <asp:DropDownList ID="DropDownList1" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </div>
                    </div>

                </div>
                <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                    id="tblMensaje">

                    <tr>
                        <td colspan="3" align="center">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                </table>
                <div runat="server"
                    id="tblGrilla" visible="false">

                    <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                        <asp:DataGrid ID="dtgMaestro" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" AllowPaging="false"
                            PagerStyle-HorizontalAlign="Center">

                            <Columns>
                                <asp:BoundColumn DataField="fecha_gas" HeaderText="Fecha gas" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_subasta" HeaderText="Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato Definitivo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190524 rq029-19--%>
                                <asp:BoundColumn DataField="codigo_contrato_eje" HeaderText="Código Ejecución Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_punto" HeaderText="Código Punto" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tipo_demanda" HeaderText="Código tipo demanda" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="sector_consumo" HeaderText="Código Sector Consumo" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_sector" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190524 rq029-19--%>
                                <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Código Punto Salida" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_salida" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190524 rq029-19--%>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Comprador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_venta" HeaderText="Código Vendedor" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_autorizada" HeaderText="Cantidad Compra" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Cantidad_v" HeaderText="Cantidad Venta" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_facturado" HeaderText="valor Compra" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_v" HeaderText="valor venta" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ind_modif" HeaderText="Modificado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nit_usuario_no_regulado" HeaderText="Nit Usuario Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_usuario_final" HeaderText="Nombre Usuario Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_mercado_relevante" HeaderText="Código Mercado Relevante" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_mercado_relev" HeaderText="Mercado Relevante" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190524 rq029-19--%>
                                <asp:BoundColumn DataField="cantidad_contratada" HeaderText="Cantidad contratada" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--20190524 rq029-19--%>
                            </Columns>

                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>

                </div>
            </div>


        </div>
    </div>


</asp:Content>
