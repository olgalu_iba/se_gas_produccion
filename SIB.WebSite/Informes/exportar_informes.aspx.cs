﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Data.SqlClient;
using System.IO;
using System.Text;


public partial class Informes_exportar_informes : System.Web.UI.Page
{
    //variables globales de la clase
    clConexion lConexion = null;
    InfoSessionVO goInfo = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            string lsdatos = "";
            string lstipo_export = "";
            string lsnombre_tabla = "";
            string lsnombre_campos = "";
            string lstitulo_informe = "";
            string lscondicion = "";
            string lsTituloParametros = "";
            string[] lscolumnas = null;
            string[] lsNombreParametros = null;
            string[] lValorParametros = null;
            string lscolumnas2 = "";
            string lsprocedimiento = "";
            string lsNombreArchivo = goInfo.Usuario.ToString() + "InformeExcel" + DateTime.Now + ".xls";
            int liNumeroParametros = 0;
            GridView gvInforme = new GridView();

            //CAPTURA NOMBRE DE LA TABLA O TITULO PARA ENCABEZADO
            if (this.Request.QueryString["nombre_tabla"] != null)
            {
                lsnombre_tabla = this.Request.QueryString["nombre_tabla"];
            }
            if (this.Request.QueryString["titulo_informe"] != null)
            {
                lstitulo_informe = this.Request.QueryString["titulo_informe"];
            }
            else
                lstitulo_informe = "SIN TITULO";

            //CAPTURA NOMBRE DE LA TABLA O TITULO PARA ENCABEZADO
            if (this.Request.QueryString["condicion"] != null)
            {
                lscondicion = this.Request.QueryString["condicion"].Replace("#", "%");
            }
            //CAPTURA Titulo de los Parametros
            if (this.Request.QueryString["TituloParametros"] != null)
            {
                lsTituloParametros = this.Request.QueryString["TituloParametros"];
            }

            //DESCOMPONE EL ARREGLO DE COLUMNAS A MOSTRAR
            if (this.Request.QueryString["columnas"] != null)
            {
                lscolumnas = this.Request.QueryString["columnas"].Split('*');
                lscolumnas2 = this.Request.QueryString["columnas"].Replace('*', ',');
                lsnombre_campos = "<tr>";
                foreach (string lsnc in lscolumnas)
                {
                    lsnombre_campos += "<td><b>" + lsnc.ToString() + "</b></td>";
                }
                lsnombre_campos += "</tr>";
            }
            //DESCOMPONE EL ARREGLO DE Nombres de Parametros
            if (this.Request.QueryString["nombreParametros"] != null)
            {
                lsNombreParametros = this.Request.QueryString["nombreParametros"].Split('*');
            }
            //Descompone el Arreglo de los Valores de los Parametros
            if (this.Request.QueryString["valorParametros"] != null)
            {
                lValorParametros = this.Request.QueryString["valorParametros"].Split('*');
            }
            //CAPTURA EL NOMBRE DEL PROCEDIMIENTO EN CASO DE QUE VENGA
            if (this.Request.QueryString["procedimiento"] != null)
            {
                lsprocedimiento = this.Request.QueryString["procedimiento"];
            }
            //SELECCIONA EL TIPO DE EXPORTACION SEGUN EL PARAMETRO 1-PROCEDIMIENTO 2-SP_SELECCIONARREGISTROS
            if (this.Request.QueryString["tipo_export"] != null)
            {
                lstipo_export = this.Request.QueryString["tipo_export"];
            }

            if (Convert.ToInt32(lstipo_export.ToString()) == 1)
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                DataSet lds = new DataSet();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = lsprocedimiento;
                lComando.CommandTimeout = 3600;
                lComando.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = lsnombre_tabla;
                lComando.Parameters.Add("@P_filtro", SqlDbType.VarChar).Value = lscondicion;
                lComando.ExecuteNonQuery();
                SqlDataAdapter lsqldata = new SqlDataAdapter(lComando);
                lsqldata.Fill(lds);
                gvInforme.DataSource = lds;
                gvInforme.DataBind();
            }
            if (Convert.ToInt32(lstipo_export.ToString()) == 2)
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                DataSet lds = new DataSet();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandTimeout = 3600;
                lComando.CommandText = lsprocedimiento;
                if (lsNombreParametros != null)
                {
                    for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametros.Length - 1; liNumeroParametros++)
                    {
                        lComando.Parameters.Add(lsNombreParametros[liNumeroParametros], SqlDbType.VarChar).Value = lValorParametros[liNumeroParametros];
                    }
                }
                int liReg = lComando.ExecuteNonQuery();
                SqlDataAdapter lsqldata = new SqlDataAdapter(lComando);
                lsqldata.Fill(lds);
                gvInforme.DataSource = lds;
                gvInforme.DataBind();
            }

            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            gvInforme.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            lform.Controls.Add(gvInforme);
            lpagina.RenderControl(lhtw);
            Response.Clear();
            lConexion.Cerrar();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            this.EnableViewState = false;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Array oTabla = lsnombre_tabla.Split(',');
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + oTabla.GetValue(0).ToString() + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='8' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.Write(lsb.ToString());
            Response.End();
            Response.Flush();
        }
        catch (Exception ex)
        {
            lblError.Text = "Ha ocurrido un error: " + ex.Message;
            lblError.Visible = true;
        }
    }


}