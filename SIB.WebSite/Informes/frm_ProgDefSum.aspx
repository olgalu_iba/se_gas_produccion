﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ProgDefSum.aspx.cs" Inherits="Informes.frm_ProgDefSum" MasterPageFile="~/PlantillaPrincipal.master" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tipo Reporte" AssociatedControlID="DdlReporte" runat="server" />
                            <asp:DropDownList ID="DdlReporte" runat="server" CssClass="form-control">
                                <%--20180126 rq107-16--%>
                                <asp:ListItem Value="A">Programación Definitiva Suministro D-1</asp:ListItem>
                                <asp:ListItem Value="D">Programación Definitiva Suministro día de gas</asp:ListItem>
                                <%--20190318 rq017-19--%>
                                <asp:ListItem Value="P">Programación Definitiva Suministro posterior a renominación D+1</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Punto del SNT" AssociatedControlID="DdlPozo" runat="server" />
                            <asp:DropDownList ID="DdlPozo" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                            <%--20180126 rq107-16--%>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Inicial" AssociatedControlID="TxtFechaIni" runat="server" />
                            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" ValidationGroup="detalle" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Final" AssociatedControlID="TxtFechaFin" runat="server" />
                            <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" ValidationGroup="detalle" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Comprador" AssociatedControlID="DdlComprador" runat="server" />
                            <asp:DropDownList ID="DdlComprador" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Vendedor" AssociatedControlID="DdlVendedor" runat="server" />
                            <asp:DropDownList ID="DdlVendedor" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" style="display: none">
                        <div class="form-group">
                            <asp:Label Text="Punto del SNT" AssociatedControlID="ddlPuntoSnt" runat="server" />
                            <asp:DropDownList ID="ddlPuntoSnt" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <%--20200727 ajsute front-end--%>
                    <%--<div class="col-sm-12 col-md-6 col-lg-4" style="display: none">
                        <div class="form-group">
                            <asp:Label Text="Año Carga" AssociatedControlID="TxtAnoCarga" runat="server" />
                            <asp:TextBox ID="TxtAnoCarga" runat="server" CssClass="form-control" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>--%>
                </div>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered">
                                <Columns>
                                    <%--20190318 rq017-19--%>
                                    <asp:BoundColumn DataField="codigo_prog_sum" HeaderText="Código Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Fecha_reporte" HeaderText="Fecha Reporte" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:yyy/MM/dd}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Fecha_gas" HeaderText="Fecha de Gas" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:yyy/MM/dd}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="operador_comprador" HeaderText="Código Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_comprador" HeaderText="Nombre Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_punto" HeaderText="Codigo Punto SNT" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_punto" HeaderText="Punto SNT" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_tipo_demanda" HeaderText="Codigo Tipo Demanda" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo Demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_sector_consumo" HeaderText="Código Sector Consumo" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_mercado" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_destino" HeaderText="Codigo de Destino" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_destino" HeaderText="Destino" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_vigente" HeaderText="Vigente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190318 rq017-19--%>
                                    <asp:BoundColumn DataField="fecha_recibo" HeaderText="Fecha Recibo Nominación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190318 rq017-19--%>
                                    <asp:BoundColumn DataField="hora_recibo" HeaderText="Hora Recibo Nominación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20200727 ajsute front-ned--%>
                                    <%--20190607 rq036-19--%>
                                    <asp:BoundColumn DataField="estado_carga" HeaderText="Estado Carga" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190607 rq036-19--%>
                                    <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190607 rq036-19--%>
                                    <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="Fecha-Hora Carga" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyy/MM/dd HH:mm}"></asp:BoundColumn>
                                    <%--20200727 fin ajsute front-ned--%>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <asp:DataGrid ID="dtgMaestroExcel" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered"
                    PagerStyle-HorizontalAlign="Center" Visible="false">
                    <Columns>
                        <%--20190318 rq017-19--%>
                        <asp:BoundColumn DataField="codigo_prog_sum" HeaderText="Código Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Fecha_reporte" HeaderText="Fecha reporte" ItemStyle-HorizontalAlign="Center"
                            DataFormatString="{0:yyy/MM/dd}"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Fecha_gas" HeaderText="Fecha de gas" ItemStyle-HorizontalAlign="Center"
                            DataFormatString="{0:yyy/MM/dd}"></asp:BoundColumn>
                        <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="operador_vendedor" HeaderText="Código Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="rol_vendedor" HeaderText="Rol comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="nombre_vendedor" HeaderText="Nombre Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_punto" HeaderText="Código Punto SNT" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_punto" HeaderText="Punto SNT" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_tipo_demanda" HeaderText="Código Tipo Demanda"
                            ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_demanda" HeaderText="Tipo demanda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_sector_consumo" HeaderText="Código Sector Consumo"
                            ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_mercado" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_destino" HeaderText="Código Destino" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_destino" HeaderText="Destino" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="cantidad" HeaderText="cantidad (MBTUD)" ItemStyle-HorizontalAlign="Right"
                            DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_estado" HeaderText="estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_vigente" HeaderText="vigente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--20190318 rq017-19--%>
                        <asp:BoundColumn DataField="fecha_recibo" HeaderText="Fecha Recibo Nominación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--20190318 rq017-19--%>
                        <asp:BoundColumn DataField="hora_recibo" HeaderText="Hora Recibo Nominación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--20200727 ajsute front-ned--%>
                        <%--20190607 rq036-19--%>
                        <asp:BoundColumn DataField="estado_carga" HeaderText="Estado Carga" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--20190607 rq036-19--%>
                        <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--20190607 rq036-19--%>
                        <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="Fecha-Hora Carga" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyy/MM/dd HH:mm}"></asp:BoundColumn>
                        <%--20200727 fin ajsute front-ned--%>
                    </Columns>
                    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                </asp:DataGrid>
            </div>
        </div>
    </div>
</asp:Content>
