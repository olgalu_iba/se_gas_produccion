﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;
using System.Text;
using Segas.Web.Elements;

namespace Informes
{
    public partial class frm_ReporteVerifOpeTrans : System.Web.UI.Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Reportes de Verificación de información Operativa y Transaccional";
        clConexion lConexion = null;
        SqlDataReader lLector;
        DataSet lds = new DataSet();
        SqlDataAdapter lsqldata = new SqlDataAdapter();

        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo.ToString();
            lConexion = new clConexion(goInfo);

            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.FiltrarOnclick += imbConsultar_Click;

            EnumBotones[] botones = { EnumBotones.Buscar };
            buttons.Inicializar(botones: botones);

            Master.Titulo = "Informes";

            if (IsPostBack) return;

            /// Llenar controles del Formulario
            lConexion.Abrir();
            //LlenarControles(lConexion.gObjConexion, DdlPozo, "m_pozo, r_punto_reporte", "codigo_pozo = codigo_punto and ind_entrada ='S' and  estado = 'A'  order by descripcion", 0, 1); //20190711 rq034-19 --20220705
            LlenarControles(lConexion.gObjConexion, DdlPozo, "m_pozo", "ind_entrada ='S' and  estado = 'A'  order by descripcion", 0, 1); //20220705
            LlenarControles(lConexion.gObjConexion, DdlPunto, "m_punto_salida_snt", " estado = 'A' order by descripcion", 0, 2);
            lConexion.Cerrar();
            LlenarTramo();
            //20190711 rq034-19
            if (goInfo.cod_comisionista == "0")
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = "1";
                lItem1.Text = "Verificación Capacidades de Transporte";
                ddlReporte.Items.Add(lItem1);
                ListItem lItem2 = new ListItem();
                lItem2.Value = "2";
                lItem2.Text = "Verificación CMMP menor a la contratación";
                ddlReporte.Items.Add(lItem2);
                ListItem lItem3 = new ListItem();
                lItem3.Value = "3";
                lItem3.Text = "Cantidad Energía Inyectada Vs energía Recibida";
                ddlReporte.Items.Add(lItem3);
                ListItem lItem4 = new ListItem();
                lItem4.Value = "4";
                lItem4.Text = "Cantidad Energía tomada Vs Entrega usr final";
                ddlReporte.Items.Add(lItem4);
                ListItem lItem5 = new ListItem();
                lItem5.Value = "5";
                lItem5.Text = "Número de contrato reportado por Transportador Vs Usuario Final";
                ddlReporte.Items.Add(lItem5);
            }
            else
            {
                string xx = Session["TipoOperador"].ToString();
                if (Session["TipoOperador"].ToString() == "P" || Session["TipoOperador"].ToString() == "I" || Session["TipoOperador"].ToString() == "T")
                {
                    ListItem lItem3 = new ListItem();
                    lItem3.Value = "3";
                    lItem3.Text = "Cantidad Energía Inyectada Vs energía Recibida";
                    ddlReporte.Items.Add(lItem3);
                }
                if (Session["TipoOperador"].ToString() == "C" || Session["TipoOperador"].ToString() == "U" || Session["TipoOperador"].ToString() == "G" || Session["TipoOperador"].ToString() == "T")
                {
                    ListItem lItem4 = new ListItem();
                    lItem4.Value = "4";
                    lItem4.Text = "Cantidad Energía tomada Vs Entrega usr final";
                    ddlReporte.Items.Add(lItem4);
                    ddlReporte_SelectedIndexChanged(null, null);
                }
            }
            ddlReporte_SelectedIndexChanged(null, null);
            hndRegPag.Value = "30";
            dtgReporte1.PageSize = Convert.ToInt16(hndRegPag.Value);
            dtgReporte2.PageSize = Convert.ToInt16(hndRegPag.Value);
            dtgReporte3.PageSize = Convert.ToInt16(hndRegPag.Value);
            dtgReporte4.PageSize = Convert.ToInt16(hndRegPag.Value);
            dtgReporte5.PageSize = Convert.ToInt16(hndRegPag.Value);

        }
        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbConsultar_Click(object sender, EventArgs e)
        {
            var lblMensaje = new StringBuilder();
            DateTime ldFechaI = DateTime.Now;
            DateTime ldFechaF = DateTime.Now;
            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo Fecha Inicial. <br>");
                }
            }
            else
                lblMensaje.Append("Debe digitar la Fecha Inicial. <br>");
            if (TxtFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
                    if (TxtFechaIni.Text.Trim().Length > 0 && ldFechaF < ldFechaI)
                        lblMensaje.Append("La Fecha Final NO puede ser Menor que la Fecha Inicial. <br>");
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Formato Inválido en el Campo Fecha Final. <br>");
                }
            }
            else
                lblMensaje.Append("Debe digitar la Fecha Final. <br>");

            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                Toastr.Warning(this, lblMensaje.ToString());
                return;
            }
            try
            {
                dtgReporte1.Visible = false;
                dtgReporte2.Visible = false;
                dtgReporte3.Visible = false;
                dtgReporte4.Visible = false;
                dtgReporte5.Visible = false;

                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.Parameters.Clear();
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetRepAudInfOpeTran";
                lComando.Parameters.Add("@P_tipo_reporte", SqlDbType.Int).Value = ddlReporte.SelectedValue;
                lComando.Parameters.Add("@P_ind_detalle", SqlDbType.VarChar).Value = "C";
                lComando.Parameters.Add("@P_fecha_inicial", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                lComando.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaFin.Text.Trim();
                lComando.Parameters.Add("@P_codigo_tramo", SqlDbType.Int).Value = DdlTramo.SelectedValue;
                lComando.Parameters.Add("@P_tipo_mercado", SqlDbType.VarChar).Value = DdlMercado.SelectedValue;
                lComando.Parameters.Add("@P_codigo_pozo", SqlDbType.Int).Value = DdlPozo.SelectedValue;
                lComando.Parameters.Add("@P_codigo_punto_salida", SqlDbType.Int).Value = DdlPunto.SelectedValue;
                lComando.Parameters.Add("@P_contrato_definitivo", SqlDbType.VarChar).Value = TxtContrato.Text;
                lComando.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = goInfo.cod_comisionista; //20190711 rq034-19
                lComando.Parameters.Add("@PageIndex", SqlDbType.Int).Value = 1;
                lComando.Parameters.Add("@PageSize", SqlDbType.Int).Value = hndRegPag.Value;
                lComando.ExecuteNonQuery();


                switch (ddlReporte.SelectedValue)
                {
                    case "1":
                        dtgReporte1.Visible = true;
                        dtgReporte1.VirtualItemCount = CantidadReg();
                        dtgReporte1.CurrentPageIndex = 0;
                        break;
                    case "2":
                        dtgReporte2.Visible = true;
                        dtgReporte2.VirtualItemCount = CantidadReg();
                        dtgReporte2.CurrentPageIndex = 0;
                        break;
                    case "3":
                        dtgReporte3.Visible = true;
                        dtgReporte3.VirtualItemCount = CantidadReg();
                        dtgReporte3.CurrentPageIndex = 0;
                        break;
                    case "4":
                        dtgReporte4.Visible = true;
                        dtgReporte4.VirtualItemCount = CantidadReg();
                        dtgReporte4.CurrentPageIndex = 0;
                        break;
                    case "5":
                        dtgReporte5.Visible = true;
                        dtgReporte5.VirtualItemCount = CantidadReg();
                        dtgReporte5.CurrentPageIndex = 0;
                        break;
                }

                traerDatos(1);
                buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
                tdTotal.Visible = true;
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se pudo generar el informe.! " + ex.Message);
            }
        }
        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            var lblMensaje = new StringBuilder();
            try
            {
                if (Convert.ToInt32(lblTotal.Text) > 200000)
                    lblMensaje.Append("Hay demasiada información y no se puede generar el excel.");
            }
            catch (Exception ex)
            {
            }

            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                Toastr.Warning(this, lblMensaje.ToString());
                return;
            }

            traerDatosExc();
            string lsNombreArchivo = goInfo.Usuario.ToString() + "InfVerifInfOpeTrans" + DateTime.Now + ".xls";
            string lstitulo_informe = ddlReporte.SelectedItem.ToString();
            string lsTituloParametros = "";
            try
            {
                if (TxtFechaIni.Text.Trim().Length > 0)
                    lsTituloParametros += " - Fecha Inicial: " + TxtFechaIni.Text.Trim();
                if (TxtFechaFin.Text.Trim().Length > 0)
                    lsTituloParametros += " - Fecha Final: " + TxtFechaFin.Text.Trim();
                if (DdlTramo.SelectedValue != "0") //20190711 rq034-19
                    lsTituloParametros += " - Tramo: " + DdlTramo.SelectedItem.ToString();
                if (DdlPozo.SelectedValue != "0")
                    lsTituloParametros += " - Punto de Entrega: " + DdlPozo.SelectedItem.ToString();
                if (DdlMercado.SelectedValue != "")
                    lsTituloParametros += " - Tipo Mercado: " + DdlMercado.SelectedItem.ToString();
                if (DdlPunto.SelectedValue != "0")
                    lsTituloParametros += " - Punto Salida: " + DdlPunto.SelectedItem.ToString();
                if (TxtContrato.Text != "")
                    lsTituloParametros += " - Contrato Definitivo: " + TxtContrato.Text;
                //20190711 rq034-19
                if (goInfo.cod_comisionista != "0")
                    lsTituloParametros += " - Operador: " + Session["NomOperador"].ToString();
                decimal ldCapacidad = 0;
                StringBuilder lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                switch (ddlReporte.SelectedValue)
                {
                    case "1":
                        dtgReporteExc1.Visible = true;
                        dtgReporteExc1.EnableViewState = false;
                        lform.Controls.Add(dtgReporteExc1);
                        break;
                    case "2":
                        dtgReporteExc2.Visible = true;
                        dtgReporteExc2.EnableViewState = false;
                        lform.Controls.Add(dtgReporteExc2);
                        break;
                    case "3":
                        dtgReporteExc3.Visible = true;
                        dtgReporteExc3.EnableViewState = false;
                        lform.Controls.Add(dtgReporteExc3);
                        break;
                    case "4":
                        dtgReporteExc4.Visible = true;
                        dtgReporteExc4.EnableViewState = false;
                        lform.Controls.Add(dtgReporteExc4);
                        break;
                    case "5":
                        dtgReporteExc5.Visible = true;
                        dtgReporteExc5.EnableViewState = false;
                        lform.Controls.Add(dtgReporteExc5);
                        break;
                }
                lpagina.RenderControl(lhtw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = System.Text.Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());

                Response.End();
                lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
                switch (ddlReporte.SelectedValue)
                {
                    case "1":
                        dtgReporteExc1.Visible = false;
                        break;
                    case "2":
                        dtgReporteExc2.Visible = false;
                        break;
                    case "3":
                        dtgReporteExc3.Visible = false;
                        break;
                    case "4":
                        dtgReporteExc4.Visible = false;
                        break;
                    case "5":
                        dtgReporteExc5.Visible = false;
                        break;
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se pudo generar el excel. " + ex.Message);
            }
        }
        protected void ddlReporte_SelectedIndexChanged(object sender, EventArgs e)
        {
            DdlTramo.SelectedValue = "0";
            DdlPozo.SelectedValue = "0";
            DdlMercado.SelectedValue = "";
            DdlPunto.SelectedValue = "0";
            TxtContrato.Text = "";
            DdlVerifica.SelectedValue = ""; //20220202 verifica operativa
            switch (ddlReporte.SelectedValue)
            {
                case "1":
                    trTramo.Visible = true;
                    trMercado.Visible = true;
                    trPozo.Visible = false;
                    trPunto.Visible = false;
                    trContrato.Visible = false;
                    trCoincide.Visible = false;//20220202 verifica operativa
                    break;
                case "2":
                    trTramo.Visible = true;
                    trMercado.Visible = true;
                    trPozo.Visible = false;
                    trPunto.Visible = false;
                    trContrato.Visible = false;
                    trCoincide.Visible = false;//20220202 verifica operativa
                    break;
                case "3":
                    trTramo.Visible = false;
                    trMercado.Visible = false;
                    trPozo.Visible = true;
                    trPunto.Visible = false;
                    trContrato.Visible = false;
                    trCoincide.Visible = true;//20220202 verifica operativa
                    break;
                case "4":
                    trTramo.Visible = false;
                    trMercado.Visible = false;
                    trPozo.Visible = false;
                    trPunto.Visible = true;
                    trContrato.Visible = false;
                    trCoincide.Visible = true;//20220202 verifica operativa
                    break;
                case "5":
                    trTramo.Visible = false;
                    trMercado.Visible = false;
                    trPozo.Visible = false;
                    trPunto.Visible = true;
                    trContrato.Visible = true;
                    trCoincide.Visible = false;//20220202 verifica operativa
                    break;
            }
        }
        protected void LlenarTramo()
        {
            DdlTramo.Items.Clear();
            string[] lsNombreParametros = { "@P_codigo_trasportador" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int };
            string[] lValorParametros = { "0" };

            lConexion.Abrir();
            SqlDataReader lLector;
            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            DdlTramo.Items.Add(lItem);

            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetTramo", lsNombreParametros, lTipoparametros, lValorParametros);
            if (lLector.HasRows)
            {
                while (lLector.Read())
                {

                    ListItem lItem1 = new ListItem();
                    lItem1.Value = lLector.GetValue(0).ToString();
                    lItem1.Text = lLector["desc_tramo"].ToString();
                    DdlTramo.Items.Add(lItem1);
                }
            }
            lLector.Close();
            lConexion.Cerrar();
        }
        private int CantidadReg()
        {
            string[] lsNombreParametros = { "@P_tipo_reporte", "@P_ind_detalle", "@P_coincide" };
            SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { ddlReporte.SelectedValue, "R", DdlVerifica.SelectedValue  };
            int liReg = 0;
            lConexion.Abrir();
            SqlDataReader lLector;

            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetRepAudInfOpeTran", lsNombreParametros, lTipoparametros, lValorParametros);
            if (lLector.HasRows)
            {
                lLector.Read();
                liReg = Convert.ToInt32(lLector["cantidad_reg"].ToString());
            }
            else
                liReg = 0;
            lLector.Close();
            lConexion.Cerrar();
            lblTotal.Text = liReg.ToString("###,###,###,###,###,##0");
            return liReg;
        }
        protected void dtgReporte1_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgReporte1.CurrentPageIndex = e.NewPageIndex;
            traerDatos(dtgReporte1.CurrentPageIndex + 1);
        }
        protected void dtgReporte2_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgReporte2.CurrentPageIndex = e.NewPageIndex;
            traerDatos(dtgReporte2.CurrentPageIndex + 1);
        }
        protected void dtgReporte3_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgReporte3.CurrentPageIndex = e.NewPageIndex;
            traerDatos(dtgReporte3.CurrentPageIndex + 1);
        }
        protected void dtgReporte4_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgReporte4.CurrentPageIndex = e.NewPageIndex;
            traerDatos(dtgReporte4.CurrentPageIndex + 1);
        }
        protected void dtgReporte5_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgReporte5.CurrentPageIndex = e.NewPageIndex;
            traerDatos(dtgReporte5.CurrentPageIndex + 1);
        }
        public void traerDatos(int current)
        {
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            SqlCommand lComando = new SqlCommand();
            lComando.Connection = lConexion.gObjConexion;
            lComando.Parameters.Clear();
            lComando.CommandTimeout = 3600;
            lComando.CommandType = CommandType.StoredProcedure;
            lComando.CommandText = "pa_GetRepAudInfOpeTran";
            lComando.Parameters.Add("@P_tipo_reporte", SqlDbType.Int).Value = ddlReporte.SelectedValue;
            lComando.Parameters.Add("@P_ind_detalle", SqlDbType.VarChar).Value = "D";
            //lComando.Parameters.Add("@P_fecha_inicial", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
            //lComando.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaFin.Text.Trim();
            //lComando.Parameters.Add("@P_codigo_tramo", SqlDbType.Int).Value = DdlTramo.SelectedValue;
            //lComando.Parameters.Add("@P_tipo_mercado", SqlDbType.VarChar).Value = DdlMercado.SelectedValue;
            //lComando.Parameters.Add("@P_codigo_pozo", SqlDbType.Int).Value = DdlPozo.SelectedValue;
            //lComando.Parameters.Add("@P_codigo_punto_salida", SqlDbType.Int).Value = DdlPunto.SelectedValue;
            //lComando.Parameters.Add("@P_contrato_definitivo", SqlDbType.VarChar).Value = TxtContrato.Text;
            lComando.Parameters.Add("@PageIndex", SqlDbType.Int).Value = 1;//20220202 verifica operativa
            lComando.Parameters.Add("@PageSize", SqlDbType.Int).Value = 99999999; //20220202 verifica operativa
            lComando.Parameters.Add("@P_coincide", SqlDbType.VarChar).Value = DdlVerifica.SelectedValue ; //20220202 verifica operaqtiva

            lComando.ExecuteNonQuery();
            lsqldata.SelectCommand = lComando;
            lsqldata.Fill(lds);
            string lsvalida = "";//20161103 rq082-16
            switch (ddlReporte.SelectedValue)
            {
                case "1":
                    dtgReporte1.DataSource = lds;
                    dtgReporte1.DataBind();
                    dtgReporte1.Visible = true;
                    //20161103 rq082-16
                    //foreach (DataGridItem Grilla in this.dtgReporte1.Items)
                    //{
                    //    if (Grilla.Cells[10].Text != "1")
                    //    {
                    //        if (lsvalida == Grilla.Cells[0].Text + "-" + Grilla.Cells[1].Text + "-" + Grilla.Cells[2].Text + "-" + Grilla.Cells[11].Text)
                    //        {
                    //            Grilla.Cells[5].Text = "";
                    //            Grilla.Cells[6].Text = "";
                    //            Grilla.Cells[8].Text = "";
                    //            Grilla.Cells[9].Text = "";
                    //        }
                    //        lsvalida = Grilla.Cells[0].Text + "-" + Grilla.Cells[1].Text + "-" + Grilla.Cells[2].Text + "-" + Grilla.Cells[11].Text;
                    //    }
                    //}
                    break;
                case "2":
                    dtgReporte2.DataSource = lds;
                    dtgReporte2.DataBind();
                    dtgReporte2.Visible = true;
                    //20161103 rq082-16
                    //foreach (DataGridItem Grilla in this.dtgReporte2.Items)
                    //{
                    //    if (Grilla.Cells[9].Text != "1")
                    //    {
                    //        if (lsvalida == Grilla.Cells[0].Text + "-" + Grilla.Cells[1].Text + "-" + Grilla.Cells[2].Text + "-" + Grilla.Cells[10].Text)
                    //        {
                    //            Grilla.Cells[5].Text = "";
                    //            Grilla.Cells[7].Text = "";
                    //            Grilla.Cells[8].Text = "";
                    //        }
                    //        lsvalida = Grilla.Cells[0].Text + "-" + Grilla.Cells[1].Text + "-" + Grilla.Cells[2].Text + "-" + Grilla.Cells[10].Text;
                    //    }
                    //}
                    break;
                case "3":
                    dtgReporte3.DataSource = lds;
                    dtgReporte3.DataBind();
                    dtgReporte3.Visible = true;
                    //20220202
                    foreach (DataGridItem Grilla in this.dtgReporte3.Items)
                    {
                        if (Grilla.Cells[8].Text == "COINCIDE")
                            Grilla.Cells[8].ForeColor = System.Drawing.Color.Green;
                        else
                            Grilla.Cells[8].ForeColor = System.Drawing.Color.Red;
                    }
                    break;
                case "4":
                    dtgReporte4.DataSource = lds;
                    dtgReporte4.DataBind();
                    dtgReporte4.Visible = true;
                    //20220202
                    foreach (DataGridItem Grilla in this.dtgReporte4.Items)
                    {
                        if (Grilla.Cells[9].Text == "COINCIDE")
                            Grilla.Cells[9].ForeColor = System.Drawing.Color.Green;
                        else
                            Grilla.Cells[9].ForeColor = System.Drawing.Color.Red;
                    }
                    break;
                case "5":
                    dtgReporte5.DataSource = lds;
                    dtgReporte5.DataBind();
                    dtgReporte5.Visible = true;
                    break;
            }
            lConexion.Cerrar();
        }
        public void traerDatosExc()
        {
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            SqlCommand lComando = new SqlCommand();
            lComando.Connection = lConexion.gObjConexion;
            lComando.Parameters.Clear();
            lComando.CommandTimeout = 3600;
            lComando.CommandType = CommandType.StoredProcedure;
            lComando.CommandText = "pa_GetRepAudInfOpeTran";
            lComando.Parameters.Add("@P_tipo_reporte", SqlDbType.Int).Value = ddlReporte.SelectedValue;
            lComando.Parameters.Add("@P_ind_detalle", SqlDbType.VarChar).Value = "D";
            //lComando.Parameters.Add("@P_fecha_inicial", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
            //lComando.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaFin.Text.Trim();
            //lComando.Parameters.Add("@P_codigo_tramo", SqlDbType.Int).Value = DdlTramo.SelectedValue;
            //lComando.Parameters.Add("@P_tipo_mercado", SqlDbType.VarChar).Value = DdlMercado.SelectedValue;
            //lComando.Parameters.Add("@P_codigo_pozo", SqlDbType.Int).Value = DdlPozo.SelectedValue;
            //lComando.Parameters.Add("@P_codigo_punto_salida", SqlDbType.Int).Value = DdlPunto.SelectedValue;
            //lComando.Parameters.Add("@P_contrato_definitivo", SqlDbType.VarChar).Value = TxtContrato.Text;
            lComando.Parameters.Add("@PageIndex", SqlDbType.Int).Value = 1;
            lComando.Parameters.Add("@PageSize", SqlDbType.Int).Value = 999999999;
            lComando.Parameters.Add("@P_coincide", SqlDbType.VarChar).Value = DdlVerifica.SelectedValue; //20220202 verifica operaqtiva

            lComando.ExecuteNonQuery();
            lsqldata.SelectCommand = lComando;
            lsqldata.Fill(lds);
            string lsValida = ""; //20161103 rq082-16
            switch (ddlReporte.SelectedValue)
            {
                case "1":
                    dtgReporteExc1.DataSource = lds;
                    dtgReporteExc1.DataBind();
                    dtgReporteExc1.Visible = true;
                    //20161103 rq082-16
                    //foreach (DataGridItem Grilla in this.dtgReporteExc1.Items)
                    //{
                    //    if (Grilla.Cells[10].Text != "1")
                    //    {
                    //        if (lsValida == Grilla.Cells[0].Text + "-" + Grilla.Cells[1].Text + "-" + Grilla.Cells[2].Text + "-" + Grilla.Cells[11].Text)
                    //        {
                    //            Grilla.Cells[5].Text = "";
                    //            Grilla.Cells[6].Text = "";
                    //            Grilla.Cells[8].Text = "";
                    //            Grilla.Cells[9].Text = "";
                    //        }
                    //        lsValida = Grilla.Cells[0].Text + "-" + Grilla.Cells[1].Text + "-" + Grilla.Cells[2].Text + "-" + Grilla.Cells[11].Text;
                    //    }
                    //}
                    break;
                case "2":
                    dtgReporteExc2.DataSource = lds;
                    dtgReporteExc2.DataBind();
                    dtgReporteExc2.Visible = true;
                    //20161103 rq082-16
                    //foreach (DataGridItem Grilla in this.dtgReporteExc2.Items)
                    //{
                    //    if (Grilla.Cells[9].Text != "1")
                    //    {
                    //        if (lsValida == Grilla.Cells[0].Text + "-" + Grilla.Cells[1].Text + "-" + Grilla.Cells[2].Text + "-" + Grilla.Cells[10].Text)
                    //        {
                    //            Grilla.Cells[5].Text = "";
                    //            Grilla.Cells[7].Text = "";
                    //            Grilla.Cells[8].Text = "";
                    //        }
                    //        lsValida = Grilla.Cells[0].Text + "-" + Grilla.Cells[1].Text + "-" + Grilla.Cells[2].Text + "-" + Grilla.Cells[10].Text;
                    //    }
                    //}
                    break;
                case "3":
                    dtgReporteExc3.DataSource = lds;
                    dtgReporteExc3.DataBind();
                    dtgReporteExc3.Visible = true;
                    break;
                case "4":
                    dtgReporteExc4.DataSource = lds;
                    dtgReporteExc4.DataBind();
                    dtgReporteExc4.Visible = true;
                    break;
                case "5":
                    dtgReporteExc5.DataSource = lds;
                    dtgReporteExc5.DataBind();
                    dtgReporteExc5.Visible = true;
                    break;
            }
            lConexion.Cerrar();
        }
    }
}