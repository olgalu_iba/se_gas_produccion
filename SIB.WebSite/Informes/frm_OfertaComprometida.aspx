﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_OfertaComprometida.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Informes.frm_OfertaComprometida" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Inicial" AssociatedControlID="TxtFechaIni" runat="server" />
                            <asp:TextBox ID="TxtFechaIni" CssClass="form-control datepicker" placeholder="yyyy/mm/dd" ValidationGroup="detalle" Width="100%" runat="server" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Final" AssociatedControlID="TxtFechaFin" runat="server" />
                            <asp:TextBox ID="TxtFechaFin" CssClass="form-control datepicker" placeholder="yyyy/mm/dd" ValidationGroup="detalle" Width="100%" runat="server" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
                            <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fuente" AssociatedControlID="ddlFuente" runat="server" />
                            <asp:DropDownList ID="ddlFuente" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                </div>
                <div runat="server" id="tblGrilla" visible="false">
                    <div class="table table-responsive">
                        <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="false" AllowPaging="false" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered">
                            <Columns>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_fuente" HeaderText="Descripcion Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cnt_tak" HeaderText="Cantidad en Contratos 'Takeorpay' (MBTUD)" DataFormatString="{0:###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cnt_firme" HeaderText="Cantidad en Contratos 'Firmes' (MBTUD)" DataFormatString="{0:###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cnt_cond" HeaderText="Cantidad en Contratos 'Firmeza Condicionada' (MBTUD)" DataFormatString="{0:###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cnt_comp" HeaderText="Cantidad en Contratos 'Opción de Compra' (MBTUD)" DataFormatString="{0:###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cnt_exp" HeaderText="Cantidad en Contratos 'Opción de Compra de Gas Contra Exportaciones' (MBTUD)" DataFormatString="{0:###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <%--20171023 rq052-17--%>
                                <asp:BoundColumn DataField="cnt_cf95" HeaderText="Cantidad en Contratos Firmes al 95% (MBTUD)" DataFormatString="{0:###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <%--20171023 rq052-17--%>
                                <asp:BoundColumn DataField="cnt_c1" HeaderText="Cantidad en Contratos de Suministro C1 (MBTUD)" DataFormatString="{0:###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <%--20171023 rq052-17--%>
                                <asp:BoundColumn DataField="cnt_c2" HeaderText="Cantidad en Contratos de Suministro C2 (MBTUD)" DataFormatString="{0:###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="oferta_comprometida" HeaderText="Oferta Comprometida en Firme (MBTUD)" DataFormatString="{0:###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
