﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ReporteDesagregacionDia.aspx.cs"
    Inherits="Informes_frm_ReporteDesagregacionDia" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
                    width="80%">
                    <tr>
                        <td align="center" class="th1">
                            <asp:Label ID="lblTitulo" runat="server" ForeColor ="White"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br /><br /><br /><br /><br /><br />
                <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
                    width="80%">
                    <tr>
                        <td class="td1">
                            Reporte
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlReporte" runat="server" OnSelectedIndexChanged="ddlReporte_SelectedIndexChanged"
                                AutoPostBack="true">
                                <asp:ListItem Value="1">Nivel de contratación por modalidad y lugar de entrega - Suministro</asp:ListItem>
                                <asp:ListItem Value="2">Nivel de contratación por modalidad y ruta - Transporte</asp:ListItem>
                                <asp:ListItem Value="3">Contratación por agente - Suministro</asp:ListItem>
                                <asp:ListItem Value="4">Contratación por agente - Transporte</asp:ListItem>
                                <asp:ListItem Value="5">Contratación por sector de consumo - Suministro</asp:ListItem>
                                <asp:ListItem Value="6">Contratación por sector de consumo - Transporte</asp:ListItem>
                                <%--20180307 rq010-18--%>
                                <asp:ListItem Value="7">Nivel de contratación por modalidad, lugar de entrega y ruta - Suministro y Transporte</asp:ListItem>
                                <asp:ListItem Value="8">Contratación por agente - Suministro y Transporte</asp:ListItem>
                                <asp:ListItem Value="9">Contratación por sector de consumo - Suministro y Transporte</asp:ListItem>
                                <%--20180307 fin rq010-18--%>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            Fecha Inicial
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            Fecha Final
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="trFuente" runat="server">
                        <td class="td1">
                            Fuente
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlFuente" runat="server">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="S">Registro de contratos del sistema</asp:ListItem>
                                <asp:ListItem Value="E">Registro de contratos externos</asp:ListItem>
                                <asp:ListItem Value="M">Registro de contratos Mayoristas</asp:ListItem> <%--20170926 rq027-17--%>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trSubasta" runat="server">
                        <td class="td1">
                            Tipo Subasta
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlSubasta" runat="server" OnSelectedIndexChanged="DdlSubasta_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trRueda" runat="server">
                        <td class="td1">
                            Tipo Rueda
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlRueda" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trModalidad" runat="server">
                        <td class="td1">
                            Modalidad de contrato
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlModalidad" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trPunto" runat="server">
                        <td class="td1">
                            Punto Entrega
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlPunto" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <%--20180307 rq010-18--%>
                    <tr id="trPtoSal" runat="server" visible="false">
                        <td class="td1">
                            Punto Salida
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlPtoSal" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trRuta" runat="server" visible="false">
                        <td class="td1">
                            Ruta
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlRuta" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trMercado" runat="server">
                        <td class="td1">
                            Tipo Mercado
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlMercado" runat="server">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="P">Primario</asp:ListItem>
                                <asp:ListItem Value="S">Secundario</asp:ListItem>
                                <asp:ListItem Value="O">Otras Transacciones del Mercado Mayorista</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trRolV" runat="server" visible="false">
                        <td class="td1">
                            Rol Vendedor
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlRolVenta" runat="server" OnSelectedIndexChanged="DdlRolVenta_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trOperadorV" runat="server" visible="false">
                        <td class="td1">
                            Operador Vendedor
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlOperadorV" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trRolC" runat="server" visible="false">
                        <td class="td1">
                            Rol Comprador
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlRolCompra" runat="server" OnSelectedIndexChanged="DdlRolCompra_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trOperadorC" runat="server" visible="false">
                        <td class="td1">
                            Operador Comprador
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlOperadorC" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trDemanda" runat="server" visible="false">
                        <td class="td1">
                            Tipo Demanda
                        </td>
                        <%--20160706 sector de conusmo--%>
                        <td class="td2">
                            <asp:DropDownList ID="DdlDemanda" runat="server" OnSelectedIndexChanged="DdlDemanda_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <%--20160706--%>
                    <tr id="trSector" runat="server" visible="false">
                        <td class="td1">
                            Sector de Consumo
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="DdlSector" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblMensaje">
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 1050px; height: 350px;">
                    <asp:DataGrid ID="dtgReporte1" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="fecha" HeaderText="fecha" ItemStyle-HorizontalAlign="Center"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fuente" HeaderText="fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_subasta" HeaderText="Tipo subasta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_rueda" HeaderText="Tipo rueda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="Tipo mercado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20170926 rq027-17--%>
                            <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Tipo modalidad" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_punto" HeaderText="punto entrega" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--rq010-18--%>
                            <asp:BoundColumn DataField="cantidad" HeaderText="cantidad (MBTUD)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cnt_contratos" HeaderText="No. contratos" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
                            <%--20160602--%>
                            <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20160602--%>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="desc fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20210305 broker--%>
                            <asp:BoundColumn DataField="ind_broker" HeaderText="Contrato Broker" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgReporte2" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="fecha" HeaderText="fecha" ItemStyle-HorizontalAlign="Center"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fuente" HeaderText="fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_subasta" HeaderText="Tipo subasta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_rueda" HeaderText="Tipo rueda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="Tipo mercado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Tipo modalidad" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_ruta" HeaderText="ruta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--rq010-18--%>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Capacidad (KPCD)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cnt_contratos" HeaderText="No. contratos" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
                            <%--20210305 broker--%>
                            <asp:BoundColumn DataField="ind_broker" HeaderText="Contrato Broker" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgReporte3" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="fecha" HeaderText="fecha" ItemStyle-HorizontalAlign="Center"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="id_registro" HeaderText="No registro" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="contrato" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20180126 rq107-16--%>
                            <asp:BoundColumn DataField="numero_operacion" HeaderText="operación" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fuente" HeaderText="fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_subasta" HeaderText="Tipo subasta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_rueda" HeaderText="Tipo rueda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="Tipo mercado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20170926 rq027-17--%>
                            <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_rol_venta" HeaderText="rol venta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_venta" HeaderText="operador venta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_rol_compra" HeaderText="rol compra" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_compra" HeaderText="operador compra" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Tipo modalidad" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entega" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--rq010-18--%>
                            <asp:BoundColumn DataField="cantidad" HeaderText="cantidad (MBTUD)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,##0.00}"></asp:BoundColumn>
                            <%--20160602--%>
                            <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20160602--%>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="desc fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20180622--%>
                            <asp:BoundColumn DataField="nit_usr" HeaderText="Nit Usr Fin" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20180622--%>
                            <asp:BoundColumn DataField="nombre_usr" HeaderText="Nombre Usr Fin" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20210305 broker--%>
                            <asp:BoundColumn DataField="ind_broker" HeaderText="Contrato Broker" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgReporte4" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="fecha" HeaderText="fecha" ItemStyle-HorizontalAlign="Center"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="id_registro" HeaderText="No registro" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="contrato" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20180126 rq107-16--%>
                            <asp:BoundColumn DataField="numero_operacion" HeaderText="operación" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fuente" HeaderText="fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_subasta" HeaderText="Tipo subasta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_rueda" HeaderText="Tipo rueda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="Tipo mercado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_rol_venta" HeaderText="rol venta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_venta" HeaderText="operador venta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_rol_compra" HeaderText="rol compra" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_compra" HeaderText="operador compra" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Tipo modalidad" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Capacidad (KPCD)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,##0.00}"></asp:BoundColumn>
                            <%--20180622--%>
                            <asp:BoundColumn DataField="nit_usr" HeaderText="Nit Usr Fin" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20180622--%>
                            <asp:BoundColumn DataField="nombre_usr" HeaderText="Nombre Usr Fin" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20210305 broker--%>
                            <asp:BoundColumn DataField="ind_broker" HeaderText="Contrato Broker" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgReporte5" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="fecha" HeaderText="fecha" ItemStyle-HorizontalAlign="Center"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_subasta" HeaderText="Tipo subasta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_rueda" HeaderText="Tipo rueda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="contrato" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20180126 rq107-16--%>
                            <asp:BoundColumn DataField="numero_operacion" HeaderText="operación" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="id_registro" HeaderText="No registro" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_rol_venta" HeaderText="rol venta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_venta" HeaderText="operador venta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_rol_compra" HeaderText="rol compra" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_compra" HeaderText="operador compra" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="Tipo mercado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20170926 rq027-17--%>
                            <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--rq010-18--%>
                            <asp:BoundColumn DataField="cantidad" HeaderText="cantidad (MBTUD)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,##0.00}"></asp:BoundColumn>
                            <%--20180126 rq107-16--%>
                            <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Tipo modalidad" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_periodo" HeaderText="Periodo Entrega" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="no_identificacion_usr" HeaderText="Nit Usr final" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_usuario" HeaderText="Nombre" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_sector_consumo" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_punto_salida" HeaderText="punto salida" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_demanda" HeaderText="demanda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_contratada" HeaderText="cnt usr fin (MBTUD)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <%--20160602--%>
                            <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20160602--%>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="desc fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20210305 broker--%>
                            <asp:BoundColumn DataField="ind_broker" HeaderText="Contrato Broker" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgReporte6" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="fecha" HeaderText="fecha" ItemStyle-HorizontalAlign="Center"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_subasta" HeaderText="Tipo subasta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_rueda" HeaderText="Tipo rueda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="contrato" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20180126 rq107-16--%>
                            <asp:BoundColumn DataField="numero_operacion" HeaderText="operación" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="id_registro" HeaderText="No registro" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_rol_venta" HeaderText="rol venta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_venta" HeaderText="operador venta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_rol_compra" HeaderText="rol compra" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_compra" HeaderText="operador compra" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="Tipo mercado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Capacidad (KPCD)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,##0.00}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_ruta" HeaderText="ruta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Tipo modalidad" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_periodo" HeaderText="Periodo Entrega" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="no_identificacion_usr" HeaderText="Nit Usr final" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_usuario" HeaderText="Nombre" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_sector_consumo" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_punto_salida" HeaderText="punto salida" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_demanda" HeaderText="demanda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_contratada" HeaderText="cnt usr fin (KPCD)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <%--20210305 broker--%>
                            <asp:BoundColumn DataField="ind_broker" HeaderText="Contrato Broker" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <%--2018/03/07 rq010-18--%>
                    <asp:DataGrid ID="dtgReporte7" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="fecha" HeaderText="fecha" ItemStyle-HorizontalAlign="Center"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fuente" HeaderText="fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_subasta" HeaderText="Tipo subasta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_rueda" HeaderText="Tipo rueda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="Tipo mercado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Tipo modalidad" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_punto" HeaderText="Punto salida" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad (MBTUD)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="capacidad" HeaderText="Capacidad (KPCD)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cnt_contratos" HeaderText="No. contratos" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,##0}"></asp:BoundColumn>
                            <%--20210305 broker--%>
                            <asp:BoundColumn DataField="ind_broker" HeaderText="Contrato Broker" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgReporte8" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="fecha" HeaderText="fecha" ItemStyle-HorizontalAlign="Center"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="id_registro" HeaderText="No registro" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="contrato" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_operacion" HeaderText="operacion" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fuente" HeaderText="fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_subasta" HeaderText="Tipo subasta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_rueda" HeaderText="Tipo rueda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="Tipo mercado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_rol_venta" HeaderText="rol venta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_venta" HeaderText="operador venta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_rol_compra" HeaderText="rol compra" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_compra" HeaderText="operador compra" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Tipo modalidad" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_punto" HeaderText="Punto salida" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="cantidad (MBTUD)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="capacidad" HeaderText="capacidad (KPCD)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,##0.00}"></asp:BoundColumn>
                            <%--20180622--%>
                            <asp:BoundColumn DataField="nit_usr" HeaderText="Nit Usr Fin" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20180622--%>
                            <asp:BoundColumn DataField="nombre_usr" HeaderText="Nombre Usr Fin" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20210305 broker--%>
                            <asp:BoundColumn DataField="ind_broker" HeaderText="Contrato Broker" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgReporte9" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        Visible="false" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="fecha" HeaderText="fecha" ItemStyle-HorizontalAlign="Center"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_subasta" HeaderText="Tipo subasta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_rueda" HeaderText="Tipo rueda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="contrato" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_operacion" HeaderText="operacion" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="id_registro" HeaderText="No registro" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_rol_venta" HeaderText="rol venta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_venta" HeaderText="operador venta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_rol_compra" HeaderText="rol compra" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_compra" HeaderText="operador compra" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="Tipo mercado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="cantidad (MBTUD)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="capacidad" HeaderText="capacidad (KPCD)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,##0.00}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_punto" HeaderText="Punto salida" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Tipo modalidad" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="usr_final" HeaderText="Nit Usr final" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_usuario" HeaderText="Nombre" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_sector_consumo" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20210305 broker--%>
                            <asp:BoundColumn DataField="ind_broker" HeaderText="Contrato Broker" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <%--20180307 rq010-18 --%>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>