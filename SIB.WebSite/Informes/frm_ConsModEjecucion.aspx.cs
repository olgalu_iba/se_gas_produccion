﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;

public partial class Informes_frm_ConsModEjecucion : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Consulta de modificaciones de ejecución de contratos";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();
    string[] lsCarperta; //20171130 rq026-17
    string Carpeta; //20171130 rq026-17
    string lsCarpetaAnt = ""; //20171130 rq026-17


    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, DdlOperador, "m_operador", " estado = 'A' and codigo_operador !=0 order by codigo_operador", 0, 4);
            lConexion.Cerrar();
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF = DateTime.Now;
        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial. <br>";
            }
        }
        if (TxtFechaFin.Text.Trim().Length > 0)
        {
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
                if (TxtFechaIni.Text.Trim() != "")
                {
                    if (TxtFechaIni.Text.Trim().Length > 0 && ldFechaF < ldFechaI)
                        lblMensaje.Text += "La Fecha Final NO puede ser Menor que la Fecha Inicial. <br>";
                }
                else
                    lblMensaje.Text += "Debe digitar la Fecha inicial antes que la final. <br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Final. <br>";
            }
        }

        //if (TxtOperacionIni.Text == "" && TxtOperacionFin.Text != "")
        //    lblMensaje.Text = "Debe digitar la operación inicial antes que la final<br>";
        if (lblMensaje.Text == "")
        {
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetContEjecucionModRep";
                if (TxtOperacion.Text != "")
                    lComando.Parameters.Add("@P_numero_operacion", SqlDbType.Int).Value = TxtOperacion.Text;
                lComando.Parameters.Add("@P_contrato_definitivo", SqlDbType.VarChar).Value = TxtContrato.Text;
                if (TxtCodEjec.Text != "")
                    lComando.Parameters.Add("@P_codigo_ejecucion", SqlDbType.Int).Value = TxtCodEjec.Text;
                if (TxtId.Text != "")
                    lComando.Parameters.Add("@P_codigo_verif", SqlDbType.Int).Value = TxtId.Text;

                lComando.Parameters.Add("@P_fecha_inicial", SqlDbType.VarChar).Value = TxtFechaIni.Text;
                if (TxtFechaFin.Text != "")
                    lComando.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaFin.Text;
                else
                    lComando.Parameters.Add("@P_fecha_final", SqlDbType.VarChar).Value = TxtFechaIni.Text;
                lComando.Parameters.Add("@P_codigo_operador", SqlDbType.Char).Value = DdlOperador.SelectedValue;
                lComando.Parameters.Add("@P_punta", SqlDbType.Char).Value = DdlPunta.SelectedValue;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgMaestro.DataSource = lds;
                dtgMaestro.DataBind();
                tblGrilla.Visible = true;
                imbExcel.Visible = true;
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "ConsModContReg" + DateTime.Now + ".xls";
        string lstitulo_informe = "Informe Modificación de Ejecución de contratos";
        string lsTituloParametros = "";
        try
        {
            lsTituloParametros = "";
            if (TxtOperacion.Text != "")
                lsTituloParametros += " - Operación: " + TxtOperacion.Text;
            if (TxtContrato.Text != "")
                lsTituloParametros += " - Contrato : " + TxtContrato.Text;
            if (TxtCodEjec.Text != "")
                lsTituloParametros += " - Codigo ejecucion: " + TxtCodEjec.Text;
            if (TxtId.Text != "")
                lsTituloParametros += " - Id registro: " + TxtId.Text;
            if (TxtFechaIni.Text != "")
                lsTituloParametros += " - Fecha Inicial: " + TxtFechaIni.Text;
            if (TxtFechaFin.Text != "")
                lsTituloParametros += " - Fecha Final: " + TxtFechaFin.Text;
            if (DdlOperador.SelectedValue != "0")
                lsTituloParametros += "  - Tipo Mercado: " + DdlOperador.SelectedItem.ToString();
            if (DdlPunta.SelectedValue != "0")
                lsTituloParametros += "  - Punta: " + DdlPunta.SelectedItem.ToString();
            dtgMaestro.Columns[21].Visible = false; //20190524 rq029-19
            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgMaestro.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            lform.Controls.Add(dtgMaestro);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='6' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
            dtgMaestro.Columns[21].Visible = true; //20190524 rq029-19
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Excel.!" + ex.Message.ToString();
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        try
        {
            lblMensajeDet.Text = "";
            hdnCodigoC.Value = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[5].Text;
            hdnCodigoV.Value = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[6].Text;
            hdnContrato.Value = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[7].Text;
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            SqlCommand lComando = new SqlCommand();
            lComando.Connection = lConexion.gObjConexion;
            lComando.CommandTimeout = 3600;
            lComando.CommandType = CommandType.StoredProcedure;
            lComando.CommandText = "pa_GetContEjecucionModRepDet";
            lComando.Parameters.Add("@P_tipo_reporte", SqlDbType.VarChar).Value = DdlReporte.SelectedValue;
            lComando.Parameters.Add("@P_codigo_ejec_c", SqlDbType.Int).Value = hdnCodigoC.Value;
            lComando.Parameters.Add("@P_codigo_ejec_v", SqlDbType.Int).Value = hdnCodigoV.Value;
            lComando.ExecuteNonQuery();
            lsqldata.SelectCommand = lComando;
            lsqldata.Fill(lds);
            if (DdlReporte.SelectedValue == "C")
            {
                dtgDetalle.Visible = true;
                dtgDetalleUsr.Visible = false;
                dtgDetalle.DataSource = lds;
                dtgDetalle.DataBind();
                lConexion.Cerrar();
                tblGrilla.Visible = false;
                tbDetalle.Visible = true;
                tblMensaje.Visible = false;
                tblDatos.Visible = false;
                //REcorro la grilla para mostrar los cambios en negrilla
                string sPunta = "";
                string sCantidad = "";
                string sValor = "";
                //string sDemanda = ""; //20190524 rq019-19
                //string sSector = ""; //20190524 rq019-19
                int liConta = 0;
                foreach (DataGridItem Grilla in this.dtgDetalle.Items)
                {
                    liConta++;
                    if (liConta == 1)
                    {
                        sPunta = Grilla.Cells[8].Text;
                        sCantidad = Grilla.Cells[15].Text;
                        sValor = Grilla.Cells[16].Text;
                        //sDemanda = Grilla.Cells[13].Text;  //20190524 rq019-19
                        //sSector = Grilla.Cells[15].Text; //20190524 rq019-19
                    }
                    else
                    {
                        if (sPunta == Grilla.Cells[8].Text)
                        {
                            if (sCantidad != Grilla.Cells[15].Text)
                                Grilla.Cells[15].Font.Bold = true;
                            if (sValor != Grilla.Cells[16].Text)
                                Grilla.Cells[16].Font.Bold = true;
                            //20190524 rq019-19
                            //if (sDemanda != Grilla.Cells[13].Text)
                            //{
                            //    Grilla.Cells[13].Font.Bold = true;
                            //    Grilla.Cells[14].Font.Bold = true;
                            //}
                            //if (sSector != Grilla.Cells[15].Text)
                            //{
                            //    Grilla.Cells[15].Font.Bold = true;
                            //    Grilla.Cells[16].Font.Bold = true;
                            //}
                            //20190524 fin rq019-19
                        }
                        else
                            sPunta = Grilla.Cells[8].Text;
                        sCantidad = Grilla.Cells[15].Text;
                        sValor = Grilla.Cells[16].Text;
                        //sDemanda = Grilla.Cells[13].Text; //20190524 rq019-19
                        //sSector = Grilla.Cells[15].Text; //20190524 rq019-19
                    }
                }
            }
            else
            {
                dtgDetalle.Visible = false;
                dtgDetalleUsr.Visible = true;
                dtgDetalleUsr.DataSource = lds;
                dtgDetalleUsr.DataBind();
                lConexion.Cerrar();
                tblGrilla.Visible = false;
                tbDetalle.Visible = true;
                tblMensaje.Visible = false;
                tblDatos.Visible = false;
                //REcorro la grilla para mostrar los cambios en negrilla
                string sDemanda = ""; //20190524 rq019-19
                string sSector = ""; //20190524 rq019-19
                string sSalida= ""; //20190524 rq019-19
                string sUsuario = "";
                string sMercado = "";
                string sCantidad = ""; //20190524 rq019-19
                int liConta = 0;
                foreach (DataGridItem Grilla in this.dtgDetalleUsr.Items)
                {
                    liConta++;
                    if (liConta == 1)
                    {
                        sDemanda = Grilla.Cells[9].Text;  //20190524 rq019-19
                        sSector = Grilla.Cells[11].Text; //20190524 rq019-19
                        sSalida= Grilla.Cells[13].Text; //20190524 rq019-19
                        sUsuario = Grilla.Cells[15].Text;
                        sMercado = Grilla.Cells[17].Text;
                        sCantidad= Grilla.Cells[19].Text; //20190524 rq019-19
                    }
                    else
                    {
                        //20190524 rq019-19
                        if (sDemanda != Grilla.Cells[9].Text)
                        {
                            Grilla.Cells[9].Font.Bold = true;
                            Grilla.Cells[10].Font.Bold = true;
                        }
                        if (sSector != Grilla.Cells[11].Text)
                        {
                            Grilla.Cells[11].Font.Bold = true;
                            Grilla.Cells[12].Font.Bold = true;
                        }
                        if (sSalida!= Grilla.Cells[13].Text)
                        {
                            Grilla.Cells[13].Font.Bold = true;
                            Grilla.Cells[14].Font.Bold = true;
                        }
                        //20190524 fin rq019-19
                        if (sUsuario != Grilla.Cells[15].Text)
                        {
                            Grilla.Cells[15].Font.Bold = true;
                            Grilla.Cells[16].Font.Bold = true;
                        }
                        if (sMercado != Grilla.Cells[17].Text)
                        {
                            Grilla.Cells[17].Font.Bold = true;
                            Grilla.Cells[18].Font.Bold = true;
                        }
                        //20190524 fin rq019-19
                        if (sCantidad!= Grilla.Cells[19].Text)
                            Grilla.Cells[19].Font.Bold = true;
                        sDemanda = Grilla.Cells[9].Text;  //20190524 rq019-19
                        sSector = Grilla.Cells[11].Text; //20190524 rq019-19
                        sSalida= Grilla.Cells[13].Text; //20190524 rq019-19
                        sUsuario = Grilla.Cells[15].Text;
                        sMercado = Grilla.Cells[17].Text;
                        sCantidad =  Grilla.Cells[19].Text; //20190524 rq019-19
                    }
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Error al consultar el detalle. " + ex.Message.ToString();
        }

    }
    protected void BtnRegresar_Click(object sender, EventArgs e)
    {
        tblGrilla.Visible = true;
        tbDetalle.Visible = false;
        tblMensaje.Visible = true;
        tblDatos.Visible = true;
    }

    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel1_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "ConsModEjecucionDet" + DateTime.Now + ".xls";
        try
        {
            string lstitulo_informe = "";
            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            if (DdlReporte.SelectedValue == "C")
            {
                lstitulo_informe = "Detalle de modificaciones de la operación " + hdnContrato.Value;
                dtgDetalle.EnableViewState = false;
                lform.Controls.Add(dtgDetalle);
            }
            else
            {
                lstitulo_informe = "Detalle de modificaciones Usuario Final de la operación " + hdnContrato.Value;
                dtgDetalleUsr.EnableViewState = false;
                lform.Controls.Add(dtgDetalleUsr);
            }
                
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Excel.!" + ex.Message.ToString();
        }
    }
}
