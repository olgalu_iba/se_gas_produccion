﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_InformacionComMpUVLP.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_InformacionComMpUVLP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">

        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }

        function formatoMiles(obj, evt) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /([0-9]*)$/i.exec(strValue)) != null) {
                obj.value = addCommas(T[1]);
            } else {
                alert('formato invalido ');
            }
        }

        function formatoMilesPre(obj, evt, decimales) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
                T[4] = T[4].substr(0, decimales);
                if (T[2] == '' && T[3] == '.') {
                    T[2] = 0;
                }
                obj.value = T[1] + addCommas(T[2]) + T[3] + T[4];
            }
            else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }

    </script>


    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" bgcolor="#85BF46">
                <table border="0" cellspacing="2" cellpadding="2" align="right">
                    <tr>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkNuevo" runat="server" NavigateUrl="~/Procesos/frm_InformacionComMpUVLP.aspx?lsIndica=N">Nuevo</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_InformacionComMpUVLP.aspx?lsIndica=L">Listar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_InformacionComMpUVLP.aspx?lsIndica=B">Consultar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/frm_contenido.aspx">Salir</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <%--<td class="tv2">
                            <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click" />
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgPdf" runat="server" ImageUrl="~/Images/Pdf.png" OnClick="ImgPdf_Click"
                                Width="70%" />
                        </td>
                        <td class="tv2">
                        </td>--%>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Subasta
            </td>
            <td class="td2">
                <asp:Label ID="lblCodSubasta" runat="server"> </asp:Label>
                -
                <asp:Label ID="lblDescSubasta" runat="server"> </asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Operador
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlOperador" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Periodo de entrega
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlPeriodo" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Punto Entrega
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlPunto" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPunto_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Meses vigentes
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtMesVig" runat="server" MaxLength="2" Width="100px" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="2">
                <asp:DataGrid ID="dtgDetalle" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="th1">
                    <Columns>
                        <asp:BoundColumn DataField="codigo_tramo" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="punto_inicio" HeaderText="Inicio"></asp:BoundColumn>
                        <asp:BoundColumn DataField="punto_fin" HeaderText="Fin"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Energ Cont">
                            <ItemTemplate>
                                <asp:TextBox ID="TxtEnergCont" runat="server" Width="100px" onkeyup="return formatoMiles(this,event,2);"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FTBETxtEnergCont" runat="server" TargetControlID="TxtEnergCont"
                                    FilterType="Custom, Numbers" ValidChars=",">
                                </ajaxToolkit:FilteredTextBoxExtender>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Energ Demand">
                            <ItemTemplate>
                                <asp:TextBox ID="TxtEenergDemand" runat="server" Width="100px" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FTBETxtEenergDemand" runat="server" TargetControlID="TxtEenergDemand"
                                    FilterType="Custom, Numbers" ValidChars=",">
                                </ajaxToolkit:FilteredTextBoxExtender>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Capac Trans">
                            <ItemTemplate>
                                <asp:TextBox ID="TxtCapacTrans" runat="server" Width="100px" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FTBETxtCapacTrans" runat="server" TargetControlID="TxtCapacTrans"
                                    FilterType="Custom, Numbers" ValidChars=",">
                                </ajaxToolkit:FilteredTextBoxExtender>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Cargos Fijos">
                            <ItemTemplate>
                                <asp:TextBox ID="TxtCargoFijo" runat="server" Width="100px" onkeyup="return formatoMilesPre(this,event);"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FTBETxtCargoFijo" runat="server" TargetControlID="TxtCargoFijo"
                                    FilterType="Custom, Numbers" ValidChars=".,">
                                </ajaxToolkit:FilteredTextBoxExtender>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Gast admon">
                            <ItemTemplate>
                                <asp:TextBox ID="TxtGastoAdmon" runat="server" Width="100px" onkeyup="return formatoMilesPre(this,event);"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FTBETxtTxtGastoAdmon" runat="server" TargetControlID="TxtGastoAdmon"
                                    FilterType="Custom, Numbers" ValidChars=".,">
                                </ajaxToolkit:FilteredTextBoxExtender>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Cargo var">
                            <ItemTemplate>
                                <asp:TextBox ID="TxtCargoVar" runat="server" Width="100px" onkeyup="return formatoMilesPre(this,event);"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FTBETxtCargoVar" runat="server" TargetControlID="TxtCargoVar"
                                    FilterType="Custom, Numbers" ValidChars=".," >
                                </ajaxToolkit:FilteredTextBoxExtender>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="energia_contratada" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="energia_demandada" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="capac_trans_contratada" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="cargos_fijos" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="gastos_admon" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="cargos_variables" Visible="false"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblBuscar" visible="false">
        <tr>
            <td class="td1">
                Rueda
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusSubasta" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Operador
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusOperador" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Sitio Entrega
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusPunto" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
        width="80%">
        <tr>
            <td colspan="2" align="center">
                <div>
                    <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgConsulta_EditCommand" OnPageIndexChanged="dtgConsulta_PageIndexChanged"
                        HeaderStyle-CssClass="th1" PageSize="30">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="codigo_capacidad_exc" HeaderText="cod" Visible="false">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_rueda" HeaderText="Rueda" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Ope" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_punto_entrega" HeaderText="Cod Pto Ent" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_punto_ent" HeaderText="desc punto ent" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar"></asp:EditCommandColumn>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </div>
                <asp:HiddenField ID="hndCodigo" runat="server" />
                <asp:HiddenField ID="hndUsrHab" runat="server" />
                <asp:HiddenField ID="hndFecha" runat="server" />
            </td>
        </tr>
    </table>

</asp:Content>
