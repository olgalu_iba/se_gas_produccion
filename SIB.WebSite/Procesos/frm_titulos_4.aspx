﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_titulos_4.aspx.cs" Inherits="Procesos_frm_titulos_4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="divOferta" runat="server" style="position: absolute; left: 0; top: 0; width: 100%;">
                <table cellpadding="0" cellspacing="0" bgcolor="#000000" align="left">
                    <tr>
                        <td align="center">
                            <asp:DataGrid ID="dtgSubasta4" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                ItemStyle-CssClass="td2" HeaderStyle-CssClass="th2" runat="server">
                                <Columns>
                                    <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" HeaderStyle-Width="60px">
                                    </asp:BoundColumn>
                                    <%--1--%><asp:BoundColumn DataField="desc_modalidad" HeaderText="Tipo" HeaderStyle-Width="100px">
                                    </asp:BoundColumn>
                                    <%--2--%><asp:BoundColumn DataField="desc_periodo" HeaderText="Duración" HeaderStyle-Width="130px"> <%--20180815 BUG230--%>
                                    </asp:BoundColumn>
                                    <%--3--%><asp:BoundColumn DataField="desc_punto_entrega" HeaderText="lugar" HeaderStyle-Width="250px">
                                    </asp:BoundColumn>
                                    <%--4--%><asp:BoundColumn DataField="cantidad_venta" HeaderText="Cantidad venta"
                                        HeaderStyle-Width="100px"></asp:BoundColumn>
                                    <%--5--%><asp:BoundColumn DataField="precio_venta" HeaderText="Precio venta" HeaderStyle-Width="70px">
                                    </asp:BoundColumn>
                                    <%--6--%><asp:BoundColumn DataField="precio_delta" HeaderText="Precio delta" HeaderStyle-Width="70px">
                                    </asp:BoundColumn>
                                    <%--7--%><asp:BoundColumn DataField="cantidad_adjudicada" HeaderText="cnt adj" HeaderStyle-Width="100px"
                                        Visible="false"></asp:BoundColumn>
                                    <%--8--%><asp:BoundColumn DataField="precio_adjudicado" HeaderText="precio adj" HeaderStyle-Width="70px"
                                        Visible="false"></asp:BoundColumn>
                                    <%--9--%><asp:BoundColumn DataField="mejor_postura" Visible="false" HeaderStyle-Width="60px">
                                    </asp:BoundColumn>
                                    <%--10--%><asp:BoundColumn DataField="hora_prox_fase" HeaderText="Hora Próx fase" 
                                        HeaderStyle-Width="60px" Visible="false"></asp:BoundColumn> <%--20180815 BUG230--%>
                                    <%--11--%><asp:BoundColumn DataField="estado" HeaderText="Estado" HeaderStyle-Width="100px">
                                    </asp:BoundColumn>
                                    <%--12--%><asp:TemplateColumn HeaderText="Ofr" HeaderStyle-Width="60px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbOfertar" runat="server" ToolTip="Ofertar" ImageUrl="~/Images/nuevo.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--13--%><asp:TemplateColumn HeaderText="Mod" HeaderStyle-Width="60px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbModificar" runat="server" ToolTip="Modificar Oferta" ImageUrl="~/Images/modificar.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--14--%><asp:TemplateColumn HeaderText="Elim" HeaderStyle-Width="60px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbEliminar" runat="server" ToolTip="Eliminar Oferta" ImageUrl="~/Images/eliminar.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--15--%><asp:TemplateColumn HeaderText="gráfica" HeaderStyle-Width="60px"> <%--20180815 BUG230--%>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbGrafica" runat="server" ToolTip="curva agregada" ImageUrl="~/Images/grafica.jpg" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--16--%><asp:BoundColumn DataField="ind_vendedor" Visible="false" HeaderStyle-Width="60px">
                                    </asp:BoundColumn>
                                    <%--17--%><asp:BoundColumn DataField="ind_postura" Visible="false" HeaderStyle-Width="60px">
                                    </asp:BoundColumn>
                                    <%--18--%><asp:BoundColumn DataField="no_posturas_compra" Visible="false" HeaderStyle-Width="60px">
                                    </asp:BoundColumn>
                                    <%--19--%><asp:BoundColumn DataField="habilita_c" Visible="false" HeaderStyle-Width="60px">
                                    </asp:BoundColumn>
                                    <%--20--%><asp:BoundColumn DataField="habilita_v" Visible="false" HeaderStyle-Width="60px">
                                    </asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="th2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="11px">
                                </HeaderStyle>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Panel1" runat="server">
    </asp:Panel>
  </asp:Content> 