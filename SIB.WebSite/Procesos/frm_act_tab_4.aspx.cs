﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using PCD_Infraestructura.Business;
using PCD_Infraestructura.Transaction;
using PCD_Infraestructura.DomainLayer;
using System.Diagnostics;

public partial class OMA_frm_act_tab_4 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    String strRutaArchivo1;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        strRutaArchivo1 = ConfigurationManager.AppSettings["RutaArchivos"].ToString();
    }
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        
        //string oArchivo = strRutaArchivo1 + "Mensaje.txt";
        //if (Session["hora_mensaje"].ToString() != File.GetCreationTime(oArchivo).ToString("HH:mm:ss") )
        //{
        //    Session["hora_mensaje"] = File.GetCreationTime(oArchivo).ToString("HH:mm:ss");
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.mensajes.location= 'frm_mensajes_1.aspx';", true);
        //}
        if (Session["mis_ofertas"].ToString() == "S")
        {
            Session["mis_ofertas"] = "N";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.ofertas.location= 'frm_mis_ofertas_4.aspx';", true);
        }
    }
}
