﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_contratoBilateral.aspx.cs"
    Inherits="Procesos_frm_contratoBilateral" validateRequest="false" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link href="../css/estilo.css" rel="stylesheet" type="text/css" />
</head>
<body background="../Images/IMG_FondoGrisDeg.png">
    <form id="frmComisionista" runat="server">
        <asp:ScriptManager ID="ScriptManagerMestro" runat="server">
        </asp:ScriptManager>
        <br />
        <div id="divPregunta" runat="server" visible="false" style="width: 500PX; height: 150PX; position: absolute; left: 450px; top: 150px; border: solid; background-color: white;">
            <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
                id="Table1">
                <tr>
                    <td align="center" class="th3">
                        <asp:Label ID="Label1" runat="server" Text="FECHA MAXIMA DE REGISTRO DE OPERACIONES BILATERALES"></asp:Label>
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td align="center" class="td2">
                        <asp:Label ID="lblMensajeReg" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" class="td2">
                        <asp:Button ID="BtnAceptar" runat="server" Text="Aceptar" OnClick="BtnAceptar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" CssClass="button1" />
                        &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="BtnCancelar" runat="server" Text="Cancelar" OnClick="BtnCancelar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" CssClass="button1" />
                    </td>
                </tr>
            </table>
        </div>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
            id="tblTitulo">
            <tr>
                <td align="center" class="th3">
                    <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    <asp:HiddenField ID="hndContrato" runat="server" />
                    <asp:HiddenField ID="hndIndica" runat="server" />
                    <asp:HiddenField ID="hndConsec" value="0" runat="server" />
                    <asp:HiddenField ID="hndActual" value="0" runat="server" />
                    <asp:HiddenField ID="hndIndModif" value="0" runat="server" />
                </td>
            </tr>
        </table>
        <br />
        <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
            width="100%">
            <tr>
                <td class="td1">Operación inicial
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtBusContratoIni" runat="server" ValidationGroup="detalle" Width="80px"></asp:TextBox>
                </td>
                <td class="td1">Operación final
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtBusContratoFin" runat="server" ValidationGroup="detalle" Width="80px"></asp:TextBox>
                </td>
                <td class="td1">Fecha Negociación Inicial
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtBusFechaIni" runat="server" ValidationGroup="detalle" Width="80px"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="ceTxtBusFechaIni" runat="server" TargetControlID="TxtBusFechaIni"
                        Format="yyyy/MM/dd">
                    </ajaxToolkit:CalendarExtender>
                </td>
                <td class="td1">Fecha Negociación final
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtBusFechaFin" runat="server" ValidationGroup="detalle" Width="80px"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="ceTxtBusFechaFin" runat="server" TargetControlID="TxtBusFechaFin"
                        Format="yyyy/MM/dd">
                    </ajaxToolkit:CalendarExtender>
                </td>
                <td class="td1">Número Contrato
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtBusContDef" runat="server" ValidationGroup="detalle" Width="80px"></asp:TextBox>
                </td>
            </tr>
            <tr id="tr01" runat="server">

                <td class="td1">Comprador
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlBusComprador" runat="server" Width="120px">
                    </asp:DropDownList>
                </td>
                <td class="td1">Vendedor
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlBusVendedor" runat="server" Width="120px">
                    </asp:DropDownList>
                </td>
                <td class="td1">Mercado
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlBusMercado" runat="server">
                        <asp:ListItem Value="">Seleccione</asp:ListItem>
                        <asp:ListItem Value="P">Primario</asp:ListItem>
                        <asp:ListItem Value="S">Secundario</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="td1">Producto
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlBusProducto" runat="server">
                        <asp:ListItem Value="">Seleccione</asp:ListItem>
                        <asp:ListItem Value="1">Suministro de gas</asp:ListItem>
                        <asp:ListItem Value="2">Capacidad de transporte</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="td1">Modalidad
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlBusModalidad" runat="server" Width="100px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="tr02" runat="server">

                <td class="td1">Contraparte
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlBusContra" runat="server" Width="120px">
                    </asp:DropDownList>
                </td>

                <td class="td1">Mercado
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlBusMercado1" runat="server">
                        <asp:ListItem Value="">Seleccione</asp:ListItem>
                        <asp:ListItem Value="P">Primario</asp:ListItem>
                        <asp:ListItem Value="S">Secundario</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="td1">Producto
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlBusProducto1" runat="server">
                        <asp:ListItem Value="">Seleccione</asp:ListItem>
                        <asp:ListItem Value="1">Suministro de gas</asp:ListItem>
                        <asp:ListItem Value="2">Capacidad de transporte</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="td1">Modalidad
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlBusModalidad1" runat="server" Width="100px">
                    </asp:DropDownList>
                </td>
                <td class="td1">Estado
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlBusEstado" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="th1" colspan="10" align="center">

                    <asp:Button ID="btnConsultar" runat="server" Text="Consultar" OnClick="btnConsultar_Click" CssClass="button1" />
                    <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" OnClick="btnNuevo_Click" CssClass="button1" />
                </td>
            </tr>
        </table>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblMensaje">
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>

        <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
            id="tblGrilla" visible="false">
            <tr>
                <td colspan="3" align="center">
                    <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                        AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1"
                        OnEditCommand="dtgConsulta_EditCommand" AllowPaging="true" PageSize="20" OnPageIndexChanged="dtgConsulta_PageIndexChanged">
                        <Columns>
                            <asp:BoundColumn DataField="puede_modif" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="No. Operacion" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_id" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociacion" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_compra" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_venta" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_mercado" HeaderText="Mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_punto" HeaderText="Punto/Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Consultar" EditText="Consultar"></asp:EditCommandColumn>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar"></asp:EditCommandColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
            id="tblCaptura" visible="false">
            <tr>
                <td class="td1">Destino Rueda
                </td>
                <td class="td2">
                    <asp:DropDownList ID="DdlDestino" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlDestino_SelectedIndexChanged">
                        <asp:ListItem Value="G" Text="Suministro de Gas"></asp:ListItem>
                        <asp:ListItem Value="T" Text="Capacidad de Transporte"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="td1">Tipo Mercado
                </td>
                <td class="td2">
                    <asp:DropDownList ID="DdlMercado" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlMercado_SelectedIndexChanged">
                        <asp:ListItem Value="P" Text="Primario"></asp:ListItem>
                        <asp:ListItem Value="S" Text="Secundario"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">Operador Comprador
                </td>
                <td class="td2">
                    <asp:DropDownList ID="DdlOperador" runat="server" Width="300px">
                    </asp:DropDownList>
                </td>
                <td class="td1">Fecha-hora Negociación
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtFechaNeg" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="ceTxtFechaNeg" runat="server" TargetControlID="TxtFechaNeg"
                        Format="yyyy/MM/dd">
                    </ajaxToolkit:CalendarExtender>
                    -
                <asp:TextBox ID="TxtHoraNeg" runat="server" MaxLength="5" Width="50px"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revTxtHoraNeg" ControlToValidate="TxtHoraNeg"
                        ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                        ErrorMessage="Formato Incorrecto para la Hora de negociación"> * </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="td1">Fecha Suscripción
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtFechaSus" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TxtFechaSus"
                        Format="yyyy/MM/dd">
                    </ajaxToolkit:CalendarExtender>
                </td>
                <td class="td1">
                    <asp:Label ID="LblFuente" runat="server">Fuente</asp:Label>
                </td>
                <td class="td2">
                    <asp:DropDownList ID="DdlFuente" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="trSumSec" runat="server" visible="false">
                <td class="td1">Conectado al SNT
                </td>
                <td class="td2">
                    <asp:DropDownList ID="DdlConectado" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlConectado_SelectedIndexChanged">
                        <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                        <asp:ListItem Value="N" Text="No"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="td1">Entrega en boca de bozo
                </td>
                <td class="td2">
                    <asp:DropDownList ID="DdlBoca" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlBoca_SelectedIndexChanged">
                        <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                        <asp:ListItem Value="N" Text="No" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">
                    <asp:Label ID="LblPunto" runat="server">Punto de entrega de la energía al comprador</asp:Label>
                </td>
                <td class="td2">
                    <asp:DropDownList ID="DdlPunto" runat="server" Width="300px">
                    </asp:DropDownList>
                    <asp:DropDownList ID="DdlCentro" runat="server" Width="300px" Visible="false">
                    </asp:DropDownList>
                </td>
                <td class="td1">Modalidad de Entrega
                </td>
                <td class="td2">
                    <asp:DropDownList ID="DdlModalidad" runat="server" Width="300px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">Periodo de Entrega
                </td>
                <td class="td2">
                    <asp:DropDownList ID="DdlPeriodo" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlPeriodo_SelectedIndexChanged"
                        Width="300px">
                    </asp:DropDownList>
                    <asp:HiddenField ID="HndTiempo" runat="server" />
                    <asp:HiddenField ID="hdfMesInicialPeriodo" runat="server" />
                    <asp:HiddenField ID="hdfDiasIni" runat="server" />
                </td>
                <td class="td1">
                    <asp:Label ID="LblNoAños" runat="server" Visible="false">No Años</asp:Label>
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtNoAños" runat="server" MaxLength="3" Width="50px" Visible="false"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtNoAños" runat="server" Enabled="True" FilterType="Custom, Numbers"
                        TargetControlID="TxtNoAños">
                    </ajaxToolkit:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td class="td1">Fecha Entrega Inicial
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtFechaIni" runat="server" MaxLength="10" Width="150px" OnTextChanged="TxtFechaIni_TextChanged"
                        AutoPostBack="true"></asp:TextBox>
                    <asp:HiddenField ID="hdfErrorFecha" runat="server" />
                    <ajaxToolkit:CalendarExtender ID="CeTxtFechaIni" runat="server" TargetControlID="TxtFechaIni"
                        Format="yyyy/MM/dd">
                    </ajaxToolkit:CalendarExtender>
                    -
                <asp:TextBox ID="TxtHoraIni" runat="server" MaxLength="5" Width="50px" Visible="false"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RevTxtHoraIni" ControlToValidate="TxtHoraIni"
                        ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                        ErrorMessage="Formato Incorrecto para la Hora de entrega inicial"> * </asp:RegularExpressionValidator>
                </td>
                <td class="td1">Fecha Entrega Final
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtFechaFin" runat="server" MaxLength="10" Width="150px" Enabled="false"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="ceTxtFechaFin" runat="server" TargetControlID="TxtFechaFin"
                        Format="yyyy/MM/dd">
                    </ajaxToolkit:CalendarExtender>
                    -
                <asp:TextBox ID="TxtHoraFin" runat="server" MaxLength="5" Width="50px" Visible="false"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revTxtHoraFin" ControlToValidate="TxtHoraFin"
                        ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                        ErrorMessage="Formato Incorrecto para la Hora de entrega final"> * </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="td1">
                    <asp:Label ID="LblCantidad" runat="server">Cantidad de energía contratada (MBTUD)</asp:Label>
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtCantidad" runat="server" MaxLength="6" Width="50px"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtCantidad" runat="server" Enabled="True" FilterType="Custom, Numbers"
                        TargetControlID="TxtCantidad">
                    </ajaxToolkit:FilteredTextBoxExtender>
                </td>
                <td class="td1">
                    <asp:Label ID="LblPrecio" runat="server">Precio a la fecha de suscripción del contrato (USD/MBTU)</asp:Label>
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtPrecio" runat="server" MaxLength="6" Width="50px"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="True"
                        FilterType="Custom, Numbers" TargetControlID="TxtPrecio" ValidChars=".">
                    </ajaxToolkit:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr id="trFlujo" runat="server" visible="false">
                <td class="td1">Sentido del flujo
                </td>
                <td class="td2">
                    <asp:DropDownList ID="DdlSentido" runat="server">
                        <asp:ListItem Value="NORMAL" Text="NORMAL"></asp:ListItem>
                        <asp:ListItem Value="CONTRAFLUJO" Text="CONTRAFLUJO"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="td1">Presión del punto final
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtPresion" runat="server" MaxLength="500" Width="300px" Text="0"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtPresion" runat="server" Enabled="True" FilterType="Custom, Numbers"
                        TargetControlID="TxtPresion" ValidChars="-">
                    </ajaxToolkit:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td class="td1">Contrato definitivo
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtContratoDef" runat="server" MaxLength="30" Width="100px"></asp:TextBox>
                </td>
                <td class="td1">Contrato Variable
                </td>
                <td class="td2">
                    <asp:DropDownList ID="DdlVariable" runat="server">
                        <asp:ListItem Value="N" Text="No"></asp:ListItem>
                        <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="th1" align="left">
                    <asp:Button ID="btnAnt" runat="server" Text="Anterior" OnClick="btnAnt_Click" CssClass="button1"  />
                    <asp:Button ID="btnSig" runat="server" Text="Siguiente" OnClick="btnSig_Click" CssClass="button1"  />

                </td>
                <td class="th1" colspan="3" align="center">
                    <asp:Button ID="btnInsertar" runat="server" Text="Insertar Otro" OnClick="btnInsertar_Click" CssClass="button1" ValidationGroup="comi"/>
                    <asp:Button ID="btnCrear" runat="server" Text="Crear" OnClick="btnCrear_Click" CssClass="button1" ValidationGroup="comi" />
                    <asp:Button ID="btnModificar" runat="server" Text="Modificar" OnClick="btnModificar_Click" CssClass="button1" ValidationGroup="comi"/>
                    <asp:Button ID="btnLimpiar" runat="server" Text="Limpiar Campos" OnClick="btnLimpiar_Click" CssClass="button1"  />
                    <asp:Button ID="btnSalir" runat="server" Text="Salir" OnClick="btnSalir_Click" CssClass="button1"  />
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <asp:ValidationSummary ID="comi" runat="server" ValidationGroup="comi" />
                </td>
            </tr>
        </table>
        <br />

    </form>
</body>
</html>
