﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;
using AjaxControlToolkit;
using Segas.Web.Elements;

public partial class Procesos_frm_titulos_6 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    //String strRutaArchivo;
    private System.TimeSpan diffResult;
    static String lsCadenaCon;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            try
            {
                lsCadenaCon = "Data Source=" + goInfo.Servidor.ToString() + "; Initial Catalog=" + goInfo.BaseDatos.ToString() + ";User ID = " + goInfo.Usuario.ToString() + "; Password=" + goInfo.Password.ToString();
                lConexion1 = new clConexion(goInfo);
                cargarDatosGrilla();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, ex.Message);
            }
        }
    }
    /// <summary>
    /// Nombre: cargarDatosGrilla
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para Cargar los datos en la Grio.
    /// Modificacion:
    /// </summary>
    private void cargarDatosGrilla()
    {
        try
        {
            //determina las columnas para el subastatodr o los operaores
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador", "@P_codigo_punto" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0" };
            lConexion.Abrir();
            dtgSubasta4.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaPuja6", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgSubasta4.DataBind();
            lConexion.Cerrar();
            dtgSubasta4.Columns[6].Visible = true;
            dtgSubasta4.Columns[12].Visible = true;
            dtgSubasta4.Columns[13].Visible = true;
            dtgSubasta4.Columns[14].Visible = true;
            if (Session["estado"].ToString() == "5" || Session["estado"].ToString() == "F" || Session["estado"].ToString() == "S" || Session["estado"].ToString() == "Z")
            {
                dtgSubasta4.Columns[6].Visible = false;
                dtgSubasta4.Columns[12].Visible = false;
                dtgSubasta4.Columns[13].Visible = false;
                dtgSubasta4.Columns[14].Visible = false;

            }
            if (Session["estado"].ToString() == "3" || Session["estado"].ToString() == "7")
            {
                dtgSubasta4.Columns[6].Visible = false;
                dtgSubasta4.Columns[12].Visible = false;
                dtgSubasta4.Columns[13].Visible = false; //20170327 rq007-17 subasta bimestral
                dtgSubasta4.Columns[14].Visible = false; //20170327 rq007-17 subasta bimestral
            }

            if (Session["estado"].ToString() == "5" || Session["estado"].ToString() == "F")
            {
                dtgSubasta4.Columns[5].Visible = false;
                dtgSubasta4.Columns[6].Visible = false;
                dtgSubasta4.Columns[7].Visible = true;
                dtgSubasta4.Columns[8].Visible = true;
                dtgSubasta4.Columns[15].Visible = true;
            }
            else
                dtgSubasta4.Columns[15].Visible = false;


            if (Session["tipoPerfil"].ToString() == "B")
            {
                dtgSubasta4.Columns[12].Visible = false;
                dtgSubasta4.Columns[13].Visible = false;
                dtgSubasta4.Columns[14].Visible = false;
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.postura.location= 'frm_posturas_6.aspx';", true);
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, ex.Message);

        }

    }
}