﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ManejoTriggers.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_ManejoTriggers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
                id="tblTitulo">
                <tr>
                    <td align="center" class="th1">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            <table border="0" align="center" cellpadding="3" cellspacing="2" width="95%" runat="server"
                id="tblBuscar">
                <tr>
                    <td class="td1">
                        Acción
                    </td>
                    <td class="td2">
                        <asp:DropDownList ID="ddlAccion" runat="server">
                            <asp:ListItem Value="C"> Crear</asp:ListItem>
                            <asp:ListItem Value="E"> Eliminar</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="td1">
                        Nombre Tabla
                    </td>
                    <td class="td2">
                        <asp:TextBox ID="txtTabla" runat="server" autocomplete="off"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvtxtTabla" runat="server" ErrorMessage="Debe Digitar el número de la tabla"
                            ControlToValidate="txtTabla" ValidationGroup="buscar">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="th1" colspan="2" align="center">
                        <asp:LinkButton ID="lkbConsultar" runat="server" OnClick="lkbConsultar_Click" ValidationGroup="buscar">Procesar</asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="buscar" />
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
