﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_IngresoOfertaCompra_6 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    String strRutaArchivo;
    String strRutaArchivo1;
    SqlDataReader lLector;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            if (this.Request.QueryString["ID"] != null && this.Request.QueryString["ID"].ToString() != "")
            {
                hndID.Value = this.Request.QueryString["ID"];
                hndAccion.Value = this.Request.QueryString["accion"];
                hdfMaxPostCompra.Value = this.Request.QueryString["maxPostCompra"];
                /// Obtengo los Datos del ID Recibido
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_id_rueda", " numero_id = " + hndID.Value + " ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    /// Lleno las etiquetas de la pantalla con los datos del Id.
                    lblRueda.Text = "No. Rueda: " + lLector["numero_rueda"].ToString();
                    hdfNoRueda.Value = lLector["numero_rueda"].ToString();
                    lblId.Text = "ID: " + this.Request.QueryString["ID"];
                    lblFecha.Text = "Fecha Rueda: " + lLector["fecha_rueda"].ToString().Substring(0, 10);
                    lblProducto.Text = "Producto: " + lLector["codigo_producto"].ToString() + "-" + lLector["desc_producto"].ToString();
                    lblUnidadMedida.Text = "Unidad Medida: " + lLector["codigo_unidad_medida"].ToString() + "-" + lLector["desc_unidad_medida"].ToString();
                    lblPuntoEntrega.Text = "Punto Entrega: " + lLector["codigo_punto_entrega"].ToString() + "-" + lLector["desc_punto_entrega"].ToString();
                    lblPeridoEnt.Text = "Periodo Entrega: " + lLector["codigo_periodo"].ToString() + "-" + lLector["desc_periodo"].ToString();
                    lblModalidad.Text = "Modalidad Contractual: " + lLector["codigo_modalidad"].ToString() + "-" + lLector["desc_modalidad"].ToString();
                    lblFechaIni.Text = "Fecha Entrega Inicial: " + lLector["fecha_entrega"].ToString().Substring(0, 10);
                    lblFechaFin.Text = "Fecha Entrega Inicial: " + lLector["fecha_entrega_fin"].ToString().Substring(0, 10);
                    hndCntVenta.Value = lLector["cantidad_total_venta"].ToString();
                    /// Reviso si se esta creando la oferta o modificando
                    if (hndAccion.Value == "M")
                        btnOfertar.Text = "Modificar Oferta";
                    lLector.Close();
                    lLector.Dispose();

                    /// Llamo el Procedimiento que arma la grilla del ingreso de las ofertas
                    string[] lsNombreParametros = { "@P_numero_id", "@P_codigo_operador", "@P_max_post_compra" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                    string[] lValorParametros = { hndID.Value, goInfo.cod_comisionista, hdfMaxPostCompra.Value };

                    dtgPosturasC.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetPosturasC", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgPosturasC.DataBind();
                    if (dtgPosturasC.Items.Count > 0)
                    {
                        TextBox oCnt = null;
                        TextBox oPre = null;
                        int ldCont = 1;
                        foreach (DataGridItem Grilla in this.dtgPosturasC.Items)
                        {
                            if (hndAccion.Value == "M")
                            {
                                Grilla.Cells[0].Text = ldCont.ToString();
                                ldCont++;
                            }
                            oCnt = (TextBox)Grilla.Cells[2].Controls[1];
                            oPre = (TextBox)Grilla.Cells[1].Controls[1];
                            //oPre.Focus = true;
                            if (Grilla.Cells[3].Text.Replace("&nbsp;", "") != "")
                                oCnt.Text = Grilla.Cells[3].Text.Replace("&nbsp;", "");
                            if (Grilla.Cells[4].Text.Replace("&nbsp;", "") != "")
                                oPre.Text = Grilla.Cells[4].Text.Replace("&nbsp;", "");
                        }
                    }
                }
                else
                    Toastr.Warning(this, "El ID enviado NO existe en el sistema!");

                lConexion.Cerrar();
            }
            else
                Toastr.Warning(this, "No Se enviaron los Parametros requeridos.!");
            //hndCodSubasta.Value = "21";
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOfertar_Click(object sender, EventArgs e)
    {

        string sCerrar = "N";
        string oError = "";
        string sProc = "";
        string lsError = "";
        string lsNoPostura = "";

        string[] lsNombreParametros = { "@P_numero_postura", "@P_numero_id", "@P_numero_rueda", "@P_codigo_operador", "@P_precio_postura", "@P_cantidad_postura", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Float, SqlDbType.Int, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "0", "0", "0", "0", "0", "0" }; ///  Accion 0=Creo la Temporal 1=Crear 2=Modificar

        lsError = ValidarEntradas();
        if (lsError == "")
        {
            lConexion.Abrir();
            SqlTransaction oTransaccion;
            oTransaccion = lConexion.gObjConexion.BeginTransaction(System.Data.IsolationLevel.Serializable);
            try
            {
                TextBox oCnt = null;
                TextBox oPre = null;
                foreach (DataGridItem Grilla in this.dtgPosturasC.Items)
                {
                    oCnt = (TextBox)Grilla.Cells[2].Controls[1];
                    oPre = (TextBox)Grilla.Cells[1].Controls[1];
                    oCnt.Text = oCnt.Text.Replace(",", "");
                    oPre.Text = oPre.Text.Replace(",", "");
                    lValorParametros[1] = hndID.Value;
                    lValorParametros[2] = hdfNoRueda.Value;
                    lValorParametros[3] = goInfo.cod_comisionista;
                    lValorParametros[4] = oPre.Text;
                    lValorParametros[5] = oCnt.Text;
                    if (hndAccion.Value == "C")
                        lsNoPostura = Grilla.Cells[0].Text.Replace(",", "");
                    else
                        lsNoPostura = Grilla.Cells[5].Text.Replace(",", "");
                    lValorParametros[6] = "0";
                    lValorParametros[0] = lsNoPostura;

                    goInfo.mensaje_error = "";
                    if (!DelegadaBase.Servicios.EjecutarProcedimientoConTransaccion(lConexion.gObjConexion, "pa_SetPosturaCompraId6", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo))
                    {
                        if (goInfo.mensaje_error != "")
                        {
                            oError = "Error al Actualizar la Postura. " + goInfo.mensaje_error.ToString() + "\\n";
                            oTransaccion.Rollback();
                            break;
                        }
                    }
                }
                if (oError == "")
                {
                    if (hndAccion.Value == "C")
                    {
                        lValorParametros[2] = hdfNoRueda.Value;
                        lValorParametros[6] = "1";
                        lValorParametros[0] = "0";
                    }
                    else
                    {
                        lValorParametros[2] = hdfNoRueda.Value;
                        lValorParametros[6] = "2";
                        lValorParametros[0] = lsNoPostura;
                    }
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetPosturaCompraId6", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        oError = "Error al Actualizar la Postura. " + goInfo.mensaje_error.ToString() + "\\n";
                        oTransaccion.Rollback();
                    }
                    else
                    {
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            if (lLector["mensaje"].ToString() != "")
                            {
                                oError = lLector["mensaje"].ToString();
                                lLector.Close();
                                lLector.Dispose();
                                oTransaccion.Rollback();
                                lConexion.Cerrar();
                            }
                            else
                            {
                                lLector.Close();
                                lLector.Dispose();
                                oTransaccion.Commit();
                                lConexion.Cerrar();
                                Session["refrescar"] = "S";
                                Session["mis_ofertas"] = "S";
                                if (hndAccion.Value == "C")
                                    Toastr.Success(this, "Posturas Ingresadas Correctamente.!");
                                else
                                    Toastr.Success(this, "Posturas Actualizadas Correctamente.!");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oTransaccion.Rollback();
                oError = "Error al realizar la oferta. " + ex.Message;
                lConexion.Cerrar();
            }
            if (oError != "")
            {
                Toastr.Warning(this, oError);
            }
        }
        else
            Toastr.Warning(this, lsError);

    }
    /// <summary>
    /// Relaiza la validacion del Ingreso de las Posturas
    /// </summary>
    /// <returns></returns>
    protected string ValidarEntradas()
    {
        string lsError = "";
        int oValor = 0;
        double oValor1 = 0;
        int ldValorAnt = 0;
        double ldPrecioAnt = 0;
        int ldConta = 0;

        TextBox oCnt = null;
        TextBox oPre = null;
        foreach (DataGridItem Grilla in this.dtgPosturasC.Items)
        {
            oCnt = (TextBox)Grilla.Cells[2].Controls[1];
            oPre = (TextBox)Grilla.Cells[1].Controls[1];
            oCnt.Text = oCnt.Text.Replace(",", "");
            oPre.Text = oPre.Text.Replace(",", "");
            if (oCnt.Text.Trim().Length > 0)
            {
                try
                {
                    // Validacion para que no permita cantidad  menor o Igual a 0 - 20150709
                    if (Convert.ToInt32(oCnt.Text.Replace(",", "")) <= 0)
                        lsError += "La Cantidad de la Postura " + Grilla.Cells[0].Text + " no puede ser menor o Igual a 0\\n";
                    else
                    {

                        if (Convert.ToInt32(oCnt.Text.Replace(",", "")) > Convert.ToInt32(hndCntVenta.Value))
                            lsError += "La Cantidad de la Postura " + Grilla.Cells[0].Text + " no puede ser mayor que la cantidad ofrecida \\n";
                    }

                }
                catch (Exception ex)
                {
                    lsError += "Valor Invalido en la Cantidad de la Postura " + Grilla.Cells[0].Text + "\\n";
                }
            }
            else
                lsError += "Debe Ingresar la Cantidad de la Postura " + Grilla.Cells[0].Text + "\\n";
            if (oPre.Text.Trim().Length > 0)
            {
                string sOferta = oPre.Text.Replace(",", "");
                int iPos = sOferta.IndexOf(".");
                if (iPos > 0)
                    if (sOferta.Length - iPos > 3)
                        lsError += "Se permiten máximo 2 decimales en el precio de a postura " + Grilla.Cells[0].Text + "\\n";
                try
                {
                    if (Convert.ToDouble(sOferta) < 0)
                        lsError += "El precio de la postura " + Grilla.Cells[0].Text + " debe ser mayor o igual que cero\\n";
                }
                catch (Exception ex)
                {
                    lsError += "El precio de la postura " + Grilla.Cells[0].Text + " no es válido\\n";
                }
            }
            else
                lsError += "Debe Ingresar el Precio de la Postura " + Grilla.Cells[0].Text + "\\n";
            if (lsError != "")
                break;
            else
            {
                if (ldConta > 0)
                {
                    if (Convert.ToInt32(oCnt.Text) < ldValorAnt)
                        lsError += "La Cantidad de la Postura " + Grilla.Cells[0].Text + " debe ser mayor a la postura anterior. \\n";
                    if (Convert.ToDouble(oPre.Text) >= ldPrecioAnt)
                        lsError += "El  Precio de la Postura " + Grilla.Cells[0].Text + " debe ser menor al de la postura anterior. \\n";

                    ldValorAnt = Convert.ToInt32(oCnt.Text);
                    ldPrecioAnt = Convert.ToDouble(oPre.Text);
                }
                else
                {
                    ldValorAnt = Convert.ToInt32(oCnt.Text);
                    ldPrecioAnt = Convert.ToDouble(oPre.Text);
                }
            }
            ldConta++;
        }
        return lsError;
    }
}