﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_cargaVenta_6 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    String strRutaCarga;
    String strRutaFTP;
    SqlDataReader lLector;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
        strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"].ToString();
        strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"].ToString();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnCargar_Click(object sender, EventArgs e)
    {
        ltCargaArchivo.Text = "";
        string lsRutaArchivo = "";
        string lsNombre = "";
        string[] lsErrores = { "", "" };
        string lsCadenaArchivo = "";
        bool oTransOK = true;
        bool oCargaOK = true;
        SqlDataReader lLector;
        SqlCommand lComando = new SqlCommand();
        Int32 liNumeroParametros = 0;
        lConexion = new clConexion(goInfo);
        string[] lsNombreParametrosO = { "@P_archivo", "@P_codigo_operador", "@P_numero_rueda", "@P_ruta_ftp" };
        SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
        Object[] lValorParametrosO = { "", goInfo.cod_comisionista, Session["numero_rueda"].ToString(), strRutaFTP };
        lsNombre = DateTime.Now.Millisecond.ToString() + FuArchivo.FileName.ToString();
        try
        {
            lsRutaArchivo = strRutaCarga + lsNombre;
            FuArchivo.SaveAs(lsRutaArchivo);
        }
        catch (Exception ex)
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Ocurrio un Problema al Cargar el Archivo de Operador por subasta-ID. " + ex.Message + "!');", true);
            Toastr.Warning(this, "Ocurrio un Problema al Cargar el Archivo. " + ex.Message);
            oTransOK = false;
        }
        /// Realiza las Validaciones de los Archivos
        if (oTransOK)
        {
            try
            {
                //trMensaje.Visible = true;
                lsCadenaArchivo = lsCadenaArchivo + "<TR><TD colspan='2'> <font color='#FFFFFF' size='-1'>" + "***LOG ARCHIVO DE CARGA *** </FONT> </TD></TR>";
                if (FuArchivo.FileName.ToString() != "")
                    lsErrores = ValidarArchivo(lsRutaArchivo);
                else
                {
                    Toastr.Warning(this, "Debe seleccionar el archivo<br>");
                    lsErrores[0] = ".";
                }
                if (lsErrores[0] == "")
                {
                    if (oCargaOK && DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"].ToString() + lsNombre, ConfigurationManager.AppSettings["UserFtp"].ToString(), ConfigurationManager.AppSettings["PwdFtp"].ToString()))
                    {
                        lValorParametrosO[0] = lsNombre;
                        lValorParametrosO[1] = goInfo.cod_comisionista;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = "pa_ValidaPlanoSBIM";
                        lComando.CommandTimeout = 3600;
                        if (lsNombreParametrosO != null)
                        {
                            for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                            {
                                lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                            }
                        }
                        lLector = lComando.ExecuteReader();
                        //lLector = DelegadaOMA.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_ValidaPlanoOpe", lsNombreParametrosO, lTipoparametrosO, lValorParametrosO);
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                ltCargaArchivo.Text = ltCargaArchivo.Text + lLector["Mensaje"].ToString() + "<br>";
                            }
                            Session["ofertas"] = "S";
                            Session["refrescar"] = "S";
                        }
                        else
                        {
                            Session["refrescar"] = "S";
                            Toastr.Success(this, "Archivo Cargado Satisfactoriamente.!");
                            Toastr.Success(this, "Ingreso de posturas de venta corecto.!");

                         
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Warning(this, "Se presento un Problema en el {FTP} del Archivo al Servidor.!");
                    }
                }
                else
                {
                    lsCadenaArchivo = lsCadenaArchivo + "<TR><TD colspan='2'> <font color='#FFFFFF' size='-1'>" + lsErrores[0] + "</FONT> </TD></TR>";
                    ltCargaArchivo.Text = ltCargaArchivo.Text + lsCadenaArchivo;
                    DelegadaBase.Servicios.registrarProceso(goInfo, "Finalizó la Carga con Errores", "Usuario : " + goInfo.nombre);
                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Se presento un Problema Al cargar los datos del archivo plano.! <br>");
            }
        }
    }

    public string[] ValidarArchivo(string lsRutaArchivo)
    {
        Int32 liNumeroLinea = 0;
        decimal ldValor = 0;
        int liTotalRegistros = 0;
        Int64 liValor = 0;
        string lsCadenaErrores = "";
        string[] lsCadenaRetorno = { "", "" };
        string[] lsDecimal;

        StreamReader lLectorArchivo = new StreamReader(lsRutaArchivo);
        try
        {
            /// Recorro el Archivo de Excel para Validarlo
            lLectorArchivo = File.OpenText(lsRutaArchivo);
            while (!lLectorArchivo.EndOfStream)
            {
                liTotalRegistros = liTotalRegistros + 1;
                /// Obtiene la fila del Archivo
                string lsLineaArchivo = lLectorArchivo.ReadLine();
                //if (lsLineaArchivo.Length > 0)
                //{
                liNumeroLinea = liNumeroLinea + 1;
                /// Pasa la linea sepaada por Comas a un Arreglo
                Array oArregloLinea = lsLineaArchivo.Split(',');
                if ((oArregloLinea.Length != 4))
                {
                    lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 4 },  en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<BR>";
                }
                else
                {
                    /// Valida Punto de entrega
                    try
                    {
                        ldValor = Convert.ToInt32(oArregloLinea.GetValue(0).ToString());
                    }
                    catch (Exception ex)
                    {
                        lsCadenaErrores = lsCadenaErrores + "El código del punto de entrega{" + oArregloLinea.GetValue(0).ToString() + "} es inválido, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                    }
                    /// Valida cantidad
                    try
                    {
                        ldValor = Convert.ToInt32(oArregloLinea.GetValue(1).ToString());
                    }
                    catch (Exception ex)
                    {
                        lsCadenaErrores = lsCadenaErrores + "la cantidad {" + oArregloLinea.GetValue(1).ToString() + "} es inválida, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                    }
                    /// Valida precio
                    try
                    {
                        ldValor = Convert.ToDecimal(oArregloLinea.GetValue(2).ToString());
                        lsDecimal = oArregloLinea.GetValue(2).ToString().Split('.');
                        if (lsDecimal.Length > 1 && lsDecimal[1].Length > 2)
                            lsCadenaErrores = lsCadenaErrores + "El Precio de reserva {" + oArregloLinea.GetValue(2).ToString() + "} debe tener máximo 2 decimales, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                    }
                    catch (Exception ex)
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Precio de reserva {" + oArregloLinea.GetValue(2).ToString() + "} es inválido, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";

                    }
                    /// Valida Delta
                    try
                    {
                        ldValor = Convert.ToDecimal(oArregloLinea.GetValue(3).ToString());
                    }
                    catch (Exception ex)
                    {
                        lsCadenaErrores = lsCadenaErrores + "El delta del Precio de reserva {" + oArregloLinea.GetValue(3).ToString() + "} es inválido, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                    }
                }
            }
            lLectorArchivo.Close();
            lLectorArchivo.Dispose();
        }
        catch (Exception ex)
        {
            lLectorArchivo.Close();
            lLectorArchivo.Dispose();
            lsCadenaRetorno[0] = lsCadenaErrores;
            lsCadenaRetorno[1] = "0";
            return lsCadenaRetorno;
        }
        lsCadenaRetorno[1] = liTotalRegistros.ToString();
        lsCadenaRetorno[0] = lsCadenaErrores;

        return lsCadenaRetorno;
    }
}