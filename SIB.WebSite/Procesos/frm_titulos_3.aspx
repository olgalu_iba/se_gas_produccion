﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_titulos_3.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Procesos_frm_titulos_3" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="divOferta" runat="server" style="position: absolute; left: 0; top: 0; width: 100%;">
                <table cellpadding="0" cellspacing="0" bgcolor="#000000" align="left">
                    <tr>
                        <td align="center">
                            <asp:DataGrid ID="dtgSubasta3" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="th2">
                                <Columns>
                                    <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" HeaderStyle-Width="70px">
                                    </asp:BoundColumn>
                                    <%--1--%><asp:BoundColumn DataField="desc_producto" HeaderText="Producto" HeaderStyle-Width="150px">
                                    </asp:BoundColumn>
                                    <%--2--%><asp:BoundColumn DataField="desc_unidad_medida" HeaderText="UM" HeaderStyle-Width="60px">
                                    </asp:BoundColumn>
                                    <%--3--%><asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" HeaderStyle-Width="100px">
                                    </asp:BoundColumn>
                                    <%--4--%><asp:BoundColumn DataField="desc_punto" HeaderText="Ruta" HeaderStyle-Width="200px">
                                    </asp:BoundColumn>
                                    <%--5--%><asp:BoundColumn DataField="desc_periodo" HeaderText="Periodo entrega" HeaderStyle-Width="100px">
                                    </asp:BoundColumn>
                                    <%--6--%><asp:BoundColumn DataField="cantidad_total_venta" HeaderText="cantidad venta"
                                        HeaderStyle-Width="100px"></asp:BoundColumn>
                                    <%--7--%><asp:BoundColumn DataField="precio_venta" HeaderText="precio venta" HeaderStyle-Width="100px">
                                    </asp:BoundColumn>  
                                    <%--8--%><asp:BoundColumn DataField="desc_estado" HeaderText="estado" HeaderStyle-Width="120px">
                                    </asp:BoundColumn>
                                    <%--9--%><asp:TemplateColumn HeaderText="Ofr" HeaderStyle-Width="45px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbOfertar" runat="server" ToolTip="Ofertar" ImageUrl="~/Images/nuevo.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--10--%><asp:TemplateColumn HeaderText="Mod" HeaderStyle-Width="45px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbModificar" runat="server" ToolTip="Modificar Oferta" ImageUrl="~/Images/modificar.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--11--%><asp:TemplateColumn HeaderText="Elim" HeaderStyle-Width="45px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbEliminar" runat="server" ToolTip="Eliminar Oferta" ImageUrl="~/Images/eliminar.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--12--%><asp:TemplateColumn HeaderText="gráf" HeaderStyle-Width="45px"> <%--20180815 BUG230--%>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbGrafica" runat="server" ToolTip="curva agregada" ImageUrl="~/Images/grafica.jpg" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <HeaderStyle CssClass="th2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="13px">
                                </HeaderStyle>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Panel1" runat="server">
    </asp:Panel>
  </asp:Content>