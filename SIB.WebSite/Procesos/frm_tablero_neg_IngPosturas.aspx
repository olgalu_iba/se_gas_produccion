﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_tablero_neg_IngPosturas.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_tablero_neg_IngPosturas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" language="javascript">
        //        document.location = no;
        function confirmar() {
            return confirm("Esta seguro que desea ofertar!")
        }
        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }

        function formatoMiles(obj, evt) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /([0-9]*)$/i.exec(strValue)) != null) {
                obj.value = addCommas(T[1]);
            } else {
                alert('formato invalido ');
            }
        }
        function formatoMilesPre(obj, evt, decimales) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
                T[4] = T[4].substr(0, decimales);
                if (T[2] == '' && T[3] == '.') {
                    T[2] = 0;
                }
                obj.value = T[1] + addCommas(T[2]) + T[3] + T[4];
            }
            else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }

    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td6" colspan="2" align="center">
                        <asp:Label ID="lblTituo" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="td6">
                        <asp:Label ID="lblPtoEnt" runat="server"></asp:Label>
                    </td>
                    <td class="td6">
                        <asp:Label ID="lblDescPuntoiRuta" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="td6">
                        Periodo Entrega:
                    </td>
                    <td class="td6">
                        <asp:Label ID="lblDescPeriodo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr runat="server" id="TrFuente" visible="false">
                    <td class="td6">
                        Fuente
                    </td>
                    <td class="td6">
                        <asp:DropDownList ID="ddlFuente" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr id="trAño" runat="server" visible="false">
                    <td class="td6">
                        No. Años:
                    </td>
                    <td class="td6">
                        <asp:TextBox ID="txtAño" runat="server" Width="150px" MaxLength="2"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbetxtAño" runat="server" TargetControlID="txtAño"
                            FilterType="Custom, Numbers">
                        </ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td class="td6">
                        Fecha Máxima para manifestar interés:
                    </td>
                    <td class="td6">
                        <asp:TextBox ID="TxtFechaMaxInteres" runat="server" Width="150px" MaxLength="10"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="CeTxtFechaMaxInteres" runat="server" TargetControlID="TxtFechaMaxInteres"
                            Format="yyyy/MM/dd">
                        </ajaxToolkit:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td class="td6">
                        Hora Máxima para manifestar interés:
                    </td>
                    <td class="td6">
                        <asp:TextBox ID="TxtHoraMaxInt" runat="server" Width="100px" MaxLength="5"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RevTxtHoraMaxInt" ControlToValidate="TxtHoraMaxInt"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora máxima para manifestar interes"> * </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="1">
                        Precio:
                    </td>
                    <td class="td6" colspan="1">
                        <asp:TextBox ID="TxtPrecio" runat="server" Width="100px" onkeyup="return formatoMilesPre(this,event,2);"></asp:TextBox>
                        <asp:Label ID="lblUndPre" runat="server"></asp:Label>
                        <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtPrecio" runat="server" Enabled="True" FilterType="Custom, Numbers"
                            TargetControlID="TxtPrecio" ValidChars=",.">
                        </ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="1">
                        Cantidad:
                    </td>
                    <td class="td6" colspan="1">
                        <asp:TextBox ID="TxtCantidad" runat="server" Width="100px" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
                        <asp:Label ID="lblUndCnt" runat="server"></asp:Label>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FTEBTxtCantidad" runat="server" Enabled="True" FilterType="Custom, Numbers"
                            TargetControlID="TxtCantidad" ValidChars=",">
                        </ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td class="td6">
                        Punta
                    </td>
                    <td class="td6">
                        <asp:DropDownList ID="ddlPunta" runat="server">
                            <asp:ListItem Value="">Seleccione</asp:ListItem>
                            <asp:ListItem Value="C">Compra</asp:ListItem>
                            <asp:ListItem Value="V">Venta</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="td6">
                        Fecha Inicial:
                    </td>
                    <td class="td6">
                        <asp:TextBox ID="txtFechaIni" runat="server" Width="150px" MaxLength="10" OnTextChanged="TxtFechaIni_TextChanged"
                            AutoPostBack="true"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="CetxtFechaIni" runat="server" TargetControlID="txtFechaIni"
                            Format="yyyy/MM/dd">
                        </ajaxToolkit:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td class="td6">
                        Fecha Final:
                    </td>
                    <td class="td6">
                        <asp:TextBox ID="txtFechaFin" runat="server" Width="150px" MaxLength="10" Enabled="false"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="CetxtFechaFin" runat="server" TargetControlID="txtFechaFin"
                            Format="yyyy/MM/dd">
                        </ajaxToolkit:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td class="td6">
                        Datos Contacto:
                    </td>
                    <td class="td6">
                        <asp:TextBox ID="TxtDatosContacto" runat="server" Width="300px" MaxLength="500" TextMode="MultiLine"
                            Rows="3"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" class="td6">
                        <br />
                        <asp:Button ID="btnOfertar" Text="Crear" runat="server" OnClientClick="return confirmar();"
                            OnClick="btnOfertar_Click" />
                        <asp:HiddenField ID="hdfNoRueda" runat="server" />
                        <asp:HiddenField ID="hdfNoid" runat="server" />
                        <asp:HiddenField ID="hdfCodigoPuntoRuta" runat="server" />
                        <asp:HiddenField ID="hdfCodigoPeriodo" runat="server" />
                        <asp:HiddenField ID="hndAccion" runat="server" />
                        <asp:HiddenField ID="hdfMedidaTiempo" runat="server" />
                        <asp:HiddenField ID="hdfErrorFecha" runat="server" />
                        <asp:HiddenField ID="hdfTipoMercado" runat="server" />
                        <asp:HiddenField ID="hdfDestinoRueda" runat="server" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
 </asp:Content>