﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using PCD_Infraestructura.Business;
using PCD_Infraestructura.Transaction;
using PCD_Infraestructura.DomainLayer;
using System.Diagnostics;

public partial class Procesos_frm_mensajes_1 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        ltTableroRF.Text = "";
        ltTableroRF.Text += "<table border='0' cellspacing='0' dellpading ='0'><tr>";
        ltTableroRF.Text += "<td  width='100%'align='center' bgcolor='#E46D0A' style='border-color:#E46D0A'  colspan='2'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>MIS MENSAJES</font></td>";
        ltTableroRF.Text += "</tr>";
        ltTableroRF.Text += "<tr>";
        ltTableroRF.Text += "<td width='10%' style='border-color:#E46D0A' align='center' bgcolor='#E46D0A'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>HORA</font></td>";
        ltTableroRF.Text += "<td width='90%' style='border-color:#E46D0A' align='center' bgcolor='#E46D0A'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>MENSAJE</font></td>";
        ltTableroRF.Text += "</tr>";

        try
        {

            string[] lsNombreParametros = { "@P_numero_rueda" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int};
            string[] lValorParametros = { Session["numero_rueda"].ToString() };
            SqlDataReader lLector;
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetMensajes", lsNombreParametros, lTipoparametros, lValorParametros);
            if (lLector.HasRows)
            {
                while (lLector.Read())
                {
                    ltTableroRF.Text += "<tr>";
                    ltTableroRF.Text += "<td width='10%' style='border-color:#F79632' align='left' bgcolor='#F79632'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["hora_mensaje"].ToString() + "</font></td>";
                    ltTableroRF.Text += "<td width='10%' style='border-color:#F79632' align='left' bgcolor='#F79632'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["mensaje"].ToString() + "</font></td>";
                    ltTableroRF.Text += "</tr>";
                }
                ltTableroRF.Text += "</table>";
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();

            //oLectorArchivo.Close();
            //oLectorArchivo.Dispose();
            /// Creacion del archivo del tablero de RF 
            /// 2011-10-03
            ///
            if (Session["refrescar_cont"].ToString() == "S")
            {
                Session["refrescar_cont"]= "N";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.contratos.location= 'frm_contratos_1.aspx';", true);
            }

        }
        catch (Exception ex)
        {
        }

        //while (!oLectorArchivo.EndOfStream)

        //  lsLineaArchivo = oLectorArchivo.ReadLine();
        //oArregloLinea = lsLineaArchivo.Split(',');
    }
}
