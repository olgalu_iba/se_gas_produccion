﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_IngresoOfertaCompra_1.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_IngresoOfertaCompra_1" EnableEventValidation="false" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">
        //        document.location = no;
        function confirmar() {
            return confirm("Esta seguro que desea ofertar!")
        }
        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }

        function formatoMiles(obj, evt) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /([0-9]*)$/i.exec(strValue)) != null) {
                obj.value = addCommas(T[1]);
            } else {
                alert('formato invalido ');
            }
        }
        function formatoMilesPre(obj, evt, decimales) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
                T[4] = T[4].substr(0, decimales);
                if (T[2] == '' && T[3] == '.') {
                    T[2] = 0;
                }
                obj.value = T[1] + addCommas(T[2]) + T[3] + T[4];
            }
            else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }

    </script>
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTituo" runat="server" Text="INGRESO POSTURAS DE COMPRA" />

                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblId" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblTipo" runat="server"></asp:Label>


                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblDuracion" runat="server"></asp:Label>

                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblLugar" runat="server"></asp:Label>

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblPrecioRes" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Tipo de Mercado</label>

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblCantidadV" runat="server"></asp:Label>

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="Label1" runat="server">Cantidad Demandada:</asp:Label>
                            <asp:TextBox ID="TxtCantidad" runat="server" Width="100px" onkeyup="return formatoMiles(this,event);"></asp:TextBox>

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="Label2" runat="server">Precio sostenimiento demanda</asp:Label>
                            <asp:TextBox ID="TxtPrecio" runat="server" Width="100px" onkeyup="return formatoMilesPre(this,event,2);"></asp:TextBox>

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="Label3" runat="server"></asp:Label>
                            <asp:Button ID="btnOfertar" Text="Crear" runat="server" OnClientClick="return confirmar();"
                                OnClick="btnOfertar_Click" />
                            <asp:HiddenField ID="hndID" runat="server" />
                            <asp:HiddenField ID="hndAccion" runat="server" />
                            <asp:HiddenField ID="hndRonda" runat="server" />
                            <asp:HiddenField ID="hndCantAnt" runat="server" />
                            <asp:HiddenField ID="hndPrecioRes" runat="server" />

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="Label4" runat="server"></asp:Label>


                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


</asp:Content>
