﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Procesos
{
    public partial class frm_frame_subasta_6 : Page
    {
        #region Propiedades

        private InfoSessionVO goInfo;
        private clConexion lConexion;
        private string strRutaArchivo;
        private DataSet lds = new DataSet();
        private SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        //fecha de la rueda en sesion del lado del cliente
        public string FechaRueda
        {
            get { return ViewState["FechaRueda"]?.ToString(); }
            set { ViewState["FechaRueda"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        //codigo de la ruta si es para subasta de transporte
        public string CodRuta
        {
            get { return ViewState["CodRuta"] != null && !string.IsNullOrEmpty(ViewState["CodRuta"].ToString()) ? ViewState["CodRuta"].ToString() : "0"; }
            set { ViewState["CodRuta"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        ////Número de la rueda en variable de sesion 
        public string CodSubasta
        {
            get { return ViewState["numero_rueda"] != null && !string.IsNullOrEmpty(ViewState["numero_rueda"].ToString()) ? ViewState["numero_rueda"].ToString() : "0"; }
            set { ViewState["numero_rueda"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        //hora en sesion 
        public string Hora
        {
            get { return ViewState["hora"] != null && !string.IsNullOrEmpty(ViewState["hora"].ToString()) ? ViewState["hora"].ToString() : ""; }
            set { ViewState["hora"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// 
        //variable de sesion para refrescar
        public string Refrescar
        {
            get { return ViewState["refrescar"] != null && !string.IsNullOrEmpty(ViewState["refrescar"].ToString()) ? ViewState["refrescar"].ToString() : "S"; }
            set { ViewState["refrescar"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        //Variable de sesion para MisOfertas
        public string MisOfertas
        {
            get { return ViewState["mis_ofertas"] != null && !string.IsNullOrEmpty(ViewState["mis_ofertas"].ToString()) ? ViewState["mis_ofertas"].ToString() : "N"; }
            set { ViewState["mis_ofertas"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        //Variable sesion CodigoPunto
        public string CodigoPunto
        {
            get { return ViewState["codigo_punto"] != null && !string.IsNullOrEmpty(ViewState["codigo_punto"].ToString()) ? ViewState["codigo_punto"].ToString() : "0"; }
            set { ViewState["codigo_punto"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        //Estado en variable de sesion estado de la rueda
        public string Estado
        {
            get { return ViewState["estado"] != null && !string.IsNullOrEmpty(ViewState["estado"].ToString()) ? ViewState["estado"].ToString() : null; }
            set { ViewState["estado"] = value; }
        }

        #endregion Propiedades

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Se cargan los valores iniciales del formulario 
                CargarPagina();

                if (IsPostBack) return;
                //Se inicializan los controles para la primera ves que se ejecuta el formulario 
                InicializarPagina();
                // Se realiza el cargue de la rueda
                CargarRueda();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void CargarPagina()
        {
            //Se redirecciona al Login si no hay una sesión iniciada    
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("~/login.aspx");
            strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"];

            string sFechaAct = DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day;
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            //se valida la existencia de la rueda
            SqlDataReader lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda", " codigo_tipo_subasta = 6 and fecha_prox_apertura = '" + sFechaAct + "' order by numero_rueda desc");
            if (lLector.HasRows)
            {
                //si tiene registros obtiene los valores del numero de la rueda, el estado y la descripción
                lLector.Read();
                CodSubasta = lLector["numero_rueda"].ToString();
                Estado = lLector["estado"].ToString();
                Session["desc_rueda"] = lLector["descripcion"].ToString();
            }
            else
            {
                //si no se encuentra valores los coloca en estado original
                CodSubasta = "0";
                Estado = "C";
                Session["desc_rueda"] = "";
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();

            //Eventos
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttonsSubasta.PuntoOnSelectedIndexChanged += ddlPunto_SelectedIndexChanged;
            //buttonsSubastaExtender.SuspenderOnclick += Suspender_Click;
            //buttonsSubastaExtender.ReactivarOnclick += btnReactivar_Click;
            buttonsSubastaExtender.OfertaOnclick += btnOfertas_Click;
            buttonsSubastaExtender.ContratoOnclick += btnContratos_Click;

            estados.TimeServer.Value = DateTime.Now.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Inicializa el contenido del formulario 
        /// </summary>
        private void InicializarPagina()
        {
            //Titulo
            Master.Titulo = "Subasta";
            //Descripcion
            Master.DescripcionPagina = "Subasta Bimestral";

            //Botones
            EnumBotonesSubasta[] botones = { EnumBotonesSubasta.Rueda, EnumBotonesSubasta.Punto };
            buttonsSubasta.Inicializar(botones: botones);
            EnumBotonesSubasta[] botonesExtender = { EnumBotonesSubasta.Oferta, EnumBotonesSubasta.Contrato };
            buttonsSubastaExtender.Inicializar(botones: botonesExtender);

            SeleccionarNombrePestanias();
        }

        /// <summary>
        /// 
        /// </summary>
        private void CargarRueda()
        {
            //Lista con los botones
            EnumBotonesSubasta[] arregloExtender = { EnumBotonesSubasta.Oferta, EnumBotonesSubasta.Contrato };
            var botonesExtender = new List<EnumBotonesSubasta>();
            botonesExtender.AddRange(arregloExtender);

            //Se crea la conexión  
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            var dummy = new DropDownList();
            buttonsSubasta.ListaRueda = dummy.Items;
            buttonsSubasta.EditarRueda = true;
            buttonsSubasta.ListaPunto = LlenarControles(lConexion.gObjConexion, "m_pozo ", "estado ='A' order by descripcion", 0, 1).Cast<ListItem>().ToArray();
            lConexion.Cerrar();
            buttonsSubasta.ValorPunto = CodRuta;
            buttonsSubasta.EditarPunto = false;

            FechaRueda = DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day;
            //FechaRueda = FechaRueda.Substring(6, 4) + "/" + FechaRueda.Substring(3, 2) + "/" + FechaRueda.Substring(0, 2);
            buttons.SwitchOnButton(EnumBotones.Ninguno);

            lConexion.Abrir();

            //valida la existencia de la rueda
            SqlDataReader lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda rue,m_tipos_rueda tpo, m_estado_gas est", " rue.numero_rueda =" + CodSubasta + " and rue.codigo_tipo_rueda = tpo.codigo_tipo_rueda and est.tipo_estado ='R' and rue.estado = est.sigla_estado");
            if (lLector.HasRows)
            {
                lLector.Read();
                var lItem1 = new ListItem { Value = "1", Text = lLector["descripcion"].ToString() };
                dummy.Items.Add(lItem1);
                buttonsSubasta.ListaRueda = dummy.Items;
                buttonsSubasta.EditarRueda = false;
                buttonsSubasta.ValorRueda = "1";

                /*Se deshabilitan los estados y cronómetros*/
                estados.Visible = true;

                estados.Pestania2.CssClass = "nav-link disabled";
                estados.Pestania3.CssClass = "nav-link disabled";
                estados.Pestania4.CssClass = "nav-link disabled";
                estados.Pestania5.CssClass = "nav-link disabled";
                estados.Pestania6.CssClass = "nav-link disabled";

                estados.Pestania2.Visible = true;
                estados.Pestania3.Visible = true;
                estados.Pestania4.Visible = true;
                estados.Pestania5.Visible = true;
                estados.Pestania6.Visible = true;

                estados.Hora2.Visible = true; //20220418
                estados.Hora3.Visible = true;//20220418
                estados.Hora4.Visible = true;//20220418
                estados.Hora5.Visible = true;//20220418

                estados.Hora2.Text = lLector["hora_ini_publ_cnt_dispi_v_sci"] + " - " + lLector["hora_fin_publ_cnt_dispi_v_sci"];//20220418
                estados.Hora3.Text = lLector["hora_ini_rec_solicitud_c_sci"] + " - " + lLector["hora_fin_rec_solicitud_c_sci"];//20220418
                estados.Hora4.Text = lLector["hora_fin_rec_solicitud_c_sci"] + @" - " + lLector["hora_ini_negociacioni_sci"];//20220418
                estados.Hora5.Text = lLector["hora_ini_negociacioni_sci"] + @" - " + lLector["hora_fin_negociacioni_sci"];//20220418

                //Tiempo espera
                divTiempoEspera.Visible = false;
                estados.Time.Value = DateTime.Now.ToShortTimeString();

                switch (lLector["estado"].ToString())
                {
                    case "C":
                        estados.Time.Value = lLector["hora_ini_publ_cnt_dispi_v_sci"].ToString();
                        break;
                    //case "1": lblHora.Text = lLector["hora_ini_publi_v_sci"].ToString() + " - " + lLector["hora_fin_publi_v_sci"].ToString();
                    //    lblFase.Text = "1 - Declaración de Cantidades Disponibles y precios de reserva"; //20160202
                    //    break;
                    case "3":
                        estados.Pestania2.CssClass = "nav-link active";
                        estados.Time.Value = lLector["hora_fin_publ_cnt_dispi_v_sci"].ToString();
                        //estados.Hora2.Visible = true;
                        buttons.SwitchOnButton(EnumBotones.Excel);
                        break;
                    case "4":
                        estados.Pestania3.CssClass = "nav-link active";
                        estados.Time.Value = lLector["hora_fin_rec_solicitud_c_sci"].ToString();                        
                        //estados.Hora3.Visible = true;
                        break;
                    case "7":
                        estados.Pestania4.CssClass = "nav-link active";
                        estados.Time.Value = lLector["hora_ini_negociacioni_sci"].ToString();
                        //estados.Hora4.Visible = true;
                        break;
                    case "5":
                        estados.Pestania5.CssClass = "nav-link active";
                        estados.Time.Value = lLector["hora_fin_negociacioni_sci"].ToString();        
                        //estados.Hora5.Visible = true;
                        break;
                    case "F":
                        estados.Time.Value = "23:59:59";
                        estados.Pestania6.CssClass = "nav-link active";
                        break;
                    default:
                        divTiempoEspera.Visible = true;
                        break;
                }
                //lblHora.Text = lLector["hora_prox_fase"].ToString();
                Estado = lLector["estado"].ToString();
                Session["destino_rueda"] = lLector["destino_rueda"].ToString();
            }
            else
            {
                estados.Time.Value = "23:59:59";
                //Estado Finalizada
                estados.Pestania6.CssClass = "nav-link active";
                //lblFase.Text = "";
                //lblHora.Text = "";
                Estado = "F";
                Session["destino_rueda"] = "G";
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();

            if (goInfo.cod_comisionista != "0" || Estado == "F" || Estado == "Z")
            {
                btnSuspender.Visible = false;
                btnReactivar.Visible = false;
            }
            else
            if (Estado == "S")
            {
                btnSuspender.Visible = false;
                btnReactivar.Visible = true;
            }
            else
            {
                btnSuspender.Visible = true;
                btnReactivar.Visible = false;
            }

            /*Se actualizan las posturas*/
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda", " numero_rueda =" + CodSubasta);
            if (lLector.HasRows)
            {
                lLector.Read();
                Estado = lLector["estado"].ToString();
            }
            else
                Estado = "C";
            lLector.Close();
            if (Session["tipoPerfil"].ToString() == "N")
            {
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_usuario_hab ", " codigo_tipo_subasta = 6 and codigo_usuario =" + goInfo.codigo_usuario + " and fecha_inicial <= '" + FechaRueda + "' and fecha_final >= '" + FechaRueda + "' and estado ='A'");
                if (!lLector.HasRows)
                {
                    Toastr.Error(this, "El usuario NO está habilitado para la rueda. Debe realizar la inscripcion ante la BMC");
                }
            }
            lLector.Close();
            lConexion.Cerrar();

            //Se actualiza la grilla
            CargarDatosGrilla();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lConn"></param>
        /// <param name="lsTabla"></param>
        /// <param name="lsCondicion"></param>
        /// <param name="liIndiceLlave"></param>
        /// <param name="liIndiceDescripcion"></param>
        /// <returns></returns>
        private IEnumerable LlenarControles(SqlConnection lConn, string lsTabla, string lsCondicion, int liIndiceLlave, int liIndiceDescripcion)
        {
            var dummy = new DropDownList();
            // Proceso para Cargar el DDL de ciudad
            SqlDataReader lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            var lItem = new ListItem { Value = "0", Text = "Seleccione" };
            dummy.Items.Add(lItem);

            while (lLector.Read())
            {
                var lItem1 = new ListItem
                {
                    Value = lLector.GetValue(liIndiceLlave).ToString(),
                    Text = lLector.GetValue(liIndiceDescripcion).ToString()
                };
                dummy.Items.Add(lItem1);
            }
            lLector.Close();
            return dummy.Items;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPunto_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CodigoPunto = buttonsSubasta.ValorPunto;
                Refrescar = "S";
                Session["refresca_mis_posturas"] = "S";
                //Se actualizan las posturas
                CargarRueda();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.titulo.location='frm_titulos_6.aspx';", true);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            try
            {
                string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador", "@P_codigo_punto" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { CodSubasta, goInfo.cod_comisionista, buttonsSubasta.ValorPunto };
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                dtgSubasta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaPuja6Exc", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgSubasta.DataBind();
                dtgSubasta.Visible = true;

                DataSet lds = new DataSet();
                SqlDataAdapter lsqldata = new SqlDataAdapter();
                string lsNombreArchivo = goInfo.Usuario + "InfRueda" + DateTime.Now + ".xls";
                string lstitulo_informe = "Consulta Ids Subasta " + buttonsSubasta.ValorRueda;
                decimal ldCapacidad = 0;
                StringBuilder lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                dtgSubasta.EnableViewState = false;
                lform.Controls.Add(dtgSubasta);
                lpagina.RenderControl(lhtw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());
                Response.End();
                lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
                dtgSubasta.Visible = false;
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected void Timer_Tick(object sender, EventArgs e)
        {
            try
            {
                string oArchivo = strRutaArchivo + "subasta-" + CodSubasta + ".txt";
                if (Hora != File.GetCreationTime(oArchivo).ToString("HH:mm:ss") || Refrescar == "S")
                {
                    Hora = File.GetCreationTime(oArchivo).ToString("HH:mm:ss");
                    //Refrescar = "N";
                    //Se recarga la rueda
                    CargarRueda();
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.prm.location= 'frm_subasta_prm_6.aspx?';", true);
                }

                if (MisOfertas == "S")
                    MisOfertas = "N";
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: cargarDatosGrilla
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para Cargar los datos en la Grio.
        /// Modificacion:
        /// </summary>
        private void CargarDatosGrilla(bool primeraCarga = false)
        {
            if (CodSubasta.Equals("0") && !primeraCarga)
            {
                dtgSubasta.Columns.Clear();
                return;
            }

            //determina las columnas para el subastatodr o los operadores
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador", "@P_codigo_punto" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { CodSubasta, goInfo.cod_comisionista, CodigoPunto };
            lConexion.Abrir();
            dtgSubasta1.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaPuja6", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgSubasta1.DataBind();
            lConexion.Cerrar();
            ImageButton imbControl;
            dtgSubasta1.Columns[6].Visible = true;
            dtgSubasta1.Columns[12].Visible = true;
            dtgSubasta1.Columns[13].Visible = true;
            dtgSubasta1.Columns[14].Visible = true;
            if (Estado == "5" || Estado == "F" || Estado == "S" || Estado == "Z")
            {
                dtgSubasta1.Columns[6].Visible = false;
                dtgSubasta1.Columns[12].Visible = false;
                dtgSubasta1.Columns[13].Visible = false;
                dtgSubasta1.Columns[14].Visible = false;

            }
            if (Estado == "3" || Estado == "7")
            {
                dtgSubasta1.Columns[6].Visible = false;
                dtgSubasta1.Columns[12].Visible = false;
                dtgSubasta1.Columns[13].Visible = false; //20170327 rq007-17 subasta bimestral
                dtgSubasta1.Columns[14].Visible = false; //20170327 rq007-17 subasta bimestral
            }

            if (Estado == "5" || Estado == "F")
            {
                dtgSubasta1.Columns[5].Visible = false;
                dtgSubasta1.Columns[6].Visible = false;
                dtgSubasta1.Columns[7].Visible = true;
                dtgSubasta1.Columns[8].Visible = true;
                dtgSubasta1.Columns[15].Visible = true;
            }
            else
                dtgSubasta1.Columns[15].Visible = false;

            foreach (DataGridItem Grilla in dtgSubasta1.Items)
            {
                imbControl = (ImageButton)Grilla.Cells[15].Controls[1];
                // imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_CurvaAgregada.aspx?ID=" + Grilla.Cells[0].Text + "','Grafica','width=900,height=500,left=250,top=90,status=no,location=0,menubar=no,toolbar=no,resizable=yes,scrollbars=yes');");

                if (Grilla.Cells[16].Text == "S")
                {
                    imbControl = (ImageButton)Grilla.Cells[12].Controls[1];
                    imbControl.ImageUrl = "~/Images/quitar.gif";
                    imbControl.Enabled = false;
                    imbControl = (ImageButton)Grilla.Cells[13].Controls[1];
                    imbControl.ImageUrl = "~/Images/quitar.gif";
                    imbControl.Enabled = false;
                    imbControl = (ImageButton)Grilla.Cells[14].Controls[1];
                    imbControl.ImageUrl = "~/Images/quitar.gif";
                    imbControl.Enabled = false;
                }
                else
                {
                    if (Grilla.Cells[17].Text == "N")
                    {
                        imbControl = (ImageButton)Grilla.Cells[12].Controls[1];
                        if (Estado == "1" || Estado == "3")
                        {
                            if (Grilla.Cells[20].Text == "S") { }
                            // imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_IngresoOfertaVenta_6.aspx?ID=" + Grilla.Cells[0].Text + "&accion=C','Ofertar','width=600,height=400,left=350,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                            else
                            {
                                imbControl = (ImageButton)Grilla.Cells[12].Controls[1];
                                imbControl.ImageUrl = "~/Images/quitar.gif";
                                imbControl.Enabled = false;
                            }
                        }
                        else
                        {
                            if (Grilla.Cells[19].Text == "S" && Grilla.Cells[4].Text != "0 " && Grilla.Cells[4].Text != "0" && Grilla.Cells[4].Text != "") { }
                            //  imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_IngresoOfertaCompra_6.aspx?ID=" + Grilla.Cells[0].Text + "&accion=C&maxPostCompra=" + Grilla.Cells[18].Text + "','Ofertar','width=600,height=450,left=350,top=150,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                            else
                            {
                                imbControl = (ImageButton)Grilla.Cells[12].Controls[1];
                                imbControl.ImageUrl = "~/Images/quitar.gif";
                                imbControl.Enabled = false;
                            }
                        }
                        imbControl = (ImageButton)Grilla.Cells[13].Controls[1];
                        imbControl.ImageUrl = "~/Images/quitar.gif";
                        imbControl.Enabled = false;
                        imbControl = (ImageButton)Grilla.Cells[14].Controls[1];
                        imbControl.ImageUrl = "~/Images/quitar.gif";
                        imbControl.Enabled = false;
                    }
                    else
                    {
                        imbControl = (ImageButton)Grilla.Cells[13].Controls[1];
                        if (Estado == "1" || Estado == "3")
                        {
                            if (Grilla.Cells[20].Text == "S") { }
                            // imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_IngresoOfertaVenta_6.aspx?ID=" + Grilla.Cells[0].Text + "&accion=M','Ofertar','width=600,height=400,left=350,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                            else
                            {
                                imbControl = (ImageButton)Grilla.Cells[13].Controls[1];
                                imbControl.ImageUrl = "~/Images/quitar.gif";
                                imbControl.Enabled = false;
                            }
                        }
                        else
                        {
                            if (Grilla.Cells[19].Text == "S") { }
                            // imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_IngresoOfertaCompra_6.aspx?ID=" + Grilla.Cells[0].Text + "&accion=M&maxPostCompra=" + Grilla.Cells[18].Text + "','Ofertar','width=600,height=450,left=350,top=150,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                            else
                            {
                                imbControl = (ImageButton)Grilla.Cells[13].Controls[1];
                                imbControl.ImageUrl = "~/Images/quitar.gif";
                                imbControl.Enabled = false;
                            }
                        }
                        imbControl = (ImageButton)Grilla.Cells[12].Controls[1];
                        imbControl.ImageUrl = "~/Images/quitar.gif";
                        imbControl.Enabled = false;
                    }
                }
            }
            if (Session["tipoPerfil"].ToString() == "B")
            {
                dtgSubasta1.Columns[12].Visible = false;
                dtgSubasta1.Columns[13].Visible = false;
                dtgSubasta1.Columns[14].Visible = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnItemCommand_Click(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                switch (((ImageButton)e.CommandSource).ID)
                {
                    case "imbOfertar":
                        if (Estado == "4")
                        {
                            OpenIngresoPosturasCompra(e.Item.Cells[0].Text, "C", e.Item.Cells[18].Text);
                        }
                        else
                        {
                            OpenIngresoPosturasOferta(e.Item.Cells[0].Text, "C");
                        }
                        break;
                    //Muetra la modificacion
                    case "imbModificar":
                        if (Estado == "4")
                        {
                            OpenIngresoPosturasCompra(e.Item.Cells[0].Text, "M", e.Item.Cells[18].Text);
                        }
                        else
                        {
                            OpenIngresoPosturasOferta(e.Item.Cells[0].Text, "M");
                        }
                        break;
                    // Muestra la grafica
                    case "imbGrafica":
                        OpenCurvaOferDemAgre(e.Item.Cells[0].Text);
                        break;
                    // Elimina el registro 
                    case "imbEliminar":
                        {
                            string[] lsNombreParametros = { "@P_numero_rueda", "@P_numero_id", "@P_codigo_operador", "@P_estado" };
                            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char };
                            string[] lValorParametros = { CodSubasta, e.Item.Cells[0].Text, goInfo.cod_comisionista, Estado };
                            lConexion = new clConexion(goInfo);
                            lConexion.Abrir();
                            SqlDataReader lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_DelPostura6", lsNombreParametros, lTipoparametros, lValorParametros);
                            if (lLector.HasRows)
                            {
                                lLector.Read();
                                Toastr.Error(this, lLector["error"].ToString());
                                lLector.Close();
                                lLector.Dispose();
                                lConexion.Cerrar();
                            }
                            else
                            {
                                Toastr.Success(this, "Oferta eliminada correctamente");
                                lLector.Close();
                                lLector.Dispose();
                                lConexion.Cerrar();
                                CargarDatosGrilla();
                            }
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void SeleccionarNombrePestanias()
        {
            //EstadoLos compradores
            estados.Pestania1.Text = "<b>1. Creada</b>";
            estados.Pestania2.Text = "<b>1. Publicaci&oacute;n</b> de la cantidad disponible";
            estados.Pestania3.Text = "<b>2. Recibo</b> de las solicitudes de compra";
            estados.Pestania4.Text = "<b>3. Espera</b> Calce";
            estados.Pestania5.Text = "<b>4. Desarrollo</b> de la subasta";
            estados.Pestania6.Text = "<b>5. Finalizada</b>";
            //Descripción 
            estados.Descripcion1.InnerText = string.Empty;
            estados.Descripcion2.InnerText = "El gestor del Mercado en esta fase publica la cantidad total de gas disponible en cada punto de entrega";
            estados.Descripcion3.InnerText = "Los compradores que están interesados en contratar el gas ofrecido en la subasta, en esta fase envían sus solicitudes de compra al Gestor del Mercado";
            estados.Descripcion4.InnerText = "En esta fase el Gestor del Mercado de gas natural determina las partes que resulten adjudicadas, tras dar aplicaci�n al proceso de subasta";
            estados.Descripcion5.InnerText = "El Gestor del Mercado en esta fase aplica para cada punto de entrega con cantidades ofertadas, el mecanismo de subasta descrito en las Resoluciones CREG 136 de 2014 y 005 de 2017";
            estados.Descripcion6.InnerText = string.Empty;
        }

        #region CONTRATOS

        protected void btnContratos_Click(object sender, EventArgs e)
        {
            // Abrir modal de ofertas
            try
            {
                //
                Cargar_contratos();

                //Se abre el modal de Declaración De Precio De Reserva
                Modal.Abrir(this, mdlContratos.ID, mdlContratosInside.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        private void Cargar_contratos()
        {
            try
            {
                string[] lsNombreParametros = { "@P_numero_rueda" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int };
                string[] lValorParametros = { CodSubasta };
                SqlDataReader lLector;
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetMisContratos", lsNombreParametros, lTipoparametros, lValorParametros);

                var builder = new StringBuilder();

                builder.Append("<table  class=\"table table-bordered\">");
                builder.Append("<thead class=\"thead-light\">");
                builder.Append("<tr>");
                builder.Append("<th width='10%'>IDG</font></th>");
                builder.Append("<th width='10%'>PRODUCTO</font></th>");
                builder.Append("<th width='30%'>VENDEDOR</font></th>");
                builder.Append("<th width='20%'>COMPRADOR</font></th>");
                builder.Append("<th width='20%'>CANTIDAD</font></th>");
                builder.Append("<th width='20%'>PRECIO</font></th>");
                builder.Append("</tr>");
                builder.Append("</thead>");

                if (lLector.HasRows)
                {
                    builder.Append("<tbody>");
                    while (lLector.Read())
                    {
                        builder.Append("<tr>");
                        builder.Append("<td width='10%'>" + lLector["numero_id"] + "</font></td>");
                        builder.Append("<td width='10%'>" + lLector["desc_producto"] + "</font></td>");
                        builder.Append("<td width='25%'>" + lLector["nombre_venta"] + "</font></td>");
                        builder.Append("<td width='20%'>" + lLector["nombre_compra"] + "</font></td>");
                        builder.Append("<td width='10%'>" + lLector["cantidad"] + "</font></td>");
                        builder.Append("<td width='35%'>" + lLector["precio"] + "</font></td>");
                        builder.Append("</tr>");
                    }
                    builder.Append("</tbody>");
                }
                builder.Append("</table>");
                ltTablero.Text = builder.ToString();

                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();

                //Se abre el modal de oferta
                Modal.Abrir(this, mdlContratos.ID, mdlContratosInside.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        protected void CloseContratos_Click(Object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Contratos
                Modal.Cerrar(this, mdlContratos.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion CONTRATOS

        #region OFERTAS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnOfertas_Click(object sender, EventArgs e)
        {
            // Abrir modal de ofertas
            try
            {
                //Se cargan las ofertas
                Cargar_ofertas();

                //Se abre el modal de Ofertas
                Modal.Abrir(this, mdlOfertas.ID, mdlOfertasInside.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void Cargar_ofertas()
        {
            try
            {
                string[] lsNombreParametros = { "@P_numero_rueda" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int };
                string[] lValorParametros = { CodSubasta };
                SqlDataReader lLector;
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetMisOFertas6", lsNombreParametros, lTipoparametros, lValorParametros);

                var builder = new StringBuilder();

                builder.Append("<table  class=\"table table-bordered table-fixed\">");
                builder.Append("<thead class=\"thead-light\">");
                builder.Append("<tr>");
                builder.Append("<th width='10%'>IDG</font></th>");
                builder.Append("<th width='20%'>CANTIDAD</font></th>");
                builder.Append("<th width='25%'>PRECIO</font></th>");
                builder.Append("<th width='25%'>DELTA PRECIO</font></th>");
                builder.Append("<th width='20%'>TIPO OFERTA</font></th>");
                builder.Append("</tr>");
                builder.Append("</thead>");

                if (lLector.HasRows)
                {
                    builder.Append("<tbody>");
                    while (lLector.Read())
                    {
                        builder.Append("<tr>");
                        builder.Append("<td width='10%'>" + lLector["numero_id"] + "</font></td>");
                        builder.Append("<td width='20%'>" + lLector["cantidad_postura"] + "</font></td>");
                        builder.Append("<td width='25%'>" + lLector["precio"] + "</font></td>");
                        builder.Append("<td width='25%'>" + lLector["precio_delta"] + "</font></td>");
                        builder.Append("<td width='20%'>" + lLector["tipo_oferta"] + "</font></td>");
                        builder.Append("</tr>");
                    }
                    builder.Append("</tbody>");
                }

                builder.Append("</table>");

                ltTableroRF.Text = builder.ToString();

                //if (Session["refrescar_cont"].ToString() == "S")
                //{
                //    Session["refrescar_cont"] = "N";
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.contratos.location= 'frm_contratos.aspx';", true);
                //}
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseOfertas_Click(Object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Ofertas
                Modal.Cerrar(this, mdlOfertas.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion OFERTAS

        #region VENTAS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="accion"></param>
        protected void OpenIngresoPosturasOferta(string id, string accion)
        {
            if (string.IsNullOrEmpty(id))
            {
                Toastr.Error(this, "No Se enviaron los Parametros requeridos.!");
                return;
            }

            lConexion = new clConexion(goInfo);

            if (Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                hndID.Value = Request.QueryString["ID"];
                hndAccion.Value = Request.QueryString["accion"];
                /// Obtengo los Datos del ID Recibido
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_id_rueda", " numero_id = " + hndID.Value + " ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    /// Lleno las etiquetas de la pantalla con los datos del Id.
                    lblRueda.Text = "No. Rueda: " + lLector["numero_rueda"];
                    hdfNoRueda.Value = lLector["numero_rueda"].ToString();
                    hdfTpoRueda.Value = lLector["codigo_tipo_rueda"].ToString();
                    lblId.Text = "ID: " + Request.QueryString["ID"];
                    lblFecha.Text = "Fecha Rueda: " + lLector["fecha_rueda"].ToString().Substring(0, 10);
                    lblProducto.Text = "Producto: " + lLector["codigo_producto"] + "-" + lLector["desc_producto"];
                    lblUnidadMedida.Text = "Unidad Medida: " + lLector["codigo_unidad_medida"] + "-" + lLector["desc_unidad_medida"];
                    lblPuntoEntrega.Text = "Punto Entrega: " + lLector["codigo_punto_entrega"] + "-" + lLector["desc_punto_entrega"]; lblPeridoEnt.Text = "Periodo Entrega: " + lLector["codigo_periodo"] + "-" + lLector["desc_periodo"];
                    lblModalidad.Text = "Modalidad Contractual: " + lLector["codigo_modalidad"] + "-" + lLector["desc_modalidad"];
                    lblFechaIni.Text = "Fecha Entrega Inicial: " + lLector["fecha_entrega"].ToString().Substring(0, 10);
                    lblFechaFin.Text = "Fecha Entrega Final: " + lLector["fecha_entrega_fin"].ToString().Substring(0, 10);
                    /// Reviso si se esta creando la oferta o modificando
                    lLector.Close();
                    lLector.Dispose();

                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_tipos_operador tpo, m_operador_subasta opeS", " tpo.sigla='" + Session["TipoOperador"] + "' and tpo.codigo_tipo_operador = opeS.codigo_tipo_operador and opeS.codigo_tipo_rueda = '" + hdfTpoRueda.Value + "'and opeS.punta ='V'");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        hdfIngDelta.Value = lLector["ing_precio_delta"].ToString();
                    }
                    else
                        hdfIngDelta.Value = "N";
                    lLector.Close();
                    lLector.Dispose();

                    if (hdfIngDelta.Value == "N")
                    {
                        trPrecioVar.Visible = false;
                        TxtPrecioVar.Text = "0";
                    }
                    /// Reviso si ya existe una oferta para el ID y Operador para obtener
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_posturas_venta", " numero_id = " + hndID.Value + " And codigo_operador = '" + goInfo.cod_comisionista + "' ");
                    if (lLector.HasRows)
                    {
                        hndAccion.Value = "M";
                        lLector.Read();
                        TxtCntDisp.Text = lLector["cantidad_postura"].ToString();
                        TxtPrecioRes.Text = lLector["precio"].ToString();
                        TxtPrecioVar.Text = lLector["precio_delta"].ToString();
                        hdfNoPostura.Value = lLector["numero_postura"].ToString();
                        btnOfertar.Text = "Modificar Oferta";
                    }
                    else
                    {
                        hndAccion.Value = "C";
                        hdfNoPostura.Value = "0";
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    btnOfertar_Click(null, null);
                }
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "StartupAlert", "alert('" + "El ID enviado NO existe en el sistema.!" + "');", true);

                lConexion.Cerrar();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnOfertar_Click(object sender, EventArgs e)
        {

            string sCerrar = "N";
            string oError = "";
            string sProc = "";
            string lsError = "";
            string lsNoPostura = "";

            lsError = ValidarEntradas();
            if (lsError == "")
            {
                string[] lsNombreParametros = { "@P_numero_postura", "@P_numero_id", "@P_numero_rueda", "@P_codigo_operador", "@P_cantidad_postura", "@P_precio", "@P_precio_delta" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Float, SqlDbType.Float };
                string[] lValorParametros = { hdfNoPostura.Value, hndID.Value, hdfNoRueda.Value, goInfo.cod_comisionista, TxtCntDisp.Text.Replace(",", ""), TxtPrecioRes.Text.Replace(",", ""), TxtPrecioVar.Text.Replace(",", "") };

                lConexion.Abrir();
                try
                {
                    goInfo.mensaje_error = "";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetPosturaVentaId6", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        oError = "Error al Actualizar la Postura. " + goInfo.mensaje_error + "\\n";
                    }
                    else
                    {
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            oError = lLector["mensaje"].ToString();
                            lLector.Close();
                            lLector.Dispose();
                            lConexion.Cerrar();
                        }
                        else
                        {
                            lLector.Close();
                            lLector.Dispose();
                            lConexion.Cerrar();
                            Session["refrescar"] = "S";
                            Session["mis_ofertas"] = "S";
                            Toastr.Success(this, hndAccion.Value == "C"
                                    ? "Posturas Ingresadas Correctamente.!"
                                    : "Posturas Actualizadas Correctamente.!");
                            Modal.Cerrar(this, mdlIngresoPosturaCompra.ID);
                            CargarDatosGrilla();
                        }
                    }
                }
                catch (Exception ex)
                {
                    oError = "Error al realizar la oferta. " + ex.Message;
                    lConexion.Cerrar();
                }
                if (oError != "")
                {
                    Toastr.Error(this, oError);
                }
            }
            else
                Toastr.Error(this, lsError);
        }

        /// <summary>
        /// Relaiza la validacion del Ingreso de las Posturas
        /// </summary>
        /// <returns></returns>
        protected string ValidarEntradas()
        {
            string lsError = "";
            int oValor = 0;
            double oValor1 = 0;

            if (TxtCntDisp.Text.Trim().Length > 0)
            {
                try
                {
                    oValor = Convert.ToInt32(TxtCntDisp.Text.Replace(",", ""));
                    if (oValor <= 0)
                        lsError += "La Cantidad Disponible debe ser mayor que 0. \\n";

                }
                catch (Exception ex)
                {
                    lsError += "Valor Invalido en la Cantidad Disponible. \\n";
                }
            }
            else
                lsError += "Debe Ingresar la Cantidad Disponible. \\n";
            if (TxtPrecioRes.Text.Trim().Length > 0)
            {
                string sOferta = TxtPrecioRes.Text.Replace(",", "");
                int iPos = sOferta.IndexOf(".");
                if (iPos > 0)
                    if (sOferta.Length - iPos > 3)
                        lsError += "Se permiten máximo 2 decimales en el precio de reserva\\n";
                try
                {
                    if (Convert.ToDouble(sOferta) < 0)
                        lsError += "El precio de reserva debe ser mayor o igual que cero\\n";
                }
                catch (Exception ex)
                {
                    lsError += "El precio de reserva digitado no es válido\\n";
                }

            }
            else
                lsError += "Debe Ingresar el precio de reserva. \\n";

            if (trPrecioVar.Visible)
            {
                if (TxtPrecioVar.Text.Trim().Length > 0)
                {
                    string sOferta = TxtPrecioRes.Text.Replace(",", "");
                    int iPos = sOferta.IndexOf(".");
                    if (iPos > 0)
                        if (sOferta.Length - iPos > 3)
                            lsError += "Se permiten máximo 2 decimales en la variacion del precio\\n";
                    try
                    {
                        Convert.ToDouble(sOferta);
                    }
                    catch (Exception ex)
                    {
                        lsError += "La variacion del precio  no es válido\\n";
                    }
                }
                else
                    lsError += "Debe Ingresar la variación del precio. \\n";
            }
            return lsError;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OpenModalVentas_Click(Object sender, EventArgs e)
        {
            // Abrir modal de ofertas
            try
            {
                //Se abre el modal de Declaración De Precio De Reserva
                Modal.Abrir(this, mdlOferta.ID, mdlOfertaInside.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseModalVentas_Click(Object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Contratos
                Modal.Cerrar(this, mdlOferta.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion VENTAS

        #region COMPRAS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="accion"></param>
        /// <param name="maxPostCompra"></param>
        protected void OpenIngresoPosturasCompra(string id, string accion, string maxPostCompra)
        {

            lConexion = new clConexion(goInfo);

            hndIDCompra.Value = id;
            hndAccionCompra.Value = accion;
            hdfMaxPostCompra.Value = maxPostCompra;
            /// Obtengo los Datos del ID Recibido
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_id_rueda", " numero_id = " + hndIDCompra.Value + " ");
            if (lLector.HasRows)
            {
                lLector.Read();
                /// Lleno las etiquetas de la pantalla con los datos del Id.
                lblRuedaCompra.Text = "No. Rueda: " + lLector["numero_rueda"];
                hdfNoRuedaCompra.Value = lLector["numero_rueda"].ToString();
                lblIdCompra.Text = "ID: " + id;
                lblFechaCompra.Text = "Fecha Rueda: " + lLector["fecha_rueda"].ToString().Substring(0, 10);
                lblProductoCompra.Text = "Producto: " + lLector["codigo_producto"] + "-" + lLector["desc_producto"];
                lblUnidadMedidaCompra.Text = "Unidad Medida: " + lLector["codigo_unidad_medida"] + "-" + lLector["desc_unidad_medida"];
                lblPuntoEntregaCompra.Text = "Punto Entrega: " + lLector["codigo_punto_entrega"] + "-" + lLector["desc_punto_entrega"];
                lblPeridoEntCompra.Text = "Periodo Entrega: " + lLector["codigo_periodo"] + "-" + lLector["desc_periodo"];
                lblModalidadCompra.Text = "Modalidad Contractual: " + lLector["codigo_modalidad"] + "-" + lLector["desc_modalidad"];
                lblFechaIniCompra.Text = "Fecha Entrega Inicial: " + lLector["fecha_entrega"].ToString().Substring(0, 10);
                lblFechaFinCompra.Text = "Fecha Entrega Inicial: " + lLector["fecha_entrega_fin"].ToString().Substring(0, 10);
                hndCntCompra.Value = lLector["cantidad_total_venta"].ToString();
                /// Reviso si se esta creando la oferta o modificando
                if (hndAccionCompra.Value == "M")
                    btnCompra.Text = "Modificar Oferta";
                lLector.Close();
                lLector.Dispose();

                /// Llamo el Procedimiento que arma la grilla del ingreso de las ofertas
                string[] lsNombreParametros = { "@P_numero_id", "@P_codigo_operador", "@P_max_post_compra" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { hndIDCompra.Value, goInfo.cod_comisionista, hdfMaxPostCompra.Value };

                dtgPosturasC.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetPosturasC", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgPosturasC.DataBind();
                if (dtgPosturasC.Items.Count > 0)
                {
                    TextBox oCnt = null;
                    TextBox oPre = null;
                    int ldCont = 1;
                    foreach (DataGridItem Grilla in dtgPosturasC.Items)
                    {
                        if (hndAccionCompra.Value == "M")
                        {
                            Grilla.Cells[0].Text = ldCont.ToString();
                            ldCont++;
                        }
                        oCnt = (TextBox)Grilla.Cells[2].Controls[1];
                        oPre = (TextBox)Grilla.Cells[1].Controls[1];
                        //oPre.Focus = true;
                        if (Grilla.Cells[3].Text.Replace("&nbsp;", "") != "")
                            oCnt.Text = Grilla.Cells[3].Text.Replace("&nbsp;", "");
                        if (Grilla.Cells[4].Text.Replace("&nbsp;", "") != "")
                            oPre.Text = Grilla.Cells[4].Text.Replace("&nbsp;", "");
                    }
                }

                lConexion.Cerrar();
                OpenModalComprar_Click(null, null);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected string ValidarEntradas1()
        {
            string lsError = "";
            int oValor = 0;
            double oValor1 = 0;
            int ldValorAnt = 0;
            double ldPrecioAnt = 0;
            int ldConta = 0;

            TextBox oCnt = null;
            TextBox oPre = null;
            foreach (DataGridItem Grilla in dtgPosturasC.Items)
            {
                oCnt = (TextBox)Grilla.Cells[2].Controls[1];
                oPre = (TextBox)Grilla.Cells[1].Controls[1];
                oCnt.Text = oCnt.Text.Replace(",", "");
                oPre.Text = oPre.Text.Replace(",", "");
                if (oCnt.Text.Trim().Length > 0)
                {
                    try
                    {
                        // Validacion para que no permita cantidad  menor o Igual a 0 - 20150709
                        if (Convert.ToInt32(oCnt.Text.Replace(",", "")) <= 0)
                            lsError += "La Cantidad de la Postura " + Grilla.Cells[0].Text + " no puede ser menor o Igual a 0\\n";
                        else
                        {

                            if (Convert.ToInt32(oCnt.Text.Replace(",", "")) > Convert.ToInt32(hndCntCompra.Value))
                                lsError += "La Cantidad de la Postura " + Grilla.Cells[0].Text + " no puede ser mayor que la cantidad ofrecida \\n";
                        }

                    }
                    catch (Exception ex)
                    {
                        lsError += "Valor Invalido en la Cantidad de la Postura " + Grilla.Cells[0].Text + "\\n";
                    }
                }
                else
                    lsError += "Debe Ingresar la Cantidad de la Postura " + Grilla.Cells[0].Text + "\\n";
                if (oPre.Text.Trim().Length > 0)
                {
                    string sOferta = oPre.Text.Replace(",", "");
                    int iPos = sOferta.IndexOf(".");
                    if (iPos > 0)
                        if (sOferta.Length - iPos > 3)
                            lsError += "Se permiten máximo 2 decimales en el precio de a postura " + Grilla.Cells[0].Text + "\\n";
                    try
                    {
                        if (Convert.ToDouble(sOferta) < 0)
                            lsError += "El precio de la postura " + Grilla.Cells[0].Text + " debe ser mayor o igual que cero\\n";
                    }
                    catch (Exception ex)
                    {
                        lsError += "El precio de la postura " + Grilla.Cells[0].Text + " no es válido\\n";
                    }
                }
                else
                    lsError += "Debe Ingresar el Precio de la Postura " + Grilla.Cells[0].Text + "\\n";
                if (lsError != "")
                    break;
                else
                {
                    if (ldConta > 0)
                    {
                        if (Convert.ToInt32(oCnt.Text) < ldValorAnt)
                            lsError += "La Cantidad de la Postura " + Grilla.Cells[0].Text + " debe ser mayor a la postura anterior. \\n";
                        if (Convert.ToDouble(oPre.Text) >= ldPrecioAnt)
                            lsError += "El  Precio de la Postura " + Grilla.Cells[0].Text + " debe ser menor al de la postura anterior. \\n";

                        ldValorAnt = Convert.ToInt32(oCnt.Text);
                        ldPrecioAnt = Convert.ToDouble(oPre.Text);
                    }
                    else
                    {
                        ldValorAnt = Convert.ToInt32(oCnt.Text);
                        ldPrecioAnt = Convert.ToDouble(oPre.Text);
                    }
                }
                ldConta++;
            }
            return lsError;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OpenModalComprar_Click(Object sender, EventArgs e)
        {
            // Abrir modal de ofertas
            try
            {
                //Se abre el modal de Declaración De Precio De Reserva
                Modal.Abrir(this, mdlIngresoPosturaCompra.ID, mdlIngresoPosturaCompraInside.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseModalComprar_Click(Object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Contratos
                Modal.Cerrar(this, mdlIngresoPosturaCompra.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion COMPRAS

        #region Ingreso Posturas de Compra

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnComprar_Click(object sender, EventArgs e)
        {
            string sCerrar = "N";
            string oError = "";
            string sProc = "";
            string lsError = "";
            string lsNoPostura = "";

            string[] lsNombreParametros = { "@P_numero_postura", "@P_numero_id", "@P_numero_rueda", "@P_codigo_operador", "@P_precio_postura", "@P_cantidad_postura", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Float, SqlDbType.Int, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "0", "0", "0", "0", "0", "0" }; ///  Accion 0=Creo la Temporal 1=Crear 2=Modificar

            lsError = ValidarEntradas1();
            if (lsError == "")
            {
                lConexion.Abrir();
                SqlTransaction oTransaccion;
                oTransaccion = lConexion.gObjConexion.BeginTransaction(IsolationLevel.Serializable);
                try
                {
                    TextBox oCnt = null;
                    TextBox oPre = null;
                    foreach (DataGridItem Grilla in dtgPosturasC.Items)
                    {
                        oCnt = (TextBox)Grilla.Cells[2].Controls[1];
                        oPre = (TextBox)Grilla.Cells[1].Controls[1];
                        oCnt.Text = oCnt.Text.Replace(",", "");
                        oPre.Text = oPre.Text.Replace(",", "");
                        lValorParametros[1] = hndIDCompra.Value;
                        lValorParametros[2] = hdfNoRuedaCompra.Value;
                        lValorParametros[3] = goInfo.cod_comisionista;
                        lValorParametros[4] = oPre.Text;
                        lValorParametros[5] = oCnt.Text;
                        if (hndAccionCompra.Value == "C")
                            lsNoPostura = Grilla.Cells[0].Text.Replace(",", "");
                        else
                            lsNoPostura = Grilla.Cells[5].Text.Replace(",", "");
                        lValorParametros[6] = "0";
                        lValorParametros[0] = lsNoPostura;

                        goInfo.mensaje_error = "";
                        if (!DelegadaBase.Servicios.EjecutarProcedimientoConTransaccion(lConexion.gObjConexion, "pa_SetPosturaCompraId6", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo))
                        {
                            if (goInfo.mensaje_error != "")
                            {
                                oError = "Error al Actualizar la Postura. " + goInfo.mensaje_error + "\\n";
                                oTransaccion.Rollback();
                                break;
                            }
                        }
                    }
                    if (oError == "")
                    {
                        if (hndAccionCompra.Value == "C")
                        {
                            lValorParametros[2] = hdfNoRuedaCompra.Value;
                            lValorParametros[6] = "1";
                            lValorParametros[0] = "0";
                        }
                        else
                        {
                            lValorParametros[2] = hdfNoRuedaCompra.Value;
                            lValorParametros[6] = "2";
                            lValorParametros[0] = lsNoPostura;
                        }
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetPosturaCompraId6", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                        if (goInfo.mensaje_error != "")
                        {
                            oError = "Error al Actualizar la Postura. " + goInfo.mensaje_error + "\\n";
                            oTransaccion.Rollback();
                        }
                        else
                        {
                            if (lLector.HasRows)
                            {
                                lLector.Read();
                                if (lLector["mensaje"].ToString() != "")
                                {
                                    oError = lLector["mensaje"].ToString();
                                    lLector.Close();
                                    lLector.Dispose();
                                    oTransaccion.Rollback();
                                    lConexion.Cerrar();
                                }
                                else
                                {
                                    lLector.Close();
                                    lLector.Dispose();
                                    oTransaccion.Commit();
                                    lConexion.Cerrar();
                                    Session["refrescar"] = "S";
                                    Session["mis_ofertas"] = "S";
                                    Toastr.Success(this, hndAccion.Value == "C" ? "Posturas Ingresadas Correctamente.!" : "Posturas Actualizadas Correctamente.!");
                                    Modal.Cerrar(this, mdlIngresoPosturaCompra.ID);
                                    CargarDatosGrilla();
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    oTransaccion.Rollback();
                    oError = "Error al realizar la oferta. " + ex.Message;
                    lConexion.Cerrar();
                }
                if (oError != "")
                {
                    Toastr.Error(this, oError);
                }
            }
            else
                Toastr.Error(this, lsError);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseIngresoPosturasCompra_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Ingreso Posturas de Compra
                Modal.Cerrar(this, mdlIngresoPosturaCompra.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Ingreso Posturas de Compra

        #region Curva de Oferta y Demanda Agregada

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lblNoId"></param>
        private void OpenCurvaOferDemAgre(string lblNoId)
        {
            var transporte = true;
            goInfo.Programa = "Curva de oferta y demanda agregada";

            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            SqlCommand lComando1 = new SqlCommand();
            SqlDataAdapter lsqldata1 = new SqlDataAdapter();
            lComando1.Connection = lConexion.gObjConexion;
            lComando1.CommandTimeout = 3600;
            lComando1.CommandType = CommandType.StoredProcedure;

            //cambios grafica 20160202
            SqlDataReader lLector;
            lComando1.CommandText = "pa_ValidarExistencia";
            lComando1.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = "t_id_rueda";
            lComando1.Parameters.Add("@P_Filtro", SqlDbType.VarChar).Value = "numero_id = " + lblNoId;
            lLector = lComando1.ExecuteReader();
            if (lLector.HasRows)
            {
                lLector.Read();
                if (lLector["codigo_producto"].ToString() == "1")
                {
                    lblPuntoCurva.Text = "Punto de Entrega: " + lLector["desc_punto_entrega"];
                    transporte = false;
                }
                else
                {
                    lblPuntoCurva.Text = "Ruta: " + lLector["desc_punto_entrega"] + "-" + lLector["desc_punto_fin"];
                }
            }
            else
                lblPuntoCurva.Text = "";
            lComando1.Parameters.Clear();
            lLector.Close();
            lLector.Dispose();

            lComando1.CommandText = "pa_GetPreciosCurva1";
            lComando1.Parameters.Add("@P_numero_id", SqlDbType.Int).Value = lblNoId;
            lComando1.ExecuteNonQuery();
            lsqldata1.SelectCommand = lComando1;
            lsqldata1.Fill(lds);
            dtgInformacion.DataSource = lds;
            dtgInformacion.DataBind();
            lConexion.Cerrar();

            //Carga la grafica
            AuctionGraphics.GenerateGraph(this, dtgInformacion, lblNoId, transporte);
            //Se abre el modal de Ingreso de Declaración de Información
            Modal.Abrir(this, mdlCurvaOfertaDemAgre.ID, mdlCurvaOfertaDemAgreInside.ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseCurvaOferDemAgre_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Ingreso de Declaración de Información
                Modal.Cerrar(this, mdlCurvaOfertaDemAgre.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Curva de Oferta y Demanda Agregada
    }
}
