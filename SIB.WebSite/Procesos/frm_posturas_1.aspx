﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_posturas_1.aspx.cs" Inherits="Procesos_frm_posturas_1"
    MasterPageFile="~/PlantillaPrincipal.master" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" language="javascript">

        window.onload = function() {
            var pos = window.name || 0;
            window.scrollTo(0, pos);
        }
        window.onunload = function() {
            window.name = self.pageYOffset || (document.documentElement.scrollTop + document.body.scrollTop);
        }

    </script>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="divOferta" runat="server" style="position: absolute; left: 0; top: 0; width: 100%;">
                <table cellpadding="0" cellspacing="0" bgcolor="#000000">
                    <tr>
                        <td align="center">
                            <asp:DataGrid ID="dtgSubasta1" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                ItemStyle-CssClass="td2" HeaderStyle-CssClass="th2" runat="server" OnItemCommand="OnItemCommand_Click">
                                <ItemStyle CssClass="td2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="15px">
                                </ItemStyle>
                                <Columns>
                                    <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" ItemStyle-HorizontalAlign="Left">
                                    </asp:BoundColumn>
                                    <%--1--%><asp:BoundColumn DataField="desc_modalidad" HeaderText="Tipo" ItemStyle-HorizontalAlign="Left">
                                    </asp:BoundColumn>
                                    <%--2--%><asp:BoundColumn DataField="tiempo_entrega" HeaderText="Duracion" ItemStyle-HorizontalAlign="Center">
                                    </asp:BoundColumn>
                                    <%--3--%><asp:BoundColumn DataField="desc_punto_entrega" HeaderText="lugar" ItemStyle-HorizontalAlign="Left">
                                    </asp:BoundColumn>
                                    <%--4--%><asp:BoundColumn DataField="precio_reserva" HeaderText="Precio Reserva"
                                        DataFormatString="{0:###,###,###,###,###,###,##0.00}" ItemStyle-HorizontalAlign="Right">
                                    </asp:BoundColumn>
                                    <%--5--%><asp:BoundColumn DataField="cantidad_venta" HeaderText="Cantidad Venta"
                                        DataFormatString="{0:###,###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right">
                                    </asp:BoundColumn>
                                    <%--6--%><asp:BoundColumn DataField="cantidad_comprador" HeaderText="Cantidad comprador"
                                        DataFormatString="{0:###,###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right">
                                    </asp:BoundColumn>
                                    <%--7--%><asp:BoundColumn DataField="precio_maximo_c" HeaderText="precio maximo compra"
                                        DataFormatString="{0:###,###,###,###,###,###,##0.00}" ItemStyle-HorizontalAlign="Right">
                                    </asp:BoundColumn>
                                    <%--8--%><asp:BoundColumn DataField="hora_inicio" HeaderText="Inicio de la ronda"
                                        ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <%--9--%><asp:BoundColumn DataField="hora_fin" HeaderText="Fin de la ronda" ItemStyle-HorizontalAlign="Center">
                                    </asp:BoundColumn>
                                    <%--10--%><asp:BoundColumn DataField="descripcion_estado" HeaderText="estado" ItemStyle-HorizontalAlign="Center">
                                    </asp:BoundColumn>
                                    <%--11--%><asp:BoundColumn DataField="ronda_actual" HeaderText="ronda" ItemStyle-HorizontalAlign="Center">
                                    </asp:BoundColumn>
                                    <%--12--%><asp:TemplateColumn HeaderText="Ofr">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbOfertar" runat="server" ToolTip="Ofertar" ImageUrl="~/Images/nuevo.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--13--%><asp:TemplateColumn HeaderText="Mod">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbModificar" runat="server" ToolTip="Modificar Oferta" ImageUrl="~/Images/modificar.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--14--%><asp:TemplateColumn HeaderText="Elim">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbEliminar" runat="server" ToolTip="Eliminar Oferta" ImageUrl="~/Images/eliminar.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--15--%><asp:BoundColumn DataField="estado" Visible="false"></asp:BoundColumn>
                                    <%--16--%><asp:BoundColumn DataField="numero_rueda" Visible="false"></asp:BoundColumn>
                                    <%--17--%><asp:BoundColumn DataField="puede_ofertar_cont" Visible="false"></asp:BoundColumn>
                                    <%--18--%><asp:BoundColumn DataField="cantidad_anterior" Visible="false"></asp:BoundColumn>
                                    <%--19--%><asp:BoundColumn DataField="prox_precio_res" HeaderText="prox precio" DataFormatString="{0:###,###,###,###,###,###,##0.00}"
                                        ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--20--%><asp:TemplateColumn HeaderText="Precio">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbprecio" runat="server" ToolTip="Definir Precio" ImageUrl="~/Images/modificar.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--21--%><asp:TemplateColumn HeaderText="Cnt Adc">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbCantidad" runat="server" ToolTip="Cantidad adicional" ImageUrl="~/Images/modificar.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--22--%><asp:TemplateColumn HeaderText="grafica">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbGrafica" runat="server" ToolTip="curva agregada" ImageUrl="~/Images/grafica.jpg" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--23--%><asp:BoundColumn DataField="cantidad_ope_v" DataFormatString="{0:###,###,###,###,###,###,##0}"
                                        ItemStyle-HorizontalAlign="Right" Visible="false"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="th2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="13px">
                                </HeaderStyle>
                            </asp:DataGrid>
                            <asp:HiddenField ID="hndCodSubasta" runat="server" />
                            <asp:HiddenField ID="HndFechaRueda" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>