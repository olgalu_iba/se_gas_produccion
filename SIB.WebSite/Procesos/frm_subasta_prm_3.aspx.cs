﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;

//using System.Windows.Forms;



public partial class Procesos_frm_subasta_prm_3 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlRuta, "m_ruta_snt", "  estado <> 'I' order by descripcion", 0, 4);
            SqlDataReader lLector;
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda rue,m_estado_gas est", " rue.numero_rueda =" + Session["numero_rueda"].ToString() + " and est.tipo_estado ='R' and rue.estado = est.sigla_estado");
            if (lLector.HasRows)
            {
                lLector.Read();
                lblRueda.Text = lLector["descripcion"].ToString();
                ImgExcel.Visible = false;
                btnReactivar.Visible = false;
                btnSuspender.Visible = true;
                switch (lLector["estado"].ToString())
                {
                    case "C": lblFase.Text = "Creada";
                        lblHora.Text = lLector["hora_prox_fase"].ToString();
                        break;
                    case "3": lblFase.Text = "Publicación de capacidad disponible";
                        lblHora.Text = lLector["hora_ini_publi_v_sci"].ToString() + " - " + lLector["hora_fin_publi_v_sci"].ToString();
                        ImgExcel.Visible = true;
                        break;
                    case "4": lblFase.Text = "Ingreso posturas de compra";
                        lblHora.Text = lLector["hora_ini_rec_solicitud_c_sci"].ToString() + " - " + lLector["hora_fin_rec_solicitud_c_sci"].ToString();
                        break;
                    case "5": lblFase.Text = "Calce de operaciones";
                        lblHora.Text = lLector["hora_ini_negociacioni_sci"].ToString() + " - " + lLector["hora_fin_negociacioni_sci"].ToString();
                        break;
                    case "T": lblFase.Text = "En espera";
                        if (lLector["hora_prox_fase"].ToString() == lLector["hora_ini_rec_solicitud_c_sci"].ToString())
                            lblHora.Text = lLector["hora_fin_publi_v_sci"].ToString() + " - "+ lLector["hora_prox_fase"].ToString();
                        else
                            lblHora.Text = lLector["hora_fin_rec_solicitud_c_sci"].ToString() + " - "+ lLector["hora_prox_fase"].ToString();
                        break;
                    case "F": lblFase.Text = "Finalizada";
                        lblHora.Text = "";
                        break;
                    case "S": lblFase.Text = "Suspendida";
                        lblHora.Text = lLector["hora_prox_fase"].ToString();
                        btnReactivar.Visible = true;
                        btnSuspender.Visible = false;
                        break;
                    case "Z": lblFase.Text = "Suspendida Definitivamente";
                        lblHora.Text = "";
                        break;
                }
                Session["estado"] = lLector["estado"].ToString();
            }
            lConexion.Cerrar();

            if (goInfo.cod_comisionista == "0")
            {
                lblAgente.Text = Session["nomOperador"].ToString();
                btnMensaje.Visible = true;
            }
            else
            {
                lblAgente.Text = Session["nomOperador"].ToString();
                btnMensaje.Visible = false;
                btnSuspender.Visible = false;
                btnReactivar.Visible = false;
            }
            if (Session["ind_tipo_rueda"].ToString() == "P")
                lblPunto.Text = "Ruta";
            else
                lblPunto.Text = "Tramo";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.titulo.location= 'frm_titulos_3.aspx?';", true);
        }
        catch (Exception ex)
        {

        }
    }
    /// <summary>
    /// Nombre: btnBuscar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para Procesar la consulta
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Session["codigo_ruta"] = ddlRuta.SelectedValue;
        Session["refrescar"] = "S";
        Session["refresca_mis_posturas"] = "S";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.postura.location= 'frm_posturas_3.aspx?';", true);
    }
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// 20160128
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador", "@P_codigo_ruta" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { Session["numero_rueda"].ToString(), goInfo.cod_comisionista, Session["codigo_ruta"].ToString() };
        lConexion.Abrir();
        dtgSubasta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaPuja3", lsNombreParametros, lTipoparametros, lValorParametros);
        dtgSubasta.DataBind();
        dtgSubasta.Visible = true;

        DataSet lds = new DataSet();
        SqlDataAdapter lsqldata = new SqlDataAdapter();
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfRueda" + DateTime.Now + ".xls";
        string lstitulo_informe = "Consulta Ids Subasta " + Session["numero_rueda"].ToString();
        decimal ldCapacidad = 0;
        StringBuilder lsb = new StringBuilder();
        ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
        StringWriter lsw = new StringWriter(lsb);
        HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
        Page lpagina = new Page();
        HtmlForm lform = new HtmlForm();
        lpagina.EnableEventValidation = false;
        lpagina.DesignerInitialize();
        lpagina.Controls.Add(lform);
        dtgSubasta.EnableViewState = false;
        lform.Controls.Add(dtgSubasta);
        lpagina.RenderControl(lhtw);
        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "aplication/vnd.ms-excel";
        Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
        Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
        Response.ContentEncoding = System.Text.Encoding.Default;
        Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
        Response.Charset = "UTF-8";
        Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
        Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
        Response.ContentEncoding = Encoding.Default;
        Response.Write(lsb.ToString());
        Response.End();
        lds.Dispose();
        lsqldata.Dispose();
        lConexion.CerrarInforme();
        dtgSubasta.Visible = false;
    }

}
