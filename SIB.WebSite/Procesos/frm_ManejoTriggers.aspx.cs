﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_ManejoTriggers : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");

        //////////Establese los permisos del sistema
        ////////EstablecerPermisosSistema();

        if (!IsPostBack)
        {
            lblTitulo.Text = "Manejo de Triggers";
            goInfo.Programa = lblTitulo.Text;
            /// Llenar controles del Formulario

        }
    }
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lkbConsultar_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        string[] lsNombreParametros = { "@P_tabla", "@P_indicador", };
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.Char };
        string[] lValorParametros = { txtTabla.Text, ddlAccion.SelectedValue };

        SqlDataReader lLector;
        lblMensaje.Text = "";
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        try
        {
            // Ejecuta procedimiento
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetTriggers", lsNombreParametros, lTipoparametros, lValorParametros);
            if (lLector.HasRows)
            {
                //lLector.Read();
                while (lLector.Read())
                {
                    lblMensaje.Text += lLector["mensaje"].ToString() + "<br>";
                }
            }
            else
            {
                lblMensaje.Text += "Error al ejecutar el procedimiento. <br>";
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lConexion.Cerrar();
            lblMensaje.Text += "Se Presentó un Problema al ejecutar el procedimiento. <br>";
        }
        if (lblMensaje.Text != "")
        {
            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";
        }

    }
}

