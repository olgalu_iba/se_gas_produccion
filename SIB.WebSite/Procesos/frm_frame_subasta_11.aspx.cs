﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Procesos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frm_frame_subasta_11 : Page
    {
        #region Propiedades

        private InfoSessionVO _goInfo;
        private clConexion _lConexion;
        private string _strRutaArchivo;
        private static string _cadenaCon;

        /// <summary>
        /// 
        /// </summary>
        public string CodSubasta
        {
            get { return ViewState["CodSubasta"] != null && !string.IsNullOrEmpty(ViewState["CodSubasta"].ToString()) ? ViewState["CodSubasta"].ToString() : "0"; }
            set { ViewState["CodSubasta"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CodRuta
        {
            get { return ViewState["CodRuta"] != null && !string.IsNullOrEmpty(ViewState["CodRuta"].ToString()) ? ViewState["CodRuta"].ToString() : "0"; }
            set { ViewState["CodRuta"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string EstadoSubasta
        {
            get { return ViewState["EstadoSubasta"] != null && !string.IsNullOrEmpty(ViewState["EstadoSubasta"].ToString()) ? ViewState["EstadoSubasta"].ToString() : "C"; }
            set { ViewState["EstadoSubasta"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Hora
        {
            get { return ViewState["hora"] != null && !string.IsNullOrEmpty(ViewState["hora"].ToString()) ? ViewState["hora"].ToString() : ""; }
            set { ViewState["hora"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string MisOfertas
        {
            get { return ViewState["mis_ofertas"] != null && !string.IsNullOrEmpty(ViewState["mis_ofertas"].ToString()) ? ViewState["mis_ofertas"].ToString() : "N"; }
            set { ViewState["mis_ofertas"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Refrescar
        {
            get { return ViewState["refrescar"] != null && !string.IsNullOrEmpty(ViewState["refrescar"].ToString()) ? ViewState["refrescar"].ToString() : "N"; }
            set { ViewState["refrescar"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string IndTipoRueda
        {
            get { return ViewState["ind_tipo_rueda"] != null && !string.IsNullOrEmpty(ViewState["ind_tipo_rueda"].ToString()) ? ViewState["ind_tipo_rueda"].ToString() : "1"; }
            set { ViewState["ind_tipo_rueda"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HoraMensaje
        {
            get { return ViewState["hora_mensaje"] != null && !string.IsNullOrEmpty(ViewState["hora_mensaje"].ToString()) ? ViewState["hora_mensaje"].ToString() : ""; }
            set { ViewState["hora_mensaje"] = value; }
        }

        #endregion Propiedades

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Se cargan los valores iniciales del formulario 
                CargarPagina();
                if (IsPostBack) return;
                //Se inicializan los controles para la primera ves que se ejecuta el formulario 

                InicializarPagina();
                // Se realiza el cargue de la rueda
                //Se crea la conexión  
                _lConexion = new clConexion(_goInfo);
                //_lConexion.Abrir();
                //SqlDataReader lLector = DelegadaBase.Servicios.LlenarControl(_lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda", " codigo_tipo_subasta =11 and fecha_prox_apertura = convert(varchar(10),getdate(),111) order by numero_rueda desc ");
                //if (lLector.HasRows)
                //{
                //    lLector.Read();
                //    CodSubasta = lLector["numero_rueda"].ToString();
                //    IndTipoRueda = lLector["ind_tipo_rueda"].ToString();
                //    EstadoSubasta = lLector["estado"].ToString();
                //}
                //lLector.Close();
                //lLector.Dispose();
                //_lConexion.Cerrar();
                _lConexion.Abrir();
                //aqui
                buttonsSubasta.ListaRueda = LlenarControles(_lConexion.gObjConexion, "t_rueda rue", "rue.fecha_rueda = convert(varchar(10),getdate(),111) and rue.codigo_tipo_subasta = 11 ", 1, 17);
                //buttonsSubasta.ListaRueda = LlenarControles(_lConexion.gObjConexion, "t_rueda rue", "rue.fecha_rueda = '2021/02/15' and rue.codigo_tipo_subasta = 11 ", 1, 17);
                _lConexion.Cerrar();

                CargarRueda();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private void CargarPagina()
        {
            //Se redirecciona al Login si no hay una sesión iniciada    
            _strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"];
            _goInfo = (InfoSessionVO)Session["infoSession"];
            if (_goInfo == null) Response.Redirect("~/login.aspx");



            //Variables
            Session["destino_rueda"] = "T";
            Session["refrescar_tit"] = "N";
            Session["refrescar_prm"] = "N";
            Session["refrescar_cont"] = "S";
            Session["ofertas"] = "N";

            //Eventos
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttonsSubasta.RuedaOnSelectedIndexChanged += ddlRueda_SelectedIndexChanged;
            buttonsSubasta.PuntoOnSelectedIndexChanged += ddlPunto_SelectedIndexChanged;
            buttonsSubastaExtender.SuspenderOnclick += Suspender_Click;
            buttonsSubastaExtender.ReactivarOnclick += OpenReactivar;
            buttonsSubastaExtender.ContratoOnclick += AbrirContratos;
            buttonsSubastaExtender.EnviarMensajeOnclick += OpenEnviarMensaje;
            buttonsSubastaExtender.MisMensajesOnclick += AbrirMensajes;

            estados.TimeServer.Value = DateTime.Now.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Nombre: ddlRueda_SelectedIndexChanged
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Obtiene la Informacion de la Inactivacion del Comisionista para Visualizarla en la pantalla
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlRueda_SelectedIndexChanged(object sender, EventArgs e)
        {
            CodSubasta = buttonsSubasta.ValorRueda;
            CargarRueda();
        }

        /// <summary>
        /// Inicializa el contenido del formulario 
        /// </summary>
        private void InicializarPagina()
        {
            //Titulo
            Master.Titulo = "Subasta";
            //Descripcion
            Master.DescripcionPagina = "Capacidad de Transporte ante Congestión Contractual"; //20220211 ajsute labels

            ////Se cargan los nombres de las pestañas 
            SeleccionarNombrePestanias();
        }

        /// <summary>
        /// 
        /// </summary>
        private void CargarRueda()
        {
            //Lista para la rueda 
            var ddlRueda = new DropDownList();
            //Lista para el punto o ruta
            var ddlNueva = new DropDownList();

            if (CodSubasta == null )
                CodSubasta = "0";

            //Se habilita la selección de la rueda   
            buttonsSubasta.EditarRueda = true;

            
            //Tiempo espera
            divTiempoEspera.Visible = false;

            //Se carga la rueda 
            //var lItemRueda = new ListItem { Value = "1", Text = lLector["descripcion"].ToString() };
            //ddlRueda.Items.Add(lItemRueda);

            //Se seleccionan los botones    
            buttons.SwitchOnButton(EnumBotones.Ninguno);
            buttonsSubasta.SwitchOnButton(EnumBotonesSubasta.Rueda, EnumBotonesSubasta.Punto);
            buttonsSubastaExtender.SwitchOnButton(EnumBotonesSubasta.EnviarMensaje, EnumBotonesSubasta.MisMensajes, EnumBotonesSubasta.Contrato, EnumBotonesSubasta.Suspender);

            //
            _lConexion = new clConexion(_goInfo);
            _lConexion.Abrir();
            SqlDataReader lLector = DelegadaBase.Servicios.LlenarControl(_lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda rue,m_estado_gas est", " rue.numero_rueda =" + CodSubasta + " and est.tipo_estado ='R' and rue.estado = est.sigla_estado");
            if (lLector.HasRows)
            {
                lLector.Read();

                /*Se deshabilitan los estados y cronómetros*/
                estados.Visible = true;

                //estados.Pestania1.CssClass = "nav-link disabled";
                estados.Pestania2.CssClass = "nav-link disabled";
                estados.Pestania3.CssClass = "nav-link disabled";
                estados.Pestania4.CssClass = "nav-link disabled";
                estados.Pestania5.CssClass = "nav-link disabled";
                estados.Pestania6.CssClass = "nav-link disabled";
                estados.Pestania7.CssClass = "nav-link disabled";

                //estados.Pestania1.Visible = true;
                estados.Pestania2.Visible = true;
                estados.Pestania3.Visible = true;
                estados.Pestania4.Visible = true;
                estados.Pestania5.Visible = true;
                estados.Pestania6.Visible = true;
                estados.Pestania7.Visible = true;

                //estados.Hora1.Visible = false;
                estados.Hora2.Visible = true; //20220419
                estados.Hora3.Visible = true; //20220419
                estados.Hora4.Visible = true; //20220419
                estados.Hora5.Visible = true; //20220419
                estados.Hora6.Visible = true; //20220419
                estados.Hora7.Visible = true; //20220419

                estados.Hora2.Text = $"{lLector["hora_ini_publi_v_sci"]} - {lLector["hora_fin_publi_v_sci"]}";
                estados.Hora3.Text = $"{lLector["hora_ini_modif_decla_sb"]} - {lLector["hora_fin_modif_decla_sb"]}";
                estados.Hora4.Text = $"{ lLector["hora_ini_negociacioni_sci"]} - {lLector["hora_fin_negociacioni_sci"]}";
                estados.Hora5.Text = $"{ lLector["hora_ini_rec_solicitud_c_sci"]} - {lLector["hora_fin_rec_solicitud_c_sci"]}";
                estados.Hora6.Text = $"{ lLector["hora_ini_publ_cnt_dispi_v_sci"]} - {lLector["hora_fin_publ_cnt_dispi_v_sci"]}";

                //Tiempo espera
                divTiempoEspera.Visible = false;
                estados.Time.Value = DateTime.Now.ToShortTimeString();

                switch (lLector["estado"].ToString())
                {
                    case "C":
                        estados.Time.Value = lLector["hora_ini_publi_v_sci"].ToString();
                        //estados.Pestania1.CssClass = "nav-link active";
                        break;
                    case "3":
                        estados.Pestania2.CssClass = "nav-link active";
                        estados.Time.Value = lLector["hora_fin_publi_v_sci"].ToString();
                        //estados.Hora2.Visible = true;
                        estados.Pestania2.Visible = true;
                        buttons.SwitchOnButton(EnumBotones.Excel);
                        break;
                    case "4":
                        estados.Pestania3.CssClass = "nav-link active";
                        estados.Time.Value = lLector["hora_fin_modif_decla_sb"].ToString();
                        //estados.Hora3.Visible = true;
                        estados.Pestania3.Visible = true;
                        break;
                    case "5":
                        estados.Pestania4.CssClass = "nav-link active";
                        estados.Time.Value = lLector["hora_fin_negociacioni_sci"].ToString();                        
                        //estados.Hora4.Visible = true;
                        estados.Pestania4.Visible = true;
                        break;
                    case "6":
                        estados.Pestania5.CssClass = "nav-link active";
                        estados.Time.Value = lLector["hora_fin_rec_solicitud_c_sci"].ToString();                        
                        //estados.Hora5.Visible = true;
                        estados.Pestania5.Visible = true;
                        break;
                    case "7":
                        estados.Pestania6.CssClass = "nav-link active";
                        estados.Time.Value = lLector["hora_fin_publ_cnt_dispi_v_sci"].ToString();
                        //estados.Hora6.Visible = true;
                        estados.Pestania6.Visible = true;
                        break;
                    case "F":
                        estados.Time.Value = "23:59:59";
                        estados.Pestania7.CssClass = "nav-link active";
                        break;
                    case "S":
                        estados.Time.Value = "23:59:59";
                        break;
                    default:
                        dtgSubasta.Columns.Clear();
                        dtgRechazo.Columns.Clear();
                        dtgContratos.Columns.Clear();
                        divTiempoEspera.Visible = true;
                        break;
                }
                EstadoSubasta = lLector["estado"].ToString();
                IndTipoRueda = lLector["ind_tipo_rueda"].ToString(); //20220211 ajsute label
            }
            else
            {
                dtgSubasta.Visible = false;
                dtgContratos.Visible = false;
                dtgRechazo.Visible = false;
                estados.Visible = false;
            }
               

            _lConexion.Cerrar();

            //20220211 ajsutre label
            //Se agrega el primer item para la ruta o tramo
            if (IndTipoRueda == null) return;
            ListItem lItem = IndTipoRueda == "1" || IndTipoRueda == "3"
                ? new ListItem { Value = "0", Text = "Seleccione Ruta" }
                : new ListItem { Value = "0", Text = "Seleccione Tramo" };
            ddlNueva.Items.Add(lItem);
            buttonsSubasta.EditarPunto = true;

            ////Se carga la subasta
            //buttonsSubasta.ListaRueda = ddlRueda.Items;
            //if (buttonsSubasta.ListaRueda.Cast<ListItem>().ToArray().Length >= 2)
            //{
            //    buttonsSubasta.ValorRueda = "1";
            //    buttonsSubasta.EditarRueda = true;
            //}

            //Se carga el punto o tramo
            _lConexion = new clConexion(_goInfo);
            _lConexion.Abrir();
            ddlNueva.Items.AddRange(LlenarControles(_lConexion.gObjConexion, "m_ruta_snt", "  estado <> 'I' order by descripcion", 0, 4).Cast<ListItem>().ToArray());
            buttonsSubasta.ListaPunto = ddlNueva.Items;
            if (buttonsSubasta.ListaPunto.Cast<ListItem>().ToArray().Length <= 1)
                buttonsSubasta.EditarPunto = false;
            buttonsSubasta.ValorPunto = "0";

            //Se establecen las columnas que se van a mostrar en la grilla de la subasta (Anteriormente se refrescaban los títulos )
            //if (!divTiempoEspera.Visible)   
            //    dtgSubasta.Columns[4].HeaderText = IndTipoRueda != null && IndTipoRueda == "P" ? "Ruta" : "Tramo";

            if (_goInfo.cod_comisionista == "0")
            {
                if (EstadoSubasta == "S")
                    buttonsSubastaExtender.SwitchOnButton(EnumBotonesSubasta.EnviarMensaje, EnumBotonesSubasta.MisMensajes, EnumBotonesSubasta.Contrato, EnumBotonesSubasta.Reactivar);
                else
                    buttonsSubastaExtender.SwitchOnButton(EnumBotonesSubasta.EnviarMensaje, EnumBotonesSubasta.MisMensajes, EnumBotonesSubasta.Contrato, EnumBotonesSubasta.Suspender);
            }
            else
            {
                buttonsSubasta.SwitchOnButton(EnumBotonesSubasta.Rueda, EnumBotonesSubasta.Punto);
                buttonsSubastaExtender.SwitchOnButton(EnumBotonesSubasta.MisMensajes, EnumBotonesSubasta.Contrato);
            }

            if (!divTiempoEspera.Visible) //20200924 ajsute compoenente
            {
                dtgSubasta.Columns[3].HeaderText = IndTipoRueda != null && (IndTipoRueda == "1" || IndTipoRueda == "3") ? "Ruta" : "Tramo";

                if (_goInfo.cod_comisionista == "0")
                {
                    dtgSubasta.Columns[9].Visible = false; //ofertar
                    dtgSubasta.Columns[10].Visible = false; //modificar
                    dtgSubasta.Columns[11].Visible = false;// eliminar
                }

                //dtgSubasta.Columns[12].Visible = EstadoSubasta != null && (EstadoSubasta == "5" || EstadoSubasta == "F");

                //Se actualizan las posturas
                CargarDatosGrilla();

            }
        }

        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lConn"></param>
        protected IEnumerable LlenarControles(SqlConnection lConn, string lsTabla, string lsCondicion, int liIndiceLlave, int liIndiceDescripcion)
        {
            var dummy = new DropDownList();
            // Proceso para Cargar el DDL de ciudad
            var lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                dummy.Items.Add(lItem1);
            }
            lLector.Close();
            return dummy.Items;
        }

        /// <summary>
        /// Nombre: cargarDatosGrilla
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para Cargar los datos en la Grio.
        /// Modificacion:
        /// </summary>
        private void CargarDatosGrilla(bool primeraCarga = false)
        {
            if (CodSubasta.Equals("0") && !primeraCarga || divTiempoEspera.Visible)
            {
                dtgSubasta.Columns.Clear();
                return;
            }

            //determina las columnas para el subastatodr o los operaores
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador", "@P_codigo_ruta" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };

            string[] primeraBusqueda = { "0", "0", "0" };
            string[] busquedasGenerales = { CodSubasta, _goInfo.cod_comisionista, CodRuta };
            string[] lValorParametros = primeraCarga ? primeraBusqueda : busquedasGenerales;

            //Se crea la conexión  
            _lConexion = new clConexion(_goInfo);
            _lConexion.Abrir();
            //Se realiza la consulta de la subasta
            if (EstadoSubasta != "6" && EstadoSubasta != "7")
            {
                dtgSubasta.Visible = true;
                dtgRechazo.Visible = false;
                dtgContratos.Visible = false;
                dtgSubasta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(_lConexion.gObjConexion, "pa_GetRuedaPuja11", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgSubasta.DataBind();
                if (EstadoSubasta == "F")
                {
                    dtgSubasta.Columns[9].Visible = false;
                    dtgSubasta.Columns[10].Visible = false;
                    dtgSubasta.Columns[11].Visible = false;
                    dtgSubasta.Columns[12].Visible = true;
                }
                else
                {
                    dtgSubasta.Columns[9].Visible = true;
                    dtgSubasta.Columns[10].Visible = true;
                    dtgSubasta.Columns[11].Visible = true;
                    dtgSubasta.Columns[12].Visible = false;
                }
            }
            if (EstadoSubasta == "6")
            {
                dtgSubasta.Visible = false;
                dtgRechazo.Visible = true;
                dtgContratos.Visible = false;
                dtgRechazo.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(_lConexion.gObjConexion, "pa_GetRuedaPuja11", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgRechazo.DataBind();
            }
            if (EstadoSubasta == "7")
            {
                dtgSubasta.Visible = false;
                dtgRechazo.Visible = false;
                dtgContratos.Visible = true;
                dtgContratos.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(_lConexion.gObjConexion, "pa_GetRuedaPuja11", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgContratos.DataBind();
            }
            _lConexion.Cerrar();

            //Se cargan los elementos de la grilla 
            //if (EstadoSubasta == "5" || EstadoSubasta == "F")
            //{
            //    dtgSubasta.Columns[12].Visible = true;
            //}
            //else
            //    dtgSubasta.Columns[12].Visible = false;


            if (_goInfo.cod_comisionista == "0")
            {
                dtgSubasta.Columns[9].Visible = false;
                dtgSubasta.Columns[10].Visible = false;
                dtgSubasta.Columns[11].Visible = false;
            }
            else
            {
                if (EstadoSubasta == "4")
                {
                    foreach (DataGridItem Grilla in dtgSubasta.Items)
                    {
                        if (Grilla.Cells[15].Text == "S")
                        {
                            Grilla.Cells[9].Controls[1].Visible = false;
                            Grilla.Cells[10].Controls[1].Visible = false;
                            Grilla.Cells[11].Controls[1].Visible = false;
                        }
                        else
                        {
                            if (Grilla.Cells[13].Text == "N")
                            {
                                Grilla.Cells[9].Controls[1].Visible = true;
                                Grilla.Cells[10].Controls[1].Visible = false;
                                Grilla.Cells[11].Controls[1].Visible = false;
                            }
                            else
                            {
                                Grilla.Cells[9].Controls[1].Visible = false;
                                Grilla.Cells[10].Controls[1].Visible = true;
                                Grilla.Cells[11].Controls[1].Visible = true;
                            }
                        }
                    }
                }
                else
                {
                    foreach (DataGridItem Grilla in dtgSubasta.Items)
                    {
                        Grilla.Cells[9].Controls[1].Visible = false;
                        Grilla.Cells[10].Controls[1].Visible = false;
                        Grilla.Cells[11].Controls[1].Visible = false;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void Timer_Tick(object sender, EventArgs e)
        {
            try
            {
                //Se actualiza el relog
                //lblRelog.Text = $"Hora: {DateTime.Now.ToString("HH:mm:ss").Substring(0, 8)}";

                var oArchivo = _strRutaArchivo + "subasta-" + CodSubasta + ".txt";
                if (Hora != File.GetCreationTime(oArchivo).ToString("HH:mm:ss"))
                {
                    Hora = File.GetCreationTime(oArchivo).ToString("HH:mm:ss");
                    MisOfertas = "S";
                    //Se recarga la rueda
                    CargarRueda();
                }
                else
                {
                    if (Refrescar == "S")
                    {
                        Refrescar = "N"; //20170929 rq048-17
                        //Se recargan las posturas
                        CargarDatosGrilla();
                    }
                }

                oArchivo = _strRutaArchivo + "Mensaje.txt";
                if (HoraMensaje != File.GetCreationTime(oArchivo).ToString("HH:mm:ss"))
                {
                    HoraMensaje = File.GetCreationTime(oArchivo).ToString("HH:mm:ss");
                }
                else
                {
                    if (MisOfertas == "S")
                        MisOfertas = "N";
                }
            }
            catch (Exception ex)
            {

                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPunto_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CodRuta = buttonsSubasta.ValorPunto;
                Refrescar = "S";
                Session["refresca_mis_posturas"] = "S";
                //Se actualizan las posturas
                CargarDatosGrilla();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// 20160128
        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            try
            {
                string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador", "@P_codigo_ruta" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { CodSubasta, _goInfo.cod_comisionista, CodRuta };
                _lConexion = new clConexion(_goInfo);
                _lConexion.Abrir();
                dtgSubastaImpresion.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(_lConexion.gObjConexion, "pa_GetRuedaPuja11", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgSubastaImpresion.DataBind();
                dtgSubastaImpresion.Visible = true;

                DataSet lds = new DataSet();
                SqlDataAdapter lsqldata = new SqlDataAdapter();
                string lsNombreArchivo = _goInfo.Usuario + "InfRueda" + DateTime.Now + ".xls";
                string lstitulo_informe = "Consulta Ids Subasta " + CodSubasta;
                decimal ldCapacidad = 0;
                StringBuilder lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                dtgSubastaImpresion.EnableViewState = false;
                lform.Controls.Add(dtgSubastaImpresion);
                lpagina.RenderControl(lhtw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + _goInfo.nombre + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());
                Response.End();
                lds.Dispose();
                lsqldata.Dispose();
                _lConexion.CerrarInforme();
                dtgSubastaImpresion.Visible = false;
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnItemCommand_Click(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                switch (((ImageButton)e.CommandSource).ID)
                {
                    // Muestra la oferta
                    case "imbOfertar":
                        btnOfertar.Text = "Crear";
                        OpenIngresoPosturaCompra(e.Item.Cells[0].Text, "C", e.Item.Cells[14].Text);
                        break;
                    //Muetra la modificacion
                    case "imbModificar":
                        btnOfertar.Text = "Modificar";
                        OpenIngresoPosturaCompra(e.Item.Cells[0].Text, "M", e.Item.Cells[14].Text);
                        break;
                    // Muestra la grafica
                    case "imbGrafica":
                        OpenCurvaOferDemAgre(e.Item.Cells[0].Text);
                        break;
                    case "imbEliminar":
                        {
                            string[] lsNombreParametros = { "@P_numero_id", "@P_codigo_operador" };
                            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                            string[] lValorParametros = { e.Item.Cells[0].Text, _goInfo.cod_comisionista };
                            SqlDataReader lLector;
                            _lConexion = new clConexion(_goInfo);
                            _lConexion.Abrir();
                            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(_lConexion.gObjConexion, "pa_DelPostura11", lsNombreParametros, lTipoparametros, lValorParametros);
                            if (lLector.HasRows)
                            {
                                while (lLector.Read())
                                {
                                    Toastr.Error(this, $"{lLector["error"]}");
                                }
                                lLector.Close();
                                lLector.Dispose();
                                _lConexion.Cerrar();
                            }
                            else
                            {
                                Toastr.Success(this, "Oferta eliminada correctamente");
                                lLector.Close();
                                lLector.Dispose();
                                _lConexion.Cerrar();
                                CargarDatosGrilla();
                            }

                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "Error al eliminar la oferta. " + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void dtgRechazo_OnItemCommandClick(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                switch (((ImageButton)e.CommandSource).ID)
                {
                    case "imbRechazar":
                        {
                            string[] lsNombreParametros = { "@P_numero_contrato", "@P_numero_id", "@P_codigo_operador" };
                            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                            string[] lValorParametros = { e.Item.Cells[1].Text, e.Item.Cells[0].Text, _goInfo.cod_comisionista };
                            SqlDataReader lLector;
                            _lConexion = new clConexion(_goInfo);
                            _lConexion.Abrir();
                            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(_lConexion.gObjConexion, "pa_DelContrato11", lsNombreParametros, lTipoparametros, lValorParametros);
                            if (lLector.HasRows)
                            {
                                while (lLector.Read())
                                {
                                    Toastr.Error(this, $"{lLector["error"]}");
                                }
                                lLector.Close();
                                lLector.Dispose();
                                _lConexion.Cerrar();
                            }
                            else
                            {
                                Toastr.Success(this, "Oferta eliminada correctamente");
                                lLector.Close();
                                lLector.Dispose();
                                _lConexion.Cerrar();
                                CargarDatosGrilla();
                            }

                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "Error al eliminar la oferta. " + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void dtgContratos_OnItemCommandClick(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                switch (((ImageButton)e.CommandSource).ID)
                {
                    case "imbGrafica":
                        {
                            OpenCurvaOferDemAgre(e.Item.Cells[0].Text);
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "Error al eliminar la oferta. " + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void SeleccionarNombrePestanias()
        {
            //Estado
            //estados.Pestania1.Text = "<b>1. Creada</b>";
            estados.Pestania2.Text = "<b>1. Publicación</b> de oferta";
            estados.Pestania3.Text = "<b>2. Declaración</b> posturas de compra";
            estados.Pestania4.Text = "<b>3. Desarrollo</b> de subasta";
            estados.Pestania5.Text = "<b>4. Rechazo</b> adjudicaciones";
            estados.Pestania6.Text = "<b>5. Publicación</b> resultados";
            estados.Pestania7.Text = "<b>6. Finalización</b> Subasta";

        }

        #region Modals

        #region Suspender

        /// <summary>
        /// Se encarga de inicializar el Modal para suspender una rueda 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Suspender_Click(object sender, EventArgs e)
        {
            SqlDataReader lLector = null;

            try
            {
                // Obtengo los Datos de la rueda
                _lConexion = new clConexion(_goInfo);
                _lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(_lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda", " numero_rueda= " + CodSubasta + " and estado not in ('F','Z')");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    // Lleno las etiquetas de la pantalla con los datos del Id.
                    lblFechaAct.Text = "Fecha Act: " + lLector["fecha_prox_apertura"];
                    lLector.Close();
                    lLector.Dispose();

                    Modal.Abrir(this, mdlSuspender.ID, mdlInsideSuspender.ID);
                }
                else
                {
                    //Se debe agregar la notificación
                    Toastr.Error(this, "La rueda ya está finalizada.!");
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
            finally
            {
                if (lLector != null)
                {
                    lLector.Dispose();
                    lLector.Close();
                    _lConexion.Cerrar();
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSuspender_Click(object sender, EventArgs e)
        {
            SqlDataReader lLector;

            try
            {
                string strRutaArchivoModal = ConfigurationManager.AppSettings["RutaArchivos"] + "mensaje.txt";
                string strRutaArchivoModal2 = ConfigurationManager.AppSettings["RutaArchivos"] + "subasta-" + CodSubasta + ".txt";

                string oError = "";
                string sMensaje = "";

                //if (TxtHora.Text == "")
                //    oError += "Debe digitar la Hora de inicio de la próxima ronda\\n";
                //if (TxtMinutos.Text == "")
                //    oError += "Debe la duración de la próxima ronda\\n";
                if (ddlDefinitivo.SelectedValue == "N")
                {
                    if (TxtFecha.Text == "")
                        oError += "Debe digitar la próxima fecha de apertura<br>";
                }
                else
                {
                    if (TxtFecha.Text != "")
                        oError += "NO Debe digitar la próxima fecha de apertura para suspensión definitiva<br>";
                }
                if (TxtObservacion.Text == "")
                    oError += "Debe digitar las observaciones de suspensión<br>";

                if (oError == "")
                {
                    string[] lsNombreParametros = { "@P_numero_rueda", "@P_fecha", "@P_ind_definitivo", "@P_observaciones" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
                    string[] lValorParametros = { CodSubasta, TxtFecha.Text, ddlDefinitivo.SelectedValue, TxtObservacion.Text };

                    _lConexion = new clConexion(_goInfo);
                    _lConexion.Abrir();
                    try
                    {
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(_lConexion.gObjConexion, "pa_setSuspender11", lsNombreParametros, lTipoparametros, lValorParametros, _goInfo);
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                                oError = lLector["error"] + "\\n";
                        }
                        if (oError == "")
                        {
                            if (ddlDefinitivo.SelectedValue == "N")
                            {
                                sMensaje = "La rueda se suspendió para reactivarse el día " + TxtFecha.Text;
                            }
                            else
                                sMensaje = "La rueda se suspendió definitivamente";
                            File.SetCreationTime(strRutaArchivoModal, DateTime.Now);
                            File.SetCreationTime(strRutaArchivoModal2, DateTime.Now);
                            Toastr.Success(this, sMensaje);
                            HoraMensaje = "";

                            CloseSuspender_Click(null, null);

                        }
                        _lConexion.Cerrar();
                    }
                    catch (Exception ex)
                    {
                        oError = "Error al suspender la rueda. " + ex.Message;
                        _lConexion.Cerrar();
                    }
                }
                if (oError != "")
                {
                    Toastr.Error(this, oError);
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseSuspender_Click(Object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Declaración de Información Archivo
                Modal.Cerrar(this, mdlSuspender.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Suspender

        #region Reactivar

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eactivar
        protected void OpenReactivar(object sender, EventArgs e)
        {
            try
            {

                /// Obtengo los Datos de la rueda
                _lConexion = new clConexion(_goInfo);
                _lConexion.Abrir();
                SqlDataReader lLector = DelegadaBase.Servicios.LlenarControl(_lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda rue", " rue.numero_rueda = " + CodSubasta);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    /// Lleno las etiquetas de la pantalla con los datos del Id.
                    hndFase.Value = lLector["fase_reactivacion"].ToString();
                    TxtHoraIniPub.Text = lLector["hora_ini_publi_v_sci"].ToString();
                    TxtHoraFinPub.Text = lLector["hora_fin_publi_v_sci"].ToString();
                    TxtHoraIniComp.Text = lLector["hora_ini_modif_decla_sb"].ToString();
                    TxtHoraFinComp.Text = lLector["hora_fin_modif_decla_sb"].ToString();
                    TxtHoraIniCalce.Text = lLector["hora_ini_negociacioni_sci"].ToString();
                    TxtHoraFinCalce.Text = lLector["hora_fin_negociacioni_sci"].ToString();
                    TxtHoraIniRechazo.Text = lLector["hora_ini_rec_solicitud_c_sci"].ToString();
                    TxtHoraFinRechazo.Text = lLector["hora_fin_rec_solicitud_c_sci"].ToString();
                    TxtHoraIniPubResult.Text = lLector["hora_ini_publ_cnt_dispi_v_sci"].ToString();
                    TxtHoraFinPubResult.Text = lLector["hora_fin_publ_cnt_dispi_v_sci"].ToString();
                    TxtHoraIniFinaliza.Text = lLector["hora_ini_publica_venta"].ToString();
                    TxtHoraFinFinaliza.Text = lLector["hora_ini_oferta_compra"].ToString();
                    lblEstado.Text = "Fase de Reactivación: ";

                    switch (hndFase.Value)
                    {
                        case "C":
                            lblEstado.Text += "Creada";
                            TxtHoraIniPub.Enabled = true;
                            TxtHoraFinPub.Enabled = true;
                            TxtHoraIniComp.Enabled = true;
                            TxtHoraFinComp.Enabled = true;
                            TxtHoraIniCalce.Enabled = true;
                            TxtHoraFinCalce.Enabled = true;
                            TxtHoraIniRechazo.Enabled = true;
                            TxtHoraFinRechazo.Enabled = true;
                            TxtHoraIniPubResult.Enabled = true;
                            TxtHoraFinPubResult.Enabled = true;
                            TxtHoraIniFinaliza.Enabled = true;
                            TxtHoraFinFinaliza.Enabled = true;
                            break;
                        case "3":
                            lblEstado.Text += "Publicación Oferta";
                            TxtHoraIniPub.Enabled = true;
                            TxtHoraFinPub.Enabled = true;
                            TxtHoraIniComp.Enabled = true;
                            TxtHoraFinComp.Enabled = true;
                            TxtHoraIniCalce.Enabled = true;
                            TxtHoraFinCalce.Enabled = true;
                            TxtHoraIniRechazo.Enabled = true;
                            TxtHoraFinRechazo.Enabled = true;
                            TxtHoraIniPubResult.Enabled = true;
                            TxtHoraFinPubResult.Enabled = true;
                            TxtHoraIniFinaliza.Enabled = true;
                            TxtHoraFinFinaliza.Enabled = true;
                            break;
                        case "4":
                            lblEstado.Text += "Declaraci&oacuten de ofertas de compra";
                            TxtHoraIniPub.Enabled = false;
                            TxtHoraFinPub.Enabled = false;
                            TxtHoraIniComp.Enabled = true;
                            TxtHoraFinComp.Enabled = true;
                            TxtHoraIniCalce.Enabled = true;
                            TxtHoraFinCalce.Enabled = true;
                            TxtHoraIniRechazo.Enabled = true;
                            TxtHoraFinRechazo.Enabled = true;
                            TxtHoraIniPubResult.Enabled = true;
                            TxtHoraFinPubResult.Enabled = true;
                            TxtHoraIniFinaliza.Enabled = true;
                            TxtHoraFinFinaliza.Enabled = true;
                            break;
                        case "5":
                            lblEstado.Text += "Desarrollo de la subasta";
                            TxtHoraIniPub.Enabled = false;
                            TxtHoraFinPub.Enabled = false;
                            TxtHoraIniComp.Enabled = false;
                            TxtHoraFinComp.Enabled = false;
                            TxtHoraIniCalce.Enabled = true;
                            TxtHoraFinCalce.Enabled = true;
                            TxtHoraIniRechazo.Enabled = true;
                            TxtHoraFinRechazo.Enabled = true;
                            TxtHoraIniPubResult.Enabled = true;
                            TxtHoraFinPubResult.Enabled = true;
                            TxtHoraIniFinaliza.Enabled = true;
                            TxtHoraFinFinaliza.Enabled = true;
                            break;
                        case "6":
                            lblEstado.Text += "Rechazo de adjudicaciones";
                            TxtHoraIniPub.Enabled = false;
                            TxtHoraFinPub.Enabled = false;
                            TxtHoraIniComp.Enabled = false;
                            TxtHoraFinComp.Enabled = false;
                            TxtHoraIniCalce.Enabled = false;
                            TxtHoraFinCalce.Enabled = false;
                            TxtHoraIniRechazo.Enabled = true;
                            TxtHoraFinRechazo.Enabled = true;
                            TxtHoraIniPubResult.Enabled = true;
                            TxtHoraFinPubResult.Enabled = true;
                            TxtHoraIniFinaliza.Enabled = true;
                            TxtHoraFinFinaliza.Enabled = true;
                            break;
                        case "7":
                            lblEstado.Text += "Publicaci&oacuten de resultados";
                            TxtHoraIniPub.Enabled = false;
                            TxtHoraFinPub.Enabled = false;
                            TxtHoraIniComp.Enabled = false;
                            TxtHoraFinComp.Enabled = false;
                            TxtHoraIniCalce.Enabled = false;
                            TxtHoraFinCalce.Enabled = false;
                            TxtHoraIniRechazo.Enabled = false;
                            TxtHoraFinRechazo.Enabled = false;
                            TxtHoraIniPubResult.Enabled = true;
                            TxtHoraFinPubResult.Enabled = true;
                            TxtHoraIniFinaliza.Enabled = true;
                            TxtHoraFinFinaliza.Enabled = true;
                            break;
                        case "8":
                            lblEstado.Text += "Finalizaci&oacuten de la subasta";
                            TxtHoraIniPub.Enabled = false;
                            TxtHoraFinPub.Enabled = false;
                            TxtHoraIniComp.Enabled = false;
                            TxtHoraFinComp.Enabled = false;
                            TxtHoraIniCalce.Enabled = false;
                            TxtHoraFinCalce.Enabled = false;
                            TxtHoraIniRechazo.Enabled = false;
                            TxtHoraFinRechazo.Enabled = false;
                            TxtHoraIniPubResult.Enabled = false;
                            TxtHoraFinPubResult.Enabled = false;
                            TxtHoraIniFinaliza.Enabled = true;
                            TxtHoraFinFinaliza.Enabled = true;
                            break;
                    }

                    lLector.Close();
                    lLector.Dispose();
                }
                else
                {
                    lblEstado.Text = "";
                    hndFase.Value = "C";
                }

                lLector.Dispose();
                lLector.Close();
                _lConexion.Cerrar();
                Modal.Abrir(this, mdlReactivar.ID, mdlInsideReactivar.ID);

            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReactivar_Click(object sender, EventArgs e)
        {
            try
            {
                var oError = "";
                var sMensaje = "";
                string strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"] + "mensaje.txt";
                string strRutaArchivo1 = ConfigurationManager.AppSettings["RutaArchivos"] + "subasta-" + CodSubasta + ".txt";

                if (TxtObservacionReac.Text == "")
                    oError += "Debe digitar las observaciones de la reactivaccion\\n";

                if (oError == "")
                {
                    string[] lsNombreParametros = { "@P_numero_rueda", "@P_observaciones", "@P_inicio_fase3", "@P_fin_fase3", "@P_inicio_fase4", "@P_fin_fase4", "@P_inicio_fase5", "@P_fin_fase5", "@P_inicio_fase6", "@P_fin_fase6", "@P_inicio_fase7", "@P_fin_fase7", "@P_inicio_fase8", "@P_fin_fase8" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
                    string[] lValorParametros = { CodSubasta, TxtObservacionReac.Text, TxtHoraIniPub.Text, TxtHoraFinPub.Text, TxtHoraIniComp.Text, TxtHoraFinComp.Text, TxtHoraIniCalce.Text, TxtHoraFinCalce.Text, TxtHoraIniRechazo.Text, TxtHoraFinRechazo.Text, TxtHoraIniPubResult.Text, TxtHoraFinPubResult.Text, TxtHoraIniFinaliza.Text, TxtHoraFinFinaliza.Text }; ///  

                    SqlDataReader lLector;
                    _lConexion = new clConexion(_goInfo);
                    _lConexion.Abrir();
                    try
                    {
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(_lConexion.gObjConexion, "pa_SetActRueda11", lsNombreParametros, lTipoparametros, lValorParametros, _goInfo);
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                                oError += lLector["error"] + "\\n";
                        }
                        if (oError == "")
                        {
                            File.SetCreationTime(strRutaArchivo, DateTime.Now);
                            File.SetCreationTime(strRutaArchivo1, DateTime.Now);
                            Toastr.Success(this, "La rueda se reactivó exitosamente");
                            Session["hora"] = "";
                            CloseReactivar_Click(null, null);
                        }
                        _lConexion.Cerrar();
                    }
                    catch (Exception ex)
                    {
                        Toastr.Error(this, "Error al reactivar la rueda. " + ex.Message);
                        _lConexion.Cerrar();
                    }
                }

                if (oError == "") return;
                Toastr.Error(this, oError);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseReactivar_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Reactivar
                Modal.Cerrar(this, mdlReactivar.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Reactivar

        #region Ingreso Posturas de Compra

        /// <summary>
        /// 
        /// </summary>
        protected void OpenIngresoPosturaCompra(string id, string accion, string maxPostCompra)
        {
            _lConexion = new clConexion(_goInfo);

            if (string.IsNullOrEmpty(id))
            {
                Toastr.Error(this, "No Se enviaron los Parametros requeridos.!");
                return;
            }

            hndID.Value = id;
            hndAccion.Value = accion;
            hdfMaxPostCompra.Value = maxPostCompra;

            /// Obtengo los Datos del ID Recibido
            _lConexion = new clConexion(_goInfo);
            _lConexion.Abrir();
            SqlDataReader lLector = DelegadaBase.Servicios.LlenarControl(_lConexion.gObjConexion, "pa_ValidarExistencia", "t_id_rueda idr, m_ruta_snt rut", " idr.numero_id = " + hndID.Value + " and idr.codigo_punto_entrega= rut.codigo_pozo_ini and idr.codigo_punto_fin= rut.codigo_pozo_fin"); //20171119 rq048-17
            if (!lLector.HasRows)
            {
                Toastr.Error(this, "El ID enviado NO existe en la base de Datos.!");
                _lConexion.Cerrar();
                return;
            }

            lLector.Read();
            /// Lleno las etiquetas de la pantalla con los datos del Id.
            lblRueda.Text = "No. Rueda: " + lLector["numero_rueda"];
            hdfNoRueda.Value = lLector["numero_rueda"].ToString();
            lblId.Text = "ID: " + id;
            lblFecha.Text = "Fecha Rueda: " + lLector["fecha_rueda"].ToString().Substring(0, 10);
            lblPuntoEntrega.Text =
                "Ruta: " + lLector["codigo_ruta"] + "-" + lLector["descripcion"]; //20171119 rq048-17
            //if (Session["ind_tipo_rueda"].ToString() == "P") //20171119 rq048-17
            //    lblPuntoEntrega.Text += "-" + lLector["desc_punto_fin"].ToString();  //20171119 rq048-17
            lblPeridoEnt.Text = "Periodo Entrega: " + lLector["codigo_periodo"] + "-" + lLector["desc_periodo"];
            lblModalidad.Text = "Modalidad Contractual: " + lLector["codigo_modalidad"] + "-" +
                                lLector["desc_modalidad"];
            /// Reviso si se esta creando la oferta o modificando
            hndCntTotal.Value = lLector["cantidad_total_venta"].ToString();
            lLector.Close();
            lLector.Dispose();

            lLector = DelegadaBase.Servicios.LlenarControl(_lConexion.gObjConexion, "pa_ValidarExistencia", "t_posturas_compra", " numero_id = " + hndID.Value + " and codigo_operador = " + _goInfo.cod_comisionista);
            if (lLector.HasRows)
            {
                lLector.Read();
                TxtCantidad.Text = lLector["cantidad_postura"].ToString();
                TxtPrecio.Text = lLector["precio"].ToString();
                TxtCantidadMin.Text = lLector["cantidad_minima"].ToString();
            }
            else
            {
                TxtCantidad.Text = "";
                TxtPrecio.Text = "";
                TxtCantidadMin.Text = "";
            }
            lLector.Close();
            lLector.Dispose();

            _lConexion.Cerrar();

            //Se abre el modal de Ingreso de Declaración de Información
            Modal.Abrir(this, mdlIngresoPosturaCompra.ID, mdlIngresoPosturaCompraInside.ID);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnOfertar_Click(object sender, EventArgs e)
        {
            string oError = "";
            string lsError = "";
            string lsNoPostura = "";

            string[] lsNombreParametros = { "@P_numero_id", "@P_numero_rueda", "@P_codigo_operador", "@P_precio_postura", "@P_cantidad_postura", "@P_cantidad_minima", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Float, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "0", "0", "0", "0", "0", "0" };

            lsError = ValidarEntradas();
            if (lsError == "")
            {
                _lConexion = new clConexion(_goInfo);
                _lConexion.Abrir();
                SqlTransaction oTransaccion;
                oTransaccion = _lConexion.gObjConexion.BeginTransaction(IsolationLevel.Serializable);
                try
                {
                    lValorParametros[0] = hndID.Value;
                    lValorParametros[1] = hdfNoRueda.Value;
                    lValorParametros[2] = _goInfo.cod_comisionista;
                    lValorParametros[3] = TxtPrecio.Text.Replace(",", "");
                    lValorParametros[4] = TxtCantidad.Text.Replace(",", "");
                    lValorParametros[5] = TxtCantidadMin.Text.Replace(",", "");
                    lValorParametros[6] = hndAccion.Value;

                    _goInfo.mensaje_error = "";

                    SqlDataReader lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(_lConexion.gObjConexion, "pa_SetPosturaCompraId11", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, _goInfo);
                    if (_goInfo.mensaje_error != "")
                    {
                        oError = "Error al Actualizar la Postura. " + _goInfo.mensaje_error + "\\n";
                        oTransaccion.Rollback();
                    }
                    else
                    {
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            if (lLector["mensaje"].ToString() != "")
                            {
                                oError = lLector["mensaje"].ToString();
                                lLector.Close();
                                lLector.Dispose();
                                oTransaccion.Rollback();
                                _lConexion.Cerrar();
                            }
                            else
                            {
                                lLector.Close();
                                lLector.Dispose();
                                oTransaccion.Commit();
                                _lConexion.Cerrar();
                                MisOfertas = "S";
                                Refrescar = "S";
                                Toastr.Success(this, hndAccion.Value == "C"
                                        ? "Posturas Ingresadas Correctamente.!"
                                        : "Posturas Actualizadas Correctamente.!");
                                //Se cierra el modal 
                                CloseIngresoPosturasCompra_Click(null, null);
                                //Se refresca la grilla  
                                CargarDatosGrilla();
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    oTransaccion.Rollback();
                    oError = "Error al realizar la oferta. " + ex.Message;
                    _lConexion.Cerrar();
                }
                if (oError != "")
                {
                    Toastr.Error(this, oError);
                }
            }
            else
                Toastr.Error(this, lsError);
        }

        /// <summary>
        /// Relaiza la validacion del Ingreso de las Posturas
        /// </summary>
        /// <returns></returns>
        protected string ValidarEntradas()
        {
            string lsError = "";
            int oValor = 0;
            double oValor1 = 0;
            int ldValorAnt = 0;
            int liCntVenta = Convert.ToInt32(hndCntTotal.Value);
            double ldPrecioAnt = 0;

            if (TxtCantidad.Text.Trim().Length > 0)
            {
                try
                {
                    // Validacion para que no permita cantidad  menor o Igual a 0 - 20150709
                    if (Convert.ToInt32(TxtCantidad.Text.Replace(",", "")) <= 0)
                        lsError += "La Cantidad de la Postura no puede ser menor o Igual a 0\\n";
                    else
                    {
                        if (Convert.ToInt32(TxtCantidad.Text.Replace(",", "")) > liCntVenta)
                            lsError += "La Cantidad de la Postura no puede ser mayor que la cantidad ofrecida\\n";
                    }
                }
                catch (Exception ex)
                {
                    lsError += "Valor Inválido en la Cantidad de la Postura \\n";
                }
            }
            else
                lsError += "Debe Ingresar la Cantidad de la Postura \\n";
            if (TxtPrecio.Text.Trim().Length > 0)
            {
                try
                {
                    if (Convert.ToDouble(TxtPrecio.Text.Replace(",", "")) < 0)
                        lsError += "El Precio debe ser mayor o igual que cero para la postura \\n";
                }
                catch (Exception ex)
                {
                    lsError += "Valor Inválido en el Precio de la Postura \\n";
                }
            }
            else
                lsError += "Debe Ingresar el Precio de la Postura \\n";
            if (TxtCantidadMin.Text.Trim().Length > 0)
            {
                try
                {
                    // Validacion para que no permita cantidad  menor o Igual a 0 - 20150709
                    if (Convert.ToInt32(TxtCantidadMin.Text.Replace(",", "")) < 0)
                        lsError += "La Cantidad mínima de la Postura no puede ser menor a 0\\n";
                    else
                    {
                        if (Convert.ToInt32(TxtCantidadMin.Text.Replace(",", "")) > Convert.ToInt32(TxtCantidad.Text.Replace(",", "")))
                            lsError += "La Cantidad mínima de la Postura no puede ser mayor que la cantidad demandada\\n";
                    }
                }
                catch (Exception ex)
                {
                    lsError += "Valor Inválido en la Cantidad mínima de la Postura \\n";
                }
            }
            else
                lsError += "Debe Ingresar la Cantidad mínima de la Postura \\n";
            return lsError;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseIngresoPosturasCompra_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Ingreso Posturas de Compra
                Modal.Cerrar(this, mdlIngresoPosturaCompra.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Ingreso Posturas de Compra

        #region Enviar Mensaje

        /// <summary>
        /// 
        /// </summary>
        protected void OpenEnviarMensaje(object sender, EventArgs e)
        {
            //Se abre el modal de Enviar Mensaje
            Modal.Abrir(this, mdlEnviarMensaje.ID, mdlEnviarMensajeInside.ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnMensaje_Click(object sender, EventArgs e)
        {
            var oError = "";
            var strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString() + "mensaje.txt";

            if (TxtMensaje.Text == "")
                oError += "Debe digitar el mensaje a enviar\\n";

            if (oError == "")
            {
                string[] lsNombreParametros = { "@P_numero_rueda", "@P_mensaje" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
                string[] lValorParametros = { CodSubasta, TxtMensaje.Text, };

                _lConexion = new clConexion(_goInfo);
                _lConexion.Abrir();
                try
                {
                    DelegadaBase.Servicios.EjecutarProcedimiento(_lConexion.gObjConexion, "pa_setMensaje", lsNombreParametros, lTipoparametros, lValorParametros, _goInfo);
                    Toastr.Success(this, "Mensaje enviado Correctamente");
                    File.SetCreationTime(strRutaArchivo, DateTime.Now);
                    //Se cierra el modal 
                    CloseIngresoPosturasCompra_Click(null, null);
                    //Se refresca la grilla  
                    //CargarDatosGrilla();

                    _lConexion.Cerrar();
                }
                catch (Exception ex)
                {
                    oError = "Error al enviar el mensaje. " + ex.Message;
                    _lConexion.Cerrar();
                }
            }
            if (oError != "")
            {
                Toastr.Error(this, oError);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseEnviarMensaje_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Enviar Mensaje
                Modal.Cerrar(this, mdlEnviarMensaje.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Enviar Mensaje

        #region Mensaje

        /// <summary>
        /// 
        /// </summary>
        protected void AbrirMensajes(object sender, EventArgs e)
        {
            try
            {
                string[] lsNombreParametros = { "@P_numero_rueda" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int };
                string[] lValorParametros = { CodSubasta.ToString() };
                SqlDataReader lLector;
                _lConexion = new clConexion(_goInfo);
                _lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(_lConexion.gObjConexion, "pa_GetMensajes", lsNombreParametros, lTipoparametros, lValorParametros);

                StringBuilder builder = new StringBuilder();

                builder.Append("<table  class=\"table table-bordered table-fixed\">");
                builder.Append("<thead class=\"thead-light\">");
                builder.Append("<tr>");
                builder.Append("<th width='10%'>IDG</HORA></th>");
                builder.Append("<th width='90%'>MENSAJE</font></th>");
                builder.Append("</tr>");
                builder.Append("</thead>");

                if (lLector.HasRows)
                {
                    builder.Append("<tbody>");
                    while (lLector.Read())
                    {
                        builder.Append("<tr>");
                        builder.Append("<td width='10%'>" + lLector["hora_mensaje"] + "</font></td>");
                        builder.Append("<td width='90%'>" + lLector["mensaje"] + "</font></td>");
                        builder.Append("</tr>");
                    }
                    builder.Append("</tbody>");
                }

                builder.Append("</table>");

                ltTableroRF.Text = builder.ToString();

                lLector.Close();
                lLector.Dispose();
                _lConexion.Cerrar();

                /// Creacion del archivo del tablero de RF 
                /// 2011-10-03
                ///
                if (MisOfertas.ToString() == "S")
                    MisOfertas = "N";

                //Se abre el modal de oferta
                Modal.Abrir(this, mdlMensaje.ID, mdlMensajeInside.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseMensajes_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Oferta
                Modal.Cerrar(this, mdlMensaje.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Mensaje

        #region Contratos

        /// <summary>
        /// 
        /// </summary>
        protected void AbrirContratos(object sender, EventArgs e)
        {
            try
            {
                var builder = new StringBuilder();

                if (EstadoSubasta.Equals("F"))
                {
                    try
                    {
                        string[] lsNombreParametros = { "@P_numero_rueda" };
                        SqlDbType[] lTipoparametros = { SqlDbType.Int };
                        string[] lValorParametros = { CodSubasta };
                        SqlDataReader lLector;
                        _lConexion = new clConexion(_goInfo);
                        _lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(_lConexion.gObjConexion, "pa_GetMisContratos11", lsNombreParametros, lTipoparametros, lValorParametros);

                        builder.Append("<table  class=\"table table-bordered\">");
                        builder.Append("<thead class=\"thead-light\">");
                        builder.Append("<tr>");
                        builder.Append("<th width='10%'>IDG</font></th>");
                        builder.Append("<th width='15%'>CONTRATO</font></th>");
                        builder.Append("<th width='25%'>COMPRADOR</font></th>");
                        builder.Append("<th width='25%'>VENDEDOR</font></th>");
                        builder.Append("<th width='12%'>CANTIDAD</font></th>");
                        builder.Append("<th width='13%'>PRECIO</font></th>");
                        builder.Append("<th width='13%'>RUTA</font></th>");
                        builder.Append("<th width='13%'>FECHA INICIAL</font></th>");
                        builder.Append("<th width='13%'>FECHA FINAL</font></th>");
                        builder.Append("</tr>");
                        builder.Append("</thead>");

                        if (lLector.HasRows)
                        {
                            builder.Append("<tbody>");
                            while (lLector.Read())
                            {
                                builder.Append("<tr>");
                                builder.Append("<td width='10%'>" + lLector["numero_id"] + "</font></td>");
                                builder.Append("<td width='15%'>" + lLector["numero_contrato"] + "</font></td>");
                                builder.Append("<td width='25%'>" + lLector["nombre_compra"] + "</font></td>");
                                builder.Append("<td width='25%'>" + lLector["nombre_venta"] + "</font></td>");
                                builder.Append("<td width='12%'>" + lLector["cantidad"] + "</font></td>");
                                builder.Append("<td width='13%'>" + lLector["precio"] + "</font></td>");
                                builder.Append("<td width='13%'>" + lLector["desc_ruta"] + "</font></td>");
                                builder.Append("<td width='13%'>" + lLector["fecha_inicial"] + "</font></td>");
                                builder.Append("<td width='13%'>" + lLector["fecha_final"] + "</font></td>");
                                builder.Append("</tr>");
                            }
                            builder.Append("</tbody>");
                        }
                        builder.Append("</table>");
                        ltTablero.Text = builder.ToString();

                        lLector.Close();
                        lLector.Dispose();
                        _lConexion.Cerrar();
                    }
                    catch (Exception ex)
                    {
                        _lConexion.Cerrar();
                        Toastr.Error(this, ex.Message);
                    }
                }
                else
                {
                    try
                    {

                        string[] lsNombreParametros = { "@P_numero_rueda" };
                        SqlDbType[] lTipoparametros = { SqlDbType.Int };
                        string[] lValorParametros = { CodSubasta };
                        SqlDataReader lLector;
                        _lConexion = new clConexion(_goInfo);
                        _lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(_lConexion.gObjConexion, "pa_GetMisOFertas11", lsNombreParametros, lTipoparametros, lValorParametros);

                        builder.Append("<table  class=\"table table-bordered\">");
                        builder.Append("<thead class=\"thead-light\">");
                        builder.Append("<tr>");
                        builder.Append("<th width='10%'>IDG</font></th>");
                        builder.Append("<th width='10%'>PUNTA</font></th>");
                        builder.Append("<th width='50%'>OPERADOR</font></th>");
                        builder.Append("<th width='15%'>CANTIDAD</font></th>");
                        builder.Append("<th width='15%'>PRECIO</font></th>");
                        builder.Append("</tr>");
                        builder.Append("</thead>");

                        if (lLector.HasRows)
                        {
                            builder.Append("<tbody>");
                            while (lLector.Read())
                            {
                                builder.Append("<tr>");
                                builder.Append("<td width='10%'>" + lLector["numero_id"] + "</font></td>");
                                builder.Append("<td width='10%'>" + lLector["punta"] + "</font></td>");
                                builder.Append("<td width='50%'>" + lLector["nombre_operador"] + "</font></td>");
                                builder.Append("<td width='15%'>" + lLector["cantidad_postura"] + "</font></td>");
                                builder.Append("<td width='15%'>" + lLector["precio"] + "</font></td>");
                                builder.Append("</tr>");
                            }
                            builder.Append("</tbody>");
                        }
                        builder.Append("</table>");
                        ltTablero.Text = builder.ToString();

                        lLector.Close();
                        lLector.Dispose();
                        _lConexion.Cerrar();
                    }
                    catch (Exception ex)
                    {
                        _lConexion.Cerrar();
                        Toastr.Error(this, ex.Message);
                    }
                }
                //Se abre el modal de oferta
                Modal.Abrir(this, mdlContratos.ID, mdlContratosInside.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseContratos_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Oferta
                Modal.Cerrar(this, mdlContratos.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Contratos

        #region Curva de Oferta y Demanda Agregada

        /// <summary>
        /// Abre el modal de la Curva de Oferta y Demanda Agregada
        /// </summary>
        private void OpenCurvaOferDemAgre(string lblNoId)
        {
            var transporte = true;
            DataSet lds = new DataSet();
            _goInfo.Programa = "Curva de oferta y demanda agregada";
            lblMensaje.Text = "";

            _lConexion = new clConexion(_goInfo);
            _lConexion.Abrir();
            SqlCommand lComando1 = new SqlCommand();
            SqlDataAdapter lsqldata1 = new SqlDataAdapter();
            lComando1.Connection = _lConexion.gObjConexion;
            lComando1.CommandTimeout = 3600;
            lComando1.CommandType = CommandType.StoredProcedure;

            //cambios grafica 20160202
            lComando1.CommandText = "pa_ValidarExistencia";
            lComando1.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = "t_id_rueda";
            lComando1.Parameters.Add("@P_Filtro", SqlDbType.VarChar).Value = "numero_id = " + lblNoId;
            SqlDataReader lLector = lComando1.ExecuteReader();
            if (lLector.HasRows)
            {
                lLector.Read();
                if (lLector["codigo_producto"].ToString() == "1")
                {
                    lblPuntoCurva.Text = "Punto de Entrega: " + lLector["desc_punto_entrega"];
                    transporte = false;
                }
                else
                {
                    lblPuntoCurva.Text = "Ruta: " + lLector["desc_punto_entrega"] + "-" + lLector["desc_punto_fin"];
                }
            }
            else
                lblPuntoCurva.Text = "";
            lComando1.Parameters.Clear();
            lLector.Close();
            lLector.Dispose();

            lComando1.CommandText = "pa_GetPreciosCurva1";
            lComando1.Parameters.Add("@P_numero_id", SqlDbType.Int).Value = lblNoId;
            lComando1.ExecuteNonQuery();
            lsqldata1.SelectCommand = lComando1;
            lsqldata1.Fill(lds);
            dtgInformacion.DataSource = lds;
            dtgInformacion.DataBind();
            _lConexion.Cerrar();

            //Carga la grafica
            AuctionGraphics.GenerateGraph(this, dtgInformacion, lblNoId, transporte);
            //Se abre el modal de Ingreso de Declaración de Información
            Modal.Abrir(this, mdlCurvaOfertaDemAgre.ID, mdlCurvaOfertaDemAgreInside.ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseCurvaOferDemAgre_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Ingreso de Declaración de Información
                Modal.Cerrar(this, mdlCurvaOfertaDemAgre.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Curva de Oferta y Demanda Agregada

        #endregion Modals

    }
}