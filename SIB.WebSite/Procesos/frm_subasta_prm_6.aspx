﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_subasta_prm_6.aspx.cs" Inherits="Procesos_frm_subasta_prm_6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    <table border="0" align="center" cellpadding="0" cellspacing="0" runat="server" id="Table1"
        width="100%">
        <tr>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Rueda: </font>
            </td>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Fase: </font>
            </td>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Hora Fase:
                </font>
            </td>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Participante:
                
                
                
                </font>
            </td>
            <td align="center" class="td4">
            <asp:Button ID="btnSuspender" Text="Suspender" runat="server" Visible="false" OnClientClick="window.open('frm_Suspender_2.aspx','','width=450,height=250,left=400,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no,scrollbars=1');" />
                <%--<asp:Button ID="btnCarga" Text="Carga Venta" runat="server" Visible="false" OnClientClick="window.open('frm_cargaVenta_6.aspx','','width=450,height=250,left=400,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no,scrollbars=1');" />--%>
            </td>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Pto Ent: </font>
            </td>
            <td align="center" class="td4">
                <asp:Button ID="btnBuscar" Text="Buscar" runat="server" OnClick="btnBuscar_Click" />
            </td>
        </tr>
        <tr>
            <td align="center" class="td4">
                <font color='#F5DA81' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Label ID="lblSubasta" runat="server"></asp:Label>
                </font>
            </td>
            <td align="center" class="td4">
                <font color='#F5DA81' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Label ID="lblFase" runat="server"></asp:Label>
                </font>
            </td>
            <td align="center" class="td4">
                <font color='#F5DA81' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Label ID="lblHora" runat="server"></asp:Label>
                </font>
            </td>
            <td align="center" class="td4">
                <font color='#F5DA81' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Label ID="lblAgente" runat="server"></asp:Label>
                </font>
            </td>
            <td align="left" class="td4">
                
                <asp:Button ID="btnReactivar" Text="Reactivar" runat="server" Visible="false" OnClientClick="window.open('frm_reactivar_6.aspx','','width=600,height=400,left=300,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no,scrollbars=1');" />
            </td>
            <td align="left" class="td4">
                <asp:DropDownList ID="ddlPunto" runat="server">
                </asp:DropDownList>
            </td>
            <td align="left" class="td4">
                <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Height="30" Visible="false" />
                <%--20160128--%>
                <asp:DataGrid ID="dtgSubasta" runat="server" AutoGenerateColumns="False" runat="server"
                    Visible="false">
                    <ItemStyle CssClass="td2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="15px">
                    </ItemStyle>
                    <Columns>
                        <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" ItemStyle-HorizontalAlign="Left">
                        </asp:BoundColumn>
                        <%--1--%><asp:BoundColumn DataField="desc_modalidad" HeaderText="Tipo Contrato" ItemStyle-HorizontalAlign="Left">
                        </asp:BoundColumn>
                        <%--2--%><asp:BoundColumn DataField="desc_periodo" HeaderText="Periodo de Entrega"
                            ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <%--3--%><asp:BoundColumn DataField="desc_punto_entrega" HeaderText="Punto de Entrega"
                            ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--4--%><asp:BoundColumn DataField="cantidad_venta" HeaderText="Cantidad" DataFormatString="{0:###,###,###,###,###,###,##0 }"
                            ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                        <%--3--%><asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left">
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="HndFechaRueda" runat="server" />
 </asp:Content>
