﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

//using System.Windows.Forms;

public partial class Procesos_frm_posturas : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    //String strRutaArchivo;
    private System.TimeSpan diffResult;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            if (!IsPostBack)
            {
                try
                {
                    hndCodSubasta.Value = Session["numero_rueda"].ToString();
                    if (hndCodSubasta.Value == "")
                        hndCodSubasta.Value = "0";
                    lConexion1 = new clConexion(goInfo);
                    //hndCodSubasta.Value = "21";
                    HndFechaRueda.Value = DateTime.Now.ToShortDateString();
                    HndFechaRueda.Value = HndFechaRueda.Value.Substring(6, 4) + "/" + HndFechaRueda.Value.Substring(3, 2) + "/" + HndFechaRueda.Value.Substring(0, 2);
                    cargarDatosGrilla();
                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, ex.Message);
                }
            }
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, ex.Message, "Session Expiro. " + ex.Message, 50000);
        }
    }
    /// <summary>
    /// Nombre: cargarDatosGrilla
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para Cargar los datos en la Grio.
    /// Modificacion:
    /// </summary>
    private void cargarDatosGrilla()
    {
        if (hndCodSubasta.Value == "0")
        {
            dtgSubasta1.Columns.Clear();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.ofertas.location= 'frm_ofertas1.aspx';", true);
        }
        else
        {
            try
            {
                if (Session["destino_rueda"].ToString() == "G")
                {
                    dtgSubasta1.Columns[4].HeaderText = "Sitio Entrega";
                }
                else
                {
                    dtgSubasta1.Columns[4].HeaderText = "Ruta";
                }
                string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_punto" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { hndCodSubasta.Value, Session["codigo_punto"].ToString() };
                lConexion.Abrir();
                if (Session["estado_rueda"].ToString() == "1" || Session["estado_rueda"].ToString() == "2")
                {
                    dtgDeclara.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaPuja", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgDeclara.DataBind();
                    dtgDeclara.Visible = true;
                    dtgSubasta1.Visible = false;
                }
                else
                {
                    dtgSubasta1.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaPuja", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgSubasta1.DataBind();
                    dtgDeclara.Visible = false;
                    dtgSubasta1.Visible = true;
                }
                if (Session["estado_rueda"].ToString() == "5" || Session["estado_rueda"].ToString() == "F")
                    dtgSubasta1.Columns[19].Visible = true;
                else
                    dtgSubasta1.Columns[19].Visible = false;

                lConexion.Cerrar();
                ImageButton imbControl;
                if (Session["estado_rueda"].ToString() == "1" || Session["estado_rueda"].ToString() == "2")
                {
                    foreach (DataGridItem Grilla in this.dtgDeclara.Items)
                    {
                        imbControl = (ImageButton)Grilla.Cells[9].Controls[1];
                        if (Grilla.Cells[10].Text == goInfo.cod_comisionista)
                            imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_ActDeclaracionInf.aspx?codigo_dec=" + Grilla.Cells[11].Text + "','Actualizar','width=450,height=250,left=350,top=150,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                        else
                            imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_ActDeclaracionPre.aspx?codigo_dec=" + Grilla.Cells[11].Text + "','Actualizar','width=450,height=250,left=350,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                        if (Grilla.Cells[10].Text != goInfo.cod_comisionista)
                        {
                            Grilla.Cells[12].Controls[1].Visible = false;
                        }
                    }
                }

                if (Session["estado_rueda"].ToString() == "4")
                {
                    dtgSubasta1.Columns[18].Visible = false;
                    dtgSubasta1.Columns[10].Visible = true;
                    dtgSubasta1.Columns[11].Visible = true;
                    dtgSubasta1.Columns[12].Visible = true;
                    foreach (DataGridItem Grilla in this.dtgSubasta1.Items)
                    {
                        if (Grilla.Cells[14].Text == "N")
                        {
                            Grilla.Cells[10].Controls[1].Visible = true;
                            Grilla.Cells[11].Controls[1].Visible = false;
                            Grilla.Cells[12].Controls[1].Visible = false;
                            imbControl = (ImageButton)Grilla.Cells[10].Controls[1];
                            imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_IngresoOfertaCompra.aspx?ID=" + Grilla.Cells[0].Text + "&accion=C&maxPostCompra=" + Grilla.Cells[16].Text + "','Ofertar','width=600,height=450,left=350,top=150,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                        }
                        else
                        {
                            Grilla.Cells[10].Controls[1].Visible = false;
                            Grilla.Cells[11].Controls[1].Visible = true;
                            Grilla.Cells[12].Controls[1].Visible = true;
                            imbControl = (ImageButton)Grilla.Cells[11].Controls[1];
                            imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_IngresoOfertaCompra.aspx?ID=" + Grilla.Cells[0].Text + "&accion=M&maxPostCompra=" + Grilla.Cells[16].Text + "','Ofertar','width=600,height=450,left=350,top=150,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                        }
                    }
                }
                if (Session["estado_rueda"].ToString() != "1" && Session["estado_rueda"].ToString() != "2" && Session["estado_rueda"].ToString() != "4")
                {
                    if (Session["estado_rueda"].ToString() == "3")
                        dtgSubasta1.Columns[18].Visible = false;
                    else
                        dtgSubasta1.Columns[18].Visible = true;
                    dtgSubasta1.Columns[10].Visible = false;
                    dtgSubasta1.Columns[11].Visible = false;
                    dtgSubasta1.Columns[12].Visible = false;

                    if (Session["estado_rueda"].ToString() == "5" || Session["estado_rueda"].ToString() == "F")
                    {
                        foreach (DataGridItem Grilla in this.dtgSubasta1.Items)
                        {
                            imbControl = (ImageButton)Grilla.Cells[19].Controls[1];
                            imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_CurvaAgregada.aspx?ID=" + Grilla.Cells[0].Text + "','Grafica','width=900,height=500,left=250,top=90,status=no,location=0,menubar=no,toolbar=no,resizable=yes,scrollbars=yes');");
                        }
                    }
                }
                if (Session["tipoPerfil"].ToString() == "B")
                {
                    dtgSubasta1.Columns[10].Visible = false;
                    dtgSubasta1.Columns[11].Visible = false;
                    dtgSubasta1.Columns[12].Visible = false;
                    dtgDeclara.Columns[9].Visible = false;
                    dtgDeclara.Columns[12].Visible = false;
                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, ex.Message);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.ofertas.location= 'frm_ofertas1.aspx';", true);
        }
    }

    public void OnItemCommand_Click(object sender, DataGridCommandEventArgs e)
    {
        string oError = "";
        string oBoton = ((System.Web.UI.WebControls.ImageButton)e.CommandSource).ID.ToString();

        if (oBoton == "imbEliminar")
        {
            try
            {
                string[] lsNombreParametros = { "@P_numero_rueda", "@P_numero_id", "@P_estado" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Char };
                string[] lValorParametros = { e.Item.Cells[15].Text, e.Item.Cells[0].Text, e.Item.Cells[13].Text };
                SqlDataReader lLector;
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_DelPostura", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                    {
                        oError += lLector["error"].ToString() + "\\n";
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                }
                else
                {
                    oError = "Oferta eliminada correctamente";
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    cargarDatosGrilla();
                }
            }
            catch (Exception ex)
            {
                oError = "Error al eliminar la oferta. " + ex.Message;
            }
            Toastr.Warning(this, oError);

        }
    }
    public void OnItemCommand1_Click(object sender, DataGridCommandEventArgs e)
    {
        string oError = "";
        string oBoton = ((System.Web.UI.WebControls.ImageButton)e.CommandSource).ID.ToString();

        if (oBoton == "imbEliminar")
        {
            try
            {
                string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_declaracion_inf", "@P_codigo_operador" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { Session["numero_rueda"].ToString(), e.Item.Cells[11].Text, goInfo.cod_comisionista  };
                SqlDataReader lLector;
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_DelDeclaracionInf", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                    {
                        oError += lLector["error"].ToString() + "\\n";
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                }
                else
                {
                    oError = "Declaraión de infromación eliminada correctamente";
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    cargarDatosGrilla();
                }
            }
            catch (Exception ex)
            {
                oError = "Error al eliminar la declaración de información. " + ex.Message;
            }
            Toastr.Warning(this, oError);

        }
    }
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgActividadEconomica.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgActividadEconomica.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
}

