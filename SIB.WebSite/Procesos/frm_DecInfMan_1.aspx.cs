﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_DecInfMan_1 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    String strRutaArchivo;
    String strRutaArchivo1;
    SqlDataReader lLector;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            hndDestino.Value = Session["destino_rueda"].ToString();
            lblRueda.Text = "No. Rueda: " + Session["numero_rueda"].ToString();
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlTitular, "m_operador", " estado ='A' and codigo_operador !=0 order by codigo_operador ", 0, 4);
            if (hndDestino.Value == "T")
            {
                lblDestino.Text = "Destino: Transporte";
                lblPunto.Text = "Ruta: ";
                LlenarControles1(lConexion.gObjConexion, ddlPunto, "m_pozo poz, m_ruta_snt rut", "distinct poz.codigo_pozo, poz.descripcion", " rut.codigo_trasportador=" + goInfo.cod_comisionista + " and poz.codigo_pozo = rut.codigo_pozo_ini and poz.estado ='A' and rut.estado ='A' order by descripcion ", 0, 1);
            }
            else
            {
                lblDestino.Text = "Destino: Suministro de Gas";
                lblPunto.Text = "Punto de Entrega: ";
                LlenarControles(lConexion.gObjConexion, ddlPunto, "m_pozo", " estado ='A' order by descripcion ", 0, 1);
                ddlPuntoFin.Visible = false; 
            }
            lConexion.Cerrar();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOfertar_Click(object sender, EventArgs e)
    {
        string oMensaje = "";
        if (ddlTitular.SelectedValue == "0")
            oMensaje += "Debe seleccionar el titular \\n";
        if (hndDestino.Value == "T")
        {
            if (ddlPunto.SelectedValue == "0")
                oMensaje += "Debe seleccionar el punto inicial de la ruta\\n";
            if (ddlPuntoFin.SelectedValue == "0" || ddlPuntoFin.SelectedValue == "")
                oMensaje += "Debe seleccionar el punto final de la ruta\\n";
        }
        else
        {
            if (ddlPunto.SelectedValue == "0")
                oMensaje += "Debe seleccionar el punto de entrega\\n";
        }
        if (TxtCntDec.Text == "")
            oMensaje += "Debe digitar la cantidad declarada\\n";
        else
        {
            try
            {
                Convert.ToInt32(TxtCntDec.Text.Replace(",", ""));
            }
            catch (Exception ex)
            {
                oMensaje += "Cantidad declarada no válida\\n";
            }
        }
        if (oMensaje == "")
        {
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_declarador", "@P_codigo_titular", "@P_punto","@P_punto_fin", "@P_cantidad_declarada" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { Session["numero_rueda"].ToString(), goInfo.cod_comisionista, ddlTitular.SelectedValue, ddlPunto.SelectedValue, "0",TxtCntDec.Text.Replace(",", "") };
            if (hndDestino.Value == "T")
                lValorParametros[4] = ddlPuntoFin.SelectedValue;
            lConexion.Abrir();
            try
            {
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetDeclaracionInf", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    oMensaje = "Error al Realizar la declaracion de información. " + goInfo.mensaje_error.ToString() + "\\n";
                }
                else
                {
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                            oMensaje = lLector["mensaje"].ToString() + "\\n";
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        Session["mis_ofertas"] = "S";
                        Toastr.Success(this, "Declaración de Información ingresada correctamente.!");
                        
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                oMensaje = "Error al registrar la declaración de información. " + ex.Message;
                lConexion.Cerrar();
            }
            if (oMensaje != "")
            {
                Toastr.Warning(this, oMensaje);
 
            }
        }
        else
            Toastr.Warning(this, oMensaje);


    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            if (lsTabla != "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }

    protected void ddlPunto_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (hndDestino.Value == "T")
        {
            ddlPuntoFin.Items.Clear();
            lConexion.Abrir();
            LlenarControles1(lConexion.gObjConexion, ddlPuntoFin, "m_pozo poz, m_ruta_snt rut", "distinct poz.codigo_pozo, poz.descripcion", " rut.codigo_trasportador=" + goInfo.cod_comisionista + " and rut.codigo_pozo_ini = "+ddlPunto.SelectedValue +" and poz.codigo_pozo = rut.codigo_pozo_fin and poz.estado ='A' and rut.estado ='A' order by descripcion ", 0, 1);
            lConexion.Cerrar();
        }
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCampos, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlCommand lComando = new SqlCommand();
        lComando.Connection = lConn;
        lComando.CommandTimeout = 3600;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_ValidarExistencia1";
        lComando.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = lsTabla;
        lComando.Parameters.Add("@P_campos", SqlDbType.VarChar).Value = lsCampos;
        lComando.Parameters.Add("@P_filtro", SqlDbType.VarChar).Value = lsCondicion;
        SqlDataReader lLector;
        lLector = lComando.ExecuteReader();

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

}
