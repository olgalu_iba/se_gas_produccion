﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using SIB.BASE;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_DeclaraInteresCompraSMP : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    SqlDataReader lLector;
    string sRutaArc = ConfigurationManager.AppSettings["rutaCarga"].ToString();

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            try
            {
                lsTitulo = "";
                // Carga informacion de combos
                //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
                if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
                {
                    lsIndica = this.Request.QueryString["lsIndica"].ToString();
                }
                if (this.Request.QueryString["fase"] != null && this.Request.QueryString["fase"].ToString() != "")
                {
                    Session["fase"] = this.Request.QueryString["fase"].ToString();
                }
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", "estado = 'A'  Order by razon_social", 0, 4);
                LlenarControles(lConexion.gObjConexion, ddlBusOperador, "m_operador", "estado = 'A'  Order by razon_social", 0, 4);
                LlenarControles(lConexion.gObjConexion, ddlSubasta, "t_rueda", "codigo_tipo_subasta = 1 and estado = 'C' ", 1, 17);
                LlenarControles(lConexion.gObjConexion, ddlBusSubasta, "t_rueda", "codigo_tipo_subasta = 1 and estado = 'C' ", 1, 17);
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_fases_subasta", " codigo_fases_subasta = " + Session["fase"].ToString() + " ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsTitulo += lLector["descripcion"].ToString() + " Subasta Suministro sin Interupciones";
                    hdfDescFase.Value = lLector["descripcion"].ToString();
                }
                lLector.Close();
                lLector.Dispose();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    hdfCantMin.Value = lLector["cantidad_minima_negociacion"].ToString();
                    hdfCantMax.Value = lLector["valor_vig_ptdv_cidv_min_minas"].ToString();
                }
                lLector.Close();
                lLector.Dispose();


                lConexion.Cerrar();
                if (Session["tipoPerfil"].ToString() == "N")
                {
                    ddlOperador.SelectedValue = goInfo.cod_comisionista;
                    ddlBusOperador.SelectedValue = goInfo.cod_comisionista;
                    ddlOperador.Enabled = false;
                    ddlBusOperador.Enabled = false;
                }
                hndFecha.Value = DateTime.Now.ToShortDateString().Substring(6, 4) + "-" + DateTime.Now.ToShortDateString().Substring(3, 2) + "-" + DateTime.Now.ToShortDateString().Substring(0, 2);
                if (lsIndica == null || lsIndica == "" || lsIndica == "L")
                {
                    Listar();
                }
                else if (lsIndica == "N")
                {
                    Nuevo();
                }
                else if (lsIndica == "B")
                {
                    Buscar();
                }
            }
            catch (Exception ex)
            {

                Toastr.Warning(this, "Problemas en la Carga de la Pagina. " + ex.Message.ToString());
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "t_comprador_rueda_hab");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgConsulta.Columns[9].Visible = (Boolean)permisos["UPDATE"];
        dtgConsulta.Columns[10].Visible = (Boolean)permisos["DELETE"];
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 22 de 2014
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        lConexion.Abrir();
        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador ope, m_tipos_operador tpo, m_operador_subasta opeS", " ope.codigo_operador =" + goInfo.cod_comisionista + " and ope.tipo_operador = tpo.sigla and tpo.codigo_tipo_operador = opeS.codigo_tipo_operador and opeS.punta = 'C' And opeS.codigo_tipo_subasta = 1 ");
        if (!lLector.HasRows)
        {
            Toastr.Warning(this, "El tipo de operador no está autorizado para " + lsTitulo + ". <br>");
            tblCaptura.Visible = false;
            tblgrilla.Visible = true;
            tblBuscar.Visible = false;
            imbCrear.Visible = false;
            imbActualiza.Visible = false;

        }
        else
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            imbCrear.Visible = true;
            imbActualiza.Visible = false;
            lblTitulo.Text = "Crear " + lsTitulo;
            ddlSubasta.Enabled = true;
            ddlModalidad.Enabled = true;
            ddlEstado.SelectedValue = "C";
            ddlEstado.Enabled = false;
        }
        lLector.Close();
        lLector.Dispose();
        lConexion.Cerrar();
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    ddlModalidad.Enabled = false;
                    ddlSubasta.Enabled = false;
                    ddlOperador.Enabled = false;
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_comprador_rueda_hab", " codigo_comprador_hab = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        try
                        {
                            ddlOperador.SelectedValue = lLector["codigo_operador"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El oeprador del registro no existe o esta inactivo<br>";
                        }
                        try
                        {
                            ddlSubasta.SelectedValue = lLector["numero_rueda"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "La rueda del registro no existe o esta inactivo<br>";
                        }
                        try
                        {
                            ddlModalidad.SelectedValue = lLector["modalidad_contrato"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "La madalidad de contrato del registro no existe o esta inactivo<br>";
                        }
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        TxtCantidad.Text = lLector["maxima_demanda"].ToString();
                        imbCrear.Visible = false;
                        if (ddlEstado.SelectedValue == "A")
                            imbActualiza.Visible = false;
                        else
                        {
                            imbActualiza.Visible = true;
                            if (ddlEstado.SelectedValue == "R" && Session["tipoPerfil"].ToString() == "N")
                                ddlEstado.SelectedValue = "S";
                        }

                    }
                    lLector.Close();
                    lLector.Dispose();
                    string[] lsNombreParametros = { "@P_codigo_tipo_subasta", "@P_codigo_fase", "@P_numero_rueda", "@P_codigo_operador" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                    String[] lValorParametros = { "1", Session["fase"].ToString(), ddlSubasta.SelectedValue, ddlOperador.SelectedValue };
                    dtgReqHab.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetReqHabSubCompra", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgReqHab.DataBind();
                    lConexion.Cerrar();
                    if (Session["tipoPerfil"].ToString() == "B")
                    {
                        ddlEstado.Enabled = true;
                        TxtCantidad.Enabled = false;
                        dtgReqHab.Columns[3].Visible = false;
                        dtgReqHab.Columns[5].Visible = false;
                    }
                    else
                    {
                        ddlEstado.Enabled = false;
                        TxtCantidad.Enabled = true;
                    }
                    foreach (DataGridItem Grilla in this.dtgReqHab.Items)
                    {
                        CheckBox Checkbox = (CheckBox)Grilla.Cells[2].Controls[1];
                        if (Grilla.Cells[4].Text == "N")
                            Checkbox.Checked = false;
                        else
                            Checkbox.Checked = true;
                    }
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    Toastr.Warning(this, "No se Puede editar el Registro por que esta Bloqueado. Codigo Comprador-Hab " + modificar.ToString());
                  

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_operador", "@P_numero_rueda", "@P_estado" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Char };
        string[] lValorParametros = { "0", "0", "" };

        try
        {
            if (ddlBusOperador.SelectedValue != "0")
                lValorParametros[0] = ddlBusOperador.SelectedValue;
            if (ddlBusSubasta.SelectedValue != "" && ddlBusSubasta.SelectedValue != "0")
                lValorParametros[1] = ddlBusSubasta.SelectedValue;
            if (ddlBusEstado.SelectedValue != "")
                lValorParametros[2] = ddlBusEstado.SelectedValue;

            lConexion.Abrir();
            dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetCompRueHab", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgConsulta.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, ex.Message);

        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgConsulta.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        DateTime ldFecha = DateTime.Now;

        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            try
            {
                if (Session["tipoPerfil"].ToString() == "N")
                {
                    if (this.dtgConsulta.Items[e.Item.ItemIndex].Cells[7].Text == "R")
                    {
                        if (ldFecha <= Convert.ToDateTime(this.dtgConsulta.Items[e.Item.ItemIndex].Cells[11].Text))
                        {
                            hndCodigo.Value = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text;
                            Modificar(hndCodigo.Value);
                        }
                        else
                        {
                            Toastr.Warning(this, " Ya se venció el plazo para modificar " + hdfDescFase.Value + " de la rueda seleccionada<br>");
                            
                        }
                    }
                    else
                    {
                        hndCodigo.Value = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text;
                        Modificar(hndCodigo.Value);
                    }
                }
                else
                {
                    hndCodigo.Value = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text;
                    Modificar(hndCodigo.Value);
                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Proboemas en la Recuperación del Registro.! " + ex.Message.ToString());
            }
        }
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            if (!VerificarExistencia("t_comprador_rueda_hab", " codigo_comporador_hab= " + this.dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text + " and estado ='C'"))
            {
                Toastr.Warning(this, "No se puede eliminar el registro porque ya fue aprobado<br>");
            }
            if (lblMensaje.Text == "")
            {
                string[] lsNombreParametros = { "@P_codigo_operador_rueda" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int };
                String[] lValorParametros = { this.dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text };
                try
                {
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_DelOpeRueHab", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        Toastr.Warning(this, "Se presento un Problema en la Eliminación del  registro del operador.!");
                        lConexion.Cerrar();
                    }
                    else
                    {
                        DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_DelOpeRueHabReq", lsNombreParametros, lTipoparametros, lValorParametros);
                        Listar();
                    }
                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, "Se presento un Problema en la Eliminación del  registro del operador.!");
                }
            }
        }

    }
    /// <summary>
    /// Nombre: dtgReqHab_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos Visualizacion de Imagen de la Grilla de Requisitos
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgReqHab_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Ver")
        {
            try
            {
                string lsRuta = "../archivos/" + this.dtgReqHab.Items[e.Item.ItemIndex].Cells[5].Text.Replace(@"\", "/");
                if (this.dtgReqHab.Items[e.Item.ItemIndex].Cells[5].Text.Trim().Length > 0)
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + lsRuta + "');", true);
                else
                    Toastr.Warning(this, "No han Realizado la Carga del Archivo.!");

            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Problemas al Recuperae el Archivo.! " + ex.Message.ToString());
            }
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lstable, string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lstable, lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='t_comprador_rueda_hab' and llave_registro='codigo_comprador_hab=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_comprador_hab=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "t_comprador_rueda_hab";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_comprador_rueda_hab", lsCondicion1);
        }
        return true;
    }
    ///// <summary>
    ///// Nombre: ImgExcel_Click
    ///// Fecha: Agosto 15 de 2008
    ///// Creador: German Eduardo Guarnizo
    ///// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    ///// Modificacion:
    ///// </summary>
    ///// <param name="sender"></param>
    ///// <param name="e"></param>
    //protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    //{
    //    string[] lValorParametros = { "0", "" };
    //    string lsParametros = "";

    //    try
    //    {
    //        Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetUsrRueHab&nombreParametros=@P_codigo_periodo*@P_descripcion&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "&columnas=codigo_rueda_usuario*numero_rueda*desc_rueda*codigo_operador*nombre_operador*codigo_usuario*login*estado&titulo_informe=Listado de Usuarios habilitados por reuda&TituloParametros=");
    //    }
    //    catch (Exception ex)
    //    {
    //        lblMensaje.Text = "No se Pude Generar el Informe.!";
    //    }

    //}
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlSubasta_SelectedIndexChanged(object sender, EventArgs e)
    {
        dtgReqHab.Dispose();
        if (ddlSubasta.SelectedValue != "0" && ddlOperador.SelectedValue != "0")
        {
            string[] lsNombreParametros = { "@P_codigo_tipo_subasta", "@P_codigo_fase", "@P_numero_rueda", "@P_codigo_operador" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            String[] lValorParametros = { "1", Session["fase"].ToString(), ddlSubasta.SelectedValue, ddlOperador.SelectedValue };

            lConexion.Abrir();
            dtgReqHab.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetReqHabSubCompra", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgReqHab.DataBind();
            lConexion.Cerrar();
            foreach (DataGridItem Grilla in this.dtgReqHab.Items)
            {
                CheckBox Checkbox = (CheckBox)Grilla.Cells[2].Controls[1];
                if (Grilla.Cells[4].Text == "N")
                    Checkbox.Checked = false;
                else
                    Checkbox.Checked = true;
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlOperador_SelectedIndexChanged(object sender, EventArgs e)
    {
        dtgReqHab.Dispose();
        if (ddlSubasta.SelectedValue != "0" && ddlOperador.SelectedValue != "0")
        {
            string[] lsNombreParametros = { "@P_codigo_tipo_subasta", "@P_codigo_fase", "@P_numero_rueda", "@P_codigo_operador" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            String[] lValorParametros = { "1", Session["fase"].ToString(), ddlSubasta.SelectedValue, ddlOperador.SelectedValue };

            lConexion.Abrir();
            dtgReqHab.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetReqHabSubCompra", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgReqHab.DataBind();
            lConexion.Cerrar();
            foreach (DataGridItem Grilla in this.dtgReqHab.Items)
            {
                CheckBox Checkbox = (CheckBox)Grilla.Cells[2].Controls[1];
                if (Grilla.Cells[4].Text == "N")
                    Checkbox.Checked = false;
                else
                    Checkbox.Checked = true;
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click1(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_operador", "@P_numero_rueda", "@P_codigo_modalidad", "@P_estado", "@P_cantidad_maxima" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Char, SqlDbType.Decimal };
        String[] lValorParametros = { "", "", "", "", "0" };
        string[] lsNombreParametros1 = { "@P_codigo_operador", "@P_numero_rueda", "@P_cod_req_hab_ope", "@P_archivo", "@P_estado", "@P_codigo_req_hab_comp" };
        SqlDbType[] lTipoparametros1 = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Char, SqlDbType.Int };
        String[] lValorParametros1 = { "", "", "", "", "", "0" };
        string sRuta = sRutaArc + hndFecha.Value + "\\" + "operador_" + ddlOperador.SelectedValue + "\\";
        int liCantidad = 0;
        bool Error = false;
        try
        {
            ///  Validacion de la Cantidad
            try
            {
                liCantidad = Convert.ToInt32(TxtCantidad.Text.Replace(",", ""));
                if (liCantidad < Convert.ToInt32(hdfCantMin.Value))
                {
                    Toastr.Warning(this, "La Cantidad de Demanda Máxima es Inferior al Valor { " + hdfCantMin.Value + " } mínimo de negociación. <br>");
                    Error = true;
                }
                if (liCantidad > Convert.ToInt32(hdfCantMax.Value))
                {
                    Toastr.Warning(this, "La Cantidad de Demanda Máxima es mayor que el valor vigente de PTDV o CIDV {" + hdfCantMax.Value + " } <br>");
                    Error = true;
                   
                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Valor Invalido en la Cantidad de Demanda Maxima.!<br>");
                Error = true;

            }
            if (!VerificarExistencia("t_rueda rue, t_usuario_hab usrH", " rue.numero_rueda= " + ddlSubasta.SelectedValue + " and rue.codigo_tipo_rueda = usrH.codigo_tipo_rueda And usrH.codigo_usuario = " + goInfo.codigo_usuario + " And usrH.estado = 'A'  And usrH.fecha_inicial <='" + hndFecha.Value + "' and usrH.fecha_final >= '" + hndFecha.Value + "'"))
            {
                Toastr.Warning(this, " El usuario no Ha sido Aprobado para el tipo de rueda.<br>");
                Error = true;

            }
            if (VerificarExistencia("t_operador_rueda_hab", " numero_rueda= " + ddlSubasta.SelectedValue + " and codigo_operador=" + ddlOperador.SelectedValue))
            {
                Toastr.Warning(this, " El operador ya se asoció  a la rueda como Vendedor, por lo que no se puede declarar como Comprador.<br>");
                Error = true;             
            }
            if (ddlOperador.SelectedValue == "0")
            {
          
                    Toastr.Warning(this, "Debe seleccionar el operador<br>");
                    Error = true;
            
            }
            if (ddlSubasta.SelectedValue == "0")
            {
                Toastr.Warning(this, "Debe seleccionar la subasta<br>");
                Error = true;
            }
            if (VerificarExistencia("t_comprador_rueda_hab", " numero_rueda= " + ddlSubasta.SelectedValue + " and codigo_operador=" + ddlOperador.SelectedValue))
            {
                Toastr.Warning(this, " El operador ya se asoció  a la rueda y fase seleccionada<br>");
                Error = true;                
            }
            if (!VerificarExistencia("t_rueda", " numero_rueda= " + ddlSubasta.SelectedValue + " and fecha_max_ofe_c_smpsi>='" + hndFecha.Value + "'"))
            {
                Toastr.Warning(this, " Ya se venció el plazo para " + hdfDescFase.Value + " de la rueda seleccionada<br>");
                Error = true;
            }

            foreach (DataGridItem Grilla in this.dtgReqHab.Items)
            {
                FileUpload Filebox = (FileUpload)Grilla.Cells[3].Controls[1];
                if (Filebox.FileName != "")
                    if (File.Exists(sRuta + Filebox.FileName))
                    {
                        Toastr.Warning(this, "El archivo: " + Filebox.FileName + ", ya se encuentra grabado en el sistema.<br>");
                        Error = true;
                        
                    }
            }

            if (!Error)
            {
                SqlTransaction oTransaccion;
                lConexion.Abrir();
                oTransaccion = lConexion.gObjConexion.BeginTransaction(System.Data.IsolationLevel.Serializable);

                lValorParametros[0] = ddlOperador.SelectedValue;
                lValorParametros[1] = ddlSubasta.SelectedValue;
                lValorParametros[2] = ddlModalidad.SelectedValue;
                lValorParametros[3] = ddlEstado.SelectedValue;
                lValorParametros[4] = TxtCantidad.Text.Trim().Replace(",", "");

                lValorParametros1[0] = ddlOperador.SelectedValue;
                lValorParametros1[1] = ddlSubasta.SelectedValue;
                if (!DelegadaBase.Servicios.EjecutarProcedimientoConTransaccion(lConexion.gObjConexion, "pa_SetCompRueHab", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo))
                {
                    Toastr.Warning(this, "Se presentó un Problema en el Ingreso de los Requisitos.! " + goInfo.mensaje_error.ToString());
                
                    oTransaccion.Rollback();
                    lConexion.Cerrar();
                }
                else
                {
                    foreach (DataGridItem Grilla in this.dtgReqHab.Items)
                    {
                        lValorParametros1[2] = Grilla.Cells[0].Text;
                        FileUpload Filebox = (FileUpload)Grilla.Cells[3].Controls[1];
                        if (Filebox.FileName != "")
                        {
                            lValorParametros1[3] = hndFecha.Value + "\\" + "operador_" + ddlOperador.SelectedValue + "\\" + Filebox.FileName;
                            try
                            {
                                System.IO.Directory.CreateDirectory(sRuta);
                            }
                            catch (Exception ex)
                            {
                                Toastr.Warning(this, "Problemas en la Carga del Archivo. " + ex.Message.ToString());
                                Error = true;
                                oTransaccion.Rollback();
                                lConexion.Cerrar();
                                break;
                            }
                            Filebox.SaveAs(sRuta + Filebox.FileName);
                        }
                        else
                            lValorParametros1[3] = "";

                        CheckBox Checkbox = (CheckBox)Grilla.Cells[2].Controls[1];
                        if (Checkbox.Checked == true)
                            lValorParametros1[4] = "S";
                        else
                            lValorParametros1[4] = "N";
                        lValorParametros1[5] = Grilla.Cells[7].Text;
                        if (!DelegadaBase.Servicios.EjecutarProcedimientoConTransaccion(lConexion.gObjConexion, "pa_SetCompRueHabReq", lsNombreParametros1, lTipoparametros1, lValorParametros1, oTransaccion))
                        {
                            Error = true;
                            Toastr.Warning(this, "Se presentó un Problema en el Ingreso de los Requisitos.!");
                            oTransaccion.Rollback();
                            lConexion.Cerrar();
                        }
                    }
                    if (!Error)
                    {
                        oTransaccion.Commit();
                        lConexion.Cerrar();
                        TxtCantidad.Text = "";
                        Toastr.Success(this, "Registro Ingresado Correctamente.!");
                       
                    }
                    else
                        Toastr.Warning(this, lblMensaje.Text);
                   
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, ex.Message);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click1(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_comprador_hab", "@P_estado", "@P_tipo_perfil", "@P_cantidad_maxima" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char, SqlDbType.Char, SqlDbType.Decimal };
        String[] lValorParametros = { "", "", Session["tipoPerfil"].ToString(), "0" };
        string[] lsNombreParametros1 = { "@P_codigo_operador", "@P_numero_rueda", "@P_cod_req_hab_ope", "@P_archivo", "@P_estado", "@P_codigo_req_hab_comp" };
        SqlDbType[] lTipoparametros1 = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Char, SqlDbType.Int };
        String[] lValorParametros1 = { "", "", "", "", "", "0" };
        string sRuta = sRutaArc + hndFecha.Value + "\\" + "operador_" + ddlOperador.SelectedValue + "\\";

        lblMensaje.Text = "";
        string lsEstado = "1";
        int liCantidad = 0;
        if (Session["tipoPerfil"].ToString() == "N")
        {
            foreach (DataGridItem Grilla in this.dtgReqHab.Items)
            {
                FileUpload Filebox = (FileUpload)Grilla.Cells[3].Controls[1];
                if (Filebox.FileName != "")
                    if (File.Exists(sRuta + Filebox.FileName))
                        lblMensaje.Text += "El archivo: " + Filebox.FileName + ", ya se encuentra grabado en el sistema.<br>";
            }
        }
        ///  Validacion de la Cantidad
        try
        {
            liCantidad = Convert.ToInt32(TxtCantidad.Text.Replace(",", ""));
            if (liCantidad < Convert.ToInt32(hdfCantMin.Value))
            {
                lblMensaje.Text += "La Cantidad de Demanda Máxima es Inferior al Valor { " + hdfCantMin.Value + " } mínimo de negociación. <br>";
            }
            if (liCantidad > Convert.ToInt32(hdfCantMax.Value))
            {
                lblMensaje.Text += "La Cantidad de Demanda Máxima es Mayor que el valor vigente de PTDV o CIDV {" + hdfCantMax.Value + " }<br>";
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text += "Valor Invalido en la Cantidad de Demanda Maxima.!<br>";
        }
        if (Session["tipoPerfil"].ToString() == "N")
        {
            if (!VerificarExistencia("t_rueda rue, t_usuario_hab usrH", " rue.numero_rueda= " + ddlSubasta.SelectedValue + " and rue.codigo_tipo_rueda = usrH.codigo_tipo_rueda  And usrH.codigo_usuario = " + goInfo.codigo_usuario + " And usrH.estado = 'A'  And usrH.fecha_inicial <='" + hndFecha.Value + "' and usrH.fecha_final >= '" + hndFecha.Value + "'"))
                lblMensaje.Text += " El Usuario no Ha sido Aprobado para el tipo de rueda.<br>";

            if (!VerificarExistencia("t_rueda", " numero_rueda= " + ddlSubasta.SelectedValue + " and fecha_max_ofe_c_smpsi>='" + hndFecha.Value + "'"))
                lblMensaje.Text += " Ya se venció el plazo para " + hdfDescFase.Value + " de la rueda seleccionada<br>";
        }
        if (lblMensaje.Text == "")
        {
            SqlTransaction oTransaccion;
            lConexion.Abrir();
            oTransaccion = lConexion.gObjConexion.BeginTransaction(System.Data.IsolationLevel.Serializable);

            try
            {
                lValorParametros[0] = hndCodigo.Value;
                lValorParametros[1] = ddlEstado.SelectedValue;
                lValorParametros[3] = TxtCantidad.Text.Trim().Replace(",", "");
                if (!DelegadaBase.Servicios.EjecutarProcedimientoConTransaccion(lConexion.gObjConexion, "pa_UptCompRueHab", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion))
                {
                    lblMensaje.Text = "Se presento un Problema en la Actualizacion del  registro del operador.!";
                    lsEstado = "0";
                    oTransaccion.Rollback();
                    lConexion.Cerrar();
                }
                else
                {
                    lValorParametros1[0] = ddlOperador.SelectedValue;
                    lValorParametros1[1] = ddlSubasta.SelectedValue;
                    foreach (DataGridItem Grilla in this.dtgReqHab.Items)
                    {
                        CheckBox Checkbox = (CheckBox)Grilla.Cells[2].Controls[1];
                        lValorParametros1[2] = Grilla.Cells[0].Text;
                        if (Session["tipoPerfil"].ToString() == "N")
                        {

                            FileUpload Filebox = (FileUpload)Grilla.Cells[3].Controls[1];
                            if (Filebox.FileName != "")
                            {
                                lValorParametros1[3] = hndFecha.Value + "\\" + "operador_" + ddlOperador.SelectedValue + "\\" + Filebox.FileName;
                                try
                                {
                                    System.IO.Directory.CreateDirectory(sRuta);
                                }
                                catch (Exception ex)
                                {
                                    lblMensaje.Text = "Problemas en la Carga del Archivo. " + ex.Message.ToString();
                                    oTransaccion.Rollback();
                                    lConexion.Cerrar();
                                    break;
                                }
                                Filebox.SaveAs(sRuta + Filebox.FileName);
                            }
                            else
                                lValorParametros1[3] = Grilla.Cells[5].Text.Trim();
                        }
                        else
                            lValorParametros1[3] = Grilla.Cells[5].Text.Trim();
                        if (Checkbox.Checked == true)
                            lValorParametros1[4] = "S";
                        else
                            lValorParametros1[4] = "N";
                        lValorParametros1[5] = Grilla.Cells[7].Text;
                        if (!DelegadaBase.Servicios.EjecutarProcedimientoConTransaccion(lConexion.gObjConexion, "pa_SetCompRueHabReq", lsNombreParametros1, lTipoparametros1, lValorParametros1, oTransaccion))
                        {
                            lblMensaje.Text = "Se presento un Problema en la Actualizacion del  registro del operador.!";
                            lsEstado = "0";
                            oTransaccion.Rollback();
                            lConexion.Cerrar();
                        }
                    }
                    if (lblMensaje.Text == "")
                    {
                        TxtCantidad.Text = "";
                        oTransaccion.Commit();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Actualizada Correctamente.!" + "');", true);
                        string lsAsunto = "";
                        string lsMensaje = "";
                        string lsEstadoMail = "";
                        string lsMail = "";
                        string lsNomOperador = "";
                        /// Obtengo el mail del Operador
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + ddlOperador.SelectedValue + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lsMail = lLector["e_mail"].ToString();
                            if (lLector["tipo_persona"].ToString() == "J")
                                lsNomOperador = lLector["razon_social"].ToString();
                            else
                                lsNomOperador = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();

                        if (Session["tipoPerfil"].ToString() == "B")
                        {
                            ///// Envio del Mail de la Aprobacion o Rechazo de los Requisitos
                            if (ddlEstado.SelectedValue == "A")
                                lsEstadoMail = "la Aprobacion";
                            else
                                lsEstadoMail = "el Rechazo";
                            lsMensaje = "Señores: " + lsNomOperador + " \n\n";
                            lsAsunto = "Notificación Aprobacion / Rechazo Declaración Interes de Compradores";
                            lsMensaje += "Nos permitimos  informarle que el Administrador del Sistema de Gas acaba de realizar " + lsEstadoMail + " de la documentación Adjuntada para Declaracion Interes de Compradores. Para la subasta " + ddlSubasta.SelectedItem.ToString() + "  \n\n\n";
                            lsMensaje += "Coordinalmente, \n\n\n";
                            lsMensaje += "Administrador Sistema de Gas \n";
                            clEmail mail = new clEmail(lsMail, lsAsunto, lsMensaje, "");
                            lblMensaje.Text = mail.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                        }
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + lblMensaje.Text + "');", true);
                    manejo_bloqueo("E", hndCodigo.Value);
                    Listar();
                }
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                manejo_bloqueo("E", hndCodigo.Value);
                Toastr.Warning(this, ex.Message);
  
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click1(object sender, EventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (hndCodigo.Value != "")
            manejo_bloqueo("E", hndCodigo.Value);
        lblMensaje.Text = "";
        Listar();
    }
}