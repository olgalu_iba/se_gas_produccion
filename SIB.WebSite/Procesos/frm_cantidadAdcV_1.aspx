﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_cantidadAdcV_1.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_cantidadAdcV_1" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" onload="window.focus()">


    <script language="javascript">
        //        document.location = no;
        function confirmar() {
            return confirm("Esta seguro que desea aumentar su oferta?!")
        }
        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }

        function formatoMiles(obj, evt) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /([0-9]*)$/i.exec(strValue)) != null) {
                obj.value = addCommas(T[1]);
            } else {
                alert('formato invalido ');
            }
        }
        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }

    </script>
        <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTituo" Text="ADICION DE DEMANDA" runat="server" />

                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>
         
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                             <asp:Label ID="lblId" runat="server"></asp:Label>
                           
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                                                  <asp:Label ID="lblTipo" runat="server"></asp:Label>
                            
                           
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                          <asp:Label ID="lblDuracion" runat="server"></asp:Label>
                        
			  </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                         <asp:Label ID="lblLugar" runat="server"></asp:Label>
                            

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                                                  <asp:Label ID="lblPrecioRes" runat="server"></asp:Label>
                         
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                           <asp:Label ID="lblCantidadV" runat="server"></asp:Label>
                           
                        </div>
                    </div>
                        <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                           <asp:Label ID="Label1" runat="server">Cantidad Adicional:</asp:Label>
                           <asp:TextBox ID="TxtCantidad" runat="server" Width="100px" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
                               <asp:Button ID="btnCambiar" Text="Actualizar" runat="server" OnClientClick="return confirmar();"
                            OnClick="btnCambiar_Click" />
                        <asp:HiddenField ID="hndID" runat="server" />
                        <asp:HiddenField ID="hndRonda" runat="server" />
                        <asp:HiddenField ID="HndCntV" runat="server" />
                            
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
             
                        </div>
                    </div>
                </div>
                   
        
          
       
       
        </div>
    </div>

    
 </asp:Content>