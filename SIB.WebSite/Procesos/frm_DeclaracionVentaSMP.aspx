﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_DeclaracionVentaSMP.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_DeclaracionVentaSMP" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">/>

    <script type="text/javascript" language="javascript">

        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }

        function formatoMiles(obj, evt) {
            var T;
            var strValue = removeCommas(obj.value);
            //if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
            if ((T = /([0-9]*)$/i.exec(strValue)) != null) {
                //                T[4] = T[4].substr(0, decimales);
                //                if (T[2] == '' && T[3] == '.') T[2] = 0;
                obj.value = addCommas(T[1]);
            } else {
                alert('formato invalido ');
            }
        }
        function formatoMilesPre(obj, evt, decimales) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
                T[4] = T[4].substr(0, decimales);
                if (T[2] == '' && T[3] == '.') {
                    T[2] = 0;
                }
                obj.value = T[1] + addCommas(T[2]) + T[3] + T[4];
            }
            else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }

    </script>

    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" bgcolor="#85BF46">
                <table border="0" cellspacing="2" cellpadding="2" align="right">
                    <tr>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_DeclaracionVentaSMP.aspx?lsIndica=L">Listar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_DeclaracionVentaSMP.aspx?lsIndica=B">Consultar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/frm_contenido.aspx">Salir</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="90%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Operador
            </td>
            <td class="td2">
                <asp:Label ID="lblOperador" runat="server"></asp:Label>
                <asp:HiddenField ID="hdfNoRueda" runat="server" />
                <asp:HiddenField ID="hdfTipoRueda" runat="server" />
                <asp:HiddenField ID="hdfCodSubasta" runat="server" />
                <asp:HiddenField ID="hdfCodModalidad" runat="server" />
                <asp:HiddenField ID="hdfNoId" runat="server" />
                <asp:HiddenField ID="hdfNoPostura" runat="server" />
                <asp:HiddenField ID="hdfDescFase" runat="server" />
                <asp:HiddenField ID="hndFecha" runat="server" />
                <asp:HiddenField ID="hdfValorVigMin" runat="server" />
                <asp:HiddenField ID="hdfCantMin" runat="server" />
                <asp:HiddenField ID="hdfIncrementos" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="td1">
                Rueda
            </td>
            <td class="td2">
                <asp:Label ID="lblSubasta" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Modalidad Contrato
            </td>
            <td class="td2">
                <asp:Label ID="lblModalidad" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Cantidad
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCantidad" runat="server" Width="150px" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Precio de Reserva
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtPrecioReserva" runat="server" Width="150px" onkeyup="return formatoMilesPre(this,event,2);"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:Button ID="imbCrear" runat="server" Text="Crear" OnClientClick="this.disabled = true;"
                    UseSubmitBehavior="false" OnClick="imbCrear_Click1" ValidationGroup="comi" />
                <asp:Button ID="imbActualiza" runat="server" Text="Actualizar" OnClientClick="this.disabled = true;"
                    UseSubmitBehavior="false" OnClick="imbActualiza_Click1" ValidationGroup="comi" />
                <asp:Button ID="imbSalir" runat="server" Text="Salir" OnClientClick="this.disabled = true;"
                    UseSubmitBehavior="false" OnClick="imbSalir_Click1" />
                <%--                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
--%>
                <ajaxToolkit:ConfirmButtonExtender ID="CbeimbCrear" runat="server" TargetControlID="imbCrear"
                    ConfirmText="Esta Seguro(a) de Ingresar la Postura ? ">
                </ajaxToolkit:ConfirmButtonExtender>
                <%--                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
--%>
                <ajaxToolkit:ConfirmButtonExtender ID="CbeimbActualiza" runat="server" TargetControlID="imbActualiza"
                    ConfirmText="Esta Seguro(a) de Modificar la Postura ? ">
                </ajaxToolkit:ConfirmButtonExtender>
                <%--                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
--%>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="90%" runat="server"
        id="tblBuscar" visible="false">
        <tr>
            <td class="td1">
                Modalidad
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusModalidad" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
        width="90%">
        <tr>
            <td colspan="2" align="center">
                <div style="overflow: scroll; width: 1100px; height: 450px;">
                    <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgConsulta_EditCommand" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="numero_id" HeaderText="No Id" ItemStyle-Width="80px">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_rueda" HeaderText="Rueda" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="tipo_rueda" HeaderText="Desc Rueda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_rueda" HeaderText="Fecha Rueda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="punto_entrega" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="periodo_entrega" HeaderText="Periodo Entrega" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_entrega" HeaderText="Fecha Entrega" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_postura" HeaderText="Cantidad Oferta" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0: ###,###,###,##0.00}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="Precio Oferta" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0: ###,###,###,##0.00}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_max_modificacion" HeaderText="Fecha Max Modificacion"
                                ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Ingresar" EditText="Ingresar"></asp:EditCommandColumn>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass="">
                            </asp:EditCommandColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>

</asp:Content>