﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_Reactivar_5 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    String strRutaArchivo;
    SqlDataReader lLector;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString() + "mensaje.txt";
            lConexion = new clConexion(goInfo);
        }
        catch (Exception ex)
        {
 
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReactivar_Click(object sender, EventArgs e)
    {
        string oError = "";
        string sMensaje = "";

        if (TxtObservacion.Text == "")
            oError += "Debe digitar las observaciones de la reactivaccion\\n";

        if (oError == "")
        {
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_observaciones"};
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar};
            string[] lValorParametros = { Session["numero_rueda"].ToString(), TxtObservacion.Text}; ///  

            SqlDataReader lLector;
            lConexion.Abrir();
            try
            {
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetActRueda", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        oError += lLector["error"].ToString() + "\\n";
                }
                if (oError == "")
                {
                    sMensaje = "la rueda se reactivó exitosamente";
                    File.SetCreationTime(strRutaArchivo, DateTime.Now);
                    Toastr.Success(this, sMensaje);
                    Session["hora"] = "";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                oError = "Error al reactivar la rueda. " + ex.Message;
                lConexion.Cerrar();
            }
        }
        if (oError != "")
        {
            Toastr.Warning(this, oError);
        }
    }
}
