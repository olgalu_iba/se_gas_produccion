﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using SIB.BASE;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_DecSubBim : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    SqlDataReader lLector;
    string sRutaArc = ConfigurationManager.AppSettings["rutaCarga"].ToString();

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            try
            {
                lsTitulo = "";
                // Carga informacion de combos
                //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
                if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
                {
                    lsIndica = this.Request.QueryString["lsIndica"].ToString();
                }
                if (this.Request.QueryString["fase"] != null && this.Request.QueryString["fase"].ToString() != "")
                {
                    Session["fase"] = this.Request.QueryString["fase"].ToString();
                }
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", "estado = 'A'  Order by razon_social", 0, 4);
                LlenarControles(lConexion.gObjConexion, ddlBusOperador, "m_operador", "estado = 'A'  Order by razon_social", 0, 4);
                LlenarControles(lConexion.gObjConexion, ddlBusFuente, "m_pozo", "estado = 'A' and ind_campo_pto ='C' Order by descripcion", 0, 1);
                LlenarControles(lConexion.gObjConexion, ddlBusPozo, "m_pozo", "estado = 'A'  Order by descripcion", 0, 1);
                LlenarControles1(lConexion.gObjConexion, ddlMes, "", "", 0, 1);
                lConexion.Cerrar();

            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas en la Carga de la Pagina. " + ex.Message.ToString();
            }
            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlBusOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
                ddlBusOperador.Enabled = false;
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }

        }
        try
        {
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_ValidaDecSubBim", null, null, null);
            if (lLector.HasRows)
            {
                lLector.Read();
                lblMensaje.Text = lLector["error"].ToString();
                hndTipoDec.Value = lLector["tipo_dec"].ToString();
                hndIngDelta.Value = lLector["ing_delta"].ToString();
                hndAccion.Value = lLector["accion"].ToString();
                if (lblMensaje.Text != "")
                {
                    hlkNuevo.Enabled = false;
                    hlkListar.Enabled = false;
                    hlkBuscar.Enabled = false;
                    imbCrear.Visible = false;
                    imbActualiza.Visible = false;
                    dtgConsulta.Columns[16].Visible = false;
                }
            }
            else
            {
                hlkNuevo.Enabled = false;
                hlkListar.Enabled = false;
                hlkBuscar.Enabled = false;
                imbCrear.Visible = false;
                imbActualiza.Visible = false;
                hndIngDelta.Value = "N";
            }
            lLector.Close();
            lLector.Dispose();
            if (hndTipoDec.Value == "P")
            {
                ddlDeclina.SelectedValue = "S";
                ddlDeclina.Enabled = false;
                hndPozo.Value = ddlFuente.SelectedValue;
                ddlFuente.Items.Clear();
                LlenarControles(lConexion.gObjConexion, ddlFuente, "m_pozo poz, m_tipo_campo tpo", "poz.estado = 'A' and poz.ind_campo_pto ='C' and poz.campo_declinacion = 'S' and poz.codigo_tipo_campo = tpo.codigo_tipo_campo and tpo.permite_decla_anti_sb ='S' Order by poz.descripcion", 0, 1);
                try { 
                    ddlFuente.SelectedValue = hndPozo.Value; 
                }
                catch (Exception ex)
                { 
                }
                hndPozo.Value = ddlPozo.SelectedValue;
                ddlPozo.Items.Clear();
                ddlFuente_SelectedIndexChanged(null, null);
                try
                {
                    ddlPozo.SelectedValue = hndPozo.Value;
                }
                catch (Exception ex)
                {
                }
                
                trDelta.Visible = false;
                dtgConsulta.Columns[2].Visible = false;
                dtgConsulta.Columns[14].Visible = false;
            }

            if (hndTipoDec.Value == "N")
            {
                ddlDeclina.SelectedValue = "N";
                ddlDeclina.Enabled = false;
                trMes.Visible = false;
                hndPozo.Value = ddlFuente.SelectedValue;
                ddlFuente.Items.Clear();
                LlenarControles(lConexion.gObjConexion, ddlFuente, "m_pozo", "estado = 'A' and ind_campo_pto ='C'  Order by descripcion", 0, 1);
                try
                {
                    ddlFuente.SelectedValue = hndPozo.Value;
                }
                catch (Exception ex)
                {
                }
                hndPozo.Value = ddlPozo.SelectedValue;
                ddlPozo.Items.Clear();
                LlenarControles(lConexion.gObjConexion, ddlPozo, "m_pozo poz, m_caracteristica_sub car", "car.codigo_tipo_subasta = 6 and car.tipo_caracteristica = 'T' and car.destino_rueda = 'G' and car.tipo_mercado = 'P' and poz.codigo_pozo = car.codigo_caracteristica and poz.estado ='A' and car.estado ='A' Order by poz.descripcion", 0, 1);
                try
                {
                    ddlPozo.SelectedValue = hndPozo.Value;
                }
                catch (Exception ex)
                {
                }

                if (hndIngDelta.Value == "S")
                {
                    trFuente.Visible = true;
                    trDelta.Visible = true;
                }
                else
                {
                    trFuente.Visible = false;
                    trDelta.Visible = false;
                }
                if (hndAccion.Value == "M")
                    hlkNuevo.Enabled = false;
                dtgConsulta.Columns[2].Visible = true;
                dtgConsulta.Columns[3].Visible = true;
                dtgConsulta.Columns[14].Visible = true;
            }


            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "Problemas en la Carga de la Pagina. " + ex.Message.ToString());
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "t_declaracion_sub_bim");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgConsulta.Columns[16].Visible = (Boolean)permisos["UPDATE"];
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 22 de 2014
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        //lConexion.Abrir();
        //lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador ope, m_tipos_operador tpo, m_operador_subasta opeS", " ope.codigo_operador =" + goInfo.cod_comisionista + " and ope.tipo_operador = tpo.sigla and tpo.codigo_tipo_operador = opeS.codigo_tipo_operador and opeS.punta = 'C' And opeS.codigo_tipo_subasta = 1 ");
        //if (!lLector.HasRows)
        //{
        //    lblMensaje.Text = "El tipo de operador no está autorizado para " + lsTitulo + ". <br>";
        //    tblCaptura.Visible = false;
        //    tblgrilla.Visible = true;
        //    tblBuscar.Visible = false;
        //    imbCrear.Visible = false;
        //    imbActualiza.Visible = false;

        //}
        //else
        //{
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        if (hndTipoDec.Value == "P")
            lblTitulo.Text = "Crear Declaración Previa";
        else
            lblTitulo.Text = "Crear Declaración Normal";
        hndCodigo.Value = "0";
        ddlFuente.SelectedValue = "0";
        ddlFuente_SelectedIndexChanged(null, null);
        TxtCantidad.Text = "";
        TxtPrecio.Text = "";
        TxtDelta.Text = "";
        //}
        //lLector.Close();
        //lLector.Dispose();
        //lConexion.Cerrar();
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        if (hndTipoDec.Value == "P")
            lblTitulo.Text = "Listar Declaración Previa";
        else
            lblTitulo.Text = "Listar Declaración Normal";
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        bool Error = false;
        if (modificar != null && modificar != "")
        {
            try
            {
               
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    ddlOperador.Enabled = false;
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_declaracion_sub_bim", " codigo_declaracion = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        try
                        {
                            ddlOperador.SelectedValue = lLector["codigo_operador"].ToString();
                        }
                        catch (Exception ex)
                        {
                            Toastr.Warning(this, "El operador del registro no existe o esta inactivo<br>");
                            Error = true;
                        }
                        ddlMes.SelectedValue = lLector["mes_rueda_dec"].ToString();
                        try
                        {
                            ddlFuente.SelectedValue = lLector["codigo_fuente"].ToString();
                            ddlFuente_SelectedIndexChanged(null, null);
                        }
                        catch (Exception ex)
                        {
                            Toastr.Warning(this, "La fuente del registro no existe o esta inactivo<br>");
                            Error = true;
                        }
                        try
                        {
                            ddlPozo.SelectedValue = lLector["codigo_pozo"].ToString();
                        }
                        catch (Exception ex)
                        {
                            Toastr.Warning(this, "EL punto del SNT del registro no existe o esta inactivo<br>");
                            Error = true;
                        }
                        TxtCantidad.Text = lLector["cantidad"].ToString();
                        TxtPrecio.Text = lLector["precio_reserva"].ToString();
                        TxtDelta.Text = lLector["precio_delta"].ToString();
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                    }
                    lLector.Close();
                    lLector.Dispose();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    Toastr.Warning(this, "No se Puede editar el Registro por que esta Bloqueado. Cádigo declaración " + modificar.ToString());

                    Error = true;
                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, ex.Message);
                Error = true;
                
            }
        }
        if (!Error)
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            if (hndTipoDec.Value == "P")
                lblTitulo.Text = "Modificar Declaración Previa";
            else
                lblTitulo.Text = "Modificar Declaración Normal";
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        if (hndTipoDec.Value == "P")
            lblTitulo.Text = "Consultar Declaración previa";
        else
            lblTitulo.Text = "Consultar Declaración normal";
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_operador", "@P_codigo_fuente", "@P_codigo_pozo" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { "0", "0", "0" };

        try
        {
            if (ddlBusOperador.SelectedValue != "0")
                lValorParametros[0] = ddlBusOperador.SelectedValue;
            if (ddlBusFuente.SelectedValue != "0")
                lValorParametros[1] = ddlBusFuente.SelectedValue;
            if (ddlBusPozo.SelectedValue != "")
                lValorParametros[2] = ddlBusPozo.SelectedValue;

            lConexion.Abrir();
            dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetDecSubBim", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgConsulta.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgConsulta.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        DateTime ldFecha = DateTime.Now;

        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            try
            {
                hndCodigo.Value = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text;
                Modificar(hndCodigo.Value);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Problemas en la Recuperación del Registro.! " + ex.Message.ToString());             
            }
        }
    }

    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lstable, string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lstable, lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetMesSubBim", null, null, null);
        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);
        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["nombre_mes"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }

    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='t_declaracion_sub_bim' and llave_registro='codigo_declaracion=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_declaracion=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "t_declaracion_sub_bim";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_declaracion_sub_bim", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlFuente_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (hndTipoDec.Value == "P")
        {
            ddlPozo.Items.Clear();
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlPozo, "m_pozo poz, m_fuente_pozo fue", "poz.estado = 'A' and fue.codigo_fuente = " + ddlFuente.SelectedValue + " and fue.codigo_pozo = poz.codigo_pozo Order by descripcion", 0, 1);
            lConexion.Cerrar();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click1(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_declaracion", "@P_codigo_operador", "@P_numero_rueda", "@P_tipo_declaracion", "@P_campo_declinacion", "@P_codigo_fuente", "@P_codigo_pozo", "@P_cantidad", "@P_precio_reserva", "@P_delta_precio", "@P_ing_delta", "@P_accion", "@P_mes_rueda_dec" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char, SqlDbType.Char, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Char, SqlDbType.Int, SqlDbType.Int };
        String[] lValorParametros = { "0", "0", "0", "", "", "0", "0", "0", "0", "0", "", "1", "0" };
        lblMensaje.Text = "";
        valida_datos();
        try
        {
            if (lblMensaje.Text == "")
            {

                lConexion.Abrir();

                lValorParametros[1] = ddlOperador.SelectedValue;
                lValorParametros[2] = "0";
                lValorParametros[3] = hndTipoDec.Value;
                lValorParametros[4] = ddlDeclina.SelectedValue;
                if (trFuente.Visible)
                    lValorParametros[5] = ddlFuente.SelectedValue;
                else
                    lValorParametros[5] = "0";
                lValorParametros[6] = ddlPozo.SelectedValue;
                lValorParametros[7] = TxtCantidad.Text.Trim().Replace(",", "");
                lValorParametros[8] = TxtPrecio.Text.Trim().Replace(",", "");
                if (trDelta.Visible)
                    lValorParametros[9] = TxtDelta.Text.Trim().Replace(",", "");
                else
                    lValorParametros[9] = "0";
                lValorParametros[10] = hndIngDelta.Value;
                lValorParametros[12] = ddlMes.SelectedValue;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetDecSubBim", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lblMensaje.Text = lLector["error"].ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    lConexion.Cerrar();
                    TxtCantidad.Text = "";
                    Toastr.Success(this, "Registro Ingresado Correctamente.!");

                    Listar();
                }
            }
            else
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "Error en el registro de la declaración." + ex.Message);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click1(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_declaracion", "@P_tipo_declaracion", "@P_cantidad", "@P_precio_reserva", "@P_delta_precio", "@P_accion", "@P_codigo_fuente", "@P_codigo_pozo", "@P_mes_rueda_dec" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
        String[] lValorParametros = { "0", "", "0", "0", "0", "2", ddlFuente.SelectedValue, ddlPozo.SelectedValue, ddlMes.SelectedValue };

        lblMensaje.Text = "";
        try
        {
            valida_datos();
            if (lblMensaje.Text == "")
            {
            
                lConexion.Abrir();
                lValorParametros[0] = hndCodigo.Value;
                lValorParametros[1] = hndTipoDec.Value;
                lValorParametros[2] = TxtCantidad.Text.Trim().Replace(",", "");
                lValorParametros[3] = TxtPrecio.Text.Trim().Replace(",", "");
                if (trDelta.Visible)
                    lValorParametros[4] = TxtDelta.Text.Trim().Replace(",", "");
                else
                    lValorParametros[4] = "0";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetDecSubBim", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lblMensaje.Text = lLector["error"].ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    lConexion.Cerrar();
                    if (hndCodigo.Value != "")
                        manejo_bloqueo("E", hndCodigo.Value);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Ingresado Correctamente.!" + "');", true);
                    Listar();
                }
            }
            else
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, ex.Message);

        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click1(object sender, EventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (hndCodigo.Value != "")
            manejo_bloqueo("E", hndCodigo.Value);
        lblMensaje.Text = "";
        Listar();
    }

    protected void valida_datos()
    {
        int liCantidad;
        double oValor1 = 0;
        if (trFuente.Visible)
            if (ddlFuente.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar la fuente.<br>";
        if (trMes.Visible)
            if (ddlMes.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el mes de la subasta.<br>";
        if (ddlPozo.SelectedValue == "0")
            lblMensaje.Text += "Debe seleccionar el punto del SNT.<br>";
        if (VerificarExistencia("t_declaracion_sub_bim dec, m_parametros_sub_bim prm", "prm.año_dec_prev = dec.año_declaracion and dec.codigo_operador = " + ddlOperador.SelectedValue + " and dec.codigo_fuente =" + ddlFuente.SelectedValue + " and dec.codigo_pozo=" + ddlPozo.SelectedValue + " and mes_rueda_dec = " + ddlMes.SelectedValue + " and codigo_declaracion <> " + hndCodigo.Value))
            lblMensaje.Text += "Ya se registró la información para el rueda-punto seleccionado.<br>";
        if (TxtCantidad.Text != "")
        {
            try
            {
                liCantidad = Convert.ToInt32(TxtCantidad.Text.Replace(",", ""));
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en la Cantidad disponible.<br>";
            }
        }
        else
            lblMensaje.Text += "Debe ingresar la cantidad.<br>";

        if (TxtPrecio.Text.Trim().Length > 0)
        {
            string sOferta = TxtPrecio.Text.Trim().Replace(",", "");
            int iPos = sOferta.IndexOf(".");
            if (iPos > 0)
                if (sOferta.Length - iPos > 3)
                    lblMensaje.Text += "Se permiten máximo 2 decimales en el precio de reserva.<br>";
            try
            {
                oValor1 = Convert.ToDouble(TxtPrecio.Text.Replace(",", ""));
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Precio de reserva.<br>";
            }
        }
        else
            lblMensaje.Text += "Debe Ingresar el Precio de reserva.<br>";

        if (trDelta.Visible)
        {
            if (TxtDelta.Text.Trim().Length > 0)
            {
                string sOferta = TxtDelta.Text.Trim().Replace(",", "");
                int iPos = sOferta.IndexOf(".");
                if (iPos > 0)
                    if (sOferta.Length - iPos > 3)
                        lblMensaje.Text += "Se permiten máximo 2 decimales en el delta del precio.<br>";
                try
                {
                    oValor1 = Convert.ToDouble(TxtDelta.Text.Replace(",", ""));
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en el delta del Precio.<br>";
                }
            }
            else
                lblMensaje.Text += "Debe Ingresar el Precio de reserva.<br>";
        }
    }
}