﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_habilitacionVendSMP.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_habilitacionVendSMP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" bgcolor="#85BF46">
                <table border="0" cellspacing="2" cellpadding="2" align="right">
                    <tr>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkNuevo" runat="server" NavigateUrl="~/Procesos/frm_habilitacionVendSMP.aspx?lsIndica=N">Nuevo</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_habilitacionVendSMP.aspx?lsIndica=L">Listar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_habilitacionVendSMP.aspx?lsIndica=B">Consultar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/frm_contenido.aspx">Salir</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <%--<td class="tv2">
                            <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click" />
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgPdf" runat="server" ImageUrl="~/Images/Pdf.png" OnClick="ImgPdf_Click"
                                Width="70%" />
                        </td>
                        <td class="tv2">
                        </td>--%>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Operador:
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlOperador" runat="server" OnSelectedIndexChanged="ddlOperador_SelectedIndexChanged"
                    AutoPostBack="true">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Subasta
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlSubasta" runat="server" OnSelectedIndexChanged="ddlSubasta_SelectedIndexChanged"
                    AutoPostBack="true">
                </asp:DropDownList>
            </td>
        </tr>
        <tr runat="server" id="TrModalidad" visible="false">
            <td class="td1">
                Modalidad Contrato
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlModalidad" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Estado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlEstado" runat="server">
                    <asp:ListItem Value="C">Creado</asp:ListItem>
                    <asp:ListItem Value="A">Aprobado</asp:ListItem>
                    <asp:ListItem Value="R">Rechazado</asp:ListItem>
                    <asp:ListItem Value="S">Solucionado</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div>
                    <asp:DataGrid ID="dtgReqHab" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        HeaderStyle-CssClass="th1" PageSize="30" OnEditCommand="dtgReqHab_EditCommand">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="codigo_req_subasta" HeaderText="cod" Visible="false">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="requisito_habilitante" HeaderText="requisito" ItemStyle-Width="300px">
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Sele.">
                                <%--<HeaderTemplate>    
                                    <asp:CheckBox ID="ChkTodos" runat="server" OnCheckedChanged="ChkTodos_CheckedChanged"
                                        AutoPostBack="true" Text="Seleccionar Todos" />
                                </HeaderTemplate>--%>
                                <ItemTemplate>
                                    <asp:CheckBox ID="ChkSeleccionar" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Archivo">
                                <ItemTemplate>
                                    <asp:FileUpload ID="FuArchivo" runat="server" EnableTheming="true" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="chequeado" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="archivo_operador" HeaderText="Nombre Archivo" ItemStyle-Width="300px">
                            </asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Ver Archivo" EditText="Ver" ItemStyle-HorizontalAlign="Center">
                            </asp:EditCommandColumn>
                            <asp:BoundColumn DataField="codigo_req_hab_ope" Visible="false"></asp:BoundColumn>
                        </Columns>
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </div>
                <asp:HiddenField ID="HiddenField1" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:Button ID="imbCrear" runat="server" Text="Crear" OnClientClick="this.disabled = true;"
                    UseSubmitBehavior="false" OnClick="imbCrear_Click1" ValidationGroup="comi" />
                <asp:Button ID="imbActualiza" runat="server" Text="Actualizar" OnClientClick="this.disabled = true;"
                    UseSubmitBehavior="false" OnClick="imbActualiza_Click1" ValidationGroup="comi" />
                <asp:Button ID="imbSalir" runat="server" Text="Salir" OnClientClick="this.disabled = true;"
                    UseSubmitBehavior="false" OnClick="imbSalir_Click1" />
                <ajaxToolkit:ConfirmButtonExtender ID="CbeimbCrear" runat="server" TargetControlID="imbCrear"
                    ConfirmText="Esta Seguro(a) de Ingresar la Información ? ">
                </ajaxToolkit:ConfirmButtonExtender>
                <ajaxToolkit:ConfirmButtonExtender ID="CbeimbActualiza" runat="server" TargetControlID="imbActualiza"
                    ConfirmText="Esta Seguro(a) de Modificar la Información ? ">
                </ajaxToolkit:ConfirmButtonExtender>
                <%--                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
--%>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblBuscar" visible="false">
        <tr>
            <td class="td1">
                Operador
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusOperador" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Subasta
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusSubasta" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Estado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusEstado" runat="server">
                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                    <asp:ListItem Value="C">Creado</asp:ListItem>
                    <asp:ListItem Value="A">Aprobado</asp:ListItem>
                    <asp:ListItem Value="R">Rechazado</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
        width="90%">
        <tr>
            <td colspan="2" align="center">
                <div>
                    <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgConsulta_EditCommand" OnPageIndexChanged="dtgConsulta_PageIndexChanged"
                        HeaderStyle-CssClass="th1" PageSize="30">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="codigo_operador_rueda" HeaderText="cod" Visible="false">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_rueda" HeaderText="Rueda" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_rueda" HeaderText="Desc Rueda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Ope" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Cod Modalidad" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="desc modalidad" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="estado" HeaderText="estado" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_aprobacion_rechazo" HeaderText="Fecha Aprobacon / Rechazo">
                            </asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass="">
                            </asp:EditCommandColumn>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                            <asp:BoundColumn DataField="fecha_max_modificacion" Visible="false"></asp:BoundColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </div>
                <asp:HiddenField ID="hndCodigo" runat="server" />
                <asp:HiddenField ID="hdfCodFase" runat="server" />
                <asp:HiddenField ID="hdfDescFase" runat="server" />
                <asp:HiddenField ID="hndFecha" runat="server" />
            </td>
        </tr>
    </table>

</asp:Content>
