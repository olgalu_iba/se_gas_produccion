﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;


public partial class Procesos_frm_frame_subasta_1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        clConexion lConexion = null;
        InfoSessionVO goInfo = null;
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        Session["hora"] = "";
        //Session["hora1"] = "";
        Session["hora_mensaje"] = "";
        Session["punta"] = "";
        Session["tipo_subasta"] = this.Request.QueryString["clase_subasta"];

        SqlDataReader lLector;
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();

        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda", " codigo_tipo_subasta =1 and estado <> 'F' ");
        if (lLector.HasRows)
        {
            lLector.Read();
            Session["numero_rueda"] = lLector["numero_rueda"].ToString();
        }
        else
            Session["numero_rueda"] = "0";
        lLector.Close();
        lLector.Dispose();
        lConexion.Cerrar();
    }
}
