﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

//using System.Windows.Forms;

public partial class Procesos_frm_posturas_3 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    //String strRutaArchivo;
    private System.TimeSpan diffResult;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            if (!IsPostBack)
            {
                try
                {
                    hndCodSubasta.Value = Session["numero_rueda"].ToString();
                    if (hndCodSubasta.Value == "")
                        hndCodSubasta.Value = "0";
                    lConexion1 = new clConexion(goInfo);
                    //hndCodSubasta.Value = "21";
                    //HndFechaRueda.Value = DateTime.Now.ToShortDateString();
                    //HndFechaRueda.Value = HndFechaRueda.Value.Substring(6, 4) + "/" + HndFechaRueda.Value.Substring(3, 2) + "/" + HndFechaRueda.Value.Substring(0, 2);
                    cargarDatosGrilla();
                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, ex.Message);
                    
                }
            }
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, ex.Message, "Session Expiro. " + ex.Message, 50000);
        }
    }
    /// <summary>
    /// Nombre: cargarDatosGrilla
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para Cargar los datos en la Grio.
    /// Modificacion:
    /// </summary>
    private void cargarDatosGrilla()
    {
        if (hndCodSubasta.Value == "0")
        {
            dtgSubasta1.Columns.Clear();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.contratos.location= 'frm_contratos_3.aspx';", true);
        }
        else
        {
            try
            {
                string[] lsNombreParametros = { "@P_numero_rueda","@P_codigo_operador", "@P_codigo_ruta" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { hndCodSubasta.Value,goInfo.cod_comisionista , Session["codigo_ruta"].ToString() };
                lConexion.Abrir();
                dtgSubasta1.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaPuja3", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgSubasta1.DataBind();

                ImageButton imbControl;

                if (Session["estado"].ToString() == "5" || Session["estado"].ToString() == "F")
                {
                    dtgSubasta1.Columns[12].Visible = true;
                    foreach (DataGridItem Grilla in this.dtgSubasta1.Items)
                    {
                        imbControl = (ImageButton)Grilla.Cells[12].Controls[1];
                        imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_CurvaAgregada.aspx?ID=" + Grilla.Cells[0].Text + "','Grafica','width=900,height=500,left=250,top=90,status=no,location=0,menubar=no,toolbar=no,resizable=yes,scrollbars=yes');");
                    }
                }
                else
                    dtgSubasta1.Columns[12].Visible = false;

                lConexion.Cerrar();

                if (goInfo.cod_comisionista == "0")
                {
                    dtgSubasta1.Columns[9].Visible = false;
                    dtgSubasta1.Columns[10].Visible = false;
                    dtgSubasta1.Columns[11].Visible = false;
                }
                else
                {
                    if (Session["estado"].ToString() == "4")
                    {
                        foreach (DataGridItem Grilla in this.dtgSubasta1.Items)
                        {
                            if (Grilla.Cells[15].Text == "S")
                            {
                                Grilla.Cells[9].Controls[1].Visible = false;
                                Grilla.Cells[10].Controls[1].Visible = false;
                                Grilla.Cells[11].Controls[1].Visible = false;
                            }
                            else
                            {
                                if (Grilla.Cells[13].Text == "N")
                                {
                                    Grilla.Cells[9].Controls[1].Visible = true;
                                    Grilla.Cells[10].Controls[1].Visible = false;
                                    Grilla.Cells[11].Controls[1].Visible = false;
                                    imbControl = (ImageButton)Grilla.Cells[9].Controls[1];
                                    imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_IngresoOfertaCompra_3.aspx?ID=" + Grilla.Cells[0].Text + "&accion=C&maxPostCompra=" + Grilla.Cells[14].Text + "','Ofertar','width=600,height=450,left=350,top=150,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                                }
                                else
                                {
                                    Grilla.Cells[9].Controls[1].Visible = false;
                                    Grilla.Cells[10].Controls[1].Visible = true;
                                    Grilla.Cells[11].Controls[1].Visible = true;
                                    imbControl = (ImageButton)Grilla.Cells[10].Controls[1];
                                    imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_IngresoOfertaCompra_3.aspx?ID=" + Grilla.Cells[0].Text + "&accion=M&maxPostCompra=" + Grilla.Cells[14].Text + "','Ofertar','width=600,height=450,left=350,top=150,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (DataGridItem Grilla in this.dtgSubasta1.Items)
                        {
                            Grilla.Cells[9].Controls[1].Visible = false;
                            Grilla.Cells[10].Controls[1].Visible = false;
                            Grilla.Cells[11].Controls[1].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, ex.Message);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.contratos.location= 'frm_contratos_3.aspx';", true);
        }
    }

    public void OnItemCommand_Click(object sender, DataGridCommandEventArgs e)
    {
        string oError = "";
        string oBoton = ((System.Web.UI.WebControls.ImageButton)e.CommandSource).ID.ToString();

        if (oBoton == "imbEliminar")
        {
            try
            {
                string[] lsNombreParametros = { "@P_numero_id", "@P_codigo_operador" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int};
                string[] lValorParametros = { e.Item.Cells[0].Text, goInfo.cod_comisionista  };
                SqlDataReader lLector;
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_DelPostura3", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                    {
                        oError += lLector["error"].ToString() + "\\n";
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                }
                else
                {
                    oError = "Oferta eliminada correctamente";
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    cargarDatosGrilla();
                }
            }
            catch (Exception ex)
            {
                oError = "Error al eliminar la oferta. " + ex.Message;
            }
            Toastr.Warning(this, oError);

        }
    }
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgActividadEconomica.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgActividadEconomica.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
}

