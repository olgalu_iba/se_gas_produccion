﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_DeclaracionCantDemandaUvlp.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Procesos.frm_DeclaracionCantDemandaUvlp" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" language="javascript">

        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }

        function formatoMiles(obj, evt) {
            var T;
            var strValue = removeCommas(obj.value);
            //if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
            if ((T = /([0-9]*)$/i.exec(strValue)) != null) {
                //                T[4] = T[4].substr(0, decimales);
                //                if (T[2] == '' && T[3] == '.') T[2] = 0;
                obj.value = addCommas(T[1]);
            } else {
                alert('formato invalido ');
            }
        }
        function formatoMilesPre(obj, evt, decimales) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
                T[4] = T[4].substr(0, decimales);
                if (T[2] == '' && T[3] == '.') {
                    T[2] = 0;
                }
                obj.value = T[1] + addCommas(T[2]) + T[3] + T[4];
            }
            else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }

    </script>
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Operador</label>
                            <asp:DropDownList ID="ddlOperador" runat="server" Width="100%" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Punto Salida</label>
                            <asp:DropDownList ID="ddlPuntoSalida" runat="server" Width="100%" CssClass="form-control" />
                        </div>
                    </div>
                </div>
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="table table-responsive">
                            <asp:DataGrid ID="dtgSuministro" runat="server" AutoGenerateColumns="False" AllowPaging="false" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" Visible="false">
                                <Columns>
                                    <%--0--%>
                                    <asp:BoundColumn DataField="desc_punto_salida" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left" />
                                    <%--1--%>
                                    <asp:BoundColumn DataField="nombre_comprador" HeaderText="Titular Compra" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px" />
                                    <%--2--%>
                                    <asp:BoundColumn DataField="total_demanda" HeaderText="Total Cantidad Demandada (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0}" ItemStyle-Width="150px" />
                                    <%--3--%>
                                    <asp:BoundColumn DataField="total_equivalente" HeaderText="Total Equivalente Cantidad Demandada (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0}" ItemStyle-Width="150px" />
                                    <%--4--%>
                                    <asp:BoundColumn DataField="ind_aprobado" HeaderText="Ind Aprobado" ItemStyle-HorizontalAlign="center" />
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

</asp:Content>
