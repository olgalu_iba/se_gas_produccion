﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;

public partial class Procesos_frm_tablero_neg_prm : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    String strRutaArchivo;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            if (!IsPostBack)
            {
                HndFechaRueda.Value = DateTime.Now.ToShortDateString();
                HndFechaRueda.Value = HndFechaRueda.Value.Substring(6, 4) + "/" + HndFechaRueda.Value.Substring(3, 2) + "/" + HndFechaRueda.Value.Substring(0, 2);
                lblFecha.Text = HndFechaRueda.Value;
                /// LLeno el Combo de la Rueda
                lConexion.Abrir();
                ddlRueda.Items.Clear();
                LlenarControles(lConexion.gObjConexion, ddlRueda, "t_rueda a, m_tipos_rueda b ", " a.codigo_tipo_rueda = b.codigo_tipo_rueda And a.estado = 'A' And b.estado = 'A' And a.codigo_tipo_subasta = 5  Order by a.descripcion", 1, 17);
                lConexion.Cerrar();
                lblAgente.Text = Session["nomOperador"].ToString();
            }
        }
        catch (Exception)
        {

        }
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// <summary>
    /// Metodo que se ejecuta al Seleccionar la Rueda en la pantalla para llemar los combos de Punto de Entrega y Periodo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlRueda_SelectedIndexChanged(object sender, EventArgs e)
    {
        string sDestino = "";
        Session["codigo_punto"] = "0";
        Session["codigo_periodo"] = "0";
        Session["destino_rueda"] = "G";
        Session["tipo_mercado"] = "P";

        if (ddlRueda.SelectedValue != "0")
        {
            try
            {
                Session["numero_rueda"] = ddlRueda.SelectedValue;
                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda rue, m_tipos_rueda tpo", " rue.numero_rueda=" + ddlRueda.SelectedValue + " and rue.codigo_tipo_rueda = tpo.codigo_tipo_rueda");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    sDestino = lLector["destino_rueda"].ToString();
                    Session["destino_rueda"] = lLector["destino_rueda"].ToString();
                    Session["tipo_mercado"] = lLector["tipo_mercado"].ToString();
                }
                else
                {
                    sDestino = "0";
                }
                lLector.Close();
                lConexion.Cerrar();

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.postura.location='frm_tablero_neg_posturas.aspx';", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Error al Refrescar el Tablero. " + ex.Message.ToString() + "');", true);
            }
        }
        else
            Session["numero_rueda"] = "0";
        lConexion.Abrir();
        ddlPuntoRuta.Items.Clear();
        ddlPeriodo.Items.Clear();
        if (sDestino == "G")
        {
            if (Session["tipo_mercado"].ToString() == "S")
                LlenarControles(lConexion.gObjConexion, ddlPuntoRuta, "m_pozo punto, m_caracteristica_sub car", "  car.estado = 'A' And car.tipo_mercado='S' And car.tipo_caracteristica = 'T' And car.codigo_caracteristica=punto.codigo_pozo order by descripcion", 0, 1);
            else
                LlenarControles(lConexion.gObjConexion, ddlPuntoRuta, "m_pozo ", "  estado = 'A'  order by descripcion", 0, 1);

            LlenarControles(lConexion.gObjConexion, ddlPeriodo, "m_periodos_entrega periodo, m_caracteristica_sub car", " car.estado = 'A' And car.tipo_caracteristica = 'E' And car.destino_rueda = 'G' And car.tipo_mercado='" + Session["tipo_mercado"].ToString() + "' And car.codigo_caracteristica=periodo.codigo_periodo And car.codigo_tipo_subasta = 5  order by descripcion ", 0, 1);
        }
        if (sDestino == "T")
        {
            LlenarControles(lConexion.gObjConexion, ddlPuntoRuta, "m_ruta_snt", "  estado ='A' order by descripcion", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlPeriodo, "m_periodos_entrega periodo, m_caracteristica_sub car", " car.estado = 'A' And car.tipo_caracteristica = 'E' And car.destino_rueda = 'T' And car.tipo_mercado='" + Session["tipo_mercado"].ToString() + "' And car.codigo_caracteristica=periodo.codigo_periodo And car.codigo_tipo_subasta = 5  order by descripcion ", 0, 1);
        }

        lConexion.Cerrar();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        ////////////////////////////////////////////////
        /// Cambio Metodo por Req. 012-17 20170309 /////
        ////////////////////////////////////////////////
        //if (ddlPuntoRuta.SelectedValue == "0" || ddlPeriodo.SelectedValue == "0")
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Debe Ingresar los Filtros de búsqueda.!" + "');", true);
        //else
        //{
        Session["codigo_punto"] = ddlPuntoRuta.SelectedValue;
        Session["codigo_periodo"] = ddlPeriodo.SelectedValue;
        if (Session["codigo_punto"].ToString().Trim() == "")
            Session["codigo_punto"] = "0";
        if (Session["codigo_periodo"].ToString().Trim() == "")
            Session["codigo_periodo"] = "0";
        if (Session["numero_rueda"].ToString().Trim() == "")
            Session["numero_rueda"] = "0";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.posturaC.location='frm_tablero_neg_posturas.aspx?punta=C';", true);
        //}

    }
    /// <summary>
    /// Restablece los parametros de Busqueda
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRestablecer_Click(object sender, EventArgs e)
    {
        ddlPuntoRuta.SelectedValue = "0";
        ddlPeriodo.SelectedValue = "0";
        Session["codigo_punto"] = "0";
        Session["codigo_periodo"] = "0";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.posturaC.location='frm_tablero_neg_posturas.aspx?punta=C';", true);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlPuntoRuta_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPuntoRuta.SelectedValue != "0")
            Session["codigo_punto"] = ddlPuntoRuta.SelectedValue;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlPeriodo_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPeriodo.SelectedValue != "0")
            Session["codigo_periodo"] = ddlPeriodo.SelectedValue;

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevoC_Click(object sender, EventArgs e)
    {
        if (ddlRueda.SelectedValue != "0" && ddlPeriodo.SelectedValue != "0" && ddlPuntoRuta.SelectedValue != "0")
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('frm_tablero_neg_IngPosturas.aspx?ID=0&punta=C','IngresoPosturas','width=540,height=500,left=350,top=100,status=no,location=0,menubar=no,toolbar=no,resizable=no');", true);
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Para Ingresar una Postura debe Ingresar los Parámetros de Busqueda de Rueda, Punto / Ruta y Período.!" + "');", true);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void btnNuevoV_Click(object sender, EventArgs e)
    //{
    //    if (ddlRueda.SelectedValue != "0" && ddlPeriodo.SelectedValue != "0" && ddlPuntoRuta.SelectedValue != "0")
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('frm_tablero_neg_IngPosturas.aspx?ID=0&punta=V','IngresoPosturas','width=540,height=500,left=350,top=100,status=no,location=0,menubar=no,toolbar=no,resizable=no');", true);
    //    else
    //    {
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Para Ingresar una Postura debe Ingresar los Parámetros de Busqueda de Rueda, Punto / Ruta y Período.!" + "');", true);
    //    }
    //}
}