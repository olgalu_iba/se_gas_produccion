﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_DeclaracionInfUvlp.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" EnableEventValidation="false" Inherits="Procesos.frm_DeclaracionInfUvlp" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" lang="javascript">

        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }

        function formatoMiles(obj, evt) {
            var T;
            var strValue = removeCommas(obj.value);
            //if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
            if ((T = /([0-9]*)$/i.exec(strValue)) != null) {
                //                T[4] = T[4].substr(0, decimales);
                //                if (T[2] == '' && T[3] == '.') T[2] = 0;
                obj.value = addCommas(T[1]);
            } else {
                alert('formato invalido ');
            }
        }
        function formatoMilesPre(obj, evt, decimales) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
                T[4] = T[4].substr(0, decimales);
                if (T[2] == '' && T[3] == '.') {
                    T[2] = 0;
                }
                obj.value = T[1] + addCommas(T[2]) + T[3] + T[4];
            }
            else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }

    </script>

    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
                            <asp:DropDownList ID="ddlOperador" CssClass="form-control" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Destino Rueda" AssociatedControlID="ddlDestino" runat="server" />
                            <%--20210531--%>
                            <asp:DropDownList ID="ddlDestino" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlDestino_SelectedIndexChanged"
                                AutoPostBack="true">
                                <asp:ListItem Value="G" Text="Suministro de Gas"></asp:ListItem>
                                <asp:ListItem Value="T" Text="Capacidad de Transporte"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgSuministro" runat="server" AutoGenerateColumns="False" AllowPaging="false" PagerStyle-HorizontalAlign="Center" OnItemCommand="dtgSuministro_EditCommand"
                                Width="100%" CssClass="table-bordered" Visible="false">
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemStyle VerticalAlign="Middle" />
                                <%--20180404 rq002-18--%>
                                <Columns>
                                    <%--0--%>
                                    <asp:TemplateColumn HeaderText="Sele.">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="ChkTodos" runat="server" OnCheckedChanged="ChkTodosSum_CheckedChanged"
                                                AutoPostBack="true" Text="Sel. Todos" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ChkSeleccionar" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--1--%>
                                    <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="No Id" ItemStyle-Width="80px" Visible="false"></asp:BoundColumn>
                                    <%--2--%>
                                    <asp:BoundColumn DataField="numero_contrato" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <%--3--%>
                                    <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Snt" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--4--%>
                                    <asp:BoundColumn DataField="operador_compra" HeaderText="Cód. Titular" ItemStyle-HorizontalAlign="center" Visible="false"></asp:BoundColumn>
                                    <%--20180815 BUG230--%>
                                    <%--5--%>
                                    <asp:BoundColumn DataField="nombre_comprador" HeaderText="Titular Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--6--%>
                                    <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--7--%>
                                    <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Número Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--8--%>
                                    <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left" DataFormatString="{0: yyyy/MM/dd}"></asp:BoundColumn>
                                    <%--9--%>
                                    <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left" DataFormatString="{0: yyyy/MM/dd}"></asp:BoundColumn>
                                    <%--10--%>
                                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad Energía Contratada (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--11--%>
                                    <asp:BoundColumn DataField="equiv_demand" HeaderText="Cantidad Energía Contratada (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--12--%>
                                    <asp:BoundColumn DataField="cantidad_declarada" HeaderText="Cantidad Energía Contratada Real(MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--13--%>
                                    <asp:BoundColumn DataField="equiv_declarada" HeaderText="Cantidad Energía Contratada Real(KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--14--%>
                                    <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cód Fte"></asp:BoundColumn>
                                    <%--20180815 BUG230--%>
                                    <%--15--%>
                                    <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente"></asp:BoundColumn>
                                    <%--16--%>
                                    <asp:BoundColumn DataField="estado" HeaderText="Estado Declaración" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <%--20180815 BUG230--%>
                                    <%--17--%>
                                    <asp:EditCommandColumn HeaderText="Energia Demandada" EditText="Energia Demandada" Visible="false" />
                                    <%--18--%>
                                    <asp:EditCommandColumn HeaderText="Complementar" EditText="Complementar" Visible="false" />
                                    <%--19--%>
                                    <asp:BoundColumn DataField="cod_cont_mp_uvlp" Visible="false"></asp:BoundColumn>
                                    <%--20--%>
                                    <asp:BoundColumn DataField="codigo_punto_salida" Visible="false"></asp:BoundColumn>
                                    <%--21--%>
                                    <asp:BoundColumn DataField="desc_punto_salida" HeaderText="Ultimo Punto Salida" ItemStyle-HorizontalAlign="Left" Visible="false"></asp:BoundColumn>
                                    <%--22--%>
                                    <asp:BoundColumn DataField="ind_cnt_dec" Visible="false"></asp:BoundColumn>
                                    <%--23--%>
                                    <asp:BoundColumn DataField="codigo_punto_entrega" Visible="false"></asp:BoundColumn>
                                    <%--24--%>
                                    <asp:BoundColumn DataField="cantidad" Visible="false"></asp:BoundColumn>
                                    <%--25--%>
                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more-1"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                    <!--begin::Nav-->
                                                    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <ul class="kt-nav">
                                                                <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lkbComplementar" CssClass="kt-nav__link" CommandName="Complementar" runat="server">
                                                                <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                                <span class="kt-nav__link-text">Complementar</span>
                                                                    </asp:LinkButton>
                                                                </li>
                                                            </ul>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgTransporte" runat="server" AutoGenerateColumns="False" AllowPaging="false" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered"
                                OnItemCommand="dtgTransporte_EditCommand" Visible="false">
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemStyle VerticalAlign="Bottom" />
                                <Columns>
                                    <%--0--%>
                                    <asp:TemplateColumn HeaderText="Sele.">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="ChkTodos" runat="server" OnCheckedChanged="ChkTodosTra_CheckedChanged"
                                                AutoPostBack="true" Text="Sel. Todos" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ChkSeleccionar" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--1--%>
                                    <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="No Id" ItemStyle-Width="80px" Visible="false"></asp:BoundColumn>
                                    <%--2--%>
                                    <asp:BoundColumn DataField="numero_contrato" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <%--3--%>
                                    <asp:BoundColumn DataField="nombre_comprador" HeaderText="Titular Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--4--%>
                                    <asp:BoundColumn DataField="desc_punto" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="220px"></asp:BoundColumn>
                                    <%--5--%>
                                    <asp:BoundColumn DataField="sentido_flujo" HeaderText="Sentido Flujo Gas" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--6--%>
                                    <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contrato" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--7--%>
                                    <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Número Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--8--%>
                                    <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left" DataFormatString="{0: yyyy/MM/dd}"></asp:BoundColumn>
                                    <%--9--%>
                                    <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left" DataFormatString="{0: yyyy/MM/dd}"></asp:BoundColumn>
                                    <%--10--%>
                                    <asp:BoundColumn DataField="cantidad" HeaderText="Capacidad Transporte Contratada (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--11--%>
                                    <asp:BoundColumn DataField="cod_cont_mp_uvlp" Visible="false"></asp:BoundColumn>
                                    <%--12--%>
                                    <asp:BoundColumn DataField="cantidad_declarada" HeaderText="Capacidad Transporte Contratada Real (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--13--%>
                                    <asp:BoundColumn DataField="estado" HeaderText="Estado Declaración"></asp:BoundColumn>
                                    <%--20180815 BUG230--%>
                                    <%--14--%>
                                    <asp:EditCommandColumn HeaderText="Complementar" EditText="Complementar" Visible="False" />
                                    <%--15--%>
                                    <asp:BoundColumn DataField="codigo_punto_entrega" Visible="false"></asp:BoundColumn>
                                    <%--16--%>
                                    <asp:BoundColumn DataField="operador_compra" Visible="false"></asp:BoundColumn>
                                    <%--17--%>
                                    <asp:BoundColumn DataField="cantidad" Visible="false"></asp:BoundColumn>
                                    <%--18--%>
                                    <asp:BoundColumn DataField="cantidad_real" Visible="false"></asp:BoundColumn>
                                    <%--19--%>
                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more-1"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                    <!--begin::Nav-->
                                                    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <ul class="kt-nav">
                                                                <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lkbComplementarTrans" CssClass="kt-nav__link" CommandName="Complementar" runat="server">
                                                                <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                                <span class="kt-nav__link-text">Complementar</span>
                                                                    </asp:LinkButton>
                                                                </li>
                                                            </ul>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <%--Excel--%>
                <asp:DataGrid ID="dtgTransporteTramo" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table-bordered" AllowPaging="false" PagerStyle-HorizontalAlign="Center" Visible="false">
                    <Columns>
                        <%--0--%>
                        <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Operación" ItemStyle-Width="80px"></asp:BoundColumn>
                        <%--1--%>
                        <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--2--%>
                        <asp:BoundColumn DataField="codigo_tramo" HeaderText="Código Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--3--%>
                        <asp:BoundColumn DataField="desc_tramo" HeaderText="Descripción del Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--4--%>
                        <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Número Contrato" ItemStyle-Width="80px"></asp:BoundColumn>
                        <%--5--%>
                        <asp:BoundColumn DataField="operador_compra" HeaderText="Cód. Titular" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--20180815 BUG230--%>
                        <%--6--%>
                        <asp:BoundColumn DataField="nombre_comprador" HeaderText="Titular Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--7--%>
                        <asp:BoundColumn DataField="sentido_flujo" HeaderText="Sentido Flujo Gas" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px"></asp:BoundColumn>
                        <%--8--%>
                        <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contrato" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px"></asp:BoundColumn>
                        <%--9--%>
                        <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left" DataFormatString="{0: yyyy/MM/dd}"></asp:BoundColumn>
                        <%--10--%>
                        <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left" DataFormatString="{0: yyyy/MM/dd}"></asp:BoundColumn>
                        <%--11--%>
                        <asp:BoundColumn DataField="cantidad" HeaderText="Capacidad Transporte Contratada (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="150px"></asp:BoundColumn>
                        <%--12--%>
                        <asp:BoundColumn DataField="cantidad_declarada" HeaderText="Capacidad Transporte Contratada Real (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="150px"></asp:BoundColumn>
                        <%--13--%>
                        <asp:BoundColumn DataField="estado" HeaderText="Estado Declaración"></asp:BoundColumn>
                        <%--20180815 BUG230--%>
                        <%--14--%>
                        <asp:BoundColumn DataField="porc_cargo_fijo" HeaderText="Porcentaje de Cargo Fijo Inversiónersión" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                        <%--15--%>
                        <asp:BoundColumn DataField="cargo_fijo_inv" HeaderText="Cargo Fijo Inversión" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###.###}"></asp:BoundColumn>
                        <%--16--%>
                        <asp:BoundColumn DataField="porc_cargo_var" HeaderText="Porcentaje de Cargo Variable Inversión" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                        <%--17--%>
                        <asp:BoundColumn DataField="cargo_variable" HeaderText="Cargo Variable Inversión" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###.###}"></asp:BoundColumn>
                        <%--18--%>
                        <asp:BoundColumn DataField="cargo_fijo_adm" HeaderText="Cargo Fijo Administración" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###.###}"></asp:BoundColumn>
                        <%--19--%>
                        <asp:BoundColumn DataField="porc_imp_tra" HeaderText="Porcentaje Impuesto Transporte" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###.00}"></asp:BoundColumn>
                        <%--20--%>
                        <asp:BoundColumn DataField="porc_cuota" HeaderText="Porcentaje Cuota de Fomento" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###.00}"></asp:BoundColumn>
                    </Columns>
                    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                </asp:DataGrid>
            </div>
        </div>
    </div>

    <%--Modals--%>
    <div class="modal fade" id="registroDecInfoSubUVLP" tabindex="-1" role="dialog" aria-labelledby="mdlRegistroDecInfoSubUVLPLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="registroDecInfoSubUVLPInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlRegistroDecInfoSubUVLPLabel" runat="server">Agregar</h5>
                        </div>
                        <div class="modal-body">
                            <div id="tblCapturaSuministro" class="row" visible="false" runat="server">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Punto salida del SNT" AssociatedControlID="ddlPunto" runat="server" />
                                        <asp:DropDownList ID="ddlPunto" runat="server" CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Cantidad Contratada por el Titular (MBTUD)" AssociatedControlID="TxtCantidadCont" runat="server" />
                                        <asp:TextBox ID="TxtCantidadCont" runat="server" CssClass="form-control"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtCantidadCont" runat="server" TargetControlID="TxtCantidadCont" FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Equivalente en KPCD de energía contratada (Tomada para el cálculo de Capacidad Excedentaria)" AssociatedControlID="TxtEquivalContraSum" runat="server" />
                                        <asp:TextBox ID="TxtEquivalContraSum" runat="server" CssClass="form-control"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtEquivalContraSum" runat="server" TargetControlID="TxtEquivalContraSum" FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                                        <asp:HiddenField ID="hdfNoId" runat="server" />
                                        <asp:HiddenField ID="hdfVerif" runat="server" />
                                        <asp:HiddenField ID="hdfCodContSum" runat="server" />
                                        <asp:HiddenField ID="hndFecha" runat="server" />
                                        <asp:HiddenField ID="hdfOperaCompra" runat="server" />
                                        <asp:HiddenField ID="hdfPuntoSalida" runat="server" />
                                        <asp:HiddenField ID="hdfIdComplementa" runat="server" />
                                        <asp:HiddenField ID="hdfCodRuta" runat="server" />
                                        <asp:HiddenField ID="hdfOpeAut" runat="server" />
                                        <asp:HiddenField ID="hdfCostosTramo" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Cantidad Demandada por el Titular (MBTUD)" AssociatedControlID="TxtCantidadDemanSum" runat="server" />
                                        <asp:TextBox ID="TxtCantidadDemanSum" runat="server" Width="100%" CssClass="form-control"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtCantidadDemanSum" runat="server" TargetControlID="TxtCantidadDemanSum" FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Equivalente en KPCD de Cantidad Demandada (Tomada para el cálculo de Capacidad Excedentaria)" AssociatedControlID="TxtEquivalDemanSum" runat="server" />
                                        <asp:TextBox ID="TxtEquivalDemanSum" runat="server" Width="100%" CssClass="form-control"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtEquivalDemanSum" runat="server" TargetControlID="TxtEquivalDemanSum" FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Fuente" AssociatedControlID="ddlFuente" runat="server" />
                                        <asp:DropDownList ID="ddlFuente" runat="server" Width="100%" CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
                                    </div>
                                </div>
                            </div>
                            <div id="tblCapturaTransporte" class="row" visible="false" runat="server">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Capacidad Contratada Real (KPCD)" AssociatedControlID="txtCapacReal" runat="server" />
                                        <asp:TextBox ID="txtCapacReal" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="comi" />
                                        <asp:HiddenField ID="hdfCapacidadOrg" runat="server" />
                                    </div>
                                </div>
                            </div>

                            <div id="divCapturaSuministro" class="row justify-content-center" runat="server">
                                <div class="form-group">
                                    <div class="btn-group btn-group-justified">
                                        <asp:Button ID="btnCrearGas" Text="Crear" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="btnCrearGas_Click" CssClass="btn btn-primary" Style="margin: 2px; border-radius: 0.3rem;" runat="server" />
                                        <asp:Button ID="btnModificaGas" Text="Modificar" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="btnModificaGas_Click" Visible="False" CssClass="btn btn-primary" Style="margin: 2px; border-radius: 0.3rem;" runat="server" />
                                        <asp:Button ID="btnLimpiar" Text="Limpiar" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="btnLimpiarGas_Click" CssClass="btn btn-primary" Style="margin: 2px; border-radius: 0.3rem;" runat="server" />
                                    </div>
                                </div>
                            </div>

                            <hr />

                            <div class="table table-responsive">
                                <asp:DataGrid ID="dtgDetalleGas" runat="server" AutoGenerateColumns="False" AllowPaging="false" PagerStyle-HorizontalAlign="Center"
                                    OnItemCommand="dtgDetalleGas_EditCommand" Width="100%" CssClass="table-bordered" Visible="false">
                                    <Columns>
                                        <%--0--%>
                                        <asp:BoundColumn DataField="cod_cont_mp_uvlp_sum" Visible="false"></asp:BoundColumn>
                                        <%--1--%>
                                        <asp:BoundColumn DataField="cod_cont_mp_uvlp" Visible="false"></asp:BoundColumn>
                                        <%--2--%>
                                        <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Pto sal"></asp:BoundColumn>
                                        <%--3--%>
                                        <asp:BoundColumn DataField="desc_punto_salida" HeaderText="descripcion"></asp:BoundColumn>
                                        <%--4--%>
                                        <asp:BoundColumn DataField="cantidad_contratada" HeaderText="Cantidad contratada MBTUD"></asp:BoundColumn>
                                        <%--5--%>
                                        <asp:BoundColumn DataField="equiv_cont_kpcd" HeaderText="Equivalente contratada KPCD"></asp:BoundColumn>
                                        <%--6--%>
                                        <asp:BoundColumn DataField="cantidad_demandada" HeaderText="Cantidad Demandada MBTUD"></asp:BoundColumn>
                                        <%--7--%>
                                        <asp:BoundColumn DataField="equiv_demand_kpcd" HeaderText="Equivalente demandada KPCD"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                            <ItemTemplate>
                                                <div class="dropdown dropdown-inline">
                                                    <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="flaticon-more-1"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                        <!--begin::Nav-->
                                                        <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <ul class="kt-nav">
                                                                    <li class="kt-nav__item">
                                                                        <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                                            <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                                            <span class="kt-nav__link-text">Modificar</span>
                                                                        </asp:LinkButton>
                                                                    </li>
                                                                    <li class="kt-nav__item">
                                                                        <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                                            <i class="kt-nav__link-icon flaticon-delete"></i>
                                                                            <span class="kt-nav__link-text">Eliminar</span>
                                                                        </asp:LinkButton>
                                                                    </li>
                                                                </ul>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        <!--end::Nav-->
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                </asp:DataGrid>
                            </div>
                            <div class="table table-responsive">
                                <asp:DataGrid ID="dtgDetalleTrans" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center" Visible="false">
                                    <Columns>
                                        <%--0--%>
                                        <asp:BoundColumn DataField="cod_cont_mp_uvlp_det" ItemStyle-Width="1" Visible="false" />
                                        <%--1--%>
                                        <asp:BoundColumn DataField="cod_cont_mp_uvlp" Visible="false"></asp:BoundColumn>
                                        <%--2--%>
                                        <asp:BoundColumn DataField="codigo_tramo" HeaderText="Tramo"></asp:BoundColumn>
                                        <%--3--%>
                                        <asp:BoundColumn DataField="desc_tramo" HeaderText="Descripción del Tramo"></asp:BoundColumn>
                                        <%--4--%>
                                        <asp:TemplateColumn HeaderText="Porcentaje de Cargo Fijo Inversión">
                                            <ItemTemplate>
                                                <asp:TextBox ID="TxtPorcFijo" runat="server" CssClass="form-control" Width="60px" OnTextChanged="TxtPorcFijo_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FTBETxtPorcFijo" runat="server" TargetControlID="TxtPorcFijo" FilterType="Custom, Numbers" ValidChars="."></ajaxToolkit:FilteredTextBoxExtender>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <%--5--%> <%--20221012--%>
                                        <asp:BoundColumn DataField="cargo_fijo_inv" HeaderText="Cargo Fijo Costo Inversión (Moneda Vigente/KPCD-AÑO)" DataFormatString="{0:###,###,###,###,###,##0.000}"></asp:BoundColumn>
                                        <%--6--%>
                                        <asp:TemplateColumn HeaderText="Porcentaje de Cargo Variable Inversión">
                                            <ItemTemplate>
                                                <asp:TextBox ID="TxtPorcVar" runat="server" CssClass="form-control" Width="60px" OnTextChanged="TxtPorcVar_TextChanged" AutoPostBack="true" />
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FTBETxtPorcVar" runat="server" TargetControlID="TxtPorcVar" FilterType="Custom, Numbers" ValidChars="."></ajaxToolkit:FilteredTextBoxExtender>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <%--7--%><%--20221012--%>
                                        <asp:BoundColumn DataField="cargo_variable" HeaderText="Cargo Variable de Costo de Inversión (Moneda Vigente/KPC)" DataFormatString="{0:###,###,###,###,###,##0.000}"></asp:BoundColumn>
                                        <%--8--%><%--20221012--%>
                                        <asp:BoundColumn DataField="costo_adm" HeaderText="Cargo Fijo AOM (COP/KPCD-AÑO)"></asp:BoundColumn>
                                        <%--9--%>
                                        <asp:BoundColumn DataField="orden" Visible="false"></asp:BoundColumn>
                                        <%--10--%>
                                        <asp:BoundColumn DataField="codigo_pozo_ini" Visible="false"></asp:BoundColumn>
                                        <%--11--%>
                                        <asp:BoundColumn DataField="codigo_pozo_fin" Visible="false"></asp:BoundColumn>
                                        <%--12--%>
                                        <asp:BoundColumn DataField="capacidad_cont" Visible="false"></asp:BoundColumn>
                                        <%--13--%>
                                        <asp:BoundColumn DataField="tiene_costo" Visible="false"></asp:BoundColumn>
                                        <%--14--%>
                                        <asp:BoundColumn DataField="costo_fijo_100" Visible="false"></asp:BoundColumn>
                                        <%--15--%>
                                        <asp:BoundColumn DataField="costo_variable_100" Visible="false"></asp:BoundColumn>
                                        <%--16--%>
                                        <asp:BoundColumn DataField="costo_adm" Visible="false"></asp:BoundColumn>
                                        <%--17--%>
                                        <asp:BoundColumn DataField="porc_cargo_fijo" Visible="false"></asp:BoundColumn>
                                        <%--18--%>
                                        <asp:BoundColumn DataField="porc_cargo_var" Visible="false"></asp:BoundColumn>
                                        <%--19--%>
                                        <asp:TemplateColumn HeaderText="Impuesto Transporte">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlImpTra" CssClass="form-control" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <%--20--%>
                                        <asp:TemplateColumn HeaderText="Cuota Fomento">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlCuota" CssClass="form-control" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <%--21--%>
                                        <asp:BoundColumn DataField="porc_imp_tra" Visible="false" DataFormatString="{0:###.00}"></asp:BoundColumn>
                                        <%--22--%>
                                        <asp:BoundColumn DataField="porc_cuota" Visible="false" DataFormatString="{0:###.00}"></asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                </asp:DataGrid>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div id="divCapturaSuministro2" visible="false" runat="server">
                                <asp:Button ID="btnCancelarGas" Text="Cancelar" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="btnCancelarGas_Click" CssClass="btn btn-secondary" runat="server" />
                            </div>
                            <div id="divCapturaTransporte" visible="false" runat="server">
                                <asp:Button ID="btnCancelarTrans" Text="Cancelar" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="btnCancelarTrans_Click" CssClass="btn btn-secondary" runat="server" />
                                <asp:Button ID="btnCrearTrans" Text="Aceptar" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="btnCrearTrans_Click" CssClass="btn btn-primary" runat="server" />
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
