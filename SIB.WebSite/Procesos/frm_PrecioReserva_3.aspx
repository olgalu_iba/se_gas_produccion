﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_PrecioReserva_3.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_PrecioReserva_3" EnableEventValidation="false" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td6" align="center">
                        <asp:Label ID="lblTitulo" runat="server" Text="DEFINICION DE PRECIOS DE RESERVA"
                            Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="td6" align="center">
                        <asp:Button ID="btnCarga" Text="Plano Precios" runat="server" OnClientClick="window.open('frm_CargaPrecio_3.aspx','','width=450,height=400,left=350,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=1,scrollbars=1');" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnAprobar" Text="Aprobar" runat="server" OnClick="btnAprobar_Click" />
                    </td>
                </tr>
                <tr>
                    <td align="center" class="td6">
                        <asp:DataGrid ID="dtgPrecios" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            PagerStyle-HorizontalAlign="Center" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                            <ItemStyle CssClass="td2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="12px">
                            </ItemStyle>
                            <Columns>
                                <%--0--%>
                                <asp:TemplateColumn HeaderText="Selec">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="ChkTodos" runat="server" OnCheckedChanged="ChkTodos_CheckedChanged"
                                            AutoPostBack="true" Text="Sel. Todos" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="ChkSeleccionar" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <%--1--%>
                                <asp:BoundColumn DataField="numero_id" HeaderText="No Id"></asp:BoundColumn>
                                <%--2--%>
                                <asp:BoundColumn DataField="desc_punto_entrega" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left">
                                </asp:BoundColumn>
                                <%--3--%>
                                <asp:BoundColumn DataField="precio_reserva_act" HeaderText="Precio act" ItemStyle-HorizontalAlign="right"
                                    DataFormatString="{0: ###,###,###,##0.00}"></asp:BoundColumn>
                                <%--4--%>
                                <asp:BoundColumn DataField="cantidad_venta_act" HeaderText="cnt vta act" ItemStyle-HorizontalAlign="right"
                                    DataFormatString="{0: ###,###,###,###,##0}"></asp:BoundColumn>
                                <%--5--%>
                                <asp:BoundColumn DataField="precio_reserva_nv" HeaderText="precio proximo" ItemStyle-HorizontalAlign="right"
                                    DataFormatString="{0: ###,###,###,##0.00}"></asp:BoundColumn>
                                <%--6--%>
                                <asp:BoundColumn DataField="cantidad_venta_nv" HeaderText="cnt vta proximo" ItemStyle-HorizontalAlign="right"
                                    DataFormatString="{0: ###,###,###,###,##0}"></asp:BoundColumn>
                                <%--7--%>
                                <asp:BoundColumn DataField="prox_ronda" Visible="false"></asp:BoundColumn>
                                <%--8--%>
                                <asp:BoundColumn DataField="precio_aprob" HeaderText="Precio Aprobado" ItemStyle-HorizontalAlign="Center">
                                </asp:BoundColumn>
                                <%--9--%>
                                <asp:TemplateColumn HeaderText="Precio" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imbprecio" runat="server" ToolTip="Definir Precio" ImageUrl="~/Images/modificar.gif" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    </asp:Content>
