﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_subasta_prm_5 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    String strRutaArchivo;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString();
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            if (!IsPostBack)
            {
                HndFechaRueda.Value = DateTime.Now.ToShortDateString();
                HndFechaRueda.Value = HndFechaRueda.Value.Substring(6, 4) + "/" + HndFechaRueda.Value.Substring(3, 2) + "/" + HndFechaRueda.Value.Substring(0, 2);
                lblFecha.Text = HndFechaRueda.Value;
                /// LLeno el Combo de la Rueda
                lConexion.Abrir();
                ddlRueda.Items.Clear();
                LlenarControles(lConexion.gObjConexion, ddlRueda, "t_rueda a ", "  a.estado = 'A' And a.codigo_tipo_subasta = 5 Order by a.descripcion", 1, 17);
                //SqlDataReader lLector;
                //lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_festivo", " fecha ='" + HndFechaRueda.Value + "' and ind_dia_habil ='S'");
                //if (lLector.HasRows)
                //    HndDiaHabil.Value = "S";
                //else
                //    HndDiaHabil.Value = "N";
                //lLector.Close();
                lConexion.Cerrar();
                lblAgente.Text = Session["nomOperador"].ToString();
            }
        }
        catch (Exception)
        {

        }
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlRueda_SelectedIndexChanged(object sender, EventArgs e)
    {
        string oArchivo = strRutaArchivo + "subasta-" + Session["numero_rueda"].ToString() + ".txt";
        string sDestino = "";
        string sMercado = "";
        Session["codigo_punto"] = "0";
        Session["punta"] = "";
        Session["codigo_modalidad"] = "0";
        Session["codigo_periodo"] = "0";


        if (ddlRueda.SelectedValue != "0")
        {
            try
            {
                Session["numero_rueda"] = ddlRueda.SelectedValue;
                //if (HndDiaHabil.Value == "S")
                btnIngresar.Visible = true;
                //else
                //  btnIngresar.Visible = false;
                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda rue, m_tipos_rueda tpo", " rue.numero_rueda=" + ddlRueda.SelectedValue + " and rue.codigo_tipo_rueda = tpo.codigo_tipo_rueda");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lblHoraNeg.Text = lLector["hora_apertura"].ToString() + " - " + lLector["hora_cierre"].ToString();
                    hndEstado.Value = lLector["estado"].ToString();
                    sDestino = lLector["destino_rueda"].ToString();
                    sMercado = lLector["tipo_mercado"].ToString();
                }
                else
                {
                    lblHoraNeg.Text = "";
                    hndEstado.Value = "F";
                    sDestino = "0";
                    sMercado = "0";
                }
                lLector.Close();
                lConexion.Cerrar();

                if (goInfo.cod_comisionista != "0" || hndEstado.Value == "F" || hndEstado.Value == "Z")
                {
                    btnSuspender.Visible = false;
                    btnReactivar.Visible = false;
                }
                else
                    if (hndEstado.Value == "S")
                    {
                        btnSuspender.Visible = false;
                        btnReactivar.Visible = true;
                    }
                    else
                    {
                        btnSuspender.Visible = true;
                        btnReactivar.Visible = false;
                    }

                Session["hora"] = File.GetCreationTime(oArchivo).ToString("HH:mm:ss");
                Session["refrescar"] = "S";
                Session["refresca_mis_posturas"] = "S";

                if (Session["tipoPerfil"].ToString() == "B")
                {
                    btnIngresar.Visible = false;
                    btnMensaje.Visible = false;
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.postura.location='frm_posturas_5.aspx';", true);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Error al Refrescar el Tablero. " + ex.Message.ToString());
                
            }
        }
        else
        {
            try
            {
                Session["numero_rueda"] = "0";
                btnIngresar.Visible = false;
                lblHoraNeg.Text = "";

                Session["hora"] = "";
                Session["refrescar"] = "S";
                Session["refresca_mis_posturas"] = "S";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.postura.location='frm_posturas_5.aspx';", true);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "La Session Expiro. " + ex.Message.ToString());
                
            }
        }
        lConexion.Abrir();
        ddlPunto.Items.Clear();
        ddlModalidad.Items.Clear();
        ddlPeriodo.Items.Clear();
        if (sDestino == "G")
        {
            if (sMercado == "S")
                LlenarControles1(lConexion.gObjConexion, ddlPunto, "m_pozo poz, m_caracteristica_sub car ", "distinct poz.codigo_pozo,poz.descripcion  ", "  poz.codigo_pozo =car.codigo_caracteristica and car.destino_rueda ='G' and car.tipo_mercado ='S' and car.codigo_tipo_subasta=5 and car.tipo_caracteristica = 'T' and poz.estado='A' and car.estado='A' ", 0, 1);
            else
                LlenarControles(lConexion.gObjConexion, ddlPunto, "m_pozo", "  estado ='A' order by descripcion", 0, 1);
            LlenarControles1(lConexion.gObjConexion, ddlModalidad, "m_modalidad_contractual mod, m_caracteristica_sub car ", "distinct mod.codigo_modalidad,mod.descripcion  ", "  mod.codigo_modalidad =car.codigo_caracteristica and car.destino_rueda ='G' and car.codigo_tipo_subasta=5 and car.tipo_caracteristica = 'M' and mod.estado='A' and car.estado='A' ", 0, 1);
            LlenarControles1(lConexion.gObjConexion, ddlPeriodo, "m_periodos_entrega per , m_caracteristica_sub car", "distinct per.codigo_periodo ,per.descripcion", "per.codigo_periodo =car.codigo_caracteristica and car.destino_rueda ='G' and car.codigo_tipo_subasta=5 and car.tipo_caracteristica = 'E' and per.estado='A' and car.estado='A'", 0, 1);
            lblPunto.Text = "Punto: ";
        }
        if (sDestino == "T")
        {
            LlenarControles(lConexion.gObjConexion, ddlPunto, "m_ruta_snt", "  estado ='A' order by descripcion", 0, 4);
            LlenarControles1(lConexion.gObjConexion, ddlModalidad, "m_modalidad_contractual mod, m_caracteristica_sub car ", "distinct mod.codigo_modalidad,mod.descripcion  ", "  mod.codigo_modalidad =car.codigo_caracteristica and car.destino_rueda ='T' and car.codigo_tipo_subasta=5 and car.tipo_caracteristica = 'M' and mod.estado='A' and car.estado='A' ", 0, 1);
            LlenarControles1(lConexion.gObjConexion, ddlPeriodo, "m_periodos_entrega per , m_caracteristica_sub car", "distinct per.codigo_periodo ,per.descripcion", "per.codigo_periodo =car.codigo_caracteristica and car.destino_rueda ='T' and car.codigo_tipo_subasta=5 and car.tipo_caracteristica = 'E' and per.estado='A' and car.estado='A'", 0, 1);
            lblPunto.Text = "Ruta: ";
        }
        lConexion.Cerrar();


    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCampos, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlCommand lComando = new SqlCommand();
        lComando.Connection = lConn;
        lComando.CommandTimeout = 3600;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_ValidarExistencia1";
        lComando.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = lsTabla;
        lComando.Parameters.Add("@P_campos", SqlDbType.VarChar).Value = lsCampos;
        lComando.Parameters.Add("@P_filtro", SqlDbType.VarChar).Value = lsCondicion;
        SqlDataReader lLector;
        lLector = lComando.ExecuteReader();

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }


    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Session["codigo_punto"] = ddlPunto.SelectedValue;
        Session["punta"] = ddlPunta.SelectedValue;
        Session["codigo_modalidad"] = ddlModalidad.SelectedValue;
        Session["codigo_periodo"] = ddlPeriodo.SelectedValue;
        Session["hora"] = "";
        Session["refrescar"] = "S";
        Session["refresca_mis_posturas"] = "S";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.postura.location='frm_posturas_5.aspx';", true);

    }

    protected void btnRestablecer_Click(object sender, EventArgs e)
    {
        ddlPunto.SelectedValue = "0";
        ddlPunta.SelectedValue = "";
        ddlModalidad.SelectedValue = "0";
        ddlPeriodo.SelectedValue = "0";
        Session["codigo_punto"] = "0";
        Session["punta"] = "";
        Session["codigo_modalidad"] = "0";
        Session["codigo_periodo"] = "0";
        Session["hora"] = "";
        Session["refrescar"] = "S";
        Session["refresca_mis_posturas"] = "S";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.postura.location='frm_posturas_5.aspx';", true);

    }
}