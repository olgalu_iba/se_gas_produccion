﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Procesos_frm_InformacionComMpUVLP : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Ingreso Información Compradores MP";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            hndFecha.Value = DateTime.Now.ToShortDateString().Substring(6, 4) + "-" + DateTime.Now.ToShortDateString().Substring(3, 2) + "-" + DateTime.Now.ToShortDateString().Substring(0, 2);
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", "estado = 'A' Order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlPunto, "m_puntos_entrega", "codigo_tipo_subasta = 3  and destino_rueda='T' and estado = 'A' Order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlPeriodo, "m_periodos_entrega ent, m_caracteristica_sub carE", "ent.codigo_periodo = carE.codigo_caracteristica and carE.tipo_caracteristica ='E' and carE.codigo_tipo_subasta = 3 and carE.estado ='A' and ent.estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlBusSubasta, "t_rueda", "codigo_tipo_subasta=3 order by fecha_rueda desc", 1, 17);
            LlenarControles(lConexion.gObjConexion, ddlBusOperador, "m_operador", "estado = 'A' Order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlBusPunto, "m_puntos_entrega", "codigo_tipo_subasta = 3  and destino_rueda='T' and estado = 'A' Order by descripcion", 0, 1);
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda", " codigo_tipo_subasta= 3 and estado <> 'F' ");
            if (lLector.HasRows)
            {
                lLector.Read();
                lblCodSubasta.Text = lLector["numero_rueda"].ToString();
                lblDescSubasta.Text = lLector["descripcion"].ToString();
                lLector.Close();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda rue, t_usuario_hab usrH", " rue.numero_rueda = " + lblCodSubasta.Text + " and rue.codigo_tipo_rueda = usrH.codigo_tipo_rueda and usrH.codigo_usuario=" + goInfo.codigo_usuario + " and usrH.estado ='A' And usrH.fecha_inicial <='" + hndFecha.Value + "' and usrH.fecha_final >= '" + hndFecha.Value + "'");
                if (lLector.HasRows)
                    hndUsrHab.Value = "S";
                else
                    hndUsrHab.Value = "N";
                lLector.Close();
            }
            else
            {
                lblCodSubasta.Text = "0";
                lblDescSubasta.Text = "0";
                hndUsrHab.Value = "N";
                lLector.Close();
            }
            lConexion.Cerrar();

            if (goInfo.cod_comisionista != "0")
            {
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
                ddlBusOperador.SelectedValue = goInfo.cod_comisionista;
                ddlBusOperador.Enabled = false;
            }
            // Carga informacion de combos
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (hndUsrHab.Value == "N")
            {
                lblMensaje.Text = "El usuario no está autorizado para el tipo de rueda";
                hlkNuevo.Enabled = false;
                hlkBuscar.Enabled = false;
                hlkListar.Enabled = false;
                tblCaptura.Visible = false;
                tblgrilla.Visible = false;
                lblTitulo.Text = "Listar " + lsTitulo;
            }
            else
            {
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador ope, m_tipos_operador tpo, m_operador_subasta opeS", " ope.codigo_operador =" + goInfo.cod_comisionista + " and ope.tipo_operador = tpo.sigla and tpo.codigo_tipo_operador = opeS.codigo_tipo_operador and opeS.punta = 'C' And opeS.codigo_tipo_subasta = 3 ");
                if (!lLector.HasRows)
                {
                    lblMensaje.Text = "El tipo de operador no está autorizado para " + lsTitulo + ". <br>";
                    hlkNuevo.Enabled = false;
                    hlkBuscar.Enabled = false;
                    hlkListar.Enabled = false;
                    tblCaptura.Visible = false;
                    tblgrilla.Visible = false;
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                }
                else
                {
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
                    {
                        lsIndica = this.Request.QueryString["lsIndica"].ToString();
                    }
                    if (lsIndica == null || lsIndica == "" || lsIndica == "L")
                    {
                        Listar();
                    }
                    else if (lsIndica == "N")
                    {
                        Nuevo();
                    }
                    else if (lsIndica == "B")
                    {
                        Buscar();
                    }
                }
            }
            if (lblMensaje.Text != "")
            {

                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";

            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "t_capac_exced");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgConsulta.Columns[6].Visible = (Boolean)permisos["UPDATE"];
        dtgConsulta.Columns[7].Visible = (Boolean)permisos["DELETE"];
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 22 de 2014
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        ddlPunto.Enabled = true;
        ddlPeriodo.Enabled = true;
        if (goInfo.cod_comisionista == "0")
            ddlOperador.Enabled = true;
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    ddlOperador.Enabled = false;
                    ddlPunto.Enabled = false;
                    ddlPeriodo.Enabled = false;
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_capac_exced", " codigo_capacidad_exc= " + modificar);
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        try
                        {
                            ddlOperador.SelectedValue = lLector["codigo_operador"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El operador del registro no existe o esta inactivo<br>";
                        }
                        try
                        {
                            ddlPunto.SelectedValue = lLector["codigo_punto_entrega"].ToString();
                            ddlPunto_SelectedIndexChanged(null, null);
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El punto de entrega del registro no existe o esta inactivo<br>";
                        }
                        try
                        {
                            ddlPeriodo.SelectedValue = lLector["codigo_periodo"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El periodo de entrega del registro no existe o esta inactivo<br>";
                        }
                        TxtMesVig.Text = lLector["meses_vigentes"].ToString();
                        TextBox oTexto;
                        foreach (DataGridItem Grilla in this.dtgDetalle.Items)
                        {
                            oTexto = (TextBox)Grilla.Cells[3].Controls[1];
                            oTexto.Text = Grilla.Cells[9].Text;
                            oTexto = (TextBox)Grilla.Cells[4].Controls[1];
                            oTexto.Text = Grilla.Cells[10].Text;
                            oTexto = (TextBox)Grilla.Cells[5].Controls[1];
                            oTexto.Text = Grilla.Cells[11].Text;
                            oTexto = (TextBox)Grilla.Cells[6].Controls[1];
                            oTexto.Text = Grilla.Cells[12].Text;
                            oTexto = (TextBox)Grilla.Cells[7].Controls[1];
                            oTexto.Text = Grilla.Cells[13].Text;
                            oTexto = (TextBox)Grilla.Cells[8].Controls[1];
                            oTexto.Text = Grilla.Cells[14].Text;
                        }

                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado. Codigo Capacidad " + modificar.ToString();

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
        if (lblMensaje.Text != "")
        {

            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";

        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador", "@P_codigo_punto_entrega" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { "0", "0", "0" };

        try
        {
            if (ddlBusSubasta.SelectedValue != "0")
                lValorParametros[0] = ddlBusSubasta.SelectedValue;
            else
                lValorParametros[0] = lblCodSubasta.Text;
            if (ddlBusOperador.SelectedValue != "0")
                lValorParametros[1] = ddlBusOperador.SelectedValue;
            if (ddlBusPunto.SelectedValue != "0")
                lValorParametros[2] = ddlBusPunto.SelectedValue;

            lConexion.Abrir();
            dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetCapacExc", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgConsulta.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
        if (lblMensaje.Text != "")
        {

            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";

        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        //Validaciones
        lblMensaje.Text = "";
        if (lblCodSubasta.Text == "0")
            lblMensaje.Text += "No hay rueda subasta seleccionada<br>";
        if (ddlOperador.SelectedValue == "0")
            lblMensaje.Text += "Debe seleccionar el operador<br>";
        if (ddlPunto.SelectedValue == "0")
            lblMensaje.Text += "Debe seleccionar el punto de entrega<br>";
        if (ddlPeriodo.SelectedValue == "0")
            lblMensaje.Text += "Debe seleccionar el periodo de entrega<br>";
        if (lblMensaje.Text == "")
            if (VerificarExistencia("t_capac_exced", " numero_rueda = " + lblCodSubasta.Text + " and codigo_operador =" + ddlOperador.SelectedValue + " and codigo_punto_entrega=" + ddlPunto.SelectedValue + " and codigo_periodo=" + ddlPeriodo.SelectedValue))
                lblMensaje.Text += "ya existe informacion para el operador-punto de entrega<br>";
        if (TxtMesVig.Text == "")
            lblMensaje.Text += "Debe digitar los meses e vigencia<br>";
        int icont = 0;
        TextBox oTexto;
        int iCapTra = 0;
        int iEnergCont = 0;
        int iEnergDemand = 0;
        foreach (DataGridItem Grilla in this.dtgDetalle.Items)
        {
            icont++;
            oTexto = (TextBox)Grilla.Cells[3].Controls[1];
            if (oTexto.Text == "")
            {
                lblMensaje.Text += "Debe digitar la cantidad de energia contratada para el tramo " + Grilla.Cells[1].Text + " - " + Grilla.Cells[2].Text + "<br>";
                iEnergCont = 0;
            }
            else
                iEnergCont = Convert.ToInt32(oTexto.Text);
            oTexto = (TextBox)Grilla.Cells[4].Controls[1];
            if (oTexto.Text == "")
            {
                lblMensaje.Text += "Debe digitar la cantidad de energia demandada para el tramo " + Grilla.Cells[1].Text + " - " + Grilla.Cells[2].Text + "<br>";
                iEnergDemand = 0;
            }
            else
                iEnergDemand = Convert.ToInt32(oTexto.Text);
            oTexto = (TextBox)Grilla.Cells[5].Controls[1];
            if (oTexto.Text == "")
            {
                lblMensaje.Text += "Debe digitar la capacidad de trasporte contratada para el tramo " + Grilla.Cells[1].Text + " - " + Grilla.Cells[2].Text + "<br>";
                iCapTra = 0;
            }
            else
                iCapTra = Convert.ToInt32(oTexto.Text);
            oTexto = (TextBox)Grilla.Cells[6].Controls[1];
            if (oTexto.Text == "")
                lblMensaje.Text += "Debe digitar los cargos fijos para el tramo " + Grilla.Cells[1].Text + " - " + Grilla.Cells[2].Text + "<br>";
            oTexto = (TextBox)Grilla.Cells[7].Controls[1];
            if (oTexto.Text == "")
                lblMensaje.Text += "Debe digitar los gastos de administración para el tramo " + Grilla.Cells[1].Text + " - " + Grilla.Cells[2].Text + "<br>";
            oTexto = (TextBox)Grilla.Cells[8].Controls[1];
            if (oTexto.Text == "")
                lblMensaje.Text += "Debe digitar los gastos variables para el tramo " + Grilla.Cells[1].Text + " - " + Grilla.Cells[2].Text + "<br>";
            if (iCapTra < iEnergCont || iCapTra < iEnergDemand)
                lblMensaje.Text += "La capacidad de trasporte debe ser mayor que la cantidad de energía contratada y la cantidad de energía demandada para el tramo " + Grilla.Cells[1].Text + " - " + Grilla.Cells[2].Text + "<br>";
        }
        if (icont == 0)
            lblMensaje.Text += "No hay detalle de tramos para la ruta seleccionada" + "<br>";

        if (lblMensaje.Text == "")
        {
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador", "@P_codigo_punto_entrega", "@P_codigo_periodo", "@P_meses_vigentes" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            String[] lValorParametros = { lblCodSubasta.Text, ddlOperador.SelectedValue, ddlPunto.SelectedValue, ddlPeriodo.SelectedValue, TxtMesVig.Text };
            SqlDataReader lLector;
            try
            {

                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetCapacExc", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    hndCodigo.Value = lLector["codigo"].ToString();
                }
                else
                    hndCodigo.Value = "0";
                lLector.Close();
                if (hndCodigo.Value != "0")
                {
                    string[] lsNombreParametros1 = { "@P_codigo_capacidad_exc", "@P_codigo_tramo", "@P_energia_contratada", "@P_energia_demandada", "@P_capac_trans_contratada", "@P_cargos_fijos", "@P_gastos_admon", "@P_cargos_variables" };
                    SqlDbType[] lTipoparametros1 = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                    String[] lValorParametros1 = { hndCodigo.Value, "0", "0", "0", "0", "0", "0", "0" };
                    foreach (DataGridItem Grilla in this.dtgDetalle.Items)
                    {
                        lValorParametros1[1] = Grilla.Cells[0].Text;
                        oTexto = (TextBox)Grilla.Cells[3].Controls[1];
                        lValorParametros1[2] = oTexto.Text.Replace(",", "");
                        oTexto = (TextBox)Grilla.Cells[4].Controls[1];
                        lValorParametros1[3] = oTexto.Text.Replace(",", "");
                        oTexto = (TextBox)Grilla.Cells[5].Controls[1];
                        lValorParametros1[4] = oTexto.Text.Replace(",", "");
                        oTexto = (TextBox)Grilla.Cells[6].Controls[1];
                        lValorParametros1[5] = oTexto.Text.Replace(",", "");
                        oTexto = (TextBox)Grilla.Cells[7].Controls[1];
                        lValorParametros1[6] = oTexto.Text.Replace(",", "");
                        oTexto = (TextBox)Grilla.Cells[8].Controls[1];
                        lValorParametros1[7] = oTexto.Text.Replace(",", "");
                        DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetCapacExcTramo", lsNombreParametros1, lTipoparametros1, lValorParametros1);
                    }
                }
                lConexion.Cerrar();
                Listar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
                lConexion.Cerrar();
            }
            
        }
        if (lblMensaje.Text != "")
        {

            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";

        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        if (lblCodSubasta.Text == "0")
            lblMensaje.Text += "No hay rueda subasta sleeccionada" + "<br>";
        if (ddlOperador.SelectedValue == "0")
            lblMensaje.Text += "Debe seleccioanr el operador" + "<br>";
        if (ddlPunto.SelectedValue == "0")
            lblMensaje.Text += "Debe seleccioanr el punto de entrega" + "<br>";
        if (ddlPeriodo.SelectedValue == "0")
            lblMensaje.Text += "Debe seleccioanr el periodod de entrega" + "<br>";
        if (TxtMesVig.Text == "")
            lblMensaje.Text += "Debe digitar los meses de vigencia" + "<br>";
        int icont = 0;
        TextBox oTexto;
        int iCapTra = 0;
        int iEnergCont = 0;
        int iEnergDemand = 0;
        foreach (DataGridItem Grilla in this.dtgDetalle.Items)
        {
            icont++;
            oTexto = (TextBox)Grilla.Cells[3].Controls[1];
            if (oTexto.Text == "")
            {
                lblMensaje.Text += "Debe digitar la cantidad de energia contratada para el tramo " + Grilla.Cells[1].Text + " - " + Grilla.Cells[2].Text + "<br>";
                iEnergCont = 0;
            }
            else
                iEnergCont = Convert.ToInt32(oTexto.Text);

            oTexto = (TextBox)Grilla.Cells[4].Controls[1];
            if (oTexto.Text == "")
            {
                lblMensaje.Text += "Debe digitar la cantidad de energia demandada para el tramo " + Grilla.Cells[1].Text + " - " + Grilla.Cells[2].Text + "<br>";
                iEnergDemand = 0;
            }
            else
                iEnergDemand = Convert.ToInt32(oTexto.Text);

            oTexto = (TextBox)Grilla.Cells[5].Controls[1];
            if (oTexto.Text == "")
            {
                lblMensaje.Text += "Debe digitar la capacidad de trasporte contratada para el tramo " + Grilla.Cells[1].Text + " - " + Grilla.Cells[2].Text + "<br>";
                iCapTra = 0;
            }
            else
                iCapTra = Convert.ToInt32(oTexto.Text);

            oTexto = (TextBox)Grilla.Cells[6].Controls[1];
            if (oTexto.Text == "")
                lblMensaje.Text += "Debe digitar los cargos fijos para el tramo " + Grilla.Cells[1].Text + " - " + Grilla.Cells[2].Text + "<br>";
            oTexto = (TextBox)Grilla.Cells[7].Controls[1];
            if (oTexto.Text == "")
                lblMensaje.Text += "Debe digitar los gastos de administración para el tramo " + Grilla.Cells[1].Text + " - " + Grilla.Cells[2].Text + "<br>";
            oTexto = (TextBox)Grilla.Cells[8].Controls[1];
            if (oTexto.Text == "")
                lblMensaje.Text += "Debe digitar los gastos variables para el tramo " + Grilla.Cells[1].Text + " - " + Grilla.Cells[2].Text + "<br>";
            if (iCapTra < iEnergCont || iCapTra < iEnergDemand)
                lblMensaje.Text += "La capacidad de trasporte debe ser mayor que la cantidad de energía contratada y la cantidad de energía demandada para el tramo " + Grilla.Cells[1].Text + " - " + Grilla.Cells[2].Text + "<br>";

        }
        if (icont == 0)
            lblMensaje.Text += "No hay detalle de tramos para la ruta seleccionada" + "<br>";

        if (lblMensaje.Text == "")
        {
            string[] lsNombreParametros = { "@P_codigo_capacidad_exc", "@P_meses_vigentes" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            String[] lValorParametros = { hndCodigo.Value, TxtMesVig.Text };
            try
            {

                lConexion.Abrir();
                DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_UptCapacExc", lsNombreParametros, lTipoparametros, lValorParametros);
                string[] lsNombreParametros1 = { "@P_codigo_capacidad_exc", "@P_codigo_tramo", "@P_energia_contratada", "@P_energia_demandada", "@P_capac_trans_contratada", "@P_cargos_fijos", "@P_gastos_admon", "@P_cargos_variables" };
                SqlDbType[] lTipoparametros1 = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                String[] lValorParametros1 = { hndCodigo.Value, "0", "0", "0", "0", "0", "0", "0" };
                foreach (DataGridItem Grilla in this.dtgDetalle.Items)
                {
                    lValorParametros1[1] = Grilla.Cells[0].Text;
                    oTexto = (TextBox)Grilla.Cells[3].Controls[1];
                    lValorParametros1[2] = oTexto.Text.Replace(",", "");
                    oTexto = (TextBox)Grilla.Cells[4].Controls[1];
                    lValorParametros1[3] = oTexto.Text.Replace(",", "");
                    oTexto = (TextBox)Grilla.Cells[5].Controls[1];
                    lValorParametros1[4] = oTexto.Text.Replace(",", "");
                    oTexto = (TextBox)Grilla.Cells[6].Controls[1];
                    lValorParametros1[5] = oTexto.Text.Replace(",", "");
                    oTexto = (TextBox)Grilla.Cells[7].Controls[1];
                    lValorParametros1[6] = oTexto.Text.Replace(",", "");
                    oTexto = (TextBox)Grilla.Cells[8].Controls[1];
                    lValorParametros1[7] = oTexto.Text.Replace(",", "");
                    DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetCapacExcTramo", lsNombreParametros1, lTipoparametros1, lValorParametros1);
                }
                lConexion.Cerrar();
                manejo_bloqueo("E", hndCodigo.Value);
                Listar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
                lConexion.Cerrar();
            }
        }
        if (lblMensaje.Text != "")
        {

            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";

        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (hndCodigo.Value != "")
            manejo_bloqueo("E", hndCodigo.Value);
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgConsulta.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            hndCodigo.Value = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text;
            Modificar(hndCodigo.Value);
        }
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            //if (!VerificarExistencia("t_rueda_usuario_hab", " codigo_rueda_usuario= " + this.dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text + " and estado ='C'"))
            //    lblMensaje.Text += "No se puede eliminar el registro porque ya fue aprobado<br>";
            //if (lblMensaje.Text == "")
            //{
            string[] lsNombreParametros = { "@P_codigo_capacidad_exc" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int };
            String[] lValorParametros = { this.dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text };
            try
            {
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_DelCapacExc", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Eliminación del  registro de información.!";
                    lConexion.Cerrar();
                }
                else
                    Listar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Se presento un Problema en la Eliminación del  registro de información.!";
            }
        }
        if (lblMensaje.Text != "")
        {

            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";

        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lstable, string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lstable, lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='t_capac_exced' and llave_registro='codigo_capacidad_exc=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_capacidad_exc=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "t_capac_exced";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_capac_exced", lsCondicion1);
        }
        return true;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlPunto_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPunto.SelectedValue != "0" && lblCodSubasta.Text != "0" && ddlOperador.SelectedValue != "0")
        {
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador", "@P_codigo_punto_entrega", "@P_codigo_capacidad_exc" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { lblCodSubasta.Text, ddlOperador.SelectedValue, ddlPunto.SelectedValue, hndCodigo.Value };

            try
            {
                lConexion.Abrir();
                dtgDetalle.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetCapacExcTramo", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgDetalle.DataBind();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }

        }
        if (lblMensaje.Text != "")
        {

            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";

        }

    }
}