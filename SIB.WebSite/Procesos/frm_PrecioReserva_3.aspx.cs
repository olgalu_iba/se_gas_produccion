﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_PrecioReserva_3 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    String strRutaArchivo;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
        strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString() + "subasta-" + Session["numero_rueda"].ToString() + ".txt";
        if (!IsPostBack)
        {
            cargarDatos();
        }
    }
    private void cargarDatos()
    {
        if (Session["numero_rueda"].ToString() == "0")
        {
            dtgPrecios.Columns.Clear();
        }
        else
        {
            try
            {
                //determina las columnas para el subastatodr o los operaores
                string[] lsNombreParametros = { "@P_numero_rueda" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int };
                string[] lValorParametros = { Session["numero_rueda"].ToString() };
                lConexion.Abrir();
                dtgPrecios.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetPrecioReserva3", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgPrecios.DataBind();
                lConexion.Cerrar();
                ImageButton imbControl;
                foreach (DataGridItem Grilla in this.dtgPrecios.Items)
                {
                    imbControl = (ImageButton)Grilla.Cells[9].Controls[1];
                    imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_definirPrecio_3.aspx?ID=" + Grilla.Cells[1].Text + "&ronda=" + Grilla.Cells[7].Text + "&precio_reserva=" + Grilla.Cells[3].Text + "','','width=400,height=300,left=350,top=150,status=no,location=0,menubar=no,toolbar=no,resizable=yes,scrollbars=yes');");
                }
            }
            catch (Exception ex)
            {

                Toastr.Warning(this, ex.Message);
             
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ChkTodos_CheckedChanged(object sender, EventArgs e)
    {
        //Selecciona o deselecciona todos los items del datagrid segun el checked del control
        CheckBox chk = (CheckBox)(sender);
        foreach (DataGridItem Grilla in this.dtgPrecios.Items)
        {
            CheckBox Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
            Checkbox.Checked = chk.Checked;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAprobar_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_numero_id", "@P_ronda" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { "", ""     };

        CheckBox chk;
        foreach (DataGridItem Grilla in this.dtgPrecios.Items)
        {
            chk = (CheckBox)Grilla.Cells[0].Controls[1];
            if (chk.Checked)
            {
                lValorParametros[0] = Grilla.Cells[1].Text;
                lValorParametros[1] = Grilla.Cells[7].Text;
                lConexion.Abrir();
                DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetAprobPrecio3", lsNombreParametros, lTipoparametros, lValorParametros);
                lConexion.Cerrar();
            }
        }
        cargarDatos();
        File.SetCreationTime(strRutaArchivo, DateTime.Now);
    }
}
