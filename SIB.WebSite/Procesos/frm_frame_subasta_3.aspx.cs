﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Procesos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frm_frame_subasta_3 : Page
    {
        #region Propiedades

        private InfoSessionVO _goInfo;
        private clConexion _lConexion;
        private string _strRutaArchivo;
        private static string _cadenaCon;

        /// <summary>
        /// 
        /// </summary>
        public string CodSubasta
        {
            get { return ViewState["CodSubasta"] != null && !string.IsNullOrEmpty(ViewState["CodSubasta"].ToString()) ? ViewState["CodSubasta"].ToString() : "0"; }
            set { ViewState["CodSubasta"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CodRuta
        {
            get { return ViewState["CodRuta"] != null && !string.IsNullOrEmpty(ViewState["CodRuta"].ToString()) ? ViewState["CodRuta"].ToString() : "0"; }
            set { ViewState["CodRuta"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string EstadoSubasta
        {
            get { return ViewState["EstadoSubasta"] != null && !string.IsNullOrEmpty(ViewState["EstadoSubasta"].ToString()) ? ViewState["EstadoSubasta"].ToString() : "C"; }
            set { ViewState["EstadoSubasta"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Hora
        {
            get { return ViewState["hora"] != null && !string.IsNullOrEmpty(ViewState["hora"].ToString()) ? ViewState["hora"].ToString() : ""; }
            set { ViewState["hora"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string MisOfertas
        {
            get { return ViewState["mis_ofertas"] != null && !string.IsNullOrEmpty(ViewState["mis_ofertas"].ToString()) ? ViewState["mis_ofertas"].ToString() : "N"; }
            set { ViewState["mis_ofertas"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Refrescar
        {
            get { return ViewState["refrescar"] != null && !string.IsNullOrEmpty(ViewState["refrescar"].ToString()) ? ViewState["refrescar"].ToString() : "N"; }
            set { ViewState["refrescar"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string IndTipoRueda
        {
            get { return ViewState["ind_tipo_rueda"] != null && !string.IsNullOrEmpty(ViewState["ind_tipo_rueda"].ToString()) ? ViewState["ind_tipo_rueda"].ToString() : "P"; }
            set { ViewState["ind_tipo_rueda"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HoraMensaje
        {
            get { return ViewState["hora_mensaje"] != null && !string.IsNullOrEmpty(ViewState["hora_mensaje"].ToString()) ? ViewState["hora_mensaje"].ToString() : ""; }
            set { ViewState["hora_mensaje"] = value; }
        }

        #endregion Propiedades

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Se cargan los valores iniciales del formulario 
                CargarPagina();

                if (IsPostBack) return;
                //Se inicializan los controles para la primera ves que se ejecuta el formulario 
                InicializarPagina();
                // Se realiza el cargue de la rueda
                CargarRueda();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void CargarPagina()
        {
            //Se redirecciona al Login si no hay una sesión iniciada    
            _goInfo = (InfoSessionVO)Session["infoSession"];
            if (_goInfo == null) Response.Redirect("~/login.aspx");
            _strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"];

            //Variables
            Session["destino_rueda"] = "T";
            Session["refrescar_tit"] = "N";
            Session["refrescar_prm"] = "N";
            Session["refrescar_cont"] = "S";
            Session["ofertas"] = "N";

            //Eventos
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttonsSubasta.PuntoOnSelectedIndexChanged += ddlPunto_SelectedIndexChanged;
            buttonsSubastaExtender.SuspenderOnclick += Suspender_Click;
            buttonsSubastaExtender.ReactivarOnclick += Reactivar_Click;
            buttonsSubastaExtender.ContratoOnclick += AbrirContratos;
            buttonsSubastaExtender.EnviarMensajeOnclick += OpenEnviarMensaje;
            buttonsSubastaExtender.MisMensajesOnclick += AbrirMensajes;

            estados.TimeServer.Value = DateTime.Now.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Inicializa el contenido del formulario 
        /// </summary>
        private void InicializarPagina()
        {
            //Titulo
            Master.Titulo = "Subasta";
            //Descripcion
            Master.DescripcionPagina = "Úselo o véndalo a largo plazo";

            //Se cargan los nombres de las pestañas 
            SeleccionarNombrePestanias();
        }

        /// <summary>
        /// 
        /// </summary>
        private void CargarRueda()
        {
            //Lista para la rueda 
            var ddlRueda = new DropDownList();
            //Lista para el punto o ruta
            var ddlNueva = new DropDownList();

            //Se crea la conexión  
            _lConexion = new clConexion(_goInfo);
            _lConexion.Abrir();
            SqlDataReader lLector = DelegadaBase.Servicios.LlenarControl(_lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda", " codigo_tipo_subasta =3 and fecha_rueda = convert(varchar(10),getdate(),111) order by numero_rueda desc ");
            if (lLector.HasRows)
            {
                lLector.Read();
                CodSubasta = lLector["numero_rueda"].ToString();
                IndTipoRueda = lLector["ind_tipo_rueda"].ToString();
                EstadoSubasta = lLector["estado"].ToString();
            }
            lLector.Close();
            lLector.Dispose();
            _lConexion.Cerrar();

            //Se habilita la selección de la rueda   
            buttonsSubasta.EditarRueda = true;

            //Se agrega el primer item para la ruta o tramo
            if (IndTipoRueda == null) return;
            ListItem lItem = IndTipoRueda == "P"
                ? new ListItem { Value = "0", Text = "Seleccione Ruta" }
                : new ListItem { Value = "0", Text = "Seleccione Tramo" };
            ddlNueva.Items.Add(lItem);
            buttonsSubasta.EditarPunto = true;

            //
            _lConexion = new clConexion(_goInfo);
            _lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(_lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda rue,m_estado_gas est", " rue.numero_rueda =" + CodSubasta + " and est.tipo_estado ='R' and rue.estado = est.sigla_estado");
            if (lLector.HasRows)
            {
                lLector.Read();

                /*Se deshabilitan los estados y cronómetros*/
                estados.Visible = true;

                estados.Pestania2.CssClass = "nav-link disabled";
                estados.Pestania3.CssClass = "nav-link disabled";
                estados.Pestania4.CssClass = "nav-link disabled";
                estados.Pestania6.CssClass = "nav-link disabled";

                estados.Pestania2.Visible = true;
                estados.Pestania3.Visible = true;
                estados.Pestania4.Visible = true;
                estados.Pestania6.Visible = true;

                //lblHora1.Visible = false;
                estados.Hora2.Visible = true; //20220418
                estados.Hora3.Visible = true;//20220418
                estados.Hora4.Visible = true;//20220418

                estados.Hora2.Text = $"{lLector["hora_ini_publi_v_sci"]} - {lLector["hora_fin_publi_v_sci"]}";
                estados.Hora3.Text = $"{lLector["hora_ini_rec_solicitud_c_sci"]} - {lLector["hora_fin_rec_solicitud_c_sci"]}";
                estados.Hora4.Text = $"{ lLector["hora_ini_negociacioni_sci"]} - {lLector["hora_fin_negociacioni_sci"]}";

                //Tiempo espera
                divTiempoEspera.Visible = false;

                //Se carga la rueda 
                var lItemRueda = new ListItem { Value = "1", Text = lLector["descripcion"].ToString() };
                ddlRueda.Items.Add(lItemRueda);

                //Se seleccionan los botones    
                buttons.SwitchOnButton(EnumBotones.Ninguno);
                buttonsSubasta.SwitchOnButton(EnumBotonesSubasta.Rueda, EnumBotonesSubasta.Punto);
                buttonsSubastaExtender.SwitchOnButton(EnumBotonesSubasta.EnviarMensaje, EnumBotonesSubasta.MisMensajes, EnumBotonesSubasta.Contrato, EnumBotonesSubasta.Suspender);

                //Tiempo espera
                divTiempoEspera.Visible = false;
                estados.Time.Value = DateTime.Now.ToShortTimeString();

                switch (lLector["estado"].ToString())
                {
                    case "C":
                        estados.Time.Value = lLector["hora_ini_publi_v_sci"].ToString();
                        break;
                    case "3":
                        estados.Pestania2.CssClass = "nav-link active";
                        estados.Time.Value = lLector["hora_fin_publi_v_sci"].ToString();        
                        //estados.Hora2.Visible = true;
                        estados.Pestania2.Visible = true;
                        buttons.SwitchOnButton(EnumBotones.Excel);
                        break;
                    case "4":
                        estados.Pestania3.CssClass = "nav-link active";
                        estados.Time.Value = lLector["hora_fin_rec_solicitud_c_sci"].ToString();
                        //estados.Hora3.Visible = true;
                        estados.Pestania3.Visible = true;
                        break;
                    case "5":
                        estados.Pestania4.CssClass = "nav-link active";
                        estados.Time.Value = lLector["hora_fin_negociacioni_sci"].ToString();
                        //estados.Hora4.Visible = true;
                        estados.Pestania4.Visible = true;
                        break;
                    //case "T":
                    //    lkbPestania5.CssClass = "nav-link active";
                    //    lblHora5.Text = lLector["hora_prox_fase"].ToString() == lLector["hora_ini_rec_solicitud_c_sci"].ToString()
                    //        ? $"{lLector["hora_fin_publi_v_sci"] } - {lLector["hora_prox_fase"]}"
                    //        : $"{lLector["hora_fin_rec_solicitud_c_sci"]} - {lLector["hora_prox_fase"]}";
                    //    lblHora5.Visible = true;
                    //    break;
                    case "F":
                        estados.Time.Value = "23:59:59";
                        estados.Pestania6.CssClass = "nav-link active";
                        break;
                    //case "S":
                    //    lkbPestania7.CssClass = "nav-link active";
                    //    lblHora7.Text = $"{lLector["hora_prox_fase"]}";
                    //    lblHora7.Visible = true;
                    //    buttonsSubasta.SwitchOnButton(EnumBotonesSubasta.Rueda, EnumBotonesSubasta.Punto);
                    //    buttonsSubastaExtender.SwitchOnButton(EnumBotonesSubasta.EnviarMensaje, EnumBotonesSubasta.MisMensajes, EnumBotonesSubasta.Contrato, EnumBotonesSubasta.Reactivar);
                    //    break;
                    //case "Z":
                    //    lkbPestania8.CssClass = "nav-link active";
                    //    break;
                    default:
                        dtgSubasta.Columns.Clear();
                        divTiempoEspera.Visible = true;
                        break;
                }
                EstadoSubasta = lLector["estado"].ToString();
            }
            _lConexion.Cerrar();

            //Se carga la subasta
            buttonsSubasta.ListaRueda = ddlRueda.Items;
            if (buttonsSubasta.ListaRueda.Cast<ListItem>().ToArray().Length >= 2)
            {
                buttonsSubasta.ValorRueda = "1";
                buttonsSubasta.EditarRueda = false;
            }

            //Se carga el punto o tramo
            _lConexion = new clConexion(_goInfo);
            _lConexion.Abrir();
            ddlNueva.Items.AddRange(LlenarControles(_lConexion.gObjConexion, "m_ruta_snt", "  estado <> 'I' order by descripcion", 0, 4).Cast<ListItem>().ToArray());
            buttonsSubasta.ListaPunto = ddlNueva.Items;
            if (buttonsSubasta.ListaPunto.Cast<ListItem>().ToArray().Length <= 1)
                buttonsSubasta.EditarPunto = false;
            buttonsSubasta.ValorPunto = "0";

            //Se establecen las columnas que se van a mostrar en la grilla de la subasta (Anteriormente se refrescaban los títulos )
            //if (!divTiempoEspera.Visible)   
            //    dtgSubasta.Columns[4].HeaderText = IndTipoRueda != null && IndTipoRueda == "P" ? "Ruta" : "Tramo";
            
            if (!divTiempoEspera.Visible) //20200924 ajsute compoenente
            {
                dtgSubasta.Columns[4].HeaderText = IndTipoRueda != null && IndTipoRueda == "P" ? "Ruta" : "Tramo";


                if (_goInfo.cod_comisionista == "0")
                {
                    dtgSubasta.Columns[9].Visible = false; //ofertar
                    dtgSubasta.Columns[10].Visible = false; //modificar
                    dtgSubasta.Columns[11].Visible = false;// eliminar
                    buttonsSubastaExtender.SwitchOnButton(EnumBotonesSubasta.EnviarMensaje, EnumBotonesSubasta.MisMensajes, EnumBotonesSubasta.Contrato);
                }
                else
                {
                    buttonsSubasta.SwitchOnButton(EnumBotonesSubasta.Rueda, EnumBotonesSubasta.Punto);
                    buttonsSubastaExtender.SwitchOnButton(EnumBotonesSubasta.MisMensajes, EnumBotonesSubasta.Contrato);
                }

                dtgSubasta.Columns[12].Visible = EstadoSubasta != null && (EstadoSubasta == "5" || EstadoSubasta == "F");

                //Se actualizan las posturas
                CargarDatosGrilla();

            }
        }

        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lConn"></param>
        protected IEnumerable LlenarControles(SqlConnection lConn, string lsTabla, string lsCondicion, int liIndiceLlave, int liIndiceDescripcion)
        {
            var dummy = new DropDownList();
            // Proceso para Cargar el DDL de ciudad
            var lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                dummy.Items.Add(lItem1);
            }
            lLector.Close();
            return dummy.Items;
        }

        /// <summary>
        /// Nombre: cargarDatosGrilla
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para Cargar los datos en la Grio.
        /// Modificacion:
        /// </summary>
        private void CargarDatosGrilla(bool primeraCarga = false)
        {
            if (CodSubasta.Equals("0") && !primeraCarga || divTiempoEspera.Visible)
            {
                dtgSubasta.Columns.Clear();
                return;
            }

            //determina las columnas para el subastatodr o los operaores
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador", "@P_codigo_ruta" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };

            string[] primeraBusqueda = { "0", "0", "0" };
            string[] busquedasGenerales = { CodSubasta, _goInfo.cod_comisionista, CodRuta };
            string[] lValorParametros = primeraCarga ? primeraBusqueda : busquedasGenerales;

            //Se crea la conexión  
            _lConexion = new clConexion(_goInfo);
            _lConexion.Abrir();
            //Se realiza la consulta de la subasta
            dtgSubasta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(_lConexion.gObjConexion, "pa_GetRuedaPuja3", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgSubasta.DataBind();
            _lConexion.Cerrar();

            //Se cargan los elementos de la grilla 
            if (EstadoSubasta == "5" || EstadoSubasta == "F")
            {
                dtgSubasta.Columns[12].Visible = true;
            }
            else
                dtgSubasta.Columns[12].Visible = false;

            _lConexion.Cerrar();

            if (_goInfo.cod_comisionista == "0")
            {
                dtgSubasta.Columns[9].Visible = false;
                dtgSubasta.Columns[10].Visible = false;
                dtgSubasta.Columns[11].Visible = false;
            }
            else
            {
                if (EstadoSubasta == "4")
                {
                    foreach (DataGridItem Grilla in dtgSubasta.Items)
                    {
                        if (Grilla.Cells[15].Text == "S")
                        {
                            Grilla.Cells[9].Controls[1].Visible = false;
                            Grilla.Cells[10].Controls[1].Visible = false;
                            Grilla.Cells[11].Controls[1].Visible = false;
                        }
                        else
                        {
                            if (Grilla.Cells[13].Text == "N")
                            {
                                Grilla.Cells[9].Controls[1].Visible = true;
                                Grilla.Cells[10].Controls[1].Visible = false;
                                Grilla.Cells[11].Controls[1].Visible = false;
                            }
                            else
                            {
                                Grilla.Cells[9].Controls[1].Visible = false;
                                Grilla.Cells[10].Controls[1].Visible = true;
                                Grilla.Cells[11].Controls[1].Visible = true;
                            }
                        }
                    }
                }
                else
                {
                    foreach (DataGridItem Grilla in dtgSubasta.Items)
                    {
                        Grilla.Cells[9].Controls[1].Visible = false;
                        Grilla.Cells[10].Controls[1].Visible = false;
                        Grilla.Cells[11].Controls[1].Visible = false;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void Timer_Tick(object sender, EventArgs e)
        {
            try
            {
                //Se actualiza el relog
                //lblRelog.Text = $"Hora: {DateTime.Now.ToString("HH:mm:ss").Substring(0, 8)}";

                var oArchivo = _strRutaArchivo + "subasta-" + CodSubasta + ".txt";
                if (Hora != File.GetCreationTime(oArchivo).ToString("HH:mm:ss"))
                {
                    Hora = File.GetCreationTime(oArchivo).ToString("HH:mm:ss");
                    MisOfertas = "S";
                    //Se recarga la rueda
                    CargarRueda();
                }
                else
                {
                    if (Refrescar == "S")
                    {
                        Refrescar = "N"; //20170929 rq048-17
                        //Se recargan las posturas
                        CargarDatosGrilla();
                    }
                }

                oArchivo = _strRutaArchivo + "Mensaje.txt";
                if (HoraMensaje != File.GetCreationTime(oArchivo).ToString("HH:mm:ss"))
                {
                    HoraMensaje = File.GetCreationTime(oArchivo).ToString("HH:mm:ss");
                }
                else
                {
                    if (MisOfertas == "S")
                        MisOfertas = "N";
                }
            }
            catch (Exception ex)
            {

                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPunto_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CodRuta = buttonsSubasta.ValorPunto;
                Refrescar = "S";
                Session["refresca_mis_posturas"] = "S";
                //Se actualizan las posturas
                CargarDatosGrilla();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// 20160128
        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            try
            {
                string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador", "@P_codigo_ruta" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { CodSubasta, _goInfo.cod_comisionista, CodRuta };
                _lConexion = new clConexion(_goInfo);
                _lConexion.Abrir();
                dtgSubastaImpresion.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(_lConexion.gObjConexion, "pa_GetRuedaPuja3", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgSubastaImpresion.DataBind();
                dtgSubastaImpresion.Visible = true;

                DataSet lds = new DataSet();
                SqlDataAdapter lsqldata = new SqlDataAdapter();
                string lsNombreArchivo = _goInfo.Usuario + "InfRueda" + DateTime.Now + ".xls";
                string lstitulo_informe = "Consulta Ids Subasta " + CodSubasta;
                decimal ldCapacidad = 0;
                StringBuilder lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                dtgSubastaImpresion.EnableViewState = false;
                lform.Controls.Add(dtgSubastaImpresion);
                lpagina.RenderControl(lhtw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + _goInfo.nombre + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());
                Response.End();
                lds.Dispose();
                lsqldata.Dispose();
                _lConexion.CerrarInforme();
                dtgSubastaImpresion.Visible = false;
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnItemCommand_Click(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                switch (((ImageButton)e.CommandSource).ID)
                {
                    // Muestra la oferta
                    case "imbOfertar":
                        OpenIngresoPosturaCompra(e.Item.Cells[0].Text, "C", e.Item.Cells[14].Text);
                        break;
                    //Muetra la modificacion
                    case "imbModificar":
                        OpenIngresoPosturaCompra(e.Item.Cells[0].Text, "M", e.Item.Cells[14].Text);
                        break;
                    // Muestra la grafica
                    case "imbGrafica":
                        OpenCurvaOferDemAgre(e.Item.Cells[0].Text);
                        break;
                    case "imbEliminar":
                        {
                            string[] lsNombreParametros = { "@P_numero_id", "@P_codigo_operador" };
                            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                            string[] lValorParametros = { e.Item.Cells[0].Text, _goInfo.cod_comisionista };
                            SqlDataReader lLector;
                            _lConexion = new clConexion(_goInfo);
                            _lConexion.Abrir();
                            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(_lConexion.gObjConexion, "pa_DelPostura3", lsNombreParametros, lTipoparametros, lValorParametros);
                            if (lLector.HasRows)
                            {
                                while (lLector.Read())
                                {
                                    Toastr.Error(this, $"{lLector["error"]}");
                                }
                                lLector.Close();
                                lLector.Dispose();
                                _lConexion.Cerrar();
                            }
                            else
                            {
                                Toastr.Success(this, "Oferta eliminada correctamente");
                                lLector.Close();
                                lLector.Dispose();
                                _lConexion.Cerrar();
                                CargarDatosGrilla();
                            }

                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "Error al eliminar la oferta. " + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void SeleccionarNombrePestanias()
        {
            //Estado
            estados.Pestania1.Text = "<b>1. Creada</b>";
            estados.Pestania2.Text = "<b>1. Publicación</b> de capacidad disponible";
            estados.Pestania3.Text = "<b>2. Ingreso</b> posturas de compra";
            estados.Pestania4.Text = "<b>3. Calce</b> de operaciones";
            estados.Pestania5.Text = "<b>5. En</b> espera";
            estados.Pestania6.Text = "<b>4. Finalizada</b>";
            estados.Pestania7.Text = "<b>7. Suspendida</b>";
            estados.Pestania8.Text = "<b>8. Suspendida</b> Definitivamente";
            //Descripción 
            estados.Descripcion1.InnerText =
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's " +
                "standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make " +
                "a type specimen book. It has survived not only five centuriesm.";
            estados.Descripcion2.InnerText =
                "El Gestor del Mercado publica la capacidad excedentaria disponible para cada una de las rutas";
            estados.Descripcion3.InnerText =
                "Los compradores, que estén interesados en contratar la capacidad ofrecida en la subasta, en esta fase envían sus solicitudes de compra al Gestor del Mercado";
            estados.Descripcion4.InnerText =
                "El Gestor del Mercado en esta fase aplica, para cada ruta, el mecanismo de subasta descrito en el Anexo 6 de la Resolución CREG 114 de 2017";
            estados.Descripcion5.InnerText = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's " +
                "standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make " +
                "a type specimen book. It has survived not only five centuriesm.";
            estados.Descripcion6.InnerText =
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's " +
                "standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make " +
                "a type specimen book. It has survived not only five centuriesm.";
            estados.Descripcion7.InnerText =
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's " +
                "standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make " +
                "a type specimen book. It has survived not only five centuriesm.";
            estados.Descripcion8.InnerText =
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's " +
                "standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make " +
                "a type specimen book. It has survived not only five centuriesm.";
        }

        #region Modals

        #region Suspender

        /// <summary>
        /// Se encarga de inicializar el Modal para suspender una rueda 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Suspender_Click(object sender, EventArgs e)
        {
            SqlDataReader lLector = null;

            try
            {
                // Obtengo los Datos de la rueda
                _lConexion = new clConexion(_goInfo);
                _lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(_lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda", " numero_rueda= " + CodSubasta + " and estado not in ('F','Z')");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    // Lleno las etiquetas de la pantalla con los datos del Id.
                    lblFechaAct.Text = "Fecha Act: " + lLector["fecha_prox_apertura"];
                    lLector.Close();
                    lLector.Dispose();

                    Modal.Abrir(this, mdlSuspender.ID, mdlInsideSuspender.ID);
                }
                else
                {
                    //Se debe agregar la notificación
                    Toastr.Error(this, "La rueda ya está finalizada.!");
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
            finally
            {
                if (lLector != null)
                {
                    lLector.Dispose();
                    lLector.Close();
                    _lConexion.Cerrar();
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSuspender_Click(object sender, EventArgs e)
        {
            SqlDataReader lLector;

            try
            {
                string strRutaArchivoModal = ConfigurationManager.AppSettings["RutaArchivos"] + "mensaje.txt";
                string strRutaArchivoModal2 = ConfigurationManager.AppSettings["RutaArchivos"] + "subasta-" + CodSubasta + ".txt";

                string oError = "";
                string sMensaje = "";

                //if (TxtHora.Text == "")
                //    oError += "Debe digitar la Hora de inicio de la próxima ronda\\n";
                //if (TxtMinutos.Text == "")
                //    oError += "Debe la duración de la próxima ronda\\n";
                if (ddlDefinitivo.SelectedValue == "N")
                {
                    if (TxtFecha.Text == "")
                        oError += "Debe digitar la próxima fecha de apertura\\n";
                }
                else
                {
                    if (TxtFecha.Text != "")
                        oError += "NO Debe digitar la próxima fecha de apertura para suspensión definitiva\\n";
                }
                if (TxtObservacion.Text == "")
                    oError += "Debe digitar las observaciones de suspensión\\n";

                if (oError == "")
                {
                    string[] lsNombreParametros = { "@P_numero_rueda", "@P_fecha", "@P_ind_definitivo", "@P_observaciones" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
                    string[] lValorParametros = { CodSubasta, TxtFecha.Text, ddlDefinitivo.SelectedValue, TxtObservacion.Text };

                    _lConexion = new clConexion(_goInfo);
                    _lConexion.Abrir();
                    try
                    {
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(_lConexion.gObjConexion, "pa_setSuspender3", lsNombreParametros, lTipoparametros, lValorParametros, _goInfo);
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                                oError = lLector["error"] + "\\n";
                        }
                        if (oError == "")
                        {
                            if (ddlDefinitivo.SelectedValue == "N")
                            {
                                sMensaje = "La rueda se suspendió para reactivarse el día " + TxtFecha.Text;
                            }
                            else
                                sMensaje = "La rueda se suspendió definitivamente";
                            File.SetCreationTime(strRutaArchivoModal, DateTime.Now);
                            File.SetCreationTime(strRutaArchivoModal2, DateTime.Now);
                            Toastr.Success(this, sMensaje);
                            HoraMensaje = "";

                            CloseSuspender_Click(null, null);

                        }
                        _lConexion.Cerrar();
                    }
                    catch (Exception ex)
                    {
                        oError = "Error al suspender la rueda. " + ex.Message;
                        _lConexion.Cerrar();
                    }
                }
                if (oError != "")
                {
                    Toastr.Error(this, oError);
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseSuspender_Click(Object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Declaración de Información Archivo
                Modal.Cerrar(this, mdlSuspender.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Suspender

        #region Reactivar

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Reactivar_Click(object sender, EventArgs e)
        {
            try
            {
                /// Obtengo los Datos de la rueda
                _lConexion = new clConexion(_goInfo);
                _lConexion.Abrir();
                SqlDataReader lLector = DelegadaBase.Servicios.LlenarControl(_lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda rue", " rue.numero_rueda = " + CodSubasta);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    /// Lleno las etiquetas de la pantalla con los datos del Id.
                    hndFase.Value = lLector["fase_reactivacion"].ToString();
                    TxtHoraIniPubUvlp.Text = lLector["hora_ini_publi_v_sci"].ToString();
                    TxtHoraFinPubUvlp.Text = lLector["hora_fin_publi_v_sci"].ToString();
                    TxtHoraIniCompUvlp.Text = lLector["hora_ini_rec_solicitud_c_sci"].ToString();
                    TxtHoraFinCompUvlp.Text = lLector["hora_fin_rec_solicitud_c_sci"].ToString();
                    TxtHoraIniCalceUvlp.Text = lLector["hora_ini_negociacioni_sci"].ToString();
                    TxtHoraFinCalceUvlp.Text = lLector["hora_fin_negociacioni_sci"].ToString();
                    lblEstado.Text = "Fase de Reactivación: ";

                    switch (hndFase.Value)
                    {
                        case "C":
                            lblEstado.Text += "Creada";
                            TxtHoraIniPubUvlp.Enabled = true;
                            TxtHoraFinPubUvlp.Enabled = true;
                            TxtHoraIniCompUvlp.Enabled = true;
                            TxtHoraFinCompUvlp.Enabled = true;
                            TxtHoraIniCalceUvlp.Enabled = true;
                            TxtHoraFinCalceUvlp.Enabled = true;
                            break;
                        case "3":
                            lblEstado.Text += "Publicación de capacidad disponible";
                            TxtHoraIniPubUvlp.Enabled = true;
                            TxtHoraFinPubUvlp.Enabled = true;
                            TxtHoraIniCompUvlp.Enabled = true;
                            TxtHoraFinCompUvlp.Enabled = true;
                            TxtHoraIniCalceUvlp.Enabled = true;
                            TxtHoraFinCalceUvlp.Enabled = true;
                            break;
                        case "4":
                            lblEstado.Text += "Ingreso posturas de compra";
                            TxtHoraIniPubUvlp.Enabled = false;
                            TxtHoraFinPubUvlp.Enabled = false;
                            TxtHoraIniCompUvlp.Enabled = true;
                            TxtHoraFinCompUvlp.Enabled = true;
                            TxtHoraIniCalceUvlp.Enabled = true;
                            TxtHoraFinCalceUvlp.Enabled = true;
                            break;
                        case "5":
                            lblEstado.Text += "Calce de operaciones";
                            TxtHoraIniPubUvlp.Enabled = false;
                            TxtHoraFinPubUvlp.Enabled = false;
                            TxtHoraIniCompUvlp.Enabled = false;
                            TxtHoraFinCompUvlp.Enabled = false;
                            TxtHoraIniCalceUvlp.Enabled = true;
                            TxtHoraFinCalceUvlp.Enabled = true;
                            break;
                    }

                    lLector.Close();
                    lLector.Dispose();
                }
                else
                {
                    lblEstado.Text = "";
                    hndFase.Value = "C";
                }

                lLector.Dispose();
                lLector.Close();
                _lConexion.Cerrar();

                Modal.Abrir(this, mdlReactivar.ID, mdlInsideReactivar.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReactivar_Click(object sender, EventArgs e)
        {
            try
            {
                var oError = "";
                var sMensaje = "";
                string strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"] + "mensaje.txt";
                string strRutaArchivo1 = ConfigurationManager.AppSettings["RutaArchivos"] + "subasta-" + CodSubasta + ".txt";

                if (TxtObservacion.Text == "")
                    oError += "Debe digitar las observaciones de la reactivaccion\\n";

                if (oError == "")
                {
                    string[] lsNombreParametros = { "@P_numero_rueda", "@P_observaciones", "@P_inicio_fase3", "@P_fin_fase3", "@P_inicio_fase4", "@P_fin_fase4", "@P_inicio_fase5", "@P_fin_fase5" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
                    string[] lValorParametros = { CodSubasta, TxtObservacion.Text, TxtHoraIniPubUvlp.Text, TxtHoraFinPubUvlp.Text, TxtHoraIniCompUvlp.Text, TxtHoraFinCompUvlp.Text, TxtHoraIniCalceUvlp.Text, TxtHoraFinCalceUvlp.Text }; ///  

                    SqlDataReader lLector;
                    _lConexion.Abrir();
                    try
                    {
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(_lConexion.gObjConexion, "pa_SetActRueda", lsNombreParametros, lTipoparametros, lValorParametros, _goInfo);
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                                oError += lLector["error"] + "\\n";
                        }
                        if (oError == "")
                        {
                            File.SetCreationTime(strRutaArchivo, DateTime.Now);
                            File.SetCreationTime(strRutaArchivo1, DateTime.Now);
                            Toastr.Success(this, "La rueda se reactivó exitosamente");
                            Session["hora"] = "";
                            CloseReactivar_Click(null, null);
                        }
                        _lConexion.Cerrar();
                    }
                    catch (Exception ex)
                    {
                        Toastr.Error(this, "Error al reactivar la rueda. " + ex.Message);
                        _lConexion.Cerrar();
                    }
                }

                if (oError == "") return;
                Toastr.Error(this, oError);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseReactivar_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Reactivar
                Modal.Cerrar(this, mdlReactivar.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Reactivar

        #region Ingreso Posturas de Compra

        /// <summary>
        /// 
        /// </summary>
        protected void OpenIngresoPosturaCompra(string id, string accion, string maxPostCompra)
        {
            _lConexion = new clConexion(_goInfo);

            if (string.IsNullOrEmpty(id))
            {
                Toastr.Error(this, "No Se enviaron los Parametros requeridos.!");
                return;
            }

            hndID.Value = id;
            hndAccion.Value = accion;
            hdfMaxPostCompra.Value = maxPostCompra;

            /// Obtengo los Datos del ID Recibido
            _lConexion = new clConexion(_goInfo);
            _lConexion.Abrir();
            SqlDataReader lLector = DelegadaBase.Servicios.LlenarControl(_lConexion.gObjConexion, "pa_ValidarExistencia", "t_id_rueda idr, m_ruta_snt rut", " idr.numero_id = " + hndID.Value + " and idr.codigo_punto_entrega= rut.codigo_pozo_ini and idr.codigo_punto_fin= rut.codigo_pozo_fin"); //20171119 rq048-17
            if (!lLector.HasRows)
            {
                Toastr.Error(this, "El ID enviado NO existe en la base de Datos.!");
                return;
            }

            lLector.Read();
            /// Lleno las etiquetas de la pantalla con los datos del Id.
            lblRueda.Text = "No. Rueda: " + lLector["numero_rueda"];
            hdfNoRueda.Value = lLector["numero_rueda"].ToString();
            lblId.Text = "ID: " + id;
            lblFecha.Text = "Fecha Rueda: " + lLector["fecha_rueda"].ToString().Substring(0, 10);
            lblPuntoEntrega.Text =
                "Ruta: " + lLector["codigo_ruta"] + "-" + lLector["descripcion"]; //20171119 rq048-17
            //if (Session["ind_tipo_rueda"].ToString() == "P") //20171119 rq048-17
            //    lblPuntoEntrega.Text += "-" + lLector["desc_punto_fin"].ToString();  //20171119 rq048-17
            lblPeridoEnt.Text = "Periodo Entrega: " + lLector["codigo_periodo"] + "-" + lLector["desc_periodo"];
            lblModalidad.Text = "Modalidad Contractual: " + lLector["codigo_modalidad"] + "-" +
                                lLector["desc_modalidad"];
            /// Reviso si se esta creando la oferta o modificando
            hndCntTotal.Value = lLector["cantidad_total_venta"].ToString();
            lLector.Close();
            lLector.Dispose();

            /// Llamo el Procedimiento que arma la grilla del ingreso de las ofertas
            string[] lsNombreParametros = { "@P_numero_id", "@P_codigo_operador", "@P_max_post_compra" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { hndID.Value, _goInfo.cod_comisionista, hdfMaxPostCompra.Value };

            dtgPosturasC.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(_lConexion.gObjConexion,
                "pa_GetPosturasC", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgPosturasC.DataBind();
            if (dtgPosturasC.Items.Count > 0)
            {
                TextBox oCnt = null;
                TextBox oPre = null;
                int ldCont = 1;
                foreach (DataGridItem Grilla in dtgPosturasC.Items)
                {
                    if (hndAccion.Value == "M")
                    {
                        Grilla.Cells[0].Text = ldCont.ToString();
                        ldCont++;
                    }

                    oCnt = (TextBox)Grilla.Cells[2].Controls[1];
                    oPre = (TextBox)Grilla.Cells[1].Controls[1];
                    //oPre.Focus = true;
                    if (Grilla.Cells[3].Text.Replace("&nbsp;", "") != "")
                        oCnt.Text = Grilla.Cells[3].Text.Replace(",", "");
                    if (Grilla.Cells[4].Text.Replace("&nbsp;", "") != "")
                        oPre.Text = Grilla.Cells[4].Text.Replace(",", "");
                }
            }
            //Se abre el modal de Ingreso de Declaración de Información
            Modal.Abrir(this, mdlIngresoPosturaCompra.ID, mdlIngresoPosturaCompraInside.ID);

            _lConexion.Cerrar();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnOfertar_Click(object sender, EventArgs e)
        {
            string oError = "";
            string lsError = "";
            string lsNoPostura = "";

            string[] lsNombreParametros = { "@P_numero_postura", "@P_numero_id", "@P_numero_rueda", "@P_codigo_operador", "@P_precio_postura", "@P_cantidad_postura", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Float, SqlDbType.Int, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "0", "0", "0", "0", "0", "0" }; ///  Accion 0=Creo la Temporal 1=Crear 2=Modificar

            lsError = ValidarEntradas();
            if (lsError == "")
            {
                _lConexion = new clConexion(_goInfo);
                _lConexion.Abrir();
                SqlTransaction oTransaccion;
                oTransaccion = _lConexion.gObjConexion.BeginTransaction(IsolationLevel.Serializable);
                try
                {
                    TextBox oCnt = null;
                    TextBox oPre = null;
                    foreach (DataGridItem Grilla in dtgPosturasC.Items)
                    {
                        oCnt = (TextBox)Grilla.Cells[2].Controls[1];
                        oPre = (TextBox)Grilla.Cells[1].Controls[1];
                        lValorParametros[1] = hndID.Value;
                        lValorParametros[2] = hdfNoRueda.Value;
                        lValorParametros[3] = _goInfo.cod_comisionista;
                        lValorParametros[4] = oPre.Text.Replace(",", "");
                        lValorParametros[5] = oCnt.Text.Replace(",", "");
                        if (hndAccion.Value == "C")
                        {
                            lsNoPostura = Grilla.Cells[0].Text.Replace("&nbsp;", "");
                        }
                        else
                        {
                            lsNoPostura = Grilla.Cells[5].Text.Replace("&nbsp;", "");
                        }
                        lValorParametros[6] = "0";
                        lValorParametros[0] = lsNoPostura;

                        _goInfo.mensaje_error = "";
                        if (!DelegadaBase.Servicios.EjecutarProcedimientoConTransaccion(_lConexion.gObjConexion, "pa_SetPosturaCompraId3", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, _goInfo))
                        {
                            if (_goInfo.mensaje_error != "")
                            {
                                oError = "Error al Actualizar la Postura. " + _goInfo.mensaje_error + "\\n";
                                oTransaccion.Rollback();
                                break;
                            }
                        }
                    }
                    if (oError == "")
                    {
                        if (hndAccion.Value == "C")
                        {
                            lValorParametros[2] = hdfNoRueda.Value;
                            lValorParametros[6] = "1";
                            lValorParametros[0] = "0";
                        }
                        else
                        {
                            lValorParametros[2] = hdfNoRueda.Value;
                            lValorParametros[6] = "2";
                            lValorParametros[0] = lsNoPostura;
                        }
                        SqlDataReader lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(_lConexion.gObjConexion, "pa_SetPosturaCompraId3", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, _goInfo);
                        if (_goInfo.mensaje_error != "")
                        {
                            oError = "Error al Actualizar la Postura. " + _goInfo.mensaje_error + "\\n";
                            oTransaccion.Rollback();
                        }
                        else
                        {
                            if (lLector.HasRows)
                            {
                                lLector.Read();
                                if (lLector["mensaje"].ToString() != "")
                                {
                                    oError = lLector["mensaje"].ToString();
                                    lLector.Close();
                                    lLector.Dispose();
                                    oTransaccion.Rollback();
                                    _lConexion.Cerrar();
                                }
                                else
                                {
                                    lLector.Close();
                                    lLector.Dispose();
                                    oTransaccion.Commit();
                                    _lConexion.Cerrar();
                                    MisOfertas = "S";
                                    Refrescar = "S";
                                    Toastr.Success(this, hndAccion.Value == "C"
                                            ? "Posturas Ingresadas Correctamente.!"
                                            : "Posturas Actualizadas Correctamente.!");
                                    //Se cierra el modal 
                                    CloseIngresoPosturasCompra_Click(null, null);
                                    //Se refresca la grilla  
                                    CargarDatosGrilla();
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    oTransaccion.Rollback();
                    oError = "Error al realizar la oferta. " + ex.Message;
                    _lConexion.Cerrar();
                }
                if (oError != "")
                {
                    Toastr.Error(this, oError);
                }
            }
            else
                Toastr.Error(this, lsError);
        }

        /// <summary>
        /// Relaiza la validacion del Ingreso de las Posturas
        /// </summary>
        /// <returns></returns>
        protected string ValidarEntradas()
        {
            string lsError = "";
            int oValor = 0;
            double oValor1 = 0;
            int ldValorAnt = 0;
            int liCntVenta = Convert.ToInt32(hndCntTotal.Value);
            double ldPrecioAnt = 0;
            int ldConta = 0;

            TextBox oCnt = null;
            TextBox oPre = null;
            foreach (DataGridItem Grilla in dtgPosturasC.Items)
            {
                oCnt = (TextBox)Grilla.Cells[2].Controls[1];
                oPre = (TextBox)Grilla.Cells[1].Controls[1];
                oCnt.Text = oCnt.Text.Replace(",", "");
                oPre.Text = oPre.Text.Replace(",", "");

                if (oCnt.Text.Trim().Length > 0)
                {
                    try
                    {
                        // Validacion para que no permita cantidad  menor o Igual a 0 - 20150709
                        if (Convert.ToInt32(oCnt.Text.Replace(",", "")) <= 0)
                            lsError += "La Cantidad de la Postura " + Grilla.Cells[0].Text + " no puede ser menor o Igual a 0\\n";
                        else
                        {
                            if (Convert.ToInt32(oCnt.Text.Replace(",", "")) > liCntVenta)
                                lsError += "La Cantidad de la Postura " + Grilla.Cells[0].Text + " no puede ser mayor que la cantidad ofrecida\\n";
                        }
                    }
                    catch (Exception ex)
                    {
                        lsError += "Valor Inválido en la Cantidad de la Postura " + Grilla.Cells[0].Text + "\\n";
                    }
                }
                else
                    lsError += "Debe Ingresar la Cantidad de la Postura " + Grilla.Cells[0].Text + "\\n";
                if (oPre.Text.Trim().Length > 0)
                {
                    try
                    {
                        if (Convert.ToDouble(oPre.Text.Replace(",", "")) < 0)
                            lsError += "El Precio debe ser mayor o igual que cero para la postura " + Grilla.Cells[0].Text + "\\n";
                    }
                    catch (Exception ex)
                    {
                        lsError += "Valor Inválido en el Precio de la Postura " + Grilla.Cells[0].Text + "\\n";
                    }
                }
                else
                    lsError += "Debe Ingresar el Precio de la Postura " + Grilla.Cells[0].Text + "\\n";
                if (lsError != "")
                    break;
                else
                {
                    if (ldConta > 0)
                    {
                        if (Convert.ToInt32(oCnt.Text.Replace(",", "")) < ldValorAnt)
                            lsError += "La Cantidad de la Postura " + Grilla.Cells[0].Text + " debe ser mayor a la postura anterior. \\n";
                        if (Convert.ToDouble(oPre.Text.Replace(",", "")) >= ldPrecioAnt)
                            lsError += "El  Precio de la Postura " + Grilla.Cells[0].Text + " debe ser menor al de la postura anterior. \\n";

                        ldValorAnt = Convert.ToInt32(oCnt.Text.Replace(",", ""));
                        ldPrecioAnt = Convert.ToDouble(oPre.Text.Replace(",", ""));
                    }
                    else
                    {
                        ldValorAnt = Convert.ToInt32(oCnt.Text.Replace(",", ""));
                        ldPrecioAnt = Convert.ToDouble(oPre.Text.Replace(",", ""));
                    }
                }
                ldConta++;
            }
            return lsError;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseIngresoPosturasCompra_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Ingreso Posturas de Compra
                Modal.Cerrar(this, mdlIngresoPosturaCompra.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Ingreso Posturas de Compra

        #region Enviar Mensaje

        /// <summary>
        /// 
        /// </summary>
        protected void OpenEnviarMensaje(object sender, EventArgs e)
        {
            //Se abre el modal de Enviar Mensaje
            Modal.Abrir(this, mdlEnviarMensaje.ID, mdlEnviarMensajeInside.ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnMensaje_Click(object sender, EventArgs e)
        {
            var oError = "";
            var strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString() + "mensaje.txt";

            if (TxtMensaje.Text == "")
                oError += "Debe digitar el mensaje a enviar\\n";

            if (oError == "")
            {
                string[] lsNombreParametros = { "@P_numero_rueda", "@P_mensaje" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
                string[] lValorParametros = { CodSubasta, TxtMensaje.Text, };

                _lConexion = new clConexion(_goInfo);
                _lConexion.Abrir();
                try
                {
                    DelegadaBase.Servicios.EjecutarProcedimiento(_lConexion.gObjConexion, "pa_setMensaje", lsNombreParametros, lTipoparametros, lValorParametros, _goInfo);
                    Toastr.Success(this, "Mensaje enviado Correctamente");
                    File.SetCreationTime(strRutaArchivo, DateTime.Now);
                    //Se cierra el modal 
                    CloseIngresoPosturasCompra_Click(null, null);
                    //Se refresca la grilla  
                    //CargarDatosGrilla();

                    _lConexion.Cerrar();
                }
                catch (Exception ex)
                {
                    oError = "Error al enviar el mensaje. " + ex.Message;
                    _lConexion.Cerrar();
                }
            }
            if (oError != "")
            {
                Toastr.Error(this, oError);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseEnviarMensaje_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Enviar Mensaje
                Modal.Cerrar(this, mdlEnviarMensaje.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Enviar Mensaje

        #region Mensaje

        /// <summary>
        /// 
        /// </summary>
        protected void AbrirMensajes(object sender, EventArgs e)
        {
            try
            {
                string[] lsNombreParametros = { "@P_numero_rueda" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int };
                string[] lValorParametros = { CodSubasta.ToString() };
                SqlDataReader lLector;
                _lConexion = new clConexion(_goInfo);
                _lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(_lConexion.gObjConexion, "pa_GetMensajes", lsNombreParametros, lTipoparametros, lValorParametros);

                StringBuilder builder = new StringBuilder();

                builder.Append("<table  class=\"table table-bordered table-fixed\">");
                builder.Append("<thead class=\"thead-light\">");
                builder.Append("<tr>");
                builder.Append("<th width='10%'>IDG</HORA></th>");
                builder.Append("<th width='90%'>MENSAJE</font></th>");
                builder.Append("</tr>");
                builder.Append("</thead>");

                if (lLector.HasRows)
                {
                    builder.Append("<tbody>");
                    while (lLector.Read())
                    {
                        builder.Append("<tr>");
                        builder.Append("<td width='10%'>" + lLector["hora_mensaje"] + "</font></td>");
                        builder.Append("<td width='90%'>" + lLector["mensaje"] + "</font></td>");
                        builder.Append("</tr>");
                    }
                    builder.Append("</tbody>");
                }

                builder.Append("</table>");

                ltTableroRF.Text = builder.ToString();

                lLector.Close();
                lLector.Dispose();
                _lConexion.Cerrar();

                /// Creacion del archivo del tablero de RF 
                /// 2011-10-03
                ///
                if (MisOfertas.ToString() == "S")
                    MisOfertas = "N";

                //Se abre el modal de oferta
                Modal.Abrir(this, mdlMensaje.ID, mdlMensajeInside.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseMensajes_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Oferta
                Modal.Cerrar(this, mdlMensaje.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Mensaje

        #region Contratos

        /// <summary>
        /// 
        /// </summary>
        protected void AbrirContratos(object sender, EventArgs e)
        {
            try
            {
                var builder = new StringBuilder();

                if (EstadoSubasta.Equals("F"))
                {
                    try
                    {
                        string[] lsNombreParametros = { "@P_numero_rueda" };
                        SqlDbType[] lTipoparametros = { SqlDbType.Int };
                        string[] lValorParametros = { CodSubasta };
                        SqlDataReader lLector;
                        _lConexion = new clConexion(_goInfo);
                        _lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(_lConexion.gObjConexion, "pa_GetMisContratos3", lsNombreParametros, lTipoparametros, lValorParametros);

                        builder.Append("<table  class=\"table table-bordered\">");
                        builder.Append("<thead class=\"thead-light\">");
                        builder.Append("<tr>");
                        builder.Append("<th width='10%'>IDG</font></th>");
                        builder.Append("<th width='15%'>CONTRATO</font></th>");
                        builder.Append("<th width='25%'>COMPRADOR</font></th>");
                        builder.Append("<th width='25%'>VENDEDOR</font></th>");
                        builder.Append("<th width='12%'>CANTIDAD</font></th>");
                        builder.Append("<th width='13%'>PRECIO</font></th>");
                        builder.Append("</tr>");
                        builder.Append("</thead>");

                        if (lLector.HasRows)
                        {
                            builder.Append("<tbody>");
                            while (lLector.Read())
                            {
                                builder.Append("<tr>");
                                builder.Append("<td width='10%'>" + lLector["numero_id"] + "</font></td>");
                                builder.Append("<td width='15%'>" + lLector["numero_contrato"] + "</font></td>");
                                builder.Append("<td width='25%'>" + lLector["nombre_compra"] + "</font></td>");
                                builder.Append("<td width='25%'>" + lLector["nombre_venta"] + "</font></td>");
                                builder.Append("<td width='12%'>" + lLector["cantidad"] + "</font></td>");
                                builder.Append("<td width='13%'>" + lLector["precio"] + "</font></td>");
                                builder.Append("</tr>");
                            }
                            builder.Append("</tbody>");
                        }
                        builder.Append("</table>");
                        ltTablero.Text = builder.ToString();

                        lLector.Close();
                        lLector.Dispose();
                        _lConexion.Cerrar();
                    }
                    catch (Exception ex)
                    {
                        _lConexion.Cerrar();
                        Toastr.Error(this, ex.Message);
                    }
                }
                else
                {
                    try
                    {

                        string[] lsNombreParametros = { "@P_numero_rueda" };
                        SqlDbType[] lTipoparametros = { SqlDbType.Int };
                        string[] lValorParametros = { CodSubasta };
                        SqlDataReader lLector;
                        _lConexion = new clConexion(_goInfo);
                        _lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(_lConexion.gObjConexion, "pa_GetMisOFertas3", lsNombreParametros, lTipoparametros, lValorParametros);

                        builder.Append("<table  class=\"table table-bordered\">");
                        builder.Append("<thead class=\"thead-light\">");
                        builder.Append("<tr>");
                        builder.Append("<th width='10%'>IDG</font></th>");
                        builder.Append("<th width='10%'>PUNTA</font></th>");
                        builder.Append("<th width='50%'>OPERADOR</font></th>");
                        builder.Append("<th width='15%'>CANTIDAD</font></th>");
                        builder.Append("<th width='15%'>PRECIO</font></th>");
                        builder.Append("</tr>");
                        builder.Append("</thead>");

                        if (lLector.HasRows)
                        {
                            builder.Append("<tbody>");
                            while (lLector.Read())
                            {
                                builder.Append("<tr>");
                                builder.Append("<td width='10%'>" + lLector["numero_id"] + "</font></td>");
                                builder.Append("<td width='10%'>" + lLector["punta"] + "</font></td>");
                                builder.Append("<td width='50%'>" + lLector["nombre_operador"] + "</font></td>");
                                builder.Append("<td width='15%'>" + lLector["cantidad_postura"] + "</font></td>");
                                builder.Append("<td width='15%'>" + lLector["precio"] + "</font></td>");
                                builder.Append("</tr>");
                            }
                            builder.Append("</tbody>");
                        }
                        builder.Append("</table>");
                        ltTablero.Text = builder.ToString();

                        lLector.Close();
                        lLector.Dispose();
                        _lConexion.Cerrar();
                    }
                    catch (Exception ex)
                    {
                        _lConexion.Cerrar();
                        Toastr.Error(this, ex.Message);
                    }
                }
                //Se abre el modal de oferta
                Modal.Abrir(this, mdlContratos.ID, mdlContratosInside.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseContratos_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Oferta
                Modal.Cerrar(this, mdlContratos.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Contratos

        #region Curva de Oferta y Demanda Agregada

        /// <summary>
        /// Abre el modal de la Curva de Oferta y Demanda Agregada
        /// </summary>
        private void OpenCurvaOferDemAgre(string lblNoId)
        {
            var transporte = true;
            DataSet lds = new DataSet();
            _goInfo.Programa = "Curva de oferta y demanda agregada";
            lblMensaje.Text = "";

            _lConexion = new clConexion(_goInfo);
            _lConexion.Abrir();
            SqlCommand lComando1 = new SqlCommand();
            SqlDataAdapter lsqldata1 = new SqlDataAdapter();
            lComando1.Connection = _lConexion.gObjConexion;
            lComando1.CommandTimeout = 3600;
            lComando1.CommandType = CommandType.StoredProcedure;

            //cambios grafica 20160202
            lComando1.CommandText = "pa_ValidarExistencia";
            lComando1.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = "t_id_rueda";
            lComando1.Parameters.Add("@P_Filtro", SqlDbType.VarChar).Value = "numero_id = " + lblNoId;
            SqlDataReader lLector = lComando1.ExecuteReader();
            if (lLector.HasRows)
            {
                lLector.Read();
                if (lLector["codigo_producto"].ToString() == "1")
                {
                    lblPuntoCurva.Text = "Punto de Entrega: " + lLector["desc_punto_entrega"];
                    transporte = false;
                }
                else
                {
                    lblPuntoCurva.Text = "Ruta: " + lLector["desc_punto_entrega"] + "-" + lLector["desc_punto_fin"];
                }
            }
            else
                lblPuntoCurva.Text = "";
            lComando1.Parameters.Clear();
            lLector.Close();
            lLector.Dispose();

            lComando1.CommandText = "pa_GetPreciosCurva1";
            lComando1.Parameters.Add("@P_numero_id", SqlDbType.Int).Value = lblNoId;
            lComando1.ExecuteNonQuery();
            lsqldata1.SelectCommand = lComando1;
            lsqldata1.Fill(lds);
            dtgInformacion.DataSource = lds;
            dtgInformacion.DataBind();
            _lConexion.Cerrar();

            //Carga la grafica
            AuctionGraphics.GenerateGraph(this, dtgInformacion, lblNoId, transporte);
            //Se abre el modal de Ingreso de Declaración de Información
            Modal.Abrir(this, mdlCurvaOfertaDemAgre.ID, mdlCurvaOfertaDemAgreInside.ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseCurvaOferDemAgre_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Ingreso de Declaración de Información
                Modal.Cerrar(this, mdlCurvaOfertaDemAgre.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Curva de Oferta y Demanda Agregada

        #endregion Modals

    }
}