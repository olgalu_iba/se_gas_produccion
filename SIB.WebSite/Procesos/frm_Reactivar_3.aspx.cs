﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_Reactivar_3 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    String strRutaArchivo;
    String strRutaArchivo1;
    SqlDataReader lLector;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString() + "mensaje.txt";
        strRutaArchivo1 = ConfigurationManager.AppSettings["RutaArchivos"].ToString() + "subasta-" + Session["numero_rueda"].ToString() + ".txt";
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            /// Obtengo los Datos de la rueda
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda rue", " rue.numero_rueda = " + Session["numero_rueda"].ToString());
            if (lLector.HasRows)
            {
                lLector.Read();
                /// Lleno las etiquetas de la pantalla con los datos del Id.
                hndFase.Value = lLector["fase_reactivacion"].ToString();
                TxtHoraIniPubUvlp.Text = lLector["hora_ini_publi_v_sci"].ToString();
                TxtHoraFinPubUvlp.Text = lLector["hora_fin_publi_v_sci"].ToString();
                TxtHoraIniCompUvlp.Text = lLector["hora_ini_rec_solicitud_c_sci"].ToString();
                TxtHoraFinCompUvlp.Text = lLector["hora_fin_rec_solicitud_c_sci"].ToString();
                TxtHoraIniCalceUvlp.Text = lLector["hora_ini_negociacioni_sci"].ToString();
                TxtHoraFinCalceUvlp.Text = lLector["hora_fin_negociacioni_sci"].ToString();
                lblEstado.Text = "Fase de Reactivación: ";

                switch (hndFase.Value)
                {
                    case "C": lblEstado.Text += "Creada";
                        TxtHoraIniPubUvlp.Enabled = true;
                        TxtHoraFinPubUvlp.Enabled = true;
                        TxtHoraIniCompUvlp.Enabled = true;
                        TxtHoraFinCompUvlp.Enabled = true;
                        TxtHoraIniCalceUvlp.Enabled = true;
                        TxtHoraFinCalceUvlp.Enabled = true;
                        break;
                    case "3": lblEstado.Text += "Publicación de capacidad disponible";
                        TxtHoraIniPubUvlp.Enabled = true;
                        TxtHoraFinPubUvlp.Enabled = true;
                        TxtHoraIniCompUvlp.Enabled = true;
                        TxtHoraFinCompUvlp.Enabled = true;
                        TxtHoraIniCalceUvlp.Enabled = true;
                        TxtHoraFinCalceUvlp.Enabled = true;
                        break;
                    case "4": lblEstado.Text += "Ingreso posturas de compra";
                        TxtHoraIniPubUvlp.Enabled = false;
                        TxtHoraFinPubUvlp.Enabled = false;
                        TxtHoraIniCompUvlp.Enabled = true;
                        TxtHoraFinCompUvlp.Enabled = true;
                        TxtHoraIniCalceUvlp.Enabled = true;
                        TxtHoraFinCalceUvlp.Enabled = true;
                        break;
                    case "5": lblEstado.Text += "Calce de operaciones";
                        TxtHoraIniPubUvlp.Enabled = false;
                        TxtHoraFinPubUvlp.Enabled = false;
                        TxtHoraIniCompUvlp.Enabled = false;
                        TxtHoraFinCompUvlp.Enabled = false;
                        TxtHoraIniCalceUvlp.Enabled = true;
                        TxtHoraFinCalceUvlp.Enabled = true;
                        break;
                }
                lLector.Close();
                lLector.Dispose();
            }
            else
            {
                lblEstado.Text = "";
                hndFase.Value = "C";
            }
            lLector.Dispose();
            lLector.Close();
            lConexion.Cerrar();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReactivar_Click(object sender, EventArgs e)
    {
        string oError = "";
        string sMensaje = "";

        //if (TxtHora.Text == "")
        //    oError += "Debe digitar la Hora de inicio de la próxima ronda\\n";
        //if (TxtMinutos.Text == "")
        //    oError += "Debe la duración de la próxima ronda\\n";
        if (TxtObservacion.Text == "")
            oError += "Debe digitar las observaciones de la reactivaccion\\n";

        if (oError == "")
        {
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_observaciones", "@P_inicio_fase3", "@P_fin_fase3", "@P_inicio_fase4", "@P_fin_fase4", "@P_inicio_fase5", "@P_fin_fase5"};
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar};
            string[] lValorParametros = { Session["numero_rueda"].ToString(), TxtObservacion.Text, TxtHoraIniPubUvlp.Text, TxtHoraFinPubUvlp.Text, TxtHoraIniCompUvlp.Text, TxtHoraFinCompUvlp.Text, TxtHoraIniCalceUvlp.Text, TxtHoraFinCalceUvlp.Text}; ///  

            SqlDataReader lLector;
            lConexion.Abrir();
            try
            {
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetActRueda", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        oError += lLector["error"].ToString() + "\\n";
                }
                if (oError == "")
                {
                    sMensaje = "La rueda se reactivó exitosamente";
                    File.SetCreationTime(strRutaArchivo, DateTime.Now);
                    File.SetCreationTime(strRutaArchivo1, DateTime.Now);
                    Toastr.Success(this, sMensaje);
                    Session["hora"] = "";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                oError = "Error al reactivar la rueda. " + ex.Message;
                lConexion.Cerrar();
            }
        }
        if (oError != "")
        {
            Toastr.Warning(this, oError);
        }
    }
}
