﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_PublicaCE.aspx.cs"
    Inherits="Procesos_frm_PublicaCE" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
            width="85%">
            <tr>
                <td align="center" class="th1">
                    <asp:Label ID="lblTitulo" runat="server" ForeColor ="White" ></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <br /><br /><br /><br /><br /><br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:Button ID="BtnCalcular" runat="server" Text="Calcular" OnClick="BtnCalcular_Click"
                    OnClientClick="MostrarCrono(); this.disabled = true;" UseSubmitBehavior="false" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">
                Tiempo transcurrido: &nbsp;
                <input type="text" name="display" id="display" size="8" value="00:00:0" class="reloj"
                    readonly="true">
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                <%--20170929 rq048-17--%>
                <asp:HiddenField id="hndFecha" runat="server" />
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla" visible="false">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 900px; height: 400px;">
                    <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="titular capacidad excedentaria"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_tramo" HeaderText="Cod Tramo" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tramo" HeaderText="tramo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="capac_exc_1" HeaderText="ce 1" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="capac_exc_2" HeaderText="ce 2" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="capac_exc_3" HeaderText="ce 3" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="capac_exc_4" HeaderText="ce 4" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="capac_exc_5" HeaderText="ce 5" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="excento_uvlp" HeaderText="NO se tiene en cuenta para la subasta"
                                ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_cen" HeaderText="Control Cen" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>