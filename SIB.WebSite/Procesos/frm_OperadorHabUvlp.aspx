﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_OperadorHabUvlp.aspx.cs"
    Inherits="Procesos_frm_OperadorHabUvlp" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
            width="90%">
            <tr>
                <td align="center" class="th1">
                    <asp:Label ID="lblTitulo" runat="server" ForeColor ="White"></asp:Label>
                </td>
            </tr>
        </table>
        <br /><br /><br /><br /><br /><br />
        <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
            width="90%">
            <tr>
                <td class="td1">
                    Operador
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlOperador" runat="server">
                    </asp:DropDownList>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                        OnClick="imbConsultar_Click" Height="40" />
                        &nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnAprobar" runat="server" Text="Aprobar" OnClientClick="return confirmar();"
                        OnClick="btnAprobar_Click" Visible="false" />
                        &nbsp;&nbsp;&nbsp;
                    <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                        Visible="false" Height="35" />
                </td>
            </tr>
        </table>
    </div>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="95%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla" visible="false">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 1150px; height: 350px;">
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        OnEditCommand="dtgMaestro_EditCommand" PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Sele.">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ChkTodos" runat="server" OnCheckedChanged="ChkTodos_CheckedChanged"
                                        AutoPostBack="true" Text="Seleccionar Todos" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkRecibir" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Cd Ope" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador_hab" Visible="false"></asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <asp:HiddenField ID="hndCodRueda" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>