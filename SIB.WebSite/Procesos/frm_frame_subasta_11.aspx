<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeFile="frm_frame_subasta_11.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Procesos.frm_frame_subasta_11" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/dev/auctionChart.js")%>"></script>
    <script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/dev/clock.js")%>"></script>

    <script language="javascript">
        //        document.location = no;
        function confirmarDemanda() {
            return confirm("Esta seguro que desea demandar!");
        }

        function confirmar() {
            return confirm("Esta seguro que desea enviar el mensaje!");
        }

        function confirmarRechazo() {
            return confirm("Esta seguro que desea rechazar el contrato?");
        }

        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }

        function formatoMiles(obj, evt) {
            var T;
            var strValue = removeCommas(obj.value);
            //if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
            if ((T = /([0-9]*)$/i.exec(strValue)) != null) {
                //                T[4] = T[4].substr(0, decimales);
                //                if (T[2] == '' && T[3] == '.') T[2] = 0;
                obj.value = addCommas(T[1]);
            } else {
                alert('formato invalido ');
            }
        }
        function formatoMilesPre(obj, evt, decimales) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
                T[4] = T[4].substr(0, decimales);
                if (T[2] == '' && T[3] == '.') {
                    T[2] = 0;
                }
                obj.value = T[1] + addCommas(T[2]) + T[3] + T[4];
            }
            else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }

    </script>

    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="kt-portlet">
                    <%--Head--%>
                    <div class="kt-portlet__head">
                        <%--Titulo--%>
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <%--20220211--%>
                                <asp:Label ID="lblTitle" Text="Subasta de Transporte Congestión Contractual" runat="server" />
                            </h3>
                        </div>
                        <%--Botones--%>
                        <asp:UpdatePanel ID="UpdatePanel12" style="margin-top: 1%" runat="server">
                            <ContentTemplate>
                                <segas:CrudButton ID="buttons" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                    <%--Contenido--%>
                    <div class="kt-portlet__body">
                        <strong>
                            <h5>
                                <input id="lblRelog" style="width: 100px; color: green; border: none;" type="text" readonly name="reloj" size="10">
                            </h5>
                        </strong>

                        <%--Estados de la subasta--%>
                        <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                            <ContentTemplate>
                                <div id="divTiempoEspera" class="row" visible="False" runat="server">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <br />
                                        <div class="form-group">
                                            <div class="alert alert-info" runat="server">
                                                <strong>&#161;Atenci&#243;n&#33;</strong>
                                                <p>&nbsp; Se encuentra en un tiempo de espera</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:Button ID="btnClickEmulate" OnClick="Timer_Tick" Style="display: none;" ClientIDMode="Static" runat="server" />
                                <segas:Estado ID="estados" Visible="false" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <%--Separador--%>
                        <div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>

                        <%--Botones--%>
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label"></div>
                            <div class="kt-portlet__head-toolbar">
                                <segas:Subasta ID="buttonsSubasta" runat="server" />
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <segas:SubastaExtender ID="buttonsSubastaExtender" runat="server" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>

                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <%--Tabla Subasta--%>
                                <div class="kt-portlet__body">
                                    <div class="tab-content">
                                        <div class="table table-responsive">
                                            <asp:DataGrid ID="dtgSubasta" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                                ViewStateMode="Enabled" OnItemCommand="OnItemCommand_Click">
                                                <Columns>
                                                    <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--1--%><asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--2--%><asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--3--%><asp:BoundColumn DataField="desc_punto" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--4--%><asp:BoundColumn DataField="fecha_entrega" HeaderText="Fecha Entrega Inicial" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                                    <%--5--%><asp:BoundColumn DataField="fecha_entrega_fin" HeaderText="Fecha Entrega Final" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                                    <%--6--%><asp:BoundColumn DataField="cantidad_total_venta" HeaderText="Cantidad Venta (KPCD)" DataFormatString="{0: ###,###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                    <%--20201020--%> <%--20220828--%>
                                                    <%--7--%><asp:BoundColumn DataField="precio_venta" HeaderText="Precio Venta (Moneda vigente/KPC)" ItemStyle-BackColor=""></asp:BoundColumn>
                                                    <%--20201020--%>
                                                    <%--8--%><asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-BackColor=""></asp:BoundColumn>
                                                    <%--9--%><asp:TemplateColumn HeaderText="Oferta">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imbOfertar" runat="server" ToolTip="Ofertar" ImageUrl="~/Images/nuevo.gif" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--10--%><asp:TemplateColumn HeaderText="Modificar">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imbModificar" runat="server" ToolTip="Modificar Oferta" ImageUrl="~/Images/modificar.gif" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--11--%><asp:TemplateColumn HeaderText="Eliminar">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imbEliminar" runat="server" ToolTip="Eliminar Oferta" ImageUrl="~/Images/eliminar.gif" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--12--%><asp:TemplateColumn HeaderText="Grafica" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imbGrafica" runat="server" ToolTip="Curva Agregada" ImageUrl="~/Images/grafica.jpg" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--13--%><asp:BoundColumn DataField="oferta_ya_realizada" Visible="false"></asp:BoundColumn>
                                                    <%--14--%><asp:BoundColumn DataField="max_posturas" Visible="false"></asp:BoundColumn>
                                                    <%--15--%><asp:BoundColumn DataField="ind_vendedor" Visible="false"></asp:BoundColumn>
                                                </Columns>
                                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                                <PagerStyle HorizontalAlign="Center"></PagerStyle>
                                            </asp:DataGrid>

                                            <asp:DataGrid ID="dtgRechazo" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center" Visible="false"
                                                ViewStateMode="Enabled" OnItemCommand="dtgRechazo_OnItemCommandClick">
                                                <Columns>
                                                    <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--1--%><asp:BoundColumn DataField="numero_contrato" HeaderText="Operaci&oacuten" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--2--%><asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--3--%><asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--4--%><asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--5--%><asp:BoundColumn DataField="fecha_entrega" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                                    <%--6--%><asp:BoundColumn DataField="fecha_entrega_fin" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                                    <%--7--%><asp:BoundColumn DataField="cantidad_postura" HeaderText="Cantidad Solicitada (KPCD)" ItemStyle-HorizontalAlign="Left" DataFormatString="{0: ###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                                    <%--8--%><asp:BoundColumn DataField="cantidad_adjudicada" HeaderText="Cantidad Adjudicada (KPCD)" ItemStyle-HorizontalAlign="Left" DataFormatString="{0: ###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                                    <%--9--%><asp:BoundColumn DataField="precio_adjudicado" HeaderText="Precio Adjudicaci&oacuten (USD/KPC)" ItemStyle-BackColor=""></asp:BoundColumn>
                                                    <%--10--%><asp:BoundColumn DataField="cantidad_minima" HeaderText="Cantidad M&iacutenima (KPCD)" ItemStyle-HorizontalAlign="Left" DataFormatString="{0: ###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                                    <%--11--%><asp:TemplateColumn HeaderText="Rechazar">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imbRechazar" runat="server" ToolTip="Eliminar Oferta" ImageUrl="~/Images/eliminar.gif" OnClientClick="return confirmarRechazo();"/>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                                <PagerStyle HorizontalAlign="Center"></PagerStyle>
                                            </asp:DataGrid>
                                            <asp:DataGrid ID="dtgContratos" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center" Visible="false"
                                                ViewStateMode="Enabled" OnItemCommand="dtgContratos_OnItemCommandClick">
                                                <Columns>
                                                    <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--1--%><asp:BoundColumn DataField="numero_contrato" HeaderText="Operaci&oacuten" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--2--%><asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--3--%><asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--4--%><asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--5--%><asp:BoundColumn DataField="fecha_entrega" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                                    <%--6--%><asp:BoundColumn DataField="fecha_entrega_fin" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                                    <%--7--%><asp:BoundColumn DataField="cantidad" HeaderText="Cantidad Solicitada (KPCD)" ItemStyle-HorizontalAlign="Left" DataFormatString="{0: ###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                                    <%--9--%><asp:BoundColumn DataField="precio" HeaderText="Precio Adjudicaci&oacuten (USD/KPC)" ItemStyle-BackColor=""></asp:BoundColumn>
                                                    <%--10--%><asp:TemplateColumn HeaderText="Gr&aacutefica">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imbGrafica" runat="server" ToolTip="Curva Agregada" ImageUrl="~/Images/grafica.jpg" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                                <PagerStyle HorizontalAlign="Center"></PagerStyle>
                                            </asp:DataGrid>

                                        </div>
                                        <%--Tabla que se exporta a Excel--%>
                                        <asp:DataGrid ID="dtgSubastaImpresion" runat="server" AutoGenerateColumns="False" Visible="false">
                                            <ItemStyle CssClass="td2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="15px"></ItemStyle>
                                            <Columns>
                                                <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" ItemStyle-HorizontalAlign="Left"
                                                    ItemStyle-Width="60px"></asp:BoundColumn>
                                                <%--1--%><asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"
                                                    ItemStyle-Width="150px"></asp:BoundColumn>
                                                <%--2--%><asp:BoundColumn DataField="desc_unidad_medida" HeaderText="Unidad Medida" ItemStyle-HorizontalAlign="Left"
                                                    ItemStyle-Width="60px"></asp:BoundColumn>
                                                <%--3--%><asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left"
                                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                                <%--4--%><asp:BoundColumn DataField="desc_punto" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"
                                                    ItemStyle-Width="200px"></asp:BoundColumn>
                                                <%--5--%><asp:BoundColumn DataField="desc_periodo" HeaderText="Periodo Entrega" ItemStyle-HorizontalAlign="Left"
                                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                                <%--6--%><asp:BoundColumn DataField="cantidad_total_venta" HeaderText="Cantidad Total Venta (KPCD)"
                                                    DataFormatString="{0: ###,###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right"
                                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                                <%--20201020--%>
                                                <%--7--%><asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-BackColor=""
                                                    ItemStyle-Width="80px"></asp:BoundColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--Modal Suspender--%>
    <div class="modal fade" id="mdlSuspender" tabindex="-1" role="dialog" aria-labelledby="mdlSuspenderLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlInsideSuspender" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlSuspenderLabel">SUSPENDER RUEDA</h5>
                        </div>
                        <div class="modal-body">
                            <h5>
                                <asp:Label ID="lblFechaAct" runat="server"></asp:Label>
                            </h5>

                            <hr>

                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="ddlDefinitivo" runat="server">Suspensi&oacuten Definitiva</asp:Label>
                                        <asp:DropDownList ID="ddlDefinitivo" CssClass="form-control selectpicker" runat="server">
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                            <asp:ListItem Value="S">Si</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="TxtFecha" runat="server">Fecha Pr&oacutexima Apertura</asp:Label>
                                        <asp:TextBox ID="TxtFecha" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="TxtObservacion" runat="server">Observaciones</asp:Label>
                                        <asp:TextBox ID="TxtObservacion" CssClass="form-control" runat="server" MaxLength="1000" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseSuspender_Click" runat="server" />
                            <%--Agregar el confirmar--%>
                            <asp:Button ID="btnSuspender" Text="Suspender" class="btn btn-primary" runat="server" OnClick="btnSuspender_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <%--Modal Reactivar--%>
    <div class="modal fade" id="mdlReactivar" tabindex="-1" role="dialog" aria-labelledby="mdlReactivarLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlInsideReactivar" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>

                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlReactivarLabel">Reactivar Rueda</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h5>
                                <asp:Label ID="lblEstadoReactivar" runat="server"></asp:Label>
                            </h5>

                            <hr>

                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label ID="lblEstado" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hndFase" runat="server" />
                                    </div>
                                </div>
                                <div id="horaComp" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="TxtHoraIniPub" runat="server">Horas Para Publicacion</asp:Label>
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <asp:Label AssociatedControlID="TxtHoraIniPub" runat="server">Hora Inicial</asp:Label>
                                                    <div class="input-group mb-3 date">
                                                        <asp:TextBox ID="TxtHoraIniPub" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="far fa-clock"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <asp:RegularExpressionValidator ID="RevTxtHoraIniPub" ControlToValidate="TxtHoraIniPub" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora de Publicacion"> * </asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <asp:Label AssociatedControlID="TxtHoraFinPub" runat="server">Hora Final</asp:Label>
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="TxtHoraFinPub" CssClass="form-control" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="far fa-clock"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <asp:RegularExpressionValidator ID="revTxtHoraFinPub" ControlToValidate="TxtHoraFinPub" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora final  de Publicacion"> * </asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="horaNeg" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="TxtHoraIniComp" runat="server"> Hora Ingreso de posturas de compra</asp:Label>
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <asp:Label AssociatedControlID="TxtHoraIniComp" runat="server">Hora Inicial</asp:Label>
                                                    <div class="input-group mb-3 date">
                                                        <asp:TextBox ID="TxtHoraIniComp" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="far fa-clock"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <asp:RegularExpressionValidator ID="revTxtHoraIniComp" ControlToValidate="TxtHoraIniComp" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora inicial de Ingreso de posturas de compra"> * </asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <asp:Label AssociatedControlID="TxtHoraFinComp" runat="server">Hora Final</asp:Label>
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="TxtHoraFinComp" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="far fa-clock"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <asp:RegularExpressionValidator ID="revTxtHoraFinComp" ControlToValidate="TxtHoraFinComp" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora final de Ingreso de posturas de compra"> * </asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="TxtHoraIniCalce" runat="server"> Horas Para desarrollo de la subasta</asp:Label>
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <asp:Label AssociatedControlID="TxtHoraIniCalce" runat="server">Hora Inicial</asp:Label>
                                                    <div class="input-group mb-3 date">
                                                        <asp:TextBox ID="TxtHoraIniCalce" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="far fa-clock"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <asp:RegularExpressionValidator ID="revTxtHoraIniCalce" ControlToValidate="TxtHoraIniCalce" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora de calce"> * </asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <asp:Label AssociatedControlID="TxtHoraFinCalce" runat="server">Hora Final</asp:Label>
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="TxtHoraFinCalce" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="far fa-clock"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <asp:RegularExpressionValidator ID="revTxtHoraFinCalce" ControlToValidate="TxtHoraFinCalce" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora final  calce"> * </asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="TxtHoraIniRechazo" runat="server"> Horas Para Rechazo de Adjudicacines</asp:Label>
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <asp:Label AssociatedControlID="TxtHoraIniRechazo" runat="server">Hora Inicial</asp:Label>
                                                    <div class="input-group mb-3 date">
                                                        <asp:TextBox ID="TxtHoraIniRechazo" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="far fa-clock"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <asp:RegularExpressionValidator ID="revTxtHoraIniRechazo" ControlToValidate="TxtHoraIniRechazo" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora de calce"> * </asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <asp:Label AssociatedControlID="TxtHoraFinRechazo" runat="server">Hora Final</asp:Label>
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="TxtHoraFinRechazo" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="far fa-clock"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <asp:RegularExpressionValidator ID="revTxtHoraFinRechazo" ControlToValidate="TxtHoraFinRechazo" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora final  calce"> * </asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="TxtHoraIniPubResult" runat="server"> Horas Para publicaci&oacuten de resultados</asp:Label>
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <asp:Label AssociatedControlID="TxtHoraIniPubResult" runat="server">Hora Inicial</asp:Label>
                                                    <div class="input-group mb-3 date">
                                                        <asp:TextBox ID="TxtHoraIniPubResult" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="far fa-clock"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <asp:RegularExpressionValidator ID="revTxtHoraIniPubResult" ControlToValidate="TxtHoraIniPubResult" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora de calce"> * </asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <asp:Label AssociatedControlID="TxtHoraFinPubResult" runat="server">Hora Final</asp:Label>
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="TxtHoraFinPubResult" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="far fa-clock"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <asp:RegularExpressionValidator ID="revTxtHoraFinPubResult" ControlToValidate="TxtHoraFinPubResult" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora final  calce"> * </asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="TxtHoraIniFinaliza" runat="server"> Horas Para finalizaci$oacuten</asp:Label>
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <asp:Label AssociatedControlID="TxtHoraIniFinaliza" runat="server">Hora Inicial</asp:Label>
                                                    <div class="input-group mb-3 date">
                                                        <asp:TextBox ID="TxtHoraIniFinaliza" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="far fa-clock"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <asp:RegularExpressionValidator ID="revTxtHoraIniFinaliza" ControlToValidate="TxtHoraIniFinaliza" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora de calce"> * </asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <asp:Label AssociatedControlID="TxtHoraFinFinaliza" runat="server">Hora Final</asp:Label>
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="TxtHoraFinFinaliza" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="far fa-clock"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <asp:RegularExpressionValidator ID="revTxtHoraFinFinaliza" ControlToValidate="TxtHoraFinFinaliza" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora final  calce"> * </asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="TxtObservacionReac" runat="server">Observaciones</asp:Label>
                                        <asp:TextBox ID="TxtObservacionReac" CssClass="form-control" runat="server" MaxLength="1000" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
                                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseReactivar_Click" runat="server" />
                            <%--Agregar el confirmar--%>
                            <asp:Button ID="Button1" Text="Reactivar" CssClass="btn btn-primary" ValidationGroup="comi" CausesValidation="true" OnClick="btnReactivar_Click" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
        </div>
    </div>

    <%--Modal Ingreso Posturas de Compra--%>
    <div class="modal fade" id="mdlIngresoPosturaCompra" tabindex="-1" role="dialog" aria-labelledby="mdlIngresoPosturaCompraLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlIngresoPosturaCompraInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlIngresoPosturaCompraLabel">Ingreso Posturas de Compra</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblRueda" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblId" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblFecha" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblPuntoEntrega" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblPeridoEnt" runat="server"></asp:Label>

                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblModalidad" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Precio (USD/KPC)" AssociatedControlID="TxtPrecio" runat="server" />
                                        <asp:TextBox ID="TxtPrecio" CssClass="form-control" runat="server" onkeyup="return formatoMilesPre(this,event,2);"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftebxtPrecio" runat="server" TargetControlID="TxtPrecio" FilterType="Custom, Numbers" ValidChars=".," />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Capacidad Demandada (KPCD)" AssociatedControlID="TxtCantidad" runat="server" />
                                        <asp:TextBox ID="TxtCantidad" CssClass="form-control" runat="server" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtCantidad" runat="server" TargetControlID="TxtCantidad" FilterType="Custom, Numbers" ValidChars="," />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Demanda M&iacutenima (KPCD)" AssociatedControlID="TxtCantidadMin" runat="server" />
                                        <asp:TextBox ID="TxtCantidadMin" CssClass="form-control" runat="server" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtCantidadMin" runat="server" TargetControlID="TxtCantidadMin" FilterType="Custom, Numbers" ValidChars="," />
                                    </div>
                                </div>
                            </div>

                            <asp:HiddenField ID="hndID" runat="server" />
                            <asp:HiddenField ID="hdfNoRueda" runat="server" />
                            <asp:HiddenField ID="hdfMaxPostCompra" runat="server" />
                            <asp:HiddenField ID="hndFechaRueda" runat="server" />
                            <asp:HiddenField ID="hndAccion" runat="server" />
                            <asp:HiddenField ID="hndCntTotal" runat="server" />
                        </div>
                        <div class="modal-footer">
                            <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseIngresoPosturasCompra_Click" runat="server" />
                            <asp:Button ID="btnOfertar" Text="Crear" OnClientClick="return confirmarDemanda();" CssClass="btn btn-primary" OnClick="btnOfertar_Click" runat="server" />
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>


    <%--Modal Curva de Oferta y Demanda Agregada--%>
    <div class="modal fade" id="mdlCurvaOfertaDemAgre" tabindex="-1" role="dialog" aria-labelledby="mdlCurvaOfertaDemAgreLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlCurvaOfertaDemAgreInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlCurvaOfertaDemAgreLabel">Curva de Oferta y Demanda Agregada</h5>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <h5>
                                <asp:Label ID="lblPuntoCurva" runat="server" Font-Bold="true"></asp:Label>
                            </h5>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <div id="chart_div" style="width: 700px; height: 500px; display: block; margin: 0 auto !important;"></div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="table table-responsive">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <asp:DataGrid ID="dtgInformacion" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                            ViewStateMode="Enabled" OnItemCommand="OnItemCommand_Click">
                                            <Columns>
                                                <asp:BoundColumn DataField="precio" HeaderText="Precio" DataFormatString="{0: ###,###,##0.00}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="cantidad_venta" HeaderText="Cantidad Venta" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,##0}"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="cantidad_compra" HeaderText="Cantidad Compra" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,##0}"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="cantidad_min" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="cantidad_max" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="precio_min" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="precio_max" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="precio_venta" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="precio_compra" Visible="false"></asp:BoundColumn>
                                                <%--20210224--%>
                                                <asp:BoundColumn DataField="grafica_compra" Visible="false"></asp:BoundColumn>
                                            </Columns>
                                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                                        </asp:DataGrid>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseCurvaOferDemAgre_Click" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%--Modal Enviar Mensaje--%>
    <div class="modal fade" id="mdlEnviarMensaje" tabindex="-1" role="dialog" aria-labelledby="mdlEnviarMensajeLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlEnviarMensajeInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlEnviarMensajeLabel">Envio de Mensajes</h5>
                </div>
                <div class="modal-body">
                    <h5>
                        <asp:Label ID="Label4" runat="server"></asp:Label>
                    </h5>

                    <hr>

                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <asp:Label ID="lblTitulo" runat="server" AssociatedControlID="TxtMensaje" Text="Envio de Mensajes" Font-Bold="true"></asp:Label>
                                        <asp:TextBox ID="TxtMensaje" CssClass="form-control" runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <asp:UpdatePanel UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseEnviarMensaje_Click" runat="server" />
                            <asp:Button ID="Button4" Text="Aceptar" CssClass="btn btn-primary" runat="server" OnClientClick="return confirmar();" OnClick="btnMensaje_Click" ValidationGroup="comi" CausesValidation="true" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%--Modal Mensajes--%>
    <div class="modal fade" id="mdlMensaje" tabindex="-1" role="dialog" aria-labelledby="mdlMensajeLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlMensajeInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlMensajeLabel">Mis Mensajes</h5>
                </div>
                <div class="modal-body">
                    <h5>
                        <asp:Label ID="Label6" runat="server"></asp:Label>
                    </h5>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <div class="table table-responsive">
                                    <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                        <ContentTemplate>
                                            <asp:Literal ID="ltTableroRF" Mode="Transform" runat="server"></asp:Literal>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:UpdatePanel ID="UpdatePanel6" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseMensajes_Click" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%--Modal Contratos--%>
    <div class="modal fade" id="mdlContratos" tabindex="-1" role="dialog" aria-labelledby="mdlContratosLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlContratosInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlContratosLabel">Mis Contratos</h5>
                </div>
                <div class="modal-body">
                    <h5>
                        <asp:Label ID="Label5" runat="server"></asp:Label>
                    </h5>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <div class="table table-responsive">
                                    <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                        <ContentTemplate>
                                            <asp:Literal ID="ltTablero" Mode="Transform" runat="server"></asp:Literal>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:UpdatePanel ID="UpdatePanel15" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseContratos_Click" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
