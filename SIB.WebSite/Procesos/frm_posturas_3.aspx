﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_posturas_3.aspx.cs" Inherits="Procesos_frm_posturas_3"
    EnableEventValidation="false" MasterPageFile="~/PlantillaPrincipal.master"%>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" language="javascript">

        window.onload = function() {
            var pos = window.name || 0;
            window.scrollTo(0, pos);
        }
        window.onunload = function() {
            window.name = self.pageYOffset || (document.documentElement.scrollTop + document.body.scrollTop);
        }

    </script>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="divOferta" runat="server" style="position: absolute; left: 0; top: 0; width: 100%;">
                <table cellpadding="0" cellspacing="0" bgcolor="#000000">
                    <tr>
                        <td align="center">
                            <asp:DataGrid ID="dtgSubasta1" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1" runat="server" OnItemCommand="OnItemCommand_Click"
                                ShowHeader="false">
                                <ItemStyle CssClass="td2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="15px">
                                </ItemStyle>
                                <Columns>
                                    <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="70px"></asp:BoundColumn>
                                    <%--1--%><asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--2--%><asp:BoundColumn DataField="desc_unidad_medida" HeaderText="UM" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="60px"></asp:BoundColumn>
                                    <%--3--%><asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--4--%><asp:BoundColumn DataField="desc_punto" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="200px"></asp:BoundColumn>
                                    <%--5--%><asp:BoundColumn DataField="desc_periodo" HeaderText="Periodo Entrega" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--6--%><asp:BoundColumn DataField="cantidad_total_venta" HeaderText="Cantidad Vta"
                                        DataFormatString="{0: ###,###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--7--%><asp:BoundColumn DataField="precio_venta" HeaderText="precio Vta" ItemStyle-BackColor=""
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--8--%><asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-BackColor=""
                                        ItemStyle-Width="120px"></asp:BoundColumn>
                                    <%--9--%><asp:TemplateColumn HeaderText="Ofr" ItemStyle-Width="45px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbOfertar" runat="server" ToolTip="Ofertar" ImageUrl="~/Images/nuevo.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--10--%><asp:TemplateColumn HeaderText="Mod" ItemStyle-Width="45px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbModificar" runat="server" ToolTip="Modificar Oferta" ImageUrl="~/Images/modificar.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--11--%><asp:TemplateColumn HeaderText="Elim" ItemStyle-Width="45px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbEliminar" runat="server" ToolTip="Eliminar Oferta" ImageUrl="~/Images/eliminar.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--12--%><asp:TemplateColumn HeaderText="graf" Visible="false" ItemStyle-Width="45px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbGrafica" runat="server" ToolTip="curva agregada" ImageUrl="~/Images/grafica.jpg" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--13--%><asp:BoundColumn DataField="oferta_ya_realizada" Visible="false" ItemStyle-Width="50px">
                                    </asp:BoundColumn>
                                    <%--14--%><asp:BoundColumn DataField="max_posturas" Visible="false" ItemStyle-Width="50px">
                                    </asp:BoundColumn>
                                    <%--15--%><asp:BoundColumn DataField="ind_vendedor" Visible="false" ItemStyle-Width="50px">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                            <asp:HiddenField ID="hndCodSubasta" runat="server" />
                            <asp:HiddenField ID="hndId" runat="server" />
                            <asp:HiddenField ID="hndSubya" runat="server" />
                            <asp:HiddenField ID="hndCiudad" runat="server" />
                            <asp:HiddenField ID="hndPunta" runat="server" />
                            <asp:HiddenField ID="HndFechaRueda" runat="server" />
                            <asp:HiddenField ID="hndParcial" runat="server" />
                            <asp:HiddenField ID="hndTipo" runat="server" />
                            <asp:HiddenField ID="hndPosScroll" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
