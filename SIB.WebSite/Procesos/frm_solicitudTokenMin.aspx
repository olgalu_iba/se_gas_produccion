﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_solicitudTokenMin.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Procesos.frm_solicitudTokenMin" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">

                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Operación" AssociatedControlID="TxtFechaOper" runat="server" />
                            <asp:TextBox ID="TxtFechaOper" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ValidationGroup="detalle" Width="100%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Número Operación" AssociatedControlID="TxtOperacion" runat="server" />
                            <asp:TextBox ID="TxtOperacion" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server" id="tblGrilla" visible="false">
                    <div class="table table-responsive">
                        <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <%--0--%>
                                <asp:TemplateColumn HeaderText="Sele.">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="ChkTodos" runat="server" OnCheckedChanged="ChkTodos_CheckedChanged"
                                            AutoPostBack="true" Text="Seleccionar Todos" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="ChkSeleccionar" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <%--1--%>
                                <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--2--%>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--3--%>
                                <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--4--%>
                                <asp:BoundColumn DataField="fecha_contrato" HeaderText="Fecha Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--5--%>
                                <asp:BoundColumn DataField="operador_venta" HeaderText="Operador Venta" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px"></asp:BoundColumn>
                                <%--6--%>
                                <asp:BoundColumn DataField="operador_compra" HeaderText="Operador Compra" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px"></asp:BoundColumn>
                                <%--7--%>
                                <asp:BoundColumn DataField="fecha_entrega_ini" HeaderText="Fecha Entrega Inicial" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--8--%>
                                <asp:BoundColumn DataField="fecha_entrega_fin" HeaderText="Fecha Entrega Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--9--%>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--10--%>
                                <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                                <%--11--%>
                                <asp:BoundColumn DataField="tiene_token" Visible="false"></asp:BoundColumn>
                                <%--12--%>
                                <asp:BoundColumn DataField="tiene_proceso" Visible="false"></asp:BoundColumn>
                                <%--13--%>
                                <asp:BoundColumn DataField="tiene_firma" Visible="false"></asp:BoundColumn>
                                <%--14--%>
                                <asp:BoundColumn DataField="estado" Visible="false"></asp:BoundColumn>
                                <%--15--%>
                                <asp:BoundColumn DataField="desc_estado" HeaderText="Estado"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <asp:HiddenField ID="hdfOperador" runat="server" />
    <asp:HiddenField ID="hdfRutaMinuta" runat="server" />
    <asp:HiddenField ID="hdfBusOper" runat="server" />

</asp:Content>
