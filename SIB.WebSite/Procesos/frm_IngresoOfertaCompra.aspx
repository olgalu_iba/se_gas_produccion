﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_IngresoOfertaCompra.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_IngresoOfertaCompra" EnableEventValidation="false" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">
        //        document.location = no;
        function confirmar() {
            return confirm("Esta seguro que desea ofertar!")
        }
        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }

        function formatoMiles(obj, evt) {
            var T;
            var strValue = removeCommas(obj.value);
            //if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
            if ((T = /([0-9]*)$/i.exec(strValue)) != null) {
                //                T[4] = T[4].substr(0, decimales);
                //                if (T[2] == '' && T[3] == '.') T[2] = 0;
                obj.value = addCommas(T[1]);
            } else {
                alert('formato invalido ');
            }
        }
        function formatoMilesPre(obj, evt, decimales) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
                T[4] = T[4].substr(0, decimales);
                if (T[2] == '' && T[3] == '.') {
                    T[2] = 0;
                }
                obj.value = T[1] + addCommas(T[2]) + T[3] + T[4];
            }
            else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }

    </script>
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTituo" Text="INGRESO POSTURAS DE COMPRA" runat="server" />

                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblRueda" runat="server"></asp:Label>

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblFecha" runat="server"></asp:Label>

                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblProducto" runat="server"></asp:Label>

                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblUnidadMedida" runat="server"></asp:Label>


                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblPuntoEntrega" runat="server"></asp:Label>

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblPeridoEnt" runat="server"></asp:Label>
                            <asp:Label ID="lblModalidad" runat="server"></asp:Label>
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red" Font-Bold="true" Text="Los Precios de Compra deben ser Ingresados de mayor a menor."
                                Font-Size="14px"></asp:Label>
                            <asp:Label ID="lblMensaje1" runat="server" ForeColor="Red" Font-Bold="true" Text="Las cantidades a Comprar deben ser Ingresados de menor a mayor."
                                Font-Size="14px"></asp:Label>

                        </div>
                    </div>
                </div>
                <asp:DataGrid ID="dtgPosturasC" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                    HeaderStyle-CssClass="th1">
                    <Columns>
                        <asp:BoundColumn DataField="codigo_postura" HeaderText="No. Postura" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Precio">
                            <ItemTemplate>
                                <asp:TextBox ID="TxtPrecio" runat="server" Width="100px" onkeyup="return formatoMilesPre(this,event,2);"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Cantidad">
                            <ItemTemplate>
                                <asp:TextBox ID="TxtCantidad" runat="server" Width="100px" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="cantidad_postura" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="precio_postura" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="numero_postura" Visible="false"></asp:BoundColumn>
                    </Columns>
                    <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                </asp:DataGrid>

                <asp:Button ID="btnOfertar" Text="Crear" runat="server" OnClientClick="return confirmar();"
                    OnClick="btnOfertar_Click" />
                <asp:HiddenField ID="hndID" runat="server" />
                <asp:HiddenField ID="hdfNoRueda" runat="server" />
                <asp:HiddenField ID="hdfMaxPostCompra" runat="server" />
                <asp:HiddenField ID="hndFechaRueda" runat="server" />
                <asp:HiddenField ID="hndAccion" runat="server" />
                <asp:HiddenField ID="hndCntTotal" runat="server" />

            </div>
        </div>

    </div>


</asp:Content>
