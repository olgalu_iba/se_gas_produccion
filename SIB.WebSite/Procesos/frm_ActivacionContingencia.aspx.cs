﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Procesos_frm_ActivacionContingencia : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Activación Contingencia";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            // Validacion para permitir al Usuario activa la contingencia.
            string lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "/" + DateTime.Now.ToShortDateString().Substring(3, 2) + "/" + DateTime.Now.ToShortDateString().Substring(0, 2);
            lblTitulo.Text = lsTitulo;
            if (!DelegadaBase.Servicios.ValidarExistencia("m_autorizado_contingencia", " login_autorizado = '" + goInfo.Usuario + "' And estado = 'A' And Cast('" + lsFecha + "' as datetime) >= fecha_inicial And Cast('" + lsFecha + "' as datetime) <= fecha_final ", goInfo))
            {
                lblMensaje.Text = "El usuario NO esta Autorizado para activar Contingencia en la Fecha Actual.!";
                hlkNuevo.Enabled = false;
                tblCaptura.Visible = false;
            }
            else
            {
                // Carga informacion de combos
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, DdlPeril, "a_grupo_usuario", " estado ='A' And tipo_perfil = 'N' order by descripcion ", 0, 1);
                LlenarControles(lConexion.gObjConexion, DdlBusPerfil, "a_grupo_usuario", " estado ='A' And tipo_perfil = 'N' order by descripcion", 0, 1);
                LlenarControles(lConexion.gObjConexion, DdlBusOperador, "m_operador", " estado ='A'  order by razon_social", 0, 4);
                lConexion.Cerrar();

                //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
                if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
                {
                    lsIndica = this.Request.QueryString["lsIndica"].ToString();
                }
                if (lsIndica == null || lsIndica == "" || lsIndica == "L")
                {
                    Listar();
                }
                else if (lsIndica == "N")
                {
                    Nuevo();
                }
                else if (lsIndica == "B")
                {
                    Buscar();
                }
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "t_activacion_contingencia");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoActriva.Visible = false;
        LblCodigoActiva.Visible = true;
        LblCodigoActiva.Text = "Automatico";
        DdlPeril.Enabled = true;
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_grupo_contingencia", "@P_codigo_operador_contingencia" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { "0", "0" };

        try
        {
            lValorParametros[0] = DdlBusPerfil.SelectedValue;
            lValorParametros[1] = DdlBusOperador.SelectedValue;
            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetActivacionContingencia", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_activacion_contingencia", "@P_codigo_grupo_contingencia", "@P_codigo_operador_contingencia", "@P_duracion_minutos", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { "0", "0", "0", "0", "1" };
        lblMensaje.Text = "";
        int liValor;

        try
        {
            if (DdlPeril.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el Perfil de la Contingencia<br>";
            if (DdlPeril.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el Operador de la Contingencia<br>";

            if (VerificarExistencia(" estado = 'A' And login_activa_contingencia = '" + goInfo.Usuario + "' "))
                lblMensaje.Text += " Ya se tiene una Contingencia Activa, por favor intente mas tarde<br>";
            if (TxtTiempo.Text.Trim().Length > 0)
            {
                try
                {
                    liValor = Convert.ToInt32(TxtTiempo.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor Inválido en Tiempo Duración<br>";
                }
            }
            else
                lblMensaje.Text += " Debe Ingresar el Tiempo de Duración<br>";
            if (lblMensaje.Text == "")
            {
                lValorParametros[1] = DdlPeril.SelectedValue;
                lValorParametros[2] = DdlOperador.SelectedValue;
                lValorParametros[3] = TxtTiempo.Text.Trim();
                lConexion.Abrir();
                DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetActivaContingencia", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensaje.Text = "Se presento un Problema en la Creación del registro.! " + goInfo.mensaje_error;
                    lConexion.Cerrar();
                }
                else
                {
                    lConexion.Cerrar();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Activación Contingencia Ejecutada Correctamente.!" + "');", true);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        lblMensaje.Text = "";
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        /// se quita la opción  finalizar la contingencia Req. 006-17 201070303 ///
        //if (((LinkButton)e.CommandSource).Text == "Eliminar")
        //{
        //    lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
        //    if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[8].Text == "A")
        //    {
        //        string[] lsNombreParametros = { "@P_codigo_activacion_contingencia", "@P_codigo_grupo_contingencia", "@P_codigo_operador_contingencia", "@P_duracion_minutos", "@P_accion" };
        //        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
        //        string[] lValorParametros = { lCodigoRegistro, "0", "0", "0", "3" };
        //        lConexion.Abrir();
        //        DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetActivaContingencia", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
        //        if (goInfo.mensaje_error != "")
        //        {
        //            lblMensaje.Text = "Se presento un Problema en la Eliminación de la Contingencia.! " + goInfo.mensaje_error;
        //            lConexion.Cerrar();
        //        }
        //        else
        //        {
        //            lConexion.Cerrar();
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Eliminación Contingencia Ejecutada Correctamente.!" + "');", true);
        //            Listar();
        //        }
        //    }
        //    else
        //        lblMensaje.Text = "No se puede eliminar un Registro de Contingencia que no este Activo.!";
        //}
        /////////////////////////////////////////////////////////////////////////
        /// Nueva opcion para finalizar la contingencia Req. 006-17 201070303 ///
        /////////////////////////////////////////////////////////////////////////
        if (((LinkButton)e.CommandSource).Text == "Finalizar Contingencia")
        {
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[8].Text == "A")
            {
                string[] lsNombreParametros = { "@P_codigo_activacion_contingencia", "@P_codigo_grupo_contingencia", "@P_codigo_operador_contingencia", "@P_duracion_minutos", "@P_accion" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { lCodigoRegistro, "0", "0", "0", "4" };
                lConexion.Abrir();
                DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetActivaContingencia", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensaje.Text = "Se presento un Problema en la Finalización de la Contingencia.! " + goInfo.mensaje_error;
                    lConexion.Cerrar();
                }
                else
                {
                    lConexion.Cerrar();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Finalización Contingencia Ejecutada Correctamente.!" + "');", true);
                    Listar();
                }
            }
            else
                lblMensaje.Text = "No se puede finalizar un Registro de Contingencia que no este Activo.!";
        }
        /////////////////////////////////////////////////////////////////////////
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("t_activacion_contingencia", lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            if (lsTabla != "a_grupo_usuario")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["razon_social"].ToString() + " - " + lLector["codigo_operador"].ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["descripcion"].ToString() + " - " + lLector["tipo_operador"].ToString();
            }
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// <summary>
    /// Metodo nuevo para llenar el combo de Operador Contingencia de a CUerdo al Perfil. Req. 006-17 20170303
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DdlPeril_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (DdlPeril.SelectedValue != "0")
            {
                string[] lsPerfil;
                lsPerfil = DdlPeril.SelectedItem.ToString().Split('-');
                lConexion.Abrir();
                DdlOperador.Items.Clear();
                LlenarControles(lConexion.gObjConexion, DdlOperador, "m_operador", " estado ='A' And tipo_operador = '" + lsPerfil[1].Trim() + "'  order by razon_social", 0, 4);
                lConexion.Cerrar();
            }
            else
                DdlOperador.Items.Clear();
        }
        catch (Exception ex)
        {

        }
    }
}