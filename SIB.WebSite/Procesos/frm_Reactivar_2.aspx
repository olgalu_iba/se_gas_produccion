﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_Reactivar_2.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_Reactivar_2" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">
        //        document.location = no;
        function confirmar() {
            return confirm("Esta seguro que desea reactivar la rueda!")
        }

    </script>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td6" align="center" colspan="4">
                        <asp:Label ID="lblTitulo" runat="server" Text="REACTIVAR RUEDA" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="4">
                        <asp:Label ID="lblEstado" runat="server"></asp:Label>
                        <asp:HiddenField ID="hndFase" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="td6" style="width: 20%">
                        Horas Declaracion de Informacion
                    </td>
                    <td class="td6" style="width: 30%">
                        <asp:TextBox ID="txtHoraIniPubV" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revtxtHoraIniPubV" ControlToValidate="txtHoraIniPubV"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora de Declaracion de Informacion"> * </asp:RegularExpressionValidator>
                        -
                        <asp:TextBox ID="txtHoraFinPubV" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revtxtHoraFinPubV" ControlToValidate="txtHoraFinPubV"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora final  de Declaracion de Informacion"> * </asp:RegularExpressionValidator>
                    </td>
                    <td class="td6" style="width: 20%">
                        Horas Para Declaracion precio de reserva
                    </td>
                    <td class="td6" style="width: 30%">
                        <asp:TextBox ID="txtHoraIniCntD" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revtxtHoraIniCntD" ControlToValidate="txtHoraIniCntD"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora de inicio de Declaracion precio de reserva"> * </asp:RegularExpressionValidator>
                        -
                        <asp:TextBox ID="txtHoraFinCntD" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revtxtHoraFinCntD" ControlToValidate="txtHoraFinCntD"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora final  de Declaracion precio de reserva"> * </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="td6">
                        Horas Para Publicacion
                    </td>
                    <td class="td6">
                        <asp:TextBox ID="txtHoraIniComp" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RevtxtHoraIniComp" ControlToValidate="txtHoraIniComp"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora de Publicacion"> * </asp:RegularExpressionValidator>
                        -
                        <asp:TextBox ID="txtHoraFinComp" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revtxtHoraFinComp" ControlToValidate="txtHoraFinComp"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora final  de Publicacion"> * </asp:RegularExpressionValidator>
                    </td>
                    <td class="td6">
                        Hora Ingreso de posturas de compra
                    </td>
                    <td class="td6">
                        <asp:TextBox ID="txtHoraIniNeg" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revtxttxtHoraIniNeg" ControlToValidate="txtHoraIniNeg"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora inicial de Ingreso de posturas de compra"> * </asp:RegularExpressionValidator>
                        -
                        <asp:TextBox ID="txtHoraFinNeg" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revtxtHoraFinNeg" ControlToValidate="txtHoraFinNeg"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora final de Ingreso de posturas de compra"> * </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="td6">
                        Horas Para calce
                    </td>
                    <td class="td6">
                        <asp:TextBox ID="txtHoraIniCalce" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revtxtHoraIniCalce" ControlToValidate="txtHoraIniCalce"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora de calce"> * </asp:RegularExpressionValidator>
                        -
                        <asp:TextBox ID="txtHoraFinCalce" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revtxtHoraFinCalce" ControlToValidate="txtHoraFinCalce"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora final  calce"> * </asp:RegularExpressionValidator>
                    </td>
                    <td class="td6">
                        Hora Modificacion contratos
                    </td>
                    <td class="td6">
                        <asp:TextBox ID="txtHoraIniCont" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revtxtHoraIniCont" ControlToValidate="txtHoraIniCont"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora inicial de modificacion de contratos"> * </asp:RegularExpressionValidator>
                        -
                        <asp:TextBox ID="txtHoraFinCont" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revtxtHoraFinCont" ControlToValidate="txtHoraFinCont"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora final de modificacion de contratos"> * </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="4">
                        Observaciones:
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="4">
                        <asp:TextBox ID="TxtObservacion" runat="server" Width="500px" MaxLength="1000" Rows="3"
                            TextMode="MultiLine"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" class="td6" colspan="4">
                        <br />
                        <asp:Button ID="btnReactivar" Text="Reactivar" runat="server" OnClientClick="return confirmar();"
                            OnClick="btnReactivar_Click" ValidationGroup="comi" CausesValidation="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center" class="td6">
                        <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
 </asp:Content>