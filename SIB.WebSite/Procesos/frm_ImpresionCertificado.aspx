﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ImpresionCertificado.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_ImpresionCertificado" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
                    width="90%">
                    <tr>
                        <td align="center" class="th1">
                            <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
                    width="90%">
                    <tr>
                        <td class="td1">
                            Fecha Corte
                        </td>
                        <td class="td1">
                            Subasta
                        </td>
                        <td class="td1">
                            Tipo Rueda
                        </td>
                        <td class="td1">
                            Fecha Contrato
                        </td>
                        <td class="td1">Número Rueda <%--20180126 rq107-16--%>
                        </td>
                        <td class="td1">
                            Operador
                        </td>
                    </tr>
                    <tr>
                        <%--20171130 rq026-17--%>
                        <td class="td2">
                            <asp:TextBox ID="TxtFechaCorte" runat="server" ValidationGroup="detalle"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="ceTxtFechaCorte" runat="server" TargetControlID="TxtFechaCorte"
                                Format="yyyy/MM/dd">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlSubasta" runat="server" OnSelectedIndexChanged="ddlSubasta_SelectedIndexChanged"
                                AutoPostBack="true" Width="220px">
                            </asp:DropDownList>
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlTipoRueda" runat="server" Width="150px">
                            </asp:DropDownList>
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtFechaIni" runat="server" ValidationGroup="detalle"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="CeTxtFechaIni" runat="server" TargetControlID="TxtFechaIni"
                                Format="yyyy/MM/dd">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtNoRueda" runat="server" ValidationGroup="detalle"></asp:TextBox>
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlOperador" runat="server" Width="220px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="90%" runat="server"
        id="tblMensaje">
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
                <asp:ImageButton ID="imbGenerar" runat="server" ImageUrl="~/Images/Crear.png" Height="40"
                    OnClick="imbGenerar_Click" />
                <%--20180126 rq107-16--%>
                                &nbsp;&nbsp;&nbsp;&nbsp; 
                                <%--20180126 rq107-16--%>
                    <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                        Visible="false" Height="35" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla" visible="false">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 1050px; height: 350px;">
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Sele.">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ChkTodos" runat="server" OnCheckedChanged="ChkTodos_CheckedChanged"
                                        AutoPostBack="true" Text="Seleccionar Todos" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="ChkSeleccionar" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="No. Contrato" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_id" HeaderText="No. Id" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_rueda" HeaderText="No. Rueda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_contrato" HeaderText="Fecha Contrato" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_venta" HeaderText="Operador Venta" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="200px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_compra" HeaderText="Operador Compra" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="200px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="tipo_rueda" HeaderText="Tipo  Rueda" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="200px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_entrega" HeaderText="Fecha Entrega" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="unidad_medida" HeaderText="Unidad de Medida" Visible="false">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="punto_entrega" HeaderText="Punto de Entrega" Visible="false">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="modalidad" HeaderText="Modalidad de Entrega" Visible="false">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="periodo_entrega" HeaderText="Periodo de Entrega" Visible="false">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Postura" Visible="false">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="Fecha Hora Postura" Visible="false">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <%--20180126 rq107-16--%>
                        <asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False" Visible="false">
                            <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                            <ItemStyle CssClass="td2"></ItemStyle>
                            <Columns>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="No. Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numero_id" HeaderText="No. Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numero_rueda" HeaderText="No. Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_contrato" HeaderText="Fecha Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_venta" HeaderText="Operador Venta" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="200px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_compra" HeaderText="Operador Compra" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="200px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tipo_rueda" HeaderText="Tipo  Rueda" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="200px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_entrega" HeaderText="Fecha Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
