﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.DataVisualization.Charting;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Drawing;

public partial class Procesos_frm_CurvaAgregada : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = "Curva de oferta y demanda agregada";
        lblMensaje.Text = "";
        try
        {
            //lblNoId.Text = this.Request.QueryString["ID"];
            string lblNoId = this.Request.QueryString["ID"];

            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            SqlCommand lComando1 = new SqlCommand();
            SqlDataAdapter lsqldata1 = new SqlDataAdapter();
            lComando1.Connection = lConexion.gObjConexion;
            lComando1.CommandTimeout = 3600;
            lComando1.CommandType = CommandType.StoredProcedure;

            //cambios grafica 20160202
            SqlDataReader lLector;
            lComando1.CommandText = "pa_ValidarExistencia";
            lComando1.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = "t_id_rueda";
            lComando1.Parameters.Add("@P_Filtro", SqlDbType.VarChar).Value = "numero_id = " + lblNoId;
            lLector = lComando1.ExecuteReader();
            if (lLector.HasRows)
            {
                lLector.Read();
                if (lLector["codigo_producto"].ToString() == "1")
                    lblPunto.Text = "Punto de Entrega: " + lLector["desc_punto_entrega"].ToString();
                else
                    lblPunto.Text = "Ruta: " + lLector["desc_punto_entrega"].ToString() + "-" + lLector["desc_punto_fin"].ToString();
            }
            else
                lblPunto.Text = "";
            lComando1.Parameters.Clear();
            lLector.Close();
            lLector.Dispose();

            lComando1.CommandText = "pa_GetPreciosCurva1";
            lComando1.Parameters.Add("@P_numero_id", SqlDbType.Int).Value = lblNoId;
            lComando1.ExecuteNonQuery();
            lsqldata1.SelectCommand = lComando1;
            lsqldata1.Fill(lds);
            dtgInformacion.DataSource = lds;
            dtgInformacion.DataBind();
            lConexion.Cerrar();
            /// Recorro la grilla de los Datos para Armar la Grafica
            double Xval = 0;
            double Xval1 = 0;
            double Yval = 0;
            int liReg = 0;
            double minX = 0;
            double xAnt = 0;
            double yUlt = 0;
            double maxX = 0;
            double minY = 0;
            double maxY = 0;
            double minX1 = 0;
            double maxX1 = 0;
            double minY1 = 0;
            double maxY1 = 0;
            double porc = 0.1;
            int liRegC = 0;
            int liRegV = 0;
            grfCurva.Series["Venta"].Points.AddXY(0, 0);
            foreach (DataGridItem Grilla in this.dtgInformacion.Items)
            {
                Xval = Convert.ToDouble(Grilla.Cells[1].Text);
                Xval1 = Convert.ToDouble(Grilla.Cells[2].Text);
                Yval = Math.Round(Convert.ToDouble(Grilla.Cells[0].Text),2);

                if (liReg == 0)
                {
                    //minX = Convert.ToDouble(Grilla.Cells[3].Text); ;
                    minX = 0;
                    maxX = Convert.ToDouble(Grilla.Cells[4].Text); ;
                    //minY = Convert.ToDouble(Grilla.Cells[5].Text) * 0.9;
                    minY = 0;
                    maxY = Convert.ToDouble(Grilla.Cells[6].Text); ;
                    minX1 = minX - Math.Round((maxX - minX) * porc);
                    if (minX1 < 0)
                        minX1 = 0;
                    maxX1 = maxX + Math.Round((maxX - minX) * porc);
                    if (maxX1 < 0)
                        maxX1 = 0;
                    minY1 = minY - Math.Round((maxY - minY) * porc);
                    if (minY1 < 0)
                        minY1 = 0;
                    maxY1 = maxY + Math.Round((maxY - minY) * porc);
                    if (maxY1 < 0)
                        maxY1 = 0;
                    if (maxX1 == minX)
                        maxX1 = maxX1 + 1;
                    if (maxY1 == minY)
                        maxY1 = maxY1 + 1;
                    //grfCurva.Series["Venta"].Points.AddXY(minX1, Yval);
                    xAnt = minX1;
                    //grfCurva.Series["Compra"].Points.AddXY(Xval1, minY1);
                }
                liReg++;
                if (xAnt != Xval)
                {
                    grfCurva.Series["Venta"].Points.AddXY(xAnt, Yval);
                    xAnt = Xval;
                }
                if (Grilla.Cells[7].Text == "S" )
                {
                    if (liRegV==0)
                        grfCurva.Series["Venta"].Points.AddXY(0, Yval);
                    liRegV++;
                    grfCurva.Series["Venta"].Points.AddXY(Xval, Yval);
                    grfCurva.Series["VentaP"].Points.AddXY(Xval, Yval);
                    grfCurva.Series["VentaP"].ToolTip = "Precio:#VALY, Cantidad:#VALX";
                }
                if (Grilla.Cells[8].Text == "S")
                {
                    if (liRegC == 0)
                        grfCurva.Series["Compra"].Points.AddXY(Xval1, 0);
                    liRegC++;
                    grfCurva.Series["Compra"].Points.AddXY(Xval1, Yval);
                    grfCurva.Series["CompraP"].Points.AddXY(Xval1, Yval);
                    grfCurva.Series["CompraP"].ToolTip = "Precio:#VALY, Cantidad:#VALX";
                    yUlt = Yval;
                }
            }
            grfCurva.Series["Venta"].Points.AddXY(Xval, Yval);
            grfCurva.Series["Compra"].Points.AddXY(0, yUlt);

            //if (minX1 == maxX1)
            //{
            //    minX1 = minX1 - 1;
            //    maxX1 = maxX1 + 1;
            //}
            //if (minY1 == maxY1)
            //{
            //    minY1 = minY1 - 1;
            //    maxY1 = maxY1 + 1;
            //}
            grfCurva.Titles.Add("GRÁFICO DE CURVA DE OFERTA Y DEMANDA AGREGADA"); // 20161021 subasta UVLP
            grfCurva.ChartAreas["ChartArea1"].AxisX.Minimum = minX1;
            grfCurva.ChartAreas["ChartArea1"].AxisX.Maximum = maxX1;
            grfCurva.ChartAreas["ChartArea1"].AxisY.Minimum = minY1;
            grfCurva.ChartAreas["ChartArea1"].AxisY.Maximum = maxY1;
            if (Session["destino_rueda"].ToString() == "G")
            {
                grfCurva.ChartAreas["ChartArea1"].AxisY.Title = "Precio (USD/MBTU]";
                grfCurva.ChartAreas["ChartArea1"].AxisX.Title = "Cantidad (MBTUD)";
            }
            else
            {
                grfCurva.ChartAreas["ChartArea1"].AxisY.Title = "Precio (Moneda vigente/KPC)"; //20161019 subata UVLP
                grfCurva.ChartAreas["ChartArea1"].AxisX.Title = "Cantidad (KPCD)";
            }
            //grfCurva.ChartAreas["ChartArea1"].AxisX.MajorGrid.Enabled = false; //20161019 subata UVLP
            //grfCurva.ChartAreas["ChartArea1"].AxisY.MajorGrid.Enabled = false;  //20161019 subata UVLP

            grfCurva.ChartAreas["ChartArea1"].AxisY.LabelStyle.Format = "{0:###,##0.00}"; //20161019 subata UVLP

            grfCurva.Legends.Add(new Legend("Grafica"));
            grfCurva.Series["CompraP"].IsVisibleInLegend = false;
            grfCurva.Series["VentaP"].IsVisibleInLegend = false;
            grfCurva.Series["Compra"].LegendText = "Demanda";
            grfCurva.Series["Venta"].LegendText = "Oferta";
        }
        catch (Exception ex)
        {

        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbRegresar_Click(object sender, ImageClickEventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
    }

}
