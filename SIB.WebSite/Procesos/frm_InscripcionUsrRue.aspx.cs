﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Procesos
{
    public partial class frm_InscripcionUsrRue : Page
    {
        private InfoSessionVO goInfo;
        private static string lsTitulo = "Inscripción de Usuarios por Tipo de Rueda";

        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        private clConexion lConexion;

        private clConexion lConexion1 = null;
        private SqlDataReader lLector;

        #region Propiedades

        /// <summary>
        /// 
        /// </summary>
        public string HndCodigo
        {
            get
            {
                return ViewState["HndCodigo"] != null && !string.IsNullOrEmpty(ViewState["HndCodigo"].ToString())
                    ? ViewState["HndCodigo"].ToString()
                    : string.Empty;
            }
            set { ViewState["HndCodigo"] = value; }
        }

        #endregion Propiedades

        private string lsIndica
        {
            get
            {
                if (ViewState["lsIndica"] == null)
                    return "";
                return (string)ViewState["lsIndica"];
            }
            set { ViewState["lsIndica"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                goInfo = (InfoSessionVO)Session["infoSession"];
                if (goInfo == null) Response.Redirect("../index.aspx");
                goInfo.Programa = lsTitulo;
                //Controlador util = new Controlador();
                /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
             *   lsIndica = N -> Nuevo (Creacion)
             *   lsIndica = L -> Listar Registros (Grilla)
             *   lsIndica = M -> Modidificar
             *   lsIndica = B -> Buscar
             * */

                //Establese los permisos del sistema
                //EstablecerPermisosSistema(); //20220615  ajuste
                lConexion = new clConexion(goInfo);

                buttons.CrearOnclick += btnCrear_Onclick;
                buttons.FiltrarOnclick += btnConsultar_Click;
                buttons.ExportarExcelOnclick += BtnExcel_Click;

                if (IsPostBack) return;

                lblTitulo.Text = lsTitulo;
                buttons.Inicializar(ruta: "t_usuario_hab");
                //Titulo
                Master.Titulo = "Subasta";
                // Se seleccionan los botones
                EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Crear };
                buttons.Inicializar(botones: botones);

                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlTipoSubasta, "m_tipos_subasta",
                    "estado = 'A' Order by descripcion", 0, 1);
                LlenarControles(lConexion.gObjConexion, ddlBusTipoSubasta, "m_tipos_subasta",
                    "estado = 'A' Order by descripcion", 0, 1);
                LlenarControles(lConexion.gObjConexion, ddlUsuario, "a_usuario",
                    "estado = 'A' and tipo_perfil='N' and ( " + goInfo.cod_comisionista + " =0 or codigo_operador = " +
                    goInfo.cod_comisionista + " and '" + Session["administrador_operador"] + "'='S' or login='" +
                    goInfo.Usuario + "') Order by login", 0, 1);
                LlenarControles(lConexion.gObjConexion, ddlBusUsuario, "a_usuario",
                    "estado = 'A' and tipo_perfil='N' and ( " + goInfo.cod_comisionista + " =0 or codigo_operador = " +
                    goInfo.cod_comisionista + " and '" + Session["administrador_operador"] + "'='S' or login='" +
                    goInfo.Usuario + "') Order by login", 0, 1);
                lConexion.Cerrar();
                //20180122 rq003-18
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        /// 20220615 ajuste
        //private void EstablecerPermisosSistema()
        //{
        //    foreach (DataGridItem Grilla in dtgConsulta.Items)
        //    {
        //        var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
        //        lkbModificar.Visible = goInfo.cod_comisionista != "0" ? false : true;
        //        var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
        //        lkbEliminar.Visible = true;
        //    }
        //}

        /// <summary>
        /// Nombre: Nuevo
        /// Fecha: Agosto 22 de 2014
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
        /// Modificacion:
        /// </summary>
        private void btnCrear_Onclick(object sender, EventArgs e)
        {
            //Se abre el modal de registro
            Modal.Abrir(this, mdlCrear.ID, mdlCrearInside.ID);
            imbCrear.Visible = true;
            imbActualiza.Visible = false;
            ddlTipoSubasta.Enabled = true;
            ddlTipoRueda.Enabled = true;
            ddlUsuario.Enabled = true;
            TxtFechaIni.Enabled = false;
            TxtFechaFin.Enabled = false;
            ddlEstado.SelectedValue = "C";
            ddlEstado.Enabled = false;
        }

        /// <summary>
        /// Nombre: Listar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            CargarDatos();
            dtgConsulta.Visible = true;
            buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Crear, EnumBotones.Excel);
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            var lblMensaje = new StringBuilder();
            mdlCrearLabel.InnerHtml = "Modificar";

            if (modificar != null && modificar != "")
            {
                try
                {
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        ddlTipoSubasta.Enabled = false;
                        ddlTipoRueda.Enabled = false;
                        ddlUsuario.Enabled = false;
                        //20220615
                        if (goInfo.cod_comisionista == "0")
                        {
                            TxtFechaIni.Enabled= true;
                            TxtFechaFin.Enabled = true;
                            ddlEstado.Enabled = true;
                        }
                        else
                        {
                            TxtFechaIni.Enabled = false;
                            TxtFechaFin.Enabled = false;
                            ddlEstado.Enabled = false;
                        }
                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_usuario_hab", " codigo_usuario_hab= " + modificar + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            try
                            {
                                ddlTipoSubasta.SelectedValue = lLector["codigo_tipo_subasta"].ToString();
                                ddlTipoSubasta_SelectedIndexChanged(null, null);
                            }
                            catch (Exception ex)
                            {
                                lblMensaje.Append("El tipo de subasta del registro no existe o esta inactivo<br>");
                            }

                            try
                            {
                                ddlTipoRueda.SelectedValue = lLector["codigo_tipo_rueda"].ToString();
                            }
                            catch (Exception ex)
                            {
                                lblMensaje.Append("EL tipo de rueda seleccionado no existe o esta finalizada<br>");
                            }

                            try
                            {
                                ddlUsuario.SelectedValue = lLector["codigo_usuario"].ToString();
                            }
                            catch (Exception ex)
                            {
                                lblMensaje.Append("El usuario del registro no existe o esta inactivo<br>");
                            }

                            if (lLector["fecha_inicial"].ToString().Substring(0, 10) != "01/01/1900")
                                TxtFechaIni.Text = lLector["fecha_inicial"].ToString().Substring(6, 4) +
                                                   lLector["fecha_inicial"].ToString().Substring(2, 4) +
                                                   lLector["fecha_inicial"].ToString().Substring(0, 2);
                            else
                                TxtFechaIni.Text =
                                    DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day;
                            if (lLector["fecha_final"].ToString().Substring(0, 10) != "01/01/1900")
                                TxtFechaFin.Text = lLector["fecha_final"].ToString().Substring(6, 4) +
                                                   lLector["fecha_final"].ToString().Substring(2, 4) +
                                                   lLector["fecha_final"].ToString().Substring(0, 2);
                            else
                                TxtFechaFin.Text = "";
                            //TxtFechaIni.Enabled = true; //20220615 ajuste
                            //TxtFechaFin.Enabled = true; //20220615 ajuste
                            ddlEstado.SelectedValue = lLector["estado"].ToString();
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                        }

                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Bloquea el Registro a Modificar
                        manejo_bloqueo("A", modificar);
                    }
                    else
                    {
                        Listar();
                        lblMensaje.Append(
                            "No se Puede editar el Registro por que esta Bloqueado. Codigo usuario-rueda" + modificar);

                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append(ex.Message);
                }
            }

            if (lblMensaje.ToString() == "")
            {
                //Se abre el modal para actualizar
                Modal.Abrir(this, mdlCrear.ID, mdlCrearInside.ID);
                mdlCrearLabel.InnerHtml = "Modificar ";
            }
            else
            {
                Toastr.Error(this, lblMensaje.ToString());
            }
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros =
            {
                "@P_codigo_tipo_subasta", "@P_codigo_tipo_rueda", "@P_codigo_usuario", "@P_codigo_operador", "@P_estado"
            };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char };
            string[] lValorParametros = { "0", "0", "0", "0", "" };

            try
            {
                if (ddlBusTipoSubasta.SelectedValue != "0")
                    lValorParametros[0] = ddlBusTipoSubasta.SelectedValue;
                if (ddlBusTipoRueda.SelectedValue != "" && ddlBusTipoRueda.SelectedValue != "0")
                    lValorParametros[1] = ddlBusTipoRueda.SelectedValue;
                if (ddlBusUsuario.SelectedValue != "0")
                    lValorParametros[2] = ddlBusUsuario.SelectedValue;
                lValorParametros[3] = goInfo.cod_comisionista;
                if (ddlBusEstado.SelectedValue != "")
                    lValorParametros[4] = ddlBusEstado.SelectedValue;
                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion,
                    "pa_GetUsrHab", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            Listar();
        }

        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgConsulta.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
        {
            var lblMensaje = new StringBuilder();
            if (e.CommandName.Equals("Modificar"))
            {
                HndCodigo = dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text;
                Modificar(HndCodigo);
            }

            if (e.CommandName.Equals("Eliminar"))
            {
                if (!VerificarExistencia("t_usuario_hab",
                    " codigo_usuario_hab= " + dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text + " and estado ='C'"))
                    lblMensaje.Append("No se puede eliminar el registro porque ya fue aprobado<br>");
                if (lblMensaje.ToString() == "")
                {
                    string[] lsNombreParametros = { "@P_codigo_usuario_hab" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int };
                    String[] lValorParametros = { dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text };
                    try
                    {
                        lConexion.Abrir();
                        if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_DelUsrHab",
                            lsNombreParametros, lTipoparametros, lValorParametros))
                        {
                            lblMensaje.Append("Se presento un Problema en la Eliminación del  registro del usuario.!");
                            lConexion.Cerrar();
                        }
                        else
                            Listar();

                        if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                        {
                            Toastr.Error(this, lblMensaje.ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        Toastr.Error(this, "Se presento un Problema en la Eliminación del  registro del usuario.!");
                    }
                }
                //20220615 ajuste
                else
                {
                    Toastr.Error(this, lblMensaje.ToString());
                }
            }   
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lstable, string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia(lstable, lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion,
            Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
            //dtgComisionista.DataBind();

            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            var lItem = new ListItem { Value = "0", Text = "Seleccione" };
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }

            lLector.Close();
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='t_usuario_hab' and llave_registro='codigo_usuario_hab=" +
                                 lscodigo_registro + "'";
            string lsCondicion1 = "codigo_usuario_hab=" + lscodigo_registro;
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }

            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_usuario_hab";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }

            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_usuario_hab", lsCondicion1);
            }

            return true;
        }

        ///// <summary>
        ///// Nombre: ImgExcel_Click
        ///// Fecha: Agosto 15 de 2008
        ///// Creador: German Eduardo Guarnizo
        ///// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        ///// Modificacion:
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
        //{
        //    string[] lValorParametros = { "0", "" };
        //    string lsParametros = "";

        //    try
        //    {
        //        Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetUsrRueHab&nombreParametros=@P_codigo_periodo*@P_descripcion&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "&columnas=codigo_usuario_hab*numero_rueda*desc_rueda*codigo_operador*nombre_operador*codigo_usuario*login*estado&titulo_informe=Listado de Usuarios habilitados por reuda&TituloParametros=");
        //    }
        //    catch (Exception ex)
        //    {
        //        lblMensaje.Text = "No se Pude Generar el Informe.!";
        //    }

        //}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlTipoSubasta_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlTipoRueda.Items.Clear();
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlTipoRueda, "m_tipos_rueda",
                " codigo_tipo_subasta = " + ddlTipoSubasta.SelectedValue + " and estado = 'A' Order by descripcion", 0,
                1);
            lConexion.Cerrar();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlBusTipoSubasta_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlBusTipoRueda.Items.Clear();
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlBusTipoRueda, "m_tipos_rueda",
                " codigo_tipo_subasta = " + ddlBusTipoSubasta.SelectedValue + " and estado = 'A' Order by descripcion",
                0, 1);
            lConexion.Cerrar();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCrear_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros =
            {
                "@P_codigo_tipo_subasta", "@P_codigo_tipo_rueda", "@P_codigo_usuario", "@P_fecha_inicial",
                "@P_fecha_final", "@P_estado"
            };
            SqlDbType[] lTipoparametros =
                {SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Char};
            String[] lValorParametros = { "", "", "", "", "", "" };
            mdlCrearLabel.InnerHtml = "Agregar";
            var lblMensaje = new StringBuilder();
            try
            {
                if (ddlTipoSubasta.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el tipo de subasta<br>");
                if (ddlTipoRueda.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el tipo de rueda<br>");
                if (ddlUsuario.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el usuario<br>");
                if (lblMensaje.ToString() == "" && VerificarExistencia("t_usuario_hab",
                        " codigo_tipo_rueda= " + ddlTipoRueda.SelectedValue + " and codigo_usuario=" +
                        ddlUsuario.SelectedValue))
                    lblMensaje.Append(" El Usuario ya se asoció  al tipo rueda seleccionado<br>");
                //if (TxtFechaIni.Text == "")
                //    lblMensaje.Text += "Debe seleccionar la fecha inicial<br>";
                //if (TxtFechaFin.Text == "")
                //    lblMensaje.Text += "Debe seleccionar la fecha final<br>";
                //try
                //{
                //    if (Convert.ToDateTime(TxtFechaIni.Text) > Convert.ToDateTime(TxtFechaFin.Text))
                //        lblMensaje.Text = "La fecha inicial debe ser menor o igual que la final";
                //}
                //catch (Exception ex)
                //{
                //    lblMensaje.Text = "Error en el formato de las fechas";
                //}


                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[0] = ddlTipoSubasta.SelectedValue;
                    lValorParametros[1] = ddlTipoRueda.SelectedValue;
                    lValorParametros[2] = ddlUsuario.SelectedValue;
                    lValorParametros[3] = TxtFechaIni.Text;
                    lValorParametros[4] = TxtFechaFin.Text;
                    lValorParametros[5] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetUsrHab",
                        lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Append("Se presentó un Problema en la inscripción del usuario.!");
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Success(this, "El registro se realizó correctamente");
                        //Se cierra el modal
                        Modal.Cerrar(this, mdlCrear.ID);
                        Listar();
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    Toastr.Error(this, lblMensaje.ToString());
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnActualiza_Click(object sender, EventArgs e)
        {
            var lblMensaje = new StringBuilder();
            if (TxtFechaIni.Text == "")
                lblMensaje.Append("Debe seleccionar la fecha inicial<br>");
            if (TxtFechaFin.Text == "")
                lblMensaje.Append("Debe seleccionar la fecha final<br>");
            try
            {
                if (Convert.ToDateTime(TxtFechaIni.Text) > Convert.ToDateTime(TxtFechaFin.Text))
                    lblMensaje.Append("La fecha inicial debe ser menor o igual que la final");
            }
            catch (Exception ex)
            {
                lblMensaje.Append("Error en el formato de las fechas");
            }

            if (lblMensaje.ToString() == "")
            {
                string[] lsNombreParametros =
                    {"@P_codigo_usuario_hab", "@P_fecha_inicial", "@P_fecha_final", "@P_estado"};
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Char };
                String[] lValorParametros = { "", "", "", "" };
                try
                {
                    lValorParametros[0] = HndCodigo;
                    lValorParametros[1] = TxtFechaIni.Text;
                    lValorParametros[2] = TxtFechaFin.Text;
                    lValorParametros[3] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_UptUsrHab",
                        lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Append("Se presento un Problema en la Actualizacion del  registro del usuario.!");
                        lConexion.Cerrar();
                    }
                    else
                    {
                        manejo_bloqueo("E", HndCodigo);
                        Toastr.Success(this, "El registro se actualizo correctamente");
                        //Se cierra el modal
                        Modal.Cerrar(this, mdlCrear.ID);
                        Listar();
                    }
                }
                catch (Exception ex)
                {
                    /// Desbloquea el Registro Actualizado
                    manejo_bloqueo("E", HndCodigo);
                }
            }

            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                Toastr.Error(this, lblMensaje.ToString());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSalir_Click1(object sender, EventArgs e)
        {
            // Desbloquea el Registro Actualizado
            if (HndCodigo != "")
                manejo_bloqueo("E", HndCodigo);
            //Se cierra el modal
            Modal.Cerrar(this, mdlCrear.ID);
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20190607 rq036-19
        protected void BtnExcel_Click(object sender, EventArgs e)
        {
            string[] lValorParametros = { "0", "0", "0", "0", "" };
            string lsParametros = "";

            try
            {
                if (ddlBusTipoSubasta.SelectedValue != "0")
                {
                    lValorParametros[0] = ddlBusTipoSubasta.SelectedValue;
                    lsParametros += " Tipo subasta: " + ddlBusTipoSubasta.SelectedItem;
                }

                if (ddlBusTipoRueda.SelectedValue != "" && ddlBusTipoRueda.SelectedValue != "0")
                {
                    lValorParametros[1] = ddlBusTipoRueda.SelectedValue;
                    lsParametros += " Tipo rueda: " + ddlBusTipoRueda.SelectedItem;
                }

                if (ddlBusUsuario.SelectedValue != "0")
                {
                    lValorParametros[2] = ddlBusUsuario.SelectedValue;
                    lsParametros += " Usuario: " + ddlBusUsuario.SelectedItem;
                }

                if (goInfo.cod_comisionista != "0")
                {
                    lValorParametros[3] = goInfo.cod_comisionista;
                    lsParametros += " Operador: " + goInfo.cod_comisionista;
                }

                if (ddlBusEstado.SelectedValue != "")
                {
                    lValorParametros[4] = ddlBusEstado.SelectedValue;
                    lsParametros += " Estado: " + ddlBusEstado.SelectedItem;
                }

                Server.Transfer(
                    "../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetUsrHab&nombreParametros=@P_codigo_tipo_subasta*@P_codigo_tipo_rueda*@P_codigo_usuario*@P_codigo_operador*@P_estado&valorParametros=" +
                    lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "*" +
                    lValorParametros[3] + "*" + lValorParametros[4] +
                    "&columnas=codigo_usuario_hab*codigo_tipo_subasta*desc_tipo_subasta*codigo_tipo_rueda*desc_tipo_rueda*codigo_usuario*nombre_usuario*rol_operador*codigo_operador*nombre_operador*fecha_inicial*fecha_final*estado*fecha_solicitud&titulo_informe=Listado Usuarios habilitados para ruedas&TituloParametros=" +
                    lsParametros);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se Pude Generar el Informe.!");
            }
        }
    }
}