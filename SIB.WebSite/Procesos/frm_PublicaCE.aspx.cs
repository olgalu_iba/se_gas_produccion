﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;

public partial class Procesos_frm_PublicaCE : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Publicación Inicial Oferta Capacidad Excedentaria";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            if (goInfo.cod_comisionista != "0")
                BtnCalcular.Visible = false;
            else
                BtnCalcular.Visible = true;

            lConexion = new clConexion(goInfo);  //20170929 rq048-17
            traeFecha();  //20170929 rq048-17
            consultar();
        }

    }
    protected void BtnCalcular_Click(object sender, EventArgs e)
    {
        SqlDataReader lLector;
        SqlCommand lComando = new SqlCommand();
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.Parameters.Add("@P_tipo_publicacion", SqlDbType.Char).Value = "I";
        lComando.CommandText = "pa_SetCapacExc";
        lComando.CommandTimeout = 3600;
        lLector = lComando.ExecuteReader();
        if (lLector.HasRows)
        {
            lLector.Read();
            lblMensaje.Text = lLector["Mensaje"].ToString();
        }
        else
            consultar();
        lLector.Close();
        lLector.Dispose();
        lConexion.Cerrar();
        
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void consultar()
    {
        lblMensaje.Text = "";
        if (lblMensaje.Text == "")
        {
            try
            {
                int liAnoAct = 0;
                liAnoAct = DateTime.Now.Year;
                dtgConsulta.Columns[3].HeaderText = hndFecha.Value;  //20170929  rq048-17
                dtgConsulta.Columns[4].HeaderText = "CE Año 2 01/12/" + (liAnoAct + 1).ToString() + " a 30/11/" + (liAnoAct + 2).ToString() + " (KPCD)";
                dtgConsulta.Columns[5].HeaderText = "CE Año 3 01/12/" + (liAnoAct + 2).ToString() + " a 30/11/" + (liAnoAct + 3).ToString() + " (KPCD)";
                dtgConsulta.Columns[6].HeaderText = "CE Año 4 01/12/" + (liAnoAct + 3).ToString() + " a 30/11/" + (liAnoAct + 4).ToString() + " (KPCD)";
                dtgConsulta.Columns[7].HeaderText = "CE Año 5 01/12/" + (liAnoAct + 4).ToString() + " a 30/11/" + (liAnoAct + 5).ToString() + " (KPCD)";

                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetCapacExc";
                lComando.Parameters.Add("@P_tipo_publicacion", SqlDbType.Char).Value = "I";
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgConsulta.DataSource = lds;
                dtgConsulta.DataBind();
                tblGrilla.Visible = true;
                imbExcel.Visible = true;

                //lComando.Parameters.Add("@P_control_cen", SqlDbType.Char).Value = "N";
                //lComando.CommandText = "pa_GetCapacExc";
                //lComando.CommandTimeout = 3600;
                //lLector = lComando.ExecuteReader();
                //if (lLector.HasRows)
                //{
                //    while (lLector.Read())
                //        lblMensaje.Text += "El tramo " + lLector["desc_tramo"].ToString()+ " del operador " + lLector["nombre_operador"].ToString() + " no pasó el control de la CEN<br>"; 
                //}
                //lLector.Close();
                //lLector.Dispose();

                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfPubCE" + DateTime.Now + ".xls";
        string lstitulo_informe = "Informe Publicación Oferta Capacidad Excedentaria";
        try
        {
            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgConsulta.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            lform.Controls.Add(dtgConsulta);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Excel.!" + ex.Message.ToString();
        }
    }
    /// <summary>
    /// Nombre: tradatos
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    /// 20170929 rq048-17
    protected void traeFecha()
    {
        lConexion.Abrir();
        SqlDataReader lLector;
        string[] lsNombreParametros = { "@P_tabla", "@P_campos", "@P_filtro" };
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
        string[] lValorParametros = { "m_parametros_uvlp", "convert(varchar(10),fecha_ini_entrega,103) as fecha_ini, convert(varchar(10),fecha_fin_entrega,103) as fecha_fin", "1=1" };

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_ValidarExistencia1", lsNombreParametros, lTipoparametros, lValorParametros);
        if (lLector.HasRows)
        {
            lLector.Read();
            hndFecha.Value = "CE Año 1 " + lLector["fecha_ini"].ToString() + " a " + lLector["fecha_fin"].ToString() + " (KPCD)";
        }

        lLector.Close();
        lLector.Dispose();
        lConexion.Cerrar();
    }

}