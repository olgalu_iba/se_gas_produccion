﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_CargaDecSubBimNormal.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Procesos.frm_CargaDecSubBimNormal" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" Text="Archivo Declaración de Información Subasta Bimestral Normal" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Archivo Contratos</label>
                            <asp:FileUpload ID="FuArchivo" runat="server" EnableTheming="true" Width="100%" CssClass="form-control btn" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
