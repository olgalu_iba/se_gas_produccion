﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_ActDeclaracionPre : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    String strRutaArchivo;
    String strRutaArchivo1;
    SqlDataReader lLector;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            hndDestino.Value = Session["destino_rueda"].ToString();
            if (this.Request.QueryString["codigo_dec"] != null && this.Request.QueryString["codigo_dec"].ToString() != "")
                hndCodDec.Value = this.Request.QueryString["codigo_dec"].ToString();
            else
                hndCodDec.Value = "0";
            lblRueda.Text = "No. Rueda: " + Session["numero_rueda"].ToString();
            string lsTabla = "";
            string lsCodicion = "";
            if (hndDestino.Value == "T")
            {
                lsTabla = "t_declaracion_inf dec, m_ruta_snt rut";
                lsCodicion = " dec.codigo_declaracion_inf = " + hndCodDec.Value + " and dec.codigo_declarador= rut.codigo_trasportador and dec.codigo_punto_ini = rut.codigo_pozo_ini and dec.codigo_punto_fin = rut.codigo_pozo_fin ";
                lblPunto.Text = "Ruta: ";
                lblDestino.Text = "Destino: Transporte";
                trPrecio.Visible = false;
            }
            else
            {
                lsTabla = "t_declaracion_inf dec, m_pozo poz";
                lsCodicion = " dec.codigo_declaracion_inf = " + hndCodDec.Value + " and dec.codigo_punto_ini = poz.codigo_pozo";
                lblPunto.Text = "Punto Entrega: ";
                lblDestino.Text = "Destino: Suministro de Gas";
            }

            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", lsTabla, lsCodicion);
            if (lLector.HasRows)
            {
                lLector.Read();
                lblCantidad.Text = lLector["cantidad"].ToString();
                TxtCntNoDisp.Text = lLector["cantidad_no_disponible"].ToString();
                TxtPrecioRes.Text = lLector["precio"].ToString();
                lblPunto.Text += lLector["descripcion"].ToString();
            }
            lLector.Close();
            lConexion.Cerrar();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOfertar_Click(object sender, EventArgs e)
    {
        string oMensaje = "";
        if (TxtCntNoDisp.Text == "")
            oMensaje += "Debe digitar la cantidad declarada\\n";
        else
        {
            try
            {
                Convert.ToInt32(TxtCntNoDisp.Text.Replace(",", ""));
            }
            catch (Exception ex)
            {
                oMensaje += "Cantidad declarada no válida\\n";
            }
        }
        if (hndDestino.Value == "G")
        {
            if (TxtPrecioRes.Text.Trim().Length > 0)
            {
                string sOferta = TxtPrecioRes.Text.Replace(",", "");
                int iPos = sOferta.IndexOf(".");
                if (iPos > 0)
                    if (sOferta.Length - iPos > 3)
                        oMensaje += "Se permiten máximo 2 decimales en el precio de reserva\\n";
                try
                {
                    if (Convert.ToDouble(sOferta) < 0)
                        oMensaje += "El precio de reserva debe ser mayor o igual que cero\\n";
                }
                catch (Exception ex)
                {
                    oMensaje += "El precio de reserva digitado no es válido\\n";
                }
            }
            else
                oMensaje += "Debe Ingresar el precio de reserva. \\n";
        }
        if (oMensaje == "")
        {
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_declaracion_inf", "@P_codigo_operador", "@P_cantidad_no_disponible", "@P_precio" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal };
            string[] lValorParametros = { Session["numero_rueda"].ToString(), hndCodDec.Value, goInfo.cod_comisionista, TxtCntNoDisp.Text.Replace(",", ""), "0" };
            if (hndDestino.Value == "G")
                lValorParametros[4] = TxtPrecioRes.Text.Replace(",", "");
            lConexion.Abrir();
            try
            {
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_UptDeclaracionPre", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    oMensaje = "Error al actualizar la declaracion de precio. " + goInfo.mensaje_error.ToString() + "\\n";
                }
                else
                {
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                            oMensaje = lLector["mensaje"].ToString() + "\\n";
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        Session["mis_ofertas"] = "S";
                        if (Session["TipoOperador"].ToString() == "G") //20201207
                            Toastr.Success(this, "Declaración de capacidades no disponibles actualizada correctamente.!", "Correcto", 50000);       
                        else //20201207
                            Toastr.Success(this, "Declaración de precio actualizada correctamente.!", "Correcto", 50000); //20201207
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                oMensaje = "Error al actualizar la declaración de precio. " + ex.Message;
                lConexion.Cerrar();
            }
            if (oMensaje != "")
            {
                Toastr.Warning(this, oMensaje, "Warning", 50000);

            }
        }
        else
            Toastr.Warning(this, oMensaje, "Warning", 50000);
    }
}
