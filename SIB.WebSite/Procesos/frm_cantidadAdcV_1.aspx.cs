﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_cantidadAdcV_1 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    String strRutaArchivo;
    String strRutaArchivo1;
    SqlDataReader lLector;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            if (this.Request.QueryString["ID"] != null && this.Request.QueryString["ID"].ToString() != "")
            {
                hndID.Value = this.Request.QueryString["ID"];
                hndRonda.Value = this.Request.QueryString["ronda"];
                HndCntV.Value = this.Request.QueryString["cantidad_v"];
                /// Obtengo los Datos del ID Recibido
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_id_rueda idr, t_precio_reserva_v preR ", " idr.numero_id = " + hndID.Value + " and idr.numero_id = preR.numero_id and preR.ronda=" + hndRonda.Value);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    /// Lleno las etiquetas de la pantalla con los datos del Id.
                    lblTipo.Text = "Tipo: " + lLector["desc_modalidad"].ToString();
                    lblDuracion.Text = lLector["tiempo_entrega"].ToString();
                    lblId.Text = "ID: " + this.Request.QueryString["ID"];
                    lblLugar.Text = "Lugar: " + lLector["desc_punto_entrega"].ToString();
                    lblPrecioRes.Text = "Precio Reserva: " + lLector["precio_reserva"].ToString();
                    lblCantidadV.Text = "Cantidad Ofrecida: " + HndCntV.Value;
                    lLector.Close();
                    lLector.Dispose();
                }
                else
                    Toastr.Warning(this, "El ID enviado NO está disponible para adicionar ofertas.!");
              
            }
            else
                Toastr.Warning(this, "No Se enviaron los Parametros requeridos.!");
       
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCambiar_Click(object sender, EventArgs e)
    {
        string oError = "";
        string sMensaje = "";
        double ldPreAct = 0;
        double ldPreRes = 0;

        if (TxtCantidad.Text == "")
            oError += "Debe digitar la cantidad adicional\\n";
        if (oError == "")
        {
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_numero_id", "@P_codigo_operador", "@P_ronda", "@P_cantidad_adc" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { Session["numero_rueda"].ToString(), hndID.Value, goInfo.cod_comisionista, hndRonda.Value, TxtCantidad.Text.Replace(",", "") }; ///  Accion C=crear, M= Modificar
            SqlDataReader lLector;
            lConexion.Abrir();
            try
            {
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetPosturaAdcV1", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        oError = lLector["error"].ToString() + "\\n";
                }
                if (oError == "")
                {
                    sMensaje = "Adición de oferta registrada correctamente";
                    Session["hora"] = "";
                    Toastr.Warning(this, sMensaje);

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                oError = "Error al realizar la adicion. " + ex.Message;
                lConexion.Cerrar();
            }
        }
        if (oError != "")
        {
            Toastr.Warning(this, oError);
            
        }
    }
}
