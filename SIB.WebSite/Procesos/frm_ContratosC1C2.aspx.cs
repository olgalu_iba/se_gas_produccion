﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;

public partial class Procesos_frm_ContratosC1C2 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Carga de contratos C1 y C2 en el SE_GAS";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lblMensaje.Text = "";
        if (!IsPostBack)
        {
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlModalidad, "m_modalidad_contractual", " estado = 'A' and sigla in ('C1','C2') order by codigo_modalidad", 0, 1);
            lConexion.Cerrar();
            consultar();
        }
    }
    protected void BtnBuscar_Click(object sender, EventArgs e)
    {
        consultar();
    }

    protected void BtnActualizar_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        if (ddlModalidad.SelectedValue == "0")
            lblMensaje.Text = "Debe seleccionar la modalidad de contrato para cargar los contratos";
        if (lblMensaje.Text == "")
        {
            SqlDataReader lLector;
            SqlCommand lComando = new SqlCommand();
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();

            lComando.Connection = lConexion.gObjConexion;
            lComando.CommandType = CommandType.StoredProcedure;
            lComando.CommandText = "pa_SetContratoC1C2";
            lComando.Parameters.Add("@P_codigo_modalidad", SqlDbType.Int).Value = ddlModalidad.SelectedValue;
            lComando.CommandTimeout = 3600;
            lLector = lComando.ExecuteReader();
            if (lLector.HasRows)
            {
                while (lLector.Read())
                    lblMensaje.Text += lLector["error"].ToString() + "<br>";
            }
            else
                lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
            if (lblMensaje.Text == "")
                consultar();
        }
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void consultar()
    {
        try
        {
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            SqlCommand lComando = new SqlCommand();
            lComando.Connection = lConexion.gObjConexion;
            lComando.CommandTimeout = 3600;
            lComando.CommandType = CommandType.StoredProcedure;
            lComando.CommandText = "pa_GetContratoC1C2";
            lComando.Parameters.Add("@P_codigo_modalidad", SqlDbType.Int).Value = ddlModalidad.SelectedValue;
            lComando.ExecuteNonQuery();
            lsqldata.SelectCommand = lComando;
            lsqldata.Fill(lds);
            dtgConsulta.DataSource = lds;
            dtgConsulta.DataBind();
            tblGrilla.Visible = true;
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Consultar la información.! " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lsTable, string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lsTable, lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

}