﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_enviarMensaje_1 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    String strRutaArchivo;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString() + "mensaje.txt";
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnMensaje_Click(object sender, EventArgs e)
    {
        string oError = "";
        string sMensaje = "";

        if (TxtMensaje.Text == "")
            oError += "Debe digitar el mensaje a enviar\\n";

        if (oError == "")
        {
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_mensaje" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
            string[] lValorParametros = { Session["numero_rueda"].ToString(), TxtMensaje.Text, };

            lConexion.Abrir();
            try
            {
                DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_setMensaje", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                sMensaje = "Mensaje enviado Correctamente";
                File.SetCreationTime(strRutaArchivo, DateTime.Now);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + sMensaje + "');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);

                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                oError = "Error al enviar el mensaje. " + ex.Message;
                lConexion.Cerrar();
            }
        }
        if (oError != "")
        {
            Toastr.Warning(this, oError);
            
        }
    }
}
