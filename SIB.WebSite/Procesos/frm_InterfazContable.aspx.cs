﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using Segas.Web.Elements;
using SFaFactuSeven;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Procesos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frm_InterfazContable : Page
    {
        private InfoSessionVO goInfo;
        private static string lsTitulo = "Interfaz Contable";
        private clConexion lConexion;
        private bool error = false;

        /// <summary>
        /// 
        /// </summary>
        private string lsIndica
        {
            get
            {
                if (ViewState["lsIndica"] == null)
                    return "";
                else
                    return (string)ViewState["lsIndica"];
            }
            set
            {
                ViewState["lsIndica"] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            lblTitulo.Text = lsTitulo;
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lConexion = new clConexion(goInfo);

            if (!IsPostBack)
            {
                //Titulo
                Master.Titulo = "Facturación";

                //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
                if (Request.QueryString["lsIndica"] != null && Request.QueryString["lsIndica"] != "")
                {
                    lsIndica = Request.QueryString["lsIndica"];
                }
                if (lsIndica == null || lsIndica == "" || lsIndica == "L")
                {
                    if (error != true)
                    {
                        TxtAno.Text = DateTime.Now.Year.ToString();
                        //CargarDatos(); //20200924 ajsute compoenente
                        // Activo el Div para el Ingreso del Usuario y Clave del Servicio Web 
                        Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
                    }
                }
                else if (lsIndica == "V")
                {

                }
                EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Facturar, EnumBotones.ModificarFacturar, EnumBotones.Excel };
                buttons.Inicializar(botones: botones);
            }
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.FacturarOnclick += btnFactura_Click;
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ModificarFacturarOnclick += btnModifcacion_Click;
        }

        /// <summary>
        /// Carga los Datos de la Grilla y los visualiza en pantalla
        /// </summary>
        private void CargarDatos()
        {
            var lblMensaje = new StringBuilder();
            if (TxtAno.Text.Trim().Length > 0)
            {
                try
                {
                    Convert.ToInt32(TxtAno.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Valor Invalido en Año Facturación.!");
                }
            }
            else
                lblMensaje.Append("Debe Ingresar Año Facturación.!");
            if (ddlMesFactura.SelectedValue == "0")
                lblMensaje.Append("Debe Ingresar Mes Facturación.!");

            if (string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                try
                {
                    // se construye la condicion dependiendo del usuario
                    string[] lsNombreParametros = { "@P_ano", "@P_mes" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                    string[] lValorParametros = { TxtAno.Text.Trim(), ddlMesFactura.SelectedValue };

                    lConexion.Abrir();
                    dtgFacturas.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetConsFacturacion", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgFacturas.DataBind();
                    if (dtgFacturas.Items.Count > 0)
                    {
                        decimal ldTotalFac = 0;

                        foreach (DataGridItem Grilla in dtgFacturas.Items)
                        {
                            ldTotalFac = ldTotalFac + Convert.ToDecimal(Grilla.Cells[4].Text.Replace(",", ""));
                        }
                        lblTotalGrillaFac.Text = "Total Facturación: " + ldTotalFac.ToString("###,###,###,###,###,##0.00");
                    }
                    else
                        lblMensaje.Append("No hay Registros para Visualizar.!");
                    lConexion.Cerrar();
                    /////////////////////////////////////////////////////////////////////////////////////
                }
                catch (Exception ex)
                {
                    lblMensaje.Append(ex.Message);
                }
            }

            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                Toastr.Error(this, lblMensaje.ToString());
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceLlave) + " - " + lLector.GetValue(liIndiceDescripcion);
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string lsNombreArchivo = goInfo.Usuario + "InfConmpFinan" + DateTime.Now + ".xls";
            string lstitulo_informe = "Informe Consulta Interfaz Facturacion - Fecha: " + TxtAno.Text;
            try
            {
                decimal ldCapacidad = 0;
                StringBuilder lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                dtgFacturas.EnableViewState = false;
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                lform.Controls.Add(dtgFacturas);

                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());
                //Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se Pude Generar el Excel.!" + ex.Message);
            }
        }

        /// <summary>   
        /// Metodo qwue realiza la consuilta a la base de datos y actualiza la informacion de la grilla
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            CargarDatos();
        }

        /// <summary>
        /// Metodo que llama el Servicio Web y Envia la Informacion de Facturacion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFactura_Click(object sender, EventArgs e)
        {
            var lblMensaje = new StringBuilder();
            var lblMensajeError = new StringBuilder();
            string lsNitOpera = "";
            string lsMensajeErr = "";
            string lsCodCompania = "";
            decimal ldValor = 0;
            bool lbContab = false;
            DateTime ldFecha;
            DateTime ldFechaFin;
            DateTime ldFechaini;
            string[] lsRetorno = { "", "" };
            string lsDesOpera = "";
            int liConta = 0;
            try
            {
                // Recorro la grilla para consumir los servicio por cada registro que se visualiza
                foreach (DataGridItem Grilla in dtgFacturas.Items)
                {
                    lsNitOpera = Grilla.Cells[1].Text;
                    if (Grilla.Cells[22].Text == "N")
                    {
                        if (Grilla.Cells[8].Text == "N")
                        {
                            hdfFechaCorte.Value = Grilla.Cells[0].Text;
                            ldValor = Convert.ToDecimal(Grilla.Cells[4].Text);
                            lsCodCompania = Grilla.Cells[16].Text;
                            if (ldValor > 0)
                            {
                                ldFecha = Convert.ToDateTime(Grilla.Cells[17].Text);
                                ldFechaFin = Convert.ToDateTime(Grilla.Cells[18].Text);
                                ldFechaini = Convert.ToDateTime(Grilla.Cells[19].Text);
                                if (!DelegadaBase.Servicios.ValidarExistencia("t_control_interfaz_facturacion", " year(fecha_corte) ='" + TxtAno.Text + "' And month(fecha_corte) = " + ddlMesFactura.SelectedValue + "  And no_documento = '" + lsNitOpera + "'  ", goInfo))
                                {
                                    /// Lamo el Metodo para la Nota debido del Mes del Servicio Prestado
                                    lsDesOpera = "SERVICIO DEL GESTOR DEL MERCADO DE GAS " + ddlMesFactura.SelectedItem + " " + TxtAno.Text;
                                    lsRetorno = enviarAnexo1(ldFecha, ldFechaFin, lsCodCompania, "832", lsNitOpera, ldValor, lsDesOpera, "D", "1");
                                    lbContab = true;
                                }
                                if (lsRetorno[0] == "OK")
                                {
                                    lblMensajeError.Append(lsRetorno[1].Trim() + "<br>");
                                    if (DelegadaBase.Servicios.ValidarExistencia("t_control_interfaz_facturacion", " year(fecha_corte) ='" + TxtAno.Text + "' And month(fecha_corte) = " + ddlMesFactura.SelectedValue + "  And no_documento = '" + lsNitOpera + "' And no_nota_credito = '0' ", goInfo))
                                    {
                                        /// Lamo el Metodo para la Nota Credito Reversion
                                        lsDesOpera = "REVERSION ND SERVICIO DEL GESTOR DEL MERCADO DE GAS " + ddlMesFactura.SelectedItem + " " + TxtAno.Text;
                                        lsRetorno = enviarAnexo1(ldFechaini, ldFechaini, lsCodCompania, "839", lsNitOpera, ldValor, lsDesOpera, "C", "2");
                                        lbContab = true;
                                    }
                                    if (lsRetorno[0] == "OK")
                                    {
                                        lblMensajeError.Append(lsRetorno[1].Trim() + "<br>");
                                        {
                                            /// Lamo el Metodo para la Factura
                                            if (DelegadaBase.Servicios.ValidarExistencia("t_control_interfaz_facturacion", " year(fecha_corte) ='" + TxtAno.Text + "' And month(fecha_corte) = " + ddlMesFactura.SelectedValue + "  And no_documento = '" + lsNitOpera + "' And no_factura = '0' ", goInfo))
                                                lsDesOpera = "SERVICIO DEL GESTOR DEL MERCADO DE GAS " + ddlMesFactura.SelectedItem + " " + TxtAno.Text;
                                            lsRetorno = enviarAnexo1(ldFechaini, ldFechaFin, lsCodCompania, "861", lsNitOpera, ldValor, lsDesOpera, "F", "3");
                                            lbContab = true;
                                        }
                                        if (lsRetorno[0] == "OK")
                                        {
                                            lblMensajeError.Append(lsRetorno[1].Trim() + "<br>");
                                            if (lbContab)
                                                liConta++;
                                        }
                                        else
                                        {
                                            lblMensaje.Append(lsRetorno[1] + "<br>");
                                            //break;
                                        }
                                    }
                                    else
                                    {
                                        lblMensaje.Append(lsRetorno[1] + "<br>");
                                        //break;
                                    }
                                }
                                else
                                {
                                    lblMensaje.Append(lsRetorno[1] + "<br>");
                                    //break;
                                }
                            }
                        }
                        else
                            lsMensajeErr += "El Nit. " + lsNitOpera + " fue modificado.\\n";
                    }
                    else
                        lsMensajeErr += "El Nit. " + lsNitOpera + " Ya fue contabilizado.\\n";

                }
                if (!string.IsNullOrEmpty(lblMensajeError.ToString()))
                {
                    if (liConta > 0)
                        Toastr.Success(this, "Interfaz Ejecutada con Exito para {" + liConta + "} registros.\\n" + lsMensajeErr);

                    else
                        Toastr.Error(this, "alert('" + "No se contabilizaron Registros.\\n");

                    CargarDatos();
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Append("Error al envio de la Informacion a Seven. " + ex.Message);
            }
            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                Toastr.Error(this, lblMensaje.ToString());
            }
        }

        /// <summary>
        /// Metodo que almacenael usuario y password del Servicio Web de Seven. 
        /// Fecha: 2017-11-14
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            var lsError = "";
            if (TxtUsrServicio.Text.Trim().Length <= 0)
                lsError = "Debe ingresar el Usuario del WebService.!";
            if (TxtClaveServicio.Text.Trim().Length <= 0)
                lsError = "Debe ingresar el Password del WebService.!";
            if (lsError == "")
            {
                try
                {
                    hdfUsrServicio.Value = TxtUsrServicio.Text;
                    hdfPwdServicio.Value = TxtClaveServicio.Text;
                    Modal.Cerrar(this, CrearRegistro.ID);
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, ex.Message);
                }
            }
            else
                Toastr.Error(this, lsError);

        }

        /// <summary>
        /// Metodo para ejecutar la interfaz de las modificaciones
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnModifcacion_Click(object sender, EventArgs e)
        {
            var lblMensaje = new StringBuilder();
            var lblMensajeError = new StringBuilder();
            string lsNitOpera = "";
            string lsCodCompania = "";
            string lsMensajeErr = "";
            decimal ldValor = 0;
            bool lbContab = false;
            decimal ldValorDv = 0;
            decimal ldValorCr = 0;
            DateTime ldFecha;
            DateTime ldFechaFin;
            DateTime ldFechaini;
            DateTime ldFechaMod;
            DateTime ldFechaModf;
            string[] lsRetorno = { "", "" };
            string lsDesOpera = "";
            int liConta = 0;
            try
            {
                // Recorro la grilla para consumir los servicio por cada registro que se visualiza
                foreach (DataGridItem Grilla in dtgFacturas.Items)
                {
                    lsNitOpera = Grilla.Cells[1].Text;
                    if (Grilla.Cells[22].Text == "N")
                    {
                        if (Grilla.Cells[8].Text == "S")
                        {
                            hdfFechaCorte.Value = Grilla.Cells[0].Text;
                            ldValor = Convert.ToDecimal(Grilla.Cells[4].Text.Replace(",", ""));
                            ldValorDv = Convert.ToDecimal(Grilla.Cells[9].Text.Replace(",", ""));
                            ldValorCr = Convert.ToDecimal(Grilla.Cells[10].Text.Replace(",", ""));
                            lsCodCompania = Grilla.Cells[16].Text;
                            ldFecha = Convert.ToDateTime(Grilla.Cells[17].Text);
                            ldFechaFin = Convert.ToDateTime(Grilla.Cells[18].Text);
                            ldFechaini = Convert.ToDateTime(Grilla.Cells[19].Text);
                            ldFechaMod = Convert.ToDateTime(Grilla.Cells[20].Text);
                            ldFechaModf = Convert.ToDateTime(Grilla.Cells[21].Text);

                            if (ldValorDv > 0)
                            {
                                if (DelegadaBase.Servicios.ValidarExistencia("t_control_interfaz_facturacion", " year(fecha_corte) ='" + TxtAno.Text + "' And month(fecha_corte) = " + ddlMesFactura.SelectedValue + "  And no_documento = '" + lsNitOpera + "' And no_nota_debito_ajuste = '0' ", goInfo))
                                {
                                    /// Lamo el Metodo para la Nota debido del Mes del Servicio Prestado de Modificacion
                                    lsDesOpera = "AJUSTE SERVICIO DEL GESTOR DEL MERCADO DE GAS " + ddlMesFactura.SelectedItem + " " + TxtAno.Text;
                                    lsRetorno = enviarAnexo1(ldFecha, ldFechaFin, lsCodCompania, "832", lsNitOpera, ldValorDv, lsDesOpera, "D", "5");
                                    lbContab = true;
                                }
                                if (lsRetorno[0] == "OK")
                                {
                                    lblMensajeError.Append(lsRetorno[1].Trim() + "<br>");
                                    if (DelegadaBase.Servicios.ValidarExistencia("t_control_interfaz_facturacion", " year(fecha_corte) ='" + TxtAno.Text + "' And month(fecha_corte) = " + ddlMesFactura.SelectedValue + "  And no_documento = '" + lsNitOpera + "' And no_factura_ajuste = '0' ", goInfo))
                                    {
                                        /// Lamo el Metodo para la Factura del Ajuste
                                        lsDesOpera = "AJUSTE SERVICIO DEL GESTOR DEL MERCADO DE GAS " + ddlMesFactura.SelectedItem + " " + TxtAno.Text;
                                        lsRetorno = enviarAnexo1(ldFechaMod, ldFechaModf, lsCodCompania, "861", lsNitOpera, ldValorDv, lsDesOpera, "F", "7");
                                        lbContab = true;
                                    }
                                    if (lsRetorno[0] == "OK")
                                    {
                                        lblMensajeError.Append(lsRetorno[1].Trim() + "<br>");
                                        if (DelegadaBase.Servicios.ValidarExistencia("t_control_interfaz_facturacion", " year(fecha_corte) ='" + TxtAno.Text + "' And month(fecha_corte) = " + ddlMesFactura.SelectedValue + "  And no_documento = '" + lsNitOpera + "' And no_nota_credito_rev_ajuste = '0' ", goInfo))
                                        {
                                            /// Lamo el Metodo para la Nota Credito Reversion Ajuste
                                            lsDesOpera = "REVERSION AJUSTE ND SERVICIO DEL GESTOR DEL MERCADO DE GAS " + ddlMesFactura.SelectedItem + " " + TxtAno.Text;
                                            lsRetorno = enviarAnexo1(ldFechaini, ldFechaFin, lsCodCompania, "839", lsNitOpera, ldValorDv, lsDesOpera, "C", "9");
                                            lbContab = true;
                                        }
                                        if (lsRetorno[0] == "OK")
                                        {
                                            lblMensajeError.Append(lsRetorno[1].Trim() + "<br>");
                                            if (lbContab)
                                                liConta++;
                                        }
                                        else
                                        {
                                            lblMensaje.Append(lsRetorno[1] + "<br>");
                                            //break;
                                        }
                                    }
                                    else
                                    {
                                        lblMensaje.Append(lsRetorno[1] + "<br>");
                                        //break;
                                    }
                                }
                                else
                                {
                                    lblMensaje.Append(lsRetorno[1] + "<br>");
                                    //break;
                                }
                            }
                            if (ldValorCr > 0)
                            {
                                if (DelegadaBase.Servicios.ValidarExistencia("t_control_interfaz_facturacion", " year(fecha_corte) ='" + TxtAno.Text + "' And month(fecha_corte) = " + ddlMesFactura.SelectedValue + "  And no_documento = '" + lsNitOpera + "' And no_nota_credito_ajuste = '0' ", goInfo))
                                {
                                    /// Lamo el Metodo para la Nota cREDITO aJUSTE del Mes del Servicio Prestado de Modificacion
                                    lsDesOpera = "AJUSTE SERVICIO DEL GESTOR DEL MERCADO DE GAS " + ddlMesFactura.SelectedItem + " " + TxtAno.Text;
                                    lsRetorno = enviarAnexo1(ldFecha, ldFecha, lsCodCompania, "839", lsNitOpera, ldValorCr, lsDesOpera, "C", "4");
                                    lbContab = true;
                                }
                                if (lsRetorno[0] == "OK")
                                {
                                    lblMensajeError.Append(lsRetorno[1].Trim() + "<br>");
                                    if (DelegadaBase.Servicios.ValidarExistencia("t_control_interfaz_facturacion", " year(fecha_corte) ='" + TxtAno.Text + "' And month(fecha_corte) = " + ddlMesFactura.SelectedValue + "  And no_documento = '" + lsNitOpera + "' And no_nota_debito_rev_ajuste = '0' ", goInfo))
                                    {
                                        /// Lamo el Metodo para la Nota Debito Reversion Modificacion
                                        lsDesOpera = "REVERSION AJUSTE SERVICIO DEL GESTOR DEL MERCADO DE GAS " + ddlMesFactura.SelectedItem + " " + TxtAno.Text;
                                        lsRetorno = enviarAnexo1(ldFechaini, ldFechaini, lsCodCompania, "832", lsNitOpera, ldValorCr, lsDesOpera, "D", "8");
                                        lbContab = true;
                                    }
                                    if (lsRetorno[0] == "OK")
                                    {
                                        lblMensajeError.Append(lsRetorno[1].Trim() + "<br>");
                                        if (DelegadaBase.Servicios.ValidarExistencia("t_control_interfaz_facturacion", " year(fecha_corte) ='" + TxtAno.Text + "' And month(fecha_corte) = " + ddlMesFactura.SelectedValue + "  And no_documento = '" + lsNitOpera + "' And no_nota_credito_rev_ajuste = '0' ", goInfo))
                                        {
                                            /// Lamo el Metodo para la Nota Credito Reversion Ajuste
                                            lsDesOpera = "AJUSTE SERVICIO DEL GESTOR DEL MERCADO DE GAS " + ddlMesFactura.SelectedItem + " " + TxtAno.Text;
                                            lsRetorno = enviarAnexo1(ldFechaini, ldFechaFin, lsCodCompania, "839", lsNitOpera, ldValorCr, lsDesOpera, "C", "6");
                                            lbContab = true;
                                        }
                                        if (lsRetorno[0] == "OK")
                                        {
                                            lblMensajeError.Append(lsRetorno[1].Trim() + "<br>");
                                            if (lbContab)
                                                liConta++;
                                        }
                                        else
                                        {
                                            lblMensaje.Append(lsRetorno[1] + "<br>");
                                            //break;
                                        }
                                    }
                                    else
                                    {
                                        lblMensaje.Append(lsRetorno[1] + "<br>");
                                        //break;
                                    }
                                }
                                else
                                {
                                    lblMensaje.Append(lsRetorno[1] + "<br>");
                                    //break;
                                }
                            }
                        }
                        else
                        {
                            lsMensajeErr += "El Nit. " + lsNitOpera + " No tiene  modificacion pendiente de contabilizar.\\n";
                        }
                    }
                    else
                    {
                        lsMensajeErr += "El Nit. " + lsNitOpera + " ya fue contabilizado.\\n";
                    }
                }
                if (!string.IsNullOrEmpty(lblMensajeError.ToString()))
                {
                    if (liConta > 0)
                        Toastr.Success(this, "Interfaz Ejecutada con Exito para {" + liConta + "} registros.\\n" + lsMensajeErr);
                    else
                        Toastr.Error(this, "No se contabilizaron Registros.\\n" + lsMensajeErr);
                    CargarDatos();
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "Error al envio de la Informacion a Seven. " + ex.Message);
            }
        }

        /// <summary>
        /// Metodo qhe Llama el Anexo Uno de la Facturacion
        /// </summary>
        /// <param name="ldFecha"></param>
        /// <param name="ldFechaFin"></param>
        /// <param name="lsCodCompania"></param>
        /// <param name="lsTipoOpera"></param>
        /// <param name="lsNitOpera"></param>
        /// <param name="ldValor"></param>
        /// <param name="lsDescripcion"></param>
        /// <returns></returns>
        protected string[] enviarAnexo1(DateTime ldFecha, DateTime ldFechaFin, string lsCodCompania, string lsTipoOpera, string lsNitOpera, decimal ldValor, string lsDescripcion, string lsTipoDoc, string lsAnexo)
        {
            string[] lsRetorno = { "OK", "" };
            try
            {
                // Llamo el Servicio Web Solo cuando la Columna Valor Total CxC es mayor a 0
                TSFaFactu oFactura = new TSFaFactu();
                // Cabecera de la Informacion
                oFactura.Emp_codi = Convert.ToInt32(lsCodCompania);
                oFactura.Top_codi = Convert.ToInt32(lsTipoOpera);
                //oFactura.Fac_nume = 1;
                oFactura.Fac_fech = ldFecha.ToUniversalTime();
                oFactura.Fac_desc = lsDescripcion;
                oFactura.Arb_csuc = "101";
                oFactura.Cli_coda = lsNitOpera;
                oFactura.Dcl_codd = 1;
                oFactura.Mon_codi = 1;
                oFactura.Fac_tdis = "A";
                oFactura.Fac_tipo = lsTipoDoc;
                oFactura.Fac_feta = ldFecha.ToUniversalTime();
                oFactura.Fac_feci = ldFecha.ToUniversalTime();
                oFactura.Fac_fecf = ldFechaFin.ToUniversalTime();
                oFactura.Fac_cref = "0";
                oFactura.Fac_fepo = ldFechaFin.ToUniversalTime();
                oFactura.Fac_fepe = ldFechaFin.ToUniversalTime();
                oFactura.Fac_fext = ldFechaFin.ToUniversalTime();
                oFactura.Fac_peri = DateTime.Now.Year.ToString();
                oFactura.Mco_cont = 0;
                oFactura.Fac_tido = "M";
                oFactura.Fac_pepe = 0;
                oFactura.Fac_pext = 0;
                oFactura.Fac_peri = TxtAno.Text;
                // Distribucion Autmática
                List<TSFaDdisp> oDistribuc = new List<TSFaDdisp>
                {
                    new TSFaDdisp { Arb_codi="10105002", Ddi_porc=100, Ddi_tipo="P", Ddi_valo=0, Tar_codi=1},
                    new TSFaDdisp { Arb_codi="101", Ddi_porc=100, Ddi_tipo="P", Ddi_valo=0, Tar_codi=2},
                    new TSFaDdisp { Arb_codi="1703001", Ddi_porc=100, Ddi_tipo="P", Ddi_valo=0, Tar_codi=3},
                    new TSFaDdisp { Arb_codi="0551", Ddi_porc=100, Ddi_tipo="P", Ddi_valo=0, Tar_codi=4},
                };

                // Detalle de la Informacion
                List<TSFaDfact> oDetalle = new List<TSFaDfact>
                {
                    new  TSFaDfact {
                        Bod_codi = 0,Pro_codi = "401180",Uni_codi = 1,Dfa_cant = 1,Dfa_valo = Convert.ToDouble(ldValor),
                        Dfa_tide = "P",Dfa_pvde = 0,Dfa_desc = lsDescripcion,Dfa_dest = "1",Ctr_cont = 0,
                        vDistribA=oDistribuc.ToArray()
                    }
                };
                // Asigno el detalle a la estructura de la factura
                oFactura.vDetalle = oDetalle.ToArray();
                SFAFACTU oServicio = new SFAFACTU();
                string lsRespuesta = oServicio.InsertarFactura(oFactura, hdfUsrServicio.Value, hdfPwdServicio.Value);
                //lblMensaje.Text += lsRespuesta + "<BR>";
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(lsRespuesta);
                XmlNodeList respuesta = xmlDoc.GetElementsByTagName("RETORNO");
                XmlNodeList MensajeError = xmlDoc.GetElementsByTagName("TXTERROR");
                XmlNodeList NoFacturaCnt = xmlDoc.GetElementsByTagName("FAC_CONT");
                XmlNodeList NoFactura = xmlDoc.GetElementsByTagName("FAC_NUME");
                Toastr.Error(this, "Nit: " + lsNitOpera + " - Respuesta: " + respuesta[0].InnerText + " - Error: " + MensajeError[0].InnerText + " - No. documento: " + NoFactura[0].InnerText + "<br>");
                if (respuesta[0].InnerText != "0")
                {
                    lsRetorno[0] = "Error";
                    lsRetorno[1] = "Nit: " + lsNitOpera + " - Respuesta: " + respuesta[0].InnerText + " - Error: " + MensajeError[0].InnerText;
                }
                else
                {
                    lsRetorno[1] = "Nit: " + lsNitOpera + " - Respuesta: " + respuesta[0].InnerText + " - No. documento: " + NoFactura[0].InnerText;
                    // Actualizo la Base de Datos con la fecha de compensacion, para que no se vuelva a ejecutar.
                    lConexion.Abrir();
                    string[] lsNombreParametrosC1 = { "@P_fecha_corte", "@P_no_documento", "@P_valor", "@P_tipo_operacion", "@P_doc_copntable" };
                    SqlDbType[] lTipoparametrosC1 = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Decimal, SqlDbType.VarChar, SqlDbType.VarChar };
                    Object[] lValorParametrosC1 = { hdfFechaCorte.Value, lsNitOpera, ldValor, lsAnexo, NoFactura[0].InnerText };
                    DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetControlInterfaz", lsNombreParametrosC1, lTipoparametrosC1, lValorParametrosC1, goInfo);
                    lConexion.Cerrar();
                }
                return lsRetorno;
            }
            catch (Exception ex)
            {
                lsRetorno[0] = "Error";
                lsRetorno[1] = "Error al envio de la Informacion a Seven. " + ex.Message + " Nit: " + lsNitOpera;
                return lsRetorno;
            }
        }
    }
}