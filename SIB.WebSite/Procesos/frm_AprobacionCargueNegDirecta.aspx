﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_AprobacionCargueNegDirecta.aspx.cs"
    Inherits="Procesos_frm_AprobacionCargueNegDirecta" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
                    width="90%">
                    <tr>
                        <td align="center" class="th1">
                            <asp:Label ID="lblTitulo" runat="server" ForeColor ="White"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br /><br /><br /><br /><br /><br />
                <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
                    width="90%">
                    <tr>
                        <td class="td1">
                            Fecha Negociación
                        </td>
                        <td class="td2">
                            <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                        </td>
                        <td class="td1">
                            Operador
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlOperador" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            Punta Inicial
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlPuntaInicial" runat="server">
                                <asp:ListItem Value="" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="C" Text="Compra"></asp:ListItem>
                                <asp:ListItem Value="V" Text="Venta"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="td1">
                            Estado
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlEstado" runat="server">
                                <asp:ListItem Value="" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="I" Text="Ingresado"></asp:ListItem>
                                <asp:ListItem Value="A" Text="Aprobado"></asp:ListItem>
                                <asp:ListItem Value="R" Text="Rechazado"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="95%" runat="server"
        id="tblMensaje">
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
                <asp:Button ID="btnAprobar" runat="server" Text="Aprobar" OnClientClick="return confirmar();"
                    OnClick="btnAprobar_Click" Visible="false" />
                <asp:Button ID="btnRechazar" runat="server" Text="Rechazar" OnClientClick="return confirmar1();"
                    OnClick="btnRechazar_Click" Visible="false" />
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla" visible="false">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 1150px; height: 350px;">
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Sele.">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ChkTodos" runat="server" OnCheckedChanged="ChkTodos_CheckedChanged"
                                        AutoPostBack="true" Text="Seleccionar Todos" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkRecibir" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="tipo_rueda" HeaderText="Tipo Rueda" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_negociacion" HeaderText="Fecha Negociacion" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="hora_negociacion" HeaderText="Hora Neg." ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="punta" HeaderText="Punta" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Width="80px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_compra" HeaderText="Operador Compra" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="operador_venta" HeaderText="Operador Venta" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="lugar_entrega" HeaderText="Lugar de Entrega" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="tipo_contrato" HeaderText="Tipo Contrato" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="periodo_entrega" HeaderText="Periodo Entrega" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="120px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="120px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="login_carga" Visible="false" HeaderText="Login Carga">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_carga" Visible="false" HeaderText="Fecha Carga">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="login_aprueba" Visible="false" HeaderText="Login Aprueba">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_aprueba" Visible="false" HeaderText="Fecha Aprueba">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="login_usuario" Visible="false" HeaderText="Fecha Usuario">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_hora_actual" Visible="false" HeaderText="Fecha Ultima Act">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_id_externo_directa" HeaderText="Id Carga"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador_c" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador_v" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="mail_c" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="mail_v" Visible="false"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>