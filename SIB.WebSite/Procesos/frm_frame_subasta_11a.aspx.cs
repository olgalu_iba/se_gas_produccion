﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Procesos
{
    public partial class frm_frame_subasta_11a : Page
    {
        #region Propiedades

        private InfoSessionVO goInfo;
        private clConexion lConexion;

        private string strRutaArchivo;

        /// <summary>
        /// 
        /// </summary>
        public string FechaRueda
        {
            get { return ViewState["FechaRueda"]?.ToString(); }
            set { ViewState["FechaRueda"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CodSubasta
        {
            get { return ViewState["CodSubasta"] != null && !string.IsNullOrEmpty(ViewState["CodSubasta"].ToString()) ? ViewState["CodSubasta"].ToString() : "0"; }
            set { ViewState["CodSubasta"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string TipoRueda
        {
            get { return ViewState["TipoRueda"]?.ToString(); }
            set { ViewState["TipoRueda"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string EstadoRueda
        {
            get { return Session["estado_rueda"] != null ? Session["estado_rueda"].ToString() : "C"; ; }
            set { Session["estado_rueda"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Destino
        {
            get { return ViewState["destino"] != null ? ViewState["destino"].ToString() : "G"; ; }
            set { ViewState["destino"] = value; }
        }


        /// <summary>
        /// 
        /// </summary>
        /// 20201207
        public string TipoRuedaTra
        {
            get { return ViewState["TipoRuedaTra"] != null ? ViewState["TipoRuedaTra"].ToString() : "R"; ; }
            set { ViewState["TipoRuedaTra"] = value; }
        }

        #endregion Propiedades

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Se cargan los valores iniciales del formulario 
                CargarPagina();
                if (IsPostBack) return;
                //Se inicializan los controles para la primera ves que se ejecuta el formulario 
                Session["hora"] = "";
                Session["mis_ofertas"] = "N";
                Session["refrescar_prm"] = "N";
                Session["refrescar_cont"] = "S";
                Session["ofertas"] = "N";

                InicializarPagina();
                CargaEstados();
                CargarDatosGrilla();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void CargarPagina()
        {
            //Se redirecciona al Login si no hay una sesión iniciada    
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("~/login.aspx");
            strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"];

            //Se inicializan las variables de sesión 
            
            //Eventos
            buttonsSubasta.RuedaOnSelectedIndexChanged += ddlRueda_SelectedIndexChanged;
            buttonsSubasta.PuntoOnSelectedIndexChanged += ddlPunto_SelectedIndexChanged;
            buttonsSubastaExtender.DecInfOnclick += DecInf_Click;
            buttonsSubastaExtender.DecManOnclick += DecIman_Click;
            buttonsSubastaExtender.DecPreOnclick += DecPrecioReserva_Click;
            buttonsSubastaExtender.OfertaOnclick += AbrirOferta;
            buttonsSubastaExtender.ContratoOnclick += AbrirContratos;
            buttonsSubastaExtender.SuspenderOnclick += Suspender_Click;
            buttonsSubastaExtender.ReactivarOnclick += Reactivar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;

            estados.TimeServer.Value = DateTime.Now.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// 
        /// </summary>
        private void CargaEstados()
        {
            // Se valida si la grilla esta visible 
            if (dtgSubasta.Columns.Count <= 0) return;

        }

        /// <summary>
        /// Inicializa el contenido del formulario 
        /// </summary>
        private void InicializarPagina()
        {
            //Titulo
            Master.Titulo = "Subasta";
            //Descripcion
            //Master.DescripcionPagina = "Úselo o véndalo a corto plazo";
            //Botones
            EnumBotonesSubasta[] botones = { EnumBotonesSubasta.Rueda, EnumBotonesSubasta.Punto };
            buttonsSubasta.Inicializar(botones: botones);
            EnumBotonesSubasta[] botonesExtender = { EnumBotonesSubasta.Oferta, EnumBotonesSubasta.Contrato };
            buttonsSubastaExtender.Inicializar(botonesExtender);

            //Se cargan los nombres de las pestañas 
            SeleccionarNombrePestanias();

            /*Se carga el select para la selección de la rueda*/
            FechaRueda = DateTime.Now.ToShortDateString();
            FechaRueda = DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day;
            //FechaRueda = FechaRueda.Substring(6, 4) + "/" + FechaRueda.Substring(3, 2) + "/" + FechaRueda.Substring(0, 2);
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            buttonsSubasta.ListaRueda = LlenarControles(lConexion.gObjConexion, "t_rueda rue", "rue.fecha_rueda = '" + FechaRueda + "' and rue.codigo_tipo_subasta = 11 ", 1, 17);
            lConexion.Cerrar();

            //Carga la información principal según la rueda seleccionada  
            if (Session["refrescar_prm"].ToString() == "S")
            {
                Session["refrescar_cont"] = "S";
                ddlRueda_SelectedIndexChanged(null, null);
            }
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// </summary>
        /// <param name="lConn"></param>
        /// <param name="lsTabla"></param>
        /// <param name="lsCondicion"></param>
        /// <param name="liIndiceLlave"></param>
        /// <param name="liIndiceDescripcion"></param>
        /// <returns></returns>
        private IEnumerable LlenarControles(SqlConnection lConn, string lsTabla, string lsCondicion, int liIndiceLlave, int liIndiceDescripcion)
        {
            var dummy = new DropDownList();

            SqlDataReader lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            while (lLector.Read())
            {
                var lItem1 = new ListItem
                {
                    Value = lLector.GetValue(liIndiceLlave).ToString(),
                    Text = lLector.GetValue(liIndiceDescripcion).ToString()
                };
                dummy.Items.Add(lItem1);
            }
            lLector.Close();
            return dummy.Items;
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        private void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            var lItem = new ListItem { Value = "0", Text = "Seleccione" };
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"] + "-" + lLector["razon_social"];
                }
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        private void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCampos, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlCommand lComando = new SqlCommand();
            lComando.Connection = lConn;
            lComando.CommandTimeout = 3600;
            lComando.CommandType = CommandType.StoredProcedure;
            lComando.CommandText = "pa_ValidarExistencia1";
            lComando.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = lsTabla;
            lComando.Parameters.Add("@P_campos", SqlDbType.VarChar).Value = lsCampos;
            lComando.Parameters.Add("@P_filtro", SqlDbType.VarChar).Value = lsCondicion;
            SqlDataReader lLector;
            lLector = lComando.ExecuteReader();

            var lItem = new ListItem { Value = "0", Text = "Seleccione" };
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles2(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Timer_Tick(object sender, EventArgs e)
        {
            try
            {
                //Se actualiza el relog
                //lblRelog.Text = $"Hora: {DateTime.Now.ToString("HH:mm:ss").Substring(0, 8)}";

                if (Session["ofertas"].ToString() == "S")
                {
                    Session["ofertas"] = "N";
                    CargarDatosGrilla();
                }
                else
                {
                    var oArchivo = strRutaArchivo + "Subasta-" + CodSubasta + ".txt";
                    var xxx = Session["hora"].ToString();
                    var yyy = File.GetCreationTime(oArchivo).ToString("HH:mm:ss");
                    if (Session["hora"].ToString() != File.GetCreationTime(oArchivo).ToString("HH:mm:ss"))
                    {
                        Session["hora"] = File.GetCreationTime(oArchivo).ToString("HH:mm:ss");
                        Session["mis_ofertas"] = "N";
                        Session["refrescar_prm"] = "S";
                        InicializarPagina();
                    }
                    else
                    {
                        if (Session["mis_ofertas"].ToString() != "S") return;
                        Session["mis_ofertas"] = "N";
                        CargarDatosGrilla();
                    }

                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void CargarDatosGrilla(bool primeraCarga = false)
        {
            try
            {
                dtgSubasta.Visible = false;

                if (CodSubasta.Equals("0") && !primeraCarga || divTiempoEspera.Visible)
                {
                    dtgSubasta.Columns.Clear();
                    return;
                }


                string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_punto" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };

                string[] primeraBusqueda = { "0", "0" };
                string[] busquedasGenerales = { CodSubasta, buttonsSubasta.ValorPunto };
                var lValorParametros = primeraCarga ? primeraBusqueda : busquedasGenerales;

                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                if (EstadoRueda != "6" && EstadoRueda != "7")
                {
                    dtgSubasta.Visible = true;
                    dtgRechazo.Visible = false;
                    dtgContratos.Visible = false;
                    dtgSubasta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaPuja11", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgSubasta.DataBind();
                    if (EstadoRueda == "F")
                    {
                        dtgSubasta.Columns[9].Visible = false;
                        dtgSubasta.Columns[10].Visible = false;
                        dtgSubasta.Columns[11].Visible = false;
                        dtgSubasta.Columns[12].Visible = true;
                    }
                    else
                        dtgSubasta.Columns[12].Visible = false;
                }
                if (EstadoRueda == "6")
                {
                    dtgSubasta.Visible = false;
                    dtgRechazo.Visible = true;
                    dtgContratos.Visible = false;
                    dtgRechazo.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaPuja11", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgRechazo.DataBind();
                }
                if (EstadoRueda == "7")
                {
                    dtgSubasta.Visible = false;
                    dtgRechazo.Visible = false;
                    dtgContratos.Visible = true;
                    dtgContratos.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaPuja11", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgContratos.DataBind();
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: ddlRueda_SelectedIndexChanged
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Obtiene la Informacion de la Inactivacion del Comisionista para Visualizarla en la pantalla
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlRueda_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Botones Subasta
            buttonsSubastaExtender.SwitchOnButton(EnumBotonesSubasta.Ninguno);
            var botones = new[] { EnumBotonesSubasta.Oferta, EnumBotonesSubasta.Contrato }.OfType<EnumBotonesSubasta>().ToList();

            //Lista para el punto o ruta
            var ddlNueva = new DropDownList();

            lConexion = new clConexion(goInfo);
            Session["refrescar_cont"] = "S";
            buttons.SwitchOnButton(EnumBotones.Ninguno); //20160128

            //Actualiza los datos de la subasta seleccionda 
            if (Session["refrescar_prm"].ToString() == "S")
            {
                Session["refrescar_prm"] = "N";
                buttonsSubasta.ValorRueda = CodSubasta;
            }

            #region Carga Estado Rueda

            lConexion.Abrir();
            SqlDataReader lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda rue,m_tipos_rueda tpo, m_estado_gas est", " rue.numero_rueda =" + buttonsSubasta.ValorRueda + " and rue.codigo_tipo_rueda = tpo.codigo_tipo_rueda and est.tipo_estado ='R' and rue.estado = est.sigla_estado");
            if (lLector.HasRows)
            {
                lLector.Read();

                /*Se deshabilitan los estados y cronómetros*/
                estados.Visible = true;

                estados.Pestania1.CssClass = "nav-link disabled";
                estados.Pestania2.CssClass = "nav-link disabled";
                estados.Pestania3.CssClass = "nav-link disabled";
                estados.Pestania4.CssClass = "nav-link disabled";
                estados.Pestania4.CssClass = "nav-link disabled";
                estados.Pestania6.CssClass = "nav-link disabled";
                estados.Pestania7.CssClass = "nav-link disabled";

                estados.Pestania1.Visible = true;
                estados.Pestania2.Visible = true;
                estados.Pestania3.Visible = true;
                estados.Pestania4.Visible = true;
                estados.Pestania5.Visible = true;
                estados.Pestania6.Visible = true;
                estados.Pestania7.Visible = true;

                estados.Hora1.Visible = false;
                estados.Hora2.Visible = false;
                estados.Hora3.Visible = false;
                estados.Hora4.Visible = false;
                estados.Hora5.Visible = false;
                estados.Hora6.Visible = false;
                estados.Hora7.Visible = false;



                EstadoRueda = lLector["estado"].ToString(); //20170508 se sube para corregir error de actualizacion
                Destino = lLector["destino_rueda"].ToString(); //20170508 se sube para corregir error de actualizacion

                //Tiempo espera
                divTiempoEspera.Visible = false;
                estados.Time.Value = DateTime.Now.ToShortTimeString();

                switch (lLector["estado"].ToString())
                {
                    case "C":
                        estados.Time.Value = lLector["hora_ini_publi_v_sci"].ToString();
                        estados.Pestania1.CssClass = "nav-link active";
                        break;
                    case "3":
                        estados.Pestania2.CssClass = "nav-link active";
                        estados.Time.Value = lLector["hora_fin_publi_v_sci"].ToString();
                        estados.Hora2.Text = $"{lLector["hora_ini_publi_v_sci"]} - {lLector["hora_fin_publi_v_sci"]}";
                        estados.Hora2.Visible = true;
                        estados.Pestania2.Visible = true;
                        buttons.SwitchOnButton(EnumBotones.Excel);
                        break;
                    case "4":
                        estados.Pestania3.CssClass = "nav-link active";
                        estados.Time.Value = lLector["hora_fin_modif_decla_sb"].ToString();
                        estados.Hora3.Text = $"{lLector["hora_ini_modif_decla_sb"]} - {lLector["hora_fin_modif_decla_sb"]}";
                        estados.Hora3.Visible = true;
                        estados.Pestania3.Visible = true;
                        break;
                    case "5":
                        estados.Pestania4.CssClass = "nav-link active";
                        estados.Time.Value = lLector["hora_fin_negociacioni_sci"].ToString();
                        estados.Hora4.Text = $"{ lLector["hora_ini_negociacioni_sci"]} - {lLector["hora_fin_negociacioni_sci"]}";
                        estados.Hora4.Visible = true;
                        estados.Pestania4.Visible = true;
                        break;
                    case "6":
                        estados.Pestania5.CssClass = "nav-link active";
                        estados.Time.Value = lLector["hora_fin_rec_solicitud_c_sci"].ToString();
                        estados.Hora5.Text = $"{ lLector["hora_ini_rec_solicitud_c_sci"]} - {lLector["hora_fin_rec_solicitud_c_sci"]}";
                        estados.Hora5.Visible = true;
                        estados.Pestania5.Visible = true;
                        break;
                    case "7":
                        estados.Pestania6.CssClass = "nav-link active";
                        estados.Time.Value = lLector["hora_fin_publ_cnt_dispi_v_sci"].ToString();
                        estados.Hora6.Text = $"{ lLector["hora_ini_publ_cnt_dispi_v_sci"]} - {lLector["hora_fin_publ_cnt_dispi_v_sci"]}";
                        estados.Hora6.Visible = true;
                        estados.Pestania6.Visible = true;
                        break;
                    case "F":
                        estados.Time.Value = "23:59:59";
                        estados.Pestania7.CssClass = "nav-link active";
                        break;
                    default:
                        dtgSubasta.Columns.Clear();
                        dtgRechazo.Columns.Clear();
                        dtgContratos.Columns.Clear();
                        divTiempoEspera.Visible = true;
                        break;
                }
                TipoRuedaTra = lLector["ind_tipo_rueda"].ToString(); //20201207
                //Se cargan los nombres de las pestañas 
                SeleccionarNombrePestanias(lLector["estado"].ToString(), lLector["descripcion_estado"].ToString());

                TipoRueda = lLector["codigo_tipo_rueda"].ToString();
            }
            else
            {
                estados.Visible = false;
                EstadoRueda = "F";
                Destino = "G";
                TipoRueda = "0";
                TipoRuedaTra = "R"; //20201207
            }
            lLector.Close();

            #endregion Carga Estado Rueda

            CodSubasta = buttonsSubasta.ValorRueda;

            if (goInfo.cod_comisionista != "0" || EstadoRueda == "F" || EstadoRueda == "Z")
            {
                //  botones.AddRange(new[] { EnumBotonesSubasta.Suspender }.OfType<EnumBotonesSubasta>().ToList());
            }
            else if (EstadoRueda == "S")
            {
                botones.AddRange(new[] { EnumBotonesSubasta.Reactivar }.OfType<EnumBotonesSubasta>().ToList());
            }
            else
            {
                botones.AddRange(new[] { EnumBotonesSubasta.Suspender }.OfType<EnumBotonesSubasta>().ToList());
            }
            lConexion.Cerrar();
            lConexion.Abrir();

            /*Se llena el select para la selección del punto o la ruta*/
            //Si la lista de rueda solo tiene el seleccione
            if (buttonsSubasta.ValorRueda == "0")
            {
                var lItem = new ListItem { Value = "0", Text = "Seleccione" };
                ddlNueva.Items.Add(lItem);
                buttonsSubasta.ListaPunto = ddlNueva.Items;
            }
            else if (Destino == "G" && sender != null && e != null)
            {
                var lItem = new ListItem { Value = "0", Text = "Seleccione Punto" };
                ddlNueva.Items.Add(lItem);
                ddlNueva.Items.AddRange(LlenarControles(lConexion.gObjConexion, "m_pozo", "  estado ='A' order by descripcion", 0, 1).Cast<ListItem>().ToArray());
                buttonsSubasta.ListaPunto = ddlNueva.Items;
            }
            else if (Destino == "T" && sender != null && e != null)
            {
                var lItem = new ListItem { Value = "0", Text = "Seleccione Ruta" };
                ddlNueva.Items.Add(lItem);
                ddlNueva.Items.AddRange(LlenarControles(lConexion.gObjConexion, "m_ruta_snt", "  estado ='A' order by descripcion", 0, 4).Cast<ListItem>().ToArray());
                buttonsSubasta.ListaPunto = ddlNueva.Items;
            }
            lConexion.Cerrar();

            //Se agregan los botones
            buttonsSubastaExtender.SwitchOnButton(botones.ToArray());

            //Se refresca la grilla  
            CargarDatosGrilla();

            if (Session["tipoPerfil"].ToString() != "B") return;
        }

        /// <summary>
        /// 
        /// </summary>
        protected void SeleccionarNombrePestanias(string estado = null, string descripcion = null)
        {
            //Estado
            estados.Pestania1.Text = "<b>1. Creada</b>";
            estados.Pestania2.Text = "<b>1. Publicación</b> de oferta";
            estados.Pestania3.Text = "<b>2. Declaración</b> posturas de compra";
            estados.Pestania4.Text = "<b>3. Desarrollo</b> de subasta";
            estados.Pestania5.Text = "<b>4. Rechazo</b> adjudicaciones";
            estados.Pestania6.Text = "<b>5. Publicación</b> resultados";
            estados.Pestania7.Text = "<b>6. Finalización</b> Subasta";

        }

        #region Eventos de los Botones

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para Procesar la consulta
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPunto_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["refrescar"] = "S";
            Session["refresca_mis_posturas"] = "S";
            CargaEstados();
            CargarDatosGrilla();
        }

        /// 20160128
        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] nombreParametros = { "@P_numero_rueda", "@P_codigo_punto" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { buttonsSubasta.ValorRueda, buttonsSubasta.ValorPunto };
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            dtgSubasta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaPujaExc", nombreParametros, lTipoparametros, lValorParametros);
            dtgSubasta.DataBind();
            dtgSubasta.Visible = true;

            DataSet lds = new DataSet();
            SqlDataAdapter lsqldata = new SqlDataAdapter();
            string nombreArchivo = goInfo.Usuario + "InfRueda" + DateTime.Now + ".xls";
            string lstituloInforme = "Consulta Ids Subasta " + buttonsSubasta.ValorRueda;
            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            dtgSubasta.EnableViewState = false;
            lform.Controls.Add(dtgSubasta);
            lpagina.RenderControl(lhtw);
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + nombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + nombreArchivo + "\"");
            Response.ContentEncoding = Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstituloInforme + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());
            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
            dtgSubasta.Visible = false;
        }

        #endregion Eventos de los Botones

        #region Eventos de las grillas   

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnItemCommand_Click(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                switch (((ImageButton)e.CommandSource).ID)
                {
                    // Muestra la oferta
                    case "imbOfertar":
                        OpenIngresoPosturasCompra(e.Item.Cells[0].Text, "C", e.Item.Cells[16].Text);
                        break;
                    //Muetra la modificacion
                    case "imbModificar":
                        OpenIngresoPosturasCompra(e.Item.Cells[0].Text, "M", e.Item.Cells[16].Text);
                        break;
                    // Muestra la grafica
                    case "imbGrafica":
                        OpenCurvaOferDemAgre(e.Item.Cells[0].Text);
                        break;
                    // Elimina el registro 
                    case "imbEliminar":
                        {
                            string[] nombreParametros = { "@P_numero_rueda", "@P_numero_id", "@P_estado" };
                            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Char };
                            object[] lValorParametros = { e.Item.Cells[15].Text, e.Item.Cells[0].Text, e.Item.Cells[13].Text };
                            SqlDataReader lLector;
                            lConexion = new clConexion(goInfo);
                            lConexion.Abrir();
                            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_DelPostura11", nombreParametros, lTipoparametros, lValorParametros);
                            if (lLector.HasRows)
                            {
                                while (lLector.Read())
                                {
                                    Toastr.Error(this, lLector["error"].ToString());
                                }
                                lLector.Close();
                                lLector.Dispose();
                                lConexion.Cerrar();
                            }
                            else
                            {
                                Toastr.Success(this, "Oferta eliminada correctamente");
                                lLector.Close();
                                lLector.Dispose();
                                lConexion.Cerrar();
                                CargarDatosGrilla();
                            }

                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnItemCommand1_Click(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                switch (((ImageButton)e.CommandSource).ID)
                {
                    //Muetra la modificacion
                    case "imbModificar":
                        if (e.Item.Cells[10].Text == goInfo.cod_comisionista)
                            ModActDeclaracionInf_Click(e.Item.Cells[11].Text);
                        else
                            ModActDeclaracionPre_Click(e.Item.Cells[11].Text);
                        break;
                    case "imbEliminar":
                        {
                            string[] nombreParametros = { "@P_numero_rueda", "@P_codigo_declaracion_inf", "@P_codigo_operador" };
                            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                            object[] lValorParametros = { CodSubasta, e.Item.Cells[11].Text, goInfo.cod_comisionista };
                            SqlDataReader lLector;
                            lConexion = new clConexion(goInfo);
                            lConexion.Abrir();
                            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_DelDeclaracionInf", nombreParametros, lTipoparametros, lValorParametros);
                            if (lLector.HasRows)
                            {
                                while (lLector.Read())
                                {
                                    Toastr.Error(this, lLector["error"].ToString());
                                }
                                lLector.Close();
                                lLector.Dispose();
                                lConexion.Cerrar();
                            }
                            else
                            {
                                Toastr.Success(this, "Declaraión de infromación eliminada correctamente");
                                lLector.Close();
                                lLector.Dispose();
                                lConexion.Cerrar();
                                CargarDatosGrilla();
                            }

                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Eventos de las grillas   

        #region Modals

        #region Declaración De Precio De Reserva

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DecPrecioReserva_Click(object sender, EventArgs e)
        {
            try
            {
                //Se abre el modal de Declaración De Precio De Reserva
                Modal.Abrir(this, mdlDeclaraPrecioRes.ID, mdlDeclaraPrecioResInside.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            try
            {
                string strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
                string strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];
                var ltCargaArchivo = new StringBuilder();
                var lsRutaArchivo = "";
                var lsNombre = "";
                string[] lsErrores = { "", "" };
                var oCargaOK = true;
                var lComando = new SqlCommand();
                lConexion = new clConexion(goInfo);
                string[] lsNombreParametrosO =
                    {"@P_archivo", "@P_codigo_trasportador", "@P_numero_rueda", "@P_ruta_ftp"};
                SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
                object[] lValorParametrosO = { "", "0", CodSubasta, strRutaFTP };
                lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;
                var slArchivo = "Debe seleccionar el archivo<br>";

                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);

                if (FuArchivo.FileName != "")
                    lsErrores = ValidarArchivo(lsRutaArchivo);
                else
                    lsErrores[0] = slArchivo;

                if (lsErrores[0] == "")
                {
                    if (DelegadaBase.Servicios.put_archivo(lsRutaArchivo,
                        ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre,
                        ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]))
                    {
                        lValorParametrosO[0] = lsNombre;
                        lValorParametrosO[1] = goInfo.cod_comisionista;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = Destino == "T" ? "pa_ValidaPlanoDecPreT" : "pa_ValidaPlanoDecPre";
                        lComando.CommandTimeout = 3600;
                        int liNumeroParametros;
                        for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                        {
                            lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                        }

                        SqlDataReader lLector = lComando.ExecuteReader();
                        //lLector = DelegadaOMA.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_ValidaPlanoOpe", lsNombreParametrosO, lTipoparametrosO, lValorParametrosO);
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                ltCargaArchivo.Append(lLector["Mensaje"] + "<br>");
                            }

                            Session["ofertas"] = "S";
                        }
                        else
                        {
                            Toastr.Success(this, HttpContext.GetGlobalResourceObject("AppResources", "DeclaracionCorrecta", CultureInfo.CurrentCulture)?.ToString());
                            //Se cierra el modal 
                            CloseDecPrecioReserva_Click(null, null);
                            //Se refresca la grilla  
                            CargarDatosGrilla();
                        }

                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString());
                        return;
                    }
                }
                else
                {
                    if (lsErrores[0].Equals(slArchivo))
                        Toastr.Warning(this, lsErrores[0]);
                    else
                        ltCargaArchivo.Append(lsErrores[0]);
                    DelegadaBase.Servicios.registrarProceso(goInfo, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCarga", CultureInfo.CurrentCulture)?.ToString(), "Usuario : " + goInfo.nombre);
                }

                //Se refresca la grilla  
                CargarDatosGrilla();

                if (ltCargaArchivo.ToString().Contains("Se crearon correctamente"))
                {
                    var split = ltCargaArchivo.ToString().Replace("registros", ";").Split(';');
                    Toastr.Success(this, $"{split[0]} registros");
                    ltCargaArchivo = ltCargaArchivo.Remove(0, split[0].Length + 9);
                }

                //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                LogCargaArchivo.DownloadBugsLog(this, ltCargaArchivo);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "Se presento un problema al cargar los datos del archivo!. <br>" + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            Int32 liNumeroLinea = 0;
            decimal ldValor = 0;
            int liTotalRegistros = 0;
            Int64 liValor = 0;
            string lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            StreamReader lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                if (Destino == "T")
                {
                    /// Recorro el Archivo de Excel para Validarlo
                    lLectorArchivo = File.OpenText(lsRutaArchivo);
                    while (!lLectorArchivo.EndOfStream)
                    {
                        liTotalRegistros = liTotalRegistros + 1;
                        /// Obtiene la fila del Archivo
                        string lsLineaArchivo = lLectorArchivo.ReadLine();
                        //if (lsLineaArchivo.Length > 0)
                        //{
                        liNumeroLinea = liNumeroLinea + 1;
                        /// Pasa la linea sepaada por Comas a un Arreglo
                        Array oArregloLinea = lsLineaArchivo.Split(',');
                        if ((oArregloLinea.Length != 3))
                        {
                            lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 3 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        else
                        {
                            /// Valida Punto de entrega
                            try
                            {
                                ldValor = Convert.ToInt32(oArregloLinea.GetValue(0).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El código del punto inicial de entrega{" + oArregloLinea.GetValue(0) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// Valida Punto de entrega fin
                            try
                            {
                                ldValor = Convert.ToInt32(oArregloLinea.GetValue(1).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El código del punto final de entrega{" + oArregloLinea.GetValue(1) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// Valida cantidad
                            try
                            {
                                ldValor = Convert.ToInt32(oArregloLinea.GetValue(2).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La cantidad no disponible  {" + oArregloLinea.GetValue(2) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                    }
                    lLectorArchivo.Close();
                    lLectorArchivo.Dispose();
                }
                else
                {
                    /// Recorro el Archivo de Excel para Validarlo
                    lLectorArchivo = File.OpenText(lsRutaArchivo);
                    while (!lLectorArchivo.EndOfStream)
                    {
                        liTotalRegistros = liTotalRegistros + 1;
                        /// Obtiene la fila del Archivo
                        string lsLineaArchivo = lLectorArchivo.ReadLine();
                        //if (lsLineaArchivo.Length > 0)
                        //{
                        liNumeroLinea = liNumeroLinea + 1;
                        /// Pasa la linea sepaada por Comas a un Arreglo
                        Array oArregloLinea = lsLineaArchivo.Split(',');
                        if ((oArregloLinea.Length != 3))
                        {
                            lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 3 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        else
                        {
                            /// Valida Punto de entrega
                            try
                            {
                                ldValor = Convert.ToInt32(oArregloLinea.GetValue(0).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El código del punto de entrega{" + oArregloLinea.GetValue(0) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// Valida cantidad
                            try
                            {
                                ldValor = Convert.ToInt32(oArregloLinea.GetValue(1).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La cantidad no disponible  {" + oArregloLinea.GetValue(1) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// Valida Precio
                            try
                            {
                                ldValor = Convert.ToDecimal(oArregloLinea.GetValue(2).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El precio de reserva {" + oArregloLinea.GetValue(2) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }

                        }
                    }
                    lLectorArchivo.Close();
                    lLectorArchivo.Dispose();
                }
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseDecPrecioReserva_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Declaración De Precio De Reserva
                Modal.Cerrar(this, mdlDeclaraPrecioRes.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Declaración De Precio De Reserva

        #region Suspender

        /// <summary>
        /// Se encarga de inicializar el Modal para suspender una rueda 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Suspender_Click(object sender, EventArgs e)
        {
            SqlDataReader lLector = null;

            try
            {
                // Obtengo los Datos de la rueda
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda rue, m_estado_gas est", " rue.numero_rueda = " + CodSubasta + " and est.tipo_estado ='R' and rue.estado = est.sigla_estado ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    // Lleno las etiquetas de la pantalla con los datos del Id.
                    lblEstado.InnerText = "Estado Actual: " + lLector["sigla_estado"] + " - " + lLector["descripcion_estado"];
                    lLector.Close();
                    lLector.Dispose();

                    Modal.Abrir(this, mdlSuspender.ID, mdlInsideSuspender.ID);
                }
                else
                {
                    lblEstado.InnerText = "";
                    Toastr.Warning(this, "Lo sentimos, no tiene una rueda que suspender.", "Advertencia!", 5000);
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
            finally
            {
                if (lLector != null)
                {
                    lLector.Dispose();
                    lLector.Close();
                    lConexion.Cerrar();
                };
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSuspender_Click(object sender, EventArgs e)
        {
            SqlDataReader lLector;

            try
            {
                string strRutaArchivoModal = ConfigurationManager.AppSettings["RutaArchivos"] + "mensaje.txt";

                string oError = "";
                string sMensaje;
                double ldPreAct = 0;
                double ldPreRes = 0;

                if (TxtObservacion.Text == "")
                    oError += "Debe digitar las observaciones de la suspension\\n";

                if (oError == "")
                {
                    string[] lsNombreParametros = { "@P_numero_rueda", "@P_ind_definitivo", "@P_observaciones" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char, SqlDbType.VarChar };
                    string[] lValorParametros = { CodSubasta, ddlDefinitivo.SelectedValue, TxtObservacion.Text }; ///  

                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    try
                    {
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetSuspRueda", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                                oError = lLector["error"] + "\\n";
                        }
                        if (oError == "")
                        {
                            sMensaje = "la rueda se suspendio exitosamente";
                            File.SetCreationTime(strRutaArchivoModal, DateTime.Now);

                            Toastr.Warning(this, sMensaje);

                            Session["hora"] = "";
                        }
                        lConexion.Cerrar();
                    }
                    catch (Exception ex)
                    {
                        oError = "Error al suspender la rueda. " + ex.Message;
                        lConexion.Cerrar();
                    }
                }
                if (oError != "")
                {
                    Toastr.Error(this, oError);
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseSuspender_Click(Object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Declaración de Información Archivo
                Modal.Cerrar(this, mdlSuspender.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Suspender

        #region Reactivar

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Reactivar_Click(object sender, EventArgs e)
        {
            SqlDataReader lLector = null;
            string fase;

            try
            {
                //Obtengo los Datos de la rueda
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda rue, m_estado_gas est", " rue.numero_rueda = " + CodSubasta + " and rue.fase_reactivacion= est.sigla_estado and est.tipo_estado ='R'");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    /// Lleno las etiquetas de la pantalla con los datos del Id.
                    fase = lLector["fase_reactivacion"].ToString();
                    lblEstado.InnerText = "Fase Re-Activación: " + fase + " - " + lLector["descripcion_estado"];
                    if (lLector["hora_ini_modif_cont"].ToString() == "") horaCont.Disabled = false;
                    lLector.Close();
                    lLector.Dispose();
                }
                else
                {
                    lblEstado.InnerText = "";
                    fase = "C";
                }
                var liFase = 0;
                if (fase != "C") liFase = Convert.ToInt16(fase);
                if (liFase > 1) horaPubV.Disabled = false;
                if (liFase > 2) horaCntD.Disabled = false;
                if (liFase > 3) horaComp.Disabled = false;
                if (liFase > 4) horaNeg.Disabled = false;

                lLector.Dispose();
                lLector.Close();
                lConexion.Cerrar();

                //Se abre el modal de Reactivar
                Modal.Abrir(this, mdlReactivar.ID, mdlInsideReactivar.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
            finally
            {
                if (lLector != null)
                {
                    lLector.Dispose();
                    lLector.Close();
                    lConexion.Cerrar();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReactivar_Click(object sender, EventArgs e)
        {
            SqlDataReader lLector;

            try
            {
                string strRutaArchivoModal = ConfigurationManager.AppSettings["RutaArchivos"] + "mensaje.txt";
                var oError = "";
                string sMensaje;

                //if (TxtHora.Text == "")
                //    oError += "Debe digitar la Hora de inicio de la próxima ronda\\n";
                //if (TxtMinutos.Text == "")
                //    oError += "Debe la duración de la próxima ronda\\n";
                if (TxtObservacionReac.Text == "")
                    oError += "Debe digitar las observaciones de la reactivaccion\\n";

                if (oError == "")
                {
                    string[] nombreParametros = { "@P_numero_rueda", "@P_observaciones", "@P_inicio_fase1", "@P_fin_fase1", "@P_inicio_fase2", "@P_fin_fase2", "@P_inicio_fase3", "@P_fin_fase3", "@P_inicio_fase4", "@P_fin_fase4", "@P_inicio_fase5", "@P_fin_fase5", "@P_inicio_fase6", "@P_fin_fase6" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
                    object[] lValorParametros = { CodSubasta, TxtObservacionReac.Text, txtHoraIniPubV.Text, txtHoraFinPubV.Text, txtHoraIniCntD.Text, txtHoraFinCntD.Text, txtHoraIniComp.Text, txtHoraFinComp.Text, txtHoraIniNeg.Text, txtHoraFinNeg.Text, txtHoraIniCalce.Text, txtHoraFinCalce.Text, txtHoraIniCont.Text, txtHoraFinCont.Text };

                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    try
                    {
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetActRueda", nombreParametros, lTipoparametros, lValorParametros, goInfo);
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                                oError += lLector["error"] + "\\n";
                        }
                        if (oError == "")
                        {
                            sMensaje = "la rueda se reactivó exitosamente";
                            File.SetCreationTime(strRutaArchivoModal, DateTime.Now);

                            Toastr.Warning(this, sMensaje);

                            Session["hora"] = "";
                            ScriptManager.RegisterStartupScript(this, GetType(), "CLOSE_WINDOW", "window.close();", true);
                        }
                        lConexion.Cerrar();
                    }
                    catch (Exception ex)
                    {
                        oError = "Error al reactivar la rueda. " + ex.Message;
                        lConexion.Cerrar();
                    }
                }
                if (oError != "")
                {
                    Toastr.Error(this, oError);
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseReactivar_Click(Object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Reactivar
                Modal.Cerrar(this, mdlReactivar.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Reactivar

        #region Declaración de Información Archivo

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DecInf_Click(object sender, EventArgs e)
        {
            try
            {
                //Se abre el modal de Ingreso de Declaración de Información
                Modal.Abrir(this, mdlDecInfo.ID, mdlDecInfolInside.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargarDec_Click(object sender, EventArgs e)
        {
            try
            {
                var strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
                var strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];
                var lsRutaArchivo = "";
                var lsNombre = "";
                string[] lsErrores = { "", "" };
                var lsCadenaArchivo = new StringBuilder();
                var oTransOK = true;
                var oCargaOK = true;
                SqlDataReader lLector;
                var lComando = new SqlCommand();
                int liNumeroParametros;
                lConexion = new clConexion(goInfo);
                string[] lsNombreParametrosO = { "@P_archivo", "@P_codigo_declarador", "@P_numero_rueda", "@P_ruta_ftp" };
                SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
                object[] lValorParametrosO = { "", "0", CodSubasta, strRutaFTP };
                lsNombre = DateTime.Now.Millisecond + FileUpload1.FileName;
                var slArchivo = "Debe seleccionar el archivo<br>";

                lsRutaArchivo = strRutaCarga + lsNombre;
                FileUpload1.SaveAs(lsRutaArchivo);

                /// Realiza las Validaciones de los Archivos
                if (FileUpload1.FileName != "")
                    lsErrores = ValidarArchivo2(lsRutaArchivo);
                else
                    lsErrores[0] = slArchivo;
                if (string.IsNullOrEmpty(lsErrores[0]))
                {
                    if (DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]))
                    {
                        lValorParametrosO[0] = lsNombre;
                        lValorParametrosO[1] = goInfo.cod_comisionista;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = Destino == "T" ? "pa_ValidaPlanoDecInfT" : "pa_ValidaPlanoDecInf";
                        lComando.CommandTimeout = 3600;

                        if (lsNombreParametrosO != null)
                        {
                            for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                            {
                                lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                            }
                        }

                        lLector = lComando.ExecuteReader();
                        //lLector = DelegadaOMA.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_ValidaPlanoOpe", lsNombreParametrosO, lTipoparametrosO, lValorParametrosO);
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                lsCadenaArchivo.Append(lLector["Mensaje"] + "<br>");
                            }

                            Session["ofertas"] = "S";
                        }
                        else
                        {
                            Session["mis_ofertas"] = "S";
                            Toastr.Success(this, HttpContext.GetGlobalResourceObject("AppResources", "DeclaracionInfoCorrecta", CultureInfo.CurrentCulture)?.ToString());
                            //Se cierra el modal de Declaración de Información
                            CloseCargarDec_Click(null, null);
                        }

                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString());
                        return;
                    }
                }
                else
                {
                    if (lsErrores[0].Equals(slArchivo))
                        Toastr.Warning(this, lsErrores[0]);
                    else
                        lsCadenaArchivo.Append(lsErrores[0]);
                    DelegadaBase.Servicios.registrarProceso(goInfo, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCarga", CultureInfo.CurrentCulture)?.ToString(), "Usuario : " + goInfo.nombre);
                }

                //Se refresca la grilla  
                CargarDatosGrilla();

                if (lsCadenaArchivo.ToString().Contains("Se crearon correctamente"))
                {
                    string[] split = lsCadenaArchivo.ToString().Replace("registros", ";").Split(';');
                    Toastr.Success(this, $"{split[0]} registros");
                    lsCadenaArchivo = lsCadenaArchivo.Remove(0, split[0].Length + 9);
                }

                //Se descarga el log para el usuario
                LogCargaArchivo.DownloadBugsLog(this, lsCadenaArchivo);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        private string[] ValidarArchivo2(string lsRutaArchivo)
        {
            Int32 liNumeroLinea = 0;
            decimal ldValor = 0;
            int liTotalRegistros = 0;
            Int64 liValor = 0;
            string lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            StreamReader lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                if (Destino == "T")
                {
                    /// Recorro el Archivo de Excel para Validarlo
                    lLectorArchivo = File.OpenText(lsRutaArchivo);
                    while (!lLectorArchivo.EndOfStream)
                    {
                        liTotalRegistros = liTotalRegistros + 1;
                        /// Obtiene la fila del Archivo
                        string lsLineaArchivo = lLectorArchivo.ReadLine();

                        liNumeroLinea = liNumeroLinea + 1;
                        /// Pasa la linea sepaada por Comas a un Arreglo
                        Array oArregloLinea = lsLineaArchivo.Split(',');
                        if ((oArregloLinea.Length != 4))
                        {
                            lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 4 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        else
                        {
                            /// Valida trasportador
                            try
                            {
                                ldValor = Convert.ToInt32(oArregloLinea.GetValue(0).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El código del titular {" + oArregloLinea.GetValue(0) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// Valida Punto de entrega
                            try
                            {
                                ldValor = Convert.ToInt32(oArregloLinea.GetValue(1).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El código del punto inicial de entrega{" + oArregloLinea.GetValue(1) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// Valida Punto de entrega fin
                            try
                            {
                                ldValor = Convert.ToInt32(oArregloLinea.GetValue(2).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El código del punto final de entrega{" + oArregloLinea.GetValue(2) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// Valida cantidad
                            try
                            {
                                ldValor = Convert.ToInt32(oArregloLinea.GetValue(3).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La cantidad {" + oArregloLinea.GetValue(3) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }

                        }
                    }
                    lLectorArchivo.Close();
                    lLectorArchivo.Dispose();
                }
                else
                {
                    /// Recorro el Archivo de Excel para Validarlo
                    lLectorArchivo = File.OpenText(lsRutaArchivo);
                    while (!lLectorArchivo.EndOfStream)
                    {
                        liTotalRegistros = liTotalRegistros + 1;
                        /// Obtiene la fila del Archivo
                        string lsLineaArchivo = lLectorArchivo.ReadLine();
                        //if (lsLineaArchivo.Length > 0)
                        //{
                        liNumeroLinea = liNumeroLinea + 1;
                        /// Pasa la linea sepaada por Comas a un Arreglo
                        Array oArregloLinea = lsLineaArchivo.Split(',');
                        if ((oArregloLinea.Length != 3))
                        {
                            lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 3 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        else
                        {
                            /// Valida trasportador
                            try
                            {
                                ldValor = Convert.ToInt32(oArregloLinea.GetValue(0).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El código del titular {" + oArregloLinea.GetValue(0) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// Valida Punto de entrega
                            try
                            {
                                ldValor = Convert.ToInt32(oArregloLinea.GetValue(1).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El código del punto de entrega{" + oArregloLinea.GetValue(1) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// Valida cantidad
                            try
                            {
                                ldValor = Convert.ToInt32(oArregloLinea.GetValue(2).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La cantidad {" + oArregloLinea.GetValue(2) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }

                        }
                    }
                    lLectorArchivo.Close();
                    lLectorArchivo.Dispose();
                }
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseCargarDec_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Declaración de Información Archivo
                Modal.Cerrar(this, mdlDecInfo.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Declaración de Información Archivo

        #region Declaración de Información

        /// <summary>
        /// 
        /// </summary>
        private void OpenIngresoDecInf()
        {
            ddlPuntoFin.Visible = true;
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlTitular, "m_operador", " estado ='A' and codigo_operador !=0 order by codigo_operador ", 0, 4);
            if (Destino == "T")
            {
                lblPunto.Text = "Ruta ";
                LlenarControles1(lConexion.gObjConexion, ddlPunto, "m_pozo poz, m_ruta_snt rut", "distinct poz.codigo_pozo, poz.descripcion", " rut.codigo_trasportador=" + goInfo.cod_comisionista + " and poz.codigo_pozo = rut.codigo_pozo_ini and poz.estado ='A' and rut.estado ='A' order by descripcion ", 0, 1);
            }
            else
            {
                lblPunto.Text = "Punto de Entrega ";
                LlenarControles(lConexion.gObjConexion, ddlPunto, "m_pozo", " estado ='A' order by descripcion ", 0, 1);
                ddlPuntoFin.Visible = false;
            }
            lConexion.Cerrar();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DecIman_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cambia el titulo 
                lblmdlIngresoDecInfoTitle.Text = "Ingreso de Declaración de Información";

                var builder = new StringBuilder();
                builder.Append("Ingrese la declaración del operador con el # de rueda ");
                builder.Append(CodSubasta);
                builder.Append(Destino.Equals("T") ? " y con destino transporte" : " y el destino Suministro de Gas");

                //Se agrega la descripción de la declaración
                lblmdlIngresoDecInfo.Text = builder.ToString();

                //Se limpian los campos del modal
                if (ddlTitular.Items.Count > 0)
                {
                    ddlTitular.SelectedIndex = 0;
                    ddlTitular.Enabled = true;
                }
                if (ddlPunto.Items.Count > 0)
                {
                    ddlPunto.SelectedIndex = 0;
                    ddlPunto.Enabled = true;
                }
                if (ddlPuntoFin.Items.Count > 0)
                {
                    ddlPuntoFin.SelectedIndex = 0;
                    ddlPuntoFin.Enabled = true;
                }

                txtCntDec.Text = string.Empty;

                //Carga los parámetros según la rueda seleccionada
                OpenIngresoDecInf();
                ddlPunto_SelectedIndexChanged2(null, null);

                //Se abre el modal de Ingreso de Declaración de Información
                Modal.Abrir(this, mdlIngresoDecInfo.ID, mdlIngresoDecInfoInside.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void ModActDeclaracionInf_Click(string codigoDec)
        {
            try
            {
                //Se cambia el titulo 
                lblmdlIngresoDecInfoTitle.Text = "Actualizaci&#243;n de Declaraci&#243;n de Informaci&#243;n";

                //Se carga la información
                ActualziacionDeclaracionInfo_Click(codigoDec);

                //Se abre el modal de Ingreso de Declaración de Información
                Modal.Abrir(this, mdlIngresoDecInfo.ID, mdlIngresoDecInfoInside.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void ActualziacionDeclaracionInfo_Click(string codigoDec)
        {
            hndCodDec.Value = codigoDec;
            var builder = new StringBuilder();
            builder.Append("Ingrese la declaración del operador con el # de rueda ");
            builder.Append(CodSubasta);

            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            LlenarControles2(lConexion.gObjConexion, ddlTitular, "m_operador", " estado ='A' order by razon_social ", 0, 4);
            LlenarControles2(lConexion.gObjConexion, ddlPunto, "m_pozo", " estado ='A' order by descripcion", 0, 1);
            LlenarControles2(lConexion.gObjConexion, ddlPuntoFin, "m_pozo", " estado ='A' order by descripcion", 0, 1);
            SqlDataReader lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_declaracion_inf", " codigo_declaracion_inf = " + codigoDec);
            if (lLector.HasRows)
            {
                lLector.Read();
                ddlTitular.SelectedValue = lLector["codigo_trasportador"].ToString();
                ddlPunto.SelectedValue = lLector["codigo_punto_ini"].ToString();
                if (Destino == "T")
                {
                    ddlPuntoFin.SelectedValue = lLector["codigo_punto_fin"].ToString();
                    builder.Append(" y con destino transporte");
                    lblPunto.Text = "Ruta: ";
                }
                else
                {
                    builder.Append(" y el destino Suministro de Gas");
                    lblPunto.Text = "Punto de Entrega: ";
                    ddlPuntoFin.Visible = false;
                }
                txtCntDec.Text = lLector["cantidad"].ToString();
            }
            lLector.Close();
            lConexion.Cerrar();
            ddlTitular.Enabled = false;
            ddlPunto.Enabled = false;
            ddlPuntoFin.Enabled = false;
        }

        /// <summary>
        /// 
        /// </summary>txtCntDec
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnOfertar_Click(object sender, EventArgs e)
        {
            if (lblmdlIngresoDecInfoTitle.Text.Equals("Ingreso de Declaración de Información"))
            {
                var oMensaje = new StringBuilder();
                if (ddlTitular.SelectedValue == "0")
                    oMensaje.Append("Debe seleccionar el titular<br>");
                if (Destino == "T")
                {
                    if (ddlPunto.SelectedValue == "0")
                        oMensaje.Append("Debe seleccionar el punto inicial de la ruta<br>");
                    if (ddlPuntoFin.SelectedValue == "0" || ddlPuntoFin.SelectedValue == "")
                        oMensaje.Append("Debe seleccionar el punto final de la ruta<br>");
                }
                else
                {
                    if (ddlPunto.SelectedValue == "0")
                        oMensaje.Append("Debe seleccionar el punto de entrega<br>");
                }

                if (txtCntDec.Text == "")
                    oMensaje.Append("Debe digitar la cantidad declarada<br>");
                else
                {
                    try
                    {
                        Convert.ToInt32(txtCntDec.Text.Replace(",", ""));
                    }
                    catch (Exception ex)
                    {
                        oMensaje.Append("Cantidad declarada no válida<br>");
                    }
                }

                if (string.IsNullOrEmpty(oMensaje.ToString()))
                {
                    string[] lsNombreParametros =
                    {
                        "@P_numero_rueda", "@P_codigo_declarador", "@P_codigo_titular", "@P_punto", "@P_punto_fin",
                        "@P_cantidad_declarada"
                    };
                    SqlDbType[] lTipoparametros =
                        {SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int};
                    string[] lValorParametros =
                    {
                        CodSubasta, goInfo.cod_comisionista, ddlTitular.SelectedValue, ddlPunto.SelectedValue, "0",
                        txtCntDec.Text.Replace(",", "")
                    };
                    if (Destino == "T")
                        lValorParametros[4] = ddlPuntoFin.SelectedValue;
                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    try
                    {
                        goInfo.mensaje_error = "";
                        var lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion,
                            "pa_SetDeclaracionInf", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                        if (goInfo.mensaje_error != "")
                        {
                            oMensaje.Append("Error al Realizar la declaracion de información. " + goInfo.mensaje_error + "<br>");
                        }
                        else
                        {
                            if (lLector.HasRows)
                            {
                                while (lLector.Read())
                                    Toastr.Error(this, lLector["mensaje"].ToString() + "<br>");
                                lLector.Close();
                                lLector.Dispose();
                                lConexion.Cerrar();
                            }
                            else
                            {
                                lLector.Close();
                                lLector.Dispose();
                                lConexion.Cerrar();
                                Session["mis_ofertas"] = "S";
                                Toastr.Success(this, "Declaración de Información ingresada correctamente.!");

                                //Se cierra el modal 
                                CloseDecIman_Click(null, null);
                                //Se refresca la grilla  
                                CargarDatosGrilla();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        oMensaje.Append("Error al registrar la declaración de información. " + ex.Message + "<br>");
                        lConexion.Cerrar();
                    }
                }
                if (!string.IsNullOrEmpty(oMensaje.ToString()))
                    Toastr.Error(this, oMensaje.ToString());
            }
            else
            {
                if (txtCntDec.Text == "")
                {
                    Toastr.Error(this, "Debe digitar la cantidad declarada");
                    return;
                }
                try
                {
                    Convert.ToInt32(txtCntDec.Text.Replace(",", ""));
                }
                catch (Exception)
                {
                    Toastr.Error(this, "Cantidad declarada no válida");
                    return;
                }

                string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_declaracion_inf", "@P_cantidad_declarada" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { CodSubasta, hndCodDec.Value, txtCntDec.Text.Replace(",", "") };
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();

                try
                {
                    goInfo.mensaje_error = "";
                    SqlDataReader lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_UptDeclaracionInf", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        Toastr.Error(this, "Error al actualizar la declaraci&#243;n de informaci&#243;n. " + goInfo.mensaje_error);
                    }
                    else
                    {
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                                Toastr.Error(this, lLector["mensaje"].ToString());
                            lLector.Close();
                            lLector.Dispose();
                            lConexion.Cerrar();
                        }
                        else
                        {
                            lLector.Close();
                            lLector.Dispose();
                            lConexion.Cerrar();
                            Session["mis_ofertas"] = "S";
                            Toastr.Success(this, "Declaraci&#243;n de Informaci&#243;n actualizada correctamente.!");

                            //Se cierra el modal 
                            CloseDecIman_Click(null, null);
                            //Se refresca la grilla  
                            CargarDatosGrilla();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, "Error al actualizar la declaraci&#243;n de informaci&#243;n. " + ex.Message);
                    lConexion.Cerrar();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseDecIman_Click(Object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Ingreso de Declaración de Información
                Modal.Cerrar(this, mdlIngresoDecInfo.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Declaración de Información

        #region Declaración de Precio

        /// <summary>
        /// 
        /// </summary>
        protected void ModActDeclaracionPre_Click(string codigoDec)
        {
            try
            {
                //Se carga la información
                RealizarDeclaracionPrecio(codigoDec);

                //Se abre el modal de Ingreso de Declaración de Información
                Modal.Abrir(this, mdlDeclaPrecio.ID, mdlDeclaPrecioInside.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="codigoDec"></param>
        private void RealizarDeclaracionPrecio(string codigoDec)
        {
            HiddenField1.Value = codigoDec;

            lblRueda2.Text = "No. Rueda: " + CodSubasta;
            string lsTabla = "";
            string lsCodicion = "";
            if (Destino == "T")
            {
                lsTabla = "t_declaracion_inf dec, m_ruta_snt rut";
                lsCodicion = " dec.codigo_declaracion_inf = " + codigoDec + " and dec.codigo_declarador= rut.codigo_trasportador and dec.codigo_punto_ini = rut.codigo_pozo_ini and dec.codigo_punto_fin = rut.codigo_pozo_fin ";
                lblPunto2.Text = "Ruta: ";
                lblDestino2.Text = "Destino: Transporte";
                trPrecio.Visible = false;
            }
            else
            {
                lsTabla = "t_declaracion_inf dec, m_pozo poz";
                lsCodicion = " dec.codigo_declaracion_inf = " + codigoDec + " and dec.codigo_punto_ini = poz.codigo_pozo";
                lblPunto2.Text = "Punto Entrega: ";
                lblDestino2.Text = "Destino: Suministro de Gas";
            }

            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            SqlDataReader lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", lsTabla, lsCodicion);
            if (lLector.HasRows)
            {
                lLector.Read();
                lblCantidad.Text = lLector["cantidad"].ToString();
                TxtCntNoDisp.Text = lLector["cantidad_no_disponible"].ToString();
                TxtPrecioRes.Text = lLector["precio"].ToString();
                lblPunto2.Text += lLector["descripcion"].ToString();
            }
            lLector.Close();
            lConexion.Cerrar();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnOfertarDeclaracionPrecio_Click(object sender, EventArgs e)
        {
            string oMensaje = "";
            if (TxtCntNoDisp.Text == "")
                oMensaje += "Debe digitar la cantidad declarada\\n";
            else
            {
                try
                {
                    Convert.ToInt32(TxtCntNoDisp.Text.Replace(",", ""));
                }
                catch (Exception)
                {
                    oMensaje += "Cantidad declarada no válida\\n";
                }
            }
            if (Destino == "G")
            {
                if (TxtPrecioRes.Text.Trim().Length > 0)
                {
                    string sOferta = TxtPrecioRes.Text.Replace(",", "");
                    int iPos = sOferta.IndexOf(".");
                    if (iPos > 0)
                        if (sOferta.Length - iPos > 3)
                            oMensaje += "Se permiten máximo 2 decimales en el precio de reserva\\n";
                    try
                    {
                        if (Convert.ToDouble(sOferta) < 0)
                            oMensaje += "El precio de reserva debe ser mayor o igual que cero\\n";
                    }
                    catch (Exception ex)
                    {
                        oMensaje += "El precio de reserva digitado no es válido\\n";
                    }
                }
                else
                    oMensaje += "Debe Ingresar el precio de reserva. \\n";
            }
            if (oMensaje == "")
            {
                string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_declaracion_inf", "@P_codigo_operador", "@P_cantidad_no_disponible", "@P_precio" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal };
                string[] lValorParametros = { CodSubasta, HiddenField1.Value, goInfo.cod_comisionista, TxtCntNoDisp.Text.Replace(",", ""), "0" };
                if (Destino == "G")
                    lValorParametros[4] = TxtPrecioRes.Text.Replace(",", "");
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                try
                {
                    goInfo.mensaje_error = "";
                    var lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_UptDeclaracionPre", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        oMensaje = "Error al actualizar la declaraci&#243;n de precio. " + goInfo.mensaje_error + "\\n";
                    }
                    else
                    {
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                                oMensaje = lLector["mensaje"] + "\\n";
                            lLector.Close();
                            lLector.Dispose();
                            lConexion.Cerrar();
                        }
                        else
                        {
                            lLector.Close();
                            lLector.Dispose();
                            lConexion.Cerrar();
                            Session["mis_ofertas"] = "S";
                            if (Session["TipoOperador"].ToString() == "G") //20201207
                                Toastr.Success(this, "Declaraci&#243;n de capacidades no disponibles actualizada correctamente.!", "Correcto!", 5000);
                            else //20201207
                                Toastr.Success(this, "Declaraci&#243;n de precio actualizada correctamente.!", "Correcto!", 5000); //20201207
                            //Se cierra el modal 
                            CloseDeclaracionPrecio_Click(null, null);
                            //Se refresca la grilla  
                            CargarDatosGrilla();
                        }
                    }
                }
                catch (Exception ex)
                {
                    oMensaje = "Error al actualizar la declaraci&#243;n de precio. " + ex.Message;
                    lConexion.Cerrar();
                }
                if (oMensaje != "")
                {
                    Toastr.Warning(this, oMensaje, "Warning!", 5000);

                }
            }
            else
                Toastr.Warning(this, oMensaje, "Warning!", 5000);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseDeclaracionPrecio_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Declarion de Precio
                Modal.Cerrar(this, mdlDeclaPrecio.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Declaración de Precio

        #region Ingreso Posturas de Compra

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="accion"></param>
        /// <param name="maxPostCompra"></param>
        private void OpenIngresoPosturasCompra(string id, string accion, string maxPostCompra)
        {
            if (string.IsNullOrEmpty(id))
            {
                Toastr.Error(this, "No se enviaron los parametros requeridos.!");
                return;
            }

            hndID.Value = id;
            hndAccion.Value = accion;
            hdfMaxPostCompra.Value = maxPostCompra;

            /// Obtengo los Datos del ID Recibido
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            SqlDataReader lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_id_rueda", " numero_id = " + hndID.Value + " ");
            if (!lLector.HasRows)
            {
                Toastr.Error(this, "El ID enviado NO existe en la base de datos.!");
                return;
            }

            lLector.Read();
            /// Lleno las etiquetas de la pantalla con los datos del Id.
            lblRueda.Text = "No. Rueda: " + lLector["numero_rueda"];
            hdfNoRueda.Value = lLector["numero_rueda"].ToString();
            //lblId.Text = "ID: " + this.Request.QueryString["ID"];
            lblFecha.Text = "Fecha Rueda: " + lLector["fecha_rueda"].ToString().Substring(0, 10);
            lblProducto.Text = "Producto: " + lLector["codigo_producto"] + "-" + lLector["desc_producto"];
            lblUnidadMedida.Text = "Unidad Medida: " + lLector["codigo_unidad_medida"] + "-" + lLector["desc_unidad_medida"];
            if (Destino == "G")
                lblPuntoEntrega.Text = "Punto Entrega: " + lLector["codigo_punto_entrega"] + "-" + lLector["desc_punto_entrega"];
            else
                lblPuntoEntrega.Text = "Ruta: " + lLector["codigo_punto_entrega"] + "-" + lLector["desc_punto_entrega"];
            lblPeridoEnt.Text = "Periodo Entrega: " + lLector["codigo_periodo"] + "-" + lLector["desc_periodo"];
            lblModalidad.Text = "Modalidad Contractual: " + lLector["codigo_modalidad"] + "-" + lLector["desc_modalidad"];
            /// Reviso si se esta creando la oferta o modificando
            if (hndAccion.Value == "M")
                btnOfertar.Text = "Modificar Oferta";
            hndCntTotal.Value = lLector["cantidad_total_venta"].ToString();
            lLector.Close();
            lLector.Dispose();

            /// Llamo el Procedimiento que arma la grilla del ingreso de las ofertas
            string[] lsNombreParametros = { "@P_numero_id", "@P_codigo_operador", "@P_max_post_compra" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { hndID.Value, goInfo.cod_comisionista, hdfMaxPostCompra.Value };

            dtgPosturasC.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetPosturasC", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgPosturasC.DataBind();
            if (dtgPosturasC.Items.Count > 0)
            {
                TextBox oCnt = null;
                TextBox oPre = null;
                int ldCont = 1;
                foreach (DataGridItem Grilla in dtgPosturasC.Items)
                {
                    if (hndAccion.Value == "M")
                    {
                        Grilla.Cells[0].Text = ldCont.ToString();
                        ldCont++;
                    }

                    oCnt = (TextBox)Grilla.Cells[2].Controls[1];
                    oPre = (TextBox)Grilla.Cells[1].Controls[1];
                    //oPre.Focus = true;
                    if (Grilla.Cells[3].Text.Replace("&nbsp;", "") != "")
                        oCnt.Text = Grilla.Cells[3].Text.Replace(",", "");
                    if (Grilla.Cells[4].Text.Replace("&nbsp;", "") != "")
                        oPre.Text = Grilla.Cells[4].Text.Replace(",", "");
                }
            }

            //Se abre el modal de Ingreso de Declaración de Información
            Modal.Abrir(this, mdlIngresoPosturaCompra.ID, mdlIngresoPosturaCompraInside.ID);

            lConexion.Cerrar();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPunto_SelectedIndexChanged2(object sender, EventArgs e)
        {
            if (Destino != "T") return;
            ddlPuntoFin.Items.Clear();
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            LlenarControles1(lConexion.gObjConexion, ddlPuntoFin, "m_pozo poz, m_ruta_snt rut", "distinct poz.codigo_pozo, poz.descripcion", " rut.codigo_trasportador=" + goInfo.cod_comisionista + " and rut.codigo_pozo_ini = " + ddlPunto.SelectedValue + " and poz.codigo_pozo = rut.codigo_pozo_fin and poz.estado ='A' and rut.estado ='A' order by descripcion ", 0, 1);
            lConexion.Cerrar();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnOfertar_Click2(object sender, EventArgs e)
        {

            string sCerrar = "N";
            string oError = "";
            string sProc = "";
            string lsError = "";
            string lsNoPostura = "";

            string[] lsNombreParametros = { "@P_numero_postura", "@P_numero_id", "@P_numero_rueda", "@P_codigo_operador", "@P_precio_postura", "@P_cantidad_postura", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Float, SqlDbType.Int, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "0", "0", "0", "0", "0", "0" }; ///  Accion 0=Creo la Temporal 1=Crear 2=Modificar

            lsError = ValidarEntradas2();
            if (lsError == "")
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlTransaction oTransaccion;
                oTransaccion = lConexion.gObjConexion.BeginTransaction(IsolationLevel.Serializable);
                try
                {
                    TextBox oCnt = null;
                    TextBox oPre = null;
                    foreach (DataGridItem Grilla in dtgPosturasC.Items)
                    {
                        oCnt = (TextBox)Grilla.Cells[2].Controls[1];
                        oPre = (TextBox)Grilla.Cells[1].Controls[1];
                        lValorParametros[1] = hndID.Value;
                        lValorParametros[2] = hdfNoRueda.Value;
                        lValorParametros[3] = goInfo.cod_comisionista;
                        lValorParametros[4] = oPre.Text.Replace(",", "");
                        lValorParametros[5] = oCnt.Text.Replace(",", "");
                        if (hndAccion.Value == "C")
                            lsNoPostura = Grilla.Cells[0].Text.Replace("&nbsp;", "");
                        else
                            lsNoPostura = Grilla.Cells[5].Text.Replace("&nbsp;", "");
                        lValorParametros[6] = "0";
                        lValorParametros[0] = lsNoPostura;

                        goInfo.mensaje_error = "";
                        if (!DelegadaBase.Servicios.EjecutarProcedimientoConTransaccion(lConexion.gObjConexion, "pa_SetPosturaCompraId", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo))
                        {
                            if (goInfo.mensaje_error != "")
                            {
                                oError = "Error al Actualizar la Postura. " + goInfo.mensaje_error + "\\n";
                                oTransaccion.Rollback();
                                break;
                            }
                        }
                    }
                    if (oError == "")
                    {
                        if (hndAccion.Value == "C")
                        {
                            lValorParametros[2] = hdfNoRueda.Value;
                            lValorParametros[6] = "1";
                            lValorParametros[0] = "0";
                        }
                        else
                        {
                            lValorParametros[2] = hdfNoRueda.Value;
                            lValorParametros[6] = "2";
                            lValorParametros[0] = lsNoPostura;
                        }
                        var lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetPosturaCompraId", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                        if (goInfo.mensaje_error != "")
                        {
                            oError = "Error al Actualizar la Postura. " + goInfo.mensaje_error + "\\n";
                            oTransaccion.Rollback();
                        }
                        else
                        {
                            if (lLector.HasRows)
                            {
                                lLector.Read();
                                if (lLector["mensaje"].ToString() != "")
                                {
                                    oError = lLector["mensaje"].ToString();
                                    lLector.Close();
                                    lLector.Dispose();
                                    oTransaccion.Rollback();
                                    lConexion.Cerrar();
                                }
                                else
                                {
                                    lLector.Close();
                                    lLector.Dispose();
                                    oTransaccion.Commit();
                                    lConexion.Cerrar();
                                    Session["mis_ofertas"] = "S";
                                    Toastr.Success(this, hndAccion.Value == "C"
                                        ? "Posturas Ingresadas Correctamente.!"
                                        : "Posturas Actualizadas Correctamente.!");
                                    //Se cierra el modal 
                                    CloseIngresoPosturasCompra_Click(null, null);
                                    //Se refresca la grilla  
                                    CargarDatosGrilla();
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    oTransaccion.Rollback();
                    oError = "Error al realizar la oferta. " + ex.Message;
                    lConexion.Cerrar();
                }
                lConexion.Cerrar();
                if (oError != "")
                {
                    Toastr.Warning(this, oError, "Warning!", 5000);

                }
            }
            else
                Toastr.Warning(this, lsError, "Warning!", 5000);


        }

        /// <summary>
        /// Relaiza la validacion del Ingreso de las Posturas
        /// </summary>
        /// <returns></returns>
        private string ValidarEntradas2()
        {
            string lsError = "";
            int oValor = 0;
            double oValor1 = 0;
            int ldValorAnt = 0;
            int liCntVenta = Convert.ToInt32(hndCntTotal.Value);
            double ldPrecioAnt = 0;
            int ldConta = 0;

            TextBox oCnt = null;
            TextBox oPre = null;
            foreach (DataGridItem Grilla in dtgPosturasC.Items)
            {
                oCnt = (TextBox)Grilla.Cells[2].Controls[1];
                oPre = (TextBox)Grilla.Cells[1].Controls[1];
                oCnt.Text = oCnt.Text.Replace(",", "");
                oPre.Text = oPre.Text.Replace(",", "");

                if (oCnt.Text.Trim().Length > 0)
                {
                    try
                    {
                        // Validacion para que no permita cantidad  menor o Igual a 0 - 20150709
                        if (Convert.ToInt32(oCnt.Text.Replace(",", "")) <= 0)
                            lsError += "La Cantidad de la Postura " + Grilla.Cells[0].Text + " no puede ser menor o Igual a 0\\n";
                        else
                        {
                            if (Convert.ToInt32(oCnt.Text.Replace(",", "")) > liCntVenta)
                                lsError += "La Cantidad de la Postura " + Grilla.Cells[0].Text + " no puede ser mayor que la cantidad ofrecida\\n";
                        }
                    }
                    catch (Exception ex)
                    {
                        lsError += "Valor Invalido en la Cantidad de la Postura " + Grilla.Cells[0].Text + "\\n";
                    }
                }
                else
                    lsError += "Debe Ingresar la Cantidad de la Postura " + Grilla.Cells[0].Text + "\\n";
                if (oPre.Text.Trim().Length > 0)
                {
                    try
                    {
                        if (Convert.ToDouble(oPre.Text.Replace(",", "")) < 0)
                            lsError += "El Precio debe ser mayor o igual que cero para la postura " + Grilla.Cells[0].Text + "\\n";
                    }
                    catch (Exception ex)
                    {
                        lsError += "Valor Invalido en el Precio de la Postura " + Grilla.Cells[0].Text + "\\n";
                    }
                }
                else
                    lsError += "Debe Ingresar el Precio de la Postura " + Grilla.Cells[0].Text + "\\n";
                if (lsError != "")
                    break;
                else
                {
                    if (ldConta > 0)
                    {
                        if (Convert.ToInt32(oCnt.Text.Replace(",", "")) < ldValorAnt)
                            lsError += "La Cantidad de la Postura " + Grilla.Cells[0].Text + " debe ser mayor a la postura anterior. \\n";
                        if (Convert.ToDouble(oPre.Text.Replace(",", "")) >= ldPrecioAnt)
                            lsError += "El  Precio de la Postura " + Grilla.Cells[0].Text + " debe ser menor al de la postura anterior. \\n";

                        ldValorAnt = Convert.ToInt32(oCnt.Text.Replace(",", ""));
                        ldPrecioAnt = Convert.ToDouble(oPre.Text.Replace(",", ""));
                    }
                    else
                    {
                        ldValorAnt = Convert.ToInt32(oCnt.Text.Replace(",", ""));
                        ldPrecioAnt = Convert.ToDouble(oPre.Text.Replace(",", ""));
                    }
                }
                ldConta++;
            }
            return lsError;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseIngresoPosturasCompra_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Ingreso Posturas de Compra
                Modal.Cerrar(this, mdlIngresoPosturaCompra.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Ingreso Posturas de Compra

        #region Curva de Oferta y Demanda Agregada

        /// <summary>
        /// Abre el modal de la Curva de Oferta y Demanda Agregada
        /// </summary>
        private void OpenCurvaOferDemAgre(string lblNoId)
        {
            var transporte = true;
            var lds = new DataSet();
            goInfo.Programa = "Curva de oferta y demanda agregada";
            lblMensaje.Text = "";

            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            var lComando1 = new SqlCommand();
            var lsqldata1 = new SqlDataAdapter();
            lComando1.Connection = lConexion.gObjConexion;
            lComando1.CommandTimeout = 3600;
            lComando1.CommandType = CommandType.StoredProcedure;

            //cambios grafica 20160202
            lComando1.CommandText = "pa_ValidarExistencia";
            lComando1.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = "t_id_rueda";
            lComando1.Parameters.Add("@P_Filtro", SqlDbType.VarChar).Value = "numero_id = " + lblNoId;
            var lLector = lComando1.ExecuteReader();
            if (lLector.HasRows)
            {
                lLector.Read();
                if (lLector["codigo_producto"].ToString() == "1")
                {
                    lblPuntoCurva.Text = "Punto de Entrega " + lLector["desc_punto_entrega"];
                    transporte = false;
                }
                else
                {
                    lblPuntoCurva.Text = "Ruta " + lLector["desc_punto_entrega"] + "-" + lLector["desc_punto_fin"];
                }
            }
            else
                lblPuntoCurva.Text = "";
            lComando1.Parameters.Clear();
            lLector.Close();
            lLector.Dispose();

            lComando1.CommandText = "pa_GetPreciosCurva1";
            lComando1.Parameters.Add("@P_numero_id", SqlDbType.Int).Value = lblNoId;
            lComando1.ExecuteNonQuery();
            lsqldata1.SelectCommand = lComando1;
            lsqldata1.Fill(lds);
            dtgInformacion.DataSource = lds;
            dtgInformacion.DataBind();
            lConexion.Cerrar();

            //Carga la grafica
            AuctionGraphics.GenerateGraph(this, dtgInformacion, lblNoId, transporte);
            //Se abre el modal de Ingreso de Declaración de Información
            Modal.Abrir(this, mdlCurvaOfertaDemAgre.ID, mdlCurvaOfertaDemAgreInside.ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseCurvaOferDemAgre_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Ingreso de Declaración de Información
                Modal.Cerrar(this, mdlCurvaOfertaDemAgre.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Curva de Oferta y Demanda Agregada

        #region Oferta

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AbrirOferta(object sender, EventArgs e)
        {
            try
            {
                string[] lsNombreParametros = { "@P_numero_rueda" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int };
                string[] lValorParametros = { CodSubasta };
                SqlDataReader lLector;
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetMisOFertas", lsNombreParametros, lTipoparametros, lValorParametros);

                StringBuilder builder = new StringBuilder();

                builder.Append("<table  class=\"table table-bordered table-fixed\">");
                builder.Append("<thead class=\"thead-light\">");
                builder.Append("<tr>");
                builder.Append("<th width='10%'>IDG</font></th>");
                builder.Append("<th width='13%'>CANTIDAD</font></th>");
                builder.Append("<th width='24%'>PRECIO</font></th>");
                builder.Append("<th width='20%'>CANTIDAD NN</font></th>");
                builder.Append("<th width='17%'>CANTIDAD ND</font></th>");
                builder.Append("<th width='16%'>TIPO OFERTA</font></th>");
                builder.Append("</tr>");
                builder.Append("</thead>");

                if (lLector.HasRows)
                {
                    builder.Append("<tbody>");
                    while (lLector.Read())
                    {
                        builder.Append("<tr>");
                        builder.Append("<td width='10%'>" + lLector["numero_id"] + "</font></td>");
                        builder.Append("<td width='13%'>" + lLector["cantidad_postura"] + "</font></td>");
                        builder.Append("<td width='24%'>" + lLector["precio"] + "</font></td>");
                        builder.Append("<td width='20%'>" + lLector["cantidad_no_nominada"] + "</font></td>");
                        builder.Append("<td width='17%'>" + lLector["cantidad_no_disponible"] + "</font></td>");
                        builder.Append("<td width='16%'>" + lLector["tipo_oferta"] + "</font></td>");
                        builder.Append("</tr>");
                    }
                    builder.Append("</tbody>");
                }

                builder.Append("</table>");

                ltTableroRF.Text = builder.ToString();

                if (Session["refrescar_cont"].ToString() == "S")
                {
                    Session["refrescar_cont"] = "N";
                }

                //Se abre el modal de oferta
                Modal.Abrir(this, mdlOferta.ID, mdlOfertaInside.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseOferta_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Oferta
                Modal.Cerrar(this, mdlOferta.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Oferta

        #region Contratos


        /// <summary>
        /// 
        /// </summary>
        protected void AbrirContratos(object sender, EventArgs e)
        {
            try
            {
                string[] lsNombreParametros = { "@P_numero_rueda" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int };
                string[] lValorParametros = { CodSubasta };
                SqlDataReader lLector;
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetMisContratos", lsNombreParametros, lTipoparametros, lValorParametros);

                var builder = new StringBuilder();

                builder.Append("<table  class=\"table table-bordered\">");
                builder.Append("<thead class=\"thead-light\">");
                builder.Append("<tr>");
                builder.Append("<th width='10%'>IDG</font></th>");
                builder.Append("<th width='10%'>CANTIDAD</font></th>");
                builder.Append("<th width='27%'>PRECIO</font></th>");
                builder.Append("<th width='20%'>COMPRADOR</font></th>");
                builder.Append("<th width='20%'>VENDEDOR</font></th>");
                builder.Append("</tr>");
                builder.Append("</thead>");

                if (lLector.HasRows)
                {
                    builder.Append("<tbody>");
                    while (lLector.Read())
                    {
                        builder.Append("<tr>");
                        builder.Append("<td width='10%'>" + lLector["numero_id"] + "</font></td>");
                        builder.Append("<td width='10%'>" + lLector["cantidad"] + "</font></td>");
                        builder.Append("<td width='27%'>" + lLector["precio"] + "</font></td>");
                        builder.Append("<td width='20%'>" + lLector["operador_compra"] + "</font></td>");
                        builder.Append("<td width='20%'>" + lLector["operador_venta"] + "</font></td>");
                        builder.Append("</tr>");
                    }
                    builder.Append("</tbody>");
                }
                builder.Append("</table>");
                ltTablero.Text = builder.ToString();

                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();

                //Se abre el modal de contratos
                Modal.Abrir(this, mdlContratos.ID, mdlContratosInside.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CloseContratos_Click(object sender, EventArgs e)
        {
            try
            {
                //Se cierra el modal de Oferta
                Modal.Cerrar(this, mdlContratos.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        #endregion Contratos

        #endregion Modals

        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void dtgContratos_OnItemCommandClick(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                switch (((ImageButton)e.CommandSource).ID)
                {
                    case "imbGrafica":
                        {
                            OpenCurvaOferDemAgre(e.Item.Cells[0].Text);
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "Error al eliminar la oferta. " + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void dtgRechazo_OnItemCommandClick(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                switch (((ImageButton)e.CommandSource).ID)
                {
                    case "imbRechazar":
                        {
                            string[] lsNombreParametros = { "@P_numero_contrato", "@P_numero_id", "@P_codigo_operador" };
                            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                            string[] lValorParametros = { e.Item.Cells[1].Text, e.Item.Cells[0].Text, goInfo.cod_comisionista };
                            SqlDataReader lLector;
                            lConexion = new clConexion(goInfo);
                            lConexion.Abrir();
                            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_DelContrato11", lsNombreParametros, lTipoparametros, lValorParametros);
                            if (lLector.HasRows)
                            {
                                while (lLector.Read())
                                {
                                    Toastr.Error(this, $"{lLector["error"]}");
                                }
                                lLector.Close();
                                lLector.Dispose();
                                lConexion.Cerrar();
                            }
                            else
                            {
                                Toastr.Success(this, "Oferta eliminada correctamente");
                                lLector.Close();
                                lLector.Dispose();
                                lConexion.Cerrar();
                                CargarDatosGrilla();
                            }

                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "Error al eliminar la oferta. " + ex.Message);
            }
        }

    }
}
