﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ContratosC1C2Bim.aspx.cs"
    Inherits="Procesos_frm_ContratosC1C2Bim" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="Label1" runat="server"></asp:Label>
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body" runat="server">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Modalidad de Contrato" AssociatedControlID="ddlModalidad" runat="server" />
                            <asp:DropDownList ID="ddlModalidad" runat="server" CssClass="form-control selectpicker" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Mes Subasta" AssociatedControlID="ddlMes" runat="server" />
                            <asp:DropDownList ID="ddlMes" runat="server" CssClass="form-control selectpicker" />
                        </div>
                    </div>
                </div>
                <%--Grilla--%>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" AllowPaging="True" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" PageSize="10"
                                OnPageIndexChanged="dtgConsulta_PageIndexChanged">
                                <Columns>
                                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Id Externo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numero_contrato_sist" HeaderText="Número de operación"
                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Vendedor" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Comprador" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_fuente" HeaderText="Nombre Fuente" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_tipo_contrato" HeaderText="Código Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                                </Columns>
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
