<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_frame_subasta_6.aspx.cs" Inherits="Procesos.frm_frame_subasta_6" MasterPageFile="~/PlantillaPrincipal.master" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/dev/auctionChart.js")%>"></script>
    <script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/dev/clock.js")%>"></script>

    <%--Ingreso de Declaración de Información--%>
    <script language="javascript">
        //        document.location = no;
        function confirmarDemanda() {
            return confirm("Esta seguro que desea demandar!");
        }

        function confirmarOferta() {
            return confirm("Esta seguro que desea ofertar!");
        }

        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }

        function formatoMiles(obj, evt) {
            var T;
            var strValue = removeCommas(obj.value);
            //if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
            if ((T = /([0-9]*)$/i.exec(strValue)) != null) {
                //                T[4] = T[4].substr(0, decimales);
                //                if (T[2] == '' && T[3] == '.') T[2] = 0;
                obj.value = addCommas(T[1]);
            } else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }
    </script>

    <%--Ingreso Posturas de Compra--%>
    <script type="text/javascript" language="javascript">

        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }

        function formatoMilesPre(obj, evt, decimales) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
                T[4] = T[4].substr(0, decimales);
                if (T[2] == '' && T[3] == '.') {
                    T[2] = 0;
                }
                obj.value = T[1] + addCommas(T[2]) + T[3] + T[4];
            }
            else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }

    </script>
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="kt-portlet">
                    <%--Head--%>
                    <div class="kt-portlet__head">
                        <%--Titulo--%>
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <asp:Label ID="lblTitle" Text="Subasta Bimestral" runat="server" />
                            </h3>
                        </div>

                        <%--Botones--%>
                        <asp:UpdatePanel style="margin-top: 1%" runat="server">
                            <ContentTemplate>
                                <segas:CrudButton ID="buttons" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                    <%--Contenido--%>
                    <div class="kt-portlet__body">
                        <strong>
                            <h5>
                                <input id="lblRelog" style="width: 100px; color: green; border: none;" type="text" readonly name="reloj" size="10">
                            </h5>
                        </strong>

                        <%--Estados de la subasta--%>
                        <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                            <ContentTemplate>
                                <div id="divTiempoEspera" class="row" visible="False" runat="server">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <br />
                                        <div class="form-group">
                                            <div class="alert alert-info" runat="server">
                                                <strong>&#161;Atenci&#243;n&#33;</strong>
                                                <p>&nbsp; Se encuentra en un tiempo de espera</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:Button ID="btnClickEmulate" OnClick="Timer_Tick" Style="display: none;" ClientIDMode="Static" runat="server" />
                                <segas:Estado ID="estados" Visible="false" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <%--Separador--%>
                        <div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>

                        <%--Botones--%>
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label"></div>
                            <div class="kt-portlet__head-toolbar">
                                <segas:Subasta ID="buttonsSubasta" runat="server" />
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <segas:SubastaExtender ID="buttonsSubastaExtender" runat="server" />
                                        <asp:Button ID="btnSuspender" Text="Suspender" runat="server" Visible="false" OnClientClick="window.open('frm_Suspender_2.aspx','','width=450,height=250,left=400,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no,scrollbars=1');" />
                                        <asp:Button ID="btnReactivar" Text="Reactivar" runat="server" Visible="false" OnClientClick="window.open('frm_reactivar_4.aspx','','width=600,height=400,left=300,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no,scrollbars=1');" />
                                        <asp:Button ID="btnCarga" Text="Carga Venta" runat="server" Visible="false" OnClientClick="window.open('frm_cargaVenta_4.aspx','','width=450,height=250,left=400,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no,scrollbars=1');" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>

                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <%--Tablas Subasta--%>
                            <div class="kt-portlet__body">
                                <div class="tab-content">
                                    <div class="table table-responsive">
                                        <asp:DataGrid ID="dtgSubasta1" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                            ViewStateMode="Enabled" OnItemCommand="OnItemCommand_Click">
                                            <Columns>
                                                <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                <%--1--%><asp:BoundColumn DataField="desc_modalidad" HeaderText="Tipo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                <%--2--%><asp:BoundColumn DataField="desc_periodo" HeaderText="Duraci&#243;n" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                                <%--3--%><asp:BoundColumn DataField="desc_punto_entrega" HeaderText="Lugar" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                <%--4--%><asp:BoundColumn DataField="cantidad_venta" HeaderText="Cantidad Venta (MBTUD)" DataFormatString="{0:###,###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn> <%--20201020--%>
                                                <%--5--%><asp:BoundColumn DataField="precio_venta" HeaderText="Precio Venta (USD/MBTU)" DataFormatString="{0:###,###,###,###,###,###,##0.00}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn> <%--20201020--%>
                                                <%--6--%><asp:BoundColumn DataField="precio_delta" HeaderText="Precio Delta (USD/MBTU)" DataFormatString="{0:###,###,###,###,###,###,##0.00}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn><%--20201020--%>
                                                <%--7--%><asp:BoundColumn DataField="cantidad_adjudicada" HeaderText="Cantidad Adjudicada (MBTUD)" DataFormatString="{0:###,###,###,###,###,###,##0.00}" ItemStyle-HorizontalAlign="Right" Visible="false"></asp:BoundColumn> <%--20201020--%>
                                                <%--8--%><asp:BoundColumn DataField="precio_adjudicado" HeaderText="Precio Adjudicado (USD/MBTU)" DataFormatString="{0:###,###,###,###,###,###,##0.00}" ItemStyle-HorizontalAlign="Right" Visible="false"></asp:BoundColumn> <%--20201020--%>
                                                <%--9--%><asp:BoundColumn DataField="mejor_postura" Visible="false" ItemStyle-Width="60px"></asp:BoundColumn>
                                                <%--10--%><asp:BoundColumn DataField="hora_prox_fase" HeaderText="Hora Porx Fase"
                                                    Visible="false" HeaderStyle-Width="60px"></asp:BoundColumn>
                                                <%--11--%><asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-Width="100px"></asp:BoundColumn>
                                                <%--12--%><asp:TemplateColumn HeaderText="Oferta">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imbOfertar" runat="server" ToolTip="Ofertar" ImageUrl="~/Images/nuevo.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--13--%><asp:TemplateColumn HeaderText="Modificar">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imbModificar" runat="server" ToolTip="Modificar Oferta" ImageUrl="~/Images/modificar.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--14--%><asp:TemplateColumn HeaderText="Eliminar">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imbEliminar" runat="server" ToolTip="Eliminar Oferta" ImageUrl="~/Images/eliminar.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--15--%><asp:TemplateColumn HeaderText="Grafica">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imbGrafica" runat="server" ToolTip="Curva Agregada" ImageUrl="~/Images/grafica.jpg" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--16--%><asp:BoundColumn DataField="ind_vendedor" Visible="false"></asp:BoundColumn>
                                                <%--17--%><asp:BoundColumn DataField="ind_postura" Visible="false"></asp:BoundColumn>
                                                <%--18--%><asp:BoundColumn DataField="no_posturas_compra" Visible="false"></asp:BoundColumn>
                                                <%--19--%><asp:BoundColumn DataField="habilita_c" Visible="false"></asp:BoundColumn>
                                                <%--20--%><asp:BoundColumn DataField="habilita_v" Visible="false"></asp:BoundColumn>
                                            </Columns>
                                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                                        </asp:DataGrid>
                                    </div>
                                    <%--Tabla que se exporta a Excel--%>
                                    <asp:DataGrid ID="dtgSubasta" runat="server" AutoGenerateColumns="False" runat="server" Visible="false">
                                        <ItemStyle CssClass="td2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="15px"></ItemStyle>
                                        <Columns>
                                            <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                            <%--1--%><asp:BoundColumn DataField="desc_modalidad" HeaderText="Tipo Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                            <%--2--%><asp:BoundColumn DataField="desc_periodo" HeaderText="Periodo de Entrega"
                                                ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                            <%--3--%><asp:BoundColumn DataField="desc_punto_entrega" HeaderText="Punto de Entrega"
                                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                            <%--4--%><asp:BoundColumn DataField="cantidad_venta" HeaderText="Cantidad (MBTUD)" DataFormatString="{0:###,###,###,###,###,###,##0 }" 
                                                ItemStyle-HorizontalAlign="Right"></asp:BoundColumn> <%--20201020--%>
                                            <%--3--%><asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <%--Widgets--%>
        <%--    <div class="col-md-3 col-xl-3">
                <div class="kt-portlet kt-portlet--height-fluid-half">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title nb">widget</h3>
                        </div>




                    </div>
                    <div class="kt-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_widget11_tab1_content">
                                <!--begin::Widget 11-->
                                <div class="kt-widget11">
                                    <div class="table-responsive">

                                        <table class="table">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>#</th>
                                                    <th>column</th>
                                                    <th>column</th>
                                                    <th>column</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>data</td>
                                                    <td>data</td>
                                                    <td>data</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>data</td>
                                                    <td>data</td>
                                                    <td>data</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td>data</td>
                                                    <td>data</td>
                                                    <td>data</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                                <!--end::Widget 11-->
                            </div>

                        </div>
                    </div>
                </div>
                <div class="kt-portlet kt-portlet--height-fluid-half">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title nb">Widget</h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_widget11_tab1_content">
                                <!--begin::Widget 11-->
                                <div class="kt-widget11">
                                    <div class="table-responsive">

                                        <table class="table">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>#</th>
                                                    <th>column</th>
                                                    <th>column</th>
                                                    <th>column</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>data</td>
                                                    <td>data</td>
                                                    <td>data</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>data</td>
                                                    <td>data</td>
                                                    <td>data</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td>data</td>
                                                    <td>data</td>
                                                    <td>data</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                                <!--end::Widget 11-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>--%>
        <%--Fin Widgets--%>
    </div>

    <%--Modales--%>

    <%--Modal Ofertas--%>
    <div class="modal fade" id="mdlOfertas" tabindex="-1" role="dialog" aria-labelledby="mdlOfertaLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlOfertasInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlOfertasLabel" runat="server">Ofertas</h5>
                        </div>
                        <div class="modal-body">
                            <h5>
                                <asp:Label ID="Label13" runat="server"></asp:Label>
                            </h5>

                            <hr>
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <div class="table table-responsive">
                                            <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                                <ContentTemplate>
                                                    <asp:Literal ID="ltTableroRF" Mode="Transform" runat="server"></asp:Literal>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseOfertas_Click" runat="server" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <%--Modal Contratos--%>
    <div class="modal fade" id="mdlContratos" tabindex="-1" role="dialog" aria-labelledby="mdlContratosLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlContratosInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="lblmdlContratos">Mis Contratos</h5>
                        </div>
                        <div class="modal-body">
                            <h5>
                                <asp:Label ID="Label14" runat="server"></asp:Label>
                            </h5>

                            <hr>

                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <div class="table table-responsive">
                                            <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                                <ContentTemplate>
                                                    <asp:Literal ID="ltTablero" runat="server"></asp:Literal>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseContratos_Click" runat="server" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <%--Modal Curva de Oferta y Demanda Agregada--%>
    <div class="modal fade" id="mdlCurvaOfertaDemAgre" tabindex="-1" role="dialog" aria-labelledby="mdlCurvaOfertaDemAgreLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlCurvaOfertaDemAgreInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlCurvaOfertaDemAgreLabel">Curva de Oferta y Demanda Agregada</h5>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <h5>
                                <asp:Label ID="lblPuntoCurva" runat="server" Font-Bold="true"></asp:Label>
                            </h5>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <hr>

                    <div class="row">

                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <div id="chart_div" style="width: 700px; height: 500px; display: block; margin: 0 auto !important;"></div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="table table-responsive">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <asp:DataGrid ID="dtgInformacion" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                            ViewStateMode="Enabled" OnItemCommand="OnItemCommand_Click">
                                            <Columns>
                                                <asp:BoundColumn DataField="precio" HeaderText="Precio (USD/MBTU)" DataFormatString="{0: ###,###,##0.00}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn> <%--20201020--%>
                                                <asp:BoundColumn DataField="cantidad_venta" HeaderText="Cantidad Venta (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,##0}"></asp:BoundColumn> <%--20201020--%>
                                                <asp:BoundColumn DataField="cantidad_compra" HeaderText="Cantidad Compra (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,##0}"></asp:BoundColumn> <%--20201020--%>
                                                <asp:BoundColumn DataField="cantidad_min" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="cantidad_max" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="precio_min" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="precio_max" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="precio_venta" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="precio_compra" Visible="false"></asp:BoundColumn>
                                                <%--20210224--%>
                                                <asp:BoundColumn DataField="grafica_compra" Visible="false"></asp:BoundColumn>
                                            </Columns>
                                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                                        </asp:DataGrid>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseCurvaOferDemAgre_Click" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%--Oferta y Demanda Agregada--%>
    <div class="modal fade" id="mdlOferta" tabindex="-1" role="dialog" aria-labelledby="mdlOfertaLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlOfertaInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlOfertaLabel">Posturas de Venta</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label ID="lblRueda" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label ID="lblId" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label ID="lblFecha" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label ID="lblProducto" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label ID="lblUnidadMedida" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label ID="lblPuntoEntrega" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label ID="lblPeridoEnt" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label ID="lblModalidad" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label ID="lblFechaIni" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label ID="lblFechaFin" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div id="trPrecioVar" runat="server" class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label ID="Label1" runat="server">Cantidad Disponible</asp:Label>
                                        <asp:TextBox ID="TxtCntDisp" runat="server" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTBETxtCntDisp" runat="server" TargetControlID="TxtCntDisp" FilterType="Custom, Numbers" ValidChars=","></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label ID="Label3" runat="server">Cantidad Disponible</asp:Label>
                                        <asp:TextBox ID="TextBox1" runat="server" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TxtCntDisp" FilterType="Custom, Numbers" ValidChars=","></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label ID="Label4" runat="server"> Precio Reserva</asp:Label>
                                        <asp:TextBox ID="TxtPrecioRes" runat="server" onkeyup="return formatoMilesPre(this,event);" Width="100%" CssClass="form-control"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTEBTxtPrecioRes" runat="server" TargetControlID="TxtPrecioRes" FilterType="Custom, Numbers" ValidChars=",."></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label ID="Label5" runat="server">Variación Precio</asp:Label>
                                        <asp:TextBox ID="TxtPrecioVar" runat="server" onkeyup="return formatoMilesPre(this,event,2);" Width="100%" CssClass="form-control"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTEBTxtPrecioVar" runat="server" TargetControlID="TxtPrecioVar" FilterType="Custom, Numbers" ValidChars=",."></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label ID="Label6" runat="server">Variación Precio</asp:Label>
                                        <asp:TextBox ID="TextBox2" runat="server" onkeyup="return formatoMilesPre(this,event,2);" Width="100%" CssClass="form-control"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="TxtPrecioVar" FilterType="Custom, Numbers" ValidChars=",."></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>

                                <asp:HiddenField ID="hndID" runat="server" />
                                <asp:HiddenField ID="hdfNoRueda" runat="server" />
                                <asp:HiddenField ID="hndFechaRueda" runat="server" />
                                <asp:HiddenField ID="hndAccion" runat="server" />
                                <asp:HiddenField ID="hdfNoPostura" runat="server" />
                                <asp:HiddenField ID="hdfTpoRueda" runat="server" />
                                <asp:HiddenField ID="hdfIngDelta" runat="server" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <asp:Button ID="btnOfertar" Text="Crear" runat="server" OnClientClick="return confirmarOferta();" OnClick="btnOfertar_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <%--Modal Ingreso Posturas de Compra--%>
    <div class="modal fade" id="mdlIngresoPosturaCompra" tabindex="-1" role="dialog" aria-labelledby="mdlIngresoPosturaCompraLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlIngresoPosturaCompraInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlIngresoPosturaCompraLabel">Ingreso Posturas de Compra</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblRuedaCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblIdCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblFechaCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblProductoCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblUnidadMedidaCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblPuntoEntregaCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblPeridoEntCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblModalidadCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblFechaIniCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblFechaFinCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="alert alert-info">
                                        <strong>&#33;Atenci&#243;n&#161;</strong>
                                        <p>&nbsp; Los Precios de Compra deben ser Ingresados de mayor a menor.</p>

                                        <strong>&#33;Atenci&#243;n&#161;</strong>
                                        <p>&nbsp; Las cantidades a Comprar deben ser Ingresados de menor a mayor.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="table table-responsive">
                                <asp:DataGrid ID="dtgPosturasC" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                    ViewStateMode="Enabled">
                                    <Columns>
                                        <asp:BoundColumn DataField="codigo_postura" HeaderText="No. Postura" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Precio">
                                            <ItemTemplate>
                                                <asp:TextBox ID="TxtPrecio" CssClass="form-control" runat="server" onkeyup="return formatoMilesPre(this,event,2);"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Cantidad">
                                            <ItemTemplate>
                                                <asp:TextBox ID="TxtCantidad" CssClass="form-control" runat="server" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="cantidad_postura" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="precio_postura" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="numero_postura" Visible="false"></asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                    <PagerStyle HorizontalAlign="Center"></PagerStyle>
                                </asp:DataGrid>
                            </div>

                            <asp:HiddenField ID="hndIDCompra" runat="server" />
                            <asp:HiddenField ID="hdfNoRuedaCompra" runat="server" />
                            <asp:HiddenField ID="hdfMaxPostCompra" runat="server" />
                            <asp:HiddenField ID="hndAccionCompra" runat="server" />
                            <asp:HiddenField ID="hndCntCompra" runat="server" />
                        </div>
                        <div class="modal-footer">
                            <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseIngresoPosturasCompra_Click" runat="server" />
                            <asp:Button ID="btnCompra" Text="Crear" runat="server" OnClientClick="return confirmarDemanda();" CssClass="btn btn-primary" OnClick="btnComprar_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

</asp:Content>
