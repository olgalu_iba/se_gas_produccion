﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_tablero_neg_posturas.aspx.cs"
    Inherits="Procesos_frm_tablero_neg_posturas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link href="../css/estiloTab.css" rel="stylesheet" type="text/css" />
</head>
<body style="background-color: #000000">
    
    <form id="frmListado" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        
    <table cellpadding="0" cellspacing="0" bgcolor="#000000" width="100%" border="1"
        style="border-bottom: #FFFFFF;">
        <%--<tr>
            <td align="center" width="50%">
                <asp:Label ID="lblCompra" runat="server" Text="COMPRA" Font-Bold="true" Font-Size="14px"
                    ForeColor="White"></asp:Label>
            </td>
            <td align="center" width="50%">
                <asp:Label ID="lblVenta" runat="server" Text="VENTA" Font-Bold="true" Font-Size="14px"
                    ForeColor="White"></asp:Label>
            </td>
        </tr>--%>
        <tr>
            <td align="left">
                <table cellpadding="0" cellspacing="0" bgcolor="#000000">
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblTitulo" runat="server" Font-Names="Arial, Helvetica, sans-serif"
                                BackColor="Black" ForeColor="White" Font-Size="14px" Font-Bold="true"></asp:Label>
                            <br />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:DataGrid ID="dtgPosturas" runat="server" AutoGenerateColumns="False" OnItemCommand="OnItemCommandC_Click"
                                PagerStyle-HorizontalAlign="Center">
                                <HeaderStyle CssClass="th2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="13px">
                                </HeaderStyle>
                                <ItemStyle CssClass="td2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="14px">
                                </ItemStyle>
                                <Columns>
                                    <%--0--%><asp:BoundColumn DataField="desc_punto_entrega" HeaderText="Punto / Ruta"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="150px"></asp:BoundColumn>
                                    <%--1--%><asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-Width="180px"></asp:BoundColumn>
                                    <%--2--%><asp:BoundColumn DataField="cantidad" HeaderText="Cnt" DataFormatString="{0:###,###,###,###,###,###,##0}"
                                        HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--3--%><asp:BoundColumn DataField="precio" HeaderText="Pre" DataFormatString="{0:###,###,###,###,###,###,##0.00}"
                                        HeaderStyle-Width="80px" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--4--%><asp:BoundColumn DataField="fecha_maxima_interes" HeaderText="Fecha Max."
                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--5--%><asp:BoundColumn DataField="hora_maxima_interes" HeaderText="Hora Max."
                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="70px"></asp:BoundColumn>
                                    <%--6--%><asp:BoundColumn DataField="desc_periodo" HeaderText="Periodo" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-Width="120px"></asp:BoundColumn>
                                    <%--7--%><asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Ini" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-Width="80px"></asp:BoundColumn>
                                    <%--8--%><asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Fin" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-Width="80px"></asp:BoundColumn>
                                    <%--9--%><asp:BoundColumn DataField="datos_contacto" HeaderText="Datos de Contacto"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="400px" ItemStyle-Wrap="true">
                                    </asp:BoundColumn>
                                    <%--10--%><asp:TemplateColumn HeaderText="Mod" HeaderStyle-Width="35px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbModificar" runat="server" ToolTip="Modificar Oferta" ImageUrl="~/Images/modificar.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--11--%><asp:TemplateColumn HeaderText="Elim" HeaderStyle-Width="35px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbEliminar" runat="server" ToolTip="Eliminar Oferta" ImageUrl="~/Images/eliminar.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--12--%><asp:BoundColumn DataField="numero_postura" Visible="false"></asp:BoundColumn>
                                    <%--13--%><asp:BoundColumn DataField="codigo_operador" Visible="false"></asp:BoundColumn>
                                    <%--14--%><asp:BoundColumn DataField="destino_rueda" Visible="false"></asp:BoundColumn>
                                    <%--15--%><asp:BoundColumn DataField="codigo_punto_ruta" Visible="false"></asp:BoundColumn>
                                    <%--16--%><asp:BoundColumn DataField="codigo_periodo" Visible="false"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hdnPunta" runat="server" />
            </td>
        </tr>
    </table>
    <%--    <asp:Panel ID="Panel1" runat="server" Style="left: 60px; top: 100px; position: static;">
    </asp:Panel>
    --%>
    </form>
</body>
</html>