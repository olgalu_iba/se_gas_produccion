﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_posturas.aspx.cs" Inherits="Procesos_frm_posturas" MasterPageFile="~/PlantillaPrincipal.master"
    EnableEventValidation="false" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">

        window.onload = function() {
            var pos = window.name || 0;
            window.scrollTo(0, pos);
        }
        window.onunload = function() {
            window.name = self.pageYOffset || (document.documentElement.scrollTop + document.body.scrollTop);
        }

    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="divOferta" runat="server" style="position: absolute; left: 0; top: 0; width: 100%;">
                <table cellpadding="0" cellspacing="0" bgcolor="#000000">
                    <tr>
                        <td align="center">
                            <asp:DataGrid ID="dtgSubasta1" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1" runat="server" OnItemCommand="OnItemCommand_Click"
                                ShowHeader="false">
                                <ItemStyle CssClass="td2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="15px">
                                </ItemStyle>
                                <Columns>
                                    <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="60px"></asp:BoundColumn>
                                    <%--1--%><asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--2--%><asp:BoundColumn DataField="desc_unidad_medida" HeaderText="UM" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="60px"></asp:BoundColumn>
                                    <%--3--%><asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="120px"></asp:BoundColumn>
                                    <%--4--%><asp:BoundColumn DataField="desc_punto_entrega" HeaderText="Sitio Entrega"
                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px"></asp:BoundColumn>
                                    <%--5--%><asp:BoundColumn DataField="fecha_entrega" HeaderText="Periodo Entrega"
                                        DataFormatString="{0:yyyy/MM/dd}" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                    </asp:BoundColumn>
                                    <%--6--%><asp:BoundColumn DataField="cantidad_total_venta" HeaderText="Cantidad Vta"
                                        DataFormatString="{0: ###,###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--7--%><asp:BoundColumn DataField="precio_venta" HeaderText="precio Vta" ItemStyle-BackColor=""
                                        ItemStyle-Width="80px"></asp:BoundColumn>
                                    <%--8--%><asp:BoundColumn DataField="cantidad_compra" HeaderText="Cantidad Compra"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundColumn>
                                    <%--9--%><asp:BoundColumn DataField="precio_venta" HeaderText="Precio Compra" ItemStyle-Width="80px">
                                    </asp:BoundColumn>
                                    <%--10--%><asp:TemplateColumn HeaderText="Ofr" ItemStyle-Width="40px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbOfertar" runat="server" ToolTip="Ofertar" ImageUrl="~/Images/nuevo.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--11--%><asp:TemplateColumn HeaderText="Mod" ItemStyle-Width="40px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbModificar" runat="server" ToolTip="Modificar Oferta" ImageUrl="~/Images/modificar.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--12--%><asp:TemplateColumn HeaderText="Elim" ItemStyle-Width="40px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbEliminar" runat="server" ToolTip="Eliminar Oferta" ImageUrl="~/Images/eliminar.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--13--%><asp:BoundColumn DataField="estado_rueda" Visible="false" ItemStyle-Width="50px">
                                    </asp:BoundColumn>
                                    <%--14--%><asp:BoundColumn DataField="oferta_ya_realizada" Visible="false" ItemStyle-Width="50px">
                                    </asp:BoundColumn>
                                    <%--15--%><asp:BoundColumn DataField="numero_rueda" Visible="false" ItemStyle-Width="50px">
                                    </asp:BoundColumn>
                                    <%--16--%><asp:BoundColumn DataField="max_posturas" Visible="false" ItemStyle-Width="50px">
                                    </asp:BoundColumn>
                                    <%--17--%><asp:BoundColumn DataField="tipo_operador" Visible="false" ItemStyle-Width="50px">
                                    </asp:BoundColumn>
                                    <%--18--%><asp:BoundColumn DataField="descripcion" HeaderText="Notas" ItemStyle-Width="100px">
                                    </asp:BoundColumn>
                                    <%--19--%><asp:TemplateColumn HeaderText="graf" Visible="false" ItemStyle-Width="40px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbGrafica" runat="server" ToolTip="curva agregada" ImageUrl="~/Images/grafica.jpg" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <asp:DataGrid ID="dtgDeclara" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1" runat="server" ShowHeader="false" OnItemCommand="OnItemCommand1_Click">
                                <ItemStyle CssClass="td2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="15px">
                                </ItemStyle>
                                <Columns>
                                    <%--0--%><asp:BoundColumn DataField="codigo_trasportador" HeaderText="cod tra" ItemStyle-HorizontalAlign="Right"
                                        ItemStyle-Width="40px"></asp:BoundColumn>
                                    <%--1--%><asp:BoundColumn DataField="nombre_trasportador" HeaderText="nombre" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="200px"></asp:BoundColumn>
                                    <%--2--%><asp:BoundColumn DataField="codigo_punto_ini" HeaderText="Pto Ent" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="70px"></asp:BoundColumn>
                                    <%--3--%><asp:BoundColumn DataField="desc_punto_ini" HeaderText="desc Pto Ent" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="200px"></asp:BoundColumn>
                                    <%--4--%><asp:BoundColumn DataField="codigo_punto_fin" HeaderText="pto Fin" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="40px" Visible="false"></asp:BoundColumn>
                                    <%--5--%><asp:BoundColumn DataField="desc_punto_fin" HeaderText="desc Pto FIn" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="200px" Visible="false"></asp:BoundColumn>
                                    <%--6--%><asp:BoundColumn DataField="cantidad" HeaderText="cnt dec" DataFormatString="{0: ###,###,###,###,###,###,##0.00}"
                                        ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--7--%><asp:BoundColumn DataField="cantidad_no_disponible" HeaderText="cnt No Disp"
                                        ItemStyle-Width="100px" DataFormatString="{0: ###,###,###,###,###,###,##0.00}"
                                        ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--8--%><asp:BoundColumn DataField="precio" HeaderText="pre res" DataFormatString="{0:###,###,###,###,###,###,##0.00 }"
                                        ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--9--%><asp:TemplateColumn HeaderText="Mod" ItemStyle-Width="40px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbModificar" runat="server" ToolTip="Modificar Oferta" ImageUrl="~/Images/modificar.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--10--%><asp:BoundColumn DataField="codigo_declarador" visible ="false" ></asp:BoundColumn>
                                    <%--11--%><asp:BoundColumn DataField="codigo_declaracion_inf" visible ="false" ></asp:BoundColumn>
                                    <%--12--%><asp:TemplateColumn HeaderText="Elim" ItemStyle-Width="40px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbEliminar" runat="server" ToolTip="Eliminar Oferta" ImageUrl="~/Images/eliminar.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <HeaderStyle CssClass="th1" Font-Names="Arial, Helvetica, sans-serif" Font-Size="13px">
                                </HeaderStyle>
                            </asp:DataGrid>
                            <asp:HiddenField ID="hndCodSubasta" runat="server" />
                            <asp:HiddenField ID="hndId" runat="server" />
                            <asp:HiddenField ID="hndSubya" runat="server" />
                            <asp:HiddenField ID="hndCiudad" runat="server" />
                            <asp:HiddenField ID="hndPunta" runat="server" />
                            <asp:HiddenField ID="HndFechaRueda" runat="server" />
                            <asp:HiddenField ID="hndParcial" runat="server" />
                            <asp:HiddenField ID="hndTipo" runat="server" />
                            <asp:HiddenField ID="hndPosScroll" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>