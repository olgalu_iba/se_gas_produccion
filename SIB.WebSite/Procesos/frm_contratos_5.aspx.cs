﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using PCD_Infraestructura.Business;
using PCD_Infraestructura.Transaction;
using PCD_Infraestructura.DomainLayer;
using System.Diagnostics;

public partial class Procesos_frm_contratos_5 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        ltTablero.Text = "";
        ltTablero.Text += "<table border='0' cellspacing='0' cellpading='0'><tr>";
        ltTablero.Text += "<td  width='100%'align='center' bgcolor='#75923C' colspan='6'><font color='#000000' face='Arial, Helvetica, sans-serif'>OPERACIONES</font></td>";
        ltTablero.Text += "</tr>";
        ltTablero.Text += "<tr>";
        ltTablero.Text += "<td width='10%' align='center' bgcolor='#75923C'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>IDG</font></td>";
        ltTablero.Text += "<td width='20%' align='center' bgcolor='#75923C'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>PRECIO</font></td>";
        ltTablero.Text += "<td width='20%' align='center' bgcolor='#75923C'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>CANTIDAD</font></td>";
        ltTablero.Text += "<td width='20%' align='center' bgcolor='#75923C'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>COMPRADOR</font></td>";
        ltTablero.Text += "<td width='30%' align='center' bgcolor='#75923C'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>VENDEDOR</font></td>";
        ltTablero.Text += "</tr>";

        try
        {
            string lsComprador = "";
            string lsVendedor = "";
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador", "@P_tipo_perfil" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
            string[] lValorParametros = { Session["numero_rueda"].ToString(), goInfo.cod_comisionista, Session["tipoPerfil"].ToString() };
            SqlDataReader lLector;
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetMisContratos5", lsNombreParametros, lTipoparametros, lValorParametros);
            if (lLector.HasRows)
            {
                while (lLector.Read())
                {
                    ltTablero.Text += "<tr>";
                    ltTablero.Text += "<td width='10%' align='center' bgcolor='#9BBB59'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["numero_id"].ToString() + "</font></td>";
                    ltTablero.Text += "<td width='35%' align='center' bgcolor='#9BBB59'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["precio"].ToString() + "</font></td>";
                    ltTablero.Text += "<td width='10%' align='center' bgcolor='#9BBB59'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["cantidad"].ToString() + "</font></td>";
                    ltTablero.Text += "<td width='25%' align='center' bgcolor='#9BBB59'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["operador_compra"].ToString() + "</font></td>";
                    ltTablero.Text += "<td width='20%' align='center' bgcolor='#9BBB59'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["lsVendedor"].ToString() + "</font></td>";
                    ltTablero.Text += "</tr>";
                }
                ltTablero.Text += "</table>";
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.mensajes.location='frm_mensajes_5.aspx';", true);
        }
        catch (Exception ex)
        {
        }
    }
}