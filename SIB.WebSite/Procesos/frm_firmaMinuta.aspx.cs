﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using iTextSharpCert.text;
using iTextSharpCert.text.pdf;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;
// Libbrerias para generar Codigo QR 20181217


namespace Procesos
{
    public partial class Procesos_frm_firmaMinuta : Page
    {
        InfoSessionVO goInfo = null;
        clConexion lConexion = null;
        SqlDataReader lLector;
        DataSet lds = new DataSet();
        SqlDataAdapter lsqldata = new SqlDataAdapter();
        string oRutaImg = ConfigurationManager.AppSettings["RutaIMG"];
        string oRutaFont = ConfigurationManager.AppSettings["RutaFont"];

        /// <summary>
        /// 
        /// </summary>
        public string HdfOper
        {
            get { return ViewState["HdfOper"] != null && !string.IsNullOrEmpty(ViewState["HdfOper"].ToString()) ? ViewState["HdfOper"].ToString() : string.Empty; }
            set { ViewState["HdfOper"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string HdfRutaMinuta
        {
            get { return ViewState["HdfRutaMinuta"] != null && !string.IsNullOrEmpty(ViewState["HdfRutaMinuta"].ToString()) ? ViewState["HdfRutaMinuta"].ToString() : string.Empty; }
            set { ViewState["HdfRutaMinuta"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = "Firma Minuta";
            lConexion = new clConexion(goInfo);

            if (IsPostBack) return;

            //Titulo
            Master.Titulo = "Informes";

            /// Llenar controles del Formulario
            HdfRutaMinuta = ConfigurationManager.AppSettings["RutaMinuta"];
            hdfRutaImg.Value = ConfigurationManager.AppSettings["RutaIMG"];
            hdfOperador.Value = goInfo.cod_comisionista;
            hdfFechaAct.Value = DateTime.Now.ToString("yyyy/MM/dd");
            actualiza_datos();

        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void actualiza_datos()
        {
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetMinutaPend";
                lComando.Parameters.Add("@P_tipo_datos", SqlDbType.Char).Value = "F";
                lComando.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = hdfOperador.Value;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgFirma.DataSource = lds;
                dtgFirma.DataBind();
                lConexion.Cerrar();
                btnConsultar_Click(null, null);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Error al generar la consulta.!");

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dtgFirma_EditCommand(object source, DataGridCommandEventArgs e)
        {
            if (this.dtgFirma.Items[e.Item.ItemIndex].Cells[11].Text == "S")
            {
                HdfOper= dtgFirma.Items[e.Item.ItemIndex].Cells[2].Text;
                LblTituloArc.Text = "Firma con código IFE de ID de registro " + HdfOper;
                TxtCodigo.Text = "";
                Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
            }
            else
                Toastr.Error(this, "No tiene un token vigente para la firma");

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dtgImprimir_EditCommand(object source, DataGridCommandEventArgs e)
        {
            string lsEstado = this.dtgImprimir.Items[e.Item.ItemIndex].Cells[12].Text;
            if (lsEstado == "3" || lsEstado == "4" || lsEstado == "5")
            {
                string lsMensaje = "";
                string[] lsNombreParametros = { "@P_numero_operacion", "@P_codigo_operador" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { this.dtgImprimir.Items[e.Item.ItemIndex].Cells[2].Text, hdfOperador.Value };
                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetImprimirMin", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                    {
                        lsMensaje += lLector["error"] + "\\n";
                    }
                }
                if (lsMensaje == "")
                {
                    string lsArchivo = "../Minuta/" + this.dtgImprimir.Items[e.Item.ItemIndex].Cells[11].Text;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + lsArchivo + "');", true);
                    actualiza_datos();
                }
                else
                    Toastr.Warning(this, lsMensaje);

            }
            else
            {
                Toastr.Warning(this, "La minuta está " + this.dtgImprimir.Items[e.Item.ItemIndex].Cells[13].Text);

            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnFirmar_Click(object sender, EventArgs e)
        {
            lblMensaje.Text = "";
            string archivoQr = "";
            string lsCodigo = "";
            string lsNombreCer = "";
            string lsPunta = "";
            string lsNumFirma = "";

            string[] lsNombreParametros = { "@P_numero_operacion", "@P_codigo_operador", "@P_token" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
            string[] lValorParametros = { HdfOper, hdfOperador.Value, TxtCodigo.Text };
            if (TxtCodigo.Text == "")
                lblMensaje.Text = "Debe ingresar el Token";
            try
            {
                if (lblMensaje.Text == "")
                {
                    lConexion.Abrir();
                    SqlDataReader lLector;
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetFirmaMinuta", lsNombreParametros, lTipoparametros, lValorParametros);
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                        {
                            if (lLector["ind_error"].ToString() == "S")
                                lblMensaje.Text += lLector["error"] + "<br>";
                            else
                            {
                                lsCodigo =  lLector["desc_minuta"] + "\n" + lLector["desc_punta"] + "\n" + lLector["nombre_ope"] + "\n" + lLector["nombre_usr"] + "\n" + TxtCodigo.Text;

                                QrEncoder oEnCoder = new QrEncoder(ErrorCorrectionLevel.H);
                                QrCode oCodigo = new QrCode();
                                oEnCoder.TryEncode(lsCodigo, out oCodigo);
                                GraphicsRenderer oGrafica = new GraphicsRenderer(new FixedCodeSize(400, QuietZoneModules.Zero), System.Drawing.Brushes.Black, System.Drawing.Brushes.White);
                                MemoryStream msQ = new MemoryStream();
                                oGrafica.WriteToStream(oCodigo.Matrix, ImageFormat.Png, msQ);
                                var ImageTmp = new Bitmap(msQ);
                                var oImagen = new Bitmap(ImageTmp, new Size(new Point(200, 200)));
                                lsPunta = lLector["punta"].ToString();
                                archivoQr = HdfRutaMinuta + HdfOper + "_" + lsPunta + ".png";
                                if (File.Exists(archivoQr))
                                    File.Delete(archivoQr);
                                oImagen.Save(archivoQr, ImageFormat.Png);
                                oImagen.Dispose();

                                lsNombreCer = lLector["nombre_cert"].ToString();
                                lsNumFirma = lLector["num_firma"].ToString();

                            }
                        }
                    }
                    lLector.Close();
                    lConexion.Cerrar();
                    if (lblMensaje.Text == "")
                    {
                        //Inlcuir token punta
                        // open the reader
                        PdfReader reader = new PdfReader(HdfRutaMinuta + lsNombreCer);
                        iTextSharpCert.text.Rectangle size = reader.GetPageSizeWithRotation(1);
                        Document document = new Document(PageSize.LETTER, 20, 20, 20, 20);

                        BaseFont bf = BaseFont.CreateFont(oRutaFont + "arialbd.ttf", BaseFont.WINANSI, true);

                        // open the writer
                        string pdfDest = HdfRutaMinuta + "MinutaC1_" + HdfOper + "_" + lsNumFirma;
                        FileStream fs = new FileStream(pdfDest, FileMode.Create, FileAccess.Write);
                        PdfWriter writer = PdfWriter.GetInstance(document, fs);
                        document.Open();

                        // the pdf content
                        PdfContentByte cb = writer.DirectContent;
                        cb.SetFontAndSize(bf, 8);
                        cb.SetColorFill(iTextSharpCert.text.Color.BLACK);

                        /// Creo el objeto Imagen
                        iTextSharpCert.text.Image codigoQr = iTextSharpCert.text.Image.GetInstance(archivoQr);
                        codigoQr.ScalePercent(35);
                        codigoQr.Alignment = Element.ALIGN_CENTER;

                        // write the text in the pdf content
                        cb.BeginText();
                        string text = "";
                        if (lsPunta == "C")
                        {
                            codigoQr.SetAbsolutePosition(440, 50);
                            text = "Código QR Comprador:" + TxtCodigo.Text;
                            cb.ShowTextAligned(1, text, 468, 35, 0);
                        }
                        else
                        {
                            codigoQr.SetAbsolutePosition(80, 50);
                            text = "Código QR Vendedor:" + TxtCodigo.Text;
                            cb.ShowTextAligned(1, text, 120, 35, 0);
                        }


                        cb.EndText();

                        for (int i = 1; i <= reader.NumberOfPages; i++)
                        {
                            // create the new page and add it to the pdf
                            PdfImportedPage page = writer.GetImportedPage(reader, i);
                            cb.AddTemplate(page, 0, 0);
                        }
                        // close the streams and voilá the file should be changed :)

                        document.Add(codigoQr);
                        document.Close();
                        fs.Close();
                        writer.Close();
                        reader.Close();
                        File.Delete(archivoQr);
                        File.Delete(HdfRutaMinuta + lsNombreCer);
                        File.Move(pdfDest, HdfRutaMinuta + lsNombreCer);
                        Modal.Cerrar(this, CrearRegistro.ID);
                        Toastr.Success(this, "Documento firmado correctamente");

                        actualiza_datos();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Error al firmar el PDF." + ex.Message;
            }
            if (lblMensaje.Text != "")
            {

                Toastr.Error(this, lblMensaje.Text);
                lblMensaje.Text = "";

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCancelar_Click(object sender, EventArgs e)
        {
            actualiza_datos();
            Modal.Cerrar(this, CrearRegistro.ID);
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            lblMensaje.Text = "";
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                lds = new DataSet();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetMinutaPend";
                lComando.Parameters.Add("@P_tipo_datos", SqlDbType.Char).Value = "I";
                lComando.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = hdfOperador.Value;
                if (TxtOperacion.Text != "")
                    lComando.Parameters.Add("@P_numero_contrato", SqlDbType.Int).Value = TxtOperacion.Text;
                if (txtFechaOper.Text != "")
                    lComando.Parameters.Add("@P_fecha_operacion", SqlDbType.Int).Value = txtFechaOper.Text;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgImprimir.DataSource = lds;
                dtgImprimir.DataBind();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Error al generar la consulta de las minutas para imprimir.!");
            }
        }
    }
}