﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;

public partial class Procesos_frm_OperadorHabUvlp : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Habilitacion de Operadores Compradores subasta UVLP";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' And codigo_operador != 0 order by razon_social", 0, 4);
            lConexion.Cerrar();
            CargarDatos();
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            lDdl.Items.Add(lItem1);
        }

        lLector.Close();
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        CargarDatos();
    }
    /// <summary>
    /// 
    /// </summary>
    protected void CargarDatos()
    {
        lblMensaje.Text = "";
        if (lblMensaje.Text == "")
        {
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetOpeHabUvlp";
                lComando.Parameters.Add("@P_codigo_operador", SqlDbType.VarChar).Value = ddlOperador.SelectedValue;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgMaestro.DataSource = lds;
                dtgMaestro.DataBind();
                if (dtgMaestro.Items.Count > 0)
                {
                    tblGrilla.Visible = true;
                    imbExcel.Visible = true;
                    btnAprobar.Visible = true;
                    CheckBox ChBox = null;
                    foreach (DataGridItem Grilla in this.dtgMaestro.Items)
                    {
                        ChBox = (CheckBox)Grilla.Cells[0].Controls[1];
                        if (Grilla.Cells[4].Text.Trim() != "0")
                        {
                            ChBox.Enabled = false;
                        }
                    }

                }
                else
                    lblMensaje.Text = "No se encontraron Registros para Visualizar.! ";
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pude Generar el Informe.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: ChkTodos_CheckedChanged
    /// Fecha: Abril 24 21 de 2012
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para chekear todos los registros
    /// Modificacion:
    /// </summary>
    protected void ChkTodos_CheckedChanged(object sender, EventArgs e)
    {
        //Selecciona o deselecciona todos los items del datagrid segun el checked del control
        CheckBox chk = (CheckBox)(sender);
        foreach (DataGridItem Grilla in this.dtgMaestro.Items)
        {
            CheckBox Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
            if (Checkbox.Enabled  )
                Checkbox.Checked = chk.Checked;
        }
        btnAprobar.Enabled = true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfCargueNeg" + DateTime.Now + ".xls";
        string lstitulo_informe = "Informe de operadores habilitados para compra en subasta UVLP";
        try
        {
            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgMaestro.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            dtgMaestro.Columns[0].Visible = false;
            dtgMaestro.Columns[5].Visible = false;
            lform.Controls.Add(dtgMaestro);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());
            Response.End();

            dtgMaestro.Columns[0].Visible = true;
            dtgMaestro.Columns[5].Visible = true;
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Excel.!" + ex.Message.ToString();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAprobar_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";

        int liProce = 0;
        string[] lsNombreParametros = { "@P_codigo_operador_hab", "@P_codigo_operador", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
        String[] lValorParametros = { "0", "0", "1" };

        CheckBox Checkbox = null;
        foreach (DataGridItem Grilla in this.dtgMaestro.Items)
        {
            Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
            if (Checkbox.Checked == true)
            {
                liProce++;
                if (Grilla.Cells[4].Text != "0")
                {
                    lblMensaje.Text += "El operador " + Grilla.Cells[2].Text + " Ya fue aprobado<br>";
                }
            }
        }
        if (liProce == 0)
            lblMensaje.Text += "Debe seleccionar registros para aprobar<br>";
        if (lblMensaje.Text == "")
        {
            lConexion.Abrir();
            SqlTransaction oTransaccion;
            oTransaccion = lConexion.gObjConexion.BeginTransaction(System.Data.IsolationLevel.Serializable);
            // Defino la trnasacción para el manejo de la consistencia de los datos.
            try
            {
                foreach (DataGridItem Grilla in this.dtgMaestro.Items)
                {
                    Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                    if (Checkbox.Checked == true)
                    {
                        lValorParametros[1] = Grilla.Cells[1].Text.Trim();
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetOpeHabUvlp", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                        if (goInfo.mensaje_error != "")
                        {
                            oTransaccion.Rollback();
                            lblMensaje.Text = "Se presento un Problema en la Aprobacion del operador {" + Grilla.Cells[2].Text.Trim() + "}.! " + goInfo.mensaje_error.ToString();
                            lConexion.Cerrar();
                            break;
                        }
                        lLector.Close();
                        lLector.Dispose();
                    }
                }
                oTransaccion.Commit();
                lblMensaje.Text = "Registros  Aprobados Correctamente.!";
                CargarDatos();

            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                oTransaccion.Rollback();
                lConexion.Cerrar();
                lblMensaje.Text = ex.Message;
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            string[] lsNombreParametros = { "@P_codigo_operador_hab", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { this.dtgMaestro.Items[e.Item.ItemIndex].Cells[4].Text, "3" };
            if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[4].Text == "0")
                lblMensaje.Text = "EL operador no se ha aprobado";
            if (lblMensaje.Text == "")
            {
                try
                {
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetOpeHabUvlp", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Text = "Se presentó un Problema en la ELiminación del registro.!";
                        lConexion.Cerrar();
                    }
                    else
                        CargarDatos();
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = "Error al eliminar el registro. " + ex.Message.ToString();
                }
            }

        }
    }

}