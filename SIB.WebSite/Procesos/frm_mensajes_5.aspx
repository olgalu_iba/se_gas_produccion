﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_mensajes_5.aspx.cs" Inherits="Procesos_frm_mensajes_5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="datacontainer" style="position: absolute; left: 0; top: 0px; width: 100%;"
        onmouseout="scrollspeed=1;">
        <table align="center" cellpadding="0" cellspacing="1" width="100%" height="100%"
            bgcolor="#000000" style='border-color: #000000' border='1' class="td5">
            <tr>
                <td width='100%'align='center' bgcolor='#E46D0A' style='border-color:#E46D0A'  colspan='5'>
                <font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>
                    MIS MENSAJES
                    
                    
                    
                    </font>
                </td>
            </tr>
            <tr>
                <td class="td5">
                    <asp:DataGrid ID="dtgMensaje" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1" runat="server" OnEditCommand="dtgMensaje_EditCommand" Width="100%">
                        <ItemStyle Font-Names="Arial, Helvetica, sans-serif" Font-Size="15px" ForeColor="#000000"
                            BackColor="#F79632"></ItemStyle>
                        <Columns>
                            <%--0--%><asp:BoundColumn DataField="codigo_operador" Visible="false"></asp:BoundColumn>
                            <%--0--%><asp:BoundColumn DataField="nombre_operador" HeaderText="NOMBRE" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--0--%><asp:BoundColumn DataField="cnt_pend" HeaderText="PEND" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--0--%><asp:BoundColumn DataField="hora_ult_mensaje" HeaderText="HORA" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="MSG" EditText="MSG" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                            <asp:EditCommandColumn HeaderText="ELIM" EditText="ELIM" HeaderStyle-CssClass="">
                            </asp:EditCommandColumn>
                        </Columns>
                        <HeaderStyle Font-Names="Arial, Helvetica, sans-serif" Font-Size="15px" ForeColor="#000000"
                            BackColor="#F79632" HorizontalAlign="Center" ></HeaderStyle>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
