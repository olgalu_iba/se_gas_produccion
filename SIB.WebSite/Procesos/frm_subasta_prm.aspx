﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_subasta_prm.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_subasta_prm" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="Table1" width="100%">
        <tr>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Rueda: </font>
                <br />
                <asp:DropDownList ID="ddlRueda" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRueda_SelectedIndexChanged"
                    Font-Names="Arial, Helvetica, sans-serif" Font-Size="15px">
                </asp:DropDownList>
            </td>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Fase: </font>
                <br />
                <font color='#F5DA81' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Label ID="lblFase" runat="server"></asp:Label>
                </font>
            </td>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Hora Fase:
                </font>
                <br />
                <font color='#F5DA81' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Label ID="lblHora" runat="server"></asp:Label>
                </font>
            </td>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Participante:
                
       
                </font>
                <br />
                <font color='#F5DA81' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Label ID="lblOperador" runat="server"></asp:Label>
                </font>
            </td>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Label ID="lblPunto" runat="server"></asp:Label>
                </font>
                <br />
                <asp:DropDownList ID="ddlPunto" runat="server" Font-Names="Arial, Helvetica, sans-serif"
                    Font-Size="15px">
                </asp:DropDownList>
            </td>
            <td align="center" class="td4">
                <asp:Button ID="btnDecInf" Text="Plano Dec Inf" runat="server" Visible="false" OnClientClick="window.open('frm_DecInf_1.aspx','','width=450,height=250,left=400,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no,scrollbars=1');" />
                <asp:Button ID="btnDecPre" Text="Plano Dec Pre" runat="server" Visible="false" OnClientClick="window.open('frm_DecPre_1.aspx','','width=450,height=250,left=400,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no,scrollbars=1');" />
                <asp:Button ID="btnDecMan" Text="Ing Dec Inf" runat="server" Visible="false" OnClientClick="window.open('frm_DecInfMan_1.aspx','','width=450,height=250,left=400,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no,scrollbars=1');" />
            </td>
            <td align="center" class="td4">
                <asp:Button ID="btnSuspender" Text="Suspender" runat="server" Visible="false" OnClientClick="window.open('frm_Suspender_2.aspx','','width=450,height=250,left=400,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no,scrollbars=1');" />
                <br />
                <asp:Button ID="btnReactivar" Text="Reactivar" runat="server" Visible="false" OnClientClick="window.open('frm_reactivar_2.aspx','','width=700,height=400,left=250,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no,scrollbars=1');" />
            </td>
            <td align="center" class="td4">
                <br />
                <asp:Button ID="btnBuscar" Text="Buscar" runat="server" OnClick="btnBuscar_Click" />
                <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Height="30" Visible="false" />
                <%--20160128--%>
                <asp:DataGrid ID="dtgSubasta" runat="server" AutoGenerateColumns="False" Visible="false">
                    <ItemStyle CssClass="td2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="15px"></ItemStyle>
                    <Columns>
                        <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--1--%><asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--2--%><asp:BoundColumn DataField="desc_unidad_medida" HeaderText="Unidad de Medida"
                            ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--3--%><asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contractual"
                            ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--4--%><asp:BoundColumn DataField="desc_punto_entrega" HeaderText="Punto de Entrega/ Ruta"
                            ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--5--%><asp:BoundColumn DataField="fecha_entrega" HeaderText="Fecha Inicial" DataFormatString="{0:yyyy/MM/dd}"
                            ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <%--6--%><asp:BoundColumn DataField="cantidad_total_venta" HeaderText="Cantidad/Capacidad"
                            DataFormatString="{0: ###,###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
        <%--<tr>
            <td align="center" class="td3">
                Subasta ID
            </td>
            <td align="center" class="td4">
                <asp:TextBox ID="txtId" runat="server"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="FtetxtId" runat="server" Enabled="True" FilterType="Custom, Numbers"
                    TargetControlID="txtId">
                </ajaxToolkit:FilteredTextBoxExtender>
            </td>
            <td class="td3">
                Subyacente
            </td>
            <td align="center" class="td4" colspan="2">
                <asp:DropDownList ID="ddlSubyacente" runat="server">
                </asp:DropDownList>
            </td>
            <td class="td3">
                Sitio Entrega
            </td>
            <td align="center" class="td4">
                <asp:DropDownList ID="ddlCiudad" runat="server">
                </asp:DropDownList>
            </td>
            <td class="td3">
                Punta
            </td>
            <td align="center" class="td4">
                <asp:DropDownList ID="ddlPunta" runat="server">
                    <asp:ListItem Text="Seleccione" Value=""></asp:ListItem>
                    <asp:ListItem Text="Compra" Value="C"></asp:ListItem>
                    <asp:ListItem Text="Venta" Value="V"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td3">
                <asp:Button ID="btnBuscar" Text="Buscar" runat="server" OnClick="btnBuscar_Click" />
            </td>
        </tr>--%>
    </table>
    <asp:HiddenField ID="HndFechaRueda" runat="server" />
    <asp:HiddenField ID="HndTpoRueda" runat="server" />
</asp:Content>
