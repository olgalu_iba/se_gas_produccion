﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_enviarMensaje_5.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_enviarMensaje_5" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" language="javascript">
        //        document.location = no;
        function confirmar() {
            return confirm("Esta seguro que desea enviar el mensaje!")
        }

    </script>



    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td6" align="center">
                        <asp:Label ID="lblTitulo" runat="server" Text="ENVIO DE MENSAJES" Font-Bold="true"></asp:Label>
                        <asp:DropDownList ID="ddlOperador" runat="server" Width="250px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="td6" align="center">
                        <asp:Label ID="lblOperador" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="td6" align="center">
                        <asp:TextBox ID="TxtMensaje" runat="server" Width="300px" TextMode="MultiLine" Rows="3"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" class="td6">
                        <br />
                        <asp:Button ID="btnMensaje" Text="Enviar" runat="server" OnClientClick="return confirmar();"
                            OnClick="btnMensaje_Click" ValidationGroup="comi" CausesValidation="true" />
                        <asp:HiddenField ID="hdfCodOperador" runat="server" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
