﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_definirHora_3 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    String strRutaArchivo;
    SqlDataReader lLector;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString() + "mensaje.txt" ;
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
                /// Obtengo los Datos del ID Recibido
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda rue", " rue.numero_rueda= " + Session["numero_rueda"].ToString() + " and rue.estado<> 'F'");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    /// Lleno las etiquetas de la pantalla con los datos del Id.
                    if (lLector["minutos_prox_ronda"].ToString()=="0")
                        lblRonda.Text = "Próxima Ronda: 0";
                    else
                        lblRonda.Text = "Próxima Ronda: " + (Convert.ToInt16(lLector["ronda_actual"].ToString()) + 1).ToString() ;
                    lblHoraFin.Text = "Hora fin ronda: " + lLector["hora_prox_fase"].ToString();
                    lLector.Close();
                    lLector.Dispose();
                }
                else
                    Toastr.Warning(this, "El ID enviado NO está disponible para actulizar proximas rondas.!");
                lLector.Dispose();
                lLector.Close();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnHora_Click(object sender, EventArgs e)
    {
        string oError = "";
        string sMensaje = "";
        double ldPreAct = 0;
        double ldPreRes = 0;

        if (TxtHora.Text == "")
            oError += "Debe digitar la Hora de inicio de la próxima ronda\\n";
        if (TxtMinutos.Text == "" || TxtMinutos.Text == "0")
            oError += "Debe digitar la duración de la próxima ronda\\n";

        if (oError == "")
        {
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_hora_ini", "@P_duracion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int};
            string[] lValorParametros = { Session["numero_rueda"].ToString(), TxtHora.Text, TxtMinutos.Text}; ///  

            SqlDataReader lLector;
            lConexion.Abrir();
            try
            {
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_setHoraRonda3", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        oError = lLector["error"].ToString() + "\\n";
                }
                if (oError == "")
                {
                    sMensaje = "Hora de la próxima fase actualizada Correctamente";
                    File.SetCreationTime(strRutaArchivo, DateTime.Now);
                    Toastr.Success(this, sMensaje);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                oError = "Error al definir la hora de inicio. " + ex.Message;
                lConexion.Cerrar();
            }
        }
        if (oError != "")
        {
            Toastr.Warning(this, oError);
        }
    }
}
