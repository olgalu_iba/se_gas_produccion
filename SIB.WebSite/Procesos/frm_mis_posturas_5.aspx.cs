﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using PCD_Infraestructura.Business;
using PCD_Infraestructura.Transaction;
using PCD_Infraestructura.DomainLayer;
using System.Diagnostics;

public partial class Procesos_frm_mis_posturas_5 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        ltTablero.Text = "";
        ltTablero.Text += "<table border='0' cellspacing='0' cellpading='0'><tr>";
        ltTablero.Text += "<td  width='100%'align='center' bgcolor='#E46D0A' style='border-color:#E46D0A' colspan='3'><font color='#000000' face='Arial, Helvetica, sans-serif'>POSTURAS PROPIAS</font></td>";
        ltTablero.Text += "</tr>";
        ltTablero.Text += "<tr>";
        ltTablero.Text += "<td width='10%' style='border-color:#E46D0A' align='center' bgcolor='#E46D0A'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>IDG</font></td>";
        ltTablero.Text += "<td width='20%' style='border-color:#E46D0A' align='center' bgcolor='#E46D0A'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>PRECIO</font></td>";
        ltTablero.Text += "<td width='20%' style='border-color:#E46D0A' align='center' bgcolor='#E46D0A'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>TIPO</font></td>";
        ltTablero.Text += "</tr>";

        try
        {

            string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { Session["numero_rueda"].ToString(), goInfo.cod_comisionista };
            SqlDataReader lLector;
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetMisPosturas5", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
            if (goInfo.mensaje_error == "")
            {
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                    {
                        ltTablero.Text += "<tr>";
                        ltTablero.Text += "<td width='10%' style='border-color:#F79632' align='left' bgcolor='#F79632'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["numero_id"].ToString() + "</font></td>";
                        ltTablero.Text += "<td width='35%' style='border-color:#F79632' align='right' bgcolor='#F79632'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["precio"].ToString() + "</font></td>";
                        ltTablero.Text += "<td width='10%' style='border-color:#F79632' align='center' bgcolor='#F79632'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["tipo_postura"].ToString() + "</font></td>";
                        ltTablero.Text += "</tr>";
                    }
                    ltTablero.Text += "</table>";
                }
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
        }
    }
}
