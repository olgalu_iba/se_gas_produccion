﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_Reactivar_6 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    String strRutaArchivo;
    SqlDataReader lLector;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString() + "mensaje.txt";
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            /// Obtengo los Datos de la rueda
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda rue, m_estado_gas est", " rue.numero_rueda = " + Session["numero_rueda"].ToString() + " and rue.fase_reactivacion= est.sigla_estado and est.tipo_estado ='R'");
            if (lLector.HasRows)
            {
                lLector.Read();
                /// Lleno las etiquetas de la pantalla con los datos del Id.
                hndFase.Value = lLector["fase_reactivacion"].ToString();
                lblEstado.Text = "Fase Re-Activación: " + hndFase.Value + " - " + lLector["descripcion_estado"].ToString();
                lLector.Close();
                lLector.Dispose();
            }
            else
            {
                lblEstado.Text = "";
                hndFase.Value = "C";
            }
            int liFase = 0;
            if (hndFase.Value != "C")
                liFase = Convert.ToInt16(hndFase.Value);
            if (liFase > 1)
            {
                txtHoraIniPubV.Enabled = false;
                txtHoraFinPubV.Enabled = false;
            }
            if (liFase > 3)
            {
                txtHoraIniCntD.Enabled = false;
                txtHoraFinCntD.Enabled = false;
            }
            if (liFase > 4)
            {
                txtHoraIniComp.Enabled = false;
                txtHoraFinComp.Enabled = false;
            }
            lLector.Dispose();
            lLector.Close();
            lConexion.Cerrar();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReactivar_Click(object sender, EventArgs e)
    {
        string oError = "";
        string sMensaje = "";

        //if (TxtHora.Text == "")
        //    oError += "Debe digitar la Hora de inicio de la próxima ronda\\n";
        //if (TxtMinutos.Text == "")
        //    oError += "Debe la duración de la próxima ronda\\n";
        if (TxtObservacion.Text == "")
            oError += "Debe digitar las observaciones de la reactivaccion\\n";

        if (oError == "")
        {
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_observaciones", "@P_inicio_fase1", "@P_fin_fase1", "@P_inicio_fase2", "@P_fin_fase2", "@P_inicio_fase3", "@P_fin_fase3", "@P_inicio_fase4", "@P_fin_fase4" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { Session["numero_rueda"].ToString(), TxtObservacion.Text, txtHoraIniPubV.Text, txtHoraFinPubV.Text, txtHoraIniCntD.Text, txtHoraFinCntD.Text, txtHoraIniComp.Text, txtHoraFinComp.Text, txtHoraIniNeg.Text, txtHoraFinNeg.Text }; ///  

            SqlDataReader lLector;
            lConexion.Abrir();
            try
            {
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetActRueda", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        oError += lLector["error"].ToString() + "\\n";
                }
                if (oError == "")
                {
                    sMensaje = "la rueda se reactivó exitosamente";
                    File.SetCreationTime(strRutaArchivo, DateTime.Now);
                    Toastr.Success(this, sMensaje);               
                    Session["hora"] = "";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                oError = "Error al reactivar la rueda. " + ex.Message;
                lConexion.Cerrar();
            }
        }
        if (oError != "")
        {
            Toastr.Warning(this, oError);
        }
    }
}