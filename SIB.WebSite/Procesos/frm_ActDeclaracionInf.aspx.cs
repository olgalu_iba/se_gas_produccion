﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_ActDeclaracionInf : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    String strRutaArchivo;
    String strRutaArchivo1;
    SqlDataReader lLector;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            hndDestino.Value = Session["destino_rueda"].ToString();
            if (this.Request.QueryString["codigo_dec"] != null && this.Request.QueryString["codigo_dec"].ToString() != "")
                hndCodDec.Value = this.Request.QueryString["codigo_dec"].ToString();
            else
                hndCodDec.Value = "0";
            lblRueda.Text = "No. Rueda: " + Session["numero_rueda"].ToString();
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlTitular, "m_operador", " estado ='A' order by razon_social ", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlPunto, "m_pozo", " estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlPuntoFin, "m_pozo", " estado ='A' order by descripcion", 0, 1);
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_declaracion_inf", " codigo_declaracion_inf = " + hndCodDec.Value);
            if (lLector.HasRows)
            {
                lLector.Read();
                ddlTitular.SelectedValue = lLector["codigo_trasportador"].ToString();
                ddlPunto.SelectedValue = lLector["codigo_punto_ini"].ToString();
                if (hndDestino.Value == "T")
                {
                    ddlPuntoFin.SelectedValue = lLector["codigo_punto_fin"].ToString();
                    lblDestino.Text = "Destino: Transporte";
                    lblPunto.Text = "Ruta: ";
                }
                else
                {
                    lblDestino.Text = "Destino: Suministro de Gas";
                    lblPunto.Text = "Punto de Entrega: ";
                    ddlPuntoFin.Visible = false;
                }
                TxtCntDec.Text = lLector["cantidad"].ToString();
            }
            lLector.Close();
            lConexion.Cerrar();
            ddlTitular.Enabled = false;
            ddlPunto.Enabled = false;
            ddlPuntoFin.Enabled = false;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOfertar_Click(object sender, EventArgs e)
    {
        string oMensaje = "";
        if (TxtCntDec.Text == "")
            oMensaje += "Debe digitar la cantidad declarada\\n";
        else
        {
            try
            {
                Convert.ToInt32(TxtCntDec.Text.Replace(",", ""));
            }
            catch (Exception ex)
            {
                oMensaje += "Cantidad declarada no válida\\n";
            }
        }
        if (oMensaje == "")
        { 
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_declaracion_inf", "@P_cantidad_declarada" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int};
            string[] lValorParametros = { Session["numero_rueda"].ToString(), hndCodDec.Value ,TxtCntDec.Text.Replace(",", "") };
            lConexion.Abrir();
            try
            {
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_UptDeclaracionInf", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    oMensaje = "Error al actualizar la declaracion de información. " + goInfo.mensaje_error.ToString() + "\\n";
                }
                else
                {
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                            oMensaje = lLector["mensaje"].ToString() + "\\n";
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        Session["mis_ofertas"] = "S";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Declaración de Información actualizada correctamente.!" + "');", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                oMensaje = "Error al actualizar la declaración de información. " + ex.Message;
                lConexion.Cerrar();
            }
            if (oMensaje != "")
            {
                Toastr.Warning(this, oMensaje, "Warning", 50000);                
            }
        }
        else
            Toastr.Warning(this, oMensaje, "Warning", 50000);
     

    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }

}
