﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;
using AjaxControlToolkit;
using Segas.Web.Elements;

public partial class Procesos_frm_titulos_3 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    //String strRutaArchivo;
    private System.TimeSpan diffResult;
    static String lsCadenaCon;
    String strRutaArchivo;
    

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            try
            {
                lsCadenaCon = "Data Source=" + goInfo.Servidor.ToString() + "; Initial Catalog=" + goInfo.BaseDatos.ToString() + ";User ID = " + goInfo.Usuario.ToString() + "; Password=" + goInfo.Password.ToString();
                strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString();
                string oArchivo = strRutaArchivo + "subasta-" + Session["numero_rueda"].ToString() + ".txt";
                lConexion1 = new clConexion(goInfo);
                if (Session["ind_tipo_rueda"].ToString() == "P")
                {
                    dtgSubasta3.Columns[4].HeaderText = "Ruta";
                }
                else
                {
                    dtgSubasta3.Columns[4].HeaderText = "Tramo";
                }

                if (goInfo.cod_comisionista == "0")
                {
                    dtgSubasta3.Columns[9].Visible = false; //ofertar
                    dtgSubasta3.Columns[10].Visible = false; //modificar
                    dtgSubasta3.Columns[11].Visible = false;// eliminar
                }
                if (Session["estado"].ToString() == "5" || Session["estado"].ToString() == "F")
                    dtgSubasta3.Columns[12].Visible = true;
                else
                    dtgSubasta3.Columns[12].Visible = false;

                cargarDatosGrilla();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.postura.location= 'frm_posturas_3.aspx?';", true);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, ex.Message);
            }
        }
    }
    /// <summary>
    /// Nombre: cargarDatosGrilla
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para Cargar los datos en la Grio.
    /// Modificacion:
    /// </summary>
    private void cargarDatosGrilla()
    {
        try
        {
            //determina las columnas para el subastatodr o los operaores
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador", "@P_codigo_ruta" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0" };
            lConexion.Abrir();
            dtgSubasta3.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaPuja3", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgSubasta3.DataBind();
            lConexion.Cerrar();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.postura.location= 'frm_posturas_3.aspx';", true);
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, ex.Message);
        }

    }
}

