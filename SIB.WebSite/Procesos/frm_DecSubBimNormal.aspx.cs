﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Procesos
{
    public partial class frm_DecSubBimNormal : Page
    {
        private InfoSessionVO goInfo;
        private static string lsTitulo = "";
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        private clConexion lConexion;
        private SqlDataReader lLector;
        private string sRutaArc = ConfigurationManager.AppSettings["rutaCarga"];

        #region Propiedades

        /// <summary>
        /// 
        /// </summary>
        private string HndTipoDec
        {
            get { return ViewState["HndTipoDec"] != null && !string.IsNullOrEmpty(ViewState["HndTipoDec"].ToString()) ? ViewState["HndTipoDec"].ToString() : string.Empty; }
            set { ViewState["HndTipoDec"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        private string HndTipoOpe
        {
            get { return ViewState["HndTipoOpe"] != null && !string.IsNullOrEmpty(ViewState["HndTipoOpe"].ToString()) ? ViewState["HndTipoOpe"].ToString() : string.Empty; }
            set { ViewState["HndTipoOpe"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        private string HndIngDelta
        {
            get { return ViewState["HndIngDelta"] != null && !string.IsNullOrEmpty(ViewState["HndIngDelta"].ToString()) ? ViewState["HndIngDelta"].ToString() : string.Empty; }
            set { ViewState["HndIngDelta"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        private string HndAccion
        {
            get { return ViewState["HndAccion"] != null && !string.IsNullOrEmpty(ViewState["HndAccion"].ToString()) ? ViewState["HndAccion"].ToString() : string.Empty; }
            set { ViewState["HndAccion"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        private string HndRueda
        {
            get { return ViewState["HndRueda"] != null && !string.IsNullOrEmpty(ViewState["HndRueda"].ToString()) ? ViewState["HndRueda"].ToString() : string.Empty; }
            set { ViewState["HndRueda"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        private string HndPozo
        {
            get { return ViewState["HndPozo"] != null && !string.IsNullOrEmpty(ViewState["HndPozo"].ToString()) ? ViewState["HndRueda"].ToString() : string.Empty; }
            set { ViewState["HndPozo"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        private string HndCodigo
        {
            get { return ViewState["HndCodigo"] != null && !string.IsNullOrEmpty(ViewState["HndCodigo"].ToString()) ? ViewState["HndCodigo"].ToString() : string.Empty; }
            set { ViewState["HndCodigo"] = value; }
        }

        #endregion Propiedades

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            var lblMensaje = string.Empty;

            //Titulo
            Master.Titulo = "Subasta";
            lblTitulo.Text = "Declaración de Subasta Bimestral Normal";

            //Se agrega el comportamiento del botón
            //buttons.ExportarExcelOnclick += lkbExcel_Click;
            buttons.Inicializar("t_declaracion_sub_bim1");
            buttons.Load += imbConsultar_Click;
            buttons.CrearOnclick += Nuevo;

            //Botones
            var botones = new List<EnumBotones>();
            EnumBotones[] arreglo = { EnumBotones.Ninguno };
            botones.AddRange(arreglo);

            lConexion = new clConexion(goInfo);

            try
            {
                lsTitulo = "";
                // Carga informacion de combos
                if (Request.QueryString["fase"] != null && Request.QueryString["fase"] != "")
                {
                    Session["fase"] = Request.QueryString["fase"];
                }
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", "estado = 'A'  Order by razon_social", 0, 4);
                LlenarControles(lConexion.gObjConexion, ddlBusOperador, "m_operador", "estado = 'A'  Order by razon_social", 0, 4);
                LlenarControles(lConexion.gObjConexion, ddlBusFuente, "m_pozo", "estado = 'A' and ind_campo_pto ='C' Order by descripcion", 0, 1);
                LlenarControles(lConexion.gObjConexion, ddlBusPozo, "m_pozo", "estado = 'A'  Order by descripcion", 0, 1);
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje = "Problemas en la Carga de la Pagina. " + ex.Message;
            }

            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlBusOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
                ddlBusOperador.Enabled = false;
                //Botones
                arreglo = new[] { EnumBotones.Crear, EnumBotones.Buscar };
                botones.AddRange(arreglo);
            }

            if (IsPostBack) return;

            try
            {
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_ValidaDecSubBim", null, null, null);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lblMensaje = lLector["error"].ToString();
                    HndTipoDec = lLector["tipo_dec"].ToString();
                    HndIngDelta = lLector["ing_delta"].ToString();
                    HndAccion = lLector["accion"].ToString();
                    HndRueda = lLector["rueda"].ToString();
                    HndTipoOpe = lLector["tipo_operador"].ToString();
                    if (lblMensaje != "")
                    {
                        dtgConsulta.Columns[16].Visible = false;
                        //Botones
                        arreglo = new[] { EnumBotones.Ninguno };
                    }
                }
                else
                {
                    //Botones
                    arreglo = new[] { EnumBotones.Ninguno };
                    HndIngDelta = "N";
                    HndRueda = "0";
                    HndTipoOpe = "";
                }
                lLector.Close();
                lLector.Dispose();
                if (HndTipoDec == "P")
                {
                    ddlDeclina.SelectedValue = "S";
                    ddlDeclina.Enabled = false;
                    HndPozo = ddlFuente.SelectedValue;
                    ddlFuente.Items.Clear();
                    LlenarControles(lConexion.gObjConexion, ddlFuente, "m_pozo poz, m_tipo_campo tpo", "poz.estado = 'A' and poz.ind_campo_pto ='C' and poz.campo_declinacion = 'S' and poz.codigo_tipo_campo = tpo.codigo_tipo_campo and tpo.permite_decla_anti_sb ='S' Order by poz.descripcion", 0, 1);
                    try
                    {
                        ddlFuente.SelectedValue = HndPozo;
                    }
                    catch (Exception ex)
                    {
                    }
                    HndPozo = ddlPozo.SelectedValue;
                    ddlPozo.Items.Clear();
                    ddlFuente_SelectedIndexChanged(null, null);
                    try
                    {
                        ddlPozo.SelectedValue = HndPozo;
                    }
                    catch (Exception ex)
                    {
                    }
                    trDelta.Visible = false;
                    dtgConsulta.Columns[2].Visible = false;
                    dtgConsulta.Columns[14].Visible = false;
                }

                if (HndTipoDec == "N")
                {
                    trdeclina.Visible = false;
                    HndPozo = ddlPozo.SelectedValue;
                    ddlPozo.Items.Clear();
                    LlenarControles(lConexion.gObjConexion, ddlPozo, "m_pozo poz", "poz.estado ='A'  Order by poz.descripcion", 0, 1);
                    try
                    {
                        ddlPozo.SelectedValue = HndPozo;
                    }
                    catch (Exception ex)
                    {
                    }
                    HndPozo = ddlFuente.SelectedValue;
                    ddlFuente.Items.Clear();
                    LlenarControles(lConexion.gObjConexion, ddlFuente, "m_pozo poz", "poz.estado = 'A' and poz.ind_campo_pto ='C' Order by poz.descripcion", 0, 1);
                    try
                    {
                        ddlFuente.SelectedValue = HndPozo;
                    }
                    catch (Exception ex)
                    {
                    }

                    if (HndIngDelta == "S")
                    {
                        divFuenteCampo.Visible = HndTipoOpe != "I";
                        trDelta.Visible = true;
                    }
                    else
                    {
                        divFuenteCampo.Visible = false;
                        trdeclina.Visible = false;
                        ddlDeclina.SelectedValue = "N";
                        trDelta.Visible = false;
                    }
                    if (HndAccion == "M")
                        if (arreglo.Contains(EnumBotones.Crear))
                        {
                            var listStr = arreglo.ToList();
                            listStr.Remove(EnumBotones.Crear);
                            arreglo = listStr.ToArray();
                        }
                    dtgConsulta.Columns[2].Visible = true;
                    dtgConsulta.Columns[3].Visible = true;
                    dtgConsulta.Columns[14].Visible = true;
                }
                lConexion.Cerrar();

                if (!string.IsNullOrEmpty(lblMensaje))
                {
                    Toastr.Error(this, lblMensaje);
                }

                //Botones
                buttons.Inicializar(botones: arreglo.ToArray());
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "Problemas en la Carga de la Pagina. " + ex.Message);
            }
        }

        /// <summary>
        /// Nombre: Nuevo
        /// Fecha: Agosto 22 de 2014
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
        /// Modificacion:
        /// </summary>
        protected void Nuevo(object sender, EventArgs e)
        {
            //lConexion.Abrir();
            //lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador ope, m_tipos_operador tpo, m_operador_subasta opeS", " ope.codigo_operador =" + goInfo.cod_comisionista + " and ope.tipo_operador = tpo.sigla and tpo.codigo_tipo_operador = opeS.codigo_tipo_operador and opeS.punta = 'C' And opeS.codigo_tipo_subasta = 1 ");
            //if (!lLector.HasRows)
            //{
            //    lblMensaje = "El tipo de operador no está autorizado para " + lsTitulo + ". <br>";
            //    tblCaptura.Visible = false;
            //    tblgrilla.Visible = true;
            //    tblBuscar.Visible = false;
            //    imbCrear.Visible = false;
            //    imbActualiza.Visible = false;

            //}
            //else
            //{
            imbCrear.Visible = true;
            imbActualiza.Visible = false;
            if (HndTipoDec == "P")
                lblTitulo.Text = "Declaración de Subasta Bimestral Previa";
            else
                lblTitulo.Text = "Declaración de Subasta Bimestral Normal";
            HndCodigo = "0";
            ddlFuente.SelectedValue = "0";
            ddlFuente_SelectedIndexChanged(null, null);
            TxtCantidad.Text = "";
            TxtPrecio.Text = "";
            TxtDelta.Text = "";
            //}
            //lLector.Close();
            //lLector.Dispose();
            //lConexion.Cerrar();
            //Abre el modal de Agregar
            Modal.Abrir(this, registroCapInye.ID, registroCapInyeInside.ID);

        }

        /// <summary>
        /// Nombre: Listar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            CargarDatos();
            if (HndTipoDec == "P")
                lblTitulo.Text = "Declaración de Subasta Bimestral Previa";
            else
                lblTitulo.Text = "Declaración de Subasta Bimestral Normal";
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            var lblMensaje = string.Empty;

            if (modificar != null && modificar != "")
            {
                try
                {
                    lblMensaje = "";
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        ddlOperador.Enabled = false;
                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_declaracion_sub_bim", " codigo_declaracion = " + modificar + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            try
                            {
                                ddlOperador.SelectedValue = lLector["codigo_operador"].ToString();
                            }
                            catch (Exception ex)
                            {
                                lblMensaje += "El operador del registro no existe o esta inactivo<br>";
                            }
                            try
                            {
                                ddlFuente.SelectedValue = lLector["codigo_fuente"].ToString();
                                ddlFuente_SelectedIndexChanged(null, null);
                            }
                            catch (Exception ex)
                            {
                                lblMensaje += "La fuente del registro no existe o esta inactivo<br>";
                            }
                            try
                            {
                                ddlPozo.SelectedValue = lLector["codigo_pozo"].ToString();
                            }
                            catch (Exception ex)
                            {
                                lblMensaje += "EL punto del SNT del registro no existe o esta inactivo<br>";
                            }
                            if (lLector["tipo_declaracion"].ToString() == "P")
                                TxtCantidad.Text = lLector["cantidad_ini_dec"].ToString();
                            else
                                TxtCantidad.Text = lLector["cantidad"].ToString();
                            TxtPrecio.Text = lLector["precio_reserva"].ToString();
                            TxtDelta.Text = lLector["precio_delta"].ToString();
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                            ddlDeclina.Enabled = false;
                            ddlFuente.Enabled = false;
                            ddlPozo.Enabled = false;
                        }

                        lLector.Close();
                        lLector.Dispose();
                        /// Bloquea el Registro a Modificar
                        manejo_bloqueo("A", modificar);
                        //Abre el modal de Agregar
                        Modal.Abrir(this, registroCapInye.ID, registroCapInyeInside.ID);
                    }
                    else
                    {
                        Listar();
                        lblMensaje = "No se Puede editar el Registro por que esta Bloqueado. Cádigo declaración " + modificar;

                    }
                }
                catch (Exception ex)
                {
                    lblMensaje = ex.Message;
                }
            }
            if (lblMensaje == "")
            {
                if (HndTipoDec == "P")
                    lblTitulo.Text = "Declaración de Subasta Bimestral Previa";
                else
                    lblTitulo.Text = "Declaración de Subasta Bimestral Normal";
            }
            if (lblMensaje != "")
            {
                Toastr.Error(this, lblMensaje);
            }
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_codigo_operador", "@P_codigo_fuente", "@P_codigo_pozo" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0" };
            var lblMensaje = string.Empty;

            try
            {
                if (ddlBusOperador.SelectedValue != "0")
                    lValorParametros[0] = ddlBusOperador.SelectedValue;
                if (ddlBusFuente.SelectedValue != "0")
                    lValorParametros[1] = ddlBusFuente.SelectedValue;
                if (ddlBusPozo.SelectedValue != "")
                    lValorParametros[2] = ddlBusPozo.SelectedValue;

                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetDecSubBim", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje = ex.Message;
            }
            if (lblMensaje != "")
            {
                Toastr.Warning(this, lblMensaje);
            }
        }

        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgConsulta.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();

        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
        {
            var lblMensaje = string.Empty;

            if (e.CommandName.Equals("Modificar"))
            {
                try
                {
                    HndCodigo = dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text;
                    Modificar(HndCodigo);
                }
                catch (Exception ex)
                {
                    lblMensaje = "Problemas en la Recuperación del Registro.! " + ex.Message;
                }
            }
            if (e.CommandName.Equals("Eliminar"))
            {
                try
                {
                    string[] lsNombreParametros = { "@P_codigo_declaracion", "@P_numero_rueda", "@P_tipo_declaracion", "@P_accion" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Char, SqlDbType.Char };
                    String[] lValorParametros = { dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text, HndRueda, HndTipoDec, "3" };
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetDecSubBimNormal", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                            lblMensaje = lLector["error"] + "<br>";
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lConexion.Cerrar();
                        Toastr.Success(this, "Registro Eliminado Correctamente.!");

                        Listar();
                    }
                }
                catch (Exception ex)
                {
                    lConexion.Cerrar();
                    lblMensaje = "Problemas en la Eliminación del Registro.! " + ex.Message;
                }
            }
            if (lblMensaje != "")
            {
                Toastr.Error(this, lblMensaje);
            }
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lstable, string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia(lstable, lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
            //dtgComisionista.DataBind();

            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='t_declaracion_sub_bim' and llave_registro='codigo_declaracion=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_declaracion=" + lscodigo_registro;
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_declaracion_sub_bim";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_declaracion_sub_bim", lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlFuente_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (HndTipoDec != "P") return;
            ddlPozo.Items.Clear();
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlPozo, "m_pozo poz, m_fuente_pozo fue", "poz.estado = 'A' and fue.codigo_fuente = " + ddlFuente.SelectedValue + " and fue.codigo_pozo = poz.codigo_pozo Order by descripcion", 0, 1);
            lConexion.Cerrar();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_declaracion", "@P_codigo_operador", "@P_numero_rueda", "@P_tipo_declaracion", "@P_campo_declinacion", "@P_codigo_fuente", "@P_codigo_pozo", "@P_cantidad", "@P_precio_reserva", "@P_delta_precio", "@P_ing_delta", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char, SqlDbType.Char, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Char, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0", "", "", "0", "0", "0", "0", "0", "", "1" };

            string lblMensaje = valida_datos();
            try
            {
                if (lblMensaje == "")
                {
                    lConexion.Abrir();

                    lValorParametros[1] = ddlOperador.SelectedValue;
                    lValorParametros[2] = HndRueda;
                    lValorParametros[3] = HndTipoDec;
                    lValorParametros[4] = ddlDeclina.SelectedValue;
                    if (divFuenteCampo.Visible)
                        lValorParametros[5] = ddlFuente.SelectedValue;
                    else
                        lValorParametros[5] = "0";
                    lValorParametros[6] = ddlPozo.SelectedValue;
                    lValorParametros[7] = TxtCantidad.Text.Trim().Replace(",", "");
                    lValorParametros[8] = TxtPrecio.Text.Trim().Replace(",", "");
                    if (trDelta.Visible)
                        lValorParametros[9] = TxtDelta.Text.Trim().Replace(",", "");
                    else
                        lValorParametros[9] = "0";
                    lValorParametros[10] = HndIngDelta;
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetDecSubBimNormal", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                            lblMensaje += lLector["error"] + "<br>";
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lConexion.Cerrar();
                        TxtCantidad.Text = "";
                        Toastr.Success(this, "Registro Ingresado Correctamente.!");
                        Modal.Cerrar(this, registroCapInye.ID);
                        Listar();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje = "Error en el registro de la declaración." + ex.Message;
            }
            if (lblMensaje != "")
            {
                Toastr.Error(this, lblMensaje);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbActualiza_Click1(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_declaracion", "@P_codigo_operador", "@P_numero_rueda", "@P_tipo_declaracion", "@P_campo_declinacion", "@P_codigo_fuente", "@P_codigo_pozo", "@P_cantidad", "@P_precio_reserva", "@P_delta_precio", "@P_ing_delta", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char, SqlDbType.Char, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Char, SqlDbType.Int };
            String[] lValorParametros = { "0", "0", "0", "", "", "0", "0", "0", "0", "0", "", "2" };

            string lblMensaje = valida_datos();

            try
            {
                if (lblMensaje == "")
                {
                    lConexion.Abrir();
                    lValorParametros[0] = HndCodigo;
                    lValorParametros[1] = ddlOperador.SelectedValue;
                    lValorParametros[2] = HndRueda;
                    lValorParametros[3] = HndTipoDec;
                    lValorParametros[4] = ddlDeclina.SelectedValue;
                    if (divFuenteCampo.Visible)
                        lValorParametros[5] = ddlFuente.SelectedValue;
                    else
                        lValorParametros[5] = "0";
                    lValorParametros[6] = ddlPozo.SelectedValue;
                    lValorParametros[7] = TxtCantidad.Text.Trim().Replace(",", "");
                    lValorParametros[8] = TxtPrecio.Text.Trim().Replace(",", "");
                    if (trDelta.Visible)
                        lValorParametros[9] = TxtDelta.Text.Trim().Replace(",", "");
                    else
                        lValorParametros[9] = "0";
                    lValorParametros[10] = HndIngDelta;
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetDecSubBimNormal", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                            lblMensaje = lLector["error"] + "<br>";
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lConexion.Cerrar();
                        if (HndCodigo != "")
                            manejo_bloqueo("E", HndCodigo);
                        Toastr.Success(this, "Registro Ingresado Correctamente.!");
                        Modal.Cerrar(this, registroCapInye.ID);
                        Listar();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje = ex.Message;
            }
            if (lblMensaje != "")
            {
                Toastr.Warning(this, lblMensaje);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancerlar_Click1(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (HndCodigo != "")
                manejo_bloqueo("E", HndCodigo);
            Listar();
            Modal.Cerrar(this, registroCapInye.ID);
        }

        protected string valida_datos()
        {
            var lblMensaje = new StringBuilder();
            int liCantidad;
            double oValor1 = 0;
            if (divFuenteCampo.Visible)
                if (ddlFuente.SelectedValue == "0")
                {
                    lblMensaje.Append("Debe seleccionar la fuente.<br>");
                }

            if (ddlPozo.SelectedValue == "0")
            {
                lblMensaje.Append("Debe seleccionar el punto del SNT<br>");
            }

            if (ddlPozo.Enabled)
                if (VerificarExistencia("t_declaracion_sub_bim dec, m_parametros_sub_bim prm", "prm.año_dec_prev_nv = dec.año_declaracion and dec.codigo_operador = " + ddlOperador.SelectedValue + " and dec.codigo_fuente =" + ddlFuente.SelectedValue + " and dec.codigo_pozo=" + ddlPozo.SelectedValue + " and dec.numero_rueda =" + HndRueda + " and codigo_declaracion <> " + HndCodigo))  //20170714 ajuste declaracion anticipada
                {
                    lblMensaje.Append("Ya se registró la información para el rueda-punto seleccionado.<br>");
                }
            if (TxtCantidad.Text != "")
            {
                try
                {
                    if (Convert.ToInt32(TxtCantidad.Text.Replace(",", "")) < 0)  //20180726 ajuste
                    {
                        lblMensaje.Append("La cantidad no puede ser menor que cero.<br>");
                    }

                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Valor Inválido en la Cantidad disponible.<br>");
                }
            }
            else
            {
                lblMensaje.Append("Debe ingresar la cantidad.<br>");
            }

            if (TxtPrecio.Text.Trim().Length > 0)
            {
                string sOferta = TxtPrecio.Text.Trim().Replace(",", "");
                int iPos = sOferta.IndexOf(".");
                if (iPos > 0)
                    if (sOferta.Length - iPos > 3)
                    {
                        lblMensaje.Append("Se permiten máximo 2 decimales en el precio de reserva.<br>");
                    }
                try
                {
                    if (Convert.ToDouble(TxtPrecio.Text.Replace(",", "")) < 0) //20180726 ajuste
                    {
                        lblMensaje.Append("El precio de reserva no puede ser menor que cero.<br>");
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append("Valor Inválido en el Precio de reserva.<br>");
                }
            }
            else
            {
                lblMensaje.Append("Debe Ingresar el Precio de reserva.<br>");
            }

            if (trDelta.Visible)
            {
                if (TxtDelta.Text.Trim().Length > 0)
                {
                    string sOferta = TxtDelta.Text.Trim().Replace(",", "");
                    int iPos = sOferta.IndexOf(".");
                    if (iPos > 0)
                        if (sOferta.Length - iPos > 3)
                        {
                            lblMensaje.Append("Se permiten máximo 2 decimales en el delta del precio.<br>");
                        }
                    try
                    {
                        if (Convert.ToDouble(TxtDelta.Text.Replace(",", "")) < 0) //20180726 ajuste
                        {
                            lblMensaje.Append("El delta del Precio no puede ser menor que cero.<br>");
                        }

                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Append("Valor Inválido en el delta del Precio.<br>");
                    }
                }
                else
                {
                    lblMensaje.Append("Debe Ingresar el Precio de reserva.<br>");
                }
            }

            return lblMensaje.ToString();
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbConsultar_Click(object sender, EventArgs e)
        {
            lblTitulo.Text = HndTipoDec == "P" ? "Declaración de Subasta Bimestral Previa" : "Declaración de Subasta Bimestral Normal";
            Listar();
        }
    }
}