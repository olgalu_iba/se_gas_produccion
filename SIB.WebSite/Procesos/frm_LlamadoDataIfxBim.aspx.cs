﻿using System;
using System.Web;
using System.Web.UI;
using SIB.Global.Presentacion;
using ConsumoRest;
using Segas.Web.Elements;

public partial class Procesos_frm_LlamadoDataIfxBim : System.Web.UI.Page
{
    static InfoSessionVO goInfo = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");

        try
        {
            /// LLamado al Servicio de DataIfx
            var client = new RestClient();
            client.EndPoint = HttpContext.Current.Session["UrlC1Bim"].ToString().Trim();
            client.Method = HttpVerb.POST;
            string lsData = "{\"idUser\":\"" + goInfo.codigo_usuario.ToString() + "\",\"UserName\":\"" + goInfo.Usuario + "\",\"Password\":\"" + goInfo.Password + "\"}";
            client.PostData = lsData;
            client.ContentType = "application/json";
            var json = client.MakeRequest();
            json = json.ToString().Replace("\"", "").Replace("\\", "");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + json + "');", true);
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "Error Llamado Subasta Bimestral C1 y C2" + ex.Message );
         

        }
    }
}