﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_AsignaProyectoPag.aspx.cs"
    Inherits="Procesos_frm_AsignaProyectoPag" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" Text="Asigancion de contratos PAG" />
                    </h3>
                </div>

            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body" runat="server">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="imbCrear" runat="server" CssClass="btn btn-primary" Text="Crear" OnClick="imbCrear_Click1" ValidationGroup="Asignar" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="imbExcel" runat="server" CssClass="btn btn-primary" Text="Excel" OnClick="imbExcel_Click1" ValidationGroup="Asignar" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        </div>
                    </div>

                </div>
                <%--Grilla--%>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" 
                                Width="100%" CssClass="table-bordered">
                                <Columns>
                                    <asp:BoundColumn DataField="numero_contrato" HeaderText="Operación"
                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="operador_venta" HeaderText="Código Venta" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Venta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="operador_compra" HeaderText="Códogo Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombe Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_ini" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_fin" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:##0.00}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_max_registro" HeaderText="Fecha Máxima Registro" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:yyyy/MM/dd HH:mm}"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
