﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_AdjudicaPtdvfRes.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_AdjudicaPtdvfRes" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript">
        //        document.location = no;
        function confirmar() {
            return confirm("Está seguro de realizar el proceso de Adjudicación?.\nSi ya se han adjudicado contratos, estos se borrarán y quedarán en estado pendiente de registro.")
        }
    </script>

    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">

                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="BtnAdjudicar" runat="server" Text="Adjudicar" OnClientClick="return confirmar();" OnClick="BtnAdjudicar_Click" />

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Perfil Contingencia</label>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="detalle" />
                            <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Operador Contingencia</label>
                            <asp:DropDownList ID="DdlOperador" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Tiempo Duración Contingencia (minutos)</label>
                            <asp:TextBox ID="TxtTiempo" runat="server" MaxLength="10"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RfvTxtTiempo" runat="server" ErrorMessage="Debe Ingresar el Tiempo de Duración de la Contingencia"
                                ControlToValidate="TxtTiempo" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                         <%--   <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                                ValidationGroup="comi" Height="40" />--%>
                          <%--  <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                                CausesValidation="false" Height="40" />--%>
                            <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />

                        </div>
                    </div>
                    <div runat="server"
                        id="tblBuscar" visible="false">
                        <label for="Name">Perfil Contingencia</label>

                        <asp:DropDownList ID="DdlBusPerfil" runat="server">
                        </asp:DropDownList>

                        <label for="Name">Operador Contingencia</label>

                        <asp:DropDownList ID="DdlBusOperador" runat="server">
                        </asp:DropDownList>

                     <%--   <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                            OnClick="imbConsultar_Click" Height="40" />--%>
                        <asp:Label ID="Label2" runat="server" ForeColor="Red"></asp:Label>

                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <div runat="server"
                                id="tblGrilla" visible="false">

                                <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                                    <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                        PagerStyle-HorizontalAlign="Center">

                                        <Columns>
                                            <asp:BoundColumn DataField="numero_contrato" HeaderText="Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="operador_venta" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="nombre_venta" HeaderText="Nombre Vendedor" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="operador_compra" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="nombre_compra" HeaderText="Nombre Comprador" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="codigo_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="desc_fuente" HeaderText="Nombre Fuente" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="center"
                                                DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="fecha_max_registro" HeaderText="Fecha Máxima Registro"
                                                ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                        </Columns>
                                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                    </asp:DataGrid>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>

            </div>

        </div>
    </div>


</asp:Content>
