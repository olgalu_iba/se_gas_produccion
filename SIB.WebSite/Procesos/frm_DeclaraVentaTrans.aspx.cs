﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Procesos
{
    public partial class frm_DeclaraVentaTrans : Page
    {
        private InfoSessionVO goInfo;
        private static string lsTitulo = "Declaración de Oferta – Subasta de transporte ante congestión contractual"; //20220211 ajsute lables
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        clConexion lConexion = null;
        private SqlDataReader lLector;
        private string gsTabla = "t_declaracion_sub_tra";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lConexion = new clConexion(goInfo);
            lblTitulo.Text = lsTitulo;
            //Titulo
            Master.Titulo = "Subasta";
            /// Llamo el procedimiento para la validacion de la Fecha válida para declaración
            if (ValidarDeclaracion())
            {
                //Activacion de los Botones
                //buttons.Inicializar(ruta: gsTabla);
                Inicializar();
                buttons.CrearOnclick += btnNuevo;
                buttons.AprobarOnclick += btnAprobar_Click;
                buttons.DesaprobarOnclick += btnDesAprobar_Click;
                buttons.FiltrarOnclick += btnConsultar_Click;
                buttons.ExportarExcelOnclick += ImgExcel_Click;
                buttons.SalirOnclick += imbSalir_Click;

                if (IsPostBack) return;
                //Establese los permisos del sistema
                EstablecerPermisosSistema();
                // Carga informacion de combos
                lConexion.Abrir();
                LlenarControles1(lConexion.gObjConexion, ddlOperador, "m_operador", " tipo_operador ='T' and estado ='A' order by razon_social", 0, 4);
                LlenarControles1(lConexion.gObjConexion, ddlBusOperador, "m_operador", " tipo_operador ='T' and estado ='A' order by razon_social", 0, 4);
                LlenarControles(lConexion.gObjConexion, ddlTrimestre, "m_trimestre", " 1=1 Order by numero_trimestre ", 0, 1);
                LlenarControles(lConexion.gObjConexion, ddlBusTrimestre, "m_trimestre", " 1=1 Order by numero_trimestre ", 0, 1);
                lConexion.Cerrar();

                if (Session["tipoPerfil"].ToString() == "N")
                {

                    ddlOperador.SelectedValue = goInfo.cod_comisionista;
                    ddlBusOperador.SelectedValue = goInfo.cod_comisionista;
                    ddlOperador.Enabled = false;
                    ddlBusOperador.Enabled = false;
                    lConexion.Abrir();
                    LlenarTramo(lConexion.gObjConexion, ddlTramo);
                    LlenarTramo(lConexion.gObjConexion, ddlBusTramo);
                    lConexion.Cerrar();
                }
                else
                {
                    dtgMaestro.Columns[10].Visible = false;
                    lConexion.Abrir();
                    LlenarTramo(lConexion.gObjConexion, ddlBusTramo);
                    lConexion.Cerrar();
                }

                Inicializar();
                Listar();
            }
            else
            {
                lConexion.Abrir();
                LlenarControles1(lConexion.gObjConexion, ddlBusOperador, "m_operador", "  codigo_operador = -1", 0, 4);
                LlenarControles(lConexion.gObjConexion, ddlBusTramo, "m_tramo", "codigo_tramo = -1 ", 0, 1);
                LlenarControles(lConexion.gObjConexion, ddlBusTrimestre, "m_trimestre", "1= -1 ", 0, 1);
                lConexion.Cerrar();

            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void Inicializar()
        {
            EnumBotones[] botones = { };
            if (Session["tipoPerfil"].ToString() == "N")
                botones = new[] { EnumBotones.Crear, EnumBotones.Buscar, EnumBotones.Excel, EnumBotones.Salir, EnumBotones.Aprobar, EnumBotones.Desaprobar };
            else
                botones = new[] { EnumBotones.Buscar, EnumBotones.Excel, EnumBotones.Salir};
            // Activacion de los Botones
            buttons.Inicializar( botones: botones);
        }
        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Enero 26 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            var permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, gsTabla);
            foreach (DataGridItem Grilla in dtgMaestro.Items)
            {
                var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                lkbModificar.Visible = (bool)permisos["UPDATE"];
                var lkbEliminar = (LinkButton)Grilla.FindControl("lnkEliminar");
                lkbEliminar.Visible = (bool)permisos["DELETE"];

                if (!lkbModificar.Visible && !lkbEliminar.Visible)
                    dtgMaestro.Columns[10].Visible = false;
            }
        }
        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Enero 27 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Realizar las validaciones de la fecha valida para habilitacion
        /// Modificacion:
        /// </summary>
        private bool ValidarDeclaracion()
        {
            bool blPuede = false;
            try
            {
                hdfTrimestre.Value = "0";
                lConexion.Abrir();
                var retorno = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "pa_ValidaDeclaraVtaTrans");
                lConexion.Cerrar();
                if (retorno.Count > 0)
                {
                    foreach (DataRowView row in retorno)
                    {
                        if (row["Mensaje"].ToString().Trim() == "")
                        {
                            hndAñoNeg.Value = row["AñoNego"].ToString().Trim();
                            hndTrimNeg.Value = row["TrimestreNego"].ToString().Trim();
                            blPuede = true;
                        }
                        else
                        {
                            Toastr.Error(this, row["Mensaje"].ToString().Trim());
                            blPuede = false;
                        }
                    }
                    return blPuede;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
                return false;
            }
        }
        /// <summary>
        /// Nombre: Nuevo
        /// Fecha: Enero 26 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
        /// Modificacion:
        /// </summary>
        private void Nuevo()
        {
            Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
            imbCrear.Visible = true;
            imbActualiza.Visible = false;
            lblTitulo.Text = lsTitulo;
            TxtCodigoCos.Visible = false;
            LblCodigoCos.Visible = true;
            LblCodigoCos.Text = "Automático";
            imbActualiza.Visible = false;
            imbCrear.Visible = true;
            TxtAño.Enabled = true;
            ddlTrimestre.Enabled = true;
            ddlTramo.Enabled = true;
            ddlOperador.Enabled = false;
        }

        /// <summary>
        /// Nombre: Listar
        /// Fecha: Enero 26 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            CargarDatos();
            dtgMaestro.Visible = true;
            lblTitulo.Text = lsTitulo;
            EstablecerPermisosSistema();
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Enero 26 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            var lblMensaje = new StringBuilder();

            if (modificar != null && modificar != "")
            {
                try
                {
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", gsTabla, " codigo_declaracion = " + modificar + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            LblCodigoCos.Text = lLector["codigo_declaracion"].ToString();
                            TxtCodigoCos.Text = lLector["codigo_declaracion"].ToString();
                            try
                            {
                                ddlTramo.SelectedValue = lLector["codigo_tramo"].ToString();
                            }
                            catch (Exception)
                            {
                                lblMensaje.Append("El tramo del registro no existe o está inactivo<br>");
                            }
                            try
                            {
                                ddlTrimestre.SelectedValue = lLector["trimestre_entrega"].ToString();
                            }
                            catch (Exception)
                            {
                                lblMensaje.Append("El Trimestre del registro no existe o está inactivo<br>");
                            }
                            TxtAño.Text = lLector["año_entrega"].ToString();
                            TxtCapacidad.Text = lLector["capacidad"].ToString();
                            ddlEstado.SelectedValue = lLector["estado"].ToString();
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                            TxtCodigoCos.Visible = false;
                            LblCodigoCos.Visible = true;
                            ddlOperador.Enabled = false;
                            ddlTrimestre.Enabled = false;
                            ddlTramo.Enabled = false;
                            TxtAño.Enabled = false;

                            Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Bloquea el Registro a Modificar
                        manejo_bloqueo("A", modificar);
                    }
                    else
                    {
                        Listar();
                        lblMensaje.Append("No se Puede editar el Registro por que está Bloqueado. Código registro " + modificar);
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append(ex.Message);
                }
            }
            if (lblMensaje.ToString() == "")
            {
                Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
                lblTitulo.Text = lsTitulo;
            }
            else
            {
                Toastr.Error(this, lblMensaje.ToString());
            }
        }

        /// <summary>
        /// Nombre: Buscar
        /// Fecha: Enero 27 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
        /// Modificacion:
        /// </summary>
        private void Buscar()
        {
            Modal.Cerrar(this, CrearRegistro.ID);
            lblTitulo.Text = lsTitulo;
        }
        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Enero 27 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_año_negociacion", "@P_trimestre_negociacion", "@P_codigo_operador", "@P_codigo_tramo", "@P_año_entrega", "@P_trimestre_entrega" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { hndAñoNeg.Value, hndTrimNeg.Value, "0", "0", "0", "0" };

            try
            {

                if (ddlBusOperador.SelectedValue != "0")
                    lValorParametros[2] = ddlBusOperador.SelectedValue;
                if (ddlBusTramo.SelectedValue != "0")
                    lValorParametros[3] = ddlBusTramo.SelectedValue;
                if (TxtBusAño.Text.Trim().Length > 0)
                    lValorParametros[4] = TxtBusAño.Text.Trim();
                if (ddlBusTrimestre.SelectedValue != "0")
                    lValorParametros[5] = ddlBusTrimestre.SelectedValue;

                lConexion.Abrir();
                dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetDeclaraVtaTrans", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgMaestro.DataBind();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }
        /// <summary>
        /// Nombre: btnConsultar_Click
        /// Fecha: Enero 27 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            Listar();
        }

        /// <summary>
        /// Nombre: Validaciones
        /// Fecha: Enero 27 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para validar campos de Entrada
        ///              en el Boton Crear.
        /// </summary>
        /// <returns></returns>
        protected StringBuilder Validaciones(string indicador)
        {
            var lblMensaje = new StringBuilder();
            try
            {
                decimal ldValor = 0;
                if (ddlOperador.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el Operador<br>");
                if (ddlTramo.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el tramo<br>");
                if (ddlTrimestre.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el trimestre<br>");
                if (TxtAño.Text.Trim() != "")
                {
                    try
                    {
                        ldValor = Convert.ToDecimal(TxtAño.Text.Trim());
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append("Valor Inválido en el Año de entrega <br>");
                    }
                }
                else
                    lblMensaje.Append("Debe Ingresar el Año de entrega<br>");
                if (TxtCapacidad.Text.Trim() != "")
                {
                    try
                    {
                        ldValor = Convert.ToInt32(TxtCapacidad.Text.Trim());
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append("Valor Inválido en la capacidad<br>");
                    }
                }
                else
                    lblMensaje.Append("Debe Ingresar la capacidad<br>");
                return lblMensaje;
            }
            catch (Exception ex)
            {
                lblMensaje.Append("Error en las Validaciones de la Información. " + ex.Message.ToString());
                return lblMensaje;
            }
        }
        /// <summary>
        /// Nombre: imbCrear_Click
        /// Fecha: Enero 27 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Crear.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_declaracion", "@P_año_negociacion", "@P_trimestre_negociacion", "@P_codigo_operador", "@P_codigo_tramo", "@P_año_entrega", "@P_trimestre_entrega", "@P_capacidad", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0", "0", "0", "0", "0", "0", "0", "1" };
            var lblMensaje = new StringBuilder();
            try
            {
                lblMensaje = Validaciones("C");
                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[1] = hndAñoNeg.Value;
                    lValorParametros[2] = hndTrimNeg.Value;
                    lValorParametros[3] = ddlOperador.SelectedValue;
                    lValorParametros[4] = ddlTramo.SelectedValue;
                    lValorParametros[5] = TxtAño.Text;
                    lValorParametros[6] = ddlTrimestre.SelectedValue;
                    lValorParametros[7] = TxtCapacidad.Text;
                    lValorParametros[8] = ddlEstado.SelectedValue;


                    var lComando = new SqlCommand();
                    int liNumeroParametros;
                    lConexion.Abrir();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_SetDeclaraVtaTrans";
                    lComando.CommandTimeout = 3600;
                    if (lsNombreParametros != null)
                    {
                        for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametros.Length - 1; liNumeroParametros++)
                        {
                            lComando.Parameters.Add(lsNombreParametros[liNumeroParametros], lTipoparametros[liNumeroParametros]).Value = lValorParametros[liNumeroParametros];
                        }
                    }
                    lLector = lComando.ExecuteReader();
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                        {
                            lblMensaje.Append(lLector["error"].ToString() + "<br>");
                        }
                    }
                    else
                    {
                        lLector.Close();
                        lConexion.Cerrar();
                        Modal.Cerrar(this, CrearRegistro.ID); //20200727
                        limpiarCampos();
                        Toastr.Success(this, "Se creo correctamente la declaración");
                        Listar();
                    }

                    //lConexion.Abrir();
                    //lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetDeclaraVtaTrans", lsNombreParametros, lTipoparametros, lValorParametros);
                    //if (lLector.HasRows)
                    //{
                    //    while (lLector.Read())
                    //        lblMensaje.Append(lLector["error"].ToString() + "<br>");
                    //}
                    //else
                    //{
                    //    lLector.Close();
                    //    lConexion.Cerrar();
                    //    Modal.Cerrar(this, CrearRegistro.ID); //20200727
                    //    limpiarCampos();
                    //    Toastr.Success(this, "Se creo correctamente la declaración");
                    //    Listar();
                    //}
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    Toastr.Error(this, lblMensaje.ToString());
            }
            catch (Exception ex)
            {
                lLector.Close();
                lConexion.Cerrar();
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: imbActualiza_Click
        /// Fecha: Enero 27 de 2021
        /// Creador: German Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Actualizar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbActualiza_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_declaracion", "@P_año_negociacion", "@P_trimestre_negociacion", "@P_codigo_operador", "@P_codigo_tramo", "@P_año_entrega", "@P_trimestre_entrega", "@P_capacidad", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0", "0", "0", "0", "0", "0", "0", "2" };
            var lblMensaje = new StringBuilder();
            try
            {
                lblMensaje = Validaciones("M");
                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[0] = LblCodigoCos.Text;
                    lValorParametros[1] = hndAñoNeg.Value;
                    lValorParametros[2] = hndTrimNeg.Value;
                    lValorParametros[3] = ddlOperador.SelectedValue;
                    lValorParametros[4] = ddlTramo.SelectedValue;
                    lValorParametros[5] = TxtAño.Text;
                    lValorParametros[6] = ddlTrimestre.SelectedValue;
                    lValorParametros[7] = TxtCapacidad.Text;
                    lValorParametros[8] = ddlEstado.SelectedValue;


                    var lComando = new SqlCommand();
                    int liNumeroParametros;
                    lConexion.Abrir();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_SetDeclaraVtaTrans";
                    lComando.CommandTimeout = 3600;
                    if (lsNombreParametros != null)
                    {
                        for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametros.Length - 1; liNumeroParametros++)
                        {
                            lComando.Parameters.Add(lsNombreParametros[liNumeroParametros], lTipoparametros[liNumeroParametros]).Value = lValorParametros[liNumeroParametros];
                        }
                    }
                    lLector = lComando.ExecuteReader();
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                        {
                            lblMensaje.Append(lLector["error"].ToString() + "<br>");
                        }
                    }
                    else
                    {
                        lLector.Close();
                        lConexion.Cerrar();
                        manejo_bloqueo("E", LblCodigoCos.Text);
                        Modal.Cerrar(this, CrearRegistro.ID); //20200727
                        limpiarCampos();
                        Toastr.Success(this, "Se modificó correctamente la declaración");
                        Listar();
                    }

                    //lConexion.Abrir();
                    //lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetDeclaraVtaTrans", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    //if (lLector.HasRows)
                    //{
                    //    while (lLector.Read())
                    //    {
                    //        lblMensaje.Append(lLector["error"].ToString());
                    //    }
                    //    lLector.Close();
                    //    lConexion.Cerrar();
                    //}
                    //else
                    //{
                    //    lLector.Close();
                    //    lConexion.Cerrar();
                    //    Modal.Cerrar(this, CrearRegistro.ID); //20200727
                    //    limpiarCampos();
                    //    Toastr.Success(this, "Se creo correctamente la declaración");
                    //    Listar();
                    //}
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    Toastr.Error(this, lblMensaje.ToString());
            }
            catch (Exception ex)
            {
                lLector.Close();
                lConexion.Cerrar();
                Toastr.Error(this, ex.Message);
            }
        }
        /// <summary>
        /// Nombre: btnConsultar_Click
        /// Fecha: Enero 27 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAprobar_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_año_negociacion", "@P_trimestre_negociacion", "@P_codigo_operador", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0", "4" };
            var lblMensaje = new StringBuilder();
            try
            {
                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[0] = hndAñoNeg.Value;
                    lValorParametros[1] = hndTrimNeg.Value;
                    lValorParametros[2] = ddlOperador.SelectedValue;

                    var lComando = new SqlCommand();
                    int liNumeroParametros;
                    lConexion.Abrir();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_SetDeclaraVtaTrans";
                    lComando.CommandTimeout = 3600;
                    if (lsNombreParametros != null)
                    {
                        for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametros.Length - 1; liNumeroParametros++)
                        {
                            lComando.Parameters.Add(lsNombreParametros[liNumeroParametros], lTipoparametros[liNumeroParametros]).Value = lValorParametros[liNumeroParametros];
                        }
                    }
                    lLector = lComando.ExecuteReader();
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                        {
                            lblMensaje.Append(lLector["error"].ToString() + "<br>");
                        }
                    }
                    else
                    {
                        lLector.Close();
                        lConexion.Cerrar();
                        Toastr.Success(this, "Se aprobó correctamente la declaración");
                        Listar();
                    }

                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    Toastr.Error(this, lblMensaje.ToString());
            }
            catch (Exception ex)
            {
                lLector.Close();
                lConexion.Cerrar();
                Toastr.Error(this, ex.Message);
            }


        }
        /// <summary>
        /// Nombre: btnConsultar_Click
        /// Fecha: Enero 27 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDesAprobar_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_año_negociacion", "@P_trimestre_negociacion", "@P_codigo_operador", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0", "5" };
            var lblMensaje = new StringBuilder();
            try
            {
                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[0] = hndAñoNeg.Value;
                    lValorParametros[1] = hndTrimNeg.Value;
                    lValorParametros[2] = ddlOperador.SelectedValue;

                    var lComando = new SqlCommand();
                    int liNumeroParametros;
                    lConexion.Abrir();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_SetDeclaraVtaTrans";
                    lComando.CommandTimeout = 3600;
                    if (lsNombreParametros != null)
                    {
                        for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametros.Length - 1; liNumeroParametros++)
                        {
                            lComando.Parameters.Add(lsNombreParametros[liNumeroParametros], lTipoparametros[liNumeroParametros]).Value = lValorParametros[liNumeroParametros];
                        }
                    }
                    lLector = lComando.ExecuteReader();
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                        {
                            lblMensaje.Append(lLector["error"].ToString() + "<br>");
                        }
                    }
                    else
                    {
                        lLector.Close();
                        lConexion.Cerrar();
                        Toastr.Success(this, "Se desaprobó correctamente la declaración");
                        Listar();
                    }

                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    Toastr.Error(this, lblMensaje.ToString());
            }
            catch (Exception ex)
            {
                lLector.Close();
                lConexion.Cerrar();
                Toastr.Error(this, ex.Message);
            }


        }

        /// <summary>
        /// Nombre: imbSalir_Click
        /// Fecha: Enero 22 de 2021
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
        ///              en el Boton Salir.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbSalir_Click(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (LblCodigoCos.Text != "")
                manejo_bloqueo("E", LblCodigoCos.Text);
            //Cierra el modal de Agregar
            //Modal.Cerrar(this, CrearRegistro.ID);
            //Listar();
            Response.Redirect("../WebForms/Home.aspx");
        }
        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Enero 22 de 2021
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Enero 22 de 2021
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            string lCodigoRegistro = "";
            if (e.CommandName == "Modificar")
            {
                if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[9].Text == "C")
                {
                    lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                    Modificar(lCodigoRegistro);
                    imbActualiza.Visible = true;
                    imbCrear.Visible = false;
                }
                else
                    Toastr.Error(this, "El registro no se puede modificar porque ya se aprobó");
            }
            if (e.CommandName == "Eliminar")
            {
                if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[9].Text == "C")
                {
                    string[] lsNombreParametros = { "@P_codigo_declaracion", "@P_año_negociacion", "@P_trimestre_negociacion", "@P_codigo_operador", "@P_accion" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                    string[] lValorParametros = { "0", "0", "0", "0", "3" };
                    var lblMensaje = new StringBuilder();
                    try
                    {
                        if (lblMensaje.ToString() == "")
                        {
                            lValorParametros[0] = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                            lValorParametros[1] = hndAñoNeg.Value;
                            lValorParametros[2] = hndTrimNeg.Value;
                            lValorParametros[3] = ddlOperador.SelectedValue;

                            var lComando = new SqlCommand();
                            int liNumeroParametros;
                            lConexion.Abrir();
                            lComando.Connection = lConexion.gObjConexion;
                            lComando.CommandType = CommandType.StoredProcedure;
                            lComando.CommandText = "pa_SetDeclaraVtaTrans";
                            lComando.CommandTimeout = 3600;
                            if (lsNombreParametros != null)
                            {
                                for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametros.Length - 1; liNumeroParametros++)
                                {
                                    lComando.Parameters.Add(lsNombreParametros[liNumeroParametros], lTipoparametros[liNumeroParametros]).Value = lValorParametros[liNumeroParametros];
                                }
                            }
                            lLector = lComando.ExecuteReader();
                            if (lLector.HasRows)
                            {
                                while (lLector.Read())
                                {
                                    lblMensaje.Append(lLector["error"].ToString() + "<br>");
                                }
                            }
                            else
                            {
                                lLector.Close();
                                lConexion.Cerrar();
                                Toastr.Success(this, "Se eliminó correctamente la declaración");
                                Listar();
                            }

                        }

                        if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                            Toastr.Error(this, lblMensaje.ToString());
                    }
                    catch (Exception ex)
                    {
                        lLector.Close();
                        lConexion.Cerrar();
                        Toastr.Error(this, ex.Message);
                    }


                }
                else
                    Toastr.Error(this, "El registro no se puede eliminar porque ya se aprobó");
            }
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Enero 22 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia(gsTabla, lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Enero 22 de 2021
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='" + gsTabla + "' and llave_registro='codigo_declaracion=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_declaracion=" + lscodigo_registro.ToString();
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = gsTabla;
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, gsTabla, lsCondicion1);
            }
            return true;
        }
        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Enero 22 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_año_negociacion", "@P_trimestre_negociacion", "@P_codigo_operador", "@P_codigo_tramo", "@P_año_entrega", "@P_trimestre_entrega" };
            string[] lValorParametros = { "0", "0", "0", "0", "0", "0" };
            string lsParametros = "";

            try
            {
                lValorParametros[0] = hndAñoNeg.Value;
                lValorParametros[1] = hndTrimNeg.Value;
                if (ddlBusOperador.SelectedValue != "0")
                {
                    lValorParametros[2] = ddlBusOperador.SelectedValue;
                    lsParametros += " Operador: " + ddlBusOperador.SelectedItem.ToString();
                }
                if (ddlBusTramo.SelectedValue != "0")
                {
                    lValorParametros[3] = ddlBusTramo.SelectedValue;
                    lsParametros += " Tramo: " + ddlBusTramo.SelectedItem.ToString();
                }
                if (TxtBusAño.Text != "")
                {
                    lValorParametros[4] = TxtBusAño.Text;
                    lsParametros += " Año Entrega: " + TxtBusAño.Text;
                }
                if (ddlBusTrimestre.SelectedValue != "0")
                {
                    lValorParametros[5] = ddlBusTrimestre.SelectedValue;
                    lsParametros += " Trimestre: " + ddlBusTrimestre.SelectedItem.ToString();
                }
                Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetDeclaraVtaTrans&nombreParametros=@P_año_negociacion*@P_trimestre_negociacion*@P_codigo_operador*@P_codigo_tramo*@P_año_entrega*@P_trimestre_entrega&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "*" + lValorParametros[3] + "*" + lValorParametros[4] + "*" + lValorParametros[5] + "&columnas=codigo_declaracion*año_negociacion*trimestre_negociacion*nombre_trimestre*codigo_operador*nombre_operador*capacidad*estado&titulo_informe=Listado de declaración subasta de transporte&TituloParametros=" + lsParametros);
            }
            catch (Exception)
            {
                Toastr.Error(this, "No se Pudo Generar el Informe.!");
            }
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Enero 22 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// </summary>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// <param name="lsTabla"></param>
        /// <param name="lsCondicion"></param>
        /// <param name="liIndiceLlave"></param>
        /// <param name="liIndiceDescripcion"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, int liIndiceLlave, int liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);

            }
            lLector.Dispose();
            lLector.Close();
        }
        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Enero 22 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// </summary>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// <param name="lsTabla"></param>
        /// <param name="lsCondicion"></param>
        /// <param name="liIndiceLlave"></param>
        /// <param name="liIndiceDescripcion"></param>
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, int liIndiceLlave, int liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();

                lDdl.Items.Add(lItem1);

            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarTramo(SqlConnection lConn, DropDownList lDdl)
        {
            lDdl.Items.Clear();
            string[] lsNombreParametros = { "@P_codigo_trasportador" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int };
            string[] lValorParametros = { "0" };

            if (ddlOperador.SelectedValue != "")
                lValorParametros[0] = ddlOperador.SelectedValue;

            SqlDataReader lLector;
            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConn, "pa_GetTramo", lsNombreParametros, lTipoparametros, lValorParametros);
            if (lLector.HasRows)
            {
                while (lLector.Read())
                {

                    ListItem lItem1 = new ListItem();
                    lItem1.Value = lLector.GetValue(0).ToString();
                    lItem1.Text = lLector["desc_tramo"].ToString();
                    lDdl.Items.Add(lItem1);
                }
            }
            lLector.Close();
        }


        ///// Eventos Nuevos para la Implementracion del UserControl

        /// <summary>
        /// Metodo del Link Nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNuevo(object sender, EventArgs e)
        {
            Nuevo();
        }

        /// <summary>
        /// Metodo del Link Listar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsulta(object sender, EventArgs e)
        {
            Listar();
        }
        /// <summary>
        /// Nombre: limpiarCampos
        /// Fecha: Enero 21 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Metodo para limpiar campos
        /// </summary>
        protected void limpiarCampos()
        {
            ddlTramo.SelectedValue = "0";
            TxtAño.Text = "";
            ddlTrimestre.SelectedValue = "0";
            TxtCapacidad.Text = "";
        }
        /// <summary>
        /// Nombre: imbCancelar_Click
        /// Fecha: Enero 25 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Metodo para Cancelar la creacion o modificacion de registros
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCancelar_Click(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (LblCodigoCos.Text != "")
                manejo_bloqueo("E", LblCodigoCos.Text);
            //Cierra el modal de Agregar
            Modal.Cerrar(this, CrearRegistro.ID);
            Listar();
        }
    }
}