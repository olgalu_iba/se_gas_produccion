﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using SIB.BASE;
using System.IO;

public partial class Procesos_frm_FinalizaSubastaND : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    SqlDataReader lLector;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            lblTitulo.Text = "Finalización ruedas Negociacion directa";
            CargarDatos();
        }
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { };
        SqlDbType[] lTipoparametros = { };
        string[] lValorParametros = { };
        try
        {
            lConexion.Abrir();
            dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaAbiND", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgConsulta.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";

        string[] lsNombreParametros = { "@P_numero_rueda" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        String[] lValorParametros = { this.dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text };
        try
        {
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_UptRuedaND", lsNombreParametros, lTipoparametros, lValorParametros);
            if (lLector.HasRows)
            {
                while (lLector.Read())
                    lblMensaje.Text += lLector["error"].ToString() + "<br>";
            }
            else
            {
                lblMensaje.Text = "La rueda se finalizó correctamernte";
                CargarDatos();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Se presentó un Problema en la finalización de la rueda. " + ex.Message;
        }

    }
}