﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using Segas.Web.Elements;
using System.Text;

public partial class Procesos_frm_ContratosC1C2Bim : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Carga de contratos Bimestrales C1 y C2 en el SE_GAS";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        Master.Titulo = lsTitulo;
        /// Activacion de los Botones
        buttons.Inicializar(botones: new[] { EnumBotones.Buscar, EnumBotones.Aprobar });
        buttons.FiltrarOnclick += btnConsultar_Click;
        buttons.AprobarOnclick += btnActualizar_Click;

        if (!IsPostBack)
        {
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlModalidad, "m_modalidad_contractual", " estado = 'A' and sigla in ('C1','C2') order by codigo_modalidad", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMes, "m_mes", " 1=1 order by mes", 0, 1);
            try
            {
                ddlMes.SelectedValue = DateTime.Now.Month.ToString();
            }
            catch (Exception ex)
            { }
            lConexion.Cerrar();
            consultar();
        }
    }
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        this.dtgConsulta.CurrentPageIndex = 0;
        consultar();
    }

    protected void btnActualizar_Click(object sender, EventArgs e)
    {
        var lblMensaje = new StringBuilder();
        if (ddlModalidad.SelectedValue == "0")
            lblMensaje.Append("Debe seleccionar la modalidad de contrato para cargar los contratos\\n");
        if (ddlMes.SelectedValue == "0")
            lblMensaje.Append("Debe seleccionar el mes de la subasta para cargar los contratos\\n");
        if (lblMensaje.ToString() == "")
        {
            SqlDataReader lLector;
            SqlCommand lComando = new SqlCommand();
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();

            lComando.Connection = lConexion.gObjConexion;
            lComando.CommandType = CommandType.StoredProcedure;
            lComando.CommandText = "pa_SetContratoC1C2Bim";
            lComando.Parameters.Add("@P_codigo_modalidad", SqlDbType.Int).Value = ddlModalidad.SelectedValue;
            lComando.Parameters.Add("@P_mes_subasta", SqlDbType.Int).Value = ddlMes.SelectedValue;
            lComando.CommandTimeout = 3600;
            lLector = lComando.ExecuteReader();
            if (lLector.HasRows)
            {
                while (lLector.Read())
                    lblMensaje.Append(lLector["error"].ToString() + " < br > ");
            }
            else
                lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
            if (lblMensaje.ToString() == "")
            {
                consultar();
                Toastr.Success(this, "Contratos cargados Correctamente.!");
            }
        }
        if (lblMensaje.ToString() != "")
            Toastr.Warning(this, lblMensaje.ToString(), "Warning!", 50000);
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void consultar()
    {
        var lblMensaje = new StringBuilder();
        if (ddlMes.SelectedValue == "0")
            lblMensaje.Append("Debe seleccionar el mes de la subasta");
        if (lblMensaje.ToString() == "")
        {
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetContratoC1C2Bim";
                lComando.Parameters.Add("@P_codigo_modalidad", SqlDbType.Int).Value = ddlModalidad.SelectedValue;
                lComando.Parameters.Add("@P_mes_subasta", SqlDbType.Int).Value = ddlMes.SelectedValue;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgConsulta.DataSource = lds;
                dtgConsulta.DataBind();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Append("No se Pudo Consultar la información.! " + ex.Message.ToString());
            }
        }
        if (lblMensaje.ToString() != "")
            Toastr.Warning(this, lblMensaje.ToString(), "Warning!", 50000);
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lsTable, string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lsTable, lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        this.dtgConsulta.CurrentPageIndex = e.NewPageIndex;
        consultar();
    }
}