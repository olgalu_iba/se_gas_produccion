﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_tablero_neg_reloj.aspx.cs"
    Inherits="Procesos_frm_tablero_neg_reloj" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link href="../css/estiloTab.css" rel="stylesheet" type="text/css" />
</head>
<body style="background-color: #00406B">
    <form id="frmListado" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table border="0" align="center" cellpadding="0" cellspacing="0" runat="server" id="Table1"
        width="100%">
        <tr valign="top">
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Hora: </font>
            </td>
        </tr>
        <tr>
            <td align="center" class="td4" valign="top">
                <font color='#F5DA81' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Timer ID="Timer1" OnTick="Timer1_Tick" runat="server" Interval="1000" />
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Timer1" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:Label ID="lblRelog" runat="server"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </font>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="HndFechaRueda" runat="server" />
    </form>
</body>
</html>
