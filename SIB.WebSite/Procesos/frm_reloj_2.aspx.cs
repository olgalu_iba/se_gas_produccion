﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
//using System.Windows.Forms;



public partial class Procesos_frm_reloj_2 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    String strRutaArchivo;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString();
        }
        catch (Exception ex)
        {
 
        }
    }
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        try
        {
            lblRelog.Text = DateTime.Now.ToString("HH:mm:ss").Substring(0, 8);
        }
        catch (Exception ex)
        {
        }

        try
        {
            if (Session["ofertas"].ToString() == "S")
            {
                Session["ofertas"] = "N";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.postura.location= 'frm_posturas.aspx';", true);
            }
            else
            {
                string oArchivo = strRutaArchivo + "Subasta-" + Session["numero_rueda"].ToString() + ".txt";
                if (Session["hora"].ToString() != File.GetCreationTime(oArchivo).ToString("HH:mm:ss"))
                {
                    Session["hora"] = File.GetCreationTime(oArchivo).ToString("HH:mm:ss");
                    Session["mis_ofertas"] = "N";
                    Session["refrescar_prm"] = "S";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.prm.location= 'frm_subasta_prm.aspx';", true);
                }
                else
                {
                    if (Session["mis_ofertas"].ToString() == "S")
                    {
                        Session["mis_ofertas"] = "N";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.postura.location= 'frm_posturas.aspx';", true);
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
}
