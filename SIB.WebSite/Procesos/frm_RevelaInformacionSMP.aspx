﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_RevelaInformacionSMP.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_RevelaInformacionSMP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div>
        <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
            width="85%">
            <tr>
                <td align="center" class="th1">
                    <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
            width="85%">
            <tr>
                <td class="td1">
                    Subasta
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlSubasta" runat="server" OnSelectedIndexChanged="ddlSubasta_SelectedIndexChanged"
                        AutoPostBack="true">
                    </asp:DropDownList>
                    <asp:HiddenField ID="hndFecha" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla" visible="false">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 900px; height: 400px;">
                    <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="vendedor_ofertante" HeaderText="Vendedor Oferente" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="300px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="modalidad" HeaderText="Modalidad Contractual" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="300px"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
 </asp:Content>
 