﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Procesos
{
    public partial class frm_CargaDecSubBimNormal : Page
    {
        InfoSessionVO goInfo = null;
        clConexion lConexion = null;
        clConexion lConexion1 = null;
        String strRutaCarga;
        String strRutaFTP;
        SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
            strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];
            buttons.CargueOnclick += BtnCargar_Click;


            if (IsPostBack) return;

            //Titulo
            Master.Titulo = "Subasta";

            //Botones
            EnumBotones[] botones = { EnumBotones.Cargue };
            buttons.Inicializar(botones: botones);


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            string lsRutaArchivo = "";
            string lsNombre = "";
            string[] lsErrores = { "", "" };
            var lsCadenaArchivo = new StringBuilder();
            bool oTransOK = true;
            bool oCargaOK = true;
            SqlDataReader lLector;
            SqlCommand lComando = new SqlCommand();
            var liNumeroParametros = 0;
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_ruta_ftp", "@P_codigo_operador" };
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            object[] lValorParametrosO = { "", strRutaFTP, goInfo.cod_comisionista };
            lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;
            try
            {
                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);

                // Realiza las Validaciones de los Archivos
                if (FuArchivo.FileName != "")
                    lsErrores = ValidarArchivo(lsRutaArchivo);
                else
                {
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ArchivoContratosRequerido", CultureInfo.CurrentCulture)?.ToString());
                    return;
                }

                if (lsErrores[0] == "")
                {
                    if (oCargaOK && DelegadaBase.Servicios.put_archivo(lsRutaArchivo,
                            ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre,
                            ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]))
                    {
                        lValorParametrosO[0] = lsNombre;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = "pa_ValidaPlanoDecSubBimNormal";
                        lComando.CommandTimeout = 3600;
                        if (lsNombreParametrosO != null)
                        {
                            for (liNumeroParametros = 0;
                                liNumeroParametros <= lsNombreParametrosO.Length - 1;
                                liNumeroParametros++)
                            {
                                lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                            }
                        }

                        lLector = lComando.ExecuteReader();
                        //lLector = DelegadaOMA.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_ValidaPlanoOpe", lsNombreParametrosO, lTipoparametrosO, lValorParametrosO);
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                            }
                        }
                        else
                        {
                            Toastr.Success(this, HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga", CultureInfo.CurrentCulture)?.ToString());
                        }

                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString());
                    }
                }
                else
                {
                    lsCadenaArchivo.Append(lsErrores[0]);
                    DelegadaBase.Servicios.registrarProceso(goInfo, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCarga", CultureInfo.CurrentCulture)?.ToString(), "Usuario : " + goInfo.nombre);
                }

                //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                NotificacionCargaArchivo.NotificacionCargueExitoso(this, lsCadenaArchivo);
                LogCargaArchivo.DownloadBugsLog(this, lsCadenaArchivo);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            Int32 liNumeroLinea = 0;
            int liTotalRegistros = 0;
            string lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };
            string[] lsDecimal;

            StreamReader lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 5))
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 5 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<BR>";
                    }
                    else
                    {
                        /// Valida la fuente
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(0).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código de la fuente {" + oArregloLinea.GetValue(0) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }

                        /// Valida Punto de entrega
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(1).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del punto del SNT {" + oArregloLinea.GetValue(1) + "} es inválido, en la línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida la cantidad
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(2).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La cantidad {" + oArregloLinea.GetValue(2) + "} es inválida, en la línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida precio
                        try
                        {
                            Convert.ToDecimal(oArregloLinea.GetValue(3).ToString());
                            lsDecimal = oArregloLinea.GetValue(3).ToString().Split('.');
                            if (lsDecimal.Length > 1 && lsDecimal[1].Length > 2)
                                lsCadenaErrores = lsCadenaErrores + "El Precio de reserva {" + oArregloLinea.GetValue(3) + "} debe tener máximo 2 decimales, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El Precio de reserva {" + oArregloLinea.GetValue(3) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";

                        }
                        /// Valida precio delta
                        try
                        {
                            Convert.ToDecimal(oArregloLinea.GetValue(4).ToString());
                            lsDecimal = oArregloLinea.GetValue(4).ToString().Split('.');
                            if (lsDecimal.Length > 1 && lsDecimal[1].Length > 2)
                                lsCadenaErrores = lsCadenaErrores + "El delta del precio {" + oArregloLinea.GetValue(4) + "} debe tener máximo 2 decimales, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El delta del precio {" + oArregloLinea.GetValue(4) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";

                        }
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }
    }
}