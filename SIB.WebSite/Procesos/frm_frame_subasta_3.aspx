<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeFile="frm_frame_subasta_3.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Procesos.frm_frame_subasta_3" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/dev/auctionChart.js")%>"></script>
    <script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/dev/clock.js")%>"></script>

    <script language="javascript">
        //        document.location = no;
        function confirmarDemanda() {
            return confirm("Esta seguro que desea demandar!");
        }

        function confirmar() {
            return confirm("Esta seguro que desea enviar el mensaje!");
        }

        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }

        function formatoMiles(obj, evt) {
            var T;
            var strValue = removeCommas(obj.value);
            //if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
            if ((T = /([0-9]*)$/i.exec(strValue)) != null) {
                //                T[4] = T[4].substr(0, decimales);
                //                if (T[2] == '' && T[3] == '.') T[2] = 0;
                obj.value = addCommas(T[1]);
            } else {
                alert('formato invalido ');
            }
        }
        function formatoMilesPre(obj, evt, decimales) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
                T[4] = T[4].substr(0, decimales);
                if (T[2] == '' && T[3] == '.') {
                    T[2] = 0;
                }
                obj.value = T[1] + addCommas(T[2]) + T[3] + T[4];
            }
            else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }

    </script>

    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="kt-portlet">
                    <%--Head--%>
                    <div class="kt-portlet__head">
                        <%--Titulo--%>
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <asp:Label ID="lblTitle" Text="&Uacuteselo o V&eacutendalo a Largo Plazo " runat="server" />
                            </h3>
                        </div>
                        <%--Botones--%>
                        <asp:UpdatePanel ID="UpdatePanel12" style="margin-top: 1%" runat="server">
                            <ContentTemplate>
                                <segas:CrudButton ID="buttons" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                    <%--Contenido--%>
                    <div class="kt-portlet__body">
                        <strong>
                            <h5>
                                <input id="lblRelog" style="width: 100px; color: green; border: none;" type="text" readonly name="reloj" size="10">
                            </h5>
                        </strong>

                        <%--Estados de la subasta--%>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div id="divTiempoEspera" class="row" visible="false" runat="server">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <br />
                                        <div class="form-group">
                                            <div class="alert alert-info" runat="server">
                                                <strong>&#33;Atenci&#243;n&#161;</strong>
                                                <p>&nbsp; Se encuentra en un tiempo de espera</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:Button ID="btnClickEmulate" OnClick="Timer_Tick" Style="display: none;" ClientIDMode="Static" runat="server" />
                                <segas:Estado ID="estados" Visible="false" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <%--Separador--%>
                        <div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>

                        <%--Botones--%>
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label"></div>
                            <div class="kt-portlet__head-toolbar">
                                <segas:Subasta ID="buttonsSubasta" runat="server" />
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <segas:SubastaExtender ID="buttonsSubastaExtender" runat="server" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>

                        <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                            <ContentTemplate>
                                <%--Tabla Subasta--%>
                                <div class="kt-portlet__body">
                                    <div class="tab-content">
                                        <div class="table table-responsive">
                                            <asp:DataGrid ID="dtgSubasta" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                                ViewStateMode="Enabled" OnItemCommand="OnItemCommand_Click">
                                                <Columns>
                                                    <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--1--%><asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--2--%><asp:BoundColumn DataField="desc_unidad_medida" HeaderText="Unidad de Medida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--3--%><asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--4--%><asp:BoundColumn DataField="desc_punto" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--5--%><asp:BoundColumn DataField="desc_periodo" HeaderText="Periodo Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--6--%><asp:BoundColumn DataField="cantidad_total_venta" HeaderText="Cantidad Venta (KPCD)" DataFormatString="{0: ###,###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn> <%--20201020--%>
                                                    <%--7--%><asp:BoundColumn DataField="precio_venta" HeaderText="Precio Venta (Moneda Vigente/KPC)" ItemStyle-BackColor=""></asp:BoundColumn> <%--20201020--%> <%--20210224 pasa validaciones--%> <%--20221012--%>
                                                    <%--8--%><asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-BackColor=""></asp:BoundColumn>
                                                    <%--9--%><asp:TemplateColumn HeaderText="Oferta">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imbOfertar" runat="server" ToolTip="Ofertar" ImageUrl="~/Images/nuevo.gif" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--10--%><asp:TemplateColumn HeaderText="Modificar">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imbModificar" runat="server" ToolTip="Modificar Oferta" ImageUrl="~/Images/modificar.gif" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--11--%><asp:TemplateColumn HeaderText="Eliminar">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imbEliminar" runat="server" ToolTip="Eliminar Oferta" ImageUrl="~/Images/eliminar.gif" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--12--%><asp:TemplateColumn HeaderText="Grafica" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imbGrafica" runat="server" ToolTip="Curva Agregada" ImageUrl="~/Images/grafica.jpg" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--13--%><asp:BoundColumn DataField="oferta_ya_realizada" Visible="false"></asp:BoundColumn>
                                                    <%--14--%><asp:BoundColumn DataField="max_posturas" Visible="false"></asp:BoundColumn>
                                                    <%--15--%><asp:BoundColumn DataField="ind_vendedor" Visible="false"></asp:BoundColumn>
                                                </Columns>
                                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                                <PagerStyle HorizontalAlign="Center"></PagerStyle>
                                            </asp:DataGrid>
                                        </div>
                                        <%--Tabla que se exporta a Excel--%>
                                        <asp:DataGrid ID="dtgSubastaImpresion" runat="server" AutoGenerateColumns="False" Visible="false">
                                            <ItemStyle CssClass="td2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="15px"></ItemStyle>
                                            <Columns>
                                                <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" ItemStyle-HorizontalAlign="Left"
                                                    ItemStyle-Width="60px"></asp:BoundColumn>
                                                <%--1--%><asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"
                                                    ItemStyle-Width="150px"></asp:BoundColumn>
                                                <%--2--%><asp:BoundColumn DataField="desc_unidad_medida" HeaderText="Unidad Medida" ItemStyle-HorizontalAlign="Left"
                                                    ItemStyle-Width="60px"></asp:BoundColumn>
                                                <%--3--%><asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left"
                                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                                <%--4--%><asp:BoundColumn DataField="desc_punto" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"
                                                    ItemStyle-Width="200px"></asp:BoundColumn>
                                                <%--5--%><asp:BoundColumn DataField="desc_periodo" HeaderText="Periodo Entrega" ItemStyle-HorizontalAlign="Left"
                                                    ItemStyle-Width="100px"></asp:BoundColumn>
                                                <%--6--%><asp:BoundColumn DataField="cantidad_total_venta" HeaderText="Cantidad Total Venta (KPCD)"
                                                    DataFormatString="{0: ###,###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right"
                                                    ItemStyle-Width="100px"></asp:BoundColumn><%--20201020--%>
                                                <%--7--%><asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-BackColor=""
                                                    ItemStyle-Width="80px"></asp:BoundColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
            <%-- <div class="col-md-3 col-xl-3">
                <div class="kt-portlet kt-portlet--height-fluid-half">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title nb">widget</h3>
                        </div>




                    </div>
                    <div class="kt-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_widget11_tab1_content">
                                <!--begin::Widget 11-->
                                <div class="kt-widget11">
                                    <div class="table-responsive">

                                        <table class="table">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>#</th>
                                                    <th>column</th>
                                                    <th>column</th>
                                                    <th>column</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>data</td>
                                                    <td>data</td>
                                                    <td>data</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>data</td>
                                                    <td>data</td>
                                                    <td>data</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td>data</td>
                                                    <td>data</td>
                                                    <td>data</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                                <!--end::Widget 11-->
                            </div>

                        </div>
                    </div>
                </div>
                <div class="kt-portlet kt-portlet--height-fluid-half">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title nb">Widget</h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_widget11_tab1_content">
                                <!--begin::Widget 11-->
                                <div class="kt-widget11">
                                    <div class="table-responsive">

                                        <table class="table">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>#</th>
                                                    <th>column</th>
                                                    <th>column</th>
                                                    <th>column</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>data</td>
                                                    <td>data</td>
                                                    <td>data</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>data</td>
                                                    <td>data</td>
                                                    <td>data</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td>data</td>
                                                    <td>data</td>
                                                    <td>data</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                                <!--end::Widget 11-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>--%>
        </div>
    </div>

    <%--Modal Suspender--%>
    <div class="modal fade" id="mdlSuspender" tabindex="-1" role="dialog" aria-labelledby="mdlSuspenderLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlInsideSuspender" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlSuspenderLabel">SUSPENDER RUEDA</h5>
                        </div>
                        <div class="modal-body">
                            <h5>
                                <asp:Label ID="lblFechaAct" runat="server"></asp:Label>
                            </h5>

                            <hr>

                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="ddlDefinitivo" runat="server">Suspensión Definitiva</asp:Label>
                                        <asp:DropDownList ID="ddlDefinitivo" CssClass="form-control selectpicker" runat="server">
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                            <asp:ListItem Value="S">Si</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="TxtFecha" runat="server">Fecha Próxima Apertura</asp:Label>
                                        <asp:TextBox ID="TxtFecha" CssClass="form-control datepicker" placeholder="yyyy/mm/dd" runat="server" ClientIDMode="Static" Width="100%" />
                                        <asp:RequiredFieldValidator ID="RfvTxtFecha" runat="server" ErrorMessage="Debe Ingresar la fecha de apertura" ControlToValidate="TxtFecha" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="TxtObservacion" runat="server">Observaciones</asp:Label>
                                        <asp:TextBox ID="TxtObservacion" CssClass="form-control" runat="server" MaxLength="1000" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseSuspender_Click" runat="server" />
                            <%--Agregar el confirmar--%>
                            <asp:Button ID="btnSuspender" Text="Suspender" class="btn btn-primary" runat="server" OnClick="btnSuspender_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <%--Modal Reactivar--%>
    <div class="modal fade" id="mdlReactivar" tabindex="-1" role="dialog" aria-labelledby="mdlReactivarLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlInsideReactivar" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlReactivarLabel">Reactivar Rueda</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5>
                        <asp:Label ID="lblEstadoReactivar" runat="server"></asp:Label>
                    </h5>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <asp:Label ID="lblEstado" runat="server"></asp:Label>
                                <asp:HiddenField ID="hndFase" runat="server" />
                            </div>
                        </div>
                        <div id="horaComp" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                            <div class="form-group">
                                <asp:Label AssociatedControlID="TxtHoraIniPubUvlp" runat="server">Horas Para Publicacion</asp:Label>
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <asp:Label AssociatedControlID="TxtHoraIniPubUvlp" runat="server">Hora Inicial</asp:Label>
                                            <div class="input-group mb-3 date">
                                                <asp:TextBox ID="TxtHoraIniPubUvlp" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <asp:RegularExpressionValidator ID="RevTxtHoraIniPubUvlp" ControlToValidate="TxtHoraIniPubUvlp" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora de Publicacion"> * </asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <asp:Label AssociatedControlID="TxtHoraFinPubUvlp" runat="server">Hora Final</asp:Label>
                                            <div class='input-group date'>
                                                <asp:TextBox ID="TxtHoraFinPubUvlp" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <asp:RegularExpressionValidator ID="revTxtHoraFinPubUvlp" ControlToValidate="TxtHoraFinPubUvlp" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora final  de Publicacion"> * </asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="horaNeg" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                            <div class="form-group">
                                <asp:Label AssociatedControlID="TxtHoraIniCompUvlp" runat="server"> Hora Ingreso de posturas de compra</asp:Label>
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <asp:Label AssociatedControlID="TxtHoraIniCompUvlp" runat="server">Hora Inicial</asp:Label>
                                            <div class="input-group mb-3 date">
                                                <asp:TextBox ID="TxtHoraIniCompUvlp" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <asp:RegularExpressionValidator ID="revTxtHoraIniCompUvlp" ControlToValidate="TxtHoraIniCompUvlp" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora inicial de Ingreso de posturas de compra"> * </asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <asp:Label AssociatedControlID="TxtHoraFinCompUvlp" runat="server">Hora Final</asp:Label>
                                            <div class='input-group date'>
                                                <asp:TextBox ID="TxtHoraFinCompUvlp" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <asp:RegularExpressionValidator ID="revTxtHoraFinCompUvlp" ControlToValidate="TxtHoraFinCompUvlp" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora final de Ingreso de posturas de compra"> * </asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <asp:Label AssociatedControlID="TxtHoraIniCalceUvlp" runat="server"> Horas Para calce</asp:Label>
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <asp:Label AssociatedControlID="TxtHoraIniCalceUvlp" runat="server">Hora Inicial</asp:Label>
                                            <div class="input-group mb-3 date">
                                                <asp:TextBox ID="TxtHoraIniCalceUvlp" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <asp:RegularExpressionValidator ID="revTxtHoraIniCalceUvlp" ControlToValidate="TxtHoraIniCalceUvlp" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora de calce"> * </asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <asp:Label AssociatedControlID="TxtHoraFinCalceUvlp" runat="server">Hora Final</asp:Label>
                                            <div class='input-group date'>
                                                <asp:TextBox ID="TxtHoraFinCalceUvlp" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <asp:RegularExpressionValidator ID="revTxtHoraFinCalceUvlp" ControlToValidate="TxtHoraFinCalceUvlp" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora final  calce"> * </asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <asp:Label AssociatedControlID="TxtObservacionReac" runat="server">Observaciones</asp:Label>
                                <asp:TextBox ID="TxtObservacionReac" CssClass="form-control" runat="server" MaxLength="1000" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseReactivar_Click" runat="server" />
                    <%--Agregar el confirmar--%>
                    <asp:Button ID="Button1" Text="Reactivar" CssClass="btn btn-primary" ValidationGroup="comi" CausesValidation="true" OnClick="btnReactivar_Click" runat="server" />
                </div>
            </div>
        </div>
    </div>

    <%--Modal Ingreso Posturas de Compra--%>
    <div class="modal fade" id="mdlIngresoPosturaCompra" tabindex="-1" role="dialog" aria-labelledby="mdlIngresoPosturaCompraLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlIngresoPosturaCompraInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlIngresoPosturaCompraLabel">Ingreso Posturas de Compra</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblRueda" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblId" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblFecha" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblPuntoEntrega" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblPeridoEnt" runat="server"></asp:Label>
                                        <asp:Label ID="lblModalidad" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="alert alert-info">
                                        <strong>&#33;Atenci&#243;n&#161;</strong>
                                        <p>&nbsp; Los Precios de Compra deben ser Ingresados de mayor a menor.</p>

                                        <strong>&#33;Atenci&#243;n&#161;</strong>
                                        <p>&nbsp; Las cantidades a Comprar deben ser Ingresados de menor a mayor.</p>
                                    </div>
                                </div>
                                <div class="table table-responsive">
                                    <asp:DataGrid ID="dtgPosturasC" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center" ViewStateMode="Enabled">
                                        <Columns>
                                            <asp:BoundColumn DataField="codigo_postura" HeaderText="No. Postura" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Precio">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtPrecio" CssClass="form-control" runat="server" onkeyup="return formatoMilesPre(this,event,2);"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Cantidad ">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtCantidad" CssClass="form-control" runat="server" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="cantidad_postura" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="precio_postura" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="numero_postura" Visible="false"></asp:BoundColumn>
                                        </Columns>
                                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                        <PagerStyle HorizontalAlign="Center"></PagerStyle>
                                    </asp:DataGrid>
                                </div>

                                <asp:HiddenField ID="hndID" runat="server" />
                                <asp:HiddenField ID="hdfNoRueda" runat="server" />
                                <asp:HiddenField ID="hdfMaxPostCompra" runat="server" />
                                <asp:HiddenField ID="hndFechaRueda" runat="server" />
                                <asp:HiddenField ID="hndAccion" runat="server" />
                                <asp:HiddenField ID="hndCntTotal" runat="server" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseIngresoPosturasCompra_Click" runat="server" />
                            <asp:Button ID="btnOfertar" Text="Crear" OnClientClick="return confirmarDemanda();" CssClass="btn btn-primary" OnClick="btnOfertar_Click" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <%--Modal Curva de Oferta y Demanda Agregada--%>
    <div class="modal fade" id="mdlCurvaOfertaDemAgre" tabindex="-1" role="dialog" aria-labelledby="mdlCurvaOfertaDemAgreLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlCurvaOfertaDemAgreInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlCurvaOfertaDemAgreLabel">Curva de Oferta y Demanda Agregada</h5>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <h5>
                                <asp:Label ID="lblPuntoCurva" runat="server" Font-Bold="true"></asp:Label>
                            </h5>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <div id="chart_div" style="width: 700px; height: 500px; display: block; margin: 0 auto !important;"></div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="table table-responsive">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <asp:DataGrid ID="dtgInformacion" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                            ViewStateMode="Enabled" OnItemCommand="OnItemCommand_Click">
                                            <Columns>
                                                <asp:BoundColumn DataField="precio" HeaderText="Precio" DataFormatString="{0: ###,###,##0.00}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="cantidad_venta" HeaderText="Cantidad Venta" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,##0}"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="cantidad_compra" HeaderText="Cantidad Compra" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,##0}"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="cantidad_min" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="cantidad_max" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="precio_min" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="precio_max" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="precio_venta" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="precio_compra" Visible="false"></asp:BoundColumn>
                                                <%--20210224--%>
                                                <asp:BoundColumn DataField="grafica_compra" Visible="false"></asp:BoundColumn>
                                            </Columns>
                                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                                        </asp:DataGrid>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseCurvaOferDemAgre_Click" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%--Modal Enviar Mensaje--%>
    <div class="modal fade" id="mdlEnviarMensaje" tabindex="-1" role="dialog" aria-labelledby="mdlEnviarMensajeLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlEnviarMensajeInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlEnviarMensajeLabel">Envio de Mensajes</h5>
                </div>
                <div class="modal-body">
                    <h5>
                        <asp:Label ID="Label4" runat="server"></asp:Label>
                    </h5>

                    <hr>

                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <asp:Label ID="lblTitulo" runat="server" AssociatedControlID="TxtMensaje" Text="Envio de Mensajes" Font-Bold="true"></asp:Label>
                                        <asp:TextBox ID="TxtMensaje" CssClass="form-control" runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <asp:UpdatePanel UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseEnviarMensaje_Click" runat="server" />
                            <asp:Button ID="Button4" Text="Aceptar" CssClass="btn btn-primary" runat="server" OnClientClick="return confirmar();" OnClick="btnMensaje_Click" ValidationGroup="comi" CausesValidation="true" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%--Modal Mensajes--%>
    <div class="modal fade" id="mdlMensaje" tabindex="-1" role="dialog" aria-labelledby="mdlMensajeLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlMensajeInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlMensajeLabel">Mis Mensajes</h5>
                </div>
                <div class="modal-body">
                    <h5>
                        <asp:Label ID="Label6" runat="server"></asp:Label>
                    </h5>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <div class="table table-responsive">
                                    <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                        <ContentTemplate>
                                            <asp:Literal ID="ltTableroRF" Mode="Transform" runat="server"></asp:Literal>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:UpdatePanel ID="UpdatePanel6" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseMensajes_Click" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%--Modal Contratos--%>
    <div class="modal fade" id="mdlContratos" tabindex="-1" role="dialog" aria-labelledby="mdlContratosLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlContratosInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlContratosLabel">Mis Contratos</h5>
                </div>
                <div class="modal-body">
                    <h5>
                        <asp:Label ID="Label5" runat="server"></asp:Label>
                    </h5>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <div class="table table-responsive">
                                    <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                        <ContentTemplate>
                                            <asp:Literal ID="ltTablero" Mode="Transform" runat="server"></asp:Literal>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:UpdatePanel ID="UpdatePanel15" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseContratos_Click" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
