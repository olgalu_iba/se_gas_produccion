﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
//using System.Windows.Forms;



public partial class Procesos_frm_reloj_3 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    String strRutaArchivo;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString();
        }
        catch (Exception ex)
        {

        }
    }
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        try
        {
            lblRelog.Text = DateTime.Now.ToString("HH:mm:ss").Substring(0, 8);
        }
        catch (Exception ex)
        {
        }

        try
        {

            string oArchivo = strRutaArchivo + "subasta-" + Session["numero_rueda"].ToString() + ".txt";
            if (Session["hora"].ToString() != File.GetCreationTime(oArchivo).ToString("HH:mm:ss"))
            {
                Session["hora"] = File.GetCreationTime(oArchivo).ToString("HH:mm:ss");
                Session["mis_ofertas"] = "S";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.prm.location= 'frm_subasta_prm_3.aspx?';", true);
            }
            else
            {
                if (Session["refrescar"].ToString() == "S")
                {
                    Session["refrescar"] = "N"; //20170929 rq048-17
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.postura.location= 'frm_posturas_3.aspx?';", true);
                }
            }

        }
        catch (Exception ex)
        {
        }
    }
}
