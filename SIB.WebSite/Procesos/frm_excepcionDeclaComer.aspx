﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_excepcionDeclaComer.aspx.cs" EnableEventValidation="false" Inherits="Procesos.frm_excepcionDeclaComer" MasterPageFile="~/PlantillaPrincipal.master" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Año Negociación" AssociatedControlID="TxtBusAñoNeg" runat="server" />
                            <asp:TextBox ID="TxtBusAñoNeg" runat="server" Width="100%" CssClass="form-control" ValidationGroup="detalle"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbeTxtBusAñoNeg" runat="server" TargetControlID="TxtBusAñoNeg"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Trimestre Negociación" AssociatedControlID="ddlBusTrimestreNeg" runat="server" />
                            <asp:DropDownList ID="ddlBusTrimestreNeg" Width="100%" CssClass="form-control" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tramo" AssociatedControlID="ddlBusTramo" runat="server" />
                            <asp:DropDownList ID="ddlBusTramo" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Año Entrega" AssociatedControlID="TxtBusAñoEnt" runat="server" />
                            <asp:TextBox ID="TxtBusAñoEnt" runat="server" Width="100%" CssClass="form-control" ValidationGroup="detalle"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtAñoEnt" runat="server" TargetControlID="TxtBusAñoEnt"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Trimestre Entrega" AssociatedControlID="ddlBusTrimestreEnt" runat="server" />
                            <asp:DropDownList ID="ddlBusTrimestreEnt" Width="100%" CssClass="form-control" runat="server" />
                        </div>
                    </div>
                </div>
                <%--Grilla--%>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgMaestro" Visible="False" AutoGenerateColumns="False" AllowPaging="True" Width="100%" CssClass="table-bordered" PageSize="10"
                                OnItemCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged" runat="server">
                                <Columns>
                                    <asp:BoundColumn DataField="codigo_excepcion" HeaderText="Código Excepción" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="año_negociacion" HeaderText="Año negociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="trimestre_negociacion" HeaderText="Trimestre Négociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_trimestre_neg" HeaderText="Descripción Trimestre Négociación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_tramo" HeaderText="Código Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_tramo" HeaderText="Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="año_entrega" HeaderText="Año Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="trimestre_entrega" HeaderText="Trimestre Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_trimestre_ent" HeaderText="Descripción Trimestre Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="puede_modif" visible="false" ></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more-1"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                    <!--begin::Nav-->
                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                        <ContentTemplate>
                                                            <ul class="kt-nav">
                                                                    <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Modificar</span>
                                                                    </asp:LinkButton>
                                                                </li>
                                                                <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lnkEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">  <%--20200727--%>
                                                            <i class="kt-nav__link-icon flaticon2-expand"></i>
                                                            <span class="kt-nav__link-text">Eliminar</span>
                                                                    </asp:LinkButton>
                                                                </li>
                                                            </ul>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%--Modals--%>
    <%--Crear Registro--%>
    <div class="modal fade" id="CrearRegistro" tabindex="-1" role="dialog" aria-labelledby="mdlCrearRegistroLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="CrearRegistroInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlCrearRegistroLabel" runat="server">Agregar</h5>
                        </div>
                        <div class="modal-body">
                            <div class="kt-portlet__body" runat="server">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6 col-lg-4">
                                        <div class="form-group">
                                            <asp:Label Text="Código Excepción" AssociatedControlID="TxtCodigoExc" runat="server" />
                                            <asp:TextBox ID="TxtCodigoExc" runat="server" MaxLength="3" CssClass="form-control" />
                                            <asp:Label ID="LblCodigoExc" runat="server" Visible="False" CssClass="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-lg-4">
                                        <div class="form-group">
                                            <asp:Label Text="Tramo" AssociatedControlID="ddlTramo" runat="server" />
                                            <asp:DropDownList ID="ddlTramo" runat="server" CssClass="form-control selectpicker" data-live-search="true"  />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-lg-4">
                                        <div class="form-group">
                                            <asp:Label Text="Año Entrega" AssociatedControlID="TxtAño" runat="server" />
                                            <asp:TextBox ID="TxtAño" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FteTxtAño" runat="server" TargetControlID="TxtAño" FilterType="Custom, Numbers" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-lg-4">
                                        <div class="form-group">
                                            <asp:Label Text="Trimestre Entrega" AssociatedControlID="ddlTrimestre" runat="server" />
                                            <asp:DropDownList ID="ddlTrimestre" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                            <asp:HiddenField ID="hdfTrimestre" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-lg-4">
                                        <div class="form-group">
                                            <asp:Label Text="Estado" AssociatedControlID="ddlEstado" runat="server" />
                                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control selectpicker" data-live-search="true" Enabled="false" >
                                                <asp:ListItem Value="A">Activo</asp:ListItem>
                                                <asp:ListItem Value="I">Inactivo</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="imbCancelar" runat="server" class="btn btn-secondary" Text="Cancelar" CausesValidation="false" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="imbCancelar_Click" />
                            <asp:Button ID="imbCrear" runat="server" CssClass="btn btn-success" Text="Crear" OnClick="imbCrear_Click" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="imbActualiza" runat="server" CssClass="btn btn-success" Text="Actualizar" OnClick="imbActualiza_Click" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
