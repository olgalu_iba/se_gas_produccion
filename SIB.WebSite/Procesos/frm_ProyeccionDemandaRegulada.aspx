﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ProyeccionDemandaRegulada.aspx.cs"
    Inherits="Procesos_frm_ProyeccionDemandaRegulada" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" bgcolor="#85BF46">
                <table border="0" cellspacing="2" cellpadding="2" align="right">
                    <tr>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkNuevo" runat="server" NavigateUrl="~/Procesos/frm_ProyeccionDemandaRegulada.aspx?lsIndica=N">Nuevo</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_ProyeccionDemandaRegulada.aspx?lsIndica=L">Listar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_ProyeccionDemandaRegulada.aspx?lsIndica=B">Consultar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/WebForms/Home.aspx">Salir</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click" />
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgPdf" runat="server" ImageUrl="~/Images/Pdf.png" OnClick="ImgPdf_Click"
                                Width="70%" />
                        </td>
                        <td class="tv2">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor ="White"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <br /><br /><br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Código Proyección Demanda Regulada
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCodigoProy" runat="server" MaxLength="3"></asp:TextBox>
                <asp:Label ID="LblCodigoProy" runat="server" Visible="False"></asp:Label>
                <asp:HiddenField  ID="hndPerIni" runat= "server" />
                <asp:HiddenField  ID="hndPerFin" runat= "server" />
            </td>
        </tr>
        <%--<tr>
            <td class="td1">
                Año
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtAno" runat="server" MaxLength="10" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtAno" runat="server" ErrorMessage="Debe Ingresar el Año"
                    ControlToValidate="TxtAno" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtAno" runat="server" ControlToValidate="TxtAno" Type="Integer"
                    Operator="DataTypeCheck" ValidationGroup="comi" ErrorMessage="El Campo Año debe Ser numérico">*</asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Mes
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlMes" runat="server" Width="150px">
                </asp:DropDownList>
            </td>
        </tr>--%>
        <tr>
            <td class="td1">
                Sector de Consumo
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlSectorConsumo" runat="server" Width="200px">
                </asp:DropDownList>
            </td>
        </tr>
        <%--20170705 rq025-17--%>
        <%--<tr>
            <td class="td1">
                Punto de Salida
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlPuntoSalida" runat="server" Width="200px">
                </asp:DropDownList>
            </td>
        </tr>--%>
        <tr>
            <td class="td1">
                Mercado Relevante
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlMercadoRelevante" runat="server" Width="200px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Cantidad <asp:Label ID="lblMes01" runat= "server" ></asp:Label>
            </td>
            <td class="td2" >
                <asp:TextBox ID="TxtCantidad01" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTEBTxtCantidad01" runat="server" TargetControlID="TxtCantidad01"
                    FilterType="Custom, Numbers" >
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Cantidad <asp:Label ID="lblMes02" runat= "server" ></asp:Label>
            </td>
            <td class="td2" >
                <asp:TextBox ID="TxtCantidad02" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftebTxtCantidad02" runat="server" TargetControlID="TxtCantidad02"
                    FilterType="Custom, Numbers" >
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Cantidad <asp:Label ID="lblMes03" runat= "server" ></asp:Label>
            </td>
            <td class="td2" >
                <asp:TextBox ID="TxtCantidad03" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftebTxtCantidad03" runat="server" TargetControlID="TxtCantidad03"
                    FilterType="Custom, Numbers" >
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Cantidad <asp:Label ID="lblMes04" runat= "server" ></asp:Label>
            </td>
            <td class="td2" >
                <asp:TextBox ID="TxtCantidad04" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftbTxtCantidad04" runat="server" TargetControlID="TxtCantidad04"
                    FilterType="Custom, Numbers" >
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Cantidad <asp:Label ID="lblMes05" runat= "server" ></asp:Label>
            </td>
            <td class="td2" >
                <asp:TextBox ID="TxtCantidad05" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftebTxtCantidad05" runat="server" TargetControlID="TxtCantidad05"
                    FilterType="Custom, Numbers" >
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Cantidad <asp:Label ID="lblMes06" runat= "server" ></asp:Label>
            </td>
            <td class="td2" >
                <asp:TextBox ID="TxtCantidad06" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftebTxtCantidad06" runat="server" TargetControlID="TxtCantidad06"
                    FilterType="Custom, Numbers" >
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Cantidad <asp:Label ID="lblMes07" runat= "server" ></asp:Label>
            </td>
            <td class="td2" >
                <asp:TextBox ID="TxtCantidad07" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="fteTxtCantidad07" runat="server" TargetControlID="TxtCantidad07"
                    FilterType="Custom, Numbers" >
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Cantidad <asp:Label ID="lblMes08" runat= "server" ></asp:Label>
            </td>
            <td class="td2" >
                <asp:TextBox ID="TxtCantidad08" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="fteCantidad08" runat="server" TargetControlID="TxtCantidad08"
                    FilterType="Custom, Numbers" >
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Cantidad <asp:Label ID="lblMes09" runat= "server" ></asp:Label>
            </td>
            <td class="td2" >
                <asp:TextBox ID="TxtCantidad09" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="fteTxtCantidad09" runat="server" TargetControlID="TxtCantidad09"
                    FilterType="Custom, Numbers" >
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Cantidad <asp:Label ID="lblMes10" runat= "server" ></asp:Label>
            </td>
            <td class="td2" >
                <asp:TextBox ID="TxtCantidad10" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="fteTxtCantidad10" runat="server" TargetControlID="TxtCantidad10"
                    FilterType="Custom, Numbers" >
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Cantidad <asp:Label ID="lblMes11" runat= "server" ></asp:Label>
            </td>
            <td class="td2" >
                <asp:TextBox ID="TxtCantidad11" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftebTxtCantidad11" runat="server" TargetControlID="TxtCantidad11"
                    FilterType="Custom, Numbers" >
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Cantidad <asp:Label ID="lblMes12" runat= "server" ></asp:Label>
            </td>
            <td class="td2" >
                <asp:TextBox ID="TxtCantidad12" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="fteTxtCantidad12" runat="server" TargetControlID="TxtCantidad12"
                    FilterType="Custom, Numbers" >
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:Button ID="btnCrear" runat="server" Text="Crear" CssClass="opbutton1" OnClick="btnCrear_Click"
                    ValidationGroup="comi" />
                <asp:Button ID="btnActualizar" runat="server" Text="Actualizar" CssClass="opbutton1"
                    OnClick="btnActualizar_Click" ValidationGroup="comi" />
                <asp:Button ID="btnSalir" runat="server" Text="Salir" CssClass="opbutton1" OnClick="btnSalir_Click"
                    CausesValidation="false" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblBuscar" visible="false">
        <tr>
            <td class="td1">
                Código Proyección Demanda Regulada
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusCodigo" runat="server" autocomplete="off"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTBETxtBusCodigo" runat="server" TargetControlID="TxtBusCodigo"
                    FilterType="Custom, Numbers">
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Sector de Consumo
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddbBusSectorConsumo" runat="server" Width="200px">
                </asp:DropDownList>
            </td>
        </tr>
        <%--20170705 rq025-17--%>
        <%--<tr>
            <td class="td1">
                Punto de Salida
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusPuntoSalida" runat="server" Width="200px">
                </asp:DropDownList>
            </td>
        </tr>--%>
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
        width="80%">
        <tr>
            <td colspan="2">
                <div>
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged"
                        HeaderStyle-CssClass="th1" PageSize="30">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="codigo_proy_demreg" HeaderText="Código Proyección Demanda Regulada"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_registro" HeaderText="Fec Registro" ItemStyle-HorizontalAlign="Left"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nom_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="sector_consumo" HeaderText="Sector Consumo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20170705 rq025-17--%>
                            <%--<asp:BoundColumn DataField="punto_salida" HeaderText="Punto de Salida" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>--%>
                            <asp:BoundColumn DataField="mercado_relevante" HeaderText="Mercado Relevante" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualizacion" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass="">
                            </asp:EditCommandColumn>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                            <asp:BoundColumn DataField="codigo_sector_consumo" visible ="false">
                            </asp:BoundColumn>
                            <%--20170705 rq025-17--%>
                            <%--<asp:BoundColumn DataField="codigo_punto_salida" visible ="false">
                            </asp:BoundColumn>--%>
                            <asp:BoundColumn DataField="codigo_mercado_relevante" visible ="false">
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>