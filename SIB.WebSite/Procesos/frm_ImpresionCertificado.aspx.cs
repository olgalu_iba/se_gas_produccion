﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;
using Segas.Web.Elements;

public partial class Procesos_frm_ImpresionCertificado : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Impresión Certificado de Negociación"; //20180815 BUG230
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' And codigo_operador != 0 order by razon_social", 0, 4); // 20180126 rq107-16
            lConexion.Cerrar();
            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
            }
            DateTime ldfecha = DateTime.Now; //20171130 rq026-17
            TxtFechaCorte.Text = ldfecha.Year.ToString() + "/" + ldfecha.Month.ToString() + "/" + ldfecha.Day.ToString(); //20171130 rq026-17
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            if (lsTabla != "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// Nombre: ChkTodos_CheckedChanged
    /// Fecha: Abril 24 21 de 2012
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para chekear todos los registros
    /// Modificacion:
    /// </summary>
    protected void ChkTodos_CheckedChanged(object sender, EventArgs e)
    {
        //Selecciona o deselecciona todos los items del datagrid segun el checked del control
        CheckBox chk = (CheckBox)(sender);
        foreach (DataGridItem Grilla in this.dtgMaestro.Items)
        {
            CheckBox Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
            if (chk.Checked)
                Checkbox.Checked = true;
            else
                Checkbox.Checked = false;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        DateTime ldFecha;//20171130 rq026-17
        if (TxtFechaCorte.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaCorte.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha de corte. <br>";
            }
        }
        else
            lblMensaje.Text += "Debe digitar la fecha de corte. <br>";

        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial. <br>";
            }
        }
        if (TxtFechaIni.Text.Trim().Length <= 0 && ddlOperador.SelectedValue == "0" && ddlTipoRueda.SelectedValue == "0" && TxtNoRueda.Text.Trim().Length <= 0)
            lblMensaje.Text += "Debe Ingresar algún Parámetro de búsqueda. <br>"; //20180126 rq107-16
        if (lblMensaje.Text == "")
        {
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetConsContrato";
                if (ddlTipoRueda.SelectedValue != "")
                    lComando.Parameters.Add("@P_tipo_rueda", SqlDbType.Int).Value = ddlTipoRueda.SelectedValue;
                else
                    lComando.Parameters.Add("@P_tipo_rueda", SqlDbType.Int).Value = "0";
                if (TxtNoRueda.Text.Trim().Length <= 0)
                    lComando.Parameters.Add("@P_numero_rueda", SqlDbType.Int).Value = 0;
                else
                    lComando.Parameters.Add("@P_numero_rueda", SqlDbType.Int).Value = TxtNoRueda.Text.Trim();
                lComando.Parameters.Add("@P_fecha_contrato", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                lComando.Parameters.Add("@P_operador", SqlDbType.Int).Value = ddlOperador.SelectedValue;
                lComando.Parameters.Add("@P_numero_id", SqlDbType.Int).Value = 0;
                lComando.Parameters.Add("@P_tipo_perfil", SqlDbType.VarChar).Value = Session["tipoPerfil"].ToString();
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgMaestro.DataSource = lds;
                dtgMaestro.DataBind();
                dtgExcel.DataSource = lds; //20180126 rq107-16
                dtgExcel.DataBind(); //20180126 rq107-16
                imbExcel.Visible = true; //20180126 rq107-16
                tblGrilla.Visible = true;
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar la Consulta.! " + ex.Message.ToString(); //20171130 rq026-17
            }
        }
        if (lblMensaje.Text != "")
        {

            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";

        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlSubasta_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSubasta.SelectedValue != "0")
        {
            lConexion.Abrir();
            ddlTipoRueda.Items.Clear();
            LlenarControles(lConexion.gObjConexion, ddlTipoRueda, "m_tipos_rueda", " estado = 'A' And codigo_tipo_subasta = " + ddlSubasta.SelectedValue + " order by descripcion", 0, 1);
            lConexion.Cerrar();
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbGenerar_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        string lsContratos = "";
        string[] lsArchivo;
        int liNumero = 0;
        try
        {
            foreach (DataGridItem Grilla in this.dtgMaestro.Items)
            {
                CheckBox Checkbox = null;
                Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];

                if (Checkbox.Checked == true)
                {
                    liNumero++;
                    lsContratos += Grilla.Cells[1].Text.Trim() + ",";
                }
            }
            if (liNumero == 0)
                lblMensaje.Text = "No Seleccionó Registros para Procesar."; //20180126 rq107-16
            //2020727 ajsute front-end
            //if (lblMensaje.Text == "")
            //{
            //    clImpresionCertificado clImp;
            //    clImp = new clImpresionCertificado();
            //    lsArchivo = clImp.generarCertificadoGas(lsContratos, TxtFechaCorte.Text, goInfo).Split('-');  //20171130 rq026-17
            //    if (lsArchivo[0] != "ERROR")
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + lsArchivo[1].Trim() + "');", true);
            //    else
            //        lblMensaje.Text = lsArchivo[1].Trim();
            //}
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            lblMensaje.Text = ex.Message;
        }
        if (lblMensaje.Text != "")
        {

            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";

        }
    }
    /// 20180126 rq107-16
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        SqlDataAdapter lsqldata = new SqlDataAdapter();
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfCurva" + DateTime.Now + ".xls";
        string lstitulo_informe = "Informe certificado de gas";
        string lsTituloParametros = "";
        try
        {
            if (TxtFechaCorte.Text.Trim().Length > 0)
                lsTituloParametros += " Fecha Corte: " + TxtFechaCorte.Text.Trim();
            if (ddlSubasta.SelectedValue != "0")
                lsTituloParametros += " - Subasta: " + ddlSubasta.SelectedItem.ToString();
            if (ddlTipoRueda.SelectedValue != "0" && ddlTipoRueda.SelectedValue != "")
                lsTituloParametros += " - Tipo rueda: " + ddlTipoRueda.SelectedItem.ToString();
            if (TxtFechaIni.Text.Trim().Length > 0)
                lsTituloParametros += " Fecha Contrato: " + TxtFechaIni.Text.Trim(); //20180126 rq107-16
            if (TxtNoRueda.Text.Trim().Length > 0)
                lsTituloParametros += " - Número Rueda: " + TxtNoRueda.Text.Trim();
            if (ddlOperador.SelectedValue != "0")
                lsTituloParametros += " - Operador: " + ddlOperador.SelectedItem.ToString();

            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgExcel.Visible = true;
            dtgExcel.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            lform.Controls.Add(dtgExcel);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            //lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
            dtgExcel.Visible = false;
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Excel.!" + ex.Message.ToString(); //20180126 rq107-16
        }
        if (lblMensaje.Text != "")
        {

            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";

        }
    }


}