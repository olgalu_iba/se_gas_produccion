﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using PCD_Infraestructura.Business;
using PCD_Infraestructura.Transaction;
using PCD_Infraestructura.DomainLayer;
using System.Diagnostics;

public partial class OMA_frm_act_tab : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    String strRutaArchivo1;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        strRutaArchivo1 = ConfigurationManager.AppSettings["RutaArchivos"].ToString();
    }
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        //string oTexto = "";
        //oTexto = "xx-Usuario " + goInfo.Usuario + " -Hora Ejecucion: " + DateTime.Now.ToString() + " -Mis Ofe:" + Session["mis_ofertas"] +".";
        string oArchivo = strRutaArchivo1 + "Subasta-" + Session["numero_rueda"].ToString() + ".txt";
        if (Session["hora"].ToString() != File.GetCreationTime(oArchivo).ToString("HH:mm:ss") )
        {
            Session["hora"] = File.GetCreationTime(oArchivo).ToString("HH:mm:ss");
            Session["mis_ofertas"] = "N";
            Session["refrescar_prm"] = "S";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.prm.location= 'frm_subasta_prm.aspx';", true);
        }
        else
        {
            if (Session["mis_ofertas"].ToString() == "S")
            {
                Session["mis_ofertas"] = "N";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.titulo.location= 'frm_titulos.aspx';", true);
            }
            //else
            //{
            //    if (Session["codigo_subasta"].ToString() == "0")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.ofertas.location= 'frm_ofertas1.aspx';", true);
            //    }
            //}
        }


        //if (goInfo.Usuario == "prb_calce_06")
        //{
        //string strRutaLog = @"C:\inetpub\wwwroot\SIB_calce\Planos\Subasta\log.txt";
        //StreamWriter sw = new StreamWriter(strRutaLog, true);
        //sw.WriteLine(oTexto + "\n");
        //sw.Close();
        //}
    }
}
