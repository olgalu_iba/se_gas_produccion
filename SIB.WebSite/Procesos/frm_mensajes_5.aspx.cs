﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using PCD_Infraestructura.Business;
using PCD_Infraestructura.Transaction;
using PCD_Infraestructura.DomainLayer;
using System.Diagnostics;

public partial class Procesos_frm_mensajes_5 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    String strRutaArchivo;

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (this.Request.QueryString["operador"] != null && this.Request.QueryString["operador"].ToString() != "")
        //if (Page.Request.Params["__EVENTTARGET"] != null && Page.Request.Params["__EVENTTARGET"] != "")
        //    eliminar(hndOperador.Value);
        //if (hndOperador.Value != "")
        //{
        //    eliminar(hndOperador.Value);
        //    hndOperador.Value = "";
        //}
        goInfo = (InfoSessionVO)Session["infoSession"];
        strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString() + "mensaje.txt";
        //ltTableroRF.Text = "";
        //ltTableroRF.Text += "<table border='0' cellspacing='0' dellpading ='0'><tr>";
        //ltTableroRF.Text += "<td  width='100%'align='center' bgcolor='#E46D0A' style='border-color:#E46D0A'  colspan='5'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>MIS MENSAJES</font></td>";
        //ltTableroRF.Text += "</tr>";
        //ltTableroRF.Text += "<tr>";
        //ltTableroRF.Text += "<td width='70%' style='border-color:#E46D0A' align='center' bgcolor='#E46D0A'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>NOMBRE</font></td>";
        //ltTableroRF.Text += "<td width='10%' style='border-color:#E46D0A' align='center' bgcolor='#E46D0A'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>PEND</font></td>";
        //ltTableroRF.Text += "<td width='10%' style='border-color:#E46D0A' align='center' bgcolor='#E46D0A'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>HORA</font></td>";
        //ltTableroRF.Text += "<td width='10%' style='border-color:#E46D0A' align='center' bgcolor='#E46D0A'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>MSG</font></td>";
        //ltTableroRF.Text += "<td width='10%' style='border-color:#E46D0A' align='center' bgcolor='#E46D0A'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>ELIM</font></td>";
        //ltTableroRF.Text += "</tr>";

        try
        {
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { Session["numero_rueda"].ToString(), goInfo.cod_comisionista };
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            dtgMensaje.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetMensajes5", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMensaje.DataBind();
            lConexion.Cerrar();
            //    SqlDataReader lLector;
            //    lConexion = new clConexion(goInfo);
            //    lConexion.Abrir();
            //    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetMensajes5", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
            //    if (goInfo.mensaje_error == "")
            //    {
            //        if (lLector.HasRows)
            //        {
            //            while (lLector.Read())
            //            {
            //                ltTableroRF.Text += "<tr>";
            //                ltTableroRF.Text += "<td width='70%' style='border-color:#F79632' align='left' bgcolor='#F79632'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["nombre_operador"].ToString() + "</font></td>";
            //                ltTableroRF.Text += "<td width='10%' style='border-color:#F79632' align='left' bgcolor='#F79632'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["cnt_pend"].ToString() + "</font></td>";
            //                ltTableroRF.Text += "<td width='10%' style='border-color:#F79632' align='left' bgcolor='#F79632'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["hora_ult_mensaje"].ToString() + "</font></td>";
            //                ltTableroRF.Text += "<td width='10%' align='center' bgcolor='#F79632'> <input type='button' name='mensajes' id='mensajes' value='MSG'  onClick='window.open(\"frm_detalleMensaje_5.aspx?codOperador=" + lLector["codigo_operador"].ToString() + "\",\"_blank\",\"top=150, left=500, width=400, height=600, scrollbars=1\");' class='campo'> </td>";
            //                ltTableroRF.Text += "<td width='10%' align='center' bgcolor='#F79632'> <input type='button' name='elimina' id='elimina' value='ElIM'  onClick='window.location.href='frm_mensajes_5.aspx?operador=" + lLector["codigo_operador"].ToString() + ";location.reload();';  class='campo'> </td>";
            //                ltTableroRF.Text += "<td width='10%' align='center' bgcolor='#F79632'> <input type='button' name='elimina' id='elimina1' value='ElIM'  onClick='actualizar(" + lLector["codigo_operador"].ToString() + ");';  class='campo'> </td>";
            //                ltTableroRF.Text += "</tr>";
            //            }
            //            ltTableroRF.Text += "</table>";
            //        }
            if (Session["refrescar_cont"].ToString() == "S")
            {
                Session["refrescar_cont"] = "N";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.contratos.location= 'frm_contratos_5.aspx';", true);
            }
            //}
            File.SetCreationTime(strRutaArchivo, DateTime.Now);
        }
        catch (Exception ex)
        {
        }
    }
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMensaje_EditCommand(object source, DataGridCommandEventArgs e)
    {
        if (((LinkButton)e.CommandSource).Text == "MSG")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('frm_detalleMensaje_5.aspx?codOperador=" + this.dtgMensaje.Items[e.Item.ItemIndex].Cells[0].Text + "','_blank','top=150, left=500, width=400, height=600, scrollbars=1');", true);
        }
        if (((LinkButton)e.CommandSource).Text == "ELIM")
        {
            eliminar(this.dtgMensaje.Items[e.Item.ItemIndex].Cells[0].Text);
        }
    }

    private void eliminar(string lsOperador)
    {
        string[] lsNombreParametros = { "@P_codigo_operador_soli", "@P_codigo_operador_elim" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { goInfo.cod_comisionista, lsOperador };
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetMensajeElim", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
        lConexion.Cerrar();
    }
}