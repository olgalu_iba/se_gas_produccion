﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using SIB.BASE;
using System.IO;

public partial class Procesos_frm_ELiminaIdND : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    SqlDataReader lLector;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            lblTitulo.Text = "Eliminación de IDs Negociacion directa";
            CargarDatos();
        }
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { };
        SqlDbType[] lTipoparametros = { };
        string[] lValorParametros = { };
        try
        {
            lConexion.Abrir();
            dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaAbiND", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgConsulta.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: dtgIds_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        hndRueda.Value = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text;
        cargarDatosIds();
    }
    /// <summary>
    /// Nombre: cargarDatosGrilla
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para Cargar los datos en la Grio.
    /// Modificacion:
    /// </summary>
    private void cargarDatosIds()
    {
        string[] lsNombreParametros = { "@P_numero_rueda" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        String[] lValorParametros = { hndRueda.Value  };
        try
        {
            lConexion.Abrir();
            dtgIds.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetIDsND", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgIds.DataBind();
            lConexion.Cerrar();
            dtgConsulta.Visible = false;
            dtgIds.Visible = true;
            imbRegresar.Visible = true;
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Error al consultar los Ids. " + ex.Message;
        }
    }

    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgIds_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";

        string[] lsNombreParametros = { "@P_numero_id" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        String[] lValorParametros = { this.dtgIds.Items[e.Item.ItemIndex].Cells[1].Text };
        try
        {
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_DelIdND", lsNombreParametros, lTipoparametros, lValorParametros);
            if (lLector.HasRows)
            {
                while (lLector.Read())
                    lblMensaje.Text += lLector["error"].ToString() + "<br>";
            }
            else
            {
                lblMensaje.Text = "El ID se eliminó correctamente";
                imbRegresar.Visible = false;
                dtgConsulta.Visible = true;
                dtgIds.Visible = false;
                CargarDatos();
                cargarDatosIds();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Se presentó un Problema en la finalización de la rueda. " + ex.Message;
        }

    }

    /// <summary>
    /// Nombre: btnRegresar_Click
    /// Fecha: mayo 6 de 2014
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: modifica un nuevo contrato
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbRegresar_Click(object sender, ImageClickEventArgs e)
    {
        CargarDatos();
        imbRegresar.Visible = false;
        dtgConsulta.Visible = true;
        dtgIds.Visible = false;
    }


}