﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_IngresoOfertaCompra_1 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    String strRutaArchivo;
    String strRutaArchivo1;
    SqlDataReader lLector;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            if (this.Request.QueryString["ID"] != null && this.Request.QueryString["ID"].ToString() != "")
            {
                hndID.Value = this.Request.QueryString["ID"];
                hndAccion.Value = this.Request.QueryString["accion"];
                hndRonda.Value = this.Request.QueryString["ronda"];
                hndCantAnt.Value = this.Request.QueryString["cantidad_ant"];
                /// Obtengo los Datos del ID Recibido
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_id_rueda idr, t_precio_reserva_v preR ", " idr.numero_id = " + hndID.Value + " and idr.numero_id = preR.numero_id and preR.ronda=" + hndRonda.Value);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    /// Lleno las etiquetas de la pantalla con los datos del Id.
                    lblTipo.Text = "Tipo: " + lLector["desc_modalidad"].ToString();
                    lblDuracion.Text = lLector["tiempo_entrega"].ToString();
                    lblId.Text = "ID: " + this.Request.QueryString["ID"];
                    lblLugar.Text = "Lugar: " + lLector["desc_punto_entrega"].ToString();
                    lblPrecioRes.Text = "Precio Reserva: " + lLector["precio_reserva"].ToString();
                    hndPrecioRes.Value = lLector["precio_reserva"].ToString();
                    lblCantidadV.Text = "Cantidad Ofrecida: " + lLector["cantidad_venta"].ToString();
                    //POne los datos a modificar

                    /// Reviso si se esta creando la oferta o modificando
                    if (hndAccion.Value == "M")
                        btnOfertar.Text = "Modificar Oferta";
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "El ID enviado NO está disponible para ofertar.!" + "');", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                }
                lLector.Close();
                lLector.Dispose();

                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_posturas_compra posC ", " posC.numero_id =" + hndID.Value + " and posC.codigo_operador = " + goInfo.cod_comisionista + " and posC.ronda = " + hndRonda.Value);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    TxtCantidad.Text = lLector["cantidad_postura"].ToString();
                    TxtPrecio.Text = lLector["precio"].ToString();
                }
                lLector.Dispose();
                lLector.Close();
                
                lConexion.Cerrar();
            }
            else
            {
                Toastr.Warning(this, "No Se enviaron los Parametros requeridos.!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
            }
            //hndCodSubasta.Value = "21";
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOfertar_Click(object sender, EventArgs e)
    {
        string oError = "";
        string sMensaje= "";
        double ldPreAct = 0;
        double ldPreRes = 0;

        if (TxtCantidad.Text == "")
            oError += "Debe digitar la cantidad demandada\\n";
        if (TxtPrecio.Text != "")
        {
            ldPreAct = Convert.ToDouble(TxtPrecio.Text.Replace(",",""));
            ldPreRes = Convert.ToDouble(hndPrecioRes.Value);
            if (ldPreAct < ldPreRes)
                oError += "El precio de sostenimiento debe ser mayor o igual que el precio de reserva\\n";
        }

        string[] lsNombreParametros = { "@P_numero_rueda", "@P_numero_id", "@P_codigo_operador", "@P_ronda", "@P_cantidad_postura", "@P_precio_sostenimiento", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Float, SqlDbType.VarChar };
        string[] lValorParametros = { Session["numero_rueda"].ToString(), hndID.Value, goInfo.cod_comisionista, hndRonda.Value, TxtCantidad.Text.Replace(",",""), "0", hndAccion.Value }; ///  Accion C=crear, M= Modificar
        //string[] lValorParametros = { "0", "", "", "", "2", "0", ""}; ///  Accion C=crear, M= Modificar
        //lValorParametros[0] = Session["numero_rueda"].ToString();
        //lValorParametros[1] = hndID.Value;
        //lValorParametros[2] = goInfo.cod_comisionista;
        //lValorParametros[3] = hndRonda.Value;
        //lValorParametros[4] = TxtCantidad.Text.Remove(',');
        //lValorParametros[6] = hndAccion.Value;
        if (TxtPrecio.Text != "")
            lValorParametros[5] = TxtPrecio.Text.Replace(",","");
        if (oError == "")
        {
            SqlDataReader lLector;
            lConexion.Abrir();
            try
            {
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetPosturaCompraId1", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        oError = lLector["error"].ToString() + "\\n";
                }
                if (oError == "")
                {
                    if (hndAccion.Value == "C")
                        sMensaje = "Posturas Ingresadas Correctamente";
                    else
                        sMensaje = "Posturas Actualizadas Correctamente";
                    Session["hora"] = "";
                    Toastr.Success(this, sMensaje);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                oError = "Error al realizar la oferta. " + ex.Message;
                lConexion.Cerrar();
            }
        }
        if (oError != "")
        {
            Toastr.Warning(this, oError);
        }
    }
}
