﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ELiminaIdND.aspx.cs"
    Inherits="Procesos_frm_ELiminaIdND" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor ="White" ></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br /><br /><br /><br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:ImageButton ID="imbRegresar" runat="server" ImageUrl="~/Images/Regresar.png"
                    Visible="false" OnClick="imbRegresar_Click" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" width="90%">
        <tr>
            <td colspan="2" align="center">
                <div>
                    <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                        AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" OnEditCommand="dtgConsulta_EditCommand"
                        HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="numero_rueda" HeaderText="rueda"></asp:BoundColumn>
                            <asp:BoundColumn DataField="descripcion" HeaderText="descripcion" ItemStyle-HorizontalAlign="left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="hora_ini" HeaderText="hora inicial" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="hora_fin" HeaderText="hora final" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="cnt_id_abi" HeaderText="IDs Abiertos" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Ids" EditText="Ids"></asp:EditCommandColumn>
                        </Columns>
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                    <asp:DataGrid ID="dtgIds" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                        Visible="false" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgIds_EditCommand" HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="numero_rueda" HeaderText="rueda"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_id" HeaderText="ID" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_negociacion" HeaderText="fecha neg"></asp:BoundColumn>
                            <asp:BoundColumn DataField="razon_social" HeaderText="operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_producto" HeaderText="producto" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_punto_entrega" HeaderText="punto entrega" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_periodo" HeaderText="periodo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="modalidad" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="precio" HeaderText="precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,##0.00}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cantidad_postura" HeaderText="cantidad" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cnt_posturas" HeaderText="posturas agresoras" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                        </Columns>
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </div>
                <asp:HiddenField id="hndRueda" runat="server"/>
            </td>
        </tr>
    </table>
</asp:Content>