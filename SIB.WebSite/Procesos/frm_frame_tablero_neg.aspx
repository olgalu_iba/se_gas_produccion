<%@ Page Language="C#" MasterPageFile="~/PlantillaPrincipal.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="frm_frame_tablero_neg.aspx.cs" Inherits="Procesos_frm_frame_tablero_neg" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" language="javascript">

        //        document.location = no;
        function confirmar() {
            return confirm("Esta seguro que desea ofertar!")
        }
        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }

        function formatoMiles(obj, evt) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /([0-9]*)$/i.exec(strValue)) != null) {
                obj.value = addCommas(T[1]);
            } else {
                alert('formato invalido ');
            }
        }
        function formatoMilesPre(obj, evt, decimales) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
                T[4] = T[4].substr(0, decimales);
                if (T[2] == '' && T[3] == '.') {
                    T[2] = 0;
                }
                obj.value = T[1] + addCommas(T[2]) + T[3] + T[4];
            }
            else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }

    </script>

    <div class="kt-container kt-grid__item kt-grid__item--fluid">

        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="kt-portlet">

                    <%--Head--%>
                    <div class="kt-portlet__head">

                        <%--Titulo--%>
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <asp:Label ID="lblTitle" Text="Tablero Negociaci�n Directa" runat="server" />
                            </h3>
                        </div>
                    </div>

                    <%--Contenido--%>
                    <div class="kt-portlet__body">
                        <%--Timer--%>
                        <asp:Timer ID="Timer1" OnTick="Timer1_Tick" runat="server" Interval="1000" />
                        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="Timer1" />
                            </Triggers>
                            <ContentTemplate>
                                <strong>
                                    <h5>
                                        <asp:Label ID="lblRelog" runat="server" Style="color: green"></asp:Label>
                                    </h5>
                                </strong>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:HiddenField ID="HiddenField1" runat="server" />

                        <%--Separador--%>
                        <div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>

                        <%--Botones--%>
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label"></div>
                            <div class="kt-portlet__head-toolbar">
                                <segas:Subasta ID="buttonsSubasta" runat="server" />
                                <%--Periodo--%>
                                <asp:DropDownList ID="ddlPeriodo" CssClass="selectpicker" OnSelectedIndexChanged="ddlPeriodo_SelectedIndexChanged" AutoPostBack="true" runat="server" />
                                <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <span data-toggle="kt-tooltip" title="Restablecer" data-placement="top">
                                            <asp:LinkButton ID="btnRestablecer" CssClass="btn btn-outline-brand btn-square" OnClick="btnRestablecer_Click" runat="server">
                                                <i class="flaticon2-refresh"></i>
                                            </asp:LinkButton>
                                        </span>
                                        <span data-toggle="kt-tooltip" title="Buscar" data-placement="top">
                                            <asp:LinkButton ID="btnBuscar" CssClass="btn btn-outline-brand btn-square" OnClick="btnBuscar_Click" runat="server">
                                        <i class="flaticon2-search-1"></i>
                                            </asp:LinkButton>
                                        </span>
                                        <span data-toggle="kt-tooltip" title="Nuevo" data-placement="top">
                                            <asp:LinkButton ID="btnNuevoC" CssClass="btn btn-outline-brand btn-square" OnClick="btnNuevoC_Click" runat="server">
                                        <i class="flaticon2-add-circular-button"></i>
                                            </asp:LinkButton>
                                        </span>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnRestablecer" />
                                        <asp:AsyncPostBackTrigger ControlID="btnNuevoC" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <asp:HiddenField ID="HndFechaRueda" runat="server" />
                        <asp:HiddenField ID="HndDiaHabil" runat="server" />
                        <asp:HiddenField ID="hndEstado" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <%--Compra--%>
        <div class="row">
            <div class="col-xl-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3>
                                <asp:Label ID="Label1" Text="Compra" class="kt-portlet__head-title nb" runat="server" />
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <%--Tablas Subasta--%>
                        <div class="table table-responsive">
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <asp:DataGrid ID="dtgPosturas2" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                        OnItemCommand="OnItemCommandC_Click2">
                                        <Columns>
                                            <%--0--%><asp:BoundColumn DataField="desc_punto_entrega" HeaderText="Punto / Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                            <%--1--%><asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                            <%--2--%><asp:BoundColumn DataField="cantidad" HeaderText="Cnt" DataFormatString="{0:###,###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                            <%--3--%><asp:BoundColumn DataField="precio" HeaderText="Pre" DataFormatString="{0:###,###,###,###,###,###,##0.00}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                            <%--4--%><asp:BoundColumn DataField="fecha_maxima_interes" HeaderText="Fecha Max." HeaderStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                            <%--5--%><asp:BoundColumn DataField="hora_maxima_interes" HeaderText="Hora Max." HeaderStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                            <%--6--%><asp:BoundColumn DataField="desc_periodo" HeaderText="Periodo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                            <%--7--%><asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Ini" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                            <%--8--%><asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Fin" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                            <%--9--%><asp:BoundColumn DataField="datos_contacto" HeaderText="Datos de Contacto" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="true"></asp:BoundColumn>
                                            <%--10--%><asp:TemplateColumn HeaderText="Mod">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imbModificar" runat="server" ToolTip="Modificar Oferta" ImageUrl="~/Images/modificar.gif" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <%--11--%><asp:TemplateColumn HeaderText="Elim">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imbEliminar" runat="server" ToolTip="Eliminar Oferta" ImageUrl="~/Images/eliminar.gif" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <%--12--%><asp:BoundColumn DataField="numero_postura" Visible="false"></asp:BoundColumn>
                                            <%--13--%><asp:BoundColumn DataField="codigo_operador" Visible="false"></asp:BoundColumn>
                                            <%--14--%><asp:BoundColumn DataField="destino_rueda" Visible="false"></asp:BoundColumn>
                                            <%--15--%><asp:BoundColumn DataField="codigo_punto_ruta" Visible="false"></asp:BoundColumn>
                                            <%--16--%><asp:BoundColumn DataField="codigo_periodo" Visible="false"></asp:BoundColumn>
                                        </Columns>
                                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                        <PagerStyle HorizontalAlign="Center"></PagerStyle>
                                    </asp:DataGrid>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>

            <%--Venta--%>
            <div class="col-xl-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3>
                                <asp:Label ID="lblTitulo" Text="Venta" class="kt-portlet__head-title nb" runat="server"></asp:Label>
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <%--Tablas Subasta--%>
                        <div class="table table-responsive">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:DataGrid ID="dtgPosturas" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                        OnItemCommand="OnItemCommandC_Click">
                                        <Columns>
                                            <%--0--%><asp:BoundColumn DataField="desc_punto_entrega" HeaderText="Punto / Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                            <%--1--%><asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                            <%--2--%><asp:BoundColumn DataField="cantidad" HeaderText="Cnt" DataFormatString="{0:###,###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                            <%--3--%><asp:BoundColumn DataField="precio" HeaderText="Pre" DataFormatString="{0:###,###,###,###,###,###,##0.00}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                            <%--4--%><asp:BoundColumn DataField="fecha_maxima_interes" HeaderText="Fecha Max." HeaderStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                            <%--5--%><asp:BoundColumn DataField="hora_maxima_interes" HeaderText="Hora Max." HeaderStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                            <%--6--%><asp:BoundColumn DataField="desc_periodo" HeaderText="Periodo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                            <%--7--%><asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Ini" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                            <%--8--%><asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Fin" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                            <%--9--%><asp:BoundColumn DataField="datos_contacto" HeaderText="Datos de Contacto" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="true"></asp:BoundColumn>
                                            <%--10--%><asp:TemplateColumn HeaderText="Mod">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imbModificar" runat="server" ToolTip="Modificar Oferta" ImageUrl="~/Images/modificar.gif" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <%--11--%><asp:TemplateColumn HeaderText="Elim">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imbEliminar" runat="server" ToolTip="Eliminar Oferta" ImageUrl="~/Images/eliminar.gif" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <%--12--%><asp:BoundColumn DataField="numero_postura" Visible="false"></asp:BoundColumn>
                                            <%--13--%><asp:BoundColumn DataField="codigo_operador" Visible="false"></asp:BoundColumn>
                                            <%--14--%><asp:BoundColumn DataField="destino_rueda" Visible="false"></asp:BoundColumn>
                                            <%--15--%><asp:BoundColumn DataField="codigo_punto_ruta" Visible="false"></asp:BoundColumn>
                                            <%--16--%><asp:BoundColumn DataField="codigo_periodo" Visible="false"></asp:BoundColumn>
                                        </Columns>
                                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                        <PagerStyle HorizontalAlign="Center"></PagerStyle>
                                    </asp:DataGrid>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--Modal Ingreso Posturas--%>
    <div class="modal fade" id="mdlIngresoPostura" tabindex="-1" role="dialog" aria-labelledby="mdlIngresoPosturaLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlIngresoPosturaInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlIngresoPosturaLabel">Ingreso Posturas</h5>
                        </div>
                        <div class="modal-body">
                            <h5>
                                <label id="lblMdlIngresoPostura" runat="server"></label>
                            </h5>

                            <hr>

                            <div class="row">

                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblPtoEnt" AssociatedControlID="lblDescPuntoiRuta" runat="server"></asp:Label>
                                        <asp:Label ID="lblDescPuntoiRuta" CssClass="form-control" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="lblDescPeriodo" runat="server">Periodo Entrega</asp:Label>
                                        <asp:Label ID="lblDescPeriodo" CssClass="form-control" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div id="TrFuente" class="col-sm-12 col-md-6 col-lg-6" visible="false" runat="server">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="ddlFuente" runat="server">Fuente</asp:Label>
                                        <asp:DropDownList ID="ddlFuente" CssClass="form-control" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                                <div id="trA�o" class="col-sm-12 col-md-6 col-lg-6" visible="false" runat="server">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="txtA�o" runat="server"> No. A�os</asp:Label>
                                        <asp:TextBox ID="txtA�o" CssClass="form-control" runat="server" MaxLength="2"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender runat="server" TargetControlID="txtA�o" FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="TxtFechaMaxInteres" runat="server">Fecha M�xima para manifestar inter�s</asp:Label>
                                        <asp:TextBox ID="TxtFechaMaxInteres" CssClass="form-control datepicker" Width="100%" placeholder="yyyy/mm/dd" MaxLength="10" ClientIDMode="Static" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="TxtHoraMaxInt" runat="server">Hora M�xima para manifestar inter�s</asp:Label>
                                        <asp:TextBox ID="TxtHoraMaxInt" CssClass="form-control" runat="server" MaxLength="5"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RevTxtHoraMaxInt" ControlToValidate="TxtHoraMaxInt" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora m�xima para manifestar interes"> * </asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="TxtPrecio" runat="server">Precio</asp:Label>
                                        <asp:TextBox ID="TxtPrecio" CssClass="form-control" runat="server" onkeyup="return formatoMilesPre(this,event,2);"></asp:TextBox>
                                        <asp:Label ID="lblUndPre" runat="server"></asp:Label>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtPrecio" runat="server" Enabled="True" FilterType="Custom, Numbers"
                                            TargetControlID="TxtPrecio" ValidChars=",."></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="TxtCantidad" runat="server">Cantidad</asp:Label>
                                        <asp:TextBox ID="TxtCantidad" CssClass="form-control" runat="server" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
                                        <asp:Label ID="lblUndCnt" runat="server"></asp:Label>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTEBTxtCantidad" runat="server" Enabled="True" FilterType="Custom, Numbers"
                                            TargetControlID="TxtCantidad" ValidChars=","></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="ddlPunta" runat="server">Punta</asp:Label>
                                        <asp:DropDownList ID="ddlPunta" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="">Seleccione</asp:ListItem>
                                            <asp:ListItem Value="C">Compra</asp:ListItem>
                                            <asp:ListItem Value="V">Venta</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="txtFechaIni" runat="server">Fecha Inicial</asp:Label>
                                        <asp:TextBox ID="txtFechaIni" CssClass="form-control datepicker" Width="100%" placeholder="yyyy/mm/dd" MaxLength="10" OnTextChanged="TxtFechaIni_TextChanged" AutoPostBack="true" ClientIDMode="Static" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="txtFechaFin" runat="server">Fecha Final</asp:Label>
                                        <asp:TextBox ID="txtFechaFin" CssClass="form-control datepicker" Width="100%" placeholder="yyyy/mm/dd" MaxLength="10" ClientIDMode="Static" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="TxtDatosContacto" runat="server">Datos Contacto</asp:Label>
                                        <asp:TextBox ID="TxtDatosContacto" CssClass="form-control" runat="server" MaxLength="500" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseIngresoPostura_Click" runat="server" />
                            <asp:Button ID="btnOfertar" Text="Crear" class="btn btn-primary" OnClientClick="return confirmar(); " OnClick="btnOfertar_Click" runat="server" />
                        </div>

                        <asp:HiddenField ID="hdfNoRueda" runat="server" />
                        <asp:HiddenField ID="hdfNoid" runat="server" />
                        <asp:HiddenField ID="hdfCodigoPuntoRuta" runat="server" />
                        <asp:HiddenField ID="hdfCodigoPeriodo" runat="server" />
                        <asp:HiddenField ID="hndAccion" runat="server" />
                        <asp:HiddenField ID="hdfMedidaTiempo" runat="server" />
                        <asp:HiddenField ID="hdfErrorFecha" runat="server" />
                        <asp:HiddenField ID="hdfTipoMercado" runat="server" />
                        <asp:HiddenField ID="hdfDestinoRueda" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
