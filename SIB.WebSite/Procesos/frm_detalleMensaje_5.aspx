﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_detalleMensaje_5.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_detalleMensaje_5" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">
        //        document.location = no;
        function confirmar() {
            return confirm("Esta seguro que desea enviar el mensaje!")
        }

    </script>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td6" align="center">
                        <asp:Label ID="lblTitulo" runat="server" Text="ENVIO DE MENSAJES" Font-Bold="true"></asp:Label>
                        <asp:DropDownList ID="ddlOperador" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="td6" align="center">
                        <asp:Label ID="lblOperador" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="td6" align="center">
                        <asp:TextBox ID="TxtMensaje" runat="server" Width="300px" TextMode="MultiLine" Rows="3"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" class="td6">
                        <br />
                        <asp:Button ID="btnMensaje" Text="Enviar" runat="server" OnClientClick="return confirmar();"
                            OnClick="btnMensaje_Click" ValidationGroup="comi" CausesValidation="true" />
                        <asp:HiddenField ID="hdfCodOperador" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" class="td6">
                        <asp:DataGrid ID="dtgSubasta5" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                            ItemStyle-CssClass="td2" HeaderStyle-CssClass="th2" runat="server">
                            <ItemStyle CssClass="td2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="15px">
                            </ItemStyle>
                            <Columns>
                                <%--0--%><asp:BoundColumn DataField="hora_mensaje" HeaderText="hora" ItemStyle-HorizontalAlign="Left">
                                </asp:BoundColumn>
                                <%--1--%><asp:BoundColumn DataField="mensaje" HeaderText="mensaje" ItemStyle-HorizontalAlign="Left">
                                </asp:BoundColumn>
                                <%--2--%><asp:BoundColumn DataField="codigo_operador" Visible="false"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="th2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="13px">
                            </HeaderStyle>
                        </asp:DataGrid>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>