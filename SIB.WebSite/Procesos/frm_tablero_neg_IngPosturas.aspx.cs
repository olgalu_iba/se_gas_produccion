﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_tablero_neg_IngPosturas : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    SqlDataReader lLector;
    String strRutaArchivo;
    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString();
            lConexion = new clConexion(goInfo);
            if (!IsPostBack)
            {
                try
                {
                    hdfNoid.Value = "0";
                    hndAccion.Value = "C";

                    lConexion.Abrir();//20170315 rq012- ajuste de tablero
                    ddlFuente.Items.Clear();//20170315 rq012- ajuste de tablero
                    LlenarControles(lConexion.gObjConexion, ddlFuente, "m_pozo ", "  estado = 'A' And ind_campo_pto = 'C'  order by descripcion", 0, 1); //20170315 rq012- ajuste de tablero

                    if (this.Request.QueryString["ID"] != null && this.Request.QueryString["ID"].ToString() != "" && this.Request.QueryString["ID"].ToString() != "0") //20170315 rq012- ajuste de tablero
                    {
                        hdfNoid.Value = this.Request.QueryString["ID"].ToString();
                        //20170315 rq012- ajuste de tablero
                        hndAccion.Value = "M";
                        //// Modificación del ID YA sea por el Dueño de la Oferta o por el Agresor
                        hdfNoid.Value = this.Request.QueryString["ID"].ToString();
                        string[] lsNombreParametros = { "@P_punta_postura", "@P_numero_postura", "@P_codigo_operador" };
                        SqlDbType[] lTipoparametros = { SqlDbType.Char, SqlDbType.Int, SqlDbType.Int };
                        string[] lValorParametros = { "", hdfNoid.Value, goInfo.cod_comisionista };
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetPosturaTabNegociacion", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                        if (goInfo.mensaje_error == "")
                        {
                            if (lLector.HasRows)
                            {
                                lLector.Read();
                                //// Lleno los controles de la pantalla
                                lblDescPuntoiRuta.Text = lLector["desc_punto_entrega"].ToString();
                                lblDescPeriodo.Text = lLector["desc_periodo"].ToString();
                                TxtFechaMaxInteres.Text = lLector["fecha_maxima_interes"].ToString();
                                TxtHoraMaxInt.Text = lLector["hora_maxima_interes"].ToString();
                                TxtPrecio.Text = lLector["precio"].ToString();
                                TxtCantidad.Text = lLector["cantidad"].ToString();
                                txtFechaIni.Text = lLector["fecha_inicial"].ToString();
                                txtFechaFin.Text = lLector["fecha_final"].ToString();
                                txtAño.Text = lLector["no_anos"].ToString();
                                ddlPunta.SelectedValue = lLector["punta_postura"].ToString();
                                hdfMedidaTiempo.Value = lLector["medida_tiempo"].ToString();
                                if (hdfMedidaTiempo.Value == "L")
                                    trAño.Visible = true;
                                if (hdfMedidaTiempo.Value == "U")
                                    txtFechaFin.Enabled = true;
                                else
                                    txtFechaFin.Enabled = false;
                                TxtDatosContacto.Text = lLector["datos_contacto"].ToString();
                                //20170315 rq012 tablero 
                                try
                                {
                                    ddlFuente.SelectedValue = lLector["codigo_fuente"].ToString();
                                }
                                catch (Exception ex)
                                {
                                }

                                hdfCodigoPuntoRuta.Value = lLector["codigo_punto_ruta"].ToString();
                                hdfCodigoPeriodo.Value = lLector["codigo_periodo"].ToString();
                                hdfNoRueda.Value = lLector["numero_rueda"].ToString();
                                hdfTipoMercado.Value = lLector["tipo_mercado"].ToString();
                                hdfDestinoRueda.Value = lLector["destino_rueda"].ToString();

                                lLector.Close();
                                lLector.Dispose();
                                btnOfertar.Text = "";
                                btnOfertar.Visible = true;
                                btnOfertar.Text = "Actualizar";
                            }
                            else
                            {
                                btnOfertar.Visible = false;
                            }
                        }
                        //20170315 fin rq012- ajuste de tablero
                    }
                    else //20170315 fin rq012- ajuste de tablero
                    {
                        hdfCodigoPuntoRuta.Value = Session["codigo_punto"].ToString();
                        hdfCodigoPeriodo.Value = Session["codigo_periodo"].ToString();
                        hdfNoRueda.Value = Session["numero_rueda"].ToString();
                        hdfTipoMercado.Value = Session["tipo_mercado"].ToString();
                        hdfDestinoRueda.Value = Session["destino_rueda"].ToString();
                    }


                    //if (this.Request.QueryString["punta"] != null && this.Request.QueryString["punta"].ToString() != "")
                    //    hdfPunta.Value = this.Request.QueryString["punta"].ToString();

                    //if (hdfPunta.Value == "C")
                    lblTituo.Text = "INGRESO POSTURAS";
                    //else
                    //    lblTituo.Text = "INGRESO POSTURAS DE VENTA";

                    if (hdfDestinoRueda.Value == "G")
                    {
                        lblPtoEnt.Text = "Pto Entrega: ";
                        lblUndPre.Text = "USD/MBTU";
                        lblUndCnt.Text = "MBTUD";
                    }
                    else
                    {
                        lblPtoEnt.Text = "Ruta: ";
                        lblUndPre.Text = "USD/KPC";
                        lblUndCnt.Text = "KPCD";
                    }

                    if (hdfTipoMercado.Value == "P" && hdfDestinoRueda.Value == "G")
                    {
                        TrFuente.Visible = true;
                    }
                    else
                        TrFuente.Visible = false;
                    if (hdfNoid.Value == "0")
                    {
                        if (hdfDestinoRueda.Value == "G")
                            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_pozo", " codigo_pozo =  " + hdfCodigoPuntoRuta.Value);
                        else
                            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_ruta_snt", " codigo_ruta = " + hdfCodigoPuntoRuta.Value);
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lblDescPuntoiRuta.Text = lLector["descripcion"].ToString();
                        }
                        lLector.Dispose();
                        lLector.Close();

                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_periodos_entrega", " codigo_periodo =  " + hdfCodigoPeriodo.Value);
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lblDescPeriodo.Text = lLector["descripcion"].ToString();
                            hdfMedidaTiempo.Value = lLector["medida_tiempo"].ToString();
                            if (hdfMedidaTiempo.Value == "L")
                                trAño.Visible = true;
                            if (hdfMedidaTiempo.Value == "U")
                                txtFechaFin.Enabled = true;
                            else
                                txtFechaFin.Enabled = false;
                        }
                        lLector.Dispose();
                        lLector.Close();

                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador =  " + goInfo.cod_comisionista);
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            TxtDatosContacto.Text = lLector["telefono"].ToString() + " - " + lLector["e_mail"].ToString() + ".";
                        }
                        lLector.Dispose();
                        lLector.Close();
                    }
                    lConexion.Cerrar(); //20170315 rq012 tablero 
                }
                catch (Exception ex)
                {

                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOfertar_Click(object sender, EventArgs e)
    {
        string oError = "";
        DateTime ldFecha;

        if (TxtCantidad.Text == "")
            oError += "Debe digitar la cantidad de la Oferta\\n";
        if (TxtPrecio.Text == "")
            oError += "Debe digitar la precio de la Oferta\\n";
        else
        {
            string sOferta = TxtPrecio.Text.Replace(",", "");
            int iPos = sOferta.IndexOf(".");
            if (iPos > 0)
                if (sOferta.Length - iPos > 3)
                    oError += "Se permiten máximo 2 decimales en el precio\\n";
            try
            {
                if (Convert.ToDouble(sOferta) <= 0)
                    oError += "El precio debe ser mayor que cero\\n";
            }
            catch (Exception ex)
            {
                oError += "El precio digitado no es válido\\n";
            }
        }
        if (TxtFechaMaxInteres.Text.Trim().Length <= 0)
            oError += "Debe ingresar la Fecha Máxima para manifestar interés\\n";
        if (TxtHoraMaxInt.Text.Trim().Length <= 0)
            oError += "Debe ingresar la Hora Máxima para manifestar interés\\n";
        else
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaMaxInteres.Text.Trim());
                if (Convert.ToDateTime(TxtFechaMaxInteres.Text.Trim() + ' ' + TxtHoraMaxInt.Text.Trim()) < DateTime.Now.Date)
                    oError += "La Fecha-Hora Máxima para manifestar interés NO puede ser menor a la fecha-hora actual.!\\n";
            }
            catch (Exception ex)
            {
                oError += "Valor Invalido en Fecha Máxima para manifestar interés.!\\n";
            }
        }
        SqlDataReader lLector;
        if (txtFechaIni.Text == "")
            oError += "Debe digitar la fecha de entrega inicial.!\\n";
        else
        {
            try
            {
                ldFecha = Convert.ToDateTime(txtFechaIni.Text.Trim());
            }
            catch (Exception ex)
            {
                oError += "Valor Invalido en Fecha Inicial.!\\n";
            }
        }
        if (hdfMedidaTiempo.Value == "U")
        {
            if (txtFechaFin.Text == "")
                oError += "Error en la fecha de entrega final.!\\n";
            else
            {
                try
                {
                    ldFecha = Convert.ToDateTime(txtFechaFin.Text.Trim());
                    if (Convert.ToDateTime(txtFechaFin.Text.Trim()) < Convert.ToDateTime(txtFechaIni.Text.Trim()))
                        oError += "La Fecha Final NO puede ser menor que la Fecha Inicial.!\\n";
                }
                catch (Exception ex)
                {
                    oError += "Valor Invalido en Fecha Final.!\\n";
                }
            }

        }
        else
        {
            if (txtFechaFin.Text == "")
                oError += "Error en la fecha de entrega final.!\\n";
        }
        if (hdfMedidaTiempo.Value == "L" && txtAño.Text == "")
            oError += "Debe ingresar los años para el periodo de tiempo multianual.!\\n";
        if (ddlPunta.SelectedValue == "")
            oError += "Debe seleccionar la punta de la oferta.!\\n";
        try
        {
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_subasta opeS, t_rueda rue, m_operador ope, m_tipos_operador tpo", " opeS.codigo_tipo_subasta = 5 and rue.numero_rueda = " + hdfNoRueda.Value + " and opeS.codigo_tipo_rueda = rue.codigo_tipo_rueda and ope.codigo_operador = " + goInfo.cod_comisionista + " and ope.tipo_operador = tpo.sigla and opeS.codigo_tipo_operador = tpo.codigo_tipo_operador  and opeS.punta ='" + ddlPunta.SelectedValue + "' and opeS.estado ='A'");
            if (!lLector.HasRows)
                oError += "El tipo de operador no está autorizado para posturas en la punta seleccionada.!\\n";
            lLector.Dispose();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lConexion.Cerrar();
        }
        /// Validaciones para Mercado Primario de Gas
        if (hdfTipoMercado.Value == "P" && hdfDestinoRueda.Value == "G")
        {
            if (ddlFuente.SelectedValue == "0")
                oError += "Debe Ingresar la  Fuente.!\\n";
            if (!DelegadaBase.Servicios.ValidarExistencia("m_pozo poz, m_caracteristica_sub carC", " poz.codigo_pozo = " + ddlFuente.SelectedValue + "  and carC.codigo_tipo_subasta = 5 and carC.tipo_caracteristica ='C' and carC.destino_rueda ='G' and carC.tipo_mercado='P' and carC.estado ='A' and codigo_caracteristica = poz.codigo_tipo_campo", goInfo))
                oError += "El tipo de campo no está parametrizado para el tipo de mercado y producto. \\n";
            if (DelegadaBase.Servicios.ValidarExistencia("m_pozo poz, m_campo_periodo cam ", " poz.codigo_pozo = " + ddlFuente.SelectedValue + "  and poz.codigo_tipo_campo= cam.codigo_tipo_campo and cam.codigo_tipo_subasta = 5 and cam.destino_rueda='G' and cam.tipo_mercado='P' and cam.estado ='A' and (cast('" + TxtFechaMaxInteres.Text.Trim() + "' as datetime) < cam.fecha_inicial or cast('" + TxtFechaMaxInteres.Text.Trim() + "' as datetime) > cast(convert(varchar(10),cam.fecha_final,111) as datetime))", goInfo))
                oError += "La fecha de negociación no está dentro del periodo para registro de negociaciones para el tipo de campo.\\n";
        }
        if (TxtDatosContacto.Text.Trim().Length <= 0)
            oError += "Debe Ingresar Datos de Contacto.!\\n";
        if (oError == "")
        {
            lConexion.Abrir();
            string[] lsNombreParametros = { "@P_numero_postura","@P_codigo_operador", "@P_punta_postura", "@P_numero_rueda", "@P_destino_rueda","@P_codigo_punto_ruta",
                                            "@P_codigo_periodo","@P_fecha_maxima_interes","@P_hora_maxima_interes","@P_cantidad","@P_precio",
                                            "@P_fecha_inicial","@P_fecha_final","@P_no_anos","@P_datos_contacto", "@P_accion", "@P_codigo_fuente" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar,SqlDbType.Int,SqlDbType.VarChar,SqlDbType.Int,
                                            SqlDbType.Int,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int,SqlDbType.Decimal, 
                                            SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar , SqlDbType.Int };
            string[] lValorParametros = { hdfNoid.Value, goInfo.cod_comisionista, ddlPunta.SelectedValue , hdfNoRueda.Value, hdfDestinoRueda.Value , hdfCodigoPuntoRuta.Value,
                                          hdfCodigoPeriodo.Value, TxtFechaMaxInteres.Text, TxtHoraMaxInt.Text, TxtCantidad.Text.Replace(",",""), TxtPrecio.Text, 
                                          txtFechaIni.Text, txtFechaFin.Text ,"0" ,TxtDatosContacto.Text.Trim(), hndAccion.Value, ddlFuente.SelectedValue  }; ///  Accion C=crear, M= Modificar

            try
            {
                if (hdfMedidaTiempo.Value == "L")
                    lValorParametros[14] = txtAño.Text;
                string oMensaje = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetPosturaTableroNeg", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error == "")
                {
                    Session["refrescar"] = "S";
                    if (hndAccion.Value == "C")
                        Toastr.Success(this, "Oferta Creada Correctamente.");
                   
                    else
                        Toastr.Success(this, "Oferta Modificada Correctamente!");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                }
                else
                    Toastr.Warning(this, "Problemas en la Creacion de la Postura. " + goInfo.mensaje_error.ToString());

        
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                oError = "Error al realizar la oferta. " + ex.Message;
                lConexion.Cerrar();
            }
        }
        if (oError != "")
        {
            Toastr.Warning(this, oError);
         
            lConexion.Cerrar();
        }
    }
    protected void TxtFechaIni_TextChanged(object sender, EventArgs e)
    {
        //// Calculo de la Fecha Final de acuerdo al periodo de entrega ingresado
        DateTime ldFecha;
        string lsDisSemana = "0";
        string lsFecha = "";
        int liAnos = 0;
        hdfErrorFecha.Value = "";
        txtFechaFin.Text = "";
        try
        {
            if (txtFechaIni.Text.Trim().Length > 0)
            {
                ldFecha = Convert.ToDateTime(txtFechaIni.Text.Trim());
                if (hdfMedidaTiempo.Value != "I")
                {
                    if (ldFecha > DateTime.Now.Date)
                    {

                        //// Valculo Fecha Final cuando es Intradiario
                        if (hdfMedidaTiempo.Value == "D")
                            txtFechaFin.Text = txtFechaIni.Text.Trim();
                        //if (hdfMedidaTiempo.Value == "D")
                        //    lblFechaFin.Text = Convert.ToDateTime(TxtFechaInicial.Text.Trim()).AddDays(1).ToString();
                        if (hdfMedidaTiempo.Value == "S")
                        {
                            lsDisSemana = Convert.ToDateTime(txtFechaIni.Text.Trim()).DayOfWeek.ToString();
                            if (lsDisSemana != "Monday")
                                hdfErrorFecha.Value = "La Fecha de Inicio debe ser el primer dia de la Semana. YA que escogio Periodo Semanal.";
                            else
                            {
                                lsFecha = Convert.ToDateTime(txtFechaIni.Text.Trim()).AddDays(6).ToString();
                            }
                        }
                        if (hdfMedidaTiempo.Value == "M")
                        {
                            if (Convert.ToInt32(txtFechaIni.Text.Trim().Substring(8, 2)) != 1)
                                hdfErrorFecha.Value = "La Fecha de Inicio debe ser el primer dia del Mes. YA que escogio Periodo Mensual.";
                            else
                            {
                                lsFecha = Convert.ToDateTime(txtFechaIni.Text.Trim()).AddMonths(1).AddDays(-1).ToString();
                            }
                        }
                        if (hdfMedidaTiempo.Value == "T")
                        {
                            int liMes = Convert.ToInt32(txtFechaIni.Text.Trim().Substring(5, 2));
                            if (liMes != 3 && liMes != 6 && liMes != 9 && liMes != 12)
                                hdfErrorFecha.Value = "El Mes de la Fecha de Inicio es Diferente al mes de inicio del trimestre.";
                            else
                            {
                                if (Convert.ToInt32(txtFechaIni.Text.Trim().Substring(8, 2)) != 1)
                                    hdfErrorFecha.Value = "La Fecha de Inicio debe ser el primer dia del Mes. YA que escogio Periodo Trimestral.";
                                else
                                {
                                    lsFecha = Convert.ToDateTime(txtFechaIni.Text.Trim()).AddMonths(3).AddDays(-1).ToString();
                                }

                            }
                        }
                        if (hdfMedidaTiempo.Value == "A")
                        {
                            if (Convert.ToInt32(txtFechaIni.Text.Trim().Substring(5, 2)) != 12)
                                hdfErrorFecha.Value = "El Mes de la Fecha de Inicio es Diferente al mes de inicio del año. YA que escogio Periodo Anual.";
                            else
                            {
                                if (Convert.ToInt32(txtFechaIni.Text.Trim().Substring(8, 2)) != 1)
                                    hdfErrorFecha.Value = "La Fecha de Inicio debe ser el primer dia del Mes. YA que escogio Periodo Anual.";
                                else
                                {
                                    lsFecha = Convert.ToDateTime(txtFechaIni.Text.Trim()).AddMonths(12).AddDays(-1).ToString();
                                }

                            }
                        }
                        if (hdfMedidaTiempo.Value == "L")
                        {
                            if (txtAño.Text.Trim().Length <= 0)
                                hdfErrorFecha.Value = "Debe Ingresar el No. de Años. YA que escogio Periodo MultiAnual.";
                            else
                            {
                                try
                                {
                                    liAnos = Convert.ToInt32(txtAño.Text.Trim());
                                    if (Convert.ToInt32(txtFechaIni.Text.Trim().Substring(5, 2)) != 12)
                                        hdfErrorFecha.Value = "El Mes de la Fecha de Inicio es Diferente al mes de inicio del año. YA que escogio Periodo MultiAnual.";
                                    else
                                    {
                                        if (Convert.ToInt32(txtFechaIni.Text.Trim().Substring(8, 2)) != 1)
                                            hdfErrorFecha.Value = "La Fecha de Inicio debe ser el primer dia del Mes. YA que escogio Periodo MultiAnual.";
                                        else
                                        {
                                            lsFecha = Convert.ToDateTime(txtFechaIni.Text.Trim()).AddYears(Convert.ToInt32(txtAño.Text.Trim())).AddDays(-1).ToString();
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    hdfErrorFecha.Value = "Valor Inválido en el Campo de No. de Años del periodo MultiAnual.";
                                }
                            }
                        }
                        try
                        {
                            if (ldFecha <= Convert.ToDateTime(TxtFechaMaxInteres.Text))
                                hdfErrorFecha.Value += "\\nLa Fecha de Inicio debe ser mayor que la fecha máxima para demostrar interes.";
                        }
                        catch
                        {
                            hdfErrorFecha.Value += "\\nError en la fecha máxima para demostrar interes.";
                        }
                    }
                    else
                        hdfErrorFecha.Value = "La Fecha Inicial debe Ser mayor a la Fecha del Día.";
                }
                else
                {
                    txtFechaFin.Text = txtFechaIni.Text.Trim();
                    if (ldFecha < Convert.ToDateTime(TxtFechaMaxInteres.Text))
                        hdfErrorFecha.Value += "\\nLa Fecha de Inicio debe ser mayor o igual que la fecha máxima para demostrar interes.";
                }

                if (hdfErrorFecha.Value != "")
                    Toastr.Warning(this, hdfErrorFecha.Value);
             
                else
                    if (lsFecha != "")
                        txtFechaFin.Text = lsFecha.Substring(6, 4) + "/" + lsFecha.Substring(3, 2) + "/" + lsFecha.Substring(0, 2);
            }
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "Valor Invalido en Fecha Inicial");
           
        }
    }
}