﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

//using System.Windows.Forms;



public partial class Procesos_frm_posturas_1 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    //String strRutaArchivo;
    private System.TimeSpan diffResult;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            if (!IsPostBack)
            {
                try
                {
                    hndCodSubasta.Value = Session["numero_rueda"].ToString();
                    if (hndCodSubasta.Value == "")
                        hndCodSubasta.Value = "0";
                    lConexion1 = new clConexion(goInfo);
                    //hndCodSubasta.Value = "21";
                    HndFechaRueda.Value = DateTime.Now.ToShortDateString();
                    HndFechaRueda.Value = HndFechaRueda.Value.Substring(6, 4) + "/" + HndFechaRueda.Value.Substring(3, 2) + "/" + HndFechaRueda.Value.Substring(0, 2);
                    cargarDatosGrilla();
                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, ex.Message);
                }
            }
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, ex.Message, "Session Expiro. " + ex.Message, 50000);
        }
    }
    /// <summary>
    /// Nombre: cargarDatosGrilla
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para Cargar los datos en la Grio.
    /// Modificacion:
    /// </summary>
    private void cargarDatosGrilla()
    {
        if (hndCodSubasta.Value == "0")
        {
            dtgSubasta1.Columns.Clear();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.contratos.location= 'frm_contratos_1.aspx';", true);
        }
        else
        {
            try
            {
                if (Session["tipo_subasta"].ToString() == "2")
                {
                    dtgSubasta1.Columns[5].HeaderText = "Cantidad_excedentaria";
                    dtgSubasta1.Columns[7].Visible = false;
                }
                else
                {
                    dtgSubasta1.Columns[5].HeaderText = "Cantidad venta";
                    dtgSubasta1.Columns[7].Visible = true;
                }
                //determina las columnas para el subastatodr o los operaores
                string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { hndCodSubasta.Value, goInfo.cod_comisionista };
                lConexion.Abrir();
                dtgSubasta1.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaPuja1", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgSubasta1.DataBind();
                lConexion.Cerrar();
                ImageButton imbControl;
                if (goInfo.cod_comisionista != "0")
                {
                    dtgSubasta1.Columns[19].Visible = false;
                    dtgSubasta1.Columns[20].Visible = false;
                }
                else
                {
                    dtgSubasta1.Columns[12].Visible = false;
                    dtgSubasta1.Columns[13].Visible = false;
                    dtgSubasta1.Columns[14].Visible = false;
                    dtgSubasta1.Columns[21].Visible = false;
                    foreach (DataGridItem Grilla in this.dtgSubasta1.Items)
                    {
                        imbControl = (ImageButton)Grilla.Cells[20].Controls[1];
                        imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_definirPrecio_1.aspx?ID=" + Grilla.Cells[0].Text + "&ronda=" + Grilla.Cells[11].Text + "&precio_reserva=" + Grilla.Cells[4].Text + "','Ofertar','width=350,height=250,left=350,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                        imbControl = (ImageButton)Grilla.Cells[22].Controls[1];
                        imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_CurvaAgregada.aspx?ID=" + Grilla.Cells[0].Text + "','Grafica','width=900,height=500,left=250,top=90,status=no,location=0,menubar=no,toolbar=no,resizable=yes,scrollbars=yes');");
                    }
                }
                if (Session["punta"].ToString() == "V")
                {
                    dtgSubasta1.Columns[12].Visible = false;
                    dtgSubasta1.Columns[13].Visible = false;
                    dtgSubasta1.Columns[14].Visible = false;
                    foreach (DataGridItem Grilla in this.dtgSubasta1.Items)
                    {
                        imbControl = (ImageButton)Grilla.Cells[22].Controls[1];
                        imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_CurvaAgregada.aspx?ID=" + Grilla.Cells[0].Text + "','Grafica','width=900,height=500,left=250,top=90,status=no,location=0,menubar=no,toolbar=no,resizable=yes,scrollbars=yes');");
                        if (Grilla.Cells[15].Text != "X")
                        {
                            imbControl = (ImageButton)Grilla.Cells[21].Controls[1];
                            imbControl.ImageUrl = "~/Images/quitar.gif";
                            imbControl.Enabled = false;
                        }
                        else
                        {
                            imbControl = (ImageButton)Grilla.Cells[21].Controls[1];
                            imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_cantidadAdcV_1.aspx?ID=" + Grilla.Cells[0].Text + "&ronda=" + Grilla.Cells[11].Text + "','Ofertar','width=350,height=250,left=350,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                        }
                    }

                }
                if (Session["punta"].ToString() == "C")
                {
                    dtgSubasta1.Columns[21].Visible = false;
                    foreach (DataGridItem Grilla in this.dtgSubasta1.Items)
                    {
                        imbControl = (ImageButton)Grilla.Cells[22].Controls[1];
                        imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_CurvaAgregada.aspx?ID=" + Grilla.Cells[0].Text + "','Grafica','width=900,height=500,left=250,top=90,status=no,location=0,menubar=no,toolbar=no,resizable=yes,scrollbars=yes');");
                        if (Grilla.Cells[15].Text != "B" || Grilla.Cells[17].Text == "N")
                        {
                            imbControl = (ImageButton)Grilla.Cells[12].Controls[1];
                            imbControl.ImageUrl = "~/Images/quitar.gif";
                            imbControl.Enabled = false;
                            imbControl = (ImageButton)Grilla.Cells[13].Controls[1];
                            imbControl.ImageUrl = "~/Images/quitar.gif";
                            imbControl.Enabled = false;
                            imbControl = (ImageButton)Grilla.Cells[14].Controls[1];
                            imbControl.ImageUrl = "~/Images/quitar.gif";
                            imbControl.Enabled = false;
                        }
                        else
                        {
                            if (Grilla.Cells[6].Text != "0")
                            {
                                imbControl = (ImageButton)Grilla.Cells[12].Controls[1];
                                imbControl.ImageUrl = "~/Images/quitar.gif";
                                imbControl.Enabled = false;
                                imbControl = (ImageButton)Grilla.Cells[13].Controls[1];
                                imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_IngresoOfertaCompra_1.aspx?ID=" + Grilla.Cells[0].Text + "&accion=M&ronda=" + Grilla.Cells[11].Text + "&cantidad_ant=" + Grilla.Cells[18].Text + "','Ofertar','width=350,height=250,left=350,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                            }
                            else
                            {
                                imbControl = (ImageButton)Grilla.Cells[13].Controls[1];
                                imbControl.ImageUrl = "~/Images/quitar.gif";
                                imbControl.Enabled = false;
                                imbControl = (ImageButton)Grilla.Cells[14].Controls[1];
                                imbControl.ImageUrl = "~/Images/quitar.gif";
                                imbControl.Enabled = false;
                                imbControl = (ImageButton)Grilla.Cells[12].Controls[1];
                                imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_IngresoOfertaCompra_1.aspx?ID=" + Grilla.Cells[0].Text + "&accion=C&ronda=" + Grilla.Cells[11].Text + "&cantidad_ant=" + Grilla.Cells[18].Text + "','Ofertar','width=350,height=270,left=350,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no');");

                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, ex.Message);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.contratos.location= 'frm_contratos_1.aspx';", true);
        }
    }

    public void OnItemCommand_Click(object sender, DataGridCommandEventArgs e)
    {
        string oError = "";
        string oBoton = ((System.Web.UI.WebControls.ImageButton)e.CommandSource).ID.ToString();

        if (oBoton == "imbEliminar")
        {
            try
            {
                string[] lsNombreParametros = { "@P_numero_id", "@P_codigo_operador", "@P_ronda" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { e.Item.Cells[0].Text, goInfo.cod_comisionista, e.Item.Cells[11].Text };
                SqlDataReader lLector;
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_DelPostura1", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    oError += lLector["error"].ToString();
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                }
                else
                {
                    oError = "Oferta eliminada correctamente";
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    cargarDatosGrilla();
                }
            }
            catch (Exception ex)
            {
                oError = "Error al eliminar la oferta. " + ex.Message;
            }
            Toastr.Warning(this, oError);

        }
    }
}
