﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;
using AjaxControlToolkit;
using Segas.Web.Elements;

public partial class Procesos_frm_posturas_5 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    //String strRutaArchivo;
    private System.TimeSpan diffResult;
    static String lsCadenaCon;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            //Panel1.Style.Add("position", "absolute");
            //Panel1.Style.Add(HtmlTextWriterStyle.Top, "300px");
            //Panel1.Style.Add(HtmlTextWriterStyle.Left, "500px");

            try
            {
                lsCadenaCon = "Data Source=" + goInfo.Servidor.ToString() + "; Initial Catalog=" + goInfo.BaseDatos.ToString() + ";User ID = " + goInfo.Usuario.ToString() + "; Password=" + goInfo.Password.ToString();
                lConexion1 = new clConexion(goInfo);
                hndCodSubasta.Value = Session["numero_rueda"].ToString();
                HndFechaRueda.Value = DateTime.Now.ToShortDateString();
                HndFechaRueda.Value = HndFechaRueda.Value.Substring(6, 4) + "/" + HndFechaRueda.Value.Substring(3, 2) + "/" + HndFechaRueda.Value.Substring(0, 2);
                cargarDatosGrilla();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, ex.Message);
            }
        }
    }
    /// <summary>
    /// Nombre: cargarDatosGrilla
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para Cargar los datos en la Grio.
    /// Modificacion:
    /// </summary>
    private void cargarDatosGrilla()
    {
        if (hndCodSubasta.Value == "0")
        {
            dtgSubasta5.Columns.Clear();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.contratos.location= 'frm_contratos_5.aspx';", true);
        }
        else
        {
            try
            {
                //determina las columnas para el subastatodr o los operaores
                string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador", "@P_numero_id", "@P_codigo_punto", "@P_punta", "@P_codigo_modalidad", "@P_codigo_periodo" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { hndCodSubasta.Value, Session["cod_comisionista"].ToString(), "0", Session["codigo_punto"].ToString(), Session["punta"].ToString(), Session["codigo_modalidad"].ToString(), Session["codigo_periodo"].ToString() };
                lConexion.Abrir();
                dtgSubasta5.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaPuja5", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgSubasta5.DataBind();
                lConexion.Cerrar();
                ImageButton imbControl;
                int RowIndex = 0;
                foreach (DataGridItem Grilla in this.dtgSubasta5.Items)
                {
                    // Programmatically reference the PopupControlExtender
                    PopupControlExtender pce = Grilla.FindControl("PopupControlExtender1") as PopupControlExtender;

                    // Set the BehaviorID
                    string behaviorID = string.Concat("pce", RowIndex);
                    pce.BehaviorID = behaviorID;
                    RowIndex += 1;


                    // Programmatically reference the Image control
                    Label l = (Label)Grilla.Cells[1].FindControl("LblCodOperador");

                    // Add the clie nt-side attributes (onmouseover & onmouseout)
                    string OnMouseOverScript = string.Format("$find('{0}').showPopup();", behaviorID);
                    string OnMouseOutScript = string.Format("$find('{0}').hidePopup();", behaviorID);

                    l.Attributes.Add("onmouseover", OnMouseOverScript);
                    l.Attributes.Add("onmouseout", OnMouseOutScript);

                    if (Session["cod_comisionista"].ToString() == Grilla.Cells[22].Text)
                    {
                        imbControl = (ImageButton)Grilla.Cells[14].Controls[1];
                        imbControl.ImageUrl = "~/Images/quitar.gif";
                        imbControl.Enabled = false;
                        if (Grilla.Cells[12].Text == "V")
                        {
                            if (Convert.ToDecimal(Grilla.Cells[10].Text) < Convert.ToDecimal(Grilla.Cells[11].Text) && Convert.ToDecimal(Grilla.Cells[11].Text) > 0)
                            {
                                imbControl = (ImageButton)Grilla.Cells[15].Controls[1];
                                imbControl.ImageUrl = "~/Images/quitar.gif";
                                imbControl.Enabled = false;
                                imbControl = (ImageButton)Grilla.Cells[16].Controls[1];
                                imbControl.ImageUrl = "~/Images/quitar.gif";
                                imbControl.Enabled = false;
                            }
                            else
                            {
                                imbControl = (ImageButton)Grilla.Cells[15].Controls[1];
                                imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_IngresoPostura_5.aspx?ID=" + Grilla.Cells[0].Text + "','Modificar','width=540,height=500,left=350,top=100,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                            }
                        }
                        else
                        {
                            if (Convert.ToDecimal(Grilla.Cells[10].Text) > Convert.ToDecimal(Grilla.Cells[11].Text) && Convert.ToDecimal(Grilla.Cells[11].Text) > 0)
                            {
                                imbControl = (ImageButton)Grilla.Cells[15].Controls[1];
                                imbControl.ImageUrl = "~/Images/quitar.gif";
                                imbControl.Enabled = false;
                                imbControl = (ImageButton)Grilla.Cells[16].Controls[1];
                                imbControl.ImageUrl = "~/Images/quitar.gif";
                                imbControl.Enabled = false;
                            }
                            else
                            {
                                imbControl = (ImageButton)Grilla.Cells[15].Controls[1];
                                imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_IngresoPostura_5.aspx?ID=" + Grilla.Cells[0].Text + "','Modificar','width=540,height=500,left=350,top=100,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                            }
                        }
                    }
                    else
                    {
                        if (Grilla.Cells[20].Text == "N")
                        {
                            imbControl = (ImageButton)Grilla.Cells[14].Controls[1];
                            imbControl.ImageUrl = "~/Images/quitar.gif";
                            imbControl.Enabled = false;

                            imbControl = (ImageButton)Grilla.Cells[15].Controls[1];
                            imbControl.ImageUrl = "~/Images/quitar.gif";
                            imbControl.Enabled = false;

                            imbControl = (ImageButton)Grilla.Cells[16].Controls[1];
                            imbControl.ImageUrl = "~/Images/quitar.gif";
                            imbControl.Enabled = false;

                        }
                        else
                        {
                            if (Grilla.Cells[13].Text == "0.00")
                            {
                                imbControl = (ImageButton)Grilla.Cells[14].Controls[1];
                                imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_IngresoPostura_5.aspx?ID=" + Grilla.Cells[0].Text + "','Ofertar','width=540,height=500,left=350,top=100,status=no,location=0,menubar=no,toolbar=no,resizable=no');");

                                imbControl = (ImageButton)Grilla.Cells[15].Controls[1];
                                imbControl.ImageUrl = "~/Images/quitar.gif";
                                imbControl.Enabled = false;

                                imbControl = (ImageButton)Grilla.Cells[16].Controls[1];
                                imbControl.ImageUrl = "~/Images/quitar.gif";
                                imbControl.Enabled = false;
                            }
                            else
                            {
                                imbControl = (ImageButton)Grilla.Cells[14].Controls[1];
                                imbControl.ImageUrl = "~/Images/quitar.gif";
                                imbControl.Enabled = false;
                                imbControl = (ImageButton)Grilla.Cells[15].Controls[1];
                                imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_IngresoPostura_5.aspx?ID=" + Grilla.Cells[0].Text + "','Modificar','width=540,height=500,left=350,top=100,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                            }
                        }
                    }
                }
                if (Session["tipoPerfil"].ToString() == "B")
                {
                    dtgSubasta5.Columns[14].Visible = false;
                    dtgSubasta5.Columns[15].Visible = false;
                    dtgSubasta5.Columns[16].Visible = false;
                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, ex.Message);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.contratos.location= 'frm_contratos_5.aspx';", true);
        }
    }

    public void OnItemCommand_Click(object sender, DataGridCommandEventArgs e)
    {
        string oError = "";
        string lsMio = "N";
        string oBoton = ((System.Web.UI.WebControls.ImageButton)e.CommandSource).ID.ToString();

        if (oBoton == "imbEliminar")
        {
            try
            {
                if (Session["cod_comisionista"].ToString() == e.Item.Cells[22].Text)
                    lsMio = "S";
                string[] lsNombreParametros = { "@P_numero_id", "@P_codigo_operador", "@P_tipo_oferta", "@P_precio", "@P_mio" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Decimal, SqlDbType.VarChar };
                string[] lValorParametros = { e.Item.Cells[0].Text, Session["cod_comisionista"].ToString(), e.Item.Cells[12].Text, e.Item.Cells[10].Text.Replace(",", ""), lsMio };
                SqlDataReader lLector;
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_DelPostura5", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error == "")
                {
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        oError += lLector["error"].ToString();
                    }
                    else
                    {
                        if (lsMio == "S")
                            oError = "Oferta eliminada correctamente";
                        else
                            oError = "Postura eliminada correctamente";
                        Session["refrescar"] = "S";
                        Session["refresca_mis_posturas"] = "S";
                    }
                }
                else
                    oError = "Error al eliminar la oferta. " + goInfo.mensaje_error.ToString();
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                oError = "Error al eliminar la oferta. " + ex.Message;
            }
            Toastr.Warning(this, oError);
        }
        if (oBoton == "imbProfundidad")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.produndidad.location= 'frm_produndidad_5.aspx?ID=" + e.Item.Cells[0].Text + "';", true);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="contextKey"></param>
    /// <returns></returns>
    [System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
    public static string GetDynamicContent(string contextKey)
    {
        string[] lsNombreParametros = { "@P_codigo_operador" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        string[] lValorParametros = { contextKey };
        StringBuilder sTemp = new StringBuilder();
        SqlConnection gConexion;
        gConexion = new SqlConnection(lsCadenaCon);
        SqlDataReader lLector;
        gConexion.Open();
        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(gConexion, "pa_GetOperador", lsNombreParametros, lTipoparametros, lValorParametros);
        if (lLector.HasRows)
        {
            lLector.Read();
            sTemp.Append("<table style='background-color:#f3f3f3; border: #336699 3px solid; width:350px; font-size:10pt; font-family:Verdana;' cellspacing='0' cellpadding='3'>");
            sTemp.Append("<tr><td style='background-color:#336699; color:white;'><b>Informacion Agente:</b></td></tr>");
            sTemp.Append("<tr><td>Nombre: " + lLector["nombre_operador"].ToString() + "</td></tr>");
            sTemp.Append("<tr><td>Direcion: " + lLector["direccion"].ToString() + "</td></tr>");
            sTemp.Append("<tr><td>Telefono: " + lLector["telefono"].ToString() + "</td></tr>");
            sTemp.Append("<tr><td>E-mail: " + lLector["e_mail"].ToString() + "</td></tr>");
            sTemp.Append("</table>");
        }
        else
            sTemp.Append("<i>No se encontrol informacion del Agente...</i>");
        lLector.Close();
        lLector.Dispose();
        gConexion.Close();
        gConexion.Dispose();
        return sTemp.ToString();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public string[] retornarCredenciales()
    {
        string[] lsCredencial;
        lsCredencial = (goInfo.Usuario + "-" + goInfo.Password).Split('-');
        return lsCredencial;
    }

}
