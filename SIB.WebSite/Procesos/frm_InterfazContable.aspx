﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_InterfazContable.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Procesos.frm_InterfazContable" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">
        document.onkeydown = function (evt) { return (evt ? evt.which : event.keyCode) != 13; }
        function confirmar() {
            return confirm("Esta seguro que desea Generar Interfaz CxC?")
        }
        function confirmar1() {
            return confirm("Esta seguro que desea Generar Interfaz CxP?")
        }
    </script>

    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Año Facturación</label>
                            <asp:TextBox ID="TxtAno" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Mes Facturación</label>
                            <asp:DropDownList ID="ddlMesFactura" runat="server" CssClass="form-control">
                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Enero"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Febrero"></asp:ListItem>
                                <asp:ListItem Value="3" Text="Marzo"></asp:ListItem>
                                <asp:ListItem Value="4" Text="Abril"></asp:ListItem>
                                <asp:ListItem Value="5" Text="Mayo"></asp:ListItem>
                                <asp:ListItem Value="6" Text="Junio"></asp:ListItem>
                                <asp:ListItem Value="7" Text="Julio"></asp:ListItem>
                                <asp:ListItem Value="8" Text="Agosto"></asp:ListItem>
                                <asp:ListItem Value="9" Text="Septiembre"></asp:ListItem>
                                <asp:ListItem Value="10" Text="Octubre"></asp:ListItem>
                                <asp:ListItem Value="11" Text="Noviembre"></asp:ListItem>
                                <asp:ListItem Value="12" Text="Diciembre"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="table table-responsive">
                    <asp:UpdatePanel ID="UptPnlTodo" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblTotalGrillaFac" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="12px"></asp:Label>
                            <asp:HiddenField ID="hdfFechaCorte" runat="server" />
                            <asp:DataGrid ID="dtgFacturas" runat="server" AllowPaging="false"
                                PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered">
                                <Columns>
                                    <asp:BoundColumn HeaderText="Fecha Corte" DataField="fecha_corte" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Nit Operador" DataField="no_documento" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Nombre Operador" DataField="nombre_participante" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="350px"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Cantidad Factura" DataField="cantidad_energia" DataFormatString="{0: ###,###,###,###,###,##0.00}"
                                        ItemStyle-HorizontalAlign="Right" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Valor Factura" DataField="valor_factura" DataFormatString="{0: ###,###,###,###,###,##0.00}"
                                        ItemStyle-HorizontalAlign="Right" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="No. Doc. Nota Cr" DataField="no_nota_credito" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="No. Doc. Nota Db" DataField="no_nota_debito" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="No. Doc. Factura" DataField="no_factura" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Modificacion" DataField="modificada" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Valor Diferencia Db" DataField="valor_debito" DataFormatString="{0: ###,###,###,###,###,##0.00}"
                                        ItemStyle-HorizontalAlign="Right" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Valor Diferencia Cr" DataField="valor_credito" DataFormatString="{0: ###,###,###,###,###,##0.00}"
                                        ItemStyle-HorizontalAlign="Right" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="No. Doc. Nota Cr Rev. Aj." DataField="no_nota_credito_rev_ajuste" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="No. Doc. Factura Aj" DataField="no_factura_ajuste" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="No. Doc. Nota Cr Aj." DataField="no_nota_credito_ajuste" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="No. Doc. Nota Db Ref. Aj." DataField="no_nota_debito_rev_ajuste" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="No. Doc. Nota Cr Cliente" DataField="no_nota_credito_cliente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_compania" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_maxima" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_final" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_inicial" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_modifica" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_modifica_fin" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="contabilizado" Visible="false"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                <PagerStyle HorizontalAlign="Center"></PagerStyle>
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="panelUpdateProgress" DisplayAfter="0" runat="server" AssociatedUpdatePanelID="UptPnlTodo">
        <ProgressTemplate>
            <asp:Panel ID="PnlCarga" runat="server" Style="position: relative; top: 30%; text-align: center;"
                CssClass="popup">
                <div class="fuente">
                    <img src="../Images/ajax-loader.gif" style="vertical-align: middle" alt="Procesando" />
                    Procesando por Favor espere ...
                </div>
            </asp:Panel>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <%-- Modal --%>
    <div class="modal fade" id="CrearRegistro" tabindex="-1" role="dialog" aria-labelledby="mdlCrearRegistroLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="CrearRegistroInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlCrearRegistroLabel" runat="server">Ingrese Usuario y Password del WebService Seven</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label Text="Ingrese el Usuario de Servicio:" AssociatedControlID="TxtUsrServicio" runat="server" />
                                        <asp:TextBox ID="TxtUsrServicio" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label Text="Ingrese la contraseña de Servicio:" AssociatedControlID="TxtClaveServicio" runat="server" />
                                        <asp:TextBox ID="TxtClaveServicio" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <asp:Button ID="btnIngresar" runat="server" Text="Ingresar" OnClick="btnIngresar_Click" CssClass="btn btn-primary" />
                            <asp:HiddenField ID="hdfUsrServicio" runat="server" />
                            <asp:HiddenField ID="hdfPwdServicio" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>

