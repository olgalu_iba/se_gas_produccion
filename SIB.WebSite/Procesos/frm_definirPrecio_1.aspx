﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_definirPrecio_1.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_definirPrecio_1" EnableEventValidation="false" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">
        //        document.location = no;
        function confirmar() {
            return confirm("Esta seguro que desea definir el precio!")
        }
        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }
        function formatoMilesPre(obj, evt, decimales) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
                T[4] = T[4].substr(0, decimales);
                if (T[2] == '' && T[3] == '.') {
                    T[2] = 0;
                }
                obj.value = T[1] + addCommas(T[2]) + T[3] + T[4];
            }
            else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }

    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td6" colspan="2" align="center">
                        <asp:Label ID="lblTitulo" runat="server" Text="DEFINICION DE PRECIOS" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="td6">
                        <asp:Label ID="lblId" runat="server"></asp:Label>
                    </td>
                    <td class="td6">
                        <asp:Label ID="lblTipo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr id="trRango" runat="server">
                    <td class="td6">
                        <asp:Label ID="lblDuracion" runat="server"></asp:Label>
                    </td>
                    <td class="td6">
                        <asp:Label ID="lblLugar" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="1">
                        <asp:Label ID="lblPrecioRes" runat="server"></asp:Label>
                    </td>
                    <td class="td6" colspan="1">
                        <asp:Label ID="lblRonda" runat="server"></asp:Label>
                    </td>
                </tr>
                <%--<tr>
                    <td class="td6" colspan="1">
                        <asp:Label ID="lblCantidadV" runat="server"></asp:Label>
                    </td>
                    <td class="td6" colspan="1">
                        <asp:Label ID="lblCantidadC" runat="server"></asp:Label>
                    </td>
                </tr>--%>
                <%--<tr>
                    <td class="td6" colspan="1">
                        Hora Inicio prox fase:
                    </td>
                    <td class="td6" colspan="1">
                        <asp:TextBox ID="TxtHora" runat="server" Width="100px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RevTxtHora" ControlToValidate="TxtHora"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora"> * </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="1">
                        Duración minutos Prox fase:
                    </td>
                    <td class="td6" colspan="1">
                        <asp:TextBox ID="TxtMinutos" runat="server" Width="100px" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
                    </td>
                </tr>--%>
                <tr>
                    <td class="td6" colspan="1">
                        Precio referencia Prox fase:
                    </td>
                    <td class="td6" colspan="1">
                        <asp:TextBox ID="TxtPrecio" runat="server" Width="100px" onkeyup="return formatoMilesPre(this,event,2);"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" class="td6">
                        <br />
                        <asp:Button ID="btnPrecio" Text="Cambiar" runat="server" OnClientClick="return confirmar();"
                            OnClick="btnPrecio_Click" />
                        <asp:HiddenField ID="hndID" runat="server" />
                        <asp:HiddenField ID="hndPrecioRes" runat="server" />
                        <asp:HiddenField ID="hndRonda" runat="server" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>