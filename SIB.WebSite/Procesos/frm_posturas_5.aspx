﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_posturas_5.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Procesos_frm_posturas_5" %>




<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">

        window.onload = function() {
            var pos = window.name || 0;
            window.scrollTo(0, pos);
        }
        window.onunload = function() {
            window.name = self.pageYOffset || (document.documentElement.scrollTop + document.body.scrollTop);
        }

    </script>


 
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="divOferta" runat="server" style="position: absolute; left: 0; top: 0; width: 100%;">
                <table cellpadding="0" cellspacing="0" bgcolor="#000000">
                    <tr>
                        <td align="center">
                            <asp:DataGrid ID="dtgSubasta5" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                ItemStyle-CssClass="td2" HeaderStyle-CssClass="th2" runat="server" OnItemCommand="OnItemCommand_Click"
                                ShowHeader="false">
                                <ItemStyle CssClass="td2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="14px">
                                </ItemStyle>
                                <Columns>
                                    <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="50px"></asp:BoundColumn>
                                    <%--1--%>
                                    <asp:TemplateColumn HeaderText="Agente" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:Label ID="LblCodOperador" runat="server" Text='<%# Eval("codigo_operador") %>' />
                                            <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" DynamicServiceMethod="GetDynamicContent"
                                                DynamicContextKey='<%# Eval("codigo_operador") %>' DynamicControlID="Panel1"
                                                TargetControlID="LblCodOperador" PopupControlID="Panel1" Position="Center">
                                            </ajaxToolkit:PopupControlExtender>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--2--%><asp:BoundColumn DataField="desc_modalidad" HeaderText="Tipo" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="110px"></asp:BoundColumn>
                                    <%--3--%><asp:BoundColumn DataField="desc_periodo" HeaderText="Periodicidad" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--4--%><asp:BoundColumn DataField="desc_punto_entrega" HeaderText="lugar" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="200px"></asp:BoundColumn>
                                    <%--5--%><asp:BoundColumn DataField="fecha_rueda" HeaderText="F. Neg." ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="80px"></asp:BoundColumn>
                                    <%--6--%><asp:BoundColumn DataField="hora_negociacion" HeaderText="Hora Neg." ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="60px"></asp:BoundColumn>
                                    <%--7--%><asp:BoundColumn DataField="fecha_entrega" HeaderText="Inicio" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Width="80px"></asp:BoundColumn>
                                    <%--8--%><asp:BoundColumn DataField="fecha_entrega_fin" HeaderText="Fin" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Width="80px"></asp:BoundColumn>
                                    <%--9--%><asp:BoundColumn DataField="cantidad_postura" HeaderText="Cantidad" DataFormatString="{0:###,###,###,###,###,###,##0}"
                                        ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--10--%><asp:BoundColumn DataField="precio" HeaderText="Precio" DataFormatString="{0:###,###,###,###,###,###,##0.00}"
                                        ItemStyle-Width="70px" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--11--%><asp:BoundColumn DataField="precio_mejor_postura" HeaderText="Precio mejor Postura"
                                        ItemStyle-Width="70px" DataFormatString="{0:###,###,###,###,###,###,##0.00}"
                                        ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <%--12--%><asp:BoundColumn DataField="tipo_oferta" HeaderText="Tipo Oferta" ItemStyle-Width="60px">
                                    </asp:BoundColumn>
                                    <%--13--%><asp:BoundColumn DataField="precioA" HeaderText="Precio Mi Postura" ItemStyle-HorizontalAlign="Right"
                                        ItemStyle-Width="70px"></asp:BoundColumn>
                                    <%--14--%><asp:TemplateColumn HeaderText="Ofr" ItemStyle-Width="35px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbOfertar" runat="server" ToolTip="Ofertar" ImageUrl="~/Images/nuevo.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--15--%><asp:TemplateColumn HeaderText="Mod" ItemStyle-Width="35px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbModificar" runat="server" ToolTip="Modificar Oferta" ImageUrl="~/Images/modificar.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--16--%><asp:TemplateColumn HeaderText="Elim" ItemStyle-Width="35px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbEliminar" runat="server" ToolTip="Eliminar Oferta" ImageUrl="~/Images/eliminar.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--17--%><asp:TemplateColumn HeaderText="Prof" ItemStyle-Width="35px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbProfundidad" runat="server" ToolTip="Profundidad Oferta"
                                                ImageUrl="~/Images/historico.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--18--%><asp:BoundColumn DataField="estado" Visible="false" ItemStyle-Width="50px">
                                    </asp:BoundColumn>
                                    <%--19--%><asp:BoundColumn DataField="numero_rueda" Visible="false" ItemStyle-Width="50px">
                                    </asp:BoundColumn>
                                    <%--20--%><asp:BoundColumn DataField="aut_ofertar" Visible="false" ItemStyle-Width="50px">
                                    </asp:BoundColumn>
                                    <%--21--%><asp:BoundColumn DataField="informacion_agente" Visible="false" ItemStyle-Width="50px">
                                    </asp:BoundColumn>
                                    <%--22--%><asp:BoundColumn DataField="codigo_operador" Visible="false" ItemStyle-Width="50px">
                                    </asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="th2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="13px">
                                </HeaderStyle>
                            </asp:DataGrid>
                            <asp:HiddenField ID="hndCodSubasta" runat="server" />
                            <asp:HiddenField ID="HndFechaRueda" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
            <%--<div id="dfInf" runat="server" style="position: static; left: 100; top: 100; width: 50%;"
                visible="false">
                <asp:Label ID="lblInfOperador" runat="server" Text="Label"></asp:Label>
            </div>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Panel1" runat="server" Style="left: 60px; top: 100px; position: static;">
    </asp:Panel>
</asp:Content>
