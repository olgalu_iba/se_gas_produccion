﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Procesos
{
    public partial class frm_excepcionControlFechaModContTra : Page
    {
        private InfoSessionVO goInfo;
        private static string lsTitulo = "Excepción control de fechas para modificación de contratos de transporte";
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        clConexion lConexion = null;
        private SqlDataReader lLector;
        private string gsTabla = "m_excep_mod_fecha_trans";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lConexion = new clConexion(goInfo);
            lblTitulo.Text = lsTitulo;
            //Titulo
            Master.Titulo = "Subasta";
            /// Llamo el procedimiento para la validacion de la Fecha válida para declaración
            //Activacion de los Botones
            //buttons.Inicializar(ruta: gsTabla);
            Inicializar();
            buttons.CrearOnclick += btnNuevo;
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.SalirOnclick += imbSalir_Click;

            if (IsPostBack) return;
            //Establese los permisos del sistema
            EstablecerPermisosSistema();

            Inicializar();
            Listar();
        }
        /// <summary>
        /// 
        /// </summary>
        private void Inicializar()
        {
            EnumBotones[] botones = { };
            botones = new[] { EnumBotones.Crear, EnumBotones.Buscar, EnumBotones.Excel, EnumBotones.Salir, };

            // Activacion de los Botones
            buttons.Inicializar(botones: botones);
        }
        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Enero 26 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            var permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, gsTabla);
            foreach (DataGridItem Grilla in dtgMaestro.Items)
            {
                var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                lkbModificar.Visible = (bool)permisos["UPDATE"];
                var lkbEliminar = (LinkButton)Grilla.FindControl("lnkEliminar");
                lkbEliminar.Visible = (bool)permisos["DELETE"];

                if (!lkbModificar.Visible && !lkbEliminar.Visible)
                    dtgMaestro.Columns[5].Visible = false;
            }
        }

        /// <summary>
        /// Nombre: Nuevo
        /// Fecha: Enero 26 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
        /// Modificacion:
        /// </summary>
        private void Nuevo()
        {
            Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
            imbCrear.Visible = true;
            imbActualiza.Visible = false;
            lblTitulo.Text = lsTitulo;
            TxtCodigoExc.Visible = false;
            LblCodigoExc.Visible = true;
            LblCodigoExc.Text = "Automático";
            imbActualiza.Visible = false;
            imbCrear.Visible = true;
            TxtNoId.Enabled = true;
            ddlEstado.SelectedValue = "A";
        }

        /// <summary>
        /// Nombre: Listar
        /// Fecha: Enero 26 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            CargarDatos();
            dtgMaestro.Visible = true;
            lblTitulo.Text = lsTitulo;
            EstablecerPermisosSistema();
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Enero 26 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            var lblMensaje = new StringBuilder();

            if (modificar != null && modificar != "")
            {
                try
                {
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", gsTabla, " codigo_excepcion = " + modificar + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            LblCodigoExc.Text = lLector["codigo_excepcion"].ToString();
                            TxtCodigoExc.Text = lLector["codigo_excepcion"].ToString();
                            TxtNoId.Text = lLector["codigo_verif"].ToString();
                            ddlEstado.SelectedValue = lLector["estado"].ToString();
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                            TxtCodigoExc.Visible = false;
                            LblCodigoExc.Visible = true;
                            TxtNoId.Enabled = false;
                            ddlEstado.Enabled = true;

                            Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Bloquea el Registro a Modificar
                        manejo_bloqueo("A", modificar);
                    }
                    else
                    {
                        Listar();
                        lblMensaje.Append("No se Puede editar el Registro por que está Bloqueado. Código registro " + modificar);
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append(ex.Message);
                }
            }
            if (lblMensaje.ToString() == "")
            {
                Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
                lblTitulo.Text = lsTitulo;
            }
            else
            {
                Toastr.Error(this, lblMensaje.ToString());
            }
        }

        /// <summary>
        /// Nombre: Buscar
        /// Fecha: Enero 27 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
        /// Modificacion:
        /// </summary>
        private void Buscar()
        {
            Modal.Cerrar(this, CrearRegistro.ID);
            lblTitulo.Text = lsTitulo;
        }
        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Enero 27 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_codigo_verif" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int };
            string[] lValorParametros = { "0" };

            try
            {
                if (TxtBusId.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtBusId.Text.Trim();
                lConexion.Abrir();
                dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetExcepModFechaTrans", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgMaestro.DataBind();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }
        /// <summary>
        /// Nombre: btnConsultar_Click
        /// Fecha: Enero 27 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            Listar();
        }

        /// <summary>
        /// Nombre: Validaciones
        /// Fecha: Enero 27 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para validar campos de Entrada
        ///              en el Boton Crear.
        /// </summary>
        /// <returns></returns>
        protected StringBuilder Validaciones(string indicador)
        {
            var lblMensaje = new StringBuilder();
            try
            {
                decimal ldValor = 0;
                if (TxtNoId.Text.Trim() != "")
                {
                    try
                    {
                        ldValor = Convert.ToInt32(TxtNoId.Text.Trim());
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append("Valor Inválido en el número de ID<br>");
                    }
                }
                else
                    lblMensaje.Append("Debe Ingresar el número de ID<br>");
                return lblMensaje;
            }
            catch (Exception ex)
            {
                lblMensaje.Append("Error en las Validaciones de la Información. " + ex.Message.ToString());
                return lblMensaje;
            }
        }
        /// <summary>
        /// Nombre: imbCrear_Click
        /// Fecha: Enero 27 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Crear.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_excepcion", "@P_codigo_verif", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "A", "1" };
            var lblMensaje = new StringBuilder();
            try
            {
                lblMensaje = Validaciones("C");
                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[1] = TxtNoId.Text;
                    lValorParametros[2] = ddlEstado.SelectedValue;


                    var lComando = new SqlCommand();
                    int liNumeroParametros;
                    lConexion.Abrir();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_SetExcepModFechaTrans";
                    lComando.CommandTimeout = 3600;
                    if (lsNombreParametros != null)
                    {
                        for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametros.Length - 1; liNumeroParametros++)
                        {
                            lComando.Parameters.Add(lsNombreParametros[liNumeroParametros], lTipoparametros[liNumeroParametros]).Value = lValorParametros[liNumeroParametros];
                        }
                    }
                    lLector = lComando.ExecuteReader();
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                        {
                            lblMensaje.Append(lLector["error"].ToString() + "<br>");
                        }
                    }
                    else
                    {
                        lLector.Close();
                        lConexion.Cerrar();
                        Modal.Cerrar(this, CrearRegistro.ID); //20200727
                        limpiarCampos();
                        Toastr.Success(this, "Se creo correctamente la excepción");
                        Listar();
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    Toastr.Error(this, lblMensaje.ToString());
            }
            catch (Exception ex)
            {
                lLector.Close();
                lConexion.Cerrar();
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: imbActualiza_Click
        /// Fecha: Enero 27 de 2021
        /// Creador: German Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Actualizar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbActualiza_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_excepcion", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "", "2" };
            var lblMensaje = new StringBuilder();
            try
            {
                lblMensaje = Validaciones("M");
                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[0] = LblCodigoExc.Text;
                    lValorParametros[1] = ddlEstado.SelectedValue;

                    var lComando = new SqlCommand();
                    int liNumeroParametros;
                    lConexion.Abrir();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_SetExcepModFechaTrans";
                    lComando.CommandTimeout = 3600;
                    if (lsNombreParametros != null)
                    {
                        for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametros.Length - 1; liNumeroParametros++)
                        {
                            lComando.Parameters.Add(lsNombreParametros[liNumeroParametros], lTipoparametros[liNumeroParametros]).Value = lValorParametros[liNumeroParametros];
                        }
                    }
                    lLector = lComando.ExecuteReader();
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                        {
                            lblMensaje.Append(lLector["error"].ToString() + "<br>");
                        }
                    }
                    else
                    {
                        lLector.Close();
                        lConexion.Cerrar();
                        manejo_bloqueo("E", LblCodigoExc.Text);
                        Modal.Cerrar(this, CrearRegistro.ID);
                        limpiarCampos();
                        Toastr.Success(this, "Se modificó correctamente la excepción");
                        Listar();
                    }

                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    Toastr.Error(this, lblMensaje.ToString());
            }
            catch (Exception ex)
            {
                lLector.Close();
                lConexion.Cerrar();
                Toastr.Error(this, ex.Message);
            }
        }
        /// <summary>
        /// Nombre: imbSalir_Click
        /// Fecha: Enero 22 de 2021
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
        ///              en el Boton Salir.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbSalir_Click(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (LblCodigoExc.Text != "")
                manejo_bloqueo("E", LblCodigoExc.Text);
            //Cierra el modal de Agregar
            //Modal.Cerrar(this, CrearRegistro.ID);
            //Listar();
            Response.Redirect("../WebForms/Home.aspx");
        }
        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Enero 22 de 2021
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Enero 22 de 2021
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            string lCodigoRegistro = "";
            if (e.CommandName == "Modificar")
            {
                lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                Modificar(lCodigoRegistro);
                imbActualiza.Visible = true;
                imbCrear.Visible = false;
            }
            if (e.CommandName == "Eliminar")
            {
                string[] lsNombreParametros = { "@P_codigo_excepcion", "@P_accion" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { "0", "3" };
                var lblMensaje = new StringBuilder();
                try
                {
                    if (lblMensaje.ToString() == "")
                    {
                        lValorParametros[0] = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;

                        var lComando = new SqlCommand();
                        int liNumeroParametros;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = "pa_SetExcepModFechaTrans";
                        lComando.CommandTimeout = 3600;
                        if (lsNombreParametros != null)
                        {
                            for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametros.Length - 1; liNumeroParametros++)
                            {
                                lComando.Parameters.Add(lsNombreParametros[liNumeroParametros], lTipoparametros[liNumeroParametros]).Value = lValorParametros[liNumeroParametros];
                            }
                        }
                        lLector = lComando.ExecuteReader();
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                lblMensaje.Append(lLector["error"].ToString() + "<br>");
                            }
                        }
                        else
                        {
                            lLector.Close();
                            lConexion.Cerrar();
                            Toastr.Success(this, "Se eliminó correctamente la excepción");
                            Listar();
                        }

                    }

                    if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                        Toastr.Error(this, lblMensaje.ToString());
                }
                catch (Exception ex)
                {
                    lLector.Close();
                    lConexion.Cerrar();
                    Toastr.Error(this, ex.Message);
                }
            }
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Enero 22 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia(gsTabla, lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Enero 22 de 2021
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='" + gsTabla + "' and llave_registro='codigo_excepcion=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_excepcion=" + lscodigo_registro.ToString();
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = gsTabla;
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, gsTabla, lsCondicion1);
            }
            return true;
        }
        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Enero 22 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_verif" };
            string[] lValorParametros = { "0" };
            string lsParametros = "";

            try
            {
                if (TxtBusId.Text != "")
                {
                    lValorParametros[0] = TxtBusId.Text;
                    lsParametros += " Id Registro: " + TxtBusId.Text;
                }
                Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetExcepModFechaTrans&nombreParametros=@P_codigo_verif&valorParametros=" + lValorParametros[0] + "&columnas=codigo_excepcion*codigo_verif*estado&titulo_informe=Listado de Excepciones de control de modificación de fecha final &TituloParametros=" + lsParametros);
            }
            catch (Exception)
            {
                Toastr.Error(this, "No se Pudo Generar el Informe.!");
            }
        }

        ///// Eventos Nuevos para la Implementracion del UserControl

        /// <summary>
        /// Metodo del Link Nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNuevo(object sender, EventArgs e)
        {
            Nuevo();
        }

        /// <summary>
        /// Metodo del Link Listar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsulta(object sender, EventArgs e)
        {
            Listar();
        }
        /// <summary>
        /// Nombre: limpiarCampos
        /// Fecha: Enero 21 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Metodo para limpiar campos
        /// </summary>
        protected void limpiarCampos()
        {
            TxtNoId.Text = "";
        }
        /// <summary>
        /// Nombre: imbCancelar_Click
        /// Fecha: Enero 25 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Metodo para Cancelar la creacion o modificacion de registros
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCancelar_Click(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (LblCodigoExc.Text != "")
                manejo_bloqueo("E", LblCodigoExc.Text);
            //Cierra el modal de Agregar
            Modal.Cerrar(this, CrearRegistro.ID);
            Listar();
        }
    }
}