﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;
using AjaxControlToolkit;
using Segas.Web.Elements;

public partial class Procesos_frm_titulos_5 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    //String strRutaArchivo;
    private System.TimeSpan diffResult;
    static String lsCadenaCon;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            try
            {
                lsCadenaCon = "Data Source=" + goInfo.Servidor.ToString() + "; Initial Catalog=" + goInfo.BaseDatos.ToString() + ";User ID = " + goInfo.Usuario.ToString() + "; Password=" + goInfo.Password.ToString();
                lConexion1 = new clConexion(goInfo);
                cargarDatosGrilla();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, ex.Message);
            }
        }
    }
    /// <summary>
    /// Nombre: cargarDatosGrilla
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para Cargar los datos en la Grio.
    /// Modificacion:
    /// </summary>
    private void cargarDatosGrilla()
    {
            try
            {
                //determina las columnas para el subastatodr o los operaores
                string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador", "@P_numero_id", "@P_codigo_punto", "@P_punta", "@P_codigo_modalidad", "@P_codigo_periodo" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { "0", "0", "0", "0", "", "0", "0"};
                lConexion.Abrir();
                dtgSubasta5.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaPuja5", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgSubasta5.DataBind();
                lConexion.Cerrar();
                if (Session["tipoPerfil"].ToString() == "B")
                {
                    dtgSubasta5.Columns[13].Visible = false;
                    dtgSubasta5.Columns[14].Visible = false;
                    dtgSubasta5.Columns[15].Visible = false;
                }
            }
            catch (Exception ex)
            {
            Toastr.Warning(this, ex.Message);
        }
        }
    }

    