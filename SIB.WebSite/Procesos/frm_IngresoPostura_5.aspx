﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_IngresoPostura_5.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_IngresoPostura_5" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" language="javascript">
        //        document.location = no;
        function confirmar() {
            return confirm("Esta seguro que desea ofertar!")
        }
        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }

        function formatoMiles(obj, evt) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /([0-9]*)$/i.exec(strValue)) != null) {
                obj.value = addCommas(T[1]);
            } else {
                alert('formato invalido ');
            }
        }
        function formatoMilesPre(obj, evt, decimales) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
                T[4] = T[4].substr(0, decimales);
                if (T[2] == '' && T[3] == '.') {
                    T[2] = 0;
                }
                obj.value = T[1] + addCommas(T[2]) + T[3] + T[4];
            }
            else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }

    </script>

    
   
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td6" colspan="4" align="center">
                        <asp:Label ID="lblTituo" runat="server" Text="INGRESO POSTURAS" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="td6">
                        Producto:
                    </td>
                    <td class="td6">
                        <asp:Label ID="lblProducto" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="td6">
                        <asp:Label ID="lblPtoEnt" runat="server"></asp:Label>
                    </td>
                    <td class="td6">
                        <asp:DropDownList ID="ddlLugarEntrega" runat="server" OnSelectedIndexChanged="ddlLugarEntrega_SelectedIndexChanged"
                            AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlPtoFin" runat="server" Visible="false">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="td6">
                        Fecha Negociacion:
                    </td>
                    <td class="td6">
                        <asp:TextBox ID="TxtFechaNegociacion" runat="server" Width="150px"></asp:TextBox>
                      <%--  <ajaxToolkit:CalendarExtender ID="CeTxtFechaNegociacion" runat="server" TargetControlID="TxtFechaNegociacion"
                            Format="yyyy/MM/dd">
                        </ajaxToolkit:CalendarExtender>--%>
                    </td>
                </tr>
                <tr runat="server">
                    <td class="td6">
                        Hora Negociación:
                    </td>
                    <td class="td6">
                        <asp:TextBox ID="TxtHoraNeg" runat="server" Width="150px" MaxLength="5"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RevTxtHoraNeg" ControlToValidate="TxtHoraNeg"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora de negociación"> * </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="td6">
                        Periodo de Entrega:
                    </td>
                    <td class="td6">
                        <asp:DropDownList ID="ddlPeriodoEnt" runat="server" OnSelectedIndexChanged="ddlPeriodoEnt_SelectedIndexChanged"
                            AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="td6">
                        Tipo Contrato:
                    </td>
                    <td class="td6">
                        <asp:DropDownList ID="ddlTipoContrato" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="td6" runat="server" id="TrNoAnos" visible="false">
                        No. Años:
                    </td>
                    <td class="td6" runat="server" id="TrNoAnos1" visible="false">
                        <asp:TextBox ID="TxtNoAnos" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="td6">
                        Inicio:
                    </td>
                    <td class="td6">
                        <asp:TextBox ID="TxtFechaInicial" runat="server" Width="150px" OnTextChanged="TxtFechaInicial_TextChanged"
                            AutoPostBack="true"></asp:TextBox>
                       <%-- <ajaxToolkit:CalendarExtender ID="CeTxtFechaInicial" runat="server" TargetControlID="TxtFechaInicial"
                            Format="yyyy/MM/dd">
                        </ajaxToolkit:CalendarExtender>--%>
                    </td>
                </tr>
                <tr>
                    <td class="td6">
                        Fin:
                    </td>
                    <td class="td6">
                        <asp:TextBox ID="TxtFechaFin" runat="server" Width="150px" Enabled="false" ></asp:TextBox>
                       <%-- <ajaxToolkit:CalendarExtender ID="CeTxtFechaFin" runat="server" TargetControlID="TxtFechaFin"
                            Format="yyyy/MM/dd">
                        </ajaxToolkit:CalendarExtender>--%>
                    </td>
                </tr>
                <tr id="trHora" runat="server" visible="false">
                    <td class="td6">
                        Hora Inicio:
                    </td>
                    <td class="td6">
                        <asp:TextBox ID="txtHoraIni" runat="server" Width="150px" MaxLength="5"></asp:TextBox>
                        <%--<ajaxToolkit:FilteredTextBoxExtender ID="FTEBtxtHoraIni" runat="server" TargetControlID="txtHoraIni"
                            FilterType="Custom, Numbers" ValidChars=":">
                        </ajaxToolkit:FilteredTextBoxExtender>--%>
                        <asp:RegularExpressionValidator ID="revtxtHoraIni" ControlToValidate="txtHoraIni"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora inicial"> * </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr id="trHoraF" runat="server" visible="false">
                    <td class="td6">
                        Hora Fin:
                    </td>
                    <td class="td6">
                        <asp:TextBox ID="txtHoraFin" runat="server" Width="150px" MaxLength="5"></asp:TextBox>
                      <%--  <ajaxToolkit:FilteredTextBoxExtender ID="FTEBtxtHoraFin" runat="server" TargetControlID="txtHoraFin"
                            FilterType="Custom, Numbers" ValidChars=":">
                        </ajaxToolkit:FilteredTextBoxExtender>--%>
                        <asp:RegularExpressionValidator ID="revtxtHoraFin" ControlToValidate="txtHoraFin"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora final"> * </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="1">
                        Precio:
                    </td>
                    <td class="td6" colspan="1">
                        <asp:TextBox ID="TxtPrecio" runat="server" Width="100px" onkeyup="return formatoMilesPre(this,event,2);"></asp:TextBox>
                       <%-- <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtPrecio" runat="server" Enabled="True" FilterType="Custom, Numbers"
                            TargetControlID="TxtPrecio" ValidChars=",.">
                        </ajaxToolkit:FilteredTextBoxExtender>--%>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="1">
                        Cantidad:
                    </td>
                    <td class="td6" colspan="1">
                        <asp:TextBox ID="TxtCantidad" runat="server" Width="100px" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
                     <%--   <ajaxToolkit:FilteredTextBoxExtender ID="FTEBTxtCantidad" runat="server" Enabled="True" FilterType="Custom, Numbers"
                            TargetControlID="TxtCantidad" ValidChars=",">
                        </ajaxToolkit:FilteredTextBoxExtender>--%>
                    </td>
                </tr>
                <tr runat="server" id="TrTipo">
                    <td class="td6">
                        Tipo Postura:
                    </td>
                    <td class="td6">
                        <asp:DropDownList ID="ddlTipoPostura" runat="server">
                            <asp:ListItem Value="S" Text="Seleccione"></asp:ListItem>
                            <asp:ListItem Value="C" Text="Compra"></asp:ListItem>
                            <asp:ListItem Value="V" Text="Venta"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" class="td6">
                        <br />
                        <asp:Button ID="btnOfertar" Text="Crear" runat="server" OnClientClick="return confirmar();"
                            OnClick="btnOfertar_Click" />
                        <asp:HiddenField ID="hdfNoRueda" runat="server" />
                        <asp:HiddenField ID="hdfNoid" runat="server" />
                        <asp:HiddenField ID="hdfCodProducto" runat="server" />
                        <asp:HiddenField ID="hdfCodOperador" runat="server" />
                        <asp:HiddenField ID="hdfCodTipoRueda" runat="server" />
                        <asp:HiddenField ID="hdfDestinoProducto" runat="server" />
                        <asp:HiddenField ID="hndAccion" runat="server" />
                        <asp:HiddenField ID="hdfTipoMercado" runat="server" />
                        <asp:HiddenField ID="hdfMedidaTiempo" runat="server" />
                        <asp:HiddenField ID="hdfMesInicialPeriodo" runat="server" />
                        <asp:HiddenField ID="hdfTipoPostura" runat="server" />
                        <asp:HiddenField ID="hdfOfMia" runat="server" />
                        <asp:HiddenField ID="hdfPrePost" runat="server" />
                        <asp:HiddenField ID="hdfHoraIni" runat="server" />
                        <asp:HiddenField ID="hdfHoraFin" runat="server" />
                        <asp:HiddenField ID="hdfErrorFecha" runat="server" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>