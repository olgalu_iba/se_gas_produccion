﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;

public partial class Procesos_frm_subasta_prm_6 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    String strRutaArchivo;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString();
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            if (!IsPostBack)
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlPunto, "m_pozo ", "estado ='A' order by descripcion", 0, 1);
                lConexion.Cerrar();
                ddlPunto.SelectedValue = Session["codigo_punto"].ToString();

                HndFechaRueda.Value = DateTime.Now.ToShortDateString();
                HndFechaRueda.Value = HndFechaRueda.Value.Substring(6, 4) + "/" + HndFechaRueda.Value.Substring(3, 2) + "/" + HndFechaRueda.Value.Substring(0, 2);
                lblAgente.Text = goInfo.nombre;
                ImgExcel.Visible = false;
                SqlDataReader lLector;
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda rue,m_tipos_rueda tpo, m_estado_gas est", " rue.numero_rueda =" + Session["numero_rueda"].ToString() + " and rue.codigo_tipo_rueda = tpo.codigo_tipo_rueda and est.tipo_estado ='R' and rue.estado = est.sigla_estado");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lblSubasta.Text = lLector["descripcion"].ToString();
                    lblFase.Text = lLector["estado"].ToString() + " - " + lLector["descripcion_estado"].ToString();
                    switch (lLector["estado"].ToString())
                    {
                        case "C": lblHora.Text = lLector["hora_ini_publ_cnt_dispi_v_sci"].ToString();
                            break;
                        //case "1": lblHora.Text = lLector["hora_ini_publi_v_sci"].ToString() + " - " + lLector["hora_fin_publi_v_sci"].ToString();
                        //    lblFase.Text = "1 - Declaración de Cantidades Disponibles y precios de reserva"; //20160202
                        //    break;
                        case "3": lblHora.Text = lLector["hora_ini_publ_cnt_dispi_v_sci"].ToString() + " - " + lLector["hora_fin_publ_cnt_dispi_v_sci"].ToString();
                            ImgExcel.Visible = true;
                            lblFase.Text = "1 - Publicación de la cantidad disponible"; //20160202
                            break;
                        case "4": lblHora.Text = lLector["hora_ini_rec_solicitud_c_sci"].ToString() + " - " + lLector["hora_fin_rec_solicitud_c_sci"].ToString();
                            lblFase.Text = "2 - Recibo de las solicitudes de compra"; //20160202
                            break;
                        case "5": lblHora.Text = lLector["hora_ini_negociacioni_sci"].ToString() + " - " + lLector["hora_fin_negociacioni_sci"].ToString();
                            lblFase.Text = "4 - Desarrollo de la subasta"; //20160202
                            break;
                        case "7": lblHora.Text = lLector["hora_fin_rec_solicitud_c_sci"].ToString() + " - " + lLector["hora_ini_negociacioni_sci"].ToString();
                            lblFase.Text = "3 - Espera Calce"; //20160202
                            break;
                        default: lblHora.Text = "";
                            break;
                    }
                    //lblHora.Text = lLector["hora_prox_fase"].ToString();
                    Session["estado"] = lLector["estado"].ToString();
                    Session["destino_rueda"] = lLector["destino_rueda"].ToString();
                }
                else
                {
                    lblFase.Text = "";
                    lblHora.Text = "";
                    Session["estado"] = "F";
                    Session["destino_rueda"] = "G";
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();

                if (goInfo.cod_comisionista != "0" || Session["estado"].ToString() == "F" || Session["estado"].ToString() == "Z")
                {
                    btnSuspender.Visible = false;
                    btnReactivar.Visible = false;
                }
                else
                    if (Session["estado"].ToString() == "S")
                    {
                        btnSuspender.Visible = false;
                        btnReactivar.Visible = true;
                    }
                    else
                    {
                        btnSuspender.Visible = true;
                        btnReactivar.Visible = false;
                    }
                //if (goInfo.cod_comisionista != "0" && (Session["estado"].ToString() == "1" || Session["estado"].ToString() == "3"))
                //    btnCarga.Visible = true;
                //else
                //    btnCarga.Visible = false;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.postura.location= 'frm_posturas_6.aspx?';", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.titulo.location='frm_titulos_6.aspx';", true);
            }
            //if (Session["tipoPerfil"].ToString() == "B")
            //{
            //    btnCarga.Visible = false;
            //}
        }
        catch (Exception ex)
        {

        }
    }
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Session["codigo_punto"] = ddlPunto.SelectedValue;
        Session["refrescar"] = "S";
        Session["refresca_mis_posturas"] = "S";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.titulo.location='frm_titulos_6.aspx';", true);

    }

    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador", "@P_codigo_punto" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { Session["numero_rueda"].ToString(), goInfo.cod_comisionista, ddlPunto.SelectedValue };
        lConexion.Abrir();
        dtgSubasta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaPuja6Exc", lsNombreParametros, lTipoparametros, lValorParametros);
        dtgSubasta.DataBind();
        dtgSubasta.Visible = true;

        DataSet lds = new DataSet();
        SqlDataAdapter lsqldata = new SqlDataAdapter();
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfRueda" + DateTime.Now + ".xls";
        string lstitulo_informe = "Consulta Ids Subasta " + lblSubasta.Text;
        decimal ldCapacidad = 0;
        StringBuilder lsb = new StringBuilder();
        ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
        StringWriter lsw = new StringWriter(lsb);
        HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
        Page lpagina = new Page();
        HtmlForm lform = new HtmlForm();
        lpagina.EnableEventValidation = false;
        lpagina.DesignerInitialize();
        lpagina.Controls.Add(lform);
        dtgSubasta.EnableViewState = false;
        lform.Controls.Add(dtgSubasta);
        lpagina.RenderControl(lhtw);
        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "aplication/vnd.ms-excel";
        Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
        Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
        Response.ContentEncoding = System.Text.Encoding.Default;
        Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
        Response.Charset = "UTF-8";
        Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
        Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
        Response.ContentEncoding = Encoding.Default;
        Response.Write(lsb.ToString());
        Response.End();
        lds.Dispose();
        lsqldata.Dispose();
        lConexion.CerrarInforme();
        dtgSubasta.Visible = false;
    }
}