﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
//using System.Windows.Forms;



public partial class Procesos_frm_subasta_prm_1 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    String strRutaArchivo;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString();
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            if (!IsPostBack)
            {

                HndFechaRueda.Value = DateTime.Now.ToShortDateString();
                HndFechaRueda.Value = HndFechaRueda.Value.Substring(6, 4) + "/" + HndFechaRueda.Value.Substring(3, 2) + "/" + HndFechaRueda.Value.Substring(0, 2);

                string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { Session["numero_rueda"].ToString(), goInfo.cod_comisionista }; ///  

                lConexion.Abrir();
                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_getPunta1", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    Session["punta"] = lLector["punta"].ToString();
                    lLector.Close();
                    lLector.Dispose();
                }
                else
                    Session["punta"] = "";

                if (goInfo.cod_comisionista == "0")
                {
                    lblAgente.Text = goInfo.nombre;
                    lblAgente.Text += " - Subastador";
                }
                else
                {
                    lblAgente.Text = Session["nomOperador"].ToString();
                    btnHora.Visible = false;
                    btnMensaje.Visible = false;
                    btnSuspender.Visible = false;
                    if (Session["punta"].ToString() == "V")
                        lblAgente.Text += " - Vendedor";
                    if (Session["punta"].ToString() == "C")
                        lblAgente.Text += " - Comprador";
                }
                lConexion.Cerrar();
            }
        }
        catch (Exception ex)
        {
 
        }
    }
    ////protected void Timer1_Tick(object sender, EventArgs e)
    ////{
    ////    try
    ////    {
    ////        lblRelog.Text = DateTime.Now.ToLongTimeString().Substring(0, 8);
    ////    }
    ////    catch (Exception ex)
    ////    {
    ////    }

    ////    try
    ////    {

    ////        //   lblRelog.ForeColor = System.Drawing.Color.Red;
    ////        string oArchivo = strRutaArchivo + Session["codigo_subasta"].ToString() + ".txt";
    ////        if (Session["hora"].ToString() != File.GetCreationTime(oArchivo).ToString("HH:mm:ss") || Session["refrescar"].ToString() == "S")
    ////        {
    ////            //Session["hora"] = File.GetCreationTime(oArchivo).ToString("HH:mm:ss");
    ////            //Session["refrescar"] = "N";
    ////            Session["hora"] = File.GetCreationTime(oArchivo).ToString("HH:mm:ss");
    ////            Session["refrescar"] = "N";
    ////            //ScriptManager.RegisterStartupScript(this, this.GetType(), "OPEN_WINDOW", "window.open('frm_posturas.aspx?ID=0&subyacente=0&ciudad=0&punta=&tipo=Precio','postura');", true);
    ////            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.postura.location= 'frm_posturas_1.aspx?ID=" + Session["id"].ToString() + "&subyacente=" + Session["subyacente"].ToString() + "&ciudad=" + Session["ciudad"].ToString() + "&punta=" + Session["punta"].ToString() + "&tipo=" + Session["tipo"].ToString() + "';", true);
    ////        }

    ////    }
    ////    catch (Exception ex)
    ////    {
    ////    }
    ////}
}
