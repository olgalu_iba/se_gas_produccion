﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_DecSubBim.aspx.cs" Inherits="Procesos_frm_DecSubBim" MasterPageFile="~/PlantillaPrincipal.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">

        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }

        function formatoMiles(obj, evt) {
            var T;
            var strValue = removeCommas(obj.value);
            //if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
            if ((T = /([0-9]*)$/i.exec(strValue)) != null) {
                //                T[4] = T[4].substr(0, decimales);
                //                if (T[2] == '' && T[3] == '.') T[2] = 0;
                obj.value = addCommas(T[1]);
            } else {
                alert('formato invalido ');
            }
        }
        function formatoMilesPre(obj, evt, decimales) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
                T[4] = T[4].substr(0, decimales);
                if (T[2] == '' && T[3] == '.') {
                    T[2] = 0;
                }
                obj.value = T[1] + addCommas(T[2]) + T[3] + T[4];
            }
            else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }

    </script>

    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" bgcolor="#85BF46">
                <table border="0" cellspacing="2" cellpadding="2" align="right">
                    <tr>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkNuevo" runat="server" NavigateUrl="~/Procesos/frm_DecSubBim.aspx?lsIndica=N">Nuevo</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_DecSubBim.aspx?lsIndica=L">Listar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_DecSubBim.aspx?lsIndica=B">Consultar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/WebForms/Home.aspx">Salir</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Operador:
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlOperador" runat="server" enable="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr runat="server" id="trdeclina">
            <td class="td1">
                Campo en Declinación?
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlDeclina" runat="server">
                    <asp:ListItem Value="S">Si</asp:ListItem>
                    <asp:ListItem Value="N">No</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr runat="server" id="trFuente">
            <td class="td1">
                Fuente o Campo
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlFuente" runat="server" OnSelectedIndexChanged="ddlFuente_SelectedIndexChanged"
                    AutoPostBack="true">
                </asp:DropDownList>
            </td>
        </tr>
        <tr runat="server" id="trMes">
            <td class="td1">
                Mes Subasta
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlMes" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Punto del SNT
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlPozo" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Cantidad disponible
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCantidad" runat="server" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Precio de reserva
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtPrecio" runat="server" onkeyup="return formatoMilesPre(this,event,2);"></asp:TextBox>
            </td>
        </tr>
        <tr id="trDelta" runat="server">
            <td class="td1">
                Delta de precio
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDelta" runat="server" onkeyup="return formatoMilesPre(this,event,2);"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:Button ID="imbCrear" runat="server" Text="Crear" OnClientClick="this.disabled = true;"
                    UseSubmitBehavior="false" OnClick="imbCrear_Click1" ValidationGroup="comi" />
                <asp:Button ID="imbActualiza" runat="server" Text="Actualizar" OnClientClick="this.disabled = true;"
                    UseSubmitBehavior="false" OnClick="imbActualiza_Click1" ValidationGroup="comi" />
                <asp:Button ID="imbSalir" runat="server" Text="Salir" OnClientClick="this.disabled = true;"
                    UseSubmitBehavior="false" OnClick="imbSalir_Click1" />
                <ajaxToolkit:ConfirmButtonExtender ID="CbeimbCrear" runat="server" TargetControlID="imbCrear"
                    ConfirmText="Esta Seguro(a) de Ingresar la Información ? ">
                </ajaxToolkit:ConfirmButtonExtender>
                <ajaxToolkit:ConfirmButtonExtender ID="CbeimbActualiza" runat="server" TargetControlID="imbActualiza"
                    ConfirmText="Esta Seguro(a) de Modificar la Información ? ">
                </ajaxToolkit:ConfirmButtonExtender>
                <%--                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
                --%>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblBuscar" visible="false">
        <tr>
            <td class="td1">
                Operador
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusOperador" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Fuente
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusFuente" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Punto del SNT
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusPozo" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
        width="90%">
        <tr>
            <td colspan="2" align="center">
                <div>
                    <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgConsulta_EditCommand" OnPageIndexChanged="dtgConsulta_PageIndexChanged"
                        HeaderStyle-CssClass="th1" PageSize="30">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <%--0--%>
                            <asp:BoundColumn DataField="codigo_declaracion" HeaderText="cod" Visible="false">
                            </asp:BoundColumn>
                            <%--1--%>
                            <asp:BoundColumn DataField="año_declaracion" HeaderText="Año Dec" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <%--2--%>
                            <asp:BoundColumn DataField="numero_rueda" HeaderText="Rueda" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <%--3--%>
                            <asp:BoundColumn DataField="fecha_rueda" HeaderText="Fecha Rueda" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <%--4--%>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod Ope" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <%--5--%>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--6--%>
                            <asp:BoundColumn DataField="tipo_declaracion" Visible="false" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--7--%>
                            <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cod Fte" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <%--8--%>
                            <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--9--%>
                            <asp:BoundColumn DataField="campo_declinacion" HeaderText="En declinacion" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--10--%>
                            <asp:BoundColumn DataField="codigo_pozo" HeaderText="Cod Punto" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <%--11--%>
                            <asp:BoundColumn DataField="desc_pozo" HeaderText="Punto SNT" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--12--%>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="right"
                                DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                            <%--13--%>
                            <asp:BoundColumn DataField="precio_reserva" HeaderText="Precio reserva" ItemStyle-HorizontalAlign="right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            <%--14--%>
                            <asp:BoundColumn DataField="precio_delta" HeaderText="delta precio" ItemStyle-HorizontalAlign="right"
                                DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                            <%--15--%>
                            <asp:BoundColumn DataField="estado" HeaderText="estado" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <%--16--%>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass="">
                            </asp:EditCommandColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </div>
                <asp:HiddenField ID="hndCodigo" runat="server" />
                <asp:HiddenField ID="hndTipoDec" runat="server" />
                <asp:HiddenField ID="hndIngDelta" runat="server" />
                <asp:HiddenField ID="hndAccion" runat="server" />
                <asp:HiddenField ID="hndPozo" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
