﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_conformaRutaTra.aspx.cs"
    Inherits="Procesos_frm_conformaRutaTra" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" Text="Conformacion Rutas Subasta de Transporte" />
                    </h3>
                </div>

            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body" runat="server">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="imbCrear" runat="server" CssClass="btn btn-primary" Text="Crear" OnClick="imbCrear_Click1" ValidationGroup="Asignar" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="imbExcel" runat="server" CssClass="btn btn-primary" Text="Excel" OnClick="imbExcel_Click1" ValidationGroup="Asignar" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        </div>
                    </div>

                </div>
                <%--Grilla--%>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" 
                                Width="100%" CssClass="table-bordered">
                                <Columns>
                                    <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_ruta" HeaderText="Código Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="año_entrega" HeaderText="Año entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="trimestre_entrega" HeaderText="Trimestre Entrega" ItemStyle-HorizontalAlign="Left" ></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_trimestre" HeaderText="Descripción Trimestre" ItemStyle-HorizontalAlign="Left" ></asp:BoundColumn>
                                    <asp:BoundColumn DataField="capacidad" HeaderText="Capacidad (KPCD)" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
