﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_Reactivar_5.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_Reactivar_5" EnableEventValidation="false" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">
        //        document.location = no;
        function confirmar() {
            return confirm("Esta seguro que desea reactivar la rueda!")
        }

    </script>



    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td6" align="center" colspan="2">
                        <asp:Label ID="lblTitulo" runat="server" Text="REACTIVAR RUEDA" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="2">
                        Observaciones:
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="2">
                        <asp:TextBox ID="TxtObservacion" runat="server" Width="250px" MaxLength="1000" Rows="3"
                            TextMode="MultiLine"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" class="td6" colspan="2">
                        <br />
                        <asp:Button ID="btnReactivar" Text="Reactivar" runat="server" OnClientClick="return confirmar();"
                            OnClick="btnReactivar_Click" ValidationGroup="comi" CausesValidation="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" class="td6">
                        <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>