﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_DecPre_1.aspx.cs" Inherits="Procesos_frm_DecPre_1" MasterPageFile="~/PlantillaPrincipal.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td class="td6" colspan="2" align="center">
                <asp:Label ID="lblTituo" runat="server" Text="DECLARACION DE PRECIO DE RESERVA" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td6" style="width: 30%;">
                Archivo
            </td>
            <td class="td6" style="width: 70%;">
                <asp:FileUpload ID="FuArchivo" runat="server" EnableTheming="true" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center" class="td6">
                <br />
                <asp:Button ID="BtnCargar" runat="server" Text="Cargue Archivo" OnClick="BtnCargar_Click" /><br />
                <asp:HiddenField ID="hndID" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center" class="td6">
                <asp:Label ID="ltCargaArchivo" runat="server" Width="100%" ForeColor="white"></asp:Label>
                <asp:HiddenField ID="hdfNomArchivo" runat="server" />
            </td>
            </td>
        </tr>
    </table>

</asp:Content>