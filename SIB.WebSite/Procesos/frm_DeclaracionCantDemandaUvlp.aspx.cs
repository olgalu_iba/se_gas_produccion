﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Procesos
{
    public partial class frm_DeclaracionCantDemandaUvlp : Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Declaración Información Total Energía Demandada Subasta Úselo o Véndalo a Largo Plazo";
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        clConexion lConexion = null;
        clConexion lConexion1 = null;
        SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.AprobarOnclick += btnAprobar_Click;
            buttons.DesaprobarOnclick += btnDesAprobar_Click;

            //Establese los permisos del sistema;

            lConexion = new clConexion(goInfo);

            if (IsPostBack) return;

            //Titulo
            Master.Titulo = "Subasta";

            try
            {
                lblTitulo.Text = lsTitulo;
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador !=0 order by codigo_operador", 0, 4);
                LlenarControles(lConexion.gObjConexion, ddlPuntoSalida, "m_punto_salida_snt", " estado = 'A' order by descripcion", 0, 2);
                lConexion.Cerrar();
                //Botones
                EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Aprobar, EnumBotones.Desaprobar };

                // Carga informacion de combos
                if (Session["tipoPerfil"].ToString() == "N")
                {
                    ddlOperador.SelectedValue = goInfo.cod_comisionista;
                    ddlOperador.Enabled = false;
                }
                else
                {
                    EnumBotones[] bt = { EnumBotones.Buscar };
                    botones = bt;
                }
                buttons.Inicializar(botones: botones);
                CargarDatos();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Problemas en la Carga de la Pagina. " + ex.Message);
            }
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                }
                lDdl.Items.Add(lItem1);

            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_codigo_operador", "@P_codigo_punto_salida" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { ddlOperador.SelectedValue, ddlPuntoSalida.SelectedValue };
            string lsIdReg = "";
            dtgSuministro.Visible = true;

            try
            {
                lConexion.Abrir();
                dtgSuministro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetDecTotDemanUvlp", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgSuministro.DataBind();
                if (dtgSuministro.Items.Count <= 0)
                {
                    dtgSuministro.Visible = false;
                    Toastr.Info(this, "No hay Registros para Visualizar.!");
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            CargarDatos();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAprobar_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_operador", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char };
            string[] lValorParametros = { ddlOperador.SelectedValue, "1" };
            var lblMensaje = new StringBuilder();
            lValorParametros[0] = goInfo.cod_comisionista;
            lConexion.Abrir();
            goInfo.mensaje_error = "";
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetTotalEnergiaDemUvlp", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
            if (goInfo.mensaje_error != "")
            {
                Toastr.Error(this, "Se presentó un Problema en la aprobación de la cantidad total de energía.! " + goInfo.mensaje_error);
                lConexion.Cerrar();
            }
            else
            {
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                    {
                        var error = lLector["Error"].ToString();
                        if (!string.IsNullOrEmpty(error))
                        {
                            lblMensaje.Append(error);
                        }
                    }
                    lLector.Close();
                }
                else
                {
                    Toastr.Success(this, "Se aprobó correctamente cantidad total de energía.!");
                    CargarDatos();
                }
            }

            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                Toastr.Error(this, lblMensaje.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDesAprobar_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_operador", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char };
            string[] lValorParametros = { ddlOperador.SelectedValue, "2" };
            lValorParametros[0] = goInfo.cod_comisionista;
            lConexion.Abrir();
            goInfo.mensaje_error = "";
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetTotalEnergiaDemUvlp", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
            if (goInfo.mensaje_error != "")
            {
                Toastr.Error(this, "Se presentó un Problema en la aprobación de la cantidad total de energía.! " + goInfo.mensaje_error);
                lConexion.Cerrar();
            }
            else
            {
                lLector.Read();
                var lsError = lLector["Error"].ToString();
                if (string.IsNullOrEmpty(lsError))
                {
                    Toastr.Success(this, "Se desaprobó correctamente cantidad total de energía.!");
                    CargarDatos();
                }
                else
                    Toastr.Error(this, lsError);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                string lsNombreArchivo = Session["login"] + "InfExcelDecInf" + DateTime.Now + ".xls";
                StringBuilder lsb = new StringBuilder();
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                lpagina.EnableEventValidation = false;
                lpagina.Controls.Add(lform);
                dtgSuministro.EnableViewState = false;

                lform.Controls.Add(dtgSuministro);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = Encoding.Default;

                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + "Contratos Disponibles para Declaracion de Informacion Suministro" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write(lsb.ToString());
                Response.End();
                Response.Flush();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }
    }
}