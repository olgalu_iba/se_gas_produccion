﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_CurvaAgregada.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Procesos_frm_CurvaAgregada" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" Text="CURVA DE OFERTA Y DEMANDA AGREGADA" />

                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblPunto" runat="server" Font-Bold="true"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">

                            <asp:Chart ID="grfCurva" runat="server" BackColor="GradientActiveCaption" BackGradientStyle="VerticalCenter"
                                Width="530px" Height="400px" Palette="Fire">
                                <Series>
                                    <asp:Series ChartArea="ChartArea1" Name="Venta" ChartType="StepLine" BorderWidth="2"
                                        XValueType="Int32" Color="Blue">
                                    </asp:Series>
                                    <asp:Series ChartArea="ChartArea1" Name="VentaP" ChartType="Point" BorderWidth="2"
                                        XValueType="Int32" Color="Red">
                                    </asp:Series>
                                    <asp:Series ChartArea="ChartArea1" Name="Compra" ChartType="StepLine" BorderWidth="2"
                                        XValueType="Int32" Color="Green">
                                    </asp:Series>
                                    <asp:Series ChartArea="ChartArea1" Name="CompraP" ChartType="Point" BorderWidth="2"
                                        XValueType="Int32" Color="Red">
                                    </asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="false">
                                    </asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>

                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">

                            <asp:Label ID="lblMensaje" runat="server"></asp:Label>
                            <asp:ImageButton ID="imbRegresar" runat="server" ImageUrl="~/Images/Regresar.png"
                                OnClick="imbRegresar_Click" Height="40" />
                        </div>
                    </div>

                    <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                        <asp:DataGrid ID="dtgInformacion" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            PagerStyle-HorizontalAlign="Center"
                            PageSize="50">
                            <Columns>
                                <asp:BoundColumn DataField="precio" HeaderText="Precio" DataFormatString="{0: ###,###,##0.00}"
                                    ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_venta" HeaderText="Cantidad Venta" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0: ###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_compra" HeaderText="Cantidad Compra" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0: ###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_min" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad_max" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="precio_min" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="precio_max" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="precio_venta" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="precio_compra" Visible="false"></asp:BoundColumn>
                            </Columns>

                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>

                </div>
            </div>
        </div>
    </div>


</asp:Content>
