﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_Suspender_2.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Procesos_frm_Suspender_2" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">
        //        document.location = no;
        function confirmar() {
            return confirm("Esta seguro que desea suspender la rueda!")
        }

    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td6" align="center" colspan="2">
                        <asp:Label ID="lblTitulo" runat="server" Text="SUSPENDER RUEDA" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="2">
                        <asp:Label ID="lblEstado" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="td6">Suspensión Definitiva
                    </td>
                    <td class="td6">
                        <asp:DropDownList ID="ddlDefinitivo" runat="server">
                            <asp:ListItem Value="N">No</asp:ListItem>
                            <asp:ListItem Value="S">Si</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="2">Observaciones:
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="2">
                        <asp:TextBox ID="TxtObservacion" runat="server" Width="350px" MaxLength="1000" Rows="3" TextMode="MultiLine"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" class="td6" colspan="2">
                        <br />
                        <asp:Button ID="btnSuspender" Text="Suspender" runat="server" OnClientClick="return confirmar();"
                            OnClick="btnSuspender_Click" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
