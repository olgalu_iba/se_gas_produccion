﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using PCD_Infraestructura.Business;
using PCD_Infraestructura.Transaction;
using PCD_Infraestructura.DomainLayer;
using System.Diagnostics;

public partial class Procesos_frm_produndidad_5 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        string lsId = "0";
        if (this.Request.QueryString["ID"] != null && this.Request.QueryString["ID"].ToString() != "")
            lsId = this.Request.QueryString["ID"].ToString();

        ltTablero.Text = "";
        ltTablero.Text += "<table border='0' cellspacing='0' cellpading='0'><tr>";
        ltTablero.Text += "<td  width='100%'align='center' bgcolor='#75923C' colspan='4'><font color='#000000' face='Arial, Helvetica, sans-serif'>MEJORES OFERTAS POR ID: <b>" + lsId + "</b></font></td>";
        ltTablero.Text += "</tr>";
        ltTablero.Text += "<tr>";
        ltTablero.Text += "<td width='10%' align='center' bgcolor='#75923C'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>AGENTE</font></td>";
        ltTablero.Text += "<td width='20%' align='center' bgcolor='#75923C'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>FECHA</font></td>";
        ltTablero.Text += "<td width='20%' align='center' bgcolor='#75923C'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>PRECIO</font></td>";
        ltTablero.Text += "<td width='20%' align='center' bgcolor='#75923C'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>MSG</font></td>";
        ltTablero.Text += "</tr>";

        try
        {

            string[] lsNombreParametros = { "@P_numero_rueda", "@P_id", "@P_codigo_operador" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { Session["numero_rueda"].ToString(), lsId, goInfo.cod_comisionista };
            SqlDataReader lLector;
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetMejoresPosturas5", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
            if (goInfo.mensaje_error == "")
            {
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                    {
                        string lsBoton = "  <input type='button' name='mensajes' id='mensajes' value='Mensaje'  onClick='window.open(\"frm_enviarMensaje_5.aspx?codOperador=" + lLector["codigo_operador"].ToString() + "\",\"_blank\",\"top=200, left=500, width=400, height=200\");' class='campo'>";
                        ltTablero.Text += "<tr>";
                        ltTablero.Text += "<td width='10%' align='center' bgcolor='#9BBB59'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["codigo_operador"].ToString() + "</font></td>";
                        ltTablero.Text += "<td width='35%' align='center' bgcolor='#9BBB59'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["fecha"].ToString() + "</font></td>";
                        ltTablero.Text += "<td width='10%' align='center' bgcolor='#9BBB59'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["precio"].ToString() + "</font></td>";
                        if (lLector["codigo_operador"].ToString() != goInfo.cod_comisionista && lLector["codigo_operador"].ToString()!="0")
                            ltTablero.Text += "<td width='10%' align='center' bgcolor='#9BBB59'> " + lsBoton + " </td>";
                        else
                            ltTablero.Text += "<td width='10%' align='center' bgcolor='#9BBB59'> </td>";
                        ltTablero.Text += "</tr>";
                    }
                    ltTablero.Text += "</table>";
                }
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
        }
    }
}
