﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using PCD_Infraestructura.Business;
using PCD_Infraestructura.Transaction;
using PCD_Infraestructura.DomainLayer;
using System.Diagnostics;

public partial class Procesos_frm_contratos : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        ltTablero.Text = "";
        ltTablero.Text += "<table border='1' style='border-color:#FF9900'><tr>";
        ltTablero.Text += "<td  width='100%'align='center' bgcolor='#333333' style='border-color:#FF9900'  colspan='5'><font color='#FF9900' face='Arial, Helvetica, sans-serif'>MIS CONTRATOS</font></td>";
        ltTablero.Text += "</tr>";
        ltTablero.Text += "<tr>";
        ltTablero.Text += "<td width='10%' style='border-color:#FF9900' align='center' bgcolor='#333333'><font color='#FF9900' face='Arial, Helvetica, sans-serif' size='3px'>IDG</font></td>";
        ltTablero.Text += "<td width='10%' style='border-color:#FF9900' align='center' bgcolor='#333333'><font color='#FF9900' face='Arial, Helvetica, sans-serif' size='3px'>CANTIDAD</font></td>";
        ltTablero.Text += "<td width='30%' style='border-color:#FF9900' align='center' bgcolor='#333333'><font color='#FF9900' face='Arial, Helvetica, sans-serif' size='3px'>PRECIO</font></td>";
        ltTablero.Text += "<td width='20%'  style='border-color:#FF9900' align='center' bgcolor='#333333'><font color='#FF9900' face='Arial, Helvetica, sans-serif' size='3px'>COMPRADOR</font></td>";
        ltTablero.Text += "<td width='20%'  style='border-color:#FF9900' align='center' bgcolor='#333333'><font color='#FF9900' face='Arial, Helvetica, sans-serif' size='3px'>VENDEDOR</font></td>";
        ltTablero.Text += "</tr>";

        try
        {

            string[] lsNombreParametros = { "@P_numero_rueda" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int};
            string[] lValorParametros = { Session["numero_rueda"].ToString() };
            SqlDataReader lLector;
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetMisContratos", lsNombreParametros, lTipoparametros, lValorParametros);
            if (lLector.HasRows)
            {
                while (lLector.Read())
                {
                    ltTablero.Text += "<tr>";
                    ltTablero.Text += "<td width='10%' style='border-color:#FF9900' align='center' bgcolor='#333333'><font color='#FF9900' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["numero_id"].ToString() + "</font></td>";
                    ltTablero.Text += "<td width='10%' style='border-color:#FF9900' align='center' bgcolor='#333333'><font color='#FF9900' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["cantidad"].ToString() + "</font></td>";
                    ltTablero.Text += "<td width='35%' style='border-color:#FF9900' align='center' bgcolor='#333333'><font color='#FF9900' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["precio"].ToString() + "</font></td>";
                    ltTablero.Text += "<td width='25%' style='border-color:#FF9900' align='center' bgcolor='#333333'><font color='#FF9900' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["operador_compra"].ToString() + "</font></td>";
                    ltTablero.Text += "<td width='20%' style='border-color:#FF9900' align='center' bgcolor='#333333'><font color='#FF9900' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["operador_venta"].ToString() + "</font></td>";
                    ltTablero.Text += "</tr>";
                }
                ltTablero.Text += "</table>";
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
            //oLectorArchivo.Close();
            //oLectorArchivo.Dispose();
            /// Creacion del archivo del tablero de RF 
            /// 2011-10-03

        }
        catch (Exception ex)
        {
        }

        //while (!oLectorArchivo.EndOfStream)

        //  lsLineaArchivo = oLectorArchivo.ReadLine();
        //oArregloLinea = lsLineaArchivo.Split(',');
    }
}
