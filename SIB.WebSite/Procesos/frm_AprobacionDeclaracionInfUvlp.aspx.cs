﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;
using System.Text;
using Segas.Web.Elements;

/// <summary>
/// 
/// </summary>
public partial class Procesos_frm_AprobacionDeclaracionInfUvlp : Page
{
    private InfoSessionVO goInfo;
    private static string lsTitulo = "Modificación de la Declaración Información Subasta Úselo o Véndalo a Largo Plazo";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    private clConexion lConexion = null;
    private clConexion lConexion1 = null;
    private SqlDataReader lLector;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;

        //Establese los permisos del sistema
        lConexion = new clConexion(goInfo);

        buttons.FiltrarOnclick += btnConsultar_Click;
        buttons.AprobarOnclick += btnAprobar_Click;

        if (IsPostBack) return;

        //Titulo
        Master.Titulo = "Subasta";

        try
        {
            lblTitulo.Text = lsTitulo;
            lConexion.Abrir();

            string dia = DateTime.Now.Day.ToString();
            string mes = DateTime.Now.Month.ToString();
            string anio = DateTime.Now.Year.ToString();

            hndFecha.Value = anio + "-" + mes + "-" + dia;

            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlFuente, "m_pozo", " estado = 'A' and ind_campo_pto ='C' order by descripcion", 0, 1);
            lConexion.Cerrar();

            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Aprobar };

            // Carga informacion de combos
            if (Session["tipoPerfil"].ToString() == "N")
            {
                if (VerificarExistencia("m_operador ope, m_tipos_operador tpo, m_operador_subasta opeS", "ope.codigo_operador = " + goInfo.cod_comisionista + " and ope.tipo_operador = tpo.sigla  and tpo.codigo_tipo_operador = opeS.codigo_tipo_operador and opeS.codigo_tipo_subasta = 3 and opeS.punta='V' and opeS.estado ='A'"))
                    hdfOpeAut.Value = "S";
                else
                    hdfOpeAut.Value = "N";
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
                EnumBotones[] bt = { EnumBotones.Buscar };
                botones = bt;
            }
            else
            {
                //btnAprobarDeman.Visible = false;
                btnComplementaGas.Visible = false;
                //btnRegDemanda.Visible = false;
                hdfOpeAut.Value = "S";
                dtgDetalleGas.Columns[8].Visible = false;
                //dtgDetalleGas.Columns[9].Visible = false; //20211021 ajsute uvlp
                //dtgDetalleTrans.Columns[9].Visible = false; //20211021 ajsute uvlp
                //dtgDetalleTrans.Columns[10].Visible = false; //20211021 ajsute uvlp
                btnCrearTrans.Visible = false;
            }

            buttons.Inicializar(botones: botones);
            CargarDatos();
        }
        catch (Exception ex)
        {
            Toastr.Error(this, "Problemas en la Carga de la Página. " + ex.Message);
        }
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            if (lsTabla != "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            lDdl.Items.Add(lItem1);

        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControlesPto(SqlConnection lConn, DropDownList lDdl, string lsPozo, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        string[] lsNombreParametros = { "@P_codigo_pozo_ini" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        string[] lValorParametros = { lsPozo };
        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetPtoSalSentido", lsNombreParametros, lTipoparametros, lValorParametros);
        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);
        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }

    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar, string lsIndica)
    {
        if (string.IsNullOrEmpty(modificar)) return;
        try
        {
            if (!manejo_bloqueo("V", modificar))
            {
                // Activo la Pantalla de Captura de Suministro
                if (lsIndica == "G")
                {
                    Modal.Abrir(this, mdlCapturaSuministro.ID, mdlCapturaSuministroInside.ID);
                    lblTitulo.Text = lsTitulo;
                    ddlPunto.Focus();
                    TxtCantidadCont.Text = "";
                    TxtEquivalContraSum.Text = "";
                    TxtCantidadDemanSum.Text = "";
                    TxtEquivalDemanSum.Text = "";
                    TxtObservaGas.Text = "";
                    CargarDetSum();
                }
                // Activo la Pantalla de Captura de Transporte
                if (lsIndica == "T")
                {
                    Modal.Abrir(this, mdlCapturaTransporte.ID, mdlCapturaTransporteInside.ID);
                    // Lleno la grilla
                    if (Session["tipoPerfil"].ToString() == "N")
                        btnCrearTrans.Visible = true;
                    CargarDetTra();
                }
            }
            else
            {
                CargarDatos();
                Toastr.Error(this, "No se Puede complementar el Registro por que esta Bloqueado. Id Registro " + modificar);
            }
        }
        catch (Exception ex)
        {
            Toastr.Error(this, ex.Message);
        }
    }

    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_operador", "@P_destino_rueda" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
        string[] lValorParametros = { ddlOperador.SelectedValue, ddlDestino.SelectedValue };
        lblTitulo.Text = lsTitulo;

        if (hdfOpeAut.Value == "S")
        {

            try
            {

                lConexion.Abrir();
                if (ddlDestino.SelectedValue == "G")
                {
                    dtgTransporte.Visible = false;
                    dtgSuministro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetModificaDeclaUvlp", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgSuministro.DataBind();
                    if (dtgSuministro.Items.Count > 0)
                    {
                        dtgSuministro.Visible = true;
                        //if (Session["tipoPerfil"].ToString() == "N")
                        //    btnAprobarDeman.Visible = false;
                        //else
                        //    btnAprobarDeman.Visible = true;
                    }
                    else
                        Toastr.Info(this, "No hay Registros para Visualizar.!");
                }
                else
                {
                    dtgSuministro.Visible = false;
                    //btnAprobarDeman.Visible = false;
                    dtgTransporte.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetModificaDeclaUvlp", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgTransporte.DataBind();
                    if (dtgTransporte.Items.Count > 0)
                    {
                        dtgTransporte.Visible = true;
                        dtgTransporteTramo.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetTramosModDecInfExc", lsNombreParametros, lTipoparametros, lValorParametros);
                        dtgTransporteTramo.DataBind();
                    }
                    else
                        Toastr.Info(this, "No hay Registros para Visualizar.!");
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }
        else
            Toastr.Error(this, "El tipo de operador no está habilitado como vendedor");
    }

    /// <summary>
    /// Nombre: dtgSuministro_EditCommand
    /// Fecha: Agosto 25 de 2015
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar la Modificacion del Registro del Contrato de Suministro con el
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgSuministro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        if (!VerificarExistencia("t_cnt_demandada_uvlp_tmp", "codigo_operador = " + goInfo.cod_comisionista + " and ano_declaracion = year(getdate())"))
        {
            if (e.CommandName.Equals("Modificar"))
            {
                try
                {
                    hdfNoId.Value = dtgSuministro.Items[e.Item.ItemIndex].Cells[19].Text;  //20180404 rq002-18
                    hdfOperaCompra.Value = dtgSuministro.Items[e.Item.ItemIndex].Cells[4].Text; //20180404 rq002-18
                    hdfPuntoSalida.Value = dtgSuministro.Items[e.Item.ItemIndex].Cells[20].Text; //20180404 rq002-18
                    hdfIdComplementa.Value = dtgSuministro.Items[e.Item.ItemIndex].Cells[19].Text; //20180404 rq002-18
                    hdfModifica.Value = dtgSuministro.Items[e.Item.ItemIndex].Cells[16].Text; //20180404 rq002-18
                    try
                    {
                        ddlFuente.SelectedValue = dtgSuministro.Items[e.Item.ItemIndex].Cells[14].Text; //20180404 rq002-18
                    }
                    catch (Exception ex)
                    {
                    }
                    string[] lsNombreParametros = { "@P_cod_cont_mp_uvlp" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int };
                    String[] lValorParametros = { hdfNoId.Value };
                    lConexion.Abrir();
                    ddlPunto.Items.Clear();
                    LlenarControlesPto(lConexion.gObjConexion, ddlPunto, dtgSuministro.Items[e.Item.ItemIndex].Cells[21].Text, 0, 1); //20180404 rq002-18
                    DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_GetModDecInfUvlpSum", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    lConexion.Cerrar();

                    Modificar(hdfNoId.Value, "G");

                }
                catch (Exception ex)
                {
                    Toastr.Error(this, "Problema en la Recuperación de la información." + ex.Message);
                }
            }
        }
        else
            Toastr.Error(this, "No se Puede modificar el Registro por que ya se aprobó la declaración total de energía");

        //if (((LinkButton)e.CommandSource).Text == "Modif. Energia Demandada")
        //{
        //    try
        //    {
        //        hdfNoId.Value = this.dtgSuministro.Items[e.Item.ItemIndex].Cells[1].Text;
        //        hdfOperaCompra.Value = this.dtgSuministro.Items[e.Item.ItemIndex].Cells[12].Text;
        //        hdfPuntoSalida.Value = this.dtgSuministro.Items[e.Item.ItemIndex].Cells[11].Text;
        //        hdfIdComplementa.Value = this.dtgSuministro.Items[e.Item.ItemIndex].Cells[9].Text;
        //        lblPuntoSalida.Text = this.dtgSuministro.Items[e.Item.ItemIndex].Cells[3].Text;
        //        lblNumeroContrato.Text = this.dtgSuministro.Items[e.Item.ItemIndex].Cells[15].Text;
        //        Modificar(hdfNoId.Value, "D");

        //    }
        //    catch (Exception ex)
        //    {
        //        lblMensaje.Text = "Problema en la Recuperacion del Registro.";
        //    }
        //}

    }

    /// <summary>
    /// Nombre: dtgTransporte_EditCommand
    /// Fecha: Agosto 25 de 2015
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar la Modificacion del Registro del Contrato de Transporte con el
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgTransporte_EditCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.Equals("Modificar"))
        {
            try
            {
                hdfNoId.Value = dtgTransporte.Items[e.Item.ItemIndex].Cells[1].Text;
                hdfIdComplementa.Value = dtgTransporte.Items[e.Item.ItemIndex].Cells[14].Text; //20180404 rq002-18
                hdfCodRuta.Value = dtgTransporte.Items[e.Item.ItemIndex].Cells[15].Text; //20180404 rq002-18
                hdfOperaCompra.Value = dtgTransporte.Items[e.Item.ItemIndex].Cells[16].Text; //20180404 rq002-18
                hdfModifica.Value = dtgTransporte.Items[e.Item.ItemIndex].Cells[12].Text;//20180404 rq002-18
                hdfCapacidadOrg.Value = dtgTransporte.Items[e.Item.ItemIndex].Cells[17].Text;//20180404 rq002-18
                txtCapacReal.Text = dtgTransporte.Items[e.Item.ItemIndex].Cells[19].Text;//20180404 rq002-18
                TxtObservaTra.Text = "";
                Modificar(hdfNoId.Value, "T");
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "Problema en la Recuperación de la información." + ex.Message);
            }
        }
    }

    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lstable, string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lstable, lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:-
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='t_contrato_verificacion' and llave_registro='codigo_verif_contrato=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_verif_contrato=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "t_contrato_verificacion";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_contrato_verificacion", lsCondicion1);
        }
        return true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnComplementaGas_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_cod_cont_mp_uvlp_sum", "@P_codigo_operador", "@P_cod_cont_mp_uvlp", "@P_codigo_punto_salida", "@P_cantidad_contratada", "@P_equiv_cont_kpcd", "@P_cantidad_demandada", "@P_equiv_demand_kpcd", "@P_accion", "@P_observacion_modifica", "@P_codigo_fuente" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char, SqlDbType.VarChar, SqlDbType.Int };
        String[] lValorParametros = { "0", goInfo.cod_comisionista, hdfNoId.Value, ddlPunto.SelectedValue, TxtCantidadCont.Text, TxtEquivalContraSum.Text, TxtCantidadDemanSum.Text, TxtEquivalDemanSum.Text, "C", TxtObservaGas.Text, ddlFuente.SelectedValue };
        int liCantidad = 0;
        var mensaje = new StringBuilder();
        if (TxtObservaGas.Text.Trim().Length <= 0)
            mensaje.Append("Debe Ingesar la Observación de la Modificación. <br>");

        if (ddlPunto.SelectedValue == "0")
            mensaje.Append("Debe seleccionar el punto de salida del SNT.!");

        ///  Validacion la cantidad contratada
        if (TxtCantidadCont.Text.Trim().Length > 0)
        {
            try
            {
                liCantidad = Convert.ToInt32(TxtCantidadCont.Text);
                if (liCantidad < 0)
                    mensaje.Append("Valor Inválido en la cantidad contratada.!");
            }
            catch (Exception ex)
            {
                mensaje.Append("Valor Inválido en la cantidad contratada.!");
            }
        }
        else
            mensaje.Append("Valor Inválido en la cantidad contratada.!");

        ///  Validacion de la Equivalente en KPCD de energía contratada
        if (TxtEquivalContraSum.Text.Trim().Length > 0)
        {
            try
            {
                liCantidad = Convert.ToInt32(TxtEquivalContraSum.Text);
                if (liCantidad < 0)
                    mensaje.Append("Valor Inválido en Equivalente en KPCD de energía contratada.!");

            }
            catch (Exception ex)
            {
                mensaje.Append("Valor Inválido en Equivalente en KPCD de energía contratada.!");
            }
        }
        else
            mensaje.Append("Valor Inválido en Equivalente en KPCD de energía contratada.!");

        ///  Validacion de la Cantidad Demandada por el Titular
        if (TxtCantidadDemanSum.Text.Trim().Length > 0)
        {
            try
            {
                liCantidad = Convert.ToInt32(TxtCantidadDemanSum.Text);
                if (liCantidad < 0)
                    mensaje.Append("Valor Inválido en Cantidad Demandada por el Titular.!");
            }
            catch (Exception ex)
            {
                mensaje.Append("Valor Inválido en Cantidad Demandada por el Titular.!");
            }
        }
        else
            mensaje.Append("Valor Inválido en Cantidad Demandada por el Titular.!");

        ///  Validacion de laEquivalente en KPCD de Cantidad Demandada
        if (TxtEquivalDemanSum.Text.Trim().Length > 0)
        {
            try
            {
                liCantidad = Convert.ToInt32(TxtEquivalDemanSum.Text);
                if (liCantidad < 0)
                    mensaje.Append("Valor Inválido en Equivalente en KPCD de Cantidad Demandada.!<br>");
            }
            catch (Exception ex)
            {
                mensaje.Append("Valor Inválido en Equivalente en KPCD de Cantidad Demandada.!<br>");
            }
        }
        else
            mensaje.Append("Debe Ingresar campo Equivalente en KPCD de Cantidad Demandada.!<br>");

        if (VerificarExistencia("t_cont_mp_uvlp_sum_tmp ", "cod_cont_mp_uvlp = " + hdfNoId.Value + " and codigo_punto_salida=" + ddlPunto.SelectedValue))
            mensaje.Append("Ya se ingresó el punto da salida .!");

        if (!string.IsNullOrEmpty(mensaje.ToString()))
        {
            Toastr.Error(this, mensaje.ToString());
            return;
        }

        string lsError = "";
        lConexion.Abrir();
        goInfo.mensaje_error = "";
        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModDecInfUvlpSum", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
        if (goInfo.mensaje_error != "")
        {
            Toastr.Error(this, "Se presentó un Problema en el Ingreso de la Declaración.! ");
            lConexion.Cerrar();
        }
        else
        {
            if (lLector.HasRows)
            {
                lLector.Read();
                Toastr.Error(this, lLector["error"].ToString());
                lLector.Close();
                lLector.Dispose();
            }
            else
            {
                ddlPunto.SelectedValue = "0";
                TxtCantidadCont.Text = "";
                TxtEquivalContraSum.Text = "";
                TxtCantidadDemanSum.Text = "";
                TxtEquivalDemanSum.Text = "";
                TxtObservaGas.Text = "";
                CargarDetSum();
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnModificaGas_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_cod_cont_mp_uvlp_sum", "@P_codigo_operador", "@P_cod_cont_mp_uvlp", "@P_codigo_punto_salida", "@P_cantidad_contratada", "@P_equiv_cont_kpcd", "@P_cantidad_demandada", "@P_equiv_demand_kpcd", "@P_accion", "@P_observacion_modifica", "@P_codigo_fuente" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char, SqlDbType.VarChar, SqlDbType.Int };
        String[] lValorParametros = { hdfCodContSum.Value, goInfo.cod_comisionista, hdfIdComplementa.Value, ddlPunto.SelectedValue, TxtCantidadCont.Text, TxtEquivalContraSum.Text, TxtCantidadDemanSum.Text, TxtEquivalDemanSum.Text, "M", TxtObservaGas.Text, ddlFuente.SelectedValue };
        int liCantidad = 0;
        var mensaje = new StringBuilder();
        ///  Validacion la cantidad contratada
        if (TxtCantidadCont.Text.Trim().Length > 0)
        {
            try
            {
                liCantidad = Convert.ToInt32(TxtCantidadCont.Text);
                if (liCantidad < 0)
                    mensaje.Append("Valor Inválido en la cantidad contratada.!<br>");
            }
            catch (Exception ex)
            {
                mensaje.Append("Valor Inválido en la cantidad contratada.!<br>");
            }
        }
        else
            mensaje.Append("Debe Ingresar la cantidad contratada.!<br>");

        ///  Validacion de la Equivalente en KPCD de energía contratada
        if (TxtEquivalContraSum.Text.Trim().Length > 0)
        {
            try
            {
                liCantidad = Convert.ToInt32(TxtEquivalContraSum.Text);
                if (liCantidad < 0)
                    mensaje.Append("Valor Inválido en Equivalente en KPCD de energía contratada.!<br>");
            }
            catch (Exception ex)
            {
                mensaje.Append("Valor Inválido en Equivalente en KPCD de energía contratada.!<br>");
            }
        }
        else
            mensaje.Append("Debe Ingresar el campo Equivalente en KPCD de energía contratada.!<br>");

        ///  Validacion de la Cantidad Demandada por el Titular
        if (TxtCantidadDemanSum.Text.Trim().Length > 0)
        {
            try
            {
                liCantidad = Convert.ToInt32(TxtCantidadDemanSum.Text);
                if (liCantidad < 0)
                    mensaje.Append("Valor Inválido en Cantidad Demandada por el Titular.!<br>");
            }
            catch (Exception ex)
            {
                mensaje.Append("Valor Inválido en Cantidad Demandada por el Titular.!<br>");
            }
        }
        else
            mensaje.Append("Debe Ingresar el campo Cantidad Demandada por el Titular.!<br>");

        ///  Validacion de laEquivalente en KPCD de Cantidad Demandada
        if (TxtEquivalDemanSum.Text.Trim().Length > 0)
        {
            try
            {
                liCantidad = Convert.ToInt32(TxtEquivalDemanSum.Text);
                if (liCantidad < 0)
                    mensaje.Append("Valor Inválido en Equivalente en KPCD de Cantidad Demandada.!<br>");
            }
            catch (Exception ex)
            {
                mensaje.Append("Valor Inválido en Equivalente en KPCD de Cantidad Demandada.!<br>");
            }
        }
        else
            mensaje.Append("Debe Ingresar el campo Equivalente en KPCD de Cantidad Demandada.!<br>");

        if (TxtObservaGas.Text == "")
            mensaje.Append("Debe Ingresar las observaciones de modificación <br>");

        if (!string.IsNullOrEmpty(mensaje.ToString()))
        {
            Toastr.Error(this, mensaje.ToString());
            return;
        }
        string lsError = "";
        lConexion.Abrir();
        goInfo.mensaje_error = "";
        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModDecInfUvlpSum", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
        if (goInfo.mensaje_error != "")
        {
            Toastr.Error(this, "Se presentó un Problema en el Ingreso de la Información.! ");
            lConexion.Cerrar();
        }
        else
        {
            if (lLector.HasRows)
            {
                lLector.Read();
                Toastr.Error(this, lLector["error"].ToString());
                lLector.Close();
                lLector.Dispose();
            }
            else
            {
                ddlPunto.Enabled = true;
                ddlPunto.SelectedValue = "0";
                TxtCantidadCont.Text = "";
                TxtEquivalContraSum.Text = "";
                TxtCantidadDemanSum.Text = "";
                TxtEquivalDemanSum.Text = "";
                TxtObservaGas.Text = "";
                CargarDetSum();
                btnComplementaGas.Visible = true;
                btnModificaGas.Visible = false;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        CargarDatos();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAprobar_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_destino_rueda", "@P_operador_compra", "@P_indica", "@P_cod_cont_mp_uvlp" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Char, SqlDbType.Int };
        String[] lValorParametros = { "0", "G", goInfo.cod_comisionista, "2", "0" };
        int liCantidad = 0;
        var mensaje = new StringBuilder();
        var mensaje2 = new StringBuilder();
        int icont = 0;
        TextBox oTexto;
        string sValor = "";
        CheckBox oCheck;
        string lsIdReg = "";
        string lsCodigoReg = "";
        string lsModifica = "S";
        // Valido el ingreso de los datos de la grilla de tramos.
        lConexion.Abrir();
        try
        {
            if (ddlDestino.SelectedValue == "G")
            {
                foreach (DataGridItem Grilla in dtgSuministro.Items)
                {
                    oCheck = (CheckBox)Grilla.Cells[0].Controls[1];
                    if (oCheck.Checked)
                    {
                        icont++;
                        lValorParametros[0] = Grilla.Cells[1].Text;
                        lValorParametros[4] = Grilla.Cells[19].Text; //20180404 rq002-18
                        lValorParametros[1] = "G";
                        goInfo.mensaje_error = "";
                        if (lsModifica == "S")
                        {
                            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModificaDeclaracionUvlp", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                            if (lLector.HasRows)
                            {
                                while (lLector.Read())
                                    mensaje.Append(lLector["Error"] + "<br>");
                                lConexion.Cerrar();
                                lLector.Close();
                                lLector.Dispose();
                                break;
                            }
                            else
                            {
                                mensaje2.Append("Aprobación de la Declaración No. {" + lValorParametros[0] + "} satisfactoria.!\\n");
                                lLector.Close();
                                lLector.Dispose();
                            }
                        }
                        else
                        {
                            mensaje.Append("La Aprobación de la Declaración No. {" + lsCodigoReg + "} NO se realizó YA que no tenía una aprobación pendiente.!\\n"); //20220106 rq001-22
                        }
                    }
                }
            }
            else
            {
                foreach (DataGridItem Grilla in dtgTransporte.Items)
                {
                    oCheck = (CheckBox)Grilla.Cells[0].Controls[1];
                    if (oCheck.Checked)
                    {
                        icont++;
                        lValorParametros[0] = Grilla.Cells[1].Text;
                        lsIdReg = Grilla.Cells[14].Text;   //20180404 rq002-18
                        lsCodigoReg = Grilla.Cells[1].Text;
                        lsModifica = Grilla.Cells[12].Text; //20180404 rq002-18
                        lValorParametros[4] = Grilla.Cells[14].Text;//20180404 rq002-18
                        lValorParametros[1] = "T";
                        goInfo.mensaje_error = "";
                        if (lsModifica == "S")
                        {
                            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModificaDeclaracionUvlp", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                            if (lLector.HasRows)
                            {
                                lLector.Read();
                                mensaje.Append(lLector["Error"] + "<br>");
                                lLector.Close();
                                lLector.Dispose();
                                break;
                            }
                            else
                            {
                                mensaje2.Append("Aprobación de la Declaración No. {" + lsCodigoReg + "} satisfactoria.!\\n");
                                lLector.Close();
                                lLector.Dispose();
                            }
                        }
                        else
                        {
                            mensaje.Append("La Aprobación de la Declaración No. {" + lsCodigoReg + "} NO se realizó YA que no tenía una aprobación pendiente.!\\n"); //20220106 rq001-22
                        }
                    }
                }
            }
            lConexion.Cerrar();
            if (icont == 0)
                mensaje.Append("No seleccionó Registros para Procesar" + "<br>");

            if (!string.IsNullOrEmpty(mensaje.ToString()))
            {
                Toastr.Error(this, mensaje.ToString());
                return;
            }
            Toastr.Success(this, mensaje2.ToString()); //20220106 rq001-22 
            CargarDatos();
        }
        catch (Exception ex)
        {
            lConexion.Cerrar();
            Toastr.Error(this, "Error en la Aprobación de los Registros " + ex.Message);
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ChkTodosSum_CheckedChanged(object sender, EventArgs e)
    {
        //Selecciona o deselecciona todos los items del datagrid segun el checked del control
        CheckBox chk = (CheckBox)(sender);
        foreach (DataGridItem Grilla in dtgSuministro.Items)
        {
            if (Grilla.Cells[0].Enabled)
            {
                CheckBox Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                Checkbox.Checked = chk.Checked;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ChkTodosTra_CheckedChanged(object sender, EventArgs e)
    {
        //Selecciona o deselecciona todos los items del datagrid segun el checked del control
        CheckBox chk = (CheckBox)(sender);
        foreach (DataGridItem Grilla in dtgTransporte.Items)
        {
            if (Grilla.Cells[0].Enabled)
            {
                CheckBox Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                Checkbox.Checked = chk.Checked;
            }
        }
    }

    /// <summary>
    /// Nombre: ddlDestino_SelectedIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ddlDestino_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarDatos();
    }

    /// <summary>
    /// Nombre: CargarDetSum
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDetSum()
    {
        string[] lsNombreParametros = { "@P_cod_cont_mp_uvlp" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        string[] lValorParametros = { hdfNoId.Value };
        try
        {
            lConexion.Abrir();
            dtgTransporte.Visible = false;
            dtgDetalleGas.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetDetModDecInfUvlpSum", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgDetalleGas.DataBind();
            dtgDetalleGas.Visible = true;
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            Toastr.Error(this, ex.Message);
        }
    }

    /// <summary>
    /// Nombre: dtgSuministro_EditCommand
    /// Fecha: Agosto 25 de 2015
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar la Complementacion del Registro del Contrato de Suministro con el
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgDetalleGas_EditCommand(object source, DataGridCommandEventArgs e)
    {
        DateTime ldFecha = DateTime.Now;
        if (e.CommandName.Equals("Modificar"))
        {
            try
            {
                hdfCodContSum.Value = dtgDetalleGas.Items[e.Item.ItemIndex].Cells[0].Text;
                ddlPunto.Enabled = false;
                ddlPunto.SelectedValue = dtgDetalleGas.Items[e.Item.ItemIndex].Cells[2].Text;
                TxtCantidadCont.Text = dtgDetalleGas.Items[e.Item.ItemIndex].Cells[4].Text;
                TxtEquivalContraSum.Text = dtgDetalleGas.Items[e.Item.ItemIndex].Cells[5].Text;
                TxtCantidadDemanSum.Text = dtgDetalleGas.Items[e.Item.ItemIndex].Cells[6].Text;
                TxtEquivalDemanSum.Text = dtgDetalleGas.Items[e.Item.ItemIndex].Cells[7].Text;
                btnModificaGas.Visible = true;
                btnComplementaGas.Visible = false;
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "Problema en la Recuperación del Registro.");
            }
        }
        if (e.CommandName.Equals("Eliminar"))
        {
            try
            {
                string[] lsNombreParametros = { "@P_cod_cont_mp_uvlp_sum", "@P_codigo_operador", "@P_cod_cont_mp_uvlp", "@P_codigo_punto_salida", "@P_accion", "@P_observacion_modifica" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char, SqlDbType.VarChar };
                String[] lValorParametros = { dtgDetalleGas.Items[e.Item.ItemIndex].Cells[0].Text, goInfo.cod_comisionista, hdfIdComplementa.Value, dtgDetalleGas.Items[e.Item.ItemIndex].Cells[2].Text, "E", TxtObservaGas.Text };
                lConexion.Abrir();
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModDecInfUvlpSum", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    Toastr.Error(this, "Se presentó un Problema al eliminar Información.! " + goInfo.mensaje_error);
                    lConexion.Cerrar();
                }
                else
                {
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        Toastr.Error(this, lLector["error"].ToString());
                        lLector.Close();
                        lLector.Dispose();
                    }
                    else
                    {

                        btnLimpiarGas_Click(null, null);
                        CargarDetSum();
                    }
                }
            }
            catch (Exception)
            {
                Toastr.Error(this, "Problema en la eliminación del Registro.");
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCrearTrans_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_destino_rueda", "@P_operador_compra", "@P_codigo_tramo", "@P_cod_cont_mp_uvlp", "@P_cod_cont_mp_uvlp_det", "@P_indica", "@P_observacion_modifica", "@P_capacidad_cont" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
        String[] lValorParametros = { hdfNoId.Value, "T", hdfOperaCompra.Value, "0", hdfIdComplementa.Value, "0", "0", TxtObservaTra.Text, txtCapacReal.Text };
        string[] lsNombreParametrosD = { "@P_cod_cont_mp_uvlp_det", "@P_codigo_verif_contrato", "@P_codigo_tramo", "@P_porc_fijo", "@P_porc_var", "@P_orden", "@P_porc_imp_tra", "@P_porc_cuota" };
        SqlDbType[] lTipoparametrosD = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.Decimal };
        String[] lValorParametrosD = { "0", hdfNoId.Value, "0", "0", "0", "0", "0", "0" };
        int ldPorcentajeF;
        int ldPorcentajeV;
        var mensaje = new StringBuilder();
        var mensaje2 = new StringBuilder();

        if (txtCapacReal.Text == "")
            mensaje.Append("Debe digitar la capacidad contratada real<br>");
        else
        {
            try
            {
                ldPorcentajeF = Convert.ToInt32(txtCapacReal.Text);
                if (ldPorcentajeF < 0)
                    mensaje.Append("La capacidad contratada real debe ser mayor que cero<br>");
                if (ldPorcentajeF > Convert.ToInt32(hdfCapacidadOrg.Value))
                    mensaje.Append("La capacidad contratada real debe ser menor o igual que la capacidad registrada en el contrato<br>");
            }
            catch (Exception)
            {
                mensaje.Append("Valor Inválido en la cantidad contratada real<br>");
            }
        }

        //valida la información registrada
        TextBox oText;
        DropDownList oDrop;
        foreach (DataGridItem Grilla in dtgDetalleTrans.Items)
        {
            ldPorcentajeF = 0;
            ldPorcentajeV = 0;
            if (Grilla.Cells[13].Text == "N")
                mensaje.Append("El tramo " + Grilla.Cells[3].Text + " no tiene definidos los costos<br>");
            else
            {
                oText = (TextBox)Grilla.Cells[4].Controls[1];
                if (oText.Text == "")
                    mensaje.Append("Debe digitar el porcentaje de costo fijo para el tramo " + Grilla.Cells[3].Text + "<br>");
                else
                {
                    try
                    {
                        ldPorcentajeF = Convert.ToInt32(oText.Text);
                        if (ldPorcentajeF > 100)
                            mensaje.Append("El porcentaje de costo fijo para el tramo " + Grilla.Cells[3].Text + " no puede ser mayor que 100<br>");
                    }
                    catch (Exception ex)
                    {
                        mensaje.Append("El porcentaje de costo fijo para el tramo " + Grilla.Cells[3].Text + " no es válido<br>");
                    }
                }

                oText = (TextBox)Grilla.Cells[6].Controls[1];
                if (oText.Text == "")
                    mensaje.Append("Debe digitar el porcentaje de costo variable para el tramo " + Grilla.Cells[3].Text + "<br>");
                else
                {
                    try
                    {
                        ldPorcentajeV = Convert.ToInt32(oText.Text);
                        if (ldPorcentajeV > 100)
                            mensaje.Append("El porcentaje de costo variable para el tramo " + Grilla.Cells[3].Text + " no puede ser mayor que 100<br>");
                    }
                    catch (Exception ex)
                    {
                        mensaje.Append("El porcentaje de costo variable para el tramo " + Grilla.Cells[3].Text + " no es válido<br>");
                    }
                }
                if (ldPorcentajeF + ldPorcentajeV != 100)
                    mensaje.Append("El porcentaje de costo fijo mas el porcentaje de costo variable para el tramo " + Grilla.Cells[3].Text + " debe ser 100<br>");

                oDrop = (DropDownList)Grilla.Cells[19].Controls[1];
                if (oDrop.SelectedValue == "0")
                    mensaje.Append("Debe seleccionar el porcentaje de impuesto de transporte para el tramo " + Grilla.Cells[3].Text + "<br>");
                oDrop = (DropDownList)Grilla.Cells[20].Controls[1];
                if (oDrop.SelectedValue == "0")
                    mensaje.Append("Debe seleccionar el porcentaje de cuota de fomento para el tramo " + Grilla.Cells[3].Text + "<br>");
            }
        }

        if (TxtObservaTra.Text == "")
            mensaje.Append("Debe digitar las observaciones de modificación<br>");

        if (!string.IsNullOrEmpty(mensaje.ToString()))
        {
            Toastr.Error(this, mensaje.ToString());
            return;
        }

        lConexion.Abrir();
        try
        {
            goInfo.mensaje_error = "";
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion,
                "pa_SetModificaDeclaracionUvlp", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
            if (goInfo.mensaje_error == "")
            {
                lLector.Read();
                mensaje.Clear();
                mensaje.Append(lLector["Error"]);
                if (string.IsNullOrEmpty(mensaje.ToString()))
                {
                    lLector.Close();
                    lLector.Dispose();
                    //hdfIdComplementa.Value = "0";
                    goInfo.mensaje_error = "";
                    foreach (DataGridItem Grilla in dtgDetalleTrans.Items)
                    {
                        ldPorcentajeF = 0;
                        ldPorcentajeV = 0;
                        lValorParametrosD[0] = Grilla.Cells[0].Text;
                        lValorParametrosD[2] = Grilla.Cells[2].Text;
                        oText = (TextBox)Grilla.Cells[4].Controls[1];
                        lValorParametrosD[3] = oText.Text;
                        oText = (TextBox)Grilla.Cells[6].Controls[1];
                        lValorParametrosD[4] = oText.Text;
                        lValorParametrosD[5] = Grilla.Cells[9].Text;
                        oDrop = (DropDownList)Grilla.Cells[19].Controls[1];
                        lValorParametrosD[6] = oDrop.SelectedValue;
                        oDrop = (DropDownList)Grilla.Cells[20].Controls[1];
                        lValorParametrosD[7] = oDrop.SelectedValue;

                        DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetModDecInfUvlpTra", lsNombreParametrosD, lTipoparametrosD, lValorParametrosD, goInfo);
                        if (goInfo.mensaje_error == "") continue;
                        mensaje2.Append("Se presentó un error en la creación de la declaración." + goInfo.mensaje_error);
                        //hdfIdComplementa.Value = "0";
                        break;
                    }

                    if (string.IsNullOrEmpty(mensaje2.ToString()))
                    {
                        CargarDatos();
                        btnSalirTrans_Click(null, null);
                        Toastr.Success(this, "Registro Ingresado Correctamente.!");
                    }
                    else
                    {
                        Toastr.Error(this, mensaje2.ToString());
                    }
                }
                else
                    Toastr.Error(this, mensaje.ToString());
            }
            else
            {
                lLector.Close();
                lLector.Dispose();
                mensaje2.Append("Se presentó un error en la creación del registro.<br>");
                //hdfIdComplementa.Value = "0";
            }

            lLector.Close();
            lLector.Dispose();
            if (string.IsNullOrEmpty(mensaje2.ToString()))
            {
                Toastr.Info(this, "Registro Ingresado Correctamente.!");
            }
            else
            {
                Toastr.Error(this, mensaje2.ToString());
            }
        }
        catch (Exception ex)
        {
            Toastr.Error(this, "Error en la Creación de la Declaración. " + ex.Message.ToString());
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TxtPorcFijo_TextChanged(object sender, EventArgs e)
    {
        TextBox oText;
        string sValor;
        foreach (DataGridItem Grilla in dtgDetalleTrans.Items)
        {
            try
            {
                oText = (TextBox)Grilla.Cells[4].Controls[1];
                if (oText.Text != Grilla.Cells[17].Text)
                {
                    Grilla.Cells[17].Text = oText.Text;
                    sValor = Math.Round((Convert.ToDouble(oText.Text) * Convert.ToDouble(Grilla.Cells[14].Text) / 100.0), 3).ToString();
                    Grilla.Cells[5].Text = sValor;
                    oText = (TextBox)Grilla.Cells[6].Controls[1];
                    oText.Focus();
                    break;
                }
            }
            catch (Exception ex)
            {
                Grilla.Cells[5].Text = "0";
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TxtPorcVar_TextChanged(object sender, EventArgs e)
    {
        TextBox oText;
        DropDownList oDrop;
        string sValor;
        foreach (DataGridItem Grilla in dtgDetalleTrans.Items)
        {
            try
            {
                oText = (TextBox)Grilla.Cells[6].Controls[1];
                if (oText.Text != Grilla.Cells[18].Text)
                {
                    Grilla.Cells[18].Text = oText.Text;
                    sValor = Math.Round((Convert.ToDouble(oText.Text) * Convert.ToDouble(Grilla.Cells[15].Text) / 100.0), 3).ToString();
                    Grilla.Cells[7].Text = sValor;
                    oDrop = (DropDownList)Grilla.Cells[19].Controls[1];
                    oDrop.Focus();
                    break;
                }
            }
            catch (Exception ex)
            {
                Grilla.Cells[7].Text = "0";
            }
        }
    }

    /// <summary>
    /// Nombre: CargarDetSum
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDetTra()
    {
        string[] lsNombreParametros = { "@P_codigo_verif_contrato" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        string[] lValorParametros = { hdfNoId.Value };
        TextBox oText;
        try
        {
            lConexion.Abrir();
            dtgDetalleTrans.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetModDecInfUvlpTra", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgDetalleTrans.DataBind();
            DropDownList ddrop;
            foreach (DataGridItem Grilla in dtgDetalleTrans.Items)
            {
                oText = (TextBox)Grilla.Cells[4].Controls[1];
                oText.Text = Grilla.Cells[17].Text;
                oText = (TextBox)Grilla.Cells[6].Controls[1];
                oText.Text = Grilla.Cells[18].Text;
                ddrop = (DropDownList)Grilla.Cells[19].Controls[1];
                LlenarControles(lConexion.gObjConexion, ddrop, "m_cargo_uvlp", " estado = 'A' and codigo_cargo = 1 order by porcentaje", 2, 2);
                try
                {
                    ddrop.SelectedValue = Grilla.Cells[21].Text;
                }
                catch (Exception ex)
                {
                }
                ddrop = (DropDownList)Grilla.Cells[20].Controls[1];
                LlenarControles(lConexion.gObjConexion, ddrop, "m_cargo_uvlp", " estado = 'A' and codigo_cargo = 2 order by porcentaje", 2, 2);
                try
                {
                    ddrop.SelectedValue = Grilla.Cells[22].Text;
                }
                catch (Exception ex)
                {
                }
            }
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            Toastr.Error(this, ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (ddlDestino.SelectedValue == "G")
            {
                string lsNombreArchivo = Session["login"] + "InfExcelDecInf" + DateTime.Now + ".xls";
                StringBuilder lsb = new StringBuilder();
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                lpagina.EnableEventValidation = false;
                lpagina.Controls.Add(lform);
                dtgSuministro.Columns[0].Visible = false;
                dtgSuministro.Columns[4].Visible = true;
                dtgSuministro.Columns[17].Visible = false;

                lform.Controls.Add(dtgSuministro);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = Encoding.Default;

                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + "Contratos de la Declaración de Información Suministro subasta UVLP" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write(lsb.ToString());
                Response.End();
                Response.Flush();
                dtgSuministro.Columns[0].Visible = true;
                dtgSuministro.Columns[4].Visible = false;
                dtgSuministro.Columns[17].Visible = true;

            }
            if (ddlDestino.SelectedValue == "T")
            {
                string lsNombreArchivo = Session["login"] + "InfExcelDecInf" + DateTime.Now + ".xls";
                StringBuilder lsb = new StringBuilder();
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                lpagina.EnableEventValidation = false;
                lpagina.Controls.Add(lform);
                dtgTransporteTramo.Visible = true;
                dtgTransporteTramo.EnableViewState = false;
                lform.Controls.Add(dtgTransporteTramo);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = Encoding.Default;

                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='5' align='left'><font face=Arial size=4>" + "Contratos de la  Declaración de Información Transporte subasta UVLP" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write(lsb.ToString());
                dtgTransporteTramo.Visible = false;
                Response.End();
                Response.Flush();
            }
        }
        catch (Exception ex)
        {
            Toastr.Error(this, "Problemas al Consultar los Contratos. " + ex.Message);
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {

    }

    protected void btnLimpiar_Click(object sender, EventArgs e)
    {
        txtCapacReal.Text = "";
        TxtObservaTra.Text = "";
    }

    #region Captura de Suministro

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSalirGas_Click(object sender, EventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (hdfNoId.Value != "")
            manejo_bloqueo("E", hdfNoId.Value);
        TxtEquivalContraSum.Text = "";
        TxtCantidadDemanSum.Text = "";
        TxtEquivalDemanSum.Text = "";
        CargarDatos();
        Modal.Cerrar(this, mdlCapturaSuministro.ID);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnLimpiarGas_Click(object sender, EventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        ddlPunto.SelectedValue = "0";
        ddlPunto.Enabled = true;
        TxtCantidadCont.Text = "";
        TxtEquivalContraSum.Text = "";
        TxtCantidadDemanSum.Text = "";
        TxtEquivalDemanSum.Text = "";
        btnModificaGas.Visible = false;
        btnComplementaGas.Visible = true;
        if (Session["tipoPerfil"].ToString() != "N")
        {
            btnComplementaGas.Visible = false;
            btnModificaGas.Visible = false;
        }
    }

    #endregion Captura de Suministro

    #region Captura de Transporte

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSalirTrans_Click(object sender, EventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (hdfNoId.Value != "")
            manejo_bloqueo("E", hdfNoId.Value);
        CargarDatos();
        Modal.Cerrar(this, mdlCapturaTransporte.ID);
    }

    #endregion Captura de Transporte
}