﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;

public partial class Procesos_frm_PrecioContReserva : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Definición de precio de operaciones de reserva";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lblMensaje.Text = "";
        if (!IsPostBack)
        {
            if (!VerificarExistencia("m_parametros_sslp", "  convert(varchar(10),getdate(),111) = fecha_precio_reserva"))
            {
                BtnPrecio.Visible = false;
                lblMensaje.Text += "No es el día para realizar el cálculo del precio de reserva<br>";
            }
            if (!VerificarExistencia("t_rueda", "  codigo_tipo_subasta = 8 and estado <> 'F'"))
            {
                BtnPrecio.Visible = false;
                lblMensaje.Text += "No hay ninguna rueda de reservas de PTDFV abierta<br>";
            }
            if (lblMensaje.Text =="")
                consultar();
        }
    }


    protected void BtnPrecio_Click(object sender, EventArgs e)
    {
        SqlDataReader lLector;
        SqlCommand lComando = new SqlCommand();
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();

        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_SetPrecioCOntRes";
        lComando.CommandTimeout = 3600;
        lLector = lComando.ExecuteReader();
        if (lLector.HasRows)
        {
            lLector.Read();
            lblMensaje.Text = lLector["Mensaje"].ToString();
        }
        else
            lLector.Close();
        lLector.Dispose();
        lConexion.Cerrar();
        if (lblMensaje.Text == "")
            consultar();
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void consultar()
    {
        try
        {
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            SqlCommand lComando = new SqlCommand();
            lComando.Connection = lConexion.gObjConexion;
            lComando.CommandTimeout = 3600;
            lComando.CommandType = CommandType.StoredProcedure;
            lComando.CommandText = "pa_GetPrecioContRes";
            lComando.ExecuteNonQuery();
            lsqldata.SelectCommand = lComando;
            lsqldata.Fill(lds);
            dtgConsulta.DataSource = lds;
            dtgConsulta.DataBind();
            tblGrilla.Visible = true;
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Consultar la información.! " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lsTable, string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lsTable, lswhere, goInfo);
    }
}