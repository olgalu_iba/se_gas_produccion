﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_IngresoPostura_5 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    SqlDataReader lLector;
    String strRutaArchivo;
    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString();

            lConexion = new clConexion(goInfo);
            string lsHoraIni = "";
            string lsHoraFin = "";
            int liHoraAct = 0;
            int liValIni = 0;
            int liValFin = 0;
            if (!IsPostBack)
            {
                try
                {
                    hdfNoid.Value = "0";
                    hndAccion.Value = "C";
                    if (this.Request.QueryString["ID"] != null && this.Request.QueryString["ID"].ToString() != "")
                        hdfNoid.Value = this.Request.QueryString["ID"].ToString();

                    hdfOfMia.Value = "N";
                    /// Obtengo los Datos del ID Recibido
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda a, m_tipos_rueda b", " a.codigo_tipo_rueda = b.codigo_tipo_rueda And a.numero_rueda = " + Session["numero_rueda"].ToString());
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        hdfNoRueda.Value = lLector["numero_rueda"].ToString();
                        hdfCodTipoRueda.Value = lLector["codigo_tipo_rueda"].ToString();
                        hdfDestinoProducto.Value = lLector["destino_rueda"].ToString();
                        hdfTipoMercado.Value = lLector["tipo_mercado"].ToString();
                        lsHoraIni = lLector["hora_ini_negociacioni_sci"].ToString();
                        hdfHoraIni.Value = lLector["hora_ini_negociacioni_sci"].ToString();
                        lsHoraFin = lLector["hora_fin_negociacioni_sci"].ToString();
                        hdfHoraFin.Value = lLector["hora_fin_negociacioni_sci"].ToString();
                    }
                    if (hdfDestinoProducto.Value == "G")
                    {
                        lblPtoEnt.Text = "Pto Entrega: ";
                        ddlPtoFin.Visible = false;
                    }
                    else
                    {
                        lblPtoEnt.Text = "Ruta: ";
                        ddlPtoFin.Visible = true;
                    }
                    lLector.Dispose();
                    lLector.Close();
                    liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
                    liValIni = ((Convert.ToDateTime(lsHoraIni).Hour * 60) + Convert.ToDateTime(lsHoraIni).Minute);
                    liValFin = ((Convert.ToDateTime(lsHoraFin).Hour * 60) + Convert.ToDateTime(lsHoraFin).Minute);
                    //si la rueda no esta activa pero el horario corresponde a rueda activa se visualiza error
                    if (liHoraAct < liValIni || liHoraAct > liValFin)
                    {
                        Toastr.Warning(this, "Esta intentando Ingresar una Oferta Fuera del Horario de Ingreso de Ofertas.");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                    }
                    else
                    {
                        if (hdfNoid.Value == "0")
                        {
                            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_producto prd, m_caracteristica_sub carP ", " carP.codigo_tipo_subasta = 5 And carP.destino_rueda = '" + hdfDestinoProducto.Value + "' and carP.tipo_mercado ='" + hdfTipoMercado.Value + "' and  carP.tipo_caracteristica ='P' and prd.codigo_producto = carP.codigo_caracteristica and carP.estado ='A'");
                            if (lLector.HasRows)
                            {
                                lLector.Read();
                                lblProducto.Text = lLector["descripcion"].ToString();
                                hdfCodProducto.Value = lLector["codigo_producto"].ToString();
                            }
                            lLector.Dispose();
                            lLector.Close();
                        }
                        /// LLeno las Grillas
                        if (hdfDestinoProducto.Value == "G" && hdfTipoMercado.Value == "P")
                            LlenarControles(lConexion.gObjConexion, ddlLugarEntrega, "m_pozo poz, m_caracteristica_sub carC ", "  poz.estado = 'A' And carC.codigo_tipo_subasta = 5 And carC.destino_rueda = 'G' and carC.tipo_mercado = 'P' and carC.tipo_caracteristica = 'C' and carC.estado ='A' and poz.codigo_tipo_campo= carC.codigo_caracteristica order by descripcion", 0, 1);
                        else
                            LlenarControles(lConexion.gObjConexion, ddlLugarEntrega, "m_pozo poz, m_caracteristica_sub carT ", " poz.estado = 'A' And carT.codigo_tipo_subasta = 5 And carT.destino_rueda = 'G' and carT.tipo_mercado = 'S' and carT.tipo_caracteristica = 'T' and carT.estado ='A' and poz.codigo_pozo= carT.codigo_caracteristica order by descripcion", 0, 1);
                        LlenarControles(lConexion.gObjConexion, ddlTipoContrato, "m_modalidad_contractual mod, m_caracteristica_sub carM ", " mod.estado = 'A' And carM.codigo_tipo_subasta = 5 And carM.destino_rueda = '" + hdfDestinoProducto.Value + "' and carM.tipo_mercado = '" + hdfTipoMercado.Value + "' and carM.tipo_caracteristica = 'M' and carM.estado ='A' and mod.codigo_modalidad= carM.codigo_caracteristica order by descripcion", 0, 1);
                        LlenarControles(lConexion.gObjConexion, ddlPeriodoEnt, "m_periodos_entrega per, m_caracteristica_sub carE ", " per.estado = 'A' And carE.codigo_tipo_subasta = 5 And carE.destino_rueda = '" + hdfDestinoProducto.Value + "' and carE.tipo_mercado = '" + hdfTipoMercado.Value + "' and carE.tipo_caracteristica = 'E' and carE.estado ='A' and per.codigo_periodo= carE.codigo_caracteristica order by descripcion", 0, 1);
                        if (hdfNoid.Value != "0")
                        {
                            hndAccion.Value = "M";
                            TxtFechaNegociacion.Enabled = false;
                            TxtHoraNeg.Enabled = false;
                            string[] lsPeriodo;
                            string lsTabla = "";
                            string lsPrecio = "0";
                            //// Modificación del ID YA sea por el Dueño de la Oferta o por el Agresor
                            TrTipo.Visible = false;
                            hdfNoid.Value = this.Request.QueryString["ID"].ToString();
                            string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador", "@P_numero_id" };
                            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                            string[] lValorParametros = { hdfNoRueda.Value, goInfo.cod_comisionista, hdfNoid.Value };
                            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetRuedaPuja5", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                            if (goInfo.mensaje_error == "")
                            {
                                if (lLector.HasRows)
                                {
                                    lLector.Read();
                                    //// Lleno los controles de la pantalla
                                    hdfTipoPostura.Value = lLector["tipo_oferta"].ToString();
                                    ddlTipoPostura.SelectedValue = lLector["tipo_oferta"].ToString();
                                    hdfCodOperador.Value = lLector["codigo_operador"].ToString();
                                    ddlLugarEntrega.SelectedValue = lLector["codigo_punto_entrega"].ToString();
                                    ddlLugarEntrega_SelectedIndexChanged(null, null);
                                    ddlPtoFin.SelectedValue = lLector["codigo_punto_fin"].ToString();
                                    ddlTipoContrato.SelectedValue = lLector["codigo_modalidad"].ToString();
                                    lblProducto.Text = lLector["desc_producto"].ToString();
                                    hdfCodProducto.Value = lLector["codigo_producto"].ToString();
                                    ddlPeriodoEnt.SelectedValue = lLector["codigo_periodo"].ToString() + "-" + lLector["medida_tiempo"].ToString() + "-" + lLector["mes_inicial_periodo"].ToString();
                                    lsPeriodo = ddlPeriodoEnt.SelectedValue.Split('-');
                                    hdfMesInicialPeriodo.Value = lsPeriodo[2].ToString().Trim();
                                    hdfMedidaTiempo.Value = lsPeriodo[1].ToString().Trim();
                                    TxtFechaNegociacion.Text = lLector["fecha_rueda"].ToString();
                                    TxtHoraNeg.Text = lLector["hora_negociacion"].ToString();
                                    TxtFechaInicial.Text = lLector["fecha_entrega"].ToString();
                                    TxtFechaFin.Text = lLector["fecha_entrega_fin"].ToString();
                                    TxtNoAnos.Text = lLector["tiempo_entrega"].ToString();
                                    TxtCantidad.Text = lLector["cantidad_postura"].ToString();
                                    hdfPrePost.Value = lLector["precio"].ToString();
                                    if (goInfo.cod_comisionista == hdfCodOperador.Value)
                                        TxtPrecio.Text = lLector["precio"].ToString();
                                    else
                                        TxtPrecio.Text = lLector["precioA"].ToString();
                                    if (hdfMedidaTiempo.Value == "I")
                                    {
                                        trHora.Visible = true;
                                        trHoraF.Visible = true;
                                        txtHoraIni.Text = lLector["hora_ini"].ToString();
                                        txtHoraFin.Text = lLector["hora_fin"].ToString();
                                    }
                                    else
                                    {
                                        trHora.Visible = false;
                                        trHoraF.Visible = false;
                                        txtHoraIni.Text = "";
                                        txtHoraFin.Text = "";
                                    }
                                    if (hdfMedidaTiempo.Value == "U")
                                        TxtFechaFin.Enabled = true;
                                    else
                                        TxtFechaFin.Enabled = false;
                                    lLector.Close();
                                    lLector.Dispose();
                                    if (hdfMedidaTiempo.Value == "L")
                                    {
                                        TrNoAnos.Visible = true;
                                        TrNoAnos1.Visible = true;
                                    }
                                    else
                                    {
                                        TrNoAnos.Visible = false;
                                        TrNoAnos1.Visible = false;
                                    }

                                    //// Verifico si el usuario que entra a la pantalla es el dueño de la oferta
                                    if (goInfo.cod_comisionista == hdfCodOperador.Value)
                                    {
                                        hdfOfMia.Value = "S";
                                        string lsOrden;
                                        if (hdfTipoPostura.Value == "C")
                                        {
                                            lsTabla = "t_posturas_venta";
                                            lsOrden = " order by precio";
                                        }
                                        else
                                        {
                                            lsTabla = "t_posturas_compra";
                                            lsOrden = " order by precio desc";
                                        }
                                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", lsTabla, " numero_id = " + hdfNoid.Value + lsOrden);
                                        if (lLector.HasRows)
                                        {
                                            lLector.Read();
                                            lsPrecio = lLector["precio"].ToString();
                                            if (hdfTipoPostura.Value == "C")
                                            {
                                                if (Convert.ToDouble(hdfPrePost.Value) >= Convert.ToDouble(lsPrecio))
                                                {
                                                    Toastr.Warning(this, "La Postura NO puede ser Modificada YA que tiene Agreaciones y con mejor Precio.");
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                                                }
                                            }
                                            else
                                            {
                                                if (Convert.ToDouble(hdfPrePost.Value) <= Convert.ToDouble(lsPrecio))
                                                {
                                                    Toastr.Warning(this,"La Postura NO puede ser Modificada YA que tiene Agreaciones y con mejor Precio.");
                                                    
                                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                                                }
                                            }
                                            ddlLugarEntrega.Enabled = false;
                                            ddlPtoFin.Enabled = false;
                                            ddlTipoContrato.Enabled = false;
                                            ddlPeriodoEnt.Enabled = false;
                                            TxtNoAnos.Enabled = false;
                                            TxtFechaInicial.Enabled = false;
                                            TxtFechaFin.Enabled = false;
                                            TxtCantidad.Enabled = false;
                                            if (hdfTipoPostura.Value == "C")
                                                btnOfertar.Text = "Comprar";
                                            else
                                                btnOfertar.Text = "Vender";
                                        }
                                        else
                                        {
                                            if (hdfTipoPostura.Value == "C")
                                                btnOfertar.Text = "Comprar";
                                            else
                                                btnOfertar.Text = "Vender";
                                        }
                                    }
                                    else
                                    {
                                        if (hdfTipoPostura.Value == "C")
                                            btnOfertar.Text = "Vender";
                                        else
                                            btnOfertar.Text = "Comprar";

                                        ddlLugarEntrega.Enabled = false;
                                        ddlPtoFin.Enabled = false;
                                        ddlTipoContrato.Enabled = false;
                                        ddlPeriodoEnt.Enabled = false;
                                        TxtNoAnos.Enabled = false;
                                        TxtFechaInicial.Enabled = false;
                                        TxtFechaFin.Enabled = false;
                                        TxtCantidad.Enabled = false;
                                        txtHoraIni.Enabled = false;
                                        txtHoraFin.Enabled = false;
                                    }
                                }
                            }

                        }
                        else
                            hdfOfMia.Value = "S";
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOfertar_Click(object sender, EventArgs e)
    {
        string oError = "";
        string sMensaje = "";
        DateTime ldFecha;
        string[] lsPeriodo;
        string lsHoraIni = "";
        string lsHoraFin = "";
        int liHoraAct = 0;
        int liValIni = 0;
        int liValFin = 0;
        string lsTabla = "";
        string lsPrecio = "0";
        string oArchivo = strRutaArchivo + "subasta-" + Session["numero_rueda"].ToString() + ".txt";

        if (hdfOfMia.Value == "S")
        {
            if (TxtCantidad.Text == "")
                oError += "Debe digitar la cantidad de la Oferta\\n";
            if (TxtPrecio.Text == "")
                oError += "Debe digitar la precio de la Oferta\\n";
            else
            {
                string sOferta = TxtPrecio.Text.Replace(",", "");
                int iPos = sOferta.IndexOf(".");
                if (iPos > 0)
                    if (sOferta.Length - iPos > 3)
                        oError += "Se permiten máximo 2 decimales en el precio\\n";
                try
                {
                    if (Convert.ToDouble(sOferta) <= 0)
                        oError += "El precio debe ser mayor que cero\\n";
                }
                catch (Exception ex)
                {
                    oError += "El precio digitado no es válido\\n";
                }
            }
            if (ddlTipoPostura.SelectedValue == "S")
                oError += "Debe seleccionar el Tipo de Postura\\n";
            if (TxtFechaInicial.Text.Trim().Length <= 0)
                oError += "Debe ingresar la Fecha Inicial\\n";
            if (TxtFechaNegociacion.Text.Trim().Length <= 0)
                oError += "Debe ingresar la Fecha de Negociacion\\n";
            if (TxtHoraNeg.Text.Trim().Length <= 0)
                oError += "Debe ingresar la Hora de Negociacion\\n";
            else
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaNegociacion.Text.Trim());
                    if (Convert.ToDateTime(TxtFechaNegociacion.Text.Trim() + ' ' + TxtHoraNeg.Text.Trim()) < DateTime.Now.Date)
                        oError += "La Fecha-Hora de Negociación NO puede ser menor a la fecha-hora actual.!\\n";
                    //else
                    //{
                    //    lConexion.Abrir();
                    //    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_festivo", " fecha='" + TxtFechaNegociacion.Text.Trim() + "' and ind_dia_habil='S'");
                    //    if (!lLector.HasRows)
                    //        oError += "La fecha de negociación debe ser un día hábil. \\n";
                    //    lLector.Close();
                    //    lConexion.Cerrar();
                    //}
                }
                catch (Exception ex)
                {
                    oError += "Valor Invalido en Fecha de negociacion.!\\n";
                }
            }
            if (hdfCodProducto.Value == "")
                oError += "NO hay producto parametrizado para el tipo de rueda\\n";
            if (ddlTipoContrato.SelectedValue == "0")
                oError += "Debe seleccionar el Tipo de Contrato\\n";
            if (hdfDestinoProducto.Value == "G")
            {
                if (ddlLugarEntrega.SelectedValue == "0")
                    oError += "Debe seleccionar el Punto de Entrega\\n";
            }
            else
            {
                if (ddlLugarEntrega.SelectedValue == "0")
                    oError += "Debe seleccionar el Punto de Entrega Inicial\\n";
                if (ddlPtoFin.SelectedValue == "0" || ddlPtoFin.SelectedValue == "")
                    oError += "Debe seleccionar el Punto de Entrega Final\\n";
            }

            if (ddlPeriodoEnt.SelectedValue == "0")
                oError += "Debe seleccionar el Periodo de Entrega\\n";
        }
        else
        {
            if (TxtPrecio.Text == "")
                oError += "Debe digitar la precio de la Oferta\\n";

        }
        if (TxtFechaFin.Text == "")
            oError += "Se debe definir la fecha de entrega final de la rueda\\n";
        else
        {
            try
            {
                DateTime ldFecha1 = Convert.ToDateTime(TxtFechaFin.Text);
                DateTime ldFecha2 = Convert.ToDateTime(TxtFechaInicial.Text);
                //if (hdfMedidaTiempo.Value == "U" && hdfDestinoProducto.Value =="G" && hdfTipoMercado.Value  =="P")
                if ( hdfDestinoProducto.Value == "G" && hdfTipoMercado.Value == "P")
                {
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_pozo poz, m_tipo_campo tpo, m_modalidad_contractual mod, m_campo_modalidad cam", " poz.codigo_pozo =" + ddlLugarEntrega.SelectedValue + " and poz.codigo_tipo_campo = tpo.codigo_tipo_campo and tpo.estado ='A' and mod.codigo_modalidad =" + ddlTipoContrato.SelectedValue + " and mod.ind_contingencia ='N' and poz.codigo_tipo_campo = cam.codigo_tipo_campo and mod.codigo_modalidad = cam.codigo_modalidad and cam.tipo_mercado = 'P' and cam.estado='A' and cam.ind_restriccion ='S'"); //20160808 modalidad tipo de campo 
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        if (lLector["mes_inicio"].ToString() != "0")
                            if (Convert.ToInt32(lLector["mes_inicio"].ToString()) != Convert.ToDateTime(TxtFechaInicial.Text).Month || Convert.ToInt32(lLector["dia_inicio"].ToString()) != Convert.ToDateTime(TxtFechaInicial.Text).Day)
                                oError += "La fecha de inicio (" + TxtFechaInicial.Text + ") no corresponde con el mes y dia parametrizado para el tipo de campo " + lLector["mes_inicio"].ToString() + "-" + lLector["dia_inicio"].ToString() + "\\n";  //20160808 modalidad campo
                        if (lLector["mes_fin"].ToString() != "0")
                            if (Convert.ToInt32(lLector["mes_fin"].ToString()) != Convert.ToDateTime(TxtFechaFin.Text).Month || Convert.ToInt32(lLector["dia_fin"].ToString()) != Convert.ToDateTime(TxtFechaFin.Text).Day)
                                oError += "La fecha de finalización (" + TxtFechaFin.Text + ") no corresponde con el mes y dia parametrizado para el tipo de campo " + lLector["mes_fin"].ToString() + "-" + lLector["dia_fin"].ToString() + "\\n"; //20160808 modalidad campo
                        if (lLector["mes_inicio"].ToString() != "0" && lLector["mes_fin"].ToString() != "0")
                        {
                            int ldAnos = ldFecha1.Year - ldFecha2.Year;
                            if (ldAnos <= 0 || ldAnos > 1 && ldAnos < 5)
                                oError += "La duración para este campo sebe ser de 1 o mayor o igual que 5 años \\n";
                        }
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                }
                if (ldFecha2 > ldFecha1)
                    oError += "La fecha final de entrega debe ser mayor o igual que la inicial\\n";
            }
            catch (Exception ex)
            {
                oError += "Fecha final de entrega no válida\\n";
            }

        }
        //valida las horas para intadiario
        if (hdfMedidaTiempo.Value == "I")
        {
            try
            {
                if (Convert.ToInt16(txtHoraIni.Text.Substring(0, 2)) > 23 || txtHoraIni.Text.Substring(2, 1) != ":" || Convert.ToInt16(txtHoraIni.Text.Substring(3, 2)) > 59)
                    oError += "Valor Invalido en la hora inicial\\n";
            }
            catch (Exception ex)
            {
                oError += "Valor Invalido en la hora inicial\\n";
            }
            try
            {
                if (Convert.ToInt16(txtHoraFin.Text.Substring(0, 2)) > 23 || txtHoraFin.Text.Substring(2, 1) != ":" || Convert.ToInt16(txtHoraFin.Text.Substring(3, 2)) > 59)
                    oError += "Valor Invalido en la hora final\\n";
            }
            catch (Exception ex)
            {
                oError += "Valor Invalido en la hora final  \\n";
            }
            try
            {
                if (Convert.ToDateTime(txtHoraIni.Text) >= Convert.ToDateTime(txtHoraFin.Text))
                    oError += "La hora inicial debe ser menor que la final\\n";
            }
            catch (Exception ex)
            {
            }
        }
        /// Verifico el horario antes de dejar registrar la oferta
        liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
        liValIni = ((Convert.ToDateTime(hdfHoraIni.Value).Hour * 60) + Convert.ToDateTime(hdfHoraIni.Value).Minute);
        liValFin = ((Convert.ToDateTime(hdfHoraFin.Value).Hour * 60) + Convert.ToDateTime(hdfHoraFin.Value).Minute);
        //si la rueda no esta activa pero el horario corresponde a rueda activa se visualiza error
        if (liHoraAct < liValIni || liHoraAct > liValFin)
        {
            Toastr.Warning(this, "Esta intentando Ingresar una Oferta Fuera del Horario de Ingreso de Ofertas.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
        }
        lConexion.Abrir();
        //// Verifico si el usuario que entra a la pantalla es el dueño de la oferta
        if (goInfo.cod_comisionista == hdfCodOperador.Value)
        {
            hdfOfMia.Value = "S";
            if (hdfTipoPostura.Value == "C")
                lsTabla = "t_posturas_venta";
            else
                lsTabla = "t_posturas_compra";
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", lsTabla, " numero_id = " + hdfNoid.Value);
            if (lLector.HasRows)
            {
                lLector.Read();
                lsPrecio = lLector["precio"].ToString();
                if (hdfTipoPostura.Value == "C")
                {
                    if (Convert.ToDouble(TxtPrecio.Text) >= Convert.ToDouble(lsPrecio))
                    {
                        Toastr.Warning(this, "La Postura NO puede ser Modificada YA que tiene Agreaciones y con mejor Precio.");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                        oError += "La Postura NO puede ser Modificada YA que tiene Agreciones y con mejor Precio. \\n";
                    }
                }
                else
                {
                    if (Convert.ToDouble(TxtPrecio.Text.Trim()) <= Convert.ToDouble(lsPrecio))
                    {
                        Toastr.Warning(this, "La Postura NO puede ser Modificada YA que tiene Agreaciones y con mejor Precio.");                        
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                        oError += "La Postura NO puede ser Modificada YA que tiene Agreciones y con mejor Precio. \\n";
                    }
                }
            }
            lLector.Close();
            lLector.Dispose();
        }
        if (hndAccion.Value == "C")
        {
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda rue, m_operador ope, m_tipos_operador tpo, m_operador_subasta opeS", " rue.numero_rueda=" + Session["numero_rueda"].ToString() + " and ope.codigo_operador =" + goInfo.cod_comisionista + " and ope.tipo_operador = tpo.sigla and rue.codigo_tipo_rueda = opeS.codigo_tipo_rueda and tpo.codigo_tipo_operador = opeS.codigo_tipo_operador and opeS.punta = '" + ddlTipoPostura.SelectedValue + "'");
            if (!lLector.HasRows)
                oError += "El tipo de operador no está autorizado para registrar el tipo de postura seleccionada. \\n";
            lLector.Close();
            lLector.Dispose();
        }
        //Verificar  si la validacion se debe hacer para MP
        //if (hdfDestinoProducto.Value == "T" && ddlTipoPostura.SelectedValue =="V")
        //{
        //    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_ruta_snt", " codigo_punto_ini="+ ddlLugarEntrega.SelectedValue + " and codigo_punto_fin=" + ddlPtoFin.SelectedValue + " codigo_trasportador="+ goInfo.cod_comisionista + " and estado ='A'");
        //            if (!lLector.HasRows)
        //    {
        //        oError += "El operador logueado no tiene la ruta asignada . \\n";
        //    }
        //}
        if (hdfMedidaTiempo.Value == "I")
        {
            if (Convert.ToDateTime(TxtFechaInicial.Text + ' ' + txtHoraIni.Text) <= Convert.ToDateTime(TxtFechaNegociacion.Text + ' ' + TxtHoraNeg.Text))
                oError += "\\nLa Fecha-hora de Inicio debe ser mayor que la fecha-hora de negociación.";
        }
        oError += hdfErrorFecha.Value;
        //valida restriccion por tipo de subasta 20160107
        if (hdfDestinoProducto.Value == "G")
        {
            try
            {
                string[] lsPeriodoX;
                lsPeriodoX = ddlPeriodoEnt.SelectedValue.Split('-');
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_pozo poz, m_restriccion_subasta res", " poz.codigo_pozo = " + ddlLugarEntrega.SelectedValue + " and res.codigo_tipo_subasta = 5 and res.codigo_tipo_rueda= " + hdfCodTipoRueda.Value + " and res.codigo_periodo=" + lsPeriodoX[0] + " and res.codigo_modalidad =" + ddlTipoContrato.SelectedValue + " and (poz.codigo_tipo_campo = res.codigo_tipo_campo or res.codigo_tipo_campo =0) and res.estado ='A'");
                if (lLector.HasRows)
                    oError += "La Modalidad, Periodo, Tipo de Mercado y Tipo de Campo atado al Punto de Entrega seleccionado está en los parametros de restricción de subasta.\\n";
                lLector.Close();
                lLector.Dispose();
            }
            catch (Exception ex)
            {
            }
        }
        //20160808 modalidad tipo de campo 
        if (Convert.ToDateTime(TxtFechaNegociacion.Text + ' ' + TxtHoraNeg.Text) <= DateTime.Now.AddSeconds(-3))
            oError += "La Fecha-hora de negociación debe ser mayor que la fecha-hora actual.\\n";
        if (oError == "")
        {
            if (hdfOfMia.Value == "S")
            {
                lsPeriodo = ddlPeriodoEnt.SelectedValue.Split('-');
                if (TxtNoAnos.Text.Trim().Length <= 0)
                    TxtNoAnos.Text = "1";
                string[] lsNombreParametros = { "@P_numero_rueda","@P_numero_id","@P_codigo_tipo_rueda","@P_codigo_operador", "@P_tipo_postura", "@P_codigo_producto", "@P_desc_producto","@P_codigo_lugar","@P_desc_lugar",
                                            "@P_codigo_modalidad","@P_desc_modalidad","@P_fecha_negociacion","@P_codigo_periodo","@P_Desc_periodo","@P_tiempo_entrega",
                                            "@P_fecha_ent_inicia","@P_fecha_ent_final","@P_cantidad","@P_precio","@P_hora_ini","@P_hora_fin", "@P_accion", "@P_codigo_punto_fin", "@P_desc_punto_fin", "@P_hora_negociacion" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar,SqlDbType.Int,SqlDbType.VarChar, SqlDbType.Int,SqlDbType.VarChar, 
                                            SqlDbType.Int,SqlDbType.VarChar, SqlDbType.VarChar,SqlDbType.Int,SqlDbType.VarChar,SqlDbType.Int,
                                            SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int, SqlDbType.Float, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar , SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar};
                string[] lValorParametros = { Session["numero_rueda"].ToString(), hdfNoid.Value, hdfCodTipoRueda.Value,goInfo.cod_comisionista,ddlTipoPostura.SelectedValue,hdfCodProducto.Value,lblProducto.Text,ddlLugarEntrega.SelectedValue,ddlLugarEntrega.SelectedItem.ToString(),
                                          ddlTipoContrato.SelectedValue,ddlTipoContrato.SelectedItem.ToString(),TxtFechaNegociacion.Text.Trim(),lsPeriodo[0].Trim(),ddlPeriodoEnt.SelectedItem.ToString(),TxtNoAnos.Text.Trim(),
                                          TxtFechaInicial.Text.Trim(),TxtFechaFin.Text.Trim(), TxtCantidad.Text.Replace(",", ""), TxtPrecio.Text.Replace(",",""), txtHoraIni.Text , txtHoraFin.Text, hndAccion.Value, "0", "", TxtHoraNeg.Text.Trim() }; ///  Accion C=crear, M= Modificar
                SqlDataReader lLector;

                try
                {
                    if (hdfDestinoProducto.Value == "T")
                    {
                        lValorParametros[22] = ddlPtoFin.SelectedValue;
                        lValorParametros[23] = ddlPtoFin.SelectedItem.ToString();
                    }
                    string oMensaje = "";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_setIdNegDirecta", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error == "")
                    {
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                if (lLector["error"].ToString() == "N")
                                {
                                    if (hndAccion.Value == "C")
                                        Toastr.Success(this, "Oferta Creada Correctamente. No. Id " + lLector["mensaje"].ToString() );  
                                    else
                                        Toastr.Success(this, "Oferta Modificada Correctamente!");
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                                    File.SetCreationTime(oArchivo, DateTime.Now);
                                    Session["refrescar"] = "S";
                                    Session["refresca_mis_posturas"] = "S";
                                }
                                else
                                    oMensaje += lLector["mensaje"].ToString() + "\\n";
                            }
                            if (oMensaje != "")
                                Toastr.Warning(this, oMensaje);
                          
                        }
                    }
                    else
                        Toastr.Warning(this, "Problemas en la Creacion de la Postura. " + goInfo.mensaje_error.ToString());
                   
                    lConexion.Cerrar();
                }
                catch (Exception ex)
                {
                    oError = "Error al realizar la oferta. " + ex.Message;
                    lConexion.Cerrar();
                }
            }
            else
            {
                string lsMejor = "N";
                /// Ingreso de las posturas de agreción de los IDs
                string[] lsNombreParametros = { "@P_numero_rueda", "@P_numero_id", "@P_codigo_operador", "@P_cantidad_postura", "@P_precio", "@P_tipo_oferta" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Float, SqlDbType.VarChar };
                string[] lValorParametros = { Session["numero_rueda"].ToString(), hdfNoid.Value, goInfo.cod_comisionista, TxtCantidad.Text.Replace(",", ""), TxtPrecio.Text.Replace(",", ""), hdfTipoPostura.Value };
                SqlDataReader lLector;
                lConexion.Abrir();
                try
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetPosturaId5", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        oError = lLector["error"].ToString();
                        lsMejor = lLector["mejor_precio"].ToString();
                    }
                    if (oError == "")
                    {
                        sMensaje = "Posturas Ingresadas Correctamente";

                        Toastr.Success(this, sMensaje); 
                         ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                        if (lsMejor == "S")
                            File.SetCreationTime(oArchivo, DateTime.Now);
                        Session["refrescar"] = "S";
                        Session["refresca_mis_posturas"] = "S";
                    }
                    lConexion.Cerrar();
                }
                catch (Exception ex)
                {
                    oError = "Error al realizar la oferta. " + ex.Message;
                    lConexion.Cerrar();
                }
            }
        }
        if (oError != "")
        {
            Toastr.Warning(this, oError);
            lConexion.Cerrar();
        }
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            if (lsTabla == "m_periodos_entrega per, m_caracteristica_sub carE ")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString() + "-" + lLector.GetValue(6).ToString() + "-" + lLector.GetValue(8).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlPeriodoEnt_SelectedIndexChanged(object sender, EventArgs e)
    {
        string[] lsPeriodo;
        TxtFechaInicial.Text = "";
        TxtFechaFin.Text = "";

        if (ddlPeriodoEnt.SelectedValue != "0")
        {
            lsPeriodo = ddlPeriodoEnt.SelectedValue.Split('-');
            hdfMesInicialPeriodo.Value = lsPeriodo[2].ToString().Trim();
            hdfMedidaTiempo.Value = lsPeriodo[1].ToString().Trim();
            if (hdfMedidaTiempo.Value == "L")
            {
                TrNoAnos.Visible = true;
                TrNoAnos1.Visible = true;
            }
            else
            {
                TrNoAnos.Visible = false;
                TrNoAnos1.Visible = false;
            }
            if (hdfMedidaTiempo.Value == "U")
            {
                TxtFechaFin.Enabled = true;
            }
            else
                TxtFechaFin.Enabled = false;

        }
        if (hdfMedidaTiempo.Value == "I")
        {
            trHora.Visible = true;
            trHoraF.Visible = true;
        }
        else
        {
            trHora.Visible = false;
            trHoraF.Visible = false;
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TxtFechaInicial_TextChanged(object sender, EventArgs e)
    {
        //// Calculo de la Fecha Final de acuerdo al periodo de entrega ingresado
        DateTime ldFecha;
        string lsDisSemana = "0";
        string lsFecha = "";
        string lsFecha1 = "";
        int liAnos = 0;
        int liMes2Tr = 0;
        int liMes3Tr = 0;
        int liMes4Tr = 0;
        hdfErrorFecha.Value = "";
        try
        {
            if (TxtFechaInicial.Text.Trim().Length > 0)
            {
                ldFecha = Convert.ToDateTime(TxtFechaInicial.Text.Trim());
                if (hdfMedidaTiempo.Value != "I")
                {
                    if (ldFecha > DateTime.Now.Date)
                    {

                        //// Valculo Fecha Final cuando es Intradiario
                        if (hdfMedidaTiempo.Value == "D")
                            TxtFechaFin.Text = TxtFechaInicial.Text.Trim();
                        //if (hdfMedidaTiempo.Value == "D")
                        //    lblFechaFin.Text = Convert.ToDateTime(TxtFechaInicial.Text.Trim()).AddDays(1).ToString();
                        if (hdfMedidaTiempo.Value == "S")
                        {
                            lsDisSemana = Convert.ToDateTime(TxtFechaInicial.Text.Trim()).DayOfWeek.ToString();
                            if (lsDisSemana != "Monday")
                                hdfErrorFecha.Value = "La Fecha de Inicio debe ser el primer dia de la Semana. YA que escogio Periodo Semanal.";
                            else
                            {
                                lsFecha = Convert.ToDateTime(TxtFechaInicial.Text.Trim()).AddDays(6).ToString();
                            }
                        }
                        if (hdfMedidaTiempo.Value == "M")
                        {
                            if (Convert.ToInt32(TxtFechaInicial.Text.Trim().Substring(8, 2)) != 1)
                                hdfErrorFecha.Value = "La Fecha de Inicio debe ser el primer dia del Mes. YA que escogio Periodo Mensual.";
                            else
                            {
                                lsFecha = Convert.ToDateTime(TxtFechaInicial.Text.Trim()).AddMonths(1).AddDays(-1).ToString();
                            }
                        }
                        if (hdfMedidaTiempo.Value == "T")
                        {
                            lsFecha1 = DateTime.Now.Year.ToString() + "/" + hdfMesInicialPeriodo.Value + "/01";
                            liMes2Tr = Convert.ToDateTime(lsFecha1).AddMonths(3).Month;
                            liMes3Tr = Convert.ToDateTime(lsFecha1).AddMonths(6).Month;
                            liMes4Tr = Convert.ToDateTime(lsFecha1).AddMonths(9).Month;
                            if (Convert.ToInt32(hdfMesInicialPeriodo.Value) != Convert.ToInt32(TxtFechaInicial.Text.Trim().Substring(5, 2)) && Convert.ToInt32(TxtFechaInicial.Text.Trim().Substring(5, 2)) != liMes2Tr && Convert.ToInt32(TxtFechaInicial.Text.Trim().Substring(5, 2)) != liMes3Tr && Convert.ToInt32(TxtFechaInicial.Text.Trim().Substring(5, 2)) != liMes4Tr)
                                hdfErrorFecha.Value = "El Mes de la Fecha de Inicio es Diferente al mes parametrizado de inicio {" + hdfMesInicialPeriodo.Value + "-" + liMes2Tr.ToString() + "-" + liMes3Tr.ToString() + "-" + liMes4Tr.ToString() + "}. YA que escogio Periodo Trimestral.";
                            else
                            {
                                if (Convert.ToInt32(TxtFechaInicial.Text.Trim().Substring(8, 2)) != 1)
                                    hdfErrorFecha.Value = "La Fecha de Inicio debe ser el primer dia del Mes. YA que escogio Periodo Trimestral.";
                                else
                                {
                                    lsFecha = Convert.ToDateTime(TxtFechaInicial.Text.Trim()).AddMonths(3).AddDays(-1).ToString();
                                }

                            }
                        }
                        if (hdfMedidaTiempo.Value == "A")
                        {
                            if (Convert.ToInt32(hdfMesInicialPeriodo.Value) != Convert.ToInt32(TxtFechaInicial.Text.Trim().Substring(5, 2)))
                                hdfErrorFecha.Value = "El Mes de la Fecha de Inicio es Diferente al mes parametrizado de inicio {" + hdfMesInicialPeriodo.Value + "}. YA que escogio Periodo Anual.";
                            else
                            {
                                if (Convert.ToInt32(TxtFechaInicial.Text.Trim().Substring(8, 2)) != 1)
                                    hdfErrorFecha.Value = "La Fecha de Inicio debe ser el primer dia del Mes. YA que escogio Periodo Anual." + "');";
                                else
                                {
                                    lsFecha = Convert.ToDateTime(TxtFechaInicial.Text.Trim()).AddMonths(12).AddDays(-1).ToString();
                                }

                            }
                        }
                        if (hdfMedidaTiempo.Value == "L")
                        {
                            if (TxtNoAnos.Text.Trim().Length <= 0)
                                hdfErrorFecha.Value = "Debe Ingresar el No. de Años. YA que escogio Periodo MultiAnual.";
                            else
                            {
                                try
                                {
                                    liAnos = Convert.ToInt32(TxtNoAnos.Text.Trim());
                                    if (Convert.ToInt32(hdfMesInicialPeriodo.Value) != Convert.ToInt32(TxtFechaInicial.Text.Trim().Substring(5, 2)))
                                        hdfErrorFecha.Value = "El Mes de la Fecha de Inicio es Diferente al mes parametrizado de inicio {" + hdfMesInicialPeriodo.Value + "}. YA que escogio Periodo MultiAnual.";
                                    else
                                    {
                                        if (Convert.ToInt32(TxtFechaInicial.Text.Trim().Substring(8, 2)) != 1)
                                            hdfErrorFecha.Value = "La Fecha de Inicio debe ser el primer dia del Mes. YA que escogio Periodo MultiAnual." + "');";
                                        else
                                        {
                                            lsFecha = Convert.ToDateTime(TxtFechaInicial.Text.Trim()).AddYears(Convert.ToInt32(TxtNoAnos.Text.Trim())).AddDays(-1).ToString();
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    hdfErrorFecha.Value = "Valor  Inválido en el Campo de No. de Años del periodo MultiAnual.";
                                }
                            }
                        }
                        try
                        {
                            if (ldFecha <= Convert.ToDateTime(TxtFechaNegociacion.Text))
                                hdfErrorFecha.Value += "\\nLa Fecha de Inicio debe ser mayor que la fecha de negociación.";
                        }
                        catch
                        {
                            hdfErrorFecha.Value += "\\nError en la fecha de negociación.";
                        }
                    }
                    else
                        hdfErrorFecha.Value = "La Fecha Inicial debe Ser mayor a la Fecha del Día.";
                }
                else
                {
                    TxtFechaFin.Text = TxtFechaInicial.Text.Trim();
                    if (ldFecha < Convert.ToDateTime(TxtFechaNegociacion.Text))
                        hdfErrorFecha.Value += "\\nLa Fecha de Inicio debe ser mayor o igual que la fecha de negociación.";
                    if (txtHoraIni.Text != "")
                        if (Convert.ToDateTime(TxtFechaInicial.Text + ' ' + txtHoraIni.Text) <= Convert.ToDateTime(TxtFechaNegociacion.Text + ' ' + TxtHoraNeg.Text))
                            hdfErrorFecha.Value += "\\nLa Fecha-hora de Inicio debe ser mayor que la fecha-hora de negociación.";
                }

                if (hdfErrorFecha.Value != "")
                    Toastr.Warning(this, hdfErrorFecha.Value);               
                else
                    if (lsFecha != "")
                        TxtFechaFin.Text = lsFecha.Substring(6, 4) + "/" + lsFecha.Substring(3, 2) + "/" + lsFecha.Substring(0, 2);
            }
        }
        catch (Exception ex)
        {
            Toastr.Warning(this,  "Valor Invalido en Fecha Inicial");
           
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlLugarEntrega_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (hdfDestinoProducto.Value == "T")
        {
            ddlPtoFin.Items.Clear();
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlPtoFin, "m_pozo poz, m_ruta_snt rut", " poz.estado = 'A' and rut.estado ='A' and rut.codigo_pozo_ini= " + ddlLugarEntrega.SelectedValue + " and poz.codigo_pozo = rut.codigo_pozo_fin ", 0, 1);
            lConexion.Cerrar();
        }
        else
        {
            lConexion.Abrir();
            ddlTipoContrato.Items.Clear();
            LlenarControles(lConexion.gObjConexion, ddlTipoContrato, "m_modalidad_contractual mod, m_pozo poz, m_campo_modalidad cam ", " mod.estado = 'A' And poz.codigo_pozo = " + ddlLugarEntrega.SelectedValue + " and poz.codigo_tipo_campo = cam.codigo_tipo_campo and cam.codigo_modalidad = mod.codigo_modalidad order by poz.descripcion", 0, 1);
            if (ddlTipoContrato.Items.Count <= 1) //Se pregunta por si no hay mmodalidades parametizadas, entonces toma las de la paramatrizacion original
            {
                ddlTipoContrato.Items.Clear();
                LlenarControles(lConexion.gObjConexion, ddlTipoContrato, "m_modalidad_contractual mod, m_caracteristica_sub carM ", " mod.estado = 'A' And carM.codigo_tipo_subasta = 5 And carM.destino_rueda = '" + hdfDestinoProducto.Value + "' and carM.tipo_mercado = '" + hdfTipoMercado.Value + "' and carM.tipo_caracteristica = 'M' and carM.estado ='A' and mod.codigo_modalidad= carM.codigo_caracteristica order by descripcion", 0, 1);
            }
            lConexion.Cerrar();
        }

    }
}
