﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Procesos
{
    public partial class frm_reservaRemanentePtdvf : Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Reserva de PTDVF/CIDVF Remanente";
        clConexion lConexion = null;
        SqlDataReader lLector;
        DataSet lds = new DataSet();
        SqlDataAdapter lsqldata = new SqlDataAdapter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo;

            buttons.FiltrarOnclick += BtnConsultar_Click;
            buttons.ReservarOnclick += BtnReservar_Click;

            if (IsPostBack) return;
            //Titulo
            Master.Titulo = "Subasta";
            buttons.Inicializar(botones: new[] { EnumBotones.Buscar, EnumBotones.Reservar });
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlFuente, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            lConexion.Cerrar();
            var lsError = "N";
            if (!VerificarExistencia("m_parametros_sslp", "  convert(varchar(10),getdate(),111) between fecha_ini_reserva and fecha_fin_reserva"))
            {
                buttons.Inicializar(botones: new[] { EnumBotones.Ninguno });
                lsError = "S";
                Toastr.Error(this, "No está dentro del plazo para realizar las reservas de PTDVF/CIDVF<br>");
            }
            if (!VerificarExistencia("m_operador ope, m_tipos_operador tpo, m_operador_subasta sub", "ope.codigo_operador = " + Session["cod_comisionista"] + " and ope.tipo_operador = tpo.sigla and sub.codigo_tipo_subasta = 8 and tpo.codigo_tipo_operador= sub.codigo_tipo_operador and sub.punta ='C' and sub.estado ='A' "))
            {
                buttons.Inicializar(botones: new[] { EnumBotones.Ninguno });
                lsError = "S";
                Toastr.Error(this, "El operador no está autorizado como comprador para las reservas de PTDVF/CIDVF");
            }
            if (lsError == "N")
                consultar();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnConsultar_Click(object sender, EventArgs e)
        {
            consultar();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnReservar_Click(object sender, EventArgs e)
        {
            var lblMensaje = new StringBuilder();
            lConexion = new clConexion(goInfo);
            int liConta = 0;
            CheckBox oCheck;
            TextBox oText;
            int liCntRes = 0;
            foreach (DataGridItem Grilla in dtgConsulta.Items)
            {
                oCheck = (CheckBox)Grilla.Cells[9].Controls[1];
                if (oCheck.Checked)
                {
                    liConta++;
                    oText = (TextBox)Grilla.Cells[8].Controls[1];
                    if (oText.Text == "")
                    {
                        lblMensaje.Append("Debe digitar la cantidad a reservar para la fuente " + Grilla.Cells[0].Text + "<br>");
                    }
                    else
                    {
                        if (Convert.ToInt32(Grilla.Cells[12].Text) < Convert.ToInt32(oText.Text))
                        {
                            lblMensaje.Append("El valor a reservar debe ser menor o igual que la PTDVF / CIDVF remanente para la fuente " + Grilla.Cells[0].Text);
                        }

                        else
                            liCntRes += Convert.ToInt32(oText.Text);
                    }
                }
                else
                    liCntRes += Convert.ToInt32(Grilla.Cells[10].Text);
            }
            if (liConta == 0)
            {
                lblMensaje.Append("Debe seleccionar al menos un registro para hacer la reserva<br>");
            }

            if (lblMensaje.ToString() == "")
            {
                lConexion.Abrir();
                SqlDataReader lLector;
                string[] lsNombreParametros = { "@P_codigo_operador", "@P_cantidad_reserva" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { goInfo.cod_comisionista, liCntRes.ToString() };
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_ValidaReservaPtdvf", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lblMensaje.Append(lLector["error"]);
                }
                lLector.Close();
                lConexion.Cerrar();
            }
            if (lblMensaje.ToString() == "")
            {
                SqlDataReader lLector;
                var lComando = new SqlCommand();

                lConexion.Abrir();
                foreach (DataGridItem Grilla in dtgConsulta.Items)
                {
                    oCheck = (CheckBox)Grilla.Cells[9].Controls[1];
                    if (oCheck.Checked)
                    {

                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = "pa_SetReservaPtdvf";
                        lComando.Parameters.Add("@P_codigo_fuente", SqlDbType.Int).Value = Grilla.Cells[0].Text;
                        lComando.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = Session["cod_comisionista"].ToString();
                        oText = (TextBox)Grilla.Cells[8].Controls[1];
                        lComando.Parameters.Add("@P_cantidad_reserva", SqlDbType.Int).Value = oText.Text;
                        lComando.CommandTimeout = 3600;
                        lLector = lComando.ExecuteReader();
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            lblMensaje.Append(lLector["Mensaje"]);
                        }
                        else
                            lLector.Close();
                        lLector.Dispose();
                        lComando.Parameters.Clear();
                    }
                }
                lConexion.Cerrar();
                if (lblMensaje.ToString() == "")
                {
                    consultar();
                    Toastr.Success(this, "Se realizo la reserva correctamente!.");
                }
            }
            if (lblMensaje.ToString() != "")
            {
                Toastr.Error(this, lblMensaje.ToString());
            }
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void consultar()
        {
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetReservaPtdvf";
                lComando.Parameters.Add("@P_codigo_fuente", SqlDbType.Int).Value = ddlFuente.SelectedValue;
                lComando.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = Session["cod_comisionista"].ToString();
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgConsulta.DataSource = lds;
                dtgConsulta.DataBind();
                dtgConsulta.Visible = true;
                lConexion.Cerrar();
                TextBox oText;
                foreach (DataGridItem Grilla in dtgConsulta.Items)
                {
                    //if (Grilla.Cells[7].Text != "0")
                    //{
                    //    oText = (TextBox)Grilla.Cells[8].Controls[1];
                    //    oText.Text = Grilla.Cells[7].Text;
                    //}
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se Pudo Consultar la información.! " + ex.Message);
            }
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lsTable, string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia(lsTable, lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

    }
}