﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Procesos
{
    public partial class frm_ModifDeclaraCantDemandaUvlp : Page
    {
        private InfoSessionVO goInfo = null;
        private static string lsTitulo = "Modificación de la Declaración Información Total Energía Demandada UVLP";
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        private clConexion lConexion = null;
        private clConexion lConexion1 = null;
        private SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        private string lsIndica
        {
            get
            {
                if (ViewState["lsIndica"] == null)
                    return "";
                else
                    return (string)ViewState["lsIndica"];
            }
            set
            {
                ViewState["lsIndica"] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.AprobarOnclick += btnAprobar_Click;
            buttons.DesaprobarOnclick += btnDesAprobar_Click;
            buttons.ExportarExcelOnclick += btnExcel_Click;

            //Establese los permisos del sistema
            lConexion = new clConexion(goInfo);

            if (!IsPostBack)
            {
                try
                {
                    lblTitulo.Text = lsTitulo;
                    lConexion.Abrir();
                    LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4);
                    LlenarControles(lConexion.gObjConexion, ddlPuntoSalida, "m_punto_salida_snt", " estado = 'A' order by descripcion", 0, 2);
                    lConexion.Cerrar();

                    //Botones
                    EnumBotones[] botones;

                    // Carga informacion de combos
                    if (Session["tipoPerfil"].ToString() == "N")
                    {
                        ddlOperador.SelectedValue = goInfo.cod_comisionista;
                        ddlOperador.Enabled = false;
                        EnumBotones[] arrayBotones = { EnumBotones.Buscar, EnumBotones.Excel, EnumBotones.Aprobar, EnumBotones.Desaprobar };
                        botones = arrayBotones;
                    }
                    else
                    {
                        EnumBotones[] arrayBotones = { EnumBotones.Buscar, EnumBotones.Excel };
                        botones = arrayBotones;
                    }

                    buttons.Inicializar(botones: botones);
                    tblgrilla.Visible = true;
                    CargarDatos();
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = "Problemas en la Carga de la Página. " + ex.Message.ToString();
                }
                if (lblMensaje.Text != "")
                {
                    Toastr.Warning(this, lblMensaje.Text);
                    lblMensaje.Text = "";
                }
            }
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                }
                lDdl.Items.Add(lItem1);

            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_codigo_operador", "@P_codigo_punto_salida" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { ddlOperador.SelectedValue, ddlPuntoSalida.SelectedValue };
            string lsIdReg = "";
            //string lsUltimoPunto = "";
            lblMensaje.Text = "";

            try
            {
                lConexion.Abrir();
                dtgSuministro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetModifDecTotDemanUvlp", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgSuministro.DataBind();
                if (dtgSuministro.Items.Count <= 0)
                    Toastr.Info(this, "No hay Registros para Visualizar.!");
                lblMensaje.Text = string.Empty;
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
            if (!string.IsNullOrEmpty(lblMensaje.Text))
            {
                Toastr.Warning(this, lblMensaje.Text);
                lblMensaje.Text = "";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            CargarDatos();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAprobar_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_operador", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char };
            string[] lValorParametros = { ddlOperador.SelectedValue, "1" };
            lblMensaje.Text = string.Empty;
            if (string.IsNullOrEmpty(lblMensaje.Text))
            {
                lValorParametros[0] = goInfo.cod_comisionista;
                lConexion.Abrir();
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModifTotalEnergiaDemUvlp", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensaje.Text = "Se presentó un problema en la aprobación de la cantidad total de energía.! " + goInfo.mensaje_error;
                    lConexion.Cerrar();
                }
                else
                {
                    var lsError = new StringBuilder();
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                            lsError.Append(lLector["Error"]);
                    }
                    lLector.Close();
                    if (string.IsNullOrEmpty(lsError.ToString()))
                    {
                        Toastr.Success(this, "Se aprobó correctamente cantidad total de energía.!");
                        CargarDatos();
                    }
                    else
                        lblMensaje.Text = lsError.ToString();
                }
            }

            if (lblMensaje.Text == "")
            {
                return;
            }

            Toastr.Warning(this, lblMensaje.Text);
            lblMensaje.Text = "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDesAprobar_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_operador", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char };
            string[] lValorParametros = { ddlOperador.SelectedValue, "2" };
            var mensaje = new StringBuilder();


            lValorParametros[0] = goInfo.cod_comisionista;
            lConexion.Abrir();
            goInfo.mensaje_error = "";
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModifTotalEnergiaDemUvlp", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
            if (goInfo.mensaje_error != "")
            {
                mensaje.Append("Se presentó un problema en la desaprobación de a cantidad total de energía.! " + goInfo.mensaje_error);
                lConexion.Cerrar();
            }
            else
            {
                string lsError = "";
                lLector.Read();
                lsError = lLector["Error"].ToString();
                if (lsError == "")
                {
                    Toastr.Success(this, "Se desaprobó Correctamente la cantidad total de energía.!");
                    CargarDatos();
                }
                else
                    mensaje.Append(lsError);
            }

            if (!string.IsNullOrEmpty(mensaje.ToString()))
            {
                Toastr.Error(this, mensaje.ToString());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExcel_Click(object sender, EventArgs e)
        {
            lblMensaje.Text = "";
            try
            {
                string lsNombreArchivo = Session["login"] + "InfExcelDecInf" + DateTime.Now + ".xls";
                StringBuilder lsb = new StringBuilder();
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                lpagina.EnableEventValidation = false;
                lpagina.Controls.Add(lform);
                dtgSuministro.EnableViewState = false;

                lform.Controls.Add(dtgSuministro);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = System.Text.Encoding.Default;

                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + "Contratos Disponibles para Declaracion de Informacion Suministro" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write(lsb.ToString());
                Response.End();
                Response.Flush();
            }
            catch (Exception ex)
            {

            }
        }
    }
}