﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using PCD_Infraestructura.Business;
using PCD_Infraestructura.Transaction;
using PCD_Infraestructura.DomainLayer;
using System.Diagnostics;

public partial class Procesos_frm_mis_ofertas_6 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        ltTableroRF.Text = "";
        ltTableroRF.Text += "<table border='1' style='border-color:#FF9900'><tr>";
        ltTableroRF.Text += "<td  width='100%'align='center' bgcolor='#E46D0A' style='border-color:#E46D0A' colspan='5'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>MIS OFERTAS</font></td>";
        ltTableroRF.Text += "</tr>";
        ltTableroRF.Text += "<tr>";
        ltTableroRF.Text += "<td width='10%' style='border-color:#E46D0A' align='center' bgcolor='#E46D0A'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>IDG</font></td>";
        ltTableroRF.Text += "<td width='10%' style='border-color:#E46D0A' align='center' bgcolor='#E46D0A'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>CANTIDAD</font></td>";
        ltTableroRF.Text += "<td width='30%' style='border-color:#E46D0A' align='center' bgcolor='#E46D0A'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>PRECIO</font></td>";
        ltTableroRF.Text += "<td width='20%'  style='border-color:#E46D0A' align='center' bgcolor='#E46D0A''><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>DELTA PRECIO</font></td>";
        ltTableroRF.Text += "<td width='10%'  style='border-color:#E46D0A' align='center' bgcolor='#E46D0A'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>TIPO OFERTA</font></td>";
        ltTableroRF.Text += "</tr>";

        try
        {

            string[] lsNombreParametros = { "@P_numero_rueda" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int };
            string[] lValorParametros = { Session["numero_rueda"].ToString() };
            SqlDataReader lLector;
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetMisOFertas6", lsNombreParametros, lTipoparametros, lValorParametros);
            if (lLector.HasRows)
            {
                while (lLector.Read())
                {
                    ltTableroRF.Text += "<tr>";
                    ltTableroRF.Text += "<td width='10%' style='border-color:#F79632' align='left' bgcolor='#F79632'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["numero_id"].ToString() + "</font></td>";
                    ltTableroRF.Text += "<td width='10%' style='border-color:#F79632' align='right' bgcolor='#F79632'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["cantidad_postura"].ToString() + "</font></td>";
                    ltTableroRF.Text += "<td width='35%' style='border-color:#F79632' align='right' bgcolor='#F79632'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["precio"].ToString() + "</font></td>";
                    ltTableroRF.Text += "<td width='25%' style='border-color:#F79632' align='right' bgcolor='#F79632'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["precio_delta"].ToString() + "</font></td>";
                    ltTableroRF.Text += "<td width='10%' style='border-color:#F79632' align='left' bgcolor='#F79632'><font color='#000000' face='Arial, Helvetica, sans-serif' size='3px'>" + lLector["tipo_oferta"].ToString() + "</font></td>";
                    ltTableroRF.Text += "</tr>";
                }
                ltTableroRF.Text += "</table>";
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
        }
    }
}