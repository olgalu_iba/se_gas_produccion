﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ActDeclaracionInf.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_ActDeclaracionInf" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript">
        //        document.location = no;
        function confirmar() {
            return confirm("Esta seguro que desea registrar la información!")
        }
        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }

        function formatoMiles(obj, evt) {
            var T;
            var strValue = removeCommas(obj.value);
            //if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
            if ((T = /([0-9]*)$/i.exec(strValue)) != null) {
                //                T[4] = T[4].substr(0, decimales);
                //                if (T[2] == '' && T[3] == '.') T[2] = 0;
                obj.value = addCommas(T[1]);
            } else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }

    </script>


<div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTituo" runat="server" Text="ACTUALZIACIÓN DE DECLARACION DE INFORMACIÓN" />
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">

                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Código Titular:</label>
                            <asp:DropDownList ID="ddlTitular" runat="server">
                            </asp:DropDownList>
                            <asp:Label ID="lblPunto" runat="server"></asp:Label>
                            <asp:Label ID="lblRueda" runat="server"></asp:Label>
                            <asp:Label ID="lblDestino" runat="server"></asp:Label>
                            <asp:DropDownList ID="ddlPunto" runat="server">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlPuntoFin" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Cantidad declarada</label>
                            <asp:TextBox ID="TxtCntDec" runat="server" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FTBETxtCntDisp" runat="server" TargetControlID="TxtCntDec"
                                FilterType="Custom, Numbers" ValidChars=","></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="btnOfertar" Text="Actualizar" runat="server" OnClientClick="return confirmar();"
                                OnClick="btnOfertar_Click" />
                            <asp:HiddenField ID="hndDestino" runat="server" />
                            <asp:HiddenField ID="hndCodDec" runat="server" />
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>

</asp:Content>
