﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_pruebaProcesos.aspx.cs"
    Inherits="Procesos_frm_pruebaProcesos" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="80%" border="0" cellpadding="0" cellspacing="0">
        <table id="tblParametros" align="center" runat="server" width="80%">
            <tr>
                <td class="th1" colspan="2">
                    <asp:Label ID="lblTitulo" runat="server" ForeColor="White" Text="PRUEBA DE PROCESOS AUTOMATICOS DEL SISTEMA"></asp:Label>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td class="td1">Prceso
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlProceso" runat="server">
                        <%-- <asp:ListItem Value="1">Creación de Ids UVLP</asp:ListItem>--%>
                        <%--<asp:ListItem Value="2">Publicación UVCP BEC</asp:ListItem>--%>
                        <%--<asp:ListItem Value="3">Publicación Oferta Comprometida</asp:ListItem>--%>
                        <asp:ListItem Value="4">Paso de información básica al BEC</asp:ListItem>
                        <asp:ListItem Value="5">Paso de Información Operativa al BEC</asp:ListItem>
                        <asp:ListItem Value="6">Paso de Información Mercado Primario al BEC</asp:ListItem>
                        <asp:ListItem Value="7">Paso de Información Mercado Secundario al BEC (debe digitar hora)</asp:ListItem>
                        <asp:ListItem Value="8">Paso de Información mercado mayorista</asp:ListItem>
                        <%--<asp:ListItem Value="9">Actualiza Sarlaft</asp:ListItem>--%>
                        <%--<asp:ListItem Value="10">Elimina cartas Superintendecia</asp:ListItem>--%>
                        <%--<asp:ListItem Value="11">Ruedas automaticas</asp:ListItem>
                        <asp:ListItem Value="12">Finalización contingencia</asp:ListItem>--%>
                        <asp:ListItem Value="13">Información general reportes BEC</asp:ListItem>
                        <asp:ListItem Value="14">Publicación subasta bimestral</asp:ListItem>
                        <asp:ListItem Value="15">Rechazo automática de solicitudes Modificación</asp:ListItem>
                        <asp:ListItem Value="16">Desbloqueo contraseña</asp:ListItem>
                        <%--<asp:ListItem Value="17">rechazo automático solicitud cambio precio</asp:ListItem>--%>
                        <asp:ListItem Value="18">Notificación solicitud modificación próxima a vencer</asp:ListItem>
                        <asp:ListItem Value="19">Finaliza Rueda</asp:ListItem>
                        <asp:ListItem Value="20">Borra declaracion  UVLP</asp:ListItem>
                        <asp:ListItem Value="21">Paso BEC Información operativa gasoducto conexión</asp:ListItem>
                        <asp:ListItem Value="22">Cambio Fecha maxima registro OTMM</asp:ListItem>
                        <asp:ListItem Value="23">Publica datos transporte</asp:ListItem><%--20201124--%>
                        <asp:ListItem Value="24">Cambio fecha transporte </asp:ListItem><%--20201124--%>
                        <asp:ListItem Value="25">Actualiza tablero control inf operativa</asp:ListItem><%--20220221--%>
                        <asp:ListItem Value="26">Actualiza tablero control inf transaccional</asp:ListItem><%--20220221--%>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">Fecha
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtFecha" placeholder="yyyy/mm/dd" Width="150px" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RfvTxtFecha" runat="server" ErrorMessage="Debe Ingresar la fecha "
                        ControlToValidate="TxtFecha" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="td1">Hora
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtHora" runat="server" Width="100px" MaxLength="5"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RevTxtHora" ControlToValidate="TxtHora"
                        ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                        ErrorMessage="Formato Incorrecto para la Hora "> * </asp:RegularExpressionValidator>
                </td>
            </tr>

            <tr>
                <td class="td1">Rueda
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtRueda" runat="server" Width="100px" MaxLength="10"></asp:TextBox>
                </td>
            </tr>
            <%--<tr>
                <td class="td1">
                    Hora 
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtHora" runat="server" Width="100px" MaxLength="5"></asp:TextBox>
                </td>
            </tr>--%>
            <tr>
                <td class="th1" colspan="2">
                    <asp:Button ID="btnProcesar" Text="Procesar" runat="server" OnClick="btnProcesar_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblMensaje1" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </table>
</asp:Content>
