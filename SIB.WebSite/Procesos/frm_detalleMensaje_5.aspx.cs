﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_detalleMensaje_5 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    String strRutaArchivo;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString() + "mensaje.txt";
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
        SqlDataReader lLector;
        if (this.Request.QueryString["codOperador"] != null && this.Request.QueryString["codOperador"].ToString() != "")
        {
            hdfCodOperador.Value = this.Request.QueryString["codOperador"].ToString();
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + hdfCodOperador.Value + " ");
            if (lLector.HasRows)
            {
                lLector.Read();
                lblOperador.Text = "Operador: " + hdfCodOperador.Value + " - " + lLector["razon_social"].ToString();
            }
            lblOperador.Visible = true;
            ddlOperador.Visible = false;
            lLector.Close();
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador", "@P_codigo_remitente" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { Session["numero_rueda"].ToString(), hdfCodOperador.Value, goInfo.cod_comisionista };
            lConexion.Abrir();
            dtgSubasta5.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetDetalleMen5", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgSubasta5.DataBind();
            lConexion.Cerrar();
            foreach (DataGridItem Grilla in this.dtgSubasta5.Items)
            {
                if (Grilla.Cells[2].Text == hdfCodOperador.Value)
                    Grilla.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                else
                    Grilla.ForeColor = System.Drawing.ColorTranslator.FromHtml("#2707F9");
            }
        }
        else
        {
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' And codigo_operador <> " + goInfo.cod_comisionista + " and codigo_operador !=0 order by codigo_operador ", 0, 4);
            lConexion.Cerrar();
            lblOperador.Visible = false;
            ddlOperador.Visible = true;
        }


    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnMensaje_Click(object sender, EventArgs e)
    {
        string oError = "";
        string sMensaje = "";

        if (TxtMensaje.Text == "")
            oError += "Debe digitar el mensaje a enviar\\n";
        if (lblOperador.Visible)
        {
            if (hdfCodOperador.Value == "0")
                oError += "No hay operador para enviar el mensaje\\n";
        }
        else
            if (ddlOperador.SelectedValue == "0")
                oError += "Debe seleccionar el operador para enviar el mensaje\\n";
            else
                hdfCodOperador.Value = ddlOperador.SelectedValue;

        if (oError == "")
        {
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_mensaje", "@P_codigo_operador", "@P_codigo_remitente" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { Session["numero_rueda"].ToString(), TxtMensaje.Text, hdfCodOperador.Value, goInfo.cod_comisionista };

            lConexion.Abrir();
            try
            {
                DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_setMensaje", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                sMensaje = "Mensaje enviado Correctamente";
                File.SetCreationTime(strRutaArchivo, DateTime.Now);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + sMensaje + "');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);

                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                oError = "Error al enviar el mensaje. " + ex.Message;
                lConexion.Cerrar();
            }
        }
        if (oError != "")
        {
            Toastr.Warning(this, oError);
        }
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            if (lsTabla != "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }

}