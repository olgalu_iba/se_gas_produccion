﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ConsultaProyDemandaReg.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_ConsultaProyDemandaReg" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />

                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Año Inicial</label>
                            <asp:TextBox ID="TxtAnoIni" runat="server" MaxLength="4" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="fteBTxtAnoIni" runat="server" TargetControlID="TxtAnoIni"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Mes Inicial</label>
                            <asp:DropDownList ID="ddlMesIni" runat="server" CssClass="form-control" >
                            </asp:DropDownList>

                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Año Final</label>
                            <asp:TextBox ID="TxtAnoFin" runat="server" MaxLength="4"  CssClass="form-control" ></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtAnoFin" runat="server" TargetControlID="TxtAnoFin"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Mes Final</label>
                            <asp:DropDownList ID="ddlMesFin" runat="server"  CssClass="form-control">
                            </asp:DropDownList>

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Operador</label>
                            <asp:DropDownList ID="DdlOperador" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group" style="display:none">
                     
                            <asp:Button ID="btnConsultar" runat="server" Text="Consultar" CssClass="boton1" OnClick="btnConsultar_Click" />
                            <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                                Visible="false" Height="35" />
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>

                        </div>
                    </div>
                </div>
                <div border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                    id="tblGrilla" visible="false">

                    <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                        <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                            PagerStyle-HorizontalAlign="Center" CssClass="table-bordered">

                            <Columns>
                                <asp:BoundColumn DataField="fecha_registro" HeaderText="Fecha Carga" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_sector_consumo" HeaderText="Código Sector Consumo"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="sector_consumo" HeaderText="Sector de Consumo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20170705 rq025-17--%>
                                <%--<asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Código Punto de Salida del SNT"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="punto_salida" HeaderText="Punto de salida del SNT" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>--%>
                                <asp:BoundColumn DataField="codigo_mercado_relevante" HeaderText="Código Mercado relevante"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="mercado_relevante" HeaderText="Mercado relevante" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad / Capacidad" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,##0}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>

                </div>


            </div>
        </div>
    </div>
</asp:Content>
