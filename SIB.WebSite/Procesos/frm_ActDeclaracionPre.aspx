﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ActDeclaracionPre.aspx.cs"
    Inherits="Procesos_frm_ActDeclaracionPre" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link href="../css/estiloTab.css" rel="stylesheet" type="text/css" />
</head>
<body style="background-color: #000000" onload="window.focus()">

    <script language="javascript">
        //        document.location = no;
        function confirmar() {
            return confirm("Esta seguro que desea registrar la información!")
        }
        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }

        function formatoMiles(obj, evt) {
            var T;
            var strValue = removeCommas(obj.value);
            //if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
            if ((T = /([0-9]*)$/i.exec(strValue)) != null) {
                //                T[4] = T[4].substr(0, decimales);
                //                if (T[2] == '' && T[3] == '.') T[2] = 0;
                obj.value = addCommas(T[1]);
            } else {
                alert('formato invalido ');
            }
        }

        function formatoMilesPre(obj, evt, decimales) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
                T[4] = T[4].substr(0, decimales);
                if (T[2] == '' && T[3] == '.') {
                    T[2] = 0;
                }
                obj.value = T[1] + addCommas(T[2]) + T[3] + T[4];
            }
            else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }

    </script>

    <form id="frmListado" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="td6" colspan="2" align="center">
                            <asp:Label ID="lblTituo" runat="server" Text="DECLARACION DE PRECIO" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="td6">
                            <asp:Label ID="lblRueda" runat="server"></asp:Label>
                        </td>
                        <td class="td6">
                            <asp:Label ID="lblDestino" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="td6">
                            <asp:Label ID="lblPunto" runat="server"></asp:Label>
                        </td>
                        <td class="td6">
                            <asp:Label ID="lblCantidad" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="td6">Cantidad no Disponible
                        </td>
                        <td class="td6">
                            <asp:TextBox ID="TxtCntNoDisp" runat="server" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTBETxtCntDisp" runat="server" TargetControlID="TxtCntNoDisp"
                                FilterType="Custom, Numbers" ValidChars=","></cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr id="trPrecio" runat="server">
                        <td class="td6">Precio Reserva
                        </td>
                        <td class="td6">
                            <asp:TextBox ID="TxtPrecioRes" runat="server" onkeyup="return formatoMilesPre(this,event);"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTEBTxtPrecioRes" runat="server" TargetControlID="TxtPrecioRes"
                                FilterType="Custom, Numbers" ValidChars=",."></cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" class="td6">
                            <br />
                            <asp:Button ID="btnOfertar" Text="Actualizar" runat="server" OnClientClick="return confirmar();"
                                OnClick="btnOfertar_Click" />
                            <asp:HiddenField ID="hndDestino" runat="server" />
                            <asp:HiddenField ID="hndCodDec" runat="server" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
