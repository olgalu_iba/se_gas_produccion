﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_subasta_prm_3.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_subasta_prm_3" EnableEventValidation="false" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    <table border="0" align="center" cellpadding="0" cellspacing="0" runat="server" id="Table1"
        width="100%">
        <tr>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Rueda: </font>
            </td>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Fase: </font>
            </td>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Hora Fase:
                </font>
            </td>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Participante:</font>
            </td>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Label ID="lblPunto" runat="server"></asp:Label></font>
            </td>
            <td align="center" class="td3">
                <asp:Button ID="btnBuscar" Text="Buscar" runat="server" OnClick="btnBuscar_Click" />
            </td>
            <td align="center" class="td4">
                <asp:Button ID="btnSuspender" Text="Suspender" runat="server" OnClientClick="window.open('frm_Suspender_3.aspx','mensaje','width=350,height=350,left=350,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no');" />
                <asp:Button ID="btnReactivar" Text="Reactivar" visible="false" runat="server" OnClientClick="window.open('frm_Reactivar_3.aspx','mensaje','width=600,height=400,left=250,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no');" />
            </td>
        </tr>
        <tr>
            <td align="left" class="td4">
                <font color='#F5DA81' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Label ID="lblRueda" runat="server"></asp:Label>
                </font>
            </td>
            <td align="left" class="td4">
                <font color='#F5DA81' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Label ID="lblFase" runat="server"></asp:Label>
                </font>
            </td>
            <td align="left" class="td4">
                <font color='#F5DA81' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Label ID="lblHora" runat="server"></asp:Label>
                </font>
            </td>
            <td align="left" class="td4">
                <font color='#F5DA81' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Label ID="lblAgente" runat="server"></asp:Label>
                </font>
            </td>
            <td align="left" class="td4">
                <asp:DropDownList ID="ddlRuta" runat="server" Font-Names="Arial, Helvetica, sans-serif"
                    Font-Size="15px">
                </asp:DropDownList>
            </td>
            <td align="center" class="td4" >
                <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Height="30" Visible="false" />
            </td>
            <td align="center" class="td4">
                <asp:Button ID="btnMensaje" Text="Mensaje" runat="server" OnClientClick="window.open('frm_enviarMensaje_1.aspx','mensaje','width=350,height=250,left=350,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no');" />
                <asp:DataGrid ID="dtgSubasta" runat="server" AutoGenerateColumns="False" Visible="false">
                    <ItemStyle CssClass="td2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="15px">
                    </ItemStyle>
                    <Columns>
                        <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="60px"></asp:BoundColumn>
                        <%--1--%><asp:BoundColumn DataField="desc_producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="150px"></asp:BoundColumn>
                        <%--2--%><asp:BoundColumn DataField="desc_unidad_medida" HeaderText="UM" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="60px"></asp:BoundColumn>
                        <%--3--%><asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="120px"></asp:BoundColumn>
                        <%--4--%><asp:BoundColumn DataField="desc_punto" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="200px"></asp:BoundColumn>
                        <%--5--%><asp:BoundColumn DataField="desc_periodo" HeaderText="Periodo Entrega" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="100px"></asp:BoundColumn>
                        <%--6--%><asp:BoundColumn DataField="cantidad_total_venta" HeaderText="Cantidad Vta (KPCD)"
                            DataFormatString="{0: ###,###,###,###,###,###,##0}" ItemStyle-HorizontalAlign="Right"
                            ItemStyle-Width="100px"></asp:BoundColumn> <%--20201020--%>
                        <%--7--%><asp:BoundColumn DataField="desc_estado" HeaderText="Estado" ItemStyle-BackColor=""
                            ItemStyle-Width="80px"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="HndFechaRueda" runat="server" />
</asp:Content>