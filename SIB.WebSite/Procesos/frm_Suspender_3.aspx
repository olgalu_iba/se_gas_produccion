﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_Suspender_3.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_Suspender_3" EnableEventValidation="false" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" language="javascript">
        //        document.location = no;
        function confirmar() {
            return confirm("Esta seguro que desea pasar la rueda para otra fecha?")
        }

    </script>

   
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td6" colspan="2" align="center">
                        <asp:Label ID="lblTitulo" runat="server" Text="CAMBIAR FECHA DE RUEDA" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="2">
                        <asp:Label ID="lblFechaAct" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="td6">
                        Suspensión Definitiva
                    </td>
                    <td class="td6">
                        <asp:DropDownList ID="ddlDefinitivo" runat="server">
                            <asp:ListItem Value="N">No</asp:ListItem>
                            <asp:ListItem Value="S">Si</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="1">
                        Fecha Próxima Apertura
                    </td>
                    <td class="td6" colspan="1">
                        <asp:TextBox ID="TxtFecha" runat="server" Width="100px" MaxLength="10"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RfvTxtFecha" runat="server" ErrorMessage="Debe Ingresar la fecha de apertura"
                            ControlToValidate="TxtFecha" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                       <%-- <ajaxToolkit:CalendarExtender ID="CeTxtFecha" runat="server" TargetControlID="TxtFecha" Format="yyyy/MM/dd">
                        </ajaxToolkit:CalendarExtender>--%>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="2">
                        Observaciones:
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="2">
                        <asp:TextBox ID="TxtObservacion" runat="server" Width="350px" MaxLength="1000" Rows="3"
                            TextMode="MultiLine"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" class="td6">
                        <br />
                        <asp:Button ID="btnSuspender" Text="Suspender" runat="server" OnClientClick="return confirmar();"
                            OnClick="btnSuspender_Click" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
 </asp:Content>