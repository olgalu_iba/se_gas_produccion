﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_CrearIdUvlp.aspx.cs"
    Inherits="Procesos_frm_CrearIdUvlp" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
            width="85%">
            <tr>
                <td align="center" class="th1">
                    <asp:Label ID="lblTitulo" runat="server" ForeColor="White"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:Button ID="BtnCrear" runat="server" Text="Crear" OnClick="BtnCrear_Click" OnClientClick="MostrarCrono(); this.disabled = true;"
                    UseSubmitBehavior="false" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">Tiempo transcurrido: &nbsp;
                <input type="text" name="display" id="display" size="8" value="00:00:0" class="reloj"
                    readonly="true">
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla" visible="false">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 900px; height: 400px;">
                    <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="numero_id" HeaderText="Id" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_rueda" HeaderText="rueda" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_rueda" HeaderText="fecha rueda" ItemStyle-HorizontalAlign="center"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_ruta" HeaderText="ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="modalidad" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_periodo" HeaderText="periodo" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_entrega" HeaderText="Fecha Ini" ItemStyle-HorizontalAlign="left"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_entrega_fin" HeaderText="Fecha Fin" ItemStyle-HorizontalAlign="left"
                                DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                            <%--20210531--%>
                            <asp:BoundColumn DataField="cantidad_total_venta" HeaderText="Cantidad ID (KPCD)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,###,##0}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="cd ope" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="operador" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                            <%--20210531--%>
                            <asp:BoundColumn DataField="cantidad_postura" HeaderText="cantidad operador (KPCD)" ItemStyle-HorizontalAlign="right"
                                DataFormatString="{0:###,###,##0}"></asp:BoundColumn>
                            <%--20210531--%> <%--20221012--%>
                            <asp:BoundColumn DataField="precio" HeaderText="precio operador (Moneda Vigente/KPC)" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0:###,##0.00}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
