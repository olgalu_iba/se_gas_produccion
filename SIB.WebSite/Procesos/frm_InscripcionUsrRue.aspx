﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_InscripcionUsrRue.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Procesos.frm_InscripcionUsrRue" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tipo Subasta" AssociatedControlID="ddlBusTipoSubasta" runat="server" />
                            <asp:DropDownList ID="ddlBusTipoSubasta" CssClass="form-control selectpicker" data-live-search="true" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlBusTipoSubasta_SelectedIndexChanged" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tipo Rueda" AssociatedControlID="ddlBusTipoRueda" runat="server" />
                            <asp:DropDownList ID="ddlBusTipoRueda" CssClass="form-control selectpicker" data-live-search="true" runat="server">
                                <asp:ListItem Value="0">Seleccione</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Usuario" AssociatedControlID="ddlBusUsuario" runat="server" />
                            <asp:DropDownList ID="ddlBusUsuario" CssClass="form-control selectpicker" data-live-search="true" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Estado" AssociatedControlID="ddlBusEstado" runat="server" />
                            <asp:DropDownList ID="ddlBusEstado" CssClass="form-control selectpicker" data-live-search="true" runat="server">
                                <asp:ListItem Value="">Seleccione</asp:ListItem>
                                <asp:ListItem Value="C">Creado</asp:ListItem>
                                <asp:ListItem Value="A">Aprobado</asp:ListItem>
                                <asp:ListItem Value="R">Rechazado</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgConsulta" runat="server" Visible="False" AutoGenerateColumns="False" AllowPaging="True" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered"
                                OnItemCommand="dtgConsulta_EditCommand" OnPageIndexChanged="dtgConsulta_PageIndexChanged" PageSize="10">
                                <Columns>
                                    <asp:BoundColumn DataField="codigo_usuario_hab" HeaderText="Código" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_tipo_subasta" HeaderText="Subasta" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_tipo_subasta" HeaderText="Descripción Sububasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_tipo_rueda" HeaderText="Rueda" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_tipo_rueda" HeaderText="Descripción Tipo Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_usuario" HeaderText="Código Usuario" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <%--20190607 rq036-19--%>
                                    <asp:BoundColumn DataField="login" HeaderText="Login usuario" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_usuario" HeaderText="Nombre" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--20190607 rq036-19--%>
                                    <asp:BoundColumn DataField="rol_operador" HeaderText="Rol Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="center" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="center" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <%--20190607 rq036-19--%>
                                    <asp:BoundColumn DataField="fecha_solicitud" HeaderText="Fecha Solicitud" ItemStyle-HorizontalAlign="center" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more-1"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                    <!--begin::Nav-->
                                                    <ul class="kt-nav">
                                                        <li class="kt-nav__item">
                                                            <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                             <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Modificar</span>
                                                            </asp:LinkButton>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon-delete"></i>
                                                            <span class="kt-nav__link-text">Eliminar</span>
                                                            </asp:LinkButton>
                                                        </li>
                                                    </ul>
                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%--Modals--%>
    <%--Modal Crear--%>
    <div class="modal fade" id="mdlCrear" tabindex="-1" role="dialog" aria-labelledby="mdlCrearLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlCrearInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlCrearLabel" runat="server">Agregar</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label Text="Tipo Subasta" AssociatedControlID="ddlTipoSubasta" runat="server" />
                                        <asp:DropDownList ID="ddlTipoSubasta" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlTipoSubasta_SelectedIndexChanged" AutoPostBack="true" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label Text="Tipo Rueda" AssociatedControlID="ddlTipoSubasta" runat="server" />
                                        <asp:DropDownList ID="ddlTipoRueda" CssClass="form-control" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label Text="Usuario" AssociatedControlID="ddlUsuario" runat="server" />
                                        <asp:DropDownList ID="ddlUsuario" CssClass="form-control" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label Text="Fecha Inicial" AssociatedControlID="TxtFechaIni" runat="server" />
                                        <asp:TextBox ID="TxtFechaIni" placeholder="yyyy/mm/dd" CssClass="form-control datepicker" runat="server" MaxLength="100" Width="100%" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label Text="Fecha Final" AssociatedControlID="TxtFechaIni" runat="server" />
                                        <asp:TextBox ID="TxtFechaFin" placeholder="yyyy/mm/dd" CssClass="form-control datepicker" runat="server" MaxLength="100" Width="100%" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label Text="Estado" AssociatedControlID="ddlEstado" runat="server" />
                                        <asp:DropDownList ID="ddlEstado" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="C">Creado</asp:ListItem>
                                            <asp:ListItem Value="A">Aprobado</asp:ListItem>
                                            <asp:ListItem Value="R">Rechazado</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="imbSalir" Text="Cancelar" UseSubmitBehavior="false" OnClientClick="this.disabled = true;" OnClick="btnSalir_Click1" CssClass="btn btn-secondary" runat="server" />
                            <asp:Button ID="imbActualiza" runat="server" Text="Actualizar" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="btnActualiza_Click" CssClass="btn btn-primary" ValidationGroup="comi" />
                            <asp:Button ID="imbCrear" runat="server" Text="Crear" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="btnCrear_Click" CssClass="btn btn-primary" ValidationGroup="comi" />
                            <ajaxToolkit:ConfirmButtonExtender ID="CbeimbCrear" runat="server" TargetControlID="imbCrear" ConfirmText="Esta Seguro(a) de Ingresar la Información ?" />
                            <ajaxToolkit:ConfirmButtonExtender ID="CbeimbActualiza" runat="server" TargetControlID="imbActualiza" ConfirmText="Esta Seguro(a) de Modificar la Información ?" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

</asp:Content>
