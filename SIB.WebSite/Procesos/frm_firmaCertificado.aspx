﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_firmaCertificado.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Procesos.Procesos_frm_firmaCertificado" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitle" Text="Firma e Impresión PDF" runat="server" />
                        <asp:HiddenField ID="hdfOperador" runat="server" />
                        <asp:HiddenField ID="hdfRutaImg" runat="server" />
                        <asp:HiddenField ID="hdfFechaAct" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <strong>
                                <div style="text-align: center">CERTIFICADOS PENDIENTES DE FIRMA (TOKEN)</div>
                            </strong>
                        </div>
                    </div>
                </div>
                <div border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                    id="tblGrilla">
                    <div class="table table-responsive" style="overflow: scroll; width: 100%; height: 350px;">
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:DataGrid ID="dtgFirma" CssClass="table-bordered" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                    PagerStyle-HorizontalAlign="Center" OnEditCommand="dtgFirma_EditCommand">
                                    <Columns>
                                        <asp:EditCommandColumn HeaderText="Firmar" EditText="Firmar"></asp:EditCommandColumn>
                                        <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="numero_contrato" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="numero_id" HeaderText="No. Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="numero_rueda" HeaderText="No. Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="fecha_contrato" HeaderText="Fecha Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="operador_venta" HeaderText="Operador Venta" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="operador_compra" HeaderText="Operador Compra" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="tipo_rueda" HeaderText="Tipo  Rueda" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="fecha_entrega" HeaderText="Fecha Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="tiene_token" Visible="false"></asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                </asp:DataGrid>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <strong>
                                <div style="text-align: center">CERTIFICADOS PENDIENTES DE IMPRESIÓN</div>
                            </strong>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label runat="server">Subasta</asp:Label>
                            <asp:DropDownList ID="ddlSubasta" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label runat="server"> Número Operación</asp:Label>
                            <asp:TextBox ID="TxtOperacion" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FtebTxtOperacion" runat="server" TargetControlID="TxtOperacion" FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label runat="server">Id Registro</asp:Label>
                            <asp:TextBox ID="TxtId" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FtebTxtId" runat="server" TargetControlID="TxtId" FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label runat="server">Estado</asp:Label>
                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                </div>

                <div class="align-self-center mx-auto">
                    <asp:Button ID="btnConsultar" runat="server" CssClass="btn btn-success" Text="Buscar" OnClick="btnConsultar_Click" ValidationGroup="detalle" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                </div>

                <hr />

                <div class="table table-responsive" style="overflow: scroll; width: 100%; height: 350px;">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgImprimir" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered"
                                PagerStyle-HorizontalAlign="Center" OnEditCommand="dtgImprimir_EditCommand">
                                <Columns>
                                    <asp:EditCommandColumn HeaderText="Imprimir" EditText="Imprimir"></asp:EditCommandColumn>
                                    <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numero_contrato" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numero_id" HeaderText="No. Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numero_rueda" HeaderText="No. Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_contrato" HeaderText="Fecha Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="operador_venta" HeaderText="Operador Venta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="operador_compra" HeaderText="Operador Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="tipo_rueda" HeaderText="Tipo  Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_entrega" HeaderText="Fecha Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_archivo" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="estado" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_estado" HeaderText="estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="CrearRegistro" tabindex="-1" role="dialog" aria-labelledby="mdlCrearRegistroLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="CrearRegistroInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <asp:Label ID="LblTituloArc" runat="server" Text=" " CssClass="modal-title"></asp:Label>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label Text="Ingrese el código IFE enviado por correo electrónico" AssociatedControlID="TxtCodigo" runat="server" />
                                        <asp:TextBox ID="TxtCodigo" runat="server" CssClass="form-control" />
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTEBTxtCodigo" runat="server" TargetControlID="TxtCodigo"
                                            FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <asp:Label ID="lblMensaje" runat="server"></asp:Label>
                                <asp:Button ID="BtnCancelar" Text="Cancelar" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" CssClass="btn btn-secondary" runat="server" CausesValidation="false" OnClick="BtnCancelar_Click" />
                                <asp:Button ID="BtnFirmar" runat="server" CssClass="btn btn-primary" Text="Firmar" OnClick="BtnFirmar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
