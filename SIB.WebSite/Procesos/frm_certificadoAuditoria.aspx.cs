﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;
using System.Text;
using Segas.Web.Elements;
// Libbrerias para generar Codigo QR 20181217
using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using System.Drawing;
using System.Drawing.Imaging;
using iTextSharpCert.text;
using iTextSharpCert.text.pdf;
using System.Collections.Generic;
using System.Web;
using System.Collections;

namespace Procesos
{

    public partial class frm_certificadoAuditoria : Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Certificado de gas para auditoría";
        clConexion lConexion = null;
        SqlDataReader lLector;
        DataSet lds = new DataSet();
        SqlDataAdapter lsqldata = new SqlDataAdapter();
        string oRutaFont = ConfigurationManager.AppSettings["RutaFont"].ToString();
        string oRutaImg = ConfigurationManager.AppSettings["RutaIMG"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lConexion = new clConexion(goInfo);
            buttons.AuditoriaOnclick += btnCrear_Click;
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;

            if (!IsPostBack)
            {
                Master.Titulo = lsTitulo;

                hdfRutaPdf.Value = ConfigurationManager.AppSettings["RutaPdf"].ToString();
                hdfRutaImg.Value = ConfigurationManager.AppSettings["RutaIMG"].ToString();
                /// Llenar controles del Formulario
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
                LlenarControles(lConexion.gObjConexion, ddlTipoRueda, "m_tipos_rueda", " 0=1 order by descripcion", 0, 1);
                LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' And codigo_operador != 0 order by razon_social", 0, 4);
                lConexion.Cerrar();

                EnumBotones[] botones = { EnumBotones.Auditoria, EnumBotones.Buscar, EnumBotones.Excel };
                buttons.Inicializar(botones: botones);

                if (Session["tipoPerfil"].ToString() == "N")
                {
                    ddlOperador.SelectedValue = goInfo.cod_comisionista;
                    ddlOperador.Enabled = false;
                }
                hdfOperador.Value = goInfo.cod_comisionista;
                TxtFechaCorte.Text = DateTime.Now.ToString("yyyy/MM/dd");
            }

        }
        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                }
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }
        /// <summary>
        /// Nombre: ChkTodos_CheckedChanged
        /// Fecha: Abril 24 21 de 2012
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para chekear todos los registros
        /// Modificacion:
        /// </summary>
        /// 20210407 ajuste auditoria
        /// 20221121
        protected void ChkTodos_CheckedChanged(object sender, EventArgs e)
        {
            //Selecciona o deselecciona todos los items del datagrid segun el checked del control
            CheckBox chk = (CheckBox)(sender);
            foreach (DataGridItem Grilla in this.dtgMaestro.Items)
            {
                CheckBox Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                if (chk.Checked)
                    Checkbox.Checked = true;
                else
                    Checkbox.Checked = false;
            }
        }
        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            var lblMensaje = "";
            hdfBusOper.Value = "N";
            DateTime ldFecha;//20171130 rq026-17
            if (TxtFechaCorte.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaCorte.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje += "Formato Inválido en el Campo Fecha de corte. <br>";
                }
            }
            else
                lblMensaje += "Debe digitar la fecha de corte. <br>";

            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje += "Formato Inválido en el Campo Fecha Contrato. <br>";
                }
            }
            if (TxtFechaIni.Text.Trim().Length <= 0 && ddlOperador.SelectedValue == "0" && ddlTipoRueda.SelectedValue == "0" && TxtNoRueda.Text.Trim().Length <= 0 && TxtId.Text == "" && TxtOperacion.Text == "")
                lblMensaje += "Debe Ingresar algún Parámetro de búsqueda. <br>";
            if (lblMensaje == "")
            {
                try
                {
                    if (TxtId.Text != "" || TxtOperacion.Text != "")
                        hdfBusOper.Value = "S";
                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    SqlCommand lComando = new SqlCommand();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandTimeout = 3600;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_GetConsContratoFirma";
                    lComando.Parameters.Add("@P_codigo_tipo_subasta", SqlDbType.Int).Value = ddlSubasta.SelectedValue;
                    lComando.Parameters.Add("@P_tipo_rueda", SqlDbType.Int).Value = ddlTipoRueda.SelectedValue;
                    if (TxtNoRueda.Text != "")
                        lComando.Parameters.Add("@P_numero_rueda", SqlDbType.Int).Value = TxtNoRueda.Text.Trim();
                    lComando.Parameters.Add("@P_fecha_contrato", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                    lComando.Parameters.Add("@P_operador", SqlDbType.Int).Value = ddlOperador.SelectedValue;
                    if (TxtId.Text != "")
                        lComando.Parameters.Add("@P_codigo_verif", SqlDbType.Int).Value = TxtId.Text;
                    if (TxtOperacion.Text != "")
                        lComando.Parameters.Add("@P_numero_contrato", SqlDbType.Int).Value = TxtOperacion.Text;
                    lComando.Parameters.Add("@P_fecha_corte", SqlDbType.VarChar).Value = TxtFechaCorte.Text.Trim();

                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgMaestro.DataSource = lds;
                    dtgMaestro.DataBind();
                    tblGrilla.Visible = true;
                    lConexion.Cerrar();
                    if (dtgMaestro.Items.Count == 0)
                        Toastr.Warning(this, "No hay contratos para los filtros seleccionados");
                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, "No se Pudo Generar la Consulta.! " + ex.Message.ToString());
                }
            }
            else
                Toastr.Warning(this, lblMensaje);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlSubasta_SelectedIndexChanged(object sender, EventArgs e)
        {
            lConexion.Abrir();
            ddlTipoRueda.Items.Clear();
            LlenarControles(lConexion.gObjConexion, ddlTipoRueda, "m_tipos_rueda", " estado = 'A' And codigo_tipo_subasta = " + ddlSubasta.SelectedValue + " order by descripcion", 0, 1);
            lConexion.Cerrar();
            try
            {
                switch (ddlSubasta.SelectedValue)
                {
                    case "4":
                        ddlTipoRueda.SelectedValue = "3";
                        break;
                    case "6":
                        ddlTipoRueda.SelectedValue = "8";
                        break;
                    case "7":
                        ddlTipoRueda.SelectedValue = "13";
                        break;
                    case "8":
                        ddlTipoRueda.SelectedValue = "14";
                        break;
                    case "9":
                        ddlTipoRueda.SelectedValue = "15";
                        break;
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20180126 rq107-16
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            SqlDataAdapter lsqldata = new SqlDataAdapter();
            string lsNombreArchivo = goInfo.Usuario.ToString() + "InfCertificado" + DateTime.Now + ".xls";
            string lstitulo_informe = "Informe certificado para auditoría";
            string lsTituloParametros = "";
            try
            {
                if (TxtFechaCorte.Text.Trim().Length > 0)
                    lsTituloParametros += " Fecha Corte: " + TxtFechaCorte.Text.Trim();
                if (ddlSubasta.SelectedValue != "0")
                    lsTituloParametros += " - Subasta: " + ddlSubasta.SelectedItem.ToString();
                if (ddlTipoRueda.SelectedValue != "0" && ddlTipoRueda.SelectedValue != "")
                    lsTituloParametros += " - Tipo rueda: " + ddlTipoRueda.SelectedItem.ToString();
                if (TxtFechaIni.Text.Trim().Length > 0)
                    lsTituloParametros += " Fecha Contrato: " + TxtFechaIni.Text.Trim();
                if (TxtNoRueda.Text.Trim().Length > 0)
                    lsTituloParametros += " - Número Rueda: " + TxtNoRueda.Text.Trim();
                if (ddlOperador.SelectedValue != "0")
                    lsTituloParametros += " - Operador: " + ddlOperador.SelectedItem.ToString();
                if (TxtId.Text.Trim().Length > 0)
                    lsTituloParametros += " - ID Registro: " + TxtId.Text.Trim();
                if (TxtOperacion.Text.Trim().Length > 0)
                    lsTituloParametros += " - Operación: " + TxtOperacion.Text.Trim();

                decimal ldCapacidad = 0;
                StringBuilder lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                dtgMaestro.Columns[0].Visible = false;
                dtgMaestro.EnableViewState = false;
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                lform.Controls.Add(dtgMaestro);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = System.Text.Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());

                Response.End();
                //lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
                dtgMaestro.Columns[0].Visible = true;
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se Pudo Generar el Excel.!" + ex.Message);
            }
        }
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("../frm_contenido.aspx");
        }
        /// <summary>
        /// Nombre: btnProvisional_Click
        /// Fecha: Abril 12 de 2019
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo Para la generacion de los Certificados Provisionales de Gas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCrear_Click(object sender, EventArgs e)
        {
            var lblMensaje = "";
            string lsContrato = "";
            string lsVerif = "";
            string lsCodSubasta = ""; //20201207
            string lsArchivo;
            int liNumero = 0;
            string[] lsNombreParametros = { "@P_codigo_verif", "@P_numero_contrato" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { "0", "0" };
            SqlDataReader lLector;
            lConexion.Abrir();

            //20210407 ajuste auditoria
            int liConta = 0;
            foreach (DataGridItem Grilla in this.dtgMaestro.Items)
            {
                CheckBox Checkbox = null;
                Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];

                if (Checkbox.Checked == true)
                    liConta++;
            }
            if (liConta == 0)
                lblMensaje = "Debe seleccionar al mennos un contrato para generar el certificado. <br>"; //20221121 OM 
            //20221121 OM 
            //if (liConta > 1)
            //    lblMensaje = "Solo debe seleccionar un contrato para generar el certificado. <br>";
            if (lblMensaje == "")
            {
                //20210407 ajuste auditoria
                try
                {
                    var liArchivo = new List<string>();//20221121 OM
                    foreach (DataGridItem Grilla in this.dtgMaestro.Items)
                    {
                        CheckBox Checkbox = null;
                        Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];

                        if (Checkbox.Checked == true)
                        {
                            liNumero++;
                            lsContrato = Grilla.Cells[2].Text;
                            lsVerif = Grilla.Cells[1].Text;
                            lValorParametros[0] = lsVerif;
                            lValorParametros[1] = lsContrato;
                            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetDatosQr", lsNombreParametros, lTipoparametros, lValorParametros);
                            if (lLector.HasRows)
                            {
                                lLector.Read();
                                clImpresionCertificado clImp;
                                clImp = new clImpresionCertificado();
                                lsArchivo = clImp.generarCertAud(lsContrato, lsVerif, goInfo);

                                string archivoQrC = "";
                                string archivoQrV = "";
                                string archivoQrBmc = "";
                                string lsCodigoC = "";
                                string lsCodigoV = "";
                                string lsCodigoBmc = "";
                                string lsTokenC = "";
                                string lsTokenV = "";
                                string lsTokenBmc = "";

                                lsTokenC = lLector["token_c"].ToString();
                                lsTokenV = lLector["token_v"].ToString();
                                lsTokenBmc = lLector["token_bmc"].ToString();
                                lsCodigoC = lLector["nombre_bmc"].ToString() + "\n" + lLector["desc_id"].ToString() + "\n" + lLector["fecha_cert"].ToString() + "\n" + lLector["desc_punta_c"].ToString() + "\n" + lLector["nombre_ope_c"].ToString() + "\n" + lsTokenC;
                                lsCodigoV = lLector["nombre_bmc"].ToString() + "\n" + lLector["desc_id"].ToString() + "\n" + lLector["fecha_cert"].ToString() + "\n" + lLector["desc_punta_v"].ToString() + "\n" + lLector["nombre_ope_v"].ToString() + "\n" + lsTokenV;
                                lsCodigoBmc = lLector["nombre_bmc"].ToString() + "\n" + lLector["desc_id"].ToString() + "\n" + lLector["fecha_cert"].ToString() + "\n" + lLector["firma_bmc"].ToString() + "\n" + lsTokenBmc;

                                lsCodSubasta = lLector["codigo_tipo_subasta"].ToString(); //20201207
                                                                                          //compra
                                QrEncoder oEnCoderC = new QrEncoder(ErrorCorrectionLevel.H);
                                QrCode oCodigoC = new QrCode();
                                oEnCoderC.TryEncode(lsCodigoC, out oCodigoC);
                                GraphicsRenderer oGraficaC = new GraphicsRenderer(new FixedCodeSize(400, QuietZoneModules.Zero), System.Drawing.Brushes.Black, System.Drawing.Brushes.White);
                                MemoryStream msQC = new MemoryStream();
                                oGraficaC.WriteToStream(oCodigoC.Matrix, ImageFormat.Png, msQC);
                                var ImageTmpC = new Bitmap(msQC);
                                var oImagenC = new Bitmap(ImageTmpC, new Size(new Point(200, 200)));
                                archivoQrC = hdfRutaPdf.Value + "cert_" + lsContrato + "_" + DateTime.Now.Millisecond.ToString() + "C.png";
                                if (File.Exists(archivoQrC))
                                    File.Delete(archivoQrC);
                                oImagenC.Save(archivoQrC, ImageFormat.Png);
                                oImagenC.Dispose();

                                //venta
                                QrEncoder oEnCoderV = new QrEncoder(ErrorCorrectionLevel.H);
                                QrCode oCodigoV = new QrCode();
                                oEnCoderV.TryEncode(lsCodigoV, out oCodigoV);
                                GraphicsRenderer oGraficaV = new GraphicsRenderer(new FixedCodeSize(400, QuietZoneModules.Zero), System.Drawing.Brushes.Black, System.Drawing.Brushes.White);
                                MemoryStream msQV = new MemoryStream();
                                oGraficaV.WriteToStream(oCodigoV.Matrix, ImageFormat.Png, msQV);
                                var ImageTmpV = new Bitmap(msQV);
                                var oImagenV = new Bitmap(ImageTmpV, new Size(new Point(200, 200)));
                                archivoQrV = hdfRutaPdf.Value + "cert_" + lsContrato + "_" + DateTime.Now.Millisecond.ToString() + "V.png";
                                if (File.Exists(archivoQrV))
                                    File.Delete(archivoQrV);
                                oImagenV.Save(archivoQrV, ImageFormat.Png);
                                oImagenV.Dispose();

                                //BMC
                                if (lLector["ver_token_bmc"].ToString() == "S")
                                {
                                    QrEncoder oEnCoderBmc = new QrEncoder(ErrorCorrectionLevel.H);
                                    QrCode oCodigoBmc = new QrCode();
                                    oEnCoderBmc.TryEncode(lsCodigoBmc, out oCodigoBmc);
                                    GraphicsRenderer oGraficaBmc = new GraphicsRenderer(new FixedCodeSize(400, QuietZoneModules.Zero), System.Drawing.Brushes.Black, System.Drawing.Brushes.White);
                                    MemoryStream msQBmc = new MemoryStream();
                                    oGraficaBmc.WriteToStream(oCodigoBmc.Matrix, ImageFormat.Png, msQBmc);
                                    var ImageTmpBmc = new Bitmap(msQBmc);
                                    var oImagenBmc = new Bitmap(ImageTmpBmc, new Size(new Point(200, 200)));
                                    archivoQrBmc = hdfRutaPdf.Value + "cert_" + lsContrato + "_" + DateTime.Now.Millisecond.ToString() + "Bmc.png";
                                    if (File.Exists(archivoQrBmc))
                                        File.Delete(archivoQrBmc);
                                    oImagenBmc.Save(archivoQrBmc, ImageFormat.Png);
                                    oImagenBmc.Dispose();
                                }

                                PdfReader reader = new PdfReader(hdfRutaPdf.Value + lsArchivo);
                                iTextSharpCert.text.Rectangle size = reader.GetPageSizeWithRotation(1);
                                Document document = new Document(PageSize.LETTER, 20, 20, 20, 20);

                                BaseFont bf = BaseFont.CreateFont(oRutaFont + "arialbd.ttf", BaseFont.WINANSI, true);

                                // open the writer
                                string lsNombrePdf = "CertGas_" + lsContrato + "_" + goInfo.Usuario + DateTime.Now.Millisecond.ToString() + ".pdf";
                                string pdfDest = hdfRutaPdf.Value + lsNombrePdf;
                                if (File.Exists(pdfDest))
                                    File.Delete(pdfDest);
                                FileStream fs = new FileStream(pdfDest, FileMode.Create, FileAccess.Write);
                                PdfWriter writer = PdfWriter.GetInstance(document, fs);
                                document.Open();

                                // the pdf content
                                PdfContentByte cb = writer.DirectContent;
                                cb.SetFontAndSize(bf, 8);
                                cb.SetColorFill(iTextSharpCert.text.Color.BLACK);

                                /// Creo el objeto Imagen
                                iTextSharpCert.text.Image codigoQrC = iTextSharpCert.text.Image.GetInstance(archivoQrC);
                                codigoQrC.ScalePercent(35);
                                codigoQrC.Alignment = Element.ALIGN_CENTER;

                                iTextSharpCert.text.Image codigoQrV = iTextSharpCert.text.Image.GetInstance(archivoQrV);
                                codigoQrV.ScalePercent(35);
                                codigoQrV.Alignment = Element.ALIGN_CENTER;

                                iTextSharpCert.text.Image codigoQrBmc = null;
                                if (archivoQrBmc != "")
                                {
                                    codigoQrBmc = iTextSharpCert.text.Image.GetInstance(archivoQrBmc);
                                    codigoQrBmc.ScalePercent(35);
                                    codigoQrBmc.Alignment = Element.ALIGN_CENTER;
                                }

                                // write the text in the pdf content
                                cb.BeginText();
                                string text = "";
                                if (lsCodSubasta == "2") //20201207
                                {
                                    codigoQrC.SetAbsolutePosition(460, 120); //20200714 ajsute error
                                    text = "Código QR Comprador:" + lsTokenC;
                                    cb.ShowTextAligned(1, text, 490, 110, 0); //20200714 ajsute error

                                    codigoQrV.SetAbsolutePosition(325, 120); //20200714 ajsute error
                                    text = "Código QR Vendedor:" + lsTokenC;
                                    cb.ShowTextAligned(1, text, 355, 110, 0); //20200714 ajsute error

                                    if (archivoQrBmc != "")
                                    {
                                        codigoQrBmc.SetAbsolutePosition(200, 120);
                                        text = "Código QR BMC:" + lsTokenBmc;
                                        cb.ShowTextAligned(1, text, 230, 110, 0);
                                    }
                                }
                                else //20201207
                                {
                                    codigoQrC.SetAbsolutePosition(440, 150); //20200714 ajsute error
                                    text = "Código QR Comprador:" + lsTokenC;
                                    cb.ShowTextAligned(1, text, 468, 140, 0); //20200714 ajsute error

                                    codigoQrV.SetAbsolutePosition(80, 150); //20200714 ajsute error
                                    text = "Código QR Vendedor:" + lsTokenC;
                                    cb.ShowTextAligned(1, text, 120, 140, 0); //20200714 ajsute error

                                    if (archivoQrBmc != "")
                                    {
                                        codigoQrBmc.SetAbsolutePosition(280, 70);
                                        text = "Código QR BMC:" + lsTokenBmc;
                                        cb.ShowTextAligned(1, text, 310, 60, 0);
                                    }
                                }
                                iTextSharpCert.text.Image imgLinea = iTextSharpCert.text.Image.GetInstance(hdfRutaImg.Value + "linea.png");
                                imgLinea.ScalePercent(35);
                                imgLinea.Alignment = Element.ALIGN_CENTER;
                                imgLinea.SetAbsolutePosition(50, 40);

                                cb.EndText();

                                for (int i = 1; i <= reader.NumberOfPages; i++)
                                {
                                    // create the new page and add it to the pdf
                                    PdfImportedPage page = writer.GetImportedPage(reader, i);
                                    cb.AddTemplate(page, 0, 0);
                                }
                                // close the streams and voilá the file should be changed :)

                                document.Add(codigoQrC);
                                document.Add(codigoQrV);
                                if (archivoQrBmc != "")
                                    document.Add(codigoQrBmc);
                                document.Add(imgLinea);
                                document.Close();
                                fs.Close();
                                writer.Close();
                                reader.Close();
                                File.Delete(archivoQrC);
                                File.Delete(archivoQrV);
                                if (archivoQrBmc != "")
                                    File.Delete(archivoQrBmc);
                                File.Delete(hdfRutaPdf.Value + lsArchivo);
                                //divToken.Visible = false;


                                if (lsArchivo != "ERROR")
                                {
                                    //20221121 OM
                                    liArchivo.Add(lsNombrePdf);
                                }
                                else
                                    lblMensaje = "Error";
                            }
                            lLector.Close();
                        }
                    }
                    lConexion.Cerrar();
                    //20221121 OM aqui
                    List<byte[]> liByteArch= new List<byte[]>();
                    string lsArchivoCert1 = "CertificadoAud_" + DateTime.Now.ToString("yyyyMMdd_HHmm") + DateTime.Now.Millisecond.ToString() + ".pdf";
                    string lsArchivoCert = hdfRutaPdf.Value + lsArchivoCert1;
                    if (File.Exists(lsArchivoCert))
                        File.Delete(lsArchivoCert);

                    {
                        foreach (var item in liArchivo)
                        {
                            PdfReader reader = new PdfReader(hdfRutaPdf.Value + item);
                            // Creamos el nuevo PDF
                            MemoryStream m = new MemoryStream();
                            Document pdf = new Document(PageSize.LETTER);
                            PdfWriter writer = PdfWriter.GetInstance(pdf, m);// Instrucción para que el PDF imprima correctamente según el tamaño de papel seleccionado.
                            writer.AddViewerPreference(PdfName.PICKTRAYBYPDFSIZE, PdfBoolean.PDFTRUE);// Añadimos los atributos del nuevo PDF

                            // Abrimos el documento
                            pdf.Open();

                            PdfContentByte cb = writer.DirectContent;

                            // Aquí declaramos el tipo de letra, tamaño y color que deseamos utilizar
                            BaseFont bf = BaseFont.CreateFont(oRutaFont + "arialbd.ttf", BaseFont.WINANSI, true);
                            cb.SetFontAndSize(bf, 12);

                            // Creamos una nueva página e importamos el contenido del paso #1: portada.pdf
                            PdfImportedPage page = writer.GetImportedPage(reader, 1);
                            cb.AddTemplate(page, 0, 0);

                            pdf.Close();
                            writer.Close();// En el caso que querramos guardarlo en una carpeta
                            byte[] bytArr = m.ToArray();
                            liByteArch.Add(bytArr);
                        }

                        byte[] bytArr1 = MergeFiles(liByteArch);


                        using (FileStream fs = File.Create(lsArchivoCert))
                        {
                            fs.Write(bytArr1, 0, (int)bytArr1.Length);
                        }
                        
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('../PDF/" + lsArchivoCert1 + "');", true);
                    //20221121 fin OM 


                }
                catch (Exception ex)
                {
                    /// Desbloquea el Registro Actualizado
                    lblMensaje = ex.Message;
                }
            } //20210407 ajsute auditoria
            if (lblMensaje.ToString() != "")
                Toastr.Error(this, lblMensaje.ToString());
        }

        //20221121 OM 
        public static byte[] MergeFiles(List<byte[]> sourceFiles)
        {
            Document document = new Document();
            using (MemoryStream ms = new MemoryStream())
            {
                PdfCopy copy = new PdfCopy(document, ms);
                document.Open();
                int documentPageCounter = 0;

                // Iterate through all pdf documents
                for (int fileCounter = 0; fileCounter < sourceFiles.Count; fileCounter++)
                {
                    // Create pdf reader
                    PdfReader reader = new PdfReader(sourceFiles[fileCounter]);
                    int numberOfPages = reader.NumberOfPages;

                    // Iterate through all pages
                    for (int currentPageIndex = 1; currentPageIndex <= numberOfPages; currentPageIndex++)
                    {
                        documentPageCounter++;
                        PdfImportedPage importedPage = copy.GetImportedPage(reader, currentPageIndex);
                        PdfCopy.PageStamp pageStamp = copy.CreatePageStamp(importedPage);

                        pageStamp.AlterContents();

                        copy.AddPage(importedPage);
                    }

                    copy.FreeReader(reader);
                    reader.Close();
                }

                document.Close();
                return ms.GetBuffer();
            }
        }
    }
}