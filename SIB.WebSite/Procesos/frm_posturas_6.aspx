﻿<%@ Page Language="C#" MasterPageFile="~/PlantillaPrincipal.master" AutoEventWireup="true" CodeFile="frm_posturas_6.aspx.cs" Inherits="Procesos_frm_posturas_6" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" lang="javascript">

        window.onload = function() {
            var pos = window.name || 0;
            window.scrollTo(0, pos);
        }
        window.onunload = function() {
            window.name = self.pageYOffset || (document.documentElement.scrollTop + document.body.scrollTop);
        }

    </script>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="divOferta" runat="server" style="position: absolute; left: 0; top: 0; width: 100%;">
                <table cellpadding="0" cellspacing="0" bgcolor="#000000">
                    <tr>
                        <td align="center">
                            <asp:DataGrid ID="dtgSubasta1" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                ItemStyle-CssClass="td2" HeaderStyle-CssClass="th2" runat="server" OnItemCommand="OnItemCommand_Click"
                                ShowHeader="false">
                                <ItemStyle CssClass="td2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="15px">
                                </ItemStyle>
                                <Columns>
                                    <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="60px"></asp:BoundColumn>
                                    <%--1--%><asp:BoundColumn DataField="desc_modalidad" HeaderText="Tipo" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--2--%><asp:BoundColumn DataField="desc_periodo" HeaderText="Duracion" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Width="130px"></asp:BoundColumn>
                                    <%--3--%><asp:BoundColumn DataField="desc_punto_entrega" HeaderText="lugar" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="250px"></asp:BoundColumn>
                                    <%--4--%><asp:BoundColumn DataField="cantidad_venta" HeaderText="Cantidad venta"
                                        DataFormatString="{0:###,###,###,###,###,###,##0 }" ItemStyle-HorizontalAlign="Right"
                                        ItemStyle-Width="100px"></asp:BoundColumn>
                                    <%--5--%><asp:BoundColumn DataField="precio_venta" HeaderText="Precio venta" DataFormatString="{0:###,###,###,###,###,###,##0.00 }"
                                        ItemStyle-HorizontalAlign="Right" ItemStyle-Width="70px"></asp:BoundColumn>
                                    <%--6--%><asp:BoundColumn DataField="precio_delta" HeaderText="Precio delta" DataFormatString="{0:###,###,###,###,###,###,##0.00}"
                                        ItemStyle-HorizontalAlign="Right" ItemStyle-Width="70px"></asp:BoundColumn>
                                    <%--7--%><asp:BoundColumn DataField="cantidad_adjudicada" HeaderText="cnt adj" DataFormatString="{0:###,###,###,###,###,###,##0.00 }"
                                        ItemStyle-HorizontalAlign="Right" Visible="false" ItemStyle-Width="100px">
                                    </asp:BoundColumn>
                                    <%--8--%><asp:BoundColumn DataField="precio_adjudicado" HeaderText="precio adj" DataFormatString="{0:###,###,###,###,###,###,##0.00}"
                                        ItemStyle-HorizontalAlign="Right" Visible="false" ItemStyle-Width="70px"></asp:BoundColumn>
                                    <%--9--%><asp:BoundColumn DataField="mejor_postura" Visible="false" ItemStyle-Width="60px">
                                    </asp:BoundColumn>
                                    <%--10--%><asp:BoundColumn DataField="hora_prox_fase" HeaderText="Hora Porx fase"
                                        Visible="false" HeaderStyle-Width="60px"></asp:BoundColumn>
                                    <%--11--%><asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-Width="100px">
                                    </asp:BoundColumn>
                                    <%--12--%><asp:TemplateColumn HeaderText="Ofr" ItemStyle-Width="60px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbOfertar" runat="server" ToolTip="Ofertar" ImageUrl="~/Images/nuevo.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--13--%><asp:TemplateColumn HeaderText="Modificación" ItemStyle-Width="60px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbModificar" runat="server" ToolTip="Modificar Oferta" ImageUrl="~/Images/modificar.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--14--%><asp:TemplateColumn HeaderText="Eliminación" ItemStyle-Width="60px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbEliminar" runat="server" ToolTip="Eliminar Oferta" ImageUrl="~/Images/eliminar.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--15--%><asp:TemplateColumn HeaderText="Grafica" ItemStyle-Width="60px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbGrafica" runat="server" ToolTip="curva agregada" ImageUrl="~/Images/grafica.jpg" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--16--%><asp:BoundColumn DataField="ind_vendedor" Visible="false" ItemStyle-Width="60px">
                                    </asp:BoundColumn>
                                    <%--17--%><asp:BoundColumn DataField="ind_postura" Visible="false" ItemStyle-Width="60px">
                                    </asp:BoundColumn>
                                    <%--18--%><asp:BoundColumn DataField="no_posturas_compra" Visible="false" ItemStyle-Width="60px">
                                    </asp:BoundColumn>
                                    <%--19--%><asp:BoundColumn DataField="habilita_c" Visible="false" ItemStyle-Width="60px">
                                    </asp:BoundColumn>
                                    <%--20--%><asp:BoundColumn DataField="habilita_v" Visible="false" ItemStyle-Width="60px">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                            <asp:HiddenField ID="hndCodSubasta" runat="server" />
                            <asp:HiddenField ID="HndFechaRueda" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    




</asp:Content>