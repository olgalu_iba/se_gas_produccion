﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;
using AjaxControlToolkit;
using Segas.Web.Elements;

public partial class Procesos_frm_titulos : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    //String strRutaArchivo;
    private System.TimeSpan diffResult;
    static String lsCadenaCon;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            if (Session["destino_rueda"].ToString() == "G")
            {
                dtgSubasta.Columns[4].HeaderText = "Sitio Entrega";
                dtgDeclara.Columns[2].HeaderText = "Pto Ent";
                dtgDeclara.Columns[3].HeaderText = "Desc Punto ent";
            }
            else
            {
                dtgSubasta.Columns[4].HeaderText = "Ruta";
                dtgDeclara.Columns[2].HeaderText = "Cod pto ini";
                dtgDeclara.Columns[3].HeaderText = "Desc ruta";
            }

            try
            {
                lsCadenaCon = "Data Source=" + goInfo.Servidor.ToString() + "; Initial Catalog=" + goInfo.BaseDatos.ToString() + ";User ID = " + goInfo.Usuario.ToString() + "; Password=" + goInfo.Password.ToString();
                lConexion1 = new clConexion(goInfo);
                cargarDatosGrilla();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, ex.Message);
            }
        }
    }
    /// <summary>
    /// Nombre: cargarDatosGrilla
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para Cargar los datos en la Grio.
    /// Modificacion:
    /// </summary>
    private void cargarDatosGrilla()
    {
            try
            {
                //determina las columnas para el subastatodr o los operaores
                string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_punto", };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int};
                string[] lValorParametros = {"0", "0"};
                lConexion.Abrir();
                if (Session["estado_rueda"].ToString() == "1" || Session["estado_rueda"].ToString() == "2")
                {
                    dtgDeclara.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaPuja", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgDeclara.DataBind();
                    //if (Session["estado_rueda"].ToString() == "2")
                    //    dtgDeclara.Columns[6].Visible = false;
                    dtgSubasta.Visible = false;
                    dtgDeclara.Visible = true;
                }
                else
                {
                    dtgSubasta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaPuja", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgSubasta.DataBind();
                    dtgDeclara.Visible = false;
                    dtgSubasta.Visible = true;
                    if (Session["estado_rueda"].ToString() == "5" || Session["estado_rueda"].ToString() == "F")
                        dtgSubasta.Columns[19].Visible = true;
                    else
                        dtgSubasta.Columns[19].Visible = false;
                }
                if (Session["estado_rueda"].ToString() == "1" || Session["estado_rueda"].ToString() == "2" || Session["estado_rueda"].ToString() == "4")
                {
                    dtgSubasta.Columns[18].Visible = false;
                    dtgSubasta.Columns[10].Visible = true;
                    dtgSubasta.Columns[11].Visible = true;
                    dtgSubasta.Columns[12].Visible = true;
                }
                else
                {
                    if (Session["estado_rueda"].ToString() == "3")
                        dtgSubasta.Columns[18].Visible = false;
                    else
                        dtgSubasta.Columns[18].Visible = true;
                    dtgSubasta.Columns[10].Visible = false;
                    dtgSubasta.Columns[11].Visible = false;
                    dtgSubasta.Columns[12].Visible = false;
                }
                if (Session["tipoPerfil"].ToString() == "B")
                {
                    dtgSubasta.Columns[10].Visible = false;
                    dtgSubasta.Columns[11].Visible = false;
                    dtgSubasta.Columns[12].Visible = false;
                    dtgDeclara.Columns[9].Visible = false;
                    dtgDeclara.Columns[10].Visible = false;
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
            Toastr.Warning(this, ex.Message);
        }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.postura.location= 'frm_posturas.aspx';", true);
        }
    }

    