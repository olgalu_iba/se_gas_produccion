﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_tablero_neg_prm.aspx.cs"
    Inherits="Procesos_frm_tablero_neg_prm" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link href="../css/estiloTab.css" rel="stylesheet" type="text/css" />
</head>
<body style="background-color: #00406B">
    <form id="frmListado" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table border="0" align="center" cellpadding="0" cellspacing="0" runat="server" id="Table1"
        width="100%">
        <tr>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Agente: </font>
            </td>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Rueda: </font>
            </td>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Punto / Ruta:
                </font>
            </td>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Periodo: </font>
            </td>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Fecha: </font>
            </td>
            <td align="center" class="td4">
                <asp:Button ID="btnBuscar" Text="Buscar" runat="server" OnClick="btnBuscar_Click" />
            </td>
        </tr>
        <tr>
            <td align="center" class="td4">
                <font color='#F5DA81' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Label ID="lblAgente" runat="server"></asp:Label>
                </font>
            </td>
            <td align="center" class="td4">
                <asp:DropDownList ID="ddlRueda" runat="server" OnSelectedIndexChanged="ddlRueda_SelectedIndexChanged"
                    AutoPostBack="true">
                </asp:DropDownList>
            </td>
            <td align="center" class="td4">
                <asp:DropDownList ID="ddlPuntoRuta" runat="server" OnSelectedIndexChanged="ddlPuntoRuta_SelectedIndexChanged"
                    AutoPostBack="true">
                </asp:DropDownList>
            </td>
            <td align="center" class="td4">
                <asp:DropDownList ID="ddlPeriodo" runat="server" OnSelectedIndexChanged="ddlPeriodo_SelectedIndexChanged"
                    AutoPostBack="true">
                </asp:DropDownList>
            </td>
            <td align="center" class="td4">
                <font color='#F5DA81' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Label ID="lblFecha" runat="server"></asp:Label>
                </font>
            </td>
            <td align="center" class="td4">
                <asp:Button ID="btnRestablecer" Text="Restablecer" runat="server" OnClick="btnRestablecer_Click" />
            </td>
        </tr>
        <tr>
            <td align="center" class="td4" colspan ="6">
                <asp:Button ID="btnNuevoC" Text="Nuevo" runat="server" OnClick="btnNuevoC_Click" />
            </td>
            <%--<td align="center" class="td4">
            </td>
            <td align="center" class="td4">
            </td>
            <td align="center" class="td4">
            </td>
            <td align="center" class="td4">
            </td>--%>
            <%--<td align="center" class="td4">
                <asp:Button ID="btnNuevoV" Text="Nuevo Venta" runat="server" OnClick="btnNuevoV_Click" />
            </td>--%>
        </tr>
    </table>
    <asp:HiddenField ID="HndFechaRueda" runat="server" />
    <asp:HiddenField ID="HndDiaHabil" runat="server" />
    <asp:HiddenField ID="hndEstado" runat="server" />
    </form>
</body>
</html>
