﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using ConsumoRest;
using Segas.Web.Elements;
using System.Net; //20210415

public partial class Procesos_frm_LlamadoDataIfx : System.Web.UI.Page
{
    static InfoSessionVO goInfo = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");

        try
        {
            /// LLamado al Servicio de DataIfx
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12; //20210415
            var client = new RestClient();
            client.EndPoint = HttpContext.Current.Session["UrlSsmp"].ToString().Trim();
            client.Method = HttpVerb.POST;
            string lsData = "{\"idUser\":\"" + goInfo.codigo_usuario.ToString() + "\",\"UserName\":\"" + goInfo.Usuario + "\",\"Password\":\"" + goInfo.Password + "\"}";
            client.PostData = lsData;
            client.ContentType = "application/json";
            var json = client.MakeRequest();
            json = json.ToString().Replace("\"", "").Replace("\\", "");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + json + "');", true);
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "Error Llamado DataIfx " + ex.Message );
         

        }
    }
}