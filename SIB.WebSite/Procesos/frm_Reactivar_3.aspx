﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_Reactivar_3.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"

    Inherits="Procesos_frm_Reactivar_3" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" language="javascript">
        //        document.location = no;
        function confirmar() {
            return confirm("Está seguro que desea reactivar la rueda!")
        }

    </script>

   
  
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td6" align="center" colspan="4">
                        <asp:Label ID="lblTitulo" runat="server" Text="REACTIVAR RUEDA" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="4">
                        <asp:Label ID="lblEstado" runat="server"></asp:Label>
                        <asp:HiddenField ID="hndFase" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="1">
                        Hora Inicio Fase de Publicación
                    </td>
                    <td class="td6" colspan="1">
                        <asp:TextBox ID="TxtHoraIniPubUvlp" runat="server" MaxLength="5" Width="50px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RevTxtHoraIniPubUvlp" ControlToValidate="TxtHoraIniPubUvlp"
                            ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                            ErrorMessage="Formato Incorrecto para la Hora Inicial de publicación"> * </asp:RegularExpressionValidator>
                    </td>
                    <td class="td6" colspan="1">
                        Hora Fin Fase de Publicación
                    </td>
                    <td class="td6" colspan="1">
                        <asp:TextBox ID="TxtHoraFinPubUvlp" runat="server" MaxLength="5" Width="50px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RevTxtHoraFinPubUvlp" ControlToValidate="TxtHoraFinPubUvlp"
                            ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                            ErrorMessage="Formato Incorrecto para la Hora Final de publicación"> * </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="1">
                        Hora Inicio Fase de Ingreso de Posturas de Compra
                    </td>
                    <td class="td6" colspan="1">
                        <asp:TextBox ID="TxtHoraIniCompUvlp" runat="server" MaxLength="5" Width="50px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RfvTxtHoraIniCompUvlp" ControlToValidate="TxtHoraIniPubUvlp"
                            ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                            ErrorMessage="Formato Incorrecto para la Hora Inicial de ingreso de posturas de compra"> * </asp:RegularExpressionValidator>
                    </td>
                    <td class="td6" colspan="1">
                        Hora Fin Fase de Ingreso de Posturas de Compra
                    </td>
                    <td class="td6" colspan="1">
                        <asp:TextBox ID="TxtHoraFinCompUvlp" runat="server" MaxLength="5" Width="50px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RevTxtHoraFinCompUvlp" ControlToValidate="TxtHoraFinCompUvlp"
                            ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                            ErrorMessage="Formato Incorrecto para la Hora Final de ingreso de posturas de compra"> * </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="1">
                        Hora Inicio Fase de Calce
                    </td>
                    <td class="td6" colspan="1">
                        <asp:TextBox ID="TxtHoraIniCalceUvlp" runat="server" MaxLength="5" Width="50px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RevTxtHoraIniCalceUvlp" ControlToValidate="TxtHoraIniCalceUvlp"
                            ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                            ErrorMessage="Formato Incorrecto para la Hora de inicio de calce"> * </asp:RegularExpressionValidator>
                    </td>
                    <td class="td6" colspan="1">
                        Hora Fin Fase de Calce
                    </td>
                    <td class="td6" colspan="1">
                        <asp:TextBox ID="TxtHoraFinCalceUvlp" runat="server" MaxLength="5" Width="50px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RevTxtHoraFinCalceUvlp" ControlToValidate="TxtHoraFinCalceUvlp"
                            ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                            ErrorMessage="Formato Incorrecto para la Hora Final de calce"> * </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="4">
                        Observaciones:
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="4">
                        <asp:TextBox ID="TxtObservacion" runat="server" Width="500px" MaxLength="1000" Rows="3"
                            TextMode="MultiLine"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" class="td6" colspan="4">
                        <br />
                        <asp:Button ID="btnReactivar" Text="Reactivar" runat="server" OnClientClick="return confirmar();"
                            OnClick="btnReactivar_Click" ValidationGroup="comi" CausesValidation="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center" class="td6">
                        <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>