﻿using System;
using System.Activities.Statements;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Procesos
{
    public partial class frm_solicitudToken : Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Creación de archivos pdf para certificados de Gas";
        clConexion lConexion = null;
        SqlDataReader lLector;
        DataSet lds = new DataSet();
        SqlDataAdapter lsqldata = new SqlDataAdapter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo;
            lConexion = new clConexion(goInfo);
            buttons.CrearOnclick += btnCrear_Click;
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.PreliminarOnclick += btnProvisional_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;

            if (IsPostBack) return;

            //Titulo
            Master.Titulo = "Certificado de Gas";
            //Descripcion
            //Master.DescripcionPagina = "D";

            hdfRutaCert.Value = ConfigurationManager.AppSettings["RutaCert"];
            elimnia_cert();
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
            //ddlSubasta.SelectedValue = "5";
            LlenarControles(lConexion.gObjConexion, ddlTipoRueda, "m_tipos_rueda", " 0=1 order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' And codigo_operador != 0 order by razon_social", 0, 4);
            lConexion.Cerrar();
            //Botones
            EnumBotones[] botones = { EnumBotones.Crear, EnumBotones.Buscar, EnumBotones.Preliminar, EnumBotones.Excel };
            buttons.Inicializar(botones: botones);

            if (Session["tipoPerfil"].ToString() == "N")
            {
                ddlOperador.SelectedValue = goInfo.cod_comisionista;
                ddlOperador.Enabled = false;
            }
            hdfOperador.Value = goInfo.cod_comisionista;
            //DateTime ldfecha = DateTime.Now;
            //TxtFechaCorte.Text = ldfecha.Year.ToString() + "/" + ldfecha.Month.ToString() + "/" + ldfecha.Day.ToString();
            TxtFechaCorte.Text = DateTime.Now.ToString("yyyy/MM/dd");
            //ddlSubasta_SelectedIndexChanged(null, null);

            //20200727 ajsute front-end
            /// Prueba Llamada certificado de GAS
            //clImpresionCertificado cert = new clImpresionCertificado();
            //var retorno = cert.generarCertProv("29127,29128", goInfo);

        }
        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"] + "-" + lLector["razon_social"];
                }
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: ChkTodos_CheckedChanged
        /// Fecha: Abril 24 21 de 2012
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para chekear todos los registros
        /// Modificacion:
        /// </summary>
        protected void ChkTodos_CheckedChanged(object sender, EventArgs e)
        {
            //Selecciona o deselecciona todos los items del datagrid segun el checked del control
            CheckBox chk = (CheckBox)(sender);
            foreach (DataGridItem Grilla in dtgMaestro.Items)
            {
                CheckBox Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                if (chk.Checked)
                    Checkbox.Checked = true;
                else
                    Checkbox.Checked = false;
            }
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                var lblMensaje = "";
                hdfBusOper.Value = "N";
                DateTime ldFecha;//20171130 rq026-17
                if (TxtFechaCorte.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtFechaCorte.Text);
                    }
                    catch (Exception ex)
                    {
                        lblMensaje += "Formato inválido en el campo fecha de corte. <br>";
                    }
                }
                else
                    lblMensaje += "Debe digitar la fecha de corte. <br>";

                if (TxtFechaIni.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
                    }
                    catch (Exception ex)
                    {
                        lblMensaje += "Formato inválido en el campo fecha contrato. <br>";
                    }
                }
                if (TxtFechaIni.Text.Trim().Length <= 0 && ddlOperador.SelectedValue == "0" && ddlTipoRueda.SelectedValue == "0" && TxtNoRueda.Text.Trim().Length <= 0 && TxtId.Text == "" && TxtOperacion.Text == "")
                    lblMensaje += "Debe ingresar algún parámetro de búsqueda. <br>";

                if (lblMensaje == "")
                {
                    if (TxtId.Text != "" || TxtOperacion.Text != "")
                        hdfBusOper.Value = "S";
                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    var lComando = new SqlCommand
                    {
                        Connection = lConexion.gObjConexion,
                        CommandTimeout = 3600,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "pa_GetConsContratoFirma"
                    };
                    lComando.Parameters.Add("@P_codigo_tipo_subasta", SqlDbType.Int).Value = ddlSubasta.SelectedValue;
                    lComando.Parameters.Add("@P_tipo_rueda", SqlDbType.Int).Value = ddlTipoRueda.SelectedValue;
                    if (TxtNoRueda.Text != "")
                        lComando.Parameters.Add("@P_numero_rueda", SqlDbType.Int).Value = TxtNoRueda.Text.Trim();
                    lComando.Parameters.Add("@P_fecha_contrato", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                    lComando.Parameters.Add("@P_operador", SqlDbType.Int).Value = ddlOperador.SelectedValue;
                    if (TxtId.Text != "")
                        lComando.Parameters.Add("@P_codigo_verif", SqlDbType.Int).Value = TxtId.Text;
                    if (TxtOperacion.Text != "")
                        lComando.Parameters.Add("@P_numero_contrato", SqlDbType.Int).Value = TxtOperacion.Text;
                    lComando.Parameters.Add("@P_fecha_corte", SqlDbType.VarChar).Value = TxtFechaCorte.Text.Trim();

                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgMaestro.DataSource = lds;
                    dtgMaestro.DataBind();
                    tblGrilla.Visible = true;
                    lConexion.Cerrar();
                    if (dtgMaestro.Items.Count == 0)
                        Toastr.Warning(this, "No hay contratos registrados para los filtros seleccionados");
                }
                else
                {
                    Toastr.Warning(this, lblMensaje);
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se pudo generar la consulta.! " + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlSubasta_SelectedIndexChanged(object sender, EventArgs e)
        {
            lConexion.Abrir();
            ddlTipoRueda.Items.Clear();
            LlenarControles(lConexion.gObjConexion, ddlTipoRueda, "m_tipos_rueda", " estado = 'A' And codigo_tipo_subasta = " + ddlSubasta.SelectedValue + " order by descripcion", 0, 1);
            lConexion.Cerrar();
            try
            {
                switch (ddlSubasta.SelectedValue)
                {
                    case "4":
                        ddlTipoRueda.SelectedValue = "3";
                        break;
                    case "6":
                        ddlTipoRueda.SelectedValue = "8";
                        break;
                    case "7":
                        ddlTipoRueda.SelectedValue = "13";
                        break;
                    case "8":
                        ddlTipoRueda.SelectedValue = "14";
                        break;
                    case "9":
                        ddlTipoRueda.SelectedValue = "15";
                        break;
                }
            }
            catch (Exception ex)
            {
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCrear_Click(object sender, EventArgs e)
        {
            var lblMensaje = "";
            string lsContratos = "0";
            int liNumero = 0;
            CheckBox Checkbox;
            try
            {
                foreach (DataGridItem Grilla in dtgMaestro.Items)
                {
                    Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                    if (!Checkbox.Checked) continue;
                    liNumero++;
                    if (Grilla.Cells[18].Text.Trim() != "R")
                        lblMensaje += "El Id de registro " + Grilla.Cells[1].Text.Trim() + " no está registrado<br>";
                    if (Grilla.Cells[14].Text.Trim() == "S")
                        lblMensaje += "El Id de registro " + Grilla.Cells[1].Text.Trim() + " tiene modificaciones posteriores a la fecha de corte<br>";
                    if (Grilla.Cells[15].Text.Trim() == "S")
                        lblMensaje += "El operador ya tiene un token vigente para el Id de registro " + Grilla.Cells[1].Text.Trim() + "<br>";
                    if (hdfOperador.Value == "0" && Grilla.Cells[16].Text.Trim() == "S" || Grilla.Cells[16].Text.Trim() == "S" && Grilla.Cells[17].Text.Trim() == "S")
                        lblMensaje += "El Id de registro " + Grilla.Cells[1].Text.Trim() + " ya tiene abierto un proceso de firma<br>";
                    lsContratos += "," + Grilla.Cells[1].Text.Trim();
                }
                if (liNumero == 0)
                    lblMensaje += "Debe seleccionar al menos un Id de registro";
                if (lblMensaje == "")
                {
                    string[] lsNombreParametros = { "@P_codigo_operador", "@P_ids_registro" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
                    string[] lValorParametros = { hdfOperador.Value, lsContratos };
                    SqlDataReader lLector;
                    clImpresionCertificado clImp;
                    clImp = new clImpresionCertificado();
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetTokenFirma", lsNombreParametros, lTipoparametros, lValorParametros);
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                        {
                            if (!File.Exists(hdfRutaCert.Value + lLector["nombre_archivo"]))
                                clImp.generarCertFirma(hdfRutaCert.Value, lLector["codigo_verif"].ToString(), lLector["nombre_archivo"].ToString(), goInfo);
                        }
                    }
                    ScriptManager.RegisterStartupScript(this, GetType(), "StartupAlert", "document.getElementById('dvProg').style.display = 'none';", true);
                    Toastr.Success(this, "Token enviado y almacenado correctamente.!");
                    btnConsultar_Click(null, null);
                }

                if (lblMensaje != "")
                {
                    Toastr.Warning(this, lblMensaje);
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20180126 rq107-16
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            SqlDataAdapter lsqldata = new SqlDataAdapter();
            string lsNombreArchivo = goInfo.Usuario + "InfCertificado" + DateTime.Now + ".xls";
            string lstitulo_informe = "Informe certificado de gas";
            string lsTituloParametros = "";
            try
            {
                if (TxtFechaCorte.Text.Trim().Length > 0)
                    lsTituloParametros += " Fecha Corte: " + TxtFechaCorte.Text.Trim();
                if (ddlSubasta.SelectedValue != "0")
                    lsTituloParametros += " - Subasta: " + ddlSubasta.SelectedItem;
                if (ddlTipoRueda.SelectedValue != "0" && ddlTipoRueda.SelectedValue != "")
                    lsTituloParametros += " - Tipo rueda: " + ddlTipoRueda.SelectedItem;
                if (TxtFechaIni.Text.Trim().Length > 0)
                    lsTituloParametros += " Fecha Contrato: " + TxtFechaIni.Text.Trim(); //20180126 rq107-16
                if (TxtNoRueda.Text.Trim().Length > 0)
                    lsTituloParametros += " - Número Rueda: " + TxtNoRueda.Text.Trim();
                if (ddlOperador.SelectedValue != "0")
                    lsTituloParametros += " - Operador: " + ddlOperador.SelectedItem;
                if (TxtId.Text.Trim().Length > 0)
                    lsTituloParametros += " - ID Registro: " + TxtId.Text.Trim();
                if (TxtOperacion.Text.Trim().Length > 0)
                    lsTituloParametros += " - Operación: " + TxtOperacion.Text.Trim();

                decimal ldCapacidad = 0;
                StringBuilder lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                dtgMaestro.Columns[0].Visible = false;
                dtgMaestro.EnableViewState = false;
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                lform.Controls.Add(dtgMaestro);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());

                Response.End();
                //lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
                dtgMaestro.Columns[0].Visible = true;
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se Pudo Generar el Excel.!" + ex.Message);
            }
        }

        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("../frm_contenido.aspx");
        }

        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void elimnia_cert()
        {
            lConexion.Abrir();
            SqlDataReader lLector;
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_DelCertificadoGas", null, null, null);
            if (lLector.HasRows)
            {
                while (lLector.Read())
                {
                    string lsArchivo = hdfRutaCert.Value + lLector["nombre_archivo"];
                    try
                    {
                        if (File.Exists(lsArchivo))
                            File.Delete(lsArchivo);
                    }
                    catch (Exception ex)
                    { }
                }
            }

        }
        /// <summary>
        /// Nombre: btnProvisional_Click
        /// Fecha: Abril 12 de 2019
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo Para la generacion de los Certificados Provisionales de Gas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnProvisional_Click(object sender, EventArgs e)
        {
            string lblMensaje = "";
            string lsContratos = "";
            string[] lsArchivo;
            int liNumero = 0;
            try
            {
                foreach (DataGridItem Grilla in dtgMaestro.Items)
                {
                    CheckBox Checkbox = null;
                    Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];

                    if (Checkbox.Checked)
                    {
                        liNumero++;
                        lsContratos += Grilla.Cells[2].Text.Trim() + ",";
                        /// Valiacion para verificar el estado del Contrato en Registro de Contratos
                        if (Grilla.Cells[18].Text.Trim() == "R")
                            lblMensaje += "La operación " + Grilla.Cells[2].Text.Trim() + " ya está registrada<br>";
                        if (Grilla.Cells[18].Text.Trim() == "S" || Grilla.Cells[18].Text.Trim() == "X")
                            lblMensaje += "La operación " + Grilla.Cells[2].Text.Trim() + " está eliminada<br>";
                        if (Grilla.Cells[19].Text.Trim() == "5")
                            lblMensaje += "La operación " + Grilla.Cells[2].Text.Trim() + " es un contrato bilateral<br>";
                        if (Grilla.Cells[19].Text.Trim() == "7")
                            lblMensaje += "La operación " + Grilla.Cells[2].Text.Trim() + " es una negociación de largo plazo<br>";
                    }
                }
                if (liNumero == 0)
                    lblMensaje = "No Seleccionó Registros para Procesar.";
                if (lblMensaje == "")
                {
                    var clImp = new clImpresionCertificado();
                    lsArchivo = clImp.generarCertProv(lsContratos, goInfo).Split('-');
                    if (lsArchivo[0].Trim().Equals("ERROR"))
                        throw new Exception(lsArchivo[1].Trim());

                    ScriptManager.RegisterStartupScript(this, GetType(), "StartupAlert", "window.open('" + lsArchivo[1].Trim() + "');", true);
                }

                if (lblMensaje != "")
                    Toastr.Warning(this, lblMensaje);
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                Toastr.Error(this, ex.Message);
            }
        }
    }
}