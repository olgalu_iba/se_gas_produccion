﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;

public partial class Procesos_frm_Suspender_2 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    String strRutaArchivo;
    SqlDataReader lLector;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString() + "mensaje.txt";
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            /// Obtengo los Datos de la rueda
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda rue, m_estado_gas est", " rue.numero_rueda = " + Session["numero_rueda"].ToString() + " and est.tipo_estado ='R' and rue.estado = est.sigla_estado " );
            if (lLector.HasRows)
            {
                lLector.Read();
                /// Lleno las etiquetas de la pantalla con los datos del Id.
                lblEstado.Text = "Estado Actual: " + lLector["sigla_estado"].ToString() + " - " + lLector["descripcion_estado"].ToString();
                lLector.Close();
                lLector.Dispose();
            }
            else
                lblEstado.Text = "";
            lLector.Dispose();
            lLector.Close();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSuspender_Click(object sender, EventArgs e)
    {
        string oError = "";
        string sMensaje = "";
        double ldPreAct = 0;
        double ldPreRes = 0;

        //if (TxtHora.Text == "")
        //    oError += "Debe digitar la Hora de inicio de la próxima ronda\\n";
        //if (TxtMinutos.Text == "")
        //    oError += "Debe la duración de la próxima ronda\\n";
        if (TxtObservacion.Text == "")
            oError += "Debe digitar las observaciones de la suspension\\n";

        if (oError == "")
        {
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_ind_definitivo", "@P_observaciones" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char, SqlDbType.VarChar };
            string[] lValorParametros = { Session["numero_rueda"].ToString(), ddlDefinitivo.SelectedValue , TxtObservacion.Text }; ///  

            SqlDataReader lLector;
            lConexion.Abrir();
            try
            {
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetSuspRueda", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        oError = lLector["error"].ToString() + "\\n";
                }
                if (oError == "")
                {
                    sMensaje = "la rueda se suspendio exitosamente";
                    File.SetCreationTime(strRutaArchivo, DateTime.Now);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + sMensaje + "');", true);
                    Session["hora"] = "";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                oError = "Error al suspender la rueda. " + ex.Message;
                lConexion.Cerrar();
            }
        }
        if (oError != "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + oError + "');", true);
        }
    }
}
