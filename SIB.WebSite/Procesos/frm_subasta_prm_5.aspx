﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_subasta_prm_5.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_subasta_prm_5" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 
    <table border="0" align="center" cellpadding="0" cellspacing="0" runat="server" id="Table1"
        width="100%">
        <tr>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Agente: </font>
            </td>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Rueda: </font>
            </td>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Horario Rueda:
                </font>
            </td>
            <td align="center" class="td4">
                <asp:Button ID="btnIngresar" Text="Crear Postura" runat="server" OnClientClick="window.open('frm_IngresoPostura_5.aspx?ID=0','IngresoPosturas','width=540,height=500,left=350,top=100,status=no,location=0,menubar=no,toolbar=no,resizable=no');"
                    Visible="false" />
            </td>
            <td align="center" class="td4">
                <asp:Button ID="btnSuspender" Text="Suspender" runat="server" Visible="false" OnClientClick="window.open('frm_Suspender_2.aspx','','width=450,height=250,left=400,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no,scrollbars=1');" />
            </td>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Fecha: </font>
            </td>
        </tr>
        <tr>
            <td align="center" class="td4">
                <font color='#F5DA81' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Label ID="lblAgente" runat="server"></asp:Label>
                </font>
            </td>
            <td align="center" class="td4">
                <asp:DropDownList ID="ddlRueda" runat="server" OnSelectedIndexChanged="ddlRueda_SelectedIndexChanged"
                    AutoPostBack="true">
                </asp:DropDownList>
            </td>
            <td align="center" class="td4">
                <font color='#F5DA81' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Label ID="lblHoraNeg" runat="server"></asp:Label>
                </font>
            </td>
            <td align="center" class="td4">
                <asp:Button ID="btnMensaje" Text="Mensaje" runat="server" OnClientClick="window.open('frm_enviarMensaje_5.aspx','mensaje','width=350,height=250,left=350,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no');" />
            </td>
            <td align="center" class="td4">
                <asp:Button ID="btnReactivar" Text="Reactivar" runat="server" Visible="false" OnClientClick="window.open('frm_reactivar_5.aspx','','width=350,height=250,left=350,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no,scrollbars=1');" />
            </td>
            <td align="center" class="td4">
                <font color='#F5DA81' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Label ID="lblFecha" runat="server"></asp:Label>
                </font>
            </td>
        </tr>
        <tr>
            <td align="center" class="td4">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Label ID="lblPunto" runat="server"></asp:Label></font>
                <asp:DropDownList ID="ddlPunto" runat="server">
                </asp:DropDownList>
            </td>
            <td align="center" class="td4">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Punta: </font>
                <asp:DropDownList ID="ddlPunta" runat="server">
                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                    <asp:ListItem Value="C">Compra</asp:ListItem>
                    <asp:ListItem Value="V">Venta</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="center" class="td4">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Modalidad:
                
                </font>
                <asp:DropDownList ID="ddlModalidad" runat="server">
                </asp:DropDownList>
            </td>
            <td align="center" class="td4">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Periodo: </font>
                <asp:DropDownList ID="ddlPeriodo" runat="server">
                </asp:DropDownList>
            </td>
            <td align="center" class="td4">
                <asp:Button ID="btnBuscar" Text="Buscar" runat="server" OnClick="btnBuscar_Click" />
            </td>
            <td align="center" class="td4">
                <asp:Button ID="btnRestablecer" Text="Restablecer" runat="server" OnClick="btnRestablecer_Click" />
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="HndFechaRueda" runat="server" />
    <asp:HiddenField ID="HndDiaHabil" runat="server" />
    <asp:HiddenField ID="hndEstado" runat="server" />
</asp:Content>
