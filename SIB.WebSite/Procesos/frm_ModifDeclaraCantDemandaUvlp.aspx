﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ModifDeclaraCantDemandaUvlp.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Procesos.frm_ModifDeclaraCantDemandaUvlp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label runat="server">Operador</asp:Label>
                            <asp:DropDownList ID="ddlOperador" runat="server" Width="100%" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label runat="server">Punto Salida</asp:Label>
                            <asp:DropDownList ID="ddlPuntoSalida" runat="server" Width="100%" CssClass="form-control" />
                            <br />
                        </div>
                    </div>
                    <div border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false" width="90%">
                    </div>
                </div>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgSuministro" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                                <Columns>
                                    <%--0--%>
                                    <asp:BoundColumn DataField="desc_punto_salida" HeaderText="Punto Salida" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--1--%>
                                    <asp:BoundColumn DataField="nombre_comprador" HeaderText="Titular Compra" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="200px"></asp:BoundColumn>
                                    <%--2--%>
                                    <asp:BoundColumn DataField="total_demanda" HeaderText="Total Cantidad Demandada (MBTUD)"
                                        ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0}" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--3--%>
                                    <asp:BoundColumn DataField="total_equivalente" HeaderText="Total Equivalente Cantidad Demandada (KPCD)"
                                        ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0}" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--4--%>
                                    <asp:BoundColumn DataField="ind_aprobado" HeaderText="Ind Aprobado" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

        </div>
    </div>


</asp:Content>
