﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_titulos.aspx.cs" Inherits="Procesos_frm_titulos" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="divOferta" runat="server" style="position: absolute; left: 0; top: 0; width: 100%;">
                <table cellpadding="0" cellspacing="0" bgcolor="#000000" align="left">
                    <tr>
                        <td align="center">
                            <asp:DataGrid ID="dtgSubasta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                ItemStyle-CssClass="td2" HeaderStyle-CssClass="th2" runat="server">
                                <Columns>
                                    <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" HeaderStyle-Width="60px">
                                    </asp:BoundColumn>
                                    <%--1--%><asp:BoundColumn DataField="desc_producto" HeaderText="Producto" HeaderStyle-Width="150px">
                                    </asp:BoundColumn>
                                    <%--2--%><asp:BoundColumn DataField="desc_unidad_medida" HeaderText="UM" HeaderStyle-Width="60px">
                                    </asp:BoundColumn>
                                    <%--3--%><asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad" HeaderStyle-Width="120px">
                                    </asp:BoundColumn>
                                    <%--4--%><asp:BoundColumn DataField="desc_punto_entrega" HeaderText="Sitio Entrega"
                                        HeaderStyle-Width="200px"></asp:BoundColumn>
                                    <%--5--%><asp:BoundColumn DataField="fecha_entrega" HeaderText="Periodo Entrega"
                                        HeaderStyle-Width="100px"></asp:BoundColumn>
                                    <%--6--%><asp:BoundColumn DataField="cantidad_total_venta" HeaderText="Cantidad Vta"
                                        HeaderStyle-Width="100px"></asp:BoundColumn>
                                    <%--7--%><asp:BoundColumn DataField="precio_venta" HeaderText="precio Vta" HeaderStyle-Width="80px">
                                    </asp:BoundColumn>
                                    <%--8--%><asp:BoundColumn DataField="cantidad_compra" HeaderText="Cantidad Compra"
                                        HeaderStyle-Width="100px"></asp:BoundColumn>
                                    <%--9--%><asp:BoundColumn DataField="precio_venta" HeaderText="Precio Compra" HeaderStyle-Width="80px">
                                    </asp:BoundColumn>
                                    <%--10--%><asp:TemplateColumn HeaderText="Ofr" HeaderStyle-Width="40px"></asp:TemplateColumn>
                                    <%--11--%><asp:TemplateColumn HeaderText="Mod" HeaderStyle-Width="40px"></asp:TemplateColumn>
                                    <%--12--%><asp:TemplateColumn HeaderText="Elim" HeaderStyle-Width="40px"></asp:TemplateColumn>
                                    <%--13--%><asp:BoundColumn DataField="estado_rueda" Visible="false" HeaderStyle-Width="50px">
                                    </asp:BoundColumn>
                                    <%--14--%><asp:BoundColumn DataField="oferta_ya_realizada" Visible="false" HeaderStyle-Width="50px">
                                    </asp:BoundColumn>
                                    <%--15--%><asp:BoundColumn DataField="numero_rueda" Visible="false" HeaderStyle-Width="50px">
                                    </asp:BoundColumn>
                                    <%--16--%><asp:BoundColumn DataField="max_posturas" Visible="false" HeaderStyle-Width="50px">
                                    </asp:BoundColumn>
                                    <%--17--%><asp:BoundColumn DataField="tipo_operador" Visible="false" HeaderStyle-Width="50px">
                                    </asp:BoundColumn>
                                    <%--18--%><asp:BoundColumn DataField="descripcion" HeaderText="Notas" HeaderStyle-Width="100px">
                                    </asp:BoundColumn>
                                    <%--19--%><asp:TemplateColumn HeaderText="gráf" Visible="false" HeaderStyle-Width="40px"> <%--20180815 BUG230--%>
                                    </asp:TemplateColumn>
                                </Columns>
                                <HeaderStyle CssClass="th2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="11px">
                                </HeaderStyle>
                            </asp:DataGrid>
                            <asp:DataGrid ID="dtgDeclara" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                ItemStyle-CssClass="td2" HeaderStyle-CssClass="th2" runat="server">
                                <Columns>
                                    <%--0--%><asp:BoundColumn DataField="codigo_trasportador" HeaderText="cód tra" HeaderStyle-Width="40px">  <%--20180815 BUG230--%>
                                    </asp:BoundColumn>
                                    <%--1--%><asp:BoundColumn DataField="nombre_trasportador" HeaderText="nombre" HeaderStyle-Width="200px">
                                    </asp:BoundColumn>
                                    <%--2--%><asp:BoundColumn DataField="codigo_punto_ini" HeaderText="Pto Ent" HeaderStyle-Width="70px">
                                    </asp:BoundColumn>
                                    <%--3--%><asp:BoundColumn DataField="desc_punto_ini" HeaderText="desc Pto Ent" HeaderStyle-Width="200px">
                                    </asp:BoundColumn>
                                    <%--4--%><asp:BoundColumn DataField="codigo_punto_fin" HeaderText="pto Fin" HeaderStyle-Width="40px"
                                        Visible="false"></asp:BoundColumn>
                                    <%--5--%><asp:BoundColumn DataField="desc_punto_fin" HeaderText="desc Pto FIn" HeaderStyle-Width="200px"
                                        Visible="false"></asp:BoundColumn>
                                    <%--6--%><asp:BoundColumn DataField="cantidad" HeaderText="cnt dec" HeaderStyle-Width="100px">
                                    </asp:BoundColumn>
                                    <%--7--%><asp:BoundColumn DataField="cantidad_no_disponible" HeaderText="cnt No Disp"
                                        HeaderStyle-Width="100px"></asp:BoundColumn>
                                    <%--8--%><asp:BoundColumn DataField="precio" HeaderText="pre res" HeaderStyle-Width="100px">
                                    </asp:BoundColumn>
                                    <%--9--%><asp:TemplateColumn HeaderText="Mod" HeaderStyle-Width="40px"></asp:TemplateColumn>
                                    <%--10--%><asp:TemplateColumn HeaderText="ELim" HeaderStyle-Width="40px"></asp:TemplateColumn>
                                </Columns>
                                <HeaderStyle CssClass="th2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="11px">
                                </HeaderStyle>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Panel1" runat="server">
    </asp:Panel>
 </asp:Content>