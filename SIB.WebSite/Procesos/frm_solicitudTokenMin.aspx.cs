﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;
using iTextSharp.text;
using iTextSharp.text.pdf;


namespace Procesos
{
    public partial class frm_solicitudTokenMin : Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Solicitud token para firma de Minuta contratos C1 y C2 bimestrales";
        clConexion lConexion = null;
        SqlDataReader lLector;
        DataSet lds = new DataSet();
        SqlDataAdapter lsqldata = new SqlDataAdapter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo;
            lConexion = new clConexion(goInfo);
            buttons.CrearOnclick += btnCrear_Click;
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;

            if (IsPostBack) return;

            //Titulo
            Master.Titulo = "Certificado de Gas";
            //Descripcion
            //Master.DescripcionPagina = "D";

            hdfRutaMinuta.Value = ConfigurationManager.AppSettings["RutaMinuta"];
            elimnia_cert();
            /// Llenar controles del Formulario
            //Botones
            EnumBotones[] botones = { EnumBotones.Crear, EnumBotones.Buscar, EnumBotones.Excel };
            buttons.Inicializar(botones: botones);

            hdfOperador.Value = goInfo.cod_comisionista;
        }
        /// <summary>
        /// Nombre: ChkTodos_CheckedChanged
        /// Fecha: Abril 24 21 de 2012
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para chekear todos los registros
        /// Modificacion:
        /// </summary>
        protected void ChkTodos_CheckedChanged(object sender, EventArgs e)
        {
            //Selecciona o deselecciona todos los items del datagrid segun el checked del control
            CheckBox chk = (CheckBox)(sender);
            foreach (DataGridItem Grilla in dtgMaestro.Items)
            {
                CheckBox Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                if (chk.Checked)
                    Checkbox.Checked = true;
                else
                    Checkbox.Checked = false;
            }
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                var lblMensaje = "";
                hdfBusOper.Value = "N";
                DateTime ldFecha;//20171130 rq026-17
                if (TxtFechaOper.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtFechaOper.Text);
                    }
                    catch (Exception ex)
                    {
                        lblMensaje += "Formato inválido en el campo fecha de operación. <br>";
                    }
                }

                if (TxtFechaOper.Text.Trim().Length <= 0 && TxtOperacion.Text == "")
                    lblMensaje += "Debe ingresar algún parámetro de búsqueda. <br>";

                if (lblMensaje == "")
                {
                    if (TxtOperacion.Text != "")
                        hdfBusOper.Value = "S";
                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    var lComando = new SqlCommand
                    {
                        Connection = lConexion.gObjConexion,
                        CommandTimeout = 3600,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "pa_GetConsMinutaFirma"
                    };
                    lComando.Parameters.Add("@P_fecha_contrato", SqlDbType.VarChar).Value = TxtFechaOper.Text;
                    if (TxtOperacion.Text != "")
                        lComando.Parameters.Add("@P_numero_contrato", SqlDbType.Int).Value = TxtOperacion.Text;
                    lComando.Parameters.Add("@P_operador", SqlDbType.Int).Value = hdfOperador.Value;

                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgMaestro.DataSource = lds;
                    dtgMaestro.DataBind();
                    tblGrilla.Visible = true;
                    lConexion.Cerrar();
                    if (dtgMaestro.Items.Count == 0)
                        Toastr.Warning(this, "No hay contratos registrados para los filtros seleccionados");
                }
                else
                {
                    Toastr.Warning(this, lblMensaje);
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se pudo generar la consulta.! " + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCrear_Click(object sender, EventArgs e)
        {
            var lblMensaje = "";
            string lsContratos = "0";
            int liNumero = 0;
            CheckBox Checkbox;
            try
            {
                foreach (DataGridItem Grilla in dtgMaestro.Items)
                {
                    Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                    if (!Checkbox.Checked) continue;
                    liNumero++;
                    if (Grilla.Cells[11].Text.Trim() == "S")
                        lblMensaje += "El operador ya tiene un token vigente para el Id de registro " + Grilla.Cells[1].Text.Trim() + "<br>";
                    if ( Grilla.Cells[12].Text.Trim() == "S" && Grilla.Cells[13].Text.Trim() == "S" )
                        lblMensaje += "La operación " + Grilla.Cells[2].Text.Trim() + " ya tiene abierto un proceso de firma<br>";
                    lsContratos += "," + Grilla.Cells[2].Text.Trim();
                }
                if (liNumero == 0)
                    lblMensaje += "Debe seleccionar al menos una operación";
                if (lblMensaje == "")
                {
                    string[] lsNombreParametros = { "@P_codigo_operador", "@P_operaciones" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
                    string[] lValorParametros = { hdfOperador.Value, lsContratos };
                    SqlDataReader lLector;
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetTokenFirmaMin", lsNombreParametros, lTipoparametros, lValorParametros);
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                        {
                            if (!File.Exists(hdfRutaMinuta.Value + lLector["nombre_archivo"]))
                                imprime_minuta(hdfRutaMinuta.Value, lLector["operacion"].ToString(), lLector["nombre_archivo"].ToString(), goInfo);
                        }
                    }
                    ScriptManager.RegisterStartupScript(this, GetType(), "StartupAlert", "document.getElementById('dvProg').style.display = 'none';", true);
                    Toastr.Success(this, "Token enviado y almacenado correctamente.!");
                    btnConsultar_Click(null, null);
                }

                if (lblMensaje != "")
                {
                    Toastr.Warning(this, lblMensaje);
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20180126 rq107-16
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            SqlDataAdapter lsqldata = new SqlDataAdapter();
            string lsNombreArchivo = goInfo.Usuario + "InfMinuta" + DateTime.Now + ".xls";
            string lstitulo_informe = "Informe Minuta Contratos bimestrales C1 y C2";
            string lsTituloParametros = "";
            try
            {
                if (TxtFechaOper.Text.Trim().Length > 0)
                    lsTituloParametros += " Fecha Operación: " + TxtFechaOper.Text.Trim();
                if (TxtOperacion.Text.Trim().Length > 0)
                    lsTituloParametros += " - Operación: " + TxtOperacion.Text.Trim();

                decimal ldCapacidad = 0;
                StringBuilder lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                dtgMaestro.Columns[0].Visible = false;
                dtgMaestro.EnableViewState = false;
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);
                lform.Controls.Add(dtgMaestro);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());

                Response.End();
                //lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
                dtgMaestro.Columns[0].Visible = true;
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se Pudo Generar el Excel.!" + ex.Message);
            }
        }

        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("../frm_contenido.aspx");
        }

        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void elimnia_cert()
        {
            lConexion.Abrir();
            SqlDataReader lLector;
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_DelCertificadoGas", null, null, null);
            if (lLector.HasRows)
            {
                while (lLector.Read())
                {
                    string lsArchivo = hdfRutaMinuta.Value + lLector["nombre_archivo"];
                    try
                    {
                        if (File.Exists(lsArchivo))
                            File.Delete(lsArchivo);
                    }
                    catch (Exception ex)
                    { }
                }
            }

        }

        /// <param name="lsFechaRueda"></param>
        /// <param name="ldNumeroOperacionIn"></param>
        /// <param name="ldNumeroOperacionFi"></param>
        /// <param name="lsIndicador"></param>
        /// 20181210 rq046-18 firma digital
        public void imprime_minuta(string lsRuta, string lsOper, string lsMinuta, InfoSessionVO lsInfo)
        {
            clConexion lConexion = new clConexion(lsInfo);
            clConexion lConexion1 = new clConexion(lsInfo);
            SqlDataReader lLector;
            SqlDataReader lLector1;
            string[] lsNombreParametros = { "@P_numero_operacion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int };
            Object[] lValorParametros = { lsOper };

            int[] liAnchoLogo = { 30, 40, 30 };
            int[] liAnchoCabecera = { 40,60};
            int[] liAnchoDetalle = { 14, 14, 16,14,14,14,14 };

            Document loDocument = new Document(PageSize.LETTER, 50, 50, 25, 25);
            string lsdirLogo = "";
            string lsNombreArchivo = "";
            //int liLineas = 0;

            string oRutaPdf = ConfigurationManager.AppSettings["RutaPDF"].ToString();
            string oRutaImg = ConfigurationManager.AppSettings["RutaIMG"].ToString();

            try
            {
                // Se instancia la Conexion
                lConexion.Abrir();
                // Definicion de los Fuentes
                iTextSharp.text.Font FuenteTitulo;
                iTextSharp.text.Font FuenteDato;
                iTextSharp.text.Font FuenteDato1;
                iTextSharp.text.Font FuenteDato2;

                Color clClaro = new Color(135, 206, 250);
                Color clAzul = new Color(50, 84, 135);
                Color clBlanco = new Color(255, 255, 255);

                FuenteTitulo = FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD, clAzul);
                FuenteDato = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, clAzul);
                FuenteDato1 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
                FuenteDato2 = FontFactory.GetFont(FontFactory.HELVETICA, 3, iTextSharp.text.Font.NORMAL);

                string lsArchivo = lsMinuta;
                lsNombreArchivo = lsRuta + lsArchivo;

                lsdirLogo = oRutaImg;
                iTextSharp.text.Image LogoGas = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\bnatop.gif");
                LogoGas.ScalePercent(30);

                iTextSharp.text.Image LogoBna = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\logo_bna1.GIF");
                LogoBna.ScalePercent(45);

                // Creo el Documento PDF
                PdfWriter.GetInstance(loDocument, new FileStream(lsNombreArchivo, FileMode.Create));
                //// Asigno la visualizacion del Numero de Pagina
                //HeaderFooter numeracion_pag = new HeaderFooter(new Phrase("Página: "), true);
                //numeracion_pag.Border = 0;
                //numeracion_pag.Alignment = Element.ALIGN_RIGHT;
                //loDocument.Footer = numeracion_pag;
                loDocument.Open();

                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetMinutaFirma", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {

                    lLector.Read();

                    //liLineas = lLector["mail_v"].ToString().Length;
                    //if (liLineas < lLector["mail_c"].ToString().Length)
                    //    liLineas = lLector["mail_c"].ToString().Length;

                    //liLineas = liLineas / 33;
                    //liLineas += 1;

                    iTextSharp.text.Table TablaLogo = new iTextSharp.text.Table(3, liAnchoLogo);
                    iTextSharp.text.Table TablaCabecera = new iTextSharp.text.Table(2, liAnchoCabecera);
                    iTextSharp.text.Table TablaDetalle = new iTextSharp.text.Table(7, liAnchoDetalle);

                    TablaLogo.Alignment = Element.ALIGN_LEFT;
                    TablaLogo.Offset = 0;
                    TablaLogo.Spacing = 0;
                    TablaLogo.WidthPercentage = 95;
                    TablaLogo.Border = 0;

                    TablaCabecera.Alignment = Element.ALIGN_LEFT;
                    TablaCabecera.Offset = 0;
                    TablaCabecera.Spacing = 0;
                    TablaCabecera.WidthPercentage = 95;
                    TablaCabecera.Border = 0;

                    TablaDetalle.Alignment = Element.ALIGN_LEFT;
                    TablaDetalle.Offset = 0;
                    TablaDetalle.Spacing = 0;
                    TablaDetalle.WidthPercentage = 95;
                    TablaDetalle.Border = 0;

                    Paragraph lpEspacios = new Paragraph(" ", FuenteDato1);

                    // Se Pinta la Cabecera del Certificado

                    Cell CellLogo1 = new Cell();
                    CellLogo1.Border = 0;
                    CellLogo1.BorderWidthLeft = 0;
                    CellLogo1.BorderWidthTop = 0;
                    CellLogo1.BorderWidthRight = 0;
                    CellLogo1.BorderWidthBottom = 0;
                    CellLogo1.AddElement(LogoBna);
                    CellLogo1.VerticalAlignment = Element.ALIGN_BOTTOM;
                    CellLogo1.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaLogo.AddCell(CellLogo1);

                    Cell CellLogo2 = new Cell();
                    CellLogo2.Border = 0;
                    CellLogo2.BorderWidthLeft = 0;
                    CellLogo2.BorderWidthTop = 0;
                    CellLogo2.BorderWidthRight = 0;
                    CellLogo2.BorderWidthBottom = 0;
                    CellLogo2.AddElement(new Paragraph("MINUTA OPERACIÓN " + lsOper, FuenteTitulo));
                    CellLogo2.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellLogo2.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaLogo.AddCell(CellLogo2);

                    Cell CellLogo3 = new Cell();
                    CellLogo3.Border = 0;
                    CellLogo3.BorderWidthLeft = 0;
                    CellLogo3.BorderWidthTop = 0;
                    CellLogo3.BorderWidthRight = 0;
                    CellLogo3.BorderWidthBottom = 0;
                    CellLogo3.AddElement(LogoGas);
                    CellLogo3.VerticalAlignment = Element.ALIGN_BOTTOM;
                    CellLogo3.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaLogo.AddCell(CellLogo3);

                    loDocument.Add(TablaLogo);

                    loDocument.Add(lpEspacios);

                    Cell CellCab1 = new Cell();
                    CellCab1.Border = 1;
                    CellCab1.BorderWidthLeft = 1;
                    CellCab1.BorderWidthTop = 1;
                    CellCab1.BorderWidthRight = 1;
                    CellCab1.BorderWidthBottom = 1;
                    CellCab1.AddElement(new Paragraph(" I. VENDEDOR:", FuenteDato));
                    CellCab1.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab1.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab1);

                    Cell CellCab2 = new Cell();
                    CellCab2.BorderWidthLeft = 0;
                    CellCab2.BorderWidthTop = 1;
                    CellCab2.BorderWidthRight = 1;
                    CellCab2.BorderWidthBottom = 1;
                    CellCab2.AddElement(new Paragraph(" " + lLector["vendedor"].ToString(), FuenteDato1));
                    CellCab2.AddElement(new Paragraph(" " + lLector["rol_venta"].ToString(), FuenteDato1));
                    CellCab2.AddElement(new Paragraph("-", FuenteDato2));
                    CellCab2.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab2.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab2);



                    Cell CellCab3 = new Cell();
                    CellCab3.Border = 0;
                    CellCab3.BorderWidthLeft = 1;
                    CellCab3.BorderWidthTop = 0;
                    CellCab3.BorderWidthRight = 1;
                    CellCab3.BorderWidthBottom = 1;
                    CellCab3.AddElement(new Paragraph(" II. COMPRADOR:", FuenteDato));
                    CellCab3.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab3.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab3);

                    Cell CellCab4 = new Cell();
                    CellCab4.Border = 0;
                    CellCab4.BorderWidthLeft = 0;
                    CellCab4.BorderWidthTop = 0;
                    CellCab4.BorderWidthRight = 1;
                    CellCab4.BorderWidthBottom = 1;
                    CellCab4.AddElement(new Paragraph(" " + lLector["comprador"].ToString(), FuenteDato1));
                    CellCab4.AddElement(new Paragraph(" " + lLector["rol_compra"].ToString(), FuenteDato1));
                    CellCab4.AddElement(new Paragraph("-", FuenteDato2));
                    CellCab4.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab4.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab4);

                    Cell CellCab4a = new Cell();
                    CellCab4a.Border = 0;
                    CellCab4a.BorderWidthLeft = 1;
                    CellCab4a.BorderWidthTop = 0;
                    CellCab4a.BorderWidthRight = 1;
                    CellCab4a.BorderWidthBottom = 1;
                    CellCab4a.AddElement(new Paragraph(" III. PUNTO DE ENTREGA:", FuenteDato));
                    CellCab4a.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab4a.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab4a);

                    Cell CellCab5 = new Cell();
                    CellCab5.Border = 1;
                    CellCab5.BorderWidthLeft = 0;
                    CellCab5.BorderWidthTop = 0;
                    CellCab5.BorderWidthRight = 1;
                    CellCab5.BorderWidthBottom = 1;
                    CellCab5.AddElement(new Paragraph(" " + lLector["desc_punto"].ToString(), FuenteDato1));
                    CellCab5.AddElement(new Paragraph("-", FuenteDato2));
                    CellCab5.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab5.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab5);

                    Cell CellCab6 = new Cell();
                    CellCab6.Border = 1;
                    CellCab6.BorderWidthLeft = 1;
                    CellCab6.BorderWidthTop = 0;
                    CellCab6.BorderWidthRight = 1;
                    CellCab6.BorderWidthBottom = 1;
                    CellCab6.AddElement(new Paragraph(" IV. CANTIDAD DE ENERGÍA CONTRATADA:", FuenteDato));
                    CellCab6.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab6.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab6);

                    Cell CellCab7 = new Cell();
                    CellCab7.Border = 1;
                    CellCab7.BorderWidthLeft = 0;
                    CellCab7.BorderWidthTop = 0;
                    CellCab7.BorderWidthRight = 1;
                    CellCab7.BorderWidthBottom = 1;
                    CellCab7.AddElement(new Paragraph(" " + lLector["cantidad"].ToString() + " MBTUD", FuenteDato1));
                    CellCab7.AddElement(new Paragraph("-", FuenteDato2));
                    CellCab7.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab7.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab7);

                    Cell CellCab8 = new Cell();
                    CellCab8.Border = 1;
                    CellCab8.BorderWidthLeft = 1;
                    CellCab8.BorderWidthTop = 0;
                    CellCab8.BorderWidthRight = 1;
                    CellCab8.BorderWidthBottom = 1;
                    CellCab8.AddElement(new Paragraph(" V. PRECIO", FuenteDato));
                    CellCab8.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab8.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab8);

                    Cell CellCab9 = new Cell();
                    CellCab9.Border = 1;
                    CellCab9.BorderWidthLeft = 0;
                    CellCab9.BorderWidthTop = 0;
                    CellCab9.BorderWidthRight = 1;
                    CellCab9.BorderWidthBottom = 1;
                    CellCab9.AddElement(new Paragraph(" " + lLector["precio"].ToString() + "USD/MBTU", FuenteDato1));
                    CellCab9.AddElement(new Paragraph("-", FuenteDato2));
                    CellCab9.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab9.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab9);

                    Cell CellCab10 = new Cell();
                    CellCab10.Border = 0;
                    CellCab10.BorderWidthLeft = 1;
                    CellCab10.BorderWidthTop = 0;
                    CellCab10.BorderWidthRight = 1;
                    CellCab10.BorderWidthBottom = 1;
                    CellCab10.AddElement(new Paragraph(" VI. GARANTÍA DE CUMPLIMIENTO", FuenteDato));
                    CellCab10.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab10.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab10);

                    Cell CellCab11 = new Cell();
                    CellCab11.Border = 0;
                    CellCab11.BorderWidthLeft = 0;
                    CellCab11.BorderWidthTop = 0;
                    CellCab11.BorderWidthRight = 1;
                    CellCab11.BorderWidthBottom = 1;
                    if (lLector["garantia_1"].ToString() != "")
                        CellCab11.AddElement(new Paragraph(" " + lLector["garantia_1"].ToString(), FuenteDato1));
                    if (lLector["garantia_2"].ToString() != "")
                        CellCab11.AddElement(new Paragraph(" " + lLector["garantia_2"].ToString(), FuenteDato1));
                    if (lLector["garantia_3"].ToString() != "")
                        CellCab11.AddElement(new Paragraph(" " + lLector["garantia_3"].ToString(), FuenteDato1));
                    if (lLector["garantia_4"].ToString() != "")
                        CellCab11.AddElement(new Paragraph(" " + lLector["garantia_4"].ToString(), FuenteDato1));
                    CellCab11.AddElement(new Paragraph("-", FuenteDato2));
                    CellCab11.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab11.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab11);

                    Cell CellCab12 = new Cell();
                    CellCab12.Border = 0;
                    CellCab12.BorderWidthLeft = 1;
                    CellCab12.BorderWidthTop = 0;
                    CellCab12.BorderWidthRight = 1;
                    CellCab12.BorderWidthBottom = 1;
                    CellCab12.AddElement(new Paragraph(" VII. VALOR DE LA GARANTÍA:", FuenteDato));
                    CellCab12.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab12.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab12);

                    Cell CellCab13 = new Cell();
                    CellCab13.Border = 0;
                    CellCab13.BorderWidthLeft = 0;
                    CellCab13.BorderWidthTop = 0;
                    CellCab13.BorderWidthRight = 1;
                    CellCab13.BorderWidthBottom = 1;
                    CellCab13.AddElement(new Paragraph(" " + lLector["valor_garantia"].ToString(), FuenteDato1));
                    CellCab13.AddElement(new Paragraph("-", FuenteDato2));
                    CellCab13.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab13.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab13);

                    Cell CellCab14 = new Cell();
                    CellCab14.Border = 0;
                    CellCab14.BorderWidthLeft = 1;
                    CellCab14.BorderWidthTop = 0;
                    CellCab14.BorderWidthRight = 1;
                    CellCab14.BorderWidthBottom = 1;
                    CellCab14.AddElement(new Paragraph(" VIII. PLAZO DE ENTREGA", FuenteDato));
                    CellCab14.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab14.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab14);

                    Cell CellCab15 = new Cell();
                    CellCab15.Border = 0;
                    CellCab15.BorderWidthLeft = 0;
                    CellCab15.BorderWidthTop = 0;
                    CellCab15.BorderWidthRight = 1;
                    CellCab15.BorderWidthBottom = 1;
                    CellCab15.AddElement(new Paragraph(" " + lLector["periodo"].ToString(), FuenteDato1));
                    CellCab15.AddElement(new Paragraph("-", FuenteDato2));
                    CellCab15.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab15.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab15);

                    Cell CellCab16 = new Cell();
                    CellCab16.Border = 0;
                    CellCab16.BorderWidthLeft = 1;
                    CellCab16.BorderWidthTop = 0;
                    CellCab16.BorderWidthRight = 1;
                    CellCab16.BorderWidthBottom = 1;
                    CellCab16.AddElement(new Paragraph(" IX. FECHA DE INICIO", FuenteDato));
                    CellCab16.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab16.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab16);

                    Cell CellCab17 = new Cell();
                    CellCab17.Border = 0;
                    CellCab17.BorderWidthLeft = 0;
                    CellCab17.BorderWidthTop = 0;
                    CellCab17.BorderWidthRight = 1;
                    CellCab17.BorderWidthBottom = 1;
                    CellCab17.AddElement(new Paragraph(" " + lLector["fecha_inicial"].ToString(), FuenteDato1));
                    CellCab17.AddElement(new Paragraph("-", FuenteDato2));
                    CellCab17.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab17.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab17);

                    Cell CellCab18 = new Cell();
                    CellCab18.Border = 0;
                    CellCab18.BorderWidthLeft = 1;
                    CellCab18.BorderWidthTop = 0;
                    CellCab18.BorderWidthRight = 1;
                    CellCab18.BorderWidthBottom = 1;
                    CellCab18.AddElement(new Paragraph(" X. FECHA DE TERMINACIÓN", FuenteDato));
                    CellCab18.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab18.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab18);

                    Cell CellCab19 = new Cell();
                    CellCab19.Border = 0;
                    CellCab19.BorderWidthLeft = 0;
                    CellCab19.BorderWidthTop = 0;
                    CellCab19.BorderWidthRight = 1;
                    CellCab19.BorderWidthBottom = 1;
                    CellCab19.AddElement(new Paragraph(" " + lLector["fecha_final"].ToString(), FuenteDato1));
                    CellCab19.AddElement(new Paragraph("-", FuenteDato2));
                    CellCab19.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab19.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab19);

                    Cell CellCab20 = new Cell();
                    CellCab20.Border = 0;
                    CellCab20.BorderWidthLeft = 1;
                    CellCab20.BorderWidthTop = 0;
                    CellCab20.BorderWidthRight = 1;
                    CellCab20.BorderWidthBottom = 1;
                    CellCab20.AddElement(new Paragraph(" XI. FECHA DE SUSCRIPCIÓN", FuenteDato));
                    CellCab20.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab20.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab20);

                    Cell CellCab21 = new Cell();
                    CellCab21.Border = 0;
                    CellCab21.BorderWidthLeft = 0;
                    CellCab21.BorderWidthTop = 0;
                    CellCab21.BorderWidthRight = 1;
                    CellCab21.BorderWidthBottom = 1;
                    CellCab21.AddElement(new Paragraph(" " + lLector["fecha_suscripcion"].ToString(), FuenteDato1));
                    CellCab21.AddElement(new Paragraph("-", FuenteDato2));
                    CellCab21.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab21.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab21);
                    
                    Cell CellCab20A = new Cell();
                    CellCab20A.Colspan = 2;
                    CellCab20A.Border = 0;
                    CellCab20A.BorderWidthLeft = 1;
                    CellCab20A.BorderWidthTop = 0;
                    CellCab20A.BorderWidthRight = 1;
                    CellCab20A.BorderWidthBottom = 1;
                    CellCab20A.AddElement(new Paragraph(" XII. DESTINO", FuenteDato));
                    CellCab20A.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab20A.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab20A);

                    loDocument.Add(TablaCabecera);

                    lConexion1.Abrir();
                    lLector1 = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion1.gObjConexion, "pa_GetRegistroContratosUsuMin", lsNombreParametros, lTipoparametros, lValorParametros);
                    if (lLector1.HasRows)
                    {
                        Cell CellCabDet01 = new Cell();
                        CellCabDet01.Border = 0;
                        CellCabDet01.BorderWidthLeft = 1;
                        CellCabDet01.BorderWidthTop = 0;
                        CellCabDet01.BorderWidthRight = 0;
                        CellCabDet01.BorderWidthBottom = 1;
                        CellCabDet01.AddElement(new Paragraph(" Tipo Demanda", FuenteDato));
                        CellCabDet01.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellCabDet01.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDetalle.AddCell(CellCabDet01);

                        Cell CellCabDet02 = new Cell();
                        CellCabDet02.Border = 0;
                        CellCabDet02.BorderWidthLeft = 1;
                        CellCabDet02.BorderWidthTop = 0;
                        CellCabDet02.BorderWidthRight = 0;
                        CellCabDet02.BorderWidthBottom = 1;
                        CellCabDet02.AddElement(new Paragraph(" Nit Usuario", FuenteDato));
                        CellCabDet02.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellCabDet02.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDetalle.AddCell(CellCabDet02);

                        Cell CellCabDet03 = new Cell();
                        CellCabDet03.Border = 0;
                        CellCabDet03.BorderWidthLeft = 1;
                        CellCabDet03.BorderWidthTop = 0;
                        CellCabDet03.BorderWidthRight = 0;
                        CellCabDet03.BorderWidthBottom = 1;
                        CellCabDet03.AddElement(new Paragraph(" Nombre usuario", FuenteDato));
                        CellCabDet03.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellCabDet03.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDetalle.AddCell(CellCabDet03);

                        Cell CellCabDet04 = new Cell();
                        CellCabDet04.Border = 0;
                        CellCabDet04.BorderWidthLeft = 1;
                        CellCabDet04.BorderWidthTop = 0;
                        CellCabDet04.BorderWidthRight = 0;
                        CellCabDet04.BorderWidthBottom = 1;
                        CellCabDet04.AddElement(new Paragraph(" Sector Consumo", FuenteDato));
                        CellCabDet04.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellCabDet04.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDetalle.AddCell(CellCabDet04);

                        Cell CellCabDet05 = new Cell();
                        CellCabDet05.Border = 0;
                        CellCabDet05.BorderWidthLeft = 1;
                        CellCabDet05.BorderWidthTop = 0;
                        CellCabDet05.BorderWidthRight = 0;
                        CellCabDet05.BorderWidthBottom = 1;
                        CellCabDet05.AddElement(new Paragraph(" Punto Salida", FuenteDato));
                        CellCabDet05.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellCabDet05.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDetalle.AddCell(CellCabDet05);

                        Cell CellCabDet06 = new Cell();
                        CellCabDet06.Border = 0;
                        CellCabDet06.BorderWidthLeft = 1;
                        CellCabDet06.BorderWidthTop = 0;
                        CellCabDet06.BorderWidthRight = 0;
                        CellCabDet06.BorderWidthBottom = 1;
                        CellCabDet06.AddElement(new Paragraph(" Cantidad Contratada", FuenteDato));
                        CellCabDet06.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellCabDet06.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDetalle.AddCell(CellCabDet06);

                        Cell CellCabDet07 = new Cell();
                        CellCabDet07.Border = 0;
                        CellCabDet07.BorderWidthLeft = 1;
                        CellCabDet07.BorderWidthTop = 0;
                        CellCabDet07.BorderWidthRight = 1;
                        CellCabDet07.BorderWidthBottom = 1;
                        CellCabDet07.AddElement(new Paragraph(" Equivalente", FuenteDato));
                        CellCabDet07.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellCabDet07.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDetalle.AddCell(CellCabDet07);

                        while (lLector1.Read())
                        {
                            Cell CellDet01 = new Cell();
                            CellDet01.Border = 0;
                            CellDet01.BorderWidthLeft = 1;
                            CellDet01.BorderWidthTop = 0;
                            CellDet01.BorderWidthRight = 0;
                            CellDet01.BorderWidthBottom = 1;
                            CellDet01.AddElement(new Paragraph(" " + lLector1["tipo_demanda"].ToString(), FuenteDato1));
                            CellDet01.AddElement(new Paragraph("-", FuenteDato2));
                            CellDet01.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDet01.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDetalle.AddCell(CellDet01);

                            Cell CellDet02 = new Cell();
                            CellDet02.Border = 0;
                            CellDet02.BorderWidthLeft = 1;
                            CellDet02.BorderWidthTop = 0;
                            CellDet02.BorderWidthRight = 0;
                            CellDet02.BorderWidthBottom = 1;
                            CellDet02.AddElement(new Paragraph(" " + lLector1["no_identificacion_usr"].ToString(), FuenteDato1));
                            CellDet02.AddElement(new Paragraph("-", FuenteDato2));
                            CellDet02.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDet02.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDetalle.AddCell(CellDet02);

                            Cell CellDet03 = new Cell();
                            CellDet03.Border = 0;
                            CellDet03.BorderWidthLeft = 1;
                            CellDet03.BorderWidthTop = 0;
                            CellDet03.BorderWidthRight = 0;
                            CellDet03.BorderWidthBottom = 1;
                            CellDet03.AddElement(new Paragraph(" " + lLector1["nombre_usuario"].ToString(), FuenteDato1));
                            CellDet03.AddElement(new Paragraph("-", FuenteDato2));
                            CellDet03.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDet03.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDetalle.AddCell(CellDet03);

                            Cell CellDet04 = new Cell();
                            CellDet04.Border = 0;
                            CellDet04.BorderWidthLeft = 1;
                            CellDet04.BorderWidthTop = 0;
                            CellDet04.BorderWidthRight = 0;
                            CellDet04.BorderWidthBottom = 1;
                            CellDet04.AddElement(new Paragraph(" " + lLector1["sector_consumo"].ToString(), FuenteDato1));
                            CellDet04.AddElement(new Paragraph("-", FuenteDato2));
                            CellDet04.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDet04.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDetalle.AddCell(CellDet04);

                            Cell CellDet05 = new Cell();
                            CellDet05.Border = 0;
                            CellDet05.BorderWidthLeft = 1;
                            CellDet05.BorderWidthTop = 0;
                            CellDet05.BorderWidthRight = 0;
                            CellDet05.BorderWidthBottom = 1;
                            CellDet05.AddElement(new Paragraph(" " + lLector1["punto_salida"].ToString(), FuenteDato1));
                            CellDet05.AddElement(new Paragraph("-", FuenteDato2));
                            CellDet05.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDet05.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDetalle.AddCell(CellDet05);

                            Cell CellDet06 = new Cell();
                            CellDet06.Border = 0;
                            CellDet06.BorderWidthLeft = 1;
                            CellDet06.BorderWidthTop = 0;
                            CellDet06.BorderWidthRight = 0;
                            CellDet06.BorderWidthBottom = 1;
                            CellDet06.AddElement(new Paragraph(" " + lLector1["cantidad_contratada"].ToString(), FuenteDato1));
                            CellDet06.AddElement(new Paragraph("-", FuenteDato2));
                            CellDet06.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDet06.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDetalle.AddCell(CellDet06);

                            Cell CellDet07 = new Cell();
                            CellDet07.Border = 0;
                            CellDet07.BorderWidthLeft = 1;
                            CellDet07.BorderWidthTop = 0;
                            CellDet07.BorderWidthRight = 1;
                            CellDet07.BorderWidthBottom = 1;
                            CellDet07.AddElement(new Paragraph(" " + lLector1["equivalente_kpcd"].ToString(), FuenteDato1));
                            CellDet07.AddElement(new Paragraph("-", FuenteDato2));
                            CellDet07.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDet07.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDetalle.AddCell(CellDet07);
                        }
                        loDocument.Add(TablaDetalle);
                    }
                    lLector1.Close();
                    lLector1.Dispose();
                    lConexion1.Cerrar();
                    TablaCabecera.DeleteAllRows();

                    Cell CellCab22 = new Cell();
                    CellCab22.Border = 0;
                    CellCab22.BorderWidthLeft = 1;
                    CellCab22.BorderWidthTop = 0;
                    CellCab22.BorderWidthRight = 1;
                    CellCab22.BorderWidthBottom = 1;
                    CellCab22.AddElement(new Paragraph(" XIII. CUENTA PARA PAGOS ", FuenteDato));
                    CellCab22.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab22.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab22);

                    Cell CellCab23 = new Cell();
                    CellCab23.Border = 0;
                    CellCab23.BorderWidthLeft = 0;
                    CellCab23.BorderWidthTop = 0;
                    CellCab23.BorderWidthRight = 1;
                    CellCab23.BorderWidthBottom = 1;
                    CellCab23.AddElement(new Paragraph(" " + lLector["cuenta_pago"].ToString(), FuenteDato1));
                    CellCab23.AddElement(new Paragraph("-", FuenteDato2));
                    CellCab23.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab23.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab23);

                    Cell CellCab24 = new Cell();
                    CellCab24.Border = 0;
                    CellCab24.BorderWidthLeft = 1;
                    CellCab24.BorderWidthTop = 0;
                    CellCab24.BorderWidthRight = 1;
                    CellCab24.BorderWidthBottom = 1;
                    CellCab24.AddElement(new Paragraph(" XIV. NOTIFICACIONES", FuenteDato));
                    CellCab24.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab24.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab24);

                    Cell CellCab25 = new Cell();
                    CellCab25.Border = 0;
                    CellCab25.BorderWidthLeft = 0;
                    CellCab25.BorderWidthTop = 0;
                    CellCab25.BorderWidthRight = 1;
                    CellCab25.BorderWidthBottom = 1;
                    CellCab25.AddElement(new Paragraph(" " + lLector["notifica_v"].ToString(), FuenteDato1));
                    CellCab25.AddElement(new Paragraph(" " + lLector["atencion_v"].ToString(), FuenteDato1));
                    CellCab25.AddElement(new Paragraph(" " + lLector["direccion_v"].ToString(), FuenteDato1));
                    CellCab25.AddElement(new Paragraph(" " + lLector["correo_v"].ToString(), FuenteDato1));
                    CellCab25.AddElement(new Paragraph(" " + lLector["telefono_v"].ToString(), FuenteDato1));
                    CellCab25.AddElement(new Paragraph(" " + lLector["fax_v"].ToString(), FuenteDato1));
                    CellCab25.AddElement(new Paragraph(" " + lLector["correo_nom_c"].ToString(), FuenteDato1));
                    CellCab25.AddElement(new Paragraph("-", FuenteDato2));
                    CellCab25.AddElement(new Paragraph("-", FuenteDato2));
                    CellCab25.AddElement(new Paragraph(" " + lLector["notifica_c"].ToString(), FuenteDato1));
                    CellCab25.AddElement(new Paragraph(" " + lLector["atencion_c"].ToString(), FuenteDato1));
                    CellCab25.AddElement(new Paragraph(" " + lLector["direccion_c"].ToString(), FuenteDato1));
                    CellCab25.AddElement(new Paragraph(" " + lLector["correo_c"].ToString(), FuenteDato1));
                    CellCab25.AddElement(new Paragraph(" " + lLector["telefono_c"].ToString(), FuenteDato1));
                    CellCab25.AddElement(new Paragraph(" " + lLector["fax_c"].ToString(), FuenteDato1));
                    CellCab25.AddElement(new Paragraph(" " + lLector["correo_nom_c"].ToString(), FuenteDato1));
                    CellCab25.AddElement(new Paragraph("-", FuenteDato2));
                    CellCab25.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab25.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab25);

                    Cell CellCab26 = new Cell();
                    CellCab26.Border = 0;
                    CellCab26.BorderWidthLeft = 1;
                    CellCab26.BorderWidthTop = 0;
                    CellCab26.BorderWidthRight = 1;
                    CellCab26.BorderWidthBottom = 1;
                    CellCab26.AddElement(new Paragraph(" XV. RESOLUCIÓN DE CONFLICTOS", FuenteDato));
                    CellCab26.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab26.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab26);

                    Cell CellCab27 = new Cell();
                    CellCab27.Border = 0;
                    CellCab27.BorderWidthLeft = 0;
                    CellCab27.BorderWidthTop = 0;
                    CellCab27.BorderWidthRight = 1;
                    CellCab27.BorderWidthBottom = 1;
                    if (lLector["conflicto_1"].ToString() != "")
                        CellCab27.AddElement(new Paragraph(" " + lLector["conflicto_1"].ToString(), FuenteDato1));
                    if (lLector["conflicto_2"].ToString() != "")
                        CellCab27.AddElement(new Paragraph(" " + lLector["conflicto_2"].ToString(), FuenteDato1));
                    CellCab27.AddElement(new Paragraph("-", FuenteDato2));
                    CellCab27.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab27.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab27);

                    Cell CellCab28 = new Cell();
                    CellCab28.Border = 0;
                    CellCab28.BorderWidthLeft = 1;
                    CellCab28.BorderWidthTop = 0;
                    CellCab28.BorderWidthRight = 1;
                    CellCab28.BorderWidthBottom = 1;
                    CellCab28.AddElement(new Paragraph(" XVI. MONEDA DE PAGO", FuenteDato));
                    CellCab28.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab28.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab28);

                    Cell CellCab29 = new Cell();
                    CellCab29.Border = 0;
                    CellCab29.BorderWidthLeft = 0;
                    CellCab29.BorderWidthTop = 0;
                    CellCab29.BorderWidthRight = 1;
                    CellCab29.BorderWidthBottom = 1;
                    if (lLector["moneda_1"].ToString() != "")
                        CellCab29.AddElement(new Paragraph(" " + lLector["moneda_1"].ToString(), FuenteDato1));
                    if (lLector["moneda_2"].ToString() != "")
                        CellCab29.AddElement(new Paragraph(" " + lLector["moneda_2"].ToString(), FuenteDato1));
                    CellCab29.AddElement(new Paragraph("-", FuenteDato2));
                    CellCab29.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellCab29.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaCabecera.AddCell(CellCab29);


                    loDocument.Add(TablaCabecera);

                    //loDocument.Add(lpEspacios);
                    //loDocument.Add(lpEspacios);
                    //loDocument.Add(lpEspacios);
                    //loDocument.Add(lpEspacios);
                    

                }
                lLector.Close();
                lLector.Dispose();
                loDocument.Close();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lConexion.Cerrar();
                loDocument.Close();
            }
        }

    }

}
