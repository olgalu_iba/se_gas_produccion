﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ConsultaDecInfPerTransSb.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Procesos.frm_ConsultaDecInfPerTransSb" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="TxtFechaRueda">Fecha Rueda</label>
                            <asp:TextBox ID="TxtFechaRueda" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control datepicker" ClientIDMode="Static" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="TxtNoRueda">Número Rueda</label>
                            <asp:TextBox ID="TxtNoRueda" runat="server" Width="100%" CssClass="form-control" MaxLength="5"></asp:TextBox>
                            <br />
                            <asp:Button ID="btnConsultar" runat="server" Text="Consultar" CssClass="btn btn-success" Visible="false" OnClick="btnConsultar_Click" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="DdlOperador">Operador</label>
                            <asp:DropDownList ID="DdlOperador" runat="server" Width="100%" CssClass="form-control">
                            </asp:DropDownList>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="detalle" />
                            <asp:Label ID="Label2" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgMaestro" runat="server" Visible="False" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered"
                                PagerStyle-HorizontalAlign="Center">
                                <Columns>
                                    <asp:BoundColumn DataField="fecha_declaracion" HeaderText="Fecha Declaración" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="tipo_declaracion" HeaderText="Tipo Declaración" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_rueda" HeaderText="Fecha Subasta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="bimestre" HeaderText="Bimestre a Subastar" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Vendedor" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_fuente" HeaderText="Código Campo / Fuente" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_campo_fuente" HeaderText="Campo Producción / Fuente"
                                        ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="punto_entrega" HeaderText="Punto de Entrega" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,###,##0}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="precio_reserva" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,##0.00}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="precio_delta" HeaderText="Delta Precio" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,##0.00}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="ptdvf" HeaderText="PTDVF" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,###,##0}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="cnt_contratada" HeaderText="cnt contratada" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,###,##0}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="dif_neg_ptdvf" HeaderText="Diferencia Nego. PTDVF" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,###,##0}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="cantidad_adjudicada" HeaderText="Cantidad Adjudicada"
                                        ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0}"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
