﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using SIB.BASE;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_DeclaracionVentaSMP : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Declaración Posturas de Vendedores Subasta Suministro sin Interrupciones";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    string sRutaArc = ConfigurationManager.AppSettings["rutaCarga"].ToString();

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            try
            {
                lsTitulo = "";
                hndFecha.Value = DateTime.Now.ToShortDateString().Substring(6, 4) + "-" + DateTime.Now.ToShortDateString().Substring(3, 2) + "-" + DateTime.Now.ToShortDateString().Substring(0, 2);
                // Carga informacion de combos
                //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
                if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
                {
                    lsIndica = this.Request.QueryString["lsIndica"].ToString();
                }
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlBusModalidad, "m_modalidad_contractual mod, m_caracteristica_sub carM", " mod.codigo_modalidad = carM.codigo_caracteristica and carM.tipo_caracteristica ='M' and carM.codigo_tipo_subasta = 1 and carM.estado ='A' and mod.estado ='A' order by descripcion", 0, 1);
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda", " codigo_tipo_subasta = 1 And estado = 'C' ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    hdfNoRueda.Value = lLector["numero_rueda"].ToString();
                    hdfTipoRueda.Value = lLector["codigo_tipo_rueda"].ToString();
                }
                else
                {
                    hdfNoRueda.Value = "0";
                    hdfTipoRueda.Value = "0";
                }
                lLector.Close();
                lLector.Dispose();

                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_fases_subasta", " codigo_fases_subasta = 2 ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsTitulo += "Declaración Posturas de Vendedores Subasta Suministro sin Interrupciones - FASE: " + lLector["descripcion"].ToString();
                    hdfDescFase.Value = lLector["descripcion"].ToString();
                }
                lLector.Close();
                lLector.Dispose();

                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + goInfo.cod_comisionista);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lblOperador.Text = goInfo.cod_comisionista + " - " + lLector["razon_social"].ToString();
                }
                lLector.Close();
                lLector.Dispose();

                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_operador_rueda_hab", " codigo_operador = " + goInfo.cod_comisionista + " And numero_rueda = " + hdfNoRueda.Value + " And codigo_fases_subasta = 1");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    hdfCodModalidad.Value = lLector["codigo_modalidad"].ToString();
                }
                else
                {
                    hdfCodModalidad.Value = "0";
                }
                lLector.Close();
                lLector.Dispose();


                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    hdfValorVigMin.Value = lLector["valor_vig_ptdv_cidv_min_minas"].ToString();
                    hdfCantMin.Value = lLector["cantidad_minima_negociacion"].ToString();
                    hdfIncrementos.Value = lLector["valor_incremento_ofertas"].ToString();
                }
                lLector.Close();
                lLector.Dispose();

                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador ope, m_tipos_operador tpo, m_operador_subasta opeS", " ope.codigo_operador =" + goInfo.cod_comisionista + " and ope.tipo_operador = tpo.sigla and tpo.codigo_tipo_operador = opeS.codigo_tipo_operador and opeS.punta = 'V' And opeS.codigo_tipo_subasta = 1 ");
                if (!lLector.HasRows)
                {
                    Toastr.Warning(this, "El tipo de operador no está autorizado para " + lsTitulo + ". <br>");
                    tblCaptura.Visible = false;
                    tblBuscar.Visible = false;
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                }
                else
                {
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    if (lsIndica == null || lsIndica == "" || lsIndica == "L")
                    {
                        Listar();
                    }
                    else if (lsIndica == "B")
                    {
                        Buscar();
                    }
                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Problemas en la Carga de la Pagina. " + ex.Message.ToString());
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "t_operador_rueda_hab");
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgConsulta.Columns[11].Visible = (Boolean)permisos["INSERT"];
        dtgConsulta.Columns[12].Visible = (Boolean)permisos["UPDATE"];
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar, string lsIndica)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                tblCaptura.Visible = true;
                tblgrilla.Visible = false;
                tblBuscar.Visible = false;
                lblTitulo.Text = "Modificar " + lsTitulo;
                TxtCantidad.Focus();
                // Carga informacion de combos
                if (lsIndica == "1")
                {
                    imbCrear.Visible = true;
                    imbActualiza.Visible = false;
                }
                else
                {
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_posturas_venta", " numero_id = " + hdfNoId.Value + " And codigo_operador =  " + goInfo.cod_comisionista);
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        TxtCantidad.Text = lLector["cantidad_postura"].ToString();
                        TxtPrecioReserva.Text = lLector["precio"].ToString();
                        hdfNoPostura.Value = lLector["numero_postura"].ToString();
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    imbCrear.Visible = false;
                    imbActualiza.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, ex.Message);

            }
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_tipo_rueda", "@P_numero_rueda", "@P_fecha_rueda", "@P_estado", "@P_modalidad", "@P_codigo_operador" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Char, SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { hdfTipoRueda.Value, hdfNoRueda.Value, "", "0", hdfCodModalidad.Value, goInfo.cod_comisionista };

        try
        {
            if (ddlBusModalidad.SelectedValue != "0")
                lValorParametros[4] = ddlBusModalidad.SelectedValue;

            lConexion.Abrir();
            dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetConsIdSMP", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgConsulta.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        DateTime ldFecha = DateTime.Now;
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            try
            {
                lblModalidad.Text = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[5].Text;
                lblSubasta.Text = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[2].Text;
                hdfNoId.Value = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text;

                if (!VerificarExistencia("t_rueda rue, t_usuario_hab usrH", " rue.numero_rueda= " + this.dtgConsulta.Items[e.Item.ItemIndex].Cells[1].Text + " and rue.codigo_tipo_rueda = usrH.codigo_tipo_rueda And usrH.codigo_usuario = " + goInfo.codigo_usuario + " And usrH.estado = 'A' And usrH.fecha_inicial <='" + hndFecha.Value + "' and usrH.fecha_final >= '" + hndFecha.Value + "'"))
                    lblMensaje.Text += " El Usuario no Ha sido Autorizado para el tipo de rueda.<br>";
                else
                {
                    if (!VerificarExistencia("t_operador_rueda_hab", " numero_rueda= " + this.dtgConsulta.Items[e.Item.ItemIndex].Cells[1].Text + " and codigo_operador=" + goInfo.cod_comisionista + " And codigo_fases_subasta = 1  And estado = 'A' "))
                        lblMensaje.Text += " La Participacion de los Vendedores para el Operador no Ha sido Aprobada.<br>";
                    else
                    {
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_posturas_venta", " numero_id = " + hdfNoId.Value + " And codigo_operador =  " + goInfo.cod_comisionista);
                        if (lLector.HasRows)
                        {
                            lLector.Close();
                            lLector.Dispose();
                            lConexion.Cerrar();
                            if (VerificarExistencia("t_rueda", " numero_rueda= " + hdfNoRueda.Value + " and fecha_max_ofe_v_smpsi>='" + hndFecha.Value + "'"))
                            {
                                if (ldFecha <= Convert.ToDateTime(this.dtgConsulta.Items[e.Item.ItemIndex].Cells[10].Text))
                                    Modificar(hdfNoId.Value, "2");
                                else
                                    lblMensaje.Text += " Ya se venció el plazo para corregir la postura " + hdfDescFase.Value + " del Id seleccionada<br>";
                            }
                            else
                                lblMensaje.Text += " Ya se venció el plazo para " + hdfDescFase.Value + " del ID Seleccionado<br>";
                        }
                        else
                            lblMensaje.Text = "El Operador NO ha ingreso Una postura Para el ID {" + hdfNoId.Value + "} Seleccionado.!";
                    }
                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Problema en la Recuperacion de la Postura del Id.");
            }
        }
        if (((LinkButton)e.CommandSource).Text == "Ingresar")
        {
            try
            {
                lblModalidad.Text = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[5].Text;
                lblSubasta.Text = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[2].Text;
                hdfNoId.Value = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text;

                if (!VerificarExistencia("t_rueda rue, t_usuario_hab usrH", " rue.numero_rueda= " + this.dtgConsulta.Items[e.Item.ItemIndex].Cells[1].Text + " and rue.codigo_tipo_rueda = usrH.codigo_tipo_rueda And usrH.codigo_usuario = " + goInfo.codigo_usuario + " And usrH.estado = 'A'  And usrH.fecha_inicial <='" + hndFecha.Value + "' and usrH.fecha_final >= '" + hndFecha.Value + "'"))
                    lblMensaje.Text += " El usuario no Ha sido Aprobado para el tipo de rueda.<br>";
                else
                {
                    if (!VerificarExistencia("t_operador_rueda_hab", " numero_rueda= " + this.dtgConsulta.Items[e.Item.ItemIndex].Cells[1].Text + " and codigo_operador=" + goInfo.cod_comisionista + " And codigo_fases_subasta = 1  And estado = 'A' "))
                        lblMensaje.Text += " La Participacion de los Vendedores para el Operador no Ha sido Aprobada.<br>";
                    else
                    {
                        if (!VerificarExistencia("t_rueda", " numero_rueda= " + this.dtgConsulta.Items[e.Item.ItemIndex].Cells[1].Text + " and fecha_max_ofe_v_smpsi>='" + hndFecha.Value + "'"))
                            lblMensaje.Text += " Ya se venció el plazo para " + hdfDescFase.Value + " del ID Seleccionado<br>";
                        else
                        {
                            if (VerificarExistencia("t_comprador_rueda_hab", " numero_rueda= " + this.dtgConsulta.Items[e.Item.ItemIndex].Cells[1].Text + " and codigo_operador=" + goInfo.cod_comisionista))
                                lblMensaje.Text += " El operador ya se asoció  a la rueda como Comprador, por lo que no se puede declarar como Vendedor.<br>";
                            else
                            {
                                lConexion.Abrir();
                                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_posturas_venta", " numero_id = " + hdfNoId.Value + " And codigo_operador =  " + goInfo.cod_comisionista);
                                if (lLector.HasRows)
                                {
                                    lblMensaje.Text = "El Operador Ya ingreso Una postura Para el ID {" + hdfNoId.Value + "} Seleccionado.!";
                                    lLector.Close();
                                    lLector.Dispose();
                                    lConexion.Cerrar();
                                }
                                else
                                {
                                    lLector.Close();
                                    lLector.Dispose();
                                    lConexion.Cerrar();
                                    Modificar(hdfNoId.Value, "1");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Problema en la Recuperacion del Id. " + ex.Message.ToString());
            }
        }

    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lstable, string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lstable, lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='t_operador_rueda_hab' and llave_registro='codigo_operador_rueda=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_operador_rueda=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "t_operador_rueda_hab";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_operador_rueda_hab", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click1(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_numero_postura", "@P_numero_id", "@P_numero_rueda", "@P_codigo_operador", "@P_precio_postura", "@P_cantidad", "@P_accion", "@P_estado" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char };
        String[] lValorParametros = { "0", hdfNoId.Value, hdfNoRueda.Value, goInfo.cod_comisionista, "0", "0", "1", "C" };
        double ldValor = 0;
        int liCantidad = 0;
        string lsEstado = "C";
        lblMensaje.Text = "";
        ///  Validacion de la Cantidad
        try
        {
            liCantidad = Convert.ToInt32(TxtCantidad.Text.Replace(",", ""));
            if (liCantidad > Convert.ToInt32(hdfValorVigMin.Value))
            {
                lblMensaje.Text += "La Cantidad de la Podtura Supera el Valor { " + hdfValorVigMin.Value + " } PTDV o CIDV aprobado por el Ministerio de Minas. <br>";
                lsEstado = "P";
            }
            if (liCantidad < Convert.ToInt32(hdfCantMin.Value))
            {
                lblMensaje.Text += "La Cantidad de la Podtura es Inferior al Valor { " + hdfCantMin.Value + " } mínimo de negociación. <br>";
                lsEstado = "P";
            }
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "Valor Invalido en la Cantidad de la Postura.!<br>");

        }
        ///  Validacion del Precio
        try
        {
            ldValor = Convert.ToDouble(TxtPrecioReserva.Text.Replace(",", ""));
            if (ldValor < 0)
            {
                lblMensaje.Text += "El precio de la Postura NO puede se menor que 0. <br>";
                lsEstado = "P";
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text += "Valor Invalido en el Precio de la Postura.!";
        }
        try
        {
            lValorParametros[4] = TxtPrecioReserva.Text.Trim().Replace(",", "");
            lValorParametros[5] = TxtCantidad.Text.Trim().Replace(",", "");
            lValorParametros[7] = lsEstado;
            lConexion.Abrir();
            goInfo.mensaje_error = "";
            if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetPosturaVentaId_SMPSI", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
            {
                lblMensaje.Text = "Se presentó un Problema en el Ingreso de la Postura.! " + goInfo.mensaje_error.ToString();
                lConexion.Cerrar();
            }
            else
            {
                TxtCantidad.Text = "";
                TxtPrecioReserva.Text = "";

                if (lsEstado == "P")
                {
                   
                    Toastr.Warning(this, lblMensaje.Text );

                    string lsAsunto = "";
                    string lsMensaje = "";
                    string lsMail = "";
                    string lsNomOperador = "";
                    /// Obtengo el mail del Operador
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + goInfo.cod_comisionista + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsMail = lLector["e_mail"].ToString();
                        if (lLector["tipo_persona"].ToString() == "J")
                            lsNomOperador = lLector["razon_social"].ToString();
                        else
                            lsNomOperador = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();

                    ///// Envio del Mail de la Aprobacion o Rechazo de los Requisitos
                    lsMensaje = "Señores: " + lsNomOperador + "  \n\n";
                    lsAsunto = "Notificación Correccion Declaracion de Vendedores";
                    lsMensaje = "Nos permitimos  informarle que la Declaracion de Vendedores se Ingreso en el Sistema con Errores. Por lo que debe ser modificada para que pueda participar en la Subasta " + lblSubasta.Text + ".  \n\n\n";
                    lsMensaje += "Coordinalmente, \n\n\n";
                    lsMensaje += "Administrador Sistema de Gas  \n";
                    clEmail mail = new clEmail(lsMail, lsAsunto, lsMensaje, "");
                    lblMensaje.Text = mail.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");

                }
                else
                {
                    TxtCantidad.Text = "";
                    TxtPrecioReserva.Text = "";
                    Toastr.Success(this, "Postura Ingresada Correctamente.!");                 
                }
                Listar();
            }
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, ex.Message);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click1(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_numero_postura", "@P_numero_id", "@P_numero_rueda", "@P_codigo_operador", "@P_precio_postura", "@P_cantidad", "@P_accion", "@P_estado" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char };
        String[] lValorParametros = { hdfNoPostura.Value, hdfNoId.Value, hdfNoRueda.Value, goInfo.cod_comisionista, "0", "0", "2", "C" };
        double ldValor = 0;
        int liCantidad = 0;
        string lsEstado = "C";
        lblMensaje.Text = "";
        ///  Validacion de la Cantidad
        try
        {
            liCantidad = Convert.ToInt32(TxtCantidad.Text.Replace(",", ""));
            if (liCantidad > Convert.ToInt32(hdfValorVigMin.Value))
            {
                lblMensaje.Text += "La Cantidad de la Podtura Supera el Valor { " + hdfValorVigMin.Value + " } PTDV o CIDV aprobado por el Ministerio de Minas. <br>";
                lsEstado = "P";
            }
            if (liCantidad < Convert.ToInt32(hdfCantMin.Value))
            {
                lblMensaje.Text += "La Cantidad de la Podtura es Inferior al Valor { " + hdfCantMin.Value + " } mínimo de negociación. <br>";
                lsEstado = "P";
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text += "Valor Invalido en la Cantidad de la Postura.!<br>";
        }
        ///  Validacion del Precio
        try
        {
            ldValor = Convert.ToDouble(TxtPrecioReserva.Text.Replace(",", ""));
            if (ldValor < 0)
            {
                lblMensaje.Text += "El precio de la Postura NO puede se menor que 0. <br>";
                lsEstado = "P";
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text += "Valor Invalido en el Precio de la Postura.!";
        }
        try
        {
            lValorParametros[4] = TxtPrecioReserva.Text.Trim().Replace(",", "");
            lValorParametros[5] = TxtCantidad.Text.Trim().Replace(",", "");
            lValorParametros[7] = lsEstado;
            lConexion.Abrir();
            goInfo.mensaje_error = "";
            if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetPosturaVentaId_SMPSI", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
            {
                lblMensaje.Text = "Se presentó un Problema en el Ingreso de la Postura.! " + goInfo.mensaje_error.ToString();
                lConexion.Cerrar();
            }
            else
            {
                if (lsEstado == "P")
                {
                    lblMensaje.Text += "La Oferta se modifico, pero tiene problemas en las validaciones, por lo que debe ser corregida para poder participar en la subasta.!<br>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + lblMensaje.Text + "');", true);
                    string lsAsunto = "";
                    string lsMensaje = "";
                    string lsMail = "";
                    string lsNomOperador = "";
                    /// Obtengo el mail del Operador
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + goInfo.cod_comisionista + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        lsMail = lLector["e_mail"].ToString();
                        if (lLector["tipo_persona"].ToString() == "J")
                            lsNomOperador = lLector["razon_social"].ToString();
                        else
                            lsNomOperador = lLector["nombres"].ToString() + " " + lLector["apellidos"].ToString();
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();

                    ///// Envio del Mail de la Aprobacion o Rechazo de los Requisitos
                    lsMensaje = "Señores: " + lsNomOperador + "  \n\n";
                    lsAsunto = "Notificación Correccion Declaracion de Vendedores";
                    lsMensaje += "Nos permitimos  informarle que la Declaracion de Vendedores se Ingreso en el Sistema con Errores. Por lo que debe ser modificada para que pueda participar en la Subasta " + lblSubasta.Text + ".  \n\n\n";
                    lsMensaje += "Coordinalmente, \n\n\n";
                    lsMensaje += "Administrador Sistema de Gas \n";
                    clEmail mail = new clEmail(lsMail, lsAsunto, lsMensaje, "");
                    lblMensaje.Text = mail.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");

                }
                else
                {
                    TxtCantidad.Text = "";
                    TxtPrecioReserva.Text = "";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Postura Actualizada Correctamente.!" + "');", true);

                }

                Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click1(object sender, EventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (hdfNoId.Value != "")
            manejo_bloqueo("E", hdfNoId.Value);
        lblMensaje.Text = "";
        Listar();
    }
}