﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;

public partial class Procesos_frm_CapacidadRemanente : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Calculo de Capacidad Remanente";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            if (goInfo.cod_comisionista != "0")
            {
                BtnCalcular.Visible = false;
                BtnAprobar.Visible = false;
            }
            else
            {
                BtnCalcular.Visible = true;
                BtnAprobar.Visible = true;
            }
            consultar();
        }
    }
    protected void BtnCalcular_Click(object sender, EventArgs e)
    {
        SqlDataReader lLector;
        SqlCommand lComando = new SqlCommand();
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_SetCapacExcRem";
        lComando.CommandTimeout = 3600;
        lLector = lComando.ExecuteReader();
        if (lLector.HasRows)
        {
            lLector.Read();
            lblMensaje.Text = lLector["Mensaje"].ToString();
        }
        else
            consultar();
        lLector.Close();
        lLector.Dispose();
        lConexion.Cerrar();
        
    }

    protected void BtnAprobar_Click(object sender, EventArgs e)
    {
        SqlDataReader lLector;
        SqlCommand lComando = new SqlCommand();
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_SetCapacExcRemAprob";
        lComando.CommandTimeout = 3600;
        lLector = lComando.ExecuteReader();
        if (lLector.HasRows)
        {
            lLector.Read();
            lblMensaje.Text = lLector["Mensaje"].ToString();
        }
        else
            consultar();
        lLector.Close();
        lLector.Dispose();
        lConexion.Cerrar();
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void consultar()
    {
        lblMensaje.Text = "";
        if (lblMensaje.Text == "")
        {
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetCapacRemanente";
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgConsulta.DataSource = lds;
                dtgConsulta.DataBind();
                tblGrilla.Visible = true;
                imbExcel.Visible = true;
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfPubRem" + DateTime.Now + ".xls";
        string lstitulo_informe = "Informe Publicación Capacidad Remanente";
        try
        {
            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgConsulta.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            lform.Controls.Add(dtgConsulta);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Excel.!" + ex.Message.ToString();
        }
    }

}