﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using AjaxControlToolkit;
using System.Drawing;
using Segas.Web.Elements;

public partial class Procesos_frm_tablero_neg_posturas : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    //String strRutaArchivo;
    private System.TimeSpan diffResult;
    static String lsCadenaCon;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            try
            {
                if (this.Request.QueryString["punta"] != null && this.Request.QueryString["punta"].ToString() != "")
                    hdnPunta.Value = this.Request.QueryString["punta"].ToString();
                else
                    hdnPunta.Value = "V";
                lsCadenaCon = "Data Source=" + goInfo.Servidor.ToString() + "; Initial Catalog=" + goInfo.BaseDatos.ToString() + ";User ID = " + goInfo.Usuario.ToString() + "; Password=" + goInfo.Password.ToString();
                lConexion1 = new clConexion(goInfo);
                ////////////////////////////////////
                /// Cambios Req. 012-17 20170309 ///
                ////////////////////////////////////
                cargarDatosGrilla();
                ////////////////////////////////////
                //if (Session["codigo_punto"].ToString() != "0" && Session["codigo_periodo"].ToString() != "0")
                //{
                //    cargarDatosGrilla();
                //}
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, ex.Message);
            }
        }

    }
    /// <summary>
    /// Nombre: cargarDatosGrillaC
    /// Fecha: Novimebre 1 de 2016
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Cargar los datos en la Grilla de las Posturas de Compra
    /// Modificacion:
    /// </summary>
    private void cargarDatosGrilla()
    {
        try
        {
            if (hdnPunta.Value == "C")
            {
                dtgPosturas.Columns[1].HeaderText = "COMPRADOR";
                lblTitulo.Text = "COMPRA";
            }
            else
            {
                dtgPosturas.Columns[1].HeaderText = "VENDEDOR";
                lblTitulo.Text = "VENTA";
            }
            //determina las columnas para el subastatodr o los operaores
            string[] lsNombreParametros = { "@P_punta_postura", "@P_codigo_punto", "@P_codigo_periodo",
                                            "@P_numero_rueda" // Campo uevo Req. 012-17 20170309
                                          };
            SqlDbType[] lTipoparametros = { SqlDbType.Char, SqlDbType.Int, SqlDbType.Int, 
                                            SqlDbType.Int // Campo uevo Req. 012-17 20170309
                                          };
            string[] lValorParametros = { hdnPunta.Value, Session["codigo_punto"].ToString(), Session["codigo_periodo"].ToString(),
                                          Session["numero_rueda"].ToString().Trim() // Campo uevo Req. 012-17 20170309
                                        };
            lConexion.Abrir();
            dtgPosturas.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetPosturaTabNegociacion", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgPosturas.DataBind();
            lConexion.Cerrar();
            ImageButton imbControl;
            foreach (DataGridItem Grilla in this.dtgPosturas.Items)
            {
                if (Session["cod_comisionista"].ToString() == Grilla.Cells[13].Text)
                {
                    Grilla.BackColor = Color.Gray;
                    imbControl = (ImageButton)Grilla.Cells[10].Controls[1];
                    imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_tablero_neg_IngPosturas.aspx?ID=" + Grilla.Cells[12].Text + "','Modificar','width=540,height=500,left=350,top=100,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                }
                else
                {
                    imbControl = (ImageButton)Grilla.Cells[10].Controls[1];
                    imbControl.ImageUrl = "~/Images/quitar.gif";
                    imbControl.Enabled = false;
                    imbControl = (ImageButton)Grilla.Cells[11].Controls[1];
                    imbControl.ImageUrl = "~/Images/quitar.gif";
                    imbControl.Enabled = false;
                }
            }
            if (hdnPunta.Value == "C")
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.posturaV.location='frm_tablero_neg_posturas.aspx?punta=V';", true);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + ex.Message + "');", true);
        }
    }
    /// <summary>
    /// Metodo para el Evento del Botón Eliminar de las Posturas de Compra
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void OnItemCommandC_Click(object sender, DataGridCommandEventArgs e)
    {
        string oError = "";
        string oBoton = ((System.Web.UI.WebControls.ImageButton)e.CommandSource).ID.ToString();
        if (oBoton == "imbEliminar")
        {
            try
            {
                string[] lsNombreParametros = { "@P_numero_postura", "@P_codigo_operador", "@P_punta_postura", "@P_accion" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
                string[] lValorParametros = { e.Item.Cells[12].Text, Session["cod_comisionista"].ToString(), "C", "E" };
                SqlDataReader lLector;
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetPosturaTableroNeg", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error == "")
                {
                    oError = "Oferta No. " + e.Item.Cells[12].Text + " Eliminada Correctamente";
                    Session["refrescar"] = "S";
                }
                else
                    oError = "Error al eliminar la oferta. " + goInfo.mensaje_error.ToString();
                lConexion.Cerrar();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + oError + "');", true);
            }
            catch (Exception ex)
            {
                oError = "Error al eliminar la oferta. " + ex.Message;
            }
        }
        ////if (oBoton == "imbModificar")
        ////{
        ////    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('frm_tablero_neg_IngPosturas.aspx?ID=" + e.Item.Cells[10].Text + "&punta=C','Modificar','width=540,height=500,left=350,top=100,status=no,location=0,menubar=no,toolbar=no,resizable=no');", true);
        ////}
    }
}