﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;

public partial class Procesos_frm_tablero_neg_reloj : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
        }
        catch (Exception ex)
        {

        }
    }
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        try
        {
            lblRelog.Text = DateTime.Now.ToString("HH:mm:ss").Substring(0, 8);
            if (Session["refrescar"].ToString() == "S")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.posturaC.location='frm_tablero_neg_posturas.aspx?punta=C';", true);
                Session["refrescar"] = "N";
            }
        }
        catch (Exception ex)
        {

        }
    }

}