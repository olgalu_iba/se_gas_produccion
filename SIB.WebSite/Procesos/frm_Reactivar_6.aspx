﻿<%@ Page Language="C#" MasterPageFile="~/PlantillaPrincipal.master" AutoEventWireup="true" CodeFile="frm_Reactivar_6.aspx.cs" Inherits="Procesos_frm_Reactivar_6" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript"  language="javascript">
        //        document.location = no;
        function confirmar() {
            return confirm("Esta seguro que desea reactivar la rueda!")
        }

    </script>

  
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td6" align="center" colspan="4">
                        <asp:Label ID="lblTitulo" runat="server" Text="REACTIVAR RUEDA" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="4">
                        <asp:Label ID="lblEstado" runat="server"></asp:Label> 
                        <asp:HiddenField ID="hndFase" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="td6" style="width:20%">
                        Horas Para Publicacion venta
                    </td>
                    <td class="td6" style="width:30%">
                        <asp:TextBox ID="txtHoraIniPubV" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revtxtHoraIniPubV" ControlToValidate="txtHoraIniPubV"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora de publicacion de venta"> * </asp:RegularExpressionValidator>
                        -
                        <asp:TextBox ID="txtHoraFinPubV" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revtxtHoraFinPubV" ControlToValidate="txtHoraFinPubV"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora final  de publicacion de venta"> * </asp:RegularExpressionValidator>
                    </td>
                    <td class="td6" style="width:20%">
                        Horas Para Publicacion de cantidad disponible
                    </td>
                    <td class="td6" style="width:30%">
                        <asp:TextBox ID="txtHoraIniCntD" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revtxtHoraIniCntD" ControlToValidate="txtHoraIniCntD"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora de inicio de publicacion de cantidad disponible"> * </asp:RegularExpressionValidator>
                        -
                        <asp:TextBox ID="txtHoraFinCntD" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revtxtHoraFinCntD" ControlToValidate="txtHoraFinCntD"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora final  de publicacion de cantidad disponible"> * </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="td6">
                        Horas Para Recibo de cantidad de compra
                    </td>
                    <td class="td6">
                        <asp:TextBox ID="txtHoraIniComp" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RevtxtHoraIniComp" ControlToValidate="txtHoraIniComp"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora de recibo de cantidad de compra"> * </asp:RegularExpressionValidator>
                        -
                        <asp:TextBox ID="txtHoraFinComp" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revtxtHoraFinComp" ControlToValidate="txtHoraFinComp"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora final  de recibo de cantidad de compra"> * </asp:RegularExpressionValidator>
                    </td>
                    <td class="td6">
                        Hora Para Negociación
                    </td>
                    <td class="td6">
                        <asp:TextBox ID="txtHoraIniNeg" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revtxttxtHoraIniNeg" ControlToValidate="txtHoraIniNeg"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora inicial de negociación"> * </asp:RegularExpressionValidator>
                        -
                        <asp:TextBox ID="txtHoraFinNeg" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revtxtHoraFinNeg" ControlToValidate="txtHoraFinNeg"
                            runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora final de negociacion"> * </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="4">
                        Observaciones:
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="4">
                        <asp:TextBox ID="TxtObservacion" runat="server" Width="500px" MaxLength="1000" Rows="3" TextMode="MultiLine" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" class="td6" colspan= "4">
                        <br />
                        <asp:Button ID="btnReactivar" Text="Reactivar" runat="server" OnClientClick="return confirmar();"
                            OnClick="btnReactivar_Click"  ValidationGroup="comi" CausesValidation="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center" class="td6">
                        <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>