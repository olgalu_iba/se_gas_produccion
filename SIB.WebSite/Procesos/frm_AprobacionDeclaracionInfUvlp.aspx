﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_AprobacionDeclaracionInfUvlp.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Procesos_frm_AprobacionDeclaracionInfUvlp" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">

        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }

        function formatoMiles(obj, evt) {
            var T;
            var strValue = removeCommas(obj.value);
            //if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
            if ((T = /([0-9]*)$/i.exec(strValue)) != null) {
                //                T[4] = T[4].substr(0, decimales);
                //                if (T[2] == '' && T[3] == '.') T[2] = 0;
                obj.value = addCommas(T[1]);
            } else {
                alert('formato invalido ');
            }
        }

        function formatoMilesPre(obj, evt, decimales) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
                T[4] = T[4].substr(0, decimales);
                if (T[2] == '' && T[3] == '.') {
                    T[2] = 0;
                }
                obj.value = T[1] + addCommas(T[2]) + T[3] + T[4];
            }
            else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }

    </script>

    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
                            <asp:DropDownList ID="ddlOperador" CssClass="form-control" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Destino Rueda" AssociatedControlID="ddlDestino" runat="server" />
                            <asp:DropDownList ID="ddlDestino" CssClass="form-control" runat="server">
                                <asp:ListItem Value="G" Text="Suministro de Gas"></asp:ListItem>
                                <asp:ListItem Value="T" Text="Capacidad de Transporte"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgSuministro" runat="server" AutoGenerateColumns="False" AllowPaging="true" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered"
                                OnItemCommand="dtgSuministro_EditCommand" Visible="false">
                                <Columns>
                                    <%--0--%>
                                    <asp:TemplateColumn HeaderText="Sele.">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="ChkTodos" runat="server" OnCheckedChanged="ChkTodosSum_CheckedChanged"
                                                AutoPostBack="true" Text="Sel. Todos" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ChkSeleccionar" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--1--%>
                                    <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="No Id" ItemStyle-Width="80px" Visible="false"></asp:BoundColumn>
                                    <%--2--%>
                                    <asp:BoundColumn DataField="numero_contrato" HeaderText="No. Operación"></asp:BoundColumn>
                                    <%--3--%>
                                    <asp:BoundColumn DataField="desc_punto" HeaderText="Punto Snt" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--4--%>
                                    <asp:BoundColumn DataField="operador_compra" HeaderText="Cód. Titular" Visible="false"></asp:BoundColumn>
                                    <%--20180815 BUG230--%>
                                    <%--5--%>
                                    <asp:BoundColumn DataField="nombre_comprador" HeaderText="Titular Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--6--%>
                                    <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--7--%>
                                    <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Número Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--8--%>
                                    <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left" DataFormatString="{0: yyyy/MM/dd}"></asp:BoundColumn>
                                    <%--9--%>
                                    <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left" DataFormatString="{0: yyyy/MM/dd}"></asp:BoundColumn>
                                    <%--10--%>
                                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad Energía Contratada (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--11--%>
                                    <asp:BoundColumn DataField="equiv_demand" HeaderText="Cantidad Energía Contratada (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--12--%>
                                    <asp:BoundColumn DataField="capacidad_real1" HeaderText="Cantidad Energía Contratada Real(MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--13--%>
                                    <asp:BoundColumn DataField="equiv_declarada" HeaderText="Cantidad Energía Contratada Real(KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--14--%>
                                    <asp:BoundColumn DataField="codigo_fuente" HeaderText="Cód Fte"></asp:BoundColumn>
                                    <%--20180815 BUG230--%>
                                    <%--15--%>
                                    <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente"></asp:BoundColumn>
                                    <%--16--%>
                                    <asp:BoundColumn DataField="modificacion" HeaderText="Pend. Aprobación"></asp:BoundColumn>
                                    <%--17--%>
                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more-1"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                    <!--begin::Nav-->
                                                    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <ul class="kt-nav">
                                                                <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lkbComplementar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                                <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                                <span class="kt-nav__link-text">Modificar</span>
                                                                    </asp:LinkButton>
                                                                </li>
                                                            </ul>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--18--%>
                                    <asp:BoundColumn DataField="desc_punto_salida" ItemStyle-HorizontalAlign="Left"
                                        Visible="false"></asp:BoundColumn>
                                    <%--19--%>
                                    <asp:BoundColumn DataField="cod_cont_mp_uvlp" Visible="false"></asp:BoundColumn>
                                    <%--20--%>
                                    <asp:BoundColumn DataField="codigo_punto_salida" Visible="false"></asp:BoundColumn>
                                    <%--21--%>
                                    <asp:BoundColumn DataField="codigo_punto" Visible="false"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgTransporte" runat="server" AutoGenerateColumns="False" AllowPaging="true" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered"
                                OnItemCommand="dtgTransporte_EditCommand" Visible="false">
                                <Columns>
                                    <%--0--%>
                                    <asp:TemplateColumn HeaderText="Sele.">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="ChkTodos" runat="server" OnCheckedChanged="ChkTodosTra_CheckedChanged"
                                                AutoPostBack="true" Text="Sel. Todos" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ChkSeleccionar" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--1--%>
                                    <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="No Id" Visible="false"></asp:BoundColumn>
                                    <%--2--%>
                                    <asp:BoundColumn DataField="numero_contrato" HeaderText="No. Operación"></asp:BoundColumn>
                                    <%--3--%>
                                    <asp:BoundColumn DataField="nombre_comprador" HeaderText="Titular Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--4--%>
                                    <asp:BoundColumn DataField="desc_punto" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="220px"></asp:BoundColumn>
                                    <%--5--%>
                                    <asp:BoundColumn DataField="sentido_flujo" HeaderText="Sentido Flujo Gas" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--6--%>
                                    <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contrato" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--7--%>
                                    <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Número Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--8--%>
                                    <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left" DataFormatString="{0: yyyy/MM/dd}"></asp:BoundColumn>
                                    <%--9--%>
                                    <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left" DataFormatString="{0: yyyy/MM/dd}"></asp:BoundColumn>
                                    <%--10--%>
                                    <asp:BoundColumn DataField="cantidad" HeaderText="Capacidad Transporte Contratada (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--11--%>
                                    <asp:BoundColumn DataField="capacidad_real1" HeaderText="Capacidad Transporte Contratada Real (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--12--%>
                                    <asp:BoundColumn HeaderText="Pend. Aprobación" DataField="modificación"></asp:BoundColumn>
                                    <%--20180815 BUG230--%>
                                    <%--13--%>
                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more-1"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                    <!--begin::Nav-->
                                                    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <ul class="kt-nav">
                                                                <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lkbComplementar2" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                                <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                                <span class="kt-nav__link-text">Modificar</span>
                                                                    </asp:LinkButton>
                                                                </li>
                                                            </ul>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--14--%>
                                    <asp:BoundColumn DataField="cod_cont_mp_uvlp" Visible="false"></asp:BoundColumn>
                                    <%--15--%>
                                    <asp:BoundColumn DataField="codigo_punto_entrega" Visible="false"></asp:BoundColumn>
                                    <%--16--%>
                                    <asp:BoundColumn DataField="operador_compra" Visible="false"></asp:BoundColumn>
                                    <%--17--%>
                                    <asp:BoundColumn DataField="cantidad" Visible="false"></asp:BoundColumn>
                                    <%--18--%>
                                    <asp:BoundColumn DataField="cantidad_real" Visible="false"></asp:BoundColumn>
                                    <%--19--%>
                                    <asp:BoundColumn DataField="capacidad_mod" Visible="false"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <%--Excel--%>
                    <asp:DataGrid ID="dtgTransporteTramo" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table-bordered"
                        AllowPaging="false" PagerStyle-HorizontalAlign="Center" Visible="false">
                        <Columns>
                            <%--0--%>
                            <asp:BoundColumn DataField="numero_contrato" HeaderText="Número Operación" ItemStyle-Width="80px"></asp:BoundColumn>
                            <%--1--%>
                            <asp:BoundColumn DataField="desc_ruta" HeaderText="Ruta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--2--%>
                            <asp:BoundColumn DataField="codigo_tramo" HeaderText="Código Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--3--%>
                            <asp:BoundColumn DataField="desc_tramo" HeaderText="Desc. Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--4--%>
                            <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Número Contrato" ItemStyle-Width="80px"></asp:BoundColumn>
                            <%--5--%>
                            <asp:BoundColumn DataField="operador_compra" HeaderText="Cód. Titular" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--20180815 BUG230--%>
                            <%--6--%>
                            <asp:BoundColumn DataField="nombre_comprador" HeaderText="Titular Compra" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--7--%>
                            <asp:BoundColumn DataField="sentido_flujo" HeaderText="Sentido Flujo Gas" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px"></asp:BoundColumn>
                            <%--8--%>
                            <asp:BoundColumn DataField="desc_modalidad" HeaderText="Modalidad Contrato" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px"></asp:BoundColumn>
                            <%--9--%>
                            <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left" DataFormatString="{0: yyyy/MM/dd}"></asp:BoundColumn>
                            <%--10--%>
                            <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left" DataFormatString="{0: yyyy/MM/dd}"></asp:BoundColumn>
                            <%--11--%>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Capacidad Transporte Contratada (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="150px"></asp:BoundColumn>
                            <%--12--%>
                            <asp:BoundColumn DataField="cantidad_declarada" HeaderText="Capacidad Transporte Contratada Real (KPCD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="150px"></asp:BoundColumn>
                            <%--13--%>
                            <asp:BoundColumn DataField="modificacion" HeaderText="Pend. Aprobación"></asp:BoundColumn>
                            <%--14--%>
                            <asp:BoundColumn DataField="porc_cargo_fijo" HeaderText="Porc. Cargo Fijo Inv" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            <%--15--%>
                            <asp:BoundColumn DataField="cargo_fijo_inv" HeaderText="Cargo Fijo Inversión" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###.###}"></asp:BoundColumn>
                            <%--16--%>
                            <asp:BoundColumn DataField="porc_cargo_var" HeaderText="Porc. Cargo Variable Inv" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            <%--17--%>
                            <asp:BoundColumn DataField="cargo_variable" HeaderText="Cargo Variable Inversión" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###.###}"></asp:BoundColumn>
                            <%--18--%>
                            <asp:BoundColumn DataField="cargo_fijo_adm" HeaderText="Cargo Fijo Administración" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###.###}"></asp:BoundColumn>
                            <%--19--%>
                            <asp:BoundColumn DataField="porc_imp_tra" HeaderText="Porcentaje Impuesto Transporte" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###.00}"></asp:BoundColumn>
                            <%--20--%>
                            <asp:BoundColumn DataField="porc_cuota" HeaderText="Porcentaje Cuota de Fomento" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###.00}"></asp:BoundColumn>
                        </Columns>
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </div>

    <%--Modals--%>

    <%--Captura de Suministro--%>
    <div class="modal fade" id="mdlCapturaSuministro" tabindex="-1" role="dialog" aria-labelledby="mdlCapturaSuministroLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlCapturaSuministroInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlCapturaSuministroLabel" runat="server">Modificar</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Punto Salida del SNT" AssociatedControlID="ddlPunto" runat="server" />
                                        <asp:DropDownList ID="ddlPunto" CssClass="form-control" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label Text="Cantidad Contratada por el Titular (MBTUD)" AssociatedControlID="TxtCantidadCont" runat="server" />
                                        <asp:TextBox ID="TxtCantidadCont" CssClass="form-control" runat="server" />
                                        <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtCantidadCont" runat="server" TargetControlID="TxtCantidadCont" FilterType="Custom, Numbers" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Equivalente en KPCD de energía contratada (Tomada para el cálculo de Capacidad Excedentaria)" AssociatedControlID="TxtEquivalContraSum" runat="server" />
                                        <asp:TextBox ID="TxtEquivalContraSum" CssClass="form-control" runat="server" />
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbTxtEquivalContraSum" runat="server" TargetControlID="TxtEquivalContraSum" FilterType="Custom, Numbers" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Cantidad Demandada por el Titular (MBTUD)" AssociatedControlID="TxtCantidadDemanSum" runat="server" />
                                        <asp:TextBox ID="TxtCantidadDemanSum" CssClass="form-control" runat="server"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtCantidadDemanSum" runat="server" TargetControlID="TxtCantidadDemanSum" FilterType="Custom, Numbers" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Equivalente en KPCD de Cantidad Demandada" AssociatedControlID="TxtCantidadDemanSum" runat="server" />
                                        <asp:TextBox ID="TxtEquivalDemanSum" CssClass="form-control" runat="server" />
                                        <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtEquivalDemanSum" runat="server" TargetControlID="TxtEquivalDemanSum" FilterType="Custom, Numbers" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Fuente" AssociatedControlID="ddlFuente" runat="server" />
                                        <asp:DropDownList ID="ddlFuente" CssClass="form-control" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Observación Modificación" AssociatedControlID="TxtObservaGas" runat="server" />
                                        <asp:TextBox ID="TxtObservaGas" CssClass="form-control" runat="server" />
                                    </div>
                                </div>
                            </div>

                            <br />

                            <div class="row justify-content-center" runat="server">
                                <div class="form-group">
                                    <div class="btn-group btn-group-justified">
                                        <asp:Button ID="btnComplementaGas" runat="server" Text="Crear" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="btnComplementaGas_Click" CssClass="btn btn-primary" />
                                        <asp:Button ID="btnModificaGas" runat="server" Text="Modificar" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="btnModificaGas_Click" Visible="false" CssClass="btn btn-primary" />
                                        <asp:Button ID="btnLimpiarGas" runat="server" Text="Limpiar" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="btnLimpiarGas_Click" CssClass="btn btn-primary" />
                                    </div>
                                </div>
                            </div>

                            <div class="table table-responsive">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <asp:DataGrid ID="dtgDetalleGas" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center"
                                            OnItemCommand="dtgDetalleGas_EditCommand" Visible="false">
                                            <Columns>
                                                <%--0--%>
                                                <asp:BoundColumn DataField="cod_cont_mp_uvlp_sum" Visible="false"></asp:BoundColumn>
                                                <%--1--%>
                                                <asp:BoundColumn DataField="cod_cont_mp_uvlp" Visible="false"></asp:BoundColumn>
                                                <%--2--%>
                                                <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Pto sal"></asp:BoundColumn>
                                                <%--3--%>
                                                <asp:BoundColumn DataField="desc_punto_salida" HeaderText="descripcion"></asp:BoundColumn>
                                                <%--4--%>
                                                <asp:BoundColumn DataField="cantidad_contratada" HeaderText="Cantidad contratada MBTUD"></asp:BoundColumn>
                                                <%--5--%>
                                                <asp:BoundColumn DataField="equiv_cont_kpcd" HeaderText="Equivalente contratada KPCD"></asp:BoundColumn>
                                                <%--6--%>
                                                <asp:BoundColumn DataField="cantidad_demandada" HeaderText="Cantidad Demandada MBTUD"></asp:BoundColumn>
                                                <%--7--%>
                                                <asp:BoundColumn DataField="equiv_demand_kpcd" HeaderText="Equivalente demandada KPCD"></asp:BoundColumn>
                                                <%--8--%>
                                                <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                                    <ItemTemplate>
                                                        <div class="dropdown dropdown-inline">
                                                            <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="flaticon-more-1"></i>
                                                            </button>
                                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                                <!--begin::Nav-->
                                                                <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <ul class="kt-nav">
                                                                            <li class="kt-nav__item">
                                                                                <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                             <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Modificar</span>
                                                                                </asp:LinkButton>
                                                                            </li>
                                                                            <li class="kt-nav__item">
                                                                                <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon-delete"></i>
                                                            <span class="kt-nav__link-text">Eliminar</span>
                                                                                </asp:LinkButton>
                                                                            </li>
                                                                        </ul>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                                <!--end::Nav-->
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                        </asp:DataGrid>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                            <asp:HiddenField ID="hdfNoId" runat="server" />
                            <asp:HiddenField ID="hdfCodContSum" runat="server" />
                            <asp:HiddenField ID="hdfOpeAut" runat="server" />
                            <asp:HiddenField ID="hndFecha" runat="server" />
                            <asp:HiddenField ID="hdfOperaCompra" runat="server" />
                            <asp:HiddenField ID="hdfPuntoSalida" runat="server" />
                            <asp:HiddenField ID="hdfIdComplementa" runat="server" />
                            <asp:HiddenField ID="hdfCodRuta" runat="server" />
                            <asp:HiddenField ID="hdfModifica" runat="server" />
                            <asp:HiddenField ID="hdfCostosTramo" runat="server" />
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="Button2" Text="Cancelar" UseSubmitBehavior="false" OnClientClick="this.disabled = true;" OnClick="btnSalirGas_Click" CssClass="btn btn-secondary" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <%--Captura de Transporte--%>
    <div class="modal fade" id="mdlCapturaTransporte" tabindex="-1" role="dialog" aria-labelledby="mdlCapturaTransporteLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlCapturaTransporteInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlCapturaTransporteLabel" runat="server">Modificar</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                                    <asp:Label runat="server" ID="lbl" AssociatedControlID="txtCapacReal" Text="Capacidad Contratada Real (KPCD)"></asp:Label>
                                    <asp:TextBox ID="txtCapacReal" runat="server" Width="100%" CssClass="form-control"></asp:TextBox>
                                    <asp:HiddenField ID="hdfCapacidadOrg" runat="server" />
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                                    <asp:Label runat="server" ID="lblObeservacionTran" AssociatedControlID="TxtObservaTra" Text="Observación Modificación"></asp:Label>
                                    <asp:TextBox ID="TxtObservaTra" runat="server" Width="100%" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <br />

                            <div class="row justify-content-center" runat="server">
                                <div class="form-group">
                                    <div class="btn-group btn-group-justified">
                                        <asp:Button ID="Button1" runat="server" Text="Limpiar" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="btnLimpiar_Click" Visible="true" CssClass="btn btn-primary" />
                                    </div>
                                </div>
                            </div>

                            <div class="table table-responsive">
                                <asp:DataGrid ID="dtgDetalleTrans" Width="100%" CssClass="table-bordered" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                    PagerStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <%--0--%>
                                        <asp:BoundColumn DataField="cod_cont_mp_uvlp_det" Visible="false"></asp:BoundColumn>
                                        <%--1--%>
                                        <asp:BoundColumn DataField="cod_cont_mp_uvlp" Visible="false"></asp:BoundColumn>
                                        <%--2--%>
                                        <asp:BoundColumn DataField="codigo_tramo" HeaderText="Tramo"></asp:BoundColumn>
                                        <%--3--%>
                                        <asp:BoundColumn DataField="desc_tramo" HeaderText="Desc Tramo"></asp:BoundColumn>
                                        <%--4--%>
                                        <asp:TemplateColumn HeaderText="Porc. Cargo Fijo Inv">
                                            <ItemTemplate>
                                                <asp:TextBox ID="TxtPorcFijo" runat="server" Width="60px" OnTextChanged="TxtPorcFijo_TextChanged"
                                                    AutoPostBack="true"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FTBETxtPorcFijo" runat="server" TargetControlID="TxtPorcFijo"
                                                    FilterType="Custom, Numbers" ValidChars="."></ajaxToolkit:FilteredTextBoxExtender>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <%--5--%> <%--20221012--%>
                                        <asp:BoundColumn DataField="cargo_fijo_inv" HeaderText="Cargo Fijo Costo Inver. (Moneda Vigente/KPCD-AÑO)"
                                            DataFormatString="{0:###,###,###,###,###,##0.000}"></asp:BoundColumn>
                                        <%--6--%>
                                        <asp:TemplateColumn HeaderText="Porc. Cargo Variable Inv">
                                            <ItemTemplate>
                                                <asp:TextBox ID="TxtPorcVar" runat="server" Width="60px" OnTextChanged="TxtPorcVar_TextChanged"
                                                    AutoPostBack="true"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FTBETxtPorcVar" runat="server" TargetControlID="TxtPorcVar"
                                                    FilterType="Custom, Numbers" ValidChars="."></ajaxToolkit:FilteredTextBoxExtender>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <%--7--%> <%--20221012--%>
                                        <asp:BoundColumn DataField="cargo_variable" HeaderText="Cargo Variable Costo Inver. (Moneda Vigente/KPC)"
                                            DataFormatString="{0:###,###,###,###,###,##0.000}"></asp:BoundColumn>
                                        <%--8--%> <%--20221012--%>
                                        <asp:BoundColumn DataField="cargo_fijo_adm" HeaderText="Cargo Fijo AOM (COP/KPCD-AÑO)"></asp:BoundColumn>
                                        <%--9--%>
                                        <asp:BoundColumn DataField="orden" Visible="false"></asp:BoundColumn>
                                        <%--10--%>
                                        <asp:BoundColumn DataField="codigo_pozo_ini" Visible="false"></asp:BoundColumn>
                                        <%--11--%>
                                        <asp:BoundColumn DataField="codigo_pozo_fin" Visible="false"></asp:BoundColumn>
                                        <%--12--%>
                                        <asp:BoundColumn DataField="capacidad_cont" Visible="false"></asp:BoundColumn>
                                        <%--13--%>
                                        <asp:BoundColumn DataField="tiene_costo" Visible="false"></asp:BoundColumn>
                                        <%--14--%>
                                        <asp:BoundColumn DataField="costo_fijo_100" Visible="false"></asp:BoundColumn>
                                        <%--15--%>
                                        <asp:BoundColumn DataField="costo_variable_100" Visible="false"></asp:BoundColumn>
                                        <%--16--%>
                                        <asp:BoundColumn DataField="costo_adm" Visible="false"></asp:BoundColumn>
                                        <%--17--%>
                                        <asp:BoundColumn DataField="porc_cargo_fijo" Visible="false"></asp:BoundColumn>
                                        <%--18--%>
                                        <asp:BoundColumn DataField="porc_cargo_var" Visible="false"></asp:BoundColumn>
                                        <%--19--%>
                                        <asp:TemplateColumn HeaderText="Impuesto Transporte">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlImpTra" runat="server">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <%--20--%>
                                        <asp:TemplateColumn HeaderText="Cuota Fomento">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlCuota" runat="server">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <%--21--%>
                                        <asp:BoundColumn DataField="porc_imp_tra" Visible="false" DataFormatString="{0:###.00}"></asp:BoundColumn>
                                        <%--22--%>
                                        <asp:BoundColumn DataField="porc_cuota" Visible="false" DataFormatString="{0:###.00}"></asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                </asp:DataGrid>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnSalirTrans" Text="Cancelar" UseSubmitBehavior="false" OnClientClick="this.disabled = true;" OnClick="btnSalirTrans_Click" CssClass="btn btn-secondary" runat="server" />
                            <asp:Button ID="btnCrearTrans" runat="server" Text="Actualizar" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="btnCrearTrans_Click" CssClass="btn btn-primary" />
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender3" runat="server" TargetControlID="btnCrearTrans" ConfirmText="Esta Seguro(a) de Crear el Registro ? "></ajaxToolkit:ConfirmButtonExtender>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

</asp:Content>
