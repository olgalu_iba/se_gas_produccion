﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_AprobacionHabOperBim.aspx.cs"
    Inherits="Procesos_frm_AprobacionHabOperBim" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
                    width="90%">
                    <tr>
                        <td align="center" class="th1">
                            <asp:Label ID="lblTitulo" runat="server" ForeColor ="White"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br /><br /><br /><br /><br /><br />
                <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
                    width="90%">
                    <tr>
                        <td class="td1">
                            Tipo Operador
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlTipo" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTipo_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td class="td1">
                            Operador
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlOperador" runat="server" Width="200px">
                            </asp:DropDownList>
                        </td>
                        <td class="td1">
                            Rueda
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlRueda" runat="server">
                            </asp:DropDownList>
                            <asp:HiddenField ID="hdfRueda" runat="server" />
                        </td>
                        <td class="td1">
                            Punta
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlPunta" runat="server">
                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="C" Text="Compra"></asp:ListItem>
                                <asp:ListItem Value="V" Text="Venta"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="95%" runat="server"
        id="tblMensaje">
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
                <asp:Button ID="btnAprobar" runat="server" Text="Aprobar" OnClientClick="return confirmar();"
                    OnClick="btnAprobar_Click" Visible="false" />
                <asp:Button ID="btnRechazar" runat="server" Text="Des-Aprobar" OnClientClick="return confirmar1();"
                    OnClick="btnRechazar_Click" Visible="false" />
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click"
                    Visible="false" Height="35" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla" visible="false">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 1150px; height: 350px;">
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        HeaderStyle-CssClass="th1">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Sele.">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ChkTodos" runat="server" OnCheckedChanged="ChkTodos_CheckedChanged"
                                        AutoPostBack="true" Text="Seleccionar Todos" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkRecibir" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Cd Ope" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ind_compra" HeaderText="Habilitado Compra" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ind_venta" HeaderText="Habilitado Venta" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <%--20170328 rq001-01--%>
                            <asp:BoundColumn DataField="tipo_declaracion" HeaderText="Tipo Declaracion" visible="false">
                            </asp:BoundColumn>
                            <%--20170328 rq001-01--%>
                            <asp:TemplateColumn HeaderText="Tipo Declaración">
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlDeclaracion" runat="server">
                                        <asp:ListItem Value="A">Ambas</asp:ListItem>
                                        <asp:ListItem Value="P">Previa</asp:ListItem>
                                        <asp:ListItem Value="N">Normal</asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <%--<asp:HiddenField ID="hndCodRueda" runat="server" />--%>
            </td>
        </tr>
    </table>
</asp:Content>