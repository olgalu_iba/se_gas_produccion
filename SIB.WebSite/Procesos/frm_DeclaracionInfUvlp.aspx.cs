﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Procesos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frm_DeclaracionInfUvlp : Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Declaración de Información Subasta Úselo o Véndalo a Largo Plazo";
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        clConexion lConexion = null;
        clConexion lConexion1 = null;
        SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        private string lsIndica
        {
            get
            {
                if (ViewState["lsIndica"] == null)
                    return "";
                else
                    return (string)ViewState["lsIndica"];
            }
            set
            {
                ViewState["lsIndica"] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //Se agrega el comportamiento del botón
            //buttons.ExportarExcelOnclick += lkbExcel_Click;
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.AprobarOnclick += btnAprobar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            // buttons.ListarOnclick += imbActualiza_Click1;

            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;

            //Establese los permisos del sistema
            lConexion = new clConexion(goInfo);

            if (IsPostBack) return;

            //Titulo
            Master.Titulo = "Subasta";

            try
            {
                lblTitulo.Text = lsTitulo;
                lConexion.Abrir();
                string dia = DateTime.Now.Day.ToString();
                string mes = DateTime.Now.Month.ToString();
                string anio = DateTime.Now.Year.ToString();

                hndFecha.Value = anio + "-" + mes + "-" + dia;
                LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and codigo_operador !=0 order by razon_social", 0, 4);
                LlenarControles(lConexion.gObjConexion, ddlFuente, "m_pozo", " estado = 'A' and ind_campo_pto ='C' order by descripcion", 0, 1);
                if (Session["tipoPerfil"].ToString() == "N")
                {
                    if (VerificarExistencia("m_operador ope, m_tipos_operador tpo, m_operador_subasta opeS", "ope.codigo_operador = " + goInfo.cod_comisionista + " and ope.tipo_operador = tpo.sigla  and tpo.codigo_tipo_operador = opeS.codigo_tipo_operador and opeS.codigo_tipo_subasta = 3 and opeS.punta='V' and opeS.estado ='A'"))
                        hdfOpeAut.Value = "S";
                    else
                    {
                        hdfOpeAut.Value = "N";
                        Toastr.Info(this, "El tipo de operador no está habilitado como vendedor");
                    }
                }
                else
                    hdfOpeAut.Value = "S";
                lConexion.Cerrar();
                //Botones
                EnumBotones[] botones = { EnumBotones.Aprobar, EnumBotones.Buscar, EnumBotones.Excel };
                buttons.Inicializar(botones: botones);
                // Carga informacion de combos
                if (Session["tipoPerfil"].ToString() == "N")
                {
                    ddlOperador.SelectedValue = goInfo.cod_comisionista;
                    ddlOperador.Enabled = false;
                }
                else
                {
                    btnCrearGas.Visible = false;
                    btnCrearTrans.Visible = false;
                    btnModificaGas.Visible = false;
                    //btnCancelarGas.Visible = false; //20210531
                    dtgDetalleGas.Columns[8].Visible = false;
                    //dtgDetalleGas.Columns[9].Visible = false; 20210531 ajsutes UVLP
                    dtgDetalleTrans.Columns[9].Visible = false;
                    dtgDetalleTrans.Columns[10].Visible = false;
                }
                tblCapturaSuministro.Visible = false;
                divCapturaSuministro.Visible = false;
                divCapturaSuministro2.Visible = false;
                btnCrearGas.Visible = false;
                btnModificaGas.Visible = false;
                btnLimpiar.Visible = false;
                CargarDatos();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "Problemas en la Carga de la Página. " + ex.Message);
            }
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"] + "-" + lLector["razon_social"];
                }
                lDdl.Items.Add(lItem1);

            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControlesPto(SqlConnection lConn, DropDownList lDdl, string lsPozo, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            string[] lsNombreParametros = { "@P_codigo_pozo_ini" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int };
            string[] lValorParametros = { lsPozo };
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetPtoSalSentido", lsNombreParametros, lTipoparametros, lValorParametros);
            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);
            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Complementar(string modificar, string lsIndica)
        {
            if (modificar != null && modificar != "")
            {
                try
                {
                    if (!manejo_bloqueo("V", modificar))
                    {
                        // Activo la Pantalla de Captura de Suministro
                        if (lsIndica == "G")
                        {

                            tblCapturaSuministro.Visible = true;
                            divCapturaSuministro.Visible = true;
                            divCapturaSuministro2.Visible = true;
                            btnCrearGas.Visible = true;
                            btnModificaGas.Visible = true;
                            btnLimpiar.Visible = true;
                            btnCancelarGas.Visible = true; //20210531
                            btnCancelarTrans.Visible = false; //20210531
                            lblTitulo.Text = "Complementar " + lsTitulo;
                            ddlPunto.Focus();
                            if (Session["tipoPerfil"].ToString() == "N")
                            {
                                btnCrearGas.Visible = true;
                                btnModificaGas.Visible = false;
                            }
                            TxtCantidadCont.Text = "";
                            TxtEquivalContraSum.Text = "";
                            TxtCantidadDemanSum.Text = "";
                            TxtEquivalDemanSum.Text = "";
                            CargarDetSum();
                            // Modal.Abrir(this, mdlSuspender.ID, mdlInsideSuspender.ID);
                        }
                        // Activo la Pantalla de Captura de Transporte
                        if (lsIndica == "T")
                        {
                            tblCapturaTransporte.Visible = true;
                            divCapturaTransporte.Visible = true;
                            lblTitulo.Text = "Complementar " + lsTitulo;
                            btnCancelarGas.Visible = false; //20210531
                            btnCancelarTrans.Visible = true; //20210531
                            if (Session["tipoPerfil"].ToString() == "N")
                            {
                                btnCrearTrans.Visible = true;
                            }
                            CargarDetTra();
                            // Modal.Abrir(this, mdlSuspender.ID, mdlInsideSuspender.ID);
                        }
                    }
                    else
                    {
                        CargarDatos();
                        Toastr.Error(this, "No se Puede complementar el Registro por que está Bloqueado. Id Registro " + modificar);
                    }
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, ex.Message);
                }
            }
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            if (hdfOpeAut.Value == "S")
            {
                string[] lsNombreParametros = { "@P_codigo_operador", "@P_destino_rueda" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
                string[] lValorParametros = { ddlOperador.SelectedValue, ddlDestino.SelectedValue };
                string lsIdReg = "";
                //string lsUltimoPunto = "";
                lblTitulo.Text = lsTitulo;
                try
                {
                    lConexion.Abrir();
                    if (ddlDestino.SelectedValue == "G")
                    {
                        dtgTransporte.Visible = false;
                        dtgSuministro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetNegMPUvlp", lsNombreParametros, lTipoparametros, lValorParametros);
                        dtgSuministro.DataBind();
                        if (dtgSuministro.Items.Count > 0)
                        {
                            foreach (DataGridItem Grilla in this.dtgSuministro.Items)
                            {
                                lsIdReg = Grilla.Cells[19].Text;  //720180404 rq002-18
                                //lsUltimoPunto = Grilla.Cells[3].Text.Trim().Replace("&nbsp;", "");
                                if (lsIdReg != "0")
                                {
                                    //if (Grilla.Cells[16].Text == "S")
                                    Grilla.Cells[0].Enabled = true;
                                    //else
                                    //    Grilla.Cells[0].Enabled = false;
                                    Grilla.Cells[17].Enabled = true; //720180404 rq002-18
                                }
                                else
                                {
                                    Grilla.Cells[0].Enabled = false;
                                    Grilla.Cells[17].Enabled = false; //720180404 rq002-18
                                }
                            }
                            dtgSuministro.Visible = true;
                        }
                        else
                            Toastr.Info(this, "No hay Registros para Visualizar.!");

                    }
                    else
                    {
                        dtgSuministro.Visible = false;
                        dtgTransporte.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetNegMPUvlp", lsNombreParametros, lTipoparametros, lValorParametros);
                        dtgTransporte.DataBind();
                        if (dtgTransporte.Items.Count > 0)
                        {
                            foreach (DataGridItem Grilla in this.dtgTransporte.Items)
                            {
                                lsIdReg = Grilla.Cells[11].Text;  //2018404 rq002-18
                                if (lsIdReg != "0")
                                    Grilla.Cells[0].Enabled = true;
                                else
                                    Grilla.Cells[0].Enabled = false;
                            }
                            dtgTransporte.Visible = true;
                            dtgTransporteTramo.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetTramosDecInfExcel", lsNombreParametros, lTipoparametros, lValorParametros);
                            dtgTransporteTramo.DataBind();
                        }
                        else
                            Toastr.Info(this, "No hay Registros para Visualizar.!");

                    }
                    lConexion.Cerrar();
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, ex.Message);
                }
            }
        }

        /// <summary>
        /// Nombre: dtgSuministro_EditCommand
        /// Fecha: Agosto 25 de 2015
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar la Complementacion del Registro del Contrato de Suministro con el
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgSuministro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            DateTime ldFecha = DateTime.Now;
            if (!VerificarExistencia("t_cnt_demandada_uvlp", "codigo_operador = " + goInfo.cod_comisionista + " and ano_declaracion = year(getdate())"))
            {

                if (e.CommandName.Equals("Complementar"))
                {
                    try
                    {
                        mdlRegistroDecInfoSubUVLPLabel.InnerText = "Complementar";
                        //Abre el modal de Agregar
                        Modal.Abrir(this, registroDecInfoSubUVLP.ID, registroDecInfoSubUVLPInside.ID);

                        hdfVerif.Value = this.dtgSuministro.Items[e.Item.ItemIndex].Cells[1].Text;
                        hdfOperaCompra.Value = this.dtgSuministro.Items[e.Item.ItemIndex].Cells[4].Text; //720180404 rq002-18
                        hdfPuntoSalida.Value = this.dtgSuministro.Items[e.Item.ItemIndex].Cells[20].Text; //720180404 rq002-18
                        hdfIdComplementa.Value = this.dtgSuministro.Items[e.Item.ItemIndex].Cells[19].Text;//720180404 rq002-18
                        //hdfCantidadSum.Value = this.dtgSuministro.Items[e.Item.ItemIndex].Cells[19].Text; //720180404 rq002-18
                        try
                        {
                            ddlFuente.SelectedValue = this.dtgSuministro.Items[e.Item.ItemIndex].Cells[14].Text; //720180404 rq002-18
                        }
                        catch (Exception ex)
                        {
                        }
                        lConexion.Abrir();
                        ddlPunto.Items.Clear();
                        LlenarControlesPto(lConexion.gObjConexion, ddlPunto, this.dtgSuministro.Items[e.Item.ItemIndex].Cells[23].Text, 0, 1); //720180404 rq002-18
                        lConexion.Cerrar();
                        Complementar(hdfVerif.Value, "G");

                    }
                    catch (Exception ex)
                    {
                        Toastr.Error(this, "Problemas en la Recuperación del Registro.");

                    }
                }
            }
            else
                Toastr.Error(this, "No se Puede complementar el Registro por que ya se aprobó la declaración total de energía");

            //if (((LinkButton)e.CommandSource).Text == "Energia Demandada")
            //{
            //    try
            //    {
            //        hdfNoId.Value = this.dtgSuministro.Items[e.Item.ItemIndex].Cells[1].Text;
            //        hdfOperaCompra.Value = this.dtgSuministro.Items[e.Item.ItemIndex].Cells[12].Text;
            //        hdfPuntoSalida.Value = this.dtgSuministro.Items[e.Item.ItemIndex].Cells[11].Text;
            //        hdfIdComplementa.Value = this.dtgSuministro.Items[e.Item.ItemIndex].Cells[9].Text;
            //        lblPuntoSalida.Text = this.dtgSuministro.Items[e.Item.ItemIndex].Cells[3].Text;
            //        lblNumeroContrato.Text = this.dtgSuministro.Items[e.Item.ItemIndex].Cells[14].Text;
            //        Complementar(hdfNoId.Value, "D");

            //    }
            //    catch (Exception ex)
            //    {
            //        lblMensaje.Text = "Problema en la Recuperacion del Registro.";
            //    }
            //}
        }

        /// <summary>
        /// Nombre: dtgTransporte_EditCommand
        /// Fecha: Agosto 25 de 2015
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar la Complementacion del Registro del Contrato de Transporte con el
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgTransporte_EditCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals("Complementar"))
            {
                try
                {
                    mdlRegistroDecInfoSubUVLPLabel.InnerText = "Complementar";
                    //Abre el modal de Agregar
                    Modal.Abrir(this, registroDecInfoSubUVLP.ID, registroDecInfoSubUVLPInside.ID);

                    hdfVerif.Value = this.dtgTransporte.Items[e.Item.ItemIndex].Cells[1].Text;
                    hdfIdComplementa.Value = this.dtgTransporte.Items[e.Item.ItemIndex].Cells[11].Text; //20180404 rq002-18
                    hdfCodRuta.Value = this.dtgTransporte.Items[e.Item.ItemIndex].Cells[15].Text; //20180404 rq002-18
                    hdfOperaCompra.Value = this.dtgTransporte.Items[e.Item.ItemIndex].Cells[16].Text; //20180404 rq002-18
                    hdfCapacidadOrg.Value = this.dtgTransporte.Items[e.Item.ItemIndex].Cells[17].Text; //20180404 rq002-18
                    txtCapacReal.Text = this.dtgTransporte.Items[e.Item.ItemIndex].Cells[18].Text; //20180404 rq002-18
                    Complementar(hdfVerif.Value, "T");
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, "Problemas en la recuperación del registro.");
                }
            }
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lstable, string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia(lstable, lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:-
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='t_contrato_verificacion' and llave_registro='codigo_verif_contrato=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_verif_contrato=" + lscodigo_registro;
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_contrato_verificacion";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_contrato_verificacion", lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCrearGas_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_cod_cont_mp_uvlp_sum", "@P_codigo_operador", "@P_cod_cont_mp_uvlp", "@P_codigo_verif_contrato", "@P_codigo_punto_salida", "@P_cantidad_contratada", "@P_equiv_cont_kpcd", "@P_cantidad_demandada", "@P_equiv_demand_kpcd", "@P_accion", "@P_codigo_fuente" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char, SqlDbType.Int };
            String[] lValorParametros = { "0", goInfo.cod_comisionista, hdfIdComplementa.Value, hdfVerif.Value, ddlPunto.SelectedValue, TxtCantidadCont.Text, TxtEquivalContraSum.Text, TxtCantidadDemanSum.Text, TxtEquivalDemanSum.Text, "C", ddlFuente.SelectedValue };
            int liCantidad = 0;
            var mensaje = new StringBuilder();
            ///  Validacion de la Equivalente en KPCD de energía contratada
            if (ddlPunto.SelectedValue == "0")
            {
                mensaje.Append("Debe seleccionar el punto de salida del SNT.!<br>");
            }

            ///  Validacion la cantidad contratada
            if (TxtCantidadCont.Text.Trim().Length > 0)
            {
                try
                {
                    liCantidad = Convert.ToInt32(TxtCantidadCont.Text);
                    if (liCantidad < 0)
                        mensaje.Append("Valor Inválido en la cantidad contratada.!");
                }
                catch (Exception)
                {
                    mensaje.Append("Valor Inválido en la cantidad contratada.!");
                }
            }
            else
                mensaje.Append("Debe Ingresar la cantidad contratada.!");

            ///  Validacion de la Equivalente en KPCD de energía contratada
            if (TxtEquivalContraSum.Text.Trim().Length > 0)
            {
                try
                {
                    liCantidad = Convert.ToInt32(TxtEquivalContraSum.Text);
                    if (liCantidad < 0)
                        mensaje.Append("Valor Inválido en Equivalente en KPCD de energía contratada.!");
                }
                catch (Exception)
                {
                    mensaje.Append("Valor Inválido en Equivalente en KPCD de energía contratada.!");
                }
            }
            else
                mensaje.Append("Valor Inválido en Equivalente en KPCD de energía contratada.!");

            ///  Validacion de la Cantidad Demandada por el Titular
            if (TxtCantidadDemanSum.Text.Trim().Length > 0)
            {
                try
                {
                    liCantidad = Convert.ToInt32(TxtCantidadDemanSum.Text);
                    if (liCantidad < 0)
                        mensaje.Append("Valor Inválido en Cantidad Demandada por el Titular.!");
                }
                catch (Exception)
                {
                    mensaje.Append("Valor Inválido en Cantidad Demandada por el Titular.!");
                }
            }
            else
                mensaje.Append("Valor Inválido en Cantidad Demandada por el Titular.!");

            ///  Validacion de laEquivalente en KPCD de Cantidad Demandada
            if (TxtEquivalDemanSum.Text.Trim().Length > 0)
            {
                try
                {
                    liCantidad = Convert.ToInt32(TxtEquivalDemanSum.Text);
                    if (liCantidad < 0)
                        mensaje.Append("Valor Inválido en Equivalente en KPCD de Cantidad Demandada.!<br>");
                }
                catch (Exception)
                {
                    mensaje.Append("Valor Inválido en Equivalente en KPCD de Cantidad Demandada.!<br>");
                }
            }
            else
                mensaje.Append("Valor Inválido en Equivalente en KPCD de Cantidad Demandada.!<br>");

            if (VerificarExistencia("t_cont_mp_uvlp con, t_cont_mp_uvlp_sum conS", "con.codigo_verif_contrato = " + hdfVerif.Value + " and con.ano_declaracion = year(getdate()) and con.cod_cont_mp_uvlp = conS.cod_cont_mp_uvlp and conS.codigo_punto_salida=" + ddlPunto.SelectedValue))
            {
                mensaje.Append("Ya se ingresó el punto de salida .!");
            }

            if (!string.IsNullOrEmpty(mensaje.ToString()))
            {
                Toastr.Error(this, mensaje.ToString());
                return;
            }

            string lsError;
            lConexion.Abrir();
            goInfo.mensaje_error = "";
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetDecInfUvlpSum", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
            if (goInfo.mensaje_error != "")
            {
                Toastr.Error(this, "Se presentó un problema en el ingreso de la información.! " + goInfo.mensaje_error);
                lConexion.Cerrar();
            }
            else
            {
                lLector.Read();
                lsError = lLector["Error"].ToString();
                if (!string.IsNullOrEmpty(lsError))
                {
                    Toastr.Error(this, lsError);
                    lLector.Close();
                    lLector.Dispose();
                }
                else
                {
                    hdfIdComplementa.Value = lLector["cod_reg"].ToString();
                    lLector.Close();
                    lLector.Dispose();
                    ddlPunto.SelectedValue = "0";
                    TxtCantidadCont.Text = "";
                    TxtEquivalContraSum.Text = "";
                    TxtCantidadDemanSum.Text = "";
                    TxtEquivalDemanSum.Text = "";

                    //Se notifica a el usuario que el registro fue realizado de manera exitosa
                    Toastr.Success(this, "Se realizo el registro de forma exitosa.!");
                    CargarDetSum();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnModificaGas_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_cod_cont_mp_uvlp_sum", "@P_codigo_operador", "@P_cod_cont_mp_uvlp", "@P_codigo_punto_salida", "@P_cantidad_contratada", "@P_equiv_cont_kpcd", "@P_cantidad_demandada", "@P_equiv_demand_kpcd", "@P_accion", "@P_codigo_fuente" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char, SqlDbType.Int };
            String[] lValorParametros = { hdfCodContSum.Value, goInfo.cod_comisionista, hdfIdComplementa.Value, ddlPunto.SelectedValue, TxtCantidadCont.Text, TxtEquivalContraSum.Text, TxtCantidadDemanSum.Text, TxtEquivalDemanSum.Text, "M", ddlFuente.SelectedValue };
            int liCantidad = 0;
            var mensaje = new StringBuilder();
            ///  Validacion la cantidad contratada
            if (TxtCantidadCont.Text.Trim().Length > 0)
            {
                try
                {
                    liCantidad = Convert.ToInt32(TxtCantidadCont.Text);
                    if (liCantidad < 0)
                        mensaje.Append("Valor Inválido en la cantidad contratada.!");
                }
                catch (Exception)
                {
                    mensaje.Append("Valor Inválido en la cantidad contratada.!");
                }
            }
            else
                mensaje.Append("Valor Inválido en la cantidad contratada.!");

            ///  Validacion de la Equivalente en KPCD de energía contratada
            if (TxtEquivalContraSum.Text.Trim().Length > 0)
            {
                try
                {
                    liCantidad = Convert.ToInt32(TxtEquivalContraSum.Text);
                    if (liCantidad < 0)
                        mensaje.Append("Valor Inválido en Equivalente en KPCD de energía contratada.!");
                }
                catch (Exception)
                {
                    mensaje.Append("Valor Inválido en Equivalente en KPCD de energía contratada.!");
                }
            }
            else
                mensaje.Append("Valor Inválido en Equivalente en KPCD de energía contratada.!");

            ///  Validacion de la Cantidad Demandada por el Titular
            if (TxtCantidadDemanSum.Text.Trim().Length > 0)
            {
                try
                {
                    liCantidad = Convert.ToInt32(TxtCantidadDemanSum.Text);
                    if (liCantidad < 0)
                    {
                        mensaje.Append("Valor Inválido en Cantidad Demandada por el Titular.!");
                    }
                }
                catch (Exception)
                {
                    mensaje.Append("Valor Inválido en Cantidad Demandada por el Titular.!");
                }
            }
            else
                mensaje.Append("Valor Inválido en Cantidad Demandada por el Titular.!");

            ///  Validacion de laEquivalente en KPCD de Cantidad Demandada
            if (TxtEquivalDemanSum.Text.Trim().Length > 0)
            {
                try
                {
                    liCantidad = Convert.ToInt32(TxtEquivalDemanSum.Text);
                    if (liCantidad < 0)
                        mensaje.Append("Valor Inválido en Equivalente en KPCD de Cantidad Demandada.!");
                }
                catch (Exception)
                {
                    mensaje.Append("Valor Inválido en Equivalente en KPCD de Cantidad Demandada.!");
                }
            }
            else
                mensaje.Append("Valor Inválido en Equivalente en KPCD de Cantidad Demandada.!");

            if (!string.IsNullOrEmpty(mensaje.ToString()))
            {
                Toastr.Error(this, mensaje.ToString());
                return;
            }

            string lsError;
            lConexion.Abrir();
            goInfo.mensaje_error = "";
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetDecInfUvlpSum", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
            if (goInfo.mensaje_error != "")
            {
                Toastr.Error(this, "Se presentó un Problema en el Ingreso de la Información.! " + goInfo.mensaje_error);
                lConexion.Cerrar();
            }
            else
            {
                lLector.Read();
                lsError = lLector["Error"].ToString();
                if (!string.IsNullOrEmpty(lsError))
                {
                    Toastr.Error(this, lsError);
                    lLector.Close();
                    lLector.Dispose();
                }
                else
                {
                    lLector.Close();
                    lLector.Dispose();
                    ddlPunto.Enabled = true;
                    ddlPunto.SelectedValue = "0";
                    TxtCantidadCont.Text = "";
                    TxtEquivalContraSum.Text = "";
                    TxtCantidadDemanSum.Text = "";
                    TxtEquivalDemanSum.Text = "";
                    CargarDetSum();
                    btnCrearGas.Visible = true;
                    btnModificaGas.Visible = false;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancelarGas_Click(object sender, EventArgs e)
        {
            /// Desbloquea el Registro ActualizadotblCapturaTransporte
            if (hdfVerif.Value != "")
                manejo_bloqueo("E", hdfVerif.Value);
            ddlPunto.SelectedValue = "0";
            ddlPunto.Enabled = true;
            TxtCantidadCont.Text = "";
            TxtEquivalContraSum.Text = "";
            TxtCantidadDemanSum.Text = "";
            TxtEquivalDemanSum.Text = "";
            btnModificaGas.Visible = false;
            dtgDetalleGas.Visible = false;
            CargarDatos();
            tblCapturaSuministro.Visible = false;
            divCapturaSuministro.Visible = false;
            divCapturaSuministro2.Visible = false;
            btnCrearGas.Visible = false;
            btnLimpiar.Visible = false;
            dtgDetalleTrans.Visible = false;

            //Cierra el modal de Agregar
            Modal.Cerrar(this, registroDecInfoSubUVLP.ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            CargarDatos();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAprobar_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_destino_rueda", "@P_operador_compra", "@P_cod_cont_mp_uvlp", "@P_cod_cont_mp_uvlp_det", "@P_indica" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            String[] lValorParametros = { "0", "T", goInfo.cod_comisionista, "0", "0", "2" };
            int icont = 0;
            CheckBox oCheck;
            string lsIdReg = "";
            string lsCodigoReg = "";
            var mensaje = new StringBuilder();
            // Valido el ingreso de los datos de la grilla de tramos.
            lConexion.Abrir();
            try
            {
                if (ddlDestino.SelectedValue == "G")
                {
                    if (!VerificarExistencia("t_cnt_demandada_uvlp", "codigo_operador = " + goInfo.cod_comisionista + " and ano_declaracion = year(getdate())"))
                    {

                        lValorParametros[1] = "G";
                        foreach (DataGridItem Grilla in this.dtgSuministro.Items)
                        {
                            oCheck = (CheckBox)Grilla.Cells[0].Controls[1];
                            if (oCheck.Checked)
                            {
                                icont++;
                                lsIdReg = Grilla.Cells[19].Text;//720180404 rq002-18
                                lsCodigoReg = Grilla.Cells[1].Text;
                                lValorParametros[3] = lsIdReg;
                                lValorParametros[0] = lsCodigoReg;
                                goInfo.mensaje_error = "";
                                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetDeclaracionUvlp", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                                if (goInfo.mensaje_error != "")
                                {
                                    Toastr.Error(this, "Se presentó un Problema en la Aprobación de la Declaración No. {" + lsCodigoReg + "}.! " + goInfo.mensaje_error, "Error!", 50000);
                                    lConexion.Cerrar();
                                    lLector.Close();
                                    lLector.Dispose();
                                    break;
                                }
                                else
                                {
                                    if (lLector.HasRows)
                                    {
                                        lLector.Read();
                                        var error = lLector["error"].ToString();
                                        if (!string.IsNullOrEmpty(error))
                                            Toastr.Error(this, error + "<br>");
                                    }
                                }
                                lLector.Close();
                                lLector.Dispose();
                            }
                        }
                    }
                    else
                        Toastr.Error(this, "No se puede aprobar la declaración de información porque ya se aprobó la cantidad total de energía");
                }
                else
                {
                    lValorParametros[1] = "T";
                    foreach (DataGridItem Grilla in this.dtgTransporte.Items)
                    {

                        oCheck = (CheckBox)Grilla.Cells[0].Controls[1];
                        if (oCheck.Checked)
                        {
                            icont++;
                            lsIdReg = Grilla.Cells[11].Text; //20180404 rq002-18
                            lsCodigoReg = Grilla.Cells[1].Text;
                            lValorParametros[3] = lsIdReg;
                            goInfo.mensaje_error = "";
                            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetDeclaracionUvlp", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                            if (goInfo.mensaje_error != "")
                            {
                                Toastr.Error(this, "Se presentó un Problema en la Aprobación de la Declaración No. {" + lsCodigoReg + "}.! " + goInfo.mensaje_error);
                                lConexion.Cerrar();
                                lLector.Close();
                                lLector.Dispose();
                                break;
                            }
                            else
                            {
                                if (lLector.HasRows)
                                {
                                    lLector.Read();
                                    mensaje.Append(lLector["error"] + "<br>");
                                }
                            }
                            lLector.Close();
                            lLector.Dispose();
                        }
                    }
                }
                lConexion.Cerrar();
                if (icont == 0)
                    mensaje.Append("No seleccionó Registros para Procesar" + "<br>");

                if (!string.IsNullOrEmpty(mensaje.ToString()))
                {
                    Toastr.Error(this, mensaje.ToString());
                    return;
                }

                Toastr.Success(this, "Registros Aprobados Correctamente.!");
                CargarDatos();
            }
            catch (Exception ex)
            {
                lConexion.Cerrar();
                Toastr.Error(this, "Error en la Aprobación de los Registros " + ex.Message);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCrearTrans_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_verif_contrato", "@P_destino_rueda", "@P_operador_compra", "@P_codigo_ruta", "@P_cod_cont_mp_uvlp", "@P_indica", "@P_capacidad_cont" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            String[] lValorParametros = { hdfVerif.Value, "T", hdfOperaCompra.Value, hdfCodRuta.Value, hdfIdComplementa.Value, "0", txtCapacReal.Text };
            string[] lsNombreParametrosD = { "@P_cod_cont_mp_uvlp_det", "@P_codigo_verif_contrato", "@P_codigo_tramo", "@P_porc_fijo", "@P_porc_var", "@P_orden", "@P_porc_imp_tra", "@P_porc_cuota" };
            SqlDbType[] lTipoparametrosD = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.Decimal };
            String[] lValorParametrosD = { "0", hdfVerif.Value, "0", "0", "0", "0", "0", "0" };
            var mensaje = new StringBuilder();
            var mensaje2 = new StringBuilder();
            int ldPorcentajeF;
            int ldPorcentajeV;

            if (txtCapacReal.Text == "")
                mensaje2.Append("Debe digitar la capacidad contratada real<br>");
            else
            {
                try
                {
                    ldPorcentajeF = Convert.ToInt32(txtCapacReal.Text);
                    if (ldPorcentajeF < 0)
                        mensaje2.Append("La capacidad contratada real debe ser mayor que cero<br>");
                    if (ldPorcentajeF > Convert.ToInt32(hdfCapacidadOrg.Value))
                        mensaje2.Append("La capacidad contratada real debe ser menor o igual que la capacidad registrada en el contrato<br>");
                }
                catch (Exception)
                {
                    mensaje2.Append("Valor Inválido en la cantidad contratada real<br>");
                }
            }

            //valida la información registrada
            TextBox oText;
            DropDownList oDrop;
            foreach (DataGridItem Grilla in this.dtgDetalleTrans.Items)
            {
                ldPorcentajeF = 0;
                ldPorcentajeV = 0;
                if (Grilla.Cells[13].Text == "N")
                    mensaje2.Append("El tramo " + Grilla.Cells[3].Text + " no tiene definidos los costos<br>");
                else
                {
                    oText = (TextBox)Grilla.Cells[4].Controls[1];
                    if (oText.Text == "")
                        mensaje2.Append("Debe digitar el porcentaje de costo fijo para el tramo " + Grilla.Cells[3].Text + " <br>");
                    else
                    {
                        try
                        {
                            ldPorcentajeF = Convert.ToInt32(oText.Text);
                            if (ldPorcentajeF > 100)
                                mensaje2.Append("El porcentaje de costo fijo para el tramo " + Grilla.Cells[3].Text + " no puede ser mayor que 100<br>");
                        }
                        catch (Exception ex)
                        {
                            mensaje2.Append("El porcentaje de costo fijo para el tramo " + Grilla.Cells[3].Text + " no es válido<br>");
                        }
                    }

                    oText = (TextBox)Grilla.Cells[6].Controls[1];
                    if (oText.Text == "")
                        mensaje2.Append("Debe digitar el porcentaje de costo variable para el tramo " + Grilla.Cells[3].Text + "<br>");
                    else
                    {
                        try
                        {
                            ldPorcentajeV = Convert.ToInt32(oText.Text);
                            if (ldPorcentajeV > 100)
                                mensaje2.Append("El porcentaje de costo variable para el tramo " + Grilla.Cells[3].Text + " no puede ser mayor que 100<br>");
                        }
                        catch (Exception)
                        {
                            mensaje2.Append("El porcentaje de costo variable para el tramo " + Grilla.Cells[3].Text + " no es válido<br>");
                        }
                    }
                    if (ldPorcentajeF + ldPorcentajeV != 100)
                        mensaje2.Append("El porcentaje de costo fijo mas el porcentaje de costo variable para el tramo " + Grilla.Cells[3].Text + " debe ser 100<br>");

                    oDrop = (DropDownList)Grilla.Cells[19].Controls[1];
                    if (oDrop.SelectedValue == "0")
                        mensaje2.Append("Debe seleccionar el porcentaje de impuesto de transporte para el tramo " + Grilla.Cells[3].Text + "<br>");

                    oDrop = (DropDownList)Grilla.Cells[20].Controls[1];
                    if (oDrop.SelectedValue == "0")
                        mensaje2.Append("Debe seleccionar el porcentaje de cuota de fomento para el tramo " + Grilla.Cells[3].Text + "<br>");
                }
            }

            if (!string.IsNullOrEmpty(mensaje2.ToString()))
            {
                Toastr.Warning(this, mensaje2.ToString());
                return;
            }

            lConexion.Abrir();
            try
            {
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion,
                    "pa_SetDeclaracionUvlp", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error == "")
                {
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        var error = lLector["error"].ToString();
                        if (!string.IsNullOrEmpty(error))
                        {
                            mensaje2.Append(error);
                            Toastr.Error(this, error);
                        }
                    }
                }

                lLector.Close();
                lLector.Dispose();
                if (string.IsNullOrEmpty(mensaje2.ToString()))
                {
                    goInfo.mensaje_error = "";
                    foreach (DataGridItem Grilla in this.dtgDetalleTrans.Items)
                    {
                        ldPorcentajeF = 0;
                        ldPorcentajeV = 0;
                        lValorParametrosD[0] = Grilla.Cells[0].Text;
                        lValorParametrosD[2] = Grilla.Cells[2].Text;
                        oText = (TextBox)Grilla.Cells[4].Controls[1];
                        lValorParametrosD[3] = oText.Text;
                        oText = (TextBox)Grilla.Cells[6].Controls[1];
                        lValorParametrosD[4] = oText.Text;
                        lValorParametrosD[5] = Grilla.Cells[9].Text;
                        oDrop = (DropDownList)Grilla.Cells[19].Controls[1];
                        lValorParametrosD[6] = oDrop.SelectedValue;
                        oDrop = (DropDownList)Grilla.Cells[20].Controls[1];
                        lValorParametrosD[7] = oDrop.SelectedValue;
                        DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetDecInfUvlpTra",
                            lsNombreParametrosD, lTipoparametrosD, lValorParametrosD, goInfo);
                        if (goInfo.mensaje_error == "") continue;
                        mensaje.Append("Se presentó un error en la creación de la declaración." + goInfo.mensaje_error);
                        hdfIdComplementa.Value = "0";
                        break;
                    }

                    if (!string.IsNullOrEmpty(mensaje.ToString()))
                    {
                        Toastr.Error(this, mensaje.ToString());
                    }
                    else
                    {
                        //Se notifica a el usuario que el registro fue realizado de manera exitosa
                        Toastr.Success(this, "Se realizo el registro de forma exitosa.!");
                        //Cierra el modal de Agregar
                        Modal.Cerrar(this, registroDecInfoSubUVLP.ID);
                        btnCancelarTrans_Click(null, null);
                        CargarDatos();
                    }
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "Error en la Creación de la Declaración. " + ex.Message);
            }

            btnCrearTrans.Visible = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnLimpiarGas_Click(object sender, EventArgs e)
        {
            ddlPunto.SelectedValue = "0";
            ddlPunto.Enabled = true;
            TxtCantidadCont.Text = "";
            TxtEquivalContraSum.Text = "";
            TxtCantidadDemanSum.Text = "";
            TxtEquivalDemanSum.Text = "";
            btnModificaGas.Visible = false;
            btnCrearGas.Visible = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancelarTrans_Click(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (hdfVerif.Value != "")
                manejo_bloqueo("E", hdfVerif.Value);
            CargarDatos();
            tblCapturaTransporte.Visible = false;
            divCapturaTransporte.Visible = false;
            dtgDetalleGas.Visible = false;
            dtgDetalleTrans.Visible = false;
            //Cierra el modal de Agregar
            Modal.Cerrar(this, registroDecInfoSubUVLP.ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlDestino.SelectedValue == "G")
                {
                    string lsNombreArchivo = Session["login"] + "InfExcelDecInf" + DateTime.Now + ".xls";
                    StringBuilder lsb = new StringBuilder();
                    StringWriter lsw = new StringWriter(lsb);
                    HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                    Page lpagina = new Page();
                    HtmlForm lform = new HtmlForm();
                    lpagina.EnableEventValidation = false;
                    lpagina.Controls.Add(lform);
                    dtgSuministro.Columns[0].Visible = false;
                    dtgSuministro.Columns[4].Visible = true;
                    dtgSuministro.Columns[17].Visible = false;
                    dtgSuministro.Columns[18].Visible = false;
                    dtgSuministro.Columns[25].Visible = false;

                    lform.Controls.Add(dtgSuministro);
                    lpagina.RenderControl(lhtw);
                    Response.Clear();

                    Response.Buffer = true;
                    Response.ContentType = "aplication/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                    Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                    Response.ContentEncoding = System.Text.Encoding.Default;

                    Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                    Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
                    Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + "Contratos Disponibles para Declaración de Información Suministro" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                    Response.Write(lsb.ToString());
                    Response.End();
                    Response.Flush();
                    dtgSuministro.Columns[0].Visible = false;
                    dtgSuministro.Columns[4].Visible = true;
                    dtgSuministro.Columns[17].Visible = false;
                    dtgSuministro.Columns[18].Visible = false;
                    dtgSuministro.Columns[25].Visible = true;

                }
                if (ddlDestino.SelectedValue == "T")
                {
                    string lsNombreArchivo = Session["login"] + "InfExcelDecInf" + DateTime.Now + ".xls";
                    StringBuilder lsb = new StringBuilder();
                    StringWriter lsw = new StringWriter(lsb);
                    HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                    Page lpagina = new Page();
                    HtmlForm lform = new HtmlForm();
                    lpagina.EnableEventValidation = false;
                    lpagina.Controls.Add(lform);
                    dtgTransporteTramo.Visible = true;
                    dtgTransporteTramo.EnableViewState = false;
                    lform.Controls.Add(dtgTransporteTramo);
                    lpagina.RenderControl(lhtw);
                    Response.Clear();

                    Response.Buffer = true;
                    Response.ContentType = "aplication/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                    Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                    Response.ContentEncoding = System.Text.Encoding.Default;

                    Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                    Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
                    Response.Write("<table><tr><th colspan='5' align='left'><font face=Arial size=4>" + "Contratos Disponibles para Declaración de Información Transporte" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                    Response.Write(lsb.ToString());
                    dtgTransporteTramo.Visible = false;
                    Response.End();
                    Response.Flush();
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "Problemas al Consultar los Contratos. " + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChkTodosSum_CheckedChanged(object sender, EventArgs e)
        {
            //Selecciona o deselecciona todos los items del datagrid segun el checked del control
            CheckBox chk = (CheckBox)(sender);
            foreach (DataGridItem Grilla in this.dtgSuministro.Items)
            {
                if (Grilla.Cells[0].Enabled)
                {
                    CheckBox Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                    Checkbox.Checked = chk.Checked;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChkTodosTra_CheckedChanged(object sender, EventArgs e)
        {
            //Selecciona o deselecciona todos los items del datagrid segun el checked del control
            CheckBox chk = (CheckBox)(sender);
            foreach (DataGridItem Grilla in this.dtgTransporte.Items)
            {
                if (Grilla.Cells[0].Enabled)
                {
                    CheckBox Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                    Checkbox.Checked = chk.Checked;
                }
            }
        }

        ///<summary>
        /// Nombre: ddlDestino_SelectedIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        /// 20210531
        protected void ddlDestino_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarDatos();
        }
        /// <summary>
        /// Nombre: CargarDetSum
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDetSum()
        {
            if (hdfOpeAut.Value == "S")
            {
                string[] lsNombreParametros = { "@P_codigo_verif_contrato" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int };
                string[] lValorParametros = { hdfVerif.Value };
                string lsIdReg = "";
                //string lsUltimoPunto = "";
                try
                {
                    lConexion.Abrir();
                    dtgTransporte.Visible = false;
                    dtgDetalleGas.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetDecInfUvlpSum", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgDetalleGas.DataBind();
                    dtgDetalleGas.Visible = true;
                    lConexion.Cerrar();
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, ex.Message);
                }
            }
        }

        /// <summary>
        /// Nombre: dtgSuministro_EditCommand
        /// Fecha: Agosto 25 de 2015
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar la Complementacion del Registro del Contrato de Suministro con el
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgDetalleGas_EditCommand(object source, DataGridCommandEventArgs e)
        {
            string lsError = "";
            DateTime ldFecha = DateTime.Now;
            if (e.CommandName.Equals("Modificar"))
            {
                try
                {
                    hdfCodContSum.Value = this.dtgDetalleGas.Items[e.Item.ItemIndex].Cells[0].Text;
                    ddlPunto.Enabled = false;
                    ddlPunto.SelectedValue = this.dtgDetalleGas.Items[e.Item.ItemIndex].Cells[2].Text;
                    TxtCantidadCont.Text = this.dtgDetalleGas.Items[e.Item.ItemIndex].Cells[4].Text;
                    TxtEquivalContraSum.Text = this.dtgDetalleGas.Items[e.Item.ItemIndex].Cells[5].Text;
                    TxtCantidadDemanSum.Text = this.dtgDetalleGas.Items[e.Item.ItemIndex].Cells[6].Text;
                    TxtEquivalDemanSum.Text = this.dtgDetalleGas.Items[e.Item.ItemIndex].Cells[7].Text;
                    btnModificaGas.Visible = true;
                    btnCrearGas.Visible = false;
                }
                catch (Exception)
                {
                    Toastr.Error(this, "Problemas en la Recuperación del Registro.");
                }
            }
            if (e.CommandName.Equals("Eliminar"))
            {
                try
                {
                    string[] lsNombreParametros = { "@P_cod_cont_mp_uvlp_sum", "@P_codigo_operador", "@P_cod_cont_mp_uvlp", "@P_accion" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char };
                    String[] lValorParametros = { this.dtgDetalleGas.Items[e.Item.ItemIndex].Cells[0].Text, goInfo.cod_comisionista, hdfIdComplementa.Value, "E" };
                    lConexion.Abrir();
                    goInfo.mensaje_error = "";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetDecInfUvlpSum", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        Toastr.Error(this, "Se presentó un Problema al eliminar Información.! " + goInfo.mensaje_error);
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lLector.Read();
                        lsError = lLector["Error"].ToString();
                        if (!string.IsNullOrEmpty(lsError))
                        {
                            Toastr.Error(this, lsError);
                            lLector.Close();
                            lLector.Dispose();
                        }
                        else
                        {
                            lLector.Close();
                            lLector.Dispose();
                            CargarDetSum();
                        }
                    }
                }
                catch (Exception)
                {
                    Toastr.Error(this, "Problemas en la eliminación del Registro.");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TxtPorcFijo_TextChanged(object sender, EventArgs e)
        {
            TextBox oText;
            string sValor;
            foreach (DataGridItem Grilla in this.dtgDetalleTrans.Items)
            {
                try
                {
                    oText = (TextBox)Grilla.Cells[4].Controls[1];
                    if (oText.Text != Grilla.Cells[17].Text)
                    {
                        Grilla.Cells[17].Text = oText.Text;
                        sValor = Math.Round((Convert.ToDouble(oText.Text) * Convert.ToDouble(Grilla.Cells[14].Text) / 100.0), 3).ToString();
                        Grilla.Cells[5].Text = sValor;
                        oText = (TextBox)Grilla.Cells[6].Controls[1];
                        oText.Focus();
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Grilla.Cells[5].Text = "0";
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TxtPorcVar_TextChanged(object sender, EventArgs e)
        {
            TextBox oText;
            DropDownList oDrop;
            string sValor;
            foreach (DataGridItem Grilla in this.dtgDetalleTrans.Items)
            {
                try
                {
                    oText = (TextBox)Grilla.Cells[6].Controls[1];
                    if (oText.Text != Grilla.Cells[18].Text)
                    {
                        Grilla.Cells[18].Text = oText.Text;
                        sValor = Math.Round((Convert.ToDouble(oText.Text) * Convert.ToDouble(Grilla.Cells[15].Text) / 100.0), 3).ToString();
                        Grilla.Cells[7].Text = sValor;
                        oDrop = (DropDownList)Grilla.Cells[19].Controls[1];
                        oDrop.Focus();
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Grilla.Cells[7].Text = "0";
                }
            }
        }

        /// <summary>
        /// Nombre: CargarDetSum
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDetTra()
        {
            string[] lsNombreParametros = { "@P_codigo_verif_contrato" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int };
            string[] lValorParametros = { hdfVerif.Value };
            TextBox oText;
            try
            {
                lConexion.Abrir();
                dtgDetalleTrans.Visible = true;
                dtgDetalleTrans.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetDecInfUvlpTra", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgDetalleTrans.DataBind();
                DropDownList ddrop;

                foreach (DataGridItem Grilla in this.dtgDetalleTrans.Items)
                {
                    oText = (TextBox)Grilla.Cells[4].Controls[1];
                    oText.Text = Grilla.Cells[17].Text;
                    oText = (TextBox)Grilla.Cells[6].Controls[1];
                    oText.Text = Grilla.Cells[18].Text;
                    ddrop = (DropDownList)Grilla.Cells[19].Controls[1];
                    LlenarControles(lConexion.gObjConexion, ddrop, "m_cargo_uvlp", " estado = 'A' and codigo_cargo = 1 order by porcentaje", 2, 2);
                    try
                    {
                        ddrop.SelectedValue = Grilla.Cells[21].Text;
                    }
                    catch (Exception ex)
                    {
                    }
                    ddrop = (DropDownList)Grilla.Cells[20].Controls[1];
                    LlenarControles(lConexion.gObjConexion, ddrop, "m_cargo_uvlp", " estado = 'A' and codigo_cargo = 2 order by porcentaje", 2, 2);
                    try
                    {
                        ddrop.SelectedValue = Grilla.Cells[22].Text;
                    }
                    catch (Exception ex)
                    {
                    }
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "Se presentó un error al consultar los datos. " + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlOperador_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}