﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_IngresoOfertaVenta_4 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    String strRutaArchivo;
    String strRutaArchivo1;
    SqlDataReader lLector;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            if (this.Request.QueryString["ID"] != null && this.Request.QueryString["ID"].ToString() != "")
            {
                hndID.Value = this.Request.QueryString["ID"];
                hndAccion.Value = this.Request.QueryString["accion"];
                /// Obtengo los Datos del ID Recibido
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_id_rueda", " numero_id = " + hndID.Value + " ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    /// Lleno las etiquetas de la pantalla con los datos del Id.
                    lblRueda.Text = "No. Rueda: " + lLector["numero_rueda"].ToString();
                    hdfNoRueda.Value = lLector["numero_rueda"].ToString();
                    hdfTpoRueda.Value = lLector["codigo_tipo_rueda"].ToString();
                    lblId.Text = "ID: " + this.Request.QueryString["ID"];
                    lblFecha.Text = "Fecha Rueda: " + lLector["fecha_rueda"].ToString().Substring(0, 10);
                    lblProducto.Text = "Producto: " + lLector["codigo_producto"].ToString() + "-" + lLector["desc_producto"].ToString();
                    lblUnidadMedida.Text = "Unidad Medida: " + lLector["codigo_unidad_medida"].ToString() + "-" + lLector["desc_unidad_medida"].ToString();
                    lblPuntoEntrega.Text = "Punto Entrega: " + lLector["codigo_punto_entrega"].ToString() + "-" + lLector["desc_punto_entrega"].ToString(); lblPeridoEnt.Text = "Periodo Entrega: " + lLector["codigo_periodo"].ToString() + "-" + lLector["desc_periodo"].ToString();
                    lblModalidad.Text = "Modalidad Contractual: " + lLector["codigo_modalidad"].ToString() + "-" + lLector["desc_modalidad"].ToString();
                    lblFechaIni.Text = "Fecha Entrega Inicial: " + lLector["fecha_entrega"].ToString().Substring(0, 10);
                    lblFechaFin.Text = "Fecha Entrega Final: " + lLector["fecha_entrega_fin"].ToString().Substring(0, 10);
                    /// Reviso si se esta creando la oferta o modificando
                    lLector.Close();
                    lLector.Dispose();

                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_tipos_operador tpo, m_operador_subasta opeS", " tpo.sigla='" + Session["TipoOperador"].ToString() + "' and tpo.codigo_tipo_operador = opeS.codigo_tipo_operador and opeS.codigo_tipo_rueda = '" + hdfTpoRueda.Value + "'and opeS.punta ='V'");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        hdfIngDelta.Value = lLector["ing_precio_delta"].ToString();
                    }
                    else
                        hdfIngDelta.Value = "N";
                    lLector.Close();
                    lLector.Dispose();

                    if (hdfIngDelta.Value == "N")
                    {
                        trPrecioVar.Visible = true;
                        TxtPrecioVar.Text = "0";
                    }
                    /// Reviso si ya existe una oferta para el ID y Operador para obtener
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_posturas_venta", " numero_id = " + hndID.Value + " And codigo_operador = '" + goInfo.cod_comisionista + "' ");
                    if (lLector.HasRows)
                    {
                        hndAccion.Value = "M";
                        lLector.Read();
                        TxtCntDisp.Text = lLector["cantidad_postura"].ToString();
                        TxtPrecioRes.Text = lLector["precio"].ToString();
                        TxtPrecioVar.Text = lLector["precio_delta"].ToString();
                        hdfNoPostura.Value = lLector["numero_postura"].ToString();
                        btnOfertar.Text = "Modificar Oferta";
                    }
                    else
                    {
                        hndAccion.Value = "C";
                        hdfNoPostura.Value = "0";
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                }
                else
                    Toastr.Warning(this, "El ID enviado NO existe en el sistema!");

                lConexion.Cerrar();
            }
            else
                Toastr.Warning(this, "No Se enviaron los Parametros requeridos.!");
            //hndCodSubasta.Value = "21";
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOfertar_Click(object sender, EventArgs e)
    {

        string sCerrar = "N";
        string oError = "";
        string sProc = "";
        string lsError = "";
        string lsNoPostura = "";

        lsError = ValidarEntradas();
        if (lsError == "")
        {
            string[] lsNombreParametros = { "@P_numero_postura", "@P_numero_id", "@P_numero_rueda", "@P_codigo_operador", "@P_cantidad_postura", "@P_precio", "@P_precio_delta" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Float, SqlDbType.Float };
            string[] lValorParametros = { hdfNoPostura.Value, hndID.Value, hdfNoRueda.Value, goInfo.cod_comisionista, TxtCntDisp.Text.Replace(",", ""), TxtPrecioRes.Text.Replace(",", ""), TxtPrecioVar.Text.Replace(",", "") };

            lConexion.Abrir();
            try
            {
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetPosturaVentaId4", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    oError = "Error al Actualizar la Postura. " + goInfo.mensaje_error.ToString() + "\\n";
                }
                else
                {
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        oError = lLector["mensaje"].ToString();
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        Session["refrescar"] = "S";
                        Session["mis_ofertas"] = "S";
                        if (hndAccion.Value == "C")
                            Toastr.Success(this, "Posturas Ingresadas Correctamente.!");
                        else
                            Toastr.Success(this, "Posturas Actualizadas Correctamente.!");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                oError = "Error al realizar la oferta. " + ex.Message;
                lConexion.Cerrar();
            }
            if (oError != "")
            {
                Toastr.Warning(this, oError);
            }   
        }
        else
            Toastr.Warning(this, lsError);

    }
    /// <summary>
    /// Relaiza la validacion del Ingreso de las Posturas
    /// </summary>
    /// <returns></returns>
    protected string ValidarEntradas()
    {
        string lsError = "";
        int oValor = 0;
        double oValor1 = 0;

        if (TxtCntDisp.Text.Trim().Length > 0)
        {
            try
            {
                oValor = Convert.ToInt32(TxtCntDisp.Text.Replace(",", ""));
                if (oValor <= 0)
                    lsError += "La Cantidad Disponible debe ser mayor que 0. \\n";

            }
            catch (Exception ex)
            {
                lsError += "Valor Invalido en la Cantidad Disponible. \\n";
            }
        }
        else
            lsError += "Debe Ingresar la Cantidad Disponible. \\n";
        if (TxtPrecioRes.Text.Trim().Length > 0)
        {
            string sOferta = TxtPrecioRes.Text.Replace(",", "");
            int iPos = sOferta.IndexOf(".");
            if (iPos > 0)
                if (sOferta.Length - iPos > 3)
                    lsError += "Se permiten máximo 2 decimales en el precio de reserva\\n";
            try
            {
                if (Convert.ToDouble(sOferta) <= 0)
                    lsError += "El precio de reserva debe ser mayor que cero\\n";
            }
            catch (Exception ex)
            {
                lsError += "El precio de reserva digitado no es válido\\n";
            }

        }
        else
            lsError += "Debe Ingresar el precio de reserva. \\n";

        if (trPrecioVar.Visible)
        {
            if (TxtPrecioVar.Text.Trim().Length > 0)
            {
                string sOferta = TxtPrecioRes.Text.Replace(",", "");
                int iPos = sOferta.IndexOf(".");
                if (iPos > 0)
                    if (sOferta.Length - iPos > 3)
                        lsError += "Se permiten máximo 2 decimales en la variacion del precio\\n";
                try
                {
                    Convert.ToDouble(sOferta);
                }
                catch (Exception ex)
                {
                    lsError += "La variacion del precio  no es válido\\n";
                }
            }
            else
                lsError += "Debe Ingresar la variación del precio. \\n";
        }
        return lsError;
    }
}