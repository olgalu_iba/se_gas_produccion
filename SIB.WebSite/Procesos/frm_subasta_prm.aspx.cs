﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;
//using System.Windows.Forms;



public partial class Procesos_frm_subasta_prm : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            if (!IsPostBack)
            {

                HndFechaRueda.Value = DateTime.Now.ToShortDateString();
                HndFechaRueda.Value = HndFechaRueda.Value.Substring(6, 4) + "/" + HndFechaRueda.Value.Substring(3, 2) + "/" + HndFechaRueda.Value.Substring(0, 2);
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlRueda, "t_rueda rue", "rue.fecha_rueda = '" + HndFechaRueda.Value + "' and rue.codigo_tipo_subasta = 2 ",1,17);
                lConexion.Cerrar();
                if (Session["tipoPerfil"].ToString() == "B")
                    lblOperador.Text = Session["nomOperador"].ToString();
                else
                    lblOperador.Text = Session["nomOperador"].ToString();
                if (Session["refrescar_prm"].ToString() == "S")
                {
                    Session["refrescar_cont"] = "S";
                    ddlRueda_SelectedIndexChanged(sender, e);
                }
            }
        }
        catch (Exception ex)
        {
 
        }
    }

    /// <summary>
    /// Nombre: ddlRueda_SelectedIndexChanged
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Obtiene la Informacion de la Inactivacion del Comisionista para Visualizarla en la pantalla
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlRueda_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["codigo_punto"] = "0";
        //Actualiza los datos de la subasta seleccionda 
        if (Session["refrescar_prm"].ToString() == "S")
        {
            Session["refrescar_prm"] = "N";
            ddlRueda.SelectedValue = Session["numero_rueda"].ToString();
        }
        Session["refrescar_cont"] = "S";
        ImgExcel.Visible = false; //20160128
        SqlDataReader lLector;
        lConexion.Abrir();
        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda rue,m_tipos_rueda tpo, m_estado_gas est", " rue.numero_rueda =" + ddlRueda.SelectedValue + " and rue.codigo_tipo_rueda = tpo.codigo_tipo_rueda and est.tipo_estado ='R' and rue.estado = est.sigla_estado");
        if (lLector.HasRows)
        {
            lLector.Read();
            lblFase.Text = lLector["estado"].ToString() + " - " + lLector["descripcion_estado"].ToString();
            Session["estado_rueda"] = lLector["estado"].ToString(); //20170508 se sube para corregir error de actualizacion
            Session["destino_rueda"] = lLector["destino_rueda"].ToString(); //20170508 se sube para corregir error de actualizacion

            switch (lLector["estado"].ToString())
            {
                case "1": lblHora.Text = lLector["hora_ini_oferta_venta"].ToString() + " - " + lLector["hora_fin_publi_v_sci"].ToString();
                    if (Session["destino_rueda"].ToString()=="G")
                        lblFase.Text = "1 - Declaración Información Productor";
                    else
                        lblFase.Text = "1 - Declaración Información Transportador";
                    break;
                case "2": lblHora.Text = lLector["hora_ini_cont_venta"].ToString() + " - " + lLector["hora_ini_publ_cnt_dispi_v_sci"].ToString();
                    if (Session["destino_rueda"].ToString() == "G")
                        lblFase.Text = "2 - Declaración Precio de reserva y cantidad no disponible (Generador Térmico)";
                    else
                        lblFase.Text = "2 - Declaración de cantidad no disponible (Generador Térmico)";

                    break;
                case "3": lblHora.Text = lLector["hora_ini_publica_venta"].ToString() + " - " + lLector["hora_fin_publ_cnt_dispi_v_sci"].ToString();
                    ImgExcel.Visible = true; //20160128
                    break;
                case "4": lblHora.Text = lLector["hora_ini_oferta_compra"].ToString() + " - " + lLector["hora_fin_rec_solicitud_c_sci"].ToString();
                    break;
                case "5": lblHora.Text = lLector["hora_ini_calce"].ToString() + " - " + lLector["hora_fin_negociacioni_sci"].ToString();
                    break;
                case "6": lblHora.Text = lLector["hora_ini_modif_cont"].ToString() + " - " + lLector["hora_ini_negociacioni_sci"].ToString();
                    break;
                default: lblHora.Text = "";
                    break;
            }
            //lblHora.Text = lLector["hora_prox_fase"].ToString();
            if (lLector["estado"].ToString() != "1" && lLector["estado"].ToString() != "2")
            {
                btnDecInf.Visible = false;
                btnDecMan.Visible = false;
                btnDecPre.Visible = false;
            }
            //else
            //{
            //    if (lLector["estado"].ToString() == "1")
            //        if (lLector["destino_rueda"].ToString() == "G")
            //        {
            //            lblFase.Text += " sobre suministro de gas";
            //        }
            //        else
            //        {
            //            lblFase.Text += " sobre trasporte de gas";
            //        }
            //}
            HndTpoRueda.Value = lLector["codigo_tipo_rueda"].ToString();
        }
        else
        {
            lblFase.Text = "";
            lblHora.Text = "";
            Session["estado_rueda"] = "F";
            Session["destino_rueda"] = "G";
            btnDecInf.Visible = false;
            btnDecMan.Visible = false;
            btnDecPre.Visible = false;
            HndTpoRueda.Value = "0";
        }
        lLector.Close();
        Session["numero_rueda"] = ddlRueda.SelectedValue;
        if (Session["estado_rueda"].ToString() == "1" || Session["estado_rueda"].ToString() == "2")
        {
            if (Session["numero_rueda"].ToString() != "0")
            {
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador ope,m_tipos_operador tpo, m_operador_subasta opeS", "ope.codigo_operador=" + goInfo.cod_comisionista + " and ope.tipo_operador = tpo.sigla and tpo.codigo_tipo_operador = opeS.codigo_tipo_operador and opeS.codigo_tipo_rueda = " + HndTpoRueda.Value + " and opeS.estado='A'");
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                    {
                        if (lLector["punta"].ToString() == "D")
                        {
                            btnDecInf.Visible = true;
                            btnDecMan.Visible = true;
                        }
                        if (lLector["punta"].ToString() == "V")
                            btnDecPre.Visible = true;
                    }
                }
            }
        }
        if (goInfo.cod_comisionista != "0" || Session["estado_rueda"].ToString() == "F" || Session["estado_rueda"].ToString() == "Z")
        {
            btnSuspender.Visible = false;
            btnReactivar.Visible = false;
        }
        else
            if (Session["estado_rueda"].ToString() == "S")
            {
                btnSuspender.Visible = false;
                btnReactivar.Visible = true;
            }
            else
            {
                btnSuspender.Visible = true;
                btnReactivar.Visible = false;
            }
        lConexion.Cerrar();
        lConexion.Abrir();
        ddlPunto.Items.Clear();
        if (Session["destino_rueda"].ToString() == "G")
        {
            LlenarControles(lConexion.gObjConexion, ddlPunto, "m_pozo", "  estado ='A' order by descripcion", 0, 1);
            lblPunto.Text = "Punto: ";
        }
        if (Session["destino_rueda"].ToString() == "T")
        {
            LlenarControles(lConexion.gObjConexion, ddlPunto, "m_ruta_snt", "  estado ='A' order by descripcion", 0, 4);
            lblPunto.Text = "Ruta: ";
        }
        lConexion.Cerrar();

        if (Session["tipoPerfil"].ToString() == "B")
        {
            btnDecInf.Visible = false;
            btnDecMan.Visible = false;
            btnDecPre.Visible = false;
        }
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "OPEN_WINDOW", "window.open('frm_posturas.aspx?ID=0&subyacente=0&ciudad=0&punta=&tipo=" + lblTipo.Text + "','postura');", true);
        //Session["refrescar"] = "S";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.titulo.location= 'frm_titulos.aspx';", true);

    }
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// Nombre: LlenarControles1
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsProc, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        string[] lsNombreParametros = { "@P_condicion" };
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar };
        string[] lValorParametros = { lsCondicion };

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, lsProc, lsNombreParametros, lTipoparametros, lValorParametros);
        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para Procesar la consulta
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Session["codigo_punto"] = ddlPunto.SelectedValue;
        Session["refrescar"] = "S";
        Session["refresca_mis_posturas"] = "S";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.titulo.location='frm_titulos.aspx';", true);
    }
    /// 20160128
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_punto" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { ddlRueda.SelectedValue, Session["codigo_punto"].ToString() };
        lConexion.Abrir();
        dtgSubasta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaPujaExc", lsNombreParametros, lTipoparametros, lValorParametros);
        dtgSubasta.DataBind();
        dtgSubasta.Visible = true;

        DataSet lds = new DataSet();
        SqlDataAdapter lsqldata = new SqlDataAdapter();
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfRueda" + DateTime.Now + ".xls";
        string lstitulo_informe = "Consulta Ids Subasta " + ddlRueda.SelectedItem.Text;
        decimal ldCapacidad = 0;
        StringBuilder lsb = new StringBuilder();
        ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
        StringWriter lsw = new StringWriter(lsb);
        HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
        Page lpagina = new Page();
        HtmlForm lform = new HtmlForm();
        lpagina.EnableEventValidation = false;
        lpagina.DesignerInitialize();
        lpagina.Controls.Add(lform);
        dtgSubasta.EnableViewState = false;
        lform.Controls.Add(dtgSubasta);
        lpagina.RenderControl(lhtw);
        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "aplication/vnd.ms-excel";
        Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
        Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
        Response.ContentEncoding = System.Text.Encoding.Default;
        Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
        Response.Charset = "UTF-8";
        Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
        Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
        Response.ContentEncoding = Encoding.Default;
        Response.Write(lsb.ToString());
        Response.End();
        lds.Dispose();
        lsqldata.Dispose();
        lConexion.CerrarInforme();
        dtgSubasta.Visible = false;
    }

}
