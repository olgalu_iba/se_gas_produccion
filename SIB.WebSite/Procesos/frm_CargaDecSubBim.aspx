﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_CargaDecSubBim.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Procesos_frm_CargaDecSubBim" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" Text="CARGA ARCHIVO DECLARACION DE INFORMACION SUBASTA BIMESTRAL" />

                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Archivo Contratos:</label>
                            <asp:FileUpload ID="FuArchivo" runat="server" EnableTheming="true" />
                            <asp:Button ID="BtnCargar" runat="server" Text="Cargue Archivos" OnClick="BtnCargar_Click"
                                OnClientClick="this.disabled = true;" UseSubmitBehavior="false" /><br />
                            <asp:HiddenField ID="hndID" runat="server" />
                            <asp:Label ID="ltCargaArchivo" runat="server" Width="100%" ForeColor="red"></asp:Label>
                            <asp:HiddenField ID="hdfNomArchivo" runat="server" />
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
