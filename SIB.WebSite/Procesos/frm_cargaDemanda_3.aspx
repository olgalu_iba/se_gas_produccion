﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_cargaDemanda_3.aspx.cs" Inherits="Procesos_frm_cargaDemanda_3" MasterPageFile="~/PlantillaPrincipal.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTituo" Text="CARGA POSTURAS DE COMPRA" runat="server" />

                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Archivo</label>
                            <asp:FileUpload ID="FuArchivo" runat="server" EnableTheming="true" />
                            <asp:Button ID="BtnCargar" runat="server" Text="Cargue Archivo" OnClick="BtnCargar_Click" /><br />
                            <asp:HiddenField ID="hndID" runat="server" />
                            <asp:Label ID="ltCargaArchivo" runat="server" Width="100%" ForeColor="white"></asp:Label>
                            <asp:HiddenField ID="hdfNomArchivo" runat="server" />

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
