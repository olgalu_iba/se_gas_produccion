﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using SIB.BASE.Negocio.Manejador;
using SIB.BASE.Negocio.Repositorio;


public partial class Procesos_frm_ModifContrato : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = "Modifcación de contratos";

        //Establese los permisos del sistema
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            lConexion.Abrir();
            ddlRueda.Items.Clear();
            LlenarControles(lConexion.gObjConexion, ddlRueda, "m_tipos_rueda tpo, t_rueda rue", "rue.estado = '6' and tpo.codigo_tipo_rueda = rue.codigo_tipo_rueda Order by rue.numero_rueda", 18, 1);
            lConexion.Cerrar();
        }
    }
    /// <summary>
    /// Nombre: btnBuscar_Click
    /// Fecha: mayo 6 de 2014
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: COnuslta los Id para modificar contratos
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje1.Text = "";
        string[] lsNombreParametros = { "@P_numero_rueda", "@P_numero_id" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { ddlRueda.SelectedValue, txtId.Text };
        if (txtId.Text != "")
        {
            try
            {
                Convert.ToInt32(txtId.Text);
            }
            catch
            {
                lblMensaje1.Text = "Número de ID no válido";
            }
        }
        else
            lValorParametros[1] = "0";

        if (lblMensaje1.Text == "")
        {
            try
            {
                lConexion.Abrir();
                dgIds.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetIDModifCont", lsNombreParametros, lTipoparametros, lValorParametros);
                dgIds.DataBind();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Error al consultar los IDs. " + ex.Message;
            }
        }
    }

    /// <summary>
    /// Nombre: dgIds_EditCommand
    /// Fecha: mayo de 2014
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: seleccuia los ID
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dgIds_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        tblParametros.Visible = false;
        tblModifica.Visible = true;
        hndId.Value = this.dgIds.Items[e.Item.ItemIndex].Cells[0].Text;
        lConexion.Abrir();
        string[] lsNombreParametros = { "@P_numero_id" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        string[] lValorParametros = { hndId.Value };
        if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_GetModifCont", lsNombreParametros, lTipoparametros, lValorParametros))
            lblMensaje.Text = "Se presento un Problema al traer los datos.!";
        else
        {
            string[] lsNombreParametros1 = { "@P_numero_id", "@P_punta" };
            SqlDbType[] lTipoparametros1 = { SqlDbType.Int, SqlDbType.Char };
            string[] lValorParametros1 = { hndId.Value, "V" };

            dgadjV.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetAdjID", lsNombreParametros1, lTipoparametros1, lValorParametros1);
            dgadjV.DataBind();
            lValorParametros1[1] = "C";
            dgadjC.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetAdjID", lsNombreParametros1, lTipoparametros1, lValorParametros1);
            dgadjC.DataBind();
            lValorParametros1[1] = "O";
            DgCont.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetAdjID", lsNombreParametros1, lTipoparametros1, lValorParametros1);
            DgCont.DataBind();
            ddlOperadorV.Items.Clear();
            LlenarControles(lConexion.gObjConexion, ddlOperadorV, "m_operador ope, t_posturas_venta posV", "posV.numero_id= " + hndId.Value + " and posV.estado ='A' and posV.codigo_operador = ope.codigo_operador ", 0, 4);
            ddlOperadorC.Items.Clear();
            LlenarControles(lConexion.gObjConexion, ddlOperadorC, "m_operador ope, t_posturas_compra posC", "posC.numero_id= " + hndId.Value + " and posC.estado ='A' and posC.codigo_operador = ope.codigo_operador ", 0, 4);
            dgDetCont.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContTmp", lsNombreParametros, lTipoparametros, lValorParametros);
            dgDetCont.DataBind();
            imbModificar.Visible = false;
            imbAgregar.Visible = true;

        }
        lConexion.Cerrar();
    }
    /// <summary>
    /// Nombre: btnCrear_Click
    /// Fecha: mayo 6 de 2014
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Crea un nuevo contrato
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbAgregar_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_numero_contrato", "@P_numero_id", "@P_operador_v", "@P_operador_c", "@P_cantidad", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char };
        string[] lValorParametros = { "0", hndId.Value, ddlOperadorV.SelectedValue, ddlOperadorC.SelectedValue, txtCantidad.Text, "C" };
        string[] lsNombreParametros1 = { "@P_numero_id", "@P_punta" };
        SqlDbType[] lTipoparametros1 = { SqlDbType.Int, SqlDbType.Char };
        string[] lValorParametros1 = { hndId.Value, "V" };
        string[] lsNombreParametros2 = { "@P_numero_id" };
        SqlDbType[] lTipoparametros2 = { SqlDbType.Int };
        string[] lValorParametros2 = { hndId.Value };

        lblMensaje.Text = "";

        if (ddlOperadorV.SelectedValue == "0")
            lblMensaje.Text += "Debe selecccionar el Operador de la venta <br>";
        if (ddlOperadorC.SelectedValue == "0")
            lblMensaje.Text += "Debe selecccionar el Operador de la compra<br>";
        try
        {
            Convert.ToInt32(txtCantidad.Text);
        }
        catch
        {
            lblMensaje.Text += "Valor de cantidad no valido";
        }

        if (lblMensaje.Text == "")
        {
            try
            {
                lConexion.Abrir();
                DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContTmp", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                //Actualiza las grillas
                dgadjV.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetAdjID", lsNombreParametros1, lTipoparametros1, lValorParametros1);
                dgadjV.DataBind();
                lValorParametros1[1] = "C";
                dgadjC.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetAdjID", lsNombreParametros1, lTipoparametros1, lValorParametros1);
                dgadjC.DataBind();
                dgDetCont.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContTmp", lsNombreParametros2, lTipoparametros2, lValorParametros2);
                dgDetCont.DataBind();
                lConexion.Cerrar();
                ddlOperadorC.SelectedValue = "0";
                ddlOperadorV.SelectedValue = "0";
                txtCantidad.Text = "0";
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Error al Crear el contrato. " + ex.Message;
            }
        }
    }

    /// <summary>
    /// Nombre: btnModificar_Click
    /// Fecha: mayo 6 de 2014
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: modifica un nuevo contrato
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbModificar_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_numero_contrato", "@P_numero_id", "@P_operador_v", "@P_operador_c", "@P_cantidad", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char };
        string[] lValorParametros = { hndContrato.Value, hndId.Value, ddlOperadorV.SelectedValue, ddlOperadorC.SelectedValue, txtCantidad.Text, "M" };
        string[] lsNombreParametros1 = { "@P_numero_id", "@P_punta" };
        SqlDbType[] lTipoparametros1 = { SqlDbType.Int, SqlDbType.Char };
        string[] lValorParametros1 = { hndId.Value, "V" };
        string[] lsNombreParametros2 = { "@P_numero_id" };
        SqlDbType[] lTipoparametros2 = { SqlDbType.Int };
        string[] lValorParametros2 = { hndId.Value };

        lblMensaje.Text = "";
        if (ddlOperadorV.SelectedValue == "0")
            lblMensaje.Text += "Debe selecccionar el Operador de la venta<br>";
        if (ddlOperadorC.SelectedValue == "0")
            lblMensaje.Text += "Debe selecccionar el Operador de la compra<br>";
        try
        {
            Convert.ToInt32(txtCantidad.Text);
        }
        catch
        {
            lblMensaje.Text += "Valor de cantidad no valido";
        }

        if (lblMensaje.Text == "")
        {
            try
            {
                lConexion.Abrir();
                DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetContTmp", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                //Actualiza las grillas
                dgadjV.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetAdjID", lsNombreParametros1, lTipoparametros1, lValorParametros1);
                dgadjV.DataBind();
                lValorParametros1[1] = "C";
                dgadjC.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetAdjID", lsNombreParametros1, lTipoparametros1, lValorParametros1);
                dgadjC.DataBind();
                dgDetCont.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContTmp", lsNombreParametros2, lTipoparametros2, lValorParametros2);
                dgDetCont.DataBind();
                lConexion.Cerrar();
                imbModificar.Visible = false;
                imbAgregar.Visible = true;
                ddlOperadorC.SelectedValue = "0";
                ddlOperadorV.SelectedValue = "0";
                txtCantidad.Text = "0";
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Error al modificar  el contrato. " + ex.Message;
            }
        }
    }
    /// <summary>
    /// Nombre: dgIds_EditCommand
    /// Fecha: mayo de 2014
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: seleccuia los ID
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dgDetCont_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        tblParametros.Visible = false;
        tblModifica.Visible = true;
        hndContrato.Value = this.dgDetCont.Items[e.Item.ItemIndex].Cells[0].Text;

        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            string[] lsNombreParametros = { "@P_numero_contrato", "@P_numero_id" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { hndContrato.Value, hndId.Value };
            string[] lsNombreParametros1 = { "@P_numero_id", "@P_punta" };
            SqlDbType[] lTipoparametros1 = { SqlDbType.Int, SqlDbType.Char };
            string[] lValorParametros1 = { hndId.Value, "V" };
            string[] lsNombreParametros2 = { "@P_numero_id" };
            SqlDbType[] lTipoparametros2 = { SqlDbType.Int };
            string[] lValorParametros2 = { hndId.Value };

            lConexion.Abrir();
            DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_DelContTmp", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
            //Actualiza las grillas
            dgadjV.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetAdjID", lsNombreParametros1, lTipoparametros1, lValorParametros1);
            dgadjV.DataBind();
            lValorParametros1[1] = "C";
            dgadjC.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetAdjID", lsNombreParametros1, lTipoparametros1, lValorParametros1);
            dgadjC.DataBind();
            dgDetCont.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetContTmp", lsNombreParametros2, lTipoparametros2, lValorParametros2);
            dgDetCont.DataBind();
            lConexion.Cerrar();
        }
        else
        {
            imbAgregar.Visible = false;
            imbModificar.Visible = true;
            ddlOperadorV.SelectedValue = this.dgDetCont.Items[e.Item.ItemIndex].Cells[1].Text;
            ddlOperadorC.SelectedValue = this.dgDetCont.Items[e.Item.ItemIndex].Cells[3].Text;
            txtCantidad.Text = this.dgDetCont.Items[e.Item.ItemIndex].Cells[5].Text;
        }
    }
    /// <summary>
    /// Nombre: btnRegresar_Click
    /// Fecha: mayo 6 de 2014
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: modifica un nuevo contrato
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbRegresar_Click(object sender, ImageClickEventArgs e)
    {
        tblModifica.Visible = false;
        tblParametros.Visible = true;
    }


    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// <summary>
    /// Nombre: btnCrear_Click
    /// Fecha: mayo 6 de 2014
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Crea un nuevo contrato
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbModificarCont_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_numero_id" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        string[] lValorParametros = { hndId.Value };

        lblMensaje.Text = "";

        try
        {
            lConexion.Abrir();
            SqlDataReader lLector;
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetModifCont", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
            if (lLector.HasRows)
            {
                while (lLector.Read())
                {
                    lblMensaje.Text += lLector["error"].ToString() + "<br>";
                }
            }
            else
            {
                tblModifica.Visible = false;
                tblParametros.Visible = true;
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Error al actualizar los contratos. " + ex.Message;
        }
    }


}