﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using PCD_Infraestructura.Business;
using PCD_Infraestructura.Transaction;
using PCD_Infraestructura.DomainLayer;
using System.Diagnostics;

public partial class Procesos_frm_act_tab_5 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    String strRutaArchivo1;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        strRutaArchivo1 = ConfigurationManager.AppSettings["RutaArchivos"].ToString();
    }
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        try
        {
            //string oTexto = "";
            //oTexto = "xx-Usuario " + goInfo.Usuario + " -Hora Ejecucion: " + DateTime.Now.ToString() + " -Mis Ofe:" + Session["mis_ofertas"] +".";
            if (Session["refresca_mis_posturas"].ToString() == "S")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.posturas.location= 'frm_mis_posturas_5.aspx';", true);
                Session["refresca_mis_posturas"] = "N";
            }
            else
            {

                string oArchivo = strRutaArchivo1 + "Mensaje.txt";
                if (Session["hora_mensaje"].ToString() != File.GetCreationTime(oArchivo).ToString("HH:mm:ss"))
                {
                    Session["hora_mensaje"] = File.GetCreationTime(oArchivo).ToString("HH:mm:ss");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.mensajes.location= 'frm_mensajes_5.aspx';", true);
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
}