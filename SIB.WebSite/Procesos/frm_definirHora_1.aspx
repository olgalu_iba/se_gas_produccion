﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_definirHora_1.aspx.cs"
    Inherits="Procesos_frm_definirHora_1" EnableEventValidation="false" MasterPageFile="~/PlantillaPrincipal.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" language="javascript">
        //        document.location = no;
        function confirmar() {
            return confirm("Esta seguro que desea definir la hora de apertura de la proxima fase!")
        }
        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }
        function formatoMiles(obj, evt) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /([0-9]*)$/i.exec(strValue)) != null) {
                obj.value = addCommas(T[1]);
            } else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }

    </script>

    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td6" colspan="2" align="center">
                        <asp:Label ID="lblTitulo" runat="server" Text="DEFINICION DE HORAS DE INICIO" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="1">
                        <asp:Label ID="lblRonda" runat="server"></asp:Label>
                    </td>
                    <td class="td6" colspan="1">
                        <asp:Label ID="lblHoraFin" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="1">
                        Hora Inicio prox fase:
                    </td>
                    <td class="td6" colspan="1">
                        <asp:TextBox ID="TxtHora" runat="server" Width="100px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RevTxtHora" ControlToValidate="TxtHora" runat="server"
                            ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                            ErrorMessage="Formato Incorrecto para la hora"> Formato Incorrecto para la hora </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="td6" colspan="1">
                        Duración minutos Prox fase:
                    </td>
                    <td class="td6" colspan="1">
                        <asp:TextBox ID="TxtMinutos" runat="server" Width="100px" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" class="td6">
                        <br />
                        <asp:Button ID="btnHora" Text="Cambiar" runat="server" OnClientClick="return confirmar();"
                            OnClick="btnHora_Click" ValidationGroup="comi" causesvalidation="true"
/>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
 </asp:Content>