﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;

public partial class Procesos_frm_AprobacionHabOperBim : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Aprobación Habilitación Rol de Operadores subasta Bimestral";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlTipo, "m_tipos_operador", " estado = 'A' order by descripcion ", 2, 1);
            LlenarControles(lConexion.gObjConexion, ddlRueda, "t_rueda", " estado not in ('F','Z') And codigo_tipo_subasta = 6 order by fecha_prox_apertura,numero_rueda", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' And codigo_operador != 0 order by razon_social", 0, 4);
            lConexion.Cerrar();
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            if (lsTabla == "t_rueda")
            {
                lItem1.Value = lLector["numero_rueda"].ToString();
                lItem1.Text = lLector["numero_rueda"].ToString() + "-" + lLector["descripcion"].ToString();
            }
            else
            {
                if (lsTabla == "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
            }
            lDdl.Items.Add(lItem1);
        }

        lLector.Close();
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        CargarDatos();
    }
    /// <summary>
    /// 
    /// </summary>
    protected void CargarDatos()
    {
        lblMensaje.Text = "";
        if (ddlRueda.SelectedValue == "0")
            lblMensaje.Text = "Debe seleccionar la rueda.";
        if (lblMensaje.Text == "")
        {
            try
            {
                hdfRueda.Value = ddlRueda.SelectedValue; //Se asigna aqui para al hacer la aoribacuón se tome la rueda con la que se conuslto
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetOperadorRol";
                lComando.Parameters.Add("@P_tipo_operador", SqlDbType.VarChar).Value = ddlTipo.SelectedValue;
                lComando.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = ddlOperador.SelectedValue;
                lComando.Parameters.Add("@P_numero_rueda", SqlDbType.VarChar).Value = ddlRueda.SelectedValue;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgMaestro.DataSource = lds;
                dtgMaestro.DataBind();
                if (dtgMaestro.Items.Count > 0)
                {
                    tblGrilla.Visible = true;
                    imbExcel.Visible = true;
                    btnAprobar.Visible = true;
                    btnRechazar.Visible = true;
                    DropDownList oDrop;
                    foreach (DataGridItem Grilla in this.dtgMaestro.Items)
                    {
                        oDrop = (DropDownList)Grilla.Cells[6].Controls[1];
                        oDrop.SelectedValue = Grilla.Cells[5].Text;
                    }
                }
                else
                    lblMensaje.Text = "No se encontraron Registros para Visualizar.! ";
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pude Generar el Informe.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: ChkTodos_CheckedChanged
    /// Fecha: Abril 24 21 de 2012
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para chekear todos los registros
    /// Modificacion:
    /// </summary>
    protected void ChkTodos_CheckedChanged(object sender, EventArgs e)
    {
        //Selecciona o deselecciona todos los items del datagrid segun el checked del control
        CheckBox chk = (CheckBox)(sender);
        foreach (DataGridItem Grilla in this.dtgMaestro.Items)
        {
            CheckBox Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
            Checkbox.Checked = chk.Checked;
        }
        btnAprobar.Enabled = true;
        btnRechazar.Enabled = true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfCargueNeg" + DateTime.Now + ".xls";
        string lstitulo_informe = "Informe de rol de operadores habilitados para subasta bimestral";
        try
        {
            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgMaestro.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            dtgMaestro.Columns[0].Visible = false;
            dtgMaestro.Columns[5].Visible = true; //20170328 rq007-17
            dtgMaestro.Columns[6].Visible = false; //20170328 rq007-17
            lform.Controls.Add(dtgMaestro);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());
            Response.End();

            dtgMaestro.Columns[0].Visible = true;
            dtgMaestro.Columns[5].Visible = false; //20170328 rq007-17
            dtgMaestro.Columns[6].Visible = true; //20170328 rq007-17
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Excel.!" + ex.Message.ToString();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAprobar_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        if (ddlPunta.SelectedValue == "0")
            lblMensaje.Text = "Debe seleccionar la punta para aprobar el registro";

        if (lblMensaje.Text == "")
        {
            int liProce = 0;
            string[] lsNombreParametros = { "@P_codigo_operador", "@P_numero_rueda", "@P_punta", "@P_estado", "@P_tipo_declaracion" }; //20170328 rq007-17
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char }; //20170328 rq007-17
            String[] lValorParametros = { "0", hdfRueda.Value, ddlPunta.SelectedValue, "A", "" }; //20170328 rq007-17

            CheckBox Checkbox = null;
            DropDownList oDrop = null;
            if (ddlPunta.SelectedValue == "C")
            {
                foreach (DataGridItem Grilla in this.dtgMaestro.Items)
                {
                    Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                    if (Checkbox.Checked == true)
                    {
                        liProce++;
                        if (Grilla.Cells[3].Text == "S")
                        {
                            lblMensaje.Text += "El Operador " + Grilla.Cells[1].Text + " Ya fue aprobado<br>";
                        }
                    }
                }
            }
            else
            {
                foreach (DataGridItem Grilla in this.dtgMaestro.Items)
                {
                    Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                    if (Checkbox.Checked == true)
                    {
                        liProce++;
                        if (Grilla.Cells[4].Text == "S")
                        {
                            lblMensaje.Text += "El Operador " + Grilla.Cells[1].Text + " Ya fue aprobado<br>";
                        }
                    }
                }
            }
            if (liProce == 0)
                lblMensaje.Text += "Debe seleccionar registros para aprobar<br>";
            if (lblMensaje.Text == "")
            {
                lConexion.Abrir();
                SqlTransaction oTransaccion;
                oTransaccion = lConexion.gObjConexion.BeginTransaction(System.Data.IsolationLevel.Serializable);
                // Defino la trnasacción para el manejo de la consistencia de los datos.
                try
                {
                    foreach (DataGridItem Grilla in this.dtgMaestro.Items)
                    {
                        Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                        oDrop = (DropDownList)Grilla.Cells[6].Controls[1];
                        if (Checkbox.Checked == true)
                        {
                            lValorParametros[0] = Grilla.Cells[1].Text.Trim();
                            lValorParametros[4] = oDrop.SelectedValue;
                            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetOperadorRol", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                            if (goInfo.mensaje_error != "")
                            {
                                oTransaccion.Rollback();
                                lblMensaje.Text = "Se presento un Problema en la Aprobación del Registro {" + Grilla.Cells[1].Text.Trim() + "}.! " + goInfo.mensaje_error.ToString();
                                lConexion.Cerrar();
                                break;
                            }
                            lLector.Close();
                            lLector.Dispose();
                        }
                    }
                    oTransaccion.Commit();
                    lblMensaje.Text = "Registros  Aprobados Correctamente.!";
                    CargarDatos();

                }
                catch (Exception ex)
                {
                    /// Desbloquea el Registro Actualizado
                    oTransaccion.Rollback();
                    lConexion.Cerrar();
                    lblMensaje.Text = ex.Message;
                }
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRechazar_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        if (ddlPunta.SelectedValue == "0")
            lblMensaje.Text = "Debe seleccionar la punta para des-aprobar el registro";

        if (lblMensaje.Text == "")
        {
            int liProce = 0;
            string[] lsNombreParametros = { "@P_codigo_operador", "@P_numero_rueda", "@P_punta", "@P_estado" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Char, SqlDbType.Char };
            String[] lValorParametros = { "0", hdfRueda.Value, ddlPunta.SelectedValue, "E" };

            CheckBox Checkbox = null;
            if (ddlPunta.SelectedValue == "C")
            {
                foreach (DataGridItem Grilla in this.dtgMaestro.Items)
                {
                    Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                    if (Checkbox.Checked == true)
                    {
                        liProce++;
                        if (Grilla.Cells[3].Text == "N")
                        {
                            lblMensaje.Text += "El Operador " + Grilla.Cells[1].Text + " No ha sido aprobado<br>";
                        }
                    }
                }
            }
            else
            {
                foreach (DataGridItem Grilla in this.dtgMaestro.Items)
                {
                    Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                    if (Checkbox.Checked == true)
                    {
                        liProce++;
                        if (Grilla.Cells[4].Text == "N")
                        {
                            lblMensaje.Text += "El Operador " + Grilla.Cells[1].Text + " No ha sido aprobado<br>";
                        }
                    }
                }
            }
            if (liProce == 0)
                lblMensaje.Text += "Debe seleccionar registros para des-aprobar<br>";
            if (lblMensaje.Text == "")
            {
                lConexion.Abrir();
                SqlTransaction oTransaccion;
                oTransaccion = lConexion.gObjConexion.BeginTransaction(System.Data.IsolationLevel.Serializable);
                // Defino la trnasacción para el manejo de la consistencia de los datos.
                try
                {
                    foreach (DataGridItem Grilla in this.dtgMaestro.Items)
                    {
                        Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                        if (Checkbox.Checked == true)
                        {
                            lValorParametros[0] = Grilla.Cells[1].Text.Trim();
                            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetOperadorRol", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                            if (goInfo.mensaje_error != "")
                            {
                                oTransaccion.Rollback();
                                lblMensaje.Text = "Se presento un Problema en la des-aprobación del Registro {" + Grilla.Cells[1].Text.Trim() + "}.! " + goInfo.mensaje_error.ToString();
                                lConexion.Cerrar();
                                break;
                            }
                            lLector.Close();
                            lLector.Dispose();
                        }
                    }
                    oTransaccion.Commit();
                    lblMensaje.Text = "Registros  des-aprobados Correctamente.!";
                    CargarDatos();

                }
                catch (Exception ex)
                {
                    /// Desbloquea el Registro Actualizado
                    oTransaccion.Rollback();
                    lConexion.Cerrar();
                    lblMensaje.Text = ex.Message;
                }
            }
        }
    }

    protected void ddlTipo_SelectedIndexChanged(object sender, EventArgs e)
    {
        lConexion.Abrir();
        ddlOperador.Items.Clear();
        if (ddlTipo.SelectedValue == "0")
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' And codigo_operador != 0 order by razon_social", 0, 4);
        else
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' And codigo_operador != 0 and tipo_operador = '" + ddlTipo.SelectedValue + "'order by razon_social", 0, 4);
        lConexion.Cerrar();
    }
}