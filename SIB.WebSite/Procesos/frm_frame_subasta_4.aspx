<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_frame_subasta_4.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" EnableEventValidation="false" Inherits="Procesos.frm_frame_subasta_4" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/dev/auctionChart.js")%>"></script>
    <script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/dev/clock.js")%>"></script>

    <script type="text/javascript" lang="javascript">

        function confirmarDemanda() {
            return confirm("Esta seguro que desea demandar!");
        }

        function confirmarOferta() {
            return confirm("Esta seguro que desea ofertar!");
        }

        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }

        function formatoMiles(obj, evt) {
            var T;
            var strValue = removeCommas(obj.value);
            //if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
            if ((T = /([0-9]*)$/i.exec(strValue)) != null) {
                //                T[4] = T[4].substr(0, decimales);
                //                if (T[2] == '' && T[3] == '.') T[2] = 0;
                obj.value = addCommas(T[1]);
            } else {
                alert('formato invalido ');
            }
        }
        function formatoMilesPre(obj, evt, decimales) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
                T[4] = T[4].substr(0, decimales);
                if (T[2] == '' && T[3] == '.') {
                    T[2] = 0;
                }
                obj.value = T[1] + addCommas(T[2]) + T[3] + T[4];
            }
            else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }
        function InvocarMetodoWeb() {
            $.ajax({
                type: "POST",
                url: "@Url.Content('~/frm_frame_subasta_4.aspx/btnOfertasModal_Click')",
                // data: '{name: "' + 'EFRAIN' + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                error: function (result) {
                    alert("Error" + result);
                }
            });
        }
        function OnSuccess(response) {
            alert(response.d);
        }

    </script>

    <%--CONTENIDO--%>
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="kt-portlet">
                    <%--Head--%>
                    <div class="kt-portlet__head">
                        <%--Titulo--%>
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <asp:Label ID="lblTitle" Text="Suministro con Interrupciones" runat="server" />
                            </h3>
                        </div>

                        <%--Botones--%>
                        <asp:UpdatePanel style="margin-top: 1%" runat="server">
                            <ContentTemplate>
                                <segas:CrudButton ID="buttons" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <%--Contenido--%>
                    <div class="kt-portlet__body">
                        <strong>
                            <h5>
                                <input id="lblRelog" style="width: 100px; color: green; border: none;" type="text" readonly name="reloj" size="10">
                            </h5>
                        </strong>

                        <%--Estados de la subasta--%>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div id="divTiempoEspera" class="row" visible="False" runat="server">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <br />
                                        <div class="form-group">
                                            <div class="alert alert-info" runat="server">
                                                <strong>&#161;Atenci&#243;n&#33;</strong>
                                                <p>&nbsp; Se encuentra en un tiempo de espera</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:Button ID="btnClickEmulate" OnClick="Timer_Tick" Style="display: none;" ClientIDMode="Static" runat="server" />
                                <segas:Estado ID="estados" Visible="false" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <%--Separador--%>
                        <div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>

                        <%--Botones--%>
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label"></div>
                            <div class="kt-portlet__head-toolbar">
                                <segas:Subasta ID="buttonsSubasta" runat="server" />
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <segas:SubastaExtender ID="buttonsSubastaExtender" runat="server" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>

                        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                            <ContentTemplate>
                                <%--Tablas Subasta--%>
                                <div class="kt-portlet__body">
                                    <div class="tab-content">
                                        <div class="table table-responsive">
                                            <asp:DataGrid ID="dtgSubasta1" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                                ViewStateMode="Enabled" OnItemCommand="OnItemCommand_Click">
                                                <Columns>
                                                    <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--1--%><asp:BoundColumn DataField="desc_modalidad" HeaderText="Tipo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--2--%><asp:BoundColumn DataField="desc_periodo" HeaderText="Duraci&oacute;n" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                                    <%--3--%><asp:BoundColumn DataField="desc_punto_entrega" HeaderText="Lugar" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                    <%--4--%><asp:BoundColumn DataField="cantidad_venta" HeaderText="Cantidad Venta (MBTUD)" DataFormatString="{0:###,###,###,###,###,###,##0 }" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn> <%--20201020--%>
                                                    <%--5--%><asp:BoundColumn DataField="precio_venta" HeaderText="Precio Venta (USD/MBTU)" DataFormatString="{0:###,###,###,###,###,###,##0.00 }" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>  <%--20201020--%>
                                                    <%--6--%><asp:BoundColumn DataField="precio_delta" HeaderText="Precio Delta (USD/MBTU)" DataFormatString="{0:###,###,###,###,###,###,##0.00}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>  <%--20201020--%>
                                                    <%--7--%><asp:BoundColumn DataField="cantidad_adjudicada" HeaderText="Cantidad Adjudicada (MBTUD)" DataFormatString="{0:###,###,###,###,###,###,##0.00 }" ItemStyle-HorizontalAlign="Right" Visible="false"></asp:BoundColumn>  <%--20201020--%>
                                                    <%--8--%><asp:BoundColumn DataField="precio_adjudicado" HeaderText="Precio Adjudicado (USD/MBTU)" DataFormatString="{0:###,###,###,###,###,###,##0.00}" ItemStyle-HorizontalAlign="Right" Visible="false"></asp:BoundColumn>  <%--20201020--%>
                                                    <%--9--%><asp:BoundColumn DataField="mejor_postura" Visible="false"></asp:BoundColumn>
                                                    <%--10--%><asp:BoundColumn DataField="hora_prox_fase" HeaderText="Hora Porx Fase" Visible="false"></asp:BoundColumn>
                                                    <%--11--%><asp:BoundColumn DataField="estado" HeaderText="Estado"></asp:BoundColumn>
                                                    <%--12--%><asp:TemplateColumn HeaderText="Oferta">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imbOfertar" runat="server" ToolTip="Ofertar" ImageUrl="~/Images/nuevo.gif" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--13--%><asp:TemplateColumn HeaderText="Modificar">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imbModificar" runat="server" ToolTip="Modificar Oferta" ImageUrl="~/Images/modificar.gif" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--14--%><asp:TemplateColumn HeaderText="Eliminar">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imbEliminar" runat="server" ToolTip="Eliminar Oferta" ImageUrl="~/Images/eliminar.gif" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--15--%><asp:TemplateColumn HeaderText="Grafica">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imbGrafica" runat="server" ToolTip="curva agregada" ImageUrl="~/Images/grafica.jpg" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--16--%><asp:BoundColumn DataField="ind_vendedor" Visible="false" ItemStyle-Width="60px"></asp:BoundColumn>
                                                    <%--17--%><asp:BoundColumn DataField="ind_postura" Visible="false" ItemStyle-Width="60px"></asp:BoundColumn>
                                                    <%--18--%><asp:BoundColumn DataField="no_posturas_compra" Visible="false" ItemStyle-Width="60px"></asp:BoundColumn>
                                                    <%--19--%><asp:BoundColumn DataField="habilita_c" Visible="false" ItemStyle-Width="60px"></asp:BoundColumn>
                                                    <%--20--%><asp:BoundColumn DataField="habilita_v" Visible="false" ItemStyle-Width="60px"></asp:BoundColumn>
                                                </Columns>

                                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                                <PagerStyle HorizontalAlign="Center"></PagerStyle>
                                            </asp:DataGrid>
                                        </div>
                                        <%--Tabla que se exporta a Excel--%>
                                        <asp:DataGrid ID="dtgSubasta" runat="server" AutoGenerateColumns="False" runat="server"
                                            Visible="false">
                                            <ItemStyle CssClass="td2" Font-Names="Arial, Helvetica, sans-serif" Font-Size="15px"></ItemStyle>
                                            <Columns>
                                                <%--0--%><asp:BoundColumn DataField="numero_id" HeaderText="IDG" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                <%--1--%><asp:BoundColumn DataField="desc_modalidad" HeaderText="Tipo Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                <%--2--%><asp:BoundColumn DataField="desc_periodo" HeaderText="Periodo de Entrega" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                                <%--3--%><asp:BoundColumn DataField="desc_punto_entrega" HeaderText="Punto de Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                <%--4--%><asp:BoundColumn DataField="cantidad_venta" HeaderText="Cantidad (MBTUD)"
                                                    DataFormatString="{0:###,###,###,###,###,###,##0 }" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn> <%--20201020--%>
                                                <%--3--%><asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--Modal Suspender--%>
    <div class="modal fade" id="mdlSuspender" tabindex="-1" role="dialog" aria-labelledby="mdlSuspenderLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlInsideSuspender" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlSuspenderLabel">SUSPENDER RUEDA</h5>
                        </div>
                        <div class="modal-body">
                            <h5>
                                <label id="lblEstado" runat="server"></label>
                            </h5>

                            <hr>

                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="ddlDefinitivo" runat="server">Suspensión Definitiva</asp:Label>
                                        <asp:DropDownList ID="ddlDefinitivo" CssClass="form-control selectpicker" runat="server">
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                            <asp:ListItem Value="S">Si</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <asp:Label AssociatedControlID="TxtObservacion" runat="server">Observaciones</asp:Label>
                                        <asp:TextBox ID="TxtObservacion" CssClass="form-control" runat="server" MaxLength="1000" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <%--Agregar el confirmar--%>
                            <asp:Button ID="Button1" Text="Suspender" class="btn btn-primary" OnClick="btnSuspender_Click" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <%--Modal Reactivar--%>
    <div class="modal fade" id="mdlReactivar" tabindex="-1" role="dialog" aria-labelledby="mdlReactivarLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlInsideReactivar" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlReactivarLabel">Reactivar Rueda</h5>
                </div>
                <div class="modal-body">
                    <h5>
                        <asp:Label ID="lblEstadoReactivar" runat="server"></asp:Label>
                    </h5>

                    <hr>

                    <div class="row">
                        <div id="horaPubV" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                            <div class="form-group">
                                <asp:Label AssociatedControlID="txtHoraIniPubV" runat="server">Horas Declaración de Información</asp:Label>
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <asp:Label AssociatedControlID="txtHoraIniPubV" runat="server">Hora Inicial</asp:Label>
                                            <div class="input-group mb-3 date">
                                                <asp:TextBox ID="txtHoraIniPubV" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <asp:RegularExpressionValidator ID="revtxtHoraIniPubV" ControlToValidate="txtHoraIniPubV" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora de Declaración de Informacion"> * </asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <asp:Label AssociatedControlID="txtHoraFinPubV" runat="server">Hora Final</asp:Label>
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txtHoraFinPubV" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <asp:RegularExpressionValidator ID="revtxtHoraFinPubV" ControlToValidate="txtHoraFinPubV" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora final  de Declaración de Informacion"> * </asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="horaCntD" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                            <div class="form-group">
                                <asp:Label AssociatedControlID="txtHoraIniCntD" runat="server">Horas Declaración Precio de Reserva</asp:Label>
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <asp:Label AssociatedControlID="txtHoraIniCntD" runat="server">Hora Inicial</asp:Label>
                                            <div class="input-group mb-3 date">
                                                <asp:TextBox ID="txtHoraIniCntD" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <asp:RegularExpressionValidator ID="revtxtHoraIniCntD" ControlToValidate="txtHoraIniCntD" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora de inicio de Declaración precio de reserva"> * </asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <asp:Label AssociatedControlID="txtHoraFinCntD" runat="server">Hora Final</asp:Label>
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txtHoraFinCntD" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <asp:RegularExpressionValidator ID="revtxtHoraFinCntD" ControlToValidate="txtHoraFinCntD" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora final  de Declaración precio de reserva"> * </asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="horaComp" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                            <div class="form-group">
                                <asp:Label AssociatedControlID="txtHoraIniComp" runat="server">Horas Para Publicación</asp:Label>
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <asp:Label AssociatedControlID="txtHoraIniComp" runat="server">Hora Inicial</asp:Label>
                                            <div class="input-group mb-3 date">
                                                <asp:TextBox ID="txtHoraIniComp" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <asp:RegularExpressionValidator ID="RevtxtHoraIniComp" ControlToValidate="txtHoraIniComp" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora de Publicación"> * </asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <asp:Label AssociatedControlID="txtHoraFinComp" runat="server">Hora Final</asp:Label>
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txtHoraFinComp" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <asp:RegularExpressionValidator ID="revtxtHoraFinComp" ControlToValidate="txtHoraFinComp" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora final  de Publicación"> * </asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="horaNeg" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                            <div class="form-group">
                                <asp:Label AssociatedControlID="txtHoraIniComp" runat="server"> Hora Ingreso de posturas de compra</asp:Label>
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <asp:Label AssociatedControlID="txtHoraIniNeg" runat="server">Hora Inicial</asp:Label>
                                            <div class="input-group mb-3 date">
                                                <asp:TextBox ID="txtHoraIniNeg" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <asp:RegularExpressionValidator ID="revtxttxtHoraIniNeg" ControlToValidate="txtHoraIniNeg" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora inicial de Ingreso de posturas de compra"> * </asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <asp:Label AssociatedControlID="txtHoraFinNeg" runat="server">Hora Final</asp:Label>
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txtHoraFinNeg" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <asp:RegularExpressionValidator ID="revtxtHoraFinNeg" ControlToValidate="txtHoraFinNeg" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora final de Ingreso de posturas de compra"> * </asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <asp:Label AssociatedControlID="txtHoraIniCalce" runat="server"> Horas Para calce</asp:Label>
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <asp:Label AssociatedControlID="txtHoraIniCalce" runat="server">Hora Inicial</asp:Label>
                                            <div class="input-group mb-3 date">
                                                <asp:TextBox ID="txtHoraIniCalce" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <asp:RegularExpressionValidator ID="revtxtHoraIniCalce" ControlToValidate="txtHoraIniCalce" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora de calce"> * </asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <asp:Label AssociatedControlID="txtHoraFinCalce" runat="server">Hora Final</asp:Label>
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txtHoraFinCalce" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <asp:RegularExpressionValidator ID="revtxtHoraFinCalce" ControlToValidate="txtHoraFinCalce" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora final  calce"> * </asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="horaCont" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                            <div class="form-group">
                                <asp:Label AssociatedControlID="txtHoraIniCont" runat="server">Hora Modificacion contratos</asp:Label>
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <asp:Label AssociatedControlID="txtHoraIniCont" runat="server">Hora Inicial</asp:Label>
                                            <div class="input-group mb-3 date">
                                                <asp:TextBox ID="txtHoraIniCont" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <asp:RegularExpressionValidator ID="revtxtHoraIniCont" ControlToValidate="txtHoraIniCont" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora inicial de modificacion de contratos"> * </asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <asp:Label AssociatedControlID="txtHoraFinCont" runat="server">Hora Final</asp:Label>
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txtHoraFinCont" CssClass="form-control datetimepicker" runat="server" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="far fa-clock"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <asp:RegularExpressionValidator ID="revtxtHoraFinCont" ControlToValidate="txtHoraFinCont" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi" ErrorMessage="Formato Incorrecto para la hora final de modificacion de contratos"> * </asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <asp:Label AssociatedControlID="TxtObservacionReac" runat="server">Observaciones</asp:Label>
                                <asp:TextBox ID="TxtObservacionReac" CssClass="form-control" runat="server" MaxLength="1000" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <%--Agregar el confirmar--%>
                    <asp:Button ID="Button2" Text="Reactivar" class="btn btn-primary" ValidationGroup="comi" CausesValidation="true" OnClick="btnReactivar_Click" runat="server" />
                </div>
            </div>
        </div>
    </div>

    <%--Modal Ingreso Posturas de Compra--%>
    <div class="modal fade" id="mdlCompraModal" tabindex="-1" role="dialog" aria-labelledby="mdlCompraModalLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlCompraModalInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlCompraModalLabel">Ingreso Posturas de Compra</h5>
                        </div>
                        <%-- inicio body--%>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblRuedaCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblIdCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblFechaCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblProductoCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblUnidadMedidaCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblPuntoEntregaCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblPeridoEntCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblFechaIniCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblModalidadCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblFechaFinCompra" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblMensajeCompra" runat="server" ForeColor="Red" Font-Bold="true" Text="" Font-Size="14px"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="alert alert-info">
                                        <strong>&#161;Atenci&#243;n&#33;</strong>
                                        <p>&nbsp; Los precios de compra deben ser ingresados de mayor a menor.</p>

                                        <strong>&#161;Atenci&#243;n&#33;</strong>
                                        <p>&nbsp; Las cantidades a comprar deben ser ingresados de menor a mayor.</p>
                                    </div>
                                </div>
                                <div class="table table-responsive">
                                    <asp:DataGrid ID="dtgPosturasC" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                                        ViewStateMode="Enabled">
                                        <Columns>
                                            <asp:BoundColumn DataField="codigo_postura" HeaderText="No. Postura" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Precio">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtPrecio" CssClass="form-control" runat="server" onkeyup="return formatoMilesPre(this,event,2);"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Cantidad">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtCantidad" CssClass="form-control" runat="server" onkeyup="return formatoMiles(this,event);"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="cantidad_postura" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="precio_postura" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="numero_postura" Visible="false"></asp:BoundColumn>
                                        </Columns>
                                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                        <PagerStyle HorizontalAlign="Center"></PagerStyle>
                                    </asp:DataGrid>
                                </div>

                            </div>
                            <asp:HiddenField ID="hndIDCompra" runat="server" />
                            <asp:HiddenField ID="hdfNoRuedaCompra" runat="server" />
                            <asp:HiddenField ID="hdfMaxPostCompra" runat="server" />
                            <asp:HiddenField ID="hndAccionCompra" runat="server" />
                            <asp:HiddenField ID="hndCntCompra" runat="server" />
                        </div>
                        <div class="modal-footer">
                            <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseCompra_Click" runat="server" />
                            <asp:Button ID="btnCompra" Text="Crear" runat="server" OnClientClick="return confirmarDemanda();" OnClick="btnComprar_Click" CssClass="btn btn-secondary" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <%--Modal Cargar posturas de venta--%>
    <div class="modal fade" id="mdlCargue" tabindex="-1" role="dialog" aria-labelledby="mdlContratosLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlInsideCargue" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="lblmdlCargue">Cargar posturas de venta</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <div class="form-group">
                                <asp:Label AssociatedControlID="FuArchivo" runat="server">Archivo</asp:Label>
                                <asp:FileUpload ID="FuArchivo" CssClass="form-control" runat="server" EnableTheming="true" />
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <div class="form-group">
                                <asp:HiddenField ID="HiddenField1" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseCargueArchivo_Click" runat="server" />
                    <asp:Button ID="BtnCargar" CssClass="btn btn-primary" Text="Cargue Archivo" OnClick="BtnCargar_Click" runat="server" />
                </div>
            </div>
        </div>
    </div>

    <%--Modal Ofertas--%>
    <div class="modal fade" id="mdlOfertas" tabindex="-1" role="dialog" aria-labelledby="mdlOfertaLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlOfertasInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlOfertasLabel" runat="server">Ofertas</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <div class="table table-responsive">
                                            <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                                <ContentTemplate>
                                                    <asp:Literal ID="ltTableroRF" Mode="Transform" runat="server"></asp:Literal>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:UpdatePanel ID="UpdatePanel16" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseOfertas_Click" runat="server" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <%--Modal Contratos--%>
    <div class="modal fade" id="mdlContratos" tabindex="-1" role="dialog" aria-labelledby="mdlContratosLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlContratosInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="lblmdlContratos">Mis Contratos</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <div class="table table-responsive">
                                            <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                                <ContentTemplate>
                                                    <asp:Literal ID="ltTablero" runat="server"></asp:Literal>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseContratos_Click" runat="server" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <%--Modal Curva de Oferta y Demanda Agregada--%>
    <div class="modal fade" id="mdlCurvaOfertaDemAgre" tabindex="-1" role="dialog" aria-labelledby="mdlCurvaOfertaDemAgreLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlCurvaOfertaDemAgreInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlCurvaOfertaDemAgreLabel">Curva de Oferta y Demanda Agregada</h5>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <h5>
                                <asp:Label ID="lblPuntoCurva" runat="server" Font-Bold="true"></asp:Label>
                            </h5>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <div id="chart_div" style="width: 700px; height: 500px; display: block; margin: 0 auto !important;"></div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="table table-responsive">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <asp:DataGrid ID="dtgInformacion" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center" ViewStateMode="Enabled">
                                            <Columns>
                                                <asp:BoundColumn DataField="precio" HeaderText="Precio (USD/MBTU)" DataFormatString="{0: ###,###,##0.00}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn> <%--20201020--%>
                                                <asp:BoundColumn DataField="cantidad_venta" HeaderText="Cantidad Venta (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,##0}"></asp:BoundColumn> <%--20201020--%>
                                                <asp:BoundColumn DataField="cantidad_compra" HeaderText="Cantidad Compra (MBTUD)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,##0}"></asp:BoundColumn> <%--20201020--%>
                                                <asp:BoundColumn DataField="cantidad_min" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="cantidad_max" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="precio_min" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="precio_max" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="precio_venta" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="precio_compra" Visible="false"></asp:BoundColumn>
                                                <%--20210224--%>
                                                <asp:BoundColumn DataField="grafica_compra" Visible="false"></asp:BoundColumn>
                                            </Columns>
                                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                            <PagerStyle HorizontalAlign="Center"></PagerStyle>
                                        </asp:DataGrid>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:Button type="button" Text="Cancelar" CssClass="btn btn-secondary" OnClick="CloseCurvaOferDemAgre_Click" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%--Modal Postura de Venta--%>
    <div class="modal fade" id="mdlOfertaModal" tabindex="-1" role="dialog" aria-labelledby="mdlOfertaModalLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlOfertaModalInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlOfertaModalLabel">Postura de Venta</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblRueda" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblId" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblFecha" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblProducto" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblUnidadMedida" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblPuntoEntrega" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblPeridoEnt" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblModalidad" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblFechaIni" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblFechaFin" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Cantidad Disponible" AssociatedControlID="TxtCntDisp" runat="server" />
                                        <asp:TextBox ID="TxtCntDisp" runat="server" onkeyup="return formatoMiles(this,event);" Width="100%" CssClass="form-control"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTBETxtCntDisp" runat="server" TargetControlID="TxtCntDisp" FilterType="Custom, Numbers" ValidChars=","></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Precio Reserva" AssociatedControlID="TxtPrecioRes" runat="server" />
                                        <asp:TextBox ID="TxtPrecioRes" runat="server" onkeyup="return formatoMilesPre(this,event);" CssClass="form-control"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTEBTxtPrecioRes" runat="server" TargetControlID="TxtPrecioRes" FilterType="Custom, Numbers" ValidChars=",."></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div id="trPrecioVar" class="col-sm-12 col-md-6 col-lg-6" runat="server">
                                    <div class="form-group">
                                        <asp:Label Text="Variaci&#243;n Precio" AssociatedControlID="TxtPrecioVar" runat="server" />
                                        <asp:TextBox ID="TxtPrecioVar" runat="server" onkeyup="return formatoMilesPre(this,event,2);" CssClass="form-control"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTEBTxtPrecioVar" runat="server" TargetControlID="TxtPrecioVar" FilterType="Custom, Numbers" ValidChars=",."></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnOfertar" Text="Crear" runat="server" OnClientClick="return confirmarOferta();" CssClass="btn btn-primary" OnClick="btnOfertar_Click" />
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        </div>

                        <asp:HiddenField ID="hndID" runat="server" />
                        <asp:HiddenField ID="hdfNoRueda" runat="server" />
                        <asp:HiddenField ID="hdfEstadoRueda" runat="server" />
                        <asp:HiddenField ID="hndFechaRueda" runat="server" />
                        <asp:HiddenField ID="hndAccion" runat="server" />
                        <asp:HiddenField ID="hdfNoPostura" runat="server" />
                        <asp:HiddenField ID="hdfTipoOperador" runat="server" />
                        <asp:HiddenField ID="hdfTpoRueda" runat="server" />
                        <asp:HiddenField ID="hdfIngDelta" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

</asp:Content>
