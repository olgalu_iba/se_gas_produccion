﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using SIB.BASE.Negocio.Manejador;
using SIB.BASE.Negocio.Repositorio;


public partial class Procesos_frm_pruebaProcesos : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = "Prueba de procesos";

        //Establese los permisos del sistema
        lConexion = new clConexion(goInfo);
    }
    /// <summary>
    /// Nombre: btnBuscar_Click
    /// Fecha: mayo 6 de 2014
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: COnuslta los Id para modificar contratos
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnProcesar_Click(object sender, EventArgs e)
    {
        string oError = "";

        ////if (TxtFecha.Text == "")
        ////    oError += "Debe digitar la fecha";

        if (oError == "")
        {
            string[] lsNombreParametros = { "@P_hora" };
            SqlDbType[] lTipoparametros = { SqlDbType.VarChar };
            string[] lValorParametros = { "00:30" };

            string[] lsNombreParametros1 = { "@P_fecha", "@P_hora" };
            SqlDbType[] lTipoparametros1 = { SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros1 = { TxtFecha.Text, TxtHora.Text };

            string[] lsNombreParametros2 = { "@P_fecha"};
            SqlDbType[] lTipoparametros2 = { SqlDbType.VarChar};
            string[] lValorParametros2 = { TxtFecha.Text};

            string[] lsNombreParametros3 = { "@P_numero_rueda" };
            SqlDbType[] lTipoparametros3 = { SqlDbType.Int };
            string[] lValorParametros3 = { TxtRueda.Text };

            SqlDataReader lLector;
            lConexion.Abrir();
            try
            {
                if (ddlProceso.SelectedValue == "1")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetControlUVLP", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se crearon los IDs";
                }
                if (ddlProceso.SelectedValue == "2")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_BecsetInfMS", lsNombreParametros1, lTipoparametros1, lValorParametros1, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se publicó la información";
                }
                if (ddlProceso.SelectedValue == "3")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_BecSetOfertaComp", lsNombreParametros2, lTipoparametros2, lValorParametros2, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se publicó la información";
                }

                if (ddlProceso.SelectedValue == "4")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_BecSetInfBasica", null, null, null, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se publicó la información";
                }
                if (ddlProceso.SelectedValue == "5")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_BecSetInfOperativa", lsNombreParametros2, lTipoparametros2, lValorParametros2, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se publicó la información";
                }
                if (ddlProceso.SelectedValue == "6")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_BecSetInfVerif", lsNombreParametros2, lTipoparametros2, lValorParametros2, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se publicó la información";
                }
                if (ddlProceso.SelectedValue == "7")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_BecsetInfMS", lsNombreParametros1, lTipoparametros1, lValorParametros1, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se publicó la información";
                }
                if (ddlProceso.SelectedValue == "8")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_BecSetMercMay", lsNombreParametros2, lTipoparametros2, lValorParametros2, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se publicó la información";
                }
                if (ddlProceso.SelectedValue == "9")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetNotifSarlaft", null, null, null, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se actualizó la información";
                }
                if (ddlProceso.SelectedValue == "10")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "sp_act_cartas", null, null, null, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se actualizó la información";
                }
                if (ddlProceso.SelectedValue == "11")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetRuedaAutMen", null, null, null, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se actualizó la información";
                }
                if (ddlProceso.SelectedValue == "12")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetRevisaContingencia", null, null, null, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se actualizó la información";
                }
                if (ddlProceso.SelectedValue == "13")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_BecSetInfRep", null, null, null, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se actualizó la información";
                }
                if (ddlProceso.SelectedValue == "14")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_BecSetPubSubBim", lsNombreParametros2, lTipoparametros2, lValorParametros2, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se publicó la información";
                }
                if (ddlProceso.SelectedValue == "15")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_AnulSolModCont", null, null, null, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se actualizó la información";
                }
                if (ddlProceso.SelectedValue == "16")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetEstadoContratos", null, null, null, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se actualizó la información";
                }
                if (ddlProceso.SelectedValue == "17")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetCambioPrecioRech", null, null, null, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se actualizó la información";
                }
                if (ddlProceso.SelectedValue == "18")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetSolModProxVencer", null, null, null, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se ejecutó el proceso";
                }
                if (ddlProceso.SelectedValue == "19")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_FinalizaRueda", lsNombreParametros3, lTipoparametros3, lValorParametros3, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se publicó la información";
                }
                if (ddlProceso.SelectedValue == "20")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_BorraDecUvlp", null, null, null, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se publicó la información";
                }
                if (ddlProceso.SelectedValue == "21")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_BecSetGasConexion", lsNombreParametros2, lTipoparametros2, lValorParametros2, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se publicó la información";
                }
                if (ddlProceso.SelectedValue == "22")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_CambioFecRegPrb", lsNombreParametros1, lTipoparametros1, lValorParametros1, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se publicó la información";
                }
                //20201124
                if (ddlProceso.SelectedValue == "23")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_BecSetInfTransporte", lsNombreParametros2, lTipoparametros2, lValorParametros2, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se publicó la información";
                }
                //20201124
                if (ddlProceso.SelectedValue == "24")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "SP_UptFechaTmp", lsNombreParametros2, lTipoparametros2, lValorParametros2, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se publicó la información";
                }
                //20220221
                if (ddlProceso.SelectedValue == "25")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetTableroControl", null, null, null, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se ejecutó el proceso";
                }
                //20220221
                if (ddlProceso.SelectedValue == "26")
                {
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetTableroControlCont", null, null, null, goInfo);
                    lLector.Close();
                    lConexion.Cerrar();
                    lblMensaje1.Text = "Se ejecutó el proceso";
                }
            }
            catch (Exception ex)
            {
                lblMensaje1.Text = "Error al ejecutar el proceso. " + ex.Message;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + oError + "');", true);
                lConexion.Cerrar();
            }
            //lblMensaje1.Text= oError;
        }
    }


}