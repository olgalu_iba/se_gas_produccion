﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ModifContrato.aspx.cs"
    Inherits="Procesos_frm_ModifContrato" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="80%" border="0" cellpadding="0" cellspacing="0">
        <table id="tblParametros" align="center" runat="server" width="80%">
            <tr>
                <td class="th1" colspan="2">
                    <asp:Label ID="lblTitulo" runat="server" ForeColor ="White"  Text ="MODIFICACION DE CONTRATOS"></asp:Label>
                    <br /><br /><br /><br /><br /><br />
                </td>
            </tr>
            <tr>
                <td class="td1">
                    Rueda
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlRueda" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">
                    Número ID
                </td>
                <td class="td2">
                    <asp:TextBox ID="txtId" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="th1" colspan="2">
                    <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                        OnClick="imbConsultar_Click" Height="40" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblMensaje1" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <asp:DataGrid ID="dgIds" runat="server" AutoGenerateColumns="false" AlternatingItemStyle-CssClass="td1"
                        ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1" OnEditCommand="dgIds_EditCommand">
                        <Columns>
                            <asp:BoundColumn HeaderText="Número Id " DataField="numero_id" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn HeaderText="fecha rueda" DataField="fecha_rueda" DataFormatString="{0:yyyy/MM/dd}"
                                ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="producto" DataField="desc_producto"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="UM" DataField="desc_unidad_medida"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="punto entrega" DataField="desc_punto_entrega" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Modalidad" DataField="desc_modalidad" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Fecha entrega" DataField="fecha_entrega" DataFormatString="{0:yyyy/MM/dd}"
                                ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="cnt adj" DataField="cantidad_adjudicada" DataFormatString="{0:###,###,###,###,###,###,###,###,##0}"
                                ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="pre adj" DataField="precio_adjudicado" DataFormatString="{0:###,###,###,###,###,###,###,###,##0.00}"
                                ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="cnt cont" DataField="cantidad_cont" DataFormatString="{0:###,###,###,###,###,###,###,###,##0}"
                                ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" ItemStyle-HorizontalAlign="center">
                            </asp:EditCommandColumn>
                        </Columns>
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
        <br />
        <table id="tblModifica" align="center" width="100%" runat="server" visible="false">
            <tr>
                <td class="th1" colspan="2">
                    MODIFICACION DE CONTRATOS
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <table id="Table1" align="center" width="95%" runat="server">
                        <tr>
                            <td class="th1">
                                CANTIDADES ADJUDICADAS VENTA
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DataGrid ID="dgadjV" runat="server" AutoGenerateColumns="false" Width="100%"
                                    AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                                    <Columns>
                                        <asp:BoundColumn HeaderText="Cd Ope" DataField="codigo_operador" ItemStyle-HorizontalAlign="center">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="Nombre" DataField="nombre_operador"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="Cnt Adj" DataField="cantidad_adjudicada" DataFormatString="{0:###,###,###,###,###,###,###,###,##0}"
                                            ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="pre Adj" DataField="Precio_adjudicado" DataFormatString="{0:###,###,###,###,###,###,###,###,##0.00}"
                                            ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="Cnt pend cont" DataField="cnt_pend_contrato" DataFormatString="{0:###,###,###,###,###,###,###,###,##0}"
                                            ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle CssClass="th1"></HeaderStyle>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td class="th1">
                                CANTIDADES ADJUDICADAS COMPRA
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DataGrid ID="dgadjC" runat="server" AutoGenerateColumns="false" Width="100%"
                                    AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                                    <Columns>
                                        <asp:BoundColumn HeaderText="Cd Ope" DataField="codigo_operador" ItemStyle-HorizontalAlign="Center">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="Nombre" DataField="nombre_operador"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="Cnt Adj" DataField="cantidad_adjudicada" DataFormatString="{0:###,###,###,###,###,###,###,###,##0}"
                                            ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="pre Adj" DataField="Precio_adjudicado" DataFormatString="{0:###,###,###,###,###,###,###,###,##0.00}"
                                            ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="Cnt pend cont" DataField="cnt_pend_contrato" DataFormatString="{0:###,###,###,###,###,###,###,###,##0}"
                                            ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle CssClass="th1"></HeaderStyle>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td class="th1">
                                CONTRATOS ORIGINALES
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DataGrid ID="DgCont" runat="server" AutoGenerateColumns="false" Width="100%"
                                    AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1">
                                    <Columns>
                                        <asp:BoundColumn HeaderText="contrato" DataField="numero_contrato" ItemStyle-HorizontalAlign="center">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="Cd Ope V" DataField="operador_venta" ItemStyle-HorizontalAlign="center">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="Nombre V" DataField="nombre_operador_v"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="Cd Ope C" DataField="operador_compra" ItemStyle-HorizontalAlign="center">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="Nombre C" DataField="nombre_operador_c"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="Cnt Cont" DataField="cantidad" DataFormatString="{0:###,###,###,###,###,###,###,###,##0}"
                                            ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle CssClass="th1"></HeaderStyle>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table id="Table2" align="center" width="95%" runat="server">
                        <tr>
                            <td class="th1" colspan="2">
                                CAMBIO EN CONTRATOS
                            </td>
                        </tr>
                        <tr>
                            <td class="td1">
                                Operador Venta:
                            </td>
                            <td class="td2">
                                <asp:DropDownList ID="ddlOperadorV" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1">
                                Operador Compra:
                            </td>
                            <td class="td2">
                                <asp:DropDownList ID="ddlOperadorC" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1">
                                Cantidad:
                            </td>
                            <td class="td2">
                                <asp:TextBox ID="txtCantidad" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center" class="th1">
                                <asp:ImageButton ID="imbAgregar" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbAgregar_Click"
                                    Height="40" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:ImageButton ID="imbModificar" runat="server" ImageUrl="~/Images/Actualizar.png"
                                    OnClick="imbModificar_Click" Height="40" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:DataGrid ID="dgDetCont" runat="server" AutoGenerateColumns="false" AlternatingItemStyle-CssClass="td1"
                                    ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1" OnEditCommand="dgDetCont_EditCommand"
                                    Width="95%">
                                    <Columns>
                                        <asp:BoundColumn HeaderText="cont" DataField="numero_contrato" ItemStyle-HorizontalAlign="center">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="Cd Ope V" DataField="operador_venta" ItemStyle-HorizontalAlign="center">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="Nombre V" DataField="nombre_operador_v"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="Cd Ope C" DataField="operador_compra" ItemStyle-HorizontalAlign="center">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="Nombre C" DataField="nombre_operador_c"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="Cnt Cont" DataField="cantidad" DataFormatString="{0:###,###,###,###,###,###,###,###,##0}"
                                            ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                        <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" ItemStyle-HorizontalAlign="center">
                                        </asp:EditCommandColumn>
                                        <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar" ItemStyle-HorizontalAlign="center">
                                        </asp:EditCommandColumn>
                                    </Columns>
                                    <HeaderStyle CssClass="th1"></HeaderStyle>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="th1" colspan="2" align="center">
                    <asp:ImageButton ID="imbModificarCont" runat="server" ImageUrl="~/Images/Crear.png"
                        OnClick="imbModificarCont_Click" Height="40" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:ImageButton ID="imbRegresar" runat="server" ImageUrl="~/Images/Regresar.png"
                        OnClick="imbRegresar_Click" Height="40" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:ValidationSummary ID="vsValorar" runat="server" ValidationGroup="valorar" />
                    <asp:HiddenField ID="hndId" runat="server" />
                    <asp:HiddenField ID="hndContrato" runat="server" />
                </td>
            </tr>
        </table>
    </table>
</asp:Content>