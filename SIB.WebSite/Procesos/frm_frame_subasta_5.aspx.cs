﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

public partial class Procesos_frm_frame_subasta_5 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            clConexion lConexion = null;
            InfoSessionVO goInfo = null;
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            Session["hora"] = "";
            //Session["hora1"] = "";
            Session["hora_mensaje"] = "";
            Session["punta"] = "";
            Session["numero_rueda"] = "0";
            Session["tipo_subasta"] = this.Request.QueryString["clase_subasta"];
            Session["refresca_mis_posturas"] = "S";
            Session["refrescar_cont"] = "S";
            Session["codigo_punto"] = "0";
            Session["punta"] = "";
            Session["codigo_modalidad"] = "0";
            Session["codigo_periodo"] = "0";
        }
        catch (Exception)
        {
 
        }
    }
}