﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

//using System.Windows.Forms;



public partial class Procesos_frm_posturas_4 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    //String strRutaArchivo;
    private System.TimeSpan diffResult;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            if (!IsPostBack)
            {
                try
                {
                    hndCodSubasta.Value = Session["numero_rueda"].ToString();
                    if (hndCodSubasta.Value == "")
                        hndCodSubasta.Value = "0";
                    lConexion1 = new clConexion(goInfo);
                    //hndCodSubasta.Value = "21";
                    HndFechaRueda.Value = DateTime.Now.ToShortDateString();
                    HndFechaRueda.Value = HndFechaRueda.Value.Substring(6, 4) + "/" + HndFechaRueda.Value.Substring(3, 2) + "/" + HndFechaRueda.Value.Substring(0, 2);
                    SqlDataReader lLector;
                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda", " numero_rueda =" + Session["numero_rueda"].ToString());
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        Session["estado"] = lLector["estado"].ToString();
                    }
                    else
                        Session["estado"] = "C";
                    lLector.Close();
                    if (Session["tipoPerfil"].ToString() == "N")
                    {
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_usuario_hab ", " codigo_tipo_subasta =4 and codigo_usuario =" + goInfo.codigo_usuario + " and fecha_inicial <= '" + HndFechaRueda.Value + "' and fecha_final >= '" + HndFechaRueda.Value + "' and estado ='A'");
                        if (!lLector.HasRows)
                        {

                            Toastr.Warning(this, "El usuario NO está habilitado para la rueda. Debe realizar la inscripcion ante la BMC");
                            
                        }
                    }
                    lLector.Close();
                    lConexion.Cerrar();


                    cargarDatosGrilla();
                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, ex.Message, "Session Expiro. " + ex.Message, 50000);
                }
            }
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, ex.Message);
        }
    }
    /// <summary>
    /// Nombre: cargarDatosGrilla
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para Cargar los datos en la Grio.
    /// Modificacion:
    /// </summary>
    private void cargarDatosGrilla()
    {
        if (hndCodSubasta.Value == "0")
        {
            dtgSubasta1.Columns.Clear();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.contratos.location= 'frm_contratos_4.aspx';", true);
        }
        else
        {
            try
            {
                //determina las columnas para el subastatodr o los operaores
                string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_operador", "@P_codigo_punto" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int , SqlDbType.Int };
                string[] lValorParametros = { hndCodSubasta.Value, goInfo.cod_comisionista, Session["codigo_punto"].ToString() };
                lConexion.Abrir();
                dtgSubasta1.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuedaPuja4", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgSubasta1.DataBind();
                lConexion.Cerrar();
                ImageButton imbControl;
                dtgSubasta1.Columns[6].Visible = true;
                dtgSubasta1.Columns[12].Visible = true;
                dtgSubasta1.Columns[13].Visible = true;
                dtgSubasta1.Columns[14].Visible = true;
                if (Session["estado"].ToString() == "5" || Session["estado"].ToString() == "F" || Session["estado"].ToString() == "S" || Session["estado"].ToString() == "Z")
                {
                    dtgSubasta1.Columns[6].Visible = false;
                    dtgSubasta1.Columns[12].Visible = false;
                    dtgSubasta1.Columns[13].Visible = false;
                    dtgSubasta1.Columns[14].Visible = false;

                }
                if (Session["estado"].ToString() == "3" || Session["estado"].ToString() == "7")
                {
                    dtgSubasta1.Columns[6].Visible = false;
                    dtgSubasta1.Columns[12].Visible = false;
                }

                if (Session["estado"].ToString() == "5" || Session["estado"].ToString() == "F")
                {
                    dtgSubasta1.Columns[5].Visible = false;
                    dtgSubasta1.Columns[6].Visible = false;
                    dtgSubasta1.Columns[7].Visible = true;
                    dtgSubasta1.Columns[8].Visible = true;
                    dtgSubasta1.Columns[15].Visible = true;
                }
                else
                    dtgSubasta1.Columns[15].Visible = false;

                foreach (DataGridItem Grilla in this.dtgSubasta1.Items)
                {
                    imbControl = (ImageButton)Grilla.Cells[15].Controls[1];
                    imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_CurvaAgregada.aspx?ID=" + Grilla.Cells[0].Text + "','Grafica','width=900,height=500,left=250,top=90,status=no,location=0,menubar=no,toolbar=no,resizable=yes,scrollbars=yes');");

                    if (Grilla.Cells[16].Text == "S")
                    {
                        imbControl = (ImageButton)Grilla.Cells[12].Controls[1];
                        imbControl.ImageUrl = "~/Images/quitar.gif";
                        imbControl.Enabled = false;
                        imbControl = (ImageButton)Grilla.Cells[13].Controls[1];
                        imbControl.ImageUrl = "~/Images/quitar.gif";
                        imbControl.Enabled = false;
                        imbControl = (ImageButton)Grilla.Cells[14].Controls[1];
                        imbControl.ImageUrl = "~/Images/quitar.gif";
                        imbControl.Enabled = false;
                    }
                    else
                    {
                        if (Grilla.Cells[17].Text == "N")
                        {
                            imbControl = (ImageButton)Grilla.Cells[12].Controls[1];
                            if (Session["estado"].ToString() == "1" || Session["estado"].ToString() == "3")
                            {
                                if (Grilla.Cells[20].Text == "S")
                                    imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_IngresoOfertaVenta_4.aspx?ID=" + Grilla.Cells[0].Text + "&accion=C','Ofertar','width=600,height=400,left=350,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                                else
                                {
                                    imbControl = (ImageButton)Grilla.Cells[12].Controls[1];
                                    imbControl.ImageUrl = "~/Images/quitar.gif";
                                    imbControl.Enabled = false;
                                }
                            }
                            else
                            {
                                if (Grilla.Cells[19].Text == "S" && Grilla.Cells[4].Text != "0 " && Grilla.Cells[4].Text != "0" && Grilla.Cells[4].Text != "")
                                    imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_IngresoOfertaCompra_4.aspx?ID=" + Grilla.Cells[0].Text + "&accion=C&maxPostCompra=" + Grilla.Cells[18].Text + "','Ofertar','width=600,height=450,left=350,top=150,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                                else
                                {
                                    imbControl = (ImageButton)Grilla.Cells[12].Controls[1];
                                    imbControl.ImageUrl = "~/Images/quitar.gif";
                                    imbControl.Enabled = false;
                                }
                            }
                            imbControl = (ImageButton)Grilla.Cells[13].Controls[1];
                            imbControl.ImageUrl = "~/Images/quitar.gif";
                            imbControl.Enabled = false;
                            imbControl = (ImageButton)Grilla.Cells[14].Controls[1];
                            imbControl.ImageUrl = "~/Images/quitar.gif";
                            imbControl.Enabled = false;
                        }
                        else
                        {
                            imbControl = (ImageButton)Grilla.Cells[13].Controls[1];
                            if (Session["estado"].ToString() == "1" || Session["estado"].ToString() == "3")
                            {
                                if (Grilla.Cells[20].Text == "S")
                                    imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_IngresoOfertaVenta_4.aspx?ID=" + Grilla.Cells[0].Text + "&accion=M','Ofertar','width=600,height=400,left=350,top=200,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                                else
                                {
                                    imbControl = (ImageButton)Grilla.Cells[13].Controls[1];
                                    imbControl.ImageUrl = "~/Images/quitar.gif";
                                    imbControl.Enabled = false;
                                }
                            }
                            else
                            {
                                if (Grilla.Cells[19].Text == "S")
                                    imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_IngresoOfertaCompra_4.aspx?ID=" + Grilla.Cells[0].Text + "&accion=M&maxPostCompra=" + Grilla.Cells[18].Text + "','Ofertar','width=600,height=450,left=350,top=150,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                                else
                                {
                                    imbControl = (ImageButton)Grilla.Cells[13].Controls[1];
                                    imbControl.ImageUrl = "~/Images/quitar.gif";
                                    imbControl.Enabled = false;
                                }
                            }
                            imbControl = (ImageButton)Grilla.Cells[12].Controls[1];
                            imbControl.ImageUrl = "~/Images/quitar.gif";
                            imbControl.Enabled = false;
                        }
                    }
                }
                if (Session["tipoPerfil"].ToString() == "B")
                {
                    dtgSubasta1.Columns[12].Visible = false;
                    dtgSubasta1.Columns[13].Visible = false;
                    dtgSubasta1.Columns[14].Visible = false;
                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, ex.Message);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "parent.contratos.location= 'frm_contratos_4.aspx';", true);
        }
    }

    public void OnItemCommand_Click(object sender, DataGridCommandEventArgs e)
    {
        string oError = "";
        string oBoton = ((System.Web.UI.WebControls.ImageButton)e.CommandSource).ID.ToString();

        if (oBoton == "imbEliminar")
        {
            try
            {
                string[] lsNombreParametros = { "@P_numero_rueda", "@P_numero_id", "@P_codigo_operador", "@P_estado" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char };
                string[] lValorParametros = { hndCodSubasta.Value, e.Item.Cells[0].Text, goInfo.cod_comisionista, Session["estado"].ToString() };
                SqlDataReader lLector;
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_DelPostura4", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    oError += lLector["error"].ToString();
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                }
                else
                {
                    oError = "Oferta eliminada correctamente";
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    cargarDatosGrilla();
                }
            }
            catch (Exception ex)
            {
                oError = "Error al eliminar la oferta. " + ex.Message;
            }
            Toastr.Warning(this, oError);

        }
    }
}
