﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_reloj_6.aspx.cs" Inherits="Procesos_frm_reloj_6" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <table border="0" align="center" cellpadding="0" cellspacing="0" runat="server" id="Table1"
        width="100%">
        <tr>
            <td align="center" class="td3">
                <font color='#99FF33' face='Arial, Helvetica, sans-serif' size='3px'>Hora: </font>
            </td>
        </tr>
        <tr>
            <td align="center" class="td4">
                <font color='#F5DA81' face='Arial, Helvetica, sans-serif' size='3px'>
                    <asp:Timer ID="Timer1" OnTick="Timer1_Tick" runat="server" Interval="1000" />
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Timer1" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:Label ID="lblRelog" runat="server"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </font>
            </td>
        </tr>
    </table>
    </asp:Content>