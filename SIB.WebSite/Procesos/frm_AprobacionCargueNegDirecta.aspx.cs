﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;

public partial class Procesos_frm_AprobacionCargueNegDirecta : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Aprobación Cargue Operaciones Negociación Directa";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' And codigo_operador != 0 order by razon_social", 0, 4);
            lConexion.Cerrar();
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        CargarDatos();
    }
    /// <summary>
    /// 
    /// </summary>
    protected void CargarDatos()
    {
        lblMensaje.Text = "";
        DateTime ldFecha;
        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Corte. <br>";
            }
        }
        if (TxtFechaIni.Text.Trim().Length <= 0 && ddlOperador.SelectedValue == "0" && ddlEstado.SelectedValue == "" && ddlPuntaInicial.SelectedValue == "")
            lblMensaje.Text += "Debe Ingresar algun Parametro de búsqueda. <br>";
        if (lblMensaje.Text == "")
        {
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetAprobCargueNegDirecta";
                if (TxtFechaIni.Text.Trim().Length > 0)
                    lComando.Parameters.Add("@P_fecha_negociacion", SqlDbType.VarChar).Value = TxtFechaIni.Text.Trim();
                if (ddlOperador.SelectedValue != "0")
                    lComando.Parameters.Add("@P_codigo_operador", SqlDbType.VarChar).Value = ddlOperador.SelectedValue;
                if (ddlPuntaInicial.SelectedValue != "")
                    lComando.Parameters.Add("@P_punta", SqlDbType.VarChar).Value = ddlPuntaInicial.SelectedValue;
                if (ddlEstado.SelectedValue != "")
                    lComando.Parameters.Add("@P_estado", SqlDbType.VarChar).Value = ddlEstado.SelectedValue;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgMaestro.DataSource = lds;
                dtgMaestro.DataBind();
                if (dtgMaestro.Items.Count > 0)
                {
                    tblGrilla.Visible = true;
                    imbExcel.Visible = true;
                    btnAprobar.Visible = true;
                    btnRechazar.Visible = true;
                }
                else
                    lblMensaje.Text = "No se encontraron Registros para Visualizar.! ";
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pude Generar el Informe.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: ChkTodos_CheckedChanged
    /// Fecha: Abril 24 21 de 2012
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para chekear todos los registros
    /// Modificacion:
    /// </summary>
    protected void ChkTodos_CheckedChanged(object sender, EventArgs e)
    {
        //Selecciona o deselecciona todos los items del datagrid segun el checked del control
        CheckBox chk = (CheckBox)(sender);
        foreach (DataGridItem Grilla in this.dtgMaestro.Items)
        {
            CheckBox Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
            Checkbox.Checked = true;
        }
        btnAprobar.Enabled = true;
        btnRechazar.Enabled = true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfCargueNeg" + DateTime.Now + ".xls";
        string lstitulo_informe = "Informe Cargue Operaciones Negociacion Directa";
        string lsTituloParametros = "";
        try
        {
            if (TxtFechaIni.Text.Trim().Length > 0)
                lsTituloParametros += "Fecha de Corte: " + TxtFechaIni.Text.Trim();
            if (ddlOperador.SelectedValue != "")
                lsTituloParametros += " - Operador : " + ddlOperador.SelectedItem.ToString();
            if (ddlPuntaInicial.SelectedValue != "")
                lsTituloParametros += " - Punta Inicial : " + ddlPuntaInicial.SelectedItem.ToString();
            if (ddlEstado.SelectedValue != "")
                lsTituloParametros += " - Estado : " + ddlEstado.SelectedItem.ToString();

            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgMaestro.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            dtgMaestro.Columns[0].Visible = false;
            dtgMaestro.Columns[14].Visible = true;
            dtgMaestro.Columns[15].Visible = true;
            dtgMaestro.Columns[16].Visible = true;
            dtgMaestro.Columns[17].Visible = true;
            dtgMaestro.Columns[18].Visible = true;
            dtgMaestro.Columns[19].Visible = true;

            lform.Controls.Add(dtgMaestro);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());
            Response.End();

            dtgMaestro.Columns[0].Visible = true;
            dtgMaestro.Columns[14].Visible = false;
            dtgMaestro.Columns[15].Visible = false;
            dtgMaestro.Columns[16].Visible = false;
            dtgMaestro.Columns[17].Visible = false;
            dtgMaestro.Columns[18].Visible = false;
            dtgMaestro.Columns[19].Visible = false;

            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Excel.!" + ex.Message.ToString();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAprobar_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        string lsCodId = "0";
        string lsCodOpC = "0";
        string lsCodOpV = "0";
        string lsMailC = "";
        string lsMailV = "";
        string lsNomOperadorC = "";
        string lsNomOperadorV = "";
        string lsTipoRueda = "";
        string lsLugar = "";
        string lsTipoCont = "";
        string lsCantidad = "";
        string lsPrecio = "";
        string lsFechaNeg = "";

        int liProce = 0;
        string[] lsNombreParametros = { "@P_codigo_id_externo_directa" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        String[] lValorParametros = { "0" };

        lConexion.Abrir();
        SqlTransaction oTransaccion;
        oTransaccion = lConexion.gObjConexion.BeginTransaction(System.Data.IsolationLevel.Serializable);
        // Defino la trnasacción para el manejo de la consistencia de los datos.
        try
        {
            foreach (DataGridItem Grilla in this.dtgMaestro.Items)
            {
                CheckBox Checkbox = null;
                Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                if (Checkbox.Checked == true)
                {
                    liProce++;
                    if (Grilla.Cells[14].Text.Trim() == "I")
                    {
                        
                        lsCodId = Grilla.Cells[21].Text.Trim();
                        lsCodOpC = Grilla.Cells[22].Text.Trim();
                        lsCodOpV = Grilla.Cells[23].Text.Trim();
                        lsMailC = Grilla.Cells[24].Text.Trim();
                        lsMailV = Grilla.Cells[25].Text.Trim();
                        lsNomOperadorC = Grilla.Cells[5].Text.Trim();
                        lsNomOperadorV = Grilla.Cells[6].Text.Trim();
                        lsTipoRueda = Grilla.Cells[1].Text.Trim();
                        lsLugar = Grilla.Cells[7].Text.Trim();
                        lsTipoCont = Grilla.Cells[8].Text.Trim();
                        lsCantidad = Grilla.Cells[12].Text.Trim();
                        lsPrecio = Grilla.Cells[13].Text.Trim();
                        lsFechaNeg = Grilla.Cells[2].Text.Trim();

                        lValorParametros[0] = lsCodId;
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetApruebaCargueNegDir", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                        if (goInfo.mensaje_error != "")
                        {
                            oTransaccion.Rollback();
                            lblMensaje.Text = "Se presento un Problema en la Aprobacion del Registro {" + lsCodId + "}.! " + goInfo.mensaje_error.ToString();
                            lConexion.Cerrar();
                            break;
                        }
                        //else
                        //{
                        //    //// Envio de la Alerta Informando la Aprobación
                        //    string lsAsunto = "";
                        //    string lsMensaje = "";

                        //    ///// Envio del Mail de la Aprobacion al Operador Compra
                        //    lsMensaje = "Señores: " + lsNomOperadorC + " \n\n";
                        //    lsAsunto = "Notificación Aprobacion de Registros Cargados Negociacion Directa";
                        //    lsMensaje += "Nos permitimos  informarle que el Administrador del Sistema de Gas acaba de realizar la Aprobación del Siguiente Registro Cargado en el Sistema de Gas;  \n\n";
                        //    lsMensaje += "Fecha Negoacion: " + lsFechaNeg + " - Punto de Entrega: " + lsLugar + " - Tipo de Contrato: " + lsTipoCont + " - Cantidad: " + lsCantidad + " - Precio: " + lsPrecio + " \n\n\n";
                        //    lsMensaje += "Coordinalmente, \n\n\n";
                        //    lsMensaje += "Administrador Sistema de Gas \n";
                        //    clEmail mailC = new clEmail(lsMailC, lsAsunto, lsMensaje, "");
                        //    lblMensaje.Text = mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                        //    if (lblMensaje.Text == "")
                        //    {
                        //        ///// Envio del Mail de la Aprobacion al Operador Venta
                        //        lsMensaje = "Señores: " + lsNomOperadorV + " \n\n";
                        //        lsAsunto = "Notificación Aprobacion de Registros Cargados Negociacion Directa";
                        //        lsMensaje += "Nos permitimos  informarle que el Administrador del Sistema de Gas acaba de realizar la Aprobación del Siguiente Registro Cargado en el Sistema de Gas;  \n\n";
                        //        lsMensaje += "Fecha Negoacion: " + lsFechaNeg + " - Punto de Entrega: " + lsLugar + " - Tipo de Contrato: " + lsTipoCont + " - Cantidad: " + lsCantidad + " - Precio: " + lsPrecio + " \n\n\n";
                        //        lsMensaje += "Coordinalmente, \n\n\n";
                        //        lsMensaje += "Administrador Sistema de Gas \n";
                        //        clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensaje, "");
                        //        lblMensaje.Text = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                        //        if (lblMensaje.Text != "")
                        //        {
                        //            oTransaccion.Rollback();
                        //            lConexion.Cerrar();
                        //            break;
                        //        }
                        //    }
                        //    else
                        //    {
                        //        oTransaccion.Rollback();
                        //        lConexion.Cerrar();
                        //        break;
                        //    }
                        //}
                        lLector.Close();
                        lLector.Dispose();
                    }
                    else
                    {
                        oTransaccion.Rollback();
                        lblMensaje.Text = "Se escogio un Registro que ya esta aprobado o Rechazado. Solo se pueden aprobar Registros Ingresados.!";
                        break;
                    }
                }
            }
            if (liProce == 0)
            {
                lblMensaje.Text = "No Selecciono Registros para Procesar!.<br>";
                oTransaccion.Rollback();
                lConexion.Cerrar();
            }
            else
            {
                if (lblMensaje.Text == "")
                {
                    oTransaccion.Commit();
                    lblMensaje.Text = "Registros     Aprobados Correctamente.!";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + lblMensaje.Text + "');", true);

                    CargarDatos();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            oTransaccion.Rollback();
            lConexion.Cerrar();
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRechazar_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        string lsCodId = "0";
        string lsCodOpC = "0";
        string lsCodOpV = "0";
        string lsMailC = "";
        string lsMailV = "";
        string lsNomOperadorC = "";
        string lsNomOperadorV = "";
        string lsTipoRueda = "";
        string lsLugar = "";
        string lsTipoCont = "";
        string lsCantidad = "";
        string lsPrecio = "";
        string lsFechaNeg = "";

        int liProce = 0;
        string[] lsNombreParametros = { "@P_codigo_id_externo_directa" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        String[] lValorParametros = { "0" };

        lConexion.Abrir();
        SqlTransaction oTransaccion;
        oTransaccion = lConexion.gObjConexion.BeginTransaction(System.Data.IsolationLevel.Serializable);
        // Defino la trnasacción para el manejo de la consistencia de los datos.
        try
        {
            foreach (DataGridItem Grilla in this.dtgMaestro.Items)
            {
                CheckBox Checkbox = null;
                Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                if (Checkbox.Checked == true)
                {
                    if (Grilla.Cells[14].Text.Trim() == "I")
                    {
                        liProce++;
                        lsCodId = Grilla.Cells[21].Text.Trim();
                        lsCodOpC = Grilla.Cells[22].Text.Trim();
                        lsCodOpV = Grilla.Cells[23].Text.Trim();
                        lsMailC = Grilla.Cells[24].Text.Trim();
                        lsMailV = Grilla.Cells[25].Text.Trim();
                        lsNomOperadorC = Grilla.Cells[5].Text.Trim();
                        lsNomOperadorV = Grilla.Cells[6].Text.Trim();
                        lsTipoRueda = Grilla.Cells[1].Text.Trim();
                        lsLugar = Grilla.Cells[7].Text.Trim();
                        lsTipoCont = Grilla.Cells[8].Text.Trim();
                        lsCantidad = Grilla.Cells[12].Text.Trim();
                        lsPrecio = Grilla.Cells[13].Text.Trim();
                        lsFechaNeg = Grilla.Cells[2].Text.Trim();

                        lValorParametros[0] = lsCodId;
                        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetRechazaCargueNegDir", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                        if (goInfo.mensaje_error != "")
                        {
                            oTransaccion.Rollback();
                            lblMensaje.Text = "Se presento un Problema en el Rechazo del Registro {" + lsCodId + "}.! " + goInfo.mensaje_error.ToString();
                            lLector.Close();
                            lLector.Dispose();
                            lConexion.Cerrar();
                            break;
                        }
                        else
                        {
                            //// Envio de la Alerta Informando la Aprobación
                            string lsAsunto = "";
                            string lsMensaje = "";

                            ///// Envio del Mail de la Aprobacion al Operador Compra
                            lsMensaje = "Señores: " + lsNomOperadorC + " \n\n";
                            lsAsunto = "Notificación Rechazo de Registros Cargados Negociacion Directa";
                            lsMensaje += "Nos permitimos  informarle que el Administrador del Sistema de Gas acaba de realizar el Rechazo del Siguiente Registro Cargado en el Sistema de Gas;  \n\n";
                            lsMensaje += "Fecha Negoacion: " + lsFechaNeg + " - Punto de Entrega: " + lsLugar + " - Tipo de Contrato: " + lsTipoCont + " - Cantidad: " + lsCantidad + " - Precio: " + lsPrecio + " \n\n\n";
                            lsMensaje += "Coordinalmente, \n\n\n";
                            lsMensaje += "Administrador Sistema de Gas \n";
                            clEmail mailC = new clEmail(lsMailC, lsAsunto, lsMensaje, "");
                            lblMensaje.Text = mailC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                            if (lblMensaje.Text == "")
                            {
                                ///// Envio del Mail de la Aprobacion al Operador Venta
                                lsMensaje = "Señores: " + lsNomOperadorV + " \n\n";
                                lsAsunto = "Notificación Rechazo de Registros Cargados Negociacion Directa";
                                lsMensaje += "Nos permitimos  informarle que el Administrador del Sistema de Gas acaba de realizar el Rechazo del Siguiente Registro Cargado en el Sistema de Gas;  \n\n";
                                lsMensaje += "Fecha Negoacion: " + lsFechaNeg + " - Punto de Entrega: " + lsLugar + " - Tipo de Contrato: " + lsTipoCont + " - Cantidad: " + lsCantidad + " - Precio: " + lsPrecio + " \n\n\n";
                                lsMensaje += "Coordinalmente, \n\n\n";
                                lsMensaje += "Administrador Sistema de Gas \n";
                                clEmail mailV = new clEmail(lsMailV, lsAsunto, lsMensaje, "");
                                lblMensaje.Text = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas");
                                if (lblMensaje.Text != "")
                                {
                                    oTransaccion.Rollback();
                                    lConexion.Cerrar();
                                    break;
                                }
                            }
                            else
                            {
                                oTransaccion.Rollback();
                                lConexion.Cerrar();
                                break;
                            }
                        }
                        lLector.Close();
                        lLector.Dispose();
                    }
                    else
                    {
                        lblMensaje.Text = "Se escogio un Registro que ya esta aprobado o Rechazado. Solo se pueden Rechazar Registros Ingresados.!";
                        break;
                    }
                }
            }
            if (liProce == 0)
            {
                lblMensaje.Text = "No Selecciono Registros para Procesar!.<br>";
                oTransaccion.Rollback();
                lConexion.Cerrar();
            }
            else
            {
                if (lblMensaje.Text == "")
                {
                    oTransaccion.Commit();
                    lblMensaje.Text = "Registros Rechazados Correctamente.!";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + lblMensaje.Text + "');", true);
                    CargarDatos();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            lblMensaje.Text = ex.Message;
        }

    }
}