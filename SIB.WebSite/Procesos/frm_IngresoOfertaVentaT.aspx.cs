﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_IngresoOfertaVentaT : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    String strRutaArchivo;
    String strRutaArchivo1;
    SqlDataReader lLector;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            if (this.Request.QueryString["ID"] != null && this.Request.QueryString["ID"].ToString() != "")
            {
                hndID.Value = this.Request.QueryString["ID"];
                hndAccion.Value = this.Request.QueryString["accion"];
                hdfEstadoRueda.Value = this.Request.QueryString["estadoRueda"];
                hdfTipoOperador.Value = this.Request.QueryString["tipoOperador"];
                /// Obtengo los Datos del ID Recibido
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_id_rueda", " numero_id = " + hndID.Value + " ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    /// Lleno las etiquetas de la pantalla con los datos del Id.
                    lblRueda.Text = "No. Rueda: " + lLector["numero_rueda"].ToString();
                    hdfNoRueda.Value = lLector["numero_rueda"].ToString();
                    lblId.Text = "ID: " + this.Request.QueryString["ID"];
                    lblFecha.Text = "Fecha Rueda: " + lLector["fecha_rueda"].ToString().Substring(0, 10);
                    lblProducto.Text = "Producto: " + lLector["codigo_producto"].ToString() + "-" + lLector["desc_producto"].ToString();
                    lblUnidadMedida.Text = "Unidad Medida: " + lLector["codigo_unidad_medida"].ToString() + "-" + lLector["desc_unidad_medida"].ToString();
                    if (Session["destino_rueda"].ToString() == "G")
                        lblPuntoEntrega.Text = "Punto Entrega: " + lLector["codigo_punto_entrega"].ToString() + "-" + lLector["desc_punto_entrega"].ToString();
                    else
                        lblPuntoEntrega.Text = "Ruta: " + lLector["codigo_punto_entrega"].ToString() + "-" + lLector["desc_punto_entrega"].ToString();
                    lblPeridoEnt.Text = "Periodo Entrega: " + lLector["codigo_periodo"].ToString() + "-" + lLector["desc_periodo"].ToString();
                    lblModalidad.Text = "Modalidad Contractual: " + lLector["codigo_modalidad"].ToString() + "-" + lLector["desc_modalidad"].ToString();
                    /// Reviso si se esta creando la oferta o modificando
                    lLector.Close();
                    lLector.Dispose();

                    if (hdfEstadoRueda.Value == "1")
                    {
                        TxtCntNoNominada.Enabled = true;
                        TxtCntNoDispo.Enabled = false;
                        TxtPrecio.Enabled = false;
                    }
                    else
                    {
                        TxtCntNoNominada.Enabled = false;
                        if (hdfTipoOperador.Value != "G")
                            TxtCntNoDispo.Enabled = false;
                        else
                            TxtCntNoDispo.Enabled = true;
                        TxtPrecio.Enabled = true;
                    }
                    if (Session["destino_rueda"].ToString() == "T")
                    {
                        TxtPrecio.Enabled = false;
                        TxtPrecio.Text = "0";
                    }
                    /// Reviso si ya existe una oferta para el ID y Operador para obtener
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_posturas_venta", " numero_id = " + hndID.Value + " And codigo_operador = '" + goInfo.cod_comisionista + "' ");
                    if (lLector.HasRows)
                    {
                        hndAccion.Value = "M";
                        lLector.Read();
                        TxtCntNoNominada.Text = lLector["cantidad_no_nominada"].ToString().Replace("&nbsp;", "");
                        TxtCntNoDispo.Text = lLector["cantidad_no_disponible"].ToString().Replace("&nbsp;", "");
                        TxtPrecio.Text = lLector["precio"].ToString().Replace("&nbsp;", "");
                        hdfNoPostura.Value = lLector["numero_postura"].ToString();
                        if (Convert.ToDecimal(TxtPrecio.Text.Trim()) != 0)
                            btnOfertar.Text = "Modificar Oferta";
                    }
                    else
                        hndAccion.Value = "C";
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();

                }
                else
                    Toastr.Warning(this, "El ID enviado NO existe en la base de Datos.!");
            }
            else
                Toastr.Warning(this, "No Se enviaron los Parametros requeridos.!");
            //hndCodSubasta.Value = "21";
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOfertar_Click(object sender, EventArgs e)
    {

        string sCerrar = "N";
        string oError = "";
        string sProc = "";
        string lsError = "";
        string lsNoPostura = "";

        string[] lsNombreParametros = { "@P_numero_postura", "@P_numero_id", "@P_numero_rueda", "@P_codigo_operador", "@P_precio_postura", "@P_cantidad_no_nominada", "@P_cantidad_no_disponible", "@P_accion", "@P_estado_rueda" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Float, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "0", "0", "0", "0", "0", "0", "0", "1" }; ///  Accion 1=Crear 2=Modificar

        lsError = ValidarEntradas();
        if (lsError == "")
        {
            lConexion.Abrir();
            try
            {
                lValorParametros[1] = hndID.Value;
                lValorParametros[2] = hdfNoRueda.Value;
                lValorParametros[3] = goInfo.cod_comisionista;
                if (hdfEstadoRueda.Value == "2")
                {
                    lValorParametros[4] = TxtPrecio.Text.Trim();
                    if (hdfTipoOperador.Value != "G")
                        lValorParametros[6] = "0";
                    else
                        lValorParametros[6] = TxtCntNoDispo.Text.Trim().Replace(",", "");
                }
                else
                    lValorParametros[5] = TxtCntNoNominada.Text.Trim().Replace(",", "");
                if (hndAccion.Value == "M")
                {
                    lValorParametros[0] = hdfNoPostura.Value;
                    lValorParametros[7] = "2";
                }
                else
                    lValorParametros[7] = "1";
                lValorParametros[8] = hdfEstadoRueda.Value;

                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetPosturaVentaId", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    oError = "Error al Actualizar la Postura. " + goInfo.mensaje_error.ToString() + "\\n";
                }
                else
                {
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        if (lLector["mensaje"].ToString() == "")
                        {
                            lLector.Close();
                            lLector.Dispose();
                            lConexion.Cerrar();
                            Session["mis_ofertas"] = "S";
                            if (hndAccion.Value == "C")
                                Toastr.Success(this, "Posturas Ingresadas Correctamente.!");
                            else
                                Toastr.Success(this, "Posturas Actualizadas Correctamente.!");  

                              
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                        }
                        else
                        {
                            oError = lLector["mensaje"].ToString();
                            lLector.Close();
                            lLector.Dispose();
                            lConexion.Cerrar();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oError = "Error al realizar la oferta. " + ex.Message;
                lConexion.Cerrar();
            }
            if (oError != "")
            {
                Toastr.Warning(this, oError);
            }
        }
        else
            Toastr.Warning(this, lsError);

    }
    /// <summary>
    /// Relaiza la validacion del Ingreso de las Posturas
    /// </summary>
    /// <returns></returns>
    protected string ValidarEntradas()
    {
        string lsError = "";
        int oValor = 0;
        double oValor1 = 0;

        if (hdfEstadoRueda.Value == "1")
        {
            if (TxtCntNoNominada.Text.Trim().Length > 0)
            {
                try
                {
                    oValor = Convert.ToInt32(TxtCntNoNominada.Text.Replace(",",""));
                    if (oValor <= 0)
                        lsError += "La Cantidad No Nominada debe ser mayor que 0. \\n";

                }
                catch (Exception ex)
                {
                    lsError += "Valor Invalido en la Cantidad No Nominada de la Postura. \\n";
                }
            }
            else
                lsError += "Debe Ingresar la Cantidad No Nominada de la Postura. \\n";
        }
        else
        {
            if (TxtCntNoDispo.Text.Trim().Length > 0)
            {
                try
                {
                    oValor = Convert.ToInt32(TxtCntNoDispo.Text.Replace(",", ""));
                    if (oValor > Convert.ToInt32(TxtCntNoNominada.Text))
                        lsError += "La Cantidad No Disponible debe ser menor o Igual a la cantidad no Nominada de la Postura. \\n";
                }
                catch (Exception ex)
                {
                    lsError += "Valor Invalido en la Cantidad No Disponible de la Postura. \\n";
                }
            }
            else
                lsError += "Debe Ingresar la Cantidad No Disponible de la Postura. \\n";

            if (TxtPrecio.Text.Trim().Length > 0)
            {
                try
                {
                    oValor1 = Convert.ToDouble(TxtPrecio.Text.Replace(",", ""));
                }
                catch (Exception ex)
                {
                    lsError += "Valor Invalido en el Precio de la Postura. \\n";
                }
            }
            else
                lsError += "Debe Ingresar el Precio de la Postura. \\n";
        }
        return lsError;
    }
}