﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_DecSubBimNormal.aspx.cs" Inherits="Procesos.frm_DecSubBimNormal" MasterPageFile="~/PlantillaPrincipal.master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" lang="javascript">

        function addCommas(sValue) {
            var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
            while (sRegExp.test(sValue)) {
                sValue = sValue.replace(sRegExp, '$1,$2');
            }
            return sValue;
        }

        function formatoMiles(obj, evt) {
            var T;
            var strValue = removeCommas(obj.value);
            //if ((T = /(^-?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) {
            if ((T = /([0-9]*)$/i.exec(strValue)) != null) {
                //                T[4] = T[4].substr(0, decimales);
                //                if (T[2] == '' && T[3] == '.') T[2] = 0;
                obj.value = addCommas(T[1]);
            } else {
                alert('formato invalido ');
            }
        }
        function formatoMilesPre(obj, evt, decimales) {
            var T;
            var strValue = removeCommas(obj.value);
            if ((T = /(^0?)([0-9]*)(\.?)([0-9]*)$/i.exec(strValue)) != null) { //20180726 ajuste
                T[4] = T[4].substr(0, decimales);
                if (T[2] == '' && T[3] == '.') {
                    T[2] = 0;
                }
                obj.value = T[1] + addCommas(T[2]) + T[3] + T[4];
            }
            else {
                alert('formato invalido ');
            }
        }

        function removeCommas(strValue) {
            var objRegExp = /,/g; //search for commas globally
            return strValue.replace(objRegExp, '');
        }

    </script>
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <div class="kt-portlet__body">
                <div class="row ">
                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <div class="form-group">
                            <asp:Label runat="server" Style="float: left">Operador</asp:Label>
                            <asp:DropDownList ID="ddlBusOperador" runat="server" Width="100%" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <div class="form-group">
                            <asp:Label runat="server" Style="float: left">Fuente</asp:Label>
                            <asp:DropDownList ID="ddlBusFuente" runat="server" Width="100%" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <div class="form-group">
                            <asp:Label runat="server" Style="float: left">Punto del SNT</asp:Label>
                            <asp:DropDownList ID="ddlBusPozo" runat="server" Width="100%" CssClass="form-control" />
                        </div>
                    </div>
                </div>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" OnItemCommand="dtgConsulta_EditCommand"
                                OnPageIndexChanged="dtgConsulta_PageIndexChanged" PageSize="10">
                                <Columns>
                                    <%--0--%>
                                    <asp:BoundColumn DataField="codigo_declaracion" HeaderText="Codigo" Visible="false" />
                                    <%--1--%>
                                    <%--20170716--%>
                                    <asp:BoundColumn DataField="año_subasta" HeaderText="Año Rueda" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <%--2--%>
                                    <asp:BoundColumn DataField="numero_rueda" HeaderText="Rueda" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <%--3--%>
                                    <asp:BoundColumn DataField="fecha_rueda" HeaderText="Fecha Rueda" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <%--4--%>
                                    <asp:BoundColumn DataField="codigo_operador" HeaderText="Codigo Operador" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <%--5--%>
                                    <asp:BoundColumn DataField="nombre_operador" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--6--%>
                                    <asp:BoundColumn DataField="tipo_declaracion" Visible="false" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--7--%>
                                    <asp:BoundColumn DataField="codigo_fuente" HeaderText="Codigo Fuente" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <%--8--%>
                                    <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--9--%>
                                    <asp:BoundColumn DataField="campo_declinacion" HeaderText="En Declinación" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <%--10--%>
                                    <asp:BoundColumn DataField="codigo_pozo" HeaderText="Codigo Punto" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <%--11--%>
                                    <asp:BoundColumn DataField="desc_pozo" HeaderText="Punto SNT" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--12--%>
                                    <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="right"
                                        DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                    <%--13--%>
                                    <asp:BoundColumn DataField="precio_reserva" HeaderText="Precio Reserva" ItemStyle-HorizontalAlign="right"
                                        DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                                    <%--14--%>
                                    <asp:BoundColumn DataField="precio_delta" HeaderText="delta precio" ItemStyle-HorizontalAlign="right"
                                        DataFormatString="{0:###,###,###,###,##0.00}"></asp:BoundColumn>
                                    <%--15--%>
                                    <asp:BoundColumn DataField="Estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <%--16--%>
                                    <%-- <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass=""></asp:EditCommandColumn>--%>
                                    <%--17--%>
                                    <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar" HeaderStyle-CssClass="" Visible="False"></asp:EditCommandColumn>
                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more-1"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                    <!--begin::Nav-->
                                                    <ul class="kt-nav">
                                                        <li class="kt-nav__item">
                                                            <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                                        <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                                        <span class="kt-nav__link-text">Modificar</span>
                                                            </asp:LinkButton>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                                        <i class="kt-nav__link-icon flaticon-delete"></i>
                                                                        <span class="kt-nav__link-text">Eliminar</span>
                                                            </asp:LinkButton>
                                                        </li>
                                                    </ul>
                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%--Modals--%>
    <%--Agregar--%>
    <div class="modal fade" id="registroCapInye" tabindex="-1" role="dialog" aria-labelledby="mdlregistroCapInyeLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="registroCapInyeInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlregistroCapInyeLabel" runat="server">Agregar</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6" runat="server">
                                    <div class="form-group">
                                        <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
                                        <asp:DropDownList ID="ddlOperador" runat="server" enable="false" Width="100%" CssClass="form-control" />
                                    </div>
                                </div>
                                <div id="trdeclina" class="col-sm-12 col-md-6 col-lg-6" runat="server">
                                    <div class="form-group">
                                        <asp:Label Text="Campo en Declinación?" AssociatedControlID="ddlDeclina" runat="server" />
                                        <asp:DropDownList ID="ddlDeclina" runat="server" Width="100%" CssClass="form-control">
                                            <asp:ListItem Value="S">Si</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="divFuenteCampo" class="col-sm-12 col-md-6 col-lg-6" runat="server">
                                    <div class="form-group">
                                        <asp:Label Text="Fuente o Campo" AssociatedControlID="ddlFuente" runat="server" />
                                        <asp:DropDownList ID="ddlFuente" runat="server" OnSelectedIndexChanged="ddlFuente_SelectedIndexChanged" AutoPostBack="True" Width="100%" CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Punto del SNT" AssociatedControlID="ddlPozo" runat="server" />
                                        <asp:DropDownList ID="ddlPozo" runat="server" Width="100%" CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Cantidad disponible" AssociatedControlID="TxtCantidad" runat="server" />
                                        <asp:TextBox ID="TxtCantidad" runat="server" onkeyup="return formatoMiles(this,event);" Width="100%" CssClass="form-control"></asp:TextBox>
                                        <%--20180726 ajuste--%>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="fteBTxtCantidad" runat="server" TargetControlID="TxtCantidad" FilterType="Custom, Numbers" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Precio de reserva" AssociatedControlID="TxtPrecio" runat="server" />
                                        <asp:TextBox ID="TxtPrecio" Width="100%" CssClass="form-control" runat="server" onkeyup="return formatoMilesPre(this,event,2);"></asp:TextBox>
                                        <%--20180726 ajuste--%>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtPrecio" runat="server" TargetControlID="TxtPrecio" FilterType="Custom, Numbers" ValidChars="."></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <div id="trDelta" runat="server">
                                            <asp:Label Text="Delta de Precio" AssociatedControlID="TxtDelta" runat="server" />
                                            <asp:TextBox ID="TxtDelta" runat="server" onkeyup="return formatoMilesPre(this,event,2);" Width="100%" CssClass="form-control"></asp:TextBox>
                                            <%--20180726 ajuste--%>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtDelta" runat="server" TargetControlID="TxtDelta" FilterType="Custom, Numbers" ValidChars="."></ajaxToolkit:FilteredTextBoxExtender>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="imbSalir" runat="server" Text="Cancelar" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="btnCancerlar_Click1" CssClass="btn btn-secondary" />
                                <asp:Button ID="imbCrear" runat="server" Text="Aceptar" OnClientClick="this.disabled = true;" CssClass="btn btn-primary" UseSubmitBehavior="false" OnClick="imbCrear_Click1" ValidationGroup="comi" />
                                <asp:Button ID="imbActualiza" runat="server" Text="Actualizar" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="imbActualiza_Click1" ValidationGroup="comi" CssClass="btn btn-primary" />
                                <ajaxToolkit:ConfirmButtonExtender ID="CbeimbCrear" runat="server" TargetControlID="imbCrear" ConfirmText="Esta Seguro(a) de Ingresar la Información ? "></ajaxToolkit:ConfirmButtonExtender>
                                <ajaxToolkit:ConfirmButtonExtender ID="CbeimbActualiza" runat="server" TargetControlID="imbActualiza" ConfirmText="Esta Seguro(a) de Modificar la Información ? "></ajaxToolkit:ConfirmButtonExtender>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
