﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_solicitudToken.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Procesos.frm_solicitudToken" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">

                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Corte" AssociatedControlID="TxtFechaCorte" runat="server" />
                            <asp:TextBox ID="TxtFechaCorte" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ValidationGroup="detalle" Width="100%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Subasta" AssociatedControlID="ddlSubasta" runat="server" />
                            <asp:DropDownList ID="ddlSubasta" runat="server" OnSelectedIndexChanged="ddlSubasta_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tipo Rueda" AssociatedControlID="ddlTipoRueda" runat="server" />
                            <asp:DropDownList ID="ddlTipoRueda" runat="server" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Contrato" AssociatedControlID="TxtFechaIni" runat="server" />
                            <asp:TextBox ID="TxtFechaIni" Width="100%" placeholder="yyyy/mm/dd" ValidationGroup="detalle" CssClass="form-control datepicker" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Número Rueda" AssociatedControlID="TxtNoRueda" runat="server" />
                            <asp:TextBox ID="TxtNoRueda" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
                            <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Id Registro" AssociatedControlID="TxtId" runat="server" />
                            <asp:TextBox ID="TxtId" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Número Operación" AssociatedControlID="TxtOperacion" runat="server" />
                            <asp:TextBox ID="TxtOperacion" runat="server" ValidationGroup="detalle" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server" id="tblGrilla" visible="false">
                    <div class="table table-responsive">
                        <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                            <Columns>
                                <asp:TemplateColumn HeaderText="Sele.">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="ChkTodos" runat="server" OnCheckedChanged="ChkTodos_CheckedChanged"
                                            AutoPostBack="true" Text="Seleccionar Todos" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="ChkSeleccionar" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="codigo_verif_contrato" HeaderText="Id Registro" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numero_contrato" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="contrato_definitivo" HeaderText="Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numero_id" HeaderText="No. Id" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numero_rueda" HeaderText="No. Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_contrato" HeaderText="Fecha Contrato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_venta" HeaderText="Operador Venta" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="operador_compra" HeaderText="Operador Compra" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tipo_rueda" HeaderText="Tipo  Rueda" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_entrega" HeaderText="Fecha Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ind_modif_pend" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tiene_token" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tiene_proceso" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tiene_firma" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="estado" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_tipo_subasta" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_estado" HeaderText="Estado"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <asp:HiddenField ID="hdfOperador" runat="server" />
    <asp:HiddenField ID="hdfRutaCert" runat="server" />
    <asp:HiddenField ID="hdfBusOper" runat="server" />

</asp:Content>
