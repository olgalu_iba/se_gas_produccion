﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_Suspender_3 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    String strRutaArchivo;
    String strRutaArchivo1;
    SqlDataReader lLector;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString() + "mensaje.txt";
        strRutaArchivo1 = ConfigurationManager.AppSettings["RutaArchivos"].ToString() + "subasta-" + Session["numero_rueda"].ToString() + ".txt";
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            /// Obtengo los Datos de la rueda
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda", " numero_rueda= " + Session["numero_rueda"] + " and estado not in ('F','Z')");
            if (lLector.HasRows)
            {
                lLector.Read();
                /// Lleno las etiquetas de la pantalla con los datos del Id.
                lblFechaAct.Text = "Fecha Act: " + lLector["fecha_prox_apertura"].ToString();
                lLector.Close();
                lLector.Dispose();
            }
            else
                Toastr.Warning(this, "La rueda ya está finalizada.!");
           
            lLector.Dispose();
            lLector.Close();
            lConexion.Cerrar();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSuspender_Click(object sender, EventArgs e)
    {
        string oError = "";
        string sMensaje = "";

        //if (TxtHora.Text == "")
        //    oError += "Debe digitar la Hora de inicio de la próxima ronda\\n";
        //if (TxtMinutos.Text == "")
        //    oError += "Debe la duración de la próxima ronda\\n";
        if (ddlDefinitivo.SelectedValue == "N")
        {
            if (TxtFecha.Text == "")
                oError += "Debe digitar la próxima fecha de apertura\\n";
        }
        else
        {
            if (TxtFecha.Text != "")
                oError += "NO Debe digitar la próxima fecha de apertura para suspensión definitiva\\n";
        }
        if (TxtObservacion.Text == "")
            oError += "Debe digitar las observaciones de suspensión\\n";

        if (oError == "")
        {
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_fecha", "@P_ind_definitivo", "@P_observaciones" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int,SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { Session["numero_rueda"].ToString(), TxtFecha.Text, ddlDefinitivo.SelectedValue, TxtObservacion.Text }; ///  

            SqlDataReader lLector;
            lConexion.Abrir();
            try
            {
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_setSuspender3", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        oError = lLector["error"].ToString() + "\\n";
                }
                if (oError == "")
                {
                    if (ddlDefinitivo.SelectedValue == "N")
                    {
                            sMensaje = "La rueda se suspendió para reactivarse el día " + TxtFecha.Text ;
                    }
                    else
                        sMensaje = "La rueda se suspendió definitivamente";
                    File.SetCreationTime(strRutaArchivo, DateTime.Now);
                    File.SetCreationTime(strRutaArchivo1, DateTime.Now);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + sMensaje + "');", true);
                    Session["hora_mensaje"] = "";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                oError = "Error al suspender la rueda. " + ex.Message;
                lConexion.Cerrar();
            }
        }
        if (oError != "")
        {
            Toastr.Warning(this, oError);
      
        }
    }
}
