﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_reservaRemanentePtdvf.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Procesos.frm_reservaRemanentePtdvf" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fuente" AssociatedControlID="ddlFuente" runat="server" />
                            <asp:DropDownList ID="ddlFuente" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                </div>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgConsulta" Visible="False" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width="100%" CssClass="table-bordered" PagerStyle-HorizontalAlign="Center">
                                <Columns>
                                    <%--0--%>
                                    <asp:BoundColumn DataField="codigo_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <%--1--%>
                                    <asp:BoundColumn DataField="desc_fuente" HeaderText="Nombre Fuente" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
                                    <%--2--%>
                                    <asp:BoundColumn DataField="fecha_ini" HeaderText="Fecha inicial" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <%--3--%>
                                    <asp:BoundColumn DataField="fecha_fin" HeaderText="Fecha final" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                    <%--4--%>
                                    <asp:BoundColumn DataField="remanente_ptdvf" HeaderText="PTDVF Remanente" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                    <%--5--%>
                                    <asp:BoundColumn DataField="remanente_cidvf" HeaderText="CIDVF Remanente" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                    <%--6--%>
                                    <asp:BoundColumn DataField="remanente_ptdvf" Visible="false"></asp:BoundColumn>
                                    <%--7--%>
                                    <asp:BoundColumn DataField="cantidad_reserva" HeaderText="PTDVF/CIDVF Reservada" ItemStyle-HorizontalAlign="Right"
                                        DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                    <%--8--%>
                                    <asp:TemplateColumn HeaderText="Cantidad Reservar">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TxtCantidadRes" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" CssClass="form-control" MaxLength="100" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--9--%>
                                    <asp:TemplateColumn HeaderText="Seleccionar">
                                        <ItemTemplate>
                                            <label class="kt-checkbox">
                                                <asp:CheckBox ID="chkSel" runat="server" />
                                                <span></span>
                                            </label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--10--%>
                                    <asp:BoundColumn DataField="cantidad_reserva" Visible="false"></asp:BoundColumn>
                                    <%--11--%>
                                    <asp:BoundColumn DataField="remanente_ptdvf" Visible="false"></asp:BoundColumn>
                                    <%--12--%>
                                    <asp:BoundColumn DataField="remanente_total" Visible="false"></asp:BoundColumn>
                                </Columns>
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
