﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using Segas.Web.Elements;

public partial class Procesos_frm_definirPrecio_1 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    String strRutaArchivo;
    SqlDataReader lLector;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
        if (!IsPostBack)
        {
            if (this.Request.QueryString["ID"] != null && this.Request.QueryString["ID"].ToString() != "")
            {
                strRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString() +"subasta-" + Session["numero_rueda"].ToString() + ".txt";
                hndID.Value = this.Request.QueryString["ID"];
                hndRonda.Value = this.Request.QueryString["ronda"];
                hndPrecioRes.Value = this.Request.QueryString["precio_reserva"];
                /// Obtengo los Datos del ID Recibido
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_id_rueda idr ", " idr.numero_id = " + hndID.Value);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    /// Lleno las etiquetas de la pantalla con los datos del Id.
                    lblTipo.Text = "Tipo: " + lLector["desc_modalidad"].ToString();
                    lblDuracion.Text = "Duración: " + lLector["tiempo_entrega"].ToString();
                    lblId.Text = "ID: " + this.Request.QueryString["ID"];
                    lblLugar.Text = "Lugar: " + lLector["desc_punto_entrega"].ToString();
                    lblPrecioRes.Text = "Precio Reserva: " + hndPrecioRes.Value;
                    lblRonda.Text = "Ronda actual: " + this.Request.QueryString["ronda"];
                    lLector.Close();
                    lLector.Dispose();
                }
                else
                {
                    Toastr.Warning(this, "El ID enviado NO existe.!");
                   
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                }
                lLector.Dispose();
                lLector.Close();
            }
            else
            {
                Toastr.Warning(this, "No Se enviaron los Parametros requeridos.!");              
                ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnPrecio_Click(object sender, EventArgs e)
    {
        string oError = "";
        string sMensaje = "";
        double ldPreAct = 0;
        double ldPreRes = 0;

        //if (TxtHora.Text == "")
        //    oError += "Debe digitar la Hora de inicio de la próxima ronda\\n";
        //if (TxtMinutos.Text == "")
        //    oError += "Debe la duración de la próxima ronda\\n";
        if (TxtPrecio.Text == "")
            oError += "Debe digitar el precio de reserva para la próxima ronda\\n";
        else
        {
            ldPreAct = Convert.ToDouble(TxtPrecio.Text.Replace(",", ""));
            ldPreRes = Convert.ToDouble(hndPrecioRes.Value);
            if (ldPreAct <= ldPreRes)
                oError += "El precio para la proxima fase debe ser mayor que precio de la ronda actual\\n";
        }

        if (oError == "")
        {
            string[] lsNombreParametros = { "@P_numero_rueda", "@P_numero_id", "@P_ronda", "@P_precio_reserva" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Float };
            string[] lValorParametros = { Session["numero_rueda"].ToString(), hndID.Value, hndRonda.Value, TxtPrecio.Text.Replace(",", "") }; ///  

            SqlDataReader lLector;
            lConexion.Abrir();
            try
            {
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_setPrecioRes", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        oError = lLector["error"].ToString() + "\\n";
                }
                if (oError == "")
                {
                    File.SetCreationTime(strRutaArchivo, DateTime.Now);
                    sMensaje = "Precio de reserva actualizado Correctamente";
                    Toastr.Success(this, sMensaje);
                    Session["hora"] = "";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                oError = "Error al definir el precio de reserva. " + ex.Message;
                lConexion.Cerrar();
            }
        }
        if (oError != "")
        {
            Toastr.Warning(this, oError);
        }
    }
}
