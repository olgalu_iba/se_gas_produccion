﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;
using Segas.Web.Elements;

public partial class Procesos_frm_ConsultaProyDemandaReg : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Consulta Proyección Demanda Regulada";
    clConexion lConexion = null;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        buttons.ExportarExcelOnclick += ImgExcel_Click;
        buttons.FiltrarOnclick += btnConsultar_Click;

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, DdlOperador, "m_operador", " estado = 'A' order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlMesIni, "m_mes", " 1=1  order by mes", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMesFin, "m_mes", " 1=1  order by mes", 0, 1);
            if (Session["tipoPerfil"].ToString() == "N")
            {
                DdlOperador.SelectedValue = goInfo.cod_comisionista;
                DdlOperador.Enabled = false;
            }
            lConexion.Cerrar();
            EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Excel };
   
            buttons.Inicializar(botones: botones);
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            if (lsTabla != "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, EventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfDecInfSb" + DateTime.Now + ".xls";
        string lstitulo_informe = "";
        string lsTituloParametros = "";
        try
        {
            lstitulo_informe = "Consulta Proyección Demanda Regulada";
            lsTituloParametros += " - Año y Mes Inicial: " + TxtAnoIni.Text.Trim()+" - "+ddlMesIni.SelectedItem.ToString();
            lsTituloParametros += " - Año y Mes Final: " + TxtAnoFin.Text.Trim() + " - " + ddlMesFin.SelectedItem.ToString();
            if (DdlOperador.SelectedValue != "0")
                lsTituloParametros += "  - Operador: " + DdlOperador.SelectedItem.ToString();
            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgMaestro.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            lform.Controls.Add(dtgMaestro);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());

            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "No se Pudo Generar el Excel.! " + ex.Message.ToString());
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        bool Error = false;
        DateTime ldFechaI = DateTime.Now;
        if (TxtAnoIni.Text == "")
        {
            Toastr.Warning(this, "Debe Seleccionar el año inicial. <br>");
            Error = true;
        }
        if (ddlMesIni.SelectedValue == "0")
        {
            Toastr.Warning(this, "Debe Seleccionar el mes inicial. <br>");
            Error = true;
        }
        if (TxtAnoFin.Text == "")
        {
            Toastr.Warning(this, "Debe Seleccionar el año final. <br>");
            Error = true;
        }
        if (ddlMesFin.SelectedValue == "0")
        {
            Toastr.Warning(this, "Debe Seleccionar el mes final. <br>");
            Error = true;
        }
        if (!Error)
        {
            if (Convert.ToInt32(TxtAnoIni.Text) * 100 + Convert.ToInt32(ddlMesIni.SelectedValue) > Convert.ToInt32(TxtAnoFin.Text) * 100 + Convert.ToInt32(ddlMesFin.SelectedValue))
            {
                Toastr.Warning(this, "El año-Mes Inicial debe ser menor o igual al año-mes final. <br>");
                Error = true;

            }
        }
        if (!Error)
        {
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetConsProyDemandaRegulada";
                lComando.Parameters.Add("@P_ano_ini", SqlDbType.Int).Value = TxtAnoIni.Text.Trim();
                lComando.Parameters.Add("@P_mes_ini", SqlDbType.Int).Value = ddlMesIni.SelectedValue;
                lComando.Parameters.Add("@P_ano_fin", SqlDbType.Int).Value = TxtAnoFin.Text.Trim();
                lComando.Parameters.Add("@P_mes_fin", SqlDbType.Int).Value = ddlMesFin.SelectedValue;
                lComando.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = DdlOperador.SelectedValue;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgMaestro.DataSource = lds;
                dtgMaestro.DataBind();
                if (dtgMaestro.Items.Count > 0)
                {
                    tblGrilla.Visible = true;
                    imbExcel.Visible = true;
                }
                else
                    Toastr.Warning(this, "No hay Datos para Visualizar.!");
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pudo Generar el Informe.! " + ex.Message.ToString());
                
            }
        }
        else
        {
            tblGrilla.Visible = false;
            imbExcel.Visible = false;
        }
    }
}