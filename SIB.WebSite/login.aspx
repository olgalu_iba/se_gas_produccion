﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="login" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="es">
<!--<![endif]-->
<!-- begin::Head -->
<head>

    <!--begin::Base Path (base relative path for assets of this page) -->
    <base href="../">

    <!--end::Base Path -->
    <meta charset="utf-8" />
    <title>SEGAS | Login</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <!--end::Fonts -->

    <!--begin::Page Scripts(used by this page) -->
    <script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
    <script type="text/javascript" src='<%=Page.ResolveUrl("~/Scripts/jquery-3.4.1.min.js")%>'></script>
    <script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/vendor/toastr.min.js")%>"></script>
    <script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/dev/toastr.js")%>"></script>

    <script type="text/javascript">
        var onloadCallback = function () {
            grecaptcha.render('dvCaptcha', {
                'sitekey': '<%=ReCaptchaKey %>',
                'callback': function (response) {
                    $.ajax({
                        type: "POST",
                        url: "Login.aspx/VerifyCaptcha",
                        data: "{response: '" + response + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (r) {
                            var captchaResponse = jQuery.parseJSON(r.d);
                            if (captchaResponse.success) {
                                $("[id*=txtCaptcha]").val(captchaResponse.success);
                                $("[id*=rfvCaptcha]").hide();
                            } else {
                                $("[id*=txtCaptcha]").val("");
                                $("[id*=rfvCaptcha]").show();
                                var error = captchaResponse["error-codes"][0];
                                $("[id*=rfvCaptcha]").html("RECaptcha error. " + error);
                            }
                        }
                    });
                }
            });
        };

        function validar() {
            var message = 'Por favor marque la casilla de verificación';

            if (typeof (grecaptcha) !== 'undefined') {
                var response = grecaptcha.getResponse();
                (response.length === 0) ? (message = '') : (message = 'Éxito!');
            }
            jQuery('#lblCaptcha').html(message);
            jQuery('#lblCaptcha').css({ "color": (message.toLowerCase() === 'éxito!') ? "green" : "red", "font-size": "80%" });
            return false;
        };
    </script>

        <%--Ocultar F12 y click derecho, copiado y pegado 20201029--%>
    <script>
        //$(document).ready(function () {
        //    $('*').bind("cut copy paste", function (e) {
        //        alert("Funcion no permitida xx");
        //        return false;
        //    });
        //});
        document.onkeydown = mykeyhandler;

        function mykeyhandler(event) {
            event = event || window.event;
            var tgt = event.target || event.srcElement;
            //if ((event.altKey && event.keyCode == 37) || (event.altKey && event.keyCode == 39) || (event.ctrlKey && event.keyCode == 78) || (event.ctrlKey && event.keyCode == 67) || (event.ctrlKey && event.keyCode == 86) || (event.ctrlKey && event.keyCode == 85) || (event.ctrlKey && event.keyCode == 45) || (event.shiftKey && event.keyCode == 45)) {
            if ((event.altKey && event.keyCode == 37) || (event.altKey && event.keyCode == 39) || (event.ctrlKey && event.keyCode == 78) || (event.ctrlKey && event.keyCode == 85) || (event.ctrlKey && event.keyCode == 45) || (event.shiftKey && event.keyCode == 45)) {
                event.cancelBubble = true;
                event.returnValue = false;
                //alert("Funcion no permitida");
                return false;
            }
            if (event.keyCode == 18 && tgt.type != "text" && tgt.type != "password" && tgt.type != "textarea" && tgt.type != "application/x-shockwave-flash") {
                return false;
            }
            if (event.keyCode == 8 && tgt.type != "text" && tgt.type != "password" && tgt.type != "textarea" && tgt.type != "application/x-shockwave-flash") {
                return false;
            }
            if (event.ctrlKey && (event.keyCode == 107 || event.keyCode == 109)) {
                return false;
            }
            if ((event.keyCode >= 112) && (event.keyCode <= 123)) {
                if (navigator.appName == "Microsoft Internet Explorer") {
                    window.event.keyCode = 0;
                }
                return false;
            }
        }

        function mouseDown(e) {
            var ctrlPressed = 0;
            var altPressed = 0;
            var shiftPressed = 0;
            if (parseInt(navigator.appVersion) > 3) {
                if (navigator.appName == "Netscape") {
                    var mString = (e.modifiers + 32).toString(2).substring(3, 6);
                    shiftPressed = (mString.charAt(0) == "1");
                    ctrlPressed = (mString.charAt(1) == "1");
                    altPressed = (mString.charAt(2) == "1");
                    self.status = "modifiers=" + e.modifiers + " (" + mString + ")"
                } else {
                    shiftPressed = event.shiftKey;
                    altPressed = event.altKey;
                    ctrlPressed = event.ctrlKey;
                }
                if (shiftPressed || altPressed || ctrlPressed)
                    alert("Funcion no permitida");
            }
            return true;
        }
        if (parseInt(navigator.appVersion) > 3) {
            document.onmousedown = mouseDown;
            if (navigator.appName == "Netscape") document.captureEvents(Event.MOUSEDOWN);
        }
        var message = "";

        function clickIE() {
            if (document.all) {
                (message);
                return false;
            }
        }

        function clickNS(e) {
            if (document.layers || (document.getElementById && !document.all)) {
                if (e.which == 2 || e.which == 3) {
                    (message);
                    return false;
                }
            }
        }
        if (document.layers) {
            document.captureEvents(Event.MOUSEDOWN);
            document.onmousedown = clickNS;
        } else {
            document.onmouseup = clickNS;
            document.oncontextmenu = clickIE;
        }
        document.oncontextmenu = new Function("return false");
        var isIEx = (navigator.appVersion.indexOf("MSIE") != -1) ? true : false;

        function alertSize() {
            var myHeight = 0;
            if (typeof (window.innerWidth) == 'number') {
                myHeight = window.innerHeight;
            } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
                myHeight = document.documentElement.clientHeight;
            } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
                myHeight = document.body.clientHeight;
            }
            return myHeight;
        }

    </script>


    <script src="./Scripts/dev/pages/login/login-1.js" type="text/javascript"></script>
    <!--end::Page Scripts -->

    <link href="./Stylesheets/vendors/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/tether.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/bootstrap-datepicker3.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/daterangepicker.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/bootstrap-select.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/bootstrap-switch.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/select2.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/nouislider.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/owl.carousel.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/owl.theme.default.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/dropzone.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/summernote.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/animate.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/toastr.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/morris.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/sweetalert2.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/socicon.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/line-awesome.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/flaticon2.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/all.min.css" rel="stylesheet" type="text/css" />

    <link href="./Stylesheets/dev/pages/login-1.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/dev/style.bundle.css" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="./media/logos/favicon.ico" />
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body style="background-position: center top; background-size: 100% 350px;" class="kt-page--loading-enabled kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent">

    <!-- begin:: Page -->
    <div class="kt-grid kt-grid--ver kt-grid--root kt-page">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
                <!--begin::Aside-->
                <div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url(./media//bg/bg-4.jpg);">
                    <div class="kt-grid__item">
                        <%--20200727 --%>
                        <%--<a href="#" class="kt-login__logo">--%>
                            <img src="<%=Page.ResolveUrl("~/media/logos/logoSegas.svg")%>" height="100px">
                        <%--</a>--%>
                    </div>
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
                        <div class="kt-grid__item kt-grid__item--middle">
                            <h3 class="kt-login__title">Sistema Electrónico de Gas</h3>
                            <h4 class="kt-login__subtitle">Somos el gestor del mercado de Gas natural en Colombia</h4>
                        </div>
                    </div>
                    <div class="kt-grid__item">
                        <div class="kt-login__info">
                            <div class="kt-login__copyright">
                                © 2020 SEGAS
                            </div>
                            <div class="kt-login__menu">
                                <a href="http://www.bmcbec.com.co/t%c3%a9rminos-y-condiciones/" class="kt-link" target="_blank">Términos y condiciones</a>  <%--20200727 --%>
                                <a href="#" class="kt-link">Contacto</a> <%--20201020--%>
                            </div>
                        </div>
                    </div>
                </div>
                <!--begin::Aside-->

                <!--begin::Content-->
                <div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">

                    <!--begin::Body-->
                    <div class="kt-login__body">

                        <!--begin::Signin-->
                        <div class="kt-login__form">
                            <div class="kt-login__title">
                                <h3>Iniciar Sesión</h3>
                            </div>

                            <!--begin::Form-->
                            <form class="kt-form" runat="server">

                                <asp:ScriptManager ID="ScriptManager1" runat="server" />

                                <div class="form-group">
                                    <asp:Label ID="lblMensaje" Style="display: inline-block;" CssClass="invalid-feedback" runat="server"></asp:Label>
                                </div>
                                <div class="form-group">
                                    <input id="txtUsuario" class="form-control" type="text" placeholder="Usuario" name="email" autocomplete="off" runat="server" validationgroup="login" />
                                    <asp:RequiredFieldValidator ID="rvUsuario" runat="server" ControlToValidate="txtUsuario" CssClass="invalid-feedback" Display="Dynamic" ValidationGroup="login" ErrorMessage="Debe ingresar el usuario" />
                                </div>
                                <div class="form-group">
                                    <input id="txtClave" class="form-control" type="password" placeholder="Contraseña" name="password" runat="server" validationgroup="login" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtClave" CssClass="invalid-feedback" Display="Dynamic" ValidationGroup="login" ErrorMessage="Debe ingresar la contraseña" />
                                </div>
                                <br />
                                <div class="form-group">
                                    <div id="dvCaptcha" style="text-align: -webkit-center"></div>
                                    <asp:TextBox ID="txtCaptcha" runat="server" Style="display: none" ValidationGroup="login" />
                                    <asp:RequiredFieldValidator ID="rfvCaptcha" CssClass="invalid-feedback" ErrorMessage="Requiere validaci&oacute;n del captcha." ControlToValidate="txtCaptcha" runat="server" Display="Dynamic" ValidationGroup="login" />
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="lblCaptcha" Text=""></asp:Label>
                                </div>
                                <br />
                                <!--begin::Action-->

                                <div class="kt-login__actions">
                                    <asp:LinkButton ID="lkbCambio" runat="server" CssClass="kt-link kt-login__link-forgot" OnClick="lkbCambio_Click">Cambiar Contraseña</asp:LinkButton>
                                    /
                                    <asp:LinkButton ID="lkbOlvido" runat="server" CssClass="kt-link kt-login__link-forgot" OnClick="lkbOlvido_Click" Text="¿Olvidó su contraseña?" ValidationGroup="recuperarLogin" />
                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="lkbOlvido" ConfirmText="Esta Seguro de restaurar la contraseña?" />
                                    <asp:Button ID="imbAceptar" runat="server" OnClientClick="validar()" OnClick="imbAceptar_Click" class="btn btn-primary btn-elevate kt-login__btn-primary" Text="Ingresar" ValidationGroup="login" CausesValidation="true" />
                                </div>
                                <!--end::Action-->
                            </form>
                            <script src="https://www.google.com/recaptcha/api.js?hl=es&onload=onloadCallback&render=explicit" async defer></script>
                            <!--end::Form-->
                        </div>
                        <!--end::Signin-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Content-->
            </div>
        </div>
    </div>

    <!-- end:: Page -->

    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = { "colors": { "state": { "brand": "#366cf3", "light": "#ffffff", "dark": "#282a3c", "primary": "#5867dd", "success": "#34bfa3", "info": "#36a3f7", "warning": "#ffb822", "danger": "#fd3995" }, "base": { "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"], "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"] } } };
    </script>
    <!-- end::Global Config -->

    <!--begin::Global Theme Bundle(used by all pages) -->
    <script type="text/javascript" src='<%=Page.ResolveUrl("~/Scripts/vendor/vendors.bundle.js")%>'></script>
    <script type="text/javascript" src='<%=Page.ResolveUrl("~/Scripts/dev/scripts.bundle.js")%>'></script>
    <!--end::Global Theme Bundle -->

</body>

<!-- end::Body -->

</html>
