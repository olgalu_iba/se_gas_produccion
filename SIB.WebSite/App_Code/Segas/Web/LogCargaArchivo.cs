﻿/*
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 * Copyright (C) 2019 Bolsa Mercantil de Colombia, Todos los Derechos Reservados
 * Archivo:               LogCargaArchivo.cs
 * Tipo:                  Clase principal
 * Autor:                 Oscar Piñeros
 * Fecha creación:        2019 Octubre 10
 * Fecha modificación:    
 * Propósito:             Clase que permite administrar el log de cargue de archivos
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 */

using System;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.UI;
using Segas.Web.Elements;

namespace Segas.Web
{
    /// <summary>
    /// Descripción breve de LogCargaArchivo
    /// </summary>
    public static class LogCargaArchivo
    {
        /// <summary>
        /// Genera un log estándar
        /// </summary>
        public static void DescargarLog(Page page, string log)
        {
            log = log.Replace("\'", "");
            string filename = $"{Guid.NewGuid()}_log.txt";
            string openModalFunction = $"descargaArchivo.txt('{filename}', '{log}');";
            ScriptManager.RegisterStartupScript(page, page.GetType(), Guid.NewGuid().ToString(), openModalFunction, true);
        }

        /// <summary>
        /// Genera un log de errores
        /// </summary>
        public static void DownloadBugsLog(Page page, StringBuilder stringLog)
        {
            if (string.IsNullOrEmpty(stringLog.ToString()) || stringLog.ToString().Trim().Equals("<br>")) return;
            stringLog.Insert(0, HttpContext.GetGlobalResourceObject("AppResources", "CabeceraLogErrores", CultureInfo.CurrentCulture)?.ToString());
            //Se descarga el log para el usuario
            DescargarLog(page, stringLog.ToString());
            Toastr.Error(page, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCargaInfOpe", CultureInfo.CurrentCulture)?.ToString());
        }
    }
}