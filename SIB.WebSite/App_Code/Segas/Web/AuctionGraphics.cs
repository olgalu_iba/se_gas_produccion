﻿/*
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 * Copyright (C) 2019 Bolsa Mercantil de Colombia, Todos los Derechos Reservados
 * Archivo:               AuctionGraphics.cs
 * Tipo:                  Clase principal
 * Autor:                 Oscar Piñeros
 * Fecha creación:        2019 Septiembre 09
 * Fecha modificación:    
 * Propósito:             Clase que permite administrar las gráficas de Demanda Agregada
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 */

using System;
using System.Data;
using System.Globalization;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Segas.Web
{

    /// <summary>
    /// Summary description for AuctionGraphics
    /// </summary>
    public static class AuctionGraphics
    {
        /// <summary>
        /// Genera la gráfica de Curva de Oferta y Demanda Agregada
        /// </summary>
        /// <param name="page"></param>
        /// <param name="dtgInformacion"></param>
        /// <param name="lblNoId"></param>
        /// <param name="transporte"></paFFram>
        public static void GenerateGraph(Control page, DataGrid dtgInformacion, string lblNoId, bool transporte = false)
        {
            var builder = new StringBuilder();

            var dt = new DataTable();
            dt.Columns.Add("ID");
            dt.Columns.Add("PRECIO");
            dt.Columns.Add("CANTIDAD_DE_VENTA");
            dt.Columns.Add("CANTIDAD_DE_COMPRA");

            var contador = 0;
            //Se agrega el punto inicial    
            DataGridItem grilla = dtgInformacion.Items[0];
            DataRow da = dt.NewRow();
            contador++;
            da[0] = contador;
            da[2] = Math.Round(decimal.Parse(grilla.Cells[2].Text), 2); //cantidad de compra
            da[1] = 0; //precio
            da[3] = 0; //cantidad de venta

            dt.Rows.Add(da);

            da = dt.NewRow();
            contador++;
            da[0] = contador;
            da[2] = Math.Round(decimal.Parse(grilla.Cells[2].Text), 2); //cantidad de compra
            da[1] = Math.Round(decimal.Parse(grilla.Cells[0].Text), 2); // precio
            da[3] = 0; //cantidad de venta

            dt.Rows.Add(da);
            var grafCompra = "S"; //20210224

            for (var index = 1; index < dtgInformacion.Items.Count; index++)
            {
                DataGridItem grilla2 = dtgInformacion.Items[index];

                if (index - 1 < 0) continue;
                DataGridItem grilla3 = dtgInformacion.Items[index - 1];

                DataRow da2 = dt.NewRow();
                contador++;
                da2[0] = contador;
                da2[2] = Math.Round(decimal.Parse(grilla2.Cells[2].Text), 2); //cantidad de compra
                da2[1] = Math.Round(decimal.Parse(grilla3.Cells[0].Text), 2);  //precio
                da2[3] = Math.Round(decimal.Parse(grilla3.Cells[1].Text), 2);  //cantidad de venta
                dt.Rows.Add(da2);

                DataRow da3 = dt.NewRow();
                contador++;
                da3[0] = contador;
                da3[2] = Math.Round(decimal.Parse(grilla2.Cells[2].Text), 2); //cantidad de compra
                da3[1] = Math.Round(decimal.Parse(grilla2.Cells[0].Text), 2);  //precio            
                da3[3] = Math.Round(decimal.Parse(grilla3.Cells[1].Text), 2);  //cantidad de venta
                dt.Rows.Add(da3);

                grafCompra = grilla2.Cells[9].Text; //20210224
            }

            //Se agrega el punto final
            DataGridItem grilla4 = dtgInformacion.Items[dtgInformacion.Items.Count - 1];
            DataRow da4 = dt.NewRow();
            contador++;
            da4[0] = contador;
            da4[2] = 0; //cantidad de compra
            da4[1] = Math.Round(decimal.Parse(grilla4.Cells[0].Text), 2); //precio
            da4[3] = Math.Round(decimal.Parse(grilla4.Cells[1].Text), 2); //cantidad de venta
            dt.Rows.Add(da4);

            builder.Append("[['Precio','Demanda', 'Puntos', 'Oferta']");
            for (var i = 0; i < dt.Rows.Count; i++)
            {
                //cantidad de venta
                var venta = dt.Rows[i]["CANTIDAD_DE_VENTA"].ToString();
                //cantidad de compra
                var compra = dt.Rows[i]["CANTIDAD_DE_COMPRA"].ToString();
                //precio
                var precio = dt.Rows[i]["PRECIO"].ToString();

                //puntos
                var punto = string.Empty;
                if (i + 1 < dt.Rows.Count && dt.Rows[i + 1]["PRECIO"].ToString().Equals(dt.Rows[i]["PRECIO"].ToString()))
                    punto = venta;

                var punto2 = string.Empty;
                if (i - 2 >= 0 && !dt.Rows[i - 2]["CANTIDAD_DE_COMPRA"].ToString().Equals(dt.Rows[i]["CANTIDAD_DE_COMPRA"].ToString()))
                    punto2 = compra;

                //Arreglo de datos para pintar en la grafica de Google
                var res = punto.Equals(string.Empty) ? punto2 : punto;

                precio = precio.Replace(',', '.');
                venta = venta.Replace(',', '.');
                res = res.Replace(',', '.');
                compra = compra.Replace(',', '.');

                if (grafCompra == "S") //2021024
                {
                    if (i % 2 == 1 && i + 1 < dt.Rows.Count && venta.Equals(dt.Rows[i + 1]["CANTIDAD_DE_VENTA"].ToString()))
                        builder.Append($", [{precio}, {venta}, , {compra}]");
                    else
                        builder.Append($", [{precio}, {venta}, {res}, {compra}]");
                }
                else //20210024
                {
                    if (i % 2 == 1 && i + 1 < dt.Rows.Count && venta.Equals(dt.Rows[i + 1]["CANTIDAD_DE_VENTA"].ToString()))
                        builder.Append($", [{precio}, {compra}, , {compra}]");
                    else
                        builder.Append($", [{precio}, {compra}, {res}, {compra}]");
                }
            }
            builder.Append("]");
            //builder.Clear();
            //builder.Append("[['Precio','Demanda', 'Puntos', 'Oferta'], [0,  ,0 ,0 ], [0.00,  , 0,0], [0.00,, 5000, 5000], [0.51, 0,5000 ,5000 ], [0.51, , 10000, 10000]]");
            ///builder.Append("[['Precio','Demanda', 'Puntos', 'Oferta'], [0,  , ,0 ], [0.00,  , ,0], [0.00,, 5000, 5000], [0.51,0 , ,5000 ], [0.51, , 10000, 10000]]");
            //builder.Append("[['Precio','Demanda', 'Puntos', 'Oferta'], [0,  , ,0 ], [0.00,  , ,0], [0.00,, 5000, 5000], [0.51,5000 , ,5000 ], [0.51, , 10000, 10000], [0.7, 10000, , 10000], [0.7, , 12000, 12000]]");

            var str = $"drawChartRefresh({builder},'{transporte}', '{null}', '{null}');";
            ScriptManager.RegisterStartupScript(page, page.GetType(), Guid.NewGuid().ToString(), str, true);
        }


        /// <summary>
        /// Genera la gráfica de Curvas de demanda
        /// </summary>
        public static void GenerateGraphDemand(Control page, DataGrid dtgInformacion, string lblNoId, bool transporte = false)
        {
            var builder = new StringBuilder();

            var dt = new DataTable();
            dt.Columns.Add("ID");
            dt.Columns.Add("PRECIO");
            dt.Columns.Add("PUNTO");
            dt.Columns.Add("CANTIDAD_DE_COMPRA");

            var contador = 0;

            //20221222 Se agrega el punto inicial    
            DataGridItem grilla = dtgInformacion.Items[0];
            DataRow da = dt.NewRow();
            contador++;
            da[0] = contador;
            da[3] = Math.Round(decimal.Parse(grilla.Cells[1].Text), 2); //cantidad de compra
            da[1] = 0; //precio
            da[2] = null; //cantidad de venta

            dt.Rows.Add(da);

            da = dt.NewRow();
            contador++;
            da[0] = contador;
            da[3] = Math.Round(decimal.Parse(grilla.Cells[1].Text), 2); //cantidad de compra
            da[1] = Math.Round(decimal.Parse(grilla.Cells[0].Text), 2); // precio
            da[2] = 0; //cantidad de venta

            dt.Rows.Add(da);
            var grafCompra = "S"; //20210224
            //20221222 fin Se agrega el punto inicial    

            decimal inicioCoordenadaX = 0;
            decimal inicioCoordenadaY = 0;
            for (var index = 1; index < dtgInformacion.Items.Count; index++)
            {
                DataGridItem grilla2 = dtgInformacion.Items[index - 1];
                DataGridItem grilla3 = dtgInformacion.Items[index];

                DataRow da2 = dt.NewRow();
                contador++;
                da2[0] = contador;
                da2[1] = Math.Round(Convert.ToDouble(grilla2.Cells[0].Text), 2);  //precio
                da2[2] = Math.Round(Convert.ToDouble(grilla2.Cells[1].Text), 2);  //punto
                da2[3] = Math.Round(Convert.ToDouble(grilla2.Cells[1].Text), 2);  //cantidad de compra
                if (contador == 1)
                    inicioCoordenadaY = Math.Round(decimal.Parse(grilla2.Cells[0].Text), 2);
                dt.Rows.Add(da2);

                DataRow da3 = dt.NewRow();
                contador++;
                da3[0] = contador;
                da3[1] = Math.Round(Convert.ToDouble(grilla2.Cells[0].Text), 2);  //precio          
                da3[2] = string.Empty;
                da3[3] = Math.Round(Convert.ToDouble(grilla3.Cells[1].Text), 2);  //cantidad de compra
                dt.Rows.Add(da3);

                // Si es el ultimo registro se agregan las dos ultimas filas
                if (index != dtgInformacion.Items.Count - 1) continue;
                var cantidadCompra = Math.Round(Convert.ToDouble(grilla3.Cells[1].Text), 2);

                DataRow da4 = dt.NewRow();
                contador++;
                da4[0] = contador;
                da4[1] = Math.Round(Convert.ToDouble(grilla3.Cells[0].Text), 2);  //precio
                da4[2] = cantidadCompra;  //punto
                da4[3] = cantidadCompra;  //cantidad de compra
                dt.Rows.Add(da4);

                DataRow da5 = dt.NewRow();
                contador++;
                da5[0] = contador;
                da5[1] = Math.Round(Convert.ToDouble(grilla3.Cells[0].Text), 2);  //precio
                da5[2] = string.Empty;  //punto
                var ctCompra = Math.Round(cantidadCompra - cantidadCompra * 0.10 > 0 ? cantidadCompra - cantidadCompra * 0.10 : 0, 2); //cantidad de compra
                da5[3] = decimal.Parse(ctCompra.ToString(CultureInfo.InvariantCulture));
                inicioCoordenadaX = decimal.Parse(ctCompra.ToString(CultureInfo.InvariantCulture));
                dt.Rows.Add(da5);
            }

            //Se agrega el punto final
            DataGridItem grilla4 = dtgInformacion.Items[dtgInformacion.Items.Count - 1];
            DataRow da6 = dt.NewRow();
            contador++;
            da6[0] = contador;
            da6[2] = Math.Round(decimal.Parse(grilla4.Cells[1].Text), 2); //precio
            da6[1] = Math.Round(decimal.Parse(grilla4.Cells[0].Text), 2); //precio
            da6[3] = 0;
            dt.Rows.Add(da6);

            builder.Append("[['Precio', 'Puntos', 'Demanda']");
            for (var i = 0; i < dt.Rows.Count; i++)
            {
                //cantidad de compra
                var compra = dt.Rows[i]["CANTIDAD_DE_COMPRA"].ToString();
                //punto
                var punto = dt.Rows[i]["PUNTO"].ToString();
                //precio
                var precio = dt.Rows[i]["PRECIO"].ToString();

                precio = precio.Replace(',', '.');
                punto = punto.Replace(',', '.');
                compra = compra.Replace(',', '.');
                if (i == dt.Rows.Count - 2)
                    builder.Append($", [{precio}, , {compra}]");
                else
                    builder.Append($", [{precio}, {punto}, {compra}]");
            }
            builder.Append("]");
            var str = $"drawChartRefresh({builder},'{transporte}','{inicioCoordenadaX}','{inicioCoordenadaY}')";
            ScriptManager.RegisterStartupScript(page, page.GetType(), Guid.NewGuid().ToString(), str, true);
        }
    }
}