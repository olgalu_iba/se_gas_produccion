﻿/*
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 * Copyright (C) 2019 Bolsa Mercantil de Colombia, Todos los Derechos Reservados
 * Archivo:               Accordion.cs
 * Tipo:                  Clase principal
 * Autor:                 Oscar Piñeros
 * Fecha creación:        2019 Agosto 12
 * Fecha modificación:    
 * Propósito:             Clase que permite administrar el acordeón de Bootstrap
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 */

using System;
using System.Web.UI;

namespace Segas.Web.Elements
{
    /// <summary>
    /// Descripción breve de Accordion
    /// </summary>
    public class Accordion
    {
        /// <summary>
        /// Selecciona una carta del acordeón y deshabilita las deseadas  
        /// </summary>
        /// <param name="page"></param>
        /// <param name="id"></param>
        /// <param name="idDisable"></param>
        /// <param name="removeCollapse"></param>
        public static void Seleccionar(Control page, string id, string idDisable, string removeCollapse)
        {
            string cardSelect = $"accordion.select('#{id}', {idDisable}, {removeCollapse});";
            ScriptManager.RegisterStartupScript(page, page.GetType(), Guid.NewGuid().ToString(), cardSelect, true);
        }
    }
}