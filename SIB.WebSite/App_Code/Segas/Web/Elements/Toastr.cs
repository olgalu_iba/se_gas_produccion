﻿/*
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 * Copyright (C) 2019 Bolsa Mercantil de Colombia, Todos los Derechos Reservados
 * Archivo:               Toastr.cs
 * Tipo:                  Clase principal
 * Autor:                 Oscar Piñeros
 * Fecha creación:        2019 Julio 26
 * Fecha modificación:    
 * Propósito:             Clase que permite administrar las notificaciones por medio del Toastr.js notifications
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 */

using System;
using System.Web.UI;

namespace Segas.Web.Elements
{
    /// <summary>
    /// Descripción breve de Toastr
    /// </summary>
    public class Toastr
    {
        /// <summary>
        /// Muestra el mensaje de información en el Toastr
        /// </summary>
        /// <param name="page"></param>
        /// <param name="mesage"></param>
        /// <param name="title"></param>
        /// <param name="timeOut"></param>
        public static void Info(Control page, string mesage, string title = "Información", int? timeOut = 10000)
        {
            var infoFunction = $"segasToastr.info('{RemplaceSequence(mesage)}', '{title}', '{timeOut}');";
            ScriptManager.RegisterStartupScript(page, page.GetType(), Guid.NewGuid().ToString(), infoFunction, true);
        }

        /// <summary>
        /// Muestra el mensaje de éxito en el Toastr
        /// </summary>
        /// <param name="page"></param>
        /// <param name="mesage"></param>
        /// <param name="title"></param>
        /// <param name="timeOut"></param>
        public static void Success(Control page, string mesage, string title = "Éxito!", int? timeOut = 10000)
        {
            var successFunction = $"segasToastr.sucess('{RemplaceSequence(mesage)}', '{title}', '{timeOut}');";
            ScriptManager.RegisterStartupScript(page, page.GetType(), Guid.NewGuid().ToString(), successFunction, true);
        }

        /// <summary>
        /// Muestra el mensaje de advertencia en el Toastr
        /// </summary>
        /// <param name="page"></param>
        /// <param name="mesage"></param>
        /// <param name="title"></param>
        /// <param name="timeOut"></param>
        public static void Warning(Control page, string mesage, string title = "Advertencia", int? timeOut = 10000)
        {
            string warningFunction = $"segasToastr.warning('{RemplaceSequence(mesage)}', '{title}', '{timeOut}');";
            ScriptManager.RegisterStartupScript(page, page.GetType(), Guid.NewGuid().ToString(), warningFunction, true);
        }

        /// <summary>
        /// Muestra el mensaje de error en el Toastr
        /// </summary>
        /// <param name="page"></param>
        /// <param name="mesage"></param>
        /// <param name="title"></param>
        /// <param name="timeOut"></param>
        public static void Error(Control page, string mesage, string title = "Error!", int? timeOut = 10000)
        {
            var errorFunction = $"segasToastr.error('{RemplaceSequence(mesage)}', '{title}', '{timeOut}');";
            ScriptManager.RegisterStartupScript(page, page.GetType(), Guid.NewGuid().ToString(), errorFunction, true);
        }

        /// <summary>
        /// Selecciona el tipo de notificación 
        /// </summary>
        /// <param name="mesage"></param>
        /// <param name="title"></param>
        /// <param name="typeToastr"></param>
        /// <param name="page"></param>
        public static void SelectToastr(Control page, string mesage, string title, EnumTypeToastr typeToastr)
        {
            switch (typeToastr)
            {
                case EnumTypeToastr.Info:
                    Info(page, RemplaceSequence(mesage), string.IsNullOrEmpty(title) ? "Información" : title, 15000);
                    break;
                case EnumTypeToastr.Success:
                    Success(page, RemplaceSequence(mesage), string.IsNullOrEmpty(title) ? "Éxito!" : title, 15000);
                    break;
                case EnumTypeToastr.Warning:
                    Warning(page, RemplaceSequence(mesage), string.IsNullOrEmpty(title) ? "Advertencia" : title, 15000);
                    break;
                case EnumTypeToastr.Error:
                    Error(page, RemplaceSequence(mesage), string.IsNullOrEmpty(title) ? "Error!" : title, 15000);
                    break;
            }
        }

        /// <summary>
        /// Remplaza la secuencia de caracteres en una cadena de caracteres 
        /// </summary>
        /// <param name="mesage"></param>
        private static string RemplaceSequence(string mesage)
        {
            return mesage.Replace("\n", "<br>").Replace("\r", "").Replace("'", "");
        }
    }
}