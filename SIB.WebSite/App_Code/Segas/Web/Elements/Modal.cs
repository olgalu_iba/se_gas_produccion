﻿/*
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 * Copyright (C) 2019 Bolsa Mercantil de Colombia, Todos los Derechos Reservados
 * Archivo:               Toastr.cs
 * Tipo:                  Clase principal
 * Autor:                 Oscar Piñeros
 * Fecha creación:        2019 Julio 23
 * Fecha modificación:    
 * Propósito:             Clase que permite administrar los modals de Bootstrap
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 */

using System;
using System.Web.UI;

/// <summary>
/// Summary description for Modal
/// </summary>
// ReSharper disable once CheckNamespace
public class Modal
{
    /// <summary>
    /// Tamaños del Modal 
    /// </summary>
    public enum Size { Normal, Large }

    /// <summary>
    /// Abre el modal
    /// </summary>
    /// <param name="page"></param>
    /// <param name="id"></param>
    /// <param name="insideId"></param>
    /// <param name="size"></param>
    public static void Abrir(Control page, string id, string insideId, Size size = Size.Large)
    {
        var openModalFunction = $"modal.open('#{id}', '#{insideId}', '{size}');";
        ScriptManager.RegisterStartupScript(page, page.GetType(), Guid.NewGuid().ToString(), openModalFunction, true);
    }

    /// <summary>
    /// Cierra el modal 
    /// </summary>
    /// <param name="page"></param>
    /// <param name="id"></param>
    public static void Cerrar(Control page, string id)
    {
        var closeModalFunction = $"modal.close('#{id}');";
        ScriptManager.RegisterStartupScript(page, page.GetType(), Guid.NewGuid().ToString(), closeModalFunction, true);
    }

    /// <summary>
    /// Selecciona la opción deseada para que realice el modal (Abrir, Cerrar)
    /// </summary>
    /// <param name="page"></param>
    /// <param name="id"></param>
    /// <param name="insideId"></param>
    /// <param name="typeModal"></param>
    /// <param name="size"></param>
    public static void SelectModal(Control page, string id, string insideId, EnumTypeModal typeModal, Size size = Size.Large)
    {
        switch (typeModal)
        {
            case EnumTypeModal.Abrir:
                Abrir(page, id, insideId, size);
                break;
            case EnumTypeModal.Cerrar:
                Cerrar(page, id);
                break;
        }
    }
}