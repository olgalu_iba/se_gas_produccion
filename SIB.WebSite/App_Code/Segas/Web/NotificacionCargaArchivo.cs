﻿/*
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 * Copyright (C) 2019 Bolsa Mercantil de Colombia, Todos los Derechos Reservados
 * Archivo:               NotificacionCargaArchivo.cs
 * Tipo:                  Clase principal
 * Autor:                 Oscar Piñeros
 * Fecha creación:        2020 Enero 13
 * Fecha modificación:    
 * Propósito:             Clase que notifica si en la carga de un archivo ha aciertos y los notifica
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 */

using System;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.UI;
using Segas.Web.Elements;

namespace Segas.Web
{
    /// <summary>
    /// Summary description for NotificacionCargaArchivo
    /// </summary>
    public static class NotificacionCargaArchivo
    {
        /// <summary>
        /// Notifica si el cargue del archivo fue exitoso. Si el cargue tiene un éxito parcial y tiene errores, se notifica el éxito parcial solamente y se dejan los errores para que sean manejados
        /// </summary>
        /// <param name="page"></param>
        /// <param name="stringLog"></param>
        public static void NotificacionCargueExitoso(Page page, StringBuilder stringLog)
        {
            if (string.IsNullOrEmpty(stringLog.ToString()) || !stringLog.ToString().Contains(HttpContext.GetGlobalResourceObject("AppResources", "CreacionCorrecta", CultureInfo.CurrentCulture)?.ToString())) return;
            string[] delim = { Environment.NewLine, "\n" }; // "\n" added in case you manually appended a newline
            string[] lines = stringLog.ToString().Split(delim, StringSplitOptions.None);
            stringLog.Clear();
            foreach (string line in lines)
            {
                if (!line.Contains(HttpContext.GetGlobalResourceObject("AppResources", "CreacionCorrecta", CultureInfo.CurrentCulture)?.ToString()))
                    stringLog.Append(line);
                else
                    Toastr.Success(page, line);
            }
        }
    }
}