﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using SIB.BASE.Negocio;
using System.Text.RegularExpressions;
using iTextSharp.text;
using iTextSharp.text.pdf;

/// <summary>
/// Clase que Genera el PDF de al Carta de Garantias
/// </summary>
public class clCartaGarantias
{
    public clCartaGarantias()  //rq122 -- creado par carta de garantias
    {
        //
        // TODO: Add constructor logic here
        //
    }
    /// <summary>
    /// Metodo que Genera el archivo PDF de la Carta de Garantias
    /// </summary>
    /// <param name="lsNitOperador">Nit Operador</param>
    /// <param name="lsInfo">Clase de la Session</param>
    /// <returns>Ruta de almacenamiento del archivo</returns>
    public string generarCarta(string lsNitOperador, InfoSessionVO lsInfo)
    {
        System.Web.SessionState.HttpSessionState session = HttpContext.Current.Session;
        clConexion lConexion = new clConexion(lsInfo);
        SqlDataReader lLector;
        SqlCommand lComando;
        // Definicion de las variables
        string oRutaCont = ConfigurationManager.AppSettings["RutaPDF"].ToString();
        //string oRutaFont = ConfigurationManager.AppSettings["RutaFont"].ToString();
        string oRutaImg = ConfigurationManager.AppSettings["RutaIMG"].ToString();
        string lsArchivoFirmaCarta = "";
        lConexion.Abrir();
        lComando = new SqlCommand();
        // Obtengo los Parametros Generales de las Cartas
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_ValidarExistencia";
        lComando.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = "m_parametros_generales";
        lComando.Parameters.Add("@P_filtro", SqlDbType.VarChar).Value = "1=1 ";
        lLector = lComando.ExecuteReader();
        if (lLector.HasRows)
        {
            while (lLector.Read())
            {
                lsArchivoFirmaCarta = lLector["archivo_firma"].ToString();
            }
        }
        lLector.Close();
        lLector.Dispose();
        lComando.Parameters.Clear();
        string lsdirPDF = "";
        string lsRutaArchivo = "";
        string lsArchivo = "";
        decimal ldValor = 0;
        // Obtengo el COntenido de la Carta
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_GetCartaGar";
        lComando.Parameters.Add("@P_no_documento", SqlDbType.VarChar).Value = lsNitOperador;
        lLector = lComando.ExecuteReader();
        if (lLector.HasRows)
        {
            while (lLector.Read())
            {
                // Se crea el Documento PDF
                Document loDocument = new Document(PageSize.LETTER, 70, 70, 20, 65);
                try
                {
                    iTextSharp.text.Image LogoFirma = iTextSharp.text.Image.GetInstance(oRutaImg + lsArchivoFirmaCarta);
                    LogoFirma.ScalePercent(50);
                    LogoFirma.Alignment = Element.ALIGN_CENTER;

                    iTextSharp.text.Image LogoCarta = iTextSharp.text.Image.GetInstance(oRutaImg + "imagen cartas.png");
                    LogoCarta.ScalePercent(60);
                    LogoCarta.Alignment = Element.ALIGN_CENTER;

                    string lsFecha = DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();

                    /// Defino las Tablas a Manejar en la Generación de Documento
                    int[] liAncho = { 35, 65 };
                    int[] liAncho1 = { 50, 25, 25 };
                    // Definicion de las Variables locales usadas
                    // Creacíón de los FONT que se van a manejar en el PDF
                    //BaseFont bArial = BaseFont.CreateFont(oRutaFont + "arial.ttf", BaseFont.WINANSI, true);
                    //BaseFont bArialBold = BaseFont.CreateFont(oRutaFont + "arialbd.ttf", BaseFont.WINANSI, true);

                    iTextSharp.text.Font FuenteNormal;
                    iTextSharp.text.Font FuenteNormal1;
                    iTextSharp.text.Font FuenteNegrita;

                    FuenteNormal = FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.NORMAL);
                    FuenteNormal1 = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                    FuenteNegrita = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                    // Se definen los tipos de letra a manejar en el PDF
                    //Font FuenteNegrita = new Font(bArialBold);
                    //FuenteNegrita.Size = 10;
                    //Font FuenteNormal = new Font(bArial);
                    //FuenteNormal.Size = 10;
                    //Font FuenteNormal1 = new Font(bArial);
                    //FuenteNormal1.Size = 8;

                    // Defino las Tablas a manejar en el Documento PDF
                    PdfPTable TablaRegistros = new PdfPTable(2);
                    TablaRegistros.SetWidths(liAncho);
                    TablaRegistros.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaRegistros.WidthPercentage = 100;

                    PdfPTable TablaRegistros1 = new PdfPTable(3);
                    TablaRegistros1.SetWidths(liAncho1);
                    TablaRegistros1.HorizontalAlignment = Element.ALIGN_CENTER;
                    TablaRegistros1.WidthPercentage = 95;

                    // Obtengo el Nombre del Archivo y la ruta del mismo en el servidor
                    lsArchivo = "CARTA GARANTIAS - " + lsNitOperador + " - " + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + "-" + DateTime.Now.Millisecond.ToString() + ".pdf";
                    // Obtengo la Ruta de los PDF
                    lsdirPDF = oRutaCont + "\\" + lsArchivo;
                    // Creo la Instancia del Documento PDF en el Servidor
                    PdfWriter pw = PdfWriter.GetInstance(loDocument, new FileStream(lsdirPDF, FileMode.Create));
                    //Crear parametros de pdf 

                    //pw.PdfVersion = '1';
                    PdfDictionary parameters = new PdfDictionary(PdfName.OUTPUTINTENT);
                    parameters.Put(PdfName.MODDATE, new PdfDate());
                    parameters.Put(PdfName.OUTPUTCONDITIONIDENTIFIER, new PdfString("sRGB IEC61966-2.1"));
                    parameters.Put(PdfName.INFO, new PdfString("sRGB IEC61966-2.1"));
                    parameters.Put(PdfName.S, PdfName.GTS_PDFXVERSION);
                    pw.ExtraCatalog.Put(PdfName.OUTPUTINTENTS, new PdfArray(parameters));
                    pw.CreateXmpMetadata();
                    /// Abro el Documento PDF
                    lsdirPDF = lsRutaArchivo;

                    loDocument.Open();
                    //crear profile (ICC - International Color Consorsium) 
                    //ICC_Profile icc = ICC_Profile.GetInstance(oRutaFont + "sRGB_Color_Space_Profile.icm");
                    //pw.SetOutputIntents("Custom", "", "http://www.color.org", "sRGB IEC61966-2.1", icc);

                    // Defino el Parrafo para los Espacios
                    Paragraph lpEspacios = new Paragraph(5, " ", FuenteNormal1);

                    Paragraph lpRadica = new Paragraph(12, lLector["no_radicado"].ToString(), FuenteNormal);
                    lpRadica.Alignment = Element.ALIGN_RIGHT;

                    // Creo los Parrafos a Incluir en la Carta
                    // Fecha de la Carta
                    Paragraph lpTitulo = new Paragraph(12, lLector["fecha_carta"].ToString(), FuenteNormal);
                    lpTitulo.Alignment = Element.ALIGN_LEFT;

                    Paragraph lpTitulo1 = new Paragraph(12, "Señor", FuenteNormal);
                    lpTitulo1.Alignment = Element.ALIGN_LEFT;

                    Paragraph lpTitulo1a = new Paragraph(12, lLector["representante_legal"].ToString(), FuenteNegrita);
                    lpTitulo1a.Alignment = Element.ALIGN_LEFT;

                    Paragraph lpTitulo1b = new Paragraph(12, "Representante Legal", FuenteNormal);
                    lpTitulo1b.Alignment = Element.ALIGN_LEFT;

                    Paragraph lpTitulo1c = new Paragraph(12, lLector["nombre_operador"].ToString(), FuenteNegrita);
                    lpTitulo1c.Alignment = Element.ALIGN_LEFT;

                    Paragraph lpTitulo1d = new Paragraph(12, lLector["direccion"].ToString(), FuenteNormal);
                    lpTitulo1d.Alignment = Element.ALIGN_LEFT;

                    Paragraph lpTitulo1e = new Paragraph(12, lLector["ciudad"].ToString(), FuenteNormal);
                    lpTitulo1e.Alignment = Element.ALIGN_LEFT;

                    Paragraph lpTitulo2 = new Paragraph();
                    lpTitulo2.Alignment = Element.ALIGN_RIGHT;
                    lpTitulo2.Add(new Chunk("       Referencia: ", FuenteNegrita));
                    lpTitulo2.Add(new Chunk("Garantía de pago servicios Gestor del Mercado de Gas", FuenteNormal));

                    Paragraph lpTitulo3 = new Paragraph();
                    lpTitulo3.Alignment = Element.ALIGN_LEFT;
                    lpTitulo3.Add(new Chunk("Respetado señor " + lLector["representante_legal"].ToString(), FuenteNormal));


                    // Logo
                    PdfPCell CellLogo = new PdfPCell();
                    CellLogo.Border = 0;
                    CellLogo.BorderWidthLeft = 0;
                    CellLogo.BorderWidthTop = 0;
                    CellLogo.BorderWidthRight = 0;
                    CellLogo.BorderWidthBottom = 0;
                    CellLogo.Colspan = 2;
                    CellLogo.AddElement(LogoCarta);
                    CellLogo.AddElement(lpEspacios);
                    CellLogo.VerticalAlignment = Element.ALIGN_TOP;
                    CellLogo.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaRegistros.AddCell(CellLogo);

                    // Fecha
                    PdfPCell CellTitulo = new PdfPCell();
                    CellTitulo.Border = 0;
                    CellTitulo.BorderWidthLeft = 0;
                    CellTitulo.BorderWidthTop = 0;
                    CellTitulo.BorderWidthRight = 0;
                    CellTitulo.BorderWidthBottom = 0;
                    CellTitulo.Colspan = 2;
                    CellTitulo.AddElement(lpTitulo);
                    CellTitulo.AddElement(lpEspacios);
                    CellTitulo.VerticalAlignment = Element.ALIGN_TOP;
                    CellTitulo.HorizontalAlignment = Element.ALIGN_CENTER;
                    TablaRegistros.AddCell(CellTitulo);

                    // Radicado
                    PdfPCell CellRadica = new PdfPCell();
                    CellRadica.Border = 0;
                    CellRadica.BorderWidthLeft = 0;
                    CellRadica.BorderWidthTop = 0;
                    CellRadica.BorderWidthRight = 0;
                    CellRadica.BorderWidthBottom = 0;
                    CellRadica.Colspan = 2;
                    CellRadica.AddElement(lpRadica);
                    CellRadica.AddElement(lpEspacios);
                    CellRadica.VerticalAlignment = Element.ALIGN_TOP;
                    CellRadica.HorizontalAlignment = Element.ALIGN_RIGHT;
                    TablaRegistros.AddCell(CellRadica);

                    // Encabezado
                    PdfPCell CellTitulo1 = new PdfPCell();
                    CellTitulo1.Border = 0;
                    CellTitulo1.BorderWidthLeft = 0;
                    CellTitulo1.BorderWidthTop = 0;
                    CellTitulo1.BorderWidthRight = 0;
                    CellTitulo1.BorderWidthBottom = 0;
                    CellTitulo1.Colspan = 2;
                    CellTitulo1.AddElement(lpTitulo1);
                    CellTitulo1.AddElement(lpTitulo1a);
                    CellTitulo1.AddElement(lpTitulo1b);
                    CellTitulo1.AddElement(lpTitulo1c);
                    CellTitulo1.AddElement(lpTitulo1d);
                    CellTitulo1.AddElement(lpTitulo1e);
                    CellTitulo1.AddElement(lpEspacios);
                    CellTitulo1.AddElement(lpEspacios);
                    CellTitulo1.AddElement(lpEspacios);
                    CellTitulo1.VerticalAlignment = Element.ALIGN_TOP;
                    CellTitulo1.HorizontalAlignment = Element.ALIGN_CENTER;
                    TablaRegistros.AddCell(CellTitulo1);

                    PdfPCell CellTitulo2 = new PdfPCell();
                    CellTitulo2.Border = 0;
                    CellTitulo2.BorderWidthLeft = 0;
                    CellTitulo2.BorderWidthTop = 0;
                    CellTitulo2.BorderWidthRight = 0;
                    CellTitulo2.BorderWidthBottom = 0;
                    CellTitulo2.Colspan = 2;
                    CellTitulo2.AddElement(lpTitulo2);
                    CellTitulo2.AddElement(lpEspacios);
                    CellTitulo2.AddElement(lpEspacios);
                    CellTitulo2.AddElement(lpEspacios);
                    CellTitulo2.VerticalAlignment = Element.ALIGN_TOP;
                    CellTitulo2.HorizontalAlignment = Element.ALIGN_CENTER;
                    TablaRegistros.AddCell(CellTitulo2);

                    PdfPCell CellTitulo3 = new PdfPCell();
                    CellTitulo3.Border = 0;
                    CellTitulo3.BorderWidthLeft = 0;
                    CellTitulo3.BorderWidthTop = 0;
                    CellTitulo3.BorderWidthRight = 0;
                    CellTitulo3.BorderWidthBottom = 0;
                    CellTitulo3.Colspan = 2;
                    CellTitulo3.AddElement(lpTitulo3);
                    CellTitulo3.AddElement(lpEspacios);
                    CellTitulo3.AddElement(lpEspacios);
                    CellTitulo3.AddElement(lpEspacios);
                    CellTitulo3.VerticalAlignment = Element.ALIGN_TOP;
                    CellTitulo3.HorizontalAlignment = Element.ALIGN_CENTER;
                    TablaRegistros.AddCell(CellTitulo3);
                    loDocument.Add(TablaRegistros);


                    // Creo el Parrafo del Encabezado de la Carta
                    Paragraph lpContenido = new Paragraph();
                    lpContenido.Add(new Chunk(lLector["texto1"].ToString(), FuenteNormal));
                    lpContenido.Alignment = Element.ALIGN_JUSTIFIED;
                    loDocument.Add(lpContenido);
                    loDocument.Add(lpEspacios);
                    loDocument.Add(lpEspacios);
                    loDocument.Add(lpEspacios);

                    // Pinto el Cuadro del Detalle de la Carta
                    Paragraph lpDetalle1 = new Paragraph(12, "Participante", FuenteNormal);
                    lpDetalle1.Alignment = Element.ALIGN_CENTER;

                    Paragraph lpDetalle2 = new Paragraph(12, "Cantidad Contratada", FuenteNormal);
                    lpDetalle2.Alignment = Element.ALIGN_CENTER;

                    Paragraph lpDetalle2a = new Paragraph(12, "MBTUD", FuenteNormal);
                    lpDetalle2a.Alignment = Element.ALIGN_CENTER;

                    Paragraph lpDetalle3 = new Paragraph(12, "Valor Garantía", FuenteNormal);
                    lpDetalle3.Alignment = Element.ALIGN_CENTER;

                    Paragraph lpDetalle3a = new Paragraph(12, "En Pesos", FuenteNormal);
                    lpDetalle3a.Alignment = Element.ALIGN_CENTER;

                    Paragraph lpDetalle1d = new Paragraph(12, " " + lLector["nombre_operador"].ToString(), FuenteNormal1);
                    lpDetalle1d.Alignment = Element.ALIGN_LEFT;

                    ldValor = Convert.ToDecimal(lLector["cnt_contratada"].ToString());
                    Paragraph lpDetalle2d = new Paragraph(12, ldValor.ToString("##,###,###") + " ", FuenteNormal1);
                    lpDetalle2d.Alignment = Element.ALIGN_RIGHT;

                    ldValor = Convert.ToDecimal(lLector["valor_garantia"].ToString());
                    Paragraph lpDetalle3d = new Paragraph(12, ldValor.ToString("###,###,###,###,###") + " ", FuenteNormal1);
                    lpDetalle3d.Alignment = Element.ALIGN_RIGHT;


                    // Cabecera del Detalle
                    PdfPCell CellCab1 = new PdfPCell();
                    CellCab1.Border = 1;
                    CellCab1.BorderWidthLeft = 1;
                    CellCab1.BorderWidthTop = 1;
                    CellCab1.BorderWidthRight = 1;
                    CellCab1.BorderWidthBottom = 1;
                    CellCab1.AddElement(lpDetalle1);
                    CellCab1.VerticalAlignment = Element.ALIGN_TOP;
                    CellCab1.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaRegistros1.AddCell(CellCab1);

                    PdfPCell CellCab2 = new PdfPCell();
                    CellCab2.Border = 1;
                    CellCab2.BorderWidthLeft = 0;
                    CellCab2.BorderWidthTop = 1;
                    CellCab2.BorderWidthRight = 1;
                    CellCab2.BorderWidthBottom = 1;
                    CellCab2.AddElement(lpDetalle2);
                    CellCab2.AddElement(lpDetalle2a);
                    CellCab2.VerticalAlignment = Element.ALIGN_TOP;
                    CellCab2.HorizontalAlignment = Element.ALIGN_CENTER;
                    TablaRegistros1.AddCell(CellCab2);

                    PdfPCell CellCab3 = new PdfPCell();
                    CellCab3.Border = 1;
                    CellCab3.BorderWidthLeft = 0;
                    CellCab3.BorderWidthTop = 1;
                    CellCab3.BorderWidthRight = 1;
                    CellCab3.BorderWidthBottom = 1;
                    CellCab3.AddElement(lpDetalle3);
                    CellCab3.AddElement(lpDetalle3a);
                    CellCab3.VerticalAlignment = Element.ALIGN_TOP;
                    CellCab3.HorizontalAlignment = Element.ALIGN_CENTER;
                    TablaRegistros1.AddCell(CellCab3);

                    // Informacion del Detalle
                    PdfPCell CellCab1d = new PdfPCell();
                    CellCab1d.Border = 1;
                    CellCab1d.BorderWidthLeft = 1;
                    CellCab1d.BorderWidthTop = 0;
                    CellCab1d.BorderWidthRight = 1;
                    CellCab1d.BorderWidthBottom = 1;
                    CellCab1d.AddElement(lpDetalle1d);
                    CellCab1d.VerticalAlignment = Element.ALIGN_TOP;
                    CellCab1d.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaRegistros1.AddCell(CellCab1d);

                    PdfPCell CellCab2d = new PdfPCell();
                    CellCab2d.Border = 1;
                    CellCab2d.BorderWidthLeft = 0;
                    CellCab2d.BorderWidthTop = 0;
                    CellCab2d.BorderWidthRight = 1;
                    CellCab2d.BorderWidthBottom = 1;
                    CellCab2d.AddElement(lpDetalle2d);
                    CellCab2d.VerticalAlignment = Element.ALIGN_TOP;
                    CellCab2d.HorizontalAlignment = Element.ALIGN_RIGHT;
                    TablaRegistros1.AddCell(CellCab2d);

                    PdfPCell CellCab3d = new PdfPCell();
                    CellCab3d.Border = 1;
                    CellCab3d.BorderWidthLeft = 0;
                    CellCab3d.BorderWidthTop = 0;
                    CellCab3d.BorderWidthRight = 1;
                    CellCab3d.BorderWidthBottom = 1;
                    CellCab3d.AddElement(lpDetalle3d);
                    CellCab3d.VerticalAlignment = Element.ALIGN_TOP;
                    CellCab3d.HorizontalAlignment = Element.ALIGN_RIGHT;
                    TablaRegistros1.AddCell(CellCab3d);
                    loDocument.Add(TablaRegistros1);
                    loDocument.Add(lpEspacios);
                    loDocument.Add(lpEspacios);
                    loDocument.Add(lpEspacios);

                    Paragraph lpContenido1 = new Paragraph();
                    lpContenido1.Add(new Chunk(lLector["texto2"].ToString(), FuenteNormal));
                    lpContenido1.Alignment = Element.ALIGN_JUSTIFIED;
                    loDocument.Add(lpContenido1);
                    loDocument.Add(lpEspacios);
                    loDocument.Add(lpEspacios);

                    Paragraph lpContenido2 = new Paragraph();
                    lpContenido2.Add(new Chunk(lLector["texto3"].ToString(), FuenteNormal));
                    lpContenido2.Alignment = Element.ALIGN_JUSTIFIED;
                    loDocument.Add(lpContenido2);
                    loDocument.Add(lpEspacios);
                    loDocument.Add(lpEspacios);


                    Paragraph lpContenido4 = new Paragraph();
                    lpContenido4.Add(new Chunk("Cordialmente,", FuenteNormal));
                    lpContenido4.Alignment = Element.ALIGN_JUSTIFIED;
                    loDocument.Add(lpContenido4);
                    loDocument.Add(lpEspacios);
                    loDocument.Add(lpEspacios);
                    loDocument.Add(lpEspacios);
                    loDocument.Add(lpEspacios);
                    loDocument.Add(lpEspacios);
                    loDocument.Add(lpEspacios);
                    loDocument.Add(lpEspacios);
                    loDocument.Add(lpEspacios);

                    // Firma de la Carta
                    Paragraph lpFirmalg = new Paragraph();
                    lpFirmalg.Add(new Chunk(LogoFirma, 0, 0));
                    lpFirmalg.Alignment = Element.ALIGN_CENTER;
                    loDocument.Add(lpFirmalg);
                    Paragraph lpFirma = new Paragraph();
                    lpFirma.Add(new Chunk(lLector["nombre_firma"].ToString(), FuenteNegrita));
                    lpFirma.Alignment = Element.ALIGN_CENTER;
                    loDocument.Add(lpFirma);

                    Paragraph lpFirma1 = new Paragraph();
                    lpFirma1.Add(new Chunk(lLector["cargo_firma"].ToString(), FuenteNormal));
                    lpFirma1.Alignment = Element.ALIGN_CENTER;
                    loDocument.Add(lpFirma1);
                    Paragraph lpFirma2 = new Paragraph();
                    lpFirma2.Add(new Chunk("Bolsa Mercantil de Colombia S.A.", FuenteNormal));
                    lpFirma2.Alignment = Element.ALIGN_CENTER;
                    loDocument.Add(lpFirma2);

                    loDocument.Close();
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();

                    return lsArchivo;
                }
                catch (Exception ex)
                {
                    try
                    {
                        lsInfo.mensaje_error = ex.Message.ToString();
                        loDocument.Close();
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        return "ERROR en la Generación de la carta. " + ex.Message.ToString();
                    }
                    catch (Exception ex1)
                    { }
                }
            }
            return "ERROR - NO HAY DATOS";
        }
        else
        {
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
            return "ERROR - NO HAY DATOS";
        }
    }

    public string generarCartaGrtia(string lsNitOperador, string lsFechaProceso, InfoSessionVO lsInfo)
    {
        System.Web.SessionState.HttpSessionState session = HttpContext.Current.Session;
        clConexion lConexion = new clConexion(lsInfo);
        clConexion lConexion1 = new clConexion(lsInfo);
        SqlDataReader lLector;
        SqlDataReader lLector1;
        SqlCommand lComando;
        SqlCommand lComando1;
        // Definicion de las variables
        string oRutaCont = ConfigurationManager.AppSettings["RutaGarantia"].ToString();
        //string oRutaFont = ConfigurationManager.AppSettings["RutaFont"].ToString();
        string oRutaImg = ConfigurationManager.AppSettings["RutaIMG"].ToString();
        string lsArchivoFirmaCarta = "";
        lConexion.Abrir();
        lComando = new SqlCommand();
        lComando1 = new SqlCommand();
        // Obtengo los Parametros Generales de las Cartas
        string lsdirPDF = "";
        string lsRutaArchivo = "";
        string lsArchivo = "";
        decimal ldValor = 0;
        // Obtengo el COntenido de la Carta
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_GetCartaGarantia";
        lComando.Parameters.Add("@P_no_documento", SqlDbType.VarChar).Value = lsNitOperador;
        lComando.Parameters.Add("@P_fecha_proceso", SqlDbType.VarChar).Value = lsFechaProceso;
        lLector = lComando.ExecuteReader();
        if (lLector.HasRows)
        {
            Document loDocument = new Document(PageSize.LETTER, 70, 70, 10, 65);
            lLector.Read();
            try
            {
                lsArchivoFirmaCarta = lLector["archivo_firma"].ToString();
                // Se crea el Documento PDF


                iTextSharp.text.Image LogoFirma = iTextSharp.text.Image.GetInstance(oRutaImg + lsArchivoFirmaCarta);
                LogoFirma.ScalePercent(50);
                LogoFirma.Alignment = Element.ALIGN_CENTER;

                iTextSharp.text.Image LogoCarta = iTextSharp.text.Image.GetInstance(oRutaImg + "imagen cartas.png");
                LogoCarta.ScalePercent(60);
                LogoCarta.Alignment = Element.ALIGN_CENTER;

                string lsFecha = DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();

                /// Defino las Tablas a Manejar en la Generación de Documento
                int[] liAncho = { 35, 65 };
                int[] liAncho1 = { 50, 25, 25 };
                // Definicion de las Variables locales usadas
                // Creacíón de los FONT que se van a manejar en el PDF
                //BaseFont bArial = BaseFont.CreateFont(oRutaFont + "arial.ttf", BaseFont.WINANSI, true);
                //BaseFont bArialBold = BaseFont.CreateFont(oRutaFont + "arialbd.ttf", BaseFont.WINANSI, true);

                iTextSharp.text.Font FuenteNormal;
                iTextSharp.text.Font FuenteNormal1;
                iTextSharp.text.Font FuenteNegrita;

                FuenteNormal = FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.NORMAL);
                FuenteNormal1 = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                FuenteNegrita = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.Font.NORMAL);
                // Se definen los tipos de letra a manejar en el PDF
                //Font FuenteNegrita = new Font(bArialBold);
                //FuenteNegrita.Size = 10;
                //Font FuenteNormal = new Font(bArial);
                //FuenteNormal.Size = 10;
                //Font FuenteNormal1 = new Font(bArial);
                //FuenteNormal1.Size = 8;

                // Defino las Tablas a manejar en el Documento PDF
                PdfPTable TableCabecera = new PdfPTable(2);
                TableCabecera.SetWidths(liAncho);
                TableCabecera.HorizontalAlignment = Element.ALIGN_LEFT;
                TableCabecera.WidthPercentage = 100;

                PdfPTable TablaRegistros = new PdfPTable(2);
                TablaRegistros.SetWidths(liAncho);
                TablaRegistros.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaRegistros.WidthPercentage = 100;

                PdfPTable TablaRegistros1 = new PdfPTable(3);
                TablaRegistros1.SetWidths(liAncho1);
                TablaRegistros1.HorizontalAlignment = Element.ALIGN_CENTER;
                TablaRegistros1.WidthPercentage = 95;

                // Obtengo el Nombre del Archivo y la ruta del mismo en el servidor
                lsArchivo = lLector["archivo"].ToString();
                // Obtengo la Ruta de los PDF
                lsdirPDF = oRutaCont + "\\" + lsArchivo;
                // Creo la Instancia del Documento PDF en el Servidor
                PdfWriter pw = PdfWriter.GetInstance(loDocument, new FileStream(lsdirPDF, FileMode.Create));
                //Crear parametros de pdf 

                //pw.PdfVersion = '1';
                PdfDictionary parameters = new PdfDictionary(PdfName.OUTPUTINTENT);
                parameters.Put(PdfName.MODDATE, new PdfDate());
                parameters.Put(PdfName.OUTPUTCONDITIONIDENTIFIER, new PdfString("sRGB IEC61966-2.1"));
                parameters.Put(PdfName.INFO, new PdfString("sRGB IEC61966-2.1"));
                parameters.Put(PdfName.S, PdfName.GTS_PDFXVERSION);
                pw.ExtraCatalog.Put(PdfName.OUTPUTINTENTS, new PdfArray(parameters));
                pw.CreateXmpMetadata();
                /// Abro el Documento PDF
                lsdirPDF = lsRutaArchivo;

                loDocument.Open();
                //crear profile (ICC - International Color Consorsium) 
                //ICC_Profile icc = ICC_Profile.GetInstance(oRutaFont + "sRGB_Color_Space_Profile.icm");
                //pw.SetOutputIntents("Custom", "", "http://www.color.org", "sRGB IEC61966-2.1", icc);

                // Defino el Parrafo para los Espacios
                Paragraph lpEspacios = new Paragraph(5, " ", FuenteNormal1);

                Paragraph lpRadica = new Paragraph(12, lLector["radicado"].ToString(), FuenteNormal);
                lpRadica.Alignment = Element.ALIGN_RIGHT;

                // Creo los Parrafos a Incluir en la Carta
                // Fecha de la Carta
                Paragraph lpTitulo = new Paragraph(12, lLector["fecha_carta"].ToString(), FuenteNormal);
                lpTitulo.Alignment = Element.ALIGN_LEFT;

                Paragraph lpTitulo1 = new Paragraph(12, lLector["trato"].ToString(), FuenteNormal);
                lpTitulo1.Alignment = Element.ALIGN_LEFT;

                Paragraph lpTitulo1a = new Paragraph(12, lLector["nombre"].ToString(), FuenteNegrita);
                lpTitulo1a.Alignment = Element.ALIGN_LEFT;

                Paragraph lpTitulo1b = new Paragraph(12, lLector["cargo"].ToString(), FuenteNormal);
                lpTitulo1b.Alignment = Element.ALIGN_LEFT;

                Paragraph lpTitulo1c = new Paragraph(12, lLector["razon_social"].ToString(), FuenteNegrita);
                lpTitulo1c.Alignment = Element.ALIGN_LEFT;

                Paragraph lpTitulo1d = new Paragraph(12, lLector["direccion"].ToString(), FuenteNormal);
                lpTitulo1d.Alignment = Element.ALIGN_LEFT;

                Paragraph lpTitulo1e = new Paragraph(12, lLector["nombre_ciudad"].ToString(), FuenteNormal);
                lpTitulo1e.Alignment = Element.ALIGN_LEFT;

                Paragraph lpTitulo2 = new Paragraph();
                lpTitulo2.Alignment = Element.ALIGN_RIGHT;
                lpTitulo2.Add(new Chunk("       Referencia: ", FuenteNegrita));
                lpTitulo2.Add(new Chunk(lLector["referencia"].ToString(), FuenteNormal));

                Paragraph lpTitulo3 = new Paragraph();
                lpTitulo3.Alignment = Element.ALIGN_LEFT;
                lpTitulo3.Add(new Chunk(lLector["saludo"].ToString() , FuenteNormal));


                // Logo
                PdfPCell CellLogo = new PdfPCell();
                CellLogo.Border = 0;
                CellLogo.BorderWidthLeft = 0;
                CellLogo.BorderWidthTop = 0;
                CellLogo.BorderWidthRight = 0;
                CellLogo.BorderWidthBottom = 0;
                CellLogo.Colspan = 2;
                CellLogo.AddElement(LogoCarta);
                CellLogo.AddElement(lpEspacios);
                CellLogo.VerticalAlignment = Element.ALIGN_TOP;
                CellLogo.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaRegistros.AddCell(CellLogo);
                //TableCabecera.AddCell(CellLogo);

                //Paragraph cabecera = new Paragraph();
                //cabecera.Add(TableCabecera);
                //HeaderFooter titulo = new HeaderFooter(cabecera, false);
                //titulo.Border = 0;
                //loDocument.Header = titulo;


                // Fecha
                PdfPCell CellTitulo = new PdfPCell();
                CellTitulo.Border = 0;
                CellTitulo.BorderWidthLeft = 0;
                CellTitulo.BorderWidthTop = 0;
                CellTitulo.BorderWidthRight = 0;
                CellTitulo.BorderWidthBottom = 0;
                CellTitulo.Colspan = 2;
                CellTitulo.AddElement(lpTitulo);
                CellTitulo.AddElement(lpEspacios);
                CellTitulo.VerticalAlignment = Element.ALIGN_TOP;
                CellTitulo.HorizontalAlignment = Element.ALIGN_CENTER;
                TablaRegistros.AddCell(CellTitulo);

                // Radicado
                PdfPCell CellRadica = new PdfPCell();
                CellRadica.Border = 0;
                CellRadica.BorderWidthLeft = 0;
                CellRadica.BorderWidthTop = 0;
                CellRadica.BorderWidthRight = 0;
                CellRadica.BorderWidthBottom = 0;
                CellRadica.Colspan = 2;
                CellRadica.AddElement(lpRadica);
                CellRadica.AddElement(lpEspacios);
                CellRadica.VerticalAlignment = Element.ALIGN_TOP;
                CellRadica.HorizontalAlignment = Element.ALIGN_RIGHT;
                TablaRegistros.AddCell(CellRadica);

                // Encabezado
                PdfPCell CellTitulo1 = new PdfPCell();
                CellTitulo1.Border = 0;
                CellTitulo1.BorderWidthLeft = 0;
                CellTitulo1.BorderWidthTop = 0;
                CellTitulo1.BorderWidthRight = 0;
                CellTitulo1.BorderWidthBottom = 0;
                CellTitulo1.Colspan = 2;
                CellTitulo1.AddElement(lpTitulo1);
                CellTitulo1.AddElement(lpTitulo1a);
                CellTitulo1.AddElement(lpTitulo1b);
                CellTitulo1.AddElement(lpTitulo1c);
                CellTitulo1.AddElement(lpTitulo1d);
                CellTitulo1.AddElement(lpTitulo1e);
                CellTitulo1.AddElement(lpEspacios);
                CellTitulo1.AddElement(lpEspacios);
                CellTitulo1.AddElement(lpEspacios);
                CellTitulo1.VerticalAlignment = Element.ALIGN_TOP;
                CellTitulo1.HorizontalAlignment = Element.ALIGN_CENTER;
                TablaRegistros.AddCell(CellTitulo1);

                PdfPCell CellTitulo2 = new PdfPCell();
                CellTitulo2.Border = 0;
                CellTitulo2.BorderWidthLeft = 0;
                CellTitulo2.BorderWidthTop = 0;
                CellTitulo2.BorderWidthRight = 0;
                CellTitulo2.BorderWidthBottom = 0;
                CellTitulo2.Colspan = 2;
                CellTitulo2.AddElement(lpTitulo2);
                CellTitulo2.AddElement(lpEspacios);
                CellTitulo2.AddElement(lpEspacios);
                CellTitulo2.AddElement(lpEspacios);
                CellTitulo2.VerticalAlignment = Element.ALIGN_TOP;
                CellTitulo2.HorizontalAlignment = Element.ALIGN_CENTER;
                TablaRegistros.AddCell(CellTitulo2);

                PdfPCell CellTitulo3 = new PdfPCell();
                CellTitulo3.Border = 0;
                CellTitulo3.BorderWidthLeft = 0;
                CellTitulo3.BorderWidthTop = 0;
                CellTitulo3.BorderWidthRight = 0;
                CellTitulo3.BorderWidthBottom = 0;
                CellTitulo3.Colspan = 2;
                CellTitulo3.AddElement(lpTitulo3);
                CellTitulo3.AddElement(lpEspacios);
                CellTitulo3.AddElement(lpEspacios);
                CellTitulo3.AddElement(lpEspacios);
                CellTitulo3.VerticalAlignment = Element.ALIGN_TOP;
                CellTitulo3.HorizontalAlignment = Element.ALIGN_CENTER;
                TablaRegistros.AddCell(CellTitulo3);
                loDocument.Add(TablaRegistros);

                lConexion1.Abrir();
                lComando1.Connection = lConexion1.gObjConexion;
                lComando1.CommandType = CommandType.StoredProcedure;
                lComando1.CommandText = "pa_GetParrafoCartaDat";
                lComando1.Parameters.Add("@P_fecha_proceso", SqlDbType.VarChar).Value = lsFechaProceso ;
                lComando1.Parameters.Add("@P_no_documento", SqlDbType.VarChar).Value = lsNitOperador ;
                lComando1.Parameters.Add("@P_previo_detalle", SqlDbType.VarChar).Value = "S";
                lLector1 = lComando1.ExecuteReader();
                if (lLector1.HasRows)
                {
                    while (lLector1.Read())
                    {
                        // Creo el Parrafo del Encabezado de la Carta
                        Paragraph lpContenido = new Paragraph();
                        lpContenido.Add(new Chunk(lLector1["parrafo"].ToString(), FuenteNormal));
                        lpContenido.Alignment = Element.ALIGN_JUSTIFIED;
                        loDocument.Add(lpContenido);
                        loDocument.Add(lpEspacios);
                        loDocument.Add(lpEspacios);
                    }
                }
                lLector1.Close();
                lLector1.Dispose();

                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);

                // Pinto el Cuadro del Detalle de la Carta
                Paragraph lpDetalle1 = new Paragraph(12, "Participante", FuenteNormal);
                lpDetalle1.Alignment = Element.ALIGN_CENTER;

                Paragraph lpDetalle2 = new Paragraph(12, "Cantidad Garantizada MBTUD(ECV v, m - 1)", FuenteNormal);
                lpDetalle2.Alignment = Element.ALIGN_CENTER;

                Paragraph lpDetalle3 = new Paragraph(12, "Valor Garantía/factura en Pesos", FuenteNormal);
                lpDetalle3.Alignment = Element.ALIGN_CENTER;

                Paragraph lpDetalle1d = new Paragraph(12, " " + lLector["razon_social"].ToString(), FuenteNormal1);
                lpDetalle1d.Alignment = Element.ALIGN_LEFT;

                ldValor = Convert.ToDecimal(lLector["cantidad_energia"].ToString());
                Paragraph lpDetalle2d = new Paragraph(12, ldValor.ToString("###,###,###,###,###") + " ", FuenteNormal1);
                lpDetalle2d.Alignment = Element.ALIGN_RIGHT;

                ldValor = Convert.ToDecimal(lLector["valor_factura"].ToString());
                Paragraph lpDetalle3d = new Paragraph(12, ldValor.ToString("$###,###,###,###,###,###") + " ", FuenteNormal1);
                lpDetalle3d.Alignment = Element.ALIGN_RIGHT;


                // Cabecera del Detalle
                PdfPCell CellCab1 = new PdfPCell();
                CellCab1.Border = 1;
                CellCab1.BorderWidthLeft = 0;
                CellCab1.BorderWidthTop = 1;
                CellCab1.BorderWidthRight = 0;
                CellCab1.BorderWidthBottom = 1;
                CellCab1.BorderColor = new Color(0, 0, 139);
                CellCab1.AddElement(lpDetalle1);
                CellCab1.VerticalAlignment = Element.ALIGN_TOP;
                CellCab1.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaRegistros1.AddCell(CellCab1);

                PdfPCell CellCab2 = new PdfPCell();
                CellCab2.Border = 1;
                CellCab2.BorderWidthLeft = 0;
                CellCab2.BorderWidthTop = 1;
                CellCab2.BorderWidthRight = 0;
                CellCab2.BorderWidthBottom = 1;
                CellCab2.BorderColor = new Color(0,0,139);
                CellCab2.AddElement(lpDetalle2);
                CellCab2.VerticalAlignment = Element.ALIGN_TOP;
                CellCab2.HorizontalAlignment = Element.ALIGN_CENTER;
                TablaRegistros1.AddCell(CellCab2);

                PdfPCell CellCab3 = new PdfPCell();
                CellCab3.Border = 1;
                CellCab3.BorderWidthLeft = 0;
                CellCab3.BorderWidthTop = 1;
                CellCab3.BorderWidthRight = 0;
                CellCab3.BorderWidthBottom = 1;
                CellCab3.BorderColor = new Color(0, 0, 139);
                CellCab3.AddElement(lpDetalle3);
                CellCab3.VerticalAlignment = Element.ALIGN_TOP;
                CellCab3.HorizontalAlignment = Element.ALIGN_CENTER;
                TablaRegistros1.AddCell(CellCab3);

                // Informacion del Detalle
                PdfPCell CellCab1d = new PdfPCell();
                CellCab1d.Border = 1;
                CellCab1d.BorderWidthLeft = 0;
                CellCab1d.BorderWidthTop = 0;
                CellCab1d.BorderWidthRight = 0;
                CellCab1d.BorderWidthBottom = 1;
                CellCab1d.BackgroundColor = new Color(176, 224, 230);
                CellCab1d.BorderColor = new Color(0, 0, 139);
                CellCab1d.AddElement(lpDetalle1d);
                CellCab1d.VerticalAlignment = Element.ALIGN_TOP;
                CellCab1d.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaRegistros1.AddCell(CellCab1d);

                PdfPCell CellCab2d = new PdfPCell();
                CellCab2d.Border = 1;
                CellCab2d.BorderWidthLeft = 0;
                CellCab2d.BorderWidthTop = 0;
                CellCab2d.BorderWidthRight = 0;
                CellCab2d.BorderWidthBottom = 1;
                CellCab2d.BackgroundColor = new Color(176, 224, 230);
                CellCab2d.BorderColor = new Color(0, 0, 139);
                CellCab2d.AddElement(lpDetalle2d);
                CellCab2d.VerticalAlignment = Element.ALIGN_TOP;
                CellCab2d.HorizontalAlignment = Element.ALIGN_RIGHT;
                TablaRegistros1.AddCell(CellCab2d);

                PdfPCell CellCab3d = new PdfPCell();
                CellCab3d.Border = 1;
                CellCab3d.BorderWidthLeft = 0;
                CellCab3d.BorderWidthTop = 0;
                CellCab3d.BorderWidthRight = 0;
                CellCab3d.BorderWidthBottom = 1;
                CellCab3d.BackgroundColor = new Color(176, 224, 230);
                CellCab3d.BorderColor = new Color(0, 0, 139);
                CellCab3d.AddElement(lpDetalle3d);
                CellCab3d.VerticalAlignment = Element.ALIGN_TOP;
                CellCab3d.HorizontalAlignment = Element.ALIGN_RIGHT;
                TablaRegistros1.AddCell(CellCab3d);
                loDocument.Add(TablaRegistros1);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);

                lComando1.Connection = lConexion1.gObjConexion;
                lComando1.Parameters.Clear();
                lComando1.CommandType = CommandType.StoredProcedure;
                lComando1.CommandText = "pa_GetParrafoCartaDat";
                lComando1.Parameters.Add("@P_fecha_proceso", SqlDbType.VarChar).Value = lsFechaProceso;
                lComando1.Parameters.Add("@P_no_documento", SqlDbType.VarChar).Value = lsNitOperador;
                lComando1.Parameters.Add("@P_previo_detalle", SqlDbType.VarChar).Value = "N";
                lLector1 = lComando1.ExecuteReader();
                if (lLector1.HasRows)
                {
                    while (lLector1.Read())
                    {
                        // Creo el Parrafo del Encabezado de la Carta
                        Paragraph lpContenido = new Paragraph();
                        lpContenido.Add(new Chunk(lLector1["parrafo"].ToString(), FuenteNormal));
                        lpContenido.Alignment = Element.ALIGN_JUSTIFIED;
                        loDocument.Add(lpContenido);
                        loDocument.Add(lpEspacios);
                        loDocument.Add(lpEspacios);
                    }
                }
                lLector1.Close();
                lLector1.Dispose();
                lConexion1.Cerrar();

                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                Paragraph lpContenido4 = new Paragraph();
                lpContenido4.Add(new Chunk("Cordial saludo,", FuenteNormal));
                lpContenido4.Alignment = Element.ALIGN_JUSTIFIED;
                loDocument.Add(lpContenido4);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);

                //Firma de la Carta
                Paragraph lpFirmalg = new Paragraph();
                lpFirmalg.Add(new Chunk(LogoFirma, 0, 0));
                lpFirmalg.Alignment = Element.ALIGN_CENTER;
                loDocument.Add(lpFirmalg);
                Paragraph lpFirma = new Paragraph();
                lpFirma.Add(new Chunk(lLector["nombre_firma"].ToString(), FuenteNegrita));
                lpFirma.Alignment = Element.ALIGN_CENTER;
                loDocument.Add(lpFirma);

                Paragraph lpFirma1 = new Paragraph();
                lpFirma1.Add(new Chunk(lLector["cargo_firma"].ToString(), FuenteNormal));
                lpFirma1.Alignment = Element.ALIGN_CENTER;
                loDocument.Add(lpFirma1);

                loDocument.Close();
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();

                return lsArchivo;
            }
            catch (Exception ex)
            {
                try
                {
                    lsInfo.mensaje_error = ex.Message.ToString();
                    loDocument.Close();
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    return "ERROR en la Generación de la carta. " + ex.Message.ToString();
                }
                catch (Exception ex1)
                { }
            }
            return "";
        }
        else
        {
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
            return "ERROR - NO HAY DATOS";
        }
    }
    //class headerfooter : PdfPageEventHelper
    //{
    //    public override void OnEndPage(PdfWriter writer, Document document)
    //    { 

    //    }
    //}
}