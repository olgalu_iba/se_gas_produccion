﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using SIB.BASE.Negocio;

/// <summary>
/// Summary description for clControlUsuario
/// </summary>
public class clControlUsuario
{
    public clControlUsuario()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    /// <summary>
    /// Nombre: validaAcceso
    /// Fecha: Agosto 20 de 2015
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para validacion de Acceso a Pantalla por Usuario
    /// Modificacion:
    /// </summary>
    /// <param name="lsPantalla">Pantalla donde el usuario esta Ingresando</param>
    /// <param name="lsusuario">Usuario que esta logeado en el Sistema</param>
    /// <param name="lsInfo">Session</param>
    public bool validaAcceso(string lsPantalla, string lsusuario, InfoSessionVO lsInfo)
    {
        clConexion lConexion = new clConexion(lsInfo);
        SqlDataReader lLector;

        try
        {
            // Se instancia la Conexion
            lConexion = new clConexion(lsInfo);
            lConexion.Abrir();

            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_autorizado_pantalla", " pantalla = '" + lsPantalla + "' And login_autorizado = '" + lsusuario + "' And estado = 'A' ");
            if (lLector.HasRows)
            {
                lLector.Close();
                lLector.Dispose();
                return true;
            }
            else
            {
                lLector.Close();
                lLector.Dispose();
                return false;
            }
        }
        catch (Exception ex)
        {
            return false;
        }
    }
}
