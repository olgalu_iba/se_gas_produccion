﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using iTextSharp.text;
using iTextSharp.text.pdf;



/// <summary>
/// Summary description for clImpresionCertificado
/// </summary>
public class clImpresionCertificado
{
    string oRutaPdf = ConfigurationManager.AppSettings["RutaPDF"].ToString();
    string oRutaImg = ConfigurationManager.AppSettings["RutaIMG"].ToString();

    public clImpresionCertificado()
    {
        //
        // TODO: Add constructor logic here
        //

    }

    /// <summary>
    /// Nombre: generarCertificadoGas
    /// Fecha: Enero 10 de 2014
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar el PDF del Certificado de Negociacion de Gas
    /// Modificacion:
    /// </summary>
    /// <param name="lsFechaRueda"></param>
    /// <param name="ldNumeroOperacionIn"></param>
    /// <param name="ldNumeroOperacionFi"></param>
    /// <param name="lsIndicador"></param>
    /// //20190717 rq050-19
    //public string generarCertificadoGas(string lsCadena, string lsFechaCorte, InfoSessionVO lsInfo) //20171130 rq026-17
    //{
    //    clConexion lConexion = new clConexion(lsInfo);
    //    clConexion lConexion1 = new clConexion(lsInfo);
    //    SqlDataReader lLector;

    //    string[] lsNombreParametros = { "@P_numero_contrato", "@P_fecha_corte" }; //20171130 rq026-17
    //    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar }; //20171130 rq026-17
    //    Object[] lValorParametros = { "0", lsFechaCorte }; //20171130 rq026-17

    //    int[] liAnchoLogo = { 40, 40, 20 };
    //    int[] liAnchoCabecera = { 20, 30, 20, 30 };
    //    int[] liAnchoContenido = { 50, 50 };
    //    int[] liAnchoLiquidacion = { 15, 25, 25, 25, 10 };
    //    Document loDocument = new Document(PageSize.LETTER, 20, 20, 20, 20);
    //    string lsdirLogo = "";
    //    string lsdirPDF = "ARCHIVO - ";
    //    string lsNombreArchivo = "";
    //    decimal ldValor = 0;
    //    string[] lsContrato;
    //    lsContrato = lsCadena.Split(',');
    //    string lsNomFirma = "";
    //    string lsCarFirma = "";
    //    string lsArcFirma = "";
    //    int liCont = 0;
    //    try
    //    {
    //        // Se instancia la Conexion
    //        lConexion = new clConexion(lsInfo);
    //        lConexion.Abrir();

    //        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
    //        if (lLector.HasRows)
    //        {
    //            lLector.Read();
    //            lsNomFirma = lLector["nombre_firma"].ToString();
    //            lsCarFirma = lLector["cargo_firma"].ToString();
    //            lsArcFirma = lLector["archivo_firma"].ToString();
    //        }
    //        lLector.Close();
    //        lLector.Dispose();

    //        // Definicion de los Fuentes
    //        iTextSharp.text.Font FuenteTitulo;
    //        iTextSharp.text.Font FuenteTitulo1;
    //        iTextSharp.text.Font FuenteTitulo2;
    //        iTextSharp.text.Font FuenteColor;
    //        iTextSharp.text.Font FuenteColor1;
    //        iTextSharp.text.Font FuenteContenido;
    //        iTextSharp.text.Font FuenteCampos;
    //        iTextSharp.text.Font FuenteLiquidacion;
    //        iTextSharp.text.Font FuenteLeyenda;
    //        FuenteTitulo = FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLUE);
    //        FuenteTitulo1 = FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.NORMAL, iTextSharp.text.Color.RED);
    //        FuenteTitulo2 = FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD);
    //        FuenteColor = FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.NORMAL, iTextSharp.text.Color.BLUE);
    //        FuenteColor1 = FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.NORMAL, iTextSharp.text.Color.BLUE);
    //        FuenteContenido = FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.BOLD);
    //        FuenteLiquidacion = FontFactory.GetFont(FontFactory.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
    //        FuenteCampos = FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.NORMAL);
    //        FuenteLeyenda = FontFactory.GetFont(FontFactory.HELVETICA, 4, iTextSharp.text.Font.NORMAL, iTextSharp.text.Color.WHITE);

    //        string lsArchivo = DateTime.Now.Millisecond.ToString() + "certificaGas" + lsInfo.Usuario.ToString() + ".pdf";
    //        lsNombreArchivo = oRutaPdf + "\\" + lsArchivo;
    //        lsdirPDF += "../PDF/" + lsArchivo;

    //        lsdirLogo = oRutaImg;
    //        iTextSharp.text.Image LogoBna = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\bnatop.gif");
    //        LogoBna.ScalePercent(30);

    //        iTextSharp.text.Image ArchivoFirma = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\" + lsArcFirma);
    //        ArchivoFirma.ScalePercent(40);

    //        iTextSharp.text.Image LogoFirma = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\logo_bna1.GIF");
    //        LogoFirma.ScalePercent(45);

    //        iTextSharp.text.Image LogoAud = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\LogoAudita.png");
    //        LogoAud.ScalePercent(80);

    //        iTextSharp.text.Image LogoVig = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\logo vigilado ALTA RESOLUCION.tif");
    //        LogoVig.ScalePercent(15);


    //        // Creo el Documento PDF
    //        PdfWriter.GetInstance(loDocument, new FileStream(lsNombreArchivo, FileMode.Create));
    //        //// Asigno la visualizacion del Numero de Pagina
    //        //HeaderFooter numeracion_pag = new HeaderFooter(new Phrase("Página: "), true);
    //        //numeracion_pag.Border = 0;
    //        //numeracion_pag.Alignment = Element.ALIGN_RIGHT;
    //        //loDocument.Footer = numeracion_pag;
    //        loDocument.Open();

    //        foreach (string lsCont in lsContrato)
    //        {
    //            if (lsCont != "")
    //            {
    //                try
    //                {
    //                    liCont = Convert.ToInt32(lsCont);
    //                }
    //                catch (Exception ex)
    //                {
    //                    break;
    //                }
    //                lValorParametros[0] = lsCont;
    //                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetCertificadoGas", lsNombreParametros, lTipoparametros, lValorParametros);
    //                if (lLector.HasRows)
    //                {

    //                    lLector.Read();
    //                    iTextSharp.text.Table TablaLogo = new iTextSharp.text.Table(3, liAnchoLogo);
    //                    TablaLogo.Border = 1;

    //                    iTextSharp.text.Table TablaCabecera = new iTextSharp.text.Table(4, liAnchoCabecera);
    //                    iTextSharp.text.Table TablaInformacion = new iTextSharp.text.Table(2, liAnchoContenido);
    //                    iTextSharp.text.Table TablaLiquidacion = new iTextSharp.text.Table(5, liAnchoLiquidacion);
    //                    iTextSharp.text.Table TablaVigilado = new iTextSharp.text.Table(2, liAnchoContenido);
    //                    iTextSharp.text.Table TablaResultado = new iTextSharp.text.Table(2, liAnchoContenido);

    //                    TablaLogo.Alignment = Element.ALIGN_LEFT;
    //                    TablaLogo.Offset = 0;
    //                    TablaLogo.Spacing = 0;
    //                    TablaLogo.WidthPercentage = 95;
    //                    TablaLogo.Border = 1;

    //                    TablaCabecera.Alignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.Offset = 0;
    //                    TablaCabecera.Spacing = 0;
    //                    TablaCabecera.WidthPercentage = 95;
    //                    TablaInformacion.Alignment = Element.ALIGN_LEFT;
    //                    TablaInformacion.Offset = 0;
    //                    TablaInformacion.Spacing = 0;
    //                    TablaInformacion.WidthPercentage = 95;

    //                    TablaLiquidacion.Offset = 0;
    //                    TablaLiquidacion.Spacing = 0;
    //                    TablaLiquidacion.WidthPercentage = 95;
    //                    TablaLiquidacion.Alignment = Element.ALIGN_LEFT;

    //                    TablaResultado.Offset = 0;
    //                    TablaResultado.Spacing = 0;
    //                    TablaResultado.WidthPercentage = 60;
    //                    TablaResultado.Alignment = Element.ALIGN_CENTER;

    //                    TablaVigilado.Alignment = Element.ALIGN_LEFT;
    //                    TablaVigilado.Offset = 0;
    //                    TablaVigilado.Spacing = 0;
    //                    TablaVigilado.WidthPercentage = 50;

    //                    Paragraph lpEspacios = new Paragraph(8, " ", FuenteLiquidacion);
    //                    Paragraph lpEspacios1 = new Paragraph(8, "X", FuenteLeyenda);
    //                    Paragraph lpTitulo = new Paragraph("CERTIFICADO DE ASIGNACIÓN", FuenteTitulo);
    //                    Paragraph lpTitulo1 = new Paragraph("COMERCIALIZACIÓN DE GAS NATURAL", FuenteTitulo1);
    //                    Paragraph lpTitulo2 = new Paragraph("No. de Operación", FuenteTitulo);
    //                    Paragraph lpNoCont = new Paragraph(lLector["numero_contrato"].ToString(), FuenteColor);
    //                    Paragraph lpIndModif = new Paragraph(lLector["ind_modif"].ToString(), FuenteColor);  //20171130 rq026-17
    //                    Paragraph lpFecha = new Paragraph("Fecha de Negociación (YYYY/MM/DD): " + lLector["fecha_contrato"].ToString(), FuenteTitulo);
    //                    Paragraph lpTitulo3 = new Paragraph(" " + lLector["destino_rueda"].ToString(), FuenteTitulo2);

    //                    // Se Pinta la Cabecera del Certificado
    //                    Cell CellLogo1 = new Cell();
    //                    CellLogo1.Border = 2;
    //                    CellLogo1.BorderWidthLeft = 2;
    //                    CellLogo1.BorderWidthTop = 2;
    //                    CellLogo1.BorderWidthRight = 2;
    //                    CellLogo1.BorderWidthBottom = 2;
    //                    CellLogo1.AddElement(LogoBna);
    //                    CellLogo1.Rowspan = 2;
    //                    CellLogo1.VerticalAlignment = Element.ALIGN_MIDDLE;
    //                    CellLogo1.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaLogo.AddCell(CellLogo1);

    //                    Cell CellLogo2 = new Cell();
    //                    CellLogo2.Border = 2;
    //                    CellLogo2.BorderWidthLeft = 0;
    //                    CellLogo2.BorderWidthTop = 2;
    //                    CellLogo2.BorderWidthRight = 2;
    //                    CellLogo2.BorderWidthBottom = 2;
    //                    CellLogo2.AddElement(lpTitulo);
    //                    CellLogo2.AddElement(lpTitulo1);
    //                    CellLogo2.AddElement(lpEspacios1);
    //                    CellLogo2.VerticalAlignment = Element.ALIGN_TOP;
    //                    CellLogo2.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaLogo.AddCell(CellLogo2);

    //                    Cell CellLogo3 = new Cell();
    //                    CellLogo3.Border = 2;
    //                    CellLogo3.BorderWidthLeft = 0;
    //                    CellLogo3.BorderWidthTop = 2;
    //                    CellLogo3.BorderWidthRight = 2;
    //                    CellLogo3.BorderWidthBottom = 2;
    //                    CellLogo3.AddElement(lpTitulo2);
    //                    CellLogo3.AddElement(lpNoCont);
    //                    CellLogo3.AddElement(lpIndModif); //20171130 rq026-17
    //                    CellLogo3.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellLogo3.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaLogo.AddCell(CellLogo3);

    //                    Cell CellLogo4 = new Cell();
    //                    CellLogo4.Border = 2;
    //                    CellLogo4.BorderWidthLeft = 0;
    //                    CellLogo4.BorderWidthTop = 0;
    //                    CellLogo4.BorderWidthRight = 2;
    //                    CellLogo4.BorderWidthBottom = 2;
    //                    CellLogo4.Colspan = 2;
    //                    CellLogo4.AddElement(lpFecha);
    //                    CellLogo4.AddElement(lpEspacios1);
    //                    CellLogo4.VerticalAlignment = Element.ALIGN_TOP;
    //                    CellLogo4.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaLogo.AddCell(CellLogo4);


    //                    Cell CellLogo5 = new Cell();
    //                    CellLogo5.Border = 2;
    //                    CellLogo5.BorderWidthLeft = 2;
    //                    CellLogo5.BorderWidthTop = 0;
    //                    CellLogo5.BorderWidthRight = 2;
    //                    CellLogo5.BorderWidthBottom = 2;
    //                    CellLogo5.Colspan = 3;
    //                    CellLogo5.AddElement(lpTitulo3);
    //                    CellLogo5.AddElement(lpEspacios1);
    //                    CellLogo5.VerticalAlignment = Element.ALIGN_TOP;
    //                    CellLogo5.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaLogo.AddCell(CellLogo5);

    //                    loDocument.Add(TablaLogo);
    //                    CellLogo1.Clear();
    //                    CellLogo2.Clear();
    //                    CellLogo3.Clear();
    //                    TablaLogo.DeleteAllRows();

    //                    Paragraph lpTitV = new Paragraph("  Vendedor:", FuenteLiquidacion);
    //                    Paragraph lpTitC = new Paragraph("  Comprador:", FuenteLiquidacion);

    //                    Paragraph lpTCod = new Paragraph("  ID del Participante:", FuenteLiquidacion);
    //                    Paragraph lpTNit = new Paragraph("  Nit:", FuenteLiquidacion);
    //                    Paragraph lpTTipo = new Paragraph("  Tipo de Participante:", FuenteLiquidacion);
    //                    Paragraph lpTDirec = new Paragraph("  Domiciliado en:", FuenteLiquidacion);
    //                    Paragraph lpMail = new Paragraph("  Mail:", FuenteLiquidacion);
    //                    Paragraph lpTelef = new Paragraph("  Teléfono:", FuenteLiquidacion); //20170530 divipola

    //                    Paragraph lpNomV = new Paragraph(lLector["nom_vendedor"].ToString(), FuenteContenido);
    //                    Paragraph lpNomC = new Paragraph(lLector["nom_comprador"].ToString(), FuenteContenido);

    //                    Paragraph lpIdV = new Paragraph(lLector["id_vendedor"].ToString(), FuenteContenido);
    //                    Paragraph lpIdC = new Paragraph(lLector["id_comprador"].ToString(), FuenteContenido);
    //                    Paragraph lpNitV = new Paragraph(lLector["nit_vendedor"].ToString(), FuenteContenido);
    //                    Paragraph lpNitC = new Paragraph(lLector["nit_comprador"].ToString(), FuenteContenido);
    //                    Paragraph lpTipoV = new Paragraph(lLector["tipo_vendedor"].ToString(), FuenteContenido);
    //                    Paragraph lpTipoC = new Paragraph(lLector["tipo_comprador"].ToString(), FuenteContenido);
    //                    Paragraph lpDirV = new Paragraph(lLector["direccion_vendedor"].ToString(), FuenteContenido);
    //                    Paragraph lpDirC = new Paragraph(lLector["direccion_comprador"].ToString(), FuenteContenido);
    //                    Paragraph lpMailV = new Paragraph(lLector["mail_v"].ToString(), FuenteContenido);
    //                    Paragraph lpMailC = new Paragraph(lLector["mail_c"].ToString(), FuenteContenido);
    //                    Paragraph lpTeleV = new Paragraph(lLector["telefono_v"].ToString(), FuenteContenido);
    //                    Paragraph lpTeleC = new Paragraph(lLector["telefono_c"].ToString(), FuenteContenido);

    //                    // Se Pinta la Tabla de Informacion de las Puntas
    //                    /// Nombre
    //                    Cell CellInf1 = new Cell();
    //                    CellInf1.BorderWidthLeft = 2;
    //                    CellInf1.BorderWidthTop = 0;
    //                    CellInf1.BorderWidthRight = 0;
    //                    CellInf1.BorderWidthBottom = 0;
    //                    CellInf1.AddElement(lpTitV);
    //                    CellInf1.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf1.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf1);

    //                    Cell CellInf2 = new Cell();
    //                    CellInf2.BorderWidthLeft = 0;
    //                    CellInf2.BorderWidthTop = 0;
    //                    CellInf2.BorderWidthRight = 2;
    //                    CellInf2.BorderWidthBottom = 0;
    //                    CellInf2.AddElement(lpNomV);
    //                    CellInf2.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf2.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf2);

    //                    Cell CellInf3 = new Cell();
    //                    CellInf3.BorderWidthLeft = 0;
    //                    CellInf3.BorderWidthTop = 0;
    //                    CellInf3.BorderWidthRight = 0;
    //                    CellInf3.BorderWidthBottom = 0;
    //                    CellInf3.AddElement(lpTitC);
    //                    CellInf3.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf3.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf3);

    //                    Cell CellInf4 = new Cell();
    //                    CellInf4.BorderWidthLeft = 0;
    //                    CellInf4.BorderWidthTop = 0;
    //                    CellInf4.BorderWidthRight = 2;
    //                    CellInf4.BorderWidthBottom = 0;
    //                    CellInf4.AddElement(lpNomC);
    //                    CellInf4.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf4.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf4);

    //                    /// Id
    //                    Cell CellInf1a = new Cell();
    //                    CellInf1a.BorderWidthLeft = 2;
    //                    CellInf1a.BorderWidthTop = 0;
    //                    CellInf1a.BorderWidthRight = 0;
    //                    CellInf1a.BorderWidthBottom = 0;
    //                    CellInf1a.AddElement(lpTCod);
    //                    CellInf1a.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf1a.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf1a);

    //                    Cell CellInf2a = new Cell();
    //                    CellInf2a.BorderWidthLeft = 0;
    //                    CellInf2a.BorderWidthTop = 0;
    //                    CellInf2a.BorderWidthRight = 2;
    //                    CellInf2a.BorderWidthBottom = 0;
    //                    CellInf2a.AddElement(lpIdV);
    //                    CellInf2a.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf2a.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf2a);

    //                    Cell CellInf3a = new Cell();
    //                    CellInf3a.BorderWidthLeft = 0;
    //                    CellInf3a.BorderWidthTop = 0;
    //                    CellInf3a.BorderWidthRight = 0;
    //                    CellInf3a.BorderWidthBottom = 0;
    //                    CellInf3a.AddElement(lpTCod);
    //                    CellInf3a.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf3a.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf3a);

    //                    Cell CellInf4a = new Cell();
    //                    CellInf4a.BorderWidthLeft = 0;
    //                    CellInf4a.BorderWidthTop = 0;
    //                    CellInf4a.BorderWidthRight = 2;
    //                    CellInf4a.BorderWidthBottom = 0;
    //                    CellInf4a.AddElement(lpIdC);
    //                    CellInf4a.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf4a.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf4a);

    //                    /// Nit
    //                    Cell CellInf1b = new Cell();
    //                    CellInf1b.BorderWidthLeft = 2;
    //                    CellInf1b.BorderWidthTop = 0;
    //                    CellInf1b.BorderWidthRight = 0;
    //                    CellInf1b.BorderWidthBottom = 0;
    //                    CellInf1b.AddElement(lpTNit);
    //                    CellInf1b.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf1b.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf1b);

    //                    Cell CellInf2b = new Cell();
    //                    CellInf2b.BorderWidthLeft = 0;
    //                    CellInf2b.BorderWidthTop = 0;
    //                    CellInf2b.BorderWidthRight = 2;
    //                    CellInf2b.BorderWidthBottom = 0;
    //                    CellInf2b.AddElement(lpNitV);
    //                    CellInf2b.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf2b.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf2b);

    //                    Cell CellInf3b = new Cell();
    //                    CellInf3b.BorderWidthLeft = 0;
    //                    CellInf3b.BorderWidthTop = 0;
    //                    CellInf3b.BorderWidthRight = 0;
    //                    CellInf3b.BorderWidthBottom = 0;
    //                    CellInf3b.AddElement(lpTNit);
    //                    CellInf3b.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf3b.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf3b);

    //                    Cell CellInf4b = new Cell();
    //                    CellInf4b.BorderWidthLeft = 0;
    //                    CellInf4b.BorderWidthTop = 0;
    //                    CellInf4b.BorderWidthRight = 2;
    //                    CellInf4b.BorderWidthBottom = 0;
    //                    CellInf4b.AddElement(lpNitC);
    //                    CellInf4b.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf4b.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf4b);

    //                    /// Tipo
    //                    Cell CellInf1c = new Cell();
    //                    CellInf1c.BorderWidthLeft = 2;
    //                    CellInf1c.BorderWidthTop = 0;
    //                    CellInf1c.BorderWidthRight = 0;
    //                    CellInf1c.BorderWidthBottom = 0;
    //                    CellInf1c.AddElement(lpTTipo);
    //                    CellInf1c.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf1c.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf1c);

    //                    Cell CellInf2c = new Cell();
    //                    CellInf2c.BorderWidthLeft = 0;
    //                    CellInf2c.BorderWidthTop = 0;
    //                    CellInf2c.BorderWidthRight = 2;
    //                    CellInf2c.BorderWidthBottom = 0;
    //                    CellInf2c.AddElement(lpTipoV);
    //                    CellInf2c.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf2c.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf2c);

    //                    Cell CellInf3c = new Cell();
    //                    CellInf3c.BorderWidthLeft = 0;
    //                    CellInf3c.BorderWidthTop = 0;
    //                    CellInf3c.BorderWidthRight = 0;
    //                    CellInf3c.BorderWidthBottom = 0;
    //                    CellInf3c.AddElement(lpTTipo);
    //                    CellInf3c.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf3c.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf3c);

    //                    Cell CellInf4c = new Cell();
    //                    CellInf4c.BorderWidthLeft = 0;
    //                    CellInf4c.BorderWidthTop = 0;
    //                    CellInf4c.BorderWidthRight = 2;
    //                    CellInf4c.BorderWidthBottom = 0;
    //                    CellInf4c.AddElement(lpTipoC);
    //                    CellInf4c.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf4c.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf4c);

    //                    /// Direccion
    //                    Cell CellInf1d = new Cell();
    //                    CellInf1d.BorderWidthLeft = 2;
    //                    CellInf1d.BorderWidthTop = 0;
    //                    CellInf1d.BorderWidthRight = 0;
    //                    CellInf1d.BorderWidthBottom = 0;
    //                    CellInf1d.AddElement(lpTDirec);
    //                    CellInf1d.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf1d.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf1d);

    //                    Cell CellInf2d = new Cell();
    //                    CellInf2d.BorderWidthLeft = 0;
    //                    CellInf2d.BorderWidthTop = 0;
    //                    CellInf2d.BorderWidthRight = 2;
    //                    CellInf2d.BorderWidthBottom = 0;
    //                    CellInf2d.AddElement(lpDirV);
    //                    CellInf2d.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf2d.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf2d);

    //                    Cell CellInf3d = new Cell();
    //                    CellInf3d.BorderWidthLeft = 0;
    //                    CellInf3d.BorderWidthTop = 0;
    //                    CellInf3d.BorderWidthRight = 0;
    //                    CellInf3d.BorderWidthBottom = 0;
    //                    CellInf3d.AddElement(lpTDirec);
    //                    CellInf3d.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf3d.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf3d);

    //                    Cell CellInf4d = new Cell();
    //                    CellInf4d.BorderWidthLeft = 0;
    //                    CellInf4d.BorderWidthTop = 0;
    //                    CellInf4d.BorderWidthRight = 2;
    //                    CellInf4d.BorderWidthBottom = 0;
    //                    CellInf4d.AddElement(lpDirC);
    //                    CellInf4d.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf4d.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf4d);

    //                    /// Mail
    //                    Cell CellInf5d = new Cell();
    //                    CellInf5d.BorderWidthLeft = 0;
    //                    CellInf5d.BorderWidthTop = 0;
    //                    CellInf5d.BorderWidthRight = 0;
    //                    CellInf5d.BorderWidthBottom = 0;
    //                    CellInf5d.AddElement(lpMail);
    //                    CellInf5d.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf5d.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf5d);

    //                    Cell CellInf6d = new Cell();
    //                    CellInf6d.BorderWidthLeft = 0;
    //                    CellInf6d.BorderWidthTop = 0;
    //                    CellInf6d.BorderWidthRight = 2;
    //                    CellInf6d.BorderWidthBottom = 0;
    //                    CellInf6d.AddElement(lpMailV);
    //                    CellInf6d.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf6d.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf6d);


    //                    Cell CellInf7d = new Cell();
    //                    CellInf7d.BorderWidthLeft = 0;
    //                    CellInf7d.BorderWidthTop = 0;
    //                    CellInf7d.BorderWidthRight = 0;
    //                    CellInf7d.BorderWidthBottom = 0;
    //                    CellInf7d.AddElement(lpMail);
    //                    CellInf7d.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf7d.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf7d);

    //                    Cell CellInf8d = new Cell();
    //                    CellInf8d.BorderWidthLeft = 0;
    //                    CellInf8d.BorderWidthTop = 0;
    //                    CellInf8d.BorderWidthRight = 2;
    //                    CellInf8d.BorderWidthBottom = 0;
    //                    CellInf8d.AddElement(lpMailC);
    //                    CellInf8d.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf8d.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf8d);


    //                    /// Telefono
    //                    Cell CellInf5t = new Cell();
    //                    CellInf5t.BorderWidthLeft = 0;
    //                    CellInf5t.BorderWidthTop = 0;
    //                    CellInf5t.BorderWidthRight = 0;
    //                    CellInf5t.BorderWidthBottom = 2;
    //                    CellInf5t.AddElement(lpTelef);
    //                    CellInf5t.AddElement(lpEspacios1);
    //                    CellInf5t.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf5t.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf5t);

    //                    Cell CellInf6t = new Cell();
    //                    CellInf6t.BorderWidthLeft = 0;
    //                    CellInf6t.BorderWidthTop = 0;
    //                    CellInf6t.BorderWidthRight = 2;
    //                    CellInf6t.BorderWidthBottom = 2;
    //                    CellInf6t.AddElement(lpTeleV);
    //                    CellInf6t.AddElement(lpEspacios1);
    //                    CellInf6t.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf6t.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf6t);

    //                    Cell CellInf7t = new Cell();
    //                    CellInf7t.BorderWidthLeft = 0;
    //                    CellInf7t.BorderWidthTop = 0;
    //                    CellInf7t.BorderWidthRight = 0;
    //                    CellInf7t.BorderWidthBottom = 2;
    //                    CellInf7t.AddElement(lpTelef);
    //                    CellInf7t.AddElement(lpEspacios1);
    //                    CellInf7t.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf7t.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf7t);

    //                    Cell CellInf8t = new Cell();
    //                    CellInf8t.BorderWidthLeft = 0;
    //                    CellInf8t.BorderWidthTop = 0;
    //                    CellInf8t.BorderWidthRight = 2;
    //                    CellInf8t.BorderWidthBottom = 2;
    //                    CellInf8t.AddElement(lpTeleC);
    //                    CellInf8t.AddElement(lpEspacios1);
    //                    CellInf8t.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellInf8t.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaCabecera.AddCell(CellInf8t);


    //                    loDocument.Add(TablaCabecera);

    //                    TablaCabecera.DeleteAllRows();
    //                    loDocument.Add(lpEspacios);

    //                    Paragraph lpTProd = new Paragraph("PRODUCTO", FuenteLiquidacion);
    //                    Paragraph lpProd = new Paragraph(lLector["producto"].ToString(), FuenteContenido);

    //                    Paragraph lpTMod = new Paragraph("Modalidad Contractual", FuenteLiquidacion);
    //                    Paragraph lpModa = new Paragraph("  " + lLector["modalidad"].ToString(), FuenteContenido);

    //                    Paragraph lpTPunto = new Paragraph("Ruta / Punto de Entrega", FuenteLiquidacion);
    //                    Paragraph lpPunto = new Paragraph("  " + lLector["punto_entrega"].ToString(), FuenteContenido);

    //                    Paragraph lpTDur = new Paragraph("Duración", FuenteLiquidacion); //20170530 divipola
    //                    Paragraph lpDura = new Paragraph("  " + lLector["duracion"].ToString(), FuenteContenido);

    //                    Paragraph lpDetProd = new Paragraph("Han negociado el producto que se detalla a continuación:", FuenteLiquidacion);

    //                    TablaLogo.Border = 0;

    //                    Cell CellLogo5x = new Cell();
    //                    CellLogo5x.Border = 0;
    //                    CellLogo5x.BorderWidthLeft = 0;
    //                    CellLogo5x.BorderWidthTop = 0;
    //                    CellLogo5x.BorderWidthRight = 0;
    //                    CellLogo5x.BorderWidthBottom = 0;
    //                    CellLogo5x.Colspan = 3;
    //                    CellLogo5x.AddElement(lpDetProd);
    //                    CellLogo5x.AddElement(lpEspacios1);
    //                    CellLogo5x.VerticalAlignment = Element.ALIGN_TOP;
    //                    CellLogo5x.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaLogo.AddCell(CellLogo5x);

    //                    loDocument.Add(TablaLogo);
    //                    CellLogo1.Clear();
    //                    CellLogo2.Clear();
    //                    CellLogo3.Clear();
    //                    TablaLogo.DeleteAllRows();

    //                    loDocument.Add(lpEspacios);

    //                    // Se Pinta la Tabla de Informacion de Producto
    //                    /// Producto
    //                    Cell CellPrd1 = new Cell();
    //                    CellPrd1.BorderWidthLeft = 2;
    //                    CellPrd1.BorderWidthTop = 2;
    //                    CellPrd1.BorderWidthRight = 2;
    //                    CellPrd1.BorderWidthBottom = 2;
    //                    CellPrd1.Rowspan = 2;
    //                    CellPrd1.AddElement(lpTProd);
    //                    CellPrd1.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellPrd1.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaLiquidacion.AddCell(CellPrd1);

    //                    Cell CellPrd2 = new Cell();
    //                    CellPrd2.BorderWidthLeft = 0;
    //                    CellPrd2.BorderWidthTop = 2;
    //                    CellPrd2.BorderWidthRight = 2;
    //                    CellPrd2.BorderWidthBottom = 2;
    //                    CellPrd2.Rowspan = 2;
    //                    CellPrd2.AddElement(lpProd);
    //                    CellPrd2.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellPrd2.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaLiquidacion.AddCell(CellPrd2);

    //                    /// Modalidad
    //                    Cell CellPrd3 = new Cell();
    //                    CellPrd3.BorderWidthLeft = 0;
    //                    CellPrd3.BorderWidthTop = 2;
    //                    CellPrd3.BorderWidthRight = 2;
    //                    CellPrd3.BorderWidthBottom = 2;
    //                    CellPrd3.AddElement(lpTMod);
    //                    CellPrd3.AddElement(lpEspacios1);
    //                    CellPrd3.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellPrd3.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaLiquidacion.AddCell(CellPrd3);

    //                    /// Punto Ent
    //                    Cell CellPrd4 = new Cell();
    //                    CellPrd4.BorderWidthLeft = 0;
    //                    CellPrd4.BorderWidthTop = 2;
    //                    CellPrd4.BorderWidthRight = 2;
    //                    CellPrd4.BorderWidthBottom = 2;
    //                    CellPrd4.AddElement(lpTPunto);
    //                    CellPrd4.AddElement(lpEspacios1);
    //                    CellPrd4.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellPrd4.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaLiquidacion.AddCell(CellPrd4);

    //                    /// Duracion
    //                    Cell CellPrd5 = new Cell();
    //                    CellPrd5.BorderWidthLeft = 0;
    //                    CellPrd5.BorderWidthTop = 2;
    //                    CellPrd5.BorderWidthRight = 2;
    //                    CellPrd5.BorderWidthBottom = 2;
    //                    CellPrd5.AddElement(lpTDur);
    //                    CellPrd5.AddElement(lpEspacios1);
    //                    CellPrd5.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellPrd5.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaLiquidacion.AddCell(CellPrd5);

    //                    /// Modalidad
    //                    Cell CellPrd3i = new Cell();
    //                    CellPrd3i.BorderWidthLeft = 0;
    //                    CellPrd3i.BorderWidthTop = 0;
    //                    CellPrd3i.BorderWidthRight = 2;
    //                    CellPrd3i.BorderWidthBottom = 2;
    //                    CellPrd3i.AddElement(lpModa);
    //                    CellPrd3i.AddElement(lpEspacios1);
    //                    CellPrd3i.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellPrd3i.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaLiquidacion.AddCell(CellPrd3i);

    //                    /// Punto Ent
    //                    Cell CellPrd4i = new Cell();
    //                    CellPrd4i.BorderWidthLeft = 0;
    //                    CellPrd4i.BorderWidthTop = 0;
    //                    CellPrd4i.BorderWidthRight = 2;
    //                    CellPrd4i.BorderWidthBottom = 2;
    //                    CellPrd4i.AddElement(lpPunto);
    //                    CellPrd4i.AddElement(lpEspacios1);
    //                    CellPrd4i.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellPrd4i.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaLiquidacion.AddCell(CellPrd4i);

    //                    /// Duracion
    //                    Cell CellPrd5i = new Cell();
    //                    CellPrd5i.BorderWidthLeft = 0;
    //                    CellPrd5i.BorderWidthTop = 0;
    //                    CellPrd5i.BorderWidthRight = 2;
    //                    CellPrd5i.BorderWidthBottom = 2;
    //                    CellPrd5i.AddElement(lpDura);
    //                    CellPrd5i.AddElement(lpEspacios1);
    //                    CellPrd5i.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellPrd5i.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaLiquidacion.AddCell(CellPrd5i);
    //                    loDocument.Add(TablaLiquidacion);
    //                    TablaLiquidacion.DeleteAllRows();

    //                    Paragraph lpDetRes = new Paragraph("Con los siguientes resultados:", FuenteLiquidacion);

    //                    Cell CellLogo5f = new Cell();
    //                    CellLogo5f.Border = 0;
    //                    CellLogo5f.BorderWidthLeft = 0;
    //                    CellLogo5f.BorderWidthTop = 0;
    //                    CellLogo5f.BorderWidthRight = 0;
    //                    CellLogo5f.BorderWidthBottom = 0;
    //                    CellLogo5f.Colspan = 3;
    //                    CellLogo5f.AddElement(lpDetRes);
    //                    CellLogo5f.AddElement(lpEspacios1);
    //                    CellLogo5f.VerticalAlignment = Element.ALIGN_TOP;
    //                    CellLogo5f.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaLogo.AddCell(CellLogo5f);

    //                    loDocument.Add(TablaLogo);
    //                    CellLogo1.Clear();
    //                    CellLogo2.Clear();
    //                    CellLogo3.Clear();
    //                    TablaLogo.DeleteAllRows();
    //                    loDocument.Add(lpEspacios);

    //                    Paragraph lpTCnt = new Paragraph("Cantidad / Capacidad Asignada", FuenteLiquidacion);
    //                    Paragraph lpCant = new Paragraph(lLector["cantidad"].ToString(), FuenteContenido);

    //                    Paragraph lpTUnd = new Paragraph("Unidad:", FuenteLiquidacion);
    //                    Paragraph lpUnid = new Paragraph(lLector["unidad_medida"].ToString(), FuenteContenido);

    //                    Paragraph lpTPre = new Paragraph("Precio de Adjudicación", FuenteLiquidacion);
    //                    Paragraph lpTPre1 = new Paragraph("(US$/MBTU) (US$ KPC)", FuenteLiquidacion);
    //                    Paragraph lpPrec = new Paragraph(lLector["precio"].ToString(), FuenteContenido);

    //                    /// Cantidad
    //                    Cell CellRes1 = new Cell();
    //                    CellRes1.BorderWidthLeft = 2;
    //                    CellRes1.BorderWidthTop = 2;
    //                    CellRes1.BorderWidthRight = 2;
    //                    CellRes1.BorderWidthBottom = 2;
    //                    CellRes1.AddElement(lpTCnt);
    //                    CellRes1.AddElement(lpEspacios1);
    //                    CellRes1.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellRes1.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaResultado.AddCell(CellRes1);

    //                    /// Unidad
    //                    Cell CellRes2 = new Cell();
    //                    CellRes2.BorderWidthLeft = 0;
    //                    CellRes2.BorderWidthTop = 2;
    //                    CellRes2.BorderWidthRight = 2;
    //                    CellRes2.BorderWidthBottom = 2;
    //                    CellRes2.AddElement(lpTUnd);
    //                    CellRes2.AddElement(lpEspacios1);
    //                    CellRes2.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellRes2.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaResultado.AddCell(CellRes2);

    //                    /// Cantidad
    //                    Cell CellRes1i = new Cell();
    //                    CellRes1i.BorderWidthLeft = 2;
    //                    CellRes1i.BorderWidthTop = 0;
    //                    CellRes1i.BorderWidthRight = 2;
    //                    CellRes1i.BorderWidthBottom = 2;
    //                    CellRes1i.AddElement(lpCant);
    //                    CellRes1i.AddElement(lpEspacios1);
    //                    CellRes1i.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellRes1i.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaResultado.AddCell(CellRes1i);

    //                    /// Unidad
    //                    Cell CellRes2i = new Cell();
    //                    CellRes2i.BorderWidthLeft = 0;
    //                    CellRes2i.BorderWidthTop = 0;
    //                    CellRes2i.BorderWidthRight = 2;
    //                    CellRes2i.BorderWidthBottom = 2;
    //                    CellRes2i.AddElement(lpUnid);
    //                    CellRes2i.AddElement(lpEspacios1);
    //                    CellRes2i.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellRes2i.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaResultado.AddCell(CellRes2i);

    //                    /// Precio
    //                    Cell CellRes3 = new Cell();
    //                    CellRes3.BorderWidthLeft = 2;
    //                    CellRes3.BorderWidthTop = 0;
    //                    CellRes3.BorderWidthRight = 2;
    //                    CellRes3.BorderWidthBottom = 2;
    //                    CellRes3.Colspan = 2;
    //                    CellRes3.AddElement(lpTPre);
    //                    CellRes3.AddElement(lpTPre1);
    //                    CellRes3.AddElement(lpEspacios1);
    //                    CellRes3.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellRes3.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaResultado.AddCell(CellRes3);

    //                    /// Precio
    //                    Cell CellRes3i = new Cell();
    //                    CellRes3i.BorderWidthLeft = 2;
    //                    CellRes3i.BorderWidthTop = 0;
    //                    CellRes3i.BorderWidthRight = 2;
    //                    CellRes3i.BorderWidthBottom = 2;
    //                    CellRes3i.Colspan = 2;
    //                    CellRes3i.AddElement(lpPrec);
    //                    CellRes3i.AddElement(lpEspacios1);
    //                    CellRes3i.VerticalAlignment = Element.ALIGN_CENTER;
    //                    CellRes3i.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaResultado.AddCell(CellRes3i);

    //                    loDocument.Add(TablaResultado);
    //                    loDocument.Add(lpEspacios);
    //                    TablaResultado.DeleteAllRows();

    //                    Paragraph lpDetNota = new Paragraph("NOTA: El vendedor y el respectivo comprador serán responsables de suscribir el contrato de compraventa en donde se deberá reflejar los resultados la presente subasta o negociación.  De igual forma, coordinarán los aspectos operativos requeridos, tales como el proceso de nominación, conforme a la regulación vigente.", FuenteLiquidacion);

    //                    Cell CellLogo5g = new Cell();
    //                    CellLogo5g.Border = 0;
    //                    CellLogo5g.BorderWidthLeft = 0;
    //                    CellLogo5g.BorderWidthTop = 0;
    //                    CellLogo5g.BorderWidthRight = 0;
    //                    CellLogo5g.BorderWidthBottom = 0;
    //                    CellLogo5g.Colspan = 3;
    //                    CellLogo5g.AddElement(lpDetNota);
    //                    CellLogo5g.VerticalAlignment = Element.ALIGN_TOP;
    //                    CellLogo5g.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaLogo.AddCell(CellLogo5g);

    //                    loDocument.Add(TablaLogo);
    //                    CellLogo1.Clear();
    //                    CellLogo2.Clear();
    //                    CellLogo3.Clear();
    //                    TablaLogo.DeleteAllRows();
    //                    loDocument.Add(lpEspacios);

    //                    Paragraph lpDetExp = new Paragraph("Expedido por:", FuenteTitulo2);

    //                    Cell CellLogo5h = new Cell();
    //                    CellLogo5h.Border = 0;
    //                    CellLogo5h.BorderWidthLeft = 0;
    //                    CellLogo5h.BorderWidthTop = 0;
    //                    CellLogo5h.BorderWidthRight = 0;
    //                    CellLogo5h.BorderWidthBottom = 0;
    //                    CellLogo5h.Colspan = 3;
    //                    CellLogo5h.AddElement(lpDetExp);
    //                    CellLogo5h.AddElement(lpEspacios1);
    //                    CellLogo5h.VerticalAlignment = Element.ALIGN_TOP;
    //                    CellLogo5h.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaLogo.AddCell(CellLogo5h);

    //                    loDocument.Add(TablaLogo);
    //                    CellLogo1.Clear();
    //                    CellLogo2.Clear();
    //                    CellLogo3.Clear();
    //                    TablaLogo.DeleteAllRows();

    //                    Cell CellLogo5s = new Cell();
    //                    CellLogo5s.Border = 0;
    //                    CellLogo5s.BorderWidthLeft = 0;
    //                    CellLogo5s.BorderWidthTop = 0;
    //                    CellLogo5s.BorderWidthRight = 0;
    //                    CellLogo5s.BorderWidthBottom = 0;
    //                    CellLogo5s.Colspan = 3;
    //                    CellLogo5s.AddElement(ArchivoFirma);
    //                    CellLogo5s.VerticalAlignment = Element.ALIGN_TOP;
    //                    CellLogo5s.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaLogo.AddCell(CellLogo5s);
    //                    loDocument.Add(TablaLogo);
    //                    CellLogo1.Clear();
    //                    CellLogo2.Clear();
    //                    CellLogo3.Clear();
    //                    TablaLogo.DeleteAllRows();

    //                    Paragraph lpRaya = new Paragraph("___________________________________", FuenteCampos);
    //                    Cell CellLogo5d = new Cell();
    //                    CellLogo5d.Border = 0;
    //                    CellLogo5d.BorderWidthLeft = 0;
    //                    CellLogo5d.BorderWidthTop = 0;
    //                    CellLogo5d.BorderWidthRight = 0;
    //                    CellLogo5d.BorderWidthBottom = 0;
    //                    CellLogo5d.Colspan = 3;
    //                    CellLogo5d.AddElement(lpRaya);
    //                    CellLogo5d.VerticalAlignment = Element.ALIGN_TOP;
    //                    CellLogo5d.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaLogo.AddCell(CellLogo5d);
    //                    loDocument.Add(TablaLogo);
    //                    CellLogo1.Clear();
    //                    CellLogo2.Clear();
    //                    CellLogo3.Clear();
    //                    TablaLogo.DeleteAllRows();


    //                    Paragraph lpNomFir = new Paragraph(lsNomFirma, FuenteCampos);
    //                    Paragraph lpCarFir = new Paragraph(lsCarFirma, FuenteCampos);
    //                    Cell CellLogo5t = new Cell();
    //                    CellLogo5t.Border = 0;
    //                    CellLogo5t.BorderWidthLeft = 0;
    //                    CellLogo5t.BorderWidthTop = 0;
    //                    CellLogo5t.BorderWidthRight = 0;
    //                    CellLogo5t.BorderWidthBottom = 0;
    //                    CellLogo5t.Colspan = 3;
    //                    CellLogo5t.AddElement(lpNomFir);
    //                    CellLogo5t.AddElement(lpCarFir);
    //                    CellLogo5t.AddElement(lpEspacios1);
    //                    CellLogo5t.VerticalAlignment = Element.ALIGN_TOP;
    //                    CellLogo5t.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    TablaLogo.AddCell(CellLogo5t);
    //                    loDocument.Add(TablaLogo);
    //                    CellLogo1.Clear();
    //                    CellLogo2.Clear();
    //                    CellLogo3.Clear();
    //                    TablaLogo.DeleteAllRows();

    //                    Cell CellLogo5v1 = new Cell();
    //                    CellLogo5v1.Border = 0;
    //                    CellLogo5v1.BorderWidthLeft = 0;
    //                    CellLogo5v1.BorderWidthTop = 0;
    //                    CellLogo5v1.BorderWidthRight = 0;
    //                    CellLogo5v1.BorderWidthBottom = 0;
    //                    CellLogo5v1.AddElement(LogoVig);
    //                    CellLogo5v1.VerticalAlignment = Element.ALIGN_TOP;
    //                    CellLogo5v1.HorizontalAlignment = Element.ALIGN_RIGHT;
    //                    TablaLogo.AddCell(CellLogo5v1);

    //                    Cell CellLogo5v = new Cell();
    //                    CellLogo5v.Border = 0;
    //                    CellLogo5v.BorderWidthLeft = 0;
    //                    CellLogo5v.BorderWidthTop = 0;
    //                    CellLogo5v.BorderWidthRight = 0;
    //                    CellLogo5v.BorderWidthBottom = 0;
    //                    CellLogo5v.Colspan = 2;
    //                    CellLogo5v.AddElement(LogoFirma);
    //                    CellLogo5v.AddElement(lpEspacios1);
    //                    CellLogo5v.VerticalAlignment = Element.ALIGN_TOP;
    //                    CellLogo5v.HorizontalAlignment = Element.ALIGN_LEFT;
    //                    TablaLogo.AddCell(CellLogo5v);
    //                    loDocument.Add(TablaLogo);
    //                    CellLogo1.Clear();
    //                    CellLogo2.Clear();
    //                    CellLogo3.Clear();
    //                    TablaLogo.DeleteAllRows();

    //                    //Paragraph lpCarAud = new Paragraph("Esta operación fue debidamente auditada por:", FuenteCampos);
    //                    //Cell CellLogo5k = new Cell();
    //                    //CellLogo5k.Border = 0;
    //                    //CellLogo5k.BorderWidthLeft = 0;
    //                    //CellLogo5k.BorderWidthTop = 0;
    //                    //CellLogo5k.BorderWidthRight = 0;
    //                    //CellLogo5k.BorderWidthBottom = 0;
    //                    //CellLogo5k.Colspan = 3;
    //                    //CellLogo5k.AddElement(lpCarAud);
    //                    //CellLogo5k.VerticalAlignment = Element.ALIGN_TOP;
    //                    //CellLogo5k.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    //TablaLogo.AddCell(CellLogo5k);
    //                    //loDocument.Add(TablaLogo);
    //                    //CellLogo1.Clear();
    //                    //CellLogo2.Clear();
    //                    //CellLogo3.Clear();
    //                    //TablaLogo.DeleteAllRows();

    //                    //Cell CellLogo5l = new Cell();
    //                    //CellLogo5l.Border = 0;
    //                    //CellLogo5l.BorderWidthLeft = 0;
    //                    //CellLogo5l.BorderWidthTop = 0;
    //                    //CellLogo5l.BorderWidthRight = 0;
    //                    //CellLogo5l.BorderWidthBottom = 0;
    //                    //CellLogo5l.Colspan = 3;
    //                    //CellLogo5l.AddElement(LogoAud);
    //                    //CellLogo5l.VerticalAlignment = Element.ALIGN_TOP;
    //                    //CellLogo5l.HorizontalAlignment = Element.ALIGN_CENTER;
    //                    //TablaLogo.AddCell(CellLogo5l);
    //                    //loDocument.Add(TablaLogo);
    //                    //CellLogo1.Clear();
    //                    //CellLogo2.Clear();
    //                    //CellLogo3.Clear();
    //                    //TablaLogo.DeleteAllRows();
    //                    loDocument.NewPage();
    //                }
    //                lLector.Close();
    //                lLector.Dispose();
    //            }
    //        }
    //        loDocument.Close();
    //        lConexion.Cerrar();
    //        return lsdirPDF;
    //    }
    //    catch (Exception ex)
    //    {
    //        lConexion.Cerrar();
    //        loDocument.Close();
    //        return "ERROR - Al Generar el PDF. " + ex.Message.ToString();
    //    }
    //}
    /// <param name="lsFechaRueda"></param>
    /// <param name="ldNumeroOperacionIn"></param>
    /// <param name="ldNumeroOperacionFi"></param>
    /// <param name="lsIndicador"></param>
    /// 20181210 rq046-18 firma digital
    public void generarCertFirma(string lsRuta, string lsIdReg, string lsCertificado, InfoSessionVO lsInfo)
    {
        clConexion lConexion = new clConexion(lsInfo);
        clConexion lConexion1 = new clConexion(lsInfo);
        SqlDataReader lLector;
        string[] lsNombreParametros = { "@P_codigo_verif" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        Object[] lValorParametros = { lsIdReg };

        int[] liAnchoLogo = { 7, 53, 40 };
        int[] liAnchoTitulo = { 30, 40, 30 };
        int[] liAnchoCabecera = { 7, 20, 18, 5, 20, 20, 10 };
        int[] liAnchoTitulo1 = { 7, 48, 45 };
        int[] liAnchoDatos1 = { 20, 1, 29, 20, 1, 29 };
        int[] liAnchoTitulo2 = { 7, 93 };
        int[] liAnchoDatos2 = { 30, 70 }; //20190717 rq050-19
        int[] liAnchoDatos2A = { 30, 30, 20, 20 };//20190717 rq050-19
        int[] liAnchoDatos3 = { 50, 50 };
        int[] liAnchoFinal = { 100 };

        Document loDocument = new Document(PageSize.LETTER, 50, 50, 25, 25);
        string lsdirLogo = "";
        string lsNombreArchivo = "";
        string lsNomFirma = "";
        string lsCarFirma = "";
        string lsArcFirma = "";
        //int liLineas = 0;
        try
        {
            // Se instancia la Conexion
            lConexion.Abrir();

            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
            if (lLector.HasRows)
            {
                lLector.Read();
                lsNomFirma = lLector["nombre_firma"].ToString();
                lsCarFirma = lLector["cargo_firma"].ToString();
                lsArcFirma = lLector["archivo_firma"].ToString();
            }
            lLector.Close();
            lLector.Dispose();

            // Definicion de los Fuentes
            iTextSharp.text.Font FuenteTitulo;
            iTextSharp.text.Font FuenteTitulo1;
            iTextSharp.text.Font FuenteDato;
            iTextSharp.text.Font FuenteDato1;
            iTextSharp.text.Font FuenteDato2;
            iTextSharp.text.Font FuenteDato3;
            iTextSharp.text.Font FuenteEspacio;
            iTextSharp.text.Font FuenteEspacioClaro;

            Color clClaro = new Color(135, 206, 250);
            Color clAzul = new Color(50, 84, 135);
            Color clBlanco = new Color(255, 255, 255);

            FuenteTitulo = FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD, clAzul);
            FuenteTitulo1 = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, clBlanco);
            FuenteDato = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, clAzul);
            FuenteDato1 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
            FuenteDato2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD);
            FuenteDato3 = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, clClaro);
            FuenteEspacio = FontFactory.GetFont(FontFactory.HELVETICA, 3, iTextSharp.text.Font.NORMAL);
            FuenteEspacioClaro = FontFactory.GetFont(FontFactory.HELVETICA, 3, iTextSharp.text.Font.NORMAL, clClaro);

            string lsArchivo = lsCertificado;
            lsNombreArchivo = lsRuta + lsArchivo;

            lsdirLogo = oRutaImg;
            iTextSharp.text.Image LogoGas = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\bnatop.gif");
            LogoGas.ScalePercent(30);

            iTextSharp.text.Image ArchivoFirma = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\" + lsArcFirma);
            ArchivoFirma.ScalePercent(40);

            iTextSharp.text.Image LogoBna = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\logo_bna1.GIF");
            LogoBna.ScalePercent(45);

            iTextSharp.text.Image RayaI = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\raya izquierda.png");
            RayaI.ScalePercent(60);

            iTextSharp.text.Image RayaD = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\raya derecha.png");
            RayaD.ScalePercent(60);

            //iTextSharp.text.Image LogoAud = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\LogoAudita.png");
            //LogoAud.ScalePercent(80);

            iTextSharp.text.Image LogoVig = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\logo vigilado ALTA RESOLUCION.tif");
            LogoVig.ScalePercent(15);


            // Creo el Documento PDF
            PdfWriter.GetInstance(loDocument, new FileStream(lsNombreArchivo, FileMode.Create));
            //// Asigno la visualizacion del Numero de Pagina
            //HeaderFooter numeracion_pag = new HeaderFooter(new Phrase("Página: "), true);
            //numeracion_pag.Border = 0;
            //numeracion_pag.Alignment = Element.ALIGN_RIGHT;
            //loDocument.Footer = numeracion_pag;
            loDocument.Open();

            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetCertificadoGasFirma", lsNombreParametros, lTipoparametros, lValorParametros);
            if (lLector.HasRows)
            {

                lLector.Read();

                //liLineas = lLector["mail_v"].ToString().Length;
                //if (liLineas < lLector["mail_c"].ToString().Length)
                //    liLineas = lLector["mail_c"].ToString().Length;

                //liLineas = liLineas / 33;
                //liLineas += 1;

                iTextSharp.text.Table TablaLogo = new iTextSharp.text.Table(3, liAnchoLogo);
                iTextSharp.text.Table TablaTitulo = new iTextSharp.text.Table(3, liAnchoTitulo);
                iTextSharp.text.Table TablaCabecera = new iTextSharp.text.Table(7, liAnchoCabecera);
                iTextSharp.text.Table TablaTitulo1 = new iTextSharp.text.Table(3, liAnchoTitulo1);
                iTextSharp.text.Table TablaTitulo2 = new iTextSharp.text.Table(2, liAnchoTitulo2);
                iTextSharp.text.Table TablaTitulo3 = new iTextSharp.text.Table(2, liAnchoTitulo2);
                iTextSharp.text.Table TablaDatos1 = new iTextSharp.text.Table(6, liAnchoDatos1);
                iTextSharp.text.Table TablaDatos2 = new iTextSharp.text.Table(2, liAnchoDatos2);
                iTextSharp.text.Table TablaDatos2A = new iTextSharp.text.Table(4, liAnchoDatos2A);  //20190717 rq050-19
                iTextSharp.text.Table TablaDatos3 = new iTextSharp.text.Table(2, liAnchoDatos3);
                iTextSharp.text.Table TablaTexto = new iTextSharp.text.Table(1, liAnchoFinal);
                iTextSharp.text.Table TablaFinal = new iTextSharp.text.Table(1, liAnchoFinal);

                TablaLogo.Alignment = Element.ALIGN_LEFT;
                TablaLogo.Offset = 0;
                TablaLogo.Spacing = 0;
                TablaLogo.WidthPercentage = 95;
                TablaLogo.Border = 0;

                TablaCabecera.Alignment = Element.ALIGN_LEFT;
                TablaCabecera.Offset = 0;
                TablaCabecera.Spacing = 0;
                TablaCabecera.WidthPercentage = 95;
                TablaCabecera.Border = 0;

                TablaTitulo.Alignment = Element.ALIGN_LEFT;
                TablaTitulo.Offset = 0;
                TablaTitulo.Spacing = 0;
                TablaTitulo.WidthPercentage = 95;
                TablaTitulo.Border = 0;

                TablaTitulo1.Alignment = Element.ALIGN_LEFT;
                TablaTitulo1.Offset = 0;
                TablaTitulo1.Spacing = 0;
                TablaTitulo1.Padding = 2;
                TablaTitulo1.WidthPercentage = 95;
                TablaTitulo1.Border = 0;

                TablaTitulo2.Alignment = Element.ALIGN_LEFT;
                TablaTitulo2.Offset = 0;
                TablaTitulo2.Spacing = 0;
                TablaTitulo2.Padding = 2;
                TablaTitulo2.WidthPercentage = 95;
                TablaTitulo2.Border = 0;

                TablaDatos1.Offset = 0;
                TablaDatos1.Spacing = 0;
                TablaDatos1.WidthPercentage = 95;
                TablaDatos1.Alignment = Element.ALIGN_LEFT;
                TablaDatos1.Border = 0;

                TablaDatos2.Offset = 0;
                TablaDatos2.Spacing = 0;
                TablaDatos2.WidthPercentage = 95;
                TablaDatos2.Alignment = Element.ALIGN_LEFT;
                TablaDatos2.Border = 0;

                //20190717 rq050-19
                TablaDatos2A.Offset = 0;
                TablaDatos2A.Spacing = 0;
                TablaDatos2A.WidthPercentage = 95;
                TablaDatos2A.Alignment = Element.ALIGN_LEFT;
                TablaDatos2A.Border = 0;
                //20190717 fin rq050-19

                TablaDatos3.Alignment = Element.ALIGN_LEFT;
                TablaDatos3.Offset = 0;
                TablaDatos3.Spacing = 0;
                TablaDatos3.WidthPercentage = 95;
                TablaDatos3.Border = 0;

                TablaTexto.Alignment = Element.ALIGN_LEFT;
                TablaTexto.Offset = 0;
                TablaTexto.Spacing = 0;
                TablaTexto.WidthPercentage = 95;
                TablaTexto.Border = 0;

                TablaFinal.Alignment = Element.ALIGN_LEFT;
                TablaFinal.Offset = 0;
                TablaFinal.Spacing = 0;
                TablaFinal.WidthPercentage = 95;
                TablaFinal.Border = 0;

                Paragraph lpEspacios = new Paragraph(" ", FuenteDato1);

                // Se Pinta la Cabecera del Certificado
                Cell CellLogo1 = new Cell();
                CellLogo1.Border = 0;
                CellLogo1.BorderWidthLeft = 0;
                CellLogo1.BorderWidthTop = 0;
                CellLogo1.BorderWidthRight = 0;
                CellLogo1.BorderWidthBottom = 0;
                CellLogo1.AddElement(LogoVig);
                CellLogo1.VerticalAlignment = Element.ALIGN_BOTTOM;
                CellLogo1.HorizontalAlignment = Element.ALIGN_RIGHT;
                TablaLogo.AddCell(CellLogo1);

                Cell CellLogo2 = new Cell();
                CellLogo2.Border = 0;
                CellLogo2.BorderWidthLeft = 0;
                CellLogo2.BorderWidthTop = 0;
                CellLogo2.BorderWidthRight = 0;
                CellLogo2.BorderWidthBottom = 0;
                CellLogo2.AddElement(LogoBna);
                CellLogo2.VerticalAlignment = Element.ALIGN_BOTTOM;
                CellLogo2.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaLogo.AddCell(CellLogo2);

                Cell CellLogo3 = new Cell();
                CellLogo3.Border = 0;
                CellLogo3.BorderWidthLeft = 0;
                CellLogo3.BorderWidthTop = 0;
                CellLogo3.BorderWidthRight = 0;
                CellLogo3.BorderWidthBottom = 0;
                CellLogo3.AddElement(LogoGas);
                CellLogo3.VerticalAlignment = Element.ALIGN_BOTTOM;
                CellLogo3.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaLogo.AddCell(CellLogo3);

                loDocument.Add(TablaLogo);


                Cell CellTit1 = new Cell();
                CellTit1.Border = 0;
                CellTit1.BorderWidthLeft = 0;
                CellTit1.BorderWidthTop = 0;
                CellTit1.BorderWidthRight = 0;
                CellTit1.BorderWidthBottom = 0;
                CellTit1.AddElement(RayaI);
                CellTit1.VerticalAlignment = Element.ALIGN_BOTTOM;
                CellTit1.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaTitulo.AddCell(CellTit1);

                Cell CellTit2 = new Cell();
                CellTit2.Border = 0;
                CellTit2.BorderWidthLeft = 0;
                CellTit2.BorderWidthTop = 0;
                CellTit2.BorderWidthRight = 0;
                CellTit2.BorderWidthBottom = 0;
                CellTit2.AddElement(new Paragraph("CERTIFICADO DE ASIGNACIÓN", FuenteTitulo));
                CellTit2.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellTit2.HorizontalAlignment = Element.ALIGN_CENTER;
                TablaTitulo.AddCell(CellTit2);

                Cell CellTit3 = new Cell();
                CellTit3.Border = 0;
                CellTit3.BorderWidthLeft = 0;
                CellTit3.BorderWidthTop = 0;
                CellTit3.BorderWidthRight = 0;
                CellTit3.BorderWidthBottom = 0;
                CellTit3.AddElement(RayaD);
                CellTit3.VerticalAlignment = Element.ALIGN_BOTTOM;
                CellTit3.HorizontalAlignment = Element.ALIGN_RIGHT;
                TablaTitulo.AddCell(CellTit3);

                loDocument.Add(TablaTitulo);
                loDocument.Add(lpEspacios);

                Cell CellBlanco = new Cell();
                CellBlanco.Border = 0;
                CellBlanco.BorderWidthLeft = 0;
                CellBlanco.BorderWidthTop = 0;
                CellBlanco.BorderWidthRight = 0;
                CellBlanco.BorderWidthBottom = 0;
                CellBlanco.AddElement(new Paragraph(" ", FuenteEspacio));
                CellBlanco.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellBlanco.HorizontalAlignment = Element.ALIGN_CENTER;

                Cell CellAzul = new Cell();
                CellAzul.Border = 0;
                CellAzul.BorderWidthLeft = 0;
                CellAzul.BorderWidthTop = 0;
                CellAzul.BorderWidthRight = 0;
                CellAzul.BorderWidthBottom = 0;
                CellAzul.BackgroundColor = clAzul;
                CellAzul.VerticalAlignment = Element.ALIGN_TOP;
                CellAzul.AddElement(new Paragraph(" ", FuenteEspacio));
                CellAzul.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellAzul.HorizontalAlignment = Element.ALIGN_CENTER;

                Cell CellClaro = new Cell();
                CellClaro.Border = 0;
                CellClaro.BorderWidthLeft = 0;
                CellClaro.BorderWidthTop = 0;
                CellClaro.BorderWidthRight = 0;
                CellClaro.BorderWidthBottom = 0;
                CellClaro.BackgroundColor = clClaro;
                CellClaro.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellClaro.AddElement(new Paragraph("X", FuenteEspacioClaro));
                CellClaro.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellClaro.HorizontalAlignment = Element.ALIGN_CENTER;


                TablaCabecera.AddCell(CellBlanco);

                Cell CellCab1 = new Cell();
                CellCab1.Border = 0;
                CellCab1.BorderWidthLeft = 0;
                CellCab1.BorderWidthTop = 0;
                CellCab1.BorderWidthRight = 0;
                CellCab1.BorderWidthBottom = 0;
                CellCab1.AddElement(new Paragraph("Fecha de Negociación:", FuenteDato));
                CellCab1.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellCab1.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaCabecera.AddCell(CellCab1);

                Cell CellCab2 = new Cell();
                CellCab2.BorderWidthLeft = 0;
                CellCab2.BorderWidthTop = 0;
                CellCab2.BorderWidthRight = 0;
                CellCab2.BorderWidthBottom = 0;
                CellCab2.BorderColorBottom = clAzul;
                CellCab2.AddElement(new Paragraph(" " + lLector["fecha_contrato"].ToString(), FuenteDato1));
                CellCab2.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellCab2.HorizontalAlignment = Element.ALIGN_CENTER;
                TablaCabecera.AddCell(CellCab2);

                TablaCabecera.AddCell(CellBlanco);

                Cell CellCab3 = new Cell();
                CellCab3.Border = 0;
                CellCab3.BorderWidthLeft = 0;
                CellCab3.BorderWidthTop = 0;
                CellCab3.BorderWidthRight = 0;
                CellCab3.BorderWidthBottom = 0;
                CellCab3.AddElement(new Paragraph("  N° de Operación:", FuenteDato));
                CellCab3.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellCab3.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaCabecera.AddCell(CellCab3);

                Cell CellCab4 = new Cell();
                CellCab4.Border = 0;
                CellCab4.BorderWidthLeft = 0;
                CellCab4.BorderWidthTop = 0;
                CellCab4.BorderWidthRight = 0;
                CellCab4.BorderWidthBottom = 0;
                CellCab4.BorderColorBottom = clAzul;
                CellCab4.AddElement(new Paragraph(" " + lLector["numero_contrato"].ToString(), FuenteDato1));
                CellCab4.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellCab4.HorizontalAlignment = Element.ALIGN_CENTER;
                TablaCabecera.AddCell(CellCab4);

                TablaCabecera.AddCell(CellBlanco);
                TablaCabecera.AddCell(CellBlanco);

                Cell CellCab5 = new Cell();
                CellCab5.Border = 0;
                CellCab5.BorderWidthLeft = 0;
                CellCab5.BorderWidthTop = 0;
                CellCab5.BorderWidthRight = 0;
                CellCab5.BorderWidthBottom = 0;
                CellCab5.AddElement(new Paragraph("Fecha Expedición:", FuenteDato));
                CellCab5.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellCab5.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaCabecera.AddCell(CellCab5);

                Cell CellCab6 = new Cell();
                CellCab6.Border = 0;
                CellCab6.BorderWidthLeft = 0;
                CellCab6.BorderWidthTop = 0;
                CellCab6.BorderWidthRight = 0;
                CellCab6.BorderWidthBottom = 0;
                CellCab4.BorderColorBottom = clAzul;
                CellCab6.AddElement(new Paragraph(" " + lLector["fecha_expedicion"].ToString(), FuenteDato1));
                CellCab6.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellCab6.HorizontalAlignment = Element.ALIGN_CENTER;
                TablaCabecera.AddCell(CellCab6);

                loDocument.Add(TablaCabecera);
                loDocument.Add(lpEspacios);

                TablaTitulo1.AddCell(CellAzul);

                Cell CellTitA1 = new Cell();
                CellTitA1.Border = 0;
                CellTitA1.BorderWidthLeft = 0;
                CellTitA1.BorderWidthTop = 0;
                CellTitA1.BorderWidthRight = 0;
                CellTitA1.BorderWidthBottom = 0;
                CellTitA1.BackgroundColor = clAzul;
                CellTitA1.AddElement(new Paragraph("Vendedor:", FuenteTitulo1));
                CellTitA1.VerticalAlignment = Element.ALIGN_TOP;
                CellTitA1.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaTitulo1.AddCell(CellTitA1);

                Cell CellTitA2 = new Cell();
                CellTitA2.Border = 0;
                CellTitA2.BorderWidthLeft = 0;
                CellTitA2.BorderWidthTop = 0;
                CellTitA2.BorderWidthRight = 0;
                CellTitA2.BorderWidthBottom = 0;
                CellTitA2.BackgroundColor = clAzul;
                CellTitA2.AddElement(new Paragraph("Comprador:", FuenteTitulo1));
                CellTitA2.VerticalAlignment = Element.ALIGN_TOP;
                CellTitA2.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaTitulo1.AddCell(CellTitA2);

                loDocument.Add(TablaTitulo1);
                loDocument.Add(lpEspacios);



                Cell CellDatoA1 = new Cell();
                CellDatoA1.Border = 0;
                CellDatoA1.BorderWidthLeft = 0;
                CellDatoA1.BorderWidthTop = 0;
                CellDatoA1.BorderWidthRight = 0;
                CellDatoA1.BorderWidthBottom = 0;
                CellDatoA1.BackgroundColor = clClaro;
                CellDatoA1.AddElement(new Paragraph(" Participante:", FuenteDato));
                CellDatoA1.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA1.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA1);

                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA2 = new Cell();
                CellDatoA2.Border = 0;
                CellDatoA2.BorderWidthLeft = 0;
                CellDatoA2.BorderWidthTop = 0;
                CellDatoA2.BorderWidthRight = 0;
                CellDatoA2.BorderWidthBottom = 0;
                CellDatoA2.AddElement(new Paragraph(lLector["nom_vendedor"].ToString(), FuenteDato1));
                CellDatoA2.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA2.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA2);

                TablaDatos1.AddCell(CellDatoA1);
                TablaDatos1.AddCell(CellBlanco);


                Cell CellDatoA4 = new Cell();
                CellDatoA4.Border = 0;
                CellDatoA4.BorderWidthLeft = 0;
                CellDatoA4.BorderWidthTop = 0;
                CellDatoA4.BorderWidthRight = 0;
                CellDatoA4.BorderWidthBottom = 0;
                CellDatoA4.AddElement(new Paragraph(lLector["nom_comprador"].ToString(), FuenteDato1));
                CellDatoA4.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA4.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA4);

                Cell CellDatoA5 = new Cell();
                CellDatoA5.Border = 0;
                CellDatoA5.BorderWidthLeft = 0;
                CellDatoA5.BorderWidthTop = 0;
                CellDatoA5.BorderWidthRight = 0;
                CellDatoA5.BorderWidthBottom = 0;
                CellDatoA5.BackgroundColor = clClaro;
                CellDatoA5.AddElement(new Paragraph(" Nit:", FuenteDato));
                CellDatoA5.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA5.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA5);

                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA6 = new Cell();
                CellDatoA6.Border = 0;
                CellDatoA6.BorderWidthLeft = 0;
                CellDatoA6.BorderWidthTop = 0;
                CellDatoA6.BorderWidthRight = 0;
                CellDatoA6.BorderWidthBottom = 0;
                CellDatoA6.AddElement(new Paragraph(lLector["nit_vendedor"].ToString(), FuenteDato1));
                CellDatoA6.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA6.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA6);

                TablaDatos1.AddCell(CellDatoA5);
                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA8 = new Cell();
                CellDatoA8.Border = 0;
                CellDatoA8.BorderWidthLeft = 0;
                CellDatoA8.BorderWidthTop = 0;
                CellDatoA8.BorderWidthRight = 0;
                CellDatoA8.BorderWidthBottom = 0;
                CellDatoA8.AddElement(new Paragraph(lLector["nit_comprador"].ToString(), FuenteDato1));
                CellDatoA8.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA8.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA8);

                Cell CellDatoA9 = new Cell();
                CellDatoA9.Border = 0;
                CellDatoA9.BorderWidthLeft = 0;
                CellDatoA9.BorderWidthTop = 0;
                CellDatoA9.BorderWidthRight = 0;
                CellDatoA9.BorderWidthBottom = 0;
                CellDatoA9.BackgroundColor = clClaro;
                CellDatoA9.AddElement(new Paragraph(" Tipo de participante:", FuenteDato));
                CellDatoA9.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA9.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA9);

                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA10 = new Cell();
                CellDatoA10.Border = 0;
                CellDatoA10.BorderWidthLeft = 0;
                CellDatoA10.BorderWidthTop = 0;
                CellDatoA10.BorderWidthRight = 0;
                CellDatoA10.BorderWidthBottom = 0;
                CellDatoA10.AddElement(new Paragraph(lLector["tipo_vendedor"].ToString(), FuenteDato1));
                CellDatoA10.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA10.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA10);

                TablaDatos1.AddCell(CellDatoA9);
                TablaDatos1.AddCell(CellBlanco);


                Cell CellDatoA11 = new Cell();
                CellDatoA11.Border = 0;
                CellDatoA11.BorderWidthLeft = 0;
                CellDatoA11.BorderWidthTop = 0;
                CellDatoA11.BorderWidthRight = 0;
                CellDatoA11.BorderWidthBottom = 0;
                CellDatoA11.AddElement(new Paragraph(lLector["tipo_comprador"].ToString(), FuenteDato1));
                CellDatoA11.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA11.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA11);

                Cell CellDatoA12 = new Cell();
                CellDatoA12.Border = 0;
                CellDatoA12.BorderWidthLeft = 0;
                CellDatoA12.BorderWidthTop = 0;
                CellDatoA12.BorderWidthRight = 0;
                CellDatoA12.BorderWidthBottom = 0;
                CellDatoA12.BackgroundColor = clClaro;
                CellDatoA12.AddElement(new Paragraph(" Domiciliado en:", FuenteDato));
                CellDatoA12.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA12.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA12);

                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA13 = new Cell();
                CellDatoA13.Border = 0;
                CellDatoA13.BorderWidthLeft = 0;
                CellDatoA13.BorderWidthTop = 0;
                CellDatoA13.BorderWidthRight = 0;
                CellDatoA13.BorderWidthBottom = 0;
                CellDatoA13.AddElement(new Paragraph(lLector["direccion_vendedor"].ToString(), FuenteDato1));
                CellDatoA13.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA13.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA13);

                TablaDatos1.AddCell(CellDatoA12);
                TablaDatos1.AddCell(CellBlanco);


                Cell CellDatoA15 = new Cell();
                CellDatoA15.Border = 0;
                CellDatoA15.BorderWidthLeft = 0;
                CellDatoA15.BorderWidthTop = 0;
                CellDatoA15.BorderWidthRight = 0;
                CellDatoA15.BorderWidthBottom = 0;
                CellDatoA15.AddElement(new Paragraph(lLector["direccion_comprador"].ToString(), FuenteDato1));
                CellDatoA15.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA15.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA15);

                Cell CellDatoA16 = new Cell();
                CellDatoA16.Border = 0;
                CellDatoA16.BorderWidthLeft = 0;
                CellDatoA16.BorderWidthTop = 0;
                CellDatoA16.BorderWidthRight = 0;
                CellDatoA16.BorderWidthBottom = 0;
                CellDatoA16.BackgroundColor = clClaro;
                CellDatoA16.AddElement(new Paragraph(" Mail:", FuenteDato));
                CellDatoA16.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA16.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA16);

                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA17 = new Cell();
                CellDatoA17.Border = 0;
                CellDatoA17.BorderWidthLeft = 0;
                CellDatoA17.BorderWidthTop = 0;
                CellDatoA17.BorderWidthRight = 0;
                CellDatoA17.BorderWidthBottom = 0;
                CellDatoA17.AddElement(new Paragraph(lLector["mail_v"].ToString(), FuenteDato1));
                CellDatoA17.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA17.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA17);

                TablaDatos1.AddCell(CellDatoA16);
                TablaDatos1.AddCell(CellBlanco);


                Cell CellDatoA19 = new Cell();
                CellDatoA19.Border = 0;
                CellDatoA19.BorderWidthLeft = 0;
                CellDatoA19.BorderWidthTop = 0;
                CellDatoA19.BorderWidthRight = 0;
                CellDatoA19.BorderWidthBottom = 0;
                CellDatoA19.AddElement(new Paragraph(lLector["mail_c"].ToString(), FuenteDato1));
                CellDatoA19.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA19.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA19);

                Cell CellDatoA20 = new Cell();
                CellDatoA20.Border = 0;
                CellDatoA20.BorderWidthLeft = 0;
                CellDatoA20.BorderWidthTop = 0;
                CellDatoA20.BorderWidthRight = 0;
                CellDatoA20.BorderWidthBottom = 0;
                CellDatoA20.BackgroundColor = clClaro;
                CellDatoA20.AddElement(new Paragraph(" Teléfono:", FuenteDato));
                CellDatoA20.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA20.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA20);

                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA21 = new Cell();
                CellDatoA21.Border = 0;
                CellDatoA21.BorderWidthLeft = 0;
                CellDatoA21.BorderWidthTop = 0;
                CellDatoA21.BorderWidthRight = 0;
                CellDatoA21.BorderWidthBottom = 0;
                CellDatoA21.AddElement(new Paragraph(lLector["telefono_v"].ToString(), FuenteDato1));
                CellDatoA21.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA21.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA21);

                TablaDatos1.AddCell(CellDatoA20);
                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA23 = new Cell();
                CellDatoA23.Border = 0;
                CellDatoA23.BorderWidthLeft = 0;
                CellDatoA23.BorderWidthTop = 0;
                CellDatoA23.BorderWidthRight = 0;
                CellDatoA23.BorderWidthBottom = 0;
                CellDatoA23.AddElement(new Paragraph(lLector["telefono_c"].ToString(), FuenteDato1));
                CellDatoA23.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA23.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA23);

                Cell CellDatoA24 = new Cell();
                CellDatoA24.Border = 0;
                CellDatoA24.BorderWidthLeft = 0;
                CellDatoA24.BorderWidthTop = 0;
                CellDatoA24.BorderWidthRight = 0;
                CellDatoA24.BorderWidthBottom = 0;
                CellDatoA24.BackgroundColor = clClaro;
                CellDatoA24.AddElement(new Paragraph(" Celular:", FuenteDato));
                CellDatoA24.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA24.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA24);

                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA25 = new Cell();
                CellDatoA25.Border = 0;
                CellDatoA25.BorderWidthLeft = 0;
                CellDatoA25.BorderWidthTop = 0;
                CellDatoA25.BorderWidthRight = 0;
                CellDatoA25.BorderWidthBottom = 0;
                CellDatoA25.AddElement(new Paragraph(lLector["celular_v"].ToString(), FuenteDato1));
                CellDatoA25.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA25.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA25);

                TablaDatos1.AddCell(CellDatoA24);
                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA27 = new Cell();
                CellDatoA27.Border = 0;
                CellDatoA27.BorderWidthLeft = 0;
                CellDatoA27.BorderWidthTop = 0;
                CellDatoA27.BorderWidthRight = 0;
                CellDatoA27.BorderWidthBottom = 0;
                CellDatoA27.AddElement(new Paragraph(lLector["celular_c"].ToString(), FuenteDato1));
                CellDatoA27.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA27.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA27);


                //20201207 subasta UVCP
                if (lLector["codigo_tipo_subasta"].ToString() == "2")
                {
                    Cell CellDatoB24 = new Cell();
                    CellDatoB24.Border = 0;
                    CellDatoB24.BorderWidthLeft = 0;
                    CellDatoB24.BorderWidthTop = 0;
                    CellDatoB24.BorderWidthRight = 0;
                    CellDatoB24.BorderWidthBottom = 0;
                    CellDatoB24.BackgroundColor = clClaro;
                    CellDatoB24.AddElement(new Paragraph(" Entidad Bancaria:", FuenteDato));
                    CellDatoB24.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB24.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos1.AddCell(CellDatoB24);

                    TablaDatos1.AddCell(CellBlanco);

                    Cell CellDatoB25 = new Cell();
                    CellDatoB25.Border = 0;
                    CellDatoB25.BorderWidthLeft = 0;
                    CellDatoB25.BorderWidthTop = 0;
                    CellDatoB25.BorderWidthRight = 0;
                    CellDatoB25.BorderWidthBottom = 0;
                    CellDatoB25.AddElement(new Paragraph(lLector["banco_pago"].ToString(), FuenteDato1));
                    CellDatoB25.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB25.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos1.AddCell(CellDatoB25);

                    TablaDatos1.AddCell(CellClaro);
                    TablaDatos1.AddCell(CellBlanco);
                    TablaDatos1.AddCell(CellBlanco);

                    Cell CellDatoC24 = new Cell();
                    CellDatoC24.Border = 0;
                    CellDatoC24.BorderWidthLeft = 0;
                    CellDatoC24.BorderWidthTop = 0;
                    CellDatoC24.BorderWidthRight = 0;
                    CellDatoC24.BorderWidthBottom = 0;
                    CellDatoC24.BackgroundColor = clClaro;
                    CellDatoC24.AddElement(new Paragraph(" Tipo de Cuenta:", FuenteDato));
                    CellDatoC24.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoC24.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos1.AddCell(CellDatoC24);

                    TablaDatos1.AddCell(CellBlanco);

                    Cell CellDatoC25 = new Cell();
                    CellDatoC25.Border = 0;
                    CellDatoC25.BorderWidthLeft = 0;
                    CellDatoC25.BorderWidthTop = 0;
                    CellDatoC25.BorderWidthRight = 0;
                    CellDatoC25.BorderWidthBottom = 0;
                    CellDatoC25.AddElement(new Paragraph(lLector["tipo_cuenta"].ToString(), FuenteDato1));
                    CellDatoC25.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoC25.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos1.AddCell(CellDatoC25);

                    TablaDatos1.AddCell(CellClaro);
                    TablaDatos1.AddCell(CellBlanco);
                    TablaDatos1.AddCell(CellBlanco);


                    Cell CellDatoD24 = new Cell();
                    CellDatoD24.Border = 0;
                    CellDatoD24.BorderWidthLeft = 0;
                    CellDatoD24.BorderWidthTop = 0;
                    CellDatoD24.BorderWidthRight = 0;
                    CellDatoD24.BorderWidthBottom = 0;
                    CellDatoD24.BackgroundColor = clClaro;
                    CellDatoD24.AddElement(new Paragraph(" No. de Cuenta:", FuenteDato));
                    CellDatoD24.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoD24.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos1.AddCell(CellDatoD24);

                    TablaDatos1.AddCell(CellBlanco);

                    Cell CellDatoD25 = new Cell();
                    CellDatoD25.Border = 0;
                    CellDatoD25.BorderWidthLeft = 0;
                    CellDatoD25.BorderWidthTop = 0;
                    CellDatoD25.BorderWidthRight = 0;
                    CellDatoD25.BorderWidthBottom = 0;
                    CellDatoD25.AddElement(new Paragraph(lLector["cuenta_pago"].ToString(), FuenteDato1));
                    CellDatoD25.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoD25.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos1.AddCell(CellDatoD25);

                    TablaDatos1.AddCell(CellClaro);
                    TablaDatos1.AddCell(CellBlanco);
                    TablaDatos1.AddCell(CellBlanco);
                }
                //20201207 fin subasta UVCP
                //espacio en blanco
                TablaDatos1.AddCell(CellClaro);
                TablaDatos1.AddCell(CellBlanco);
                TablaDatos1.AddCell(CellBlanco);
                TablaDatos1.AddCell(CellClaro);
                TablaDatos1.AddCell(CellBlanco);
                TablaDatos1.AddCell(CellBlanco);

                loDocument.Add(TablaDatos1);
                loDocument.Add(lpEspacios);

                TablaTitulo2.AddCell(CellAzul);

                Cell CellTitB1 = new Cell();
                CellTitB1.Border = 0;
                CellTitB1.BorderWidthLeft = 0;
                CellTitB1.BorderWidthTop = 0;
                CellTitB1.BorderWidthRight = 0;
                CellTitB1.BorderWidthBottom = 0;
                CellTitB1.BackgroundColor = clAzul;
                CellTitB1.AddElement(new Paragraph("Han negociado el producto que se detalla a continuación:", FuenteTitulo1));
                CellTitB1.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellTitB1.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaTitulo2.AddCell(CellTitB1);

                loDocument.Add(TablaTitulo2);
                loDocument.Add(lpEspacios);

                //20190717 rq050-19
                if (lLector["codigo_modalidad"].ToString() != "22")
                {
                    Cell CellDatoB1 = new Cell();
                    CellDatoB1.Border = 0;
                    CellDatoB1.BorderWidthLeft = 0;
                    CellDatoB1.BorderWidthTop = 0;
                    CellDatoB1.BorderWidthRight = 0;
                    CellDatoB1.BorderWidthBottom = 0;
                    CellDatoB1.BackgroundColor = clClaro;
                    CellDatoB1.AddElement(new Paragraph(" Producto:", FuenteDato));
                    CellDatoB1.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB1.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2.AddCell(CellDatoB1);

                    Cell CellDatoB2 = new Cell();
                    CellDatoB2.Border = 0;
                    CellDatoB2.BorderWidthLeft = 0;
                    CellDatoB2.BorderWidthTop = 0;
                    CellDatoB2.BorderWidthRight = 0;
                    CellDatoB2.BorderWidthBottom = 0;
                    CellDatoB2.AddElement(new Paragraph(" " + lLector["producto"].ToString(), FuenteDato1));
                    CellDatoB2.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB2.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2.AddCell(CellDatoB2);

                    Cell CellDatoB3 = new Cell();
                    CellDatoB3.Border = 0;
                    CellDatoB3.BorderWidthLeft = 0;
                    CellDatoB3.BorderWidthTop = 0;
                    CellDatoB3.BorderWidthRight = 0;
                    CellDatoB3.BorderWidthBottom = 0;
                    CellDatoB3.BackgroundColor = clClaro;
                    CellDatoB3.AddElement(new Paragraph(" Modalidad Contractual:", FuenteDato));
                    CellDatoB3.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB3.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2.AddCell(CellDatoB3);

                    Cell CellDatoB4 = new Cell();
                    CellDatoB4.Border = 0;
                    CellDatoB4.BorderWidthLeft = 0;
                    CellDatoB4.BorderWidthTop = 0;
                    CellDatoB4.BorderWidthRight = 0;
                    CellDatoB4.BorderWidthBottom = 0;
                    CellDatoB4.AddElement(new Paragraph(" " + lLector["modalidad"].ToString(), FuenteDato1));
                    CellDatoB4.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB4.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2.AddCell(CellDatoB4);


                    Cell CellDatoB5 = new Cell();
                    CellDatoB5.Border = 0;
                    CellDatoB5.BorderWidthLeft = 0;
                    CellDatoB5.BorderWidthTop = 0;
                    CellDatoB5.BorderWidthRight = 0;
                    CellDatoB5.BorderWidthBottom = 0;
                    CellDatoB5.BackgroundColor = clClaro;
                    CellDatoB5.AddElement(new Paragraph(" Ruta/Punto de entrega:", FuenteDato));
                    CellDatoB5.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB5.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2.AddCell(CellDatoB5);

                    Cell CellDatoB6 = new Cell();
                    CellDatoB6.Border = 0;
                    CellDatoB6.BorderWidthLeft = 0;
                    CellDatoB6.BorderWidthTop = 0;
                    CellDatoB6.BorderWidthRight = 0;
                    CellDatoB6.BorderWidthBottom = 0;
                    CellDatoB6.AddElement(new Paragraph(" " + lLector["punto_entrega"].ToString(), FuenteDato1));
                    CellDatoB6.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB6.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2.AddCell(CellDatoB6);

                    Cell CellDatoB7 = new Cell();
                    CellDatoB7.Border = 0;
                    CellDatoB7.BorderWidthLeft = 0;
                    CellDatoB7.BorderWidthTop = 0;
                    CellDatoB7.BorderWidthRight = 0;
                    CellDatoB7.BorderWidthBottom = 0;
                    CellDatoB7.BackgroundColor = clClaro;
                    CellDatoB7.AddElement(new Paragraph(" Duración:", FuenteDato));
                    CellDatoB7.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB7.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2.AddCell(CellDatoB7);

                    Cell CellDatoB8 = new Cell();
                    CellDatoB8.Border = 0;
                    CellDatoB8.BorderWidthLeft = 0;
                    CellDatoB8.BorderWidthTop = 0;
                    CellDatoB8.BorderWidthRight = 0;
                    CellDatoB8.BorderWidthBottom = 0;
                    CellDatoB8.AddElement(new Paragraph(" " + lLector["duracion"].ToString(), FuenteDato1));
                    CellDatoB8.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB8.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2.AddCell(CellDatoB8);

                    //20220920 ajsute certificado
                    Cell CellDatoB9 = new Cell();
                    CellDatoB9.Border = 0;
                    CellDatoB9.BorderWidthLeft = 0;
                    CellDatoB9.BorderWidthTop = 0;
                    CellDatoB9.BorderWidthRight = 0;
                    CellDatoB9.BorderWidthBottom = 0;
                    CellDatoB9.BackgroundColor = clClaro;
                    CellDatoB9.AddElement(new Paragraph(" Tipo de subasta:", FuenteDato));
                    CellDatoB9.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB9.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2.AddCell(CellDatoB9);

                    //20220920 ajsute certificado
                    Cell CellDatoB10 = new Cell();
                    CellDatoB10.Border = 0;
                    CellDatoB10.BorderWidthLeft = 0;
                    CellDatoB10.BorderWidthTop = 0;
                    CellDatoB10.BorderWidthRight = 0;
                    CellDatoB10.BorderWidthBottom = 0;
                    CellDatoB10.AddElement(new Paragraph(" " + lLector["desc_tipo_rueda"].ToString(), FuenteDato1));
                    CellDatoB10.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB10.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2.AddCell(CellDatoB10);

                    //espacio en blanco
                    TablaDatos2.AddCell(CellClaro);
                    TablaDatos2.AddCell(CellBlanco);

                    loDocument.Add(TablaDatos2);
                    loDocument.Add(lpEspacios);
                }
                else  //20190717 rq050-19
                {
                    Cell CellDatoB1 = new Cell();
                    CellDatoB1.Border = 0;
                    CellDatoB1.BorderWidthLeft = 0;
                    CellDatoB1.BorderWidthTop = 0;
                    CellDatoB1.BorderWidthRight = 0;
                    CellDatoB1.BorderWidthBottom = 0;
                    CellDatoB1.BackgroundColor = clClaro;
                    CellDatoB1.AddElement(new Paragraph(" Producto:", FuenteDato));
                    CellDatoB1.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB1.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB1);

                    Cell CellDatoB2 = new Cell();
                    CellDatoB2.Border = 0;
                    CellDatoB2.BorderWidthLeft = 0;
                    CellDatoB2.BorderWidthTop = 0;
                    CellDatoB2.BorderWidthRight = 0;
                    CellDatoB2.BorderWidthBottom = 0;
                    CellDatoB2.AddElement(new Paragraph(" " + lLector["producto"].ToString(), FuenteDato1));
                    CellDatoB2.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB2.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB2);

                    Cell CellDatoB1A = new Cell();
                    CellDatoB1A.Border = 0;
                    CellDatoB1A.BorderWidthLeft = 0;
                    CellDatoB1A.BorderWidthTop = 0;
                    CellDatoB1A.BorderWidthRight = 0;
                    CellDatoB1A.BorderWidthBottom = 0;
                    CellDatoB1A.BackgroundColor = clClaro;
                    CellDatoB1A.AddElement(new Paragraph(" Firmeza Mínima:", FuenteDato));
                    CellDatoB1A.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB1A.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB1A);

                    Cell CellDatoB2A = new Cell();
                    CellDatoB2A.Border = 0;
                    CellDatoB2A.BorderWidthLeft = 0;
                    CellDatoB2A.BorderWidthTop = 0;
                    CellDatoB2A.BorderWidthRight = 0;
                    CellDatoB2A.BorderWidthBottom = 0;
                    CellDatoB2A.AddElement(new Paragraph(" " + lLector["firmeza_minima"].ToString(), FuenteDato1));
                    CellDatoB2A.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB2A.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB2A);

                    Cell CellDatoB3 = new Cell();
                    CellDatoB3.Border = 0;
                    CellDatoB3.BorderWidthLeft = 0;
                    CellDatoB3.BorderWidthTop = 0;
                    CellDatoB3.BorderWidthRight = 0;
                    CellDatoB3.BorderWidthBottom = 0;
                    CellDatoB3.BackgroundColor = clClaro;
                    CellDatoB3.AddElement(new Paragraph(" Modalidad Contractual:", FuenteDato));
                    CellDatoB3.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB3.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB3);

                    Cell CellDatoB4 = new Cell();
                    CellDatoB4.Border = 0;
                    CellDatoB4.BorderWidthLeft = 0;
                    CellDatoB4.BorderWidthTop = 0;
                    CellDatoB4.BorderWidthRight = 0;
                    CellDatoB4.BorderWidthBottom = 0;
                    CellDatoB4.AddElement(new Paragraph(" " + lLector["modalidad"].ToString(), FuenteDato1));
                    CellDatoB4.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB4.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB4);

                    TablaDatos2A.AddCell(CellClaro);
                    TablaDatos2A.AddCell(CellBlanco);

                    Cell CellDatoB5 = new Cell();
                    CellDatoB5.Border = 0;
                    CellDatoB5.BorderWidthLeft = 0;
                    CellDatoB5.BorderWidthTop = 0;
                    CellDatoB5.BorderWidthRight = 0;
                    CellDatoB5.BorderWidthBottom = 0;
                    CellDatoB5.BackgroundColor = clClaro;
                    CellDatoB5.AddElement(new Paragraph(" Ruta/Punto de entrega:", FuenteDato));
                    CellDatoB5.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB5.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB5);

                    Cell CellDatoB6 = new Cell();
                    CellDatoB6.Border = 0;
                    CellDatoB6.BorderWidthLeft = 0;
                    CellDatoB6.BorderWidthTop = 0;
                    CellDatoB6.BorderWidthRight = 0;
                    CellDatoB6.BorderWidthBottom = 0;
                    CellDatoB6.AddElement(new Paragraph(" " + lLector["punto_entrega"].ToString(), FuenteDato1));
                    CellDatoB6.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB6.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB6);

                    TablaDatos2A.AddCell(CellClaro);
                    TablaDatos2A.AddCell(CellBlanco);

                    Cell CellDatoB7 = new Cell();
                    CellDatoB7.Border = 0;
                    CellDatoB7.BorderWidthLeft = 0;
                    CellDatoB7.BorderWidthTop = 0;
                    CellDatoB7.BorderWidthRight = 0;
                    CellDatoB7.BorderWidthBottom = 0;
                    CellDatoB7.BackgroundColor = clClaro;
                    CellDatoB7.AddElement(new Paragraph(" Duración:", FuenteDato));
                    CellDatoB7.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB7.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB7);

                    Cell CellDatoB8 = new Cell();
                    CellDatoB8.Border = 0;
                    CellDatoB8.BorderWidthLeft = 0;
                    CellDatoB8.BorderWidthTop = 0;
                    CellDatoB8.BorderWidthRight = 0;
                    CellDatoB8.BorderWidthBottom = 0;
                    CellDatoB8.AddElement(new Paragraph(" " + lLector["duracion"].ToString(), FuenteDato1));
                    CellDatoB8.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB8.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB8);

                    //20220920 ajuste certificado
                    Cell CellDatoB9 = new Cell();
                    CellDatoB9.Border = 0;
                    CellDatoB9.BorderWidthLeft = 0;
                    CellDatoB9.BorderWidthTop = 0;
                    CellDatoB9.BorderWidthRight = 0;
                    CellDatoB9.BorderWidthBottom = 0;
                    CellDatoB9.BackgroundColor = clClaro;
                    CellDatoB9.AddElement(new Paragraph(" Tipo de subasta:", FuenteDato));
                    CellDatoB9.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB9.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB9);

                    //20220920 ajuste certificado
                    Cell CellDatoB10 = new Cell();
                    CellDatoB10.Border = 0;
                    CellDatoB10.BorderWidthLeft = 0;
                    CellDatoB10.BorderWidthTop = 0;
                    CellDatoB10.BorderWidthRight = 0;
                    CellDatoB10.BorderWidthBottom = 0;
                    CellDatoB10.AddElement(new Paragraph(" " + lLector["desc_tipo_rueda"].ToString(), FuenteDato1));
                    CellDatoB10.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB10.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB10);

                    TablaDatos2A.AddCell(CellClaro);
                    TablaDatos2A.AddCell(CellBlanco);

                    //espacio en blanco
                    TablaDatos2A.AddCell(CellClaro);
                    TablaDatos2A.AddCell(CellBlanco);

                    TablaDatos2A.AddCell(CellClaro);
                    TablaDatos2A.AddCell(CellBlanco);

                    loDocument.Add(TablaDatos2A);
                    loDocument.Add(lpEspacios);
                }


                TablaTitulo2.DeleteAllRows();
                TablaTitulo2.AddCell(CellAzul);

                Cell CellTitC1 = new Cell();
                CellTitC1.Border = 0;
                CellTitC1.BorderWidthLeft = 0;
                CellTitC1.BorderWidthTop = 0;
                CellTitC1.BorderWidthRight = 0;
                CellTitC1.BorderWidthBottom = 0;
                CellTitC1.BackgroundColor = clAzul;
                CellTitC1.AddElement(new Paragraph("Con los siguientes resultados:", FuenteTitulo1));
                CellTitC1.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellTitC1.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaTitulo2.AddCell(CellTitC1);

                loDocument.Add(TablaTitulo2);

                Cell CellDatoC1 = new Cell();
                CellDatoC1.Border = 0;
                CellDatoC1.BorderWidthLeft = 0;
                CellDatoC1.BorderWidthTop = 0;
                CellDatoC1.BorderWidthRight = 0;
                CellDatoC1.BorderWidthBottom = 0;
                CellDatoC1.BackgroundColor = clClaro;
                CellDatoC1.AddElement(new Paragraph(" Cantidad/ Capacidad asignada:", FuenteDato));
                CellDatoC1.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoC1.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos3.AddCell(CellDatoC1);

                Cell CellDatoC2 = new Cell();
                CellDatoC2.Border = 0;
                CellDatoC2.BorderWidthLeft = 0;
                CellDatoC2.BorderWidthTop = 0;
                CellDatoC2.BorderWidthRight = 0;
                CellDatoC2.BorderWidthBottom = 0;
                CellDatoC2.AddElement(new Paragraph(" " + lLector["cantidad"].ToString(), FuenteDato1));
                CellDatoC2.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoC2.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos3.AddCell(CellDatoC2);

                Cell CellDatoC3 = new Cell();
                CellDatoC3.Border = 0;
                CellDatoC3.BorderWidthLeft = 0;
                CellDatoC3.BorderWidthTop = 0;
                CellDatoC3.BorderWidthRight = 0;
                CellDatoC3.BorderWidthBottom = 0;
                CellDatoC3.BackgroundColor = clClaro;
                CellDatoC3.AddElement(new Paragraph(" Unidad:", FuenteDato));
                CellDatoC3.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoC3.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos3.AddCell(CellDatoC3);

                Cell CellDatoC4 = new Cell();
                CellDatoC4.Border = 0;
                CellDatoC4.BorderWidthLeft = 0;
                CellDatoC4.BorderWidthTop = 0;
                CellDatoC4.BorderWidthRight = 0;
                CellDatoC4.BorderWidthBottom = 0;
                CellDatoC4.AddElement(new Paragraph(" " + lLector["unidad_medida"].ToString(), FuenteDato1));
                CellDatoC4.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoC4.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos3.AddCell(CellDatoC4);

                Cell CellDatoC5 = new Cell();
                CellDatoC5.Border = 0;
                CellDatoC5.BorderWidthLeft = 0;
                CellDatoC5.BorderWidthTop = 0;
                CellDatoC5.BorderWidthRight = 0;
                CellDatoC5.BorderWidthBottom = 0;
                CellDatoC5.BackgroundColor = clClaro;
                CellDatoC5.AddElement(new Paragraph(" Precio de adjudicación (US$/MBTU)(Moneda Vigente/KPC):", FuenteDato)); //20220828
                CellDatoC5.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoC5.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos3.AddCell(CellDatoC5);

                Cell CellDatoC6 = new Cell();
                CellDatoC6.Border = 0;
                CellDatoC6.BorderWidthLeft = 0;
                CellDatoC6.BorderWidthTop = 0;
                CellDatoC6.BorderWidthRight = 0;
                CellDatoC6.BorderWidthBottom = 0;
                CellDatoC6.AddElement(new Paragraph(lLector["precio"].ToString(), FuenteDato1)); //20220828
                CellDatoC6.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoC6.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos3.AddCell(CellDatoC6);

                Cell CellDatoC7 = new Cell();
                CellDatoC7.Border = 0;
                CellDatoC7.BorderWidthLeft = 0;
                CellDatoC7.BorderWidthTop = 0;
                CellDatoC7.BorderWidthRight = 0;
                CellDatoC7.BorderWidthBottom = 0;
                CellDatoC7.BackgroundColor = clClaro;
                CellDatoC7.AddElement(new Paragraph(" Valor del Contrato (Moneda vigente):", FuenteDato)); //20220828
                CellDatoC7.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoC7.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos3.AddCell(CellDatoC7);

                Cell CellDatoC8 = new Cell();
                CellDatoC8.Border = 0;
                CellDatoC8.BorderWidthLeft = 0;
                CellDatoC8.BorderWidthTop = 0;
                CellDatoC8.BorderWidthRight = 0;
                CellDatoC8.BorderWidthBottom = 0;
                CellDatoC8.AddElement(new Paragraph(Convert.ToDecimal(lLector["valor"].ToString()).ToString("###,###,###,###,###,###.00"), FuenteDato2));//20220828
                CellDatoC8.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoC8.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos3.AddCell(CellDatoC8);

                //espacio en blanco
                TablaDatos3.AddCell(CellClaro);
                TablaDatos3.AddCell(CellBlanco);

                loDocument.Add(TablaDatos3);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);


                Paragraph lpDetNota = new Paragraph("NOTA: El vendedor y el respectivo comprador serán responsables de suscribir el contrato de compraventa en donde se deberá reflejar los resultados la presente subasta o negociación.  De igual forma, coordinarán los aspectos operativos requeridos, tales como el proceso de nominación, conforme a la regulación vigente.", FuenteDato2);

                //TablaTexto.AddCell(CellBlanco); 

                Cell CellLogo5g = new Cell();
                CellLogo5g.Border = 0;
                CellLogo5g.BorderWidthLeft = 0;
                CellLogo5g.BorderWidthTop = 0;
                CellLogo5g.BorderWidthRight = 0;
                CellLogo5g.BorderWidthBottom = 0;
                CellLogo5g.AddElement(lpDetNota);
                CellLogo5g.VerticalAlignment = Element.ALIGN_TOP;
                CellLogo5g.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                TablaTexto.AddCell(CellLogo5g);

                loDocument.Add(TablaTexto);
                //while (liLineas - 4 >0)
                //{
                //    loDocument.Add(lpEspacios);
                //    liLineas = liLineas-1;
                //}

                //loDocument.Add(lpEspacios);

                TablaTexto.DeleteAllRows();

                //20201207
                int lAlinea = Element.ALIGN_CENTER;
                if (lLector["codigo_tipo_subasta"].ToString() == "2")
                    lAlinea = Element.ALIGN_LEFT;
                // fin 20201207
                Paragraph lpDetExp = new Paragraph("Expedido por:", FuenteDato1);

                //TablaTexto.AddCell(CellBlanco);

                Cell CellLogo5h = new Cell();
                CellLogo5h.Border = 0;
                CellLogo5h.BorderWidthLeft = 0;
                CellLogo5h.BorderWidthTop = 0;
                CellLogo5h.BorderWidthRight = 0;
                CellLogo5h.BorderWidthBottom = 0;
                CellLogo5h.AddElement(lpDetExp);
                CellLogo5h.VerticalAlignment = Element.ALIGN_TOP;
                CellLogo5h.HorizontalAlignment = lAlinea; //20201207
                TablaTexto.AddCell(CellLogo5h);

                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(TablaTexto);
                TablaTexto.DeleteAllRows();

                //TablaTexto.AddCell(CellBlanco);

                Cell CellLogo5i = new Cell();
                CellLogo5i.Border = 0;
                CellLogo5i.BorderWidthLeft = 0;
                CellLogo5i.BorderWidthTop = 0;
                CellLogo5i.BorderWidthRight = 0;
                CellLogo5i.BorderWidthBottom = 0;
                CellLogo5i.AddElement(ArchivoFirma);
                CellLogo5i.VerticalAlignment = Element.ALIGN_TOP;
                CellLogo5i.HorizontalAlignment = lAlinea; //20201207
                TablaTexto.AddCell(CellLogo5i);

                loDocument.Add(TablaTexto);
                TablaTexto.DeleteAllRows();


                Paragraph lpRaya = new Paragraph("___________________________", FuenteDato1); //20201207

                //TablaTexto.AddCell(CellBlanco);

                Cell CellLogo5d = new Cell();
                CellLogo5d.Border = 0;
                CellLogo5d.BorderWidthLeft = 0;
                CellLogo5d.BorderWidthTop = 0;
                CellLogo5d.BorderWidthRight = 0;
                CellLogo5d.BorderWidthBottom = 0;
                CellLogo5d.AddElement(lpRaya);
                CellLogo5d.VerticalAlignment = Element.ALIGN_TOP;
                CellLogo5d.HorizontalAlignment = lAlinea; //20201207
                TablaTexto.AddCell(CellLogo5d);

                //TablaTexto.AddCell(CellBlanco);

                Paragraph lpNomFir = new Paragraph(lsNomFirma, FuenteDato1);
                Paragraph lpCarFir = new Paragraph(lsCarFirma, FuenteDato1);
                Cell CellLogo5t = new Cell();
                CellLogo5t.Border = 0;
                CellLogo5t.BorderWidthLeft = 0;
                CellLogo5t.BorderWidthTop = 0;
                CellLogo5t.BorderWidthRight = 0;
                CellLogo5t.BorderWidthBottom = 0;
                CellLogo5t.AddElement(lpNomFir);
                CellLogo5t.AddElement(lpCarFir);
                CellLogo5t.AddElement(lpEspacios);
                CellLogo5t.VerticalAlignment = Element.ALIGN_TOP;
                CellLogo5t.HorizontalAlignment = lAlinea; //20201207
                TablaTexto.AddCell(CellLogo5t);

                loDocument.Add(TablaTexto);
                //loDocument.Add(lpEspacios);
                //loDocument.Add(lpEspacios);
                //loDocument.Add(lpEspacios);
                //loDocument.Add(lpEspacios);
                //loDocument.Add(lpEspacios);
                //loDocument.Add(lpEspacios);
                //loDocument.Add(lpEspacios);
                //loDocument.Add(lpEspacios);
                //loDocument.Add(lpEspacios);
                //loDocument.Add(lpEspacios);
                //TablaFinal.AddCell(CellAzul);

                //loDocument.Add(TablaFinal);
                loDocument.NewPage();
            }
            lLector.Close();
            lLector.Dispose();
            loDocument.Close();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lConexion.Cerrar();
            loDocument.Close();
        }
    }
    /// <summary>
    /// Nombre: generarCertProv
    /// Fecha: Abril 12 de 2019
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar el PDF de los Certificados Provisionales de Gas
    /// Modificacion:
    /// </summary>
    /// <param name="lsCadena"></param>
    public string generarCertProv(string lsCadena, InfoSessionVO lsInfo)
    {
        clConexion lConexion = new clConexion(lsInfo);
        clConexion lConexion1 = new clConexion(lsInfo);
        SqlDataReader lLector;
        string[] lsNombreParametros = { "@P_numero_contrato" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        Object[] lValorParametros = { 0 };

        int[] liAnchoLogo = { 7, 53, 40 };
        int[] liAnchoTitulo = { 20, 60, 20 };
        int[] liAnchoCabecera = { 7, 20, 18, 5, 20, 20, 10 };
        int[] liAnchoTitulo1 = { 7, 48, 45 };
        int[] liAnchoDatos1 = { 20, 1, 29, 20, 1, 29 };
        int[] liAnchoTitulo2 = { 7, 93 };
        int[] liAnchoDatos2 = { 30, 70 }; //20190717 rq050-19
        int[] liAnchoDatos2A = { 30, 30, 20, 20 };//20190717 rq050-19
        int[] liAnchoDatos3 = { 50, 50 };
        int[] liAnchoFinal = { 100 };

        Document loDocument = new Document(PageSize.LETTER, 50, 50, 25, 25);
        string lsdirPDF = "ARCHIVO -";
        string[] lsContrato;
        lsContrato = lsCadena.Split(',');
        string lsdirLogo = "";
        string lsNombreArchivo = "";
        int liCont = 0;
        try
        {
            // Se instancia la Conexion
            lConexion.Abrir();

            // Definicion de los Fuentes
            iTextSharp.text.Font FuenteTitulo;
            iTextSharp.text.Font FuenteTitulo1;
            iTextSharp.text.Font FuenteDato;
            iTextSharp.text.Font FuenteDato1;
            iTextSharp.text.Font FuenteDato2;
            iTextSharp.text.Font FuenteDato3;
            iTextSharp.text.Font FuenteEspacio;
            iTextSharp.text.Font FuenteEspacioClaro;

            Color clClaro = new Color(135, 206, 250);
            Color clAzul = new Color(50, 84, 135);
            Color clBlanco = new Color(255, 255, 255);

            FuenteTitulo = FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD, clAzul);
            FuenteTitulo1 = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, clBlanco);
            FuenteDato = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, clAzul);
            FuenteDato1 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
            FuenteDato2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD);
            FuenteDato3 = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, clClaro);
            FuenteEspacio = FontFactory.GetFont(FontFactory.HELVETICA, 3, iTextSharp.text.Font.NORMAL);
            FuenteEspacioClaro = FontFactory.GetFont(FontFactory.HELVETICA, 3, iTextSharp.text.Font.NORMAL, clClaro);

            string lsArchivo = DateTime.Now.Millisecond.ToString() + "certificaGas" + lsInfo.Usuario.ToString() + ".pdf";
            lsNombreArchivo = oRutaPdf + "\\" + lsArchivo;
            lsdirPDF += "../PDF/" + lsArchivo;

            lsdirLogo = oRutaImg;
            iTextSharp.text.Image LogoGas = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\bnatop.gif");
            LogoGas.ScalePercent(30);

            iTextSharp.text.Image LogoBna = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\logo_bna1.GIF");
            LogoBna.ScalePercent(45);

            iTextSharp.text.Image RayaI = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\raya izquierda.png");
            RayaI.ScalePercent(50);

            iTextSharp.text.Image RayaD = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\raya derecha.png");
            RayaD.ScalePercent(50);

            iTextSharp.text.Image LogoVig = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\logo vigilado ALTA RESOLUCION.tif");
            LogoVig.ScalePercent(15);


            // Creo el Documento PDF
            PdfWriter.GetInstance(loDocument, new FileStream(lsNombreArchivo, FileMode.Create));
            loDocument.Open();
            foreach (string lsCont in lsContrato)
            {
                if (lsCont != "")
                {
                    try
                    {
                        liCont = Convert.ToInt32(lsCont);
                    }
                    catch (Exception)
                    {
                        break;
                    }
                    lValorParametros[0] = lsCont;

                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetCertificadoGasProv", lsNombreParametros, lTipoparametros, lValorParametros);
                    if (lLector.HasRows)
                    {

                        lLector.Read();
                        iTextSharp.text.Table TablaLogo = new iTextSharp.text.Table(3, liAnchoLogo);
                        iTextSharp.text.Table TablaTitulo = new iTextSharp.text.Table(3, liAnchoTitulo);
                        iTextSharp.text.Table TablaCabecera = new iTextSharp.text.Table(7, liAnchoCabecera);
                        iTextSharp.text.Table TablaTitulo1 = new iTextSharp.text.Table(3, liAnchoTitulo1);
                        iTextSharp.text.Table TablaTitulo2 = new iTextSharp.text.Table(2, liAnchoTitulo2);
                        iTextSharp.text.Table TablaTitulo3 = new iTextSharp.text.Table(2, liAnchoTitulo2);
                        iTextSharp.text.Table TablaDatos1 = new iTextSharp.text.Table(6, liAnchoDatos1);
                        iTextSharp.text.Table TablaDatos2 = new iTextSharp.text.Table(2, liAnchoDatos2);
                        iTextSharp.text.Table TablaDatos2A = new iTextSharp.text.Table(4, liAnchoDatos2A);  //20190717 rq050-19
                        iTextSharp.text.Table TablaDatos3 = new iTextSharp.text.Table(2, liAnchoDatos3);
                        iTextSharp.text.Table TablaTexto = new iTextSharp.text.Table(1, liAnchoFinal);
                        iTextSharp.text.Table TablaFinal = new iTextSharp.text.Table(1, liAnchoFinal);

                        TablaLogo.Alignment = Element.ALIGN_LEFT;
                        TablaLogo.Offset = 0;
                        TablaLogo.Spacing = 0;
                        TablaLogo.WidthPercentage = 95;
                        TablaLogo.Border = 0;

                        TablaCabecera.Alignment = Element.ALIGN_LEFT;
                        TablaCabecera.Offset = 0;
                        TablaCabecera.Spacing = 0;
                        TablaCabecera.WidthPercentage = 95;
                        TablaCabecera.Border = 0;

                        TablaTitulo.Alignment = Element.ALIGN_LEFT;
                        TablaTitulo.Offset = 0;
                        TablaTitulo.Spacing = 0;
                        TablaTitulo.WidthPercentage = 95;
                        TablaTitulo.Border = 0;

                        TablaTitulo1.Alignment = Element.ALIGN_LEFT;
                        TablaTitulo1.Offset = 0;
                        TablaTitulo1.Spacing = 0;
                        TablaTitulo1.Padding = 2;
                        TablaTitulo1.WidthPercentage = 95;
                        TablaTitulo1.Border = 0;

                        TablaTitulo2.Alignment = Element.ALIGN_LEFT;
                        TablaTitulo2.Offset = 0;
                        TablaTitulo2.Spacing = 0;
                        TablaTitulo2.Padding = 2;
                        TablaTitulo2.WidthPercentage = 95;
                        TablaTitulo2.Border = 0;

                        TablaDatos1.Offset = 0;
                        TablaDatos1.Spacing = 0;
                        TablaDatos1.WidthPercentage = 95;
                        TablaDatos1.Alignment = Element.ALIGN_LEFT;
                        TablaDatos1.Border = 0;

                        TablaDatos2.Offset = 0;
                        TablaDatos2.Spacing = 0;
                        TablaDatos2.WidthPercentage = 95;
                        TablaDatos2.Alignment = Element.ALIGN_LEFT;
                        TablaDatos2.Border = 0;

                        //20190717 rq050-19
                        TablaDatos2A.Offset = 0;
                        TablaDatos2A.Spacing = 0;
                        TablaDatos2A.WidthPercentage = 95;
                        TablaDatos2A.Alignment = Element.ALIGN_LEFT;
                        TablaDatos2A.Border = 0;
                        //20190717 fin rq050-19

                        TablaDatos3.Alignment = Element.ALIGN_LEFT;
                        TablaDatos3.Offset = 0;
                        TablaDatos3.Spacing = 0;
                        TablaDatos3.WidthPercentage = 95;
                        TablaDatos3.Border = 0;

                        TablaTexto.Alignment = Element.ALIGN_LEFT;
                        TablaTexto.Offset = 0;
                        TablaTexto.Spacing = 0;
                        TablaTexto.WidthPercentage = 95;
                        TablaTexto.Border = 0;

                        TablaFinal.Alignment = Element.ALIGN_LEFT;
                        TablaFinal.Offset = 0;
                        TablaFinal.Spacing = 0;
                        TablaFinal.WidthPercentage = 95;
                        TablaFinal.Border = 0;

                        Paragraph lpEspacios = new Paragraph(" ", FuenteDato1);

                        // Se Pinta la Cabecera del Certificado
                        Cell CellLogo1 = new Cell();
                        CellLogo1.Border = 0;
                        CellLogo1.BorderWidthLeft = 0;
                        CellLogo1.BorderWidthTop = 0;
                        CellLogo1.BorderWidthRight = 0;
                        CellLogo1.BorderWidthBottom = 0;
                        CellLogo1.AddElement(LogoVig);
                        CellLogo1.VerticalAlignment = Element.ALIGN_BOTTOM;
                        CellLogo1.HorizontalAlignment = Element.ALIGN_RIGHT;
                        TablaLogo.AddCell(CellLogo1);

                        Cell CellLogo2 = new Cell();
                        CellLogo2.Border = 0;
                        CellLogo2.BorderWidthLeft = 0;
                        CellLogo2.BorderWidthTop = 0;
                        CellLogo2.BorderWidthRight = 0;
                        CellLogo2.BorderWidthBottom = 0;
                        CellLogo2.AddElement(LogoBna);
                        CellLogo2.VerticalAlignment = Element.ALIGN_BOTTOM;
                        CellLogo2.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaLogo.AddCell(CellLogo2);

                        Cell CellLogo3 = new Cell();
                        CellLogo3.Border = 0;
                        CellLogo3.BorderWidthLeft = 0;
                        CellLogo3.BorderWidthTop = 0;
                        CellLogo3.BorderWidthRight = 0;
                        CellLogo3.BorderWidthBottom = 0;
                        CellLogo3.AddElement(LogoGas);
                        CellLogo3.VerticalAlignment = Element.ALIGN_BOTTOM;
                        CellLogo3.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaLogo.AddCell(CellLogo3);

                        loDocument.Add(TablaLogo);


                        Cell CellTit1 = new Cell();
                        CellTit1.Border = 0;
                        CellTit1.BorderWidthLeft = 0;
                        CellTit1.BorderWidthTop = 0;
                        CellTit1.BorderWidthRight = 0;
                        CellTit1.BorderWidthBottom = 0;
                        CellTit1.AddElement(RayaI);
                        CellTit1.VerticalAlignment = Element.ALIGN_BOTTOM;
                        CellTit1.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaTitulo.AddCell(CellTit1);

                        Cell CellTit2 = new Cell();
                        CellTit2.Border = 0;
                        CellTit2.BorderWidthLeft = 0;
                        CellTit2.BorderWidthTop = 0;
                        CellTit2.BorderWidthRight = 0;
                        CellTit2.BorderWidthBottom = 0;
                        CellTit2.AddElement(new Paragraph("* INFORMACIÓN PRELIMINAR DE ASIGNACIÓN", FuenteTitulo));
                        CellTit2.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellTit2.HorizontalAlignment = Element.ALIGN_CENTER;
                        TablaTitulo.AddCell(CellTit2);

                        Cell CellTit3 = new Cell();
                        CellTit3.Border = 0;
                        CellTit3.BorderWidthLeft = 0;
                        CellTit3.BorderWidthTop = 0;
                        CellTit3.BorderWidthRight = 0;
                        CellTit3.BorderWidthBottom = 0;
                        CellTit3.AddElement(RayaD);
                        CellTit3.VerticalAlignment = Element.ALIGN_BOTTOM;
                        CellTit3.HorizontalAlignment = Element.ALIGN_RIGHT;
                        TablaTitulo.AddCell(CellTit3);

                        loDocument.Add(TablaTitulo);
                        loDocument.Add(lpEspacios);

                        Cell CellBlanco = new Cell();
                        CellBlanco.Border = 0;
                        CellBlanco.BorderWidthLeft = 0;
                        CellBlanco.BorderWidthTop = 0;
                        CellBlanco.BorderWidthRight = 0;
                        CellBlanco.BorderWidthBottom = 0;
                        CellBlanco.AddElement(new Paragraph(" ", FuenteEspacio));
                        CellBlanco.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellBlanco.HorizontalAlignment = Element.ALIGN_CENTER;

                        Cell CellAzul = new Cell();
                        CellAzul.Border = 0;
                        CellAzul.BorderWidthLeft = 0;
                        CellAzul.BorderWidthTop = 0;
                        CellAzul.BorderWidthRight = 0;
                        CellAzul.BorderWidthBottom = 0;
                        CellAzul.BackgroundColor = clAzul;
                        CellAzul.VerticalAlignment = Element.ALIGN_TOP;
                        CellAzul.AddElement(new Paragraph(" ", FuenteEspacio));
                        CellAzul.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellAzul.HorizontalAlignment = Element.ALIGN_CENTER;

                        Cell CellClaro = new Cell();
                        CellClaro.Border = 0;
                        CellClaro.BorderWidthLeft = 0;
                        CellClaro.BorderWidthTop = 0;
                        CellClaro.BorderWidthRight = 0;
                        CellClaro.BorderWidthBottom = 0;
                        CellClaro.BackgroundColor = clClaro;
                        CellClaro.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellClaro.AddElement(new Paragraph("X", FuenteEspacioClaro));
                        CellClaro.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellClaro.HorizontalAlignment = Element.ALIGN_CENTER;

                        TablaCabecera.AddCell(CellBlanco);

                        Cell CellCab1 = new Cell();
                        CellCab1.Border = 0;
                        CellCab1.BorderWidthLeft = 0;
                        CellCab1.BorderWidthTop = 0;
                        CellCab1.BorderWidthRight = 0;
                        CellCab1.BorderWidthBottom = 0;
                        CellCab1.AddElement(new Paragraph("Fecha de Negociación:", FuenteDato));
                        CellCab1.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellCab1.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaCabecera.AddCell(CellCab1);

                        Cell CellCab2 = new Cell();
                        CellCab2.BorderWidthLeft = 0;
                        CellCab2.BorderWidthTop = 0;
                        CellCab2.BorderWidthRight = 0;
                        CellCab2.BorderWidthBottom = 0;
                        CellCab2.BorderColorBottom = clAzul;
                        CellCab2.AddElement(new Paragraph(" " + lLector["fecha_contrato"].ToString(), FuenteDato1));
                        CellCab2.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellCab2.HorizontalAlignment = Element.ALIGN_CENTER;
                        TablaCabecera.AddCell(CellCab2);

                        TablaCabecera.AddCell(CellBlanco);

                        Cell CellCab3 = new Cell();
                        CellCab3.Border = 0;
                        CellCab3.BorderWidthLeft = 0;
                        CellCab3.BorderWidthTop = 0;
                        CellCab3.BorderWidthRight = 0;
                        CellCab3.BorderWidthBottom = 0;
                        CellCab3.AddElement(new Paragraph("  N° de Operación:", FuenteDato));
                        CellCab3.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellCab3.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaCabecera.AddCell(CellCab3);

                        Cell CellCab4 = new Cell();
                        CellCab4.Border = 0;
                        CellCab4.BorderWidthLeft = 0;
                        CellCab4.BorderWidthTop = 0;
                        CellCab4.BorderWidthRight = 0;
                        CellCab4.BorderWidthBottom = 0;
                        CellCab4.BorderColorBottom = clAzul;
                        CellCab4.AddElement(new Paragraph(" " + lLector["numero_contrato"].ToString(), FuenteDato1));
                        CellCab4.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellCab4.HorizontalAlignment = Element.ALIGN_CENTER;
                        TablaCabecera.AddCell(CellCab4);

                        TablaCabecera.AddCell(CellBlanco);
                        TablaCabecera.AddCell(CellBlanco);

                        Cell CellCab5 = new Cell();
                        CellCab5.Border = 0;
                        CellCab5.BorderWidthLeft = 0;
                        CellCab5.BorderWidthTop = 0;
                        CellCab5.BorderWidthRight = 0;
                        CellCab5.BorderWidthBottom = 0;
                        CellCab5.AddElement(new Paragraph("Fecha Expedición:", FuenteDato));
                        CellCab5.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellCab5.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaCabecera.AddCell(CellCab5);

                        Cell CellCab6 = new Cell();
                        CellCab6.Border = 0;
                        CellCab6.BorderWidthLeft = 0;
                        CellCab6.BorderWidthTop = 0;
                        CellCab6.BorderWidthRight = 0;
                        CellCab6.BorderWidthBottom = 0;
                        CellCab4.BorderColorBottom = clAzul;
                        CellCab6.AddElement(new Paragraph(" " + lLector["fecha_expedicion"].ToString(), FuenteDato1));
                        CellCab6.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellCab6.HorizontalAlignment = Element.ALIGN_CENTER;
                        TablaCabecera.AddCell(CellCab6);

                        loDocument.Add(TablaCabecera);
                        loDocument.Add(lpEspacios);

                        TablaTitulo1.AddCell(CellAzul);

                        Cell CellTitA1 = new Cell();
                        CellTitA1.Border = 0;
                        CellTitA1.BorderWidthLeft = 0;
                        CellTitA1.BorderWidthTop = 0;
                        CellTitA1.BorderWidthRight = 0;
                        CellTitA1.BorderWidthBottom = 0;
                        CellTitA1.BackgroundColor = clAzul;
                        CellTitA1.AddElement(new Paragraph("Vendedor:", FuenteTitulo1));
                        CellTitA1.VerticalAlignment = Element.ALIGN_TOP;
                        CellTitA1.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaTitulo1.AddCell(CellTitA1);

                        Cell CellTitA2 = new Cell();
                        CellTitA2.Border = 0;
                        CellTitA2.BorderWidthLeft = 0;
                        CellTitA2.BorderWidthTop = 0;
                        CellTitA2.BorderWidthRight = 0;
                        CellTitA2.BorderWidthBottom = 0;
                        CellTitA2.BackgroundColor = clAzul;
                        CellTitA2.AddElement(new Paragraph("Comprador:", FuenteTitulo1));
                        CellTitA2.VerticalAlignment = Element.ALIGN_TOP;
                        CellTitA2.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaTitulo1.AddCell(CellTitA2);

                        loDocument.Add(TablaTitulo1);
                        loDocument.Add(lpEspacios);

                        Cell CellDatoA1 = new Cell();
                        CellDatoA1.Border = 0;
                        CellDatoA1.BorderWidthLeft = 0;
                        CellDatoA1.BorderWidthTop = 0;
                        CellDatoA1.BorderWidthRight = 0;
                        CellDatoA1.BorderWidthBottom = 0;
                        CellDatoA1.BackgroundColor = clClaro;
                        CellDatoA1.AddElement(new Paragraph(" Participante:", FuenteDato));
                        CellDatoA1.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA1.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA1);

                        TablaDatos1.AddCell(CellBlanco);

                        Cell CellDatoA2 = new Cell();
                        CellDatoA2.Border = 0;
                        CellDatoA2.BorderWidthLeft = 0;
                        CellDatoA2.BorderWidthTop = 0;
                        CellDatoA2.BorderWidthRight = 0;
                        CellDatoA2.BorderWidthBottom = 0;
                        CellDatoA2.AddElement(new Paragraph(lLector["nom_vendedor"].ToString(), FuenteDato1));
                        CellDatoA2.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA2.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA2);

                        TablaDatos1.AddCell(CellDatoA1);
                        TablaDatos1.AddCell(CellBlanco);

                        Cell CellDatoA4 = new Cell();
                        CellDatoA4.Border = 0;
                        CellDatoA4.BorderWidthLeft = 0;
                        CellDatoA4.BorderWidthTop = 0;
                        CellDatoA4.BorderWidthRight = 0;
                        CellDatoA4.BorderWidthBottom = 0;
                        CellDatoA4.AddElement(new Paragraph(lLector["nom_comprador"].ToString(), FuenteDato1));
                        CellDatoA4.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA4.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA4);

                        Cell CellDatoA5 = new Cell();
                        CellDatoA5.Border = 0;
                        CellDatoA5.BorderWidthLeft = 0;
                        CellDatoA5.BorderWidthTop = 0;
                        CellDatoA5.BorderWidthRight = 0;
                        CellDatoA5.BorderWidthBottom = 0;
                        CellDatoA5.BackgroundColor = clClaro;
                        CellDatoA5.AddElement(new Paragraph(" Nit:", FuenteDato));
                        CellDatoA5.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA5.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA5);

                        TablaDatos1.AddCell(CellBlanco);

                        Cell CellDatoA6 = new Cell();
                        CellDatoA6.Border = 0;
                        CellDatoA6.BorderWidthLeft = 0;
                        CellDatoA6.BorderWidthTop = 0;
                        CellDatoA6.BorderWidthRight = 0;
                        CellDatoA6.BorderWidthBottom = 0;
                        CellDatoA6.AddElement(new Paragraph(lLector["nit_vendedor"].ToString(), FuenteDato1));
                        CellDatoA6.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA6.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA6);

                        TablaDatos1.AddCell(CellDatoA5);
                        TablaDatos1.AddCell(CellBlanco);

                        Cell CellDatoA8 = new Cell();
                        CellDatoA8.Border = 0;
                        CellDatoA8.BorderWidthLeft = 0;
                        CellDatoA8.BorderWidthTop = 0;
                        CellDatoA8.BorderWidthRight = 0;
                        CellDatoA8.BorderWidthBottom = 0;
                        CellDatoA8.AddElement(new Paragraph(lLector["nit_comprador"].ToString(), FuenteDato1));
                        CellDatoA8.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA8.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA8);

                        Cell CellDatoA9 = new Cell();
                        CellDatoA9.Border = 0;
                        CellDatoA9.BorderWidthLeft = 0;
                        CellDatoA9.BorderWidthTop = 0;
                        CellDatoA9.BorderWidthRight = 0;
                        CellDatoA9.BorderWidthBottom = 0;
                        CellDatoA9.BackgroundColor = clClaro;
                        CellDatoA9.AddElement(new Paragraph(" Tipo de participante:", FuenteDato));
                        CellDatoA9.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA9.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA9);

                        TablaDatos1.AddCell(CellBlanco);

                        Cell CellDatoA10 = new Cell();
                        CellDatoA10.Border = 0;
                        CellDatoA10.BorderWidthLeft = 0;
                        CellDatoA10.BorderWidthTop = 0;
                        CellDatoA10.BorderWidthRight = 0;
                        CellDatoA10.BorderWidthBottom = 0;
                        CellDatoA10.AddElement(new Paragraph(lLector["tipo_vendedor"].ToString(), FuenteDato1));
                        CellDatoA10.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA10.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA10);

                        TablaDatos1.AddCell(CellDatoA9);
                        TablaDatos1.AddCell(CellBlanco);

                        Cell CellDatoA11 = new Cell();
                        CellDatoA11.Border = 0;
                        CellDatoA11.BorderWidthLeft = 0;
                        CellDatoA11.BorderWidthTop = 0;
                        CellDatoA11.BorderWidthRight = 0;
                        CellDatoA11.BorderWidthBottom = 0;
                        CellDatoA11.AddElement(new Paragraph(lLector["tipo_comprador"].ToString(), FuenteDato1));
                        CellDatoA11.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA11.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA11);

                        Cell CellDatoA12 = new Cell();
                        CellDatoA12.Border = 0;
                        CellDatoA12.BorderWidthLeft = 0;
                        CellDatoA12.BorderWidthTop = 0;
                        CellDatoA12.BorderWidthRight = 0;
                        CellDatoA12.BorderWidthBottom = 0;
                        CellDatoA12.BackgroundColor = clClaro;
                        CellDatoA12.AddElement(new Paragraph(" Domiciliado en:", FuenteDato));
                        CellDatoA12.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA12.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA12);

                        TablaDatos1.AddCell(CellBlanco);

                        Cell CellDatoA13 = new Cell();
                        CellDatoA13.Border = 0;
                        CellDatoA13.BorderWidthLeft = 0;
                        CellDatoA13.BorderWidthTop = 0;
                        CellDatoA13.BorderWidthRight = 0;
                        CellDatoA13.BorderWidthBottom = 0;
                        CellDatoA13.AddElement(new Paragraph(lLector["direccion_vendedor"].ToString(), FuenteDato1));
                        CellDatoA13.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA13.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA13);

                        TablaDatos1.AddCell(CellDatoA12);
                        TablaDatos1.AddCell(CellBlanco);

                        Cell CellDatoA15 = new Cell();
                        CellDatoA15.Border = 0;
                        CellDatoA15.BorderWidthLeft = 0;
                        CellDatoA15.BorderWidthTop = 0;
                        CellDatoA15.BorderWidthRight = 0;
                        CellDatoA15.BorderWidthBottom = 0;
                        CellDatoA15.AddElement(new Paragraph(lLector["direccion_comprador"].ToString(), FuenteDato1));
                        CellDatoA15.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA15.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA15);

                        Cell CellDatoA16 = new Cell();
                        CellDatoA16.Border = 0;
                        CellDatoA16.BorderWidthLeft = 0;
                        CellDatoA16.BorderWidthTop = 0;
                        CellDatoA16.BorderWidthRight = 0;
                        CellDatoA16.BorderWidthBottom = 0;
                        CellDatoA16.BackgroundColor = clClaro;
                        CellDatoA16.AddElement(new Paragraph(" Mail:", FuenteDato));
                        CellDatoA16.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA16.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA16);

                        TablaDatos1.AddCell(CellBlanco);

                        Cell CellDatoA17 = new Cell();
                        CellDatoA17.Border = 0;
                        CellDatoA17.BorderWidthLeft = 0;
                        CellDatoA17.BorderWidthTop = 0;
                        CellDatoA17.BorderWidthRight = 0;
                        CellDatoA17.BorderWidthBottom = 0;
                        CellDatoA17.AddElement(new Paragraph(lLector["mail_v"].ToString(), FuenteDato1));
                        CellDatoA17.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA17.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA17);

                        TablaDatos1.AddCell(CellDatoA16);
                        TablaDatos1.AddCell(CellBlanco);

                        Cell CellDatoA19 = new Cell();
                        CellDatoA19.Border = 0;
                        CellDatoA19.BorderWidthLeft = 0;
                        CellDatoA19.BorderWidthTop = 0;
                        CellDatoA19.BorderWidthRight = 0;
                        CellDatoA19.BorderWidthBottom = 0;
                        CellDatoA19.AddElement(new Paragraph(lLector["mail_c"].ToString(), FuenteDato1));
                        CellDatoA19.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA19.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA19);

                        Cell CellDatoA20 = new Cell();
                        CellDatoA20.Border = 0;
                        CellDatoA20.BorderWidthLeft = 0;
                        CellDatoA20.BorderWidthTop = 0;
                        CellDatoA20.BorderWidthRight = 0;
                        CellDatoA20.BorderWidthBottom = 0;
                        CellDatoA20.BackgroundColor = clClaro;
                        CellDatoA20.AddElement(new Paragraph(" Teléfono:", FuenteDato));
                        CellDatoA20.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA20.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA20);

                        TablaDatos1.AddCell(CellBlanco);

                        Cell CellDatoA21 = new Cell();
                        CellDatoA21.Border = 0;
                        CellDatoA21.BorderWidthLeft = 0;
                        CellDatoA21.BorderWidthTop = 0;
                        CellDatoA21.BorderWidthRight = 0;
                        CellDatoA21.BorderWidthBottom = 0;
                        CellDatoA21.AddElement(new Paragraph(lLector["telefono_v"].ToString(), FuenteDato1));
                        CellDatoA21.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA21.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA21);

                        TablaDatos1.AddCell(CellDatoA20);
                        TablaDatos1.AddCell(CellBlanco);

                        Cell CellDatoA23 = new Cell();
                        CellDatoA23.Border = 0;
                        CellDatoA23.BorderWidthLeft = 0;
                        CellDatoA23.BorderWidthTop = 0;
                        CellDatoA23.BorderWidthRight = 0;
                        CellDatoA23.BorderWidthBottom = 0;
                        CellDatoA23.AddElement(new Paragraph(lLector["telefono_c"].ToString(), FuenteDato1));
                        CellDatoA23.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA23.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA23);

                        Cell CellDatoA24 = new Cell();
                        CellDatoA24.Border = 0;
                        CellDatoA24.BorderWidthLeft = 0;
                        CellDatoA24.BorderWidthTop = 0;
                        CellDatoA24.BorderWidthRight = 0;
                        CellDatoA24.BorderWidthBottom = 0;
                        CellDatoA24.BackgroundColor = clClaro;
                        CellDatoA24.AddElement(new Paragraph(" Celular:", FuenteDato));
                        CellDatoA24.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA24.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA24);

                        TablaDatos1.AddCell(CellBlanco);

                        Cell CellDatoA25 = new Cell();
                        CellDatoA25.Border = 0;
                        CellDatoA25.BorderWidthLeft = 0;
                        CellDatoA25.BorderWidthTop = 0;
                        CellDatoA25.BorderWidthRight = 0;
                        CellDatoA25.BorderWidthBottom = 0;
                        CellDatoA25.AddElement(new Paragraph(lLector["celular_v"].ToString(), FuenteDato1));
                        CellDatoA25.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA25.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA25);

                        TablaDatos1.AddCell(CellDatoA24);
                        TablaDatos1.AddCell(CellBlanco);

                        Cell CellDatoA27 = new Cell();
                        CellDatoA27.Border = 0;
                        CellDatoA27.BorderWidthLeft = 0;
                        CellDatoA27.BorderWidthTop = 0;
                        CellDatoA27.BorderWidthRight = 0;
                        CellDatoA27.BorderWidthBottom = 0;
                        CellDatoA27.AddElement(new Paragraph(lLector["celular_c"].ToString(), FuenteDato1));
                        CellDatoA27.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoA27.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos1.AddCell(CellDatoA27);

                        //20201207 subasta UVCP
                        if (lLector["codigo_tipo_subasta"].ToString() == "2")
                        {
                            Cell CellDatoB24 = new Cell();
                            CellDatoB24.Border = 0;
                            CellDatoB24.BorderWidthLeft = 0;
                            CellDatoB24.BorderWidthTop = 0;
                            CellDatoB24.BorderWidthRight = 0;
                            CellDatoB24.BorderWidthBottom = 0;
                            CellDatoB24.BackgroundColor = clClaro;
                            CellDatoB24.AddElement(new Paragraph(" Entidad Bancaria:", FuenteDato));
                            CellDatoB24.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB24.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos1.AddCell(CellDatoB24);

                            TablaDatos1.AddCell(CellBlanco);

                            Cell CellDatoB25 = new Cell();
                            CellDatoB25.Border = 0;
                            CellDatoB25.BorderWidthLeft = 0;
                            CellDatoB25.BorderWidthTop = 0;
                            CellDatoB25.BorderWidthRight = 0;
                            CellDatoB25.BorderWidthBottom = 0;
                            CellDatoB25.AddElement(new Paragraph(lLector["banco_pago"].ToString(), FuenteDato1));
                            CellDatoB25.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB25.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos1.AddCell(CellDatoB25);

                            TablaDatos1.AddCell(CellClaro);
                            TablaDatos1.AddCell(CellBlanco);
                            TablaDatos1.AddCell(CellBlanco);

                            Cell CellDatoC24 = new Cell();
                            CellDatoC24.Border = 0;
                            CellDatoC24.BorderWidthLeft = 0;
                            CellDatoC24.BorderWidthTop = 0;
                            CellDatoC24.BorderWidthRight = 0;
                            CellDatoC24.BorderWidthBottom = 0;
                            CellDatoC24.BackgroundColor = clClaro;
                            CellDatoC24.AddElement(new Paragraph(" Tipo de Cuenta:", FuenteDato));
                            CellDatoC24.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoC24.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos1.AddCell(CellDatoC24);

                            TablaDatos1.AddCell(CellBlanco);

                            Cell CellDatoC25 = new Cell();
                            CellDatoC25.Border = 0;
                            CellDatoC25.BorderWidthLeft = 0;
                            CellDatoC25.BorderWidthTop = 0;
                            CellDatoC25.BorderWidthRight = 0;
                            CellDatoC25.BorderWidthBottom = 0;
                            CellDatoC25.AddElement(new Paragraph(lLector["tipo_cuenta"].ToString(), FuenteDato1));
                            CellDatoC25.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoC25.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos1.AddCell(CellDatoC25);

                            TablaDatos1.AddCell(CellClaro);
                            TablaDatos1.AddCell(CellBlanco);
                            TablaDatos1.AddCell(CellBlanco);


                            Cell CellDatoD24 = new Cell();
                            CellDatoD24.Border = 0;
                            CellDatoD24.BorderWidthLeft = 0;
                            CellDatoD24.BorderWidthTop = 0;
                            CellDatoD24.BorderWidthRight = 0;
                            CellDatoD24.BorderWidthBottom = 0;
                            CellDatoD24.BackgroundColor = clClaro;
                            CellDatoD24.AddElement(new Paragraph(" No. de Cuenta:", FuenteDato));
                            CellDatoD24.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoD24.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos1.AddCell(CellDatoD24);

                            TablaDatos1.AddCell(CellBlanco);

                            Cell CellDatoD25 = new Cell();
                            CellDatoD25.Border = 0;
                            CellDatoD25.BorderWidthLeft = 0;
                            CellDatoD25.BorderWidthTop = 0;
                            CellDatoD25.BorderWidthRight = 0;
                            CellDatoD25.BorderWidthBottom = 0;
                            CellDatoD25.AddElement(new Paragraph(lLector["cuenta_pago"].ToString(), FuenteDato1));
                            CellDatoD25.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoD25.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos1.AddCell(CellDatoD25);

                            TablaDatos1.AddCell(CellClaro);
                            TablaDatos1.AddCell(CellBlanco);
                            TablaDatos1.AddCell(CellBlanco);
                        }
                        //20201207 fin subasta UVCP

                        //espacio en blanco
                        TablaDatos1.AddCell(CellClaro);
                        TablaDatos1.AddCell(CellBlanco);
                        TablaDatos1.AddCell(CellBlanco);
                        TablaDatos1.AddCell(CellClaro);
                        TablaDatos1.AddCell(CellBlanco);
                        TablaDatos1.AddCell(CellBlanco);

                        loDocument.Add(TablaDatos1);
                        loDocument.Add(lpEspacios);

                        TablaTitulo2.AddCell(CellAzul);

                        Cell CellTitB1 = new Cell();
                        CellTitB1.Border = 0;
                        CellTitB1.BorderWidthLeft = 0;
                        CellTitB1.BorderWidthTop = 0;
                        CellTitB1.BorderWidthRight = 0;
                        CellTitB1.BorderWidthBottom = 0;
                        CellTitB1.BackgroundColor = clAzul;
                        CellTitB1.AddElement(new Paragraph("Han negociado el producto que se detalla a continuación:", FuenteTitulo1));
                        CellTitB1.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellTitB1.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaTitulo2.AddCell(CellTitB1);

                        loDocument.Add(TablaTitulo2);
                        loDocument.Add(lpEspacios);
                        //20190717 rq050-19
                        if (lLector["codigo_modalidad"].ToString() != "22")
                        {
                            Cell CellDatoB1 = new Cell();
                            CellDatoB1.Border = 0;
                            CellDatoB1.BorderWidthLeft = 0;
                            CellDatoB1.BorderWidthTop = 0;
                            CellDatoB1.BorderWidthRight = 0;
                            CellDatoB1.BorderWidthBottom = 0;
                            CellDatoB1.BackgroundColor = clClaro;
                            CellDatoB1.AddElement(new Paragraph(" Producto:", FuenteDato));
                            CellDatoB1.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB1.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2.AddCell(CellDatoB1);

                            Cell CellDatoB2 = new Cell();
                            CellDatoB2.Border = 0;
                            CellDatoB2.BorderWidthLeft = 0;
                            CellDatoB2.BorderWidthTop = 0;
                            CellDatoB2.BorderWidthRight = 0;
                            CellDatoB2.BorderWidthBottom = 0;
                            CellDatoB2.AddElement(new Paragraph(" " + lLector["producto"].ToString(), FuenteDato1));
                            CellDatoB2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB2.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2.AddCell(CellDatoB2);

                            Cell CellDatoB3 = new Cell();
                            CellDatoB3.Border = 0;
                            CellDatoB3.BorderWidthLeft = 0;
                            CellDatoB3.BorderWidthTop = 0;
                            CellDatoB3.BorderWidthRight = 0;
                            CellDatoB3.BorderWidthBottom = 0;
                            CellDatoB3.BackgroundColor = clClaro;
                            CellDatoB3.AddElement(new Paragraph(" Modalidad Contractual:", FuenteDato));
                            CellDatoB3.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB3.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2.AddCell(CellDatoB3);

                            Cell CellDatoB4 = new Cell();
                            CellDatoB4.Border = 0;
                            CellDatoB4.BorderWidthLeft = 0;
                            CellDatoB4.BorderWidthTop = 0;
                            CellDatoB4.BorderWidthRight = 0;
                            CellDatoB4.BorderWidthBottom = 0;
                            CellDatoB4.AddElement(new Paragraph(" " + lLector["modalidad"].ToString(), FuenteDato1));
                            CellDatoB4.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB4.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2.AddCell(CellDatoB4);

                            Cell CellDatoB5 = new Cell();
                            CellDatoB5.Border = 0;
                            CellDatoB5.BorderWidthLeft = 0;
                            CellDatoB5.BorderWidthTop = 0;
                            CellDatoB5.BorderWidthRight = 0;
                            CellDatoB5.BorderWidthBottom = 0;
                            CellDatoB5.BackgroundColor = clClaro;
                            CellDatoB5.AddElement(new Paragraph(" Ruta/Punto de entrega:", FuenteDato));
                            CellDatoB5.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB5.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2.AddCell(CellDatoB5);

                            Cell CellDatoB6 = new Cell();
                            CellDatoB6.Border = 0;
                            CellDatoB6.BorderWidthLeft = 0;
                            CellDatoB6.BorderWidthTop = 0;
                            CellDatoB6.BorderWidthRight = 0;
                            CellDatoB6.BorderWidthBottom = 0;
                            CellDatoB6.AddElement(new Paragraph(" " + lLector["punto_entrega"].ToString(), FuenteDato1));
                            CellDatoB6.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB6.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2.AddCell(CellDatoB6);

                            Cell CellDatoB7 = new Cell();
                            CellDatoB7.Border = 0;
                            CellDatoB7.BorderWidthLeft = 0;
                            CellDatoB7.BorderWidthTop = 0;
                            CellDatoB7.BorderWidthRight = 0;
                            CellDatoB7.BorderWidthBottom = 0;
                            CellDatoB7.BackgroundColor = clClaro;
                            CellDatoB7.AddElement(new Paragraph(" Duración:", FuenteDato));
                            CellDatoB7.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB7.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2.AddCell(CellDatoB7);

                            Cell CellDatoB8 = new Cell();
                            CellDatoB8.Border = 0;
                            CellDatoB8.BorderWidthLeft = 0;
                            CellDatoB8.BorderWidthTop = 0;
                            CellDatoB8.BorderWidthRight = 0;
                            CellDatoB8.BorderWidthBottom = 0;
                            CellDatoB8.AddElement(new Paragraph(" " + lLector["duracion"].ToString(), FuenteDato1));
                            CellDatoB8.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB8.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2.AddCell(CellDatoB8);

                            //20220920 ajsute cerficiado
                            Cell CellDatoB9 = new Cell();
                            CellDatoB9.Border = 0;
                            CellDatoB9.BorderWidthLeft = 0;
                            CellDatoB9.BorderWidthTop = 0;
                            CellDatoB9.BorderWidthRight = 0;
                            CellDatoB9.BorderWidthBottom = 0;
                            CellDatoB9.BackgroundColor = clClaro;
                            CellDatoB9.AddElement(new Paragraph(" Tipo de Subasta:", FuenteDato));
                            CellDatoB9.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB9.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2.AddCell(CellDatoB9);

                            //20220920 ajsute cerficiado
                            Cell CellDatoB10 = new Cell();
                            CellDatoB10.Border = 0;
                            CellDatoB10.BorderWidthLeft = 0;
                            CellDatoB10.BorderWidthTop = 0;
                            CellDatoB10.BorderWidthRight = 0;
                            CellDatoB10.BorderWidthBottom = 0;
                            CellDatoB10.AddElement(new Paragraph(" " + lLector["desc_tipo_rueda"].ToString(), FuenteDato1));
                            CellDatoB10.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB10.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2.AddCell(CellDatoB10);
                            //espacio en blanco
                            TablaDatos2.AddCell(CellClaro);
                            TablaDatos2.AddCell(CellBlanco);

                            loDocument.Add(TablaDatos2);
                            loDocument.Add(lpEspacios);

                        }
                        else  //20190717 rq050-19
                        {
                            Cell CellDatoB1 = new Cell();
                            CellDatoB1.Border = 0;
                            CellDatoB1.BorderWidthLeft = 0;
                            CellDatoB1.BorderWidthTop = 0;
                            CellDatoB1.BorderWidthRight = 0;
                            CellDatoB1.BorderWidthBottom = 0;
                            CellDatoB1.BackgroundColor = clClaro;
                            CellDatoB1.AddElement(new Paragraph(" Producto:", FuenteDato));
                            CellDatoB1.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB1.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2A.AddCell(CellDatoB1);

                            Cell CellDatoB2 = new Cell();
                            CellDatoB2.Border = 0;
                            CellDatoB2.BorderWidthLeft = 0;
                            CellDatoB2.BorderWidthTop = 0;
                            CellDatoB2.BorderWidthRight = 0;
                            CellDatoB2.BorderWidthBottom = 0;
                            CellDatoB2.AddElement(new Paragraph(" " + lLector["producto"].ToString(), FuenteDato1));
                            CellDatoB2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB2.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2A.AddCell(CellDatoB2);

                            Cell CellDatoB1A = new Cell();
                            CellDatoB1A.Border = 0;
                            CellDatoB1A.BorderWidthLeft = 0;
                            CellDatoB1A.BorderWidthTop = 0;
                            CellDatoB1A.BorderWidthRight = 0;
                            CellDatoB1A.BorderWidthBottom = 0;
                            CellDatoB1A.BackgroundColor = clClaro;
                            CellDatoB1A.AddElement(new Paragraph(" Firmeza Mínima:", FuenteDato));
                            CellDatoB1A.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB1A.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2A.AddCell(CellDatoB1A);

                            Cell CellDatoB2A = new Cell();
                            CellDatoB2A.Border = 0;
                            CellDatoB2A.BorderWidthLeft = 0;
                            CellDatoB2A.BorderWidthTop = 0;
                            CellDatoB2A.BorderWidthRight = 0;
                            CellDatoB2A.BorderWidthBottom = 0;
                            CellDatoB2A.AddElement(new Paragraph(" " + lLector["firmeza_minima"].ToString(), FuenteDato1));
                            CellDatoB2A.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB2A.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2A.AddCell(CellDatoB2A);

                            Cell CellDatoB3 = new Cell();
                            CellDatoB3.Border = 0;
                            CellDatoB3.BorderWidthLeft = 0;
                            CellDatoB3.BorderWidthTop = 0;
                            CellDatoB3.BorderWidthRight = 0;
                            CellDatoB3.BorderWidthBottom = 0;
                            CellDatoB3.BackgroundColor = clClaro;
                            CellDatoB3.AddElement(new Paragraph(" Modalidad Contractual:", FuenteDato));
                            CellDatoB3.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB3.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2A.AddCell(CellDatoB3);

                            Cell CellDatoB4 = new Cell();
                            CellDatoB4.Border = 0;
                            CellDatoB4.BorderWidthLeft = 0;
                            CellDatoB4.BorderWidthTop = 0;
                            CellDatoB4.BorderWidthRight = 0;
                            CellDatoB4.BorderWidthBottom = 0;
                            CellDatoB4.AddElement(new Paragraph(" " + lLector["modalidad"].ToString(), FuenteDato1));
                            CellDatoB4.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB4.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2A.AddCell(CellDatoB4);

                            TablaDatos2A.AddCell(CellClaro);
                            TablaDatos2A.AddCell(CellBlanco);

                            Cell CellDatoB5 = new Cell();
                            CellDatoB5.Border = 0;
                            CellDatoB5.BorderWidthLeft = 0;
                            CellDatoB5.BorderWidthTop = 0;
                            CellDatoB5.BorderWidthRight = 0;
                            CellDatoB5.BorderWidthBottom = 0;
                            CellDatoB5.BackgroundColor = clClaro;
                            CellDatoB5.AddElement(new Paragraph(" Ruta/Punto de entrega:", FuenteDato));
                            CellDatoB5.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB5.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2A.AddCell(CellDatoB5);

                            Cell CellDatoB6 = new Cell();
                            CellDatoB6.Border = 0;
                            CellDatoB6.BorderWidthLeft = 0;
                            CellDatoB6.BorderWidthTop = 0;
                            CellDatoB6.BorderWidthRight = 0;
                            CellDatoB6.BorderWidthBottom = 0;
                            CellDatoB6.AddElement(new Paragraph(" " + lLector["punto_entrega"].ToString(), FuenteDato1));
                            CellDatoB6.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB6.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2A.AddCell(CellDatoB6);

                            TablaDatos2A.AddCell(CellClaro);
                            TablaDatos2A.AddCell(CellBlanco);

                            Cell CellDatoB7 = new Cell();
                            CellDatoB7.Border = 0;
                            CellDatoB7.BorderWidthLeft = 0;
                            CellDatoB7.BorderWidthTop = 0;
                            CellDatoB7.BorderWidthRight = 0;
                            CellDatoB7.BorderWidthBottom = 0;
                            CellDatoB7.BackgroundColor = clClaro;
                            CellDatoB7.AddElement(new Paragraph(" Duración:", FuenteDato));
                            CellDatoB7.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB7.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2A.AddCell(CellDatoB7);

                            Cell CellDatoB8 = new Cell();
                            CellDatoB8.Border = 0;
                            CellDatoB8.BorderWidthLeft = 0;
                            CellDatoB8.BorderWidthTop = 0;
                            CellDatoB8.BorderWidthRight = 0;
                            CellDatoB8.BorderWidthBottom = 0;
                            CellDatoB8.AddElement(new Paragraph(" " + lLector["duracion"].ToString(), FuenteDato1));
                            CellDatoB8.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB8.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2A.AddCell(CellDatoB8);

                            //20220920 ajsute certificado
                            Cell CellDatoB9 = new Cell();
                            CellDatoB9.Border = 0;
                            CellDatoB9.BorderWidthLeft = 0;
                            CellDatoB9.BorderWidthTop = 0;
                            CellDatoB9.BorderWidthRight = 0;
                            CellDatoB9.BorderWidthBottom = 0;
                            CellDatoB9.BackgroundColor = clClaro;
                            CellDatoB9.AddElement(new Paragraph(" Tipo de Subasta:", FuenteDato));
                            CellDatoB9.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB9.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2A.AddCell(CellDatoB9);

                            //20220920 ajsute certificado
                            Cell CellDatoB10 = new Cell();
                            CellDatoB10.Border = 0;
                            CellDatoB10.BorderWidthLeft = 0;
                            CellDatoB10.BorderWidthTop = 0;
                            CellDatoB10.BorderWidthRight = 0;
                            CellDatoB10.BorderWidthBottom = 0;
                            CellDatoB10.AddElement(new Paragraph(" " + lLector["desc_tipo_rueda"].ToString(), FuenteDato1));
                            CellDatoB10.VerticalAlignment = Element.ALIGN_MIDDLE;
                            CellDatoB10.HorizontalAlignment = Element.ALIGN_LEFT;
                            TablaDatos2A.AddCell(CellDatoB10);

                            TablaDatos2A.AddCell(CellClaro);
                            TablaDatos2A.AddCell(CellBlanco);

                            //espacio en blanco
                            TablaDatos2A.AddCell(CellClaro);
                            TablaDatos2A.AddCell(CellBlanco);

                            TablaDatos2A.AddCell(CellClaro);
                            TablaDatos2A.AddCell(CellBlanco);

                            loDocument.Add(TablaDatos2A);
                            loDocument.Add(lpEspacios);

                        }


                        TablaTitulo2.DeleteAllRows();
                        TablaTitulo2.AddCell(CellAzul);

                        Cell CellTitC1 = new Cell();
                        CellTitC1.Border = 0;
                        CellTitC1.BorderWidthLeft = 0;
                        CellTitC1.BorderWidthTop = 0;
                        CellTitC1.BorderWidthRight = 0;
                        CellTitC1.BorderWidthBottom = 0;
                        CellTitC1.BackgroundColor = clAzul;
                        CellTitC1.AddElement(new Paragraph("Con los siguientes resultados:", FuenteTitulo1));
                        CellTitC1.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellTitC1.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaTitulo2.AddCell(CellTitC1);

                        loDocument.Add(TablaTitulo2);

                        Cell CellDatoC1 = new Cell();
                        CellDatoC1.Border = 0;
                        CellDatoC1.BorderWidthLeft = 0;
                        CellDatoC1.BorderWidthTop = 0;
                        CellDatoC1.BorderWidthRight = 0;
                        CellDatoC1.BorderWidthBottom = 0;
                        CellDatoC1.BackgroundColor = clClaro;
                        CellDatoC1.AddElement(new Paragraph(" Cantidad/ Capacidad asignada:", FuenteDato));
                        CellDatoC1.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoC1.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos3.AddCell(CellDatoC1);

                        Cell CellDatoC2 = new Cell();
                        CellDatoC2.Border = 0;
                        CellDatoC2.BorderWidthLeft = 0;
                        CellDatoC2.BorderWidthTop = 0;
                        CellDatoC2.BorderWidthRight = 0;
                        CellDatoC2.BorderWidthBottom = 0;
                        CellDatoC2.AddElement(new Paragraph(" " + lLector["cantidad"].ToString(), FuenteDato1));
                        CellDatoC2.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoC2.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos3.AddCell(CellDatoC2);

                        Cell CellDatoC3 = new Cell();
                        CellDatoC3.Border = 0;
                        CellDatoC3.BorderWidthLeft = 0;
                        CellDatoC3.BorderWidthTop = 0;
                        CellDatoC3.BorderWidthRight = 0;
                        CellDatoC3.BorderWidthBottom = 0;
                        CellDatoC3.BackgroundColor = clClaro;
                        CellDatoC3.AddElement(new Paragraph(" Unidad:", FuenteDato));
                        CellDatoC3.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoC3.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos3.AddCell(CellDatoC3);

                        Cell CellDatoC4 = new Cell();
                        CellDatoC4.Border = 0;
                        CellDatoC4.BorderWidthLeft = 0;
                        CellDatoC4.BorderWidthTop = 0;
                        CellDatoC4.BorderWidthRight = 0;
                        CellDatoC4.BorderWidthBottom = 0;
                        CellDatoC4.AddElement(new Paragraph(" " + lLector["unidad_medida"].ToString(), FuenteDato1));
                        CellDatoC4.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoC4.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos3.AddCell(CellDatoC4);

                        Cell CellDatoC5 = new Cell();
                        CellDatoC5.Border = 0;
                        CellDatoC5.BorderWidthLeft = 0;
                        CellDatoC5.BorderWidthTop = 0;
                        CellDatoC5.BorderWidthRight = 0;
                        CellDatoC5.BorderWidthBottom = 0;
                        CellDatoC5.BackgroundColor = clClaro;
                        CellDatoC5.AddElement(new Paragraph(" Precio de adjudicación (US$/MBTU)(Moneda Vigente/KPC):", FuenteDato)); //20220828
                        CellDatoC5.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoC5.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos3.AddCell(CellDatoC5);

                        Cell CellDatoC6 = new Cell();
                        CellDatoC6.Border = 0;
                        CellDatoC6.BorderWidthLeft = 0;
                        CellDatoC6.BorderWidthTop = 0;
                        CellDatoC6.BorderWidthRight = 0;
                        CellDatoC6.BorderWidthBottom = 0;
                        CellDatoC6.AddElement(new Paragraph(lLector["precio"].ToString(), FuenteDato1));//20220828
                        CellDatoC6.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoC6.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos3.AddCell(CellDatoC6);

                        Cell CellDatoC7 = new Cell();
                        CellDatoC7.Border = 0;
                        CellDatoC7.BorderWidthLeft = 0;
                        CellDatoC7.BorderWidthTop = 0;
                        CellDatoC7.BorderWidthRight = 0;
                        CellDatoC7.BorderWidthBottom = 0;
                        CellDatoC7.BackgroundColor = clClaro;
                        CellDatoC7.AddElement(new Paragraph(" Valor del Contrato (Moneda vigente):", FuenteDato)); //20220828
                        CellDatoC7.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoC7.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos3.AddCell(CellDatoC7);

                        Cell CellDatoC8 = new Cell();
                        CellDatoC8.Border = 0;
                        CellDatoC8.BorderWidthLeft = 0;
                        CellDatoC8.BorderWidthTop = 0;
                        CellDatoC8.BorderWidthRight = 0;
                        CellDatoC8.BorderWidthBottom = 0;
                        CellDatoC8.AddElement(new Paragraph(Convert.ToDecimal(lLector["valor"].ToString()).ToString("###,###,###,###,###,###.00"), FuenteDato2)); //20220828
                        CellDatoC8.VerticalAlignment = Element.ALIGN_MIDDLE;
                        CellDatoC8.HorizontalAlignment = Element.ALIGN_LEFT;
                        TablaDatos3.AddCell(CellDatoC8);

                        //espacio en blanco
                        TablaDatos3.AddCell(CellClaro);
                        TablaDatos3.AddCell(CellBlanco);

                        loDocument.Add(TablaDatos3);
                        loDocument.Add(lpEspacios);
                        loDocument.Add(lpEspacios);

                        Paragraph lpAclara = new Paragraph("* Se emitirá el certificado definitivo de la asignación, hasta tanto, se realice el registro del respectivo contrato.", FuenteDato2);

                        loDocument.Add(lpAclara);
                        loDocument.Add(lpEspacios);

                        Paragraph lpDetNota = new Paragraph("NOTA: El vendedor y el respectivo comprador serán responsables de suscribir el contrato de compraventa en donde se deberá reflejar los resultados la presente subasta o negociación.  De igual forma, coordinarán los aspectos operativos requeridos, tales como el proceso de nominación, conforme a la regulación vigente.", FuenteDato2);

                        //Cell CellLogo5g = new Cell();
                        //CellLogo5g.Border = 0;
                        //CellLogo5g.BorderWidthLeft = 0;
                        //CellLogo5g.BorderWidthTop = 0;
                        //CellLogo5g.BorderWidthRight = 0;
                        //CellLogo5g.BorderWidthBottom = 0;
                        //CellLogo5g.AddElement(lpAclara);
                        //CellLogo5g.VerticalAlignment = Element.ALIGN_TOP;
                        //CellLogo5g.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                        //TablaTexto.AddCell(CellLogo5g);

                        //Cell CellLogo5e = new Cell();
                        //CellLogo5e.Border = 0;
                        //CellLogo5e.BorderWidthLeft = 0;
                        //CellLogo5e.BorderWidthTop = 0;
                        //CellLogo5e.BorderWidthRight = 0;
                        //CellLogo5e.BorderWidthBottom = 0;
                        //CellLogo5e.AddElement(lpEspacios);
                        //CellLogo5e.VerticalAlignment = Element.ALIGN_TOP;
                        //CellLogo5e.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                        //TablaTexto.AddCell(CellLogo5e);


                        Cell CellLogo6g = new Cell();
                        CellLogo6g.Border = 0;
                        CellLogo6g.BorderWidthLeft = 0;
                        CellLogo6g.BorderWidthTop = 0;
                        CellLogo6g.BorderWidthRight = 0;
                        CellLogo6g.BorderWidthBottom = 0;
                        CellLogo6g.AddElement(lpDetNota);
                        CellLogo6g.VerticalAlignment = Element.ALIGN_TOP;
                        CellLogo6g.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                        TablaTexto.AddCell(CellLogo6g);

                        loDocument.Add(TablaTexto);
                        TablaTexto.DeleteAllRows();
                        loDocument.Add(TablaTexto);
                        loDocument.NewPage();
                    }
                    lLector.Close();
                    lLector.Dispose();
                }
            }
            loDocument.Close();
            lConexion.Cerrar();
            return lsdirPDF;
        }
        catch (Exception ex)
        {
            lConexion.Cerrar();
            loDocument.Close();
            return "ERROR - Al Generar el PDF. " + ex.Message.ToString();
        }
    }

    /// <summary>
    /// Nombre: generarCertProv
    /// Fecha: Abril 12 de 2019
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar el PDF de los Certificados Provisionales de Gas
    /// Modificacion:
    /// </summary>
    /// <param name="lsCadena"></param>
    public string generarCertAud(string lsContrato, string lsVerif, InfoSessionVO lsInfo)
    {
        clConexion lConexion = new clConexion(lsInfo);
        clConexion lConexion1 = new clConexion(lsInfo);
        SqlDataReader lLector;
        string[] lsNombreParametros = { "@P_numero_contrato", "@P_codigo_verif" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
        Object[] lValorParametros = { lsContrato, lsVerif };

        int[] liAnchoLogo = { 7, 53, 40 };
        int[] liAnchoTitulo = { 20, 60, 20 };
        int[] liAnchoCabecera = { 7, 20, 18, 5, 20, 20, 10 };
        int[] liAnchoTitulo1 = { 7, 48, 45 };
        int[] liAnchoDatos1 = { 20, 1, 29, 20, 1, 29 };
        int[] liAnchoTitulo2 = { 7, 93 };
        int[] liAnchoDatos2 = { 30, 70 }; //20190717 rq050-19
        int[] liAnchoDatos2A = { 30, 30, 20, 20 };//20190717 rq050-19
        int[] liAnchoDatos3 = { 50, 50 };
        int[] liAnchoFinal = { 100 };

        Document loDocument = new Document(PageSize.LETTER, 50, 50, 25, 25);
        string lsdirPDF = "ARCHIVO -";
        string lsdirLogo = "";
        string lsNombreArchivo = "";
        string lsNomFirma = "";
        string lsCarFirma = "";
        string lsArcFirma = "";

        try
        {
            // Se instancia la Conexion
            lConexion.Abrir();


            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
            if (lLector.HasRows)
            {
                lLector.Read();
                lsNomFirma = lLector["nombre_firma"].ToString();
                lsCarFirma = lLector["cargo_firma"].ToString();
                lsArcFirma = lLector["archivo_firma"].ToString();
            }
            lLector.Close();
            lLector.Dispose();


            // Definicion de los Fuentes
            iTextSharp.text.Font FuenteTitulo;
            iTextSharp.text.Font FuenteTitulo1;
            iTextSharp.text.Font FuenteDato;
            iTextSharp.text.Font FuenteDato1;
            iTextSharp.text.Font FuenteDato2;
            iTextSharp.text.Font FuenteDato3;
            iTextSharp.text.Font FuenteEspacio;
            iTextSharp.text.Font FuenteEspacioClaro;

            Color clClaro = new Color(135, 206, 250);
            Color clAzul = new Color(50, 84, 135);
            Color clBlanco = new Color(255, 255, 255);

            FuenteTitulo = FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD, clAzul);
            FuenteTitulo1 = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, clBlanco);
            FuenteDato = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD, clAzul);
            FuenteDato1 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL);
            FuenteDato2 = FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD);
            FuenteDato3 = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, clClaro);
            FuenteEspacio = FontFactory.GetFont(FontFactory.HELVETICA, 3, iTextSharp.text.Font.NORMAL);
            FuenteEspacioClaro = FontFactory.GetFont(FontFactory.HELVETICA, 3, iTextSharp.text.Font.NORMAL, clClaro);

            string lsArchivo = DateTime.Now.Millisecond.ToString() + "certGas_" + lsContrato + lsInfo.Usuario.ToString() + ".pdf";
            lsNombreArchivo = oRutaPdf + "\\" + lsArchivo;
            if (File.Exists(lsNombreArchivo))
                File.Delete(lsNombreArchivo);
            lsdirPDF += "../PDF/" + lsArchivo;

            lsdirLogo = oRutaImg;
            iTextSharp.text.Image LogoGas = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\bnatop.gif");
            LogoGas.ScalePercent(30);

            iTextSharp.text.Image ArchivoFirma = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\" + lsArcFirma);
            ArchivoFirma.ScalePercent(40);

            iTextSharp.text.Image LogoBna = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\logo_bna1.GIF");
            LogoBna.ScalePercent(45);

            iTextSharp.text.Image RayaI = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\raya izquierda.png");
            RayaI.ScalePercent(50);

            iTextSharp.text.Image RayaD = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\raya derecha.png");
            RayaD.ScalePercent(50);

            iTextSharp.text.Image LogoVig = iTextSharp.text.Image.GetInstance(lsdirLogo + "\\logo vigilado ALTA RESOLUCION.tif");
            LogoVig.ScalePercent(15);


            // Creo el Documento PDF
            PdfWriter.GetInstance(loDocument, new FileStream(lsNombreArchivo, FileMode.Create));
            loDocument.Open();
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetCertificadoGasAud", lsNombreParametros, lTipoparametros, lValorParametros);
            if (lLector.HasRows)
            {

                lLector.Read();
                iTextSharp.text.Table TablaLogo = new iTextSharp.text.Table(3, liAnchoLogo);
                iTextSharp.text.Table TablaTitulo = new iTextSharp.text.Table(3, liAnchoTitulo);
                iTextSharp.text.Table TablaCabecera = new iTextSharp.text.Table(7, liAnchoCabecera);
                iTextSharp.text.Table TablaTitulo1 = new iTextSharp.text.Table(3, liAnchoTitulo1);
                iTextSharp.text.Table TablaTitulo2 = new iTextSharp.text.Table(2, liAnchoTitulo2);
                iTextSharp.text.Table TablaTitulo3 = new iTextSharp.text.Table(2, liAnchoTitulo2);
                iTextSharp.text.Table TablaDatos1 = new iTextSharp.text.Table(6, liAnchoDatos1);
                iTextSharp.text.Table TablaDatos2 = new iTextSharp.text.Table(2, liAnchoDatos2);
                iTextSharp.text.Table TablaDatos2A = new iTextSharp.text.Table(4, liAnchoDatos2A);  //20190717 rq050-19
                iTextSharp.text.Table TablaDatos3 = new iTextSharp.text.Table(2, liAnchoDatos3);
                iTextSharp.text.Table TablaTexto = new iTextSharp.text.Table(1, liAnchoFinal);
                iTextSharp.text.Table TablaFinal = new iTextSharp.text.Table(1, liAnchoFinal);

                TablaLogo.Alignment = Element.ALIGN_LEFT;
                TablaLogo.Offset = 0;
                TablaLogo.Spacing = 0;
                TablaLogo.WidthPercentage = 95;
                TablaLogo.Border = 0;

                TablaCabecera.Alignment = Element.ALIGN_LEFT;
                TablaCabecera.Offset = 0;
                TablaCabecera.Spacing = 0;
                TablaCabecera.WidthPercentage = 95;
                TablaCabecera.Border = 0;

                TablaTitulo.Alignment = Element.ALIGN_LEFT;
                TablaTitulo.Offset = 0;
                TablaTitulo.Spacing = 0;
                TablaTitulo.WidthPercentage = 95;
                TablaTitulo.Border = 0;

                TablaTitulo1.Alignment = Element.ALIGN_LEFT;
                TablaTitulo1.Offset = 0;
                TablaTitulo1.Spacing = 0;
                TablaTitulo1.Padding = 2;
                TablaTitulo1.WidthPercentage = 95;
                TablaTitulo1.Border = 0;

                TablaTitulo2.Alignment = Element.ALIGN_LEFT;
                TablaTitulo2.Offset = 0;
                TablaTitulo2.Spacing = 0;
                TablaTitulo2.Padding = 2;
                TablaTitulo2.WidthPercentage = 95;
                TablaTitulo2.Border = 0;

                TablaDatos1.Offset = 0;
                TablaDatos1.Spacing = 0;
                TablaDatos1.WidthPercentage = 95;
                TablaDatos1.Alignment = Element.ALIGN_LEFT;
                TablaDatos1.Border = 0;

                TablaDatos2.Offset = 0;
                TablaDatos2.Spacing = 0;
                TablaDatos2.WidthPercentage = 95;
                TablaDatos2.Alignment = Element.ALIGN_LEFT;
                TablaDatos2.Border = 0;

                //20190717 rq050-19
                TablaDatos2A.Offset = 0;
                TablaDatos2A.Spacing = 0;
                TablaDatos2A.WidthPercentage = 95;
                TablaDatos2A.Alignment = Element.ALIGN_LEFT;
                TablaDatos2A.Border = 0;
                //20190717 fin rq050-19

                TablaDatos3.Alignment = Element.ALIGN_LEFT;
                TablaDatos3.Offset = 0;
                TablaDatos3.Spacing = 0;
                TablaDatos3.WidthPercentage = 95;
                TablaDatos3.Border = 0;

                TablaTexto.Alignment = Element.ALIGN_LEFT;
                TablaTexto.Offset = 0;
                TablaTexto.Spacing = 0;
                TablaTexto.WidthPercentage = 95;
                TablaTexto.Border = 0;

                TablaFinal.Alignment = Element.ALIGN_LEFT;
                TablaFinal.Offset = 0;
                TablaFinal.Spacing = 0;
                TablaFinal.WidthPercentage = 95;
                TablaFinal.Border = 0;

                Paragraph lpEspacios = new Paragraph(" ", FuenteDato1);

                // Se Pinta la Cabecera del Certificado
                Cell CellLogo1 = new Cell();
                CellLogo1.Border = 0;
                CellLogo1.BorderWidthLeft = 0;
                CellLogo1.BorderWidthTop = 0;
                CellLogo1.BorderWidthRight = 0;
                CellLogo1.BorderWidthBottom = 0;
                CellLogo1.AddElement(LogoVig);
                CellLogo1.VerticalAlignment = Element.ALIGN_BOTTOM;
                CellLogo1.HorizontalAlignment = Element.ALIGN_RIGHT;
                TablaLogo.AddCell(CellLogo1);

                Cell CellLogo2 = new Cell();
                CellLogo2.Border = 0;
                CellLogo2.BorderWidthLeft = 0;
                CellLogo2.BorderWidthTop = 0;
                CellLogo2.BorderWidthRight = 0;
                CellLogo2.BorderWidthBottom = 0;
                CellLogo2.AddElement(LogoBna);
                CellLogo2.VerticalAlignment = Element.ALIGN_BOTTOM;
                CellLogo2.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaLogo.AddCell(CellLogo2);

                Cell CellLogo3 = new Cell();
                CellLogo3.Border = 0;
                CellLogo3.BorderWidthLeft = 0;
                CellLogo3.BorderWidthTop = 0;
                CellLogo3.BorderWidthRight = 0;
                CellLogo3.BorderWidthBottom = 0;
                CellLogo3.AddElement(LogoGas);
                CellLogo3.VerticalAlignment = Element.ALIGN_BOTTOM;
                CellLogo3.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaLogo.AddCell(CellLogo3);

                loDocument.Add(TablaLogo);


                Cell CellTit1 = new Cell();
                CellTit1.Border = 0;
                CellTit1.BorderWidthLeft = 0;
                CellTit1.BorderWidthTop = 0;
                CellTit1.BorderWidthRight = 0;
                CellTit1.BorderWidthBottom = 0;
                CellTit1.AddElement(RayaI);
                CellTit1.VerticalAlignment = Element.ALIGN_BOTTOM;
                CellTit1.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaTitulo.AddCell(CellTit1);

                Cell CellTit2 = new Cell();
                CellTit2.Border = 0;
                CellTit2.BorderWidthLeft = 0;
                CellTit2.BorderWidthTop = 0;
                CellTit2.BorderWidthRight = 0;
                CellTit2.BorderWidthBottom = 0;
                CellTit2.AddElement(new Paragraph("CERTIFICADO PARA AUDITORIA", FuenteTitulo));
                CellTit2.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellTit2.HorizontalAlignment = Element.ALIGN_CENTER;
                TablaTitulo.AddCell(CellTit2);

                Cell CellTit3 = new Cell();
                CellTit3.Border = 0;
                CellTit3.BorderWidthLeft = 0;
                CellTit3.BorderWidthTop = 0;
                CellTit3.BorderWidthRight = 0;
                CellTit3.BorderWidthBottom = 0;
                CellTit3.AddElement(RayaD);
                CellTit3.VerticalAlignment = Element.ALIGN_BOTTOM;
                CellTit3.HorizontalAlignment = Element.ALIGN_RIGHT;
                TablaTitulo.AddCell(CellTit3);

                loDocument.Add(TablaTitulo);
                loDocument.Add(lpEspacios);

                Cell CellBlanco = new Cell();
                CellBlanco.Border = 0;
                CellBlanco.BorderWidthLeft = 0;
                CellBlanco.BorderWidthTop = 0;
                CellBlanco.BorderWidthRight = 0;
                CellBlanco.BorderWidthBottom = 0;
                CellBlanco.AddElement(new Paragraph(" ", FuenteEspacio));
                CellBlanco.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellBlanco.HorizontalAlignment = Element.ALIGN_CENTER;

                Cell CellAzul = new Cell();
                CellAzul.Border = 0;
                CellAzul.BorderWidthLeft = 0;
                CellAzul.BorderWidthTop = 0;
                CellAzul.BorderWidthRight = 0;
                CellAzul.BorderWidthBottom = 0;
                CellAzul.BackgroundColor = clAzul;
                CellAzul.VerticalAlignment = Element.ALIGN_TOP;
                CellAzul.AddElement(new Paragraph(" ", FuenteEspacio));
                CellAzul.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellAzul.HorizontalAlignment = Element.ALIGN_CENTER;

                Cell CellClaro = new Cell();
                CellClaro.Border = 0;
                CellClaro.BorderWidthLeft = 0;
                CellClaro.BorderWidthTop = 0;
                CellClaro.BorderWidthRight = 0;
                CellClaro.BorderWidthBottom = 0;
                CellClaro.BackgroundColor = clClaro;
                CellClaro.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellClaro.AddElement(new Paragraph("X", FuenteEspacioClaro));
                CellClaro.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellClaro.HorizontalAlignment = Element.ALIGN_CENTER;

                TablaCabecera.AddCell(CellBlanco);

                Cell CellCab1 = new Cell();
                CellCab1.Border = 0;
                CellCab1.BorderWidthLeft = 0;
                CellCab1.BorderWidthTop = 0;
                CellCab1.BorderWidthRight = 0;
                CellCab1.BorderWidthBottom = 0;
                CellCab1.AddElement(new Paragraph("Fecha de Negociación:", FuenteDato));
                CellCab1.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellCab1.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaCabecera.AddCell(CellCab1);

                Cell CellCab2 = new Cell();
                CellCab2.BorderWidthLeft = 0;
                CellCab2.BorderWidthTop = 0;
                CellCab2.BorderWidthRight = 0;
                CellCab2.BorderWidthBottom = 0;
                CellCab2.BorderColorBottom = clAzul;
                CellCab2.AddElement(new Paragraph(" " + lLector["fecha_contrato"].ToString(), FuenteDato1));
                CellCab2.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellCab2.HorizontalAlignment = Element.ALIGN_CENTER;
                TablaCabecera.AddCell(CellCab2);

                TablaCabecera.AddCell(CellBlanco);

                Cell CellCab3 = new Cell();
                CellCab3.Border = 0;
                CellCab3.BorderWidthLeft = 0;
                CellCab3.BorderWidthTop = 0;
                CellCab3.BorderWidthRight = 0;
                CellCab3.BorderWidthBottom = 0;
                CellCab3.AddElement(new Paragraph("  N° de Operación:", FuenteDato));
                CellCab3.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellCab3.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaCabecera.AddCell(CellCab3);

                Cell CellCab4 = new Cell();
                CellCab4.Border = 0;
                CellCab4.BorderWidthLeft = 0;
                CellCab4.BorderWidthTop = 0;
                CellCab4.BorderWidthRight = 0;
                CellCab4.BorderWidthBottom = 0;
                CellCab4.BorderColorBottom = clAzul;
                CellCab4.AddElement(new Paragraph(" " + lLector["numero_contrato"].ToString(), FuenteDato1));
                CellCab4.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellCab4.HorizontalAlignment = Element.ALIGN_CENTER;
                TablaCabecera.AddCell(CellCab4);

                TablaCabecera.AddCell(CellBlanco);
                TablaCabecera.AddCell(CellBlanco);

                Cell CellCab5 = new Cell();
                CellCab5.Border = 0;
                CellCab5.BorderWidthLeft = 0;
                CellCab5.BorderWidthTop = 0;
                CellCab5.BorderWidthRight = 0;
                CellCab5.BorderWidthBottom = 0;
                CellCab5.AddElement(new Paragraph("Fecha Expedición:", FuenteDato));
                CellCab5.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellCab5.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaCabecera.AddCell(CellCab5);

                Cell CellCab6 = new Cell();
                CellCab6.Border = 0;
                CellCab6.BorderWidthLeft = 0;
                CellCab6.BorderWidthTop = 0;
                CellCab6.BorderWidthRight = 0;
                CellCab6.BorderWidthBottom = 0;
                CellCab4.BorderColorBottom = clAzul;
                CellCab6.AddElement(new Paragraph(" " + lLector["fecha_expedicion"].ToString(), FuenteDato1));
                CellCab6.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellCab6.HorizontalAlignment = Element.ALIGN_CENTER;
                TablaCabecera.AddCell(CellCab6);

                loDocument.Add(TablaCabecera);
                loDocument.Add(lpEspacios);

                TablaTitulo1.AddCell(CellAzul);

                Cell CellTitA1 = new Cell();
                CellTitA1.Border = 0;
                CellTitA1.BorderWidthLeft = 0;
                CellTitA1.BorderWidthTop = 0;
                CellTitA1.BorderWidthRight = 0;
                CellTitA1.BorderWidthBottom = 0;
                CellTitA1.BackgroundColor = clAzul;
                CellTitA1.AddElement(new Paragraph("Vendedor:", FuenteTitulo1));
                CellTitA1.VerticalAlignment = Element.ALIGN_TOP;
                CellTitA1.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaTitulo1.AddCell(CellTitA1);

                Cell CellTitA2 = new Cell();
                CellTitA2.Border = 0;
                CellTitA2.BorderWidthLeft = 0;
                CellTitA2.BorderWidthTop = 0;
                CellTitA2.BorderWidthRight = 0;
                CellTitA2.BorderWidthBottom = 0;
                CellTitA2.BackgroundColor = clAzul;
                CellTitA2.AddElement(new Paragraph("Comprador:", FuenteTitulo1));
                CellTitA2.VerticalAlignment = Element.ALIGN_TOP;
                CellTitA2.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaTitulo1.AddCell(CellTitA2);

                loDocument.Add(TablaTitulo1);
                loDocument.Add(lpEspacios);

                Cell CellDatoA1 = new Cell();
                CellDatoA1.Border = 0;
                CellDatoA1.BorderWidthLeft = 0;
                CellDatoA1.BorderWidthTop = 0;
                CellDatoA1.BorderWidthRight = 0;
                CellDatoA1.BorderWidthBottom = 0;
                CellDatoA1.BackgroundColor = clClaro;
                CellDatoA1.AddElement(new Paragraph(" Participante:", FuenteDato));
                CellDatoA1.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA1.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA1);

                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA2 = new Cell();
                CellDatoA2.Border = 0;
                CellDatoA2.BorderWidthLeft = 0;
                CellDatoA2.BorderWidthTop = 0;
                CellDatoA2.BorderWidthRight = 0;
                CellDatoA2.BorderWidthBottom = 0;
                CellDatoA2.AddElement(new Paragraph(lLector["nom_vendedor"].ToString(), FuenteDato1));
                CellDatoA2.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA2.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA2);

                TablaDatos1.AddCell(CellDatoA1);
                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA4 = new Cell();
                CellDatoA4.Border = 0;
                CellDatoA4.BorderWidthLeft = 0;
                CellDatoA4.BorderWidthTop = 0;
                CellDatoA4.BorderWidthRight = 0;
                CellDatoA4.BorderWidthBottom = 0;
                CellDatoA4.AddElement(new Paragraph(lLector["nom_comprador"].ToString(), FuenteDato1));
                CellDatoA4.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA4.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA4);

                Cell CellDatoA5 = new Cell();
                CellDatoA5.Border = 0;
                CellDatoA5.BorderWidthLeft = 0;
                CellDatoA5.BorderWidthTop = 0;
                CellDatoA5.BorderWidthRight = 0;
                CellDatoA5.BorderWidthBottom = 0;
                CellDatoA5.BackgroundColor = clClaro;
                CellDatoA5.AddElement(new Paragraph(" Nit:", FuenteDato));
                CellDatoA5.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA5.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA5);

                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA6 = new Cell();
                CellDatoA6.Border = 0;
                CellDatoA6.BorderWidthLeft = 0;
                CellDatoA6.BorderWidthTop = 0;
                CellDatoA6.BorderWidthRight = 0;
                CellDatoA6.BorderWidthBottom = 0;
                CellDatoA6.AddElement(new Paragraph(lLector["nit_vendedor"].ToString(), FuenteDato1));
                CellDatoA6.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA6.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA6);

                TablaDatos1.AddCell(CellDatoA5);
                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA8 = new Cell();
                CellDatoA8.Border = 0;
                CellDatoA8.BorderWidthLeft = 0;
                CellDatoA8.BorderWidthTop = 0;
                CellDatoA8.BorderWidthRight = 0;
                CellDatoA8.BorderWidthBottom = 0;
                CellDatoA8.AddElement(new Paragraph(lLector["nit_comprador"].ToString(), FuenteDato1));
                CellDatoA8.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA8.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA8);

                Cell CellDatoA9 = new Cell();
                CellDatoA9.Border = 0;
                CellDatoA9.BorderWidthLeft = 0;
                CellDatoA9.BorderWidthTop = 0;
                CellDatoA9.BorderWidthRight = 0;
                CellDatoA9.BorderWidthBottom = 0;
                CellDatoA9.BackgroundColor = clClaro;
                CellDatoA9.AddElement(new Paragraph(" Tipo de participante:", FuenteDato));
                CellDatoA9.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA9.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA9);

                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA10 = new Cell();
                CellDatoA10.Border = 0;
                CellDatoA10.BorderWidthLeft = 0;
                CellDatoA10.BorderWidthTop = 0;
                CellDatoA10.BorderWidthRight = 0;
                CellDatoA10.BorderWidthBottom = 0;
                CellDatoA10.AddElement(new Paragraph(lLector["tipo_vendedor"].ToString(), FuenteDato1));
                CellDatoA10.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA10.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA10);

                TablaDatos1.AddCell(CellDatoA9);
                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA11 = new Cell();
                CellDatoA11.Border = 0;
                CellDatoA11.BorderWidthLeft = 0;
                CellDatoA11.BorderWidthTop = 0;
                CellDatoA11.BorderWidthRight = 0;
                CellDatoA11.BorderWidthBottom = 0;
                CellDatoA11.AddElement(new Paragraph(lLector["tipo_comprador"].ToString(), FuenteDato1));
                CellDatoA11.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA11.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA11);

                Cell CellDatoA12 = new Cell();
                CellDatoA12.Border = 0;
                CellDatoA12.BorderWidthLeft = 0;
                CellDatoA12.BorderWidthTop = 0;
                CellDatoA12.BorderWidthRight = 0;
                CellDatoA12.BorderWidthBottom = 0;
                CellDatoA12.BackgroundColor = clClaro;
                CellDatoA12.AddElement(new Paragraph(" Domiciliado en:", FuenteDato));
                CellDatoA12.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA12.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA12);

                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA13 = new Cell();
                CellDatoA13.Border = 0;
                CellDatoA13.BorderWidthLeft = 0;
                CellDatoA13.BorderWidthTop = 0;
                CellDatoA13.BorderWidthRight = 0;
                CellDatoA13.BorderWidthBottom = 0;
                CellDatoA13.AddElement(new Paragraph(lLector["direccion_vendedor"].ToString(), FuenteDato1));
                CellDatoA13.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA13.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA13);

                TablaDatos1.AddCell(CellDatoA12);
                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA15 = new Cell();
                CellDatoA15.Border = 0;
                CellDatoA15.BorderWidthLeft = 0;
                CellDatoA15.BorderWidthTop = 0;
                CellDatoA15.BorderWidthRight = 0;
                CellDatoA15.BorderWidthBottom = 0;
                CellDatoA15.AddElement(new Paragraph(lLector["direccion_comprador"].ToString(), FuenteDato1));
                CellDatoA15.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA15.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA15);

                Cell CellDatoA16 = new Cell();
                CellDatoA16.Border = 0;
                CellDatoA16.BorderWidthLeft = 0;
                CellDatoA16.BorderWidthTop = 0;
                CellDatoA16.BorderWidthRight = 0;
                CellDatoA16.BorderWidthBottom = 0;
                CellDatoA16.BackgroundColor = clClaro;
                CellDatoA16.AddElement(new Paragraph(" Mail:", FuenteDato));
                CellDatoA16.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA16.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA16);

                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA17 = new Cell();
                CellDatoA17.Border = 0;
                CellDatoA17.BorderWidthLeft = 0;
                CellDatoA17.BorderWidthTop = 0;
                CellDatoA17.BorderWidthRight = 0;
                CellDatoA17.BorderWidthBottom = 0;
                CellDatoA17.AddElement(new Paragraph(lLector["mail_v"].ToString(), FuenteDato1));
                CellDatoA17.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA17.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA17);

                TablaDatos1.AddCell(CellDatoA16);
                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA19 = new Cell();
                CellDatoA19.Border = 0;
                CellDatoA19.BorderWidthLeft = 0;
                CellDatoA19.BorderWidthTop = 0;
                CellDatoA19.BorderWidthRight = 0;
                CellDatoA19.BorderWidthBottom = 0;
                CellDatoA19.AddElement(new Paragraph(lLector["mail_c"].ToString(), FuenteDato1));
                CellDatoA19.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA19.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA19);

                Cell CellDatoA20 = new Cell();
                CellDatoA20.Border = 0;
                CellDatoA20.BorderWidthLeft = 0;
                CellDatoA20.BorderWidthTop = 0;
                CellDatoA20.BorderWidthRight = 0;
                CellDatoA20.BorderWidthBottom = 0;
                CellDatoA20.BackgroundColor = clClaro;
                CellDatoA20.AddElement(new Paragraph(" Teléfono:", FuenteDato));
                CellDatoA20.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA20.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA20);

                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA21 = new Cell();
                CellDatoA21.Border = 0;
                CellDatoA21.BorderWidthLeft = 0;
                CellDatoA21.BorderWidthTop = 0;
                CellDatoA21.BorderWidthRight = 0;
                CellDatoA21.BorderWidthBottom = 0;
                CellDatoA21.AddElement(new Paragraph(lLector["telefono_v"].ToString(), FuenteDato1));
                CellDatoA21.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA21.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA21);

                TablaDatos1.AddCell(CellDatoA20);
                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA23 = new Cell();
                CellDatoA23.Border = 0;
                CellDatoA23.BorderWidthLeft = 0;
                CellDatoA23.BorderWidthTop = 0;
                CellDatoA23.BorderWidthRight = 0;
                CellDatoA23.BorderWidthBottom = 0;
                CellDatoA23.AddElement(new Paragraph(lLector["telefono_c"].ToString(), FuenteDato1));
                CellDatoA23.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA23.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA23);

                Cell CellDatoA24 = new Cell();
                CellDatoA24.Border = 0;
                CellDatoA24.BorderWidthLeft = 0;
                CellDatoA24.BorderWidthTop = 0;
                CellDatoA24.BorderWidthRight = 0;
                CellDatoA24.BorderWidthBottom = 0;
                CellDatoA24.BackgroundColor = clClaro;
                CellDatoA24.AddElement(new Paragraph(" Celular:", FuenteDato));
                CellDatoA24.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA24.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA24);

                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA25 = new Cell();
                CellDatoA25.Border = 0;
                CellDatoA25.BorderWidthLeft = 0;
                CellDatoA25.BorderWidthTop = 0;
                CellDatoA25.BorderWidthRight = 0;
                CellDatoA25.BorderWidthBottom = 0;
                CellDatoA25.AddElement(new Paragraph(lLector["celular_v"].ToString(), FuenteDato1));
                CellDatoA25.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA25.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA25);

                TablaDatos1.AddCell(CellDatoA24);
                TablaDatos1.AddCell(CellBlanco);

                Cell CellDatoA27 = new Cell();
                CellDatoA27.Border = 0;
                CellDatoA27.BorderWidthLeft = 0;
                CellDatoA27.BorderWidthTop = 0;
                CellDatoA27.BorderWidthRight = 0;
                CellDatoA27.BorderWidthBottom = 0;
                CellDatoA27.AddElement(new Paragraph(lLector["celular_c"].ToString(), FuenteDato1));
                CellDatoA27.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoA27.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos1.AddCell(CellDatoA27);

                //20201207 subasta UVCP
                if (lLector["codigo_tipo_subasta"].ToString() == "2")
                {
                    Cell CellDatoB24 = new Cell();
                    CellDatoB24.Border = 0;
                    CellDatoB24.BorderWidthLeft = 0;
                    CellDatoB24.BorderWidthTop = 0;
                    CellDatoB24.BorderWidthRight = 0;
                    CellDatoB24.BorderWidthBottom = 0;
                    CellDatoB24.BackgroundColor = clClaro;
                    CellDatoB24.AddElement(new Paragraph(" Entidad Bancaria:", FuenteDato));
                    CellDatoB24.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB24.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos1.AddCell(CellDatoB24);

                    TablaDatos1.AddCell(CellBlanco);

                    Cell CellDatoB25 = new Cell();
                    CellDatoB25.Border = 0;
                    CellDatoB25.BorderWidthLeft = 0;
                    CellDatoB25.BorderWidthTop = 0;
                    CellDatoB25.BorderWidthRight = 0;
                    CellDatoB25.BorderWidthBottom = 0;
                    CellDatoB25.AddElement(new Paragraph(lLector["banco_pago"].ToString(), FuenteDato1));
                    CellDatoB25.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB25.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos1.AddCell(CellDatoB25);

                    TablaDatos1.AddCell(CellClaro);
                    TablaDatos1.AddCell(CellBlanco);
                    TablaDatos1.AddCell(CellBlanco);

                    Cell CellDatoC24 = new Cell();
                    CellDatoC24.Border = 0;
                    CellDatoC24.BorderWidthLeft = 0;
                    CellDatoC24.BorderWidthTop = 0;
                    CellDatoC24.BorderWidthRight = 0;
                    CellDatoC24.BorderWidthBottom = 0;
                    CellDatoC24.BackgroundColor = clClaro;
                    CellDatoC24.AddElement(new Paragraph(" Tipo de Cuenta:", FuenteDato));
                    CellDatoC24.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoC24.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos1.AddCell(CellDatoC24);

                    TablaDatos1.AddCell(CellBlanco);

                    Cell CellDatoC25 = new Cell();
                    CellDatoC25.Border = 0;
                    CellDatoC25.BorderWidthLeft = 0;
                    CellDatoC25.BorderWidthTop = 0;
                    CellDatoC25.BorderWidthRight = 0;
                    CellDatoC25.BorderWidthBottom = 0;
                    CellDatoC25.AddElement(new Paragraph(lLector["tipo_cuenta"].ToString(), FuenteDato1));
                    CellDatoC25.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoC25.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos1.AddCell(CellDatoC25);

                    TablaDatos1.AddCell(CellClaro);
                    TablaDatos1.AddCell(CellBlanco);
                    TablaDatos1.AddCell(CellBlanco);


                    Cell CellDatoD24 = new Cell();
                    CellDatoD24.Border = 0;
                    CellDatoD24.BorderWidthLeft = 0;
                    CellDatoD24.BorderWidthTop = 0;
                    CellDatoD24.BorderWidthRight = 0;
                    CellDatoD24.BorderWidthBottom = 0;
                    CellDatoD24.BackgroundColor = clClaro;
                    CellDatoD24.AddElement(new Paragraph(" No. de Cuenta:", FuenteDato));
                    CellDatoD24.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoD24.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos1.AddCell(CellDatoD24);

                    TablaDatos1.AddCell(CellBlanco);

                    Cell CellDatoD25 = new Cell();
                    CellDatoD25.Border = 0;
                    CellDatoD25.BorderWidthLeft = 0;
                    CellDatoD25.BorderWidthTop = 0;
                    CellDatoD25.BorderWidthRight = 0;
                    CellDatoD25.BorderWidthBottom = 0;
                    CellDatoD25.AddElement(new Paragraph(lLector["cuenta_pago"].ToString(), FuenteDato1));
                    CellDatoD25.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoD25.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos1.AddCell(CellDatoD25);

                    TablaDatos1.AddCell(CellClaro);
                    TablaDatos1.AddCell(CellBlanco);
                    TablaDatos1.AddCell(CellBlanco);
                }
                //20201207 fin subasta UVCP

                //espacio en blanco
                TablaDatos1.AddCell(CellClaro);
                TablaDatos1.AddCell(CellBlanco);
                TablaDatos1.AddCell(CellBlanco);
                TablaDatos1.AddCell(CellClaro);
                TablaDatos1.AddCell(CellBlanco);
                TablaDatos1.AddCell(CellBlanco);

                loDocument.Add(TablaDatos1);
                loDocument.Add(lpEspacios);

                TablaTitulo2.AddCell(CellAzul);

                Cell CellTitB1 = new Cell();
                CellTitB1.Border = 0;
                CellTitB1.BorderWidthLeft = 0;
                CellTitB1.BorderWidthTop = 0;
                CellTitB1.BorderWidthRight = 0;
                CellTitB1.BorderWidthBottom = 0;
                CellTitB1.BackgroundColor = clAzul;
                CellTitB1.AddElement(new Paragraph("Han negociado el producto que se detalla a continuación:", FuenteTitulo1));
                CellTitB1.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellTitB1.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaTitulo2.AddCell(CellTitB1);

                loDocument.Add(TablaTitulo2);
                loDocument.Add(lpEspacios);
                //20190717 rq050-19
                if (lLector["codigo_modalidad"].ToString() != "22")
                {
                    Cell CellDatoB1 = new Cell();
                    CellDatoB1.Border = 0;
                    CellDatoB1.BorderWidthLeft = 0;
                    CellDatoB1.BorderWidthTop = 0;
                    CellDatoB1.BorderWidthRight = 0;
                    CellDatoB1.BorderWidthBottom = 0;
                    CellDatoB1.BackgroundColor = clClaro;
                    CellDatoB1.AddElement(new Paragraph(" Producto:", FuenteDato));
                    CellDatoB1.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB1.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2.AddCell(CellDatoB1);

                    Cell CellDatoB2 = new Cell();
                    CellDatoB2.Border = 0;
                    CellDatoB2.BorderWidthLeft = 0;
                    CellDatoB2.BorderWidthTop = 0;
                    CellDatoB2.BorderWidthRight = 0;
                    CellDatoB2.BorderWidthBottom = 0;
                    CellDatoB2.AddElement(new Paragraph(" " + lLector["producto"].ToString(), FuenteDato1));
                    CellDatoB2.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB2.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2.AddCell(CellDatoB2);

                    Cell CellDatoB3 = new Cell();
                    CellDatoB3.Border = 0;
                    CellDatoB3.BorderWidthLeft = 0;
                    CellDatoB3.BorderWidthTop = 0;
                    CellDatoB3.BorderWidthRight = 0;
                    CellDatoB3.BorderWidthBottom = 0;
                    CellDatoB3.BackgroundColor = clClaro;
                    CellDatoB3.AddElement(new Paragraph(" Modalidad Contractual:", FuenteDato));
                    CellDatoB3.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB3.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2.AddCell(CellDatoB3);

                    Cell CellDatoB4 = new Cell();
                    CellDatoB4.Border = 0;
                    CellDatoB4.BorderWidthLeft = 0;
                    CellDatoB4.BorderWidthTop = 0;
                    CellDatoB4.BorderWidthRight = 0;
                    CellDatoB4.BorderWidthBottom = 0;
                    CellDatoB4.AddElement(new Paragraph(" " + lLector["modalidad"].ToString(), FuenteDato1));
                    CellDatoB4.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB4.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2.AddCell(CellDatoB4);

                    Cell CellDatoB5 = new Cell();
                    CellDatoB5.Border = 0;
                    CellDatoB5.BorderWidthLeft = 0;
                    CellDatoB5.BorderWidthTop = 0;
                    CellDatoB5.BorderWidthRight = 0;
                    CellDatoB5.BorderWidthBottom = 0;
                    CellDatoB5.BackgroundColor = clClaro;
                    CellDatoB5.AddElement(new Paragraph(" Ruta/Punto de entrega:", FuenteDato));
                    CellDatoB5.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB5.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2.AddCell(CellDatoB5);

                    Cell CellDatoB6 = new Cell();
                    CellDatoB6.Border = 0;
                    CellDatoB6.BorderWidthLeft = 0;
                    CellDatoB6.BorderWidthTop = 0;
                    CellDatoB6.BorderWidthRight = 0;
                    CellDatoB6.BorderWidthBottom = 0;
                    CellDatoB6.AddElement(new Paragraph(" " + lLector["punto_entrega"].ToString(), FuenteDato1));
                    CellDatoB6.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB6.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2.AddCell(CellDatoB6);

                    Cell CellDatoB7 = new Cell();
                    CellDatoB7.Border = 0;
                    CellDatoB7.BorderWidthLeft = 0;
                    CellDatoB7.BorderWidthTop = 0;
                    CellDatoB7.BorderWidthRight = 0;
                    CellDatoB7.BorderWidthBottom = 0;
                    CellDatoB7.BackgroundColor = clClaro;
                    CellDatoB7.AddElement(new Paragraph(" Duración:", FuenteDato));
                    CellDatoB7.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB7.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2.AddCell(CellDatoB7);

                    Cell CellDatoB8 = new Cell();
                    CellDatoB8.Border = 0;
                    CellDatoB8.BorderWidthLeft = 0;
                    CellDatoB8.BorderWidthTop = 0;
                    CellDatoB8.BorderWidthRight = 0;
                    CellDatoB8.BorderWidthBottom = 0;
                    CellDatoB8.AddElement(new Paragraph(" " + lLector["duracion"].ToString(), FuenteDato1));
                    CellDatoB8.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB8.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2.AddCell(CellDatoB8);

                    //20220920 ajsute certificado
                    Cell CellDatoB9 = new Cell();
                    CellDatoB9.Border = 0;
                    CellDatoB9.BorderWidthLeft = 0;
                    CellDatoB9.BorderWidthTop = 0;
                    CellDatoB9.BorderWidthRight = 0;
                    CellDatoB9.BorderWidthBottom = 0;
                    CellDatoB9.BackgroundColor = clClaro;
                    CellDatoB9.AddElement(new Paragraph(" Tipo de Subasta:", FuenteDato));
                    CellDatoB9.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB9.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2.AddCell(CellDatoB9);

                    //20220920 ajsute certificado
                    Cell CellDatoB10 = new Cell();
                    CellDatoB10.Border = 0;
                    CellDatoB10.BorderWidthLeft = 0;
                    CellDatoB10.BorderWidthTop = 0;
                    CellDatoB10.BorderWidthRight = 0;
                    CellDatoB10.BorderWidthBottom = 0;
                    CellDatoB10.AddElement(new Paragraph(" " + lLector["desc_tipo_rueda"].ToString(), FuenteDato1));
                    CellDatoB10.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB10.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2.AddCell(CellDatoB10);

                    //espacio en blanco
                    TablaDatos2.AddCell(CellClaro);
                    TablaDatos2.AddCell(CellBlanco);

                    loDocument.Add(TablaDatos2);
                    loDocument.Add(lpEspacios);

                }
                else  //20190717 rq050-19
                {
                    Cell CellDatoB1 = new Cell();
                    CellDatoB1.Border = 0;
                    CellDatoB1.BorderWidthLeft = 0;
                    CellDatoB1.BorderWidthTop = 0;
                    CellDatoB1.BorderWidthRight = 0;
                    CellDatoB1.BorderWidthBottom = 0;
                    CellDatoB1.BackgroundColor = clClaro;
                    CellDatoB1.AddElement(new Paragraph(" Producto:", FuenteDato));
                    CellDatoB1.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB1.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB1);

                    Cell CellDatoB2 = new Cell();
                    CellDatoB2.Border = 0;
                    CellDatoB2.BorderWidthLeft = 0;
                    CellDatoB2.BorderWidthTop = 0;
                    CellDatoB2.BorderWidthRight = 0;
                    CellDatoB2.BorderWidthBottom = 0;
                    CellDatoB2.AddElement(new Paragraph(" " + lLector["producto"].ToString(), FuenteDato1));
                    CellDatoB2.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB2.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB2);

                    Cell CellDatoB1A = new Cell();
                    CellDatoB1A.Border = 0;
                    CellDatoB1A.BorderWidthLeft = 0;
                    CellDatoB1A.BorderWidthTop = 0;
                    CellDatoB1A.BorderWidthRight = 0;
                    CellDatoB1A.BorderWidthBottom = 0;
                    CellDatoB1A.BackgroundColor = clClaro;
                    CellDatoB1A.AddElement(new Paragraph(" Firmeza Mínima:", FuenteDato));
                    CellDatoB1A.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB1A.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB1A);

                    Cell CellDatoB2A = new Cell();
                    CellDatoB2A.Border = 0;
                    CellDatoB2A.BorderWidthLeft = 0;
                    CellDatoB2A.BorderWidthTop = 0;
                    CellDatoB2A.BorderWidthRight = 0;
                    CellDatoB2A.BorderWidthBottom = 0;
                    CellDatoB2A.AddElement(new Paragraph(" " + lLector["firmeza_minima"].ToString(), FuenteDato1));
                    CellDatoB2A.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB2A.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB2A);

                    Cell CellDatoB3 = new Cell();
                    CellDatoB3.Border = 0;
                    CellDatoB3.BorderWidthLeft = 0;
                    CellDatoB3.BorderWidthTop = 0;
                    CellDatoB3.BorderWidthRight = 0;
                    CellDatoB3.BorderWidthBottom = 0;
                    CellDatoB3.BackgroundColor = clClaro;
                    CellDatoB3.AddElement(new Paragraph(" Modalidad Contractual:", FuenteDato));
                    CellDatoB3.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB3.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB3);

                    Cell CellDatoB4 = new Cell();
                    CellDatoB4.Border = 0;
                    CellDatoB4.BorderWidthLeft = 0;
                    CellDatoB4.BorderWidthTop = 0;
                    CellDatoB4.BorderWidthRight = 0;
                    CellDatoB4.BorderWidthBottom = 0;
                    CellDatoB4.AddElement(new Paragraph(" " + lLector["modalidad"].ToString(), FuenteDato1));
                    CellDatoB4.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB4.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB4);

                    TablaDatos2A.AddCell(CellClaro);
                    TablaDatos2A.AddCell(CellBlanco);

                    Cell CellDatoB5 = new Cell();
                    CellDatoB5.Border = 0;
                    CellDatoB5.BorderWidthLeft = 0;
                    CellDatoB5.BorderWidthTop = 0;
                    CellDatoB5.BorderWidthRight = 0;
                    CellDatoB5.BorderWidthBottom = 0;
                    CellDatoB5.BackgroundColor = clClaro;
                    CellDatoB5.AddElement(new Paragraph(" Ruta/Punto de entrega:", FuenteDato));
                    CellDatoB5.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB5.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB5);

                    Cell CellDatoB6 = new Cell();
                    CellDatoB6.Border = 0;
                    CellDatoB6.BorderWidthLeft = 0;
                    CellDatoB6.BorderWidthTop = 0;
                    CellDatoB6.BorderWidthRight = 0;
                    CellDatoB6.BorderWidthBottom = 0;
                    CellDatoB6.AddElement(new Paragraph(" " + lLector["punto_entrega"].ToString(), FuenteDato1));
                    CellDatoB6.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB6.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB6);

                    TablaDatos2A.AddCell(CellClaro);
                    TablaDatos2A.AddCell(CellBlanco);

                    Cell CellDatoB7 = new Cell();
                    CellDatoB7.Border = 0;
                    CellDatoB7.BorderWidthLeft = 0;
                    CellDatoB7.BorderWidthTop = 0;
                    CellDatoB7.BorderWidthRight = 0;
                    CellDatoB7.BorderWidthBottom = 0;
                    CellDatoB7.BackgroundColor = clClaro;
                    CellDatoB7.AddElement(new Paragraph(" Duración:", FuenteDato));
                    CellDatoB7.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB7.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB7);

                    Cell CellDatoB8 = new Cell();
                    CellDatoB8.Border = 0;
                    CellDatoB8.BorderWidthLeft = 0;
                    CellDatoB8.BorderWidthTop = 0;
                    CellDatoB8.BorderWidthRight = 0;
                    CellDatoB8.BorderWidthBottom = 0;
                    CellDatoB8.AddElement(new Paragraph(" " + lLector["duracion"].ToString(), FuenteDato1));
                    CellDatoB8.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB8.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB8);

                    //20220920 ajuste certificado
                    Cell CellDatoB9 = new Cell();
                    CellDatoB9.Border = 0;
                    CellDatoB9.BorderWidthLeft = 0;
                    CellDatoB9.BorderWidthTop = 0;
                    CellDatoB9.BorderWidthRight = 0;
                    CellDatoB9.BorderWidthBottom = 0;
                    CellDatoB9.BackgroundColor = clClaro;
                    CellDatoB9.AddElement(new Paragraph(" Tipo de Subasta:", FuenteDato));
                    CellDatoB9.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB9.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB9);

                    //20220920 ajuste certificado
                    Cell CellDatoB10 = new Cell();
                    CellDatoB10.Border = 0;
                    CellDatoB10.BorderWidthLeft = 0;
                    CellDatoB10.BorderWidthTop = 0;
                    CellDatoB10.BorderWidthRight = 0;
                    CellDatoB10.BorderWidthBottom = 0;
                    CellDatoB10.AddElement(new Paragraph(" " + lLector["desc_tipo_rueda"].ToString(), FuenteDato1));
                    CellDatoB10.VerticalAlignment = Element.ALIGN_MIDDLE;
                    CellDatoB10.HorizontalAlignment = Element.ALIGN_LEFT;
                    TablaDatos2A.AddCell(CellDatoB10);

                    TablaDatos2A.AddCell(CellClaro);
                    TablaDatos2A.AddCell(CellBlanco);

                    //espacio en blanco
                    TablaDatos2A.AddCell(CellClaro);
                    TablaDatos2A.AddCell(CellBlanco);

                    TablaDatos2A.AddCell(CellClaro);
                    TablaDatos2A.AddCell(CellBlanco);

                    loDocument.Add(TablaDatos2A);
                    loDocument.Add(lpEspacios);

                }


                TablaTitulo2.DeleteAllRows();
                TablaTitulo2.AddCell(CellAzul);

                Cell CellTitC1 = new Cell();
                CellTitC1.Border = 0;
                CellTitC1.BorderWidthLeft = 0;
                CellTitC1.BorderWidthTop = 0;
                CellTitC1.BorderWidthRight = 0;
                CellTitC1.BorderWidthBottom = 0;
                CellTitC1.BackgroundColor = clAzul;
                CellTitC1.AddElement(new Paragraph("Con los siguientes resultados:", FuenteTitulo1));
                CellTitC1.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellTitC1.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaTitulo2.AddCell(CellTitC1);

                loDocument.Add(TablaTitulo2);

                Cell CellDatoC1 = new Cell();
                CellDatoC1.Border = 0;
                CellDatoC1.BorderWidthLeft = 0;
                CellDatoC1.BorderWidthTop = 0;
                CellDatoC1.BorderWidthRight = 0;
                CellDatoC1.BorderWidthBottom = 0;
                CellDatoC1.BackgroundColor = clClaro;
                CellDatoC1.AddElement(new Paragraph(" Cantidad/ Capacidad asignada:", FuenteDato));
                CellDatoC1.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoC1.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos3.AddCell(CellDatoC1);

                Cell CellDatoC2 = new Cell();
                CellDatoC2.Border = 0;
                CellDatoC2.BorderWidthLeft = 0;
                CellDatoC2.BorderWidthTop = 0;
                CellDatoC2.BorderWidthRight = 0;
                CellDatoC2.BorderWidthBottom = 0;
                CellDatoC2.AddElement(new Paragraph(" " + lLector["cantidad"].ToString(), FuenteDato1));
                CellDatoC2.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoC2.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos3.AddCell(CellDatoC2);

                Cell CellDatoC3 = new Cell();
                CellDatoC3.Border = 0;
                CellDatoC3.BorderWidthLeft = 0;
                CellDatoC3.BorderWidthTop = 0;
                CellDatoC3.BorderWidthRight = 0;
                CellDatoC3.BorderWidthBottom = 0;
                CellDatoC3.BackgroundColor = clClaro;
                CellDatoC3.AddElement(new Paragraph(" Unidad:", FuenteDato));
                CellDatoC3.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoC3.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos3.AddCell(CellDatoC3);

                Cell CellDatoC4 = new Cell();
                CellDatoC4.Border = 0;
                CellDatoC4.BorderWidthLeft = 0;
                CellDatoC4.BorderWidthTop = 0;
                CellDatoC4.BorderWidthRight = 0;
                CellDatoC4.BorderWidthBottom = 0;
                CellDatoC4.AddElement(new Paragraph(" " + lLector["unidad_medida"].ToString(), FuenteDato1));
                CellDatoC4.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoC4.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos3.AddCell(CellDatoC4);

                Cell CellDatoC5 = new Cell();
                CellDatoC5.Border = 0;
                CellDatoC5.BorderWidthLeft = 0;
                CellDatoC5.BorderWidthTop = 0;
                CellDatoC5.BorderWidthRight = 0;
                CellDatoC5.BorderWidthBottom = 0;
                CellDatoC5.BackgroundColor = clClaro;
                CellDatoC5.AddElement(new Paragraph(" Precio de adjudicación (US$/MBTU)(Moneda Vigente/KPC):", FuenteDato)); //20220828
                CellDatoC5.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoC5.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos3.AddCell(CellDatoC5);

                Cell CellDatoC6 = new Cell();
                CellDatoC6.Border = 0;
                CellDatoC6.BorderWidthLeft = 0;
                CellDatoC6.BorderWidthTop = 0;
                CellDatoC6.BorderWidthRight = 0;
                CellDatoC6.BorderWidthBottom = 0;
                CellDatoC6.AddElement(new Paragraph(lLector["precio"].ToString(), FuenteDato1)); //20220828
                CellDatoC6.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoC6.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos3.AddCell(CellDatoC6);

                Cell CellDatoC7 = new Cell();
                CellDatoC7.Border = 0;
                CellDatoC7.BorderWidthLeft = 0;
                CellDatoC7.BorderWidthTop = 0;
                CellDatoC7.BorderWidthRight = 0;
                CellDatoC7.BorderWidthBottom = 0;
                CellDatoC7.BackgroundColor = clClaro;
                CellDatoC7.AddElement(new Paragraph(" Valor del Contrato (Moneda vigente):", FuenteDato)); //20220828
                CellDatoC7.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoC7.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos3.AddCell(CellDatoC7);

                Cell CellDatoC8 = new Cell();
                CellDatoC8.Border = 0;
                CellDatoC8.BorderWidthLeft = 0;
                CellDatoC8.BorderWidthTop = 0;
                CellDatoC8.BorderWidthRight = 0;
                CellDatoC8.BorderWidthBottom = 0;
                CellDatoC8.AddElement(new Paragraph(Convert.ToDecimal(lLector["valor"].ToString()).ToString("###,###,###,###,###,###.00"), FuenteDato2)); //20220828
                CellDatoC8.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellDatoC8.HorizontalAlignment = Element.ALIGN_LEFT;
                TablaDatos3.AddCell(CellDatoC8);

                //espacio en blanco
                TablaDatos3.AddCell(CellClaro);
                TablaDatos3.AddCell(CellBlanco);

                loDocument.Add(TablaDatos3);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);

                //loDocument.Add(lpEspacios); 20200714 ajsute
                //loDocument.Add(lpEspacios); 20200714 ajsute

                Paragraph lpDetNota = new Paragraph("NOTA: El vendedor y el respectivo comprador serán responsables de suscribir el contrato de compraventa en donde se deberá reflejar los resultados la presente subasta o negociación.  De igual forma, coordinarán los aspectos operativos requeridos, tales como el proceso de nominación, conforme a la regulación vigente.", FuenteDato2);

                //Cell CellLogo5g = new Cell();
                //CellLogo5g.Border = 0;
                //CellLogo5g.BorderWidthLeft = 0;
                //CellLogo5g.BorderWidthTop = 0;
                //CellLogo5g.BorderWidthRight = 0;
                //CellLogo5g.BorderWidthBottom = 0;
                //CellLogo5g.AddElement(lpAclara);
                //CellLogo5g.VerticalAlignment = Element.ALIGN_TOP;
                //CellLogo5g.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                //TablaTexto.AddCell(CellLogo5g);

                //Cell CellLogo5e = new Cell();
                //CellLogo5e.Border = 0;
                //CellLogo5e.BorderWidthLeft = 0;
                //CellLogo5e.BorderWidthTop = 0;
                //CellLogo5e.BorderWidthRight = 0;
                //CellLogo5e.BorderWidthBottom = 0;
                //CellLogo5e.AddElement(lpEspacios);
                //CellLogo5e.VerticalAlignment = Element.ALIGN_TOP;
                //CellLogo5e.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                //TablaTexto.AddCell(CellLogo5e);

                //20201207
                int lAlinea = Element.ALIGN_CENTER;
                if (lLector["codigo_tipo_subasta"].ToString() == "2")
                    lAlinea = Element.ALIGN_LEFT;
                // fin 20201207


                Cell CellLogo6g = new Cell();
                CellLogo6g.Border = 0;
                CellLogo6g.BorderWidthLeft = 0;
                CellLogo6g.BorderWidthTop = 0;
                CellLogo6g.BorderWidthRight = 0;
                CellLogo6g.BorderWidthBottom = 0;
                CellLogo6g.AddElement(lpDetNota);
                CellLogo6g.VerticalAlignment = Element.ALIGN_TOP;
                CellLogo6g.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                TablaTexto.AddCell(CellLogo6g);

                //20200714 ajuste
                Cell CellBlanco1 = new Cell();
                CellBlanco1.Border = 0;
                CellBlanco1.BorderWidthLeft = 0;
                CellBlanco1.BorderWidthTop = 0;
                CellBlanco1.BorderWidthRight = 0;
                CellBlanco1.BorderWidthBottom = 0;
                CellBlanco1.AddElement(new Paragraph("X", FuenteTitulo1));
                CellBlanco1.VerticalAlignment = Element.ALIGN_MIDDLE;
                CellBlanco1.HorizontalAlignment = Element.ALIGN_CENTER;

                TablaTexto.AddCell(CellBlanco1);
                //TablaTexto.AddCell(CellBlanco1); //20220920 ajsute certificado
                //20200714 fin ajuste

                Cell CellLogo5i = new Cell();
                CellLogo5i.Border = 0;
                CellLogo5i.BorderWidthLeft = 0;
                CellLogo5i.BorderWidthTop = 0;
                CellLogo5i.BorderWidthRight = 0;
                CellLogo5i.BorderWidthBottom = 0;
                CellLogo5i.AddElement(ArchivoFirma);
                CellLogo5i.VerticalAlignment = Element.ALIGN_TOP;
                CellLogo5i.HorizontalAlignment = lAlinea; //20201207
                TablaTexto.AddCell(CellLogo5i);

                loDocument.Add(TablaTexto);
                TablaTexto.DeleteAllRows();


                Paragraph lpRaya = new Paragraph("___________________________", FuenteDato1); //20201207

                //TablaTexto.AddCell(CellBlanco);

                Cell CellLogo5d = new Cell();
                CellLogo5d.Border = 0;
                CellLogo5d.BorderWidthLeft = 0;
                CellLogo5d.BorderWidthTop = 0;
                CellLogo5d.BorderWidthRight = 0;
                CellLogo5d.BorderWidthBottom = 0;
                CellLogo5d.AddElement(lpRaya);
                CellLogo5d.VerticalAlignment = Element.ALIGN_TOP;
                CellLogo5d.HorizontalAlignment = lAlinea; //20201207
                TablaTexto.AddCell(CellLogo5d);

                //TablaTexto.AddCell(CellBlanco);

                Paragraph lpNomFir = new Paragraph(lsNomFirma, FuenteDato1);
                Paragraph lpCarFir = new Paragraph(lsCarFirma, FuenteDato1);
                Cell CellLogo5t = new Cell();
                CellLogo5t.Border = 0;
                CellLogo5t.BorderWidthLeft = 0;
                CellLogo5t.BorderWidthTop = 0;
                CellLogo5t.BorderWidthRight = 0;
                CellLogo5t.BorderWidthBottom = 0;
                CellLogo5t.AddElement(lpNomFir);
                CellLogo5t.AddElement(lpCarFir);
                CellLogo5t.AddElement(lpEspacios);
                CellLogo5t.VerticalAlignment = Element.ALIGN_TOP;
                CellLogo5t.HorizontalAlignment = lAlinea; //20201207
                TablaTexto.AddCell(CellLogo5t);

                loDocument.Add(TablaTexto);
                TablaTexto.DeleteAllRows();
                loDocument.Add(TablaTexto);

                loDocument.NewPage();
            }
            lLector.Close();
            lLector.Dispose();

            loDocument.Close();
            lConexion.Cerrar();
            return lsArchivo;
        }
        catch (Exception ex)
        {
            lConexion.Cerrar();
            loDocument.Close();
            return "ERROR - Al Generar el PDF. " + ex.Message.ToString();
        }
    }
}
