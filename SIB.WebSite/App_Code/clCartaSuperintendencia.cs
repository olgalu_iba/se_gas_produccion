﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using SIB.BASE.Negocio;
using System.Text.RegularExpressions;
using iTextSharp.text;
using iTextSharp.text.pdf;

/// <summary>
/// Clase que Genera el PDF de lal Carta de la SUperintendencias para enviar por correo electronico
/// </summary>
public class clCartaSuperintendencia
{
    public clCartaSuperintendencia()
    {
        //
        // TODO: Add constructor logic here
        ///
    }
    /// <summary>
    /// Metodo que Genera el archivo PDF de la Carta a la Superintendencia
    /// </summary>
    /// <param name="liCodigoCarta">Código de la Carta</param>
    /// <param name="lsInfo">Clase de la Session</param>
    /// <returns>Ruta de almacenamiento del archivo</returns>
    public string generarCartas(int liCodigoCarta, InfoSessionVO lsInfo)
    {
        System.Web.SessionState.HttpSessionState session = HttpContext.Current.Session;
        clConexion lConexion = new clConexion(lsInfo);
        clConexion lConexion1 = new clConexion(lsInfo);
        SqlDataReader lLector;
        SqlDataReader lLector1;
        SqlCommand lComando;
        SqlCommand lComando1;
        // Definicion de las variables
        string oRutaCont = ConfigurationManager.AppSettings["RutaCartas"].ToString();
        //string oRutaFont = ConfigurationManager.AppSettings["RutaFont"].ToString();
        string oRutaImg = ConfigurationManager.AppSettings["RutaIMG"].ToString();
        string lsNomDestino = "";
        string lsCargoDestino = "";
        string lsEmpresaDestino = "";
        string lsDireccionDestino = "";
        string lsCiudadDestino = "";
        string lsTextoEncabezadoCarta = "";
        string lsNomFirmaCarta = "";
        string lsCargoFirmaCarta = "";
        string lsArchivoFirmaCarta = "";
        string lsMes = "";
        string lsCodSeccion = "0";
        bool blEncontro = false;

        lConexion.Abrir();
        lComando = new SqlCommand();
        lComando1 = new SqlCommand();
        // Obtengo los Parametros de la Carta
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_GetCartaSuperintendencia";
        lComando.Parameters.Add("@P_codigo_carta", SqlDbType.Int).Value = liCodigoCarta;
        lLector = lComando.ExecuteReader();
        if (lLector.HasRows)
        {
            while (lLector.Read())
            {
                lsNomDestino = lLector["nombre_destinatario"].ToString();
                lsCargoDestino = lLector["cargo_destinatario"].ToString();
                lsEmpresaDestino = lLector["empresa_destinatario"].ToString();
                lsDireccionDestino = lLector["direccion_destinatario"].ToString();
                lsCiudadDestino = lLector["ciudad_destinatario"].ToString();
                lsTextoEncabezadoCarta = lLector["texto_encabezado_carta"].ToString();
                blEncontro = true;
            }
        }
        lLector.Close();
        lLector.Dispose();
        lComando.Parameters.Clear();
        // Obtengo los Parametros Generales de las Cartas
        lComando.Connection = lConexion.gObjConexion;
        lComando.CommandType = CommandType.StoredProcedure;
        lComando.CommandText = "pa_ValidarExistencia";
        lComando.Parameters.Add("@P_tabla", SqlDbType.VarChar).Value = "m_parametros_cartas_super param, m_mes mes";
        lComando.Parameters.Add("@P_filtro", SqlDbType.VarChar).Value = "1=1 And mes.mes = " + DateTime.Now.Month.ToString();
        lLector = lComando.ExecuteReader();
        if (lLector.HasRows)
        {
            while (lLector.Read())
            {
                lsNomFirmaCarta = lLector["nombre_firma_carta"].ToString();
                lsCargoFirmaCarta = lLector["cargo_firma_carta"].ToString();
                lsArchivoFirmaCarta = lLector["archivo_firma_carta"].ToString();
                lsMes = lLector["nombre_mes"].ToString();
            }
        }
        lLector.Close();
        lLector.Dispose();
        lComando.Parameters.Clear();

        string lsdirPDF = "";
        string lsRutaArchivo = "";
        string lsArchivo = "";
        if (blEncontro)
        {
            // Se crea el Documento PDF
            Document loDocument = new Document(PageSize.LETTER, 70, 70, 65, 65);

            try
            {
                iTextSharp.text.Image LogoFirma = iTextSharp.text.Image.GetInstance(oRutaImg + lsArchivoFirmaCarta);
                LogoFirma.ScalePercent(50);
                LogoFirma.Alignment = Element.ALIGN_CENTER;

                string lsFecha = DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();

                /// Defino las Tablas a Manejar en la Generación de Documento
                int[] liAncho = { 35, 65 };
                int[] liAncho1 = { 100 };
                // Definicion de las Variables locales usadas
                // Creacíón de los FONT que se van a manejar en el PDF
                //BaseFont bArial = BaseFont.CreateFont(oRutaFont + "arial.ttf", BaseFont.WINANSI, true);
                //BaseFont bArialBold = BaseFont.CreateFont(oRutaFont + "arialbd.ttf", BaseFont.WINANSI, true);

                // Se definen los tipos de letra a manejar en el PDF
                iTextSharp.text.Font FuenteNegrita;
                FuenteNegrita = FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD);

                iTextSharp.text.Font FuenteNormal;
                FuenteNormal = FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.NORMAL);

                iTextSharp.text.Font FuenteNormal1;
                FuenteNormal1 = FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL);

                // Defino las Tablas a manejar en el Documento PDF
                iTextSharp.text.Table TablaRegistros = new iTextSharp.text.Table(2, liAncho);
                TablaRegistros.WidthPercentage = 95;
                TablaRegistros.Border = 0;
                TablaRegistros.Alignment = Element.ALIGN_LEFT;

                iTextSharp.text.Table TablaRegistros1 = new iTextSharp.text.Table(1, liAncho1);
                TablaRegistros1.WidthPercentage = 95;
                TablaRegistros1.Border = 0;
                TablaRegistros1.Alignment = Element.ALIGN_LEFT;

                // Obtengo el Nombre del Archivo y la ruta del mismo en el servidor
                lsArchivo = "CARTA - " + liCodigoCarta.ToString() + " - " + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + "-" + DateTime.Now.Millisecond.ToString() + ".pdf";
                // Obtengo la Ruta de los PDF
                lsRutaArchivo = "\\" + lsFecha + "\\" + lsArchivo;
                lsdirPDF = oRutaCont + "\\" + lsFecha;
                if (!Directory.Exists(lsdirPDF))
                {
                    Directory.CreateDirectory(lsdirPDF);
                }
                lsdirPDF = oRutaCont + "\\" + lsRutaArchivo;
                // Creo la Instancia del Documento PDF en el Servidor
                PdfWriter.GetInstance(loDocument, new FileStream(lsdirPDF, FileMode.Create));
                //PdfAWriter pw = PdfAWriter.GetInstance(loDocument, new FileStream(lsdirPDF, FileMode.Create), PdfAConformanceLevel.PDF_A_1B);
                //Crear parametros de pdf 
                //pw.PdfVersion = '1';
                //PdfDictionary parameters = new PdfDictionary(PdfName.OUTPUTINTENT);
                //parameters.Put(PdfName.MODDATE, new PdfDate());
                //parameters.Put(PdfName.OUTPUTCONDITIONIDENTIFIER, new PdfString("sRGB IEC61966-2.1"));
                //parameters.Put(PdfName.INFO, new PdfString("sRGB IEC61966-2.1"));
                //parameters.Put(PdfName.S, PdfName.GTS_PDFXVERSION);
                //pw.ExtraCatalog.Put(PdfName.OUTPUTINTENTS, new PdfArray(parameters));
                //pw.CreateXmpMetadata();
                /// Abro el Documento PDF
                lsdirPDF = lsRutaArchivo;

                loDocument.Open();
                //crear profile (ICC - International Color Consorsium) 
                //ICC_Profile icc = ICC_Profile.GetInstance(oRutaFont + "sRGB_Color_Space_Profile.icm");
                //pw.SetOutputIntents("Custom", "", "http://www.color.org", "sRGB IEC61966-2.1", icc);

                // Defino el Parrafo para los Espacios
                Paragraph lpEspacios = new Paragraph(5, " ", FuenteNormal1);

                // Creo los Parrafos a Incluir en la Carta
                // Fecha de la Carta

                Paragraph lpTitulo = new Paragraph(12, "Bogotá D.C. " + DateTime.Now.Day.ToString() + " de " + lsMes + " de " + DateTime.Now.Year.ToString(), FuenteNormal);
                lpTitulo.Alignment = Element.ALIGN_LEFT;
                loDocument.Add(lpTitulo);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);

                Paragraph lpTitulo1 = new Paragraph(12, "Doctor", FuenteNormal);
                lpTitulo1.Alignment = Element.ALIGN_LEFT;

                Paragraph lpTitulo1a = new Paragraph(12, lsNomDestino, FuenteNegrita);
                lpTitulo1a.Alignment = Element.ALIGN_LEFT;

                Paragraph lpTitulo1b = new Paragraph(12, "Cargo " + lsCargoDestino, FuenteNormal);
                lpTitulo1b.Alignment = Element.ALIGN_LEFT;

                Paragraph lpTitulo1c = new Paragraph(12, lsEmpresaDestino, FuenteNegrita);
                lpTitulo1c.Alignment = Element.ALIGN_LEFT;

                Paragraph lpTitulo1d = new Paragraph(12, lsDireccionDestino, FuenteNormal);
                lpTitulo1d.Alignment = Element.ALIGN_LEFT;

                Paragraph lpTitulo1e = new Paragraph(12, lsCiudadDestino, FuenteNormal);
                lpTitulo1e.Alignment = Element.ALIGN_LEFT;

                Paragraph lpTitulo2 = new Paragraph();
                lpTitulo2.Alignment = Element.ALIGN_LEFT;
                lpTitulo2.Add(new Chunk("                   Referencia: ", FuenteNegrita));
                lpTitulo2.Add(new Chunk("Reporte Información SEGAS", FuenteNormal));

                Paragraph lpTitulo3 = new Paragraph();
                lpTitulo3.Alignment = Element.ALIGN_LEFT;
                lpTitulo3.Add(new Chunk("Respetado doctor " + lsNomDestino, FuenteNormal));

                loDocument.Add(lpTitulo1);
                loDocument.Add(lpTitulo1a);
                loDocument.Add(lpTitulo1b);
                loDocument.Add(lpTitulo1c);
                loDocument.Add(lpTitulo1d);
                loDocument.Add(lpTitulo1e);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpTitulo2);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpTitulo3);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);

                // Titulo
                //Cell CellTitulo = new Cell();
                //CellTitulo.Border = 0;
                //CellTitulo.BorderWidthLeft = 0;
                //CellTitulo.BorderWidthTop = 0;
                //CellTitulo.BorderWidthRight = 0;
                //CellTitulo.BorderWidthBottom = 0;
                //CellTitulo.Colspan = 2;
                //CellTitulo.AddElement(lpTitulo);
                //CellTitulo.AddElement(lpEspacios);
                //CellTitulo.AddElement(lpEspacios);
                //CellTitulo.AddElement(lpEspacios);
                //CellTitulo.VerticalAlignment = Element.ALIGN_TOP;
                //CellTitulo.HorizontalAlignment = Element.ALIGN_LEFT;
                //TablaRegistros.AddCell(CellTitulo);

                //Cell CellTitulo1 = new Cell();
                //CellTitulo1.Border = 0;
                //CellTitulo1.BorderWidthLeft = 0;
                //CellTitulo1.BorderWidthTop = 0;
                //CellTitulo1.BorderWidthRight = 0;
                //CellTitulo1.BorderWidthBottom = 0;
                //CellTitulo1.Colspan = 2;
                //CellTitulo1.AddElement(lpTitulo1);
                //CellTitulo1.AddElement(lpTitulo1a);
                //CellTitulo1.AddElement(lpTitulo1b);
                //CellTitulo1.AddElement(lpTitulo1c);
                //CellTitulo1.AddElement(lpTitulo1d);
                //CellTitulo1.AddElement(lpTitulo1e);
                //CellTitulo1.AddElement(lpEspacios);
                //CellTitulo1.AddElement(lpEspacios);
                //CellTitulo1.AddElement(lpEspacios);
                //CellTitulo1.VerticalAlignment = Element.ALIGN_TOP;
                //CellTitulo1.HorizontalAlignment = Element.ALIGN_LEFT;
                //TablaRegistros.AddCell(CellTitulo1);

                //Cell CellTitulo2 = new Cell();
                //CellTitulo2.Border = 0;
                //CellTitulo2.BorderWidthLeft = 0;
                //CellTitulo2.BorderWidthTop = 0;
                //CellTitulo2.BorderWidthRight = 0;
                //CellTitulo2.BorderWidthBottom = 0;
                //CellTitulo2.Colspan = 2;
                //CellTitulo2.AddElement(lpTitulo2);
                //CellTitulo2.AddElement(lpEspacios);
                //CellTitulo2.AddElement(lpEspacios);
                //CellTitulo2.AddElement(lpEspacios);
                //CellTitulo2.VerticalAlignment = Element.ALIGN_TOP;
                //CellTitulo2.HorizontalAlignment = Element.ALIGN_LEFT;
                //TablaRegistros.AddCell(CellTitulo2);

                //Cell CellTitulo3 = new Cell();
                //CellTitulo3.Border = 0;
                //CellTitulo3.BorderWidthLeft = 0;
                //CellTitulo3.BorderWidthTop = 0;
                //CellTitulo3.BorderWidthRight = 0;
                //CellTitulo3.BorderWidthBottom = 0;
                //CellTitulo3.Colspan = 2;
                //CellTitulo3.AddElement(lpTitulo3);
                //CellTitulo3.AddElement(lpEspacios);
                //CellTitulo3.AddElement(lpEspacios);
                //CellTitulo3.AddElement(lpEspacios);
                //CellTitulo3.VerticalAlignment = Element.ALIGN_TOP;
                //CellTitulo3.HorizontalAlignment = Element.ALIGN_LEFT;
                //TablaRegistros.AddCell(CellTitulo3);
                //loDocument.Add(TablaRegistros);
                //TablaRegistros.DeleteAllRows();

                // Creo el Parrafo del Encabezado de la Carta
                Paragraph lpContenido = new Paragraph();
                lpContenido.Add(new Chunk(lsTextoEncabezadoCarta, FuenteNormal));
                lpContenido.Alignment = Element.ALIGN_JUSTIFIED;
                loDocument.Add(lpContenido);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);


                // Obtengo el COntenido de la Carta
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetSeccionCartaSuper";
                lComando.Parameters.Add("@P_ccodigo_seccion_carta", SqlDbType.Int).Value = 0;
                lComando.Parameters.Add("@P_codigo_carta", SqlDbType.Int).Value = liCodigoCarta.ToString();
                lLector = lComando.ExecuteReader();
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                    {
                        lsCodSeccion = lLector["codigo_seccion_carta"].ToString();
                        // Creo el Parrafo de la Seccion de la Carta
                        Paragraph lpContenido1 = new Paragraph();
                        lpContenido1.Add(new Chunk(lLector["numero_seccion"].ToString() + " ", FuenteNegrita));
                        lpContenido1.Add(new Chunk(lLector["descripcion_seccion"].ToString(), FuenteNormal));
                        lpContenido1.Alignment = Element.ALIGN_JUSTIFIED;
                        loDocument.Add(lpContenido1);
                        loDocument.Add(lpEspacios);
                        loDocument.Add(lpEspacios);

                        // Obtengo el Detalle de la Seccion
                        lConexion1.Abrir();
                        // Obtengo el COntenido de la Carta
                        lComando1.Connection = lConexion1.gObjConexion;
                        lComando1.CommandType = CommandType.StoredProcedure;
                        lComando1.CommandText = "pa_GetDetalleSeccionCartaSuper";
                        lComando1.Parameters.Add("@P_ccodigo_seccion_carta", SqlDbType.Int).Value = lsCodSeccion;
                        lComando1.Parameters.Add("@P_codigo_carta", SqlDbType.Int).Value = liCodigoCarta.ToString();
                        lLector1 = lComando1.ExecuteReader();
                        lComando1.Parameters.Clear();
                        if (lLector1.HasRows)
                        {
                            while (lLector1.Read())
                            {
                                // Creo el Parrafo del Contenido la Seccion de la Carta
                                Paragraph lpContenido1d = new Paragraph();
                                lpContenido1d.Add(new Chunk("- ", FuenteNegrita));
                                lpContenido1d.Add(new Chunk(lLector1["descripcion_detalle"].ToString(), FuenteNormal));
                                lpContenido1d.Alignment = Element.ALIGN_JUSTIFIED;
                                loDocument.Add(lpContenido1d);
                                loDocument.Add(lpEspacios);
                            }
                        }
                        lLector1.Close();
                        lLector1.Dispose();
                        lConexion1.Cerrar();
                        loDocument.Add(lpEspacios);
                    }
                }
                lLector.Close();
                lLector.Dispose();
                lComando.Parameters.Clear();

                // Creo el Parrafo del Cierre de la Carta
                Paragraph lpContenido2 = new Paragraph();
                lpContenido2.Add(new Chunk("Es importante recalcar que la información consolidada es de carácter confidencial y corresponde a las declaraciones de los participantes; de esta manera, la información puede estar sujeta a modificaciones dado el comportamiento del mercado de acuerdo a lo contenido en el numeral 1 del anexo 2 de la Resolución CREG 089 de 2013. Agradecemos el buen uso de la misma.", FuenteNormal));
                lpContenido2.Alignment = Element.ALIGN_JUSTIFIED;
                loDocument.Add(lpContenido2);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);

                Paragraph lpContenido3 = new Paragraph();
                lpContenido3.Add(new Chunk("Quedamos atentos,", FuenteNormal));
                lpContenido3.Alignment = Element.ALIGN_JUSTIFIED;
                loDocument.Add(lpContenido3);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);

                Paragraph lpContenido4 = new Paragraph();
                lpContenido4.Add(new Chunk("Cordialmente,", FuenteNormal));
                lpContenido4.Alignment = Element.ALIGN_JUSTIFIED;
                loDocument.Add(lpContenido4);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);
                loDocument.Add(lpEspacios);

                // Firma de la Carta
                Paragraph lpFirmalg = new Paragraph();
                lpFirmalg.Add(new Chunk(LogoFirma, 0, 0));
                lpFirmalg.Alignment = Element.ALIGN_CENTER;
                loDocument.Add(lpFirmalg);
                Paragraph lpFirma = new Paragraph();
                lpFirma.Add(new Chunk(lsNomFirmaCarta, FuenteNegrita));
                lpFirma.Alignment = Element.ALIGN_CENTER;
                loDocument.Add(lpFirma);
                Paragraph lpFirma1 = new Paragraph();
                lpFirma1.Add(new Chunk(lsCargoFirmaCarta, FuenteNormal));
                lpFirma1.Alignment = Element.ALIGN_CENTER;
                loDocument.Add(lpFirma1);
                Paragraph lpFirma2 = new Paragraph();
                lpFirma2.Add(new Chunk("Bolsa Mercantil de Colombia S.A.", FuenteNormal));
                lpFirma2.Alignment = Element.ALIGN_CENTER;
                loDocument.Add(lpFirma2);

                loDocument.Close();
                return lsdirPDF;
            }
            catch (Exception ex)
            {
                loDocument.Close();
                return "ERROR en la Generacion del Contrato. " + ex.Message.ToString();
            }
        }
        else
            return "ERROR - NO HAY DATOS";
    }
}