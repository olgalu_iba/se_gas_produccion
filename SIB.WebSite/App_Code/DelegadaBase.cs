﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.BASE.Negocio;
using SIB.Global.Presentacion;

/// <summary>
/// Summary description for DelegadaBase
/// </summary>
public class DelegadaBase : DelegadaSIB
{
    public DelegadaBase()
    {
    }

    private static INegocioBase ServicioNegocio = FachadaBase.Instance;

    public static INegocioBase Servicios
    {
        get { return ServicioNegocio; }
    }

}
