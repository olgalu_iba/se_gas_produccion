﻿// (c) Copyright Microsoft Corporation.
// This source is subject to the Microsoft Permissive License.
// See http://www.microsoft.com/resources/sharedsource/licensingbasics/sharedsourcelicenses.mspx.
// All other rights reserved.

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using AjaxControlToolkit;



[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]

public class AutoComplete : WebService
{
    public AutoComplete()
    {
    }

    /// <summary>
    /// Metodo que nos retorna la hora del servidor
    /// </summary>
    /// <returns></returns>
    /// 20201030
    [WebMethod()]
    public string ObtenerHora()
    {
        string Hora = "";
        Hora = DateTime.Now.ToString("HH:mm:ss");
        return Hora;
    }
    [WebMethod()]
    public string[] GetCompletionList(string prefixText, int count)
    {
        if (count == 0)
        {
            count = 10;
        }

        if (prefixText.Equals("xyz"))
        {
            return new string[0];
        }

        Random random = new Random();
        List<string> items = new List<string>(count);
        for (int i = 0; i < count; i++)
        {
            char c1 = (char)random.Next(65, 90);
            char c2 = (char)random.Next(97, 122);
            char c3 = (char)random.Next(97, 122);

            items.Add(prefixText + c1 + c2 + c3);
        }

        return items.ToArray();
    }

    /////////////////////////////////////////////////////////
    /// METODOS PARA AYUDA SITIO DE ENTREGA  ////
    /////////////////////////////////////////////////////////
    /// <summary>
    /// Nombre: GetListaPais
    /// Fecha: Agosto 10 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para obtenr la Lista de Valores para el DDl de Pais
    /// Modificacion:
    /// </summary>
    /// <param name="lsValorCategoria"></param>
    /// <param name="lsCategoria"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public CascadingDropDownNameValue[] GetListaPais(string knownCategoryValues, string category)
    {
        clConexion lConexion = null;
        InfoSessionVO goInfo = (InfoSessionVO)Session["infoSession"];
        SqlDataReader lLector;
        List<CascadingDropDownNameValue> loListado = new List<CascadingDropDownNameValue>();
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_div_geografica", " codigo_pais >0 AND codigo_departamento = 0 AND codigo_ciudad = 0 ORDER BY nombre_ubicacion");
        while (lLector.Read())
        {
            loListado.Add(new CascadingDropDownNameValue(lLector.GetValue(3).ToString(), lLector.GetValue(4).ToString())); //20170530 divipola
        }
        lLector.Close();
        lLector.Dispose();
        lConexion.Cerrar();
        return loListado.ToArray();
    }
    /// <summary>
    /// Nombre: GetListaDepartamento
    /// Fecha: Agosto 10 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para obtenr la Lista de Valores para el DDl de Departamento
    /// Modificacion:
    /// 20170530 divipola
    /// </summary>
    /// <param name="lsValorCategoria"></param>
    /// <param name="lsCategoria"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public CascadingDropDownNameValue[] GetListaDepartamento(string knownCategoryValues, string category)
    {
        clConexion lConexion = null;
        InfoSessionVO goInfo = (InfoSessionVO)Session["infoSession"];
        SqlDataReader lLector;
        List<CascadingDropDownNameValue> loListado = new List<CascadingDropDownNameValue>();
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_divipola", " codigo_ciudad = '0' and estado ='A' ORDER BY nombre_departamento");
        while (lLector.Read())
        {
            loListado.Add(new CascadingDropDownNameValue(lLector.GetValue(2).ToString(), lLector.GetValue(1).ToString())); //20170530 divipola
        }
        lLector.Close();
        lLector.Dispose();
        lConexion.Cerrar();
        return loListado.ToArray();
    }
    /// <summary>
    /// Nombre: GetListaCiudad
    /// Fecha: Agosto 10 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para obtenr la Lista de Valores para el DDl de Ciudad
    /// Modificacion:
    /// 20170530 divipola
    /// </summary>
    /// <param name="lsValorCategoria"></param>
    /// <param name="lsCategoria"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public CascadingDropDownNameValue[] GetListaCiudad(string knownCategoryValues, string category)
    {
        clConexion lConexion = null;
        Int32 lintTeamId = 0;
        InfoSessionVO goInfo = (InfoSessionVO)Session["infoSession"];
        SqlDataReader lLector;
        List<CascadingDropDownNameValue> loListado = new List<CascadingDropDownNameValue>();
        StringDictionary kvPlayer = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);
        if (!kvPlayer.ContainsKey("Dpto") || !Int32.TryParse(kvPlayer["Dpto"], out lintTeamId))
        {
            return null;
        }
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_divipola", "codigo_departamento = " + lintTeamId + " and codigo_ciudad <> '0' and codigo_centro ='0' AND estado ='A' ORDER BY nombre_ciudad"); //20170530 divipola
        while (lLector.Read())
        {
            loListado.Add(new CascadingDropDownNameValue(lLector.GetValue(4).ToString(), lLector.GetValue(3).ToString()));
        }
        lLector.Close();
        lLector.Dispose();
        lConexion.Cerrar();
        return loListado.ToArray();
    }

    /// <summary>
    /// Nombre: GetListaCentro
    /// Fecha: Agosto 10 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para obtenr la Lista de Valores para el DDl de Ciudad
    /// Modificacion:
    /// 20170530 divipola
    /// </summary>
    /// <param name="lsValorCategoria"></param>
    /// <param name="lsCategoria"></param>
    /// <returns></returns>
    /// //20170530 divipola
    [WebMethod(EnableSession = true)]
    public CascadingDropDownNameValue[] GetListaCentro(string knownCategoryValues, string category)
    {
        clConexion lConexion = null;
        Int32 lintTeamId = 0;
        InfoSessionVO goInfo = (InfoSessionVO)Session["infoSession"];
        SqlDataReader lLector;
        List<CascadingDropDownNameValue> loListado = new List<CascadingDropDownNameValue>();
        StringDictionary kvPlayer = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);
        if (!kvPlayer.ContainsKey("Ciudad") || !Int32.TryParse(kvPlayer["Ciudad"], out lintTeamId))
        {
            return null;
        }
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_divipola", "codigo_ciudad = " + lintTeamId + " and codigo_centro <> '0' AND estado ='A' ORDER BY nombre_centro");
        while (lLector.Read())
        {
            loListado.Add(new CascadingDropDownNameValue(lLector.GetValue(6).ToString(), lLector.GetValue(5).ToString()));
        }
        lLector.Close();
        lLector.Dispose();
        lConexion.Cerrar();
        return loListado.ToArray();
    }

    /// <summary>
    /// AutoCompletar para el campo de Usuario Final en el Registrro de los COntratos
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public string[] GetCompletionListUsuarioFinal(string prefixText, int count)
    {
        clConexion lConexion = null;
        InfoSessionVO goInfo = (InfoSessionVO)Session["infoSession"];
        SqlDataReader lLector;
        prefixText = prefixText.ToUpper();
        List<string> lvalores = new List<string>();
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_usuario_final_verifica", "  estado='A' And (NOMBRE LIKE '%" + prefixText + "%' Or no_documento like '%" + prefixText + "%' ) ORDER BY nombre ");
        while (lLector.Read())
        {
            lvalores.Add(lLector["no_documento"].ToString() + " - " + lLector["codigo_tipo_doc"].ToString() + " - " + lLector["nombre"].ToString());
        }
        lLector.Close();
        lLector.Dispose();
        lConexion.Cerrar();
        return lvalores.ToArray();
    }

}

