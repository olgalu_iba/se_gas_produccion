﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Presentacion;

public partial class frm_menu : System.Web.UI.Page
{
    InfoSessionVO goInfo;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("Login.aspx");

        if (!IsPostBack)
        {
            //Quitar viejos menus si los hay
            for (int x = 0; x < tvMenu.Nodes.Count; x++)
                tvMenu.Nodes.RemoveAt(x);

            var lista = DelegadaBase.Servicios.consultarMenu(goInfo, 0);
            foreach (Hashtable opcion in lista)
            {
                TreeNode nodoPrincipal = new TreeNode();
                nodoPrincipal.SelectAction = TreeNodeSelectAction.None;
                nodoPrincipal.Text = opcion["menu"].ToString();
                nodoPrincipal.Value = opcion["codigo_grupo_menu"].ToString();
                if (opcion["ruta"] != null)
                    nodoPrincipal.NavigateUrl = opcion["ruta"].ToString();
                nodoPrincipal.Target = opcion["frame"].ToString();
                tvMenu.Nodes.Add(nodoPrincipal);
                cargaSubmenus(nodoPrincipal, Convert.ToInt32(opcion["codigo_menu"]));
            }
        }
    }

    void cargaSubmenus(TreeNode NodoPadre, int liPadre)
    {
        var lista = DelegadaBase.Servicios.consultarMenu(goInfo, liPadre);

        TreeNode UltimoNodo = null;

        foreach (Hashtable opcion in lista)
        {

            TreeNode nodo = new TreeNode();
            nodo.Text = opcion["menu"].ToString();
            nodo.Value = opcion["codigo_grupo_menu"].ToString();
            if (opcion["ruta"] != null)
                nodo.NavigateUrl = opcion["ruta"].ToString();
            if (opcion["frame"] != null)
                nodo.Target = opcion["frame"].ToString();

            if (!tvMenu.Nodes.Contains(nodo))
            {
                NodoPadre.ChildNodes.Add(nodo);
                UltimoNodo = nodo;
            }

            if (opcion["ruta"].ToString().Trim().Length == 0)
            {
                UltimoNodo.SelectAction = TreeNodeSelectAction.None;
                cargaSubmenus(UltimoNodo, Convert.ToInt32(opcion["codigo_menu"]));
            }
        }
    }
}