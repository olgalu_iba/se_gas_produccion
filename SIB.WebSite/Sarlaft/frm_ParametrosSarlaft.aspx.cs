﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Sarlaft_frm_ParametrosSarlaft : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Parámetros Sarlaft";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    string gsTabla = "m_parametros_sarlaft";

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        //EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (VerificarExistencia("1=1"))
            {
                Modificar();
            }
            else
            {
                Nuevo();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    //private void EstablecerPermisosSistema()
    //{
    //    Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, gsTabla);
    //    imbCrear.Visible = (Boolean)permisos["INSERT"];
    //    imbActualiza.Visible = (Boolean)permisos["UPDATE"];
    //}
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar()
    {
        try
        {
            lblMensaje.Text = "";
            /// Verificar Si el Registro esta Bloqueado
            if (!manejo_bloqueo("V", ""))
            {
                // Carga informacion de combos
                imbCrear.Visible = false;
                imbActualiza.Visible = true;

                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", gsTabla, " 1=1 ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    TxtDiaHabPri.Text = lLector["dia_hab_pri_alerta"].ToString();
                    TxtDiaHabSeg.Text = lLector["dia_hab_seg_alerta"].ToString();
                    TxtDiaCalCtrl.Text = lLector["dia_cal_alerta_ctrl"].ToString();
                    TxtObsEst.Text = lLector["texto_alerta_act_est"].ToString();
                    TxtAntVcto.Text = lLector["texto_alerta_ant_vcto"].ToString();
                    TxtControl.Text = lLector["texto_alerta_ctrl"].ToString();
                    TxtObservacion.Text = "";
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                /// Bloquea el Registro a Modificar
                manejo_bloqueo("A", "");
            }
            else
            {
                tblCaptura.Visible = false;
                lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado.";

            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_dia_hab_pri_alerta", "@P_dia_hab_seg_alerta", "@P_dia_cal_alerta_ctrl", "@P_texto_alerta_act_est", "@P_texto_alerta_ant_vcto", "@P_texto_alerta_ctrl", "@P_observaciones" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
        string[] lValorParametros = { "", "", "", "", "", "", "", "" };
        lblMensaje.Text = "";
        if (TxtDiaHabPri.Text == "")
            lblMensaje.Text += "Debe digitar los días hábiles para la primera alerta antes del vencimiento.<br>";
        if (TxtDiaHabSeg.Text == "")
            lblMensaje.Text += "Debe digitar los días hábiles para la segunda alerta antes del vencimiento.<br>";
        if (TxtDiaCalCtrl.Text == "")
            lblMensaje.Text += "Debe digitar los días calendario para alerta después del vencimiento.<br>";
        if (TxtObsEst.Text == "")
            lblMensaje.Text += "Debe digitar el texto de la alerta por cambio de estado.<br>";
        if (TxtAntVcto.Text == "")
            lblMensaje.Text += "Debe digitar el texto de las alertas antes del vencimiento.<br>";
        if (TxtControl.Text == "")
            lblMensaje.Text += "Debe digitar el texto de la alerta después del vencimiento.<br>";
        if (TxtObservacion.Text == "")
            lblMensaje.Text += "Debe digitar las observaciones de creación del registro.<br>";
        if (lblMensaje.Text == "")
        {
            try
            {
                lValorParametros[0] = TxtDiaHabPri.Text.Trim();
                lValorParametros[1] = TxtDiaHabSeg.Text.Trim();
                lValorParametros[2] = TxtDiaCalCtrl.Text.Trim();
                lValorParametros[3] = TxtObsEst.Text.Trim();
                lValorParametros[4] = TxtAntVcto.Text.Trim();
                lValorParametros[5] = TxtControl.Text.Trim();
                lValorParametros[6] = TxtObservacion.Text.Trim();

                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParametrosSarlaft", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación de Parámetros de sarlaft.!";
                    lConexion.Cerrar();
                }
                else
                {
                    lblMensaje.Text = "";
                    Response.Redirect("~/frm_contenido.aspx");
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Error al crear el registro." + ex.Message;
            }
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_dia_hab_pri_alerta", "@P_dia_hab_seg_alerta", "@P_dia_cal_alerta_ctrl", "@P_texto_alerta_act_est", "@P_texto_alerta_ant_vcto", "@P_texto_alerta_ctrl", "@P_observaciones" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
        string[] lValorParametros = { "", "", "", "", "", "", "", "" };
        lblMensaje.Text = "";
        if (TxtDiaHabPri.Text == "")
            lblMensaje.Text += "Debe digitar los días hábiles para la primera alerta antes del vencimiento.<br>";
        if (TxtDiaHabSeg.Text == "")
            lblMensaje.Text += "Debe digitar los días hábiles para la segunda alerta antes del vencimiento.<br>";
        if (TxtDiaCalCtrl.Text == "")
            lblMensaje.Text += "Debe digitar los días calendario para alerta después del vencimiento.<br>";
        if (TxtObsEst.Text == "")
            lblMensaje.Text += "Debe digitar el texto de la alerta por cambio de estado.<br>";
        if (TxtAntVcto.Text == "")
            lblMensaje.Text += "Debe digitar el texto de las alertas antes del vencimiento.<br>";
        if (TxtControl.Text == "")
            lblMensaje.Text += "Debe digitar el texto de la alerta después del vencimiento.<br>";
        if (TxtObservacion.Text == "")
            lblMensaje.Text += "Debe digitar las observaciones de modificación del registro.<br>";

        if (lblMensaje.Text == "")
        {
            try
            {
                lValorParametros[0] = TxtDiaHabPri.Text.Trim();
                lValorParametros[1] = TxtDiaHabSeg.Text.Trim();
                lValorParametros[2] = TxtDiaCalCtrl.Text.Trim();
                lValorParametros[3] = TxtObsEst.Text.Trim();
                lValorParametros[4] = TxtAntVcto.Text.Trim();
                lValorParametros[5] = TxtControl.Text.Trim();
                lValorParametros[6] = TxtObservacion.Text.Trim();

                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParametrosSarlaft", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización de los Parametros de Sarlaft.!" + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", "");
                    lblMensaje.Text = "";
                    Response.Redirect("~/frm_contenido.aspx");
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Error en la actualización del registro." + ex.Message ;
            }
        }

    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        manejo_bloqueo("E", "");
        lblMensaje.Text = "";
        Response.Redirect("~/WebForms/Home.aspx");
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(gsTabla, lswhere, goInfo);
    }
    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_parametros_sarlaft' ";
        string lsCondicion1 = "";
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = gsTabla;
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, gsTabla, lsCondicion1);
        }
        return true;
    }
}