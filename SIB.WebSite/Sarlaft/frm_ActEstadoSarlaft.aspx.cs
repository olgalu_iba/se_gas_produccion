﻿using System;
using System.Collections;
using System.Globalization;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.DataVisualization.Charting;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Sarlaft_frm_ActEstadoSarlaft : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Modificación de Estado Sarlaft";
    clConexion lConexion = null;
    DataSet lds = new DataSet();
    string sRutaArc = ConfigurationManager.AppSettings["rutaCargaSarlaft"].ToString();
    string[] lsCarperta;
    string Carpeta;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        lsCarperta = sRutaArc.Split('\\');
        Carpeta = lsCarperta[lsCarperta.Length - 2];

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' order by razon_social", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlOperadorMod, "m_operador", " estado = 'A' order by razon_social", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlEstado, "m_estado_gas", " tipo_estado= 'S' ", 2, 3);
            LlenarControles(lConexion.gObjConexion, ddlEstadoMod, "m_estado_gas", " tipo_estado= 'S' ", 2, 3);
            lConexion.Cerrar();
            CargarDatos();
            if (goInfo.codigo_grupo_usuario == 1 || goInfo.codigo_grupo_usuario == 40)
            {
                TxtFechaActMod.Enabled = false;
                TxtFechaVinMod.Enabled = false;
                TxtMemo.Enabled = false;
            }
            else if (goInfo.codigo_grupo_usuario != 39 )
            {
                TxtFechaActMod.Enabled = false;
                TxtFechaVinMod.Enabled = false;
                TxtMemo.Enabled = false;
                ddlEstado.Enabled = false;
                btnModificar.Visible = false;
            }
        }
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            if (lsTabla != "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector["no_documento"].ToString();
                lItem1.Text = lLector["no_documento"].ToString() + "-" + lLector["razon_social"].ToString();
            }

            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        CargarDatos();
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        lblMensaje.Text = "";
        string[] lsNombreParametros = { "@P_no_documento", "@P_fecha_vinculacion", "@P_fecha_vencimiento", "@P_estado" };
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
        string[] lValorParametros = { ddlOperador.SelectedValue, TxtFechaVin.Text, TxtFechaVcto.Text, ddlEstado.SelectedValue };

        if (lblMensaje.Text == "")
        {
            try
            {
                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetEstadoSarlaft", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                lConexion.Cerrar();
                if (dtgConsulta.Items.Count <= 0)
                    lblMensaje.Text = "No se encontraron Registros.";
                else
                {
                    tblGrilla.Visible = true;
                    imbExcel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo consultar la información.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            try
            {
                ddlOperadorMod.SelectedValue = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text;
                if (this.dtgConsulta.Items[e.Item.ItemIndex].Cells[4].Text == "&nbsp;")
                    TxtMemo.Text = "";
                else
                    TxtMemo.Text = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[4].Text;
                if (this.dtgConsulta.Items[e.Item.ItemIndex].Cells[3].Text == "&nbsp;")
                {
                    TxtFechaVinMod.Text = "";
                    if (goInfo.codigo_grupo_usuario == 39)
                        TxtFechaVinMod.Enabled = true;
                }
                else
                {
                    TxtFechaVinMod.Text = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[3].Text;
                    TxtFechaVinMod.Enabled = false;
                }
                if (this.dtgConsulta.Items[e.Item.ItemIndex].Cells[5].Text == "&nbsp;")
                    TxtFechaActMod.Text = "";
                else
                    TxtFechaActMod.Text = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[5].Text;
                if (this.dtgConsulta.Items[e.Item.ItemIndex].Cells[6].Text == "&nbsp;")
                    TxtFechaVctoMod.Text = "";
                else
                    TxtFechaVctoMod.Text = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[6].Text;
                ddlEstadoMod.SelectedValue = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[7].Text;
                tblSolicitud.Visible = false;
                tblGrilla.Visible = false;
                tblDatos.Visible = true;
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al modificar el registro. " + ex.Message.ToString();
            }
        }
    }

    /// <summary>
    /// Exportacion a Excel de la Información  de la Grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbExcel_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        try
        {
            string lsNombreArchivo = Session["login"] + "InfEstSarlaft" + DateTime.Now + ".xls";
            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            lpagina.EnableEventValidation = false;
            lpagina.Controls.Add(lform);
            dtgConsulta.EnableViewState = false;
            dtgConsulta.Columns[9].Visible = false;
            lform.Controls.Add(dtgConsulta);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;

            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["NomOperador"] + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + "Consulta Estado Sarlaft" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write(lsb.ToString());
            Response.End();
            Response.Flush();
            dtgConsulta.Columns[9].Visible = true;
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas al Consultar los Registros. " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnModificar_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_no_documento", "@P_fecha_vinculacion", "@P_fecha_actualizacion", "@P_numero_memo", "@P_estado" };
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
        string[] lValorParametros = { ddlOperadorMod.SelectedValue, TxtFechaVinMod.Text, TxtFechaActMod.Text, TxtMemo.Text, ddlEstadoMod.SelectedValue };
        lblMensaje.Text = "";
        try
        {
            if (goInfo.codigo_grupo_usuario == 39)
            {
                if (TxtMemo.Text == "")
                    lblMensaje.Text = "Debe digitar el número del Memo<br>";
                if (TxtFechaVinMod.Text == "")
                    lblMensaje.Text += "Debe digitar la fecha de vinculación<br>";
                else
                    try 
                    {
                        Convert.ToDateTime(TxtFechaVinMod.Text);
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += "La fecha de vinculación no es válida<br>";
                    }
                if (TxtFechaActMod.Text == "")
                    lblMensaje.Text += "Debe digitar la fecha de actualización<br>";
                else
                    try
                    {
                        Convert.ToDateTime(TxtFechaActMod.Text);
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += "La fecha de Actualización no es válida<br>";
                    }
                if (lblMensaje.Text =="")
                    try
                    {
                        if (Convert.ToDateTime(TxtFechaVinMod.Text) > Convert.ToDateTime(TxtFechaActMod.Text))
                            lblMensaje.Text += "La fecha de vinculación debe ser menor o igual que la fecha de actualización<br>";
                    }
                    catch (Exception ex)
                    {
                    }
                if (ddlEstadoMod.SelectedValue == "0")
                    lblMensaje.Text += "Debe seleccionar el estado<br>";
            }
            if (goInfo.codigo_grupo_usuario == 1 || goInfo.codigo_grupo_usuario == 40)
            {
                if (ddlEstadoMod.SelectedValue != "N" && ddlEstadoMod.SelectedValue != "P")
                    lblMensaje.Text = "El estado seleccionado debe ser No actualizado o en proceso de actualización<br>";
            }
            if (lblMensaje.Text == "")
            {
                lConexion.Abrir();
                goInfo.mensaje_error = "";
                DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetEstadoSarlaft", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensaje.Text = "Se presentó un problema en la actualización del estado.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Operador actualizado Correctamente.!" + "');", true);
                    CargarDatos();
                    tblDatos.Visible = false;
                    tblGrilla.Visible = true;
                    tblSolicitud.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }

    protected void btnRegresar_Click(object sender, EventArgs e)
    {
        CargarDatos();
        tblDatos.Visible = false;
        tblGrilla.Visible = true;
        tblSolicitud.Visible = true;
    }
    protected void TxtFechaActMod_TextChanged(object sender, EventArgs e)
    {
        try 
        {
            if (TxtFechaActMod.Text == "")
                TxtFechaVctoMod.Text = "";
            else
            {
                TxtFechaVctoMod.Text = Convert.ToDateTime(TxtFechaActMod.Text).AddYears(1).AddDays(-1).ToString();
                TxtFechaVctoMod.Text = TxtFechaVctoMod.Text.Substring(6, 4) + "/" + TxtFechaVctoMod.Text.Substring(3, 2) + "/" + TxtFechaVctoMod.Text.Substring(0, 2);
            }
        }
        catch (Exception ex)
        {
            TxtFechaVcto.Text = "";
        }
    }
}