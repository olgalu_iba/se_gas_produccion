﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ConsultaDocSarlaft.aspx.cs"
    Inherits="Sarlaft_frm_ConsultaDocSarlaft" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
            width="100%">
            <tr>
                <td align="center" class="th1" style="width: 80%;">
                    <asp:Label ID="lblTitulo" runat="server" ForeColor ="White"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table id="tblSolicitud" runat="server" border="0" align="center" cellpadding="3"
        cellspacing="2" width="80%">
        <tr>
            <td class="td1">
                Tipo Documento
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlTipoDoc" runat="server" Width="250px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="6" align="center">
                <asp:Button ID="btnConsultar" runat="server" Text="Buscar" OnClick="btnConsultar_Click"
                    OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/exel 2007 3D.png"
                    Height="40" OnClick="imbExcel_Click" Visible="false" />
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="6" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="14px"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 1150px; height: 450px;">
                    <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                        AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1"
                        OnEditCommand="dtgConsulta_EditCommand">
                        <Columns>
                            <%--0--%>
                            <asp:BoundColumn DataField="codigo_doc_sarlaft" HeaderText="Código Archivo" ItemStyle-HorizontalAlign="Left"> <%--20180815 BUG230--%>
                            </asp:BoundColumn>
                            <%--1--%>
                            <asp:BoundColumn DataField="codigo_tipo_doc" HeaderText="Cód Tipo Doc"></asp:BoundColumn> <%--20180815 BUG230--%>
                            <%--2--%>
                            <asp:BoundColumn DataField="desc_documento" HeaderText="Tipo Documento" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="250px"></asp:BoundColumn>
                            <%--3--%>
                            <asp:BoundColumn DataField="nombre_archivo" HeaderText="Archivo"></asp:BoundColumn>
                            <%--4--%>
                            <asp:BoundColumn DataField="ruta_archivo" Visible="false"></asp:BoundColumn>
                            <%--5--%>
                            <asp:EditCommandColumn HeaderText="Ver" EditText="Ver Archivo"></asp:EditCommandColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
    </asp:Content>
