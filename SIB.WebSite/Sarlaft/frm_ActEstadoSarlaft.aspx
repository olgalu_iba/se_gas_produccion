﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ActEstadoSarlaft.aspx.cs"
    Inherits="Sarlaft_frm_ActEstadoSarlaft" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
            width="100%">
            <tr>
                <td align="center" class="th1" style="width: 80%;">
                    <asp:Label ID="lblTitulo" runat="server" ForeColor="White" ></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <br /><br /><br /><br /><br /><br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table id="tblSolicitud" runat="server" border="0" align="center" cellpadding="3"
        cellspacing="2" width="80%">
        <tr>
            <td class="td1">Operador
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlOperador" runat="server" Width="250px">
                </asp:DropDownList>
            </td>
            <td class="td1">Fecha Vinculación
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaVin" runat="server" ValidationGroup="detalle" Width="100px" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
            <%--    <ajaxToolkit:CalendarExtender ID="CeTxtFechaVin" runat="server" TargetControlID="TxtFechaVin"
                    Format="yyyy/MM/dd">
                </ajaxToolkit:CalendarExtender>--%>
            </td>
            <td class="td1">Fecha Vencimiento
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaVcto" runat="server" ValidationGroup="detalle" Width="100px" CssClass="form-control datepicker"  ClientIDMode="Static"></asp:TextBox>
          <%--      <ajaxToolkit:CalendarExtender ID="ceTxtFechaVcto" runat="server" TargetControlID="TxtFechaVcto"
                    Format="yyyy/MM/dd">
                </ajaxToolkit:CalendarExtender>--%>
            </td>
            <td class="td1">Estado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlEstado" runat="server" Width="250px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="8" align="center">
                <asp:Button ID="btnConsultar" runat="server" Text="Buscar" OnClick="btnConsultar_Click"
                    OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/exel 2007 3D.png"
                    Height="40" OnClick="imbExcel_Click" Visible="false" />
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="8" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="14px"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 1150px; height: 450px;">
                    <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                        AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1"
                        OnEditCommand="dtgConsulta_EditCommand">
                        <Columns>
                            <%--0--%>
                            <asp:BoundColumn DataField="no_documento" HeaderText="Nit" ItemStyle-HorizontalAlign="Left" Visible="false"></asp:BoundColumn>
                            <%--1--%>
                            <asp:BoundColumn DataField="razon_social" HeaderText="Razón Social"></asp:BoundColumn>
                            <%--20180815 BUG230--%>
                            <%--2--%>
                            <asp:BoundColumn DataField="nit_operador" HeaderText="Nit" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--3--%>
                            <asp:BoundColumn DataField="fecha_vinculacion" HeaderText="Fecha Vinculación" ItemStyle-Width="110"></asp:BoundColumn>
                            <%--20180815 BUG230--%>
                            <%--4--%>
                            <asp:BoundColumn DataField="numero_memo" HeaderText="Memo" Visible="false"></asp:BoundColumn>
                            <%--5--%>
                            <asp:BoundColumn DataField="fecha_actualizacion" HeaderText="Fecha Actualización" ItemStyle-Width="110"></asp:BoundColumn>
                            <%--20180815 BUG230--%>
                            <%--6--%>
                            <asp:BoundColumn DataField="fecha_vencimiento" HeaderText="Fecha Vencimiento" ItemStyle-Width="110"></asp:BoundColumn>
                            <%--7--%>
                            <asp:BoundColumn DataField="estado" HeaderText="estado"></asp:BoundColumn>
                            <%--8--%>
                            <asp:BoundColumn DataField="desc_estado" HeaderText="estado sarlaft"></asp:BoundColumn>
                            <%--9--%>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar"></asp:EditCommandColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
    <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
        width="100%" visible="false">
        <tr>
            <td class="td1">Operador:
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlOperadorMod" runat="server" Width="250px" Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">Número Memo de aprobación:
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtMemo" runat="server" Width="150px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">Fecha Vinculación
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaVinMod" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
              <%--  <ajaxToolkit:CalendarExtender ID="CeTxtFechaVinMod" runat="server" TargetControlID="TxtFechaVinMod"
                    Format="yyyy/MM/dd">
                </ajaxToolkit:CalendarExtender>--%>
            </td>
        </tr>
        <tr>
            <td class="td1">Fecha Actualización
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaActMod" runat="server" MaxLength="10" Width="150px" AutoPostBack="true"
                    OnTextChanged="TxtFechaActMod_TextChanged"></asp:TextBox>
             <%--   <ajaxToolkit:CalendarExtender ID="CeTxtFechaActMod" runat="server" TargetControlID="TxtFechaActMod"
                    Format="yyyy/MM/dd">
                </ajaxToolkit:CalendarExtender>--%>
            </td>
        </tr>
        <tr>
            <td class="td1">Fecha Vencimiento
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaVctoMod" runat="server" MaxLength="10" Width="150px" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">Estado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlEstadoMod" runat="server" Width="250px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:Button ID="btnModificar" runat="server" Text="Modificar" OnClick="btnModificar_Click"
                    OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnRegresar" runat="server" Text="Regresar" OnClick="btnRegresar_Click"
                    OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="6" align="center">
                <asp:Label ID="Label1" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="14px"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
