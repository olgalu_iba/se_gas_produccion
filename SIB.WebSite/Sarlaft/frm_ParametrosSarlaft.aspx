﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ParametrosSarlaft.aspx.cs"
    Inherits="Sarlaft_frm_ParametrosSarlaft" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor ="White"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Días Hábiles antes de vencimiento para envío de primera alerta Sarlaft
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDiaHabPri" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="fteTxtDiaHabPri" runat="server" Enabled="True" FilterType="Custom, Numbers"
                    TargetControlID="TxtDiaHabPri">
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Días Hábiles antes de vencimiento para envío de segunda alerta Sarlaft
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDiaHabSeg" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="fteTxtDiaHabSeg" runat="server" Enabled="True" FilterType="Custom, Numbers"
                    TargetControlID="TxtDiaHabPri">
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Días calendario después de vencimiento para envío de alerta a entidades de control
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDiaCalCtrl" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="fteTxtDiaCalCtrl" runat="server" Enabled="True"
                    FilterType="Custom, Numbers" TargetControlID="TxtDiaCalCtrl">
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Texto de alerta por actualización de estado Sarlaft
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtObsEst" runat="server" MaxLength="1000" Width="500px" Rows="3"
                    TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Texto de alertas antes del vencimiento
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtAntVcto" runat="server" MaxLength="1000" Width="500px" Rows="3"
                    TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Texto de alerta después del vencimiento
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtControl" runat="server" MaxLength="1000" Width="500px" Rows="3"
                    TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Observaciones de cambio
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtObservacion" runat="server" MaxLength="1000" Width="500px" Rows="3"
                    TextMode="MultiLine"></asp:TextBox>
            </td>   
        </tr>
        <tr>
            <td class="th1" colspan="4" align="center">
                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>