﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Web.Services;
using System.Web.UI;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

public partial class login : Page
{
    string lsTitulo = "Autenticar en la aplicación";
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;

    /// <summary>
    /// 
    /// </summary>
    private static readonly string _ReCaptchaKey = ConfigurationManager.AppSettings.Get("ReCaptchaKey");
    public string ReCaptchaKey => _ReCaptchaKey;

    /// <summary>
    /// 
    /// </summary>
    private static readonly string ReCaptchaSecret = ConfigurationManager.AppSettings.Get("ReCaptchaSecret");

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;
        Response.AppendHeader("Cache-Control", "no-store");
        //20220211 contraseña
        if (this.Request.QueryString["cambio"] != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('Contraseña modificada correctamente');", true);
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbAceptar_Click(object sender, EventArgs e)
    {
        try
        {
            lblMensaje.Text = ""; //20220214 contraseña
            //Se borra la respuesta del Captcha por si falla 
            txtCaptcha.Text = string.Empty;

            if (txtClave.Value.Trim().Length > 0)
            {
                string lsCondicion = "";
                string ObaseDatos = ConfigurationManager.AppSettings["BaseDatos"];
                string Oservidor = ConfigurationManager.AppSettings["Servidor"];
                string ObaseDatosInf = ConfigurationManager.AppSettings["BaseDatosInf"]; //20211110 conexion replica
                string OservidorInf = ConfigurationManager.AppSettings["ServidorInf"]; //20211110 conexion replcia
                string Ousuario = Request.Form["TxtUsuario"];
                string Oclave = Request.Form["TxtClave"].Trim();
                string Oclave1 = Request.Form["TxtClave"].Trim();
                clConexion lConexion = null;
                //CSHA256 oSHA256 = new CSHA256();
                InfoSessionVO info = new InfoSessionVO(Oservidor, ObaseDatos, Ousuario, Oclave, "", "", OservidorInf, ObaseDatosInf);//20211110 conexion replcia
                info.Programa = lsTitulo;

                //Verificar si se pudo conectar con la clave plana
                if (DelegadaBase.Servicios.autenticar(info))
                {
                    Session["TxtUser"] = Ousuario;
                    Session["TxtPassword_Old"] = Oclave;
                    Session["cambio"] = "no";
                    Response.Redirect("~/Login_New.aspx");
                    return;
                }


                //20220211 contraseña
                string lsUsuarGenerico = ConfigurationManager.AppSettings["UserGenerico"];
                string lsClaveGenerico = ConfigurationManager.AppSettings["PwdGenerico"];
                info = new InfoSessionVO(Oservidor, ObaseDatos, lsUsuarGenerico, lsClaveGenerico, "", "", OservidorInf, ObaseDatosInf);
                lConexion = new clConexion(info);
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "a_usuario", " login = '" + Ousuario + "'");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    if (lLector["estado_contr_fall"].ToString() == "I")
                        lblMensaje.Text = "usuario bloquedo por contraseña. Comuníquese con el administrador<br>";//20220616 broker
                    //20220616 broker
                    if (lLector["habilitado_broker"].ToString() == "S")
                        lblMensaje.Text = "Usuario no habilitado para el sistema se_gas";
                    lLector.Close();
                }
                else //20220616 broker
                    lblMensaje.Text = "Usuario no existe en el sistema";
                if (lblMensaje.Text == "")
                {
                    //20220211 contraseña
                    // Control Cambio Contraseña 20160822
                    info.Password = DelegadaBase.Servicios.CifrarCadena(Oclave);
                    info.Usuario = Ousuario; //20220211 contraseña
                    if (DelegadaBase.Servicios.autenticar(info))
                    {
                        lConexion = new clConexion(info);

                        //20220211 contraseña
                        lConexion.Abrir();
                        string[] lsNombreParametros = { "@P_cadena" };
                        SqlDbType[] lTipoparametros = { SqlDbType.VarChar };
                        string[] lValorParametros = { "update a_usuario set intentos_fallidos =0 where login = '" + txtUsuario.Value + "'" };
                        DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_EjecutarCadena", lsNombreParametros, lTipoparametros, lValorParametros, info);
//                        lConexion.Cerrar();
                        //20220211 fin contraseña
  //                      lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "a_usuario", " login = '" + Ousuario + "' and estado ='A'");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            if (lLector["cambio_contrasena"].ToString() == "S") // Control Cambio Contraseña 20160822
                            {
                                lLector.Close();
                                lLector.Dispose();
                                lConexion.Cerrar();
                                Session["TxtUser"] = Ousuario;
                                Session["TxtPassword_Old"] = Oclave;
                                Session["cambio"] = "no";
                                Response.Redirect("~/Login_New.aspx");
                                return;
                            }
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        //}
                        //if (DelegadaBase.Servicios.autenticar(info))
                        //{
                        lConexion = new clConexion(info);
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "a_usuario a, m_parametros_generales, m_parametros_sslp", " a.login = '" + Ousuario + "' and a.estado ='A'");  //20170913 RQ041-17 
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            if (lLector["estado"].ToString() == "A") // COntrol del estado del usuario al ingresar 20160712
                            {
                                //Valida la contraseña
                                string lsErrorCont = DelegadaBase.Servicios.ValidaCont(Oclave, info.Password, Ousuario, "I", info);
                                if (lsErrorCont == "")
                                {

                                    Session["DiasExp"] = lLector["dias_exp_cont"].ToString();
                                    info.codigo_grupo_usuario = Convert.ToInt32(lLector["codigo_grupo_usuario"].ToString());
                                    info.nombre = lLector["nombre"].ToString();
                                    info.codigo_usuario = Convert.ToInt32(lLector["codigo_usuario"].ToString());
                                    info.cod_comisionista = lLector["codigo_operador"].ToString();
                                    Session["infoSession"] = info;
                                    Session["codigo_subasta"] = "0";
                                    Session["TipoOperador"] = "";
                                    Session["DescTipoOperador"] = "";
                                    Session["MailGestor"] = lLector["mail_gestor"].ToString();
                                    Session["MailUsuario"] = lLector["e_mail"].ToString(); //20210915  visulaizacon y consutlas
                                    Session.Timeout = 9000;
                                    Session["cod_comisionista"] = lLector["codigo_operador"].ToString();
                                    Session["subastador"] = "N";
                                    Session["NomOperador"] = lLector["nombre"].ToString();
                                    Session["UrlSsmp"] = lLector["url_ssmp"].ToString();  //rq041-17 
                                                                                          // Traer Informacion para visulizar en la Pagina de Cabcera
                                                                                          // Traer Nombre Comisionista
                                                                                          // Traer Nombre Grupo usuario
                                    lConexion1 = new clConexion(info);
                                    lConexion1.Abrir();
                                    lsCondicion = " codigo_grupo_usuario = " + lLector["codigo_grupo_usuario"];
                                    SqlDataReader lLector1;
                                    lLector1 = DelegadaBase.Servicios.LlenarControl(lConexion1.gObjConexion, "pa_ValidarExistencia", "a_grupo_usuario", lsCondicion);
                                    if (lLector1.HasRows)
                                    {
                                        lLector1.Read();
                                        Session["NomGrupoUsuario"] = lLector1.GetValue(0) + " - " + lLector1.GetValue(1);
                                        //20210915  visulaizacon y consutlas
                                        if (lLector1["descripcion_adicional"].ToString() != "")
                                            Session["NomGrupoUsuarioAdc"] = lLector1["descripcion_adicional"].ToString();
                                        else
                                            Session["NomGrupoUsuarioAdc"] = lLector1["descripcion"].ToString();
                                        Session["tipoPerfil"] = lLector1["tipo_perfil"].ToString();
                                        Session["administrador_operador"] = lLector1["administrador_operador"].ToString();
                                        Session["subastador"] = lLector1["subastador"].ToString();
                                    }
                                    lLector1.Close();
                                    lLector1.Dispose();

                                    if (lLector["codigo_operador"].ToString() != "0")
                                    {
                                        lsCondicion = " codigo_operador = " + lLector["codigo_operador"];
                                        lLector1 = DelegadaBase.Servicios.LlenarControl(lConexion1.gObjConexion, "pa_ValidarExistencia", "m_operador ope, m_tipos_operador tpo ", lsCondicion + " and ope.tipo_operador = tpo.sigla");
                                        if (lLector1.HasRows)
                                        {
                                            lLector1.Read();
                                            Session["NomOperador"] = lLector1["codigo_operador"] + " - " + lLector1["razon_social"];
                                            Session["IdOperador"] = lLector1["codigo_operador"] + "-" + lLector1["codigo_tipo_doc"] + "-" + lLector1["no_documento"] + "-" + lLector1["razon_social"];
                                            Session["TipoOperador"] = lLector1["tipo_operador"].ToString();
                                            Session["DescTipoOperador"] = lLector1["descripcion"].ToString();
                                        }
                                        lLector1.Close();
                                        lLector1.Dispose();
                                    }
                                    /// Obtiene el Ultimo Ingreso al Sistema
                                    lsCondicion = "1=1 And usuario = '" + txtUsuario.Value.Trim() + "' And proceso = 'Acceso al sistema' order by fecha_hora desc ";
                                    lLector1 = DelegadaBase.Servicios.LlenarControl(lConexion1.gObjConexion, "pa_ValidarExistencia", "a_auditoria_proceso", lsCondicion);
                                    if (lLector1.HasRows)
                                    {
                                        lLector1.Read();
                                        Session["UltimoIngreso"] = lLector1["fecha_hora"].ToString();
                                    }
                                    else
                                    {
                                        Session["UltimoIngreso"] = DateTime.Now.ToString();
                                    }
                                    lLector1.Close();
                                    lLector1.Dispose();
                                    lLector.Close();
                                    lLector.Dispose();
                                    lConexion.Cerrar();
                                    lConexion1.Cerrar();
                                    DelegadaBase.Servicios.borrar_a_bloqueo_registro_usuario(info, Ousuario);
                                    DelegadaBase.Servicios.accesoSistema(info);
                                    Response.Redirect("./WebForms/Home.aspx");
                                    return;
                                }
                                else
                                {
                                    lblMensaje.Text = lsErrorCont;
                                }

                            }
                            else
                                lblMensaje.Text = "El usuario no está activo";
                        }
                    }
                    else
                    {
                        
                        //20220211 contraseña
                        info = new InfoSessionVO(Oservidor, ObaseDatos, lsUsuarGenerico, lsClaveGenerico, "", "", OservidorInf, ObaseDatosInf);
                        lConexion = new clConexion(info);
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "a_usuario, m_parametros_adc", " login = '" + Ousuario + "'");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            try
                            {
                                int liMaxInt = Convert.ToInt32(lLector["int_fall_contr"].ToString());
                                int liIntAct = Convert.ToInt32(lLector["intentos_fallidos"].ToString());
                                string lsTiempoDesb = lLector["minut_desbl_contr"].ToString();
                                liIntAct++;
                                string[] lsNombreParametros = { "@P_cadena" };
                                SqlDbType[] lTipoparametros = { SqlDbType.VarChar };
                                string[] lValorParametros = { "" };
                                lLector.Close();
                                if (liMaxInt <= liIntAct)
                                {
                                    lValorParametros[0] = "update a_usuario set estado_contr_fall ='I', intentos_fallidos =" + liIntAct.ToString() + ", fecha_bloq_contr = getdate() where login = '" + txtUsuario.Value + "'";
                                    DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_EjecutarCadena", lsNombreParametros, lTipoparametros, lValorParametros, info);
                                    lblMensaje.Text = "Usuario bloqueado por intentos fallidos. Espere "+ lsTiempoDesb + " minutos e intente de nuevo o comuníquese con el Administrador del SEGAS al correo <u>gestordegas@bolsamercantil.com.co</u>";
                                }
                                else
                                {
                                    lValorParametros[0] = "update a_usuario set intentos_fallidos =" + liIntAct.ToString() + " where login = '" + txtUsuario.Value + "'";
                                    DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_EjecutarCadena", lsNombreParametros, lTipoparametros, lValorParametros, info);
                                    lblMensaje.Text = "Su usuario o contraseña no son validos. Le quedan "+ (liMaxInt - liIntAct).ToString() + " intentos";
                                }
                            }
                            catch (Exception ex)
                            {
                                lLector.Close();
                            }

                        }
                        //20220211 fin contraseña
                    }
                }//20220211 contraseña
            }
            else
                lblMensaje.Text = "Debe Ingresar la Contraseña.!";
        }
        catch (Exception ex)
        {
            Toastr.Error(this, ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lkbCambio_Click(object sender, EventArgs e)
    {
        Session["TxtUser"] = Request.Form["TxtUsuario"];
        Session["TxtPassword_Old"] = Request.Form["TxtClave"];
        Session["cambio"] = "si";
        Response.Redirect("~/Login_New.aspx");
        return;
    }

    /// <summary>
    /// Metodo para cambiar la contraseña del usuario cuandso ingresa a Olvido Su Contraseña
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lkbOlvido_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        try
        {
            if (txtUsuario.Value.Trim().Length > 0)
            {
                string lsCavleEncript = "";
                string lsUsuarGenerico = ConfigurationManager.AppSettings["UserGenerico"];
                string lsClaveGenerico = ConfigurationManager.AppSettings["PwdGenerico"];
                string ObaseDatos = ConfigurationManager.AppSettings["BaseDatos"];
                string Oservidor = ConfigurationManager.AppSettings["Servidor"];
                string ObaseDatosInf = ConfigurationManager.AppSettings["BaseDatosInf"]; //20211110 conexion replica
                string OservidorInf = ConfigurationManager.AppSettings["ServidorInf"]; //20211110 conexion replcia
                clConexion lConexion = null;
                //CSHA256 oSHA256 = new CSHA256();
                InfoSessionVO info = new InfoSessionVO(Oservidor, ObaseDatos, lsUsuarGenerico, lsClaveGenerico, "", "", OservidorInf, ObaseDatosInf);//20211110 conexion replcia
                info.Programa = "Restauracion de Contraseña";
                if (DelegadaBase.Servicios.autenticar(info))
                {
                    // Valido que el usuario exista en el sistema
                    lConexion = new clConexion(info);
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "a_usuario", " login = '" + txtUsuario.Value.Trim() + "' ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        string lsMail = lLector["e_mail"].ToString();
                        string lsNombre = lLector["nombre"].ToString();
                        // Genero la Clave aleatoria
                        string lsClave = encriptar.RandomString(10);
                        lsCavleEncript = DelegadaBase.Servicios.CifrarCadena(lsClave);
                        lLector.Close();
                        lLector.Dispose();
                        /// Actualizo la Clave del Usuario
                        try
                        {
                            string[] lsNombreParametros = { "@P_cadena" };
                            SqlDbType[] lTipoparametros = { SqlDbType.VarChar };
                            string[] lValorParametros = { "ALTER LOGIN [" + txtUsuario.Value.Trim() + "] WITH PASSWORD=N'" + lsCavleEncript + "' " };
                            DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_EjecutarCadena", lsNombreParametros, lTipoparametros, lValorParametros, info);
                            if (info.mensaje_error == "" || info.mensaje_error == "NO SE ACTUALIZARON REGISTROS.")
                            {
                                lValorParametros[0] = "Update a_usuario SET cambio_contrasena = 'S', estado_contr_fall='A', intentos_fallidos=0 Where login = '" + txtUsuario.Value.Trim() + "' "; //20220211 contraseña
                                DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_EjecutarCadena", lsNombreParametros, lTipoparametros, lValorParametros);

                                ///// Envio del Mail de la Solicitud de la Correcion
                                string lsAsunto = "Notificación Restauración de Contraseña";
                                string lsMensaje = "De acuerdo a la solicitud enviada, nos permitimos informar que la nueva contraseña del usuario " + txtUsuario.Value + " es " + lsClave + " <br><br>";
                                lsMensaje += "Cordialmente, <br><br><br>";
                                lsMensaje += "Administrador de SEGAS <br>";
                                lsMensaje = "Estimado(a) " + lsNombre + " <br><br>" + lsMensaje;
                                clEmail mail = new clEmail(lsMail, lsAsunto, lsMensaje, "");
                                lblMensaje.Text = mail.enviarMailRec(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], txtUsuario.Value, "Sistema de Gas - Restauración Contraseña");
                                if (lblMensaje.Text == "")
                                {
                                    Toastr.Success(this, "Restauración de Contraseña Finalizada. Por Favor revise su e_mail.!");
                                }
                            }
                            else
                            {
                                lblMensaje.Text = "Usuario NO existe en la Instancia de la base de Datos.!";
                                Toastr.Error(this, lblMensaje.Text);
                            }
                        }
                        catch (Exception ex)
                        {
                            Toastr.Error(this, "Problemas actulizar contraseña!" + ex.Message);
                        }
                    }
                    else
                        Toastr.Error(this, "El Usuario Ingresado NO existe en la Base de Datos.!");
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                }
                else
                    Toastr.Error(this, "No Se puedo Conextar con la base de Datos. Favor comunicar error a la BMC.!");
            }
            else
                Toastr.Error(this, "Debe Ingresar el Usuario para Ingresar por esta opción.!");
        }
        catch (Exception ex)
        {
            Toastr.Error(this, "Se presento un problema en el Proceso de Olvido su Contraseña.! " + ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="response"></param>
    /// <returns></returns>
    [WebMethod]
    public static string VerifyCaptcha(string response)
    {
        var url = "https://www.google.com/recaptcha/api/siteverify?secret=" + ReCaptchaSecret + "&response=" + response;
        return new WebClient().DownloadString(url);
    }
}