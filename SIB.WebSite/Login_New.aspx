﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login_New.aspx.cs" Inherits="Login_New" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <!--begin::Base Path (base relative path for assets of this page) -->
    <base href="../">

    <!--end::Base Path -->
    <meta charset="utf-8" />
    <title>.:: BNA - Actualizar Clave ::.</title>
    <link href="css/estilo.css" rel="stylesheet" type="text/css" />
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
    <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=es&onload=onloadCallback&render=explicit" async defer></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <!--end::Fonts -->

    <!--begin::Page Scripts(used by this page) -->
    <script src="./Script/dev/pages/login/login-1.js" type="text/javascript"></script>
    <!--end::Page Scripts -->

    <link href="./Stylesheets/vendors/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/tether.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/bootstrap-datepicker3.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/daterangepicker.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/bootstrap-select.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/bootstrap-switch.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/select2.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/nouislider.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/owl.carousel.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/owl.theme.default.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/dropzone.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/summernote.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/animate.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/toastr.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/morris.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/sweetalert2.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/socicon.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/line-awesome.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/flaticon2.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/vendors/all.min.css" rel="stylesheet" type="text/css" />

    <link href="./Stylesheets/dev/pages/login-1.css" rel="stylesheet" type="text/css" />
    <link href="./Stylesheets/dev/style.bundle.css" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="./media/logos/favicon.ico" />
</head>
<body style="background-position: center top; background-size: 100% 350px;" class="kt-page--loading-enabled kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent">
    <!-- begin:: Page -->
    <div class="kt-grid kt-grid--ver kt-grid--root kt-page">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
                <!--begin::Aside-->
                <div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url(./media//bg/bg-4.jpg);">
                    <div class="kt-grid__item">
                        <%--20200727--%>
                        <%--<a href="#" class="kt-login__logo">--%>
                            <img src="./media/logos/logoSegas.svg" height="60px">
                        <%--</a>--%>
                    </div>
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
                        <div class="kt-grid__item kt-grid__item--middle">
                            <h3 class="kt-login__title">Sistema Electrónico de GAS</h3>
                            <h4 class="kt-login__subtitle">Somos el gestor del mercado de GAS Natural en Colombia</h4>
                        </div>
                    </div>
                    <div class="kt-grid__item">
                        <div class="kt-login__info">
                            <div class="kt-login__copyright">
                                © 2019 SEGAS
                            </div>
                            <div class="kt-login__menu">
                                <a href="http://www.bmcbec.com.co/t%c3%a9rminos-y-condiciones/" class="kt-link" target="_blank">Términos y condiciones</a> <%--20200727--%>
                                <a href="#" class="kt-link">Contácto</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--begin::Aside-->

                <!--begin::Content-->
                <div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">

                    <!--begin::Body-->
                    <div class="kt-login__body">

                        <!--begin::Signin-->
                        <div class="kt-login__form">
                            <div class="kt-login__title">
                                <h2>Acceso al sistema</h2>
                                <h7>Diligencie su nombre de Usuario, la contraseña anterior, la nueva contraseña y la
                                    confirmación de esta</h7>
                            </div>

                            <!--begin::Form-->
                            <form class="kt-form" action="" novalidate="novalidate" runat="server">

                                <asp:ScriptManager ID="ScriptManager1" runat="server" />

                                <div class="row">

                                   <%-- <div class="form-group">
                                        <asp:Label ID="Label1" Style="display: inline-block;" CssClass="invalid-feedback" runat="server"></asp:Label>
                                    </div>--%>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label for="TxtUser">Usuario   </label>
                                            <asp:TextBox ID="TxtUser" runat="server"  CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="El id. de usuario no puede estar en blanco!!"
                                                ControlToValidate="TxtUser" Font-Size="8pt" Display="Dynamic" runat="server">
                                            </asp:RequiredFieldValidator>

                                        </div>
                                        <br/>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label for="TxtPassword_Old">Contraseña Anterior  </label>

                                            <asp:TextBox ID="TxtPassword_Old" runat="server"  CssClass="form-control" TextMode="Password"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="La contraseña anterior no puede estar en blanco!!"
                                                ControlToValidate="TxtPassword_Old" Font-Size="8pt" Display="Dynamic" runat="server">
                                            </asp:RequiredFieldValidator>


                                        </div>
                                        <br/>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label for="TxtPassword_New">Contraseña Nueva </label>
                                            <asp:TextBox ID="TxtPassword_New" runat="server"   CssClass="form-control" TextMode="Password"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="La contraseña nueva no puede estar en blanco!!"
                                                ControlToValidate="TxtPassword_New" Font-Size="8pt" Display="Dynamic" runat="server">
                                            </asp:RequiredFieldValidator>

                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label for="TxtPassword_Confirm">Confirmación de Contraseña </label>

                                            <asp:TextBox ID="TxtPassword_Confirm" runat="server"  CssClass="form-control" TextMode="Password"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="La confirmación de la contraseña nueva no puede estar en blanco!!"
                                                ControlToValidate="TxtPassword_Confirm" Font-Size="8pt" Display="Dynamic" runat="server">
                                            </asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="CompareValidator1" ErrorMessage="La confirmación y la contraseña nueva no son iguales!!"
                                                ControlToValidate="TxtPassword_Confirm" ControlToCompare="TxtPassword_New" Font-Size="8pt"
                                                Display="Dynamic" runat="server">
                                            </asp:CompareValidator><br />
                                            <asp:Label ID="lblMensaje" runat="server" Width="280px" ForeColor="Red"></asp:Label>

                                        </div>
                                        <br/>
                                    </div>
                                    <div style="text-align: left">
                                        <asp:Button ID="imbAceptar" runat="server" Text="Aceptar" CssClass="btn btn-success"
                                            OnClick="btnIngresar_Click" ForeColor="White" />
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                        <!--end::Signin-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Content-->
            </div>
        </div>
    </div>

    <!-- end:: Page -->


</body>
</html>
