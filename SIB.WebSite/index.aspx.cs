﻿using System;
using System.Web.UI;

public partial class Index : Page
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Redirect("~/login.aspx");
    }
}
