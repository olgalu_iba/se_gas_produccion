﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;
using System.Text;
using Segas.Web.Elements;

public partial class Facturacion_frm_ConsultaFacturacion : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Consulta Facturación"; //20170126 rq122 ajuste facturacion y gar 
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        //Se agrega el comportamiento del botón
        buttons.ExportarExcelOnclick += ImgExcel_Click;
        buttons.FiltrarOnclick += imbConsultar_Click;


        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlMes, "m_mes", " 1=1 order by mes", 0, 1);
            lConexion.Cerrar();

            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar };
            buttons.Inicializar(botones: botones);
        }
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            if (lsTabla != "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            lDdl.Items.Add(lItem1);

        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, EventArgs e)
    {
        CargarDatos();
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        bool Error = false;
        int ldValor;
        if (TxtAno.Text.Trim().Length > 0)
        {
            try
            {
                ldValor = Convert.ToInt32(TxtAno.Text.Trim());
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Formato Inválido en el Campo Ano. <br>");
                Error = true;
            }
        }
        else
        {
            Toastr.Warning(this, "Debe Ingresar algun Parametro de búsqueda. <br>");
            Error = true;
       
        }
        if (!Error)
        {
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetConsFacturacion";
                lComando.Parameters.Add("@P_ano", SqlDbType.Int).Value = TxtAno.Text.Trim();
                lComando.Parameters.Add("@P_mes", SqlDbType.Int).Value = ddlMes.SelectedValue;
                lComando.Parameters.Add("@P_plano_descargado", SqlDbType.VarChar).Value = ddlPlanoDescargado.SelectedValue;
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgMaestro.DataSource = lds;
                dtgMaestro.DataBind();
                tblGrilla.Visible = true;
                buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pude Generar el Informe.! " + ex.Message.ToString());
            }
        }
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, EventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfFactura" + DateTime.Now + ".xls";
        string lstitulo_informe = "Informe Facturacion";
        string lsTituloParametros = "";
        try
        {
            lsTituloParametros += "Ano: " + TxtAno.Text.Trim();
            if (ddlMes.SelectedValue != "0")
                lsTituloParametros += " - Mes: " + ddlMes.SelectedItem.ToString();
            if (ddlPlanoDescargado.SelectedValue != "")
                lsTituloParametros += " - Plano Descargado: " + ddlPlanoDescargado.SelectedItem.ToString();
            dtgMaestro.Columns[11].Visible = false;  //2017115 rq040-17
            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgMaestro.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);

            lform.Controls.Add(dtgMaestro);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());
            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
            dtgMaestro.Columns[11].Visible = true;  //2017115 rq040-17
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "No se Pude Generar el Excel.!" + ex.Message.ToString());
        }
    }
    /// <summary>
    /// Nombre: dtgMaestro_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            try
            {
                hdfFechaCorte.Value = e.Item.Cells[0].Text;
                lblFechaCorte.Text = e.Item.Cells[0].Text;
                hdfNoDocumento.Value = e.Item.Cells[1].Text;
                lblOperador.Text = e.Item.Cells[1].Text + " - " + e.Item.Cells[2].Text;
                lblValorFactura.Text = e.Item.Cells[4].Text;
                hdfValorFactura.Value = e.Item.Cells[4].Text.Replace(",", "");
                if (DateTime.Now <= Convert.ToDateTime(e.Item.Cells[8].Text))
                {
                    tblModifica.Visible = true;
                    tblGrilla.Visible = false;
                    //tblDatos.Visible = false;
                }
                else
                {
                    Toastr.Warning(this, "Ya se venció el Plazo {" + e.Item.Cells[8].Text + "} para la Modificación de la Factura.!");
                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Problemas en la Recuperación de la Información. " + ex.Message.ToString());
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnActualizar_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_fecha_corte", "@P_no_documento", "@P_valor_ajuste", "@P_observacion" };
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Decimal, SqlDbType.VarChar };
        string[] lValorParametros = { hdfFechaCorte.Value, hdfNoDocumento.Value, TxtValorAJuste.Text, TxtObservacion.Text };
        lblMensaje.Text = "";
        decimal ldValor = 0;

        try
        {
            if (TxtValorAJuste.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar el Valor del Ajuste . <br>";
            else
            {
                try
                {
                    ldValor = Convert.ToDecimal(TxtValorAJuste.Text.Trim());
                    if (ldValor > Convert.ToDecimal(hdfValorFactura.Value))
                        lblMensaje.Text += "El Valor del Ajuste NO puede ser mayor que el valor de la Factura. <br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Invalido en Valor del Ajuste. <br>";
                }
            }
            if (TxtObservacion.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la Observación del Ajuste . <br>";
            if (lblMensaje.Text == "")
            {
                lConexion.Abrir();
                goInfo.mensaje_error = "";
                DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetActFactura", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensaje.Text = "Se presento un Problema en la Actualizacion de la Factura.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    lConexion.Cerrar();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Actualizado Correctamente.!" + "');", true);
                    tblGrilla.Visible = true;
                    //tblDatos.Visible = true;
                    tblModifica.Visible = false;
                    TxtValorAJuste.Text = "";
                    TxtObservacion.Text = "";
                    CargarDatos();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRegresar_Click(object sender, EventArgs e)
    {
        tblModifica.Visible = false;
        tblGrilla.Visible = true;
        //tblDatos.Visible = true;
        TxtValorAJuste.Text = "";
        TxtObservacion.Text = "";
    }
}