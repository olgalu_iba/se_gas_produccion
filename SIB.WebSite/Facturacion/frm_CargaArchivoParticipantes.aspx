﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_CargaArchivoParticipantes.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Facturacion_frm_CargaArchivoParticipantes" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="95%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th1">
                <asp:Label ID="lblTitulo" runat="server" forecolor="white"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <br /><br /><br /><br /><br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="95%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Seleccione el Archivo de Participantes
            </td>
            <td class="td2">
                <asp:FileUpload ID="FuArchivoEnergia" runat="server" Width="60%" EnableTheming="true" />
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:Button ID="btnCargar" runat="server" Text="Cargar" OnClick="btnCargar_Click"
                    OnClientClick="MostrarCrono(); this.disabled = true;" UseSubmitBehavior="false" />
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">
                Tiempo transcurrido: &nbsp;
                <input type="text" name="display" id="display" size="8" value="00:00:0" class="reloj"
                    readonly="true">
                <asp:Label ID="lblTotalRegistros" runat="server" Visible="false"></asp:Label>
            </td>
        </tr>
        <tr id="trMensaje" visible="false" runat="server">
            <td colspan="2">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                <asp:Literal ID="ltCargaArchivo" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>