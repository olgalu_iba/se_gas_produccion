﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ConsultaFacturacion.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Facturacion_frm_ConsultaFacturacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name"> Año facturación</label>
                            <asp:TextBox ID="TxtAno" runat="server" ValidationGroup="detalle"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name"> Mes Facturación</label>
                          <asp:DropDownList ID="ddlMes" runat="server">
                            </asp:DropDownList>

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name"> Plano Descargado</label>
                       <asp:DropDownList ID="ddlPlanoDescargado" runat="server">
                                <asp:ListItem Value="" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="Label2" runat="server" ForeColor="Red"></asp:Label>
                            <asp:Literal ID="ltCargaArchivo" runat="server"></asp:Literal>
                        </div>
                    </div>


                </div>
                 <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
       
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                <asp:HiddenField ID="hdfFechaCorte" runat="server" />
                <asp:HiddenField ID="hdfNoDocumento" runat="server" />
                <asp:HiddenField ID="hdfValorFactura" runat="server" />
            </td>
        </tr>
    </table>
    <div  runat="server"
        id="tblGrilla" visible="false">
       
                  <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                    <asp:DataGrid ID="dtgMaestro" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" AllowPaging="false"
                        PagerStyle-HorizontalAlign="Center"  OnEditCommand="dtgMaestro_EditCommand">
                     
                        <Columns>
                            <%--0--%>
                            <asp:BoundColumn DataField="fecha_corte" HeaderText="Fecha Corte" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--1--%>
                            <asp:BoundColumn DataField="no_documento" HeaderText="No. Identificación" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn> <%--20170126 rq122--%>
                            <%--2--%>
                            <asp:BoundColumn DataField="nombre_participante" HeaderText="Nombre Participante"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="280px"></asp:BoundColumn>
                            <%--3--%>
                            <asp:BoundColumn DataField="cantidad_energia" HeaderText="Cantidad de Energía" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--4--%>
                            <asp:BoundColumn DataField="valor_factura" HeaderText="Valor Factura" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--5--%>
                            <%--20180524 rq038-17--%>
                            <asp:BoundColumn DataField="contabilizado" HeaderText="Interfaz generada" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--6--%>
                            <asp:BoundColumn DataField="fecha_proceso" HeaderText="Fecha Proceso" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="120px"></asp:BoundColumn>
                            <%--7--%>
                            <asp:BoundColumn DataField="modificada" HeaderText="Modificada" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--8--%>
                            <asp:BoundColumn DataField="fecha_maxima_modifica" Visible="false"></asp:BoundColumn>
                            <%--20171115 rq040-17--%>
                            <asp:BoundColumn DataField="valor_debito" HeaderText="Nota Débito" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0: ###,###,###,###,##0.00}" ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--10--%>
                            <%--20171115 rq040-17--%>
                            <asp:BoundColumn DataField="valor_credito" HeaderText="Nota Crédito" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0: ###,###,###,###,##0.00}" ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--11--%>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar"></asp:EditCommandColumn>
                        </Columns>
                        
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />  
                    </asp:DataGrid>
                </div>
           
    </div>
    <table id="tblModifica" runat="server" border="0" align="center" cellpadding="3"
        cellspacing="2" width="80%" visible="false">
        <tr>
            <td class="td1">
                Fecha Corte
            </td>
            <td class="td2">
                <asp:Label ID="lblFechaCorte" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
            </td>
            <td class="td1">
                Operador
            </td>
            <td class="td2">
                <asp:Label ID="lblOperador" runat="server" ForeColor="Red" Font-Bold="true" Width="200px"></asp:Label>
            </td>
            <td class="td1">
                Valor Factura
            </td>
            <td class="td2">
                <asp:Label ID="lblValorFactura" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Valor Ajuste
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtValorAJuste" runat="server" ValidationGroup="detalle" Width="100px"
                    MaxLength="30"></asp:TextBox>
            </td>
            <td class="td1">
                Observación Ajuste
            </td>
            <td class="td2" colspan="3">
                <asp:TextBox ID="TxtObservacion" runat="server" ValidationGroup="detalle" TextMode="MultiLine"
                    Rows="2" Width="500px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="6" align="center">
                <asp:Button ID="btnActualizar" runat="server" Text="Actualizar" OnClick="btnActualizar_Click"
                    OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                <asp:Button ID="btnRegresar" runat="server" Text="Regresar" OnClick="btnRegresar_Click" />
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="6" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="14px"></asp:Label>
            </td>
        </tr>
    </table>
            </div>
        </div>
    </div>
  
   
 </asp:Content>