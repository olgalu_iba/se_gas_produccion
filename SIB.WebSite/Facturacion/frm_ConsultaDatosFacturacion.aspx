﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ConsultaDatosFacturacion.aspx.cs" Inherits="Facturacion_frm_ConsultaDatosFacturacion" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Año Facturación" AssociatedControlID="TxtAno" runat="server" />
                            <asp:TextBox ID="TxtAno" runat="server" Width="100%" CssClass="form-control" ValidationGroup="detalle"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtAno" runat="server" TargetControlID="TxtAno"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Mes Facturación" AssociatedControlID="ddlMes" runat="server" />
                            <asp:DropDownList ID="ddlMes" Width="100%" CssClass="form-control" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
                            <asp:DropDownList ID="ddlOperador" Width="100%" CssClass="form-control" runat="server" />
                        </div>
                    </div>
                </div>

                <div runat="server" visible="false" id="tblGrilla">
                    <div class="table table-responsive">
                        <%--20180126 rq107-16--%>
                        <asp:DataGrid ID="dtgMaestro" AutoGenerateColumns="False" AllowPaging="true" PageSize="20" OnPageIndexChanged="dtgMaestro_PageIndexChanged" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" runat="server">
                            <Columns>
                                <%--0--%>
                                <asp:BoundColumn DataField="fecha_corte" HeaderText="Fecha Corte" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <%--1--%>
                                <asp:BoundColumn DataField="no_documento" HeaderText="No. Identificación (NIT)" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--2--%>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Participante"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                
                                <%--4--%>
                                <asp:BoundColumn DataField="valor_cu" HeaderText="𝐶𝑈𝑚−1 " ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                                <%--5--%>
                                <asp:BoundColumn DataField="valor_iae" HeaderText="𝐼𝐴𝐸𝑎 ($)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <%--6--%>
                                <asp:BoundColumn DataField="valor_i" HeaderText="I" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                                <%--7--%>
                                <asp:BoundColumn DataField="valor_ipp" HeaderText="𝐼𝑃𝑃𝑚−1" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                                <%--8--%>
                                <asp:BoundColumn DataField="valor_ipp_base" HeaderText="𝐼𝑃𝑃0" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                                <%--*--%>
                                <asp:BoundColumn DataField="cantidad_energia" HeaderText="Cantidad de Energía Participante - 𝐸𝐶𝑉𝑣,𝑚−1 (MBTUD)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0: ###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--10--%>
                                <asp:BoundColumn DataField="cantidad_total" HeaderText="Cantidad de Energía Total - Σ(Σ𝐸𝐶𝑉𝑖,𝑣𝑖+Σ𝐸𝐶𝑉𝑗,𝑣𝑗)𝑣 (MBTUD)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0: ###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--3--%>
                                <asp:BoundColumn DataField="valor_factura" HeaderText="Valor Factura ($)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--10--%>
                                <asp:BoundColumn DataField="valor_total" HeaderText="Total a facturar ($)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0: ###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--11--%>
                                <asp:BoundColumn DataField="fecha_proceso" HeaderText="Fecha Proceso" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <%--12--%>
                                <asp:BoundColumn DataField="modificada" HeaderText="Modificada" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <%--13--%>
                                <asp:BoundColumn DataField="no_nota_debito" HeaderText="Nota Débito ($)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,##0}"></asp:BoundColumn>
                                <%--14--%>
                                <asp:BoundColumn DataField="no_nota_credito" HeaderText="Nota Crédito ($)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,##0}"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                    <asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False" AllowPaging="false" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" Visible="false">
                        <Columns>
                                <%--0--%>
                                <asp:BoundColumn DataField="fecha_corte" HeaderText="Fecha Corte" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <%--1--%>
                                <asp:BoundColumn DataField="no_documento" HeaderText="No. Identificación (NIT)" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--2--%>
                                <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Participante"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                
                                <%--4--%>
                                <asp:BoundColumn DataField="valor_cu" HeaderText="CUm-1 " ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                                <%--5--%>
                                <asp:BoundColumn DataField="valor_iae" HeaderText="IAEa ($)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,##0}"></asp:BoundColumn>
                                <%--6--%>
                                <asp:BoundColumn DataField="valor_i" HeaderText="I" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                                <%--7--%>
                                <asp:BoundColumn DataField="valor_ipp" HeaderText="IPPm-1" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                                <%--8--%>
                                <asp:BoundColumn DataField="valor_ipp_base" HeaderText="IPP0" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,##0.00}"></asp:BoundColumn>
                                <%--*--%>
                                <asp:BoundColumn DataField="cantidad_energia" HeaderText="Cantidad de Energía Participante - ECVv,m-1 (MBTUD)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0: ###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--10--%>
                                <asp:BoundColumn DataField="cantidad_total" HeaderText="Cantidad de Energía Total - E(ECVivi+EECVj,vj)v (MBTUD)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0: ###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--3--%>
                                <asp:BoundColumn DataField="valor_factura" HeaderText="Valor Factura ($)" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--10--%>
                                <asp:BoundColumn DataField="valor_total" HeaderText="Total a facturar ($)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0: ###,###,###,###,###,##0}"></asp:BoundColumn>
                                <%--11--%>
                                <asp:BoundColumn DataField="fecha_proceso" HeaderText="Fecha Proceso" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <%--12--%>
                                <asp:BoundColumn DataField="modificada" HeaderText="Modificada" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                <%--13--%>
                                <asp:BoundColumn DataField="no_nota_debito" HeaderText="Nota Débito ($)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,##0}"></asp:BoundColumn>
                                <%--14--%>
                                <asp:BoundColumn DataField="no_nota_credito" HeaderText="Nota Crédito ($)" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0:###,##0}"></asp:BoundColumn>
                        </Columns>
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </div>
</asp:Content>


