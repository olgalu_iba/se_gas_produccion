﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;
using System.Text;
using System.Web.UI;
using Segas.Web.Elements;

/// <summary>
/// 
/// </summary>
public partial class Facturacion_frm_PlanoContabilidad : Page
{
    private InfoSessionVO goInfo;
    private static string lsTitulo = "Genera Plano Contable";
    private clConexion lConexion;
    private clConexion lConexion1;
    private SqlDataReader lLector;
    private DataSet lds = new DataSet();
    private SqlDataAdapter lsqldata = new SqlDataAdapter();
    private string sRutaArc = ConfigurationManager.AppSettings["rutaCarga"];

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo;
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);
        //Se agrega el comportamiento del botón

        buttons.FiltrarOnclick += imbConsultar_Click;

        if (IsPostBack) return;
        //Titulo
        Master.Titulo = "Facturación";
        // Llenar controles del Formulario
        lConexion.Abrir();
        LlenarControles(lConexion.gObjConexion, ddlMes, "m_mes", " 1=1 order by mes", 0, 1);
        lConexion.Cerrar();

        //Botones
        buttons.Inicializar(botones: new[] { EnumBotones.Buscar });
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        // Proceso para Cargar el DDL de ciudad
        SqlDataReader lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        var lItem = new ListItem { Value = "0", Text = "Seleccione" };
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            var lItem1 = new ListItem();
            if (lsTabla != "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"] + "-" + lLector["razon_social"];
            }
            lDdl.Items.Add(lItem1);

        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, EventArgs e)
    {
        HplDescargaConta.Visible = false;

        string lsNombreArchivo = "planoContaGas" + goInfo.Usuario + DateTime.Now.Millisecond + ".txt";
        string lsRuta = "";
        string lsRuta1 = "";
        int liConsecutivo = 0;
        lsRuta = sRutaArc + lsNombreArchivo;
        lsRuta1 = "../archivos/" + lsNombreArchivo;
        HplDescargaConta.NavigateUrl = lsRuta1;
        SqlDataReader lLector;
        StreamWriter oArchivo = File.CreateText(lsRuta);
        var mensaje = new StringBuilder();
        string lsCadena = "";
        lConexion = new clConexion(goInfo);
        string lsAno = "0";
        string lsMes = "0";
        string lsDia = "0";
        string lsDiaF = "0";
        string lsAnoF = "0";
        string lsMesF = "0";
        string lsFechaFact = "";
        string lsFechaVenc = "";
        string[] lsNombreParametros = { "@P_ano", "@P_mes" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
        Object[] lValorParametros = { TxtAno.Text.Trim(), ddlMes.SelectedValue };

        string[] lsNombreParametrosF = { "@P_fecha", "@P_no_documento" };
        SqlDbType[] lTipoparametrosF = { SqlDbType.VarChar, SqlDbType.VarChar };
        String[] lValorParametrosF = { "", "0" };

        int liValor = 0;
        if (TxtAno.Text.Trim().Length > 0)
        {
            try
            {
                liValor = Convert.ToInt32(TxtAno.Text.Trim());
            }
            catch (Exception)
            {
                mensaje.Append("Formato Inválido en el Campo Año. <br>");
            }
        }
        else
            mensaje.Append("Debe Ingresar Año. <br>");

        if (ddlMes.SelectedValue == "0")
            mensaje.Append("Debe Ingresar Mes. <br>");

        if (!string.IsNullOrEmpty(mensaje.ToString()))
        {
            Toastr.Error(this, mensaje.ToString());
            return;
        }
        lConexion.Abrir();
        lConexion1.Abrir();
        SqlTransaction oTransaccion = lConexion1.gObjConexion.BeginTransaction(IsolationLevel.Serializable);
        try
        {
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetConsFacturacion", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
            if (lLector.HasRows)
            {
                while (lLector.Read())
                {
                    lsAnoF = lLector["fecha_factura"].ToString().Substring(0, 4);
                    lsMesF = lLector["fecha_factura"].ToString().Substring(5, 2);
                    lsDiaF = lLector["fecha_factura"].ToString().Substring(8, 2);
                    lsFechaFact = lsDiaF + "/" + lsMesF + "/" + lsAnoF;
                    lsAno = lLector["fecha_vencimiento"].ToString().Substring(0, 4);
                    lsMes = lLector["fecha_vencimiento"].ToString().Substring(5, 2);
                    lsDia = lLector["fecha_vencimiento"].ToString().Substring(8, 2);
                    lsFechaVenc = lsDia + "/" + lsMes + "/" + lsAno;
                    lsAno = lLector["fecha_corte"].ToString().Substring(0, 4);
                    lsMes = lLector["fecha_corte"].ToString().Substring(5, 2);

                    liConsecutivo++;
                    lsCadena = "861"; // Tipo Operacion
                    lsCadena += "\t" + liConsecutivo; // No Factura
                    lsCadena += "\t" + lsFechaFact; // Fecha Factura
                    lsCadena += "\t" + "101"; // Sucursal
                    lsCadena += "\t" + lLector["no_documento"]; // Cliente
                    lsCadena += "\t" + "1"; // Detalle Cliente
                    lsCadena += "\t" + "1"; // Contacto
                    lsCadena += "\t" + "0"; // Actividad Economica
                    lsCadena += "\t" + "1"; // Moneda
                    lsCadena += "\t" + "0"; // Valor Tasa
                    lsCadena += "\t" + lsFechaFact; // Fecha Tasa
                    lsCadena += "\t" + "SERVICIO GESTOR MERCADO DE GAS " + lsMes + "-" + lsAno; // Descripcion
                    lsCadena += "\t" + "0"; // Referencia para Factura
                    lsCadena += "\t" + lsFechaFact; // Fecha de Pago Ordinario
                    lsCadena += "\t" + lsFechaFact; // Fecha de Pago Ex-Ordinario
                    lsCadena += "\t" + "0"; // % de Recargo Extraordinario
                    lsCadena += "\t" + lsFechaFact; // Fecha de Pago Extemporaneo
                    lsCadena += "\t" + "0"; // % de Recargo Extemporaneo
                    lsCadena += "\t" + "1"; // Consecutivo Detalle
                    lsCadena += "\t" + "401180"; // Producto
                    lsCadena += "\t" + "0"; // Bodega
                    lsCadena += "\t" + "1"; // Unidad de Medida
                    lsCadena += "\t" + "1"; // Cantidad
                    lsCadena += "\t" + lLector["valor_factura"]; // Valor Producto
                    lsCadena += "\t" + "P"; // Tipo Descuento
                    lsCadena += "\t" + "0"; // Valor de descuento
                    lsCadena += "\t" + "SERVICIO GESTOR MERCADO DE GAS " + lsMesF + "-" + lsAnoF; // Descripcion Detalle
                    lsCadena += "\t" + "101"; // Sucursal
                    lsCadena += "\t" + "0608001"; // Centro de Costo
                    lsCadena += "\t" + "0551"; // Proyecto
                    lsCadena += "\t" + "10105002"; // Area
                    lsCadena += "\t" + "1"; // Vendedor
                    lsCadena += "\t" + "F"; // Tipo Factura
                    lsCadena += "\t" + "A"; // Estado Documento
                    lsCadena += "\t" + lsFechaFact; // Fecha Inicial Factura
                    lsCadena += "\t" + lsFechaVenc; // Fecha Final Factura
                    lsCadena += "\t" + "1"; // Destino
                    lsCadena += "\t" + "N"; // Clase de Nota
                    lsCadena += "\n";
                    oArchivo.Write(lsCadena);
                    /// Actualizo la información del Registro como ya descargado el archivo de la factura.
                    lValorParametrosF[0] = lLector["fecha_corte"].ToString();
                    lValorParametrosF[1] = lLector["no_documento"].ToString();
                    if (!DelegadaBase.Servicios.EjecutarProcedimientoConTransaccion(lConexion1.gObjConexion, "pa_SetActualContab", lsNombreParametrosF, lTipoparametrosF, lValorParametrosF, oTransaccion, goInfo))
                    {
                        mensaje.Append("Se presento un Problema en la Actualizacion del Mail Proceso.! " + goInfo.mensaje_error);
                        oTransaccion.Rollback();
                        lConexion1.Cerrar();
                        break;
                    }

                }
                if (string.IsNullOrEmpty(mensaje.ToString()))
                {
                    oTransaccion.Commit();
                    lConexion1.Cerrar();
                    lConexion.Cerrar();
                    trBajar.Visible = true;
                    HplDescargaConta.Visible = true;
                }
                else
                {
                    trBajar.Visible = false;
                    HplDescargaConta.Visible = false;
                }
            }
            else
            {
                Toastr.Info(this, "No se Encontraron Registros Para Exportar.!");
                trBajar.Visible = false;
            }
            oArchivo.Close();
            oArchivo.Dispose();
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            oArchivo.Close();
            oArchivo.Dispose();
            oTransaccion.Rollback();
            lConexion.Cerrar();
            lConexion1.Cerrar();
            Toastr.Error(this, "Se Presento un Problema en el Proceso de Exportación de los Datos. <br>" + ex.Message);
        }
    }
}