﻿using System;
using System.Collections;
using System.Globalization;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.DataVisualization.Charting;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Facturacion_frm_datosOperador : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Modificación De operadores para cartas de facturación";
    clConexion lConexion = null;
    DataSet lds = new DataSet();


    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and tipo_operador in ('P','I') order by razon_social", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlOperadorMod, "m_operador", " estado = 'A' and tipo_operador in ('P','I') order by razon_social", 0, 1);
            lConexion.Cerrar();
            CargarDatos();
        }
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            if (lsTabla != "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector["no_documento"].ToString();
                lItem1.Text = lLector["no_documento"].ToString() + "-" + lLector["razon_social"].ToString();
            }

            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        CargarDatos();
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        lblMensaje.Text = "";
        string[] lsNombreParametros = { "@P_no_documento" };
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar };
        string[] lValorParametros = { ddlOperador.SelectedValue };

        if (lblMensaje.Text == "")
        {
            try
            {
                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetOperadorFact", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                lConexion.Cerrar();
                if (dtgConsulta.Items.Count <= 0)
                    lblMensaje.Text = "No se encontraron Registros.";
                else
                {
                    tblGrilla.Visible = true;
                    imbExcel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo consultar la información.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            try
            {
                ddlOperadorMod.SelectedValue = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text;
                TxtRazon.Text = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[1].Text;
                TxtRepresentante.Text = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[2].Text;
                TxtDireccion.Text = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[3].Text;
                TxtCiudad.Text = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[4].Text;
                TxtRadicado.Text = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[5].Text;
                if (this.dtgConsulta.Items[e.Item.ItemIndex].Cells[5].Text == "&nbsp;")
                    TxtRadicado.Text = "";
                else
                    TxtRadicado.Text = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[5].Text;
                if (this.dtgConsulta.Items[e.Item.ItemIndex].Cells[6].Text == "&nbsp;")
                    ddlEstado.SelectedValue = "A";
                else
                    ddlEstado.SelectedValue = this.dtgConsulta.Items[e.Item.ItemIndex].Cells[6].Text;
                tblSolicitud.Visible = false;
                tblGrilla.Visible = false;
                tblDatos.Visible = true;
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al modificar el registro. " + ex.Message.ToString();
            }
        }
    }

    /// <summary>
    /// Exportacion a Excel de la Información  de la Grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbExcel_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        try
        {
            string lsNombreArchivo = Session["login"] + "InfDatosOpeFact" + DateTime.Now + ".xls";
            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            lpagina.EnableEventValidation = false;
            lpagina.Controls.Add(lform);
            dtgConsulta.EnableViewState = false;
            dtgConsulta.Columns[7].Visible = false;
            lform.Controls.Add(dtgConsulta);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;

            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["NomOperador"] + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='5' align='left'><font face=Arial size=4>" + "Consulta Datos Operadores facturacion" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write(lsb.ToString());
            Response.End();
            Response.Flush();
            dtgConsulta.Columns[7].Visible = true;
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas al Consultar los Registros. " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnModificar_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_no_documento", "@P_representante_legal", "@P_direccion", "@P_ciudad", "@P_no_radicado", "@P_estado" };
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
        string[] lValorParametros = { ddlOperadorMod.SelectedValue, TxtRepresentante.Text, TxtDireccion.Text, TxtCiudad.Text, TxtRadicado.Text, ddlEstado.SelectedValue };
        lblMensaje.Text = "";
        try
        {
            if (TxtRepresentante.Text == "")
                lblMensaje.Text = "Debe digitar el nombre del representante legal<br>";
            if (TxtDireccion.Text == "")
                lblMensaje.Text = "Debe digitar la dirección<br>";
            if (TxtCiudad.Text == "")
                lblMensaje.Text = "Debe digitar la ciudad<br>";
            if (TxtRadicado.Text == "")
                lblMensaje.Text = "Debe digitar el número del radicado<br>";
            if (lblMensaje.Text == "")
            {
                lConexion.Abrir();
                goInfo.mensaje_error = "";
                DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetDatosOpeFact", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensaje.Text = "Se presentó un problema en la actualización de los datos del operador.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Datos actualizados Correctamente.!" + "');", true);
                    CargarDatos();
                    tblDatos.Visible = false;
                    tblGrilla.Visible = true;
                    tblSolicitud.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }

    protected void btnRegresar_Click(object sender, EventArgs e)
    {
        CargarDatos();
        tblDatos.Visible = false;
        tblGrilla.Visible = true;
        tblSolicitud.Visible = true;
    }
}