﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_PlanoContabilidad.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Facturacion_frm_PlanoContabilidad" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Año Facturación" AssociatedControlID="TxtAno" runat="server" />
                            <asp:TextBox ID="TxtAno" runat="server" CssClass="form-control" ValidationGroup="detalle" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Mes Facturación" AssociatedControlID="ddlMes" runat="server" />
                            <asp:DropDownList ID="ddlMes" runat="server" CssClass="form-control" />
                        </div>
                    </div>
                    <div id="trBajar" visible="false" class="col-sm-12 col-md-6 col-lg-4" runat="server">
                        <div class="form-group">
                            <asp:Label ID="Label2" runat="server" ForeColor="Red"></asp:Label>
                            <asp:Literal ID="ltCargaArchivo" runat="server"></asp:Literal>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                            <b>Para descargar el archivo plano, Ubíquese sobre el vínculo y presione el</b><br />
                            <b>botón derecho del mouse y de clic en la opción de Guardar destino como...</b><br />
                            <asp:HyperLink ID="HplDescargaConta" runat="server" Visible="false">Descarga Archivo Plano Contabilidad</asp:HyperLink>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
