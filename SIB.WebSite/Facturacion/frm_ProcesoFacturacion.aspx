﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ProcesoFacturacion.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Facturacion_frm_ProcesoFacturacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Proceso</label>
                            <asp:TextBox ID="TxtFechaIni" runat="server" ValidationGroup="detalle" CssClass="form-control" ></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4" style="display:none">
                        <div class="form-group">
                            <label for="Name">Modificacion</label>
                            <asp:DropDownList ID="ddlPlanoDescargado" runat="server" CssClass="form-control" >
                                <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                            </asp:DropDownList>

                        </div>
                    </div>
                    <%--20200727--%>
                   <%-- <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                            <asp:Label ID="Label2" runat="server" ForeColor="Red"></asp:Label>
                            <asp:Literal ID="ltCargaArchivo" runat="server"></asp:Literal>
                        </div>
                    </div>--%>


                </div>
            </div>
        </div>
    </div>

</asp:Content>
