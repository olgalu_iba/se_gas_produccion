﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_datosOperador.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Facturacion_frm_datosOperador" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
            width="100%">
            <tr>
                <td align="center" class="th1" style="width: 80%;">
                    <asp:Label ID="lblTitulo" runat="server" forecolor="white"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <br /><br /><br /><br /><br /><br /><br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table id="tblSolicitud" runat="server" border="0" align="center" cellpadding="3"
        cellspacing="2" width="80%">
        <tr>
            <td class="td1">
                Operador
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlOperador" runat="server" Width="250px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="8" align="center">
                <asp:Button ID="btnConsultar" runat="server" Text="Buscar" OnClick="btnConsultar_Click"
                    OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/exel 2007 3D.png"
                    Height="40" OnClick="imbExcel_Click" Visible="false" />
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="8" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="14px"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 1150px; height: 450px;">
                    <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                        AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1"
                        OnEditCommand="dtgConsulta_EditCommand">
                        <Columns>
                            <%--0--%>
                            <asp:BoundColumn DataField="no_documento" HeaderText="Nit" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--1--%>
                            <asp:BoundColumn DataField="razon_social" HeaderText="razaon social" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--2--%>
                            <asp:BoundColumn DataField="representante_legal" HeaderText="Representante legal">
                            </asp:BoundColumn>
                            <%--3--%>
                            <asp:BoundColumn DataField="direccion" HeaderText="direccion"></asp:BoundColumn>
                            <%--4--%>
                            <asp:BoundColumn DataField="ciudad" HeaderText="ciudad"></asp:BoundColumn>
                            <%--5--%>
                            <asp:BoundColumn DataField="no_radicado" HeaderText="radicado"></asp:BoundColumn>
                            <%--6--%>
                            <asp:BoundColumn DataField="estado" HeaderText="estado"></asp:BoundColumn>
                            <%--7--%>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar"></asp:EditCommandColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
    <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
        width="100%" visible="false">
        <tr>
            <td class="td1">
                Operador:
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlOperadorMod" runat="server" Width="250px" Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Razón Social
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtRazon" runat="server" MaxLength="200" Width="300px" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Representante Legal
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtRepresentante" runat="server" MaxLength="200" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Dirección
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDireccion" runat="server" MaxLength="100" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Ciudad
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCiudad" runat="server" MaxLength="100" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Número de Radicado
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtRadicado" runat="server" MaxLength="50" Width="100px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Estado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlEstado" runat="server" Width="250px">
                    <asp:ListItem Value="A">Activo</asp:ListItem>
                    <asp:ListItem Value="I">Inactivo</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:Button ID="btnModificar" runat="server" Text="Modificar" OnClick="btnModificar_Click"
                    OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnRegresar" runat="server" Text="Regresar" OnClick="btnRegresar_Click"
                    OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="6" align="center">
                <asp:Label ID="Label1" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="14px"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>