﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;
using System.Text;
using Segas.Web.Elements;

public partial class Facturacion_frm_ConsultaDatosFacturacion : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Consulta Datos Facturación";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);

        buttons.ExportarExcelOnclick += ImgExcel_Click;
        buttons.FiltrarOnclick += btnConsultar_Click;

        if (IsPostBack) return;

        Master.Titulo = "Facturación";
        /// Llenar controles del Formulario
        lConexion.Abrir();
        LlenarControles(lConexion.gObjConexion, ddlMes, "m_mes", " 1=1 order by mes", 0, 1);
        LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " tipo_operador ='P' and estado = 'A' order by razon_social", 0, 4); 
        lConexion.Cerrar();
        //20210422 ajsute facturacion
        if (Session["tipoPerfil"].ToString() == "N")
        {
            ddlOperador.SelectedValue = goInfo.cod_comisionista;
            ddlOperador.Enabled = false;
        }
        //Botones
        EnumBotones[] botones = { EnumBotones.Buscar, EnumBotones.Excel };
        buttons.Inicializar(botones: botones);

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            if (lsTabla != "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            lDdl.Items.Add(lItem1);

        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        dtgMaestro.CurrentPageIndex = 0; //20180126 rq107 - 16
        CargarDatos();
    }

    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        var lblMensaje = new StringBuilder();
        int ldValor;
        if (TxtAno.Text.Trim().Length > 0)
        {
            try
            {
                ldValor = Convert.ToInt32(TxtAno.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Append("Formato Inválido en el Campo Año. <br>");
            }
        }
        //20210422 ajsute facturacion
        else
            lblMensaje.Append("Debe Ingresar el año de facturación. <br>");
        //20210422 ajsute facturacion
        if (ddlMes.SelectedValue == "0" )
            lblMensaje.Append("Debe seleccionar el mes de facturación. <br>");
        if (!string.IsNullOrEmpty(lblMensaje.ToString()))
        {
            Toastr.Warning(this, lblMensaje.ToString());
            return;
        }

        try
        {
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            SqlCommand lComando = new SqlCommand();
            lComando.Connection = lConexion.gObjConexion;
            lComando.CommandTimeout = 3600;
            lComando.CommandType = CommandType.StoredProcedure;
            lComando.CommandText = "pa_GetDatosFacturacion";
            lComando.Parameters.Add("@P_año_facturacion", SqlDbType.Int).Value = TxtAno.Text.Trim();
            lComando.Parameters.Add("@P_mes_facturacion", SqlDbType.Int).Value = ddlMes.SelectedValue;
            lComando.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = ddlOperador.SelectedValue;
            lComando.ExecuteNonQuery();
            lsqldata.SelectCommand = lComando;
            lsqldata.Fill(lds);
            dtgMaestro.DataSource = lds;
            dtgMaestro.DataBind();
            dtgExcel.DataSource = lds;
            dtgExcel.DataBind();
            tblGrilla.Visible = true;
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            Toastr.Error(this, "No se Pude Generar el Informe.! " + ex.Message.ToString());
        }

    }
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    /// 20180126 rq107-16
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();
    }

    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Exportar la Grilla a Excel
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, EventArgs e)
    {
        string lsNombreArchivo = goInfo.Usuario.ToString() + "InfFactura" + DateTime.Now + ".xls";
        string lstitulo_informe = "Consulta Datos Facturación";
        string lsTituloParametros = "";
        dtgExcel.Visible = true;
        try
        {
            if (TxtAno.Text.Trim().Length > 0)
                lsTituloParametros += "Año: " + TxtAno.Text.Trim();
            if (ddlMes.SelectedValue != "0")
                lsTituloParametros += " - Mes: " + ddlMes.SelectedItem.ToString();
            if (ddlOperador.SelectedValue != "0")
                lsTituloParametros += " - Operador: " + ddlOperador.SelectedItem.ToString();

            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgExcel.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);

            lform.Controls.Add(dtgExcel);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());
            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
            dtgExcel.Visible = false;
        }
        catch (Exception ex)
        {
            Toastr.Error(this, "No se Pudo Generar el Excel.!" + ex.Message);
        }
    }
}