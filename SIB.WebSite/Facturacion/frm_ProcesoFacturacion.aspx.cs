﻿using System;
using System.Collections;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using Segas.Web.Elements;

public partial class Facturacion_frm_ProcesoFacturacion : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Proceso de Facturación";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        //Se agrega el comportamiento del botón
     
        buttons.FiltrarOnclick += imbConsultar_Click;

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            DateTime ldfecha;
            ldfecha = DateTime.Now;
            TxtFechaIni.Text = ldfecha.Year.ToString() + "/" + ldfecha.Month.ToString().PadLeft(2,'0') + "/" + ldfecha.Day.ToString().PadLeft(2, '0');
            //TxtFecha.Enabled = false;

            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar };
            buttons.Inicializar(botones: botones);
        }

    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, EventArgs e)
    {
        bool Error = false;
        DateTime ldFecha;
        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Formato Inválido en el Campo Fecha Corte. <br>");
                Error = true;
            }
        }
        if (TxtFechaIni.Text.Trim().Length <= 0)
        {
            Toastr.Warning(this, "Debe Ingresar algun Parametro de búsqueda. <br>");
            Error = true;
        }
        if (!Error)
        {
            var lblMensaje = new StringBuilder();
            try
            {
                string[] lsNombreParametros = { "@P_fecha_proceso" };
                SqlDbType[] lTipoparametros = { SqlDbType.VarChar };
                string[] lValorParametros = { TxtFechaIni.Text };
                lConexion.Abrir();
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetFacturacion", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    Toastr.Warning(this, "Se presento un Problema en el Proceso de Facturacion.! " + goInfo.mensaje_error.ToString());
                    lConexion.Cerrar();
                }
                else
                {
                    //lblMensaje.Text = "";
                    string lsAsunto = "";
                    string lsMensaje = "";
                    string lsMail = "";
                    SortedList oListaBmc = new SortedList();
                    lLector.Read();
                    if (lLector["mensaje"].ToString() != "OK")
                    {
                        lblMensaje.Append(lLector["mensaje"].ToString()+"<br>");
                        //lblMensaje.Text = lLector["mensaje"].ToString() + "\\n";  //20170126 rq122 ajuste facturacion y gar 
                        while (lLector.Read())
                        {
                            lblMensaje.Append(lLector["mensaje"].ToString() + "<br>");
                            //lblMensaje.Text = lLector["mensaje"].ToString() + "\\n"; //20170126 rq122 ajuste facturacion y gar
                        }
                        lLector.Close();
                        lLector.Dispose();
                        
                        /// Envio del Mail del Proceso de Facturacion
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_mail_procesos", " codigo_proceso = 1 ");
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                if (!oListaBmc.Contains(lLector["mail_destinatario"].ToString()))
                                    oListaBmc.Add(lLector["mail_destinatario"].ToString(), "");
                            }
                        }
                        lLector.Close();
                        lLector.Dispose();

                        ///// Envio del Mail de la Solicitud de la Correcion
                        lsAsunto = "Notificación Ejecución Proceso de Facturación con Errores";
                        lsMensaje = "Nos permitimos  informarle que el Administrador de SE_GAS acaba de ejecutar el proceso de facturación con errores, con Fecha de Proceso: " + TxtFechaIni.Text + ". <br><br>";
                        lsMensaje += "Errores: " + lblMensaje.ToString()+ " <br><br><br>";
                        lsMensaje += "Cordialmente, <br><br><br>";
                        lsMensaje += "Administrador SEGAS <br>";
                        lsMensaje = "Señores BMC: <br><br>" + lsMensaje;
                        /// Envio de los Mail de los Operadores Contrarios
                        for (int i = 0; i < oListaBmc.Count; i++)
                        {
                            lsMail = oListaBmc.GetKey(i).ToString();
                            clEmail mailBMC = new clEmail(lsMail, lsAsunto, lsMensaje, "");
                            lblMensaje.Append(mailBMC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas"));
                        }
                    }
                    else
                    {
                        lLector.Close();
                        lLector.Dispose();
                        Toastr.Success(this, "Proceso Ejecutado con Exito.!");
                        if (lblMensaje.ToString() != "")
                        {
                            Toastr.Success(this, lblMensaje.ToString());
                        }

                        //20210422 ajsute facturacion
                        //creacion de cartas de garantias
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_facturacion_gas", " fecha_proceso_pant=  '" + TxtFechaIni.Text + "'");
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                clCartaGarantias clCarta = new clCartaGarantias();
                                string lsError = clCarta.generarCartaGrtia(lLector["no_documento"].ToString(), TxtFechaIni.Text, goInfo);
                            }
                        }
                        lLector.Close();
                        lLector.Dispose();
                        //20210422 fin ajsute facturacion

                        /// Envio del Mail del Proceso de Facturacion
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_mail_procesos", " codigo_proceso = 1 ");
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                if (!oListaBmc.Contains(lLector["mail_destinatario"].ToString()))
                                    oListaBmc.Add(lLector["mail_destinatario"].ToString(), "");
                            }
                        }
                        lLector.Close();
                        lLector.Dispose();

                        ///// Envio del Mail de la Solicitud de la Correcion
                        lsAsunto = "Notificación Ejecución Proceso de Facturación Exitosa";
                        lsMensaje = "Nos permitimos  informarle que el Administrador de SE_GAS acaba de ejecutar el proceso de facturación de forma exitosa, con Fecha de Proceso: " + TxtFechaIni.Text + ". <br><br>";
                        lsMensaje += "Cordialmente, <br><br><br>";
                        lsMensaje += "Administrador SEGAS <br>";
                        lsMensaje = "Señores BMC: <br><br>" + lsMensaje;
                        /// Envio de los Mail de los Operadores Contrarios
                        for (int i = 0; i < oListaBmc.Count; i++)
                        {
                            lsMail = oListaBmc.GetKey(i).ToString();
                            clEmail mailBMC = new clEmail(lsMail, lsAsunto, lsMensaje, "");
                            lblMensaje.Append(mailBMC.enviarMail(ConfigurationManager.AppSettings["UserSmtp"].ToString(), ConfigurationManager.AppSettings["PwdSmtp"].ToString(), ConfigurationManager.AppSettings["ServidorSmtp"].ToString(), goInfo.Usuario, "Sistema de Gas"));
                        }
                    }
                    lLector.Close();
                    lLector.Dispose();


                    lConexion.Cerrar();
                }
                if (lblMensaje.ToString()!= "")
                    Toastr.Warning(this, lblMensaje.ToString());
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pude Generar el Informe.! " + ex.Message.ToString());
            }
        }
    }
}