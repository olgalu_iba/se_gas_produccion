﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ConsultaContFacturados.aspx.cs" Inherits="Facturacion_frm_ConsultaContFacturados" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Año Facturación" AssociatedControlID="TxtAno" runat="server" />
                            <asp:TextBox ID="TxtAno" runat="server" Width="100%" CssClass="form-control" ValidationGroup="detalle"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtAno" runat="server" TargetControlID="TxtAno"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Mes Facturación" AssociatedControlID="ddlMes" runat="server" />
                            <asp:DropDownList ID="ddlMes" Width="100%" CssClass="form-control" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
                            <asp:DropDownList ID="ddlOperador" Width="100%" CssClass="form-control selectpicker" data-live-search="true" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operación" AssociatedControlID="TxtContrato" runat="server" />
                            <asp:TextBox ID="TxtContrato" runat="server" Width="100%" CssClass="form-control" ValidationGroup="detalle"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TxtContrato"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <%--<div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Comprador" AssociatedControlID="ddlComprador" runat="server" />
                            <asp:DropDownList ID="ddlComprador" Width="100%" CssClass="form-control" runat="server" />
                        </div>
                    </div>--%>
                </div>

                <div runat="server" visible="false" id="tblGrilla">
                    <div class="table table-responsive">
                        <%--20180126 rq107-16--%>
                        <asp:DataGrid ID="dtgMaestro" AutoGenerateColumns="False" AllowPaging="true" PageSize="20" OnPageIndexChanged="dtgMaestro_PageIndexChanged" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" runat="server">
                            <Columns>
                            <%--0--%>
                            <asp:BoundColumn DataField="fecha_corte" HeaderText="Fecha Corte" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--1--%>
                            <asp:BoundColumn DataField="numero_operacion" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn> <%--20170126 rq122--%> 
                            <%--2--%>
                            <asp:BoundColumn DataField="modalidad_contractual" HeaderText="Modalidad Contractual"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="120px"></asp:BoundColumn>
                            <%--3--%>
                            <asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="250px"></asp:BoundColumn>
                            <%--4--%>
                            <asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="250px"></asp:BoundColumn>
                            <%--5--%>
                            <asp:BoundColumn DataField="punta" HeaderText="Punta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--6--%>
                            <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="140px"></asp:BoundColumn>
                            <%--7--%>
                            <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="140px"></asp:BoundColumn>
                            <%--8--%>
                            <asp:BoundColumn DataField="tipo_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--*--%>
                            <asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--10--%>
                            <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--11--%>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="100px"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                    <asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False" AllowPaging="false" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" Visible="false">
                        <Columns>
                            <%--0--%>
                            <asp:BoundColumn DataField="fecha_corte" HeaderText="Fecha Corte" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--1--%>
                            <asp:BoundColumn DataField="numero_operacion" HeaderText="No. Operación" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn> <%--20170126 rq122--%> 
                            <%--2--%>
                            <asp:BoundColumn DataField="modalidad_contractual" HeaderText="Modalidad Contractual"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="120px"></asp:BoundColumn>
                            <%--3--%>
                            <asp:BoundColumn DataField="vendedor" HeaderText="Vendedor" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="250px"></asp:BoundColumn>
                            <%--4--%>
                            <asp:BoundColumn DataField="comprador" HeaderText="Comprador" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="250px"></asp:BoundColumn>
                            <%--5--%>
                            <asp:BoundColumn DataField="punta" HeaderText="Punta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--6--%>
                            <asp:BoundColumn DataField="fecha_inicial" HeaderText="Fecha Inicial" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="140px"></asp:BoundColumn>
                            <%--7--%>
                            <asp:BoundColumn DataField="fecha_final" HeaderText="Fecha Final" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="140px"></asp:BoundColumn>
                            <%--8--%>
                            <asp:BoundColumn DataField="tipo_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--*--%>
                            <asp:BoundColumn DataField="producto" HeaderText="Producto" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--10--%>
                            <asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="100px"></asp:BoundColumn>
                            <%--11--%>
                            <asp:BoundColumn DataField="cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"
                                DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="100px"></asp:BoundColumn>
                        </Columns>
                        <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

                    
