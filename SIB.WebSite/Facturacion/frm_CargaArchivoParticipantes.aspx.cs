﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;

public partial class Facturacion_frm_CargaArchivoParticipantes : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Carga Archivo Energía Participantes";  //20180815 BUG230
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;
    string sRutaArc = ConfigurationManager.AppSettings["rutaCarga"].ToString();

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);
    }

    /// <summary>
    /// Nombre: ValidarArchivo
    /// Fecha: Julio 5 de 2012
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para realizar la validación del archivo plano
    /// Modificacion:
    /// </summary>
    /// <param name="lsRutaArchivo"></param>
    /// <returns></returns>
    public string[] ValidarArchivo(string lsRutaArchivo)
    {
        Int32 liNumeroLinea = 0;
        decimal ldValor = 0;
        string lsCadenaErrores = "";
        string[] lsCadenaRetorno = { "", "" };
        int liTotalRegistros = 0;
        DateTime ldFecha;

        StreamReader lLectorArchivo = new StreamReader(lsRutaArchivo);
        try
        {
            lConexion.Abrir();
            /// Recorro el Archivo de Excel para Validarlo
            lLectorArchivo = File.OpenText(lsRutaArchivo);
            while (!lLectorArchivo.EndOfStream)
            {
                liTotalRegistros = liTotalRegistros + 1;
                /// Obtiene la fila del Archivo
                string lsLineaArchivo = lLectorArchivo.ReadLine();
                if (lsLineaArchivo.Length > 0)
                {
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if (oArregloLinea.Length != 6)
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 5 },  en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                    }
                    else
                    {
                        /// Valida Fecha
                        if (oArregloLinea.GetValue(0).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "No Ingreso la Fecha  {" + oArregloLinea.GetValue(0).ToString() + "}, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(0).ToString().Trim());
                                if (oArregloLinea.GetValue(0).ToString().Trim().Length != 10)
                                    lsCadenaErrores = lsCadenaErrores + "Valor Invalido en  la Fecha  {" + oArregloLinea.GetValue(0).ToString() + "}, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Invalido en  la Fecha  {" + oArregloLinea.GetValue(0).ToString() + "}, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                            }
                        }

                        /// No Documento Participante
                        if (oArregloLinea.GetValue(1).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "No Ingreso el  DOcumento del Participante  {" + oArregloLinea.GetValue(1).ToString() + "}, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                if (!DelegadaBase.Servicios.ValidarExistencia("m_operador", " no_documento = '" + oArregloLinea.GetValue(1).ToString().Trim() + "' And estado = 'A'  ", goInfo))
                                    lsCadenaErrores = lsCadenaErrores + "El Documento del Participante  {" + oArregloLinea.GetValue(1).ToString() + "} NO  existe en la Base de datos, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El No Documento del Participante  {" + oArregloLinea.GetValue(1).ToString() + "} no es válido, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                            }
                        }
                        /// Valida Producto o Destino Rueda
                        if (oArregloLinea.GetValue(2).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "No Ingreso El Producto  {" + oArregloLinea.GetValue(2).ToString() + "}, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        else
                        {
                            if (oArregloLinea.GetValue(2).ToString().Trim() != "G" && oArregloLinea.GetValue(2).ToString().Trim() != "T")
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido  en Producto  {" + oArregloLinea.GetValue(2).ToString() + "} debe ser {G o T}, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// Tipo de Subasta y Tipo de Contrato
                        if (oArregloLinea.GetValue(3).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "No Ingreso el  Tipo de Subasta  {" + oArregloLinea.GetValue(3).ToString() + "}, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        else
                        {
                            if (oArregloLinea.GetValue(4).ToString().Trim().Length <= 0)
                                lsCadenaErrores = lsCadenaErrores + "No Ingreso el  Tipo de Contrato  {" + oArregloLinea.GetValue(4).ToString() + "}, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                            else
                            {
                                try
                                {
                                    if (!DelegadaBase.Servicios.ValidarExistencia("m_tipos_subasta", " codigo_tipo_subasta = '" + oArregloLinea.GetValue(3).ToString().Trim() + "' And estado = 'A'  ", goInfo))
                                        lsCadenaErrores = lsCadenaErrores + "El Tipo de Subasta  {" + oArregloLinea.GetValue(3).ToString() + "} NO  existe en la Base de datos, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                                    else
                                    {
                                        if (!DelegadaBase.Servicios.ValidarExistencia("m_modalidad_contractual", " codigo_modalidad = " + oArregloLinea.GetValue(4).ToString().Trim() + " And  estado = 'A'  ", goInfo))
                                            lsCadenaErrores = lsCadenaErrores + "El Tipo de Contrato  {" + oArregloLinea.GetValue(4).ToString() + "} NO  existe en la Base de datos o NO pertenece a la Subasta Ingresada, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                                        else
                                        {
                                            if (!DelegadaBase.Servicios.ValidarExistencia("m_subastas_facturar", " codigo_tipo_subasta = " + oArregloLinea.GetValue(3).ToString().Trim() + " And codigo_modalidad = " + oArregloLinea.GetValue(4).ToString().Trim() + " And destino_rueda = '" + oArregloLinea.GetValue(2).ToString().Trim() + "' And estado = 'A'  ", goInfo))
                                                lsCadenaErrores = lsCadenaErrores + "La Subasta, Tipo de Contrato y Producto  NO  existe en la Base de datos de Subastas para Facturar, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                                        }
                                    }

                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "Valor Inválido en los Campos de la Subasta o Tipo de Contrato  {" + oArregloLinea.GetValue(1).ToString() + "} no es válido, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                                }
                            }
                        }
                        /// Valida antidad de Energia
                        if (oArregloLinea.GetValue(5).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "No Ingreso Cantidad de Energia  {" + oArregloLinea.GetValue(5).ToString() + "}, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldValor = Convert.ToDecimal(oArregloLinea.GetValue(5).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La Cantidad de Energía  {" + oArregloLinea.GetValue(8).ToString() + "} no es válido, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                            }
                        }
                    }
                }
            }
            lLectorArchivo.Close();
            lLectorArchivo.Dispose();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lLectorArchivo.Close();
            lLectorArchivo.Dispose();
            lsCadenaRetorno[0] = lsCadenaErrores;
            lsCadenaRetorno[1] = "0";
        }
        lsCadenaRetorno[0] = lsCadenaErrores;
        lsCadenaRetorno[1] = liTotalRegistros.ToString();
        return lsCadenaRetorno;
    }
    /// <summary>
    /// Realiza la Carga del Archivo Plano
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCargar_Click(object sender, EventArgs e)
    {
        string lsRutaArchivo = "";
        string lsNombreArchivo = "";
        string[] lsRetorno = { "", "" };
        string lsErrores = "";
        string lsError = "";
        bool oTransOK = true;
        ltCargaArchivo.Text = "";
        /// Verifica que se pueda Cargar al Servidor el Archivo de Precios
        lsNombreArchivo = DateTime.Now.Millisecond.ToString() + FuArchivoEnergia.FileName.ToString();
        try
        {
            lsRutaArchivo = sRutaArc + lsNombreArchivo;
            FuArchivoEnergia.SaveAs(lsRutaArchivo);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Ocurrio un Problema al Cargar el Archivo.!');", true);
            lblMensaje.Text = "Problemas en la Carga del Archivo. " + ex.Message.ToString();
            oTransOK = false;
            trMensaje.Visible = true;
        }
        /// Realiza las Validaciones de los Archivos
        if (oTransOK)
        {
            try
            {
                trMensaje.Visible = true;
                ltCargaArchivo.Text = ltCargaArchivo.Text + "***LOG DE CARGA DE ARCHIVO *** <br>";
                lsRetorno = ValidarArchivo(lsRutaArchivo);
                lsErrores = lsRetorno[0];
                if (lsErrores == "")
                {
                    string[] lsNombreParametros = { "@P_fecha", "@P_no_documento", "@P_destino_rueda", "@P_codigo_tipo_subasta", "@P_codigo_modalidad", "@P_cantidad" };
                    SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal };
                    String[] lValorParametros = { "", "0", "", "0", "0", "0" };

                    lConexion.Abrir();
                    SqlTransaction oTransaccion;
                    oTransaccion = lConexion.gObjConexion.BeginTransaction(System.Data.IsolationLevel.Serializable);
                    StreamReader lLectorArchivo = new StreamReader(lsRutaArchivo);

                    /// Recorro el Archivo de Excel para Actualizar los datos
                    lLectorArchivo = File.OpenText(lsRutaArchivo);
                    while (!lLectorArchivo.EndOfStream)
                    {
                        /// Obtiene la fila del Archivo
                        string lsLineaArchivo = lLectorArchivo.ReadLine();
                        if (lsLineaArchivo.Length > 0)
                        {
                            Array oArregloLinea = lsLineaArchivo.Split(',');
                            lValorParametros[0] = oArregloLinea.GetValue(0).ToString(); // Fecha
                            lValorParametros[1] = oArregloLinea.GetValue(1).ToString(); // No Documento
                            lValorParametros[2] = oArregloLinea.GetValue(2).ToString(); // Destino Rueda
                            lValorParametros[3] = oArregloLinea.GetValue(3).ToString(); // Tipo Subasta
                            lValorParametros[4] = oArregloLinea.GetValue(4).ToString(); // Modalidad Contractual
                            lValorParametros[5] = oArregloLinea.GetValue(5).ToString(); // Cantidad

                            if (!DelegadaBase.Servicios.EjecutarProcedimientoConTransaccion(lConexion.gObjConexion, "pa_SetArchivoParticipantes", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo))
                            {
                                lsErrores += "Se presentó un Problema en el Ingreso de los Requisitos.! " + goInfo.mensaje_error.ToString();
                                oTransaccion.Rollback();
                                lConexion.Cerrar();
                                break;
                            }
                        }
                    }
                    if (lsErrores == "")
                    {
                        oTransaccion.Commit();
                        lConexion.Cerrar();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Carga Realizada con Exito.!');", true);
                        lsErrores += "<br> Archivo Cargado Correctamente.!";
                    }
                    lLectorArchivo.Close();
                    lLectorArchivo.Dispose();
                }
            }
            catch (Exception ex)
            {
                lsErrores += "Error en la carga del archivo plano " + ex.Message.ToString(); ;
            }
            if (lsErrores.Length > 0)
            {
                trMensaje.Visible = true;
                ltCargaArchivo.Text = lsErrores;
            }
        }
    }
}
