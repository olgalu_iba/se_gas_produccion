﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_indicadorFacturacion.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Facturacion_frm_indicadorFacturacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Inicial</label>
                            <asp:TextBox ID="TxtFechaIni" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Final</label>
                            <asp:TextBox ID="TxtFechaFin" runat="server" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>

                </div>
                <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
                    id="tblMensaje">

                    <tr>
                        <td colspan="3" align="center">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                            <asp:HiddenField ID="hdfFechaCorte" runat="server" />
                            <asp:HiddenField ID="hdfNoDocumento" runat="server" />
                            <asp:HiddenField ID="hdfValorFactura" runat="server" />
                        </td>
                    </tr>
                </table>
                <div runat="server"
                    id="tblGrilla" visible="false">

                    <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                        <asp:DataGrid ID="dtgMaestro" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" AllowPaging="false"
                            PagerStyle-HorizontalAlign="Center" OnItemCommand="dtgMaestro_EditCommand" >

                            <Columns>
                                <%--0--%>
                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--1--%>
                                <asp:BoundColumn DataField="trimestre" HeaderText="No. Trimestre" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--2--%>
                                <asp:BoundColumn DataField="desc_trimestre" HeaderText="Trimestre"
                                    ItemStyle-HorizontalAlign="Left" ></asp:BoundColumn>
                                <%--3--%>
                                <asp:BoundColumn DataField="numerador" HeaderText="Cálculo Onir" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                <%--4--%>
                                <asp:BoundColumn DataField="denominador" HeaderText="Cálculo Ototal" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                <%--5--%>
                                <asp:BoundColumn DataField="ind_factura" HeaderText="Indicador Oportunidad de Facturación" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0: ###,###,###,##0.00 }%"></asp:BoundColumn>
                                <%--6--%>
                                <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                    <ItemTemplate>
                                        <div class="dropdown dropdown-inline">
                                            <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="flaticon-more-1"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                <!--begin::Nav-->
                                                <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <ul class="kt-nav">
                                                            <li class="kt-nav__item">
                                                                <asp:LinkButton ID="lkbDetalle" CssClass="kt-nav__link" CommandName="Detalle" runat="server">
                                                             <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Detalle</span>
                                                                </asp:LinkButton>
                                                            </li>
                                                        </ul>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <!--end::Nav-->
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>

                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--Modals--%>
    <div class="modal fade" id="detalle" tabindex="-1" role="dialog" aria-labelledby="mdldetalle" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="detalleInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="lblDetalle" runat="server">Detalle</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:DataGrid ID="dtgDetalle" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" AllowPaging="false"
                                            PagerStyle-HorizontalAlign="Center" >
                                            <Columns>
                                                <%--0--%>
                                                <asp:BoundColumn DataField="año" HeaderText="Año" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                <%--1--%>
                                                <asp:BoundColumn DataField="mes" HeaderText="No. Mes" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                                <%--2--%>
                                                <asp:BoundColumn DataField="nombre_mes" HeaderText="Mes"
                                                    ItemStyle-HorizontalAlign="Left" ></asp:BoundColumn>
                                                <%--3--%>
                                                <asp:BoundColumn DataField="fecha_maxima" HeaderText="Fecha Máxima Facturación" ItemStyle-HorizontalAlign="Right"
                                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                                <%--4--%>
                                                <asp:BoundColumn DataField="fecha_trasmite" HeaderText="Fecha Facturación" ItemStyle-HorizontalAlign="Right"
                                                    DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                            </Columns>
                                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                        </asp:DataGrid>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="detalle" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button Text="Regresar" CssClass="btn btn-secondary" OnClick="Cancel_OnClick" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

</asp:Content>
