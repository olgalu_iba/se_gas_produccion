﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.WebControls;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Text;
using System.Web.UI;
using Segas.Web.Elements;

public partial class Facturacion_frm_ConsultaCartaGarantias : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Consulta Cartas de Garantías";
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    String strRutaCarga;
    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);

        buttons.FiltrarOnclick += btnConsultar_Click;
        strRutaCarga = ConfigurationManager.AppSettings["RutaGarantia"].ToString();

        if (IsPostBack) return;

        Master.Titulo = "Facturación";
        /// Llenar controles del Formulario
        lConexion.Abrir();
        LlenarControles(lConexion.gObjConexion, ddlMes, "m_mes", " 1=1 order by mes", 0, 1);
        LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " tipo_operador ='P' and estado = 'A' order by razon_social", 0, 1); //20170126 rq122 ajuste facturacion y gar //20210422 ajsute facturacion
        lConexion.Cerrar();
        //20210422 ajsute facturacion
        if (Session["tipoPerfil"].ToString() == "N")
        {
            ddlOperador.SelectedValue = goInfo.cod_comisionista;
            ddlOperador.Enabled = false;
        }
        //Botones
        EnumBotones[] botones = { EnumBotones.Buscar };
        buttons.Inicializar(botones: botones);

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            if (lsTabla != "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            lDdl.Items.Add(lItem1);

        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        dtgMaestro.CurrentPageIndex = 0; //20180126 rq107 - 16
        CargarDatos();
    }

    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        var lblMensaje = new StringBuilder();
        int ldValor;
        if (TxtAno.Text.Trim().Length > 0)
        {
            try
            {
                ldValor = Convert.ToInt32(TxtAno.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Append("Formato Inválido en el Campo Año. <br>");
            }
        }
        //20210422 ajsute facturacion
        else
            lblMensaje.Append("Debe Ingresar el año de facturación. <br>");
        //20210422 ajsute facturacion
        if (ddlMes.SelectedValue == "0" )
            lblMensaje.Append("Debe seleccionar el mes de facturación. <br>");
        if (!string.IsNullOrEmpty(lblMensaje.ToString()))
        {
            Toastr.Warning(this, lblMensaje.ToString());
            return;
        }

        try
        {
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            SqlCommand lComando = new SqlCommand();
            lComando.Connection = lConexion.gObjConexion;
            lComando.CommandTimeout = 3600;
            lComando.CommandType = CommandType.StoredProcedure;
            lComando.CommandText = "pa_GetCartagarantiaVer";
            lComando.Parameters.Add("@P_año_facturacion", SqlDbType.Int).Value = TxtAno.Text.Trim();
            lComando.Parameters.Add("@P_mes_facturacion", SqlDbType.Int).Value = ddlMes.SelectedValue;
            lComando.Parameters.Add("@P_codigo_operador", SqlDbType.VarChar).Value = ddlOperador.SelectedValue;
            lComando.ExecuteNonQuery();
            lsqldata.SelectCommand = lComando;
            lsqldata.Fill(lds);
            dtgMaestro.DataSource = lds;
            dtgMaestro.DataBind();
            tblGrilla.Visible = true;
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "No se Pudo Generar el Informe.! " + ex.Message.ToString());
        }

    }
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    /// 20180126 rq107-16
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();
    }

    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        var lblMensaje = new StringBuilder();
        if (e.CommandName.Equals("Ver"))
        {
            try
            {
                if (Session["tipoPerfil"].ToString() == "N")
                {
                    string[] lsNombreParametros = { "@P_codigo_carta" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int };
                    string[] lValorParametros = { e.Item.Cells[0].Text, "3" };
                    try
                    {
                        lConexion.Abrir();
                        DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetCartaGarantia", lsNombreParametros, lTipoparametros, lValorParametros);
                    }
                    catch (Exception ex)
                    {
                    }
                }
                string lsCarpetaAnt = "";
                string[] lsCarperta;
                lsCarperta = strRutaCarga.Split('\\');
                lsCarpetaAnt = lsCarperta[lsCarperta.Length - 2];
                var lsRuta = "../" + lsCarpetaAnt + "/" + e.Item.Cells[5].Text.Replace(@"\", "/");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + lsRuta + "');", true);
                CargarDatos();
            }
            catch (Exception ex)
            {
                lblMensaje.Append("Error al visualizar el archivo. " + ex.Message.ToString());
            }
        }
    }

}