﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ConsultaCartaGarantias.aspx.cs" Inherits="Facturacion_frm_ConsultaCartaGarantias" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Año Facturación" AssociatedControlID="TxtAno" runat="server" />
                            <asp:TextBox ID="TxtAno" runat="server" Width="100%" CssClass="form-control" ValidationGroup="detalle"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebTxtAno" runat="server" TargetControlID="TxtAno"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Mes Facturación" AssociatedControlID="ddlMes" runat="server" />
                            <asp:DropDownList ID="ddlMes" Width="100%" CssClass="form-control" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
                            <asp:DropDownList ID="ddlOperador" Width="100%" CssClass="form-control" runat="server" />
                        </div>
                    </div>
                </div>

                <div runat="server" visible="false" id="tblGrilla">
                    <div class="table table-responsive">
                        <asp:DataGrid ID="dtgMaestro" AutoGenerateColumns="False" AllowPaging="true" PageSize="20" OnPageIndexChanged="dtgMaestro_PageIndexChanged" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" runat="server" OnItemCommand="dtgMaestro_EditCommand">
                            <Columns>
                                <%--0--%>
                                <asp:BoundColumn DataField="codigo_carta" visible ="false" ></asp:BoundColumn>
                                <%--1--%>
                                <asp:BoundColumn DataField="fecha_corte" HeaderText="Fecha Corte" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--2--%>
                                <asp:BoundColumn DataField="no_documento" HeaderText="Nit Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--3--%>
                                <asp:BoundColumn DataField="razon_social" HeaderText="nombre_participante"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--4--%>
                                <asp:BoundColumn DataField="ind_visualizado" HeaderText="Visualizado Operador"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--5--%>
                                <asp:BoundColumn DataField="ruta_carta" visible ="false" ></asp:BoundColumn>
                                <%--6--%>
                                <asp:BoundColumn DataField="login_visualiza" HeaderText="Usuario Visualiza"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--7--%>
                                <asp:BoundColumn DataField="fecha_hora_visualiza" HeaderText="Fecha-hora Visualiza"
                                    ItemStyle-HorizontalAlign="Left" ></asp:BoundColumn>
                                <%--8--%>
                                <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                    <ItemTemplate>
                                        <div class="dropdown dropdown-inline">
                                            <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="flaticon-more-1"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                <!--begin::Nav-->
                                                <ul class="kt-nav">
                                                    <li class="kt-nav__item">
                                                        <asp:LinkButton ID="lkbVer" CssClass="kt-nav__link" CommandName="Ver" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Ver</span>
                                                                            </asp:LinkButton>
                                                    </li>
                                                </ul>
                                                <!--end::Nav-->
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>


