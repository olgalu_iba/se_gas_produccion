﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_Usuario.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Seguridad_frm_Usuario" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>

                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Buscar--%>
            <div class="kt-portlet__body" runat="server">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Código Usuario</label>
                            <asp:TextBox ID="txtBusCodigoUsuario" runat="server" autocomplete="off" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FTBEtxtBusCodigoUsuario" runat="server" TargetControlID="txtBusCodigoUsuario"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Nombre</label>
                            <asp:TextBox ID="txtBusDescripcion" runat="server" autocomplete="off" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:AutoCompleteExtender runat="server" BehaviorID="AutoCompleteEx" ID="autoCompleteExtenderBusDescripcion"
                                TargetControlID="txtBusDescripcion" ServicePath="~/WebService/AutoComplete.asmx"
                                ServiceMethod="GetCompletionListDescripcionEmpaque" MinimumPrefixLength="2" CompletionInterval="1000"
                                EnableCaching="true" CompletionSetCount="20" CompletionListCssClass="autocomplete_completionListElement"
                                CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                DelimiterCharacters=";,:">
                                <Animations>
                        <OnShow>
                            <Sequence>
                                <%-- Make the completion list transparent and then show it --%>
                                <OpacityAction Opacity="0" />
                                <HideAction Visible="true" />
                                
                                <%--Cache the original size of the completion list the first time
                                    the animation is played and then set it to zero --%>
                                <ScriptAction Script="
                                    // Cache the size and setup the initial size
                                    var behavior = $find('AutoCompleteEx');
                                    if (!behavior._height) {
                                        var target = behavior.get_completionList();
                                        behavior._height = target.offsetHeight - 2;
                                        target.style.height = '0px';
                                    }" />
                                
                                <%-- Expand from 0px to the appropriate size while fading in --%>
                                <Parallel Duration=".4">
                                    <FadeIn />
                                    <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx')._height" />
                                </Parallel>
                            </Sequence>
                        </OnShow>
                        <OnHide>
                            <%-- Collapse down to 0px and fade out --%>
                            <Parallel Duration=".4">
                                <FadeOut />
                                <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx')._height" EndValue="0" />
                            </Parallel>
                        </OnHide>
                                </Animations>
                            </ajaxToolkit:AutoCompleteExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Operador</label>
                            <asp:DropDownList ID="ddlOperador1" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <%--20220211--%>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Usuario Bloqueado por Contraseña</label>
                            <asp:DropDownList ID="ddlBusBloqueado" runat="server" CssClass="form-control selectpicker">
                                <asp:ListItem Value ="">Seleccione</asp:ListItem>
                                <asp:ListItem Value ="I">Si</asp:ListItem>
                                <asp:ListItem Value ="A">No</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <%--20220211--%>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Grupo de Usuario</label>
                            <asp:DropDownList ID="ddlBusGrupoUsuario" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <%--20220211--%>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Estado Usuario</label>
                            <asp:DropDownList ID="ddlBusEstado" runat="server" CssClass="form-control selectpicker">
                                <asp:ListItem Value ="">Seleccione</asp:ListItem>
                                <asp:ListItem Value ="A">Activo</asp:ListItem>
                                <asp:ListItem Value ="I">Inactivo</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>

                <%--Mensaje--%>

                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>

                <%--Grilla--%>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgUsuario" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered" PageSize="30"
                                OnItemCommand="dtgUsuario_EditCommand" OnPageIndexChanged="dtgUsuario_PageIndexChanged">
                                <Columns>
                                    <%--20180126 rq107-16--%>
                                    <asp:BoundColumn DataField="codigo_usuario" HeaderText="Código Usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="login" HeaderText="Login" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_usuario" HeaderText="Nombre" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="300px"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_expiracion" HeaderText="Fecha Expiración" ItemStyle-HorizontalAlign="Left"
                                        DataFormatString="{0: yyyy/MM/dd}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_grupo_usuario" HeaderText="Desc. Grupo Usuario"
                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px"></asp:BoundColumn>
                                    <%--20180126 rq107-16--%>
                                    <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="200px"></asp:BoundColumn>
                                    <%--20190607 rq036-19--%>
                                    <asp:BoundColumn DataField="e_mail" HeaderText="E-Mail" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_grupo_usuario" Visible="false"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more-1"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                    <!--begin::Nav-->
                                                    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <ul class="kt-nav">
                                                                <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                                    <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                                    <span class="kt-nav__link-text">Modificar</span>
                                                                    </asp:LinkButton>
                                                                </li>
                                                                <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                                    <i class="kt-nav__link-icon flaticon-delete"></i>
                                                                    <span class="kt-nav__link-text">Eliminar</span>
                                                                    </asp:LinkButton>
                                                                </li>
                                                            </ul>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>

                                </Columns>
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <%--Modals--%>
    <div class="modal fade" id="registroUsuario" tabindex="-1" role="dialog" aria-labelledby="mdlregistrousuarioLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="registroUsuarioInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlregistroMenuLabel" runat="server">Agregar</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Código Usuario</label>
                                        <asp:TextBox ID="TxtCodigoUsuario" runat="server" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                        <asp:Label ID="LblCodigoUsuario" runat="server" Visible="false" CssClass="form-control"></asp:Label>
                                        <asp:Label ID="lblGrupoAnterior" runat="server" Visible="false" CssClass="form-control"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Login Usuario</label>
                                        <<asp:TextBox ID="TxtLoginUsuario" runat="server" MaxLength="20" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RfvTxtLoginUsuario" runat="server" ErrorMessage="Debe Ingresar El Login del Usuario"
                                            ControlToValidate="TxtLoginUsuario" ValidationGroup="detalle"> * </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RevDescripcion" ControlToValidate="TxtLoginUsuario"
                                            runat="server" ValidationExpression="^([0-9a-zA-Z-_.])*$" ErrorMessage="Valor inválido en el campo Login" ValidationGroup="detalle"> * </asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Grupo Usuario</label>
                                        <asp:DropDownList ID="ddlGrupoUsuario" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlGrupoUsuario_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="hdfTipoPerfil" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Contraseña</label>
                                        <asp:TextBox ID="TxtContrasena" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Confirmación Contraseña</label>
                                        <asp:TextBox ID="TxtConfirma" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                                        <asp:CompareValidator ID="CVConfContraseña" runat="server" ErrorMessage="La Contraseña no Coincide."
                                            Display="Dynamic" ControlToValidate="TxtConfirma" ControlToCompare="TxtContrasena" ValidationGroup="detalle">*
                                        </asp:CompareValidator>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Nombre Usuario</label>
                                        <asp:TextBox ID="TxtNombreUsuario" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RfvTxtNombreUsuario" runat="server" ErrorMessage="Debe Ingresar El Nombre del Usuario"
                                            ControlToValidate="TxtNombreUsuario" ValidationGroup="detalle"> * </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div id="TrCom1" runat="server" class="col-sm-12 col-md-6 col-lg-4" visible="false">
                                    <div class="form-group">
                                        <label>Operador</label>
                                        <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4" id="TrCom3" visible="false" runat="server">
                                    <div class="form-group">
                                        <label>Puede Consultar Toda Información  del Operador</label>
                                        <asp:DropDownList ID="ddlConsInfOPera" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                            <asp:ListItem Value="S">Si</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Email</label><%--20220602--%>
                                        <asp:TextBox ID="TxtEmail" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RevEmail" ControlToValidate="TxtEmail" runat="server"
                                            ValidationExpression="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$" ErrorMessage="Valor inválido en el campo Email" ValidationGroup="detalle"> * </asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="rfvTxtEmail" runat="server" ErrorMessage="Debe Ingresar el mail de contacto"
                                            ControlToValidate="TxtEmail" ValidationGroup="detalle"> * </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <%--20210305 broker--%>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Teléfono</label>
                                        <asp:TextBox ID="TxtTelefono" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvTxtTelefono" runat="server" ErrorMessage="Debe Ingresar el número de teléfono"
                                            ControlToValidate="TxtTelefono" ValidationGroup="detalle"> * </asp:RequiredFieldValidator>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtTelefono" runat="server" TargetControlID="TxtTelefono"
                                            FilterType="Custom, Numbers" ValidChars= "-"></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <%--20210305 broker--%>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Cargo</label>
                                        <asp:TextBox ID="TxtCargo" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvTxtCargo" runat="server" ErrorMessage="Debe Ingresar el cargo"
                                            ControlToValidate="TxtTelefono" ValidationGroup="detalle"> * </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <%--20210305 broker--%>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Habilitado Broker</label>
                                        <asp:DropDownList ID="ddlBroker" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                            <asp:ListItem Value="S">Si</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4" id="TrObs" visible="false" runat="server">
                                    <div class="form-group">
                                        <label>Observación Cambio</label>
                                        <asp:TextBox ID="TxtObservaCambio" runat="server" CssClass="form-control" MaxLength="1000"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Estado</label>
                                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="A">Activo</asp:ListItem>
                                            <asp:ListItem Value="I">Inactivo</asp:ListItem>
                                            <asp:ListItem Value="X">Eliminado</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:ValidationSummary ID="VsActividadEmpaque" runat="server" ValidationGroup="detalle" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="imbCrear" runat="server" CssClass="btn btn-primary" Text="Crear" OnClick="imbCrear_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="imbCrearOtro" runat="server" CssClass="btn btn-primary" Text="Crear e Inserta otro" OnClick="imbCrearOtro_Click" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="imbActualiza" runat="server" class="btn btn-primary" Text="Actualizar" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="imbActualiza_Click1" CausesValidation="false" />
                            <asp:Button ID="imbDesbCont" runat="server" class="btn btn-primary" Text="Desb Clave" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="imbDesbCont_Click1" CausesValidation="false" /> <%--20220211--%>
                            <asp:Button ID="imbSalir" runat="server" class="btn btn-secondary" Text="Salir" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="imbSalir_Click" CausesValidation="false" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
