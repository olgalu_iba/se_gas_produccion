﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using Segas.Web.Elements;

public partial class Seguridad_frm_GrupoUsuario : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Grupo Usuario";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    SqlDataReader lLector;
    string gsTabla = "a_grupo_usuario";

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lConexion = new clConexion(goInfo);
        //Titulo
        Master.Titulo = "Grupo Usuario";
        /// Activacion de los Botones
        buttons.Inicializar(ruta: gsTabla);
        buttons.CrearOnclick += btnNuevo;
        buttons.FiltrarOnclick += btnConsultar_Click;
        buttons.ExportarExcelOnclick += ImgExcel_Click;
        buttons.ExportarPdfOnclick += ImgPdf_Click;

        if (!IsPostBack)
        {
            //Establese los permisos del sistema
            EstablecerPermisosSistema();
            Listar();
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, gsTabla);
        //hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        //hlkListar.Enabled = (Boolean)permisos["SELECT"];
        //hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        foreach (DataGridItem Grilla in dtgGrupoUsuario.Items)
        {
            var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
            lkbModificar.Visible = (bool)permisos["UPDATE"];  
            var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
            lkbEliminar.Visible = (bool)permisos["DELETE"];  
        }
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        TxtDescripcion.Enabled = true;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoGrupoUsuario.Visible = false;
        LblCodigoGrupoUsuario.Visible = true;
        LblCodigoGrupoUsuario.Text = "Automatico";
        ddlTipoPerfil_SelectedIndexChanged(null, null);
        //Cierra el modal de Agregar
        Modal.Abrir(this, registroGrupoUsuario.ID, registroGrupoUsuarioInside.ID);
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        this.dtgGrupoUsuario.CurrentPageIndex = 0; //20210915 visualizacion 
        CargarDatos();
       
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    /////////////////////////////////////////////////////////////////////////////
                    //// Ajuste recuperacion registro pasra no usar LINK Req. 006-17 20170303 ///
                    /////////////////////////////////////////////////////////////////////////////
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "a_grupo_usuario", " codigo_grupo_usuario = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoGrupoUsuario.Text = lLector["codigo_grupo_usuario"].ToString();
                        TxtDescripcion.Text = lLector["descripcion"].ToString();
                        TxtDescripcionAdc.Text = lLector["descripcion_adicional"].ToString(); //20210915 visualizacion
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        TxtDescripcion.Enabled = false;
                        try
                        {
                            ddlTipoPerfil.SelectedValue = lLector["tipo_perfil"].ToString();
                            ddlTipoPerfil_SelectedIndexChanged(null, null);
                        }
                        catch (Exception)
                        {
                            lblMensaje.Text += "El tipo de perfil del registro no existe o esta inactivo<br>";
                        }
                        ddlAdmon.SelectedValue = lLector["administrador_operador"].ToString();
                        ddlSubastador.SelectedValue = lLector["subastador"].ToString();
                        ddlContingencia.SelectedValue = lLector["habilitado_contingencia"].ToString();
                        if (ddlTipoPerfil.SelectedValue == "N")
                        {
                            try
                            {
                                ddlTipoOperador.SelectedValue = lLector["tipo_operador"].ToString();
                            }
                            catch (Exception)
                            {
                                lblMensaje.Text += "El tipo de operador del registro no existe o esta inactivo<br>";
                            }
                        }
                        if (lblMensaje.Text !="") //20210915 visualizacion
                        {
                            Toastr.Warning(this,lblMensaje.Text, "Warning!", 50000);
                            lblMensaje.Text = "";
                        }
                        
                        /////////////////////////////////////////////////////////////////////////////
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoGrupoUsuario.Visible = false;
                        LblCodigoGrupoUsuario.Visible = true;
                    }
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);

                    //Abre el modal de Agregar
                    Modal.Abrir(this, registroGrupoUsuario.ID, registroGrupoUsuarioInside.ID);
                }
                else
                {
                    Toastr.Warning(this, "No se Puede editar el Registro por que esta Bloqueado. Codigo grupoUsuario =  " + modificar.ToString(), "Warning!", 50000);

                }
            }
            catch (Exception ex)
            {
                lConexion.Cerrar();
                Toastr.Warning(this, "Error al modificar.  " + ex.Message, "Warning!", 50000);
            }
        }
        if (lblMensaje.Text == "")
        {
           
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
       
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        
        string[] lsNombreParametros = { "@P_codigo_grupo_usuario", "@P_descripcion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "" };

        try
        {
            if (txtBusCodigoGrupo.Text != "" && txtBusCodigoGrupo.Text != null)
                lValorParametros[0] = txtBusCodigoGrupo.Text.Trim();
            if (txtBusDescripcion.Text.Trim().Length > 0)
                lValorParametros[1] = txtBusDescripcion.Text.Trim();

            lConexion.Abrir();
            dtgGrupoUsuario.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetGrupoUsuario", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgGrupoUsuario.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: dtgActividadEconomica_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgGrupoUsuario_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgGrupoUsuario.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();
    }
    /// <summary>
    /// Nombre: dtgActividadEconomica_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgGrupoUsuario_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigogrupoUsuario = "";
        string[] lsNombreParametros = { "@P_codigo_grupo_usuario", "@P_descripcion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
        string[] lValorParametros = { "", "" };
        lConexion = new clConexion(goInfo);
        lblMensaje.Text = "";
        if (e.CommandName.Equals("Modificar"))
        {
            lCodigogrupoUsuario = this.dtgGrupoUsuario.Items[e.Item.ItemIndex].Cells[0].Text;
            mdlregistroGrupoUsuarioLabel.InnerText = "Modificar";
            Modificar(lCodigogrupoUsuario);
        }
        if (e.CommandName.Equals("Eliminar"))
        {
            lblMensaje.Text = "";
            /// Verificar Si el Registro esta Bloqueado
            if (!manejo_bloqueo("V", this.dtgGrupoUsuario.Items[e.Item.ItemIndex].Cells[0].Text))
            {
                lCodigogrupoUsuario = this.dtgGrupoUsuario.Items[e.Item.ItemIndex].Cells[0].Text;
                try
                {
                    lConexion.Abrir();
                    lValorParametros[0] = lCodigogrupoUsuario;
                    lValorParametros[1] = this.dtgGrupoUsuario.Items[e.Item.ItemIndex].Cells[1].Text;
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_DelGrupoUsuario", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        Toastr.Warning(this, "Se presento un Problema en la Eliminación del Grupo de Usuario.!", "Warning!", 50000);
                        lConexion.Cerrar();
                    }
                    DelegadaBase.Servicios.borrar_a_grupo_usuario(goInfo, Convert.ToInt32(lCodigogrupoUsuario));
                    Toastr.Success(this, "Registro Eliminado Correctamente", "Correcto!", 50000);
                }
                catch (Exception ex)
                {
                    lConexion.Cerrar();
                    lblMensaje.Text = ex.Message;
                }
            }
            else
            {
                Toastr.Warning(this, "No se Puede Eliminar el Registro por que esta Bloqueado. Codigo grupoUsuario =  " + this.dtgGrupoUsuario.Items[e.Item.ItemIndex].Cells[0].Text.ToString(), "Warning!", 50000);
            }
            Listar();
        }

    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lsTabla, string lsCondicion)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lsTabla, lsCondicion, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgActividadEconomica.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgActividadEconomica.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_grupo_usuario)
    {
        string lsCondicion = "nombre_tabla='a_grupo_usuario' and llave_registro='codigo_grupo_usuario=" + lscodigo_grupo_usuario + "'";
        string lsCondicion1 = "codigo_grupo_usuario=" + lscodigo_grupo_usuario.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "a_grupo_usuario";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "a_grupo_usuario", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, EventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_grupo_usuario > 0";
            Server.Transfer("../Informes/exportar_excel.aspx?tipo_export=1&procedimiento=pa_ValidarExistencia&columnas=codigo_grupo_usuario*descripcion*descripcion_adicional*tipo_perfil*administrador_operador*subastador*tipo_operador*habilitado_contingencia*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion + "&nombre_tabla=a_grupo_usuario&titulo_informe=Listado Tabla"); //20210915 visualizacion
        }
        catch (Exception)
        {
            Toastr.Warning(this, "No se pudo generar el reporte", "Warning!", 50000);
        }
    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, EventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_grupo_usuario > 0";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=a_grupo_usuario&procedimiento=pa_ValidarExistencia&columnas=codigo_grupo_usuario*descripcion*tipo_perfil*administrador_operador*subastador*tipo_operador*habilitado_contingencia*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception)
        {
            Toastr.Warning(this, "No se pudo generar el informe", "Warning!", 50000);
        }
    }
    /// <summary>
    /// Metodo Nuevo para habilitar la captura de los campos nuevos Req. 006-17 20170303
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlTipoPerfil_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlTipoPerfil.SelectedValue == "B")
            TrConti.Visible = true;
        else
            TrConti.Visible = false;
        if (ddlTipoPerfil.SelectedValue == "N")
            TrOpera.Visible = true;
        else
            TrOpera.Visible = false;

        lConexion.Abrir();
        ddlTipoOperador.Items.Clear();
        LlenarControles(lConexion.gObjConexion, ddlTipoOperador, "m_tipos_operador", " estado = 'A' order by descripcion", 2, 1);
        lConexion.Cerrar();


    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click1(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        string[] lsNombreParametros = { "@P_codigo_grupo_usuario", // Campo nuevo Req. 006-07 20170303
                                        "@P_descripcion", "@P_estado", "@P_tipo_perfil", "@P_administrador_operador", "@P_subastador",
                                        "@P_habilitado_contingencia","@P_tipo_operador","@P_indica",
                                        "@P_descripcion_adicional" //20210915 visualizacion
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int, // Campo nuevo Req. 006-07 20170303
                                        SqlDbType.VarChar //20210915 visualizacion
                                      };
        string[] lValorParametros = { "0", // Campo nuevo Req. 006-07 20170303
                                      "", "", "", "", "",
                                      ddlContingencia.SelectedValue, ddlTipoOperador.SelectedValue, "1", // Campo nuevo Req. 006-07 20170303
                                      TxtDescripcionAdc.Text ////20210915 visualizacion
                                    };
        try
        {
            lConexion.Abrir();
            /// Llama el metodo que valida si el codigo de la actividad existe en la base de datos.
            if (TxtDescripcion.Text.Contains(' '))
                lblMensaje.Text += "La descripción del grupo de usuario no debe contener espacios en blanco<br>";
            //20210915 visualizacion
            if (TxtDescripcionAdc.Text.Trim() =="")
                lblMensaje.Text += "Debe ingresar la descripción adicional<br>";
            if (VerificarExistencia(gsTabla, "descripcion ='" + TxtDescripcion.Text + "'"))
                lblMensaje.Text += "Ya existe la descripción del grupo de usuario <br>";
            if (ddlTipoPerfil.SelectedValue != "N" && ddlAdmon.SelectedValue == "S")
                lblMensaje.Text += "Solo si pertenece al grupo operador, puede ser administrador de operador<br>";
            if (ddlTipoPerfil.SelectedValue != "B" && ddlSubastador.SelectedValue == "S")
                lblMensaje.Text += "Solo si pertenece al grupo BMC, puede ser subastador<br>";
            ///////////////////////////////////////////////
            /// Nuevas validaciones Req. 006-07 20170303 //
            ///////////////////////////////////////////////
            if (ddlTipoPerfil.SelectedValue != "B" && ddlContingencia.SelectedValue == "S")
                lblMensaje.Text += "Solo si pertenece al grupo BMC, puede ser habilitado para Contingencia<br>";
            if (ddlTipoPerfil.SelectedValue == "N" && ddlTipoOperador.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar el Tipo de Operador para el tipo de Perfil negociador<br>";
            ///////////////////////////////////////////////
            if (lblMensaje.Text == "")
            {
                if (!VerificarExistencia("sys.database_principals", "name ='" + TxtDescripcion.Text + "'"))
                {
                    // Llamo Procedimiento para la Creacion del Grupo de Usuario en la base de Datos y en el Servidor como Rol.
                    lValorParametros[1] = TxtDescripcion.Text;
                    lValorParametros[2] = ddlEstado.SelectedValue.Trim();
                    lValorParametros[3] = ddlTipoPerfil.SelectedValue.Trim();
                    lValorParametros[4] = ddlAdmon.SelectedValue.Trim();
                    lValorParametros[5] = ddlSubastador.SelectedValue.Trim();
                    DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetGrupoUsuario", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        Toastr.Warning(this, "Se presento un Problema en la Creación del Grupo de Usuario.! " + goInfo.mensaje_error, "Warning!", 50000);
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lConexion.Cerrar();
                        Listar();
                        Toastr.Success(this, "Se creo el registro de forma exitosa.!");
                        //Cierra el modal de Agregar
                        Modal.Cerrar(this, registroGrupoUsuario.ID);
                    }
                }
                else
                {
                    Toastr.Warning(this, "YA existe un Rol con ese Nombre en la Base de Datos.", "Warning!", 50000);
                }
            }
            else
            {
                Toastr.Warning(this, lblMensaje.Text, "Warning!", 50000);
                lblMensaje.Text = "";
            }
        }
        catch (Exception ex)
        {
            lConexion.Cerrar();
            Toastr.Warning(this, "Error al crear el registro. " + ex.Message, "Warning!", 50000);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click1(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        string[] lsNombreParametros = { "@P_codigo_grupo_usuario", // Campo nuevo Req. 006-07 20170303
                                        "@P_descripcion", "@P_estado", "@P_tipo_perfil", "@P_administrador_operador", "@P_subastador",
                                        "@P_habilitado_contingencia","@P_tipo_operador","@P_indica",
                                        "@P_descripcion_adicional" //20210915 visualizacion
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int, // Campo nuevo Req. 006-07 20170303
                                        SqlDbType.VarChar //20210915 visualizacion
                                      };
        string[] lValorParametros = { LblCodigoGrupoUsuario.Text, // Campo nuevo Req. 006-07 20170303
                                      "", "", "", "", "",
                                      ddlContingencia.SelectedValue, ddlTipoOperador.SelectedValue, "2", // Campo nuevo Req. 006-07 20170303
                                      TxtDescripcionAdc.Text ////20210915 visualizacion
                                    };
        try
        {
            //20210915 visualizacion
            if (TxtDescripcionAdc.Text.Trim() == "")
                lblMensaje.Text += "Debe ingresar la descripción adicional<br>";
            if (ddlAdmon.SelectedValue == "S" && ddlTipoPerfil.SelectedValue != "N")
                lblMensaje.Text = "Solo si es de tipo operador Negociador puede ser declarado como administrador de operador";
            if (ddlSubastador.SelectedValue == "S" && ddlTipoPerfil.SelectedValue != "B")
                lblMensaje.Text = "Solo si es de tipo operador BMC puede ser declarado como subastador";
            ///////////////////////////////////////////////
            /// Nuevas validaciones Req. 006-07 20170303 //
            ///////////////////////////////////////////////
            if (ddlTipoPerfil.SelectedValue != "B" && ddlContingencia.SelectedValue == "S")
                lblMensaje.Text += "Solo si pertenece al grupo BMC, puede ser habilitado para Contingencia<br>";
            if (ddlTipoPerfil.SelectedValue == "N" && ddlTipoOperador.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar el Tipo de Operador para el tipo de Perfil negociador<br>";
            ///////////////////////////////////////////////
            if (lblMensaje.Text == "")
            {
                lConexion.Abrir();
                // Llamo Procedimiento para la Creacion del Grupo de Usuario en la base de Datos y en el Servidor como Rol.
                lValorParametros[1] = TxtDescripcion.Text;
                lValorParametros[2] = ddlEstado.SelectedValue.Trim();
                lValorParametros[3] = ddlTipoPerfil.SelectedValue.Trim();
                lValorParametros[4] = ddlAdmon.SelectedValue.Trim();
                lValorParametros[5] = ddlSubastador.SelectedValue.Trim();
                DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetGrupoUsuario", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    Toastr.Warning(this, "Se presento un Problema en la Actualización del Grupo de Usuario.! ", "Warning!", 50000);
                    lConexion.Cerrar();
                }
                else
                {
                    lConexion.Cerrar();
                    manejo_bloqueo("E", LblCodigoGrupoUsuario.Text);
                    Listar();
                    Toastr.Success(this, "Se realizo la actualización de forma exitosa.!");
                    //Cierra el modal de Agregar
                    Modal.Cerrar(this, registroGrupoUsuario.ID);
                }
            }
            else
            {
                Toastr.Warning(this,lblMensaje.Text, "Warning!", 50000);
                lblMensaje.Text = "";
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoGrupoUsuario.Text);
            Toastr.Warning(this, "Error al actualziar. " + ex.Message, "Warning!", 50000);
        }
    }
    /// <summary>
    /// ss
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click1(object sender, EventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        manejo_bloqueo("E", LblCodigoGrupoUsuario.Text);
        TxtDescripcion.Text = "";
        TxtDescripcionAdc.Text = ""; //20210915 visualizacion
        Modal.Cerrar(this, registroGrupoUsuario.ID);//20210915 visualizacion
        //Response.Redirect("~/frm_contenido.aspx");
        Listar();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        mdlregistroGrupoUsuarioLabel.InnerText = "Agregar";
        Listar();
    }
    ///// Eventos Nuevos para la Implementracion del UserControl
    /// <summary>
    /// Metodo del Link Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo(object sender, EventArgs e)
    {
        mdlregistroGrupoUsuarioLabel.InnerText = "Agregar";
        Nuevo();
    }
    /// <summary>
    /// Metodo del Link Listar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnListar(object sender, EventArgs e)
    {
        Listar();
    }
    
}