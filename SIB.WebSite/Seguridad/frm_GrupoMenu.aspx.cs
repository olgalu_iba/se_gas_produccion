﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using Segas.Web.Elements;

public partial class Seguridad_frm_GrupoMenu : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Menú";

    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    string gsTabla = "a_grupo_menu";

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../Login.aspx");
        goInfo.Programa = lsTitulo;

        //Establese los permisos del sistemasss
        
        lConexion = new clConexion(goInfo);
        //Titulo
        Master.Titulo = "Menú";
        /// Activacion de los Botones
        buttons.Inicializar(ruta: "a_grupo_menu");
        buttons.CrearOnclick += btnNuevo;
        buttons.FiltrarOnclick += btnConsultar_Click;
        buttons.ExportarExcelOnclick += ImgExcel_Click;
        buttons.ExportarPdfOnclick += ImgPdf_Click;


        //buttons.Inicializar(ruta: "t_energia_a_suminsitrar");
        //buttons.CrearOnclick += btnNuevo;
        //buttons.FiltrarOnclick += btnConsultar_Click;
        //buttons.ExportarExcelOnclick += ImgExcel_Click;
        //buttons.ExportarPdfOnclick += ImgPdf_Click;

        if (!IsPostBack)
        {
            EstablecerPermisosSistema();
            /// Llenar controles
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlGrupoUsuario, "a_grupo_usuario", " estado='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlGrupo1, "a_grupo_usuario", " estado='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlPadre1, "a_menu", " estado='A' order by menu", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMenu, "a_menu", " estado='A' order by menu", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMenu1, "a_menu", " estado='A' order by menu", 0, 1);
            lConexion.Cerrar();
            Listar();
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, gsTabla);
        //hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        //hlkListar.Enabled = (Boolean)permisos["SELECT"];
        //hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        foreach (DataGridItem Grilla in dtgMenu.Items)
        {
            var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
            lkbModificar.Visible = (bool)permisos["UPDATE"];  
            var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
            lkbEliminar.Visible = (bool)permisos["DELETE"];  
        }
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        Modal.Abrir(this, registroGrupoMenu.ID, registroGrupoMenu.ID);
        imbCrear.Visible = true;
        imbCrearOtro.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoGrupoMenu.Visible = false;
        LblCodigoGrupoMenu.Visible = true;
        LblCodigoGrupoMenu.Text = "Automatico";
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        //tblCaptura.Visible = false;
        //tblgrilla.Visible = true;
        //tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    a_grupo_menu MenuBuscar = new a_grupo_menu();
                    MenuBuscar.codigo_grupo_menu = Convert.ToInt32(modificar);
                    a_grupo_menu Menu = DelegadaBase.Servicios.traer_a_grupo_menu(goInfo, MenuBuscar);
                    if (Menu != null)
                    {
                        LblCodigoGrupoMenu.Text = Menu.codigo_grupo_menu.ToString();
                        ddlGrupoUsuario.SelectedValue = Menu.codigo_grupo_usuario.ToString();
                        carga_menu();
                        carga_menu_padre();
                        try
                        {
                            ddlPadre.SelectedValue = Menu.id_padre.ToString();
                        }
                        catch (Exception)
                        {
                            lblMensaje.Text += "El menú padre del registro no existe o esta inactivo<br>";
                        }
                        try
                        {
                            ddlMenu.SelectedValue = Menu.codigo_menu.ToString();
                        }
                        catch (Exception)
                        {
                            lblMensaje.Text += "El menú del registro no existe o esta inactivo<br>";
                        }
                        if (lblMensaje.Text != "")
                        {
                            Toastr.Warning(this, lblMensaje.Text, "Warning!", 50000);
                            lblMensaje.Text = "";
                        }
                        TxtOrden.Text = Menu.orden.ToString();
                        ddlConsulta.SelectedValue = Menu.consultar.ToString();
                        ddlCrea.SelectedValue = Menu.crear.ToString();
                        ddlModifica.SelectedValue = Menu.modificar.ToString();
                        ddlElimina.SelectedValue = Menu.eliminar.ToString();
                        imbCrear.Visible = false;
                        imbCrearOtro.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoGrupoMenu.Visible = false;
                        LblCodigoGrupoMenu.Visible = true;
                    }
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                    //Abre el modal de Agregar
                    Modal.Abrir(this, registroGrupoMenu.ID, registroGrupoMenu.ID);
                }
                else
                {
                    Listar();
                    Toastr.Warning(this, "No se Puede editar el Registro por que esta Bloqueado. Codigo Grupo Menu =  ", "Warning!", 50000);

                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this,"Error al modificar. "+ ex.Message, "Warning!", 50000);
            }
        }
        if (lblMensaje.Text == "")
        {
            //tblCaptura.Visible = true;
            //tblgrilla.Visible = false;
            //tblBuscar.Visible = false;
            //Abre el modal de Agregar
            Modal.Abrir(this, registroGrupoMenu.ID, registroGrupoMenu.ID);
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    //private void Buscar()
    //{
    //    //tblCaptura.Visible = false;
    //    //tblgrilla.Visible = false;
    //    //tblBuscar.Visible = true;
    //    lblTitulo.Text = "Consultar " + lsTitulo;
    //}
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            /////////////////////////////////////////////////////////////////////////////////////
            /// Para Llenar una Grilla Con la Clase clProcesosSql
            ////////////////////////////////////////////////////////////////////////////////////
            string[] lsNombreParametros = { "@P_grupo_usuario", "@P_menu_padre", "@P_menu" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { ddlGrupo1.SelectedValue, ddlPadre1.SelectedValue, ddlMenu1.SelectedValue };
            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            dtgMenu.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetGrupoMenu", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMenu.DataBind();
            lConexion.Cerrar();
            /////////////////////////////////////////////////////////////////////////////////////
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, ex.Message);
        }
        if (lblMensaje.Text == "") return;
        Toastr.Warning(this, lblMensaje.Text);
        lblMensaje.Text = "";
    }
    /// <summary>
    /// Nombre: dtgActividadEconomica_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMenu_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMenu.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgActividadEconomica_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMenu_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoGrupoMenu = "";
        lblMensaje.Text = "";
        if (e.CommandName.Equals("Modificar"))
        {
            mdlregistroGrupoMenuLabel.InnerText = "Modificar";
            lCodigoGrupoMenu = this.dtgMenu.Items[e.Item.ItemIndex].Cells[0].Text;
            Modificar(lCodigoGrupoMenu);
        }
        if (e.CommandName.Equals("Eliminar"))
        {
            lblMensaje.Text = "";
            /// Verificar Si el Registro esta Bloqueado
            if (!manejo_bloqueo("V", this.dtgMenu.Items[e.Item.ItemIndex].Cells[0].Text))
            {
                lCodigoGrupoMenu = this.dtgMenu.Items[e.Item.ItemIndex].Cells[0].Text;
                try
                {
                    DelegadaBase.Servicios.borrar_a_grupo_menu(goInfo, Convert.ToInt32(lCodigoGrupoMenu));
                    Toastr.Success(this, "Registro Eliminado Correctamente", "Correcto!", 50000);
                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, "Error al Eliminar. " + ex.Message, "Warning!", 50000);
                }
            }
            else
            {
                Toastr.Warning(this, "No se Puede Eliminar el Registro por que esta Bloqueado. Codigo Grupo Menu =  " + this.dtgMenu.Items[e.Item.ItemIndex].Cells[0].Text.ToString(), "Warning!", 50000);
            }
            Listar();
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lsTabla, string lsCondicion)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lsTabla, lsCondicion, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceLlave).ToString() + " - " + lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_grupo_menu)
    {
        string lsCondicion = "nombre_tabla='a_grupo_menu' and llave_registro='codigo_grupo_menu=" + lscodigo_grupo_menu + "'";
        string lsCondicion1 = "codigo_grupo_menu=" + lscodigo_grupo_menu.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "a_grupo_menu";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "a_grupo_menu", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, EventArgs e)
    {
        try
        {
            Server.Transfer("../Informes/exportar_excel.aspx?tipo_export=2&procedimiento=pa_GetGrupoMenu&nombreParametros=@P_grupo_usuario*@P_menu_padre*@P_menu&valorParametros=" + ddlGrupo1.SelectedValue + "*" + ddlPadre1.SelectedValue + "*" + ddlMenu1.SelectedValue + "&columnas=codigo_grupo_menu*codigo_grupo_usuario*desc_grupo_usuario*id_padre*desc_menu_padre*codigo_menu*desc_menu*desc_consulta*desc_crea*desc_modifica*desc_elimina&titulo_informe=Consulta de menús por grupo de usuario");
        }
        catch (Exception)
        {
            Toastr.Warning(this, "No se Pudo Generar el Informe.!", "Warning!", 50000);
        }
    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, EventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_grupo_menu > 0";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=a_grupo_menu&procedimiento=pa_ValidarExistencia&columnas=codigo_grupo_menu*id_padre*codigo_grupo_usuario*codigo_menu*orden*consultar*crear*modificar*eliminar*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);

        }
        catch (Exception)
        {
            Toastr.Warning(this, "No se Pudo Generar el Informe.!", "Warning!", 50000);
        }
    }

    /// <summary>
    /// Nombre: carga_menu
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Muestra el menú que se crea
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void carga_menu()
    {
        //for (int x = 0; x < tvMenu.Nodes.Count; x++)
        //    tvMenu.Nodes.RemoveAt(x);
        //tvMenu.Dispose();
        tvMenu.Nodes.Clear();
        if (ddlGrupoUsuario.SelectedValue != "0")
        {
            var lista = DelegadaBase.Servicios.consultarMenuGrupo(goInfo, 0, Convert.ToInt32(ddlGrupoUsuario.SelectedValue));
            foreach (Hashtable opcion in lista)
            {
                TreeNode nodoPrincipal = new TreeNode();
                nodoPrincipal.SelectAction = TreeNodeSelectAction.None;
                nodoPrincipal.Text = opcion["menu"].ToString();
                nodoPrincipal.Value = opcion["codigo_grupo_menu"].ToString();
                //if (opcion["ruta"] != null)
                //    nodoPrincipal.NavigateUrl = opcion["ruta"].ToString();
                //nodoPrincipal.Target = opcion["frame"].ToString();
                tvMenu.Nodes.Add(nodoPrincipal);
                cargaSubmenus(nodoPrincipal, Convert.ToInt32(opcion["codigo_menu"]));
            }
            tvMenu.ExpandAll();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="NodoPadre"></param>
    /// <param name="liPadre"></param>
    void cargaSubmenus(TreeNode NodoPadre, int liPadre)
    {
        var lista = DelegadaBase.Servicios.consultarMenuGrupo(goInfo, liPadre, Convert.ToInt32(ddlGrupoUsuario.SelectedValue));

        TreeNode UltimoNodo = null;

        foreach (Hashtable opcion in lista)
        {

            TreeNode nodo = new TreeNode();
            nodo.Text = opcion["menu"].ToString();
            nodo.Value = opcion["codigo_grupo_menu"].ToString();
            //if (opcion["ruta"] != null)
            //    nodo.NavigateUrl = opcion["ruta"].ToString();
            //if (opcion["frame"] != null)
            //    nodo.Target = opcion["frame"].ToString();

            if (!tvMenu.Nodes.Contains(nodo))
            {
                NodoPadre.ChildNodes.Add(nodo);
                UltimoNodo = nodo;
            }

            if (opcion["ruta"].ToString().Trim().Length == 0)
            {
                UltimoNodo.SelectAction = TreeNodeSelectAction.None;
                cargaSubmenus(UltimoNodo, Convert.ToInt32(opcion["codigo_menu"]));
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlGrupoUsuario_SelectedIndexChanged(object sender, EventArgs e)
    {
        carga_menu();
        carga_menu_padre();
    }
    /// <summary>
    /// 
    /// </summary>
    protected void carga_menu_padre()
    {
        ddlPadre.Items.Clear();
        lConexion = new clConexion(goInfo);
        lConexion.Abrir();
        LlenarControles(lConexion.gObjConexion, ddlPadre, "a_menu mnu, a_grupo_menu grp", " mnu.codigo_menu = grp.codigo_menu and grp.codigo_grupo_usuario=" + ddlGrupoUsuario.SelectedValue + " and estado='A' order by mnu.menu", 0, 1);
        lConexion.Cerrar();
    }
    ///// Eventos Nuevos para la Implementracion del UserControl
    /// <summary>
    /// Metodo del Link Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo(object sender, EventArgs e)
    {
        mdlregistroGrupoMenuLabel.InnerText = "Agregar";
        Nuevo();
    }
    /// <summary>
    /// Metodo del Link Listar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Metodo del Link Consultar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void btnConsulta(object sender, EventArgs e)
    //{
    //    Buscar();
    //}
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrearOtro_Click(object sender, EventArgs e)
    {
        try
        {
            lblMensaje.Text = "";
            /// Llama el metodo que valida si el codigo de la actividad existe en la base de datos.
            if (ddlPadre.SelectedValue == ddlMenu.SelectedValue)
                lblMensaje.Text = "El Menú debe ser diferente al menú padre";
            if (VerificarExistencia(gsTabla, "codigo_grupo_usuario =" + ddlGrupoUsuario.SelectedValue + " and codigo_menu=" + ddlMenu.SelectedValue))
                lblMensaje.Text = "El Menú seleccionado ya se encuentra asignado al grupo de usuario";
            if (lblMensaje.Text == "")
            {
                a_grupo_menu Menu = new a_grupo_menu();
                Menu.codigo_grupo_menu = 0;
                Menu.id_padre = Convert.ToInt32(ddlPadre.SelectedValue);
                Menu.codigo_grupo_usuario = Convert.ToInt32(ddlGrupoUsuario.SelectedValue);
                Menu.codigo_menu = Convert.ToInt32(ddlMenu.SelectedValue);
                Menu.orden = Convert.ToInt32(TxtOrden.Text);
                Menu.consultar = Convert.ToInt32(ddlConsulta.SelectedValue);
                Menu.crear = Convert.ToInt32(ddlCrea.SelectedValue);
                Menu.modificar = Convert.ToInt32(ddlModifica.SelectedValue);
                Menu.eliminar = Convert.ToInt32(ddlElimina.SelectedValue);
                DelegadaBase.Servicios.guardar_a_grupo_menu(goInfo, Menu);
                lblMensaje.Text = "";
                /// Ajuste para llamar el procedimiento de asignacion masiva de menus de visualizacion de archivos 20160707
                if (VerificarExistencia("m_parametros_generales", "codigo_menu_visual_archivos =" + ddlPadre.SelectedValue + " "))  //20181115 ajuste
                {
                    string[] lsNombreParametros = { "@P_codigo_grupo_usuario", "@P_codigo_menu_padre" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                    string[] lValorParametros = { ddlGrupoUsuario.SelectedValue, ddlMenu.SelectedValue };
                    lConexion.Abrir();
                    DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetAsignaMenuArchivos", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        Toastr.Warning(this, "Se presento un Problema en la Asignación de los Menus de Archivos de Informacion.! " + goInfo.mensaje_error.ToString(), "Warning!", 50000);
                    }
                    lConexion.Cerrar();
                }
                //Cierra el modal de Agregar
                Modal.Cerrar(this, registroGrupoMenu.ID);
                carga_menu();
                carga_menu_padre();
            }
            else
            {
                Toastr.Warning(this, lblMensaje.Text, "Warning!", 50000);
                lblMensaje.Text = "";
            }

        }
        catch (Exception ex)
        {
            Toastr.Warning(this,"Error al crear el registro. "+ ex.Message, "Warning!", 50000);

        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click1(object sender, EventArgs e)
    {
        try
        {
            lblMensaje.Text = "";
            /// Llama el metodo que valida si el codigo de la actividad existe en la base de datos.
            if (ddlPadre.SelectedValue == ddlMenu.SelectedValue)
                lblMensaje.Text = "El Menú debe ser diferente al menú padre";
            if (VerificarExistencia(gsTabla, "codigo_grupo_usuario =" + ddlGrupoUsuario.SelectedValue + " and codigo_menu=" + ddlMenu.SelectedValue))
                lblMensaje.Text = "El Menú seleccionado ya se encuentra asignado al grupo de usuario";
            if (lblMensaje.Text == "")
            {
                a_grupo_menu Menu = new a_grupo_menu();
                Menu.codigo_grupo_menu = 0;
                Menu.id_padre = Convert.ToInt32(ddlPadre.SelectedValue);
                Menu.codigo_grupo_usuario = Convert.ToInt32(ddlGrupoUsuario.SelectedValue);
                Menu.codigo_menu = Convert.ToInt32(ddlMenu.SelectedValue);
                Menu.orden = Convert.ToInt32(TxtOrden.Text);
                Menu.consultar = Convert.ToInt32(ddlConsulta.SelectedValue);
                Menu.crear = Convert.ToInt32(ddlCrea.SelectedValue);
                Menu.modificar = Convert.ToInt32(ddlModifica.SelectedValue);
                Menu.eliminar = Convert.ToInt32(ddlElimina.SelectedValue);
                DelegadaBase.Servicios.guardar_a_grupo_menu(goInfo, Menu);
                /// Ajuste para llamar el procedimiento de asignacion masiva de menus de visualizacion de archivos 20160707
                if (VerificarExistencia("m_parametros_generales", "codigo_menu_visual_archivos =" + ddlPadre.SelectedValue + " ")) //201811145 ajuste
                {
                    string[] lsNombreParametros = { "@P_codigo_grupo_usuario", "@P_codigo_menu_padre" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                    string[] lValorParametros = { ddlGrupoUsuario.SelectedValue, ddlMenu.SelectedValue };
                    lConexion.Abrir();
                    DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetAsignaMenuArchivos", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        Toastr.Warning(this, "Se presento un Problema en la Asignación de los Menus de Archivos de Informacion.! " + goInfo.mensaje_error.ToString(), "Warning!", 50000);
                    }
                    lConexion.Cerrar();
                }
                ///
                lblMensaje.Text = "";
                //Cierra el modal de Agregar
                Modal.Cerrar(this, registroGrupoMenu.ID);
                Listar();
            }
            else
            {
                Toastr.Warning(this, lblMensaje.Text, "Warning!", 50000);
                lblMensaje.Text = "";
            }
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, ex.Message, "Warning!", 50000);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click1(object sender, EventArgs e)
    {
        try
        {
            lblMensaje.Text = "";
            /// Llama el metodo que valida si el codigo de la actividad existe en la base de datos.
            if (ddlPadre.SelectedValue == ddlMenu.SelectedValue)
                lblMensaje.Text += "El Menú debe ser diferente al menú padre";
            if (VerificarExistencia(gsTabla, "codigo_grupo_menu <> " + LblCodigoGrupoMenu.Text + " and codigo_grupo_usuario =" + ddlGrupoUsuario.SelectedValue + " and codigo_menu=" + ddlMenu.SelectedValue))
                lblMensaje.Text += "El Menú seleccionado ya se encuentra asignado al grupo de usuario";
            if (lblMensaje.Text == "")
            {
                a_grupo_menu Menu = new a_grupo_menu();
                Menu.codigo_grupo_menu = Convert.ToInt32(LblCodigoGrupoMenu.Text);
                Menu.id_padre = Convert.ToInt32(ddlPadre.SelectedValue);
                Menu.codigo_grupo_usuario = Convert.ToInt32(ddlGrupoUsuario.SelectedValue);
                Menu.codigo_menu = Convert.ToInt32(ddlMenu.SelectedValue);
                Menu.orden = Convert.ToInt32(TxtOrden.Text);
                Menu.consultar = Convert.ToInt32(ddlConsulta.SelectedValue);
                Menu.crear = Convert.ToInt32(ddlCrea.SelectedValue);
                Menu.modificar = Convert.ToInt32(ddlModifica.SelectedValue);
                Menu.eliminar = Convert.ToInt32(ddlElimina.SelectedValue);
                DelegadaBase.Servicios.modificar_a_grupo_menu(goInfo, Menu);

                //20181115 ajuste
                if (VerificarExistencia("m_parametros_generales", "codigo_menu_visual_archivos =" + ddlPadre.SelectedValue + " "))
                {
                    string[] lsNombreParametros = { "@P_codigo_grupo_usuario", "@P_codigo_menu_padre" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                    string[] lValorParametros = { ddlGrupoUsuario.SelectedValue, ddlMenu.SelectedValue };
                    lConexion.Abrir();
                    DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetAsignaMenuArchivos", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        Toastr.Warning(this, "Se presento un Problema en la Asignación de los Menus de Archivos de Informacion.! " + goInfo.mensaje_error.ToString(), "Warning!", 50000);
                    }
                    lConexion.Cerrar();
                }

                /// Desbloquea el Registro Actualizado
                manejo_bloqueo("E", LblCodigoGrupoMenu.Text);
                //Cierra el modal de Agregar
                Modal.Cerrar(this, registroGrupoMenu.ID);
                Listar();
                Toastr.Success(this, "Registro Modicicado Correctamente", "Correcto!", 50000);
            }
            else
            {
                Toastr.Warning(this, lblMensaje.Text, "Warning!", 50000);
                lblMensaje.Text = "";
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoGrupoMenu.Text);
            Toastr.Warning(this, ex.Message, "Warning!", 50000);
            
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click1(object sender, EventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        manejo_bloqueo("E", LblCodigoGrupoMenu.Text);
        //Cierra el modal de Agregar
        Modal.Cerrar(this, registroGrupoMenu.ID);
        //Response.Redirect("~/frm_contenido.aspx");
        Listar();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void btnConsulta_Click(object sender, EventArgs e)
    //{
    //    Buscar();
    //}
}