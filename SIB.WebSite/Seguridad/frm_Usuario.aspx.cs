﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using Segas.Web.Elements;

public partial class Seguridad_frm_Usuario : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Usuario";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null; //20211110 conexion replica
    SqlDataReader lLector;
    string gsTabla = "a_usuario";

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;

        lConexion = new clConexion(goInfo);
        //Titulo
        Master.Titulo = "Usuario";
        /// Activacion de los Botones
        buttons.Inicializar(ruta: gsTabla);
        buttons.CrearOnclick += btnNuevo;
        buttons.CrearOnclick += btnNuevo;
        buttons.FiltrarOnclick += btnConsultar_Click;
        buttons.ExportarExcelOnclick += ImgExcel_Click;
        buttons.ExportarPdfOnclick += ImgPdf_Click;

        if (!IsPostBack)
        {
            //Establese los permisos del sistema
            EstablecerPermisosSistema();
            /// Llenar controles
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlGrupoUsuario, "a_grupo_usuario", " codigo_grupo_usuario <> '0' order by descripcion", 0, 1);
            LlenarControles1(lConexion.gObjConexion, ddlOperador1, "m_operador", " codigo_operador <> '0'  and estado='A' and codigo_operador !=0 order by razon_social", 0, 4);  //20180122 rq003-18
            LlenarControles(lConexion.gObjConexion, ddlBusGrupoUsuario, "a_grupo_usuario", " codigo_grupo_usuario <> '0' order by descripcion", 0, 1); //20220211
            lConexion.Cerrar();
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            Listar();
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, gsTabla);

        foreach (DataGridItem Grilla in dtgUsuario.Items)
        {
            var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
            lkbModificar.Visible = (bool)permisos["UPDATE"];
            var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
            lkbEliminar.Visible = (bool)permisos["DELETE"];
        }
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        limpiarCampos();
        Modal.Abrir(this, registroUsuario.ID, registroUsuario.ID);
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        imbDesbCont.Visible = false; //20220211 contraseña
        TxtLoginUsuario.Enabled = true;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoUsuario.Visible = false;
        LblCodigoUsuario.Visible = true;
        LblCodigoUsuario.Text = "Automatico";
        imbCrearOtro.Visible = true;
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", gsTabla, " codigo_usuario = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoUsuario.Text = lLector["codigo_usuario"].ToString();
                        TxtLoginUsuario.Text = lLector["login"].ToString();
                        TxtContrasena.Text = lLector["clave"].ToString();
                        TxtNombreUsuario.Text = lLector["nombre"].ToString();
                        //TxtFechaExpiracion.Text = lLector["fecha_expiracion"].ToString();
                        try
                        {
                            ddlGrupoUsuario.SelectedValue = lLector["codigo_grupo_usuario"].ToString();
                            ddlGrupoUsuario_SelectedIndexChanged(null, null);
                        }
                        catch (Exception)
                        {
                            lblMensaje.Text += "El grupo de usuario del registro no existe o está inactivo<br>"; //20180126 rq107 - 16
                        }
                        try
                        {
                            ddlOperador.SelectedValue = lLector["codigo_operador"].ToString();
                        }
                        catch (Exception)
                        {
                            lblMensaje.Text += "El operador del registro no existe o está inactivo<br>"; //20180126 rq107 - 16
                        }
                        //20210305 broker
                        TxtTelefono.Text = lLector["telefono"].ToString();
                        TxtCargo.Text = lLector["cargo"].ToString();
                        try
                        {
                            ddlBroker.SelectedValue = lLector["habilitado_broker"].ToString();
                        }
                        catch (Exception)
                        {
                            ddlBroker.SelectedValue = "N";
                        }
                        //20210305 fin broker
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        lblGrupoAnterior.Text = ddlGrupoUsuario.SelectedItem.ToString();
                        ddlConsInfOPera.SelectedValue = lLector["cons_toda_inf_operador"].ToString();
                        /// Campo Nuevo de Email Requerimiento de Restauracoin de Claves 20160707
                        TxtEmail.Text = lLector["e_mail"].ToString();
                        ///
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoUsuario.Visible = false;
                        LblCodigoUsuario.Visible = true;
                        //20220211 contraseña
                        if (lLector["estado_contr_fall"].ToString()=="I")
                            imbDesbCont.Visible = true; 
                        else
                            imbDesbCont.Visible = false;

                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                    Modal.Abrir(this, registroUsuario.ID, registroUsuario.ID);
                }
                else
                {
                    Listar();
                    Toastr.Warning(this, "No se Puede editar el Registro por que está Bloqueado. Código Usuario =  " + modificar.ToString(), "Warning!", 50000);

                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, ex.Message, "Warning!", 50000);
            }
        }
        if (lblMensaje.Text == "")
        {
            lblTitulo.Text = "Modificar " + lsTitulo;
            TxtLoginUsuario.Enabled = false;
        }
    }

    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_usuario", "@P_login_usuario", "@P_nombre", "@P_codigo_operador", "@P_estado_contr_fall", "@P_grupo_usuario", "@P_estado" };  //20180122 rq003-18 //20220211
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar }; //20180122 rq003-18 //20220211
        string[] lValorParametros = { "0", "", "", ddlOperador1.SelectedValue, ddlBusBloqueado.SelectedValue, ddlBusGrupoUsuario.SelectedValue, ddlBusEstado.SelectedValue };//20180122 rq003-18 //20220211

        try
        {
            if (txtBusCodigoUsuario.Text != "" && txtBusCodigoUsuario.Text != null)
                lValorParametros[0] = txtBusCodigoUsuario.Text;
            if (txtBusDescripcion.Text.Trim().Length > 0)
                lValorParametros[2] = txtBusDescripcion.Text.Trim();

            lConexion.Abrir();
            dtgUsuario.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetUsuario", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgUsuario.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, ex.Message, "Warning!", 50000);
        }
    }
    /// <summary>
    /// Nombre: dtgActividadEconomica_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgUsuario_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgUsuario.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgActividadEconomica_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgUsuario_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoUsuario = "";
        lblMensaje.Text = "";
        if (e.CommandName.Equals("Modificar"))
        {
            //mdlregistroUsuarioLabel.InnerText = "Modificar";
            lCodigoUsuario = this.dtgUsuario.Items[e.Item.ItemIndex].Cells[0].Text;
            imbCrearOtro.Visible = false;
            TrObs.Visible = true;
            TxtObservaCambio.Text = "";
            Modificar(lCodigoUsuario);
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lsTabla, string lsCondicion)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lsTabla, lsCondicion, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgActividadEconomica.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgActividadEconomica.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            if (lsTabla != "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    /// 20190607 rq036-19
    protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgActividadEconomica.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgActividadEconomica.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();

            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }

    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_grupo_usuario)
    {
        string lsCondicion = "nombre_tabla='a_usuario' and llave_registro='codigo_grupo_usuario=" + lscodigo_grupo_usuario + "'";
        string lsCondicion1 = "codigo_grupo_usuario=" + lscodigo_grupo_usuario.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "a_usuario";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "a_usuario", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, EventArgs e)
    {
        string[] lValorParametros = { "0", "", "", "0" };  //20180122 rq003-18
        string lsParametros = "";

        try
        {
            if (txtBusCodigoUsuario.Text != "" && txtBusCodigoUsuario.Text != null)
            {
                lValorParametros[0] = txtBusCodigoUsuario.Text;
                lsParametros += " Código Usuario: " + txtBusCodigoUsuario.Text; //20180126 rq107 - 16
            }
            if (txtBusDescripcion.Text.Trim().Length > 0)
            {
                lValorParametros[2] = txtBusDescripcion.Text.Trim();
                lsParametros += " - Nombre Usuario: " + txtBusDescripcion.Text;
            }
            //20180122 rq003-18
            if (ddlOperador1.SelectedValue != "0")
            {
                lValorParametros[3] = ddlOperador1.SelectedValue;
                lsParametros += " - Operador: " + ddlOperador1.SelectedItem.ToString();
            }

            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetUsuario&nombreParametros=@P_codigo_usuario*@P_login_usuario*@P_nombre*@P_codigo_operador&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "*" + lValorParametros[3] + "&columnas=Producto*valor_negocio*dias_plazo*valor_tasa_venta*valor_tasa_compra*valor_tasa_cierre&titulo_informe=Listado de Usuarios&TituloParametros=" + lsParametros); //20180122 rq003-18
        }
        catch (Exception)
        {
            Toastr.Warning(this, "No se Pudo Generar el Informe.!", "Warning!", 50000);
        }
    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, EventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_usuario > 0";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=a_usuario&procedimiento=pa_ValidarExistencia&columnas=codigo_usuario*login*nombre*codigo_grupo_usuario*estado*e_mail*login_usuario*fecha_hora_actual&condicion=" + lsCondicion); //20190607 rq036-19
        }
        catch (Exception)
        {
            Toastr.Warning(this, "No se Pudo Generar el Informe.!", "Warning!", 50000);
        }
    }

    /// <summary>
    /// Nombre: ddlGrupoUsuario_SelectedIndexChanged
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Habilita los campos de Comisionista, si el grupo es 4
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlGrupoUsuario_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlOperador.Items.Clear();
        if (VerificarExistencia("a_grupo_usuario", " codigo_grupo_usuario = " + ddlGrupoUsuario.SelectedValue + " And tipo_perfil = 'N' "))
        {
            lConexion.Abrir();
            TrCom1.Visible = true;
            TrCom3.Visible = true;
            hdfTipoPerfil.Value = "N";
            LlenarControles1(lConexion.gObjConexion, ddlOperador, "m_operador ope, a_grupo_usuario grp", " ope.codigo_operador <> '0'  and ope.estado='A' and ope.codigo_operador !=0 and grp.codigo_grupo_usuario =" + ddlGrupoUsuario.SelectedValue + "  and (ope.tipo_operador = grp.tipo_operador or grp.tipo_operador='0') order by ope.razon_social", 0, 4); //20190607 rq036-19
            lConexion.Cerrar();
        }
        else
        {
            hdfTipoPerfil.Value = "B";
            TrCom1.Visible = false;
            TrCom3.Visible = false;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click1(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_login", "@P_clave", "@P_nombre", "@P_codigo_grupo_usuario", "@P_fecha_expiracion",
                                        "@P_codigo_operador", "@P_estado", "@P_nombre_grupo", "@P_cons_inf_operador",
                                        "@P_e_mail", // Campo nuevo req Restauracion de Claves 2160707
                                        "@P_telefono", "@P_cargo", "@P_habilitado_broker" //20210305 broker
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar,
                                        SqlDbType.Int, SqlDbType.VarChar , SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar, // Campo nuevo req Restauracion de Claves 2160707
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar//20210305 broker
                                        };
        string[] lValorParametros = { "", "", "", "", "", "0", "", "", "N",
                                      TxtEmail.Text, // Campo nuevo req Restauracion de Claves 2160707
                                      TxtTelefono.Text ,TxtCargo.Text ,ddlBroker.SelectedValue //20210305 broker
                                    };
        lblMensaje.Text = "";
        string lsContrasena = "";
        try
        {
            lConexion.Abrir();
            /// Llama el metodo que valida si el codigo de la actividad existe en la base de datos.
            if (!VerificarExistencia(gsTabla, "login ='" + TxtLoginUsuario.Text + "'"))
            {
                if (!VerificarExistencia("sys.database_principals", "name ='" + TxtLoginUsuario.Text + "'"))
                {
                    if (ddlGrupoUsuario.SelectedValue == "0")
                        lblMensaje.Text = "Debe Seleccionar El Grupo de Usuario. <br>";
                    if (hdfTipoPerfil.Value == "N")
                    {
                        if (ddlOperador.SelectedValue == "0")
                            lblMensaje.Text = "Debe Seleccionar El Operador. <br>";
                    }
                    if (TxtContrasena.Text.Trim().Length <= 0)
                    {
                        lblMensaje.Text = "Debe Ingresar la Contraseña. <br>";
                    }
                    //20210305 broker
                    if (ddlBroker.SelectedValue == "S")
                    {
                        if (TxtTelefono.Text == "")
                            lblMensaje.Text += "Debe Ingresar el número del teléfono. <br>";
                        if (TxtCargo.Text == "")
                            lblMensaje.Text += "Debe Ingresar el cargo. <br>";
                    }

                    ///

                    if (lblMensaje.Text == "")
                    {
                        // Se encrypta la contraseña
                        lsContrasena = DelegadaBase.Servicios.CifrarCadena(TxtContrasena.Text);
                        //lsContrasena = TxtContrasena.Text;
                        // Llamo Procedimiento para la Creacion del  Usuario en la base de Datos y en el Servidor.
                        lValorParametros[0] = TxtLoginUsuario.Text;
                        lValorParametros[1] = lsContrasena;
                        lValorParametros[2] = TxtNombreUsuario.Text;
                        lValorParametros[3] = ddlGrupoUsuario.SelectedValue;
                        lValorParametros[4] = "";
                        if (hdfTipoPerfil.Value == "N")
                        {
                            lValorParametros[5] = ddlOperador.SelectedValue;
                            lValorParametros[8] = ddlConsInfOPera.SelectedValue;
                        }
                        lValorParametros[6] = ddlEstado.SelectedValue;
                        lValorParametros[7] = ddlGrupoUsuario.SelectedItem.ToString();
                        if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetUsuario", lsNombreParametros, lTipoparametros, lValorParametros))
                        {
                            Toastr.Warning(this, "Se presentó un Problema en la Creación del  Usuario.!", "Warning!", 50000);
                            lConexion.Cerrar();
                        }
                        else
                        {
                            //20211110 conexion replica
                            try
                            {
                                lConexion1.AbrirInforme();
                                DelegadaBase.Servicios.EjecutarProcedimiento(lConexion1.gObjConexion, "pa_SetUsuario", lsNombreParametros, lTipoparametros, lValorParametros);
                                lConexion1.CerrarInforme();
                            }
                            catch (Exception ex) 
                            { }
                            limpiarCampos();
                            Modal.Cerrar(this, registroUsuario.ID);
                            Listar();
                        }
                    }
                }
                else
                {
                    Toastr.Warning(this, "YA existe un Usuario con ese Nombre en la Base de Datos.", "Warning!", 50000);
                }
            }
            else
            {
                Toastr.Warning(this, "El Login de Usuario YA existe", "Warning!", 50000);
            }
        }
        catch (Exception ex)
        {
            lConexion.Cerrar();
            Toastr.Warning(this, ex.Message, "Warning!", 50000);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrearOtro_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_login", "@P_clave", "@P_nombre", "@P_codigo_grupo_usuario", "@P_fecha_expiracion",
                                        "@P_codigo_operador", "@P_estado", "@P_nombre_grupo", "@P_cons_inf_operador",
                                        "@P_e_mail" ,// Campo nuevo req Restauracion de Claves 2160707
                                        "@P_telefono", "@P_cargo", "@P_habilitado_broker" //20210305 broker
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar,
                                        SqlDbType.Int, SqlDbType.VarChar , SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar, // Campo nuevo req Restauracion de Claves 2160707
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar//20210305 broker
                                        };
        string[] lValorParametros = { "", "", "", "", "", "0", "", "", "N",
                                      TxtEmail.Text, // Campo nuevo req Restauracion de Claves 2160707
                                      TxtTelefono.Text ,TxtCargo.Text ,ddlBroker.SelectedValue //20210305 broker
                                    };
        lblMensaje.Text = "";
        string lsContrasena = "";
        try
        {
            lConexion.Abrir();
            /// Llama el metodo que valida si el codigo de la actividad existe en la base de datos.
            if (!VerificarExistencia(gsTabla, "login ='" + TxtLoginUsuario.Text + "'"))
            {
                if (!VerificarExistencia("sys.database_principals", "name ='" + TxtLoginUsuario.Text + "'"))
                {
                    if (ddlGrupoUsuario.SelectedValue == "0")
                        lblMensaje.Text = "Debe Seleccionar El Grupo de Usuario. <br>";
                    if (ddlGrupoUsuario.SelectedValue == "3" || ddlGrupoUsuario.SelectedValue == "4")
                    {
                        if (ddlOperador.SelectedValue == "0")
                            lblMensaje.Text = "Debe Seleccionar El Operador. <br>";
                    }
                    //20210305 broker
                    if (ddlBroker.SelectedValue == "S")
                    {
                        if (TxtTelefono.Text == "")
                            lblMensaje.Text += "Debe Ingresar el número del teléfono. <br>";
                        if (TxtCargo.Text == "")
                            lblMensaje.Text += "Debe Ingresar el cargo. <br>";
                    }
                    if (lblMensaje.Text == "")
                    {
                        // Se encrypta la contraseña
                        lsContrasena = DelegadaBase.Servicios.CifrarCadena(TxtContrasena.Text);
                        //lsContrasena = TxtContrasena.Text;
                        // Llamo Procedimiento para la Creacion del  Usuario en la base de Datos y en el Servidor.
                        lValorParametros[0] = TxtLoginUsuario.Text;
                        lValorParametros[1] = lsContrasena;
                        lValorParametros[2] = TxtNombreUsuario.Text;
                        lValorParametros[3] = ddlGrupoUsuario.SelectedValue;
                        lValorParametros[4] = "";
                        if (hdfTipoPerfil.Value == "N")
                        {
                            lValorParametros[5] = ddlOperador.SelectedValue;
                            lValorParametros[8] = ddlConsInfOPera.SelectedValue;
                        }
                        lValorParametros[6] = ddlEstado.SelectedValue;
                        lValorParametros[7] = ddlGrupoUsuario.SelectedItem.ToString();
                        if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetUsuario", lsNombreParametros, lTipoparametros, lValorParametros))
                        {
                            Toastr.Warning(this, "Se presentó un Problema en la Creación del  Usuario.!", "Warning!", 50000);
                            lConexion.Cerrar();
                        }
                        else
                        {
                            Toastr.Success(this, "Se Creo Exitosamente el Usuario: " + TxtLoginUsuario.Text + ".!", "Correcto!", 50000);
                            TxtLoginUsuario.Text = "";
                            TxtContrasena.Text = "";
                            TxtConfirma.Text = "";
                        }
                    }
                    else
                    {
                        Toastr.Warning(this, lblMensaje.Text, "Warning!", 50000);
                    }
                }
                else
                {
                    Toastr.Warning(this, "YA existe un Usuario con ese Nombre en la Base de Datos.", "Warning!", 50000);
                }
            }
            else
            {
                Toastr.Warning(this, "El Login de Usuario YA existe", "Warning!", 50000);
            }
        }
        catch (Exception ex)
        {
            lConexion.Cerrar();
            Toastr.Warning(this, ex.Message, "Warning!", 50000);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click1(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        string[] lsNombreParametros = { "@P_codigo_usuario","@P_login", "@P_clave", "@P_nombre", "@P_codigo_grupo_usuario", "@P_fecha_expiracion",
                                        "@P_codigo_operador","@P_estado", "@P_nombre_grupo", "@P_nombre_grupo_ant", "@P_cons_inf_operador", "@P_observacion_cambio",
                                        "@P_e_mail", // Campo nuevo req Restauracion de Claves 2160707  
                                        "@P_telefono", "@P_cargo", "@P_habilitado_broker" //20210305 broker
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int,SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar, // Campo nuevo req Restauracion de Claves 2160707
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar //20210305 broker
                                      };
        string[] lValorParametros = { "", "", "", "", "", "", "0", "", "", "", "N", "",
                                      TxtEmail.Text, // Campo nuevo req Restauracion de Claves 2160707
                                      TxtTelefono.Text ,TxtCargo.Text ,ddlBroker.SelectedValue //20210305 broker
                                    };
        try
        {
            if (ddlGrupoUsuario.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar El Grupo de Usuario. <br>";
            if (hdfTipoPerfil.Value == "N")
            {
                if (ddlOperador.SelectedValue == "0")
                    lblMensaje.Text += "Debe Seleccionar El Operador. <br>";
            }
            //20210305 broker
            if (ddlBroker.SelectedValue == "S")
            {
                if (TxtTelefono.Text == "")
                    lblMensaje.Text += "Debe Ingresar el número del teléfono. <br>";
                if (TxtCargo.Text == "")
                    lblMensaje.Text += "Debe Ingresar el cargo. <br>";
            }
            if (TxtObservaCambio.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la Observación del Cambio. <br>";
            if (lblMensaje.Text == "")
            {
                // Se encrypta la contraseña
                if (TxtContrasena.Text !="") //20200810 ajuste
                    TxtContrasena.Text = DelegadaBase.Servicios.CifrarCadena(TxtContrasena.Text);
                // Llamo Procedimiento para la Creacion del  Usuario en la base de Datos y en el Servidor.
                lValorParametros[0] = LblCodigoUsuario.Text;
                lValorParametros[1] = TxtLoginUsuario.Text;
                lValorParametros[2] = TxtContrasena.Text;
                lValorParametros[3] = TxtNombreUsuario.Text;
                lValorParametros[4] = ddlGrupoUsuario.SelectedValue;
                lValorParametros[5] = "";
                if (hdfTipoPerfil.Value == "N")
                {
                    lValorParametros[6] = ddlOperador.SelectedValue;
                    lValorParametros[10] = ddlConsInfOPera.SelectedValue;
                }
                lValorParametros[7] = ddlEstado.SelectedValue;
                lValorParametros[8] = ddlGrupoUsuario.SelectedItem.ToString();
                lValorParametros[9] = lblGrupoAnterior.Text;
                lValorParametros[11] = TxtObservaCambio.Text.Trim();
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_UptUsuario", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    Toastr.Warning(this, "Se presento un Problema en la Actualización del Usuario.!", "Warning!", 50000);
                    lConexion.Cerrar();

                }
                else
                {
                    lConexion.Cerrar();
                    manejo_bloqueo("E", LblCodigoUsuario.Text);
                    limpiarCampos();
                    Listar();
                    Modal.Cerrar(this, registroUsuario.ID);
                    Toastr.Success(this, "Registro Modificado Correctamente", "Correcto!", 50000);

                }
            }
            else //20210305 broker
                Toastr.Warning(this, lblMensaje.Text , "Warning!", 50000);

        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            lConexion.Cerrar();
            manejo_bloqueo("E", LblCodigoUsuario.Text);
            Toastr.Warning(this, ex.Message, "Warning!", 50000);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20220211 contraseña
    protected void imbDesbCont_Click1(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        string[] lsNombreParametros = { "@P_codigo_usuario"};
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        string[] lValorParametros = { "0"};
        try
        {
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = LblCodigoUsuario.Text;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetDesbContrasena", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    Toastr.Warning(this, "Se presento un Problema en el desbloqueo de la clave.!", "Warning!", 50000);
                    lConexion.Cerrar();

                }
                else
                {
                    lConexion.Cerrar();
                    manejo_bloqueo("E", LblCodigoUsuario.Text);
                    limpiarCampos();
                    Listar();
                    Modal.Cerrar(this, registroUsuario.ID);
                    Toastr.Success(this, "Clave desbloqueada Correctamente", "Correcto!", 50000);
                }
            }
            else //20210305 broker
                Toastr.Warning(this, lblMensaje.Text, "Warning!", 50000);
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            lConexion.Cerrar();
            manejo_bloqueo("E", LblCodigoUsuario.Text);
            Toastr.Warning(this, ex.Message, "Warning!", 50000);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, EventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        manejo_bloqueo("E", LblCodigoUsuario.Text);
        //Response.Redirect("~/frm_contenido.aspx");
        limpiarCampos();
        Listar();
        Modal.Cerrar(this, registroUsuario.ID);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// 
    /// </summary>
    protected void limpiarCampos()
    {
        TxtLoginUsuario.Text = "";
        ddlGrupoUsuario.SelectedValue = "0";
        TxtContrasena.Text = "";
        TxtNombreUsuario.Text = "";
        ddlOperador.SelectedValue = "0";
        TxtEmail.Text = "";
    }
    ///// Eventos Nuevos para la Implementracion del UserControl
    /// <summary>
    /// Metodo del Link Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo(object sender, EventArgs e)
    {
        //mdlregistroUsuarioLabel.InnerText = "Agregar";
        TrObs.Visible = false;
        Nuevo();
    }
 
}