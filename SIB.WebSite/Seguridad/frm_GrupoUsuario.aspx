﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_GrupoUsuario.aspx.cs"  Inherits="Seguridad_frm_GrupoUsuario" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">    
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <%--Buscar--%>
            <div class="kt-portlet__body" runat="server">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Código Grupo Usuario</label>
                            <asp:TextBox ID="txtBusCodigoGrupo" runat="server" autocomplete="off" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FTBEtxtBusCodigoGrupo" runat="server" TargetControlID="txtBusCodigoGrupo"
                                FilterType="Custom, Numbers">
                            </ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Descripción</label>
                            <asp:TextBox ID="txtBusDescripcion" runat="server" autocomplete="off" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:AutoCompleteExtender runat="server" BehaviorID="AutoCompleteEx" ID="autoCompleteExtenderBusDescripcion"
                                TargetControlID="txtBusDescripcion" ServicePath="~/WebService/AutoComplete.asmx" 
                                ServiceMethod="GetCompletionListDescripcionEmpaque" MinimumPrefixLength="2" CompletionInterval="1000"
                                EnableCaching="true" CompletionSetCount="20" CompletionListCssClass="autocomplete_completionListElement"
                                CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                DelimiterCharacters=";,:">
                                <Animations>
                        <OnShow>
                            <Sequence>
                                <%-- Make the completion list transparent and then show it --%>
                                <OpacityAction Opacity="0" />
                                <HideAction Visible="true" />
                                
                                <%--Cache the original size of the completion list the first time
                                    the animation is played and then set it to zero --%>
                                <ScriptAction Script="
                                    // Cache the size and setup the initial size
                                    var behavior = $find('AutoCompleteEx');
                                    if (!behavior._height) {
                                        var target = behavior.get_completionList();
                                        behavior._height = target.offsetHeight - 2;
                                        target.style.height = '0px';
                                    }" />
                                
                                <%-- Expand from 0px to the appropriate size while fading in --%>
                                <Parallel Duration=".4">
                                    <FadeIn />
                                    <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx')._height" />
                                </Parallel>
                            </Sequence>
                        </OnShow>
                        <OnHide>
                            <%-- Collapse down to 0px and fade out --%>
                            <Parallel Duration=".4">
                                <FadeOut />
                                <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx')._height" EndValue="0" />
                            </Parallel>
                        </OnHide>
                                </Animations>
                            </ajaxToolkit:AutoCompleteExtender>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="buscar" />
                        </div>
                    </div>
                </div>

                <%--Mensaje--%>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
                <%--Grilla--%>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgGrupoUsuario" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                Width="100%" CssClass="table-bordered" PageSize="15"
                                OnItemCommand="dtgGrupoUsuario_EditCommand" OnPageIndexChanged="dtgGrupoUsuario_PageIndexChanged">
                                <Columns>
                                    <asp:BoundColumn DataField="codigo_grupo_usuario" HeaderText="Codigo Grupo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="descripcion" HeaderText="Descripcion" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="500px"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="tipo_perfil" HeaderText="Tipo Perfil" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <%--Campo nuevos por Req. 006-17 20170303--%>
                                    <asp:BoundColumn DataField="habilitado_contingencia" HeaderText="Habilitado Contingencia"
                                        ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more-1"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                    <!--begin::Nav-->
                                                    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <ul class="kt-nav">
                                                                <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                                    <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                                    <span class="kt-nav__link-text">Modificar</span>
                                                                    </asp:LinkButton>
                                                                </li>
                                                                <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                                    <i class="kt-nav__link-icon flaticon-delete"></i>
                                                                    <span class="kt-nav__link-text">Eliminar</span>
                                                                    </asp:LinkButton>
                                                                </li>
                                                            </ul>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%--Modals--%>
    <div class="modal fade" id="registroGrupoUsuario" tabindex="-1" role="dialog" aria-labelledby="mdlregistroGrupoUsuarioLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="registroGrupoUsuarioInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlregistroGrupoUsuarioLabel" runat="server">Agregar</h5>
                        </div>
                        <div class="modal-body">

                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Código Grupo Usuario</label>
                                        <asp:TextBox ID="TxtCodigoGrupoUsuario" runat="server" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                        <asp:Label ID="LblCodigoGrupoUsuario" runat="server" Visible="false" CssClass="form-control"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Descripción</label>
                                        <asp:TextBox ID="TxtDescripcion" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RfvDescripcion" runat="server" ErrorMessage="Debe Ingresar la Descripcion del Grupo"
                                            ControlToValidate="TxtDescripcion" ValidationGroup="buscar"> * </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RevDescripcion" ControlToValidate="TxtDescripcion"
                                            runat="server" ValidationExpression="^([0-9a-zA-Z-_])*$" ErrorMessage="Valor invalido en el campo Descripcion"> * </asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <%--20210915 visualizacion--%>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Descripción Adicional</label>
                                        <asp:TextBox ID="TxtDescripcionAdc" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvTxtDescripcionAdc" runat="server" ErrorMessage="Debe Ingresar la Descripcion adicional del Grupo"
                                            ControlToValidate="TxtDescripcionAdc" ValidationGroup="buscar"> * </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Tipo Perfil</label>
                                        <asp:DropDownList ID="ddlTipoPerfil" runat="server" OnSelectedIndexChanged="ddlTipoPerfil_SelectedIndexChanged"
                                            AutoPostBack="true" CssClass="form-control">
                                            <asp:ListItem Value="B">Bmc</asp:ListItem>
                                            <asp:ListItem Value="N">Negociador</asp:ListItem>
                                            <asp:ListItem Value="E">Externo</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Administrador de Operador</label>
                                        <asp:DropDownList ID="ddlAdmon" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                            <asp:ListItem Value="S">Si</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Subastador</label>
                                        <asp:DropDownList ID="ddlSubastador" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                            <asp:ListItem Value="S">Si</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <%--Campos nuevos por Req. 006-17 20170303--%>
                                <div class="col-sm-12 col-md-6 col-lg-4" runat="server" id="TrConti" visible="false">
                                    <div class="form-group">
                                        <label>Habilitado para Contingencia</label>
                                        <asp:DropDownList ID="ddlContingencia" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                            <asp:ListItem Value="S">Si</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4" runat="server" id="TrOpera" visible="false">
                                    <div class="form-group">
                                        <label>Tipo de Operador</label>
                                        <asp:DropDownList ID="ddlTipoOperador" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                            <asp:ListItem Value="S">Si</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Estado</label>
                                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="A">Activo</asp:ListItem>
                                            <asp:ListItem Value="I">Inactivo</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:ValidationSummary ID="VsActividadEmpaque" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="imbCrear" runat="server" CssClass="btn btn-success" Text="Crear" OnClick="imbCrear_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="imbActualiza" runat="server" CssClass="btn btn-success" Text="Actualizar" OnClick="imbActualiza_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="imbSalir" runat="server" class="btn btn-secondary" Text="Salir" CausesValidation="false" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="imbSalir_Click1" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
