﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using Segas.Web.Elements;
public partial class Seguridad_frm_Menu : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Menú";
    a_menu loMenuBuscar = new a_menu();
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    string gsTabla = "a_menu";

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Establese los permisos del sistema

        //Titulo
        Master.Titulo = "Menú";

        lConexion = new clConexion(goInfo);
        /// Activacion de los Botones
        buttons.Inicializar(ruta: gsTabla);
        buttons.CrearOnclick += btnNuevo;
        buttons.FiltrarOnclick += btnConsultar_Click;
        buttons.ExportarExcelOnclick += ImgExcel_Click;
        buttons.ExportarPdfOnclick += ImgPdf_Click;


        if (!IsPostBack)
        {
            EstablecerPermisosSistema();
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            Listar();
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, gsTabla );

        foreach (DataGridItem Grilla in dtgMenu.Items)
        {
            var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
            lkbModificar.Visible = (bool)permisos["UPDATE"];  //20170216 Modif Inf Operativa Participantes Req.003-17 //20190306 rq013-19
            var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
            lkbEliminar.Visible = (bool)permisos["DELETE"];  //20170216 Modif Inf Operativa Participantes Req.003-17 //20190306 rq013-19
        }
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        Modal.Abrir(this, registroMenu.ID, registroMenu.ID);
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoMenu.Visible = false;
        LblCodigoMenu.Visible = true;
        LblCodigoMenu.Text = "Automatico";
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    a_menu MenuBuscar = new a_menu();
                    MenuBuscar.codigo_menu = Convert.ToInt32(modificar);
                    a_menu Menu = DelegadaBase.Servicios.traer_a_menu(goInfo, MenuBuscar);
                    if (Menu != null)
                    {
                        LblCodigoMenu.Text = Menu.codigo_menu.ToString();
                        TxtDescripcionMenu.Text = Menu.menu.ToString();
                        TxtRuta.Text = Menu.ruta.ToString();
                        TxtFrame.Text = Menu.frame.ToString();
                        TxtTabla.Text = Menu.tabla.ToString();
                        ddlVisualizar.SelectedValue = Menu.visualizar.ToString();
                        ddlEstado.SelectedValue = Menu.estado.ToString();
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoMenu.Visible = false;
                        LblCodigoMenu.Visible = true;
                        //TrObs.Visible = true;
                    }
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                    Modal.Abrir(this, registroMenu.ID, registroMenu.ID);
                }
                else
                {
                    Listar();
                    Toastr.Warning(this, "No se Puede editar el Registro por que esta Bloqueado. Codigo Menu =  " + modificar.ToString(), "Warning!", 50000);

                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, ex.Message, "Warning!", 50000);
            }
        }
        if (lblMensaje.Text == "")
        {
            Modal.Abrir(this, registroMenu.ID, registroMenu.ID);

            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    //private void Buscar()
    //{
    //    lblTitulo.Text = "Consultar " + lsTitulo;
    //}
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            //FLLG - Agrega Validacion para controlar texto txtBusCodigoMenu.Text vacio
            if (txtBusCodigo.Text != "" && txtBusCodigo.Text != null)
            {
                loMenuBuscar.codigo_menu = Convert.ToInt32(txtBusCodigo.Text);
            }

            loMenuBuscar.menu = txtBusDescripcion.Text;
            List<a_menu> listaMenu = new List<a_menu>();
            listaMenu.Add(loMenuBuscar);
            List<String> llFiltros = new List<string>();
            llFiltros.Add("=");
            llFiltros.Add("=");
            string lsCondicion = "codigo_menu >0 ";
            dtgMenu.DataSource = DelegadaBase.Servicios.consultarTabla<a_menu>(goInfo, listaMenu, llFiltros, lsCondicion);
            dtgMenu.DataBind();
            /////////////////////////////////////////////////////////////////////////////////////
            /// Para Llenar una Grilla Con la Clase clProcesosSql
            ////////////////////////////////////////////////////////////////////////////////////
            //lConexion = new clConexion(goInfo);
            //lConexion.Abrir();
            //dtgActividadEconomica.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
            //dtgActividadEconomica.DataBind();
            //lConexion.Cerrar();
            /////////////////////////////////////////////////////////////////////////////////////
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, ex.Message, "Warning!", 50000);
        }
        if (lblMensaje.Text == "") return;
        Toastr.Warning(this, lblMensaje.Text);
        lblMensaje.Text = "";
    }
    /// <summary>
    /// Nombre: dtgActividadEconomica_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMenu_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMenu.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgActividadEconomica_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMenu_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoMenu = "";
        lblMensaje.Text = "";
        if (e.CommandName.Equals("Modificar"))
        {
            mdlregistroMenuLabel.InnerText = "Modificar";
            lCodigoMenu = this.dtgMenu.Items[e.Item.ItemIndex].Cells[0].Text;
            Modificar(lCodigoMenu);
        }
        if (e.CommandName.Equals("Eliminar"))
        {
            lblMensaje.Text = "";
            /// Verificar Si el Registro esta Bloqueado
            if (!manejo_bloqueo("V", this.dtgMenu.Items[e.Item.ItemIndex].Cells[0].Text))
            {
                lCodigoMenu = this.dtgMenu.Items[e.Item.ItemIndex].Cells[0].Text;
                try
                {
                    DelegadaBase.Servicios.borrar_a_menu(goInfo, Convert.ToInt32(lCodigoMenu));
                    Toastr.Success(this, "Registro Eliminado Correctamente", "Correcto!", 50000);
                }
                catch (Exception ex)
                {

                    Toastr.Warning(this, ex.Message, "Warning!", 50000);
                    Toastr.Warning(this, "Error al Eliminar. " + ex.Message, "Warning!", 50000);
                }
            }
            else
            {
                Toastr.Warning(this, "No se Puede Eliminar el Registro por que esta Bloqueado. Codigo Menu =  " + this.dtgMenu.Items[e.Item.ItemIndex].Cells[0].Text.ToString(), "Warning!", 50000);
            }
            Listar();
        }

    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lsTabla, string lsCondicion)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lsTabla, lsCondicion, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_Menu)
    {
        string lsCondicion = "nombre_tabla='a_menu' and llave_registro='codigo_menu=" + lscodigo_Menu + "'";
        string lsCondicion1 = "codigo_menu=" + lscodigo_Menu.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "a_menu";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "a_menu", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, EventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_menu > 0";
            Server.Transfer("../Informes/exportar_excel.aspx?tipo_export=1&procedimiento=pa_ValidarExistencia&columnas=codigo_menu*menu*ruta*frame*tabla*visualizar*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion + "&nombre_tabla=a_menu&titulo_informe=Listado Tabla");
        }
        catch (Exception)
        {
            Toastr.Warning(this, "No se Pudo Generar el Informe.!", "Warning!", 50000);
        }
    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, EventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_menu > 0";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=a_menu&procedimiento=pa_ValidarExistencia&columnas=codigo_menu*menu*ruta*frame*tabla*visualizar*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception)
        {
            Toastr.Warning(this, "No se Pudo Generar el Informe.!", "Warning!", 50000);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click1(object sender, EventArgs e)
    {
        try
        {
            /// Llama el metodo que valida si el codigo de la actividad existe en la base de datos.
            //if (!VerificarExistencia(gsTabla, "menu ='" + TxtDescripcionMenu.Text + "'"))
            //{
            //if (VerificarExistencia("sysobjects", "name ='" + TxtTabla.Text + "'") || TxtTabla.Text.Trim().Length <= 0)
            //{
            a_menu Menu = new a_menu();
            Menu.codigo_menu = 0;
            Menu.menu = TxtDescripcionMenu.Text;
            Menu.visualizar = Char.Parse(ddlVisualizar.SelectedValue.Trim().Substring(0, 1));
            Menu.frame = TxtFrame.Text;
            Menu.tabla = TxtTabla.Text;
            Menu.ruta = TxtRuta.Text;
            Menu.estado = Char.Parse(ddlEstado.SelectedValue.Trim().Substring(0, 1));
            DelegadaBase.Servicios.guardar_a_menu(goInfo, Menu);
            lblMensaje.Text = "";
            Modal.Cerrar(this, registroMenu.ID);
            Listar();
            //}
            //else
            //    lblMensaje.Text = "La Tabla Ingresada NO existe en la Base de Datos.";
            //}
            //else
            //{
            //    lblMensaje.Text = "La Descripcion de Menu YA existe";
            //}
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, ex.Message, "Warning!", 50000);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click1(object sender, EventArgs e)
    {
        try
        {
            if (!VerificarExistencia(gsTabla, "codigo_menu <> " + LblCodigoMenu.Text + " and  menu ='" + TxtDescripcionMenu.Text + "'"))
            {

                //if (VerificarExistencia("sysobjects", "name ='" + TxtTabla.Text + "'") || TxtTabla.Text.Trim().Length <= 0)
                //{
                a_menu Menu = new a_menu();
                Menu.codigo_menu = Convert.ToInt32(LblCodigoMenu.Text);
                Menu.menu = TxtDescripcionMenu.Text;
                Menu.visualizar = Char.Parse(ddlVisualizar.SelectedValue.Trim().Substring(0, 1));
                Menu.frame = TxtFrame.Text;
                Menu.tabla = TxtTabla.Text;
                Menu.ruta = TxtRuta.Text;
                Menu.estado = Char.Parse(ddlEstado.SelectedValue.Trim().Substring(0, 1));
                DelegadaBase.Servicios.modificar_a_menu(goInfo, Menu);
                /// Desbloquea el Registro Actualizado
                manejo_bloqueo("E", LblCodigoMenu.Text);
                Modal.Cerrar(this, registroMenu.ID);
                Listar();
                Toastr.Success(this, "Registro Modicicado Correctamente", "Correcto!", 50000);
                //}
                //else
                //    lblMensaje.Text = "La Tabla Ingresada NO existe en la Base de Datos.";
            }
            else
            {
                Toastr.Warning(this, "Descripcion de tipo de Menu YA Existe", "Warning!", 50000);

            }

        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoMenu.Text);
            Toastr.Warning(this, ex.Message, "Warning!", 50000);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click1(object sender, EventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        manejo_bloqueo("E", LblCodigoMenu.Text);
        Modal.Cerrar(this, registroMenu.ID);
        //Response.Redirect("~/frm_contenido.aspx");
        Listar();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        Listar();
    }
    ///// Eventos Nuevos para la Implementracion del UserControl
    /// <summary>
    /// Metodo del Link Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo(object sender, EventArgs e)
    {
        mdlregistroMenuLabel.InnerText = "Agregar";
        Nuevo();
    }

    /// <summary>
    /// Metodo del Link Listar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void btnListar(object sender, EventArgs e)
    //{
    //    Listar();
    //}
    /// <summary>
    /// Metodo del Link Consultar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void btnConsulta(object sender, EventArgs e)
    //{
    //    Buscar();
    //}
}
