﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_GrupoMenu.aspx.cs" Inherits="Seguridad_frm_GrupoMenu" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <%--Buscar--%>
            <div class="kt-portlet__body" runat="server">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Grupo usuario</label>
                            <asp:DropDownList ID="ddlGrupo1" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Menú Padre</label>
                            <asp:DropDownList ID="ddlPadre1" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Menú</label>
                            <asp:DropDownList ID="ddlMenu1" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <%--<div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="btnConsultar" runat="server" CssClass="btn btn-success" Text="Buscar" OnClick="btnConsultar_Click" ValidationGroup="detalle" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="buscar" />
                        </div>
                    </div>
                </div>--%>
                <%--</div>--%>
                <%--Mensaje--%>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>

                <%--Grilla--%>
                <%--<div class="kt-portlet__body" runat="server" id="tblgrilla" visible="false">--%>
                <%--<div class="row">--%>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgMenu" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                Width="100%" CssClass="table-bordered" PageSize="30"
                                OnItemCommand="dtgMenu_EditCommand" OnPageIndexChanged="dtgMenu_PageIndexChanged">
                                <Columns>
                                    <asp:BoundColumn DataField="codigo_grupo_menu" HeaderText="Cod" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_grupo_usuario" HeaderText="Grupo Usuario" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_menu_padre" HeaderText="Menú Padre" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_menu" HeaderText="Menú" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="orden" HeaderText="Orden" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_consulta" HeaderText="consulta" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_crea" HeaderText="crea" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_modifica" HeaderText="modifica" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_elimina" HeaderText="elimina" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more-1"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                    <!--begin::Nav-->
                                                    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <ul class="kt-nav">
                                                                <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                                    <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                                    <span class="kt-nav__link-text">Modificar</span>
                                                                    </asp:LinkButton>
                                                                </li>
                                                                <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                                    <i class="kt-nav__link-icon flaticon-delete"></i>
                                                                    <span class="kt-nav__link-text">Eliminar</span>
                                                                    </asp:LinkButton>
                                                                </li>
                                                            </ul>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <%--Modals--%>
  
    <div class="modal fade " id="registroGrupoMenu" tabindex="-1" role="dialog" aria-labelledby="mdlregistroGrupoMenuLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog modal-lg" id="registroGrupoMenuInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content ">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header " >
                            <h5 class="modal-title" id="mdlregistroGrupoMenuLabel" runat="server">Agregar</h5>
                        </div>
                        <div class="modal-body ">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <div style="width: auto; height: 450px; overflow: scroll">
                                            <asp:TreeView ID="tvMenu" runat="server" ImageSet="Arrows" LineImagesFolder="~/TreeLineImages"
                                                ExpandDepth="FullyExpand">
                                                <ParentNodeStyle Font-Bold="False" />
                                                <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                                                <SelectedNodeStyle Font-Underline="True" HorizontalPadding="0px" VerticalPadding="0px"
                                                    ForeColor="#5555DD" />
                                            </asp:TreeView>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label>Código Grupo Menú</label>
                                                <asp:TextBox ID="TxtCodigoGrupoMenu" runat="server" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                                <asp:Label ID="LblCodigoGrupoMenu" runat="server" Visible="false" CssClass="form-control"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label>Grupo usuario</label>
                                                <asp:DropDownList ID="ddlGrupoUsuario" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlGrupoUsuario_SelectedIndexChanged" CssClass="form-control">
                                                </asp:DropDownList>
                                                <asp:CompareValidator ID="cvddlGrupoUsuario" runat="server" ControlToValidate="ddlGrupoUsuario"
                                                    ValidationGroup="VsGrupoMenu" ErrorMessage="Debe seleccionar el grupo de usuario"
                                                    Operator="NotEqual" ValueToCompare="0">*</asp:CompareValidator>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label>Menú Padre</label>
                                                <asp:DropDownList ID="ddlPadre" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label>Menú</label>
                                                <asp:DropDownList ID="ddlMenu" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                                <asp:CompareValidator ID="cvddlMenu" runat="server" ControlToValidate="ddlMenu" ValidationGroup="VsGrupoMenu"
                                                    ErrorMessage="Debe seleccionar el menú a asignar" Operator="NotEqual" ValueToCompare="0">*</asp:CompareValidator>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label>Orden</label>
                                                <asp:TextBox ID="TxtOrden" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvTxtOrden" runat="server" ControlToValidate="TxtOrden"
                                                    ValidationGroup="VsGrupoMenu" ErrorMessage="Debe digitar el número de orden">*</asp:RequiredFieldValidator>
                                                <asp:CompareValidator ID="cvTxtOrden" runat="server" ErrorMessage="El número de orden debe ser numérico"
                                                    ControlToValidate="TxtOrden" Type="Integer" Operator="DataTypeCheck" ValidationGroup="VsGrupoMenu">*</asp:CompareValidator>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label>Consulta</label>
                                                <asp:DropDownList ID="ddlConsulta" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="1">Si</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label>Crea</label>
                                                <asp:DropDownList ID="ddlCrea" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="1">Si</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label>Modifica</label>
                                                <asp:DropDownList ID="ddlModifica" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="1">Si</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label>Elimina</label>
                                                <asp:DropDownList ID="ddlElimina" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="1">Si</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:ValidationSummary ID="VsGrupoMenu" runat="server" ValidationGroup="VsGrupoMenu" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="imbCrearOtro" runat="server" CssClass="btn btn-primary" Text="Crear e Insertar Otro" OnClick="imbCrearOtro_Click" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="imbCrear" runat="server" CssClass="btn btn-primary" Text="Crear" OnClick="imbCrear_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="imbActualiza" runat="server" CssClass="btn btn-primary" Text="Actualizar" OnClick="imbActualiza_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="imbSalir" runat="server" class="btn btn-secondary" Text="Salir" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="imbSalir_Click1" CausesValidation="false" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
