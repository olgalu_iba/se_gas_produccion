﻿/*
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 * Copyright (C) 2019 Bolsa Mercantil de Colombia, Todos los Derechos Reservados
 * Archivo:               Home.cs
 * Tipo:                  Clase principal
 * Autor:                 Oscar Piñeros
 * Fecha creación:        2019 Julio 05
 * Fecha modificación:    
 * Propósito:             Clase que permite administrar el home de la aplicación 
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace WebForms
{
    public partial class Home : Page
    {
        private InfoSessionVO goInfo;
        private clConexion lConexion;

        /// <summary>
        /// Propiedad que almacena los estados de las tareas pendientes
        /// </summary>
        public string ColorEstados
        {
            get { return ViewState["ColorEstados"] != null && !string.IsNullOrEmpty(ViewState["ColorEstados"].ToString()) ? ViewState["ColorEstados"].ToString() : string.Empty; }
            set { ViewState["ColorEstados"] = value; }
        }

        //20220221
        private string DetOpe
        {
            get
            {
                if (ViewState["DetOpe"] == null)
                    return "";
                else
                    return (string)ViewState["DetOpe"];
            }
            set
            {
                ViewState["DetOpe"] = value;
            }
        }
        //20220221
        private string CodOpe
        {
            get
            {
                if (ViewState["CodOpe"] == null)
                    return "0";
                else
                    return (string)ViewState["CodOpe"];
            }
            set
            {
                ViewState["CodOpe"] = value;
            }
        }
        //20220221
        private string DetTra
        {
            get
            {
                if (ViewState["DetTra"] == null)
                    return "";
                else
                    return (string)ViewState["DetTra"];
            }
            set
            {
                ViewState["DetTra"] = value;
            }
        }
        /// <summary>
        /// Evento donde se cargarán los valores para su página 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null)
            {
                Response.Redirect("../login.aspx");
                return;
            }
            goInfo.Programa = "HOME";
            if (IsPostBack) return;
            //Titulo
            Master.Titulo = "Sistema Electrónico de GAS";
            //Descripcion
            Master.DescripcionPagina = "Somos el gestor del mercado de Gas natural en Colombia";
            //Se notifica de la carga del sistema
            Toastr.Info(this, "Hola, bienvenido al sistema!", "Sistema Web");
        }

        /// <summary>
        /// WIDGET 1
        /// Precio histórico y cantidades
        /// </summary>
        /// <returns></returns>
        protected string ObtenerDatos_pa_GetInfOporTran()
        {
            try
            {
                return OportunidadesTransaccionales();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
                return "[]";
            }
        }

        /// <summary>
        /// WIDGET 2 
        /// Obtener Información Oportunidad Transporte Secundario
        /// </summary>
        /// <returns></returns>
        protected string ObtenerDatos_pa_GetInfOporTran_S()
        {
            try
            {
                return OportunidadesTransaccionales(false);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
                return "[]";
            }
        }

        /// <summary>
        /// Trae la información de las oportunidades transaccionales según el mercado
        /// </summary>
        /// <param name="mercadoPrimario"></param>
        private string OportunidadesTransaccionales(bool mercadoPrimario = true)
        {
            //Precio histórico y cantidades
            var strDatos = new StringBuilder();
            var ldsOportunidad = new DataSet();

            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            var lComando1 = new SqlCommand();
            var lsqldata1 = new SqlDataAdapter();
            lComando1.Connection = lConexion.gObjConexion;
            lComando1.CommandTimeout = 3600;
            lComando1.CommandType = CommandType.StoredProcedure;

            var dt = new DataGrid();
            lComando1.CommandText = "pa_GetInfOporTran";
            lComando1.Parameters.Add("@P_tipo_mercado", SqlDbType.Text).Value = mercadoPrimario ? 'P' : 'S';
            lComando1.ExecuteNonQuery();
            lsqldata1.SelectCommand = lComando1;
            lsqldata1.Fill(ldsOportunidad);
            dt.DataSource = ldsOportunidad;
            dt.DataBind();

            lConexion.Cerrar();

            var separador = string.Empty;
            strDatos.Append("[['Mercados','Fecha Inicio','Fecha Finalizacion'],");

            // Recorro la grilla de los Datos para Armar la Grafica
            foreach (DataGridItem item in dt.Items)
            {
                string mercados = item.Cells[mercadoPrimario ? 1 : 2].Text;
                string fechaInicio = item.Cells[mercadoPrimario ? 2 : 3].Text;
                string fechaFinalizacion = item.Cells[mercadoPrimario ? 3 : 4].Text;

                // Arreglo de datos para pintar en la grafica de Google
                strDatos.Append($"{separador}['{mercados}', '{fechaInicio}', '{fechaFinalizacion}']");
                separador = ", ";
            }
            strDatos.Append("]");
            strDatos.Replace("&nbsp;", "0");
            if (!strDatos.ToString().Equals("[['Mercados','Fecha Inicio','Fecha Finalizacion'],]"))
                return strDatos.ToString();
            strDatos.Clear();
            strDatos.Append("'Null'");
            return strDatos.ToString();
        }

        /// <summary>
        /// WIDGET 3
        /// Tablero de Precios y Cantidades Secundario
        /// </summary>
        /// <returns></returns>
        protected string ObtenerDatos_pa_GetInfPreCant()
        {
            try
            {
                return PreciosCantidades();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
                return "[]";
            }
        }

        /// <summary>
        /// WIDGET 4
        /// Tablero de Precios y Cantidades Secundario
        /// </summary>
        /// <returns></returns>
        protected string ObtenerDatos_pa_GetInfPreCant_S()
        {
            try
            {
                return PreciosCantidades(false);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
                return "[]";
            }
        }

        /// <summary>
        /// Consulta el precio promedio ponderado por mecanismo según el mercado
        /// </summary>
        /// <param name="mercadoPrimario"></param>
        /// <returns></returns>
        private string PreciosCantidades(bool mercadoPrimario = true)
        {
            //TABLERO DE PRECIOS Y CANTIDADES
            var strDatos = new StringBuilder();
            var ldsPrecant = new DataSet();

            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            var lComando1 = new SqlCommand();
            var lsqldata1 = new SqlDataAdapter();
            lComando1.Connection = lConexion.gObjConexion;
            lComando1.CommandTimeout = 3600;
            lComando1.CommandType = CommandType.StoredProcedure;

            var dt = new DataGrid();
            lComando1.CommandText = "pa_GetInfPreCant";
            lComando1.Parameters.Add("@P_tipo_mercado ", SqlDbType.Text).Value = mercadoPrimario ? 'P' : 'S';
            lComando1.ExecuteNonQuery();
            lsqldata1.SelectCommand = lComando1;
            lsqldata1.Fill(ldsPrecant);
            dt.DataSource = ldsPrecant;
            dt.DataBind();

            lConexion.Cerrar();

            var separador = string.Empty;
            strDatos.Append("[['Descripción Subasta','Fecha','Precio Promedio'],");
            // Recorro la grilla de los Datos para Armar la Grafica
            foreach (DataGridItem item in dt.Items)
            {
                string descripcion = item.Cells[0].Text;
                string fecha = item.Cells[1].Text;
                string precioPromedio = item.Cells[2].Text;

                // Arreglo de datos para pintar en la grafica de Google
                strDatos.Append($"{separador}['{descripcion}', '{fecha}', '{precioPromedio}']");
                separador = ", ";
            }
            strDatos.Append("]");
            strDatos.Replace("&nbsp;", string.Empty);
            if (!strDatos.ToString().Equals("[['Descripción Subasta','Fecha','Precio Promedio'],]"))
                return strDatos.ToString();
            strDatos.Clear();
            strDatos.Append("'Null'");
            return strDatos.ToString();
        }

        /// <summary>
        /// WIDGET 5
        /// Consulta las tareas pendientes del usuario
        /// </summary>
        /// <returns></returns>
        protected string ObtenerDatos_pa_GetTableroControlIni() //20220221
        {
            try
            {
                //20220221
                if (this.Request.QueryString["DetOpe"] != null && this.Request.QueryString["DetOpe"].ToString() != "")
                {
                    DetOpe = this.Request.QueryString["DetOpe"].ToString();
                }
                if (this.Request.QueryString["CodOpe"] != null && this.Request.QueryString["CodOpe"].ToString() != "")
                {
                    CodOpe = this.Request.QueryString["CodOpe"].ToString();
                }
                if (DetOpe == "S")
                {
                    table_div2.Attributes.CssStyle.Add("overflow-y", "scroll");

                    var strDatosPendientes = new StringBuilder();
                    var ldsPendientes = new DataSet();

                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    var lComando1 = new SqlCommand();
                    var lsqldata1 = new SqlDataAdapter();
                    lComando1.Connection = lConexion.gObjConexion;
                    lComando1.CommandTimeout = 3600;
                    lComando1.CommandType = CommandType.StoredProcedure;

                    var dtPendientes = new DataGrid();
                    lComando1.CommandText = "pa_GetTableroControlIni"; 
                    lComando1.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = goInfo.cod_comisionista;
                    lComando1.Parameters.Add("@P_codigo_reporte", SqlDbType.Int).Value = CodOpe;
                    lComando1.Parameters.Add("@P_ind_detalle", SqlDbType.VarChar).Value = "D"; 
                    lComando1.ExecuteNonQuery();
                    lsqldata1.SelectCommand = lComando1;
                    lsqldata1.Fill(ldsPendientes);
                    dtPendientes.DataSource = ldsPendientes;
                    dtPendientes.DataBind();

                    lConexion.Cerrar();

                    var separador = string.Empty;
                    var separador2 = string.Empty;
                    strDatosPendientes.Append("[['Tipo Energía','Fecha'],");
                    var colorEstados = new StringBuilder();

                    foreach (DataGridItem item in dtPendientes.Items)
                    {
                        // Recorro la grilla de los Datos para Armar la Grafica
                        string actividad = item.Cells[0].Text;
                        string fecha = item.Cells[1].Text;

                        // Arreglo de datos para pintar en la grafica de Google
                        strDatosPendientes.Append($"{separador}['{actividad}', '{fecha}']");
                        separador = ", ";
                        separador2 = "&";
                    }
                    strDatosPendientes.Append("]");
                    strDatosPendientes.Replace("&nbsp;", string.Empty);
                    ColorEstados = $"'{colorEstados}'";
                    if (!strDatosPendientes.ToString().Equals("[['Tipo Energía','Fecha'],]"))
                        return strDatosPendientes.ToString();
                    strDatosPendientes.Clear();
                    strDatosPendientes.Append("'Null'");
                    return strDatosPendientes.ToString();
                }
                else
                {
                    var strDatosPendientes = new StringBuilder();
                    var ldsPendientes = new DataSet();

                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    var lComando1 = new SqlCommand();
                    var lsqldata1 = new SqlDataAdapter();
                    lComando1.Connection = lConexion.gObjConexion;
                    lComando1.CommandTimeout = 3600;
                    lComando1.CommandType = CommandType.StoredProcedure;

                    var dtPendientes = new DataGrid();
                    lComando1.CommandText = "pa_GetTableroControlIni"; //20220221
                    lComando1.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = goInfo.cod_comisionista;
                    lComando1.Parameters.Add("@P_ind_detalle", SqlDbType.VarChar).Value = "R"; //20220221
                    lComando1.ExecuteNonQuery();
                    lsqldata1.SelectCommand = lComando1;
                    lsqldata1.Fill(ldsPendientes);
                    dtPendientes.DataSource = ldsPendientes;
                    dtPendientes.DataBind();

                    lConexion.Cerrar();

                    var separador = string.Empty;
                    var separador2 = string.Empty;
                    strDatosPendientes.Append("[['Tipo Energía','Estado Actual'],");
                    var colorEstados = new StringBuilder();

                    foreach (DataGridItem item in dtPendientes.Items)
                    {
                        // Recorro la grilla de los Datos para Armar la Grafica
                        string actividad = item.Cells[1].Text;
                        string estado = "";
                        if (item.Cells[2].Text == "N")
                            estado = "<a href = \"../WebForms/Home.aspx?DetOpe=S&CodOpe="+ item.Cells[0].Text + "\" > " + item.Cells[3].Text + "</a>";
                        else
                            estado = item.Cells[3].Text;
                        colorEstados.Append($"{separador2}{item.Cells[4].Text}");

                        // Arreglo de datos para pintar en la grafica de Google
                        strDatosPendientes.Append($"{separador}['{actividad}', '{estado}']");
                        separador = ", ";
                        separador2 = "&";
                    }
                    strDatosPendientes.Append("]");
                    strDatosPendientes.Replace("&nbsp;", string.Empty);
                    ColorEstados = $"'{colorEstados}'";
                    if (!strDatosPendientes.ToString().Equals("[['Tipo Energía','Estado Actual'],]"))
                        return strDatosPendientes.ToString();
                    strDatosPendientes.Clear();
                    strDatosPendientes.Append("'Null'");
                    return strDatosPendientes.ToString();
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
                return "'Null'";
            }
        }

        /// <summary>
        /// WIDGET 5A
        /// Consulta las tareas pendientes del usuario
        /// </summary>
        /// <returns></returns>
        /// 20220221 registro extermporaneo
        protected string ObtenerDatos_pa_GetTableroControlContIni() 
        {
            try
            {
                if (this.Request.QueryString["DetTra"] != null && this.Request.QueryString["DetTra"].ToString() != "")
                {
                    DetTra = this.Request.QueryString["DetTra"].ToString();
                }
                if (DetTra == "S")
                {
                    table_div2A.Attributes.CssStyle.Add("overflow-y", "scroll");

                    var strDatosPendientes = new StringBuilder();
                    var ldsPendientes = new DataSet();

                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    var lComando1 = new SqlCommand();
                    var lsqldata1 = new SqlDataAdapter();
                    lComando1.Connection = lConexion.gObjConexion;
                    lComando1.CommandTimeout = 3600;
                    lComando1.CommandType = CommandType.StoredProcedure;

                    var dtPendientes = new DataGrid();
                    lComando1.CommandText = "pa_GetTableroControlContIni";
                    lComando1.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = goInfo.cod_comisionista;
                    lComando1.Parameters.Add("@P_ind_detalle", SqlDbType.VarChar).Value = "D";
                    lComando1.ExecuteNonQuery();
                    lsqldata1.SelectCommand = lComando1;
                    lsqldata1.Fill(ldsPendientes);
                    dtPendientes.DataSource = ldsPendientes;
                    dtPendientes.DataBind();

                    lConexion.Cerrar();

                    var separador = string.Empty;
                    var separador2 = string.Empty;
                    strDatosPendientes.Append("[['Operación','Tipo Negociación', 'Estado'],");
                    var colorEstados = new StringBuilder();

                    foreach (DataGridItem item in dtPendientes.Items)
                    {
                        // Recorro la grilla de los Datos para Armar la Grafica
                        string operacion = item.Cells[0].Text;
                        string subasta = item.Cells[1].Text;
                        string estado = item.Cells[2].Text;

                        // Arreglo de datos para pintar en la grafica de Google
                        strDatosPendientes.Append($"{separador}['{operacion}','{subasta}', '{estado}']");
                        separador = ", ";
                        separador2 = "&";
                    }
                    strDatosPendientes.Append("]");
                    strDatosPendientes.Replace("&nbsp;", string.Empty);
                    ColorEstados = $"'{colorEstados}'";
                    if (!strDatosPendientes.ToString().Equals("[['Operación','Tipo Negociación', 'Estado'],]"))
                        return strDatosPendientes.ToString();
                    strDatosPendientes.Clear();
                    strDatosPendientes.Append("'Null'");
                    return strDatosPendientes.ToString();
                }
                else
                {
                    var strDatosPendientes = new StringBuilder();
                    var ldsPendientes = new DataSet();

                    lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    var lComando1 = new SqlCommand();
                    var lsqldata1 = new SqlDataAdapter();
                    lComando1.Connection = lConexion.gObjConexion;
                    lComando1.CommandTimeout = 3600;
                    lComando1.CommandType = CommandType.StoredProcedure;

                    var dtPendientes = new DataGrid();
                    lComando1.CommandText = "pa_GetTableroControlContIni"; //20220221
                    lComando1.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = goInfo.cod_comisionista;
                    lComando1.Parameters.Add("@P_ind_detalle", SqlDbType.VarChar).Value = "R"; //20220221
                    lComando1.ExecuteNonQuery();
                    lsqldata1.SelectCommand = lComando1;
                    lsqldata1.Fill(ldsPendientes);
                    dtPendientes.DataSource = ldsPendientes;
                    dtPendientes.DataBind();

                    lConexion.Cerrar();

                    var separador = string.Empty;
                    var separador2 = string.Empty;
                    strDatosPendientes.Append("[['Estado','Detalle'],");
                    var colorEstados = new StringBuilder();

                    foreach (DataGridItem item in dtPendientes.Items)
                    {
                        // Recorro la grilla de los Datos para Armar la Grafica
                        string estado = item.Cells[0].Text;
                        string detalle = "";
                        if (item.Cells[3].Text == "S")
                            detalle = "<a href = \"../WebForms/Home.aspx?DetTra=S\" > " + item.Cells[1].Text + "</a>";
                        else
                            detalle = item.Cells[1].Text;
                        colorEstados.Append($"{separador2}{item.Cells[2].Text}");

                        // Arreglo de datos para pintar en la grafica de Google
                        strDatosPendientes.Append($"{separador}['{estado}', '{detalle}']");
                        separador = ", ";
                        separador2 = "&";
                    }
                    strDatosPendientes.Append("]");
                    strDatosPendientes.Replace("&nbsp;", string.Empty);
                    ColorEstados = $"'{colorEstados}'";
                    if (!strDatosPendientes.ToString().Equals("[['Estado','Detalle'],]"))
                        return strDatosPendientes.ToString();
                    strDatosPendientes.Clear();
                    strDatosPendientes.Append("'Null'");
                    return strDatosPendientes.ToString();
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
                return "'Null'";
            }
        }

        /// <summary>
        /// WIDGET 6
        /// Contratación por Sector Consumo
        /// </summary>
        /// <returns></returns>
        protected string ObtenerDatos_pa_GetInfGrafIniMes()
        {
            try
            {
                var strDatos = new StringBuilder();
                var ldsConsumo = new DataSet();
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();

                var lComando1 = new SqlCommand();
                var lsqldata1 = new SqlDataAdapter();
                lComando1.Connection = lConexion.gObjConexion;
                lComando1.CommandTimeout = 3600;
                lComando1.CommandType = CommandType.StoredProcedure;

                var dt = new DataGrid();
                lComando1.CommandText = "pa_GetInfGrafIniMes";
                lComando1.ExecuteNonQuery();
                lsqldata1.SelectCommand = lComando1;
                lsqldata1.Fill(ldsConsumo);
                dt.DataSource = ldsConsumo;
                dt.DataBind();
                lConexion.Cerrar();

                strDatos.Append("[['Fecha', 'Comercial', 'Industrial', 'Residencial',  'Refinería', 'GNVC','Generación Térmica', 'Otros']"); //20211228

                // Recorro la grilla de los datos para Armar la Grafica
                foreach (DataGridItem grilla in dt.Items)
                {
                    string fecha = grilla.Cells[2].Text;
                    string comercial = grilla.Cells[3].Text;
                    string industrial = grilla.Cells[4].Text;
                    string residencial = grilla.Cells[5].Text;
                    //string petroquimica = grilla.Cells[6].Text; //20211228
                    string refineria = grilla.Cells[6].Text;
                    string gnvc = grilla.Cells[7].Text;
                    string generacionTermica = grilla.Cells[8].Text;
                    string otros = grilla.Cells[9].Text;

                    // Arreglo de datos para pintar en la grafica de Google
                    strDatos.Append($",['{fecha}',{comercial},{industrial},{residencial},{refineria},{gnvc},{generacionTermica},{otros}]"); //20211228
                }
                strDatos.Append("]");
                strDatos.Replace("&nbsp;", "0");
                if (!strDatos.ToString().Equals("[['Fecha', 'Comercial', 'Industrial', 'Residencial','Refinería', 'GNVC','Generación Térmica', 'Otros']]")) //20211228
                    return strDatos.ToString();
                strDatos.Clear();
                strDatos.Append("'Null'");
                return strDatos.ToString();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
                return "[]";
            }
        }

        /// <summary>
        /// WIDGET 7 
        /// Consulta la contratación por Sector Consumo
        /// </summary>
        /// <returns></returns>
        protected string ObtenerDatos_pa_GetInfGrafIniMesMs()
        {
            try
            {
                var strDatos = new StringBuilder();
                var puntos = new Dictionary<int, string>();
                var lstPrecioConsumidor = new List<PrecioConsumidorVO>();

                var ldsConsumoS = new DataSet();
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                var lComando1 = new SqlCommand();
                var lsqldata1 = new SqlDataAdapter();
                lComando1.Connection = lConexion.gObjConexion;
                lComando1.CommandTimeout = 3600;
                lComando1.CommandType = CommandType.StoredProcedure;

                var dt = new DataGrid();
                lComando1.CommandText = "pa_GetInfGrafIniMesMs";
                lComando1.Parameters.Add("@P_codigo_operador", SqlDbType.Int).Value = goInfo.cod_comisionista;
                lComando1.ExecuteNonQuery();
                lsqldata1.SelectCommand = lComando1;
                lsqldata1.Fill(ldsConsumoS);
                dt.DataSource = ldsConsumoS;
                dt.DataBind();

                lConexion.Cerrar();

                // Se recorre los datos de la grilla para Armar la Grafica
                foreach (DataGridItem item in dt.Items)
                {
                    if (!puntos.ContainsKey(int.Parse(item.Cells[1].Text)))
                        puntos.Add(int.Parse(item.Cells[1].Text), item.Cells[2].Text);

                    var precioConsumidor = new PrecioConsumidorVO
                    {
                        Cantidad = double.Parse(item.Cells[3].Text),
                        Codigo = int.Parse(item.Cells[1].Text),
                        Fecha = item.Cells[0].Text,
                        Punto = item.Cells[2].Text
                    };

                    lstPrecioConsumidor.Add(precioConsumidor);
                }

                /*Se arma la cabecera del arreglo para la grafica*/
                var separador = string.Empty;
                var cabecera = new StringBuilder();
                var data = new StringBuilder();

                cabecera.Append("[['Fecha',");

                //Se agrupa la data de cada punto por la fecha
                IEnumerable<IGrouping<string, PrecioConsumidorVO>> enumerable = lstPrecioConsumidor.GroupBy(g => g.Fecha);

                var head = true;
                //Se agrega cada fila agrupada por la fecha
                foreach (IGrouping<string, PrecioConsumidorVO> precioConsumidor in enumerable)
                {
                    data.Append($"{separador}['{precioConsumidor.Key}', ");
                    separador = string.Empty;
                    foreach (KeyValuePair<int, string> punto in puntos)
                    {
                        var puntoTieneCantidad = false;
                        foreach (PrecioConsumidorVO item in precioConsumidor)
                        {
                            if (punto.Key != item.Codigo) continue;
                            data.Append($"{separador} {item.Cantidad}");
                            puntoTieneCantidad = true;
                        }
                        // Si el punto no tiene ninguna cantidad se le asigna un valor de 0 
                        if (!puntoTieneCantidad)
                            data.Append($"{separador} {0}");

                        //Cabecera
                        if (head)
                            cabecera.Append($"{separador}'{punto.Value}'");
                        separador = ", ";
                    }

                    if (head)
                    {
                        cabecera.Append("],");
                        strDatos.Append(cabecera);
                        head = false;
                    }
                    data.Append("]");
                }
                data.Append("]");
                strDatos.Append(data);
                if (!strDatos.ToString().Equals("]"))
                    return strDatos.ToString();
                strDatos.Clear();
                strDatos.Append("'Null'");
                return strDatos.ToString();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
                return "[]";
            }
        }

        /// <summary>
        /// WIDGET 8
        /// Consulta gráfica de precio y cantidad 
        /// </summary>
        /// <returns></returns>
        protected string ObtenerDatos_pa_GetInfGrafDiaMs()
        {
            try
            {
                var strDatos = new StringBuilder();
                var ldsPrecioCan = new DataSet();

                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                var lComando1 = new SqlCommand();
                var lsqldata1 = new SqlDataAdapter();
                lComando1.Connection = lConexion.gObjConexion;
                lComando1.CommandTimeout = 3600;
                lComando1.CommandType = CommandType.StoredProcedure;

                var dt = new DataGrid();
                lComando1.CommandText = "pa_GetInfGrafDiaMs";
                lComando1.ExecuteNonQuery();
                lsqldata1.SelectCommand = lComando1;
                lsqldata1.Fill(ldsPrecioCan);
                dt.DataSource = ldsPrecioCan;
                dt.DataBind();

                lConexion.Cerrar();

                strDatos.Append("[['Fecha','Cantidad','Precio'],");

                // Recorro la grilla de los Datos para Armar la Grafica
                var separador = string.Empty;
                foreach (DataGridItem item in dt.Items)
                {
                    string fecha = item.Cells[0].Text;
                    var cantidad = Convert.ToDouble(item.Cells[1].Text);
                    string cantidadPrecio = item.Cells[2].Text;

                    //Arreglo de datos para pintar en la grafica de Google
                    strDatos.Append($"{separador}[");
                    strDatos.Append($"'{fecha}', {cantidad}, {cantidadPrecio}");
                    strDatos.Append("]");
                    separador = ", ";
                }
                strDatos.Append("]");
                strDatos.Replace("&nbsp;", "0");
                if (!strDatos.ToString().Equals("[['Fecha', 'Cantidad', 'Precio'],]"))
                    return strDatos.ToString();
                strDatos.Clear();
                strDatos.Append("'Null'");
                return strDatos.ToString();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
                return "[]";
            }
        }
    }
}
