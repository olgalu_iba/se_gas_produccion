﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Segas.Web.Elements;
using SIB.Global.Presentacion;

namespace WebForms.Parametros
{
    public partial class frm_parametros : Page
    {
        private InfoSessionVO goInfo;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("~/login.aspx");

            if (IsPostBack) return;

            //Titulo
            Master.Titulo = "Parametros";
            //Descripcion
            Master.DescripcionPagina = "Utice las opciones que desea parametrizar";
            //Guarda las opciones a seleccionar   
            var sb = new StringBuilder();

            List<Hashtable> listaSubmenus = DelegadaBase.Servicios.consultarMenu(goInfo, Convert.ToInt32(Request.QueryString["idParametros"]));
            foreach (Hashtable opcion in listaSubmenus)
            {

                sb.Append("<div class=\"col-lg-3\">");
                sb.Append("<div class=\"kt-portlet kt-iconbox kt-iconbox--wave\">");
                sb.Append("<div class=\"kt-portlet__body\">");
                sb.Append("<div class=\"kt-iconbox__body\">");
                sb.Append("<div class=\"kt-iconbox__icon\">");
                sb.Append("<i class=\"flaticon-doc\"style=\"font-size:30px;\"></i>");
                sb.Append("</div>");
                sb.Append("<div class=\"kt-iconbox__desc\">");

                sb.Append("<h3 class=\"kt-iconbox__title\">");
                sb.Append("<a class=\"kt-link\"href =\"" + Page.ResolveUrl(opcion["ruta"].ToString()) + "\"> " + opcion["menu"] + "</a>");
                sb.Append("</h3>");

                sb.Append("</div>");
                sb.Append("</div>");
                sb.Append("</div>");
                sb.Append("</div>");
                sb.Append("</div>");
            }

            lblMenu.Text = sb.ToString();

            //Se notifica al usuario lo que tiene que realizar en la pantalla
            Toastr.Info(this, "Selecciona el menú que deseas ver", "Recuerda"); //20200727
        }
    }
}
