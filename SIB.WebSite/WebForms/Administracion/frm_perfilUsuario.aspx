﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_perfilUsuario.aspx.cs" Inherits="Forms.Administracion.PerfilUsuario" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-container kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <asp:Label ID="lblTitle" runat="server" />
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body ">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <asp:Label Text="Código Usuario" AssociatedControlID="txtCodUsuario" runat="server" />
                                <asp:TextBox ID="txtCodUsuario" Enabled="False" class="form-control" runat="server" />
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <asp:Label Text="Nombre" AssociatedControlID="txtName" runat="server" />
                                <asp:TextBox ID="txtName" Enabled="False" class="form-control" runat="server" />
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <asp:Label Text="Login Usuario" AssociatedControlID="txtUsuario" runat="server" />
                                <asp:TextBox ID="txtUsuario" Enabled="False" class="form-control" runat="server" />
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <asp:Label Text="Grupo Usuario" AssociatedControlID="txtGrupoUsuario" runat="server" />
                                <asp:TextBox ID="txtGrupoUsuario" Enabled="False" class="form-control" runat="server" />
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <asp:Label Text="Tipo de Perfil" AssociatedControlID="lblPerfil" runat="server" />
                                <asp:TextBox ID="lblPerfil" Enabled="False" class="form-control" runat="server" />
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <asp:Label Text="Email" AssociatedControlID="txtEmail" runat="server" />
                                <asp:TextBox ID="txtEmail" Enabled="False" class="form-control" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
