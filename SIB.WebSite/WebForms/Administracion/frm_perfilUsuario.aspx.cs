﻿using SIB.Global.Presentacion;
using System;
using System.Web.UI;

namespace Forms.Administracion
{
    public partial class PerfilUsuario : Page
    {
        /// <summary>
        /// Evento donde se cargarán los valores para la página 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            //Titulo
            Master.Titulo = "Perfil de usuario";
            //Descripcion
            Master.DescripcionPagina = "Administre los datos de su cuenta";
            var goInfo = (InfoSessionVO)Session["infoSession"];
            lblTitle.Text = "Información Básica";

            txtName.Text = goInfo.nombre;
            txtCodUsuario.Text = goInfo.codigo_usuario.ToString();
            txtUsuario.Text = goInfo.Usuario;
            txtGrupoUsuario.Text = Session["NomGrupoUsuario"].ToString();
            txtEmail.Text = Session["MailUsuario"].ToString();//20210915  visulaizacon y consutlas
            lblPerfil.Text = Session["tipoPerfil"].ToString();
        }
    }
}