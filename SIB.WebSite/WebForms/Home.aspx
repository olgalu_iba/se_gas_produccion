﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PlantillaPrincipal.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" EnableEventValidation="false" Inherits="WebForms.Home" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <%--WIDGET 1 Tablero de Oportunidades Transaccionales--%>
    <script type="text/javascript">

        google.charts.load('current', { 'packages': ['table'] });
        google.charts.setOnLoadCallback(drawTable);

        function drawTable() {
            var array = <%=ObtenerDatos_pa_GetInfOporTran()%>;
            if (array === 'Null') {
                document.getElementById('nav-home-tab').style.display = "none";
                return;
            }
            var data = google.visualization.arrayToDataTable(array);

            var options = {
                showRowNumber: false, width: '100%', height: '100%', cssClassNames: { tableCell: 'small-font' }
            };

            var table = new google.visualization.Table(document.getElementById('table_div_OportunidadesTrans'));
            table.draw(data, options);
        }
    </script>

    <%--WIDGET 2 Tablero de Oportunidades Transaccionales Mercado Secundario--%>
    <script type="text/javascript">

        google.charts.load('current', { 'packages': ['table'] });
        google.charts.setOnLoadCallback(drawTable);

        function drawTable() {
            var array = <%=ObtenerDatos_pa_GetInfOporTran_S()%>;
            if (array === 'Null') {
                document.getElementById('nav-profile-tab').style.display = "none";
                return;
            }
            var data = google.visualization.arrayToDataTable(array);

            var options = {
                showRowNumber: false, width: '100%', height: '100%', cssClassNames: { tableCell: 'small-font' }
            };

            var table = new google.visualization.Table(document.getElementById('table_div_OportunidadesTrans_S'));
            table.draw(data, options);
        }
    </script>

    <%--WIDGET 3 Tablero de precios y cantidades--%>
    <script type="text/javascript">
        //TABLERO DE PRECIOS Y CANTIDADES 
        google.charts.load('current', { 'packages': ['table'] });
        google.charts.setOnLoadCallback(drawTable);

        function drawTable() {
            var array = <%=ObtenerDatos_pa_GetInfPreCant()%>;
            if (array === 'Null') {
                document.getElementById('nav-mer-primario2-tab').style.display = "none";
                return;
            }
            var data = google.visualization.arrayToDataTable(array);

            data.setColumnProperty(2, 'className', 'text-right');

            var options = {
                showRowNumber: false, width: '100%', height: '100%', cssClassNames: { tableCell: 'small-font' }
            };

            var table = new google.visualization.Table(document.getElementById('table_div'));
            table.draw(data, options);
        }

    </script>
    <style>
        .small-font {
            font-size: 12px;
            font-family: Poppins;
            /*text-align:right;*/
        }

        .cell-font {
            font-size: 12px;
            text-align: right;
        }
    </style>

    <%--WIDGET 4 Tablero de precios y cantidades Secundario--%>
    <script type="text/javascript">
        //TABLERO DE PRECIOS Y CANTIDADES 
        google.charts.load('current', { 'packages': ['table'] });
        google.charts.setOnLoadCallback(drawTable);

        function drawTable() {
            var array = <%=ObtenerDatos_pa_GetInfPreCant_S()%>;
            if (array === 'Null') {
                document.getElementById('nav-mer-sec2-tab').style.display = "none";
                return;
            }
            var data = google.visualization.arrayToDataTable(array);

            var options = {
                showRowNumber: false, width: '100%', height: '100%', cssClassNames: { tableCell: 'small-font' }
            };

            data.setColumnProperty(2, 'className', 'text-right');
            var table = new google.visualization.Table(document.getElementById('table_div_s'));

            table.draw(data, options);
        }

    </script>

    <%--WIDGET 5 tablero de operativa pendiente--%>
    <script type="text/javascript">

        google.charts.load('current', { 'packages': ['table'] });
        google.charts.setOnLoadCallback(drawTable);
        var data;
        //20220221
        function drawTable() {
            var array = <%=ObtenerDatos_pa_GetTableroControlIni()%>;
            if (array === 'Null') {
                document.getElementById('widget-5').style.display = "none";
                return;
            }
            data = google.visualization.arrayToDataTable(array);
            var str = <%=ColorEstados%>;
            var res = str.split("&");

            if (Array.isArray(res) && res.length) {
                res.forEach(myFunction);
            }

            var options = {
                showRowNumber: false, allowHtml: true, width: '100%', height: '100%', cssClassNames: { tableCell: 'small-font' }
            };

            var table = new google.visualization.Table(document.getElementById('table_div2'));
            table.draw(data, options);
            switch (array.toString().substr(19, 23)) {
                case 'Cantidad Energía Inyect':
                    google.visualization.events.addListener(table, 'select', function (e) {
                        window.location.href = '../Verificacion/frm_CapacidadInyectada.aspx';
                    }
                    );
                    break;
                case 'Cantidad de energía cam':
                    google.visualization.events.addListener(table, 'select', function (e) {
                        window.location.href = '../Verificacion/frm_EnergiaConsNal.aspx';
                    }
                    );
                    break;
                case 'Cantidad Energía a Expo':
                    google.visualization.events.addListener(table, 'select', function (e) {
                        window.location.href = '../Verificacion/frm_EnergiaExp.aspx';
                    }
                    );
                    break;
                case 'Cantidad Energía de Par':
                    google.visualization.events.addListener(table, 'select', function (e) {
                        window.location.href = '../Verificacion/frm_energiaParqueo.aspx';
                    }
                    );
                    break;
                case 'Cantidad Energía Recibi':
                    google.visualization.events.addListener(table, 'select', function (e) {
                        window.location.href = '../Verificacion/frm_EnergiaRecibida.aspx';
                    }
                    );
                    break;
                case 'Cantidad Energía a Sumi':
                    google.visualization.events.addListener(table, 'select', function (e) {
                        window.location.href = '../Verificacion/frm_EnergiaSum.aspx';
                    }
                    );
                    break;
                case 'Cantidad Energía Tomada':
                    google.visualization.events.addListener(table, 'select', function (e) {
                        window.location.href = '../Verificacion/frm_EnergiaTomada.aspx';
                    }
                    );
                    break;
                case 'Cantidad Energía tomada':
                    google.visualization.events.addListener(table, 'select', function (e) {
                        window.location.href = '../Verificacion/frm_EnergiAsignacion.aspx';
                    }
                    );
                    break;
                case 'Cantidad Energía Transf':
                    google.visualization.events.addListener(table, 'select', function (e) {
                        window.location.href = '../Verificacion/frm_energiaTrasferida.aspx';
                    }
                    );
                    break;
                case 'Cantidad Energía Autori':
                    google.visualization.events.addListener(table, 'select', function (e) {
                        window.location.href = '../Verificacion/frm_energiaTrasportar.aspx';
                    }
                    );
                    break;
                case 'Entrega a usuarios fina':
                    google.visualization.events.addListener(table, 'select', function (e) {
                        window.location.href = '../Verificacion/frm_EntregaUsuarioFinal.aspx';
                    }
                    );
                    break;
                case 'Cantidad Energía entreg':
                    switch (array.toString().substr(19, 30)) {
                        case 'Cantidad Energía entregada GNC':
                            google.visualization.events.addListener(table, 'select', function (e) {
                                window.location.href = '../Verificacion/frm_EnergiaGnc.aspx';
                            }
                            );
                            break;
                        case 'Cantidad Energía entregada a g':
                            google.visualization.events.addListener(table, 'select', function (e) {
                                window.location.href = '../Verificacion/frm_EnergiaGasoducto.aspx';
                            }
                            );
                    }
                    break;
                case 'Cantidad Energía estaci':
                    google.visualization.events.addListener(table, 'select', function (e) {
                        window.location.href = '../Verificacion/frm_EnergiaEstacion.aspx';
                    }
                    );
                    break;
            }
            //if (array.toString().substr(13, 5).localeCompare('Fecha') ==0) {
            //    google.visualization.events.addListener(table, 'select', function (e) {
            //        window.location.href ='Contratos/frm_contrato.aspx?idInf=981';
            //    }
            //    );
            //}

        }

        function myFunction(item, index) {
            var color = '';
            if (item === 'Rojo')
                color = 'background-color: #f4c3b7;';
            if (item === 'Amarillo')
                color = 'background-color: #ffe0b2;';
            if (item === 'Verde')
                color = 'background-color: #b7dfb9;';

            data.setProperty(index, 0, 'style', color);
            data.setProperty(index, 1, 'style', color);
            // data.setProperty(index, 2, 'style', color); //20220221
        }
    </script>


    //20220221
    <%--WIDGET 5A tablero de operativa pendiente--%>
    <script type="text/javascript">

        google.charts.load('current', { 'packages': ['table'] });
        google.charts.setOnLoadCallback(drawTable);
        var data;
        
        function drawTable() {
            var array = <%=ObtenerDatos_pa_GetTableroControlContIni()%>;
            if (array === 'Null') {
                document.getElementById('widget-5A').style.display = "none";
                return;
            }
            data = google.visualization.arrayToDataTable(array);
            var str = <%=ColorEstados%>;
            var res = str.split("&");

            if (Array.isArray(res) && res.length) {
                res.forEach(myFunction);
            }

            var options = {
                showRowNumber: false, allowHtml: true, width: '100%', height: '100%', cssClassNames: { tableCell: 'small-font' }
            };

            var table = new google.visualization.Table(document.getElementById('table_div2A'));
            table.draw(data, options);
            if (array.toString().substr(0, 9).localeCompare('Operación') ==0) {
                google.visualization.events.addListener(table, 'select', function (e) {
                    window.location.href ='Contratos/frm_contrato.aspx?idInf=981';
                }
                );
            }

        }

        function myFunction(item, index) {
            var color = '';
            if (item === 'Rojo')
                color = 'background-color: #f4c3b7;';
            if (item === 'Amarillo')
                color = 'background-color: #ffe0b2;';
            if (item === 'Verde')
                color = 'background-color: #b7dfb9;';

            data.setProperty(index, 0, 'style', color);
            data.setProperty(index, 1, 'style', color);
            // data.setProperty(index, 2, 'style', color); //20220221
        }
    </script>


    <%--WIDGET 6 CONTRATACION POR SECTOR CONSUMO--%>
    <script type="text/javascript">
        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(drawChartArea);

        function drawChartArea() {
            var array = <%=ObtenerDatos_pa_GetInfGrafIniMes()%>;
            if (array === 'Null') {
                document.getElementById('widget-6').style.display = "none";
                return;
            }
            var data = google.visualization.arrayToDataTable(array);

            var options = {
                vAxis: { title: 'MBTUD', titleTextStyle: { color: '#333' } },
                //hAxis: { titleTextStyle: { color: '#333' } },
                chartArea: { color: '#fff' },
                width: 600,
                height: 300,
                backgroundColor: '#fff',
                connectSteps: false,
                //lineWidth: 3,
                legend: { position: 'top' },
                isStacked: true
            };

            var chart = new google.visualization.SteppedAreaChart(document.getElementById('chartArea_div_consumo'));
            chart.draw(data, options);
        }
    </script>

    <%--WIDGET 7 Contratacion Propia Mercado Secundario--%>
    <script type="text/javascript">
        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(drawChartArea);

        function drawChartArea() {
            var array = <%=ObtenerDatos_pa_GetInfGrafIniMesMs()%>;
            if (array === 'Null') {
                document.getElementById('widget-7').style.display = "none";
                return;
            }
            var data = google.visualization.arrayToDataTable(array);

            var options = {
                //title: 'Compras Mercado Secundario gas por punto de entrega - Todas las Modalidades',
                vAxis: { title: 'MBTUD', titleTextStyle: { color: '#333' } },
                //hAxis: { title: 'Fecha', titleTextStyle: { color: '#333' } },
                chartArea: { color: '#fff' },
                width: 600,
                height: 300,
                backgroundColor: '#fff',
                legend: { position: 'top' },
                connectSteps: false,
                isStacked: true
            };

            var chart = new google.visualization.SteppedAreaChart(document.getElementById('chartArea_Precio_contratacion'));
            chart.draw(data, options);
        }
    </script>

    <%--WIDGET 8 GRAFICO DE PRECIO Y CANTIDAD--%>
    <script type="text/javascript">
        google.charts.load('current', { 'packages': ['corechart', 'scatter'] });
        google.charts.setOnLoadCallback(drawStuff);

        function drawStuff() {
            var array = <%=ObtenerDatos_pa_GetInfGrafDiaMs()%>;
            if (array === 'Null') {
                document.getElementById('widget-8').style.display = "none";
                return;
            }
            var data = google.visualization.arrayToDataTable(array);

            var classicOptions = {

                //title: 'Gráfico de precio y cantidad',
                width: 600,
                height: 300,
                series: {

                    0: { targetAxisIndex: 0, type: 'bars' },
                    1: { targetAxisIndex: 1, type: 'line' }
                },
                lineWidth: 3,
                legend: { position: 'top' },
                vAxes: {
                    0: { title: 'Cantidad contratada MBTUD' },
                    1: { title: 'Precio USD/MBTU' }
                }
            };

            function drawClassicChart() {
                var chart = new google.visualization.ComboChart(document.getElementById('chartArea_div_consumo_s'));
                chart.draw(data, classicOptions);
            }
            drawClassicChart();
        };

    </script>

    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <!--Begin::Row-->
        <div class="row">
            <!--begin:: Widgets/Quick Stats-->
            <%--widget 1--%>
            <div class="col-sm-12 col-md-12 col-lg-6">
                <div class="kt-portlet kt-portlet--border-bottom-brand">
                    <div class="kt-portlet__body kt-portlet__body--fluid" style="height: 400px; flex-grow: 0">
                        <div class="kt-widget26">
                            <div class="kt-widget26__content" style="width: 100%; flex-grow: 0">
                                <span class="kt-widget26__number">Mecanismo de Comercialización</span>
                                <span class="kt-widget26__desc">Oportunidades transaccionales</span>
                            </div>
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-mercado-primario" role="tab" aria-controls="nav-home" aria-selected="true">Mercado Primario</a>
                                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-mercado-secundario" role="tab" aria-controls="nav-profile" aria-selected="false">Mercado Secundario</a>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-mercado-primario" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <div id="table_div_OportunidadesTrans" runat="server" enableviewstate="true" visible="true" clientidmode="Static"></div>
                                </div>
                                <div class="tab-pane fade" id="nav-mercado-secundario" role="tabpanel" aria-labelledby="nav-profile-tab">
                                    <div id="table_div_OportunidadesTrans_S" runat="server" enableviewstate="true" visible="true" clientidmode="Static"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-space-20"></div>
            </div>
            <%--widget 3 y 4--%>
            <div id="widget-3" class="col-sm-12 col-md-12 col-lg-6">
                <div class="kt-portlet kt-portlet--border-bottom-brand">
                    <div class="kt-portlet__body kt-portlet__body--fluid" style="height: 400px; flex-grow: 0">
                        <div class="kt-widget26">
                            <div class="kt-widget26__content" style="width: 100%; flex-grow: 0">
                                <span class="kt-widget26__number">Precios de Referencia</span>
                                <span class="kt-widget26__desc">Precio promedio ponderado por mecanismo</span>
                            </div>
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab2" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-mer-primario2-tab" data-toggle="tab" href="#nav-mercado-primario2" role="tab" aria-controls="nav-home" aria-selected="true">Mercado Primario</a>
                                    <a class="nav-item nav-link" id="nav-mer-sec2-tab" data-toggle="tab" href="#nav-mercado-secundario2" role="tab" aria-controls="nav-profile" aria-selected="false">Mercado Secundario</a>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent2">
                                <div class="tab-pane fade show active" id="nav-mercado-primario2" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <div id="table_div" runat="server" enableviewstate="true" visible="true" clientidmode="Static"></div>
                                </div>
                                <div class="tab-pane fade" id="nav-mercado-secundario2" role="tabpanel" aria-labelledby="nav-profile-tab">
                                    <div id="table_div_s" runat="server" enableviewstate="true" visible="true" clientidmode="Static"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-space-20"></div>
            </div>
            <%--widget 5--%><%--20220221--%>
            <%--<div id="widget-5" class="col-sm-12 col-md-12 col-lg-6">
                <div class="kt-portlet kt-portlet--border-bottom-brand">
                    <div class="kt-portlet__body kt-portlet__body--fluid" style="height: 425px; flex-grow: 0">
                        <div class="kt-widget26">
                            <div class="kt-widget26__content" style="width: 100%; flex-grow: 0">
                                <span class="kt-widget26__number">Tablero de Control</span>
                                <span class="kt-widget26__desc">Tareas pendientes</span>
                            </div>
                            <div id="table_div2" runat="server" enableviewstate="true" visible="true" clientidmode="Static"></div>
                        </div>
                    </div>
                </div>
                <div class="kt-space-20"></div>
            </div>--%>
            <%--20220221--%><%--widget 5--%>
            <div id="widget-5" class="col-sm-12 col-md-12 col-lg-6">
                <div class="kt-portlet kt-portlet--border-bottom-brand">
                    <div class="kt-portlet__body kt-portlet__body--fluid" style="height: 425px; flex-grow: 0">
                        <div class="kt-widget26">
                            <div class="kt-widget26__content" style="width: 100%; flex-grow: 0;">
                                <span class="kt-widget26__number">Tablero de control</span>
                                <span class="kt-widget26__desc">Información Operativa</span>
                            </div>
                            <div id="table_div2" runat="server" enableviewstate="true" visible="true" clientidmode="Static"></div>
                        </div>
                    </div>
                </div>
                <div class="kt-space-20"></div>
            </div>
            <%--20220221--%><%--widget 5A--%>
            <div id="widget-5A" class="col-sm-12 col-md-12 col-lg-6">
                <div class="kt-portlet kt-portlet--border-bottom-brand">
                    <div class="kt-portlet__body kt-portlet__body--fluid" style="height: 425px; flex-grow: 0">
                        <div class="kt-widget26">
                            <div class="kt-widget26__content" style="width: 100%; flex-grow: 0;">
                                <span class="kt-widget26__number">Tablero de control</span>
                                <span class="kt-widget26__desc">Información Transaccional</span>
                            </div>
                            <div id="table_div2A" runat="server" enableviewstate="true" visible="true" clientidmode="Static"></div>
                        </div>
                    </div>
                </div>
                <div class="kt-space-20"></div>
            </div>

            <%--widget 6--%>
            <div id="widget-6" class="col-sm-12 col-md-12 col-lg-6">
                <div class="kt-portlet kt-portlet--border-bottom-brand">
                    <div class="kt-portlet__body kt-portlet__body--fluid">
                        <div class="kt-widget26">
                            <div class="kt-widget26__content" style="width: 100%; flex-grow: 0">
                                <span class="kt-widget26__number">Contratación Mercado Primario</span>
                                <span class="kt-widget26__desc">Contratación por sector de consumo</span>
                            </div>
                            <div id="chartArea_div_consumo" runat="server" enableviewstate="true" visible="true" clientidmode="Static"></div>
                        </div>
                    </div>
                </div>
            </div>
            <%--widget 7--%>
            <div id="widget-7" class="col-sm-12 col-md-12 col-lg-6">
                <div class="kt-portlet kt-portlet--border-bottom-brand">
                    <div class="kt-portlet__body kt-portlet__body--fluid">
                        <div class="kt-widget26">
                            <div class="kt-widget26__content" style="width: 100%; flex-grow: 0">
                                <span class="kt-widget26__number">Contratación Mercado Secundario</span>
                                <span class="kt-widget26__desc">Contratación por punto de entrega/tramo</span>
                            </div>
                            <div id="chartArea_Precio_contratacion" runat="server" enableviewstate="true" visible="true" clientidmode="Static"></div>
                        </div>
                    </div>
                </div>
            </div>
            <%--widget 8--%>
            <div id="widget-8" class="col-sm-12 col-md-12 col-lg-6">
                <div class="kt-portlet kt-portlet--border-bottom-brand">
                    <div class="kt-portlet__body kt-portlet__body--fluid">
                        <div class="kt-widget26">
                            <div class="kt-widget26__content" style="width: 100%; flex-grow: 0">
                                <span class="kt-widget26__number">Precio Histórico y Cantidades</span>
                                <span class="kt-widget26__desc">Cantidad contratada y precio del mercado secundario</span>
                            </div>
                            <div id="chartArea_div_consumo_s" runat="server" enableviewstate="true" visible="true" clientidmode="Static"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
