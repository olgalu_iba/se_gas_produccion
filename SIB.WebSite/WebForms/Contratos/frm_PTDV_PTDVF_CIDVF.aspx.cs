﻿/*
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 * Copyright (C) 2019 Bolsa Mercantil de Colombia, Todos los Derechos Reservados
 * Archivo:               frm_PTDV_PTDVF_CIDVF.cs
 * Tipo:                  Clase principal
 * Autor:                 Oscar Piñeros
 * Fecha creación:        2019 Agosto 02
 * Fecha modificación:    
 * Propósito:             Clase que gestiona los formularios de PTDVF y CIDVF
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Presentacion;

namespace WebForms.Contratos
{
    // ReSharper disable once InconsistentNaming
    public partial class frm_PTDV_PTDVF_CIDVF : Page
    {
        #region Propiedades

        /// <summary>
        /// Propiedad donde se guardan los botones según el DropDownList
        /// </summary>
        private List<EnumBotones[]> BotonesArray
        {
            get
            {
                if (ViewState["BotonesArray"] != null)
                    return (List<EnumBotones[]>)ViewState["BotonesArray"];

                var arrayAcordion = new List<EnumBotones[]>();
                for (var i = 0; i < 8; i++)
                {
                    arrayAcordion.Add(new[] {
                        EnumBotones.Ninguno
                    });
                }
                return arrayAcordion;
            }
            set { ViewState["BotonesArray"] = value; }
        }

        #endregion Propiedades

        #region Inicializar 

        /// <summary>
        /// Evento donde se cargarán los valores para su página 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Se cargan los valores iniciales del formulario 
                InfoSessionVO infoSessionVo = CargarPagina();
                if (IsPostBack) return;
                //Se inicializan los controles para la primera ves que se ejecuta el formulario 
                InicializarPagina();
                //Se cargan las funcionalidades
                CargarSubmenus(infoSessionVo);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Inicializa los componentes del formulario 
        /// </summary>
        private InfoSessionVO CargarPagina()
        {
            //Se redirecciona al Login si no hay una sesión iniciada    
            var goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("~/login.aspx");
            // ReSharper disable once PossibleNullReferenceException
            goInfo.Programa = "Contratos";

            //Botones
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ExportarExcelOnclick += LkbExcel_Click;
            buttons.CargueOnclick += BtnCargar_Click;

            /*PTDV PTDVF y CIDVF*/

            //PTDVF y CIDVF
            ptdvfCidvf.ToastrEvent += Toastr_Event;
            ptdvfCidvf.LogCargaArchivoEvent += LogCargaArchivo_Event;
            ptdvfCidvf.ButtonsEvent += Buttons_Event;
            //Reporte Información PTDVF y CIDVF
            repoInfoPtdvfCidvf.ToastrEvent += Toastr_Event;
            repoInfoPtdvfCidvf.LogCargaArchivoEvent += LogCargaArchivo_Event;
            repoInfoPtdvfCidvf.ButtonsEvent += Buttons_Event;
            //Carga Archivo PTDV
            cargaArchivoPTDV.ToastrEvent += Toastr_Event;
            cargaArchivoPTDV.LogCargaArchivoEvent += LogCargaArchivo_Event;
            cargaArchivoPTDV.ButtonsEvent += Buttons_Event;

            //20220118
            //Carga Archivo PTDV Administrador
            cargaArchivoPTDVAdm.ToastrEvent += Toastr_Event;
            cargaArchivoPTDVAdm.LogCargaArchivoEvent += LogCargaArchivo_Event;
            cargaArchivoPTDVAdm.ButtonsEvent += Buttons_Event;

            return goInfo;
        }

        /// <summary>
        /// Inicializa el contenido del formulario 
        /// </summary>
        private void InicializarPagina()
        {
            //Titulo
            Master.Titulo = "Contratos";
            //Descripcion
            //Master.DescripcionPagina = "Capacidad";
            //Botones
            buttons.Inicializar(botones: new[] { EnumBotones.Ninguno });

            /*Registro de contratos*/
            ptdvfCidvf.NameController = ptdvfCidvf.ID;
            repoInfoPtdvfCidvf.NameController = repoInfoPtdvfCidvf.ID;
            cargaArchivoPTDV.NameController = cargaArchivoPTDV.ID;
            cargaArchivoPTDVAdm.NameController = cargaArchivoPTDVAdm.ID; //20220118
        }

        /// <summary>
        /// Carga la selección de funcionalidades según la región del acordeón 
        /// </summary>
        /// <param name="infoSessionVo"></param>
        private void CargarSubmenus(InfoSessionVO infoSessionVo)
        {
            // Se traen todos los submenús 
            List<Hashtable> listaSubmenus = DelegadaBase.Servicios.consultarMenu(infoSessionVo, Convert.ToInt32(Request.QueryString["idInf"]));

            ddlPtdCidv.Items.Clear();
            var lItem = new ListItem { Value = @"0", Text = @"Seleccione" };
            ddlPtdCidv.Items.Add(lItem);


            if (listaSubmenus.Count <= 0)
            {
                Response.Redirect("~/WebForms/Home.aspx");
                Toastr.Info(this, "Lo sentimos, pero no tiene ninguna funcionalidad configurada para este menú");
            }

            for (var index = 0; index < listaSubmenus.Count; index++)
            {
                Hashtable subSubmenus = listaSubmenus[index];
                var lItem1 = new ListItem { Value = subSubmenus["codigo_menu"].ToString(), Text = subSubmenus["menu"].ToString() };
                ddlPtdCidv.Items.Add(lItem1);
            }
        }

        #endregion Inicializar

        #region Eventos

        #region CURD Control

        /// <summary>
        /// Evento que controla el botón de consultar 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            if (ddlPtdCidv.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idRepInfPTDVFCIDVF", CultureInfo.CurrentCulture)?.ToString()))
                // Reporte Información PTDVF y CIDVF
                repoInfoPtdvfCidvf.btnConsultar_Click(sender, e);
        }

        /// <summary>
        /// Evento que controla el botón de cargar documentos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            if (ddlPtdCidv.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idPTDVFCIDVF", CultureInfo.CurrentCulture)?.ToString()))
                // PTDVF y CIDVF
                ptdvfCidvf.BtnCargar_Click(sender, e);
            else if (ddlPtdCidv.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaArchivoPTDV", CultureInfo.CurrentCulture)?.ToString()))
                // Carga Archivo PTDV
                cargaArchivoPTDV.BtnCargar_Click(sender, e);
            //20220118
            else if (ddlPtdCidv.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaArchivoPTDVAdm", CultureInfo.CurrentCulture)?.ToString()))
                // Carga Archivo PTDV
                cargaArchivoPTDVAdm.BtnCargar_Click(sender, e);
        }

        /// <summary>
        /// Evento que controla el botón de exportar a excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LkbExcel_Click(object sender, EventArgs e)
        {
            if (ddlPtdCidv.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idRepInfPTDVFCIDVF", CultureInfo.CurrentCulture)?.ToString()))
                // Reporte Información PTDVF y CIDVF
                repoInfoPtdvfCidvf.btnExcel_Click(sender, e);
        }

        /// <summary>
        /// Selecciona los botones según la carta del acordeón seleccionada
        /// </summary>
        protected void Buttons_Event(EnumBotones[] botones, DropDownList ddlSelect, string webController)
        {
            //Si no trae el nombre del Wuc quiere decir que fue seleccionado desde el formulario principal, de
            //lo contrario se selecciona el ddl según el nombre del controlador.

            if (string.IsNullOrEmpty(webController))
            {
                SaveButtons(botones, ddlSelect);
                return;
            }

            /*Registro de contratos*/
            string[] arrayController = { "cargaArchivoPTDV", "repoInfoPtdvfCidvf", "ptdvfCidvf", "cargaArchivoPTDVAdm" }; //20220118
            foreach (string usController in arrayController)
            {
                if (!usController.Equals(webController)) continue;
                SaveButtons(botones, ddlPtdCidv);
                return;
            }
        }

        /// <summary>
        /// Guarda y selecciona los botones según el formulario de contratos
        /// </summary>
        /// <param name="botones"></param>
        /// <param name="ddlSelect"></param>
        private void SaveButtons(EnumBotones[] botones, DropDownList ddlSelect)
        {
            if (botones != null)
            {
                // Se cargan los 
                EnumBotones[][] arrayFuncionalidad = BotonesArray.ToArray();
                arrayFuncionalidad[ddlSelect.SelectedIndex - 1] = botones;
                BotonesArray = arrayFuncionalidad.ToList();
            }
            else if (!ddlSelect.SelectedValue.Equals("0"))
            {
                botones = BotonesArray.ToArray()[ddlSelect.SelectedIndex - 1];
            }
            else
            {
                EnumBotones[] ninguno = { EnumBotones.Ninguno };
                botones = ninguno;
            }
            buttons.SwitchOnButton(botones);
        }

        #region  Card DropDownList

        /// <summary>
        /// Selecciona la pantalla que desea visualizar en el PTDVF y CIDVF
        /// </summary>
        protected void ddlPtdCidv_SelectedIndexChanged(object sender, EventArgs e)
        {
            ptdvfCidvf.Visible = false;
            repoInfoPtdvfCidvf.Visible = false;
            cargaArchivoPTDV.Visible = false;
            cargaArchivoPTDVAdm.Visible = false; //20220118

            // PTDVF y CIDVF
            if (ddlPtdCidv.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idPTDVFCIDVF", CultureInfo.CurrentCulture)?.ToString()))
            {
                ptdvfCidvf.Visible = true;
                // Se inicializa el formulario
                if (!ptdvfCidvf.Inicializado)
                    ptdvfCidvf.InicializarFormulario();
            }
            // Reporte Información PTDVF y CIDVF
            if (ddlPtdCidv.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idRepInfPTDVFCIDVF", CultureInfo.CurrentCulture)?.ToString()))
            {
                repoInfoPtdvfCidvf.Visible = true;
                // Se inicializa el formulario
                if (!repoInfoPtdvfCidvf.Inicializado)
                    repoInfoPtdvfCidvf.InicializarFormulario();
            }
            // Carga Archivo PTDV
            else if (ddlPtdCidv.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaArchivoPTDV", CultureInfo.CurrentCulture)?.ToString()))
            {
                cargaArchivoPTDV.Visible = true;
                // Se inicializa el formulario
                if (!cargaArchivoPTDV.Inicializado)
                    cargaArchivoPTDV.InicializarFormulario();
            }
            //20220118
            else if (ddlPtdCidv.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaArchivoPTDVAdm", CultureInfo.CurrentCulture)?.ToString()))
            {
                cargaArchivoPTDVAdm.Visible = true;
                // Se inicializa el formulario
                if (!cargaArchivoPTDVAdm.Inicializado)
                    cargaArchivoPTDVAdm.InicializarFormulario();
            }

            //Se seleccionan los botones   
            Buttons_Event(null, ddlPtdCidv, null);
        }

        #endregion Card DropDownList

        #endregion CURD Control

        /// <summary>
        /// Controla los Toastrs
        /// </summary>
        protected void Toastr_Event(string message, EnumTypeToastr typeToastr)
        {
            Toastr.SelectToastr(this, message, null, typeToastr);
        }

        /// <summary>
        /// Controla el log de errores de un cargue por archivo    
        /// </summary>
        protected void LogCargaArchivo_Event(StringBuilder message)
        {
            LogCargaArchivo.DownloadBugsLog(this, message);
        }

        #endregion Eventos
    }
}