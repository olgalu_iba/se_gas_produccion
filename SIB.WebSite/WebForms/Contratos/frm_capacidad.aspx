﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_capacidad.aspx.cs" Inherits="WebForms.Contratos.frm_capacidad" MasterPageFile="~/PlantillaPrincipal.master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitle" Text="Gestión de Capacidad" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="form-group">
                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla.</p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="form-group">
                            <label>Capacidad</label>
                            <asp:DropDownList ID="ddlCapacidad" Style="z-index: 10000" runat="server" CssClass="form-control selectpicker" OnSelectedIndexChanged="ddlCapacidad_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Capacidad Disponible Primaria"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Capacidad Máxima Mediano Plazo"></asp:ListItem>
                                <asp:ListItem Value="3" Text="Informe Capacidad Disponible Primaria"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <%--Capacidad Disponible Primaria--%>
                <segas:CapacidadDisponiblePrimaria ID="capacidadDisponiblePrimaria" runat="server" Visible="False" />
                <%--Capacidad Máxima Mediano Plazo--%>
                <segas:CapaMaxiMediPlazo ID="capaMaxiMediPlazo" runat="server" Visible="False" />
                <%--Informe Capacidad Disponible Primaria--%>
                <segas:InfoCapaDispoPrim ID="infoCapaDispoPrim" runat="server" Visible="False" />
            </div>
        </div>
    </div>

</asp:Content>
