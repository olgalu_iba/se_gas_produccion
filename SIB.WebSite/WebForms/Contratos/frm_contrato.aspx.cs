﻿/*
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 * Copyright (C) 2019 Bolsa Mercantil de Colombia, Todos los Derechos Reservados
 * Archivo:               frm_contrato.cs
 * Tipo:                  Clase principal
 * Autor:                 Oscar Piñeros
 * Fecha creación:        2019 Agosto 02
 * Fecha modificación:    2020 Marzo 13
 * Propósito:             Clase que gestiona los formularios de contratos
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Presentacion;

namespace WebForms.Contratos
{
    /// <summary>
    /// 
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public partial class frm_contrato : Page
    {
        #region Propiedades

        /// <summary>
        /// Propiedad que almacena el número del acordeón que el usuario selecciono
        /// </summary>
        public string Pestania
        {
            get { return ViewState["Pestania"] != null && !string.IsNullOrEmpty(ViewState["Pestania"].ToString()) ? ViewState["Pestania"].ToString() : "1"; }
            set { ViewState["Pestania"] = value; }
        }

        /// <summary>
        /// Propiedad que almacena el código de identificación del controlador que el usuario esta visualizando
        /// </summary>
        private string Show
        {
            get { return ViewState["Show"] != null && !string.IsNullOrEmpty(ViewState["Show"].ToString()) ? ViewState["Show"].ToString() : string.Empty; }
            set { ViewState["Show"] = value; }
        }

        /// <summary>
        /// Propiedad que contiene el arreglo de códigos de identificación que se va a enviar al frontend
        /// </summary>
        private string Collapse
        {
            get { return ViewState["Collapse"] != null && !string.IsNullOrEmpty(ViewState["Collapse"].ToString()) ? ViewState["Collapse"].ToString() : "['#collapseRegCon', '#collapseRegConExt', '#collapseModCon', '#collapseModContraCesion', '#collapseActPreInd', '#collapseSolEliCon', '#collapseProyeDemReg', '#collapseResMerReg']"; }
        }

        /// <summary>
        /// Propiedad que especifica si la sección del acordeón seleccionada se expande o se contrae
        /// </summary>
        private string RemoveCollapse
        {
            get { return ViewState["removeCollapse"] != null && !string.IsNullOrEmpty(ViewState["removeCollapse"].ToString()) ? ViewState["removeCollapse"].ToString() : "true"; }
            set { ViewState["removeCollapse"] = value; }
        }

        /// <summary>
        /// Propiedad que establece si es la primera ves que se carga el control y se abre la primera sección del acordeón
        /// </summary>
        private bool Carga
        {
            get { return (bool?)ViewState["Carga"] ?? true; }
            set { ViewState["Carga"] = value; }
        }

        /// <summary>
        /// Propiedad donde se guardan los botones según la pantalla del acordeón seleccionada 
        /// </summary>
        private List<List<ContractButtonVO>> BotonesArray
        {
            get
            {
                if (ViewState["BotonesArray"] != null)
                    return (List<List<ContractButtonVO>>)ViewState["BotonesArray"];

                var arrayAcordion = new List<List<ContractButtonVO>>();
                for (var i = 0; i < 8; i++)
                {
                    var lstFuncionalidad = new List<ContractButtonVO>();
                    for (var j = 0; j < 6; j++)
                    {
                        EnumBotones[] botones = { EnumBotones.Ninguno };
                        var buttonVo = new ContractButtonVO { Botones = botones };
                        lstFuncionalidad.Add(buttonVo);
                    }
                    arrayAcordion.Add(lstFuncionalidad);
                }
                return arrayAcordion;
            }
            set { ViewState["BotonesArray"] = value; }
        }

        #endregion Propiedades

        #region Inicializar 

        /// <summary>
        /// Evento donde se cargarán los valores para su página 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Se cargan los valores iniciales del formulario 
                InfoSessionVO infoSessionVo = CargarPagina();
                if (IsPostBack) return;
                //Se inicializan los controles para la primera ves que se ejecuta el formulario 
                InicializarPagina();
                // Se carga el acordeón según los permisos del usuario 
                CargarAcordeon(infoSessionVo);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Inicializa los componentes del formulario 
        /// </summary>
        private InfoSessionVO CargarPagina()
        {
            //Se redirecciona al Login si no hay una sesión iniciada    
            var goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("~/login.aspx");
            // ReSharper disable once PossibleNullReferenceException
            goInfo.Programa = "Contratos";

            //Botones
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ActualizarOnclick += btnActualizar_Click;
            buttons.AprobarOnclick += btnAprobar_Click;
            buttons.RechazarOnclick += brnRechazar_Click;
            buttons.CrearOnclick += CrearOnclick_Click;
            buttons.ExportarExcelOnclick += LkbExcel_Click;
            buttons.ExportarPdfOnclick += LkbPDF_Click;
            buttons.CargueOnclick += BtnCargar_Click;
            buttons.SolicitudEliminacionOnclick += BrnSolEli_Click;

            /*Registro de contratos*/

            //Registro de contratos
            regContrato.ToastrEvent += Toastr_Event;
            regContrato.ModalEvent += Modal_Event;
            regContrato.ButtonsEvent += Buttons_Event;
            //Ingreso Contrato Bilateral
            contratoBilateral.ToastrEvent += Toastr_Event;
            contratoBilateral.ModalEvent += Modal_Event;
            contratoBilateral.ButtonsEvent += Buttons_Event;
            //Archivo Negociacion Bilateral
            cargaArchiNegBila.ToastrEvent += Toastr_Event;
            cargaArchiNegBila.ModalEvent += Modal_Event;
            cargaArchiNegBila.LogCargaArchivoEvent += LogCargaArchivo_Event;
            cargaArchiNegBila.ButtonsEvent += Buttons_Event;
            //Registro de contratos de reserva PTDVF
            regContResPTDVF.ToastrEvent += Toastr_Event;
            regContResPTDVF.ModalEvent += Modal_Event;
            regContResPTDVF.ButtonsEvent += Buttons_Event;
            //Inf Transaccional Gestor
            infTransaccionalGestor.ToastrEvent += Toastr_Event;
            infTransaccionalGestor.ModalEvent += Modal_Event; //20210224
            infTransaccionalGestor.LogCargaArchivoEvent += LogCargaArchivo_Event;
            infTransaccionalGestor.ButtonsEvent += Buttons_Event;
            //Inf Transaccional Otras Transacciones del Mercado Mayorista
            infTransOtrasTransMercadoMayorista.ToastrEvent += Toastr_Event;
            infTransOtrasTransMercadoMayorista.ModalEvent += Modal_Event;
            infTransOtrasTransMercadoMayorista.LogCargaArchivoEvent += LogCargaArchivo_Event;
            infTransOtrasTransMercadoMayorista.ButtonsEvent += Buttons_Event;
            //Registro de contratos mayorista
            registroContratosMayorista.ToastrEvent += Toastr_Event;
            registroContratosMayorista.ModalEvent += Modal_Event;
            registroContratosMayorista.ButtonsEvent += Buttons_Event;
            //Archivo Negociaciones de Largo Plazo
            archivoNegLargoPlazo.ToastrEvent += Toastr_Event;
            archivoNegLargoPlazo.ModalEvent += Modal_Event;
            archivoNegLargoPlazo.LogCargaArchivoEvent += LogCargaArchivo_Event;
            archivoNegLargoPlazo.ButtonsEvent += Buttons_Event;

            //20200727 ajsute front-end
            //carga PDF COntrato
            cargaContratoPdf.ToastrEvent += Toastr_Event;
            cargaContratoPdf.ModalEvent += Modal_Event;
            cargaContratoPdf.ButtonsEvent += Buttons_Event;
            cargaContratoPdf.DocumentEvent += Document_Event;

            //Carga tasa de cambio
            cargaTasaCambioCont.ToastrEvent += Toastr_Event;
            cargaTasaCambioCont.LogCargaArchivoEvent += LogCargaArchivo_Event;
            cargaTasaCambioCont.ButtonsEvent += Buttons_Event;
            //20200727 fin ajsute front-end

            //20200925 bimestral C1 
            //registroMinutaC1.ToastrEvent += Toastr_Event;
            //registroMinutaC1.ModalEvent += Modal_Event;
            //registroMinutaC1.ButtonsEvent += Buttons_Event;

            // 20220505 Desistimiento contrato
            desistimientoContrato.ToastrEvent += Toastr_Event;
            desistimientoContrato.ModalEvent += Modal_Event;
            desistimientoContrato.ButtonsEvent += Buttons_Event;
            desistimientoContrato.DocumentEvent += Document_Event;

            /*Registro Contratos Extemporáneo*/

            //Carga Archivo Bilateral Extemporáneo
            cargaArchivoBilExt.ToastrEvent += Toastr_Event;
            cargaArchivoBilExt.ModalEvent += Modal_Event;
            cargaArchivoBilExt.LogCargaArchivoEvent += LogCargaArchivo_Event;
            cargaArchivoBilExt.ButtonsEvent += Buttons_Event;
            //Inf Transaccional Extemporánea
            infTransExtemp.ToastrEvent += Toastr_Event;
            infTransExtemp.ModalEvent += Modal_Event; //20210224
            infTransExtemp.LogCargaArchivoEvent += LogCargaArchivo_Event;
            infTransExtemp.ButtonsEvent += Buttons_Event;
            // 20220221 Ingreso Contrato Bilateral extemporaneo
            contratoBilateralExt.ToastrEvent += Toastr_Event;
            contratoBilateralExt.ModalEvent += Modal_Event;
            contratoBilateralExt.ButtonsEvent += Buttons_Event;
            // 20220221 REgistro de contrato extemporaneo
            registroContratoExt.ToastrEvent += Toastr_Event;
            registroContratoExt.ModalEvent += Modal_Event;
            registroContratoExt.ButtonsEvent += Buttons_Event;
            // 20220221 REgistro de contrato mayorista extemporaneo
            registroContratosMayoristaExt.ToastrEvent += Toastr_Event;
            registroContratosMayoristaExt.ModalEvent += Modal_Event;
            registroContratosMayoristaExt.ButtonsEvent += Buttons_Event;
            // 20220221 cargade contrato mayorista extemporaneo
            infTransMayExtemp.ToastrEvent += Toastr_Event;
            infTransMayExtemp.ModalEvent += Modal_Event; //20210224
            infTransMayExtemp.LogCargaArchivoEvent += LogCargaArchivo_Event;
            infTransMayExtemp.ButtonsEvent += Buttons_Event;
            /*Modificación Contratos*/

            //Modificación Contratos
            modContratos.ToastrEvent += Toastr_Event;
            modContratos.ModalEvent += Modal_Event;
            modContratos.ButtonsEvent += Buttons_Event;
            modContratos.DocumentEvent += Document_Event;
            //Carga Modificación Contratos
            cargaModContratos.ToastrEvent += Toastr_Event;
            cargaModContratos.ModalEvent += Modal_Event;
            cargaModContratos.LogCargaArchivoEvent += LogCargaArchivo_Event;
            cargaModContratos.ButtonsEvent += Buttons_Event;

            /*Modificación Contratos Cesion*/

            //Modificación Contratos Cesión
            modificaRegistroCession.ToastrEvent += Toastr_Event;
            modificaRegistroCession.ModalEvent += Modal_Event;
            modificaRegistroCession.ButtonsEvent += Buttons_Event;
            modificaRegistroCession.LogCargaArchivoEvent += LogCargaArchivo_Event;
            modificaRegistroCession.DocumentEvent += Document_Event;
            //Carga Modificación Contratos Cesión
            cargaModificaRegistroCession.ToastrEvent += Toastr_Event;
            cargaModificaRegistroCession.ModalEvent += Modal_Event;
            cargaModificaRegistroCession.ButtonsEvent += Buttons_Event;
            cargaModificaRegistroCession.LogCargaArchivoEvent += LogCargaArchivo_Event;

            /*Actualización Precios*/

            //Actualización Precios Indexados
            actPreciosIndexador.ToastrEvent += Toastr_Event;
            actPreciosIndexador.ButtonsEvent += Buttons_Event;
            actPreciosIndexador.ModalEvent += Modal_Event;

            //Carga solicitud cambio precio
            cargaSolCambioPrecio.ToastrEvent += Toastr_Event;
            cargaSolCambioPrecio.LogCargaArchivoEvent += LogCargaArchivo_Event;
            cargaSolCambioPrecio.ButtonsEvent += Buttons_Event;

            /*Eliminaciones o Modificaciones*/

            //Solicitud Eliminación Contrato
            solEliContrato.ToastrEvent += Toastr_Event;
            solEliContrato.ButtonsEvent += Buttons_Event;

            /*Proyección Demanda Regulada*/

            //Proyección Demanda Regulada
            carguePlanoProyeDemaRegu.ToastrEvent += Toastr_Event;
            carguePlanoProyeDemaRegu.ButtonsEvent += Buttons_Event;
            carguePlanoProyeDemaRegu.LogCargaArchivoEvent += LogCargaArchivo_Event;
            //Cargue Plano Proyección Demanda Regulada
            proyeDemandaRegulada.ToastrEvent += Toastr_Event;
            proyeDemandaRegulada.ButtonsEvent += Buttons_Event;
            proyeDemandaRegulada.ModalEvent += Modal_Event;

            //Se selecciona la carta del acordeón
            SelectTag(null, null);

            return goInfo;
        }

        /// <summary>
        /// Inicializa el contenido del formulario 
        /// </summary>
        private void InicializarPagina()
        {
            //Titulo
            Master.Titulo = "Contratos";
            //Descripcion
            //Master.DescripcionPagina = "Registro de contratos";
            //Botones
            buttons.Inicializar(botones: new[] { EnumBotones.Ninguno });

            /*Registro de contratos*/
            regContrato.NameController = regContrato.ID;
            contratoBilateral.NameController = contratoBilateral.ID;
            cargaArchiNegBila.NameController = cargaArchiNegBila.ID;
            regContResPTDVF.NameController = regContResPTDVF.ID;
            infTransaccionalGestor.NameController = infTransaccionalGestor.ID;
            infTransOtrasTransMercadoMayorista.NameController = infTransOtrasTransMercadoMayorista.ID;
            registroContratosMayorista.NameController = registroContratosMayorista.ID;
            archivoNegLargoPlazo.NameController = archivoNegLargoPlazo.ID;
            archivoNegLargoPlazo.NameController = archivoNegLargoPlazo.ID;
            cargaContratoPdf.NameController = cargaContratoPdf.ID; //2020727 ajsute front-end
            cargaTasaCambioCont.NameController = cargaTasaCambioCont.ID; //2020727 ajsute front-end
            //registroMinutaC1.NameController = registroMinutaC1.ID; //2020925 bimestral C1
            desistimientoContrato.NameController = desistimientoContrato.ID; //20220505 desisitmeinto

            /*Registro Contratos Extemporáneo*/
            cargaArchivoBilExt.NameController = cargaArchivoBilExt.ID;
            infTransExtemp.NameController = infTransExtemp.ID;
            contratoBilateralExt.NameController = contratoBilateralExt.ID; //20220221 bilateral extemporaneo
            registroContratoExt.NameController = registroContratoExt.ID; //20220221 bilateral extemporaneo
            registroContratosMayoristaExt.NameController = registroContratosMayoristaExt.ID; //20220221 bilateral extemporaneo
            infTransMayExtemp.NameController = infTransMayExtemp.ID;//20220221 bilateral extemporaneo

            /*Modificación Contratos*/
            modContratos.NameController = modContratos.ID;
            cargaModContratos.NameController = cargaModContratos.ID;

            /*Modificación Contratos Cesion*/
            modificaRegistroCession.NameController = modificaRegistroCession.ID;
            cargaModificaRegistroCession.NameController = cargaModificaRegistroCession.ID;

            /*Actualización Precios*/
            actPreciosIndexador.NameController = actPreciosIndexador.ID;
            cargaSolCambioPrecio.NameController = cargaSolCambioPrecio.ID;

            /*Eliminaciones o Modificaciones*/
            solEliContrato.NameController = solEliContrato.ID;

            /*Proyección Demanda Regulada*/
            carguePlanoProyeDemaRegu.NameController = carguePlanoProyeDemaRegu.ID;
            proyeDemandaRegulada.NameController = proyeDemandaRegulada.ID;
        }

        /// <summary>
        /// Carga el acordeón y trae los submenús de cada región 
        /// </summary>
        private void CargarAcordeon(InfoSessionVO infoSessionVo)
        {
            // Se traen todos los submenús 
            List<Hashtable> listaSubmenus = DelegadaBase.Servicios.consultarMenu(infoSessionVo, Convert.ToInt32(Request.QueryString["idInf"]));
            // Se consultan los submenús para cargar las regiones del acordeón  
            foreach (Hashtable acordeon in listaSubmenus)
            {
                //Registro de Contratos
                if (acordeon["codigo_menu"].ToString().Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idAcRegCont", CultureInfo.CurrentCulture)?.ToString()))
                {
                    // Se activa la franja de registro de contratos
                    acoRegContratos.Visible = true;
                    // Se cargan los submenús de la franja de registro de Contratos
                    CargarSubmenusAcordeon(infoSessionVo, acordeon, ddlRegContra);
                }
                //Registro contratos Extemporáneos
                if (acordeon["codigo_menu"].ToString().Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idAcRegContExte", CultureInfo.CurrentCulture)?.ToString()))
                {
                    // Se activa la franja de registro de contratos extemporáneos
                    acoRegContraExt.Visible = true;
                    // Se cargan los submenús de la franja de registro de contratos extemporáneos
                    CargarSubmenusAcordeon(infoSessionVo, acordeon, ddlRegContraExt);
                }
                //Modificación de Contratos
                if (acordeon["codigo_menu"].ToString().Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idAcModCont", CultureInfo.CurrentCulture)?.ToString()))
                {
                    // Se activa la franja de modificación de contratos
                    acoModContra.Visible = true;
                    // Se cargan los submenús de la franja de modificación de contratos
                    CargarSubmenusAcordeon(infoSessionVo, acordeon, ddlModContratos);
                }
                //Modificación Contratos Cesión
                if (acordeon["codigo_menu"].ToString().Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idAcModContCe", CultureInfo.CurrentCulture)?.ToString()))
                {
                    // Se activa la franja de modificación de contratos cesión
                    acoModContraCesion.Visible = true;
                    // Se cargan los submenús de la franja de modificación de contratos cesión
                    CargarSubmenusAcordeon(infoSessionVo, acordeon, ddlModContraCesion);
                }
                //Actualización Precios
                if (acordeon["codigo_menu"].ToString().Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idAcActuaPreInde", CultureInfo.CurrentCulture)?.ToString()))
                {
                    // Se activa la franja de actualización precios
                    acoActOreInde.Visible = true;
                    // Se cargan los submenús de actualización precios
                    CargarSubmenusAcordeon(infoSessionVo, acordeon, ddlActPreInd);
                }
                //Eliminaciones o Modificaciones
                if (acordeon["codigo_menu"].ToString().Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idAcEliMod", CultureInfo.CurrentCulture)?.ToString()))
                {
                    // Se activa la franja de eliminaciones o modificaciones
                    acoEliMod.Visible = true;
                    // Se cargan los submenús de eliminaciones o modificaciones
                    CargarSubmenusAcordeon(infoSessionVo, acordeon, ddlSolEliCon);
                }
                //Proyección Demanda Regulada
                if (acordeon["codigo_menu"].ToString().Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idAcProyeDemReg", CultureInfo.CurrentCulture)?.ToString()))
                {
                    // Se activa la franja de proyección demanda regulada
                    acoProDemReg.Visible = true;
                    // Se cargan los submenús de proyección demanda regulada
                    CargarSubmenusAcordeon(infoSessionVo, acordeon, ddlProyeDemReg);
                }
            }
        }

        /// <summary>
        /// Carga la selección de funcionalidades según la región del acordeón 
        /// </summary>
        /// <param name="infoSessionVo"></param>
        /// <param name="acordeon"></param>
        /// <param name="ddl"></param>
        private void CargarSubmenusAcordeon(InfoSessionVO infoSessionVo, Hashtable acordeon, DropDownList ddl)
        {
            ddl.Items.Clear();
            var lItem = new ListItem { Value = @"0", Text = @"Seleccione" };
            ddl.Items.Add(lItem);

            List<Hashtable> listaSubSubmenus = DelegadaBase.Servicios.consultarMenu(infoSessionVo, Convert.ToInt32(acordeon["codigo_menu"]));
            foreach (Hashtable subMenus in listaSubSubmenus)
            {
                var lItem1 = new ListItem { Value = subMenus["codigo_menu"].ToString(), Text = subMenus["menu"].ToString() };
                ddl.Items.Add(lItem1);
            }
        }

        #endregion Inicializar

        #region Eventos

        #region CURD Control

        /// <summary>
        /// Evento que controla el botón de consultar 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            switch (Pestania)
            {
                case "1":
                    if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idRegCont", CultureInfo.CurrentCulture)?.ToString()))
                        //Registro Contratos
                        regContrato.btnConsultar_Click(sender, e);
                    else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idIngContBilateral", CultureInfo.CurrentCulture)?.ToString()))
                        //Ingreso Contrato Bilateral
                        contratoBilateral.btnConsultar_Click(sender, e);
                    else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idRegContReservaPTDVF", CultureInfo.CurrentCulture)?.ToString()))
                        //Registro de contratos de reserva PTDVF
                        regContResPTDVF.btnConsultar_Click(sender, e);
                    else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idRegiContMayorista", CultureInfo.CurrentCulture)?.ToString()))
                        //Registro de contratos mayorista
                        registroContratosMayorista.btnConsultar_Click(sender, e);
                    //20200727 ajustes front-end
                    else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaPdfCont", CultureInfo.CurrentCulture)?.ToString()))
                        //carga pdf contrato
                        cargaContratoPdf.btnConsultar_Click(sender, e);
                    //20200925 bimestral c1
                    //else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idMinutaC1", CultureInfo.CurrentCulture)?.ToString()))
                    //    //carga pdf contrato
                    //    registroMinutaC1.btnConsultar_Click(sender, e);
                    //20220505 desistimeinto
                    else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idDesistimientoContrato", CultureInfo.CurrentCulture)?.ToString()))
                        //desistimiento
                        desistimientoContrato.btnConsultar_Click(sender, e);
                    break;
                    //20220221 registro manual
                case "2":
                    if (ddlRegContraExt.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idIngContBilateralExt", CultureInfo.CurrentCulture)?.ToString()))
                        //Ingreso Contrato Bilateral
                        contratoBilateralExt.btnConsultar_Click(sender, e);
                    else if (ddlRegContraExt.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idRegContExt", CultureInfo.CurrentCulture)?.ToString()))
                        //registro extemporaneo
                        registroContratoExt.btnConsultar_Click(sender, e);
                    else if (ddlRegContraExt.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idRegiContMayoristaExt", CultureInfo.CurrentCulture)?.ToString()))
                        //registro extemporaneo
                        registroContratosMayoristaExt.btnConsultar_Click(sender, e);
                    break;
                case "3":
                    if (ddlModContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idModContratos", CultureInfo.CurrentCulture)?.ToString()))
                        //Modificación Contratos
                        modContratos.btnConsultar_Click(sender, e);
                    break;
                case "4":
                    if (ddlModContraCesion.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idModContCesion", CultureInfo.CurrentCulture)?.ToString()))
                        //Modificación Contratos Cesión
                        modificaRegistroCession.btnConsultar_Click(sender, e);
                    break;
                case "5":
                    if (ddlActPreInd.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idActPreciosInd", CultureInfo.CurrentCulture)?.ToString()))
                        //Actualización Precios Indexado
                        actPreciosIndexador.btnConsultar_Click(sender, e);
                    break;
                case "6":
                    if (ddlSolEliCon.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idSolEliCont", CultureInfo.CurrentCulture)?.ToString()))
                        //Solicitud eliminación contrato
                        solEliContrato.btnConsultar_Click(sender, e);
                    break;
                case "7":
                    if (ddlProyeDemReg.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idProyecDemandaRegulada", CultureInfo.CurrentCulture)?.ToString()))
                        //Proyección Demanda Regulada
                        proyeDemandaRegulada.btnConsultar_Click(sender, e);
                    break;
            }
        }

        /// <summary>
        /// Evento que controla el botón de actualizar 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            switch (Pestania)
            {
                case "5":
                    if (ddlActPreInd.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idActPreciosInd", CultureInfo.CurrentCulture)?.ToString()))
                        //Actualización Precios Indexado
                        actPreciosIndexador.btnActualizar_Click(sender, e);
                    break;
            }
        }

        /// <summary>
        /// Evento que controla el botón de aprobar 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAprobar_Click(object sender, EventArgs e)
        {
            switch (Pestania)
            {
                case "5":
                    if (ddlActPreInd.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idActPreciosInd", CultureInfo.CurrentCulture)?.ToString()))
                        //Actualización Precios Indexado
                        actPreciosIndexador.btnAprobar_Click(sender, e);
                    break;
                case "6":
                    if (ddlSolEliCon.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idSolEliCont", CultureInfo.CurrentCulture)?.ToString()))
                        //Solicitud eliminación contrato
                        if (!Session["tipoPerfil"].ToString().Equals("B"))
                            solEliContrato.btnAprobRechaOpera_Click(sender, e);
                        else
                            solEliContrato.btnAprobRechaBmc_Click(sender, e);
                    break;
            }
        }

        /// <summary>
        /// Evento que controla el botón de rechazar 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void brnRechazar_Click(object sender, EventArgs e)
        {
            switch (Pestania)
            {
                case "5":
                    if (ddlActPreInd.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idActPreciosInd", CultureInfo.CurrentCulture)?.ToString()))
                        //Actualización Precios Indexado
                        actPreciosIndexador.btnRechazoMas_Click(sender, e);
                    break;
                case "6":
                    if (ddlSolEliCon.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idSolEliCont", CultureInfo.CurrentCulture)?.ToString()))
                        //Solicitud eliminación contrato
                        if (!Session["tipoPerfil"].ToString().Equals("B"))
                            solEliContrato.btnRFechazaOpera_Click(sender, e);
                        else
                            solEliContrato.btnRechzoBmc_Click(sender, e);
                    break;
            }
        }

        /// <summary>
        /// Evento que controla el botón de crear
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrearOnclick_Click(object sender, EventArgs e)
        {
            switch (Pestania)
            {
                case "1":
                    if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idIngContBilateral", CultureInfo.CurrentCulture)?.ToString()))
                        //Ingreso Contrato Bilateral
                        contratoBilateral.btnNuevo_Click(sender, e);
                    else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idRegiContMayorista", CultureInfo.CurrentCulture)?.ToString()))
                        //Registro de contratos mayorista
                        registroContratosMayorista.btnInsertar_Click(sender, e);
                    break;
                    //20220221 registro extemporaneo
                case "2":
                    if (ddlRegContraExt.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idIngContBilateralExt", CultureInfo.CurrentCulture)?.ToString()))
                        //Ingreso Contrato Bilateral extemporaneo
                        contratoBilateralExt.btnNuevo_Click(sender, e);
                    else if (ddlRegContraExt.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idRegiContMayoristaExt", CultureInfo.CurrentCulture)?.ToString()))
                        //Ingreso Contrato Bilateral extemporaneo
                        registroContratosMayoristaExt.btnInsertar_Click(sender, e);
                    break;
                case "7":
                    if (ddlProyeDemReg.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idProyecDemandaRegulada", CultureInfo.CurrentCulture)?.ToString()))
                        //Proyección Demanda Regulada
                        proyeDemandaRegulada.Nuevo();
                    break;
            }
        }

        /// <summary>
        /// Evento que controla el botón de solicitar eliminación
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BrnSolEli_Click(object sender, EventArgs e)
        {
            switch (Pestania)
            {
                case "6":
                    if (ddlSolEliCon.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idSolEliCont", CultureInfo.CurrentCulture)?.ToString()))
                        //Solicitud eliminación contrato
                        solEliContrato.btnSolicitud_Click(sender, e);
                    break;
            }
        }

        /// <summary>
        /// Evento que controla el botón de cargar de documentos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            switch (Pestania)
            {
                case "1":
                    if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaArchivoNegBilateral", CultureInfo.CurrentCulture)?.ToString()))
                        //Carga Archivo Negociacion Bilateral
                        cargaArchiNegBila.BtnCargar_Click(sender, e);
                    else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idInfTransaccionalGestor", CultureInfo.CurrentCulture)?.ToString()))
                        //Inf Transaccional Gestor
                        infTransaccionalGestor.BtnCargar_Click(sender, e);
                    else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idInfTransOtrasTransMercadoMayorista", CultureInfo.CurrentCulture)?.ToString()))
                        //Inf Transaccional Otras Transacciones del Mercado Mayorista
                        infTransOtrasTransMercadoMayorista.BtnCargar_Click(sender, e);
                    else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idArchNegLargoPlazo", CultureInfo.CurrentCulture)?.ToString()))
                        //Archivo Negociaciones de Largo Plazo
                        archivoNegLargoPlazo.BtnCargar_Click(sender, e);
                    //20200727 ajuste front-end
                    //else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaPdfCont", CultureInfo.CurrentCulture)?.ToString()))
                    //    //Archivo Negociaciones de Largo Plazo
                    //    cargaContratoPdf.BtnCargar_Click(sender, e);
                    else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaTasaCambioCont", CultureInfo.CurrentCulture)?.ToString()))
                        //Archivo tasa de cambio
                        cargaTasaCambioCont.BtnCargar_Click(sender, e);
                    break;
                case "2":
                    if (ddlRegContraExt.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idArchivoBilExtemp", CultureInfo.CurrentCulture)?.ToString()))
                        //Archivo Bilateral Extemporáneo
                        cargaArchivoBilExt.BtnCargar_Click(sender, e);
                    else if (ddlRegContraExt.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idInfTranExtemp", CultureInfo.CurrentCulture)?.ToString())) //20200924 ajsute compoent
                        //Inf Transaccional Extemporánea
                        infTransExtemp.BtnCargar_Click(sender, e);
                    //manual extemporanea
                    else if (ddlRegContraExt.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idInfTranMayExtemp", CultureInfo.CurrentCulture)?.ToString()))
                        //Inf Transaccional Extemporánea
                        infTransMayExtemp.BtnCargar_Click(sender, e);
                    break;
                case "3":
                    if (ddlModContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaModContratos", CultureInfo.CurrentCulture)?.ToString()))
                        //Carga Modificación Contratos
                        cargaModContratos.BtnCargar_Click(sender, e);
                    break;
                case "4":
                    if (ddlModContraCesion.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaModContratosCesion", CultureInfo.CurrentCulture)?.ToString()))
                        //Carga Modificación Contratos Cesión
                        cargaModificaRegistroCession.BtnCargar_Click(sender, e);
                    break;
                case "5":
                    if (ddlActPreInd.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idArchivoSolCambioPrecio", CultureInfo.CurrentCulture)?.ToString()))
                        //Archivo solicitud cambio precio
                        cargaSolCambioPrecio.BtnCargar_Click(sender, e);
                    break;
                case "7":
                    if (ddlProyeDemReg.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaPlanoProDemRegulada", CultureInfo.CurrentCulture)?.ToString()))
                        //Cargue Plano Proyección Demanda Regulada
                        carguePlanoProyeDemaRegu.BtnCargar_Click(sender, e);
                    break;
            }
        }

        /// <summary>
        /// Evento que controla el botón de exportar a excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LkbExcel_Click(object sender, EventArgs e)
        {
            switch (Pestania)
            {
                case "1":
                    if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idRegCont", CultureInfo.CurrentCulture)?.ToString()))
                        //Registro Contratos
                        regContrato.lkbExcel_Click(sender, e);
                    else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idRegContReservaPTDVF", CultureInfo.CurrentCulture)?.ToString()))
                        //Registro de contratos de reserva PTDVF
                        regContResPTDVF.lkbExcel_Click(sender, e);
                    else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idRegiContMayorista", CultureInfo.CurrentCulture)?.ToString()))
                        //Registro de contratos mayorista
                        registroContratosMayorista.lkbExcel_Click(sender, e);
                    //20200727 ajsue front-end
                    else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaPdfCont", CultureInfo.CurrentCulture)?.ToString()))
                        //Carga prdf contrato
                        cargaContratoPdf.lkbExcel_Click(sender, e);
                    //20200925 subasta bimestral C1
                    //20220505 desistimiento 
                    //else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idDesistimiento", CultureInfo.CurrentCulture)?.ToString()))
                    //    //Carga prdf contrato
                    //    desistimientoContrato.lkbExcel_Click(sender, e);
                    break;
                    //20220221 registro manula aqui
                case "2":
                    if (ddlRegContraExt.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idRegContExt", CultureInfo.CurrentCulture)?.ToString()))
                        //Modificación Contratos
                        registroContratoExt.lkbExcel_Click(sender, e);
                    else if (ddlRegContraExt.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idRegiContMayoristaExt", CultureInfo.CurrentCulture)?.ToString()))
                        //Modificación Contratos
                        registroContratosMayoristaExt.lkbExcel_Click(sender, e);
                    break;
                case "3":
                    if (ddlModContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idModContratos", CultureInfo.CurrentCulture)?.ToString()))
                        //Modificación Contratos
                        modContratos.lkbExcel_Click(sender, e);
                    break;
                case "4":
                    if (ddlModContraCesion.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idModContCesion", CultureInfo.CurrentCulture)?.ToString()))
                        //Modificación Contratos Cesión
                        modificaRegistroCession.lkbExcel_Click(sender, e);
                    break;
                case "5":
                    if (ddlActPreInd.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idActPreciosInd", CultureInfo.CurrentCulture)?.ToString()))
                        //Actualización Precios Indexado
                        actPreciosIndexador.lkbExcel_Click(sender, e);
                    break;
                case "6":
                    if (ddlSolEliCon.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idSolEliCont", CultureInfo.CurrentCulture)?.ToString()))
                        //Solicitud eliminación contrato
                        solEliContrato.lkbExcel_Click(sender, e);
                    break;
                case "7":
                    if (ddlProyeDemReg.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idProyecDemandaRegulada", CultureInfo.CurrentCulture)?.ToString()))
                        //Proyección Demanda Regulada
                        proyeDemandaRegulada.ImgExcel_Click(sender, e);
                    break;
            }
        }

        /// <summary>
        /// Evento que controla el botón de exportar a pdf
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LkbPDF_Click(object sender, EventArgs e)
        {
            switch (Pestania)
            {
                case "7":
                    if (ddlProyeDemReg.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idProyecDemandaRegulada", CultureInfo.CurrentCulture)?.ToString()))
                        //Proyección Demanda Regulada
                        proyeDemandaRegulada.ImgPdf_Click(sender, e);
                    break;
            }
        }

        /// <summary>
        /// Selecciona los botones según la carta del acordeón seleccionada
        /// </summary>
        protected void Buttons_Event(EnumBotones[] botones, DropDownList ddlSelect, string webController)
        {
            //Si no trae el nombre del Wuc quiere decir que fue seleccionado desde el formulario principal, de
            //lo contrario se selecciona el ddl según el nombre del controlador.

            if (string.IsNullOrEmpty(webController))
            {
                SaveButtons(botones, ddlSelect);
                return;
            }

            /*Registro de contratos*/
            string[] arrayController = { "regContrato", "contratoBilateral", "cargaArchiNegBila", "regContResPTDVF", "infTransaccionalGestor", "infTransOtrasTransMercadoMayorista", "registroContratosMayorista", "archivoNegLargoPlazo", "cargaContratoPdf", "cargaTasaCambioCont", "desistimientoContrato" };  //20200727 ajuste front -ens //20200925 //20220505
            if (arrayController.Any(usController => usController.Equals(webController))) 
            {
                SaveButtons(botones, ddlRegContra);
                return;
            }

            /*Registro contratos Extemporáneo*/
            arrayController = new[] { "cargaArchivoBilExt", "infTransExtemp", "contratoBilateralExt", "registroContratoExt", "registroContratosMayoristaExt", "infTransMayExtemp" }; //20220221 registro extemporaneo
            if (arrayController.Any(usController => usController.Equals(webController)))
            {
                SaveButtons(botones, ddlRegContraExt);
                return;
            }

            /*Modificación Contratos*/
            arrayController = new[] { "modContratos", "cargaModContratos" };
            if (arrayController.Any(usController => usController.Equals(webController)))
            {
                SaveButtons(botones, ddlModContratos);
                return;
            }

            /*Modificación Contratos Cesión*/
            arrayController = new[] { "modificaRegistroCession", "cargaModificaRegistroCession" };
            if (arrayController.Any(usController => usController.Equals(webController)))
            {
                SaveButtons(botones, ddlModContraCesion);
                return;
            }

            /*Actualización Precios*/
            arrayController = new[] { "actPreciosIndexador", "cargaSolCambioPrecio" };
            if (arrayController.Any(usController => usController.Equals(webController)))
            {
                SaveButtons(botones, ddlActPreInd);
                return;
            }

            /*Eliminaciones o Modificaciones*/
            arrayController = new[] { "solEliContrato" };
            if (arrayController.Any(usController => usController.Equals(webController)))
            {
                SaveButtons(botones, ddlSolEliCon);
                return;
            }

            /*Proyección Demanda Regulada*/
            arrayController = new[] { "proyeDemandaRegulada", "carguePlanoProyeDemaRegu" };
            if (arrayController.Any(usController => usController.Equals(webController)))
            {
                SaveButtons(botones, ddlProyeDemReg);
            }
        }

        /// <summary>
        /// Guarda y selecciona los botones según el formulario de contratos
        /// </summary>
        /// <param name="botones"></param>
        /// <param name="ddlSelect"></param>
        private void SaveButtons(EnumBotones[] botones, DropDownList ddlSelect)
        {
            if (botones != null)
            {
                List<List<ContractButtonVO>> arrayAcordion = BotonesArray;
                List<ContractButtonVO> arrayFuncionalidad = BotonesArray[int.Parse(Pestania) - 1];
                foreach (ContractButtonVO buttonVo in arrayFuncionalidad.Where(buttonVo => buttonVo.Codigo == 0 || buttonVo.Codigo == int.Parse(ddlSelect.SelectedValue)))
                {
                    buttonVo.Codigo = int.Parse(ddlSelect.SelectedValue);
                    buttonVo.Botones = botones;
                    break;
                }
                List<ContractButtonVO> lstFuncionalidad = arrayFuncionalidad;
                arrayAcordion[int.Parse(Pestania) - 1] = lstFuncionalidad;
                BotonesArray = arrayAcordion;
            }
            else if (!ddlSelect.SelectedValue.Equals("0"))
            {
                List<ContractButtonVO> contractButtonVos = BotonesArray[int.Parse(Pestania) - 1];
                foreach (ContractButtonVO contractButtonVo in contractButtonVos.Where(contractButtonVo => contractButtonVo.Codigo == int.Parse(ddlSelect.SelectedValue)))
                {
                    botones = contractButtonVo.Botones;
                    break;
                }
            }
            else
            {
                botones = new[] { EnumBotones.Ninguno };
            }
            buttons.SwitchOnButton(botones);
        }

        #endregion CURD Control

        #region  Card DropDownList

        /// <summary>
        /// Selecciona la pantalla que desea visualizar en el registro de contratos
        /// </summary>
        protected void ddlRegContratos_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*Se cargan los controles*/
            regContrato.Visible = false;
            contratoBilateral.Visible = false;
            cargaArchiNegBila.Visible = false;
            regContResPTDVF.Visible = false;
            infTransaccionalGestor.Visible = false;
            infTransOtrasTransMercadoMayorista.Visible = false;
            registroContratosMayorista.Visible = false;
            archivoNegLargoPlazo.Visible = false;
            cargaContratoPdf.Visible = false; //20200727 ajsute front-end
            cargaTasaCambioCont.Visible = false; //20200727 ajsute front-end
            //registroMinutaC1.Visible = false; //20200925 subata bimealtral c1
            desistimientoContrato.Visible = false; //20220505 desistimiento 

            //Registro Contratos
            if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idRegCont", CultureInfo.CurrentCulture)?.ToString()))
            {
                regContrato.Visible = true;
                // Se inicializa el formulario
                if (!regContrato.Inicializado)
                    regContrato.InicializarFormulario();
            }
            //Ingreso Contrato Bilateral
            else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idIngContBilateral", CultureInfo.CurrentCulture)?.ToString()))
            {
                contratoBilateral.Visible = true;
                // Se inicializa el formulario
                if (!contratoBilateral.Inicializado)
                    contratoBilateral.InicializarFormulario();
            }
            //Carga Archivo Negociacion Bilateral
            else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaArchivoNegBilateral", CultureInfo.CurrentCulture)?.ToString()))
            {
                cargaArchiNegBila.Visible = true;
                // Se inicializa el formulario
                if (!cargaArchiNegBila.Inicializado)
                    cargaArchiNegBila.InicializarFormulario();
            }
            //Registro de contratos de reserva PTDVF
            else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idRegContReservaPTDVF", CultureInfo.CurrentCulture)?.ToString()))
            {
                regContResPTDVF.Visible = true;
                // Se inicializa el formulario
                if (!regContResPTDVF.Inicializado)
                    regContResPTDVF.InicializarFormulario();
            }
            //Inf Transaccional Gestor
            else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idInfTransaccionalGestor", CultureInfo.CurrentCulture)?.ToString()))
            {
                infTransaccionalGestor.Visible = true;
                // Se inicializa el formulario
                if (!infTransaccionalGestor.Inicializado)
                    infTransaccionalGestor.InicializarFormulario();
            }
            //Inf Transaccional Otras Transacciones del Mercado Mayorista
            else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idInfTransOtrasTransMercadoMayorista", CultureInfo.CurrentCulture)?.ToString()))
            {
                infTransOtrasTransMercadoMayorista.Visible = true;
                // Se inicializa el formulario
                if (!infTransOtrasTransMercadoMayorista.Inicializado)
                    infTransOtrasTransMercadoMayorista.InicializarFormulario();
            }
            //Registro de contratos mayorista
            else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idRegiContMayorista", CultureInfo.CurrentCulture)?.ToString()))
            {
                registroContratosMayorista.Visible = true;
                // Se inicializa el formulario
                if (!registroContratosMayorista.Inicializado)
                    registroContratosMayorista.InicializarFormulario();
            }
            //Archivo Negociaciones de Largo Plazo
            else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idArchNegLargoPlazo", CultureInfo.CurrentCulture)?.ToString()))
            {
                archivoNegLargoPlazo.Visible = true;
                // Se inicializa el formulario
                if (!archivoNegLargoPlazo.Inicializado)
                    archivoNegLargoPlazo.InicializarFormulario();
            }
            //20200727 asjute front-end
            //carga PDf contrato
            else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaPdfCont", CultureInfo.CurrentCulture)?.ToString()))
            {
                cargaContratoPdf.Visible = true;
                // Se inicializa el formulario
                if (!cargaContratoPdf.Inicializado)
                    cargaContratoPdf.InicializarFormulario();
            }
            //20200727 asjute front-end
            //carga PDf contrato
            else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaTasaCambioCont", CultureInfo.CurrentCulture)?.ToString()))
            {
                cargaTasaCambioCont.Visible = true;
                // Se inicializa el formulario
                if (!cargaTasaCambioCont.Inicializado)
                    cargaTasaCambioCont.InicializarFormulario();
            }
            //20200925 bimestral C1
            //else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idMinutaC1", CultureInfo.CurrentCulture)?.ToString()))
            //{
            //    registroMinutaC1.Visible = true;
            //    // Se inicializa el formulario
            //    if (!registroMinutaC1.Inicializado)
            //        registroMinutaC1.InicializarFormulario();
            //}
            //20220505 desistimiento
            else if (ddlRegContra.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idDesistimientoContrato", CultureInfo.CurrentCulture)?.ToString()))
            {
                desistimientoContrato.Visible = true;
                // Se inicializa el formulario
                if (!desistimientoContrato.Inicializado)
                    desistimientoContrato.InicializarFormulario();
            }
            else if (!ddlRegContra.SelectedValue.Equals("0"))
            {
                Toastr.Error(this, "Esta funcionalidad no existe el Mosaico de Contratos");
            }

            //Se seleccionan los botones   
            Buttons_Event(null, ddlRegContra, null);
        }

        /// <summary>
        /// Selecciona la pantalla que desea visualizar en el registro de contratos Extemporáneo
        /// </summary>
        protected void ddlRegContratosExt_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargaArchivoBilExt.Visible = false;
            infTransExtemp.Visible = false;
            contratoBilateralExt.Visible = false; //20220221 registro extemporaneo 
            registroContratoExt.Visible = false;//20220221 registro extemporaneo
            registroContratosMayoristaExt.Visible = false;//20220221 registro extemporaneo
            infTransMayExtemp.Visible = false; //20220221 registro extemporane

            //Archivo Bilateral Extemporáneo
            if (ddlRegContraExt.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idArchivoBilExtemp", CultureInfo.CurrentCulture)?.ToString()))
            {
                cargaArchivoBilExt.Visible = true;
                // Se inicializa el formulario
                if (!cargaArchivoBilExt.Inicializado)
                    cargaArchivoBilExt.InicializarFormulario();
            }
            //Inf Transaccional Extemporánea
            else if (ddlRegContraExt.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idInfTranExtemp", CultureInfo.CurrentCulture)?.ToString()))
            {
                infTransExtemp.Visible = true;
                // Se inicializa el formulario
                if (!infTransExtemp.Inicializado)
                    infTransExtemp.InicializarFormulario();
            }
            //20220221 registro extemporaneo
            else if (ddlRegContraExt.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idIngContBilateralExt", CultureInfo.CurrentCulture)?.ToString()))
            {
                contratoBilateralExt.Visible = true;
                // Se inicializa el formulario
                if (!contratoBilateralExt.Inicializado)
                    contratoBilateralExt.InicializarFormulario();
            }
            //20220221 registro extemporaneo
            else if (ddlRegContraExt.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idRegContExt", CultureInfo.CurrentCulture)?.ToString()))
            {
                registroContratoExt.Visible = true;
                // Se inicializa el formulario
                if (!registroContratoExt.Inicializado)
                    registroContratoExt.InicializarFormulario();
            }
            //20220221 registro extemporaneo
            else if (ddlRegContraExt.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idRegiContMayoristaExt", CultureInfo.CurrentCulture)?.ToString()))
            {
                registroContratosMayoristaExt.Visible = true;
                // Se inicializa el formulario
                if (!registroContratosMayoristaExt.Inicializado)
                    registroContratosMayoristaExt.InicializarFormulario();
            }
            //20220221 registro extemporaneo
            else if (ddlRegContraExt.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idInfTranMayExtemp", CultureInfo.CurrentCulture)?.ToString()))
            {
                infTransMayExtemp.Visible = true;
                // Se inicializa el formulario
                if (!infTransMayExtemp.Inicializado)
                    infTransMayExtemp.InicializarFormulario();
            }
            else if (!ddlRegContraExt.SelectedValue.Equals("0"))
            {
                Toastr.Error(this, "Esta funcionalidad no existe el Mosaico de Contratos");
            }

            //Se seleccionan los botones   
            Buttons_Event(null, ddlRegContraExt, null);
        }

        /// <summary>
        /// Selecciona la pantalla que desea visualizar en la modificación de contratos
        /// </summary>
        protected void ddlModContratos_SelectedIndexChanged(object sender, EventArgs e)
        {
            modContratos.Visible = false;
            cargaModContratos.Visible = false;

            //Modificación Contratos
            if (ddlModContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idModContratos", CultureInfo.CurrentCulture)?.ToString()))
            {
                modContratos.Visible = true;
                // Se inicializa el formulario
                if (!modContratos.Inicializado)
                    modContratos.InicializarFormulario();
            }
            //Carga Modificación Contratos
            else if (ddlModContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaModContratos", CultureInfo.CurrentCulture)?.ToString()))
            {
                cargaModContratos.Visible = true;
                // Se inicializa el formulario
                if (!cargaModContratos.Inicializado)
                    cargaModContratos.InicializarFormulario();
            }
            else if (!ddlModContratos.SelectedValue.Equals("0"))
            {
                Toastr.Error(this, "Esta funcionalidad no existe el Mosaico de Contratos");
            }

            //Se seleccionan los botones   
            Buttons_Event(null, ddlModContratos, null);
        }

        /// <summary>
        /// Selecciona la pantalla que desea visualizar en la modificación de contratos cesión
        /// </summary>
        protected void ddlModContraCesion_SelectedIndexChanged(object sender, EventArgs e)
        {
            modificaRegistroCession.Visible = false;
            cargaModificaRegistroCession.Visible = false;

            //Modificación Contratos Cesión
            if (ddlModContraCesion.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idModContCesion", CultureInfo.CurrentCulture)?.ToString()))
            {
                modificaRegistroCession.Visible = true;
                // Se inicializa el formulario
                if (!modificaRegistroCession.Inicializado)
                    modificaRegistroCession.InicializarFormulario();
            }
            //Carga Modificación Contratos Cesión
            else if (ddlModContraCesion.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaModContratosCesion", CultureInfo.CurrentCulture)?.ToString()))
            {
                cargaModificaRegistroCession.Visible = true;
                // Se inicializa el formulario
                if (!cargaModificaRegistroCession.Inicializado)
                    cargaModificaRegistroCession.InicializarFormulario();
            }
            else if (!ddlModContraCesion.SelectedValue.Equals("0"))
            {
                Toastr.Error(this, "Esta funcionalidad no existe el Mosaico de Contratos");
            }

            //Se seleccionan los botones   
            Buttons_Event(null, ddlModContraCesion, null);
        }

        /// <summary>
        /// Selecciona la pantalla que desea visualizar en la actualización de precios
        /// </summary>
        protected void ddlActPreInd_SelectedIndexChanged(object sender, EventArgs e)
        {
            actPreciosIndexador.Visible = false;
            cargaSolCambioPrecio.Visible = false;

            //Modificación Contratos Cesión
            if (ddlActPreInd.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idActPreciosInd", CultureInfo.CurrentCulture)?.ToString()))
            {
                actPreciosIndexador.Visible = true;
                // Se inicializa el formulario
                if (!actPreciosIndexador.Inicializado)
                    actPreciosIndexador.InicializarFormulario();
            }
            //Archivo solicitud cambio precio
            else if (ddlActPreInd.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idArchivoSolCambioPrecio", CultureInfo.CurrentCulture)?.ToString()))
            {
                cargaSolCambioPrecio.Visible = true;
                // Se inicializa el formulario
                if (!cargaSolCambioPrecio.Inicializado)
                    cargaSolCambioPrecio.InicializarFormulario();
            }
            else if (!ddlActPreInd.SelectedValue.Equals("0"))
            {
                Toastr.Error(this, "Esta funcionalidad no existe el Mosaico de Contratos");
            }

            //Se seleccionan los botones   
            Buttons_Event(null, ddlActPreInd, null);
        }

        /// <summary>
        /// Selecciona la pantalla que desea visualizar en las eliminaciones o modificaciones
        /// </summary>
        protected void ddlSolEliCon_SelectedIndexChanged(object sender, EventArgs e)
        {
            solEliContrato.Visible = false;

            //Solicitud eliminación contrato
            if (ddlSolEliCon.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idSolEliCont", CultureInfo.CurrentCulture)?.ToString()))
            {
                solEliContrato.Visible = true;
                // Se inicializa el formulario
                if (!solEliContrato.Inicializado)
                    solEliContrato.InicializarFormulario();
            }
            else if (!ddlSolEliCon.SelectedValue.Equals("0"))
            {
                Toastr.Error(this, "Esta funcionalidad no existe el Mosaico de Contratos");
            }

            //Se seleccionan los botones   
            Buttons_Event(null, ddlSolEliCon, null);
        }

        /// <summary>
        /// Selecciona la pantalla que desea visualizar en la proyección de demanda regulada
        /// </summary>
        protected void ddlProyeDemReg_SelectedIndexChanged(object sender, EventArgs e)
        {
            carguePlanoProyeDemaRegu.Visible = false;
            proyeDemandaRegulada.Visible = false;

            //Proyección Demanda Regulada
            if (ddlProyeDemReg.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idProyecDemandaRegulada", CultureInfo.CurrentCulture)?.ToString()))
            {
                // Se inicializa el formulario
                if (!proyeDemandaRegulada.Inicializado)
                    proyeDemandaRegulada.InicializarFormulario();
                proyeDemandaRegulada.Visible = true;
            }
            //Cargue Plano Proyección Demanda Regulada
            else if (ddlProyeDemReg.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaPlanoProDemRegulada", CultureInfo.CurrentCulture)?.ToString()))
            {
                // Se inicializa el formulario
                if (!carguePlanoProyeDemaRegu.Inicializado)
                    carguePlanoProyeDemaRegu.InicializarFormulario();
                carguePlanoProyeDemaRegu.Visible = true;
            }
            else if (!ddlProyeDemReg.SelectedValue.Equals("0"))
            {
                Toastr.Error(this, "Esta funcionalidad no existe el Mosaico de Contratos");
            }

            //Se seleccionan los botones   
            Buttons_Event(null, ddlProyeDemReg, null);
        }

        #endregion Card DropDownList

        /// <summary>
        /// Controla los Toastrs
        /// </summary>
        protected void Toastr_Event(string message, EnumTypeToastr typeToastr)
        {
            Toastr.SelectToastr(this, message, null, typeToastr);
        }

        /// <summary>
        /// Controla el log de errores de un cargue por archivo    
        /// </summary>
        protected void LogCargaArchivo_Event(StringBuilder message)
        {
            NotificacionCargaArchivo.NotificacionCargueExitoso(this, message);
            LogCargaArchivo.DownloadBugsLog(this, message);
        }

        /// <summary>
        /// Controla la apertura de los Modals
        /// </summary>
        /// <param name="id"></param>
        /// <param name="insideId"></param>
        /// <param name="typeModal"></param>
        /// <param name="size"></param>
        protected void Modal_Event(string id, string insideId, EnumTypeModal typeModal, Modal.Size size = Modal.Size.Large)
        {
            Modal.SelectModal(this, id, insideId, typeModal, size);
        }

        /// <summary>
        /// Controla la apertura de una nueva ventana 
        /// </summary>
        /// <param name="pathDocument"></param>
        protected void Document_Event(string pathDocument)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "StartupAlert", "window.open('" + pathDocument + "');", true);
        }

        /// <summary>
        /// Guarda la pestaña para el manejo de los botones del CRUD Control y el PostBack 
        /// </summary>
        protected void SelectTag(object sender, EventArgs e)
        {
            var arrayCollapseId = new[] { "collapseRegCon", "collapseRegConExt", "collapseModCon", "collapseModContraCesion", "collapseActPreInd", "collapseSolEliCon", "collapseProyeDemReg", "collapseResMerReg" };
            if (Carga)
                Show = arrayCollapseId[0];

            if (sender != null)
            {
                var clickedButton = (Button)sender;
                Carga = true;

                switch (clickedButton.ID)
                {
                    case "btnRegContra":
                        if (Show.Equals(arrayCollapseId[0]))
                        {
                            RemoveCollapse = "true";
                            Show = string.Empty;
                            break;
                        }
                        Pestania = "1";
                        Show = arrayCollapseId[0];
                        //Se seleccionan los botones
                        SaveButtons(null, ddlRegContra);
                        break;
                    case "btnRegContraExt":
                        if (Show.Equals(arrayCollapseId[1]))
                        {
                            RemoveCollapse = "true";
                            Show = string.Empty;
                            break;
                        }
                        Pestania = "2";
                        Show = arrayCollapseId[1];
                        //Se seleccionan los botones
                        SaveButtons(null, ddlRegContraExt);
                        break;
                    case "btnModContratos":
                        if (Show.Equals(arrayCollapseId[2]))
                        {
                            RemoveCollapse = "true";
                            Show = string.Empty;
                            break;
                        }
                        Pestania = "3";
                        Show = arrayCollapseId[2];
                        //Se seleccionan los botones
                        SaveButtons(null, ddlModContratos);
                        break;
                    case "btnModContraCesion":
                        if (Show.Equals(arrayCollapseId[3]))
                        {
                            RemoveCollapse = "true";
                            Show = string.Empty;
                            break;
                        }
                        Pestania = "4";
                        Show = arrayCollapseId[3];
                        //Se seleccionan los botones
                        SaveButtons(null, ddlModContraCesion);
                        break;
                    case "btnActPreInd":
                        if (Show.Equals(arrayCollapseId[4]))
                        {
                            RemoveCollapse = "true";
                            Show = string.Empty;
                            break;
                        }
                        Pestania = "5";
                        Show = arrayCollapseId[4];
                        //Se seleccionan los botones
                        SaveButtons(null, ddlActPreInd);
                        break;
                    case "btnSolEliCon":
                        if (Show.Equals(arrayCollapseId[5]))
                        {
                            RemoveCollapse = "true";
                            Show = string.Empty;
                            break;
                        }
                        Pestania = "6";
                        Show = arrayCollapseId[5];
                        //Se seleccionan los botones
                        SaveButtons(null, ddlSolEliCon);
                        break;
                    case "btnProyeDemReg":
                        if (Show.Equals(arrayCollapseId[6]))
                        {
                            RemoveCollapse = "true";
                            Show = string.Empty;
                            break;
                        }
                        Pestania = "7";
                        Show = arrayCollapseId[6];
                        //Se seleccionan los botones
                        SaveButtons(null, ddlProyeDemReg);
                        break;
                }

                //Se selecciona la carta del acordeón
                if (RemoveCollapse.Equals("false") && !string.IsNullOrEmpty(Show))
                    RemoveCollapse = "true";
                else if (RemoveCollapse.Equals("true") && string.IsNullOrEmpty(Show))
                    RemoveCollapse = "false";
            }

            // Se selecciona o contrae la sección del acordeón correspondiente
            Accordion.Seleccionar(this, Show, Collapse, RemoveCollapse);
            Carga = false;
        }

        #endregion Eventos
    }
}