﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_contrato.aspx.cs" Inherits="WebForms.Contratos.frm_contrato" MasterPageFile="~/PlantillaPrincipal.master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" enctype="multipart/form-data" method="post" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitle" Text="Gestión de Contratos" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <asp:UpdatePanel ID="UdpButtons" style="margin-top: 1%" runat="server">
                    <ContentTemplate>
                        <segas:CrudButton ID="buttons" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="accordion" id="accordionRgCon">
                    <%--Registro Contratos--%>
                    <div id="acoRegContratos" class="card" visible="False" runat="server">
                        <div class="card-header" id="headingRegCon">
                            <h2 class="mb-0">
                                <asp:Button ID="btnRegContra" Text="Registros de Contratos" CssClass="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseRegCon" aria-expanded="true" aria-controls="collapseRegCon" OnClick="SelectTag" runat="server" />
                            </h2>
                        </div>
                        <div id="collapseRegCon" class="collapse" aria-labelledby="headingRegCon" data-parent="#accordionRgCon">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group">
                                            <%--20210707--%>
                                            <p>A través de este módulo podrá declarar la información de los contratos de suministro de gas natural  y capacidad de transporte negociados en el mercado mayorista, de acuerdo con lo establecido en las Resoluciones CREG 185 y 186 de 2020 y todas aquellas que la modifiquen, adicionen o sustituyan.</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group">
                                            <label>Funcionalidad</label>
                                            <asp:DropDownList ID="ddlRegContra" runat="server" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="ddlRegContratos_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Registro de Contratos"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Ingreso Contrato Bilateral"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="Archivo Negociación Bilateral"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="Información Transaccional Gestor"></asp:ListItem>
                                                <asp:ListItem Value="5" Text="Registro de contratos de reserva PTDVF/CIDVF"></asp:ListItem>
                                                <asp:ListItem Value="6" Text="Información Transaccional Otras Transacciones del Mercado Mayorista"></asp:ListItem>
                                                <asp:ListItem Value="7" Text="Registro de Contratos Mayorista"></asp:ListItem>
                                                <asp:ListItem Value="8" Text="Archivo Negociaciones de Largo Plazo"></asp:ListItem>
                                                <asp:ListItem Value="9" Text="Carga PDF Contratos"></asp:ListItem> <%--20200727 ajsute front-end--%>
                                                <asp:ListItem Value="10" Text="Carga Tasa de Cambio Contratos"></asp:ListItem> <%--20200727 ajsute front-end--%>
                                                <%--<asp:ListItem Value="11" Text="Registro minuta C1 Bimetral"></asp:ListItem>--%> <%--2020925 bimestral c1--%>
                                                <asp:ListItem Value="12" Text="Desistimiento Contrato"></asp:ListItem> <%--20220505 desistimiento--%>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <%--Registro Contratos--%>
                                <segas:RegistroContrato ID="regContrato" runat="server" Visible="False" />
                                <%--Ingreso Contrato Bilateral--%>
                                <segas:ContratoBilateral ID="contratoBilateral" runat="server" Visible="False" />
                                <%--Carga Archivo Negociacion Bilateral--%>
                                <segas:CargaArchivoNegBila ID="cargaArchiNegBila" runat="server" Visible="False" />
                                <%--Registro de contratos de reserva PTDVF--%>
                                <segas:RegContResPTDVF ID="regContResPTDVF" runat="server" Visible="False" />
                                <%--Inf Transaccional Gestor--%>
                                <segas:InfTransaccionalGestor ID="infTransaccionalGestor" runat="server" Visible="False" />
                                <%--Inf Transaccional Otras Transacciones del Mercado Mayorista--%>
                                <segas:InfTransOtrasTransMercadoMayorista ID="infTransOtrasTransMercadoMayorista" runat="server" Visible="False" />
                                <%--Registro de Contratos de Mercado Mayorista--%>
                                <segas:RegistroContratosMayorista ID="registroContratosMayorista" runat="server" Visible="False" />
                                <%--Archivo Negociaciones de Largo Plazo--%>
                                <segas:ArchivoNegLargoPlazo ID="archivoNegLargoPlazo" runat="server" Visible="False" />
                                <%--20200727 ajsute front-end--%>
                                <%--carga contrato PDF--%>
                                <segas:CargaContratoPdf ID="cargaContratoPdf" runat="server" Visible="False" />
                                <%--20200727 ajsute front-end--%>
                                <%--carga tasa de cambio--%>
                                <segas:CargaTasaCambioCont ID="cargaTasaCambioCont" runat="server" Visible="False" />
                                <%--20200925 bimestral c1--%>
                                <%--<segas:RegistroMinutaC1 ID="registroMinutaC1" runat="server" Visible="False" />--%>
                                <%--20220505 Desisitmienton--%>
                                <segas:DesistimientoContrato ID="desistimientoContrato" runat="server" Visible="False" />
                            </div>
                        </div>
                    </div>
                    <%--Registro contratos Extemporáneo--%>
                    <div id="acoRegContraExt" class="card" visible="False" runat="server">
                        <div class="card-header" id="headingRegConExt">
                            <h2 class="mb-0">
                                <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Button ID="btnRegContraExt" Text="Registro Contratos Extemporáneo" CssClass="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseRegConExt" aria-expanded="false" aria-controls="collapseRegConExt" OnClick="SelectTag" runat="server" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </h2>
                        </div>
                        <div id="collapseRegConExt" class="collapse" aria-labelledby="headingRegConExt" data-parent="#accordionRgCon">
                            <div class="card-body flex-fill">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group">
                                            <%--20210707--%>
                                            <p>A través de este módulo podrá declarar de manera extemporánea (fuera de los plazos establecidos por la regulación vigente), la información transaccional de los contratos suscritos en los mercados primario y secundario de suministro de gas natural y capacidad de transporte, en los términos de las Resoluciones CREG 185 y 186 de 2020 y todas aquellas que la modifiquen, adicionen o sustituyan.</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group">
                                            <label>Funcionalidad</label>
                                            <asp:DropDownList ID="ddlRegContraExt" runat="server" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="ddlRegContratosExt_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Archivo Bilateral Extemporáneo"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Inf Transaccional Extemporánea"></asp:ListItem>
                                                <%--20220221--%>
                                                <asp:ListItem Value="3" Text="Ingreso Contrato Bilateral Extemporáneo"></asp:ListItem>
                                                <%--20220221--%>
                                                <asp:ListItem Value="4" Text="Registro de Contrato Extemporáneo"></asp:ListItem>
                                                <%--20220221--%>
                                                <asp:ListItem Value="5" Text="Registro contrato Mayorista Extemporáneo"></asp:ListItem>
                                                <%--20220221--%>
                                                <asp:ListItem Value="6" Text="Carga contrato Mayorista Extemporáneo"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <%--Archivo Bilateral Extemporáneo--%>
                                <segas:CargaArchivoBilExt ID="cargaArchivoBilExt" runat="server" Visible="False" />
                                <%--Inf Transaccional Extemporánea--%>
                                <segas:InfTransExtemp ID="infTransExtemp" runat="server" Visible="False" />
                                <%--20220221--%>
                                <segas:ContratoBilateralExt ID="contratoBilateralExt" runat="server" Visible="False" />
                                <%--20220221--%>
                                <segas:RegistroContratoExt ID="registroContratoExt" runat="server" Visible="False" />
                                <%--20220221--%>
                                <segas:RegistroContratosMayoristaExt ID="registroContratosMayoristaExt" runat="server" Visible="False" />
                                <%--20220221--%>
                                <segas:InfTransMayExtemp ID="infTransMayExtemp" runat="server" Visible="False" />
                            </div>
                        </div>
                    </div>
                    <%--Modificación de Contratos--%>
                    <div id="acoModContra" class="card" visible="False" runat="server">
                        <div class="card-header" id="headingModCon">
                            <h2 class="mb-0">
                                <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Button ID="btnModContratos" Text="Modificación de Contratos" CssClass="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseModCon" aria-expanded="false" aria-controls="collapseModCon" OnClick="SelectTag" runat="server" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </h2>
                        </div>
                        <div id="collapseModCon" class="collapse" aria-labelledby="headingModCon" data-parent="#accordionRgCon">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group">
                                            <%--20210707--%>
                                            <p>A través de este módulo podrá actualizar los contratos de suministro de gas natural  y capacidad de transporte negociados en el mercado mayorista, en los casos en los que haya modificaciones a la información declarada por los vendedores y compradores al momento de registro del contrato.</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group">
                                            <label>Funcionalidad</label>
                                            <asp:DropDownList ID="ddlModContratos" runat="server" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="ddlModContratos_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Modificación de Contratos"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Archivo Modificación Contratos"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <%--Modificación Contratos--%>
                                <segas:ModContratos ID="modContratos" runat="server" Visible="False" />
                                <%--Archivo Modificación Contratos--%>
                                <segas:CargaModContratos ID="cargaModContratos" runat="server" Visible="False" />
                            </div>
                        </div>
                    </div>
                    <%--Modificación Contratos Cesión--%>
                    <div id="acoModContraCesion" class="card" visible="False" runat="server">
                        <div class="card-header" id="headingModContraCesion">
                            <h2 class="mb-0">
                                <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Button ID="btnModContraCesion" Text="Modificación Contratos Cesión" CssClass="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseModContraCesion" aria-expanded="false" aria-controls="collapseModContraCesion" OnClick="SelectTag" runat="server" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </h2>
                        </div>
                        <div id="collapseModContraCesion" class="collapse" aria-labelledby="headingModContraCesion" data-parent="#accordionRgCon">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group">
                                            <%--20210707--%>
                                            <p>A través de este módulo podrá actualizar los contratos de suministro de gas natural  y capacidad de transporte negociados en el mercado mayorista, en los casos en los que haya cesiones parciales o totales de los derechos contractuales.</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group">
                                            <label>Funcionalidad</label>
                                            <asp:DropDownList ID="ddlModContraCesion" runat="server" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="ddlModContraCesion_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Modificación Contratos Cesión"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Archivo Modificación Contratos Cesión"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <%--Modificación Contratos Cesión--%>
                                <segas:ModificaRegistroCession ID="modificaRegistroCession" runat="server" Visible="False" />
                                <%--Archivo Modificación Contratos Cesión--%>
                                <segas:CargaModificaRegistroCession ID="cargaModificaRegistroCession" runat="server" Visible="False" />
                            </div>
                        </div>
                    </div>
                    <%--Actualización Precios--%>
                    <div id="acoActOreInde" class="card" visible="False" runat="server">
                        <div class="card-header" id="headingActPreInd">
                            <h2 class="mb-0">
                                <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Button ID="btnActPreInd" Text="Actualización Precios Indexados" CssClass="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseActPreInd" aria-expanded="false" aria-controls="collapseActPreInd" OnClick="SelectTag" runat="server" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </h2>
                        </div>
                        <div id="collapseActPreInd" class="collapse" aria-labelledby="headingActPreInd" data-parent="#accordionRgCon">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group">
                                            <%--20210707--%>
                                            <p>A través de este módulo podrá actualizar los precios pactados en los contratos de suministro de gas natural, de acuerdo con lo establecido en la Resolución CREG 186 de 2020 y todas aquellas que la modifiquen, adicionen o sustituyan.</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group">
                                            <label>Funcionalidad</label>
                                            <asp:DropDownList ID="ddlActPreInd" runat="server" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="ddlActPreInd_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Actualización Precios Indexados"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Archivo Solicitud Cambio Precio"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <%--Actualización Precios Indexado--%>
                                <segas:ActPreciosIndexador ID="actPreciosIndexador" runat="server" Visible="False" />
                                <%--Archivo Solicitud Cambio Precio--%>
                                <segas:CargaSolCambioPrecio ID="cargaSolCambioPrecio" runat="server" Visible="False" />
                            </div>
                        </div>
                    </div>
                    <%--Eliminaciones--%>
                    <div id="acoEliMod" class="card" visible="False" runat="server">
                        <div class="card-header" id="headingSolEliCon">
                            <h2 class="mb-0">
                                <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Button ID="btnSolEliCon" Text="Eliminaciones" CssClass="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSolEliCon" aria-expanded="false" aria-controls="collapseSolEliCon" OnClick="SelectTag" runat="server" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </h2>
                        </div>
                        <div id="collapseSolEliCon" class="collapse" aria-labelledby="headingSolEliCon" data-parent="#accordionRgCon">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group">
                                            <p>A través de este módulo podrá gestionar la eliminación de aquellos contratos  de los mercados primario y secundario de suministro de gas  y capacidad de transporte que no hayan sido registrados en la plataforma</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group">
                                            <label>Funcionalidad</label>
                                            <asp:DropDownList ID="ddlSolEliCon" runat="server" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="ddlSolEliCon_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Solicitud Eliminación Contrato"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <%--Solicitud Eliminación Contrato--%>
                                <segas:SolEliContrato ID="solEliContrato" runat="server" Visible="False" />
                            </div>
                        </div>
                    </div>
                    <%--Proyección Demanda Regulada--%>
                    <div id="acoProDemReg" class="card" visible="False" runat="server">
                        <div class="card-header" id="headingProyeDemReg">
                            <h2 class="mb-0">
                                <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Button ID="btnProyeDemReg" Text="Proyección Demanda Regulada" CssClass="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseProyeDemReg" aria-expanded="false" aria-controls="collapseProyeDemReg" OnClick="SelectTag" runat="server" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </h2>
                        </div>
                        <div id="collapseProyeDemReg" class="collapse" aria-labelledby="headingProyeDemReg" data-parent="#accordionRgCon">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group">
                                            <p>A través de este módulo podrá registrar la proyección de la atención de la demanda regulada para el siguiente año de gas</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group">
                                            <label>Funcionalidad</label>
                                            <asp:DropDownList ID="ddlProyeDemReg" runat="server" CssClass="form-control selectpicker" OnSelectedIndexChanged="ddlProyeDemReg_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Value="0" Text="Seleccione" />
                                                <asp:ListItem Value="1" Text="Proyección Demanda Regulada" />
                                                <asp:ListItem Value="2" Text="Archivo Plano Proyección Demanda Regulada" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <%--Proyección Demanda Regulada--%>
                                <segas:ProyeDemandaRegulada ID="proyeDemandaRegulada" runat="server" Visible="False" />
                                <%--Archivo Plano Proyección Demanda Regulada--%>
                                <segas:CarguePlanoProyeDemaRegu ID="carguePlanoProyeDemaRegu" runat="server" Visible="False" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
