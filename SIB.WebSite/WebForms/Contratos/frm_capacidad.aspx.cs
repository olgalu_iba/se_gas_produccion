﻿/*
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 * Copyright (C) 2019 Bolsa Mercantil de Colombia, Todos los Derechos Reservados
 * Archivo:               frm_capacidad.cs
 * Tipo:                  Clase principal
 * Autor:                 Oscar Piñeros
 * Fecha creación:        2019 Agosto 02
 * Fecha modificación:    
 * Propósito:             Clase que gestiona los formularios de capacidad
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Presentacion;

namespace WebForms.Contratos
{
    // ReSharper disable once InconsistentNaming
    public partial class frm_capacidad : Page
    {
        #region Propiedades

        /// <summary>
        /// Propiedad donde se guardan los botones según el DropDownList
        /// </summary>
        private List<EnumBotones[]> BotonesArray
        {
            get
            {
                if (ViewState["BotonesArray"] != null)
                    return (List<EnumBotones[]>)ViewState["BotonesArray"];

                var arrayAcordion = new List<EnumBotones[]>();
                for (var i = 0; i < 8; i++)
                {
                    arrayAcordion.Add(new[] {
                        EnumBotones.Ninguno
                    });
                }
                return arrayAcordion;
            }
            set { ViewState["BotonesArray"] = value; }
        }

        /// <summary>
        /// Propiedad que almacena el nombre que busca para traer los permisos  
        /// </summary>
        private List<string> PermissionButtons
        {
            get
            {
                if (ViewState["PermissionButtons"] != null)
                    return (List<string>)ViewState["PermissionButtons"];

                var arrayPermissio = new List<string>();
                for (var i = 0; i < 8; i++)
                {
                    arrayPermissio.Add(string.Empty);
                }
                return arrayPermissio;
            }
            set { ViewState["PermissionButtons"] = value; }
        }

        #endregion Propiedades

        #region Inicializar

        /// <summary>
        /// Evento donde se cargarán los valores para su página 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Se cargan los valores iniciales del formulario 
                InfoSessionVO infoSessionVo = CargarPagina();
                if (IsPostBack) return;
                //Se inicializan los controles para la primera ves que se ejecuta el formulario 
                InicializarPagina();
                //Se cargan las funcionalidades
                CargarSubmenus(infoSessionVo);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Inicializa los componentes del formulario 
        /// </summary>
        private InfoSessionVO CargarPagina()
        {
            //Se redirecciona al Login si no hay una sesión iniciada    
            var goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("~/login.aspx");
            // ReSharper disable once PossibleNullReferenceException
            goInfo.Programa = "Capacidad";

            //Botones
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.CrearOnclick += CrearOnclick_Click;
            buttons.ExportarExcelOnclick += LkbExcel_Click;
            buttons.ExportarPdfOnclick += LkbPDF_Click;
            buttons.CargueOnclick += BtnCargar_Click;

            /*Capacidad*/

            //Capacidad Disponible Primaria
            capacidadDisponiblePrimaria.ToastrEvent += Toastr_Event;
            capacidadDisponiblePrimaria.LogCargaArchivoEvent += LogCargaArchivo_Event;
            capacidadDisponiblePrimaria.ButtonsEvent += Buttons_Event;

            //Capacidad Máxima Mediano Plazo
            capaMaxiMediPlazo.ToastrEvent += Toastr_Event;
            capaMaxiMediPlazo.ModalEvent += Modal_Event;
            capaMaxiMediPlazo.ButtonsEvent += Buttons_Event;

            //Informe Capacidad Disponible Primaria
            infoCapaDispoPrim.ToastrEvent += Toastr_Event;
            infoCapaDispoPrim.ButtonsEvent += Buttons_Event;

            return goInfo;
        }

        /// <summary>
        /// Inicializa el contenido del formulario 
        /// </summary>
        private void InicializarPagina()
        {
            //Titulo
            Master.Titulo = "Capacidad";
            //Descripcion
            //Master.DescripcionPagina = "Capacidad";
            //Botones
            buttons.Inicializar(botones: new[] { EnumBotones.Ninguno });

            /*Registro de contratos*/
            capacidadDisponiblePrimaria.NameController = capacidadDisponiblePrimaria.ID;
            capaMaxiMediPlazo.NameController = capaMaxiMediPlazo.ID;
            infoCapaDispoPrim.NameController = infoCapaDispoPrim.ID;
        }

        /// <summary>
        /// Carga la selección de funcionalidades según la región del acordeón 
        /// </summary>
        /// <param name="infoSessionVo"></param>
        private void CargarSubmenus(InfoSessionVO infoSessionVo)
        {
            // Se traen todos los submenús 
            var listaSubmenus = DelegadaBase.Servicios.consultarMenu(infoSessionVo, Convert.ToInt32(Request.QueryString["idInf"]));

            ddlCapacidad.Items.Clear();
            var lItem = new ListItem { Value = @"0", Text = @"Seleccione" };
            ddlCapacidad.Items.Add(lItem);


            if (listaSubmenus.Count <= 0)
            {
                Response.Redirect("~/WebForms/Home.aspx");
                Toastr.Info(this, "Lo sentimos, pero no tiene ninguna funcionalidad configurada para este menú");
            }

            for (var index = 0; index < listaSubmenus.Count; index++)
            {
                Hashtable subSubmenus = listaSubmenus[index];
                var lItem1 = new ListItem { Value = subSubmenus["codigo_menu"].ToString(), Text = subSubmenus["menu"].ToString() };
                ddlCapacidad.Items.Add(lItem1);
            }
        }

        #endregion Inicializar

        #region Eventos

        #region CURD Control

        /// <summary>
        /// Evento que controla el botón de crear 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrearOnclick_Click(object sender, EventArgs e)
        {
            if (ddlCapacidad.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCapMaxMedPlazo", CultureInfo.CurrentCulture)?.ToString()))
                // Capacidad Máxima Mediano Plazo
                capaMaxiMediPlazo.btnNuevo_Click(sender, e);
        }

        /// <summary>
        /// Evento que controla el botón de consultar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            if (ddlCapacidad.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCapMaxMedPlazo", CultureInfo.CurrentCulture)?.ToString()))
                //Informe Capacidad Disponible Primaria
                capaMaxiMediPlazo.btnConsultar_Click(sender, e);

            else if (ddlCapacidad.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idInfCapDispPrim", CultureInfo.CurrentCulture)?.ToString()))
                // Informe Capacidad Disponible Primaria
                infoCapaDispoPrim.btnConsultar_Click(sender, e);
        }

        /// <summary>
        /// Evento que controla el botón de cargar documentos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            if (ddlCapacidad.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCapDisPrim", CultureInfo.CurrentCulture)?.ToString()))
                //Capacidad Disponible Primaria
                capacidadDisponiblePrimaria.BtnCargar_Click(sender, e);
        }

        /// <summary>
        /// Evento que controla el botón de exportar a Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LkbExcel_Click(object sender, EventArgs e)
        {
            if (ddlCapacidad.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCapMaxMedPlazo", CultureInfo.CurrentCulture)?.ToString()))
                //Informe Capacidad Disponible Primaria
                capaMaxiMediPlazo.ImgExcel_Click(sender, e);
            //20200924 ajsute componente olga
            else if (ddlCapacidad.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idInfCapDispPrim", CultureInfo.CurrentCulture)?.ToString()))
                //Informe Capacidad Disponible Primaria
                infoCapaDispoPrim.btnExcel_Click (sender, e);
        }

        /// <summary>
        /// Evento que controla el botón de exportar a PDF
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LkbPDF_Click(object sender, EventArgs e)
        {
            if (ddlCapacidad.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCapMaxMedPlazo", CultureInfo.CurrentCulture)?.ToString()))
                //Informe Capacidad Disponible Primaria
                capaMaxiMediPlazo.ImgPdf_Click(sender, e);
        }

        /// <summary>
        /// Selecciona los botones según la carta del acordeón seleccionada
        /// </summary>
        protected void Buttons_Event(EnumBotones[] botones, DropDownList ddlSelect, string webController, string route = null)
        {
            //Si no trae el nombre del Wuc quiere decir que fue seleccionado desde el formulario principal, de
            //lo contrario se selecciona el ddl según el nombre del controlador.
            if (string.IsNullOrEmpty(webController))
            {
                SaveButtons(botones, ddlSelect, route);
                return;
            }

            /*Capacidad*/
            string[] arrayController = { "capacidadDisponiblePrimaria", "capaMaxiMediPlazo", "infoCapaDispoPrim" };
            foreach (string usController in arrayController)
            {
                if (!usController.Equals(webController)) continue;
                SaveButtons(botones, ddlCapacidad, route);
                return;
            }
        }

        /// <summary>
        /// Guarda y selecciona los botones según el formulario de contratos
        /// </summary>
        /// <param name="botones"></param>
        /// <param name="ddlSelect"></param>
        /// <param name="route"></param>
        private void SaveButtons(EnumBotones[] botones, DropDownList ddlSelect, string route = null)
        {
            if (string.IsNullOrEmpty(route))
            {
                buttons.Inicializar(ruta: route);
                route = PermissionButtons.ToArray()[ddlSelect.SelectedIndex - 1];
            }
            if (!string.IsNullOrEmpty(route))
            {
                buttons.Inicializar(ruta: route);
                // Se cargan los 
                string[] arrayPermission = PermissionButtons.ToArray();
                arrayPermission[ddlSelect.SelectedIndex - 1] = route;
                PermissionButtons = arrayPermission.ToList();
            }
            else if (botones != null)
            {
                // Se cargan los 
                EnumBotones[][] arrayFuncionalidad = BotonesArray.ToArray();
                arrayFuncionalidad[ddlSelect.SelectedIndex - 1] = botones;
                BotonesArray = arrayFuncionalidad.ToList();
            }
            else if (ddlSelect.SelectedIndex != 0)
            {
                botones = BotonesArray.ToArray()[ddlSelect.SelectedIndex - 1];
            }
            else
            {
                botones = new[] { EnumBotones.Ninguno };
            }
            buttons.SwitchOnButton(botones);
        }

        #region  Card DropDownList

        /// <summary>
        /// Selecciona la pantalla que desea visualizar en la capacidad
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlCapacidad_SelectedIndexChanged(object sender, EventArgs e)
        {
            capacidadDisponiblePrimaria.Visible = false;
            capaMaxiMediPlazo.Visible = false;
            infoCapaDispoPrim.Visible = false;

            // Capacidad Disponible Primaria
            if (ddlCapacidad.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCapDisPrim", CultureInfo.CurrentCulture)?.ToString()))
            {
                capacidadDisponiblePrimaria.Visible = true;
                // Se inicializa el formulario
                if (!capacidadDisponiblePrimaria.Inicializado)
                    capacidadDisponiblePrimaria.InicializarFormulario();
            }
            // Capacidad Máxima Mediano Plazo
            else if (ddlCapacidad.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCapMaxMedPlazo", CultureInfo.CurrentCulture)?.ToString()))
            {
                capaMaxiMediPlazo.Visible = true;
                // Se inicializa el formulario
                if (!capaMaxiMediPlazo.Inicializado)
                    capaMaxiMediPlazo.InicializarFormulario();
            }
            // Informe Capacidad Disponible Primaria
            else if (ddlCapacidad.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idInfCapDispPrim", CultureInfo.CurrentCulture)?.ToString()))
            {
                infoCapaDispoPrim.Visible = true;
                // Se inicializa el formulario
                if (!infoCapaDispoPrim.Inicializado)
                    infoCapaDispoPrim.InicializarFormulario();
            }
            //Se seleccionan los botones   
            Buttons_Event(null, ddlCapacidad, null);
        }

        #endregion Card DropDownList

        #endregion CURD Control

        /// <summary>
        /// Controla los Toastrs
        /// </summary>
        protected void Toastr_Event(string message, EnumTypeToastr typeToastr)
        {
            Toastr.SelectToastr(this, message, null, typeToastr);
        }

        /// <summary>
        /// Controla el log de errores de un cargue por archivo    
        /// </summary>
        protected void LogCargaArchivo_Event(StringBuilder message)
        {
            LogCargaArchivo.DownloadBugsLog(this, message);
        }

        /// <summary>
        /// Controla la apertura de los Modals
        /// </summary>
        /// <param name="id"></param>
        /// <param name="insideId"></param>
        /// <param name="typeModal"></param>
        protected void Modal_Event(string id, string insideId, EnumTypeModal typeModal)
        {
            Modal.SelectModal(this, id, insideId, typeModal);
        }

        #endregion Eventos
    }
}