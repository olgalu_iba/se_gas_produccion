﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ReporteEjecucionContrato.aspx.cs" Inherits="WebForms.Contratos.frm_ReporteEjecucionContrato" MasterPageFile="~/PlantillaPrincipal.master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitle" Text="Reporte Ejecución de Contratos" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="form-group">
                            <p>A través de este módulo podrá declarar la información de la ejecución diaria de las negociaciones directas pactadas en el mercado secundario bajo la modalidad contractual con interrupciones, de acuerdo a lo establecido en la Resolución CREG 021 de 2019 que modifica la Resolución CREG 114 de 2017.</p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="form-group">
                            <label>Funcionalidad</label>
                            <asp:DropDownList ID="ddlRepoEjeContratos" Style="z-index: 10000" runat="server" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="ddlRepoEjeContratos_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Ejecución de Contrato"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Carga Ejecución Contrato"></asp:ListItem>
                                <asp:ListItem Value="3" Text="Ejecución de Contrato Extemporáneo"></asp:ListItem>
                                <asp:ListItem Value="4" Text="Carga Ejecución Contrato Extemporáneo"></asp:ListItem>
                                <asp:ListItem Value="5" Text="Consulta Ejecución Contrato"></asp:ListItem>
                                <asp:ListItem Value="6" Text="Consulta Ejecución Contrato Usuario Final"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <%--Ejecución de contrato--%>
                <segas:EjecucionContrato ID="ejecucionContrato" runat="server" Visible="False" />
                <%--Carga Ejecución contrato--%>
                <segas:CargaEjecucionContrato ID="cargaEjecucionContrato" runat="server" Visible="False" />
                <%--Registro de Ejecución de Contratos Extemporáneo--%>
                <segas:EjecuContratoExtempo ID="ejecuContratoExtempo" runat="server" Visible="False" />
                <%--Carga Ejecución contrato extemporáneo--%>
                <segas:CargaEjeContratoExtem ID="cargaEjeContratoExtem" runat="server" Visible="False" />
                <%--Consulta Ejecución contrato--%>
                <segas:ConsultaEjecuContrato ID="consultaEjecuContrato" runat="server" Visible="False" />
                <%--Consulta Ejecución contrato Usuario Final--%>
                <segas:ConsultaEjecuContratoUsuFinal ID="consultaEjecuContratoUsuFinal" runat="server" Visible="False" />
            </div>
        </div>
    </div>

</asp:Content>
