﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_PTDV_PTDVF_CIDVF.aspx.cs" Inherits="WebForms.Contratos.frm_PTDV_PTDVF_CIDVF" MasterPageFile="~/PlantillaPrincipal.master" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" enctype="multipart/form-data" method="post" runat="Server">

    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitle" Text="PTDVF y CIDVF" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="form-group">
                            <%--20210804 modificacion ptdvf--%>
                            <p>A través de este módulo los productores-comercializadores y los comercializadores de gas importado podrán declarar la Producción Total Disponible para la Venta en Firme (PTDVF) y la Cantidad Importada Disponible para la Venta en Firme (CIDVF), respectivamente; de acuerdo a lo establecido en la normatividad vigente.</p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="form-group">
                            <label>Funcionalidad</label>
                            <asp:DropDownList ID="ddlPtdCidv" Style="z-index: 10000" runat="server" CssClass="form-control selectpicker" OnSelectedIndexChanged="ddlPtdCidv_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="1" Text="PTDVF y CIDVF"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Reporte Información PTDVF y CIDVF"></asp:ListItem>
                                <asp:ListItem Value="3" Text="Carga Archivo PTDV"></asp:ListItem>
                                <%--20220118--%>
                                <asp:ListItem Value="4" Text="Carga Archivo PTDV Adminsitrador"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <%--PTDVF y CIDVF--%>
                <segas:PtdvfCidvf ID="ptdvfCidvf" runat="server" Visible="False" />
                <%--Reporte Información PTDVF y CIDVF--%>
                <segas:RepoInfoPtdvfCidvf ID="repoInfoPtdvfCidvf" runat="server" Visible="False" />
                <%--Carga Archivo PTDV--%>
                <segas:CargaArchivoPTDV ID="cargaArchivoPTDV" runat="server" Visible="False" />
                <%--20220118--%>
                <segas:CargaArchivoPTDVAdm ID="cargaArchivoPTDVAdm" runat="server" Visible="False" />
            </div>
        </div>
    </div>
</asp:Content>
