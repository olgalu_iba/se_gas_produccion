﻿/*
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 * Copyright (C) 2019 Bolsa Mercantil de Colombia, Todos los Derechos Reservados
 * Archivo:               frm_ReporteEjecucionContrato.cs
 * Tipo:                  Clase principal
 * Autor:                 Oscar Piñeros
 * Fecha creación:        2019 Agosto 02
 * Fecha modificación:    
 * Propósito:             Clase que gestiona los formularios de reporte ejecución de contratos
 ---------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Presentacion;

namespace WebForms.Contratos
{
    /// <summary>
    /// 
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public partial class frm_ReporteEjecucionContrato : Page
    {
        #region Propiedades

        /// <summary>
        /// Propiedad donde se guardan los botones según el DropDownList
        /// </summary>
        private List<EnumBotones[]> BotonesArray
        {
            get
            {
                if (ViewState["BotonesArray"] != null)
                    return (List<EnumBotones[]>)ViewState["BotonesArray"];

                var arrayAcordion = new List<EnumBotones[]>();
                for (var i = 0; i < 8; i++)
                {
                    arrayAcordion.Add(new[] {
                        EnumBotones.Ninguno
                    });
                }
                return arrayAcordion;
            }
            set { ViewState["BotonesArray"] = value; }
        }

        #endregion Propiedades

        #region Inicializar 

        /// <summary>
        ///El evento de carga de la página se activa antes del evento de carga para cualquier control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Se cargan los valores iniciales del formulario 
                InfoSessionVO infoSessionVo = CargarPagina();
                if (IsPostBack) return;
                //Se inicializan los controles para la primera ves que se ejecuta el formulario 
                InicializarPagina();
                //Se cargan las funcionalidades
                CargarSubmenus(infoSessionVo);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Inicializa los componentes del formulario 
        /// </summary>
        private InfoSessionVO CargarPagina()
        {
            //Se redirecciona al Login si no hay una sesión iniciada    
            var goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("~/login.aspx");
            // ReSharper disable once PossibleNullReferenceException
            goInfo.Programa = "ReporteEjecucionContrato";

            //Botones
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ExportarExcelOnclick += LkbExcel_Click;
            buttons.CargueOnclick += BtnCargar_Click;

            /*Reporte Ejecución de Contratos*/

            // Ejecución de contrato
            ejecucionContrato.ToastrEvent += Toastr_Event;
            ejecucionContrato.ModalEvent += Modal_Event;
            ejecucionContrato.ButtonsEvent += Buttons_Event;
            // Carga Ejecución contrato
            cargaEjecucionContrato.ToastrEvent += Toastr_Event;
            cargaEjecucionContrato.ModalEvent += Modal_Event;
            cargaEjecucionContrato.LogCargaArchivoEvent += LogCargaArchivo_Event;
            cargaEjecucionContrato.ButtonsEvent += Buttons_Event;
            // Ejecución de contrato extemporáneo
            ejecuContratoExtempo.ToastrEvent += Toastr_Event;
            ejecuContratoExtempo.ModalEvent += Modal_Event;
            ejecuContratoExtempo.ButtonsEvent += Buttons_Event;
            //Carga Ejecución contrato extemporáneo
            cargaEjeContratoExtem.ToastrEvent += Toastr_Event;
            cargaEjeContratoExtem.ModalEvent += Modal_Event;
            cargaEjeContratoExtem.LogCargaArchivoEvent += LogCargaArchivo_Event;
            cargaEjeContratoExtem.ButtonsEvent += Buttons_Event;
            //Consulta Ejecución contrato
            consultaEjecuContrato.ToastrEvent += Toastr_Event;
            consultaEjecuContrato.ModalEvent += Modal_Event;
            consultaEjecuContrato.ButtonsEvent += Buttons_Event;
            //Consulta Ejecución contrato Usuario Final
            consultaEjecuContratoUsuFinal.ToastrEvent += Toastr_Event;
            consultaEjecuContratoUsuFinal.ModalEvent += Modal_Event;
            consultaEjecuContratoUsuFinal.ButtonsEvent += Buttons_Event;

            return goInfo;
        }

        /// <summary>
        /// Inicializa el contenido del formulario 
        /// </summary>
        private void InicializarPagina()
        {
            //Titulo
            Master.Titulo = "Contratos";
            //Descripcion
            //Master.DescripcionPagina = "Capacidad";
            //Botones
            buttons.Inicializar(botones: new[] { EnumBotones.Ninguno });

            /*Reporte Ejecución de Contratos*/
            ejecucionContrato.NameController = ejecucionContrato.ID;
            cargaEjecucionContrato.NameController = cargaEjecucionContrato.ID;
            ejecuContratoExtempo.NameController = ejecuContratoExtempo.ID;
            cargaEjeContratoExtem.NameController = cargaEjeContratoExtem.ID;
            consultaEjecuContrato.NameController = consultaEjecuContrato.ID;
            consultaEjecuContratoUsuFinal.NameController = consultaEjecuContratoUsuFinal.ID;
        }

        /// <summary>
        /// Carga la selección de funcionalidades según la región del acordeón 
        /// </summary>
        /// <param name="infoSessionVo"></param>
        private void CargarSubmenus(InfoSessionVO infoSessionVo)
        {
            // Se traen todos los submenús 
            List<Hashtable> listaSubmenus = DelegadaBase.Servicios.consultarMenu(infoSessionVo, Convert.ToInt32(Request.QueryString["idInf"]));

            ddlRepoEjeContratos.Items.Clear();
            var lItem = new ListItem { Value = @"0", Text = @"Seleccione" };
            ddlRepoEjeContratos.Items.Add(lItem);


            if (listaSubmenus.Count <= 0)
            {
                Response.Redirect("~/WebForms/Home.aspx");
                Toastr.Info(this, "Lo sentimos, pero no tiene ninguna funcionalidad configurada para este menú");
            }

            for (var index = 0; index < listaSubmenus.Count; index++)
            {
                Hashtable subSubmenus = listaSubmenus[index];
                var lItem1 = new ListItem { Value = subSubmenus["codigo_menu"].ToString(), Text = subSubmenus["menu"].ToString() }; //20200924 Ajuste Componente
                ddlRepoEjeContratos.Items.Add(lItem1);
            }
        }

        #endregion Inicializar

        #region Eventos

        #region CURD Control

        /// <summary>
        /// Evento que controla el botón de consultar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            if (ddlRepoEjeContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idEjecuciónContrato", CultureInfo.CurrentCulture)?.ToString()))
                // Ejecución de contrato
                ejecucionContrato.btnConsultar_Click(sender, e);
            else if (ddlRepoEjeContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idEjecContExt", CultureInfo.CurrentCulture)?.ToString()))
                // Ejecución de contrato extemporáneo
                ejecuContratoExtempo.btnConsultar_Click(sender, e);
            else if (ddlRepoEjeContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idConsEjecCont", CultureInfo.CurrentCulture)?.ToString()))
                // Consulta Ejecución contrato
                consultaEjecuContrato.btnConsultar_Click(sender, e);
            else if (ddlRepoEjeContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idConsEjecContUsuFinal", CultureInfo.CurrentCulture)?.ToString()))
                // Consulta Ejecución contrato Usuario Final
                consultaEjecuContratoUsuFinal.btnConsultar_Click(sender, e);
        }

        /// <summary>
        /// Evento que controla el botón de cargar documentos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            if (ddlRepoEjeContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaEjecCont", CultureInfo.CurrentCulture)?.ToString()))
                // Carga Ejecución contrato
                cargaEjecucionContrato.BtnCargar_Click(sender, e);
            if (ddlRepoEjeContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaEjecContExt", CultureInfo.CurrentCulture)?.ToString()))
                // Carga Ejecución contrato extemporánea
                cargaEjeContratoExtem.BtnCargar_Click(sender, e);
        }

        /// <summary>
        /// Evento que controla el botón de exportar a Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LkbExcel_Click(object sender, EventArgs e)
        {
            if (ddlRepoEjeContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idEjecuciónContrato", CultureInfo.CurrentCulture)?.ToString()))
                // Ejecución de contrato
                ejecucionContrato.lkbExcel_Click(sender, e);
            if (ddlRepoEjeContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idEjecContExt", CultureInfo.CurrentCulture)?.ToString()))
                // Ejecución de contrato extemporáneo
                ejecuContratoExtempo.lkbExcel_Click(sender, e);
            if (ddlRepoEjeContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idConsEjecCont", CultureInfo.CurrentCulture)?.ToString()))
                // Consulta Ejecución contrato
                consultaEjecuContrato.btnExcel_Click(sender, e);
            if (ddlRepoEjeContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idConsEjecContUsuFinal", CultureInfo.CurrentCulture)?.ToString()))
                // Consulta Ejecución contrato Usuario Final
                consultaEjecuContratoUsuFinal.btnExcel_Click(sender, e);
        }

        /// <summary>
        /// Selecciona los botones según la carta del acordeón seleccionada
        /// </summary>
        protected void Buttons_Event(EnumBotones[] botones, DropDownList ddlSelect, string webController)
        {
            //Si no trae el nombre del Wuc quiere decir que fue seleccionado desde el formulario principal, de
            //lo contrario se selecciona el ddl según el nombre del controlador.

            if (string.IsNullOrEmpty(webController))
            {
                SaveButtons(botones, ddlSelect);
                return;
            }

            /*Registro de contratos*/
            string[] arrayController = { "ejecucionContrato", "cargaEjecucionContrato", "ejecuContratoExtempo", "cargaEjeContratoExtem", "consultaEjecuContrato", "consultaEjecuContratoUsuFinal" };
            foreach (string usController in arrayController)
            {
                if (!usController.Equals(webController)) continue;
                SaveButtons(botones, ddlRepoEjeContratos);
                return;
            }
        }

        /// <summary>
        /// Guarda y selecciona los botones según el formulario de contratos
        /// </summary>
        /// <param name="botones"></param>
        /// <param name="ddlSelect"></param>
        private void SaveButtons(EnumBotones[] botones, DropDownList ddlSelect)
        {
            if (botones != null)
            {
                // Se cargan los 
                EnumBotones[][] arrayFuncionalidad = BotonesArray.ToArray();
                arrayFuncionalidad[ddlSelect.SelectedIndex - 1] = botones; //20200924 ajsute compoenente
                BotonesArray = arrayFuncionalidad.ToList();
            }
            else if (!ddlSelect.SelectedValue.Equals("0"))
            {
                botones = BotonesArray.ToArray()[ddlSelect.SelectedIndex - 1];//20200924 ajsute compoenente agregarsado por Olga
            }
            else
            {
                EnumBotones[] ninguno = { EnumBotones.Ninguno };
                botones = ninguno;
            }
            buttons.SwitchOnButton(botones);
        }

        #region  Card DropDownList

        /// <summary>
        /// Selecciona la pantalla que desea visualizar en el reporte de ejecución de contratos
        /// </summary>
        /// 20200924 ajsute compoenente
        protected void ddlRepoEjeContratos_SelectedIndexChanged(object sender, EventArgs e)
        {
            ejecucionContrato.Visible = false;

            cargaEjecucionContrato.Visible = false;

            ejecuContratoExtempo.Visible = false;

            cargaEjeContratoExtem.Visible = false;

            consultaEjecuContrato.Visible = false;

            consultaEjecuContratoUsuFinal.Visible = false;




            if (ddlRepoEjeContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idEjecuciónContrato", CultureInfo.CurrentCulture)?.ToString()))

            {

                ejecucionContrato.Visible = true;

                // Se inicializa el formulario

                if (!ejecucionContrato.Inicializado)

                    ejecucionContrato.InicializarFormulario();

            }

            if (ddlRepoEjeContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaEjecCont", CultureInfo.CurrentCulture)?.ToString()))

            {

                cargaEjecucionContrato.Visible = true;

                // Se inicializa el formulario

                if (!cargaEjecucionContrato.Inicializado)

                    cargaEjecucionContrato.InicializarFormulario();

            }

            else if (ddlRepoEjeContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idEjecContExt", CultureInfo.CurrentCulture)?.ToString()))

            {

                ejecuContratoExtempo.Visible = true;

                // Se inicializa el formulario

                if (!ejecuContratoExtempo.Inicializado)

                    ejecuContratoExtempo.InicializarFormulario();

            }

            else if (ddlRepoEjeContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idCargaEjecContExt", CultureInfo.CurrentCulture)?.ToString()))

            {

                cargaEjeContratoExtem.Visible = true;

                // Se inicializa el formulario

                if (!cargaEjeContratoExtem.Inicializado)

                    cargaEjeContratoExtem.InicializarFormulario();

            }

            else if (ddlRepoEjeContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idConsEjecCont", CultureInfo.CurrentCulture)?.ToString()))

            {

                consultaEjecuContrato.Visible = true;

                // Se inicializa el formulario

                if (!consultaEjecuContrato.Inicializado)

                    consultaEjecuContrato.InicializarFormulario();

            }

            else if (ddlRepoEjeContratos.SelectedValue.Equals(HttpContext.GetGlobalResourceObject("App_Configuration", "idConsEjecContUsuFinal", CultureInfo.CurrentCulture)?.ToString()))

            {

                consultaEjecuContratoUsuFinal.Visible = true;

                // Se inicializa el formulario

                if (!consultaEjecuContratoUsuFinal.Inicializado)

                    consultaEjecuContratoUsuFinal.InicializarFormulario();

            }




            //Se seleccionan los botones   

            Buttons_Event(null, ddlRepoEjeContratos, null);
        }


        #endregion Card DropDownList

        #endregion CURD Control

        /// <summary>
        /// Controla los Toastrs
        /// </summary>
        protected void Toastr_Event(string message, EnumTypeToastr typeToastr)
        {
            Toastr.SelectToastr(this, message, null, typeToastr);
        }

        /// <summary>
        /// Controla el log de errores de un cargue por archivo    
        /// </summary>
        protected void LogCargaArchivo_Event(StringBuilder message)
        {
            LogCargaArchivo.DownloadBugsLog(this, message);
        }

        /// <summary>
        /// Controla la apertura de los Modals
        /// </summary>
        /// <param name="id"></param>
        /// <param name="insideId"></param>
        /// <param name="typeModal"></param>
        protected void Modal_Event(string id, string insideId, EnumTypeModal typeModal)
        {
            Modal.SelectModal(this, id, insideId, typeModal);
        }

        #endregion Eventos
    }
}