﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_menu.aspx.cs" Inherits="frm_menu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="css/estilo.css" rel="stylesheet" type="text/css" />
</head>
<body background="Images/IMG_FondoAzul.png">
    <form id="form1" runat="server">
    <table width="100%">
        <tr>
            <td align="center" class="th1">
                MENU
            </td>
        </tr>
        <tr>
            <td>
                <asp:TreeView ID="tvMenu" runat="server" ImageSet="Arrows" LineImagesFolder="~/TreeLineImages"
                    ExpandDepth="0">
                    <RootNodeStyle Font-Bold="true" ForeColor="#85BF46" Font-Size="12px" />
                    <ParentNodeStyle Font-Bold="true" ForeColor="#004FA5" Font-Size="12px" />
                    <NodeStyle Font-Bold="true" ForeColor="#004FA5" Font-Size="12px" />
                    <HoverNodeStyle Font-Underline="True" ForeColor="#004FA5" />
                    <SelectedNodeStyle Font-Underline="True" HorizontalPadding="0px" VerticalPadding="0px"
                        ForeColor="#5555DD" />
                    <%--    <Nodes>
                        <asp:TreeNode SelectAction="None" Text="MENU" Value="Base" Expanded="True">
                            <asp:TreeNode SelectAction="None" Text="Base" Value="Base" Expanded="True">
                                <asp:TreeNode SelectAction="None" Text="Definiciones de Negocio" Value="Definiciones de Negocio"
                                    Expanded="True">
                                    <asp:TreeNode NavigateUrl="~/Default.aspx" Target="contenido" 
                                        Text="Compania" Value="Compania"></asp:TreeNode>
                                    <asp:TreeNode NavigateUrl="~/BASE/frm_ActividadEconomica.aspx" Target="contenido"
                                        Text="Actividad Economica" Value="Actividad Economica"></asp:TreeNode>
                                </asp:TreeNode>
                            </asp:TreeNode>
                            <asp:TreeNode SelectAction="None" Text="OPE" Value="OPE" Expanded="True">
                                <asp:TreeNode Text="Listar Ope" Value="Listar Ope"></asp:TreeNode>
                            </asp:TreeNode>
                        </asp:TreeNode>
                    </Nodes>
                    <NodeStyle Font-Names="Tahoma" Font-Size="10pt" ForeColor="Black" 
                        HorizontalPadding="5px" NodeSpacing="0px" VerticalPadding="0px" />--%>
                </asp:TreeView>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
