﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class frm_contenido : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LblMensaje.Text  = "";
        int oDias = Convert.ToInt16(Session["DiasExp"].ToString());
        if (oDias <= 15)
        {
            LblMensaje.Text  = "La contraseña expirará en " + oDias.ToString() + " dias.!!!";
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + LblMensaje.Text + "');", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + LblMensaje.Text + "');", true);
        }
    }
}
