﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_tramos : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Tramos";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    SqlDataReader lLector;
    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        
        if (!IsPostBack)
        {
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlTrasportador, "m_operador", " tipo_operador ='T' and estado = 'A' order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlBusTrasportador, "m_operador", " tipo_operador ='T' and estado = 'A' order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlPozoIni, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlPozoFin, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlBusPozoIni, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlBusPozoFin, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlTipoProyecto, "m_tipo_proyecto", " estado = 'A' order by descripcion", 0, 1); // Campo nuevo Req. 005-2021 20210120
            lConexion.Cerrar();

            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_tramo");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[13].Visible = (Boolean)permisos["UPDATE"]; // Ajuste Indice Req. 079 Mapa Interactivo 20170314 //20170705 rq025-17 // Ajuste Req. 005-2021 20210120//20221012
        dtgMaestro.Columns[14].Visible = false;  // Ajuste Indice Req. 079 Mapa Interactivo 20170314 //20170705 rq025-17 // Ajuste Req. 005-2021 20210120//20221012
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoTramo.Visible = false;
        LblCodigoTramo.Visible = true;
        LblCodigoTramo.Text = "Automático";  //20170705 rq025-17
        ddlTrasportador.Enabled = true;
        ddlPozoIni.Enabled = true;
        ddlPozoFin.Enabled = true;
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_tramo ", " codigo_tramo = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoTramo.Text = lLector["codigo_tramo"].ToString();
                        TxtCodigoTramo.Text = lLector["codigo_tramo"].ToString();
                        ddlExcentoUvlp.SelectedValue = lLector["excento_uvlp"].ToString();
                        try
                        {
                            ddlTrasportador.SelectedValue = lLector["codigo_trasportador"].ToString();
                        }
                        catch (Exception)
                        {
                            lblMensaje.Text += "El trasportador del registro no existe o esta inactivo<br>";
                        }
                        try
                        {
                            ddlPozoIni.SelectedValue = lLector["codigo_pozo_ini"].ToString();
                        }
                        catch (Exception)
                        {
                            lblMensaje.Text += "El Punto Inicial del registro no existe o esta inactivo<br>";
                        }
                        try
                        {
                            ddlPozoFin.SelectedValue = lLector["codigo_pozo_fin"].ToString();
                        }
                        catch (Exception)
                        {
                            lblMensaje.Text += "El Punto Final del registro no existe o esta inactivo<br>";
                        }
                        ddlPublicaMapa.SelectedValue = lLector["ind_pub_bec_mapa"].ToString(); //Campo nuevo Req. 079 Mapa Interactivo 20170314
                        ddlFlujo.SelectedValue = lLector["ind_tipo_tramo"].ToString();  //20170705 rq025-17 indicadres MP fase II
                        // Campo nuevo Req. 005-2021 20210120 //
                        try
                        {
                            ddlTipoProyecto.SelectedValue = lLector["codigo_tipo_proyecto"].ToString();
                        }
                        catch (Exception)
                        {
                            lblMensaje.Text += "El Tipo Proyecto del registro no existe o esta inactivo<br>";
                        }
                        /// Hasta Aqui
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoTramo.Visible = false;
                        LblCodigoTramo.Visible = true;
                        //20221012
                        try
                        {
                            ddlMoneda.SelectedValue = lLector["tipo_moneda"].ToString();
                        }
                        catch (Exception)
                        {
                            ddlMoneda.SelectedValue = "USD";
                        }
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado. Codigo Tramo " + modificar.ToString();

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_tramo", "@P_codigo_trasportador", "@P_codigo_pozo_ini", "@P_codigo_pozo_fin", "@P_estado" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar};
        string[] lValorParametros = { "0", ddlBusTrasportador.SelectedValue, ddlBusPozoIni.SelectedValue, ddlBusPozoFin.SelectedValue, "" };

        try
        {
            if (TxtBusCodigoTramo.Text.Trim().Length > 0)
                lValorParametros[0] = TxtBusCodigoTramo.Text.Trim();

            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetTramo", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_tramo", "@P_codigo_trasportador", "@P_codigo_pozo_ini", "@P_codigo_pozo_fin", "@P_estado", "@P_accion", "@P_excento_uvlp",
                                        "@P_ind_pub_bec_mapa", // Campo nuevo Req. 079 Mapa Interactivo 20170314
                                        "@P_ind_tipo_tramo", //20170705 rq025-17 indicadres MP fase III
                                        "@P_codigo_tipo_proyecto", // Campo nuevo Req. 005-2021 20210120
                                        "@P_tipo_moneda" //20221012
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar,
                                        SqlDbType.VarChar, // Campo nuevo Req. 079 Mapa Interactivo 20170314
                                        SqlDbType.VarChar, //20170705 rq025-17 indicadres MP fase III
                                        SqlDbType.Int, // Campo nuevo Req. 005-2021 20210120
                                        SqlDbType.VarChar //20221012 
                                      };
        string[] lValorParametros = { "0", "0", "0", "0", "0", "1", ddlExcentoUvlp.SelectedValue,
                                      ddlPublicaMapa.SelectedValue, // Campo nuevo Req. 079 Mapa Interactivo 20170314
                                      ddlFlujo.SelectedValue, //20170705 rq025-17 indicadres MP fase III
                                      ddlTipoProyecto.SelectedValue, // Campo nuevo Req. 005-2021 20210120
                                      ddlMoneda.SelectedValue //20221012
                                    };
        lblMensaje.Text = "";

        try
        {
            if (ddlTrasportador.SelectedValue == "0")
                lblMensaje.Text += "debe seleccionar el trasportador<br>";
            if (ddlPozoIni.SelectedValue == "0")
                lblMensaje.Text += "debe seleccionar el punto Inicial<br>";
            if (ddlPozoFin.SelectedValue == "0")
                lblMensaje.Text += "debe seleccionar el punto Final<br>";
            if (ddlPozoIni.SelectedValue == ddlPozoFin.SelectedValue)
                lblMensaje.Text += "Los puntos inicial y final del tramo deben ser distintos <br>";
            if (VerificarExistencia(" codigo_pozo_ini= " + ddlPozoIni.SelectedValue + " and codigo_pozo_fin =" + ddlPozoFin.SelectedValue))
                lblMensaje.Text += "El tramo formado por los puntos seleccionados ya está definido<br>";
            // Campo Nuevo Req. 005-2021 20210120
            if (ddlTipoProyecto.SelectedValue == "0")
                lblMensaje.Text += "debe seleccionar el tipo de proyecto<br>";
            if (lblMensaje.Text == "")
            {
                lValorParametros[1] = ddlTrasportador.SelectedValue;
                lValorParametros[2] = ddlPozoIni.SelectedValue;
                lValorParametros[3] = ddlPozoFin.SelectedValue;
                lValorParametros[4] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetTramo", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Creación del tramo.!";
                    lConexion.Cerrar();
                }
                else
                    Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_tramo", "@P_estado", "@P_accion", "@P_excento_uvlp",
                                        "@P_ind_pub_bec_mapa", // Campo nuevo Req. 079 Mapa Interactivo 20170314
                                        "@P_ind_tipo_tramo", //20170705 rq025-17 indicadres MP fase III
                                        "@P_codigo_tipo_proyecto", // Campo nuevo Req. 005-2021 20210120
                                        "@P_tipo_moneda" //20221012
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar,
                                        SqlDbType.VarChar, // Campo nuevo Req. 079 Mapa Interactivo 20170314
                                        SqlDbType.VarChar, //20170705 rq025-17 indicadres MP fase III
                                        SqlDbType.Int, // Campo nuevo Req. 005-2021 20210120
                                        SqlDbType.VarChar //20221012
                                      };
        string[] lValorParametros = { "0", "", "2", ddlExcentoUvlp.SelectedValue,
                                      ddlPublicaMapa.SelectedValue, // Campo nuevo Req. 079 Mapa Interactivo 20170314
                                      ddlFlujo.SelectedValue, //20170705 rq025-17 indicadres MP fase III
                                      ddlTipoProyecto.SelectedValue, // Campo nuevo Req. 005-2021 20210120
                                      ddlMoneda.SelectedValue //20221012
                                    };
        lblMensaje.Text = "";
        try
        {
            // Campo Nuevo Req. 005-2021 20210120
            if (ddlTipoProyecto.SelectedValue == "0")
                lblMensaje.Text += "debe seleccionar el tipo de proyecto<br>";
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = LblCodigoTramo.Text;
                lValorParametros[1] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetTramo", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Actualizacion del tramo.!";
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigoTramo.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoTramo.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (LblCodigoTramo.Text != "")
            manejo_bloqueo("E", LblCodigoTramo.Text);
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        //20160907  subasta UVLP
        if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[7].Text != "E")
        {
            if (((LinkButton)e.CommandSource).Text == "Modificar")
            {
                lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                ddlTrasportador.Enabled = false;
                ddlPozoIni.Enabled = false;
                ddlPozoFin.Enabled = false;
                Modificar(lCodigoRegistro);
            }
        }
        else
        {
            lblMensaje.Text = "Los tramos especiales no se pueden modificar por esta opción";
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("m_tramo", lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_tramo' and llave_registro='codigo_tramo=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_tramo=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_tramo";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_tramo", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "0", "0", "0", "" };
        string lsParametros = "";

        try
        {
            if (TxtBusCodigoTramo.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusCodigoTramo.Text.Trim();
                lsParametros += " Codigo Tramo : " + TxtBusCodigoTramo.Text;
            }
            if (ddlBusTrasportador.SelectedValue != "0")
            {
                lValorParametros[1] = ddlBusTrasportador.SelectedValue;
                lsParametros += " Trasportador : " + ddlBusTrasportador.SelectedItem;
            }
            if (ddlBusPozoIni.SelectedValue != "0")
            {
                lValorParametros[2] = ddlBusPozoIni.SelectedValue;
                lsParametros += " punto ini : " + ddlBusPozoIni.SelectedItem;
            }
            if (ddlBusPozoFin.SelectedValue != "0")
            {
                lValorParametros[3] = ddlBusPozoFin.SelectedValue;
                lsParametros += " punto fin : " + ddlBusPozoFin.SelectedItem;
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetTramo&nombreParametros=@P_codigo_tramo*@P_codigo_trasportador*@P_codigo_pozo_ini*@P_codigo_pozo_fin*@P_estado&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "*" + lValorParametros[3] + "*" + lValorParametros[4] + "&columnas=codigo_tramo*codigo_trasportador*nombre_trasportador*codigo_pozo_ini*desc_pozo_ini*codigo_pozo_fin*desc_pozo_fin*desc_flujo*tipo_proyecto*tipo_moneda*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado Tramos&TituloParametros=" + lsParametros); //20170705 rq025-17 indicadores MP d¡fase III // Campo nuevo Req. 005-2021 20210120 //20221012
        }
        catch (Exception)
        {
            lblMensaje.Text = "No se Pud0 Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_tramo <> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_tramo&procedimiento=pa_ValidarExistencia&columnas=codigo_tramo*codigo_trasportador*codigo_pozo_ini*codigo_pozo_fin*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }
    }
}